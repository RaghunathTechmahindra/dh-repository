package mosApp.MosSystem;

/**
 * 09.Mar.2009 DVG #DG826 LEN357820: 	Irene Gatto System administrator error on deal 2454
 * 29/Sep/2008 DVG #DG758 LEN322725: Irena Sakic Deal is locked - user is confused as to why  
 * 16/May/2006 DVG #DG420 #3152  MYNEXT - Disable Auto Adjudication screen
 * 28-Oct-2008 FXP22615 Reverted back to version 5529 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.duplicate.DupeCheck;
import com.basis100.deal.duplicate.DuplicateFinder;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.FinderException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.TiledViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;

/**
 * <p>Title: DealHandlerCommon</p>
 * <p>Description: Common class for all deal handling activities.</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 *
 * @version 1.11  <br>
 * Date: 08/02/2006 <br>
 * @author: NBC/PP Implementation Team<br>
 * Change:  <br>
 *	added methods to handle insert/update requests for new identification <br>
 *	- added following methods <br>
 *		handleAddIdentificationType()<br>
 *		handleAddIdentificationType(Borrower borrower, CalcMonitor dcm)<br>
 *		handleUpdateIdentification(String propName, String[] keyValues, 
 *		CalcMonitor dcm)<br>
 * @version 1.12  <br>
 * Date: June 17 2008<br>
 * @author: MCM Implementation Team<br>
 * Change:  <br>
 *	added methods to handle submit from the component details screen<br>
 *	- added following methods <br>
 *		handleDECSubmit()<br>
 *		handleDECSubmit(boolean confirmed)<br>
 * @version 1.12(July 4, 2008): fixed handleDECSubmit for artf732615
 * @author: MCM Implementation Team
 * @version 1.13(July 10, 2008): XS 2.39 modified handleDECSubmit for DEC
 * @author: MCM Implementation Team
 * @version 1.14(July 14, 2008): XS 2.47 and 2.39 modified validateDealEntry for DEC Rules 
 * @author: MCM Implementation Team
 * @version 1.15(July 16, 2008): XS 2.47: modified Error/Warning popup display condition based on artf743632
 * @version 1.16(July 18, 2008): XS 2.32: delete component modified: handleDelete(Object obj)
 * @version 1.17(July 24, 2008): XS 2.40: updated validateComponent() for adding DMC Rules.
 * @version 1.18(July 29, 2008): XS 2.40: updated validateDealEntry() for firing DMC rules. 
 * @version 1.19(July 29, 2008): XS 2.60: updated to handle pagelet and TileView to save data. 
 * @version 1.20(Aug 1, 2008): artf751756: changed handleDelete component.ejbRemove(true) from false
 * @version 1.21(Aug 6, 2008): XS_2.48: Removed the call of DCC- businessrules.   
 * @version 1.22(Oct 7, 2008): FXP22741, changed business rule validation for compoents.
 * @version 1.23(Oct 10, 2008): Modified validateComponents method
 * @author: MCM Implementation Team
 * 
 */
public abstract class DealHandlerCommon extends PageHandlerCommon
implements Cloneable, Sc
{

    private final static Log _log = LogFactory.getLog(DealHandlerCommon.class);
    //***** Change by NBC/PP Implementation Team - Version 1.6 - Start *****//
    protected static final String CHANGED_ESTIMATED_CLOSING_DATE_FLAG = "Y";
    //***** Change by NBC/PP Implementation Team - Version 1.6 - End *****//
    /**
     * Special usage of DealHandlerCommon PageEntry members:
     * 
     *   pageCondition1 is used to trigger "IS_COMMITMENT_RESEND" after "CONFIRM_SUBMIT_TO_AU" is done

     */
    /***********************************************************************
     ************   Abstract Methods
     ************************************************************************/

    public DealHandlerCommon()
    {
        super();
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    public DealHandlerCommon(String name)
    {
        this(null,name); // No parent
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    protected DealHandlerCommon(View parent, String name)
    {
        super(parent,name);
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    public abstract void SaveEntity(ViewBean webPage)
    throws Exception ;

    /**
     *
     *
     */
    protected void handleExit()
    {
        try
        {
            navigateToNextPage(true);

            // standard navigation
        }
        catch(Exception ex)
        {
            getSessionResourceKit().cleanTransaction();
            logger.error("Exception @handleExit - ignored");
            logger.error(ex);
        }

    }


    /***********************************************************************
     ************   Handle Dialog Responses
     ************************************************************************/
    public void handleNoDisplayActMessage(boolean skip)
    {

        SessionResourceKit srk = getSessionResourceKit();

        TreeMap getActions = new TreeMap();

        if (skip == false) return;

        if (skip == true)
        {
            //getActions =(TreeMap)(theSession.getActMessage()).getResponseObject();
            getActions =(TreeMap)(theSessionState.getActMessage()).getResponseObject();
            handleCreditBureauMultiActionSupport(getActions);
            return;
        }
    }


    /**
     *
     *
     */
    public void handleCustomActMessageOk(String[] args)
    {

        logger.debug("DHC@handleCustomActMessageOk:: start");
		if (args == null){
           logger.debug("DHC@handleCustomActMessageOk:: args is null");
			return; //#DG758 will never happen ...
        }
        // Deal deal = null;
        ////PageEntry pg = theSession.getCurrentPage();

        logger.debug("DHC@handleCustomActMessageOk::ArgsSize: " + args.length);
        logger.debug("DHC@handleCustomActMessageOk::Args[0]: " + args[0]);

        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        SysLogger logger = srk.getSysLogger();

        TreeMap getActions = new TreeMap();
		//#DG758 moved up
		//if (args == null) return;

        if (args [ 0 ].equals("CONFIRMDELETE"))
        {
            // continue after a delete confirmed (e.g. delete property)
            ////handleDelete((theSession.getActMessage()).getResponseObject());
            logger.debug("DHC@handleCustomActMessageOk::I am before handleDelete");
            handleDelete((theSessionState.getActMessage()).getResponseObject());

            return;
        }

        if (args [ 0 ].equals("CONFIRM_EXPORT"))
        {
            // continue after the 'delete' button confirmed (e.g. update percentOutGDS flag of a liability
            // or physically delete record from the database). This action called 'confirm_export'
            // to support delete from the liability screen and 'export/delete' multiple functionality
            // from the 'Credit Bureau Liability' section.
            ////getActions =(TreeMap)(theSession.getActMessage()).getResponseObject();
            getActions =(TreeMap)(theSessionState.getActMessage()).getResponseObject();
            handleCreditBureauMultiActionSupport(getActions);

            return;
        }


        // This opportunity is reserved in case if the Product Team decides to go
        // back to the interactive dialog with the user for the 'Export/Delete Liability'
        // multiple web event.
        /**
    if (args[0].equals("CONFIRM_EXPORT_DELETE_CREDIT_BUREAU"))
    {
        // continue after an export confirmed (e.g. update percentOutGDS flag as joint action)

        getActions = (TreeMap) (theSession.getActMessage()).getResponseObject();
        handleCreditBureauMultiActionSupport( getActions );

        return;
    }

    if (args[0].equals("CONFIRM_DELETE_CREDIT_BUREAU"))
    {
        // continue after an export confirmed (e.g. physically delete Credit Bureau
        // Liability entity or update percentOutGDS flag)

        getActions = (TreeMap) (theSession.getActMessage()).getResponseObject();
        handleCreditBureauMultiActionSupport( getActions );

        return;
    }
         **/
        if (args [ 0 ].equals("EXIT"))
        {
            // navigate away from sreen after viewing messages
            handleExit();
            return;
        }

        if (args [ 0 ].equals("CONTINUE_DEASUBMIT"))
        {
            // continuing Applicant Entry Submit after seeing dup. borrower warning (or other) messages
            logger.debug("@DealHandlerCommon.handleCustomActMessageOk: Continue DEA Submit after Duplicate Borrower notification.");
            handleDEASubmit(true);
            return;
        }

        if (args [ 0 ].equals("CONTINUE_DEPSUBMIT"))
        {
            // continuing Property Entry Submit after seeing dup. property warning (or other) messages
            logger.debug("@DealHandlerCommon.handleCustomActMessageOk: Continue DEP Submit after Duplicate Property notification.");
            handleDEPSubmit(true);
            return;
        }


        // Special handle for copy PrimaryBorrower's Current address when create new borrower
        if (args [ 0 ].equals("ADDR_YES"))
        {
            handleAddApplicant(true);
            return;
        }


        // Special handle for not to copy PrimaryBorrower's Current address when create new borrower
        if (args [ 0 ].equals("ADDR_NO"))
        {
            handleAddApplicant(false);
            return;
        }


        // Post decision options
        if (args [ 0 ].equals("POST_DECISION_VIEW_ONLY"))
        {
            handlePostDecisionQueryOptions(0);
            return;
        }

        if (args [ 0 ].equals("POST_DECISION_UNLOCK_NO_DOC"))
        {
            handlePostDecisionQueryOptions(1);
            return;
        }

        if (args [ 0 ].equals("POST_DECISION_UNLOCK_WITH_DOC"))
        {
            handlePostDecisionQueryOptions(2);
            return;
        }

        //--Sync--//
        //--> This was missed in the previous sync.  Fixed by Billy 28Feb2003
        // Added Dialog message for user to request for Commitment resend -- Changed by Billy 12June2002
        if (args[0].equals("IS_COMMITMENT_RESEND")) {
            setActiveMessageToAlert(
                    BXResources.getSysMsg("DEAL_MODY_IS_COMMITMENT_RESEND",
                            theSessionState.getLanguageId()),
                            ActiveMsgFactory.ISCUSTOMDIALOG2, "COMMITMENT_RESEND_YES", "EXIT");
            return;
        }

        if (args[0].equals("COMMITMENT_RESEND_YES")) {
            handleCommitmentResend();
            handleExit();
            return;
        }

        //--CervusPhaseII--start--//
        ViewBean thePage = getCurrNDPage();

        if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_YES"))
        {
            logger.trace("DEH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
            provideOverrideDealLockActivity(pg, srk, thePage);
            return;
        }

        if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_NO"))
        {
            logger.trace("DEH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

            theSessionState.setActMessage(null);
            theSessionState.overrideDealLock(true);
            handleCancelStandard(false);

            return;
        }
        //--CervusPhaseII--end--//
        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
        if (args [ 0 ].equals("AU_SUBMIT_YES"))
        {
            logger.trace("Katya: DHC@handleCustomActMessageOk_AU_SUBMIT_YES: Submit to AU");
            if ((PropertiesCache.getInstance().getProperty(-1, "com.basis100.fxp.clientid.gecf", "N")).equals("Y")){
                requestGeMoneyDoc();

                if (pg.getPageCondition1()== true){
                    logger.debug("Katya:DHC@handleCustomActMessageOk_AU_SUBMIT_YES:: generating IS_COMMITMENT_RESEND dialog");
                    pg.setPageCondition1(false);
                    // show IS_COMMITMENT_RESEND dialog
                    setActiveMessageToAlert( BXResources.getSysMsg( "DEAL_MODY_IS_COMMITMENT_RESEND_WITH_NO_WARNING",
                            theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMDIALOG2, "COMMITMENT_RESEND_YES",
                    "EXIT");
                    logger.debug("DHC@@handleCustomActMessageOk_AU_SUBMIT_YES: returning back....");
                    return;
                }
            }

            theSessionState.setActMessage(null);
            handleExit();
            return;
        }
        if (args [ 0 ].equals("AU_SUBMIT_NO"))
        {
            logger.trace("Katya: DHC@handleCustomActMessageOk_AU_SUBMIT_NO: Submit to AU");
            if(pg.getPageCondition1() == true)
            {
                logger.debug("DHC@handleCustomActMessageOk_AU_SUBMIT_YES:: generating IS_COMMITMENT_RESEND dialog");
                pg.setPageCondition1(false);
                // show IS_COMMITMENT_RESEND dialog
                setActiveMessageToAlert(BXResources.getSysMsg("DEAL_MODY_IS_COMMITMENT_RESEND_WITH_NO_WARNING",
                        theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMDIALOG2, "COMMITMENT_RESEND_YES",
                "EXIT");
                logger.debug("DHC@@handleCustomActMessageOk_AU_SUBMIT_YES: returning back....");
                return;
            }

            theSessionState.setActMessage(null);
            handleExit();
            return;
        }

        if (args [ 0 ].equals("IS_SUBMIT_TO_AU"))
        {
            logger.debug("DHC@handleCustomActMessageOk IS_SUBMIT_TO_AU: Setting alert to CONFIRM_SUBMIT_TO_AU");
            setActiveMessageToAlert(
                    BXResources.getSysMsg("CONFIRM_SUBMIT_TO_AU", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISYESNO, "AU_SUBMIT_YES", "AU_SUBMIT_NO");
            return;
        }
        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------
        
        //4.3GR, Update Total Loan Amount -- start
        if (args [ 0 ].equals(MI_PREMIUM_UPDATED))
        {
        	//this method is to called by only UW and DE screen.
        	theSessionState.setActMessage(null);
        	handleOKOnAMLForUpdatedMIPremium();
        	return;
        }
        //4.3GR, Update Total Loan Amount -- end
    }

//  -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
    /**
     * call DocumentRequest for "GE Special Broker Approval" in the fixed format
     */
    protected void requestGeMoneyDoc(){
        try {
            srk.beginTransaction();
            PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

            // get deal
            CalcMonitor dcm = CalcMonitor.getMonitor(srk);
            Deal deal = null;
            deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), dcm);

            // request document
            logger.debug("DHC@requestGeMoneyDoc(): before submit a request for GEMoney upload");
            DocumentRequest.requestGEMoneyUpload(srk, deal);
            logger.debug("DHC@requestGeMoneyDoc(): after submit a request for GEMoney upload");
            srk.commitTransaction();
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.debug("Error in DHC@requestGeMoneyDoc(): " + ex);
            setStandardFailMessage();

        }
    }
//  -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------


    /***********************************************************************
     ************   Submit Handlers & Validation Functions
     ************************************************************************/
    protected void handlePostDecisionQueryOptions(int answer)
    {
        ////PageEntry pg = theSession.getCurrentPage();
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        logger.debug("DHC@handlePostDecisionQueryOptions::Asnwer: " + answer);

        //==> Modified by Billy 27May2003
        //==> For bug fix when user swapping Language when Decision Mod Dialog displaying
        // Display the post-decision Dialogue only once when 1st entering the page.
        // And force user to pick one of the option otherwise redisplay.
        //pg.setPageCondition4(true);
        //===============================================================================

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            if (answer == 0)
            {
                // do not unlock the deal ...
                // ensure view mode, drop tx copy, remove deal lock
                srk.beginTransaction();
                DLM dlm = DLM.getInstance();
                dlm.unlock(pg.getPageDealId(), pg.getPageMosUserId(), srk, true);

                // free transactional deal copies (if any)
                ////DealEditControl dec = theSession.getDec();
                DealEditControl dec = theSessionState.getDec();
                if (dec != null)
                {
                    dec.dropToTxLevel(0, srk, pg);
                    pg.setPageDealCID(dec.getCID());
                    ////theSession.setDec(null);
                    theSessionState.setDec(null);
                }
                srk.commitTransaction();

                // set page view only (not editable)
                pg.setEditable(false);
            }
            else if (answer == 1)
            {
                (new DecisionModificationHandler(this)).perfromDecisionModificationRequest(Mc.DM_NON_CRITICAL_NO_DOC_PREP, pg, srk, false);

                // NOte : the following has been done in DealEntryHandler
                // save state to indicate dialog already presented and processed

                //--CervusPhaseII--start--//
                // It was commented after Billy fixed a bug for the language swap.
                // Should be reset here when the DealModStateMachine is finished
                //pg.setPageCondition4(true);
                //--CervusPhaseII--end--//

                // navigate to self - to ensure creation of tx copy when page regenerated
                getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pg));
                navigateToNextPage(true);
            }
            else
            {
                (new DecisionModificationHandler(this)).perfromDecisionModificationRequest(Mc.DM_NON_CRITICAL_DOC_PREP, pg, srk, false);

                // NOte : the following has been done in DealEntryHandler
                // save state to indicate dialog already presented and processed
                //--CervusPhaseII--start--//
                // It was commented after Billy fixed a bug for the language swap.
                // It was commented after Billy fixed a bug for the language swap.
                // Should be reset here when the DealModStateMachine is finished
                //pg.setPageCondition4(true);
                //--CervusPhaseII--end--//
                // navigate to self - to ensure creation of tx copy when page regenerated
                getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pg));
                navigateToNextPage(true);
            }

            // save state to indicate dialog already presented and processed
            // Should be reset here when the DealModStateMachine is finished
            //pg.setPageCondition4(true);

        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handlePostDecisionQueryOptions: option = " + answer);
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /***********************************************************************
     ************   Submit Handlers & Validation Functions
     ************************************************************************/
    /**
     * handleDESubmit
     * 	Performs business functions on submission of the Deal Modification screen
     * 
     * @param none<br>
     * @revision 1.9: <br>
     * 	Date: 29 Jun 2006<br>
     * 	Author: NBC/PP Implementation Team <br>
     *  Change: <br>
     *  	Modified calls to workflowTrigger to set floatDown flags on the deal if required, before calling<br>
     */ 
    public void handleDESubmit()
    {
        PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        boolean firstEverSubmit = false;

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);

            // always adopt transactional copy on submit
            standardAdoptTxCopy(pe, true);

            // get deal
            CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
            Deal deal = DBA.getDeal(srk, pe.getPageDealId(), pe.getPageDealCID(), dcm);

            // check for first submit (ever) - via (before submit) deal entry flag in page entry
            if (pe.isDealEntry() == true)
            {
                logger.debug("@DealHandlerCommon.handleDESubmit: first ever submit for new deal");
                firstEverSubmit = true;
                pe.setDealEntry(false);

                // assign user to deal (as administrator or underwriter according to user type
                ////int uId = getTheSession().getSessionUserId();
                int uId = theSessionState.getSessionUserId();
                ////int uType = getTheSession().getSessionUserType();
                int uType = theSessionState.getSessionUserType();
                if (uType == Mc.USER_TYPE_JR_ADMIN || uType == Mc.USER_TYPE_ADMIN || uType == Mc.USER_TYPE_SR_ADMIN)
                {
                    logger.debug("@DealHandlerCommon.handleDESubmit: admin assigned, uid=" + uId);
                    deal.setAdministratorId(uId);
                }
                else if (uType == Mc.USER_TYPE_JR_UNDERWRITER || uType == Mc.USER_TYPE_UNDERWRITER ||(uType >= Mc.USER_TYPE_SR_UNDERWRITER && uType < USER_TYPE_SYS_ADMINISTRATOR))
                {
                    logger.debug("@DealHandlerCommon.handleDESubmit: uw assigned, uid=" + uId);
                    deal.setUnderwriterUserId(uId);
                }
            }

            ComboBox cbProductType =(ComboBox) getCurrNDPage().getDisplayField(pgDealEntryViewBean.CHILD_CBPRODUCTTYPE);
            int ProductTypeId = (new Integer((cbProductType.getValue()).toString())).intValue();
            deal.setProductTypeId(ProductTypeId);


            // set channel media to manual if not already set as electronic
            String channelMedia = "" + deal.getChannelMedia();
            if (! channelMedia.equals("E")) deal.setChannelMedia("M");
            deal.ejbStore();
            logger.debug("@DealHandlerCommon.handleDESubmit: perform calcs");
            doCalculation(dcm);

            // perform validations - other update activity, etc.
            logger.debug("@DealHandlerCommon.handleDESubmit: perfrom (BRE) validations");
            PassiveMessage pm = validateDealEntry(deal, pe, srk);
            //#DG826 rewritten
            //Start of FXP26973
            //pm can never be null.
            boolean criticalDataProblems = pm.getCritical();
            boolean nonCriticalDataProblems = pm.getNumMessages() > 0;
            //End of FXP26973

            // true even when all critical but no matter!
            boolean stayOnPage = criticalDataProblems || nonCriticalDataProblems;
            //#DG420 boolean isSubmitToAU = (PropertiesCache.getInstance().getProperty("com.basis100.fxp.clientid.gecf", "N")).equals("Y") ;
            boolean isSubmitToAU = (PropertiesCache.getInstance().getProperty(-1, "com.filogix.fxp.adjudication.enabled", "N")).equals("Y");
            ActiveMessage am = null;

            // --------------------- Catherine, 20-May-05, for GE, pvcs #1408  begin ---------------------
            String btCancel = "EXIT";
            if (isSubmitToAU)
            {
                btCancel = "IS_SUBMIT_TO_AU";
            }
            logger.debug("DHC@handleDESubmit: btCancel = " + btCancel);
            // --------------------- Catherine, 20-May-05, for GE, pvcs #1408  end ---------------------

            final int langId = theSessionState.getLanguageId();
            final int statId = deal.getStatusId();
            //#DG826 rewritten
            logger.debug("DHC@handleDESubmit: deal status (incomplete=0):"+statId+";critical data problems="+criticalDataProblems);
            if (criticalDataProblems)	{
                // --------------------- Catherine, 20-May-05, for GE, pvcs #1408  begiAUn ---------------------
               final String msg = statId == Mc.DEAL_INCOMPLETE? 
               		DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL_INCOMPLETE_STATUS: 
              			DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL;
					am = setActiveMessageToAlert(BXResources.getSysMsg(msg, langId),
                        ActiveMsgFactory.ISCUSTOMDIALOG, btCancel);

                // Catherine: uncomment lines below, when temporary fix for GE is not needed
//              am = setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL", theSessionState.getLanguageId()),
//              ActiveMsgFactory.ISCUSTOMDIALOG, "EXIT");
                // --------------------- Catherine, 20-May-05, for GE, pvcs #1408  end ---------------------
            }
            else
            {

                // transition to received status (incomplete and no critical problems)
                  if (statId == Mc.DEAL_INCOMPLETE)
                {
                    logger.debug("@DealHandlerCommon.handleDESubmit: status to received");

                    //Add log to deal history -- Billy 16May2001
                    DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
                    String msg = hLog.statusChange(deal.getStatusId(), "Deal Entry/Modify");
                    deal.setStatusDate(new Date());
                    deal.setStatusId(Mc.DEAL_RECEIVED);
                    deal.ejbStore();
                    hLog.log(deal.getDealId(), deal.getCopyId(), msg, Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
                }

                // Added Dialog message for user to request for Commitment resend -- Changed by Billy 12June2002
                String respondString = "EXIT";
                DealStatusManager dsm = DealStatusManager.getInstance(srk);
                if (dsm.getStatusCategory(deal.getStatusId(), srk) ==  Mc.DEAL_STATUS_CATEGORY_POST_DECISION)
                {
                    respondString = "IS_COMMITMENT_RESEND";

                    // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
                    // pageCondition1 is used to trigger "IS_COMMITMENT_RESEND" after "CONFIRM_SUBMIT_TO_AU" is done
                    pe.setPageCondition1(true);
                    logger.debug("Katya:DHC@handleDESubmit:: pageCondition1() = " + (pe.getPageCondition1()== true ? "true" : "false"));
                    // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------

                }
                // --------------------- Catherine, 20-May-05, for GE, pvcs #1408  begin ---------------------
                if (isSubmitToAU)
                {
                    respondString = "IS_SUBMIT_TO_AU";
                }
                logger.debug("Katya: DHC@handleDESubmit: btCancel = " + btCancel);
                // --------------------- Catherine, 20-May-05, for GE, pvcs #1408  end ---------------------

                  logger.debug("DHC@handleDESubmit::RespondString: " + respondString+";StayOnPage: " + stayOnPage);
                logger.debug("DHC@handleDESubmit::StayOnPage: " + stayOnPage);

                if (stayOnPage) {
                    logger.debug("@DealHandlerCommon.handleDESubmit: stayOnPage = true. Setting alert to DEAL_ENTRY_MOD_SAVED_WITH_WARNINGS");
                    am = setActiveMessageToAlert(
                            BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_WITH_WARNINGS",
                                    theSessionState.getLanguageId()),
                                    ActiveMsgFactory.ISCUSTOMDIALOG, respondString);
                }
                else {
                    // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------

                    logger.debug("@DealHandlerCommon.handleDESubmit: stayOnPage = false.");
                    if (isSubmitToAU)
                    {
                        logger.debug("@DealHandlerCommon.handleDESubmit: Setting alert to CONFIRM_SUBMIT_TO_AU");
                        stayOnPage = true;
                        am = setActiveMessageToAlert(
                                BXResources.getSysMsg("CONFIRM_SUBMIT_TO_AU", theSessionState.getLanguageId()),
                                ActiveMsgFactory.ISYESNO, "AU_SUBMIT_YES", "AU_SUBMIT_NO");
                    } else {
                        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------
                        //Bug fix -- Billy 11July2002
                        if (respondString.equals("IS_COMMITMENT_RESEND")) {
                            ///temp to test
                            stayOnPage = true;
                            logger.debug("@DealHandlerCommon.handleDESubmit: respondString = IS_COMMITMENT_RESEND. Setting alert to DEAL_MODY_IS_COMMITMENT_RESEND_WITH_NO_WARNING");
                            am = setActiveMessageToAlert(
                                    BXResources.getSysMsg(
                                            "DEAL_MODY_IS_COMMITMENT_RESEND_WITH_NO_WARNING",
                                            theSessionState.getLanguageId()),
                                            ActiveMsgFactory.ISCUSTOMDIALOG2, "COMMITMENT_RESEND_YES",
                            "EXIT");
                        }
                        else {

                            logger.debug("@DealHandlerCommon.handleDESubmit: Setting alert to DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS");
                            am = setActiveMessageToAlert(
                                    BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS",
                                            theSessionState.getLanguageId()),
                                            ActiveMsgFactory.ISCUSTOMCONFIRM, respondString);
                        }
                    }
                }
                //===============================================================================================

            }
			//#DG826 rewritten
			if (nonCriticalDataProblems)	{		 
	         if (am != null && srk.getDiagnosticViaClient())		 
            {
                Vector v = deal.dumpEntity(null);
                int lim = v.size();
	            for(int i = 0; i < lim; ++ i) 
	            	am.addMsg((String) v.elementAt(i), PassiveMessage.INFO);
            }

                logger.debug("@DealHandlerCommon.handleDESubmit: set (passive) messages");
                pm.setGenerate(true);
                ////getTheSession().setPasMessage(pm);
                theSessionState.setPasMessage(pm);
            }

            // check post-decision actions
            if (criticalDataProblems == false && deal.getDecisionModificationTypeId() != Mc.DM_NO_MOD_REQUESTED)
            {
	        //--Sync--//--> This was missed in the previous sync.  Fixed by Billy 28Feb2003
                // Added Dialog message for user to request for Commitment resend -- Changed by Billy 12June2002
                // request commitment letter if necessary
	        //if (deal.getDecisionModificationTypeId() == Mc.DM_NON_CRITICAL_DOC_PREP)	{
                //	CCM ccm = new CCM(srk);
                //	ccm.requestCommitmentDoc(srk, deal);
                //	ccm.requestElectronicResponseToBroker(srk, deal);
                //}
                //==============================================================================================
                deal.setDecisionModificationTypeId(Mc.DM_NO_MOD_REQUESTED);
                deal.ejbStore();
            }

            // check change after MI in progress/in place
            setMIIntegrityMsg(srk, pe);

            //***** Change by NBC/PP Implementation Team - Version 1.9 - Start *****//  \                           
//          if (firstEverSubmit) workflowTrigger(1, srk, pe, null);
//          else workflowTrigger(2, srk, pe, null);

            // hit workflow engine to close the RFD task and re-open if needed     
            if (firstEverSubmit) 
            {
                // existing code to trigger the workflow for the very first time
                workflowTrigger(1, srk, pe, null);
            }
            else
            {
//              comment out the below RFD(rate float down) process  --start--
                /*     		// check if lender profile supports RFD
     		LenderProfile dealLender = deal.getLenderProfile();
     		boolean rfdSupported = false;
     		if (dealLender != null)
     		{
                 rfdSupported = dealLender.isSupportRateFloatDown();
            }

            if (rfdSupported && isChangedEstimatedClosingDate())
            {
         	   // set float down completed flag on deal
         	   deal.setFloatDownCompletedFlag("Y");
         	   deal.ejbStore();

         	   // call workflow to close the existing RFD task
         	   workflowTrigger(2, srk, pe, null);

         	   // set the flags again
         	   deal.setFloatDownCompletedFlag("N");
               deal.setFloatDownTaskCreatedFlag("N");
               deal.ejbStore();

               workflowTrigger(2, srk, pe, null);

               // set the Task Created Flag
               deal.setFloatDownTaskCreatedFlag("Y");
               deal.ejbStore();
             }
             else
             {
            	 // RFD is not supported, 
            	 // or estimated closing date was not changed
                 workflowTrigger(2, srk, pe, null);
             }
                 */            
//              comment out the below RFD(rate float down) process  --end--

                boolean postDecisionStatus = false;
                if (deal.getStatusId()>= 11 && deal.getStatusId()<= 20)
                {
                    postDecisionStatus = true;
                }

                // while the estimated closing date is changed and Deal is in post decision status
                if (isChangedEstimatedClosingDate() && postDecisionStatus )
                {	 
                    // call workflow to close the opening tasks which is based on ClosingDate 
                    workflowTrigger(3, srk, pe, null);
                }

                workflowTrigger(2, srk, pe, null);
            }
            //***** Change by NBC/PP Implementation Team - Version 1.9 - Start *****//                 

            logger.debug("DHC@handleDESubmit::StayOnPage_Again: " + stayOnPage);

            // decide where going next (standard navigation if not regenerating page)
            if (stayOnPage)
            {
                // navigate to self - to ensure creation of tx copy when page regenerated
                logger.debug("DHC@handleDESubmit::StayOnPage_PageLabel: " + PageEntry.getPageEntryAsCopy(pe).getPageLabel());
                getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pe));
            }
            if (firstEverSubmit)
            {
                // code here
            }

            navigateToNextPage();
            srk.commitTransaction();
            logger.debug("@DealHandlerCommon.handleDESubmit: tx commit and finished");
            return;
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleDESubmit().");
            logger.error(ex);
            if (firstEverSubmit == true) pe.setDealEntry(true);
        }


        // an exception has occured - ensure we navigate away from page since we can't guar
        // presence of a tx copy on regeneration
        navigateAwayFromFromPage(true);
    }

    /**
     * isChangedEstimatedClosingDate
     * 	Returns boolean depending on value of hidden flag value on Deal Modification page
     * 
     * @param none<br>
     * @return boolean : the result of the method <br>
     * @author NBC/PP Implementation Team
     */
    protected boolean isChangedEstimatedClosingDate ()
    {
        boolean isChangedDate = false;

        ViewBean currNDPage = getCurrNDPage();
        //    Get the flag value, flag is true if estimated closing date has //changed
        String flagChangedEstimatedClosingDate = (String)currNDPage.getDisplayFieldValue("hdChangedEstimatedClosingDate");

        //     CHANGED_ESTIMATED_CLOSING_DATE_FLAG is a constant, true is "Y"
        if(flagChangedEstimatedClosingDate.equalsIgnoreCase(CHANGED_ESTIMATED_CLOSING_DATE_FLAG))
        {
            isChangedDate = true;
        }

        return isChangedDate;
    }


    /**
     * <p>validateDealEntry</p>
     * 
     * @version 1.14 July 14, 2008 MCM Team (XS 2.39 added component validation for DCC and DEC)
     *
     */
    public PassiveMessage validateDealEntry(Deal deal, PageEntry pe, SessionResourceKit srk)
    throws Exception
    {
        PassiveMessage pm = new PassiveMessage();

        BusinessRuleExecutor brExec = new BusinessRuleExecutor();

        Vector v = new Vector();

        v.add("DI-%");

        v.add("IC-%");

        v.add("DE-%");


        ////brExec.BREValidator(getTheSession(), pe, srk, pm, v, true);
        brExec.BREValidator(theSessionState, pe, srk, pm, v, true);

        // Check if MI rules should run as well
        if (pe.getPageCondition6() == true)
        {
            // add MI rules if indicator set to required
            if (deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL &&(deal.getMIIndicatorId() == Mc.MII_UW_REQUIRED || deal.getMIIndicatorId() == Mc.MII_REQUIRED_STD_GUIDELINES))
            {
                // check institution property to ensure this is enabled (note default is Y if property absent)
                if ((PropertiesCache.getInstance().getProperty(-1, "institution.validate.mi.at.deal.resolve", "Y")).equals("Y"))
                {
                    PassiveMessage pmForMI = new PassiveMessage();
                    ////brExec.BREValidator(getTheSession(), pe, srk, pmForMI, "MI-%", true);
                    brExec.BREValidator(theSessionState, pe, srk, pmForMI, "MI-%", true);

                    // Check if need to treat all MI Rule results as Non-Critical (note default is N if property absent)
                    //if((PropertiesCache.getInstance().getProperty("com.basis100.deal.uw.treatmirulesasnoncritical", "N")).equals("Y"))
                    //{
                    //  logger.debug("BILLY ==> Set all MI Rule Results as Non-critical !!");
                    //  pmForMI.setAllAsNonCritcal();
                    //}
                    pm.addAllMessages(pmForMI);
                }
            }
        }


        // local scope rules ...
        int lim = 0;

        Object [ ] objs = null;


        // borrower rules
        try
        {
            // logger.debug("@DealHandlerCommon.validateDealEntry: collect borrowers for DEA rules");
            Collection borrowers = deal.getBorrowers();
            lim = borrowers.size();
            objs = borrowers.toArray();
        }
        catch(Exception e)
        {
            lim = 0;
        }


        // assume no borrowers
        for(int i = 0; i < lim; ++ i)
        {
            Borrower borr =(Borrower) objs [ i ];
            brExec.setCurrentProperty(- 1);
            // MCM Team STARTS(XS 2.39)
            brExec.setCurrentComponent(- 1);
            // MCM Team ENDSS(XS 2.39)
            brExec.setCurrentBorrower(borr.getBorrowerId());

            logger.debug("@DealHandlerCommon.validateDealEntry: DEA rules, borrower num=" + i);
            ////PassiveMessage pmBorr = brExec.BREValidator(getTheSession(), pe, srk, null, "DEA-%", true);
            PassiveMessage pmBorr = brExec.BREValidator(theSessionState, pe, srk, null, "DEA-%", true);

            if (pmBorr != null)
            {
                pm.addSeparator();
                pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM", theSessionState.getLanguageId()) +
                        borr.formFullName(), PassiveMessage.INFO);
                pm.addAllMessages(pmBorr);
                pm.addSeparator();
            }
        }


        // property rules
        try
        {
            // logger.debug("@DealHandlerCommon.validateDealEntry: collect properties for DEP rules");
            Collection properties = deal.getProperties();
            lim = properties.size();
            objs = properties.toArray();
        }
        catch(Exception e)
        {
            lim = 0;
        }


        // assume no properties
        for(int i = 0; i < lim; ++ i)
        {
            Property prop =(Property) objs [ i ];
            brExec.setCurrentProperty(prop.getPropertyId());
            brExec.setCurrentBorrower(- 1);
            //MCM TEAM STARTS (XS 2.39)
            brExec.setCurrentComponent(- 1);
            //MCM TEAM ENDS (XS 2.39)

            logger.debug("@DealHandlerCommon.validateDealEntry: DEP rules, propert num=" + i);
            ////PassiveMessage pmProp = brExec.BREValidator(getTheSession(), pe, srk, null, "DEP-%", true);
            PassiveMessage pmProp = brExec.BREValidator(theSessionState, pe, srk, null, "DEP-%", true);

            if (pmProp != null)
            {
                pm.addSeparator();
                pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM", theSessionState.getLanguageId())  +
                        prop.formPropertyAddress(), PassiveMessage.INFO);
                pm.addAllMessages(pmProp);
                pm.addSeparator();
            }
        }

        /***************MCM Impl team changes starts - FXP22741*******************/
        
        PassiveMessage pmComp = validateComponents(pe, srk);
        pm.addAllMessages(pmComp);
        
//        //MCM TEAM STARTS (XS 2.39)
//        // Component Validation
//        try
//        {
//            // logger.debug("@DealHandlerCommon.validateDealEntry: collect properties for DEP rules");
//            Collection<Component> components = deal.getComponents();
//            lim = components.size();
//            objs = components.toArray();
//        }
//        catch(Exception e)
//        {
//            lim = 0;
//        }
//
//        //XS_2.40 DMC MCM IMPL STARTS  
//        try
//        {
//            brExec.setCurrentComponent(-1);
//            brExec.setCurrentBorrower(- 1);
//            brExec.setCurrentProperty(- 1);
//            PassiveMessage pmComp = brExec.BREValidator(theSessionState, pe, srk, null, "DMC-%", true);
//            if (pmComp.getNumMessages() > 0)
//            {
//                pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_COMPONENT_PROBLEM", 
//                        theSessionState.getLanguageId())
//                        + BXResources.getPickListDescription(-1, "COMPONENTTYPE",0, theSessionState.getLanguageId())
//                        + " ", PassiveMessage.INFO);
//                pm.addAllMessages(pmComp);
//            }
//        }
//        catch (Exception e)
//        {
//            _log.trace("Faild validate Components" );
//        }
//        //XS_2.40 DMC MCM IMPL ENDS
//
//        //06-Aug-2008 |XS_2.48| Removed the call of DCC- businessrules.
//        int lastCompTypeid = -1; //FXP22741, changed numbering 
//        int compSequence = 0; //FXP22741, changed numbering 
//        for(int i = 0; i < lim; ++ i)
//        {
//            Component comp =(Component) objs [ i ];
//            brExec.setCurrentComponent(comp.getComponentId());
//            brExec.setCurrentBorrower(- 1);
//            brExec.setCurrentProperty(- 1);
//
//            //FXP22741, changed numbering 
//            compSequence = (lastCompTypeid != comp.getComponentTypeId()) ? 1 : compSequence + 1;
//
//            PassiveMessage pmCompDEC = brExec.BREValidator(theSessionState, pe, srk, null, "DEC-%", true);
//            if (pmCompDEC != null)
//            {
//                pm.addSeparator();
//                pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_COMPONENT_PROBLEM", theSessionState.getLanguageId())
//                        + BXResources.getPickListDescription(-1, "COMPONENTTYPE", comp.getComponentTypeId(), theSessionState.getLanguageId())
//                        + " " + compSequence, PassiveMessage.INFO);
//                pm.addAllMessages(pmCompDEC);
//                pm.addSeparator();
//            }
//            lastCompTypeid = comp.getComponentTypeId(); //FXP22741, changed numbering 
//        }
//        //MCM TEAM ENDS (XS 2.39)
        
        /***************MCM Impl team changes ends - FXP22741*********************/

        /*
      // Add Duplicate Deal Checking -- by BILLY 07March2001
      DupeCheck theDCheck = DupeCheck.check(srk, deal);
      if(theDCheck.isDuplicateFound() || theDCheck.isCautionFound())
      {
        // Duplicated Deals found -- Add the warning message(s) to pm
        pm.addSeparator();
        pm.addMsg("The following message(s) for Duplicated Deal checking: ", PassiveMessage.INFO);
        pm.addAllMessages(theDCheck.getMessage());
        pm.addSeparator();
      }
         */
        // Add sub-title based on Screen spec. -- by BILLY 02Feb2001
        /* LS ticket merge (EXCH2.000157) for 4.2GR 
        if (pm.getNumMessages() > 0)
        {
            String tmpMsg = BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM", theSessionState.getLanguageId())
            + "<br>" + "(Deal #: " + deal.getDealId() + ")";
            pm.setInfoMsg(tmpMsg);
        } 


        brExec.close();
		*/
        return pm;

    }


    // In event of data change after MI application initiated (or completed!) post a message to the
    // user warning that changes may invalidate the policy.
    //
    // Message to appear in active message - new active message created if on not already set.

    private void setMIIntegrityMsg(SessionResourceKit srk, PageEntry pg)
    {
        if (pg.isPageDataModified() == false || pg.getPageCondition5() == true) return;

        ActiveMessage am = theSessionState.getActMessage();

        if (am == null)
        {
            setActiveMessageToAlert("", ActiveMsgFactory.ISCUSTOMCONFIRM);
            ////am = getTheSession().getActMessage();
            am = theSessionState.getActMessage();
            if (am == null) return;

            // should never happen ... but
        }

        am.addMsg(BXResources.getSysMsg("MI_DATA_CHANGED_WARNING", theSessionState.getLanguageId()), ActiveMessage.NONCRITICAL);

    }


    // --- //
    public void handleDEASubmit()
    {
        handleDEASubmit(false);
    }

    /**
     *
     *
     */
    public void handleDEASubmit(boolean confirmed)
    {
        logger.debug("@DealHandlerCommon.handleDEASubmit");

        PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            // get borrower
            CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

            String [ ] keyValues = {pgApplicantEntryViewBean.CHILD_HDAPPLICANTID, pgApplicantEntryViewBean.CHILD_HDAPPLICANTCOPYID};
            int primaryid =(int) Double.parseDouble(readFromPage(keyValues [ 0 ], - 1));
            int copyid =(int) Double.parseDouble(readFromPage(keyValues [ 1 ], - 1));

            Borrower borrower =(Borrower) getEntity("Borrower", primaryid, copyid, dcm);
            if (confirmed == false)
            {
                // pre-submit behavior validations/checks
                // Check for duplicated borrowers if LastName set
                if (borrower.getBorrowerLastName() != null && ! borrower.getBorrowerLastName().equals(""))
                {
                    try
                    {
                        // get min date based on the current date
                        Date minDate = DuplicateFinder.getMinSearchDate(srk, null);

                        //--Release2.1--//
                        //// Translation lookup.
                        DupeCheck theDCheck = DupeCheck.check(srk, borrower, minDate,  theSessionState.getLanguageId());
                        if (theDCheck.isDuplicateFound())
                        {
                            // Duplicated Deals found -- Add the warning message(s) to am
                            ActiveMessage am = setActiveMessageToAlert(BXResources.getSysMsg("DUPECHECK_FOUND_DUPLICATED", theSessionState.getLanguageId()),
                                    ActiveMsgFactory.ISCUSTOMDIALOG, "CONTINUE_DEASUBMIT");
                            am.addAllMessages(theDCheck.getMessage());
                            return;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception @DealHandlerCommon.handleDEASubmit: Checking for duplicate applicant - ignored");
                        logger.error(e);
                    }
                }
            }

            // start submit transaction ...
            srk.beginTransaction();
            //ticket5060 save solicitate state
            ComboBox cbProductType =(ComboBox) getCurrNDPage().getDisplayField(pgApplicantEntryViewBean.CHILD_CBSOLICITATION);
            int solicitationId = (new Integer((cbProductType.getValue()).toString())).intValue();
            borrower.setSolicitationId(solicitationId);
            borrower.ejbStore();

            //ticket5060
            noAuditIfIncomplete(pe, null);

            // always adopt transactional copy on submit
            standardAdoptTxCopy(pe, true);

            // New requirement -- to populate Work Phone # if empty
            //  - By Billy 10Apr2001
            checkAndPopulateWorkPhoneNum(borrower, srk);

            // perform validations - other update activity, etc.
            PassiveMessage pm = validateBorrower(borrower, pe, srk);
            boolean criticalDataProblems = false;

            // pm.getCritical();
            boolean nonCriticalDataProblems = false;

            // pm.getNumMessages() > 0; // true even when all critical but no matter!
            boolean stayOnPage = false;

            // criticalDataProblems || nonCriticalDataProblems;
            if (criticalDataProblems == true)
            {
                setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL", theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMDIALOG);
            }
            else
            {
                // no critical problems ...
                if (stayOnPage) setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_WITH_WARNINGS", theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMDIALOG);
            }
            if (pm != null)
            {
                pm.setGenerate(true);
                ////getTheSession().setPasMessage(pm);
                theSessionState.setPasMessage(pm);
            }
            doCalculation(dcm);
            if (stayOnPage)
            {
                // navigate to self - to ensure creation of tx copy when page regenerated
                getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pe));
            }

            // trigger workflow:
            //
            // The only task known to generate in this case is "Review Deal Notes". A an optimization we
            // pre-check the generation criteria to avoid needless trigger
            //
            Deal deal = DBA.getDeal(srk, pe.getPageDealId(), pe.getPageDealCID());
            String checkNotes = deal.getCheckNotesFlag() + "N";
            if (checkNotes.startsWith("Y")) workflowTrigger(2, srk, pe, null);

            // standard navigation - back to parent
            navigateToNextPage();
            srk.commitTransaction();
            return;
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleDEASubmit().");
            logger.error(ex);
        }


        // an exception has occured - ensure we navigate away from page since we can't guar
        // presence of a tx copy on regeneration
        navigateAwayFromFromPage(true);

    }


    /**
     *
     *
     */
    private void checkAndPopulateWorkPhoneNum(Borrower borrower, SessionResourceKit srk)
    {
        try
        {
            //Check and get the CurrentFullTime Employment History
            EmploymentHistory eHist = new EmploymentHistory(srk);
            eHist.setSilentMode(true);
            eHist = eHist.findByCurrentFullTimeEmployment((BorrowerPK) borrower.getPk());
            Contact theEContact = eHist.getContact();
            boolean isBWorkNumSet = false;
            if (theEContact == null) return;

            //Populate the Borrower's Work phone # from CurrentFullTime Employment History if Phone# and Ext are null
            if ((borrower.getBorrowerWorkPhoneNumber() == null || borrower.getBorrowerWorkPhoneNumber().equals("")) 
            		&&(borrower.getBorrowerWorkPhoneExtension() == null || borrower.getBorrowerWorkPhoneExtension().equals("")))
            {
                isBWorkNumSet = false;
            }
            else
            {
                isBWorkNumSet = true;
            }

            //Populate the Borrower's Work phone # from CurrentFullTime Employment History if Phone# and Ext are null
            if ((theEContact.getContactPhoneNumber() == null || theEContact.getContactPhoneNumber().equals("")) 
                &&(theEContact.getContactPhoneNumberExtension() == null || theEContact.getContactPhoneNumberExtension().equals("")))
            {
                //Set Contact Phone # and Ext from Borrower if Borrower's Phone # is set
                if (isBWorkNumSet == true)
                {
                    theEContact.setContactPhoneNumber(borrower.getBorrowerWorkPhoneNumber());
                    theEContact.setContactPhoneNumberExtension(borrower.getBorrowerWorkPhoneExtension());
                    theEContact.ejbStore();
                    return;
                }
            }
            else
            {
                //Set Borrower's Work Phone Num if it is empty
                if (isBWorkNumSet == false)
                {
                    borrower.setBorrowerWorkPhoneNumber(theEContact.getContactPhoneNumber());
                    borrower.setBorrowerWorkPhoneExtension(theEContact.getContactPhoneNumberExtension());
                    borrower.ejbStore();
                    return;
                }
            }
        }
        catch(FinderException fe)
        {
            return;

            // nothing to do if no Current and Fulltime Employment
        }
        catch(Exception e)
        {
            logger.error("Eaception @DealHandlerCommon.checkAndPopulateWorkPhoneNum - ignored");
            logger.error(e);
        }

    }


    /**
     *
     *
     */
    public PassiveMessage validateBorrower(Borrower borrower, PageEntry pe, SessionResourceKit srk)
    throws Exception
    {
        PassiveMessage pm = new PassiveMessage();

        BusinessRuleExecutor brExec = new BusinessRuleExecutor();

        brExec.setCurrentProperty(- 1);

        brExec.setCurrentBorrower(borrower.getBorrowerId());

        PassiveMessage pmBorr = new PassiveMessage();

        brExec.BREValidator(theSessionState, pe, srk, pmBorr, "DEA-%");
        if (pmBorr.getNumMessages() > 0)
        {
            // Add sub-title based on Screen spec. -- by BILLY 02Feb2001
            String tmpMsg = BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM", theSessionState.getLanguageId())
            + "<br>" + "(Deal #: " + borrower.getDealId() + ")";
            pm.setInfoMsg(tmpMsg);
            pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM", theSessionState.getLanguageId())
                    + borrower.formFullName(), PassiveMessage.INFO);
            pm.addAllMessages(pmBorr);
            return pm;
        }

        return null;

    }



    public void handleDEPSubmit()
    {
        handleDEPSubmit(false);

    }


    /**
     *
     *
     */
    public void handleDEPSubmit(boolean confirmed)
    {
        logger.debug("@DealHandlerCommon.handleDEPSubmit");

        PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            // get property
            CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
            String [ ] keyValues =
            {
                    "hdPropertyId", "hdPropertyCopyId"			}
            ;
            int primaryid =(int) Double.parseDouble(readFromPage(keyValues [ 0 ], - 1));
            int copyid =(int) Double.parseDouble(readFromPage(keyValues [ 1 ], - 1));
            Property property =(Property) getEntity("Property", primaryid, copyid, dcm);

            // pre-submit behavior validations/checks
            if (confirmed == false)
            {
                // Check for duplicated properties if StreetName set
                if (property.getPropertyStreetName() != null && ! property.getPropertyStreetName().equals(""))
                {
                    try
                    {
                        // get min date based on the current date
                        Date minDate = DuplicateFinder.getMinSearchDate(srk, null);
                        DupeCheck theDCheck = DupeCheck.check(srk, property, minDate,  theSessionState.getLanguageId());

                        if (theDCheck.isDuplicateFound() || theDCheck.isCautionFound())
                        {
                            // Duplicated Deals found -- Add the warning message(s) to am
                            ActiveMessage am = setActiveMessageToAlert(BXResources.getSysMsg("DUPECHECK_FOUND_DUPLICATED", theSessionState.getLanguageId()),
                                    ActiveMsgFactory.ISCUSTOMDIALOG, "CONTINUE_DEPSUBMIT");
                            am.addAllMessages(theDCheck.getMessage());
                            return;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception DealHandlerCommon.handleDEPSubmit: Checking for duplicate property - ignored");
                        logger.error(e);
                    }
                }
            }

            // submit transaction ...
            srk.beginTransaction();
            noAuditIfIncomplete(pe, null);

            // always adopt transactional copy on submit
            standardAdoptTxCopy(pe, true);

            // perform validations - other update activity, etc.
            PassiveMessage pm = validateProperty(property, pe, srk);
            boolean criticalDataProblems = false;

            // pm.getCritical();
            boolean nonCriticalDataProblems = false;

            // pm.getNumMessages() > 0; // true even when all critical but no matter!
            boolean stayOnPage = false;

            // criticalDataProblems || nonCriticalDataProblems;
            if (criticalDataProblems == true)
            {
                setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL", theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMDIALOG);
            }
            else
            {
                // no critical problems ...
                if (stayOnPage) setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_WITH_WARNINGS", theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMDIALOG);
            }
            if (pm != null)
            {
                pm.setGenerate(true);
                ////getTheSession().setPasMessage(pm);
                theSessionState.setPasMessage(pm);
            }
            doCalculation(dcm);
            if (stayOnPage)
            {
                // navigate to self - to ensure creation of tx copy when page regenerated
                getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pe));
            }

            // trigger workflow:
            //
            // The only task known to generate in this case is "Review Deal Notes". A an optimization we
            // pre-check the generation criteria to avoid needless trigger
            //
            Deal deal = DBA.getDeal(srk, pe.getPageDealId(), pe.getPageDealCID());
            String checkNotes = deal.getCheckNotesFlag() + "N";
            if (checkNotes.startsWith("Y")) workflowTrigger(2, srk, pe, null);

            // standard navigation - back to parent
            navigateToNextPage();
            srk.commitTransaction();
            return;
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleDEPSubmit().");
            logger.error(ex);
        }


        // an exception has occured - ensure we navigate away from page since we can't guar
        // presence of a tx copy on regeneration
        navigateAwayFromFromPage(true);

    }


    /**
     *
     *
     */
    public PassiveMessage validateProperty(Property property, PageEntry pe, SessionResourceKit srk)
    throws Exception
    {
        PassiveMessage pm = new PassiveMessage();

        BusinessRuleExecutor brExec = new BusinessRuleExecutor();

        brExec.setCurrentProperty(property.getPropertyId());

        brExec.setCurrentBorrower(- 1);

        PassiveMessage pmProp = new PassiveMessage();

        ////brExec.BREValidator(getTheSession(), pe, srk, pmProp, "DEP-%");
        brExec.BREValidator(theSessionState, pe, srk, pmProp, "DEP-%");

        if (pmProp.getNumMessages() > 0)
        {
            // Add sub-title based on Screen spec. -- by BILLY 02Feb2001
            String tmpMsg = BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM", theSessionState.getLanguageId())
            + "<br>" + "(Deal #: " + property.getDealId() + ")";
            pm.setInfoMsg(tmpMsg);
            pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM", theSessionState.getLanguageId())
                    + property.formPropertyAddress(), PassiveMessage.INFO);
            pm.addAllMessages(pmProp);
            return pm;
        }

        return null;

    }


    /***********************************************************************
     ************   Handle Delete Entities
     *** Modified to handle multiple delete by Billy 01Aug2001
     ************************************************************************/

    protected void handleDelete(boolean confirmed, int type, Vector rowindexes,
    	String primaryIdVar, String primaryColName, String copyIdVar, String entity) {
    	
  		//#DG758 SysLogger logger = getSessionResourceKit().getSysLogger(); 
  	  Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());

			//PageEntry pg = theSession.getCurrentPage();
        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        ////if (disableEditButton(pg) == true) return;
        if (disableEditButton(currPg) == true) return;

        String tmp = "";

        for(int i = 0;i < rowindexes.size();i ++)
        {
            //// Auto-conversion is not done proper. In any case it shold be (at least temporarily)
            //// returned to the original expression to escape the overhead of the extra primitive
            //// type conversion. It is an expensive operation.
            ////tmp +=((Integer)com.iplanet.jato.util.TypeConverter.asInt(rowindexes.elementAt(i))) + ", ";
            tmp += ((Integer)rowindexes.elementAt(i)).intValue() + ", ";
        }
        logger.debug("@DealHandlerCommon.handleDelete: Rowindexes = " + tmp);
			logger.debug("@DealHandlerCommon.handleDelete: unconfirmed delete entity = "
							+ entity + ", pId = " + primaryIdVar + ", cId = " + copyIdVar
							+ ", primColName = " + primaryColName+ " - dialog for confirmation");

        logger.debug("@DealHandlerCommon.handleDelete: unconfirmed delete entity = " + entity + ", pId = " + primaryIdVar + ", cId = " + copyIdVar + ", primColName = " + primaryColName + " - dialog for confirmation");

        DeleteEntityProperties dep = new DeleteEntityProperties(type, rowindexes, 
            primaryIdVar, primaryColName, copyIdVar, entity, theSessionState.getLanguageId());


        // Check if any row selected
        if (rowindexes == null || rowindexes.size() <= 0)
        {
            setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_NOT_SELECTED", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
        }
        else
        {
            setActiveMessageToAlert(dep.message, ActiveMsgFactory.ISCUSTOMDIALOG, "CONFIRMDELETE");
            ////(theSession.getActMessage()).setResponseObject(dep);
            (theSessionState.getActMessage()).setResponseObject(dep);
        }


        // Set Target to reload the page on the position before left
        ////if (dep.targetTag != TARGET_NONE) getTheSession().getCurrentPage().setLoadTarget(dep.targetTag);
        if (dep.targetTag != TARGET_NONE) currPg.setLoadTarget(dep.targetTag);

        ////getTheSession().getCurrentPage().setPageCondition3(true);
        currPg.setPageCondition3(true);

        return;

    }


    // Method to delete only one row at a time

    protected void handleDelete(boolean confirmed, int type, int rowindex, String primaryIdVar, String primaryColName, String copyIdVar, String entity)
    {
        Vector rowindexes = new Vector();

        rowindexes.add(new Integer(rowindex));

        handleDelete(confirmed, type, rowindexes, primaryIdVar, primaryColName, copyIdVar, entity);

        return;

    }


    /**
     *
     *  @version 1.16 XS 2.32: delete component modified:
     *  @author MCM Team
     */
    protected void handleDelete(Object obj)
    {
        DeleteEntityProperties dep =(DeleteEntityProperties) obj;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            int primaryid = 0;
            int copyid = 0;
            int currRow = - 1;
			for (int i = 0; i < dep.rowindexes.size(); i++) {
                //// Wrong auto-convertion.
                ////currRowTypeConverter.asInt(=((Integer)(dep.rowindexes.elementAt(i))));
                currRow = ((Integer)(dep.rowindexes.elementAt(i))).intValue();
                primaryid =(int) Double.parseDouble(readFromPage(dep.primaryIdVar, currRow));
                copyid =(int) Double.parseDouble(readFromPage(dep.copyIdVar, currRow));
                DealEntity de = getEntity(dep.entityName, primaryid, copyid, dcm);

                // check for employment history being deleted - if so remove linked income record (if one)
                Income income = null;
                if (dep.entityName.equals("EmploymentHistory"))
                {
                    EmploymentHistory eh =(EmploymentHistory) de;
                    if (eh.getIncomeId() > 0) income = eh.getIncome();
                    de.ejbRemove();
                    if (income != null)
                    {
                        //Set the Calc Monitor -- The EmploymentHistory doesn't have it.
                        income.setCalcMonitor(dcm);
                        income.ejbRemove();
                    }
                }
                else if (dep.entityName.equals("BorrowerAddress"))
                {
                    BorrowerAddress ba =(BorrowerAddress) de;
                    Addr addr = new Addr(getSessionResourceKit());
                    addr = addr.findByPrimaryKey(new AddrPK(ba.getAddrId(), ba.getCopyId()));
                    de.ejbRemove(true);
                    if (addr != null) addr.ejbRemove();
                }
                /****************MCM Impl Team XS_2.32 changes starts********************/      
                else if (dep.entityName.equals("Component")){
                    // MCM Team:changed 
                    de.ejbRemove(true);
                }
                /****************MCM Impl Team XS_2.32 changes ends********************/  
                else
                {
                    de.ejbRemove(true);
                }
            }

            //for
            srk.commitTransaction();

            // Set Target to reload the page on the position before left

            PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

            ////if (dep.targetTag != TARGET_NONE) getTheSession().getCurrentPage().setLoadTarget(dep.targetTag);
            if (dep.targetTag != TARGET_NONE)
                currPg.setLoadTarget(dep.targetTag);

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
            //// if(getTheSession().getCurrentPage().getPageId() == Mc.PGNM_APPLICANT_ENTRY ||
            ////      getTheSession().getCurrentPage().getPageId() == Mc.PGNM_PROPERTY_ENTRY)
            if(currPg.getPageId() == Mc.PGNM_APPLICANT_ENTRY ||
                    currPg.getPageId() == Mc.PGNM_PROPERTY_ENTRY || 
                    currPg.getPageId() == Mc.PGNM_COMPONENT_DETAILS)
            {
                ////getTheSession().getCurrentPage().setPageCondition3(true);
                currPg.setPageCondition3(true);
            }

        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleDelete");
            logger.error(ex);
            setStandardFailMessage();
        }

    }

    //////////////

    protected void handleCreditBureauExportDelete(
    	boolean confirmed, int type, Vector exportRowindexes, 
    	Vector deleteRowindexes, String primaryIdVar, String primaryColName,
    	String copyIdVar, String entity, int flag)
    {
    		//#DG758 SysLogger logger = getSessionResourceKit().getSysLogger();


        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(pg) == true) return;


        // Check if anything selected -- Billy 22Nov2001
        if (exportRowindexes.size() == 0 && deleteRowindexes.size() == 0)
        {
            setActiveMessageToAlert(BXResources.getSysMsg("CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
            return;
        }

        Vector pureExpInxs;// = new Vector();
        Vector expAndDelInxs;// = new Vector();
        Vector pureDelInxs;// = new Vector();


        // 1. Form arrays of indexes for pure export, pure delete, and export/delete
        // functionalities
        expAndDelInxs = getIntersection(exportRowindexes, deleteRowindexes);

        pureExpInxs = getAdjunct(exportRowindexes, expAndDelInxs);

        pureDelInxs = getAdjunct(deleteRowindexes, expAndDelInxs);

        ExportEntityProperties eep = new ExportEntityProperties(type, pureExpInxs, primaryIdVar, primaryColName, copyIdVar, entity, flag, theSessionState.getLanguageId());

        DeleteEntityProperties dep = new DeleteEntityProperties(type, pureDelInxs, primaryIdVar, primaryColName, copyIdVar, entity, theSessionState.getLanguageId());

        ExportDeleteEntityProperties edep = new ExportDeleteEntityProperties(type, expAndDelInxs, primaryIdVar, primaryColName, copyIdVar, entity, theSessionState.getLanguageId());

        HashMap content = new HashMap();

        TreeMap objects = new TreeMap();

        //ActiveMessage am = null;


        // 2. Create switcher to take a proper action and form a TreeMap of objects
        // to set it for the current session.
        if (pureExpInxs.size() > 0)
        {
            Integer one = new Integer(1);
            content.put(one, pureExpInxs);
            objects.put(one, eep);
        }

        if (pureDelInxs.size() > 0)
        {
            Integer two = new Integer(2);
            content.put(two, pureDelInxs);
            objects.put(two, dep);
        }

        if (expAndDelInxs.size() > 0)
        {
            Integer three = new Integer(3);
            content.put(three, expAndDelInxs);
            objects.put(three, edep);
        }

        Set keys = content.entrySet();

        Iterator iter = keys.iterator();


        //3. Set an appropriate objects combination in each case.
        while(iter.hasNext())
        {
            Map.Entry entry =(Map.Entry) iter.next();
            Object key = entry.getKey();
            Object value = entry.getValue();

            //// Auto-conversion is not done proper. In any case it shold be (at least temporarily)
            //// returned to the original expression to escape the overhead of the extra primitive
            //// type conversion. It is an expensive operation.
            ////int action =((Integer)com.iplanet.jato.util.TypeConverter.asInt(key));

            int action = ((Integer)key).intValue();

            switch(action)
            {
            case 1 : setActiveMessageToAlert(ActiveMsgFactory.ISNODIALOG);

            //This call is reserved in case if the Product Team decides to go
            // back to the interactive dialog with the user.
            //setActiveMessageToAlert(eep.message, ActiveMsgFactory.ISCUSTOMDIALOG, "CONFIRM_EXPORT");
            ////(theSession.getActMessage()).setResponseObject(objects);
            (theSessionState.getActMessage()).setResponseObject(objects);

            // Set Target to reload the page on the position before left
            ////if (eep.targetTag != TARGET_NONE) getTheSession().getCurrentPage().setLoadTarget(eep.targetTag);
            if (eep.targetTag != TARGET_NONE) pg.setLoadTarget(eep.targetTag);
            break;
            case 2 : setActiveMessageToAlert(ActiveMsgFactory.ISNODIALOG);

            //This call is reserved in case if the Product Team decides to go
            // back to the interactive dialog with the user.
            //setActiveMessageToAlert(dep.message, ActiveMsgFactory.ISCUSTOMDIALOG, "CONFIRM_DELETE_CREDIT_BUREAU");
            ////(theSession.getActMessage()).setResponseObject(objects);
            (theSessionState.getActMessage()).setResponseObject(objects);

            // Set Target to reload the page on the position before left
            ////if (dep.targetTag != TARGET_NONE) getTheSession().getCurrentPage().setLoadTarget(dep.targetTag);
            if (dep.targetTag != TARGET_NONE) pg.setLoadTarget(dep.targetTag);
            break;
            case 3 : setActiveMessageToAlert(ActiveMsgFactory.ISNODIALOG);

            //This call is reserved in case if the Product Team decides to go
            // back to the interactive dialog with the user.
            //setActiveMessageToAlert(edep.message, ActiveMsgFactory.ISCUSTOMDIALOG, "CONFIRM_EXPORT_DELETE_CREDIT_BUREAU");
            ////(theSession.getActMessage()).setResponseObject(objects);
            (theSessionState.getActMessage()).setResponseObject(objects);

            // Set Target to reload the page on the position before left
            ////if (edep.targetTag != TARGET_NONE) getTheSession().getCurrentPage().setLoadTarget(edep.targetTag);
            if (edep.targetTag != TARGET_NONE) pg.setLoadTarget(edep.targetTag);
            break;
            case 4 : setActiveMessageToAlert(BXResources.getSysMsg("CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
            break;
            }
        }


        // end while
        return;

    }


    /**
     *
     *
     */
    protected void handleCreditBureauMultiActionSupport(TreeMap inActions)
    {
        Set keys = inActions.entrySet();

        Iterator iter = keys.iterator();

        while(iter.hasNext())
        {
            Map.Entry entry =(Map.Entry) iter.next();
            Object key = entry.getKey();
            Object value = entry.getValue();

            //// Auto-conversion is not done proper. In any case it shold be (at least temporarily)
            //// returned to the original expression to escape the overhead of the extra primitive
            //// type conversion. It is an expensive operation.
            ////int objectType =((Integer)com.iplanet.jato.util.TypeConverter.asInt(key));

            int objectType = ((Integer)key).intValue();

            switch(objectType)
            {
            case 1 : handleCreditBureauExport((ExportEntityProperties) value);
            break;
            case 2 : handleDeleteCreditBureau((DeleteEntityProperties) value);
            break;
            case 3 : handleCreditBureauExportDelete((ExportDeleteEntityProperties) value);
            break;
            }
        }

        return;

    }


    /**
     *
     *
     */
    protected void handleCreditBureauExport(Object obj)
    {
        ExportEntityProperties eep =(ExportEntityProperties) obj;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        SessionResourceKit srk = getSessionResourceKit();

        ////PageEntry pg = theSession.getCurrentPage();

        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            int primaryid = 0;
            int copyid = 0;
            int currRow = - 1;
            int flag = 0;
            for(int i = 0; i < eep.rowindexes.size(); i ++)
            {
                /// Wrong auto-convertion.
                ////currRowTypeConverter.asInt(=((Integer)(eep.rowindexes.elementAt(i))));
                currRow = ((Integer)(eep.rowindexes.elementAt(i))).intValue();
                primaryid =(int) Double.parseDouble(readFromPage(eep.primaryIdVar, currRow));
                copyid =(int) Double.parseDouble(readFromPage(eep.copyIdVar, currRow));
                flag = eep.getLiabExportDirectionFlag();
                DealEntity de = getEntity(eep.entityName, primaryid, copyid, dcm);
                try
                {
                    int borrowerId = pg.getPageCriteriaId1();
                    Liability liability = new Liability(srk, dcm);
                    Collection borLiabs = liability.findByBorrower(new BorrowerPK(borrowerId, copyid));

                    // percentOutGDS equals 1 after the exporting from the Credit Bureau Section
                    Iterator it = borLiabs.iterator();
                    int lId = 0;
                    while(it.hasNext())
                    {
                        liability =(Liability) it.next();
                        lId = liability.getLiabilityId();
                        double percentGDS = liability.getPercentOutGDS();

                        // select the specific liablity from the collection and
                        // vefify condititions for export: from CreditBureauLiability
                        // section to the Liability section or vice versa
                        if (lId == primaryid)
                        {
                            if (flag == 1 &&
                                    //from CreditBureauLiability to Liability
                                    (percentGDS == Mc.CREDIT_BUREAU_LIABILITY_ORIGINAL || percentGDS == Mc.CREDIT_BUREAU_LIAB_EXPORTED_TO_LIABILITY))
                            {
                                liability.setPercentOutGDS(Mc.CREDIT_BUREAU_LIAB_CHANGED_IN_LIABILITY_SECTION);
                                liability.setIncludeInTDS("Y");
                                liability.setIncludeInGDS("N");
                                liability.ejbStore();
                            }

                            // end if flag = 1
                            //from Liability to CreditBureauLiability: flag = 0;
                            if (flag == 0)
                            {
                                if (percentGDS == Mc.CREDIT_BUREAU_LIAB_CHANGED_IN_LIABILITY_SECTION)
                                {
                                    liability.setPercentOutGDS(Mc.CREDIT_BUREAU_LIAB_EXPORTED_TO_LIABILITY);
                                    liability.setIncludeInTDS("N");
                                    liability.setIncludeInGDS("N");
                                    liability.ejbStore();
                                }
                                else if (percentGDS == Mc.CREDIT_BUREAU_LIABILITY_DELETED_AFTER_MODIFICATION)	// usual remove

                                {
                                    liability.ejbRemove();
                                }
                            }

                            // if flag = 0
                        }

                        // end if primaryId
                    }

                    // end while
                }
                catch(Exception e)
                {
                    logger.error("Exception @DealEntryHandler.handleCreditBureauExport(...)");
                    logger.error(e);
                }
            }

            //for
            try
            {
                doCalculation(dcm);
            }
            catch(Exception ex)
            {
                logger.error("Exception @doCalculation"+ex);
                throw new Exception("Exception @DealEntryHandler.handleCreditBureauExport");
            }
            srk.commitTransaction();

            // Set Target to reload the page on the position before left
            ////if (eep.targetTag != TARGET_NONE) getTheSession().getCurrentPage().setLoadTarget(eep.targetTag);
            if (eep.targetTag != TARGET_NONE) pg.setLoadTarget(eep.targetTag);
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleCreditBureauExport");
            logger.error(ex);
            setStandardFailMessage();
        }

    }

    /**
     *
     *
     */
    protected void handleCreditBureauExportDelete(Object obj)
    {
        ExportDeleteEntityProperties edep =(ExportDeleteEntityProperties) obj;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        SessionResourceKit srk = getSessionResourceKit();


        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            int primaryid = 0;
            int copyid = 0;
            int currRow = - 1;
            for(int i = 0; i < edep.rowindexes.size(); i ++)
            {
                // Wrong auto-convertion.
        //currRowTypeConverter.asInt(=((Integer)(edep.rowindexes.elementAt(i))));
                currRow = ((Integer)(edep.rowindexes.elementAt(i))).intValue();
                primaryid =(int) Double.parseDouble(readFromPage(edep.primaryIdVar, currRow));
                copyid =(int) Double.parseDouble(readFromPage(edep.copyIdVar, currRow));
                DealEntity de = getEntity(edep.entityName, primaryid, copyid, dcm);
                try
                {
                    int borrowerId = pg.getPageCriteriaId1();
                    Liability liability = new Liability(srk, dcm);
                    Collection borLiabs = liability.findByBorrower(new BorrowerPK(borrowerId, copyid));

                    // percentOutGDS equals 1 after the exporting from the Credit Bureau Section
                    Iterator it = borLiabs.iterator();
                    int lId = 0;
                    while(it.hasNext())
                    {
                        liability =(Liability) it.next();
                        lId = liability.getLiabilityId();
                        double percentGDS = liability.getPercentOutGDS();

                        // select the specific liablity from the collection and
                        // vefify condititions for export: from CreditBureauLiability
                        // section to the Liability section or vice versa
                        if (lId == primaryid)
                        {
                            // checkboxes for this joint action are available only for
                            // these values of percentGDS
                            if (percentGDS == Mc.CREDIT_BUREAU_LIABILITY_ORIGINAL || percentGDS == Mc.CREDIT_BUREAU_LIAB_EXPORTED_TO_LIABILITY)
                            {
                                liability.setPercentOutGDS(Mc.CREDIT_BUREAU_LIABILITY_DELETED_AFTER_MODIFICATION);
                                liability.setIncludeInTDS("Y");
                                liability.setIncludeInGDS("N");
                                liability.ejbStore();
                            }

                            // if
                        }

                        // while
                    }

                    // for
                }
                catch(Exception e)
                {
                    logger.error("Exception @DealEntryHandler.handleCreditBureauExportDelete(...)");
                    logger.error(e);
                }
            }

            //for
            try
            {
                doCalculation(dcm);
            }
            catch(Exception ex)
            {
                logger.error("Exception @doCalculation:"+ex);		//#DG758
                throw new Exception("Exception @DealEntryHandler.handleCreditBureauExport");
            }
            srk.commitTransaction();

            // Set Target to reload the page on the position before left
            if (edep.targetTag != TARGET_NONE) pg.setLoadTarget(edep.targetTag);

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
	      //getTheSession().getCurrentPage().setPageCondition3(true);
            pg.setPageCondition3(true);
        }
        catch (Exception ex)
        {
            srk.cleanTransaction();

            logger.error("Exception @handleCreditBureauExport");
            logger.error(ex);

            setStandardFailMessage();
        }
    }


    /**
     *
     *
     */
    protected void handleDeleteCreditBureau(Object obj)
    {
        DeleteEntityProperties dep =(DeleteEntityProperties) obj;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        SessionResourceKit srk = getSessionResourceKit();


        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            int primaryid = 0;
            int copyid = 0;
            int currRow = - 1;
            for(int i = 0; i < dep.rowindexes.size(); i ++)
            {
                // Wrong auto-convertion.
        //currRowTypeConverter.asInt(=((Integer)(dep.rowindexes.elementAt(i))));
                currRow = ((Integer)(dep.rowindexes.elementAt(i))).intValue();
                primaryid =(int) Double.parseDouble(readFromPage(dep.primaryIdVar, currRow));
                copyid =(int) Double.parseDouble(readFromPage(dep.copyIdVar, currRow));
                DealEntity de = getEntity(dep.entityName, primaryid, copyid, dcm);
                try
                {
                    int borrowerId = pg.getPageCriteriaId1();
                    Liability liability = new Liability(srk, dcm);
                    Collection borLiabs = liability.findByBorrower(new BorrowerPK(borrowerId, copyid));

                    // percentOutGDS equals 1 after the exporting from the Credit Bureau Section
                    Iterator it = borLiabs.iterator();
                    int lId = 0;
                    while(it.hasNext())
                    {
                        liability =(Liability) it.next();
                        lId = liability.getLiabilityId();
                        double percentGDS = liability.getPercentOutGDS();

                        // select the specific liablity from the collection and
                        // vefify condititions for export: from CreditBureauLiability
                        // section to the Liability section or vice versa
                        if (lId == primaryid)
                        {
                            if (percentGDS == Mc.CREDIT_BUREAU_LIAB_CHANGED_IN_LIABILITY_SECTION)
                            {
                                liability.setPercentOutGDS(Mc.CREDIT_BUREAU_LIABILITY_DELETED_AFTER_MODIFICATION);
                                liability.ejbStore();
                            }

                            // end if percentGDS == 1
                            if (percentGDS == Mc.CREDIT_BUREAU_LIABILITY_ORIGINAL || percentGDS == Mc.CREDIT_BUREAU_LIAB_EXPORTED_TO_LIABILITY || percentGDS == Mc.CREDIT_BUREAU_LIABILITY_DELETED_AFTER_MODIFICATION)
                            {
                                liability.ejbRemove();
                            }

                            // end if percentGDS == 2, 3 or 0
                        }

                        // end external if
                    }
                }
                catch(Exception e)
                {
                    logger.error("Exception @DealEntryHandler.handleDeleteCreditBureau(...)");
                    logger.error(e);
                }
            }

            //for
            try
            {
                doCalculation(dcm);
            }
            catch(Exception ex)
            {
                logger.error("Exception @doCalculation:"+ex);		//#DG758
                throw new Exception("Exception @DealEntryHandler.handleDeleteCreditBureau");
            }
            srk.commitTransaction();

            // Set Target to reload the page on the position before left
            //if (dep.targetTag != TARGET_NONE) getTheSession().getCurrentPage().setLoadTarget(dep.targetTag);
            if (dep.targetTag != TARGET_NONE) pg.setLoadTarget(dep.targetTag);

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
      //getTheSession().getCurrentPage().setPageCondition3(true);
            pg.setPageCondition3(true);

        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleCreditBureauExport");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    protected void handleCreditBureauExport(boolean confirmed, int type, Vector rowindexes, String primaryIdVar, String primaryColName, String copyIdVar, String entity, int flag)
    {
        logger.trace("--T-----> @DealEntryHandler.handleCreditBureauExport(...)");

        SysLogger logger = getSessionResourceKit().getSysLogger();

        ////PageEntry pg = theSession.getCurrentPage();
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        if (disableEditButton(pg) == true) return;

        String tmp = "";

        for(int i = 0; i < rowindexes.size(); i ++)
        {
            // Auto-conversion is not done proper. In any case it shold be (at least temporarily)
			      // returned to the original expression to escape the overhead of the extra primitive
			      // type conversion. It is an expensive operation.
			      //tmp +=((Integer)com.iplanet.jato.util.TypeConverter.asInt(rowindexes.elementAt(i))) + ", ";
            tmp += ((Integer)rowindexes.elementAt(i)).intValue() + ", ";
        }

        ExportEntityProperties eep = new ExportEntityProperties(type, rowindexes, primaryIdVar, primaryColName, copyIdVar, entity, flag, theSessionState.getLanguageId());


        // Check if any row selected
        if (rowindexes == null || rowindexes.size() <= 0)
        {
            setActiveMessageToAlert(BXResources.getSysMsg("CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_NOT_SELECTED", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
        }
        else
        {
            setActiveMessageToAlert(eep.message, ActiveMsgFactory.ISCUSTOMDIALOG, "CONFIRM_EXPORT");
            TreeMap expFromLiabObject = new TreeMap();
            Integer one = new Integer(1);
            expFromLiabObject.put(one, eep);
            ////(theSession.getActMessage()).setResponseObject(expFromLiabObject);
            (theSessionState.getActMessage()).setResponseObject(expFromLiabObject);

            // Set Target to reload the page on the position before left
            ////if (eep.targetTag != TARGET_NONE) getTheSession().getCurrentPage().setLoadTarget(eep.targetTag);
            if (eep.targetTag != TARGET_NONE) pg.setLoadTarget(eep.targetTag);
        }

        return;

    }


    /***********************************************************************
     ************   Handle Viewiewing Sub Pages
     ************************************************************************/
    public void handleApplicantsView(int rowindx, String filedName, String copyfiledname)
    {
        handleApplicantsView(rowindx, filedName, copyfiledname, Mc.TARGET_NONE);

    }


    /**
     *
     *
     */
    //--> Adjusted by Billy for Applicant Screen -- 26July2002
    public void handleApplicantsView(int rowindx, String filedName, String copyfiledname, int loadTarget)
    {
        int borrid = 0;

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        //int copyid = getTheSession().getCurrentPage().getPageDealCID();
        int copyid = currPg.getPageDealCID();

        //IF there's more than one applicant then ND stores the html vales
        //in to list need the get correct one using btn clicked index
        //if (borrlist instanceof Vector){
		      // Auto-conversion version is wrong. It is adjusted as is follows.
		      //borrid =((Vector)com.iplanet.jato.util.TypeConverter.asInt(borrlist).get(rowindx));
        //	borrid =((Integer)((Vector)borrlist).elementAt(rowindx)).intValue();
        //}
        //else {
        //	borrid = com.iplanet.jato.util.TypeConverter.asInt(borrlist);
        //}

        borrid = com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField(filedName, rowindx));

        PageEntry pgEntry = setupSubPagePageEntry(currPg, Mc.PGNM_APPLICANT_ENTRY);

        pgEntry.setPageCriteriaId1(borrid);

        pgEntry.setPageCriteriaId2(copyid);


        // Set target position when loading the page
        if (loadTarget != Mc.TARGET_NONE) pgEntry.setLoadTarget(loadTarget);

        // make sure to display 'Cancel' button -- By BILLY 03May2002
        pgEntry.setPageCondition3(false);

        getSavedPages().setNextPage(pgEntry);

        navigateToNextPage(true);

    }

    /**
     *
     *
     */
    public void handlePropertyView(int rowindx, String fieldName, String copyfiledname)
    {
        Object propertylist =(Object)(getCurrNDPage().getDisplayFieldValue(fieldName));
        //logger.debug("DHC@handlePropertyView::propertyList: " + propertylist);

        Object copylist =(Object)(getCurrNDPage().getDisplayFieldValue(copyfiledname));
        //logger.debug("DHC@handlePropertyView::copyList: " + copylist);

        ////PageEntry currPg = theSessionState.getCurrentPage();
        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();
        //logger.debug("DHC@handlePropertyView::CurrentPage: " + currPg);

        if (propertylist == null)
        {
            return;
        }

        int propertyid = 0;

        int copyid = 0;

        //IF there's more than one applicant then ND stores the html vales
        //in to list need the get correct one using btn clicked index
        //if (propertylist instanceof Vector)
        //{
            // Auto-conversion is wrong. It is adjusted as is follows.
      //propertyid = ((Vector)com.iplanet.jato.util.TypeConverter.asInt(propertylist).get(rowindx));
        ///copyid = ((Vector)com.iplanet.jato.util.TypeConverter.asInt(copylist).get(rowindx));

    //   propertyid = ((Integer)((Vector)propertylist).elementAt(rowindx)).intValue();
        //	copyid = ((Integer)((Vector)copylist).elementAt(rowindx)).intValue();
        //}
        //else
        //{
    //  propertyid = com.iplanet.jato.util.TypeConverter.asInt(propertylist);
        //	copyid = com.iplanet.jato.util.TypeConverter.asInt(copylist);
        //}

        propertyid = com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField(fieldName, rowindx));
        copyid = com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField(copyfiledname, rowindx));

        //logger.debug("DHC@handlePropertyView::RowIndex: " + rowindx);
        //logger.debug("DHC@handlePropertyView::PropertyId: " + propertyid);
        //logger.debug("DHC@handlePropertyView::CopyId: " + copyid);

        PageEntry pgEntry = setupSubPagePageEntry(currPg, Mc.PGNM_PROPERTY_ENTRY);

        pgEntry.setPageCriteriaId1(propertyid);

        pgEntry.setPageCriteriaId2(copyid);

        // make sure to display 'Cancel' button -- By BILLY 03May2002
        pgEntry.setPageCondition3(false);
        //logger.debug("DHC@handlePropertyView::After setup Page Criteria");

        getSavedPages().setNextPage(pgEntry);
        //logger.debug("DHC@handlePropertyView::After getSavedPages");

        navigateToNextPage(true);
        //logger.debug("DHC@handlePropertyView::After navigateToNextPage");
    }

    /**
     *
     *
     */
    public void handleDisplayPartySummary()
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

    		//#DG758 SysLogger logger = srk.getSysLogger();
    	  Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());

        try
        {
            srk.beginTransaction();
            PageEntry subPg = setupSubPagePageEntry(pg, Mc.PGNM_PARTY_SUMMARY);
            getSavedPages().setNextPage(subPg);
            navigateToNextPage();
            srk.commitTransaction();
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleDisplayPartySummary().");
            logger.error(e);
            setStandardFailMessage();
        }

    }

    //-- FXLink Phase II --//
    //--> Method to check if Change Source is allowed
  //-->   - Source cannot be changed if deal.ChannelMedia = 'E'  --> By Billy 17Nov2003
    public boolean isChangeSourceAllowed()
    {
        try
        {
            PageEntry pg = theSessionState.getCurrentPage();
            String channelMedia = new Deal(srk, null).getChannelMedia(pg.getPageDealId(), pg.getPageDealCID());
            if(channelMedia != null && channelMedia.equals("E"))
                return false;
            else
                return true;
        }
        catch(Exception e)
        {
            logger.error("Exception occurred @DealHandlerCommon.isChangeSourceAllowed() : " + e);
            return true;
        }
    }
    //==========================================================

    //-- FXLink Phase II --//
    //--> Method to check if display Market Type
    //-->   - Both Label and Description is not dispalyed if mccMarketType field = null
    //--> By Billy 18Nov2003
    public boolean isDisplayMarketType()
    {
        try
        {
            PageEntry pg = theSessionState.getCurrentPage();
            String theMarketType = new Deal(srk, null).getMccMarketType(pg.getPageDealId(), pg.getPageDealCID());
            if(theMarketType == null || theMarketType.trim().equals(""))
                return false;
            else
                return true;
        }
        catch(Exception e)
        {
            logger.error("Exception occurred @DealHandlerCommon.isDisplayMarketType() : " + e);
            return false;
        }
    }
    //--> Method to get the Market Type Label
    //--> By Billy 18Nov2003
    public String getMarketTypeLabel()
    {
        return BXResources.getGenericMsg("MARKETTYPE_LABEL", theSessionState.getLanguageId());
    }
    //==========================================================

    /**
     *
     *
     */
    public void handleChangeSource()
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(pg) == true) return;

        SessionResourceKit srk = getSessionResourceKit();

    		//#DG758 SysLogger logger = srk.getSysLogger();
    	  Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());
        try
        {
            srk.beginTransaction();
            PageEntry subPg = setupSubPagePageEntry(pg, Mc.PGNM_SOB_SEARCH);
            getSavedPages().setNextPage(subPg);
            navigateToNextPage();
            srk.commitTransaction();
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleSourceOfBusinessSearch().");
            logger.error(e);
            setStandardFailMessage();
        }

    }

    //--DJ_PT_CR--start//
    private int getPartyTypeId()
    {
        doPartySearchModel theDO = (doPartySearchModel)
        RequestManager.getRequestContext().getModelManager().getModel(doPartySearchModel.class);

        int indx = 0;

        Object partyType = theDO.getValue(theDO.FIELD_DFPARTYTYPE);
        if(partyType != null) {
            indx = new Integer(partyType.toString()).intValue();
            logger.debug("DHC@getPartyTypeId:: " + new Integer(partyType.toString()).intValue());
        }

        return indx;
    }

  // Display Transit Number related info (and label) only if the Party Type is
  // 'Origination Branch'.
    public boolean isDisplayBranchOriginationInfo()
    {
        try
        {
            PageEntry pg = theSessionState.getCurrentPage();
            Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

            Collection partyProfileIds = deal.getPartyProfiles();
            Iterator iParty = partyProfileIds.iterator();
            int partyTypeId = 0;

            while(iParty.hasNext())
            {
                partyTypeId =((PartyProfile)iParty.next()).getPartyTypeId();

                if(partyTypeId == Mc.PARTY_TYPE_ORIGINATION_BRANCH)
                    break;
            }
            logger.debug("DHC@isDisplayBranchOriginationInfo::PartyTypeId: " + partyTypeId);

            if(partyTypeId != Mc.PARTY_TYPE_ORIGINATION_BRANCH)
                return false;
            else
                return true;
        }
        catch(Exception e)
        {
            logger.error("Exception occurred @UWorksheet.isDisplayBranchOriginationInfo() : " + e);
            return false;
        }
    }

    public String getTransitNumberLabel()
    {
        return BXResources.getGenericMsg("BRANCH_ORIGINATION_LABEL", theSessionState.getLanguageId());
    }

    public boolean isDisplayBranchOriginationLabel()
    {
        boolean isDisplayBranchOriginationLabel = false; // default.
        // regular execution path
        if (PropertiesCache.getInstance().getProperty(-1, "com.basis100.calc.isdjclient", "N").equals("N"))
        {
            isDisplayBranchOriginationLabel = false;
        }
        // DJ execution path
        else if (PropertiesCache.getInstance().getProperty(-1, "com.basis100.calc.isdjclient", "N").equals("Y"))
        {
            isDisplayBranchOriginationLabel = true;
        }
        logger.debug("DHC@isDisplayBranchOriginationLabel::isDisplayBranchOriginationLabel? " + isDisplayBranchOriginationLabel);

        return isDisplayBranchOriginationLabel;
    }
    //--DJ_PT_CR--end//

    /**
     *
     *
     */
    public void handleReviewBridge()
    {

        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

    		//#DG758 SysLogger logger = srk.getSysLogger();
    	  Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());
        try
        {
            srk.beginTransaction();
            PageEntry subPg = setupSubPagePageEntry(pg, Mc.PGNM_BRIDGE_REVIEW);
            getSavedPages().setNextPage(subPg);
            navigateToNextPage();
            srk.commitTransaction();
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleReviewBridge().");
            logger.error(e);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    public void handleFeeReview()
    {

        PageEntry pg = theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

    		//#DG758 SysLogger logger = srk.getSysLogger();
    	  Log logger = LogFactory.getLog(getClass().getName()+':'+srk.getIdentity());
        try
        {
            srk.beginTransaction();
            PageEntry subPg = setupSubPagePageEntry(pg, Mc.PGNM_FEE_REVIEW);
            getSavedPages().setNextPage(subPg);
            navigateToNextPage();
            srk.commitTransaction();
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleFeeReview.");
            logger.error(e);
            setStandardFailMessage();
        }

    }


    /***********************************************************************
     ************   Adding, Updating Deal Entities
     ************************************************************************/
    public void handleAddDownpayment()
    {

        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(pg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());


        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            int dealid = pg.getPageDealId();
            int copyid = pg.getPageDealCID();
            handleAddDownpayment(getDeal(dealid, copyid, dcm), dcm);
            pg.setLoadTarget(TARGET_DE_DOWNPAYMENT);
            doCalculation(dcm);
            srk.commitTransaction();
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddDownpayment");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    private void handleAddDownpayment(Deal deal, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "DownPaymentSource", deal, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddDownpayment");
            logger.error(ex);
            throw new Exception("Add entity exception.");
        }

    }


    /**
     *
     *
     */
    public void handleAddEscrowPayment()
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(pg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());


        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            int dealid = pg.getPageDealId();
            int copyid = pg.getPageDealCID();
            Deal deal = getDeal(dealid, copyid, dcm);
            handleAddEscrowPayment(deal, dcm);
            pg.setLoadTarget(TARGET_DE_ESCROW);
            doCalculation(dcm);
            srk.commitTransaction();
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddEscrowPayment");
            logger.error(ex);
            setStandardFailMessage();
        }

    }

    /**
     *
     *
     */
    private void handleAddEscrowPayment(Deal deal, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "EscrowPayment", deal, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddEscrowPayment");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }

    }

    /**
     *
     *
     */
    public void handleUpdateDeal(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For Deal Update");
            updateForMainRows(propList, keyValues, "Deal", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateDeal");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateDownPay(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For Down Pay Update");
            updateForRepeatedRows(propList, keyValues, "DownPaymentSource", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateDownPay");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateEscrowPay(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For Escrow Pay Update");
            updateForRepeatedRows(propList, keyValues, "EscrowPayment", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateEscrowPay");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }

    }


    /// Adding Applicant handles also adding Borrower Address, Employement History,
    /// Income, Credit Ref, Asset, Liability

    public void handleAddApplicant()
    {

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        //if (disableEditButton(pg) == true) return;
        if (disableEditButton(currPg) == true) return;

        SessionResourceKit srk = getSessionResourceKit();

        //int dealid = getTheSession().getCurrentPage().getPageDealId();
        int dealid = currPg.getPageDealId();

        //int copyid = theSession.getCurrentPage().getPageDealCID();
        int copyid = currPg.getPageDealCID();

        // Check if the Primary Borrower exists
        try
        {
            Borrower thePBorrower = new Borrower(srk, null);
            thePBorrower = thePBorrower.findByPrimaryBorrower(dealid, copyid);
            BorrowerAddress theCurrAdd = new BorrowerAddress(srk, null);
            theCurrAdd = theCurrAdd.findByCurrentAddress(thePBorrower.getBorrowerId(), copyid);
            if (theCurrAdd == null)
            {
                //Shouldn't happened -- just in case
                handleAddApplicant(false);
                return;
            }
        }
        catch(FinderException fe)
        {
            // PrimaryBorrower not defined yet or the PrimaryBorrower doesn't has current address
            handleAddApplicant(false);
            return;
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddApplicant");
            logger.error(ex);
            setStandardFailMessage();
        }


        // The PrimaryBorrower's current address found
        //    -- Display Dialog to ask user to copy address or not
        setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_ADD_BORROWER_COPY_ADDR", theSessionState.getLanguageId()),
                ActiveMsgFactory.ISCUSTOMDIALOG2, "ADDR_YES", "ADDR_NO");

        // navigateToNextPage();
        return;

    }


    /**
     *
     *
     */
    public void handleAddApplicant(boolean isCopyAddress)
    {

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());


        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            //int dealid = getTheSession().getCurrentPage().getPageDealId();
            //int copyid = theSession.getCurrentPage().getPageDealCID();
            int dealid = currPg.getPageDealId();
            int copyid = currPg.getPageDealCID();

            Deal deal = getDeal(dealid, copyid, dcm);

            // in advance determine borrower number
            int borrNumber = 1;
            int numberOfBorrowers = 1;
            if ((deal.getNumberOfBorrowers() + deal.getNumberOfGuarantors()) > 0)
            {
                Collection borrowers = deal.getBorrowers();
                int lim = borrowers.size();
                Object [ ] objs = borrowers.toArray();
                int highest = - 1;
                for(int i = 0;
                i < lim;
                ++ i)
                {
                    int bn =((Borrower) objs [ i ]).getBorrowerNumber();
                    if (bn > highest) highest = bn;
                }
                numberOfBorrowers = deal.getNumberOfBorrowers() + 1;
                borrNumber = highest + 1;
            }
            Borrower borrower =(Borrower) DBA.createEntity(this.getSessionResourceKit(), "Borrower", deal, dcm);
            deal.setNumberOfBorrowers(numberOfBorrowers);
            borrower.setBorrowerNumber(borrNumber);
            if (borrNumber == 1)
            {
                deal.setNumberOfGuarantors(0);
                borrower.setPrimaryBorrowerFlag("Y");

                // default to primary borrower on first
            }
            deal.ejbStore();

            // Check if need to copy Borrower address
            if (isCopyAddress)
            {
                // Check if the Primary Borrower and Current exists
                try
                {
                    Borrower thePBorrower = new Borrower(srk, null);
                    thePBorrower = thePBorrower.findByPrimaryBorrower(dealid, copyid);
                    if (thePBorrower.getBorrowerId() > 0)
                    {
                        // Copy HomePhone # and FirstTimeBuyer flag
                        borrower.setBorrowerHomePhoneNumber(thePBorrower.getBorrowerHomePhoneNumber());
                        borrower.setFirstTimeBuyer(thePBorrower.getFirstTimeBuyer());

                        // Copy Current address if exist
                        BorrowerAddress theCurrAdd = new BorrowerAddress(srk, null);
                        theCurrAdd = theCurrAdd.findByCurrentAddress(thePBorrower.getBorrowerId(), copyid);
                        if (theCurrAdd.getBorrowerAddressId() > 0)
                        {
                            // Do copy
                            BorrowerAddress theBA = new BorrowerAddress(srk, null);
                            theBA = theBA.copyBAddress(borrower, theCurrAdd);
                        }
                    }
                }
                catch(FinderException fe)
                {
                    // PrimaryBorrower not defined yet or the PrimaryBorrower doesn't has current address
                    logger.warning("(nonCritical) @handleAddApplicant -- Try to copy PrimaryBorrower's Current Address but not found !!");
                }
            }
            borrower.ejbStore();
            doCalculation(dcm);

            // go directly to the new borrower
            PageEntry pgEntry = setupSubPagePageEntry(currPg, Mc.PGNM_APPLICANT_ENTRY);
            pgEntry.setPageCriteriaId1(borrower.getBorrowerId());
            pgEntry.setPageCriteriaId2(borrower.getCopyId());
            getSavedPages().setNextPage(pgEntry);
            navigateToNextPage();
            srk.commitTransaction();
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddApplicant");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    public void handleAddBorrowerAddress()
    {
        // PageEntry pg = theSession.getCurrentPage();
        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        //if (disableEditButton(pg) == true) return;
        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());


        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            Borrower borrower = getSelectedBorrower(dcm);
            if (borrower != null)
            {
                handleAddBorrowerAddress(borrower, dcm);
                //getTheSession().getCurrentPage().setLoadTarget(TARGET_APP_ADDRESS);
                currPg.setLoadTarget(TARGET_APP_ADDRESS);
                doCalculation(dcm);
            }
            srk.commitTransaction();

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
            currPg.setPageCondition3(true);
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddBorrowerAddress");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    private void handleAddBorrowerAddress(String borrowerIdLabel, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Borrower borrower = getBorrower(borrowerIdLabel, dcm);
            if (borrower == null) return;
            handleAddBorrowerAddress(borrower, dcm);
        }
        catch(Exception e)
        {
            logger.error("Exception @handleAddBorrowerAddress");
            logger.error(e);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    private void handleAddBorrowerAddress(Borrower borrower, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            BorrowerAddress borrAddr =(BorrowerAddress) DBA.createEntity(this.getSessionResourceKit(), "BorrowerAddress", borrower, dcm, true);
            borrAddr.ejbStore();
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddBorrowerAddress");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleAddEmploymentHistory()
    {

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());


        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            Borrower borrower = getSelectedBorrower(dcm);
            if (borrower != null)
            {
                handleAddEmploymentHistory(borrower, dcm);
                //getTheSession().getCurrentPage().setLoadTarget(TARGET_APP_EMPLOYMENT);
                currPg.setLoadTarget(TARGET_APP_EMPLOYMENT);
                doCalculation(dcm);
            }
            srk.commitTransaction();

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
            currPg.setPageCondition3(true);

        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddEmploymentHistory");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    private void handleAddEmploymentHistory(String borrowerIdLabel, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Borrower borrower = getBorrower(borrowerIdLabel, dcm);
            if (borrower == null) return;
            handleAddEmploymentHistory(borrower, dcm);
        }
        catch(Exception e)
        {
            logger.error("Exception @handleAddEmploymentHistory");
            logger.error(e);
            throw new Exception("Add entity handler exception");
        }
    }


    /**
     *
     *
     */
    private void handleAddEmploymentHistory(Borrower borrower, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            EmploymentHistory emphist =(EmploymentHistory) DBA.createEntity(this.getSessionResourceKit(), "EmploymentHistory", borrower, dcm, true);
            DBA.createEntity(this.getSessionResourceKit(), "Income", emphist, dcm, true);

            //Contact contact = (Contact)DBA.createEntity("Contact",emphist,true);
            //DBA.createEntity("Addr",contact,true);
            //contact.ejbStore();
            emphist.ejbStore();
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddEmploymentHistory");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }

    }
    //  ***** Change by NBC Impl. Team - Version 1.11 - Start *****//

    /**
     * <p>
     * Handles add request for new Identification Type
     * </p>
     */
    public void handleAddIdentificationType() {

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true)
            return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        SessionResourceKit srk = getSessionResourceKit();

        try {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            Borrower borrower = getSelectedBorrower(dcm);
            if (borrower != null) {
                handleAddIdentificationType(borrower, dcm);
                currPg.setLoadTarget(TARGET_APP_IDENTIFICATION);
                doCalculation(dcm);
            }
            srk.commitTransaction();

            currPg.setPageCondition3(true);
        } catch (Exception ex) {
            srk.cleanTransaction();
            logger.error("Exception @handleAddIdentification");
            logger.error(ex);
            setStandardFailMessage();
        }

    }

    /**
     * <p>
     * Handles add request for new Identification Type
     * </p>
     * 
     * @param borrower
     * @param dcm
     * @throws Exception
     */
    private void handleAddIdentificationType(Borrower borrower, CalcMonitor dcm)
    throws Exception {
        try {
            DBA.createEntity(this.getSessionResourceKit(),
                    "BorrowerIdentification", borrower, dcm);
        } catch (Exception ex) {
            logger.error("Exception @handleAddIncome");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }
    }

    // ***** Change by NBC Impl. Team - Version 1.11 - End *****//

    /**
     *
     *
     */
    public void handleAddIncome()
    {

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            Borrower borrower = getSelectedBorrower(dcm);
            if (borrower != null)
            {
                handleAddIncome(borrower, dcm);
                //getTheSession().getCurrentPage().setLoadTarget(TARGET_APP_INCOME);
                currPg.setLoadTarget(TARGET_APP_INCOME);
                doCalculation(dcm);
            }
            srk.commitTransaction();

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
            currPg.setPageCondition3(true);
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddIncome");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    private void handleAddIncome(String borrowerIdLabel, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Borrower borrower = getBorrower(borrowerIdLabel, dcm);
            if (borrower == null) return;
            handleAddIncome(borrower, dcm);
        }
        catch(Exception e)
        {
            logger.error("Exception @handleAddIncome");
            logger.error(e);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    private void handleAddIncome(Borrower borrower, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "Income", borrower, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddIncome");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleAddCreditRef()
    {

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());


        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            Borrower borrower = getSelectedBorrower(dcm);
            if (borrower != null)
            {
                handleAddCreditRef(borrower, dcm);
                //getTheSession().getCurrentPage().setLoadTarget(TARGET_APP_CREDIT);
                currPg.setLoadTarget(TARGET_APP_CREDIT);
                doCalculation(dcm);
            }
            srk.commitTransaction();

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
            currPg.setPageCondition3(true);
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddCreditRef");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    private void handleAddCreditRef(String borrowerIdLabel, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Borrower borrower = getBorrower(borrowerIdLabel, dcm);
            if (borrower == null) return;
            handleAddCreditRef(borrower, dcm);
        }
        catch(Exception e)
        {
            logger.error("Exception @handleAddCreditRef");
            logger.error(e);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    private void handleAddCreditRef(Borrower borrower, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "CreditReference", borrower, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddCreditRef");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleAddAsset()
    {

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());


        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            Borrower borrower = getSelectedBorrower(dcm);
            if (borrower != null)
            {
                handleAddAsset(borrower, dcm);
                //getTheSession().getCurrentPage().setLoadTarget(TARGET_APP_ASSET);
                currPg.setLoadTarget(TARGET_APP_ASSET);
                doCalculation(dcm);
            }
            srk.commitTransaction();

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
            currPg.setPageCondition3(true);
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddAsset");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    private void handleAddAsset(String borrowerIdLabel, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            logger.debug("DHC@StampVova");
            Borrower borrower = getBorrower(borrowerIdLabel, dcm);
            if (borrower == null) return;
            handleAddAsset(borrower, dcm);
        }
        catch(Exception e)
        {
            logger.error("Exception @handleAddAsset");
            logger.error(e);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    private void handleAddAsset(Borrower borrower, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "Asset", borrower, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddAsset");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }
    }


    /**
     *
     *
     */
    public void handleAddLiability()
    {

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());


        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            Borrower borrower = getSelectedBorrower(dcm);
            if (borrower != null)
            {
                handleAddLiability(borrower, dcm);
                //getTheSession().getCurrentPage().setLoadTarget(TARGET_APP_LIAB);
                currPg.setLoadTarget(TARGET_APP_LIAB);
                doCalculation(dcm);
            }
            srk.commitTransaction();

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
            currPg.setPageCondition3(true);
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddLiability");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    private void handleAddLiability(String borrowerIdLabel, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Borrower borrower = getBorrower(borrowerIdLabel, dcm);
            if (borrower == null) return;
            handleAddLiability(borrower, dcm);
        }
        catch(Exception e)
        {
            logger.error("Exception @handleAddLiability");
            logger.error(e);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    private void handleAddLiability(Borrower borrower, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "Liability", borrower, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddLiability");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateApplicant(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For Applicant Update");
            updateForMainRows(propList, keyValues, "Borrower", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateApplicant");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateApplicantAddress(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For ApplicantAddress Update");
            updateForRepeatedRows(propList, keyValues, "BorrowerAddress", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateApplicantAddress");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateApplicantAddressAddr(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For ApplicantAddressAddr Update");
            updateForRepeatedRows(propList, keyValues, "Addr", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateApplicantAddressAddr");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateOtherIncome(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For Other Income Update");
            updateForRepeatedRows(propList, keyValues, "Income", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateOtherIncome");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateCreditRef(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For ApplicantAddressAddr Update");
            updateForRepeatedRows(propList, keyValues, "CreditReference", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateCreditRef");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateAsset(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For ApplicantAddressAddr Update");
            updateForRepeatedRows(propList, keyValues, "Asset", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateAsset");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateLiability(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For ApplicantAddressAddr Update");
            updateForRepeatedRows(propList, keyValues, "Liability", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateLiability");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }

    //  ***** Change by NBC Impl. Team - Version 1.11 - Start *****//
    /**
     * <p>
     * Handles update request for new Identification Type
     * </p>
     * 
     * @param propName
     * @param keyValues
     * @param dcm
     * @throws Exception
     */
    public void handleUpdateIdentification(String propName, String[] keyValues,
            CalcMonitor dcm) throws Exception {
        try {
            Hashtable propList = getPropertiesForThisPage(propName);

            if (propList == null)
				throw new Exception("Null Property File For handleUpdateIdentification Update");

			updateForRepeatedRows(propList, keyValues, "BorrowerIdentification", dcm);

        } catch (Exception ex) {
            logger.error("Exception @handleUpdateIdentification");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }
    }    

    //  ***** Change by NBC Impl. Team - Version 1.11 - End *****//

    /**
     *
     *
     */
    public void handleUpdateEmployment(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For handleUpdateEmployment Update");
            updateForRepeatedRows(propList, keyValues, "EmploymentHistory", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateEmployment");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateEmploymentContact(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For ApplicantAddressAddr Update");
            updateForRepeatedRows(propList, keyValues, "Contact", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateEmploymentContact");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateEmploymentIncome(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For ApplicantAddressAddr Update");
            updateForRepeatedRows(propList, keyValues, "Income", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateEmploymentIncome");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateEmploymentAddr(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For ApplicantAddressAddr Update");
            updateForRepeatedRows(propList, keyValues, "Addr", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateEmploymentAddr");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    ///////////////////Property Handlers

    public void handleAddProperty()
    {
        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            //int dealid = getTheSession().getCurrentPage().getPageDealId();
            //int copyid = theSession.getCurrentPage().getPageDealCID();
            int dealid = currPg.getPageDealId();
            int copyid = currPg.getPageDealCID();

            Deal deal = getDeal(dealid, copyid, dcm);

            // determine property number in advance
            int propNumber = 1;
            int numberOfProperties = 1;
            if (deal.getNumberOfProperties() > 0)
            {
                Collection properties = deal.getProperties();
                int lim = properties.size();
                Object [ ] objs = properties.toArray();
                int highest = - 1;
                for(int i = 0;
                i < lim;
                ++ i)
                {
                    int bn =((Property) objs [ i ]).getPropertyOccurenceNumber();
                    if (bn > highest) highest = bn;
                }
                numberOfProperties = lim + 1;
                propNumber = highest + 1;
            }
            Property prop =(Property) DBA.createEntity(this.getSessionResourceKit(), "Property", deal, dcm);
            deal.setNumberOfProperties(numberOfProperties);
            prop.setPropertyOccurenceNumber(propNumber);
            if (propNumber == 1) prop.setPrimaryPropertyFlag("Y");
            deal.ejbStore();
            prop.ejbStore();
            doCalculation(dcm);

            // go directly to the new property
            PageEntry pgEntry = setupSubPagePageEntry(currPg, Mc.PGNM_PROPERTY_ENTRY);
            pgEntry.setPageCriteriaId1(prop.getPropertyId());
            pgEntry.setPageCriteriaId2(prop.getCopyId());
            getSavedPages().setNextPage(pgEntry);
            navigateToNextPage();
            srk.commitTransaction();
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddProperty");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    public void handleAddPropertyExpense()
    {
        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(currPg) == true) return;

        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            noAuditIfIncomplete(null, null);
            Property property = getSelectedProperty(dcm);
            if (property != null)
            {
                handleAddPropertyExpense(property, dcm);
                ////getTheSession().getCurrentPage().setLoadTarget(TARGET_PRO_PROPERTY_EXPENSE);
                currPg.setLoadTarget(TARGET_PRO_PROPERTY_EXPENSE);
                doCalculation(dcm);
            }
            srk.commitTransaction();

            // Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
            currPg.setPageCondition3(true);
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAddPropertyExpense");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    private void handleAddPropertyExpense(String propertyIdLabel, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Property property = getProperty(propertyIdLabel, dcm);
            if (property == null) return;
            handleAddPropertyExpense(property, dcm);
        }
        catch(Exception e)
        {
            logger.error("Exception @handleAddPropertyExpense");
            logger.error(e);
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    private void handleAddPropertyExpense(Property property, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "PropertyExpense", property, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddPropertyExpense");
            // Catherine, 16-May-05 -------- begin ---------
            // logger.error(ex);
            logger.error(StringUtil.stack2string(ex));
            // Catherine, 16-May-05 -------- end ---------
            throw new Exception("Add entity handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdateProperty(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        logger.debug("@DealHandlerCommon.handleUpdateProperty:: propName =" + propName);

        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For handleUpdateProeprty Update");
            updateForMainRows(propList, keyValues, "Property", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateProperty");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    /**
     *
     *
     */
    public void handleUpdatePropertyExpense(String propName, String[] keyValues, CalcMonitor dcm)
    throws Exception
    {
        try
        {
            Hashtable propList = getPropertiesForThisPage(propName);
            if (propList == null) throw new Exception("Null Property File For handleUpdatePropertyExpense Update");
            updateForRepeatedRows(propList, keyValues, "PropertyExpense", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdatePropertyExpense");
            logger.error(ex);
            throw new Exception("Update handler exception");
        }

    }


    /////////////////
    ////Genarl Update Call
    /////////////////

    public void saveData(PageEntry pg, boolean calledInTransaction)
    {

        PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            noAuditIfIncomplete(pg, null);
            if (calledInTransaction == false) srk.beginTransaction();
            SaveEntity(getCurrNDPage());
            if (calledInTransaction == false) srk.commitTransaction();
            if (srk.getModified()) pe.setModified(true);
        }
        catch(Exception ex)
        {
            if (calledInTransaction == false) srk.cleanTransaction();
            logger.error("Exception @saveDate().");
            logger.error(ex);
        }

    }


    /*Assume that primary Key Value is specified at index 0 of the keyValues Array
     **and Copy Id Value is specified at index 1 of the keyValues Array
     **This method call is used for sigle row values
     **Get primary id and copy id value from current page
     **Contrsuct the entity instance using entity builder
     **Call parsePageValues to Entity properties
     **/
    public void updateForMainRows(Hashtable propList, String[] keyValues, String entityName, CalcMonitor dcm)
    throws Exception
    {

        //logger.debug("--T--> @DealHandlerCommon.updateForMIanRows:: entityName =" + entityName);
        try
        {
            int primaryid =(int) Double.parseDouble(readFromPage(keyValues [ 0 ], - 1));
            int copyid =(int) Double.parseDouble(readFromPage(keyValues [ 1 ], - 1));

            //logger.debug("--T--> @DealHandlerCommon.updateForMIanRows:: primaryId=" + primaryid + ", copyId=" + copyid);
            DealEntity de = getEntity(entityName, primaryid, copyid, dcm);

            //logger.debug("--T--> @DealHandlerCommon.updateForMIanRows:: entity obj="+ de);
            parsePageValuesToEntity(propList, de, - 1);
            de.ejbStore();
        }
        catch(Exception ex)
        {
            logger.warning("Exception @updateForMainRows: Entity name = <" + entityName + ">, Page key fields = <id=" + keyValues [ 0 ] + ", cid=" + keyValues [ 1 ] + "> msg = " + ex + " - ignored");
        }
        catch(Throwable th)
        {
            logger.warning("Exception @updateForMainRows: Entity name = <" + entityName + ">, Page key fields = <id=" + keyValues [ 0 ] + ", cid=" + keyValues [ 1 ] + "> msg = " + th + " - ignored");
        }

    }


    /**
     * <p>updateForRepeatedRows</p>
     * <p>to save data: Original method before MCM. It used to include 
     * all process that is included in the next method now</p>
     * 
     * @param Hashtable propList
     * @param String[] keyValues
     * @param String entityname
     * @param CalcMonitor dcm
     * 
     * @version 1.19 MCM Team (XS 2.60)
     */
    // Assume that primary Key Value is specified at index 0 of the keyValues Array
    // and Copy Id Value is specified at index 1 of the keyValues Array
    public void updateForRepeatedRows(Hashtable propList, String[] keyValues, String entityName, CalcMonitor dcm)
    throws Exception {
        updateForRepeatedRows(propList, keyValues,  entityName, dcm, getCurrNDPage());
    }

    /**
     * <p>updateForRepeatedRows</p>
     * <p>to save data: new method with parent ViewBean</p>
     * 
     * @param Hashtable propList
     * @param String[] keyValues
     * @param String entityname
     * @param CalcMonitor dcm
     * @param ViewBean parent ( currNDPage or Pagelet )
     * 
     * @version 1.19 MCM Team (XS 2.60)
     */
    public void updateForRepeatedRows(Hashtable propList, String[] keyValues, 
            String entityName, CalcMonitor dcm, ViewBean parent)
    throws Exception
    {

        //get Primary key Value
        //get Total Row Index Value for this Primary Key Value
        //if it's equal or more than zero then loop thru until end of total
        //no of rows and call
        //parsePageValuesToEntity with rowindex Valus as loop index Value
        //if it's single line then call
        //parsePageValuesToEntity with rowindex Value as -1
        try
        {
            // MCM Team. passing parent viewBean i.e. getCurrNDPage or pagelet
            int noOfRows = getTotalNoOfRowsForThisPageVar(keyValues [ 0 ], parent);

            //logger.debug("DHC@updateForRepeatedRows: " + keyValues[0]);
            //logger.debug("DHC@updateForRepeatedRows@noOfRows: " + noOfRows);

            if (noOfRows < 0)
            {
                //logger.debug("@DealHandlerCommon.updateForRepeatedRows: Entity name = <" + entityName + ">, Page key fields = <id=" + keyValues[0] + ", cid=" + keyValues[1] + ">, no rows  - ignored");
                return;
            }
            int primaryid = 0;
            int copyid = 0;
            for(int i = 0; i < noOfRows; i ++)
            {

                try
                {
                    // MCM Team. passing parent viewBean i.e. getCurrNDPage or pagelet STARTS
                    primaryid =(int) Double.parseDouble(readFromPage(keyValues [ 0 ], i, parent));
                    copyid =(int) Double.parseDouble(readFromPage(keyValues [ 1 ], i, parent));
                    DealEntity de = getEntity(entityName, primaryid, copyid, dcm);
                    parsePageValuesToEntity(propList, de, i, parent);
                    // MCM Team. passing parent viewBean i.e. getCurrNDPage or pagelet ENDS

                    //logger.debug("@DHC.updateForRepeatedRows:counter: " + i);
                    //logger.debug("@DHC.updateForRepeatedRows:primaryId: " + primaryid);

                    de.ejbStore();
                }
                catch(Exception ee)
                {
                    logger.warning("Exception @DealHandlerCommon.updateForRepeatedRows: Entity name = " 
                        + entityName + ", Keys = <primaryId=(" + keyValues[0] + "=" + primaryid 
                        + "), cid=(" + keyValues[1] + "=" + copyid + ")>, exception=<" + ee + "> - ignored");
                }

            }
        }
        catch(Exception ex)
        {
            logger.warning("Exception @DealHandlerCommon.updateForRepeatedRows: Entity name = " + entityName 
                + ", Keys = <primaryId=(" + keyValues[0] + "), cid=(" + keyValues[1] 
                + ")>, exception=<" + ex + "> - ignored");
        }

    }

    /**
     * <p>getTotalNoOfRowsForThisPageVar</p>
     * <p>to save data: Original method before MCM. It used to include 
     * all process that is included in the next method now</p>
     * 
     * @param String pageVar
     * @return int no of rows of tile
     * 
     * @version 1.19 MCM Team (XS 2.60)
     */
    public int getTotalNoOfRowsForThisPageVar(String pageVar)
    throws Exception {

        return getTotalNoOfRowsForThisPageVar(pageVar, getCurrNDPage());
    }

    /**
     * <p>getTotalNoOfRowsForThisPageVar</p>
     * <p>new method with parent ViewBean</p>
     * 
     * @param String pageVar
     * @param ViewBean parent (CurrNDPage or pagelet)
     * @return int no of rows of tile

     * @version 1.19 MCM Team (XS 2.60)
     */
    public int getTotalNoOfRowsForThisPageVar(String pageVar, ViewBean parent)
    throws Exception
    {
        try
        {
            if ((pageVar == null) ||(pageVar.equals(""))) throw new Exception("Invalid Page Variable Requested");

            int tileCounter = 0;

            String [] repFields = new String [2];
            StringTokenizer rep = new StringTokenizer(pageVar, "/");

            repFields[0] = rep.nextToken(); // repeatable Name
            repFields[1] = rep.nextToken(); // repeatable FieldName

            //logger.debug("DHC@getTotalNoOfRowsForThisPageVar::repFields[0]: " + repFields[0]);
            //logger.debug("DHC@getTotalNoOfRowsForThisPageVar::repFields[1]: " + repFields[1]);

            if (repFields[0] == null || repFields[0].trim().equals("") || repFields[1] == null || repFields[1].trim().equals(""))
            {
                return -1;
            }
            //MCM Team - XS 2.60 use parent viewBean, i.e. Current page or pagelet to get tile 


            TiledViewBase tiledView = (TiledViewBase) parent.getChild(repFields[0]);

            //--> Should try to avoid to use nextTile call. Because this will cause the NextTile event
            //--> executed multiple time and will lost some input value as well
            //--> Changed by BILLY 06Aug2002
            //tiledView.resetTileIndex();
            //while(tiledView.nextTile())
//          logger.debug("DHC@getTotalNoOfRowsForThisPageVar::tiledView.getNumTiles()=" + tiledView.getNumTiles());
            while(tileCounter < tiledView.getNumTiles())
            {
                tiledView.setTileIndex(tileCounter);
                tileCounter++;
                //logger.debug("DHC@getTotalNoOfRowsForThisPageVar::tileCounter: " + tileCounter + " The FieldValue = " + tiledView.getDisplayFieldValue(repFields[1]));

            }
            //--> The above is for debug purpose only.  Will be removed later.
            //==============================================================


            /*
            if (filedValueList == null)            {
                throw new Exception("Page Display Field no found (no such)");
            }
            if (filedValueList instanceof Vector)            {
                return((Vector) filedValueList).size();
            }      */
            return tileCounter;
        }
        catch(Exception ex)
        {
            logger.error("Exception @getTotalNoOfRowsForThisPageVar: DspFldName = <" + pageVar + ">");
            logger.error(ex);
        }

        return - 1;

    }

    /* PageCondition7 defines whether rate locked or not. In the locked case
     ** an appropriate field Values must NOT be set to entity. This is view only
     ** mode which should be implemented for any view only mode through the project
     ** during the release 2 phase.
     ** 1. PageCondition7 is false (regular case).
     **loop thru fieldlist contents
     **	get entity properties which is key value
     **	get Page Variable Contents which is attached value for entity properties
     **	get Page Variable Value from getThisDisplayFieldValue()
     **	setEntity Field Value
     **end loop
     ** 2. PageCondition7 is true (rate locked case).
     **loop thru fieldlist contents
     **	get entity properties which is key value
     **	get Page Variable Contents which is attached value for entity properties
     **  get Page Variable Contents which should be skipped for propagation to the entity
     **	get Page Variable Value from getThisDisplayFieldValue() for this subset
     **	setEntity Field Value for this subset.
     **end loop
       //--BMO_MI_CR--start//
     ** In addition to PageCondition7 logic new PageCondition9 defines wherther MI
     ** process is handled inside BXP or outside.
     ** 1. PageCondition9 is false (regular client, not allow the db update from the screen for these fields)
     ** (isMIProcessOutsideBXP-->false)
     **loop thru fieldlist contents
     **	get entity properties which is key value
     **	get Page Variable Contents which is attached value for entity properties
     **  get Page Variable Contents which should be skipped for propagation to the entity
     **	get Page Variable Value from getThisDisplayFieldValue() for this subset
     **	setEntity Field Value for this subset.
     **end loop
     ** 2. PageCondition9 is true (isMIProcessOutsideBXP-->true) (allow db update)
     **loop thru fieldlist contents
     **	get entity properties which is key value
     **	get Page Variable Contents which is attached value for entity properties
     **	get Page Variable Value from getThisDisplayFieldValue()
     **	setEntity Field Value
     **end loop
       //--BMO_MI_CR--end//
       //--Ticket#1006--start--//
     ** The logic above totally simplified since the number of page conditions combinations
     ** is growing. The HashSet containing the elements which should be skipped either
     ** from DE/DM or UW page is created in the appropriate handler (taking into account
     ** logic driven by various conditions).
     ** 1. If no elements should be skipped, just follow the very original logic (this
     ** is only sub-pages such ApplicantEntry and PropertyEntry now since DM/UW pages
     ** eventually have skipped elements always:
     **loop thru fieldlist contents
     **       get entity properties which is key value
     **	get Page Variable Contents which is attached value for entity properties
     **	get Page Variable Value from getThisDisplayFieldValue()
     **	setEntity Field Value
     **end loop
     ** 2. After this (for DE/DM/UW most probably):
     **loop thru fieldlist contents
     **  a). get entity properties which is key value
     **	get Page Variable Contents which is attached value for entity properties
     **  b). get Page Variable Contents which should be skipped for propagation to the entity
     **	get Page Variable Value from getThisDisplayFieldValue() for this subset
     **	setEntity Field Value for this subset.
     **end loop
       //--Ticket#1006--end--//
     * 
     * MCM Team (July 2, 2008) oritinal method before MCM XS 2.60 
     * 
     **/
    public DealEntity parsePageValuesToEntity(Hashtable fieldList, DealEntity entity, int rowindex)
    {
        return parsePageValuesToEntity(fieldList, entity, rowindex, getCurrNDPage());
    }

    /**
     * <p>parsePageValuesToEntity</p>
     * <p> new method that includes parent ViewBean to handle pagelet</p>
     * @param fieldList HashTable
     * @param entity DealEntity
     * @param rowindex int
     * @param parent ViewBean  currentPage ViewBean or pagelet
     * @return DealEntity
     * 
     */
    public DealEntity parsePageValuesToEntity(Hashtable fieldList,
    		DealEntity entity, int rowindex, ViewBean parent) {

    	String entityProp = null;
    	String pageVarValue = null;
    	String[][] pageVarContents = null;

    	PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    	HashSet<String> pageExclVars = pg.getPageExcludePageVars();

    	DealUIParser dealUIParser = new DealUIParser(srk);
    	for (Enumeration enu = fieldList.keys(); enu.hasMoreElements();) {
    		try {
    			entityProp = (String) enu.nextElement();
    			pageVarContents = (String[][]) fieldList.get(entityProp);

    			// skip the filed
    			if (pageExclVars != null
    					&& pageExclVars.contains(pageVarContents[1][0])) continue;

    			pageVarValue = getThisDisplayFieldValue(
    					pageVarContents, rowindex, parent, dealUIParser);

    			entity.setField(entityProp, pageVarValue);

    		} catch (Exception ex) {
    			logger.error("Exception @parsePageValuesToEntity: Entity = <"
    					+ entity + ">, Mapping Property-Page = <eProperty="
    					+ entityProp + ", page[dspfld=" + pageVarValue
    					+ ", use=" + pageVarContents + "]> - ignored" + ex);
    			logger.error(ex);
    		}
    	}
    	return entity;
    }

    /*
     **get General Method Name from Index 0 of array Holder
     **init paramarray with size equal to page var contents.size() -1
     ** //new array size is less than 1 bcose first index is used to hold the method name
     **init util class instance which contains all parsing methods
     **
     **loop thru array holder from row index 1
     **	get page var name from [i,0] where i is current index
     **	get Value for this Page Var from the current Page
     **	get individual parse method for this Page Var from [i,1] where i is current index
     **
     **	if there's method name then
     **		call individual parse method with parameter of page var value
     **
     ** 	store the returned ie parsed value to paramarray
     **end loop
     **
     **if there's general method name then
     **	call general method pass paramarray for final parsing
     **
     **return final String Value
     *
     * MCM Team: Original method before MCM 2.60. i.e.e no VeiwBean in param
     * 
     * @param pageVarContents String[][]
     * @param rowindex int
     * @return String field Value in page
     **/
    protected String getThisDisplayFieldValue(String[][] pageVarContents, int rowindex)
    throws Exception
    {
        return getThisDisplayFieldValue(pageVarContents, rowindex, getCurrNDPage(), new DealUIParser(srk));
    }

    /**        
     * <p>getThisDisplayFieldValue<p>
     * <p> get field value from passing ViewBean</p>
     * 
     * @param pageVarContents String[][]
     * @param rowindex int
     * @param ViewBean parent
     * @return String field Value in page
     * @version 1.19 MCM Team: XS_2.60. added param ViewBean i.e. current page view or pagelet
     **/
    protected String getThisDisplayFieldValue(String[][] pageVarContents,
    		int rowindex, ViewBean parent, DealUIParser dealUIParser)
    throws Exception {

    	String pageVarName = "";
    	String pageVarValue = "";
    	String indParseMethodName = "";
    	try
    	{
    		String genMethodName = pageVarContents [ 0 ] [ 0 ];
    		String [ ] paramarray = new String [ pageVarContents.length - 1 ];
    		//String tmpvar = "";
    		for(int i = 1; i < pageVarContents.length; i ++)
    		{
    			pageVarName = pageVarContents [ i ] [ 0 ];
    			indParseMethodName = pageVarContents [ i ] [ 1 ];
    			pageVarValue = readFromPage(pageVarName, rowindex, parent);
    			//logger.error("DHC@getThisDisplayFieldValue::PageVarValue: " + pageVarValue);

    			if (isValidMethodName(indParseMethodName))
    			{
    				//logger.error("DHC@getThisDisplayFieldValue::IndividMethodName: " + indParseMethodName);
    				pageVarValue = executeThisIndividMethod(dealUIParser,
    						indParseMethodName,
    						pageVarValue);
    			}
    			paramarray [ i - 1 ] = pageVarValue;
    		}
    		if (isValidMethodName(genMethodName))
    		{
    			//logger.error("DHC@getThisDisplayFieldValue::GenMethodName: " + genMethodName);
    			pageVarValue = executeThisMethod(dealUIParser, genMethodName, paramarray);
    		}
    		return pageVarValue;
    	}
    	catch(Exception ex)
    	{
    		logger.error("Exception occurred DHC@getThisDisplayFieldValue: " 
    				+"pageVarName="+pageVarName+";pageVarValue="+pageVarValue+";indParseMethodName="+indParseMethodName+";"
    				+ ex.toString());
    		throw new Exception("Exception occurred DHC@getThisDisplayFieldValue");
    	}

    }

    //--Ticket#575--16Sep2004--start--//
  // Very important addition to the generic framework method of propogation of data
  // from DE/DM/UW screens. The indivial method (defined as usual in DealUIParser class)
  // may be called from the dbprop file using the exclamation sign, e.g.:
  // interestTypeId=cbUWProduct!getInterestTypeIdFromServlet
  // Note, that value propogated to db is indirectly obtained from the field based
  // on its value.
    protected String executeThisIndividMethod(Object object, String methodname, String paramname)
    {

        logger.debug("DHC@executeIndividualMethod single Param Name: method name = " + methodname + ", param name = " + paramname
      		  +";object="+object);
        String retvalue = "";

        try
        {
            Class classinstant = object.getClass();
            //logger.debug("DHC@executeIndividualMethod::Class: " + classinstant);

            Class [ ] paramtype = new Class [ 1 ];
            paramtype [ 0 ] = paramname.getClass();
            //logger.debug("DHC@executeIndividualMethod::ParamType: " + paramtype[ 0 ]);

            Method method = classinstant.getMethod(methodname, paramtype);
            Object [ ] objpm = new Object [ 1 ];

            //logger.debug("DHC@executeIndividualMethod::MetodReturnType: " + (method.getReturnType()).toString());
            //logger.debug("DHC@executeIndividualMethod::MetodParameterType: " + (method.getParameterTypes()[0]).toString());
            //logger.debug("DHC@executeIndividualMethod::MetodParameterLength: " + (method.getParameterTypes()).length);

            objpm [ 0 ] = paramname;
            //logger.debug("DHC@executeIndividualMethod::paramName: " + paramname);
            //logger.debug("DHC@executeIndividualMethod::methodName: " + methodname);

            retvalue =(String) method.invoke(object, objpm);

        }
        catch(NoSuchMethodException e)
        {
            logger.error("Exception occured DHC@executeIndividualMethod: no such method = <" + methodname + ">, exception=" + e.getMessage());
        }
        catch(Exception ex)
        {
            logger.error("Exception occured DHC@executeIndividualMethod");
            logger.error(ex);
        }

        return retvalue;
    }
    //--Ticket#575--16Sep2004--end--//

    /**
     *
     *
     */
    protected String executeThisMethod(Object object, String methodname, String[] params)
    {
        String retvalue = "";

        try
        {
            Class classinstant = object.getClass();
            Class [ ] paramtype = new Class [ 1 ];
            paramtype [ 0 ] = params.getClass();
            Method method = classinstant.getMethod(methodname, paramtype);
            Object [ ] objpm = new Object [ 1 ];
            objpm [ 0 ] = params;

            //logger.debug("DHC@executeGenMethod::Params[0]: " + params[ 0 ]);
            //logger.debug("DHC@executeGenMethod::MetodName: " + method.getName());
            //logger.debug("DHC@executeGen::MetodReturnType: " + (method.getReturnType()).toString());
            //logger.debug("DHC@executeGen::MetodParameterType: " + (method.getParameterTypes()[0]).toString());
            //logger.debug("DHC@executeGen::MetodParameterLength: " + (method.getParameterTypes()).length);

            retvalue =(String) method.invoke(object, objpm);

                        //logger.debug("DHC@executeGen::MetodParameterLength:RetValue: " + retvalue);
        }
        catch(NoSuchMethodException e)
        {
            logger.error("Exception occured @executeThisMethod: no such method = <" + methodname + ">, exception=" + e.getMessage());
        }
        catch(Exception ex)
        {
            logger.error("Exception occured @executeThisMethod:"+object+";methodname="+methodname+";params="+params);	//#DG826
            logger.error(ex);
        }

        return retvalue;

    }


    /**
     *
     *
     */
    private boolean isValidMethodName(String methodname)
    {
        if ((methodname != null) &&(methodname.trim().length() > 1)) return true;

        return false;

    }
    private static Hashtable updatePropList=new Hashtable();


    public Hashtable getPropertiesForThisPage(String pageFileName)
    {
        Hashtable retlist =(Hashtable) updatePropList.get(pageFileName);

        if (retlist == null)
        {
            retlist = loadThisProperties(pageFileName);
            updatePropList.put(pageFileName, retlist);
        }

        return retlist;

    }


    private Hashtable loadThisProperties(String filename)
    {
        String simpleClassName = this.getClass().getSimpleName();
        logger.info("DBProp className: " + simpleClassName);

        URL lsLocation = this.getClass()
        .getResource(simpleClassName + ".class");

        String dir = lsLocation.toString();

        int index = dir.indexOf("mosApp/MosSystem");

//      String dbPropertiesPath = dir.substring("file:/".length(), index) + "com/filogix/express/web/dbprops/";      
        String dbPropertiesPath = dir.substring(0, index) + "com/filogix/express/web/dbprops/";      

        return loadThisProperties(dbPropertiesPath, filename);

    }


    /*
     **Check for List holder whether Property file has been loaded already;
     **if  loaded then
     **	return the Hashtable list which has been loaded already
     **if not
        load the property file for the given name // which contains entity property as key value and Page variable with defined tag and method needed to be operated on the Page values

     **loop thru property contents
        get key value 			//entity property name
        get the Value for this key 		//page variable + method need to be use to parse the page values seperated with defined tags
        send the value to parseParameterToArray() and get String array Holder
        put in to Hashtable List (key Value and String Array Holder contains parsed Value)
     **end loop

     **/
    private Hashtable loadThisProperties(String pathname, String propname)
    {
        pathname = pathname.trim();

        Properties prop = new Properties();

        if (!(pathname.endsWith("\\") || pathname.endsWith("/")))
        {
            String sufix = "\\";
            if (pathname.indexOf('/') != - 1) sufix = "/";
            pathname = pathname + sufix;
        }

        //String filedesc = pathname + propname;

        try
        {
//          FileInputStream fi = new FileInputStream(pathname + propname);
            FileInputStream fi = new FileInputStream( new File( new URI(pathname + propname)));
            prop.load(fi);
        }
        catch(Exception ex)
        {
            logger.error("Exception @loadThisProperties, loading DB properties file = " +(pathname + propname));
            logger.error(ex);
        }

        String entityProp = "";

        String pageVarValue = "";

        String pageVar = "";

        Hashtable ret = new Hashtable();

        for(Enumeration enumeration = prop.keys();
        enumeration.hasMoreElements();
        )
        {
            try
            {
                entityProp =(String) enumeration.nextElement();
                pageVar =(String) prop.get(entityProp);
                String [ ] [ ] varValues = parseParameterToArray(pageVar);
                ret.put(entityProp, varValues);
            }
            catch(Exception ex)
            {
                continue;
            }
        }

        return ret;

    }


    /* General Get Methods For Cotainer Deal Entities
     */
    public Deal getDeal(int dealId, int copyId, CalcMonitor dcm)
    throws Exception
    {
        Deal deal = null;

        try
        {
            deal = DBA.getDeal(getSessionResourceKit(), dealId, copyId, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Error @getDeal: dealId = " + dealId + ", copyId = " + copyId);
            logger.error(ex);
            throw new Exception("Locate deal entity exception");
        }

        return deal;

    }


    /****
     ** Getter(s) for Borrower Options Are
     ** for currently selected Property, gets property Id from session state
     ** using hidden page Variable
     ** Passing Borrower Id, and Copy Id
     *****/
    public Borrower getSelectedBorrower(CalcMonitor dcm)
    {
        Borrower borrower = null;

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        try
        {
            ////int borrowerid = theSession().getPageCriteriaId1();
            ////int copyid = theSession.getCurrentPage().getPageDealCID();
            int borrowerid = currPg.getPageCriteriaId1();
            int copyid = currPg.getPageDealCID();

            borrower = getBorrower(borrowerid, copyid, dcm);
        }
        catch(Exception e)
        {
            logger.error(e);
            borrower = null;
        }

        return borrower;

    }


    /**
     *
     *
     */
    public Borrower getBorrower(String borrowerIdLabel, CalcMonitor dcm)
    {
        Borrower borrower = null;

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        try
        {
            ////int copyid = theSession.getCurrentPage().getPageDealCID();
            int borrowerid = getIdValueFromPage(borrowerIdLabel);
            int copyid = currPg.getPageDealCID();

            borrower = getBorrower(borrowerid, copyid, dcm);
        }
        catch(Exception e)
        {
            logger.error(e);
            borrower = null;
        }

        return borrower;

    }


    /**
     *
     *
     */
    public Borrower getBorrower(int borrowerId, int copyId, CalcMonitor dcm)
    throws Exception
    {
        Borrower borrower = null;

        try
        {
            borrower =(Borrower) getEntity("Borrower", borrowerId, copyId, dcm);
        }
        catch(Exception ex)
        {
            borrower = null;
        }

        return borrower;

    }


    /****
     ** Getter(s) for Property Options Are for currently selected Property
     ** which gets property Id from session state
     ** using hidden page Variable
     ** Passing property Id, and Copy Id
     *****/
    public Property getSelectedProperty(CalcMonitor dcm)
    {
        Property property = null;

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        try
        {
            ////int propertyid = theSession.getCurrentPage().getPageCriteriaId1();
            ////int copyid = theSession.getCurrentPage().getPageDealCID();
            int propertyid = currPg.getPageCriteriaId1();
            int copyid = currPg.getPageDealCID();

            property = getProperty(propertyid, copyid, dcm);
        }
        catch(Exception e)
        {
            logger.error(e);
            property = null;
        }

        return property;

    }


    /**
     *
     *
     */
    public Property getProperty(String propertyIdLabel, CalcMonitor dcm)
    {
        Property property = null;

        PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

        try
        {
            int propertyid = getIdValueFromPage(propertyIdLabel);
            ////int copyid = theSession.getCurrentPage().getPageDealCID();
            int copyid = currPg.getPageDealCID();

            property = getProperty(propertyid, copyid, dcm);
        }
        catch(Exception e)
        {
            logger.error(e);
            property = null;
        }

        return property;

    }


    /**
     *
     *
     */
    public Property getProperty(int propertyid, int copyid, CalcMonitor dcm)
    throws Exception
    {
        Property property = null;

        try
        {
            property =(Property) getEntity("Property", propertyid, copyid, dcm);
        }
        catch(Exception ex)
        {
            property = null;
        }

        return property;

    }


    /*
     ** get General method Name if any name attached
     ** seperated with '@'
     ** if found then then seperate the Param Name and general Method Name
     ** add general method name in to return Array holder in index 0
     ** tokenize to separate the multiple display variable if any
     ** get individual parse method name for the single display variable if any
     ** assign to the return array holder first column to page Varaible Name and
     ** individual parse method name method name is assigned to column 2
     **/
    private String[][] parseParameterToArray(String paramName)
    {
        String genMethdName = "";

        int tmpindx = paramName.lastIndexOf("@");

        if (tmpindx > 1)
        {
            genMethdName = paramName.substring(tmpindx + 1);
            paramName = paramName.substring(0, tmpindx);
        }

        StringTokenizer st = new StringTokenizer(paramName, "+");

        int ind = st.countTokens();

        String [ ] [ ] pms = new String [ ind + 1 ] [ 2 ];

        pms [ 0 ] [ 0 ] = genMethdName;

        pms [ 0 ] [ 1 ] = "";

        if (ind > 1)
        {
            int j = 1;
            String [ ] tt = null;
            while(st.hasMoreElements())
            {
                //if (j>2)
                //  break;
                tt = getIndividualParseMethodName((String) st.nextElement());
                pms [ j ] [ 0 ] = tt [ 0 ];
                pms [ j ] [ 1 ] = tt [ 1 ];
                j ++;
            }
        }
        else
        {
            String [ ] f = getIndividualParseMethodName(paramName);
            pms [ 1 ] [ 0 ] = f [ 0 ];
            pms [ 1 ] [ 1 ] = f [ 1 ];
        }

        return pms;

    }


    /*
     ** individual parse method for page variable seperated by '!'
     ** tokenize the line with delim '!'
     ** only one method is allowed so check for no of iterations
     **/
    private String[] getIndividualParseMethodName(String aLine)
    {
        StringTokenizer st = new StringTokenizer(aLine, "!");

        String [ ] pms =
        {
                "", ""		}
        ;

        int i = 0;

        String tm = "";

        while(st.hasMoreElements())
        {
            if (i > 1) break;
            pms [ i ] =(String) st.nextElement();
            i ++;
        }

        return pms;

    }


    /**
     * It is important to note that this implementation of DisplayField assumes that a display field is
     * ALWAYS bound to a model.
     * Due to this fact the following method should be redesigned in accordancy with the JATO framework.
     * Modified currently for the complilation purposes only.
     */
    public String displayPhoneExtension(DisplayField theField)
    {
        //String val = theField.getHtmlText();
        String val = theField.stringValue();

        if (val != null && val.trim().length() != 0)
        {
            //theField.setHtmlText("x" + val);
            theField.setValue("x" + val);

            //return CSpPage.PROCEED;
            return val;
        }

        //return CSpPage.SKIP;
        return "";
    }




    protected Vector getIntersection(Vector exportInxs, Vector deleteInxs)
    {
        Vector retInxs = new Vector();

        for (int i = 0; i < exportInxs.size(); i++)
        {
            for (int j = 0; j < deleteInxs.size(); j++)
        {
                                // Auto-conversion is not done proper. In any case it shold be (at least temporarily)
                                // returned to the original expression to escape the overhead of the extra primitive
                                // type conversion. It is an expensive operation.
                                //if (((Integer)com.iplanet.jato.util.TypeConverter.asInt(exportInxs.elementAt(i))) ==((Integer)com.iplanet.jato.util.TypeConverter.asInt(deleteInxs.elementAt(j))))
                if( ((Integer)exportInxs.elementAt(i)).intValue() == ((Integer)deleteInxs.elementAt(j)).intValue())
                {
                    retInxs.add((Integer)(exportInxs.elementAt(i)));
                }
            }
        }

        return retInxs;

    }


    protected Vector getAdjunct(Vector exportInxs, Vector interscectionInxs)
    {
   	 //#DG826 there might be a bug here - the same input vector is changed and send bak!
        for (int i = 0; i < exportInxs.size(); i++)
        {
            for (int j = 0; j < interscectionInxs.size(); j++)
        {
                                // Auto-conversion is not done proper. In any case it shold be (at least temporarily)
                                // returned to the original expression to escape the overhead of the extra primitive
                                // type conversion. It is an expensive operation.
                                //if (((Integer)com.iplanet.jato.util.TypeConverter.asInt(exportInxs.elementAt(i))) ==((Integer)com.iplanet.jato.util.TypeConverter.asInt(interscectionInxs.elementAt(j))))
                if( ((Integer)exportInxs.elementAt(i)).intValue() == ((Integer)interscectionInxs.elementAt(j)).intValue())
                {
                    exportInxs.remove((Integer)(exportInxs.elementAt(i)));
                }
            }
        }

        return exportInxs;

    }

    //
    // Basically do nothing - just a refresh
    // Added flag to force to trigger recal with TotalNetLoan -- By BILLY 09May2002
    public void handleRecalculate(boolean forceCal)
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        if (disableEditButton(pg) == true)
            return;

        if(forceCal == true)
        {
            SessionResourceKit srk  = getSessionResourceKit();
            CalcMonitor dcm         = CalcMonitor.getMonitor(srk);
            try
            {
                Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), dcm);
                // Force to trigger calc by TotalLoanAmount
                   deal.forceChange("netLoanAmount");
                // FXP32254 - 30 Sept 2011
                //dcm.inputEntity(deal);  
                dcm.inputCreatedEntity(deal);
                
                Iterator <Borrower> borrowerIter = deal.getBorrowers().iterator();
                while ( borrowerIter.hasNext() ){
                	dcm.inputCreatedEntity(borrowerIter.next());
                }
                
                Iterator <Property> propertyIter = deal.getProperties().iterator();
                while ( propertyIter.hasNext() ){
                	dcm.inputCreatedEntity(propertyIter.next());
                }
                
                doCalculation(dcm);
            }
            catch (Exception ex)
            {
            	logger.error("Exception @handleRecalculate :: " + ex);	//#DG758
            }
        }
        //  default behaviour .. do nothing (its all in the regeneration of page)
    }

    //--Sync--//
    //--> This was missed in the previous sync.  Fixed by Billy 28Feb2003
    // Added Dialog message for user to request for Commitment resend -- Changed by Billy 12June2002
    // New method to handle commitment resending
    public void handleCommitmentResend()
    {
        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();

            int dealid = theSessionState.getCurrentPage().getPageDealId();
            int copyid = theSessionState.getCurrentPage().getPageDealCID();

            // get deal
            CalcMonitor dcm = CalcMonitor.getMonitor(srk);
            Deal deal = DBA.getDeal(srk, dealid, copyid, dcm);

            // Reset commitmentProduced flag to force to send
            deal.setCommitmentProduced("N");
            CCM ccm = new CCM(srk);
            ccm.requestCommitmentDoc(srk, deal);
            ccm.requestElectronicResponseToBroker(srk, deal);

            deal.ejbStore();

            srk.commitTransaction();
        }
        catch (Exception ex)
        {
            srk.cleanTransaction();

            logger.error("Exception @handleCommitmentResend");
            logger.error(ex);
            setStandardFailMessage();
        }
    }
    //--DisableProductRateTicket#570--10Aug2004--start--//
    protected String getPageEditableFlag(PageEntry pg)
    {
        String ret = "Y"; //default
        if (pg.isEditable() == true)
            ret = "Y";
        else
            ret = "N";

        return ret;
    }

    protected int getDealRateStatusId(int dealPricingProfileId)
    {
        int statusid = 0; //default rate status is enable.

        try
        {
            JdbcExecutor jExec = srk.getJdbcExecutor();

            String sql =
                "SELECT P.PRICINGSTATUSID" +
                " FROM  PRICINGPROFILE P, PRICINGRATEINVENTORY I" +
                " WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID AND" +
                " I.PRICINGRATEINVENTORYID = " + dealPricingProfileId;

            int key = jExec.execute(sql);
            while(jExec.next(key))
            {
                statusid = jExec.getInt(key, 1);
                break;
            }
            jExec.closeData(key);

        }
        catch(Exception e)
        {
       logger.error("UWH@getDealRateStatusId Exception at getting statusId:"+e);	//#DG758
        }

        logger.debug("UWH@getDealRateStatusId: " + statusid);

        return statusid;
    }

    protected java.util.Date getDealRateDisDate(int dealPricingProfileId)
    {
        java.util.Date disRateDate = null; //default is not disabled rate date.

        try
        {
            JdbcExecutor jExec = srk.getJdbcExecutor();

            String sql =
                "SELECT P.RATEDISABLEDDATE" +
                " FROM  PRICINGPROFILE P, PRICINGRATEINVENTORY I" +
                " WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID AND" +
                " I.PRICINGRATEINVENTORYID = " + dealPricingProfileId;

            int key = jExec.execute(sql);
            while(jExec.next(key))
            {
                disRateDate = jExec.getDate(key, 1);
                break;
            }
            jExec.closeData(key);

        }
        catch(Exception e)
        {
       logger.error("UWH@getDealRateDisDate Exception at getting statusId:"+e);	//#DG758
        }

        logger.debug("UWH@getDealRateDisDate: " + disRateDate.toString());

        return disRateDate;

    }
    //--DisableProductRateTicket#570--10Aug2004--end--//

    /**
     * Description: Handles the submit from Component Details Page   * 
     * @author: MCM Impl Team.
     * @version 1.12 2008-6-17
     */
    public void handleDECSubmit()
    {
        handleDECSubmit(false);

    }

    /**
     * Description: Handles the submit from Component Details Page   * 
     * @author: MCM Impl Team.
     * @version 1.2 2008-6-17
     * @version 1.13 2008-7-3 fixed not to saved
     * @version 1.14 2008-7-11 mofidifed BR (xs 2.39)
     * @version 1.15 2008-7-16 mofidifed error popup condition base on artf743632(xs 2.39)
     */
    public void handleDECSubmit(boolean confirmed)
    {
        logger.debug("@DealHandlerCommon.handleDEPSubmit");

        PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
            Deal deal = DBA.getDeal(srk, pe.getPageDealId(), pe.getPageDealCID(), dcm);


            // submit transaction ...
            srk.beginTransaction();
            noAuditIfIncomplete(pe, null);

            // always adopt transactional copy on submit
            standardAdoptTxCopy(pe, true);

            BusinessRuleExecutor brExec = new BusinessRuleExecutor();

            /***************MCM Impl team changes starts - FXP22741*******************/
            // PassiveMessage pm = validateComponent(pe, srk);
            PassiveMessage pm = validateComponents(pe, srk);
            /***************MCM Impl team changes ends - FXP22741*********************/
            
            boolean criticalDataProblems = false;
            boolean nonCriticalDataProblems = false;
            boolean stayOnPage = false;

            if (criticalDataProblems == true)
            {
                setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL", theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMDIALOG);
            }
            else
            {
                if (stayOnPage) setActiveMessageToAlert(BXResources.getSysMsg("DEAL_ENTRY_MOD_SAVED_WITH_WARNINGS", theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMDIALOG);
            }
            // MCM Team XS 2.47 based on response artf743632
            if (pm != null && pm.getNumMessages() > 0)
            {
                pm.setGenerate(true);
                ////getTheSession().setPasMessage(pm);
                theSessionState.setPasMessage(pm);
            }
            doCalculation(dcm);
            if (stayOnPage)
            {
                // navigate to self - to ensure creation of tx copy when page regenerated
                getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pe));
            }


            // trigger workflow:
            //
            // The only task known to generate in this case is "Review Deal Notes". A an optimization we
            // pre-check the generation criteria to avoid needless trigger
            //
            /*         Deal deal = DBA.getDeal(srk, pe.getPageDealId(), pe.getPageDealCID());
         String checkNotes = deal.getCheckNotesFlag() + "N";
         if (checkNotes.startsWith("Y")) workflowTrigger(2, srk, pe, null);
             */
            // standard navigation - back to parent
            navigateToNextPage();
            srk.commitTransaction();
            return;
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleDEPSubmit().");
            logger.error(ex);
        }


        // an exception has occured - ensure we navigate away from page since we can't guar
        // presence of a tx copy on regeneration
        navigateAwayFromFromPage(true);

    }

//    /**
//     * <p>validateComponent</p>
//     * <p>Description: validate all component on ComponentDetails screen</p>
//     * 
//     * @param PageEntry pe
//     * @param SessionResourceKit srk
//     * @return PassiveMessage
//     * 
//     * @author: MCM Impl Team.
//     * @version 1.14 2008-7-11 (XS 2.39/40) initial version
//     */
//    public PassiveMessage validateComponent(PageEntry pe,  
//            SessionResourceKit srk) throws Exception {
//
//        PassiveMessage pm = new PassiveMessage();
//        //XS_2.40 DMC MCM IMPL STARTS  
//        try
//        {
//            BusinessRuleExecutor brExec = new BusinessRuleExecutor();
//            brExec.setCurrentComponent(-1);
//            brExec.setCurrentBorrower(- 1);
//            brExec.setCurrentProperty(- 1);
//            PassiveMessage pmComp = brExec.BREValidator(theSessionState, pe, srk, null, "DMC-%", true);
//            if (pmComp.getNumMessages() > 0)
//            {
//                pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_COMPONENT_PROBLEM", 
//                        theSessionState.getLanguageId())
//                        + BXResources.getPickListDescription(-1, "COMPONENTTYPE",0, theSessionState.getLanguageId())
//                        + " ", PassiveMessage.INFO);
//                pm.addAllMessages(pmComp);
//            }
//        }
//        catch (Exception e)
//        {
//            _log.trace("Faild validate ComponentMortgage" );
//        }
//        //XS_2.40 DMC MCM IMPL ENDS
//        try {
//            String mtgPrefix = pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP + "/";
//
//            String [ ] compPropkeys = {  
//                    mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_HDCOMPONENTIDMTG, 
//                    mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_HDCOPYIDMTG 
//            };
//            validateComponent(compPropkeys, pm, pe);
//        } catch (Exception ex) {
//            _log.error("Exception @DHC:validateComponent:validateComponent Component in MTG ");
//            _log.error(ex);
//            _log.error(StringUtil.stack2string(ex));
//            throw ex;
//        }
//        //-- ComponentMortgage Tile section ends --//
//
//        //-- ComponentLOC Tile section starts --//
//        try {
//            String [ ] complockeys = {
//                    "RepeatedLOC/hdComponentIDLOC", "RepeatedLOC/hdCopyIDLOC"
//            };
//            validateComponent(complockeys, pm, pe);
//        } catch(Exception ex)  {
//            _log.error("Exception @DHC:validateComponent: Component in LOC ");
//            _log.error(ex);
//            _log.error(StringUtil.stack2string(ex));
//            throw ex;
//        }
//        //-- ComponentLOC Tile section ends --//
//
//        //-- ComponentCreditCard Tile section starts --//
//        try {
//            String ccPrefix = pgComponentDetailsViewBean.CHILD_TILEDCCCOMP + "/";
//
//            String [ ] compPropkeys = {  
//                    ccPrefix + pgComponentCreditCardTiledView.CHILD_HDCOMPONENTID, 
//                    ccPrefix + pgComponentCreditCardTiledView.CHILD_HDCOPYID 
//            };
//            validateComponent(compPropkeys, pm, pe);
//        } catch(Exception ex)  {
//            _log.error("Exception @DHC:validateComponent: Component in Credit Card ");
//            _log.error(ex);
//            _log.error(StringUtil.stack2string(ex));
//            throw ex;
//        } try {
//            String ccPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDOVERDRAFT + "/";
//            String [ ] compPropkeys = {  
//                    ccPrefix + pgComponentDetailsOverdraftTiledView.CHILD_HDCOMPONENTID, 
//                    ccPrefix + pgComponentDetailsOverdraftTiledView.CHILD_HDCOPYID 
//            };
//            validateComponent(compPropkeys, pm, pe);
//
//        } catch(Exception ex)  {
//            _log.error("Exception @DHC:validateComponent: Component in Overdraft ");
//            _log.error(ex);
//            _log.error(StringUtil.stack2string(ex));
//            throw ex;
//        }
//        /***************MCM Impl team changes ends - XS_2.38*********************/
//
//        try {
//            String [ ] complockeys = {
//                    "RepeatedLoan/hdComponentIDLoan", "RepeatedLoan/hdCopyIDLoan"
//            };
//            validateComponent(complockeys, pm, pe);
//        } catch(Exception ex)  {
//            _log.error("Exception @DHC:validateComponent: Component in Loan ");
//            _log.error(ex);
//            _log.error(StringUtil.stack2string(ex));
//            throw ex;
//        }
//        return pm;
//    }
//    /**
//     * <P> validateComponent </p>
//     * <p> validate a type of components. i.e. componenttype = Mtg, LOC,,,,
//     * MCM XS_2.39/40
//     *  
//     * @param keyValues String[]
//     * @param pm PassiveMessage
//     * @param pe PageEntry
//     */
//    public void validateComponent(String[] compPropkeys, PassiveMessage pm, PageEntry pe) {
//
//        int lim = 0;
//        Component[ ] comps = null;
//        String prefix;
//        //-- ComponentMortgage Tile section starts --//
//        try {
//            int lastCompTypeid = -1; //FXP22741, changed numbering 
//            int compSequence = 0; //FXP22741, changed numbering 
//            comps = getComponentsByPropKeys(compPropkeys);
//            lim = comps.length;
//            for(int i = 0; i < lim; ++ i) {
//                try {
//                    BusinessRuleExecutor brExec = new BusinessRuleExecutor();
//                    brExec.setCurrentComponent(comps[i].getComponentId());
//                    brExec.setCurrentBorrower(- 1);
//                    brExec.setCurrentProperty(- 1);
//                    //FXP22741, changed numbering 
//                    compSequence = (lastCompTypeid != comps[i].getComponentTypeId()) ? 1 : compSequence + 1;
//
//                    PassiveMessage pmComp = brExec.BREValidator(theSessionState, pe, srk, null, "DEC-%", true);
//                    if (pmComp.getNumMessages() > 0) {
//                        pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_COMPONENT_PROBLEM", 
//                                theSessionState.getLanguageId())
//                                + BXResources.getPickListDescription(-1, "COMPONENTTYPE", 
//                                        comps[i].getComponentTypeId(), theSessionState.getLanguageId())
//                                        + " " + compSequence, PassiveMessage.INFO);
//                        pm.addAllMessages(pmComp);
//                    }
//                } catch (Exception e ){
//                    logger.info(e.getMessage());
//                }
//                lastCompTypeid = comps[i].getComponentTypeId(); //FXP22741, changed numbering
//            }
//        } catch (Exception mtge) {
//            _log.trace("Faild validate ComponentMortgage" );
//        }
//    }

//    /**
//     * <P> getComponentsByPropKeys </p>
//     * <p> get array of component by key that is component field names on JSP 
//     * MCM XS_2.39/40
//     *  
//     * @param keyValues String[]
//     * @return Component[]
//     */
//    public Component[] getComponentsByPropKeys(String[] keyValues) {
//
//        try {
//            int noOfRows = getTotalNoOfRowsForThisPageVar(keyValues [ 0 ]);
//
//            if (noOfRows < 0) {
//                return null;
//            }
//
//            int primaryid = 0;
//            int copyid = 0;
//            Component[] comps = new Component[noOfRows];
//            for(int i = 0; i < noOfRows; i ++)
//            {
//                try
//                {
//                    primaryid =(int) Double.parseDouble(readFromPage(keyValues [ 0 ], i));
//                    copyid =(int) Double.parseDouble(readFromPage(keyValues [ 1 ], i));
//                    Component comp = (Component)getEntity("Component", primaryid, copyid, null);
//                    comps[i] = comp;
//                } catch (Exception ex) {
//                    _log.error("Exception @ComponentDetailsHandler:SaveData:save Component in MTG ");
//                    _log.error(ex);
//                    _log.error(StringUtil.stack2string(ex));
//                }
//            }
//            return comps;
//        } catch (Exception e) {
//            _log.error("Exception @ComponentDetailsHandler:SaveData:save Component in MTG ");
//            _log.error(e);
//            _log.error(StringUtil.stack2string(e));
//            return null;
//        }
//    }

    /**
     * <p>saveCompInfoCheckBox</p>
     * <p>save MI Allocate and Tax Allocate checkbox in component Information 
     * section on DealEntry/DealMod or UWorksheet screen.</p>
     * 
     * @param pageletName as a child
     * @param dcm CalcMonitor
     * 
     * @version 1.19 MCM Team (XS 2.60)
     */
    protected void saveCompInfoCheckBox(String pageletName, CalcMonitor dcm) {

        SessionResourceKit srk = getSessionResourceKit();

        //-- CompInfoMortgage Tile section starts --//
        try {
            String mtgPrefix = pgComponentInfoPageletViewBean.CHILD_TILEDMTGCOMP + "/";

            String [ ] compPropkeys = {  
                    mtgPrefix + pgCompInfoMTGTiledView.CHILD_HDCOMPONENTID, 
                    mtgPrefix + pgCompInfoMTGTiledView.CHILD_HDCOPYID 
            };
            // must path pagelet ViewBean
            handleUpdateCompInfoMTG(compPropkeys, dcm, (ViewBean)getCurrNDPage().getChild(pageletName));
        } catch (Exception ex) {
            logger.error("Exception @UWH:saveCompInfoCheckBox:save Component in MTG ");
            logger.error(ex);
            logger.error(StringUtil.stack2string(ex));
        }
        //-- CompInfoMortgage Tile section ends --//

        //-- CompInfoLOC Tile section starts --//
        try {
            String locPrefix = pgComponentInfoPageletViewBean.CHILD_TILEDLOCCOMP + "/";

            String [ ] complockeys = {
                    locPrefix + pgCompInfoLOCTiledView.CHILD_HDCOMPONENTID, 
                    locPrefix + pgCompInfoLOCTiledView.CHILD_HDCOPYID 
            };
            // must path pagelet ViewBean
            handleUpdateCompInfoLOC(complockeys, dcm, (ViewBean)getCurrNDPage().getChild(pageletName));
        } catch(Exception ex)  {
            logger.error("Exception @UWH:SaveData:saveCompInfoCheckBox Component in LOC ");
            logger.error(ex);
            logger.error(StringUtil.stack2string(ex));
        }
        //-- CompInfoLOC Tile section ends --//
    }

    /**
     * <P> handleUpdateCompInfoMTG </p>
     * <p> save component mortgage section data into DB</p>
     * 
     * @param keyValues String[]
     * @param dcm CalcMonitor
     * @param parent ViewBean this is pagelet ViewBean 
     * 
     * @version 1.1 MCM Team (XS 2.60)
     */
    public void handleUpdateCompInfoMTG(String[] keyValues, CalcMonitor dcm, ViewBean parent)
    throws Exception {

        try {
            Hashtable propList = getPropertiesForThisPage(COMPONENTINFO_MTG_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For Component Update");
            updateForRepeatedRows(propList, keyValues, "ComponentMtg", dcm, parent);
        } catch(Exception ex)  {
            logger.error("Exception @ComponentDetailsHandler:SaveData:save MTG ");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }
    }

    /**
     * <P> handleUpdateCompInfoLOC </p>
     * <p> save component LOC section data into DB</p>
     * 
     * @param keyValues String[]
     * @param dcm CalcMonitor
     * @param parent ViewBean this is pagelet ViewBean 
     * 
     * @version 1.1 MCM Team (XS 2.60)
     */
    public void handleUpdateCompInfoLOC(String[] keyValues, CalcMonitor dcm, ViewBean parent)
    throws Exception {
        try
        {
            Hashtable propList1 = getPropertiesForThisPage(COMPONENTINFO_LOC_PROP_FILE_NAME);
            if (propList1 == null) throw new Exception("Null Property File For ComponentLOC");
            updateForRepeatedRows(propList1, keyValues, "ComponentLOC", dcm, parent);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateApplicantAddress");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }
    }

    public PassiveMessage validateComponents(PageEntry pe,
            SessionResourceKit srk) throws Exception {

        PassiveMessage pm = new PassiveMessage();
        
        Deal deal = DBA.getDeal(srk, pe.getPageDealId(), pe
                .getPageDealCID());
        Collection<Component> components = deal.getComponents();
        
        try {
            int langId = theSessionState.getLanguageId();
            BusinessRuleExecutor brExec = new BusinessRuleExecutor();
            brExec.setCurrentComponent(-1);
            brExec.setCurrentBorrower(-1);
            brExec.setCurrentProperty(-1);
            
            PassiveMessage pmComp = new PassiveMessage();
            brExec.BREValidator(
                    theSessionState, pe, srk, pmComp, "DMC-%", true);
            
            if (pmComp.getNumMessages() > 0) {
                pm.addMsg(BXResources.getSysMsg(
                        "PASSIVE_MESSAGE_SUBTITLE_ACROSS_COMPONENTS_PROBLEM", langId)
                        , PassiveMessage.INFO);
                pm.addAllMessages(pmComp);
            }

            int lastCompTypeid = -1;
            int compSequence = 0;

            for (Component comp : components) {

                brExec.setCurrentComponent(comp.getComponentId());
                brExec.setCurrentBorrower(-1);
                brExec.setCurrentProperty(-1);

                int compTypeId = comp.getComponentTypeId();
                compSequence = (lastCompTypeid != compTypeId) ? 1 : compSequence + 1;
                PassiveMessage pmCompDEC = new PassiveMessage();
                brExec.BREValidator(
                        theSessionState, pe, srk, pmCompDEC, "DEC-%", true);
                if (pmCompDEC.getNumMessages() > 0) {
                    pm.addSeparator();
                    pm.addMsg(BXResources.getSysMsg(
                            "PASSIVE_MESSAGE_SUBTITLE_COMPONENT_PROBLEM", langId)
                            + BXResources.getPickListDescription(-1, "COMPONENTTYPE", compTypeId, langId)
                            + " " + compSequence,
                                    PassiveMessage.INFO);
                    pm.addAllMessages(pmCompDEC);
                    pm.addSeparator();
                }
                lastCompTypeid = compTypeId;
            }
        } catch (Exception e) {
            _log.error("Exception @DHC:validateComponents");
            _log.error(e);
            _log.error(StringUtil.stack2string(e));
            throw e;
        }
        return pm;
    }
    
    }
    
class DeleteEntityProperties extends Object
    implements Serializable
{
    protected int type;
    protected String message;
    protected String diag;
    protected int targetTag=Mc.TARGET_NONE;
    protected Vector rowindexes;
    protected String primaryIdVar;
    protected String primaryColName;
    protected String copyIdVar;
    protected String entityName;

    //--Release2.1--//
    //--> Added LanguageId for Multilinggual Support
    //--> By Billy 10Dec2002
    /**
     *
     *  @version 1.16 XS 2.32: delete component modified:
     *  @author MCM Team
     */
    DeleteEntityProperties(int type, Vector rowindexes, String primaryIdVar, String primaryColName, String copyIdVar, String entityName, int languageId)
    {
        this.rowindexes = rowindexes;

        this.primaryIdVar = primaryIdVar;

        this.primaryColName = primaryColName;

        this.copyIdVar = copyIdVar;

        this.entityName = entityName;

        this.type = type;

        switch(type)
        {
        case 1 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_APPLICANT", languageId);
        break;
        case 2 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_PROPERTY", languageId);
        break;
        case 3 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_DOWN_PAYMENT", languageId);
        break;
        case 4 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ESCROW_PAYMENT", languageId);
        break;
        case 5 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_PROPERTY_EXPENSE", languageId);
        break;
        case 6 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ADDRESS", languageId);
        targetTag = Mc.TARGET_APP_ADDRESS;
        break;
        case 7 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_EMP_RECORD", languageId);
        targetTag = Mc.TARGET_APP_EMPLOYMENT;
        break;
        case 8 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_INCOME_RECORD", languageId);
        targetTag = Mc.TARGET_APP_INCOME;
        break;
        case 9 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_CREDIT_REFERENCE", languageId);
        targetTag = Mc.TARGET_APP_CREDIT;
        break;
        case 10 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ASSET", languageId);
        targetTag = Mc.TARGET_APP_ASSET;
        break;
        case 11 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_LIAB", languageId);
        targetTag = Mc.TARGET_APP_LIAB;
        break;
        case 12 : message = BXResources.getSysMsg("CONFIRM_DELETE_CREDIT_BUREAU_LIAB", languageId);
        targetTag = Mc.TARGET_APP_CREDIT_BUREAU_LIAB;
        break;
        //--DJ_CR136--23Nov2004--start--//
        case 13 : message = BXResources.getSysMsg("CONFIRM_DELETE_INSURE_ONLY_APPLICANT", languageId);
        targetTag = Mc.TARGET_LD_INSUREONLYAPPLICANT;
        break;
        //--DJ_CR136--23Nov2004--end--//

        //  ***** Change by NBC Impl. Team - Version 1.11 - Start *****//
        case 14 : message = BXResources.getSysMsg(
                "CONFIRM_DELETE_INSURE_ONLY_IDENTIFICATION", languageId);
        targetTag = Mc.TARGET_APP_IDENTIFICATION;
        break;
        //  ***** Change by NBC Impl. Team - Version 1.11 - Start *****//

        /****************MCM Impl Team XS_2.32 changes starts********************/
        case 15 : message = BXResources.getSysMsg(
                "COMPONENT_DETAIL_CONFIRM_DELETE_COMPONENT", languageId);
        break;
        /****************MCM Impl Team XS_2.32 changes ends********************/
        }

        String tmpIndxs = "";

        for(int i = 0; i < rowindexes.size();	i ++)
        {
            tmpIndxs += ((Integer)rowindexes.elementAt(i)).intValue() + ", ";
        }

        diag = "Type = " + type + "(indexes(" + tmpIndxs + "), " + primaryIdVar + ", " + primaryColName + ", " + copyIdVar + ", " + entityName + ")";

    }

}


class ExportEntityProperties extends Object
implements Serializable
{
    protected int type;
    protected String message;
    protected String diag;
    protected int targetTag=Mc.TARGET_NONE;
    protected Vector rowindexes;
    protected String primaryIdVar;
    protected String primaryColName;
    protected String copyIdVar;
    protected String entityName;
    private int liabExportDirectionFlag;

    //--Release2.1--//
    //--> Added LanguageId for Multilinggual Support
    //--> By Billy 10Dec2002
    ExportEntityProperties(int type, Vector rowindexes, String primaryIdVar, String primaryColName, String copyIdVar, String entityName, int flag, int languageId)
    {
        this.rowindexes = rowindexes;

        this.primaryIdVar = primaryIdVar;

        this.primaryColName = primaryColName;

        this.copyIdVar = copyIdVar;

        this.entityName = entityName;

        this.type = type;

        this.liabExportDirectionFlag = flag;

        switch(type)
        {
        case 1 : message = BXResources.getSysMsg("DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_LIAB", languageId);
        targetTag = Mc.TARGET_APP_LIAB;
        break;
        case 12 : message = BXResources.getSysMsg("CONFIRM_EXPORT_CREDIT_BUREAU_LIAB", languageId);
        targetTag = Mc.TARGET_APP_CREDIT_BUREAU_LIAB;
        break;
        }

        String tmpIndxs = "";

        for(int i = 0; i < rowindexes.size(); i ++)
        {
                  // Auto-conversion is not done proper. In any case it shold be (at least temporarily)
                  // returned to the original expression to escape the overhead of the extra primitive
                  // type conversion. It is an expensive operation.
                  //tmpIndxs +=((Integer)com.iplanet.jato.util.TypeConverter.asInt(rowindexes.elementAt(i))) + ", ";

            tmpIndxs += ((Integer)rowindexes.elementAt(i)).intValue() + ", ";
        }

        diag = "Type = " + type + "(indexes(" + tmpIndxs + "), " + primaryIdVar + ", " + primaryColName + ", " + copyIdVar + ", " + entityName + ", " + copyIdVar + ")";

    }


    protected void setLiabExportDirectionFlag(int flag)
    {
        liabExportDirectionFlag = flag;

    }


    protected int getLiabExportDirectionFlag()
    {
        return liabExportDirectionFlag;

    }



}


class ExportDeleteEntityProperties extends Object
implements Serializable
{
    protected int type;
    protected String message;
    protected String diag;
    protected int targetTag=Mc.TARGET_NONE;
    protected Vector rowindexes;
    protected String primaryIdVar;
    protected String primaryColName;
    protected String copyIdVar;
    protected String entityName;
    private int liabExportDirectionFlag;

    ExportDeleteEntityProperties(int type, Vector rowindexes, String primaryIdVar, String primaryColName, String copyIdVar, String entityName, int languageId)
    {
        this.rowindexes = rowindexes;

        this.primaryIdVar = primaryIdVar;

        this.primaryColName = primaryColName;

        this.copyIdVar = copyIdVar;

        this.entityName = entityName;

        this.type = type;

        switch(type)
        {
        case 12 : message = BXResources.getSysMsg("CONFIRM_EXPORT_DELETE_CREDIT_BUREAU_LIAB", languageId);
        targetTag = Mc.TARGET_APP_CREDIT_BUREAU_LIAB;
        break;
        }

        String tmpIndxs = "";

        for(int i = 0; i < rowindexes.size(); i ++)
        {
            //// Auto-conversion is not done proper. In any case it shold be (at least temporarily)
            //// returned to the original expression to escape the overhead of the extra primitive
            //// type conversion. It is an expensive operation.
            ////tmpIndxs +=((Integer)com.iplanet.jato.util.TypeConverter.asInt(rowindexes.elementAt(i))) + ", ";

            tmpIndxs += ((Integer)rowindexes.elementAt(i)).intValue() + ", ";
        }

        diag = "Type = " + type + "(" + "indexes(" + tmpIndxs + ")" + ", " + primaryIdVar + ", " + primaryColName + ", " + copyIdVar + ", " + entityName + ")";

    }

}

