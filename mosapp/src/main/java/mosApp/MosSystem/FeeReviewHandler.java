package mosApp.MosSystem;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.Fee;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.util.TypeConverter;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.OptionList;

/**
 *
 *
 */
public class FeeReviewHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	private static final String FEE_REVIEW_SAVE_MSG="SAVEWORK";

	/**
	 *
	 *
	 */
	public FeeReviewHandler cloneSS()
	{
		return(FeeReviewHandler) super.cloneSafeShallow();
	}


	///////////////////////////////////////////////////////////////////////////
	//This method is used to populate the fields in the header
	//with the appropriate information
	public void populatePageDisplayFields()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		populatePageShellDisplayFields();
		// Quick Link Menu display
		displayQuickLinkMenu();
		
		populateTaskNavigator(pg);
		populatePageDealSummarySnapShot();
		populatePreviousPagesLinks();
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
		revisePrevNextButtonGeneration(pg, tds);
		populateFeeTypeDescriptionComboBox();
		prepareJScriptData();
		setJSValidation();
	}

	/////////////////////////////////////////////////////////////////////
	public void setJSValidation()
	{
		// Validate # of Dependants
		HtmlDisplayFieldBase df = null;
		String theExtraHtml = null;
		ViewBean thePage = getCurrNDPage();

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbFeeAmount");
//		#4786
		theExtraHtml = " onBlur=\"isFieldInDecRange(0, 99999999999.99);\"";
		df.setExtraHtml(theExtraHtml);

		// Set the compare date to today
		//  Validate Month
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/cbMonth");
		theExtraHtml = "onBlur=\"isFieldValidDate('tbYear', 'cbMonth', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);

		//  Validate Day
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbDay");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('tbYear', 'cbMonth', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Year
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbYear");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('tbYear', 'cbMonth', 'tbDay');\"";
		df.setExtraHtml(theExtraHtml);
	}

	///////////////////////////////////////////////////////////////////////////
	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//
	public void setupBeforePageGeneration()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		if (pg.getSetupBeforeGenerationCalled() == true) return;
		pg.setSetupBeforeGenerationCalled(true);
		setupDealSummarySnapShotDO(pg);

		// set the criteria for the populating data object
		//String applID = pg.getPageDealApplicationId();

		QueryModelBase feesDO =(QueryModelBase)
        (RequestManager.getRequestContext().getModelManager().getModel(doDealFeeModel.class));

		feesDO.clearUserWhereCriteria();
		feesDO.addUserWhereCriterion("dfDealId", "=", new Integer(pg.getPageDealId()));
		feesDO.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));

		QueryModelBase theDO =(QueryModelBase)
        (RequestManager.getRequestContext().getModelManager().getModel(doTotalFeeAmountModel.class));

		theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion("dfDealId", "=", new Integer(pg.getPageDealId()));
		theDO.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));

		// fee display state (corresponds to list of existing tasks repeated
		// display object) ::this object is the default (named "DEFAULT") for this page
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
		Hashtable pst = pg.getPageStateTable();

		if (pst == null) pst = new Hashtable();

		if (tds == null)
		{

			// first invocation!
			saveDealData();

			// determine number of notes (shown) per display page
			String voName = "Repeated1";
			int perPage = ((TiledView)(getCurrNDPage().getChild(voName))).getMaxDisplayTiles();
			int numRows = 0;

			// determine number of deal fee
			try
			{
				feesDO.executeSelect(null);
				numRows = feesDO.getSize();
//logger.debug("BILLY ===> The # of Fee = " + numRows);

				// set up and save task display state object
				tds = new PageCursorInfo("DEFAULT", voName, perPage, numRows);
				tds.setRefresh(true);
				pst.put(tds.getName(), tds);
			}
			catch(Exception e)
			{
				logger.error("Exception @FeeReviewHandler.setupBeforePageGeneration()");
				logger.error(e);
				setStandardFailMessage();
				return;
			}
		}


		// determine if we will force display of first page
		if (tds.isTotalRowsChanged())
		{
      try{
        // number of rows has changed - unconditional display of first page
        TiledView vo =(TiledView)(getCurrNDPage().getChild(tds.getVoName()));
        tds.setCurrPageNdx(0);
        vo.resetTileIndex();

        //Bug fixed -- to sync the total number of rows display -- By BILLY 11March2002
        feesDO.executeSelect(null);

//logger.debug("BILLY ===> (Row changed) Set total number of rows = " + feesDO.getSize());
        tds.setTotalRows(feesDO.getSize());

        //=============================================================================
        tds.setTotalRowsChanged(false);
      }
      catch(Exception e)
      {
        logger.error("Exception @FeeReviewHandler.setupBeforePageGeneration()");
				logger.error(e);
				setStandardFailMessage();
				return;
      }
		}
	}


	///////////////////////////////////////////////////////////////////////////
  //--> This method is not necessary as the Model is already setup @ setupBeforePageGeneration
  //--> Commented out by Billy 12Sept2002
  /*
	public int getRowDisplaySetting(String dobject)
	{
		try
		{
			PageEntry pg = theSession.getCurrentPage();
			Integer cspDealId = new Integer(pg.getPageDealId());
			Integer cspDealCPId = new Integer(pg.getPageDealCID());
			com.iplanet.jato.model.sql.QueryModelBase chkDoObject =(com.iplanet.jato.model.sql.QueryModelBase) CSpider.getDataObject(dobject);
			chkDoObject.clearUserWhereCriteria();
			chkDoObject.addUserWhereCriterion("dfDealId", "=", cspDealId);
			chkDoObject.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);
			chkDoObject.execute();
			int noofrows = chkDoObject.getLastResults().getNumRows();
			if (noofrows > 0) return CSpPage.PROCEED;
			return CSpPage.SKIP;
		}
		catch(Exception e)
		{
			logger.error("Exception FeeReview@getRowDisplayString() ");
			logger.error(e);
			return CSpPage.SKIP;
		}
	}
  */

	///////////////////////////////////////////////////////////////////////////
	public void prepareJScriptData()
	{
		int defaultPaymentDateTypeId = 0;
		Date defaultPaymentDate = null;
		int feeTypeId = 0;
		int defMonth = 0;
		int defDay = 0;
		int defYear = 0;
		int defFeePaymentMethodId = 0;
		String defFeeAmount = null;
		//int defFeeStatusId = 0;

		StringBuffer feeTypeArr = new StringBuffer();
		StringBuffer amountArr = new StringBuffer();
		StringBuffer methodArr = new StringBuffer();
		StringBuffer monthArr = new StringBuffer();
		StringBuffer dayArr = new StringBuffer();
		StringBuffer yearArr = new StringBuffer();

		//int errorCode = 0;

		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			ViewBean currNDPage = getCurrNDPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			String sqlStr = "SELECT DEFAULTFEEPAYMENTDATETYPEID, F1.FEETYPEID, DEFAULTFEEAMOUNT," + " DEFAULTPAYMENTMETHODID FROM FEE F1 WHERE F1.FEEID IN( SELECT MIN( F.FEEID) FROM FEE F," + " FEETYPE T WHERE  F.FEETYPEID = T.FEETYPEID GROUP BY F.FEETYPEID)";

      //--> Modified to use MOS framework JDBCExecutor
      //--> By Billy 13Sept2002
      JdbcExecutor jExec = srk.getJdbcExecutor();
			int key = jExec.execute(sqlStr);
      boolean isFirst = true;
			while (jExec.next(key))
			{
      	String delim;
				if (isFirst == true)
        {
          delim = "[";
          isFirst = false;
        }
				else
          delim = ",";

				//	get Default Payment Date Criteria Id
        defaultPaymentDateTypeId = jExec.getInt(key,1);
				defaultPaymentDate = getDefaultPaymentDate(pst, defaultPaymentDateTypeId);

				// Split date into month-day-year
				defMonth = defaultPaymentDate.getMonth() + 1;
				monthArr.append(delim + defMonth);
				defDay = defaultPaymentDate.getDate();
				dayArr.append(delim + defDay);
				defYear = defaultPaymentDate.getYear() + 1900;
				yearArr.append(delim + defYear);

				//Get Fee Type Id
				feeTypeId = jExec.getInt(key, 2);
				feeTypeArr.append(delim + feeTypeId);

				//Get Default Fee Amount
				defFeeAmount = "" + jExec.getDouble(key, 3);
				if (defFeeAmount.length() == 0)
				{
					defFeeAmount = "0.00";
				}
				amountArr.append(delim + defFeeAmount);

				//Get Default Payment Method Id
				defFeePaymentMethodId = jExec.getInt(key, 4);
				methodArr.append(delim + defFeePaymentMethodId);
			}//end while

      jExec.closeData(key);

			monthArr.append("]");
			dayArr.append("]");
			yearArr.append("]");
			feeTypeArr.append("]");
			amountArr.append("]");
			methodArr.append("]");
			ComboBox comboBox =(ComboBox) getCurrNDPage().getDisplayField("Repeated1/cbFeeDescription");
			String onChangeAttrib = new String("onChange=\"findSelectedOptionAndSetDefaults(" + feeTypeArr.toString() + "," + amountArr.toString() + "," + methodArr.toString() + "," + monthArr.toString() + "," + dayArr.toString() + "," + yearArr.toString() + "," + Mc.FEE_STATUS_REQUIRED + ");\"");
			comboBox.setExtraHtml(onChangeAttrib);
		}
		catch(Exception e)
		{
			logger.error("Exception @FeeReviewHandler.prepareJScriptData()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}

	///////////////////////////////////////////////////////////////////////////
	private Date getDefaultPaymentDate(Hashtable pst, int defaultPaymentDateTypeId)
	{
		Date defaultPaymentDate = null;

		try
		{
			if (defaultPaymentDateTypeId == FEE_PAYMENT_DATE_CURRENT)
			{
				defaultPaymentDate =(Date) pst.get("TodayDate");
			}
			else if (defaultPaymentDateTypeId == FEE_PAYMENT_DATE_CURRENT_AND_TEN)
			{
				GregorianCalendar calendar =(GregorianCalendar) GregorianCalendar.getInstance();
				Date todayDate =(Date) pst.get("TodayDate");
				calendar.setTime(todayDate);
				calendar.add(Calendar.DATE, 10);
				defaultPaymentDate = calendar.getTime();
			}
			else if (defaultPaymentDateTypeId == FEE_PAYMENT_DATE_OFFER_RETURN)
			{
				defaultPaymentDate =(Date) pst.get("DealReturnDate");
			}
			else if (defaultPaymentDateTypeId == FEE_PAYMENT_DATE_FUNDS_ADVANCED)
			{
				defaultPaymentDate =(Date) pst.get("EstClosingDate");
			}
			if (defaultPaymentDate == null)
			{
				return(Date) pst.get("TodayDate");
			}
			return defaultPaymentDate;
		}
		catch(Exception e)
		{
			logger.error("Exception @FeeReviewHandler.getDefaultPaymentDate()");
			logger.error(e);
			setStandardFailMessage();
			return null;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	private void saveDealData()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();
		Hashtable pst = pg.getPageStateTable();
		Date myDate = null;

		try
		{
			//CalcMonitor.getMonitor(srk)
			Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
			if (deal.getEstimatedClosingDate() != null)
			{
				myDate = deal.getEstimatedClosingDate();
				pst.put("EstClosingDate", myDate);
			}
			if (deal.getReturnDate() != null)
			{
				myDate = deal.getReturnDate();
				pst.put("DealReturnDate", myDate);
			}
			pst.put("MIPremiumAmount", new Double(deal.getMIPremiumAmount()));
			pst.put("TodayDate",new java.util.Date());
		}
		catch(Exception e)
		{
			logger.error("Exception @FeeReviewHandler.saveDealData()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////
  //--> This is not necessary to save trhe Data Model, as we can always get
  //--> the Model from ModelManager
  //--> Commented out by Billy 12Sept2002
  /*
	public void saveDataObject(CSpDataObject data)
	{
		if (! data.getName().equals("doDealFee")) return;

		if (! data.succeeded())
		{
			logger.error("ERROR IN THE DATAOBJECT!!");
			return;
		}
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		pst.put("result", data);

		//// ALERT
		//// if (srk.getModified())
		////	pg.setModified(true);
		pg.setModified(true);
		return;
	}
  */

	///////////////////////////////////////////////////////////////////////////
	public void displayFeeDate(int Index)
	{
		Date myDate = null;

		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			ViewBean currNDPage = getCurrNDPage();
			Hashtable pst = pg.getPageStateTable();

      QueryModelBase data =(QueryModelBase)
        (RequestManager.getRequestContext().getModelManager().getModel(doDealFeeModel.class));

			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

			//rowIndex = getCursorAbsoluteNdx(tds, Index);

			if (data.getSize() <= 0)
			{
				return;
			}

			myDate =(Date)data.getValue("dfFeePaymentDate");
			if (myDate != null)
			{
				getCurrNDPage().setDisplayFieldValue("Repeated1/cbMonth", new Integer(myDate.getMonth() + 1));
   			getCurrNDPage().setDisplayFieldValue("Repeated1/tbYear", new String("" +(myDate.getYear() + 1900)));
        getCurrNDPage().setDisplayFieldValue("Repeated1/tbDay", new String("" + myDate.getDate()));
     	}
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception @FeeReviewHandler.displayFeeDate: row is " + Index);
			logger.error(e);
			return;
		}

	}

	///////////////////////////////////////////////////////////////////////////
	public void handleForwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
		handleForwardBackwardCommon(pg, tds, - 1, true);
	}


	///////////////////////////////////////////////////////////////////////////
	public void handleBackwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
		handleForwardBackwardCommon(pg, tds, - 1, false);
	}


	///////////////////////////////////////////////////////////////////////////
	public void handleSubmit()
	{
		PageEntry pe = theSessionState.getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();

		// The following is not necessary -- as it is handled by the JavaScript
		// -- By BILLY 11March2002
		//if(isInvalidFeeAmount())
		//{
		//	setActiveMessageToAlert(Sc.FEE_REVIEW_INVALID_NUMBER,ActiveMsgFactory.ISCUSTOMCONFIRM);
		//	return;
		//}
		//=========================================================================================
		if (validateData(pe, srk) == false) return;

		// an active and/or passive message will have been set in this case
		try
		{
			srk.beginTransaction();
			updateData(pe, srk);

			// business logic as required
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);

			// Change to Generate OffSet (UserFee Only) -- Billy 08March2002
			if (generateOffSetFee(pe, srk, dcm) < 0)
			{
				srk.cleanTransaction();
				return;
			}
			standardAdoptTxCopy(pe, true);
			dcm.calc();
			PassiveMessage pm = new PassiveMessage();
			BusinessRuleExecutor brExec = new BusinessRuleExecutor();
			brExec.BREValidator(getTheSessionState(), pe, srk, pm, "FEE-%");

			//brExec.BREValidator(getTheSessionState(), pe, srk, pm, v);
			boolean anyMessages = pm.getNumMessages() > 0;
			boolean criticalDataProblems = pm.getCritical();
			if (anyMessages == true && criticalDataProblems == false)
			{
				pm.setGenerate(true);
				getTheSessionState().setPasMessage(pm);
				navigateToNextPage();
			}
			else if (criticalDataProblems == true)
			{
				// critical (at least one)
				pm.setGenerate(true);
				getTheSessionState().setPasMessage(pm);
				getSavedPages().setNextPage(pe);
				navigateToNextPage();
			}
			else
			{
				navigateToNextPage();
			}
			srk.commitTransaction();
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception occurred @handleSubmit()");
			logger.error(e);
			logger.error("deal fee: deal Id = " + pe.getPageDealId() + ", copy Id = " + pe.getPageDealCID());
			setStandardFailMessage();
			navigateAwayFromFromPage(true);
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	public void updateData(PageEntry pe, SessionResourceKit srk)
	{
	}

	///////////////////////////////////////////////////////////////////////////
	public void saveData(PageEntry pg, boolean calledInTransaction)
	{
		int i = 0;
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

			//logger.debug("--- Toni ---> 00000000000000000000000000000000");
			if (calledInTransaction == false)
			{
				srk.beginTransaction();
			}

			// update entities
			int feesOnPage = tds.getNumberOfDisplayRowsOnPage();

			//logger.debug("--- Toni ---> 11111111111111111111111111111111111");
			for(i = 0;	i < feesOnPage;	++ i)
			{
				updateDealFee(i, pg, srk, dcm);
			}

			//logger.debug("--- Toni ---> 22222222222222222222222222222222222");
			dcm.calc();
			if (calledInTransaction == false)
			{
				srk.commitTransaction();
			}

			//logger.debug("--- Toni ---> 33333333333333333333333333333333333");
		}
		catch(Exception e)
		{
			if (calledInTransaction == false)
			{
				srk.cleanTransaction();
			}
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Fee Review Problem encountered updating deal fee: " + "deal Id = " + pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID() + ", line number = " + i);
			logger.error(e);
			return;
		}

	}


	///////////////////////////////////////////////////////////////////////////

	private void updateDealFee(int rowNdx, PageEntry pg, SessionResourceKit srk, CalcMonitor dcm)
		throws Exception
	{
		int month = 0;
		int day = 0;
		int year = 0;
		int feeType = 0;
		int dealFeeId = 0;

		try
		{
			dealFeeId = TypeConverter.asInt(getRepeatedField("Repeated1/hdDealFeeId", rowNdx), -1);
			if(dealFeeId == -1)
			{
				return;
			}
			DealFee df = new DealFee(srk, dcm, dealFeeId, pg.getPageDealCID());

			// read and set properties
			/// Fee Payment Date
			month = TypeConverter.asInt(getRepeatedField("Repeated1/cbMonth", rowNdx), 0) - 1;
      day = TypeConverter.asInt(getRepeatedField("Repeated1/tbDay", rowNdx), 0);
      year = TypeConverter.asInt(getRepeatedField("Repeated1/tbYear", rowNdx), 0);

      if(month > 0 && day > 0 && year > 0)
      {
        GregorianCalendar calendar =(GregorianCalendar) GregorianCalendar.getInstance();
        calendar.set(year, month, day);
        df.setFeePaymentDate(calendar.getTime());
      }

			//logger.debug("--- Toni ---> 88888888888888888888888888888888888888888");
			//Fee Id for the new Fee Type Id
      feeType =TypeConverter.asInt(getRepeatedField("Repeated1/cbFeeDescription", rowNdx), -1);

			//logger.debug("--- Toni ---> 99999999999999999999999999999999999999999");
      //--> Modified to use Fee Entity to get the FeeId
      //--> By Billy 12Sept2002
      /*
			CSpDBResult result = CSpDataObject.executeImmediate("orcl", "SELECT MIN(FEEID) FROM FEE WHERE FEETYPEID = " + feeType);
			int errorCode = result.getResultStatus().getErrorCode();
			if (errorCode != CSpDataObject.SUCCESS && errorCode != CSpDataObject.END_OF_FETCH)
			{
				throw new Exception("Fee Review: SQL Error");
			}
      */
      int theFeeId = 0;
      if(feeType > 0)
      {
        try
        {
          theFeeId = new Fee(srk, null).findFirstByType(feeType).getFeeId();
        }
        catch(Exception e)
        {
          logger.warning("@FeeReviewHandler.updateDealFee : FeeId not find (default to 0) : " + e);
        }
      }

			df.setFeeId(theFeeId);

      //Set Fee Amount
      df.setFeeAmount(TypeConverter.asDouble(getRepeatedField("Repeated1/tbFeeAmount", rowNdx), 0));
      //Fee Payment Method Id
      df.setFeePaymentMethodId(TypeConverter.asInt(getRepeatedField("Repeated1/cbPaymentMethod", rowNdx), 0));
			//Fee Status Id
      df.setFeeStatusId(TypeConverter.asInt(getRepeatedField("Repeated1/cbFeeStatus", rowNdx), 0));

			df.ejbStore();
		}
		catch(Exception ex)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Problem encountered updating deal fee: " + "deal Id = " + pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID() + ", deal fee Id = " + dealFeeId);
			logger.error(ex.toString());
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	public void deleteDealFee(RequestInvocationEvent event)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			if (disableEditButton(pg) == true) return;
			pst.put("rowIndex", new Integer(getRowNdxFromWebEventMethod(event)));
			pg.setPageCondition3(true);

			// Raise Cond 3
			setActiveMessageToAlert(BXResources.getSysMsg("FEE_REVIEW_DELETE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMDIALOG);
		}
		catch(Exception e)
		{
			logger.error("Exception @FeeReviewHandler.deleteDealFee()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	public void handleCustomActMessageOk(String[] args)
	{
		// check for "A" response
		// if (args[0].equals(FEE_REVIEW_SAVE_MSG))
		//{
		// trigger std handler method for this case but with confirmed set
		handleOnDialogButton();
	}


	///////////////////////////////////////////////////////////////////////////
	public void handleOnDialogButton()
	{
		int dealFeeId = 0;
		PageEntry pg = getTheSessionState().getCurrentPage();

		if (pg.getPageCondition3() == false)
		{
			return;
		}

		pg.setPageCondition3(false);

		try
		{
			SessionResourceKit srk = getSessionResourceKit();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
			int rowNdx =((Integer)(pst.get("rowIndex"))).intValue();

			dealFeeId = TypeConverter.asInt(getRepeatedField("Repeated1/hdDealFeeId", rowNdx));
			logger.trace("FeeReviewHandler.handleOnDialogButton :: Deleting dealFeeId = " + dealFeeId);

			srk.beginTransaction();
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);
			DealFee df = new DealFee(srk, dcm, dealFeeId, pg.getPageDealCID());

			//Check if any OffSet Fee (UserType Only) to remove -- Billy 08March2002
			int offSetFeeTypeId = df.checkOffSetFeeTypeId(Sc.FEE_GENERATION_TYPE_USER);
			if (offSetFeeTypeId >= 0)
			{
				DealFee offSetDF = new DealFee(srk, dcm)
                  .findFirstByDealAndType(new DealPK(pg.getPageDealId(), pg.getPageDealCID()), offSetFeeTypeId);
				if (offSetDF != null)
				{
					// Remove the OffSet Fee if found
					offSetDF.ejbRemove();
				}
			}
			df.ejbRemove();
			dcm.calc();
			srk.commitTransaction();

			// The following may not necessary !! -- BILLY 08March2002
			tds.setTotalRows(tds.getTotalRows() - 1);
			tds.setTotalRowsChanged(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @FeeReviewHandler.handleOnDialogButton()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	public void addDealFee()
	{
		SessionResourceKit srk = getSessionResourceKit();
		PageEntry pg = getTheSessionState().getCurrentPage();

		if (disableEditButton(pg) == true) return;

		try
		{
			PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

			// Get the selected FeeType
			int selectedFeeId = - 1;
			selectedFeeId = TypeConverter.asInt(getCurrNDPage().getDisplayFieldValue("cbAddFeeDesc"), -1);

			// Check if FeeType selected
			if (selectedFeeId == - 1) return;

			// Get the selected FeeType
			Fee theFee = new Fee(srk, null);
			try
			{
				theFee = theFee.findFirstByType(selectedFeeId);
			}
			catch(Exception e)
			{
				// No such FeeType -- Create default DealFee
				theFee = null;
			}
			srk.beginTransaction();
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);
			DealFee df = new DealFee(srk, dcm);
			df.create(new DealPK(pg.getPageDealId(), pg.getPageDealCID()));
			if (theFee != null)
			{
				df.setFeeId(selectedFeeId);
				df.setFeePaymentDate(getDefaultPaymentDate(pg.getPageStateTable(), theFee.getDefaultFeePaymentDateTypeId()));
				df.setFeePaymentMethodId(theFee.getDefaultPaymentMethodId());
				df.setFeeAmount(theFee.getDefaultFeeAmount());
			}
			else
			{
				df.setFeePaymentMethodId(0);
			}
			df.setFeeStatusId(0);
			df.ejbStore();
			dcm.calc();
			srk.commitTransaction();
			tds.setTotalRows(tds.getTotalRows() + 1);
			tds.setTotalRowsChanged(true);
			return;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Problem encountered adding deal fee: " + "deal Id = " + pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID());
			logger.error(e);
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	public boolean validateData(PageEntry pe, SessionResourceKit srk)
	{
		try
		{
			checkFeeTypeValidation(pe, srk);
			return true;
		}
		catch(Exception e)
		{
			logger.error("Validation problem encountered: " + "deal Id = " + pe.getPageDealId() + ", copy Id = " + pe.getPageDealCID());
			logger.error(e.getMessage());
			return false;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	private void checkFeeTypeValidation(PageEntry pg, SessionResourceKit srk)
		throws Exception
	{
		//--> Modified to use MOS framework JDBCExecutor
    //--> By Billy 12Sept2002
		String sqlStr = "SELECT T.FEETYPEDESCRIPTION, COUNT(*), MIN(T.FEETYPEID) FROM DEALFEE D, FEE F, FEETYPE T"
      + " WHERE D.FEEID = F.FEEID AND F.FEETYPEID = T.FEETYPEID"
      + " AND D.DEALID = " + pg.getPageDealId()
      + " AND D.COPYID = " + pg.getPageDealCID()
      + " GROUP BY T.FEETYPEDESCRIPTION";

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sqlStr);

      while (jExec.next(key))
      {
        if(jExec.getInt(key,2) > 1) // duplication
        {
          setActiveMessageToAlert(BXResources.getSysMsg("FEE_TYPE_DUPLICATION_WARNING", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
          throw new Exception("Fee Review Check: Fee Type Validation.  "
            + " Fee Type ( " + jExec.getString(key, 1) + " ) has "
            + jExec.getInt(key,2) + " copies.");
        }
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Fee Type: SQL Error : " + e);
      throw new Exception("Fee Type: SQL Error");
    }
	}

	///////////////////////////////////////////////////////////////////////////
	private void populateFeeTypeDescriptionComboBox()
	{
		ComboBox comboBox =(ComboBox)(getCurrNDPage().getDisplayField("Repeated1/cbFeeDescription"));
    OptionList option = (OptionList)comboBox.getOptions();
		option.clear();

		ComboBox comboBoxAdd =(ComboBox)(getCurrNDPage().getDisplayField("cbAddFeeDesc"));
    OptionList optionAdd = (OptionList)comboBoxAdd.getOptions();
		optionAdd.clear();

		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			String sqlStr = "SELECT DISTINCT T1.FEETYPEID TYPEID, T1.FEETYPEDESCRIPTION DESCRIPTION"
        + " FROM FEETYPE T1, FEE F1 WHERE F1.FEETYPEID = T1.FEETYPEID"
        + " AND F1.FEEGENERATIONTYPEID = " + Mc.FEE_GENERATION_TYPE_USER
        + " ORDER BY T1.FEETYPEID"; // LS Merge FXP24618
			JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sqlStr);

      //--Release2.1--//
      //--> Get Non-Select Label from ResourceBundle
      //optionAdd.add(0, "Please Select ...", "-1");
      int languageId = theSessionState.getLanguageId();
      optionAdd.add(0,
        BXResources.getGenericMsg("CBFEETYPEDESC_NONSELECTED_LABEL", languageId),
        "-1");
      //============================================

      String tmpLabel;
      int i=0;
      while (jExec.next(key))
      {
        //--Release2.1--//
        //--> Get Fee Type Description from ResourceBundle
        //option.add(i, jExec.getString(key, 2),  jExec.getString(key, 1));
        //optionAdd.add(i+1, jExec.getString(key, 2),  jExec.getString(key, 1));
        tmpLabel = BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"FEETYPE", jExec.getString(key, 1), languageId);
        option.add(i, tmpLabel, jExec.getString(key, 1));
        optionAdd.add(i+1, tmpLabel, jExec.getString(key, 1));
        //================================================
        i++;
			}
      jExec.closeData(key);

		}
		catch(Exception ex)
		{
			logger.error("Fee Rewiew: Fee Type: Problem encountered: " + "deal Id = " + pg.getPageDealId() + ", copy Id = " + pg.getPageDealCID());
      logger.error(ex.toString());
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	//		SYSTEM-GENERATED FEES
	// This is not used any more -- FeeReview Screen should not touch any System Fees
	//    -- By Billy 07March2002
	private int generateSystemFee(PageEntry pg, SessionResourceKit srk, CalcMonitor dcm)
	{
		Hashtable pst = pg.getPageStateTable();
		int dealId = pg.getPageDealId();
		int copyId = pg.getPageDealCID();

		try
		{
			DealFee df = new DealFee(srk, dcm);

      //--> Modified to use MOS framework JDBCExecutor
      //--> By Billy 12Sept2002
			// Delete all previosly generated system fees
			String sqlStr = "DELETE FROM DEALFEE WHERE DEALFEEID IN(SELECT DF.DEALFEEID FROM DEALFEE DF, FEE F" + " WHERE F.FEEID=DF.FEEID AND F.FEEGENERATIONTYPEID=" + Sc.FEE_GENERATION_TYPE_SYSTEM + " AND DF.DEALID=" + dealId + " AND DF.COPYID=" + copyId + ")";
			JdbcExecutor jExec = srk.getJdbcExecutor();
      jExec.executeUpdate(sqlStr);

			// generate all <offsetting> fees
			sqlStr = "SELECT MIN(S_F.FEEID), MIN(U_DF.FEEAMOUNT)," + " MIN(S_F.DEFAULTPAYMENTMETHODID)" + " FROM  FEE U_F, DEALFEE U_DF, FEE S_F WHERE" + " S_F.FEETYPEID=U_F.OFFSETINGFEETYPEID AND" + " U_F.FEEID=U_DF.FEEID AND U_DF.DEALID=" + dealId + " AND U_DF.COPYID=" + copyId + " AND U_F.FEEGENERATIONTYPEID = " + Mc.FEE_GENERATION_TYPE_USER + " AND S_F.FEEGENERATIONTYPEID = " + Mc.FEE_GENERATION_TYPE_SYSTEM +
//		" AND S_F.FEETYPEID <>" + Sc.FEE_TYPE_NONE +
			" AND U_F.FEETYPEID <>" + Sc.FEE_TYPE_CMHC_FEE +
//PROCESSED FURTHER
			" GROUP BY S_F.FEETYPEID ";
			int key = jExec.execute(sqlStr);
			while (jExec.next(key))
			{
				df.create(new DealPK(dealId, copyId));
				df.setFeeId(jExec.getInt(key, 1));
				int defaultPaymentDateTypeId = jExec.getInt(key,3);
				df.setFeePaymentMethodId(defaultPaymentDateTypeId);
				df.setFeePaymentDate(getDefaultPaymentDate(pst, defaultPaymentDateTypeId));
				df.setFeeStatusId(Sc.FEE_STATUS_REQUIRED);

				df.setFeeAmount(jExec.getDouble(key, 2));

				df.ejbStore();
			}
      jExec.closeData(key);

			// Commented the following out, it is not necessary to do this -- By BILLY 23Jan2002
			//=========================================================================================
			//CSpCriteriaSQLObject dfDo = (CSpCriteriaSQLObject)getModel(doDealFeeModel.class);
			//dfDo.clearUserWhereCriteria();
			//dfDo.addUserWhereCriterion("dfDealId", "=",
			//	    new String("" + dealId));
			//dfDo.addUserWhereCriterion("dfCopyId", "=",
			//	    new String("" + copyId));
			//dfDo.addUserWhereCriterion("dfFeeTypeId", "=",
			//	    new Integer(Sc.FEE_TYPE_CMHC_FEE));
			//dfDo.execute();
			//if( dfDo.getLastResults().getNumRows() > 0)
			//{
			// 	updateDealFeeRecord(pst, dealId, copyId, srk, dcm, Sc.FEE_TYPE_CMHC_PREMIUM);
			//	updateDealFeeRecord(pst, dealId, copyId, srk, dcm, Sc.FEE_TYPE_CMHC_PREMIUM_PAYABLE);
			//}
			// =========================================================================================
			return 0;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Error in the FeeReview.generateSystemFee()");
			logger.error(e);
			return - 1;
		}
	}


	///////////////////////////////////////////////////////////////////////////
	private void updateDealFeeRecord(Hashtable pst, int dealId, int copyId, SessionResourceKit srk, CalcMonitor dcm, int feeType)
		throws Exception
	{
		DealFee df = new DealFee(srk, dcm);
		df.create(new DealPK(dealId, copyId));

    //--> Modified to use MOS framework JDBCExecutor
    //--> By Billy 12Sept2002
		String sqlStr = "SELECT FEETYPEID, MIN(FEEID), MIN(DEFAULTPAYMENTMETHODID)"
      + " FROM FEE WHERE FEETYPEID =" + feeType
      + " GROUP BY FEETYPEID";

		JdbcExecutor jExec = srk.getJdbcExecutor();
    int key = jExec.execute(sqlStr);

		int feeId =jExec.getInt(key, 2);

		df.setFeeId(feeId);

		int defaultPaymentDateTypeId = jExec.getInt(key,3);

		df.setFeePaymentMethodId(defaultPaymentDateTypeId);

		df.setFeePaymentDate(getDefaultPaymentDate(pst, defaultPaymentDateTypeId));

		df.setFeeStatusId(Sc.FEE_STATUS_REQUIRED);

		df.setFeeAmount(TypeConverter.asDouble(pst.get("MIPremiumAmount")));

		df.ejbStore();

    jExec.closeData(key);
	}

	///////////////////////////////////////////////////////////////////////////
	public String generateLabels(String fieldname)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
		
//		#4702
		int languageId = theSessionState.getLanguageId();

		if (tds.getTotalRows() > 0)
		{
			//logger.debug("--- Toni ---> fieldname = " + fieldname);
			if (fieldname.equals("stDollarSign"))
			{
				//logger.debug("--- Toni ---> dollar.");
				return ("$");
			}
			if (fieldname.equals("stMonthLabel"))
			{
				//logger.debug("--- Toni ---> Mth.");
//				return ("Mth:");
				return BXResources.getGenericMsg("MONTH_SHORT", languageId);
			}
			if (fieldname.equals("stDayLabel"))
			{
				//logger.debug("--- Toni ---> Day.");
//				return ("Day:");
				return BXResources.getGenericMsg("DAY_SHORT", languageId);
			}
			if (fieldname.equals("stYearLabel"))
			{
				//logger.debug("--- Toni ---> Yr.");
//				return ("Yr:");
				return BXResources.getGenericMsg("YEAR_SHORT", languageId);
			}
		}
		return "";
	}


	//==========================================================================
	// Added pop-up section to get FeeTypeId before create
	// -- Modified by Billy 21Nov2001
  //--Release2.1--//
  //Display the appropiate image based on current language
  //--> By Billy 27Nov2002
	public String setAddFeeButtonHTML()
	{
		String langStr = "";
    //No extension for English
    if(theSessionState.getLanguageId() != Mc.LANGUAGE_PREFERENCE_ENGLISH)
      langStr = "_" + BXResources.getLocaleCode(theSessionState.getLanguageId());
    return "<a href=\"javascript:openAddFeeDialog()\"><img src=\"../images/addfee"+
      langStr + ".gif\" ALIGN=TOP height=15 alt=\"\" border=\"0\"></a>";
	}


	///////////////////////////////////////////////////////////////////////////
	//		GENERATED OffSet FEES
	//    -- By Billy 07March2002
	private int generateOffSetFee(PageEntry pg, SessionResourceKit srk, CalcMonitor dcm)
	{
		Hashtable pst = pg.getPageStateTable();
		int dealId = pg.getPageDealId();
		int copyId = pg.getPageDealCID();

		try
		{
			// Get All Master DealFees for the deal (Fees with an OffSet Fee)
			Collection allMasterFees = null;
			try
			{
				allMasterFees = new DealFee(srk, dcm).findAllMasterFees(new DealPK(dealId, 
                           copyId), Sc.FEE_GENERATION_TYPE_USER);
			}
			catch(Exception e)
			{
				// No record found
				allMasterFees = new Vector();
			}
			Iterator iMasterFees = allMasterFees.iterator();
			DealFee masterDF = null;
			while(iMasterFees.hasNext())
			{
				masterDF =(DealFee) iMasterFees.next();
				int theOffSetFeeTypeId = 0;

				// Get the OffSet FeeId
				try
				{
					Fee fee = masterDF.getFee();
					if (fee != null) theOffSetFeeTypeId = fee.getOffSetingFeeTypeId();
				}
				catch(Exception e)
				{
					logger.error("FeeReviewHandler.generateOffSetFee :: FeeId = " + masterDF.getFeeId() + " not found. -- " + e.getMessage());
				}

				// Check if the OffSet Fee already exist
				DealFee offsetDF = new DealFee(srk, dcm).findFirstByDealAndType(new DealPK(dealId, 
                    copyId), theOffSetFeeTypeId);
				if (offsetDF != null)
				{
					// Set the Amount to be the same as the MasterDealFee
					offsetDF.setFeeAmount(masterDF.getFeeAmount());
					offsetDF.setFeeStatusId(masterDF.getFeeStatusId());
				}
				else
				{
					// Create new OffSet DealFee
					Fee fee = new Fee(srk, dcm, theOffSetFeeTypeId);
					offsetDF = new DealFee(srk, dcm);
					offsetDF.create(new DealPK(dealId, copyId));
					offsetDF.setFeeId(theOffSetFeeTypeId);
					offsetDF.setFeePaymentMethodId(fee.getDefaultPaymentMethodId());
					offsetDF.setFeePaymentDate(getDefaultPaymentDate(pst, fee.getDefaultPaymentMethodId()));
					offsetDF.setFeeAmount(masterDF.getFeeAmount());
					offsetDF.setFeeStatusId(masterDF.getFeeStatusId());
				}
				offsetDF.ejbStore();
			}
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Error in the FeeReviewHandler.generateOffSetFee()");
			logger.error(e);
			return - 1;
		}
		return 0;
	}
}

