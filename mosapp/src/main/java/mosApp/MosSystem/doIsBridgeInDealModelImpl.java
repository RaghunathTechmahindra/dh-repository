package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doIsBridgeInDealModelImpl extends QueryModelBase
	implements doIsBridgeInDealModel
{
	/**
	 *
	 *
	 */
	public doIsBridgeInDealModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgeDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGEDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgeDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGEDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgeCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGECOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgeCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGECOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgePurposeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGEPURPOSEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgePurposeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGEPURPOSEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BRIDGE.BRIDGEID, BRIDGE.DEALID, BRIDGE.COPYID, BRIDGE.BRIDGEPURPOSEID, DEAL.DEALID, DEAL.COPYID FROM DEAL, BRIDGE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, BRIDGE";
	public static final String STATIC_WHERE_CRITERIA=" (DEAL.DEALID  =  BRIDGE.DEALID) AND (DEAL.COPYID  =  BRIDGE.COPYID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBRIDGEID="BRIDGE.BRIDGEID";
	public static final String COLUMN_DFBRIDGEID="BRIDGEID";
	public static final String QUALIFIED_COLUMN_DFBRIDGEDEALID="BRIDGE.DEALID";
	public static final String COLUMN_DFBRIDGEDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFBRIDGECOPYID="BRIDGE.COPYID";
	public static final String COLUMN_DFBRIDGECOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFBRIDGEPURPOSEID="BRIDGE.BRIDGEPURPOSEID";
	public static final String COLUMN_DFBRIDGEPURPOSEID="BRIDGEPURPOSEID";
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEID,
				COLUMN_DFBRIDGEID,
				QUALIFIED_COLUMN_DFBRIDGEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEDEALID,
				COLUMN_DFBRIDGEDEALID,
				QUALIFIED_COLUMN_DFBRIDGEDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGECOPYID,
				COLUMN_DFBRIDGECOPYID,
				QUALIFIED_COLUMN_DFBRIDGECOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEPURPOSEID,
				COLUMN_DFBRIDGEPURPOSEID,
				QUALIFIED_COLUMN_DFBRIDGEPURPOSEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

