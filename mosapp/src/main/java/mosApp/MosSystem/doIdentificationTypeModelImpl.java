package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * <p>
 * Title: doIdentificationTypeModelImpl
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.1 Date: 8/11/2006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: Added borrower id and copy id fields -  Fix for defect 4215<br>
 * 
 */
public class doIdentificationTypeModelImpl extends QueryModelBase implements
		doIdentificationTypeModel {

	public doIdentificationTypeModelImpl() {
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}

	public void initialize() {
	}

	protected String beforeExecute(ModelExecutionContext context,
			int queryType, String sql) throws ModelControlException {

		return sql;
	}

	/**
	 * afterExecute
	 * 
	 * @param ModelExecutionContext,queryType
	 *            <br>
	 * 
	 * @return void : the result of afterExecute <br>
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
			throws ModelControlException {

		String defaultInstanceStateName = getRequestContext().getModelManager()
				.getDefaultModelInstanceName(SessionStateModel.class);
		SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
				.getModelManager().getModel(SessionStateModel.class,
						defaultInstanceStateName, true);
		int languageId = theSessionState.getLanguageId();

		while (this.next()) {
			// Convert SALUTATION Desc.
			this.setDfIdentificationType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
					"IDENTIFICATIONTYPE", this.getDfIdentificationType(),
					languageId));

		}
		// Reset Location
		this.beforeFirst();
	}

	/**
	 * getDfIdentificationNumber
	 * 
	 * @param <br>
	 * 
	 * @return String : the result of getDfIdentificationNumber <br>
	 */
	public String getDfIdentificationNumber() {
		return (String) getValue(FIELD_DFIDENTIFICATIONNUMBER);
	}

	/**
	 * setDfIdentificationNumber
	 * 
	 * @param String
	 *            <br>
	 * 
	 * @return void : the result of setDfIdentificationNumber <br>
	 */
	public void setDfIdentificationNumber(String value) {
		setValue(FIELD_DFIDENTIFICATIONNUMBER, value);
	}

	/**
	 * getDfIdentificationType
	 * 
	 * @param <br>
	 * 
	 * @return String : the result of getDfIdentificationType <br>
	 */
	public String getDfIdentificationType() {
		return (String) getValue(FIELD_DFIDENTIFICATIONTYPE);
	}

	/**
	 * setDfIdentificationType
	 * 
	 * @param String
	 *            <br>
	 * 
	 * @return void : the result of setDfIdentificationType <br>
	 */
	public void setDfIdentificationType(String value) {
		setValue(FIELD_DFIDENTIFICATIONTYPE, value);
	}

	/**
	 * getDfIdentificationSourceCountry
	 * 
	 * @param <br>
	 * 
	 * @return String : the result of getDfIdentificationSourceCountry <br>
	 */
	public String getDfIdentificationSourceCountry() {
		return (String) getValue(FIELD_DFIDENTIFICATIONSOURCECOUNTRY);
	}

	/**
	 * setDfIdentificationSourceCountry
	 * 
	 * @param String
	 *            <br>
	 * 
	 * @return void : the result of setDfIdentificationSourceCountry <br>
	 */
	public void setDfIdentificationSourceCountry(String value) {
		setValue(FIELD_DFIDENTIFICATIONSOURCECOUNTRY, value);
	}

	protected void onDatabaseError(ModelExecutionContext context,
			int queryType, SQLException exception) {
	}

	public static final String DATA_SOURCE_NAME = "jdbc/orcl";
//	 ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final String SELECT_SQL_TEMPLATE = "SELECT DISTINCT BORROWERIDENTIFICATION.IDENTIFICATIONID ,BORROWERIDENTIFICATION.BORROWERID,BORROWERIDENTIFICATION.COPYID,"
			+ "BORROWERIDENTIFICATION.IDENTIFICATIONNUMBER , to_char(BORROWERIDENTIFICATION.IDENTIFICATIONTYPEID) IDENTIFICATIONTYPEID_STR , BORROWERIDENTIFICATION.IDENTIFICATIONCOUNTRY,	"
			+ " BORROWERIDENTIFICATION.INSTITUTIONPROFILEID "
			+ " FROM BORROWERIDENTIFICATION  " + "__WHERE__  ";
//	 ***** Change by NBC Impl. Team - Version 1.1 - End *****//

	public static final String MODIFYING_QUERY_TABLE_NAME = "BORROWERIDENTIFICATION";

	public static final String STATIC_WHERE_CRITERIA = "";

	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFIDENTIFICATIONNUMBER = "BORROWERIDENTIFICATION.IDENTIFICATIONNUMBER";

	public static final String COLUMN_DFIDENTIFICATIONNUMBER = "IDENTIFICATIONNUMBER";

	public static final String QUALIFIED_COLUMN_DFIDENTIFICATIONTYPE = "BORROWERIDENTIFICATION.IDENTIFICATIONTYPEID_STR";

	public static final String COLUMN_DFIDENTIFICATIONTYPE = "IDENTIFICATIONTYPEID_STR";

	public static final String QUALIFIED_COLUMN_DFIDENTIFICATIONSOURCECOUNTRY = "BORROWERIDENTIFICATION.IDENTIFICATIONCOUNTRY";

	public static final String COLUMN_DFIDENTIFICATIONSOURCECOUNTRY = "IDENTIFICATIONCOUNTRY";
//	 ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final String QUALIFIED_COLUMN_DFBORROWERID = "BORROWERIDENTIFICATION.BORROWERID";

	public static final String COLUMN_DFBORROWERID = "BORROWERID";

	public static final String QUALIFIED_COLUMN_DFCOPYID = "BORROWERIDENTIFICATION.COPYID";

	public static final String COLUMN_DFCOPYID = "COPYID";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "BORROWERIDENTIFICATION.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	
	/**
	 * <p>
	 * returns borrower Id
	 * </p>
	 * 
	 * @return - returns borrower id
	 */
	public java.math.BigDecimal getDfBorrowerId() {
		return (java.math.BigDecimal) getValue(FIELD_DFBORROWERID);
	}

	/**
	 * <p>
	 * sets borrower id
	 * </p>
	 * 
	 * @param -
	 *            sets borrower id
	 */
	public void setDfBorrowerId(java.math.BigDecimal value) {
		setValue(FIELD_DFBORROWERID, value);
	}

	/**
	 * <p>
	 * returns copy Id
	 * </p>
	 * 
	 * @return - returns copy id
	 */
	public java.math.BigDecimal getDfCopyId() {
		return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);
	}

	/**
	 * <p>
	 * sets copy id
	 * </p>
	 * 
	 * @param -
	 *            sets copy id
	 */
	public void setDfCopyId(java.math.BigDecimal value) {
		setValue(FIELD_DFCOPYID, value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }
	// ***** Change by NBC Impl. Team - Version 1.1 - End *****//
	
	
	
	// //////////////////////////////////////////////////////////////////////////
	// Instance variables
	// //////////////////////////////////////////////////////////////////////////

	// //////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	// //////////////////////////////////////////////////////////////////////////

	// [[SPIDER_EVENTS BEGIN

	static {

		// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFBORROWERID, COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID, java.math.BigDecimal.class,
				true, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFCOPYID, COLUMN_DFCOPYID, QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class, true, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
		
		// ***** Change by NBC Impl. Team - Version 1.1 - End *****//
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFIDENTIFICATIONNUMBER, COLUMN_DFIDENTIFICATIONNUMBER,
				QUALIFIED_COLUMN_DFIDENTIFICATIONNUMBER, String.class, false,
				false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFIDENTIFICATIONTYPE, COLUMN_DFIDENTIFICATIONTYPE,
				QUALIFIED_COLUMN_DFIDENTIFICATIONTYPE, String.class, false,
				false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFIDENTIFICATIONSOURCECOUNTRY,
				COLUMN_DFIDENTIFICATIONSOURCECOUNTRY,
				QUALIFIED_COLUMN_DFIDENTIFICATIONSOURCECOUNTRY, String.class,
				false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	}

}
