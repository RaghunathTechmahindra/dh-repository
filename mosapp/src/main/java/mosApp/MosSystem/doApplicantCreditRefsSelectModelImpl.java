package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doApplicantCreditRefsSelectModelImpl extends QueryModelBase
	implements doApplicantCreditRefsSelectModel
{
	/**
	 *
	 *
	 */
	public doApplicantCreditRefsSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditRefId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITREFID);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditRefId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITREFID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditReferenceTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITREFERENCETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditReferenceTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITREFERENCETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfReferenceDesc()
	{
		return (String)getValue(FIELD_DFREFERENCEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceDesc(String value)
	{
		setValue(FIELD_DFREFERENCEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfInstitutionName()
	{
		return (String)getValue(FIELD_DFINSTITUTIONNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfInstitutionName(String value)
	{
		setValue(FIELD_DFINSTITUTIONNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTimeWithReference()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTIMEWITHREFERENCE);
	}


	/**
	 *
	 *
	 */
	public void setDfTimeWithReference(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTIMEWITHREFERENCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAccountNumber()
	{
		return (String)getValue(FIELD_DFACCOUNTNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfAccountNumber(String value)
	{
		setValue(FIELD_DFACCOUNTNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCurrentBalance()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCURRENTBALANCE);
	}


	/**
	 *
	 *
	 */
	public void setDfCurrentBalance(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCURRENTBALANCE,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT CREDITREFERENCE.BORROWERID, " +
            "CREDITREFERENCE.CREDITREFERENCEID, CREDITREFERENCE.COPYID, " +
            "CREDITREFERENCE.CREDITREFTYPEID, CREDITREFERENCE.CREDITREFERENCEDESCRIPTION, " +
            "CREDITREFERENCE.INSTITUTIONNAME, CREDITREFERENCE.TIMEWITHREFERENCE, " +
            "CREDITREFERENCE.ACCOUNTNUMBER, CREDITREFERENCE.CURRENTBALANCE " +
            "FROM CREDITREFERENCE  __WHERE__  " +
            "ORDER BY CREDITREFERENCE.CREDITREFERENCEID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="CREDITREFERENCE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="CREDITREFERENCE.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFCREDITREFID="CREDITREFERENCE.CREDITREFERENCEID";
	public static final String COLUMN_DFCREDITREFID="CREDITREFERENCEID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="CREDITREFERENCE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFCREDITREFERENCETYPEID="CREDITREFERENCE.CREDITREFTYPEID";
	public static final String COLUMN_DFCREDITREFERENCETYPEID="CREDITREFTYPEID";
	public static final String QUALIFIED_COLUMN_DFREFERENCEDESC="CREDITREFERENCE.CREDITREFERENCEDESCRIPTION";
	public static final String COLUMN_DFREFERENCEDESC="CREDITREFERENCEDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFINSTITUTIONNAME="CREDITREFERENCE.INSTITUTIONNAME";
	public static final String COLUMN_DFINSTITUTIONNAME="INSTITUTIONNAME";
	public static final String QUALIFIED_COLUMN_DFTIMEWITHREFERENCE="CREDITREFERENCE.TIMEWITHREFERENCE";
	public static final String COLUMN_DFTIMEWITHREFERENCE="TIMEWITHREFERENCE";
	public static final String QUALIFIED_COLUMN_DFACCOUNTNUMBER="CREDITREFERENCE.ACCOUNTNUMBER";
	public static final String COLUMN_DFACCOUNTNUMBER="ACCOUNTNUMBER";
	public static final String QUALIFIED_COLUMN_DFCURRENTBALANCE="CREDITREFERENCE.CURRENTBALANCE";
	public static final String COLUMN_DFCURRENTBALANCE="CURRENTBALANCE";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITREFID,
				COLUMN_DFCREDITREFID,
				QUALIFIED_COLUMN_DFCREDITREFID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITREFERENCETYPEID,
				COLUMN_DFCREDITREFERENCETYPEID,
				QUALIFIED_COLUMN_DFCREDITREFERENCETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCEDESC,
				COLUMN_DFREFERENCEDESC,
				QUALIFIED_COLUMN_DFREFERENCEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINSTITUTIONNAME,
				COLUMN_DFINSTITUTIONNAME,
				QUALIFIED_COLUMN_DFINSTITUTIONNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTIMEWITHREFERENCE,
				COLUMN_DFTIMEWITHREFERENCE,
				QUALIFIED_COLUMN_DFTIMEWITHREFERENCE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACCOUNTNUMBER,
				COLUMN_DFACCOUNTNUMBER,
				QUALIFIED_COLUMN_DFACCOUNTNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCURRENTBALANCE,
				COLUMN_DFCURRENTBALANCE,
				QUALIFIED_COLUMN_DFCURRENTBALANCE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

