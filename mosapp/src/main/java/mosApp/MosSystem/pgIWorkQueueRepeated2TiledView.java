package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgIWorkQueueRepeated2TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgIWorkQueueRepeated2TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doPAWorkQueueModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStBPSource().setValue(CHILD_STBPSOURCE_RESET_VALUE);
		getStBPSourceFirm().setValue(CHILD_STBPSOURCEFIRM_RESET_VALUE);
		getStBPBorrower().setValue(CHILD_STBPBORROWER_RESET_VALUE);
		getHdBPDealId().setValue(CHILD_HDBPDEALID_RESET_VALUE);
		getStBPDealNumber().setValue(CHILD_STBPDEALNUMBER_RESET_VALUE);
		getHdBPWqID().setValue(CHILD_HDBPWQID_RESET_VALUE);
		getHdBPTaskId().setValue(CHILD_HDBPTASKID_RESET_VALUE);
		getStBPTaskDescription().setValue(CHILD_STBPTASKDESCRIPTION_RESET_VALUE);
		getStBPTaskStatus().setValue(CHILD_STBPTASKSTATUS_RESET_VALUE);
		getStBPTaskPriority().setValue(CHILD_STBPTASKPRIORITY_RESET_VALUE);
		getStBPTaskDueDate().setValue(CHILD_STBPTASKDUEDATE_RESET_VALUE);
		getStBPTimeStatus().setValue(CHILD_STBPTIMESTATUS_RESET_VALUE);
		getStBPTaskWarning().setValue(CHILD_STBPTASKWARNING_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STBPSOURCE,StaticTextField.class);
		registerChild(CHILD_STBPSOURCEFIRM,StaticTextField.class);
		registerChild(CHILD_STBPBORROWER,StaticTextField.class);
		registerChild(CHILD_HDBPDEALID,HiddenField.class);
		registerChild(CHILD_STBPDEALNUMBER,StaticTextField.class);
		registerChild(CHILD_HDBPWQID,HiddenField.class);
		registerChild(CHILD_HDBPTASKID,HiddenField.class);
		registerChild(CHILD_STBPTASKDESCRIPTION,StaticTextField.class);
		registerChild(CHILD_STBPTASKSTATUS,StaticTextField.class);
		registerChild(CHILD_STBPTASKPRIORITY,StaticTextField.class);
		registerChild(CHILD_STBPTASKDUEDATE,StaticTextField.class);
		registerChild(CHILD_STBPTIMESTATUS,StaticTextField.class);
		registerChild(CHILD_STBPTASKWARNING,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoPAWorkQueueModel());;
				break;

			case MODEL_TYPE_UPDATE:
			  break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
        ;
			  break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
    //--> The following was moved to the AfterAllModelExecuted Event.
    //--> The reason is JATO framework calling this method before executing
    //--> Models.  Therefore the handleClearPriorityAlertTasks will clear all
    //--> unknowledge flags ==> no row displayed on the list.
    //--> By Billy 23July2002
    /*
    logger = SysLog.getSysLogger("IWQR2TW");
    logger.debug("IWQR2TW@beginDisplay :: Just in !!");

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();

		handler.pageGetState(this.getParentViewBean());

		handler.handleClearPriorityAlertTasks();

		handler.pageSaveState();
    */
    if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
    //logger = SysLog.getSysLogger("IWQR2TW");

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
      // Put migrated repeated_onBeforeRowDisplayEvent code here
      int rowNum = this.getTileIndex();
      //logger.debug("IWQR2TW@nextTile::rowNum: " + rowNum);

      iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();

      handler.pageGetState(this.getParentViewBean());

      handler.populatedRepeatedFields(2, rowNum);

      handler.pageSaveState();
    }

		return movedToRow;


		// The following code block was migrated from the Repeated2_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatedRepeatedFields(1, event.getRowIndex());

		handler.pageSaveState();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

    //logger = SysLog.getSysLogger("IWQR2TW");
    //logger.debug("IWQR2TW@afterAllModelsExecute :: Start !!");

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.handleClearPriorityAlertTasks();
		handler.pageSaveState();
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STBPSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPAWorkQueueModel(),
				CHILD_STBPSOURCE,
				doPAWorkQueueModel.FIELD_DFCONTACTLASTNAME,
				CHILD_STBPSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPSOURCEFIRM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPAWorkQueueModel(),
				CHILD_STBPSOURCEFIRM,
				doPAWorkQueueModel.FIELD_DFSFSHORTNAME,
				CHILD_STBPSOURCEFIRM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPBORROWER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBPBORROWER,
				CHILD_STBPBORROWER,
				CHILD_STBPBORROWER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDBPDEALID))
		{
			HiddenField child = new HiddenField(this,
				getdoPAWorkQueueModel(),
				CHILD_HDBPDEALID,
				doPAWorkQueueModel.FIELD_DFDEALID,
				CHILD_HDBPDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPDEALNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPAWorkQueueModel(),
				CHILD_STBPDEALNUMBER,
				doPAWorkQueueModel.FIELD_DFDEALID,
				CHILD_STBPDEALNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDBPWQID))
		{
			HiddenField child = new HiddenField(this,
				getdoPAWorkQueueModel(),
				CHILD_HDBPWQID,
				doPAWorkQueueModel.FIELD_DFASSIGNEDTASKSWORKQUEUEID,
				CHILD_HDBPWQID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDBPTASKID))
		{
			HiddenField child = new HiddenField(this,
				getdoPAWorkQueueModel(),
				CHILD_HDBPTASKID,
				doPAWorkQueueModel.FIELD_DFTASKID,
				CHILD_HDBPTASKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPTASKDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPAWorkQueueModel(),
				CHILD_STBPTASKDESCRIPTION,
				doPAWorkQueueModel.FIELD_DFTASK_TASKLABEL,
				CHILD_STBPTASKDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPTASKSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPAWorkQueueModel(),
				CHILD_STBPTASKSTATUS,
				doPAWorkQueueModel.FIELD_DFTASKSTATUS_TSDESCRIPTION,
				CHILD_STBPTASKSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPTASKPRIORITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPAWorkQueueModel(),
				CHILD_STBPTASKPRIORITY,
				doPAWorkQueueModel.FIELD_DFPRIORITY_PDESCRIPTION,
				CHILD_STBPTASKPRIORITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPTASKDUEDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPAWorkQueueModel(),
				CHILD_STBPTASKDUEDATE,
				doPAWorkQueueModel.FIELD_DFDUETIMESTAMP,
				CHILD_STBPTASKDUEDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPTIMESTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPAWorkQueueModel(),
				CHILD_STBPTIMESTATUS,
				doPAWorkQueueModel.FIELD_DFTMDESCRIPTION,
				CHILD_STBPTIMESTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBPTASKWARNING))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBPTASKWARNING,
				CHILD_STBPTASKWARNING,
				CHILD_STBPTASKWARNING_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPSource()
	{
		return (StaticTextField)getChild(CHILD_STBPSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPSourceFirm()
	{
		return (StaticTextField)getChild(CHILD_STBPSOURCEFIRM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPBorrower()
	{
		return (StaticTextField)getChild(CHILD_STBPBORROWER);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdBPDealId()
	{
		return (HiddenField)getChild(CHILD_HDBPDEALID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPDealNumber()
	{
		return (StaticTextField)getChild(CHILD_STBPDEALNUMBER);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdBPWqID()
	{
		return (HiddenField)getChild(CHILD_HDBPWQID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdBPTaskId()
	{
		return (HiddenField)getChild(CHILD_HDBPTASKID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPTaskDescription()
	{
		return (StaticTextField)getChild(CHILD_STBPTASKDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPTaskStatus()
	{
		return (StaticTextField)getChild(CHILD_STBPTASKSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPTaskPriority()
	{
		return (StaticTextField)getChild(CHILD_STBPTASKPRIORITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPTaskDueDate()
	{
		return (StaticTextField)getChild(CHILD_STBPTASKDUEDATE);
	}


	/**
	 *
	 *
	 */
	public boolean beginStBPTaskDueDateDisplay(ChildDisplayEvent event)
	{
		//Object value = getStBPTaskDueDate().getValue();
		// The following code block was migrated from the stBPTaskDueDate_onBeforeDisplayEvent method
		iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.convertToUserTimeZone(this.getParentViewBean(), "Repeated2/stBPTaskDueDate");
		handler.pageSaveState();

    return true;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPTimeStatus()
	{
		return (StaticTextField)getChild(CHILD_STBPTIMESTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBPTaskWarning()
	{
		return (StaticTextField)getChild(CHILD_STBPTASKWARNING);
	}


	/**
	 *
	 *
	 */
	public doPAWorkQueueModel getdoPAWorkQueueModel()
	{
		if (doPAWorkQueue == null)
			doPAWorkQueue = (doPAWorkQueueModel) getModel(doPAWorkQueueModel.class);
		return doPAWorkQueue;
	}


	/**
	 *
	 *
	 */
	public void setdoPAWorkQueueModel(doPAWorkQueueModel model)
	{
			doPAWorkQueue = model;
	}


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////




	/* MigrationToDo : Repeated2_onBeforeHtmlOutputEvent is OBSOLETE.
	   There is no J2EE analog--manual migration required.
	public int Repeated2_onBeforeHtmlOutputEvent(CSpHtmlOutputEvent event)
	{
		iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.handleClearPriorityAlertTasks();

		handler.pageSaveState();

		return;

	}
	 */




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STBPSOURCE="stBPSource";
	public static final String CHILD_STBPSOURCE_RESET_VALUE="";
	public static final String CHILD_STBPSOURCEFIRM="stBPSourceFirm";
	public static final String CHILD_STBPSOURCEFIRM_RESET_VALUE="";
	public static final String CHILD_STBPBORROWER="stBPBorrower";
	public static final String CHILD_STBPBORROWER_RESET_VALUE="";
	public static final String CHILD_HDBPDEALID="hdBPDealId";
	public static final String CHILD_HDBPDEALID_RESET_VALUE="";
	public static final String CHILD_STBPDEALNUMBER="stBPDealNumber";
	public static final String CHILD_STBPDEALNUMBER_RESET_VALUE="";
	public static final String CHILD_HDBPWQID="hdBPWqID";
	public static final String CHILD_HDBPWQID_RESET_VALUE="";
	public static final String CHILD_HDBPTASKID="hdBPTaskId";
	public static final String CHILD_HDBPTASKID_RESET_VALUE="";
	public static final String CHILD_STBPTASKDESCRIPTION="stBPTaskDescription";
	public static final String CHILD_STBPTASKDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_STBPTASKSTATUS="stBPTaskStatus";
	public static final String CHILD_STBPTASKSTATUS_RESET_VALUE="";
	public static final String CHILD_STBPTASKPRIORITY="stBPTaskPriority";
	public static final String CHILD_STBPTASKPRIORITY_RESET_VALUE="";
	public static final String CHILD_STBPTASKDUEDATE="stBPTaskDueDate";
	public static final String CHILD_STBPTASKDUEDATE_RESET_VALUE="";
	public static final String CHILD_STBPTIMESTATUS="stBPTimeStatus";
	public static final String CHILD_STBPTIMESTATUS_RESET_VALUE="";
	public static final String CHILD_STBPTASKWARNING="stBPTaskWarning";
	public static final String CHILD_STBPTASKWARNING_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private iWorkQueueHandler handler=new iWorkQueueHandler();

	private doPAWorkQueueModel doPAWorkQueue=null;

  public SysLogger logger;


}

