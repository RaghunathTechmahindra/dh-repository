package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doCreditReviewModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerType();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfExistingClient();

	
	/**
	 * 
	 * 
	 */
	public void setDfExistingClient(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfExistClientComments();

	
	/**
	 * 
	 * 
	 */
	public void setDfExistClientComments(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfGTS();

	
	/**
	 * 
	 * 
	 */
	public void setDfGTS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfGTS3Year();

	
	/**
	 * 
	 * 
	 */
	public void setDfGTS3Year(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTDS3Year();

	
	/**
	 * 
	 * 
	 */
	public void setDfTDS3Year(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNetWorth();

	
	/**
	 * 
	 * 
	 */
	public void setDfNetWorth(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBureauNameId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBureauNameId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfBureauOnfile();

	
	/**
	 * 
	 * 
	 */
	public void setDfBureauOnfile(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfBureauProfile();

	
	/**
	 * 
	 * 
	 */
	public void setDfBureauProfile(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfAuthorization();

	
	/**
	 * 
	 * 
	 */
	public void setDfAuthorization(java.sql.Timestamp value);
	
	/**
	 * 
	 * 
	 */
	public String getDfAuthorizationMethod();

	
	/**
	 * 
	 * 
	 */
	public void setDfAuthorizationMethod(String value);
	
	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfSuffixId();

	
	/**
	 * 
	 * 
	 */
	public void setDfSuffixId(java.math.BigDecimal value);
	
	/**
	 * 
	 * 
	 */
	public String getDfBureauSummary();

	
	/**
	 * 
	 * 
	 */
	public void setDfBureauSummary(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCreditScore();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditScore(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTimesBankrupt();

	
	/**
	 * 
	 * 
	 */
	public void setDfTimesBankrupt(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBankruptStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBankruptStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getNSFOccurenceStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setNSFOccurenceStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPaymentHistoryTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPaymentHistoryTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerGeneralStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerGeneralStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryBorrowerFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryBorrowerFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCreditBureauSummary();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditBureauSummary(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFBORROWERNAME="dfBorrowerName";
	public static final String FIELD_DFBORROWERTYPE="dfBorrowerType";
	public static final String FIELD_DFEXISTINGCLIENT="dfExistingClient";
	public static final String FIELD_DFEXISTCLIENTCOMMENTS="dfExistClientComments";
	public static final String FIELD_DFGTS="dfGTS";
	public static final String FIELD_DFGTS3YEAR="dfGTS3Year";
	public static final String FIELD_DFTDS="dfTDS";
	public static final String FIELD_DFTDS3YEAR="dfTDS3Year";
	public static final String FIELD_DFNETWORTH="dfNetWorth";
	public static final String FIELD_DFBUREAUNAMEID="dfBureauNameId";
	public static final String FIELD_DFBUREAUONFILE="dfBureauOnfile";
	public static final String FIELD_DFBUREAUPROFILE="dfBureauProfile";
	public static final String FIELD_DFBUREAUSUMMARY="dfBureauSummary";
	public static final String FIELD_DFCREDITSCORE="dfCreditScore";
	public static final String FIELD_DFTIMESBANKRUPT="dfTimesBankrupt";
	public static final String FIELD_DFBANKRUPTSTATUSID="dfBankruptStatusId";
	public static final String FIELD_NSFOCCURENCESTATUSID="NSFOccurenceStatusId";
	public static final String FIELD_DFPAYMENTHISTORYTYPEID="dfPaymentHistoryTypeId";
	public static final String FIELD_DFBORROWERGENERALSTATUSID="dfBorrowerGeneralStatusId";
	public static final String FIELD_DFPRIMARYBORROWERFLAG="dfPrimaryBorrowerFlag";
	public static final String FIELD_DFCREDITBUREAUSUMMARY="dfCreditBureauSummary";
	public static final String FIELD_DFAUTHORIZATION="dfAuthorization";
	public static final String FIELD_DFAUTHORIZATIONMETHOD="dfAuthorizationMethod";
	public static final String FIELD_DFSUFFIXID="dfSuffixId";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

