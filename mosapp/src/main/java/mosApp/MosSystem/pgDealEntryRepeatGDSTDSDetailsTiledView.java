package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealEntryRepeatGDSTDSDetailsTiledView extends RequestHandlingTiledViewBase
  implements TiledView, RequestHandler
{
  /**
   *
   *
   */
  public pgDealEntryRepeatGDSTDSDetailsTiledView(View parent, String name)
  {
    super(parent, name);
    setMaxDisplayTiles(10);
    setPrimaryModelClass( doDealEntryGDSTDSAppDetailsModel.class );
    registerChildren();
    initialize();
  }


  /**
   *
   *
   */
  protected void initialize()
  {
  }


  /**
   *
   *
   */
  public void resetChildren()
  {
    getHdGDSTDSApplicantId().setValue(CHILD_HDGDSTDSAPPLICANTID_RESET_VALUE);
    getHdGDSTDSApplicantCopyId().setValue(CHILD_HDGDSTDSAPPLICANTCOPYID_RESET_VALUE);
    getStGDSTDSFirstName().setValue(CHILD_STGDSTDSFIRSTNAME_RESET_VALUE);
    getStGDSTDSMiddleInitial().setValue(CHILD_STGDSTDSMIDDLEINITIAL_RESET_VALUE);
    getStGDSTDSLastName().setValue(CHILD_STGDSTDSLASTNAME_RESET_VALUE);
    getStGDSTDSSuffix().setValue(CHILD_STGDSTDSSUFFIX_RESET_VALUE);
    getStGDSApplicantType().setValue(CHILD_STGDSAPPLICANTTYPE_RESET_VALUE);
    getStApplicantGDS().setValue(CHILD_STAPPLICANTGDS_RESET_VALUE);
    getStApplicant3YrGDS().setValue(CHILD_STAPPLICANT3YRGDS_RESET_VALUE);
    getStApplicantTDS().setValue(CHILD_STAPPLICANTTDS_RESET_VALUE);
    getStApplicant3YrTDS().setValue(CHILD_STAPPLICANT3YRTDS_RESET_VALUE);
    //--DJ_LDI_CR--start--//
    getStInsProportionsDesc().setValue(CHILD_STINSURANCEPROPORTIONS_RESET_VALUE);
    getStLifeCoverage().setValue(CHILD_STLIFECOVERAGE_RESET_VALUE);
    getStDisabilityCoverage().setValue(CHILD_STDISABILITYCOVERAGE_RESET_VALUE);
    ////getStLifeDisabilityPremium().setValue(CHILD_STLIFEDISABILITYPREMIUM,StaticTextField.class);
    getStIncludeLifeDisContentStart().setValue(CHILD_STINCLUDELIFEDISCONTENTSTART_RESET_VALUE);
    getStIncludeLifeDisContentEnd().setValue(CHILD_STINCLUDELIFEDISCONTENTEND_RESET_VALUE);
    //--DJ_LDI_CR--end--//
    //--DJ_CR201.2--start//
    getStGuarantorOtherLoans().setValue(CHILD_STGUARANTOROTHERLOANS_RESET_VALUE);
    //--DJ_CR201.2--end//
  }


  /**
   *
   *
   */
  protected void registerChildren()
  {
    registerChild(CHILD_HDGDSTDSAPPLICANTID,HiddenField.class);
    registerChild(CHILD_HDGDSTDSAPPLICANTCOPYID,HiddenField.class);
    registerChild(CHILD_STGDSTDSFIRSTNAME,StaticTextField.class);
    registerChild(CHILD_STGDSTDSMIDDLEINITIAL,StaticTextField.class);
    registerChild(CHILD_STGDSTDSLASTNAME,StaticTextField.class);
    registerChild(CHILD_STGDSTDSSUFFIX,StaticTextField.class);
    registerChild(CHILD_STGDSAPPLICANTTYPE,StaticTextField.class);
    registerChild(CHILD_STAPPLICANTGDS,StaticTextField.class);
    registerChild(CHILD_STAPPLICANT3YRGDS,StaticTextField.class);
    registerChild(CHILD_STAPPLICANTTDS,StaticTextField.class);
    registerChild(CHILD_STAPPLICANT3YRTDS,StaticTextField.class);
    //--DJ_LDI_CR--start--//
    registerChild(CHILD_STINSURANCEPROPORTIONS,StaticTextField.class);
    registerChild(CHILD_STLIFECOVERAGE,StaticTextField.class);
    registerChild(CHILD_STDISABILITYCOVERAGE,StaticTextField.class);
    ////registerChild(CHILD_STLIFEDISABILITYPREMIUM,StaticTextField.class);
    registerChild(CHILD_STINCLUDELIFEDISCONTENTSTART,StaticTextField.class);
    registerChild(CHILD_STINCLUDELIFEDISCONTENTEND,StaticTextField.class);
    //--DJ_LDI_CR--end--//
    //--DJ_CR201.2--start//
    registerChild(CHILD_STGUARANTOROTHERLOANS,StaticTextField.class);
    //--DJ_CR201.2--end//
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList=new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        modelList.add(getdoDealEntryGDSTDSAppDetailsModel());;
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return (Model[])modelList.toArray(new Model[0]);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event)
    throws ModelControlException
  {

    // This is the analog of NetDynamics repeated_onBeforeDisplayEvent
    // Ensure the primary model is non-null; if null, it would cause havoc
    // with the various methods dependent on the primary model
    if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    super.beginDisplay(event);
    resetTileIndex();
  }


  /**
   *
   *
   */
  public boolean nextTile()
    throws ModelControlException
  {

    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
    boolean movedToRow = super.nextTile();

    if (movedToRow)
    {
      // Put migrated repeated_onBeforeRowDisplayEvent code here
    }

    return movedToRow;

  }


  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {

    // This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);

  }


  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {

    // This is the analog of NetDynamics repeated_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }


  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected View createChild(String name)
  {
    if (name.equals(CHILD_HDGDSTDSAPPLICANTID))
    {
      HiddenField child = new HiddenField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_HDGDSTDSAPPLICANTID,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFBORROWERID,
        CHILD_HDGDSTDSAPPLICANTID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_HDGDSTDSAPPLICANTCOPYID))
    {
      HiddenField child = new HiddenField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_HDGDSTDSAPPLICANTCOPYID,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFCOPYID,
        CHILD_HDGDSTDSAPPLICANTCOPYID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STGDSTDSFIRSTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STGDSTDSFIRSTNAME,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFFIRSTNAME,
        CHILD_STGDSTDSFIRSTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STGDSTDSMIDDLEINITIAL))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STGDSTDSMIDDLEINITIAL,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFMIDDLEINITIAL,
        CHILD_STGDSTDSMIDDLEINITIAL_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STGDSTDSLASTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STGDSTDSLASTNAME,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFLASTNAME,
        CHILD_STGDSTDSLASTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STGDSTDSSUFFIX))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STGDSTDSSUFFIX,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFSUFFIX,
        CHILD_STGDSTDSSUFFIX_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STGDSAPPLICANTTYPE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STGDSAPPLICANTTYPE,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFBORROWERTYPE,
        CHILD_STGDSAPPLICANTTYPE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAPPLICANTGDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STAPPLICANTGDS,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFBORROWERGDS,
        CHILD_STAPPLICANTGDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAPPLICANT3YRGDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STAPPLICANT3YRGDS,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFBORROWERGDS3YR,
        CHILD_STAPPLICANT3YRGDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAPPLICANTTDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STAPPLICANTTDS,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFBORROWERTDS,
        CHILD_STAPPLICANTTDS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STAPPLICANT3YRTDS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STAPPLICANT3YRTDS,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFBORROWERTDS3YR,
        CHILD_STAPPLICANT3YRTDS_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_LDI_CR--start--//
    else
    if (name.equals(CHILD_STINSURANCEPROPORTIONS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STINSURANCEPROPORTIONS,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFINSURANCEPROPORTIONSID,
        CHILD_STINSURANCEPROPORTIONS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STLIFECOVERAGE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STLIFECOVERAGE,
        getdoDealEntryGDSTDSAppDetailsModel().FIELD_DFLIFESTATUSID,
        CHILD_STLIFECOVERAGE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STDISABILITYCOVERAGE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STDISABILITYCOVERAGE,
        getdoDealEntryGDSTDSAppDetailsModel().FIELD_DFDISABILITYSTATUSID,
        CHILD_STDISABILITYCOVERAGE_RESET_VALUE,
        null);
      return child;
    }
    /**
    else
    if (name.equals(CHILD_STLIFEDISABILITYPREMIUM))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STLIFEDISABILITYPREMIUM,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFCOMBINEDLDPREMIUM,
        CHILD_STLIFEDISABILITYPREMIUM_RESET_VALUE,
        null);
      return child;
    }
    **/
    else
    if (name.equals(CHILD_STINCLUDELIFEDISCONTENTSTART))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDELIFEDISCONTENTSTART,
        CHILD_STINCLUDELIFEDISCONTENTSTART,
        CHILD_STINCLUDELIFEDISCONTENTSTART_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STINCLUDELIFEDISCONTENTEND))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDELIFEDISCONTENTEND,
        CHILD_STINCLUDELIFEDISCONTENTEND,
        CHILD_STINCLUDELIFEDISCONTENTEND_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_LDI_CR--end--//
    //--DJ_CR201.2--start//
    else
    if (name.equals(CHILD_STGUARANTOROTHERLOANS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoDealEntryGDSTDSAppDetailsModel(),
        CHILD_STGUARANTOROTHERLOANS,
        doDealEntryGDSTDSAppDetailsModel.FIELD_DFGUARANTOROTHERLOANS,
        CHILD_STGUARANTOROTHERLOANS_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_CR201.2--end//
    else
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
  }


  /**
   *
   *
   */
  public HiddenField getHdGDSTDSApplicantId()
  {
    return (HiddenField)getChild(CHILD_HDGDSTDSAPPLICANTID);
  }


  /**
   *
   *
   */
  public HiddenField getHdGDSTDSApplicantCopyId()
  {
    return (HiddenField)getChild(CHILD_HDGDSTDSAPPLICANTCOPYID);
  }


  /**
   *
   *
   */
  public StaticTextField getStGDSTDSFirstName()
  {
    return (StaticTextField)getChild(CHILD_STGDSTDSFIRSTNAME);
  }


  /**
   *
   *
   */
  public StaticTextField getStGDSTDSMiddleInitial()
  {
    return (StaticTextField)getChild(CHILD_STGDSTDSMIDDLEINITIAL);
  }


  /**
   *
   *
   */
  public StaticTextField getStGDSTDSLastName()
  {
    return (StaticTextField)getChild(CHILD_STGDSTDSLASTNAME);
  }
  

  /**
   *
   *
   */
  public StaticTextField getStGDSTDSSuffix()
  {
    return (StaticTextField)getChild(CHILD_STGDSTDSSUFFIX);
  }



  /**
   *
   *
   */
  public StaticTextField getStGDSApplicantType()
  {
    return (StaticTextField)getChild(CHILD_STGDSAPPLICANTTYPE);
  }


  /**
   *
   *
   */
  public StaticTextField getStApplicantGDS()
  {
    return (StaticTextField)getChild(CHILD_STAPPLICANTGDS);
  }


  /**
   *
   *
   */
  public StaticTextField getStApplicant3YrGDS()
  {
    return (StaticTextField)getChild(CHILD_STAPPLICANT3YRGDS);
  }


  /**
   *
   *
   */
  public StaticTextField getStApplicantTDS()
  {
    return (StaticTextField)getChild(CHILD_STAPPLICANTTDS);
  }


  /**
   *
   *
   */
  public StaticTextField getStApplicant3YrTDS()
  {
    return (StaticTextField)getChild(CHILD_STAPPLICANT3YRTDS);
  }

  //--DJ_LDI_CR--start--//
  /**
   *
   *
   */
  public StaticTextField getStInsProportionsDesc()
  {
    return (StaticTextField)getChild(CHILD_STINSURANCEPROPORTIONS);
  }

  /**
   *
   *
   */
  public StaticTextField getStLifeCoverage()
  {
    return (StaticTextField)getChild(CHILD_STLIFECOVERAGE);
  }

  /**
   *
   *
   */
  public StaticTextField getStDisabilityCoverage()
  {
    return (StaticTextField)getChild(CHILD_STDISABILITYCOVERAGE);
  }

  /**
   *
   *
   */
  /**
  public StaticTextField getStLifeDisabilityPremium()
  {
    return (StaticTextField)getChild(CHILD_STLIFEDISABILITYPREMIUM);
  }
  **/

  /**
   *
   *
   */
  public StaticTextField getStIncludeLifeDisContentStart()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDELIFEDISCONTENTSTART);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludeLifeDisContentEnd()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDELIFEDISCONTENTEND);
  }
  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
  /**
   *
   *
   */
  public StaticTextField getStGuarantorOtherLoans()
  {
    return (StaticTextField)getChild(CHILD_STGUARANTOROTHERLOANS);
  }

  //--> Bug fix to support Bilingual : By Billy 19May2004
  public String endStGuarantorOtherLoansDisplay(ChildContentDisplayEvent event)
  {
    DealEntryHandler handler = (DealEntryHandler)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    String rc = handler.setYesOrNo(event.getContent());
    handler.pageSaveState();

    return rc;
  }

  //========================================================

  //--DJ_CR201.2--end//

  /**
   *
   *
   */
  public doDealEntryGDSTDSAppDetailsModel getdoDealEntryGDSTDSAppDetailsModel()
  {
    if (doDealEntryGDSTDSAppDetails == null)
      doDealEntryGDSTDSAppDetails = (doDealEntryGDSTDSAppDetailsModel) getModel(doDealEntryGDSTDSAppDetailsModel.class);
    return doDealEntryGDSTDSAppDetails;
  }


  /**
   *
   *
   */
  public void setdoDealEntryGDSTDSAppDetailsModel(doDealEntryGDSTDSAppDetailsModel model)
  {
      doDealEntryGDSTDSAppDetails = model;
  }




  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Child accessors
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Child rendering methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Repeated event methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_HDGDSTDSAPPLICANTID="hdGDSTDSApplicantId";
  public static final String CHILD_HDGDSTDSAPPLICANTID_RESET_VALUE="";
  public static final String CHILD_HDGDSTDSAPPLICANTCOPYID="hdGDSTDSApplicantCopyId";
  public static final String CHILD_HDGDSTDSAPPLICANTCOPYID_RESET_VALUE="";
  public static final String CHILD_STGDSTDSFIRSTNAME="stGDSTDSFirstName";
  public static final String CHILD_STGDSTDSFIRSTNAME_RESET_VALUE="";
  public static final String CHILD_STGDSTDSMIDDLEINITIAL="stGDSTDSMiddleInitial";
  public static final String CHILD_STGDSTDSMIDDLEINITIAL_RESET_VALUE="";
  public static final String CHILD_STGDSTDSLASTNAME="stGDSTDSLastName";
  public static final String CHILD_STGDSTDSLASTNAME_RESET_VALUE="";
  public static final String CHILD_STGDSTDSSUFFIX="stGDSTDSSuffix";
  public static final String CHILD_STGDSTDSSUFFIX_RESET_VALUE="";
  public static final String CHILD_STGDSAPPLICANTTYPE="stGDSApplicantType";
  public static final String CHILD_STGDSAPPLICANTTYPE_RESET_VALUE="";
  public static final String CHILD_STAPPLICANTGDS="stApplicantGDS";
  public static final String CHILD_STAPPLICANTGDS_RESET_VALUE="";
  public static final String CHILD_STAPPLICANT3YRGDS="stApplicant3YrGDS";
  public static final String CHILD_STAPPLICANT3YRGDS_RESET_VALUE="";
  public static final String CHILD_STAPPLICANTTDS="stApplicantTDS";
  public static final String CHILD_STAPPLICANTTDS_RESET_VALUE="";
  public static final String CHILD_STAPPLICANT3YRTDS="stApplicant3YrTDS";
  public static final String CHILD_STAPPLICANT3YRTDS_RESET_VALUE="";

  //--DJ_LDI_CR--start--//
  public static final String CHILD_STINSURANCEPROPORTIONS="stInsProportions";
  public static final String CHILD_STINSURANCEPROPORTIONS_RESET_VALUE="";

  public static final String CHILD_STLIFECOVERAGE="stLifeCoverage";
  public static final String CHILD_STLIFECOVERAGE_RESET_VALUE="";

  public static final String CHILD_STDISABILITYCOVERAGE="stDisabilityCoverage";
  public static final String CHILD_STDISABILITYCOVERAGE_RESET_VALUE="";

  ////public static final String CHILD_STLIFEDISABILITYPREMIUM="stLifeDisabilityPremium";
  ////public static final String CHILD_STLIFEDISABILITYPREMIUM_RESET_VALUE="";

  public static final String CHILD_STINCLUDELIFEDISCONTENTSTART="stIncludeLifeDisContentStart";
  public static final String CHILD_STINCLUDELIFEDISCONTENTSTART_RESET_VALUE="";

  public static final String CHILD_STINCLUDELIFEDISCONTENTEND="stIncludeLifeDisContentEnd";
  public static final String CHILD_STINCLUDELIFEDISCONTENTEND_RESET_VALUE="";
  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
  public static final String CHILD_STGUARANTOROTHERLOANS="stGuarantorOtherLoans";
  public static final String CHILD_STGUARANTOROTHERLOANS_RESET_VALUE="";
  //--DJ_CR201.2--end//


  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  private doDealEntryGDSTDSAppDetailsModel doDealEntryGDSTDSAppDetails=null;
  private DealEntryHandler handler=new DealEntryHandler();

}

