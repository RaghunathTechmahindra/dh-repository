package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

public interface doUWProgressAdvanceTypeModel
    extends QueryModel, SelectQueryModel
{
  public java.math.BigDecimal getDfDealId();

  public void setDfDealId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfCopyId();

  public void setDfCopyId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfProgressAdvanceTypeId();

  public void setDfProgressAdvanceTypeId(java.math.BigDecimal value);

  public String getDfProgressAdvanceTypeDesc();

  public void setDfProgressAdvanceTypeDesc(String value);

  // //////////////////////////////////////////////////////////////////////////
  // Class variables
  // //////////////////////////////////////////////////////////////////////////

  public static final String FIELD_DFDEALID = "dfDealId";

  public static final String FIELD_DFCOPYID = "dfCopyId";

  public static final String FIELD_DFPROGRESSADVANCETYPEID = "dfProgressAdvanceTypeId";

  public static final String FIELD_DFPROGRESSADVANCETYPEDESC = "dfProgressAdvanceTypeDesc";

  // //////////////////////////////////////////////////////////////////////////
  // Instance variables
  // //////////////////////////////////////////////////////////////////////////

}
