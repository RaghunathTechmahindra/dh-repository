package mosApp.MosSystem;


import java.util.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedPropertyDetailsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedPropertyDetailsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(1);
		setPrimaryModelClass( doDetailViewPropertiesModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStPDprimaryProperty().setValue(CHILD_STPDPRIMARYPROPERTY_RESET_VALUE);
		getStPDstreetNumber().setValue(CHILD_STPDSTREETNUMBER_RESET_VALUE);
		getStPDStreetName().setValue(CHILD_STPDSTREETNAME_RESET_VALUE);
		getStPDStreetType().setValue(CHILD_STPDSTREETTYPE_RESET_VALUE);
		getStPDUnitNumber().setValue(CHILD_STPDUNITNUMBER_RESET_VALUE);
		getStPDStreetDirection().setValue(CHILD_STPDSTREETDIRECTION_RESET_VALUE);
		getStPDAddressLine2().setValue(CHILD_STPDADDRESSLINE2_RESET_VALUE);
		getStPDcity().setValue(CHILD_STPDCITY_RESET_VALUE);
		getStPDprovince().setValue(CHILD_STPDPROVINCE_RESET_VALUE);
		getStPDpostCode1().setValue(CHILD_STPDPOSTCODE1_RESET_VALUE);
		getStPDpostCode2().setValue(CHILD_STPDPOSTCODE2_RESET_VALUE);
		getStPDlegalLine1().setValue(CHILD_STPDLEGALLINE1_RESET_VALUE);
		getStPDlegalLine2().setValue(CHILD_STPDLEGALLINE2_RESET_VALUE);
		getStPDlegalLine3().setValue(CHILD_STPDLEGALLINE3_RESET_VALUE);
		getStPDwellingType().setValue(CHILD_STPDWELLINGTYPE_RESET_VALUE);
		getStPDwellingStyle().setValue(CHILD_STPDWELLINGSTYLE_RESET_VALUE);
		getStPDBuilderName().setValue(CHILD_STPDBUILDERNAME_RESET_VALUE);
		getStPDGarageSize().setValue(CHILD_STPDGARAGESIZE_RESET_VALUE);
		getStPDGarageType().setValue(CHILD_STPDGARAGETYPE_RESET_VALUE);
		getStPDNewConstructor().setValue(CHILD_STPDNEWCONSTRUCTOR_RESET_VALUE);
		getStPDStructureAge().setValue(CHILD_STPDSTRUCTUREAGE_RESET_VALUE);
		getStPDNoOfUnits().setValue(CHILD_STPDNOOFUNITS_RESET_VALUE);
		getStPDNumberOfBedrooms().setValue(CHILD_STPDNUMBEROFBEDROOMS_RESET_VALUE);
		getStPDLivingSpace().setValue(CHILD_STPDLIVINGSPACE_RESET_VALUE);
		getStPDLivingSpaceUnitOfMeasure().setValue(CHILD_STPDLIVINGSPACEUNITOFMEASURE_RESET_VALUE);
		getStPDLotSizeUnitOfMeasure().setValue(CHILD_STPDLOTSIZEUNITOFMEASURE_RESET_VALUE);
		getStPDlotSize().setValue(CHILD_STPDLOTSIZE_RESET_VALUE);
		getStPDHeatType().setValue(CHILD_STPDHEATTYPE_RESET_VALUE);
		getStPDInsulatedWithUFFI().setValue(CHILD_STPDINSULATEDWITHUFFI_RESET_VALUE);
		getStPDWater().setValue(CHILD_STPDWATER_RESET_VALUE);
		getStPDSewage().setValue(CHILD_STPDSEWAGE_RESET_VALUE);
		getStPDMLSFlag().setValue(CHILD_STPDMLSFLAG_RESET_VALUE);
		getStPDPropertyUsage().setValue(CHILD_STPDPROPERTYUSAGE_RESET_VALUE);
		getStPDOccupancyType().setValue(CHILD_STPDOCCUPANCYTYPE_RESET_VALUE);
		getStPDPropertyType().setValue(CHILD_STPDPROPERTYTYPE_RESET_VALUE);
		getStPDropertyLocation().setValue(CHILD_STPDROPERTYLOCATION_RESET_VALUE);
		getStPDZoning().setValue(CHILD_STPDZONING_RESET_VALUE);
		getStPDPurchasePrice().setValue(CHILD_STPDPURCHASEPRICE_RESET_VALUE);
		getStPDLandValue().setValue(CHILD_STPDLANDVALUE_RESET_VALUE);
		getStPDEquityAvailable().setValue(CHILD_STPDEQUITYAVAILABLE_RESET_VALUE);
		getStPDEstimatedAppraisalValue().setValue(CHILD_STPDESTIMATEDAPPRAISALVALUE_RESET_VALUE);
		getStPDActualAppraisalValue().setValue(CHILD_STPDACTUALAPPRAISALVALUE_RESET_VALUE);
		getStPDAppraisalDate().setValue(CHILD_STPDAPPRAISALDATE_RESET_VALUE);
		getStPDAppraisalSource().setValue(CHILD_STPDAPPRAISALSOURCE_RESET_VALUE);
		getStPDAppraiserFistName().setValue(CHILD_STPDAPPRAISERFISTNAME_RESET_VALUE);
		getStPDAppraiserAddress().setValue(CHILD_STPDAPPRAISERADDRESS_RESET_VALUE);
		getStPDAppraisalPhone().setValue(CHILD_STPDAPPRAISALPHONE_RESET_VALUE);
		getStPDAppraiserFax().setValue(CHILD_STPDAPPRAISERFAX_RESET_VALUE);
		getStPDAppPhoneExt().setValue(CHILD_STPDAPPPHONEEXT_RESET_VALUE);
		getStPDAppraiserInitial().setValue(CHILD_STPDAPPRAISERINITIAL_RESET_VALUE);
		getStPDAppraiserLastName().setValue(CHILD_STPDAPPRAISERLASTNAME_RESET_VALUE);

    //--Release3.1--begins
    //--by Hiro Apr 7, 2006
    getStTenure().setValue(CHILD_STTENURE_RESET_VALUE);
    getStMiEnergyEfficiency().setValue(CHILD_STMIENERGYEFFICIENCY_RESET_VALUE);
    getStSubdivisionDiscount().setValue(CHILD_STSUBDIVISIONDISCOUNT_RESET_VALUE);
    getStOnReserveTrustAgreement().setValue(CHILD_STONRESERVETRUSTAGREEMENT_RESET_VALUE);
    //--Release3.1--ends
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STPDPRIMARYPROPERTY,StaticTextField.class);
		registerChild(CHILD_STPDSTREETNUMBER,StaticTextField.class);
		registerChild(CHILD_STPDSTREETNAME,StaticTextField.class);
		registerChild(CHILD_STPDSTREETTYPE,StaticTextField.class);
		registerChild(CHILD_STPDSTREETDIRECTION,StaticTextField.class);
		registerChild(CHILD_STPDUNITNUMBER,StaticTextField.class);
		registerChild(CHILD_STPDADDRESSLINE2,StaticTextField.class);
		registerChild(CHILD_STPDCITY,StaticTextField.class);
		registerChild(CHILD_STPDPROVINCE,StaticTextField.class);
		registerChild(CHILD_STPDPOSTCODE1,StaticTextField.class);
		registerChild(CHILD_STPDPOSTCODE2,StaticTextField.class);
		registerChild(CHILD_STPDLEGALLINE1,StaticTextField.class);
		registerChild(CHILD_STPDLEGALLINE2,StaticTextField.class);
		registerChild(CHILD_STPDLEGALLINE3,StaticTextField.class);
		registerChild(CHILD_STPDWELLINGTYPE,StaticTextField.class);
		registerChild(CHILD_STPDWELLINGSTYLE,StaticTextField.class);
		registerChild(CHILD_STPDBUILDERNAME,StaticTextField.class);
		registerChild(CHILD_STPDGARAGESIZE,StaticTextField.class);
		registerChild(CHILD_STPDGARAGETYPE,StaticTextField.class);
		registerChild(CHILD_STPDNEWCONSTRUCTOR,StaticTextField.class);
		registerChild(CHILD_STPDSTRUCTUREAGE,StaticTextField.class);
		registerChild(CHILD_STPDNOOFUNITS,StaticTextField.class);
		registerChild(CHILD_STPDNUMBEROFBEDROOMS,StaticTextField.class);
		registerChild(CHILD_STPDLIVINGSPACE,StaticTextField.class);
		registerChild(CHILD_STPDLIVINGSPACEUNITOFMEASURE,StaticTextField.class);
		registerChild(CHILD_STPDLOTSIZEUNITOFMEASURE,StaticTextField.class);
		registerChild(CHILD_STPDLOTSIZE,StaticTextField.class);
		registerChild(CHILD_STPDHEATTYPE,StaticTextField.class);
		registerChild(CHILD_STPDINSULATEDWITHUFFI,StaticTextField.class);
		registerChild(CHILD_STPDWATER,StaticTextField.class);
		registerChild(CHILD_STPDSEWAGE,StaticTextField.class);
		registerChild(CHILD_STPDMLSFLAG,StaticTextField.class);
		registerChild(CHILD_STPDPROPERTYUSAGE,StaticTextField.class);
		registerChild(CHILD_STPDOCCUPANCYTYPE,StaticTextField.class);
		registerChild(CHILD_STPDPROPERTYTYPE,StaticTextField.class);
		registerChild(CHILD_STPDROPERTYLOCATION,StaticTextField.class);
		registerChild(CHILD_STPDZONING,StaticTextField.class);
		registerChild(CHILD_STPDPURCHASEPRICE,StaticTextField.class);
		registerChild(CHILD_STPDLANDVALUE,StaticTextField.class);
		registerChild(CHILD_STPDEQUITYAVAILABLE,StaticTextField.class);
		registerChild(CHILD_STPDESTIMATEDAPPRAISALVALUE,StaticTextField.class);
		registerChild(CHILD_STPDACTUALAPPRAISALVALUE,StaticTextField.class);
		registerChild(CHILD_STPDAPPRAISALDATE,StaticTextField.class);
		registerChild(CHILD_STPDAPPRAISALSOURCE,StaticTextField.class);
		registerChild(CHILD_STPDAPPRAISERFISTNAME,StaticTextField.class);
		registerChild(CHILD_STPDAPPRAISERADDRESS,StaticTextField.class);
		registerChild(CHILD_STPDAPPRAISALPHONE,StaticTextField.class);
		registerChild(CHILD_STPDAPPRAISERFAX,StaticTextField.class);
		registerChild(CHILD_STPDAPPPHONEEXT,StaticTextField.class);
		registerChild(CHILD_STPDAPPRAISERINITIAL,StaticTextField.class);
		registerChild(CHILD_STPDAPPRAISERLASTNAME,StaticTextField.class);
    
    //--Release3.1--begins
    //--by Hiro Apr 7, 2006
    registerChild(CHILD_STTENURE, StaticTextField.class);
    registerChild(CHILD_STMIENERGYEFFICIENCY, StaticTextField.class);
    registerChild(CHILD_STSUBDIVISIONDISCOUNT, StaticTextField.class);
    registerChild(CHILD_STONRESERVETRUSTAGREEMENT, StaticTextField.class);
    //--Release3.1--ends
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewPropertiesModel());;modelList.add(getdoViewAppraiserInfoModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedPropertyDetails_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedPropertyDetails(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the RepeatedPropertyDetails_onBeforeRowDisplayEvent method
		  DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      handler.setPropertyDetails(this, this.getTileIndex());
      handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
    // This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STPDPRIMARYPROPERTY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDPRIMARYPROPERTY,
				doDetailViewPropertiesModel.FIELD_DFPRIMARYPROPERTY,
				CHILD_STPDPRIMARYPROPERTY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDSTREETNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDSTREETNUMBER,
				doDetailViewPropertiesModel.FIELD_DFSTREETNUMBER,
				CHILD_STPDSTREETNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDSTREETNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDSTREETNAME,
				doDetailViewPropertiesModel.FIELD_DFSTREETNAME,
				CHILD_STPDSTREETNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDSTREETTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDSTREETTYPE,
				doDetailViewPropertiesModel.FIELD_DFSTREETTYPE,
				CHILD_STPDSTREETTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDSTREETDIRECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDSTREETDIRECTION,
				doDetailViewPropertiesModel.FIELD_DFSTREETDIRECTION,
				CHILD_STPDSTREETDIRECTION_RESET_VALUE,
				null);
			return child;
		}
		else if (name.equals(CHILD_STPDUNITNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDUNITNUMBER,
				doDetailViewPropertiesModel.FIELD_DFPROPERTYUNITNUMBER,
				CHILD_STPDUNITNUMBER_RESET_VALUE,
				null);
			return child;
		}
		if (name.equals(CHILD_STPDADDRESSLINE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDADDRESSLINE2,
				doDetailViewPropertiesModel.FIELD_DFADDRESSLINE,
				CHILD_STPDADDRESSLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDCITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDCITY,
				doDetailViewPropertiesModel.FIELD_DFCITY,
				CHILD_STPDCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDPROVINCE,
				doDetailViewPropertiesModel.FIELD_DFPROVINCE,
				CHILD_STPDPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDPOSTCODE1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDPOSTCODE1,
				doDetailViewPropertiesModel.FIELD_DFPOSTALFSA,
				CHILD_STPDPOSTCODE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDPOSTCODE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDPOSTCODE2,
				doDetailViewPropertiesModel.FIELD_DFPOSTALLDU,
				CHILD_STPDPOSTCODE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDLEGALLINE1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDLEGALLINE1,
				doDetailViewPropertiesModel.FIELD_DFLEGALLINE1,
				CHILD_STPDLEGALLINE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDLEGALLINE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDLEGALLINE2,
				doDetailViewPropertiesModel.FIELD_DFLEGALLINE2,
				CHILD_STPDLEGALLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDLEGALLINE3))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDLEGALLINE3,
				doDetailViewPropertiesModel.FIELD_DFLEGALLINE3,
				CHILD_STPDLEGALLINE3_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDWELLINGTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDWELLINGTYPE,
				doDetailViewPropertiesModel.FIELD_DFDWELLINGTYPE,
				CHILD_STPDWELLINGTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDWELLINGSTYLE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDWELLINGSTYLE,
				doDetailViewPropertiesModel.FIELD_DFDWELLINGSTYLE,
				CHILD_STPDWELLINGSTYLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDBUILDERNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDBUILDERNAME,
				doDetailViewPropertiesModel.FIELD_DFBUILDERNAME,
				CHILD_STPDBUILDERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDGARAGESIZE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDGARAGESIZE,
				doDetailViewPropertiesModel.FIELD_DFGARAGESIZE,
				CHILD_STPDGARAGESIZE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDGARAGETYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDGARAGETYPE,
				doDetailViewPropertiesModel.FIELD_DFGARAGETYPE,
				CHILD_STPDGARAGETYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDNEWCONSTRUCTOR))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDNEWCONSTRUCTOR,
				doDetailViewPropertiesModel.FIELD_DFNEWCONSTRUCTIONDESC,
				CHILD_STPDNEWCONSTRUCTOR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDSTRUCTUREAGE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDSTRUCTUREAGE,
				doDetailViewPropertiesModel.FIELD_DFSTRUCTUREAGE,
				CHILD_STPDSTRUCTUREAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDNOOFUNITS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDNOOFUNITS,
				doDetailViewPropertiesModel.FIELD_DFNOOFUNITS,
				CHILD_STPDNOOFUNITS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDNUMBEROFBEDROOMS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDNUMBEROFBEDROOMS,
				doDetailViewPropertiesModel.FIELD_DFNUMBEROFBEDROOMS,
				CHILD_STPDNUMBEROFBEDROOMS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDLIVINGSPACE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDLIVINGSPACE,
				doDetailViewPropertiesModel.FIELD_DFLIVINGSPACE,
				CHILD_STPDLIVINGSPACE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDLIVINGSPACEUNITOFMEASURE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPDLIVINGSPACEUNITOFMEASURE,
				CHILD_STPDLIVINGSPACEUNITOFMEASURE,
				CHILD_STPDLIVINGSPACEUNITOFMEASURE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDLOTSIZEUNITOFMEASURE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPDLOTSIZEUNITOFMEASURE,
				CHILD_STPDLOTSIZEUNITOFMEASURE,
				CHILD_STPDLOTSIZEUNITOFMEASURE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDLOTSIZE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDLOTSIZE,
				doDetailViewPropertiesModel.FIELD_DFLOTSIZE,
				CHILD_STPDLOTSIZE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDHEATTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDHEATTYPE,
				doDetailViewPropertiesModel.FIELD_DFHEATTYPE,
				CHILD_STPDHEATTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDINSULATEDWITHUFFI))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDINSULATEDWITHUFFI,
				doDetailViewPropertiesModel.FIELD_DFUFFIINSULATION,
				CHILD_STPDINSULATEDWITHUFFI_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDWATER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDWATER,
				doDetailViewPropertiesModel.FIELD_DFWATER,
				CHILD_STPDWATER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDSEWAGE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDSEWAGE,
				doDetailViewPropertiesModel.FIELD_DFSEWAGE,
				CHILD_STPDSEWAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDMLSFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDMLSFLAG,
				doDetailViewPropertiesModel.FIELD_DFMLSFLAG,
				CHILD_STPDMLSFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDPROPERTYUSAGE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDPROPERTYUSAGE,
				doDetailViewPropertiesModel.FIELD_DFPROPERTYUSAGE,
				CHILD_STPDPROPERTYUSAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDOCCUPANCYTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDOCCUPANCYTYPE,
				doDetailViewPropertiesModel.FIELD_DFOCCUPANCY,
				CHILD_STPDOCCUPANCYTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDPROPERTYTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDPROPERTYTYPE,
				doDetailViewPropertiesModel.FIELD_DFPROPERTYTYPE,
				CHILD_STPDPROPERTYTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDROPERTYLOCATION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDROPERTYLOCATION,
				doDetailViewPropertiesModel.FIELD_DFPROPERTYLOCATION,
				CHILD_STPDROPERTYLOCATION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDZONING))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDZONING,
				doDetailViewPropertiesModel.FIELD_DFZONING,
				CHILD_STPDZONING_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDPURCHASEPRICE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDPURCHASEPRICE,
				doDetailViewPropertiesModel.FIELD_DFPURCHASEPRICE,
				CHILD_STPDPURCHASEPRICE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDLANDVALUE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDLANDVALUE,
				doDetailViewPropertiesModel.FIELD_DFLANDVALUE,
				CHILD_STPDLANDVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDEQUITYAVAILABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDEQUITYAVAILABLE,
				doDetailViewPropertiesModel.FIELD_DFEQUITYAVAILABLE,
				CHILD_STPDEQUITYAVAILABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDESTIMATEDAPPRAISALVALUE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDESTIMATEDAPPRAISALVALUE,
				doDetailViewPropertiesModel.FIELD_DFESTIMATEVALUE,
				CHILD_STPDESTIMATEDAPPRAISALVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDACTUALAPPRAISALVALUE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDACTUALAPPRAISALVALUE,
				doDetailViewPropertiesModel.FIELD_DFACTUALAPPRAISEDVALUE,
				CHILD_STPDACTUALAPPRAISALVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPRAISALDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDAPPRAISALDATE,
				doDetailViewPropertiesModel.FIELD_DFAPPRAISALDATE,
				CHILD_STPDAPPRAISALDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPRAISALSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertiesModel(),
				CHILD_STPDAPPRAISALSOURCE,
				doDetailViewPropertiesModel.FIELD_DFAPPRAISALSOURCE,
				CHILD_STPDAPPRAISALSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPRAISERFISTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoViewAppraiserInfoModel(),
				CHILD_STPDAPPRAISERFISTNAME,
				doViewAppraiserInfoModel.FIELD_DFAPPRAISERFIRSTNAME,
				CHILD_STPDAPPRAISERFISTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPRAISERADDRESS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoViewAppraiserInfoModel(),
				CHILD_STPDAPPRAISERADDRESS,
				doViewAppraiserInfoModel.FIELD_DFAPPRAISERADDR1,
				CHILD_STPDAPPRAISERADDRESS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPRAISALPHONE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoViewAppraiserInfoModel(),
				CHILD_STPDAPPRAISALPHONE,
				doViewAppraiserInfoModel.FIELD_DFAPPRAISERPHONENUMBER,
				CHILD_STPDAPPRAISALPHONE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPRAISERFAX))
		{
			StaticTextField child = new StaticTextField(this,
				getdoViewAppraiserInfoModel(),
				CHILD_STPDAPPRAISERFAX,
				doViewAppraiserInfoModel.FIELD_DFAPPRAISERFAXNUMBER,
				CHILD_STPDAPPRAISERFAX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPPHONEEXT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoViewAppraiserInfoModel(),
				CHILD_STPDAPPPHONEEXT,
				doViewAppraiserInfoModel.FIELD_DFAPPRAISERPHONEEXTENSION,
				CHILD_STPDAPPPHONEEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPRAISERINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoViewAppraiserInfoModel(),
				CHILD_STPDAPPRAISERINITIAL,
				doViewAppraiserInfoModel.FIELD_DFAPPRAISERINITIAL,
				CHILD_STPDAPPRAISERINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPDAPPRAISERLASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoViewAppraiserInfoModel(),
				CHILD_STPDAPPRAISERLASTNAME,
				doViewAppraiserInfoModel.FIELD_DFAPPRAISERLASTNAME,
				CHILD_STPDAPPRAISERLASTNAME_RESET_VALUE,
				null);
			return child;
		}
    
    //--Release3.1--begins
    //--by Hiro Apr 10, 2006
    else if (name.equals(CHILD_STTENURE))
    {
      StaticTextField child = new StaticTextField(this,
          getdoDetailViewPropertiesModel(), 
          CHILD_STTENURE,
          doDetailViewPropertiesModel.FIELD_DFTENURE,
          CHILD_STTENURE_RESET_VALUE, null);
      return child;
    }
    else if (name.equals(CHILD_STMIENERGYEFFICIENCY))
    {
      StaticTextField child = new StaticTextField(this,
          getdoDetailViewPropertiesModel(), 
          CHILD_STMIENERGYEFFICIENCY,
          doDetailViewPropertiesModel.FIELD_DFMIENERGYEFFICIENCY,
          CHILD_STMIENERGYEFFICIENCY_RESET_VALUE, null);
      return child;
    }
    else if (name.equals(CHILD_STSUBDIVISIONDISCOUNT))
    {
      StaticTextField child = new StaticTextField(this,
          getdoDetailViewPropertiesModel(), 
          CHILD_STSUBDIVISIONDISCOUNT,
          doDetailViewPropertiesModel.FIELD_DFSUBDIVISIONDISCOUNT,
          CHILD_STSUBDIVISIONDISCOUNT_RESET_VALUE, null);
      return child;
    }
    else if (name.equals(CHILD_STONRESERVETRUSTAGREEMENT))
    {
      StaticTextField child = new StaticTextField(this,
          getdoDetailViewPropertiesModel(), 
          CHILD_STONRESERVETRUSTAGREEMENT,
          doDetailViewPropertiesModel.FIELD_DFONRESERVETRUSTAGREEMENT,
          CHILD_STONRESERVETRUSTAGREEMENT_RESET_VALUE, null);
      return child;
    }
		//  --Release3.1--ends
		
    else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDprimaryProperty()
	{
		return (StaticTextField)getChild(CHILD_STPDPRIMARYPROPERTY);
	}


	/**
	 *
	 *
	 */
	public String endStPDprimaryPropertyDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stPDprimaryProperty_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDstreetNumber()
	{
		return (StaticTextField)getChild(CHILD_STPDSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDStreetName()
	{
		return (StaticTextField)getChild(CHILD_STPDSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDStreetType()
	{
		return (StaticTextField)getChild(CHILD_STPDSTREETTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDStreetDirection()
	{
		return (StaticTextField)getChild(CHILD_STPDSTREETDIRECTION);
	}
	
	/**
	 *
	 *
	 */
	public StaticTextField getStPDUnitNumber()
	{
		return (StaticTextField)getChild(CHILD_STPDUNITNUMBER);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStPDAddressLine2()
	{
		return (StaticTextField)getChild(CHILD_STPDADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDcity()
	{
		return (StaticTextField)getChild(CHILD_STPDCITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDprovince()
	{
		return (StaticTextField)getChild(CHILD_STPDPROVINCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDpostCode1()
	{
		return (StaticTextField)getChild(CHILD_STPDPOSTCODE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDpostCode2()
	{
		return (StaticTextField)getChild(CHILD_STPDPOSTCODE2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDlegalLine1()
	{
		return (StaticTextField)getChild(CHILD_STPDLEGALLINE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDlegalLine2()
	{
		return (StaticTextField)getChild(CHILD_STPDLEGALLINE2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDlegalLine3()
	{
		return (StaticTextField)getChild(CHILD_STPDLEGALLINE3);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDwellingType()
	{
		return (StaticTextField)getChild(CHILD_STPDWELLINGTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDwellingStyle()
	{
		return (StaticTextField)getChild(CHILD_STPDWELLINGSTYLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDBuilderName()
	{
		return (StaticTextField)getChild(CHILD_STPDBUILDERNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDGarageSize()
	{
		return (StaticTextField)getChild(CHILD_STPDGARAGESIZE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDGarageType()
	{
		return (StaticTextField)getChild(CHILD_STPDGARAGETYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDNewConstructor()
	{
		return (StaticTextField)getChild(CHILD_STPDNEWCONSTRUCTOR);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDStructureAge()
	{
		return (StaticTextField)getChild(CHILD_STPDSTRUCTUREAGE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDNoOfUnits()
	{
		return (StaticTextField)getChild(CHILD_STPDNOOFUNITS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDNumberOfBedrooms()
	{
		return (StaticTextField)getChild(CHILD_STPDNUMBEROFBEDROOMS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDLivingSpace()
	{
		return (StaticTextField)getChild(CHILD_STPDLIVINGSPACE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDLivingSpaceUnitOfMeasure()
	{
		return (StaticTextField)getChild(CHILD_STPDLIVINGSPACEUNITOFMEASURE);
	}


	/**
	 *
	 *
	 */
	public String endStPDLivingSpaceUnitOfMeasureDisplay(ChildContentDisplayEvent event)
	{
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setLivingSpaceUnitOfMeasure(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDLotSizeUnitOfMeasure()
	{
		return (StaticTextField)getChild(CHILD_STPDLOTSIZEUNITOFMEASURE);
	}


	/**
	 *
	 *
	 */
	public String endStPDLotSizeUnitOfMeasureDisplay(ChildContentDisplayEvent event)
	{
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setLotSizeUnitOfMeasure(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDlotSize()
	{
		return (StaticTextField)getChild(CHILD_STPDLOTSIZE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDHeatType()
	{
		return (StaticTextField)getChild(CHILD_STPDHEATTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDInsulatedWithUFFI()
	{
		return (StaticTextField)getChild(CHILD_STPDINSULATEDWITHUFFI);
	}


	/**
	 *
	 *
	 */
	public String endStPDInsulatedWithUFFIDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stPDInsulatedWithUFFI_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDWater()
	{
		return (StaticTextField)getChild(CHILD_STPDWATER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDSewage()
	{
		return (StaticTextField)getChild(CHILD_STPDSEWAGE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDMLSFlag()
	{
		return (StaticTextField)getChild(CHILD_STPDMLSFLAG);
	}


	/**
	 *
	 *
	 */
	public String endStPDMLSFlagDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stPDMLSFlag_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDPropertyUsage()
	{
		return (StaticTextField)getChild(CHILD_STPDPROPERTYUSAGE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDOccupancyType()
	{
		return (StaticTextField)getChild(CHILD_STPDOCCUPANCYTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDPropertyType()
	{
		return (StaticTextField)getChild(CHILD_STPDPROPERTYTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDropertyLocation()
	{
		return (StaticTextField)getChild(CHILD_STPDROPERTYLOCATION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDZoning()
	{
		return (StaticTextField)getChild(CHILD_STPDZONING);
	}


	/**
	 *
	 *
	 */
	public String endStPDZoningDisplay(ChildContentDisplayEvent event)
	{
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setZoningDesc(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDPurchasePrice()
	{
		return (StaticTextField)getChild(CHILD_STPDPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDLandValue()
	{
		return (StaticTextField)getChild(CHILD_STPDLANDVALUE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDEquityAvailable()
	{
		return (StaticTextField)getChild(CHILD_STPDEQUITYAVAILABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDEstimatedAppraisalValue()
	{
		return (StaticTextField)getChild(CHILD_STPDESTIMATEDAPPRAISALVALUE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDActualAppraisalValue()
	{
		return (StaticTextField)getChild(CHILD_STPDACTUALAPPRAISALVALUE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppraisalDate()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPRAISALDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppraisalSource()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPRAISALSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppraiserFistName()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPRAISERFISTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppraiserAddress()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPRAISERADDRESS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppraisalPhone()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPRAISALPHONE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppraiserFax()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPRAISERFAX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppPhoneExt()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPPHONEEXT);
	}

  //--Release3.1--begins
  // by Hiro Apr 6, 2006

  public StaticTextField getStTenure()
  {
    return (StaticTextField) getChild(CHILD_STTENURE);
  }
  
  public StaticTextField getStMiEnergyEfficiency()
  {
    return (StaticTextField) getChild(CHILD_STMIENERGYEFFICIENCY);
  }

  public String endStMiEnergyEfficiencyDisplay(ChildContentDisplayEvent event)
  {
    DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    String rc = handler.setYesOrNo(event.getContent());
    handler.pageSaveState();

    return rc;
  }
  
  public StaticTextField getStSubdivisionDiscount()
  {
    return (StaticTextField) getChild(CHILD_STSUBDIVISIONDISCOUNT);
  }

  public String endStSubdivisionDiscountDisplay(ChildContentDisplayEvent event)
  {
    DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    String rc = handler.setYesOrNo(event.getContent());
    handler.pageSaveState();

    return rc;
  }
  
  public StaticTextField getStOnReserveTrustAgreement()
  {
    return (StaticTextField) getChild(CHILD_STONRESERVETRUSTAGREEMENT);
  }

  //--Release3.1-- ends

	/**
	 *
	 *
	 */
	public String endStPDAppPhoneExtDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stPDAppPhoneExt_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setXPhoneExtInsidePropDetailRepeated(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppraiserInitial()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPRAISERINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPDAppraiserLastName()
	{
		return (StaticTextField)getChild(CHILD_STPDAPPRAISERLASTNAME);
	}


	/**
	 *
	 *
	 */
	public doDetailViewPropertiesModel getdoDetailViewPropertiesModel()
	{
		if (doDetailViewProperties == null)
			doDetailViewProperties = (doDetailViewPropertiesModel) getModel(doDetailViewPropertiesModel.class);
		return doDetailViewProperties;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewPropertiesModel(doDetailViewPropertiesModel model)
	{
			doDetailViewProperties = model;
	}


	/**
	 *
	 *
	 */
	public doViewAppraiserInfoModel getdoViewAppraiserInfoModel()
	{
		if (doViewAppraiserInfo == null)
			doViewAppraiserInfo = (doViewAppraiserInfoModel) getModel(doViewAppraiserInfoModel.class);
		return doViewAppraiserInfo;
	}


	/**
	 *
	 *
	 */
	public void setdoViewAppraiserInfoModel(doViewAppraiserInfoModel model)
	{
			doViewAppraiserInfo = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STPDPRIMARYPROPERTY="stPDprimaryProperty";
	public static final String CHILD_STPDPRIMARYPROPERTY_RESET_VALUE="";
	public static final String CHILD_STPDSTREETNUMBER="stPDstreetNumber";
	public static final String CHILD_STPDSTREETNUMBER_RESET_VALUE="";
	public static final String CHILD_STPDSTREETNAME="stPDStreetName";
	public static final String CHILD_STPDSTREETNAME_RESET_VALUE="";
	public static final String CHILD_STPDSTREETTYPE="stPDStreetType";
	public static final String CHILD_STPDSTREETTYPE_RESET_VALUE="";
	public static final String CHILD_STPDSTREETDIRECTION="stPDStreetDirection";
	public static final String CHILD_STPDSTREETDIRECTION_RESET_VALUE="";
	public static final String CHILD_STPDADDRESSLINE2="stPDAddressLine2";
	
	public static final String CHILD_STPDUNITNUMBER="stPDUnitNumber";
	public static final String CHILD_STPDUNITNUMBER_RESET_VALUE="";
	
	public static final String CHILD_STPDADDRESSLINE2_RESET_VALUE="";
	public static final String CHILD_STPDCITY="stPDcity";
	public static final String CHILD_STPDCITY_RESET_VALUE="";
	public static final String CHILD_STPDPROVINCE="stPDprovince";
	public static final String CHILD_STPDPROVINCE_RESET_VALUE="";
	public static final String CHILD_STPDPOSTCODE1="stPDpostCode1";
	public static final String CHILD_STPDPOSTCODE1_RESET_VALUE="";
	public static final String CHILD_STPDPOSTCODE2="stPDpostCode2";
	public static final String CHILD_STPDPOSTCODE2_RESET_VALUE="";
	public static final String CHILD_STPDLEGALLINE1="stPDlegalLine1";
	public static final String CHILD_STPDLEGALLINE1_RESET_VALUE="";
	public static final String CHILD_STPDLEGALLINE2="stPDlegalLine2";
	public static final String CHILD_STPDLEGALLINE2_RESET_VALUE="";
	public static final String CHILD_STPDLEGALLINE3="stPDlegalLine3";
	public static final String CHILD_STPDLEGALLINE3_RESET_VALUE="";
	public static final String CHILD_STPDWELLINGTYPE="stPDwellingType";
	public static final String CHILD_STPDWELLINGTYPE_RESET_VALUE="";
	public static final String CHILD_STPDWELLINGSTYLE="stPDwellingStyle";
	public static final String CHILD_STPDWELLINGSTYLE_RESET_VALUE="";
	public static final String CHILD_STPDBUILDERNAME="stPDBuilderName";
	public static final String CHILD_STPDBUILDERNAME_RESET_VALUE="";
	public static final String CHILD_STPDGARAGESIZE="stPDGarageSize";
	public static final String CHILD_STPDGARAGESIZE_RESET_VALUE="";
	public static final String CHILD_STPDGARAGETYPE="stPDGarageType";
	public static final String CHILD_STPDGARAGETYPE_RESET_VALUE="";
	public static final String CHILD_STPDNEWCONSTRUCTOR="stPDNewConstructor";
	public static final String CHILD_STPDNEWCONSTRUCTOR_RESET_VALUE="";
	public static final String CHILD_STPDSTRUCTUREAGE="stPDStructureAge";
	public static final String CHILD_STPDSTRUCTUREAGE_RESET_VALUE="";
	public static final String CHILD_STPDNOOFUNITS="stPDNoOfUnits";
	public static final String CHILD_STPDNOOFUNITS_RESET_VALUE="";
	public static final String CHILD_STPDNUMBEROFBEDROOMS="stPDNumberOfBedrooms";
	public static final String CHILD_STPDNUMBEROFBEDROOMS_RESET_VALUE="";
	public static final String CHILD_STPDLIVINGSPACE="stPDLivingSpace";
	public static final String CHILD_STPDLIVINGSPACE_RESET_VALUE="";
	public static final String CHILD_STPDLIVINGSPACEUNITOFMEASURE="stPDLivingSpaceUnitOfMeasure";
	public static final String CHILD_STPDLIVINGSPACEUNITOFMEASURE_RESET_VALUE="";
	public static final String CHILD_STPDLOTSIZEUNITOFMEASURE="stPDLotSizeUnitOfMeasure";
	public static final String CHILD_STPDLOTSIZEUNITOFMEASURE_RESET_VALUE="";
	public static final String CHILD_STPDLOTSIZE="stPDlotSize";
	public static final String CHILD_STPDLOTSIZE_RESET_VALUE="";
	public static final String CHILD_STPDHEATTYPE="stPDHeatType";
	public static final String CHILD_STPDHEATTYPE_RESET_VALUE="";
	public static final String CHILD_STPDINSULATEDWITHUFFI="stPDInsulatedWithUFFI";
	public static final String CHILD_STPDINSULATEDWITHUFFI_RESET_VALUE="";
	public static final String CHILD_STPDWATER="stPDWater";
	public static final String CHILD_STPDWATER_RESET_VALUE="";
	public static final String CHILD_STPDSEWAGE="stPDSewage";
	public static final String CHILD_STPDSEWAGE_RESET_VALUE="";
	public static final String CHILD_STPDMLSFLAG="stPDMLSFlag";
	public static final String CHILD_STPDMLSFLAG_RESET_VALUE="";
	public static final String CHILD_STPDPROPERTYUSAGE="stPDPropertyUsage";
	public static final String CHILD_STPDPROPERTYUSAGE_RESET_VALUE="";
	public static final String CHILD_STPDOCCUPANCYTYPE="stPDOccupancyType";
	public static final String CHILD_STPDOCCUPANCYTYPE_RESET_VALUE="";
	public static final String CHILD_STPDPROPERTYTYPE="stPDPropertyType";
	public static final String CHILD_STPDPROPERTYTYPE_RESET_VALUE="";
	public static final String CHILD_STPDROPERTYLOCATION="stPDropertyLocation";
	public static final String CHILD_STPDROPERTYLOCATION_RESET_VALUE="";
	public static final String CHILD_STPDZONING="stPDZoning";
	public static final String CHILD_STPDZONING_RESET_VALUE="";
	public static final String CHILD_STPDPURCHASEPRICE="stPDPurchasePrice";
	public static final String CHILD_STPDPURCHASEPRICE_RESET_VALUE="";
	public static final String CHILD_STPDLANDVALUE="stPDLandValue";
	public static final String CHILD_STPDLANDVALUE_RESET_VALUE="";
	public static final String CHILD_STPDEQUITYAVAILABLE="stPDEquityAvailable";
	public static final String CHILD_STPDEQUITYAVAILABLE_RESET_VALUE="";
	public static final String CHILD_STPDESTIMATEDAPPRAISALVALUE="stPDEstimatedAppraisalValue";
	public static final String CHILD_STPDESTIMATEDAPPRAISALVALUE_RESET_VALUE="";
	public static final String CHILD_STPDACTUALAPPRAISALVALUE="stPDActualAppraisalValue";
	public static final String CHILD_STPDACTUALAPPRAISALVALUE_RESET_VALUE="";
	public static final String CHILD_STPDAPPRAISALDATE="stPDAppraisalDate";
	public static final String CHILD_STPDAPPRAISALDATE_RESET_VALUE="";
	public static final String CHILD_STPDAPPRAISALSOURCE="stPDAppraisalSource";
	public static final String CHILD_STPDAPPRAISALSOURCE_RESET_VALUE="";
	public static final String CHILD_STPDAPPRAISERFISTNAME="stPDAppraiserFistName";
	public static final String CHILD_STPDAPPRAISERFISTNAME_RESET_VALUE="";
	public static final String CHILD_STPDAPPRAISERADDRESS="stPDAppraiserAddress";
	public static final String CHILD_STPDAPPRAISERADDRESS_RESET_VALUE="";
	public static final String CHILD_STPDAPPRAISALPHONE="stPDAppraisalPhone";
	public static final String CHILD_STPDAPPRAISALPHONE_RESET_VALUE="";
	public static final String CHILD_STPDAPPRAISERFAX="stPDAppraiserFax";
	public static final String CHILD_STPDAPPRAISERFAX_RESET_VALUE="";
	public static final String CHILD_STPDAPPPHONEEXT="stPDAppPhoneExt";
	public static final String CHILD_STPDAPPPHONEEXT_RESET_VALUE="";
	public static final String CHILD_STPDAPPRAISERINITIAL="stPDAppraiserInitial";
	public static final String CHILD_STPDAPPRAISERINITIAL_RESET_VALUE="";
	public static final String CHILD_STPDAPPRAISERLASTNAME="stPDAppraiserLastName";
	public static final String CHILD_STPDAPPRAISERLASTNAME_RESET_VALUE="";

  //--Release3.1--begins
  //--by Hiro Apr 10, 2006
  public static final String CHILD_STTENURE="stTenure";
  public static final String CHILD_STTENURE_RESET_VALUE="";
  
  public static final String CHILD_STMIENERGYEFFICIENCY="stMiEnergyEfficiency";
  public static final String CHILD_STMIENERGYEFFICIENCY_RESET_VALUE="";

  public static final String CHILD_STSUBDIVISIONDISCOUNT="stSubdivisionDiscount";
  public static final String CHILD_STSUBDIVISIONDISCOUNT_RESET_VALUE="";

  public static final String CHILD_STONRESERVETRUSTAGREEMENT="stOnReserveTrustAgreement";
  public static final String CHILD_STONRESERVETRUSTAGREEMENT_RESET_VALUE="";
  
  //--Release3.1--ends

  ////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewPropertiesModel doDetailViewProperties=null;
	private doViewAppraiserInfoModel doViewAppraiserInfo=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

