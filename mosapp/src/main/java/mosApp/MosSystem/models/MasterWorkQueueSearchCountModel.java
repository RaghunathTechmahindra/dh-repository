package mosApp.MosSystem.models;

import com.iplanet.jato.model.sql.SelectQueryModel;

public interface MasterWorkQueueSearchCountModel extends SelectQueryModel {

	public static final String FIELD_DFWQNUMOFROWS = "dfWQNumOfRows";

}
