package mosApp.MosSystem.models;

import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

public class MasterWorkQueueSearchCountModelImpl extends QueryModelBase implements MasterWorkQueueSearchCountModel {

	private static final long serialVersionUID = -1506126104171479917L;

	public static final String DATA_SOURCE_NAME = "jdbc/orcl";
	public static final String QUALIFIED_COLUMN_DFWQNUMOFROWS = "ASSIGNEDTASKSWORKQUEUE.ASSIGNEDTASKSWORKQUEUEID.TOTAL_COUNT";
	public static final String COLUMN_DFWQNUMOFROWS = "TOTAL_COUNT";
	public static final String MODIFYING_QUERY_TABLE_NAME = "ASSIGNEDTASKSWORKQUEUE, DEAL, USERPROFILE, GROUPPROFILE, BRANCHPROFILE, CONTACT c1 ";
	public static final String SELECT_SQL_TEMPLATE = " SELECT COUNT(*) TOTAL_COUNT FROM ASSIGNEDTASKSWORKQUEUE,DEAL,USERPROFILE,GROUPPROFILE,BRANCHPROFILE,CONTACT c1 __WHERE__ ";
	public static final String STATIC_WHERE_CRITERIA;

	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

	static {
		StringBuffer sql = new StringBuffer();
		sql.append(" deal.institutionprofileid=assignedtasksworkqueue.institutionprofileid  ");
		sql.append(" AND deal.dealid=assignedtasksworkqueue.dealid  ");
		sql.append(" AND assignedtasksworkqueue.institutionprofileid=userprofile.institutionprofileid  ");
		sql.append(" AND assignedtasksworkqueue.userprofileid=userprofile.userprofileid   ");
		sql.append(" AND userprofile.institutionprofileid=groupprofile.institutionprofileid  ");
		sql.append(" AND userprofile.groupprofileid=groupprofile.groupprofileid  ");
		sql.append(" AND branchprofile.institutionprofileid=groupprofile.institutionprofileid  ");
		sql.append(" AND branchprofile.branchprofileid=groupprofile.branchprofileid  ");
		sql.append(" AND userprofile.institutionprofileid=c1.institutionprofileid  ");
		sql.append(" AND userprofile.contactid=c1.contactid  ");
		sql.append(" AND DEAL.SCENARIORECOMMENDED='Y'");
		sql.append(" AND ASSIGNEDTASKSWORKQUEUE.TASKID<50000");
		sql.append(" AND DEAL.COPYTYPE<>'T' ");
		sql.append(" AND c1.copyid=1 ");
		STATIC_WHERE_CRITERIA = sql.toString();

		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFWQNUMOFROWS, COLUMN_DFWQNUMOFROWS, QUALIFIED_COLUMN_DFWQNUMOFROWS, java.math.BigDecimal.class, false, false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
	}

	public MasterWorkQueueSearchCountModelImpl() {
		super();
		setDataSourceName(DATA_SOURCE_NAME);
		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
		setFieldSchema(FIELD_SCHEMA);
	}

}
