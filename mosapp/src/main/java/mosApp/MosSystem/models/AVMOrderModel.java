/*
 * @(#)AVMOrderModel.java    2006-2-13
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package mosApp.MosSystem.models;

import java.sql.Timestamp;
import java.math.BigDecimal;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * AVMOrderModel defines the bound fields and getters and setters to access
 * those fields.
 *
 * @version   1.0 2006-2-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface AVMOrderModel
    extends QueryModel, SelectQueryModel {

    /**
     * fields definitions.
     */
    public final static String FIELD_DFAVMPROVIDERID = "dfAVMProviderId";
    public final static String FIELD_DFAVMPRODUCTID = "dfAVMProductId";
    public final static String FIELD_DFAVMREQUESTSTATUS = "dfAVMRequestStatus";
    public final static String FIELD_DFAVMREQUESTDATE = "dfAVMRequestDate";
    public final static String FIELD_DFAVMREQUESTSTATUSMSG =
        "dfAVMRequestStatusMsg";
    public final static String FIELD_DFAVMDEALID = "dfAVMDealId";
    public final static String FIELD_DFAVMCOPYID = "dfAVMCopyId";
    public final static String FIELD_DFAVMPROPERTYID = "dfAVMPropertyId";
    public final static String FIELD_DFAVMPROPERTYCOPYID = "dfAVMPropertyCopyId";

    /**
     * getter and setters.
     */
    public BigDecimal getDfAVMProviderId();
    public void setDfAVMProviderId(BigDecimal id);

    public BigDecimal getDfAVMProductId();
    public void setDfAVMProductId(BigDecimal id);

    public Timestamp getDfAVMRequestDate();
    public void setDfAVMRequestDate(Timestamp date);

    public String getDfAVMRequestStatus();
    public void setDfAVMRequestStatus(String status);

    public String getDfAVMRequestStatusMsg();
    public void setDfAVMRequestStatusMsg(String msg);

    public BigDecimal getDfAVMDealId();
    public void setDfAVMDealId(BigDecimal id);

    public BigDecimal getDfAVMCopyId();
    public void setDfAVMCopyId(BigDecimal id);

    public BigDecimal getDfAVMPropertyId();
    public void setDfAVMPropertyId(BigDecimal id);

    public BigDecimal getDfAVMPropertyCopyId();
    public void setDfAVMPropertyCopyId(BigDecimal id);
}
