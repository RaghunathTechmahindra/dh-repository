/*
 * @(#)AVMOrderModelImpl.java    2006-2-13
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package mosApp.MosSystem.models;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;
import mosApp.MosSystem.SessionStateModel;
import mosApp.MosSystem.SessionStateModelImpl;

/**
 * AVMOrderModelImpl - 
 *
 * @version   1.0 2006-2-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AVMOrderModelImpl extends QueryModelBase
    implements AVMOrderModel {

    // The logger
    private static Log _log = LogFactory.getLog(AVMOrderModelImpl.class);

    public static final String DATA_SOURCE_NAME="jdbc/orcl";
    // the SQL template.
    public static final String SELECT_SQL_TEMPLATE =
        "SELECT DISTINCT " +
        "REQUEST.REQUESTDATE, " +
        "REQUEST.DEALID, " +
        "REQUEST.COPYID, " +
        "SERVICEREQUEST.PROPERTYID, " +
        "REQUEST.SERVICEPRODUCTID, " +
        "SERVICEPRODUCT.SERVICEPROVIDERID, " +
        "to_char(REQUEST.REQUESTSTATUSID) STATUSID_STR, " +
        "REQUEST.STATUSDATE, " +
        "REQUEST.STATUSMESSAGE " +
        "FROM " +
        "REQUEST, SERVICEREQUEST, SERVICEPRODUCT " +
        "__WHERE__  " +
        "ORDER BY REQUEST.REQUESTDATE DESC";
    // the modifying query table name.
    public static final String MODIFYING_QUERY_TABLE_NAME =
        "REQUEST, SERVICEREQUEST, SERVICEPRODUCT";
    // the static where query criteria.
    public static final String STATIC_WHERE_CRITERIA =
        " (SERVICEREQUEST.INSTITUTIONPROFILEID = REQUEST.INSTITUTIONPROFILEID) AND " +
        " (SERVICEREQUEST.INSTITUTIONPROFILEID = SERVICEPRODUCT.INSTITUTIONPROFILEID) AND " +
        " (SERVICEREQUEST.REQUESTID = REQUEST.REQUESTID) AND " +
        " (REQUEST.SERVICEPRODUCTID = SERVICEPRODUCT.SERVICEPRODUCTID) AND " +
        " (SERVICEPRODUCT.SERVICETYPEID = 6) AND " +
        " (SERVICEPRODUCT.SERVICESUBTYPEID = 1) ";
    // the query field schema.
    public static final QueryFieldSchema FIELD_SCHEMA =
        new QueryFieldSchema();

    // column name in the SQL template.
    public static final String QUALIFIED_COLUMN_DFAVMDEALID =
        "REQUEST.DEALID";
    public static final String COLUMN_DFAVMDEALID =
        "DEALID";
    public static final String QUALIFIED_COLUMN_DFAVMCOPYID =
        "REQUEST.COPYID";
    public static final String COLUMN_DFAVMCOPYID =
        "COPYID";
    public static final String QUALIFIED_COLUMN_DFAVMPROPERTYID =
        "SERVICEREQUEST.PROPERTYID";
    public static final String COLUMN_DFAVMPROPERTYID =
        "PROPERTYID";
    public static final String QUALIFIED_COLUMN_DFAVMPROPERTYCOPYID =
        "SERVICEREQUEST.COPYID";
    public static final String COLUMN_DFAVMPROPERTYCOPYID =
        "COPYID";
    public static final String QUALIFIED_COLUMN_DFAVMPROVIDERID =
        "SERVICEPRODUCT.SERVICEPROVIDERID";
    public static final String COLUMN_DFAVMPROVIDERID =
        "SERVICEPROVIDERID";
    public static final String QUALIFIED_COLUMN_DFAVMPRODUCTID =
        "REQUEST.SERVICEPRODUCTID";
    public static final String COLUMN_DFAVMPRODUCTID =
        "SERVICEPRODUCTID";
    public static final String QUALIFIED_COLUMN_DFAVMREQUESTSTATUS =
        "REQUEST.STATUSID_STR";
    public static final String COLUMN_DFAVMREQUESTSTATUS =
        "STATUSID_STR";
    public static final String QUALIFIED_COLUMN_DFAVMREQUESTDATE =
        "REQUEST.STATUSDATE";
    public static final String COLUMN_DFAVMREQUESTDATE =
        "STATUSDATE";
    public static final String QUALIFIED_COLUMN_DFAVMREQUESTSTATUSMSG =
        "REQUEST.STATUSMESSAGE";
    public static final String COLUMN_DFAVMREQUESTSTATUSMSG =
        "STATUSMESSAGE";

    // build the association.
    static {
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMPROVIDERID,
                COLUMN_DFAVMPROVIDERID,
                QUALIFIED_COLUMN_DFAVMPROVIDERID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMPRODUCTID,
                COLUMN_DFAVMPRODUCTID,
                QUALIFIED_COLUMN_DFAVMPRODUCTID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMDEALID,
                COLUMN_DFAVMDEALID,
                QUALIFIED_COLUMN_DFAVMDEALID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMCOPYID,
                COLUMN_DFAVMCOPYID,
                QUALIFIED_COLUMN_DFAVMCOPYID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMPROPERTYID,
                COLUMN_DFAVMPROPERTYID,
                QUALIFIED_COLUMN_DFAVMPROPERTYID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMPROPERTYCOPYID,
                COLUMN_DFAVMPROPERTYCOPYID,
                QUALIFIED_COLUMN_DFAVMPROPERTYCOPYID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMREQUESTSTATUS,
                COLUMN_DFAVMREQUESTSTATUS,
                QUALIFIED_COLUMN_DFAVMREQUESTSTATUS,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMREQUESTSTATUSMSG,
                COLUMN_DFAVMREQUESTSTATUSMSG,
                QUALIFIED_COLUMN_DFAVMREQUESTSTATUSMSG,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFAVMREQUESTDATE,
                COLUMN_DFAVMREQUESTDATE,
                QUALIFIED_COLUMN_DFAVMREQUESTDATE,
                Timestamp.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
    }

    /**
     * Constructor function
     */
    public AVMOrderModelImpl() {

        super();
        setDataSourceName(DATA_SOURCE_NAME);
        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
        setFieldSchema(FIELD_SCHEMA);
        initialize();
    }

    /**
     * initializing from JATO framework.
     */
    public void initialize() {}

    /**
     * before execute.
     */
    protected  String beforeExecute(ModelExecutionContext context,
                                   int queryType, String sql)
        throws ModelControlException {

        if (_log.isDebugEnabled())
            _log.debug("SQL befor Execute: " + sql);
        return sql;
    }

    /**
     * after execute, we need change some language sensitive value for the
     * result.
     */
    protected void afterExecute(ModelExecutionContext context,
                                int queryType)
        throws ModelControlException {

        if (_log.isDebugEnabled())
            _log.debug("Return Record: " + getSize());
        if (getSize() < 1) {
            // do nothing.
            return;
        }
        // try to get language id from the session state model.
        String defaultInstanceStateName =
            getRequestContext().getModelManager().
            getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl sessionState = (SessionStateModelImpl)
            getRequestContext().getModelManager().
            getModel(SessionStateModel.class,
                     defaultInstanceStateName, true);
        int languageId = sessionState.getLanguageId();
		int institutionId = sessionState.getDealInstitutionId();

        // get the status from the BXResources and set to the field.
        if (_log.isDebugEnabled())
            _log.debug("Trying to get the status from BXResources");
        setDfAVMRequestStatus(
                      BXResources.
                      getPickListDescription(institutionId, "REQUESTSTATUS",
                                             getDfAVMRequestStatus(),
                                             languageId));
    }

    /**
     * getter and setter implementaiton.
     */
    public BigDecimal getDfAVMProviderId() {
        return (BigDecimal) getValue(FIELD_DFAVMPROVIDERID);
    }
    public void setDfAVMProviderId(BigDecimal id) {
        setValue(FIELD_DFAVMPROVIDERID, id);
    }

    public BigDecimal getDfAVMProductId() {
        return (BigDecimal) getValue(FIELD_DFAVMPRODUCTID);
    }
    public void setDfAVMProductId(BigDecimal id) {
        setValue(FIELD_DFAVMPRODUCTID, id);
    }

    public Timestamp getDfAVMRequestDate() {
        return (Timestamp) getValue(FIELD_DFAVMREQUESTDATE);
    }
    public void setDfAVMRequestDate(Timestamp date) {
        setValue(FIELD_DFAVMREQUESTDATE, date);
    }

    public String getDfAVMRequestStatus() {
        return (String) getValue(FIELD_DFAVMREQUESTSTATUS);
    }
    public void setDfAVMRequestStatus(String status) {
        setValue(FIELD_DFAVMREQUESTSTATUS, status);
    }

    public String getDfAVMRequestStatusMsg() {
        return (String) getValue(FIELD_DFAVMREQUESTSTATUSMSG);
    }
    public void setDfAVMRequestStatusMsg(String msg) {
        setValue(FIELD_DFAVMREQUESTSTATUSMSG, msg);
    }

    public BigDecimal getDfAVMDealId() {
        return (BigDecimal) getValue(FIELD_DFAVMDEALID);
    }
    public void setDfAVMDealId(BigDecimal id) {
        setValue(FIELD_DFAVMDEALID, id);
    }

    public BigDecimal getDfAVMCopyId() {
        return (BigDecimal) getValue(FIELD_DFAVMCOPYID);
    }
    public void setDfAVMCopyId(BigDecimal id) {
        setValue(FIELD_DFAVMCOPYID, id);
    }

    public BigDecimal getDfAVMPropertyId() {
        return (BigDecimal) getValue(FIELD_DFAVMPROPERTYID);
    }
    public void setDfAVMPropertyId(BigDecimal id) {
        setValue(FIELD_DFAVMPROPERTYID, id);
    }

    public BigDecimal getDfAVMPropertyCopyId() {
        return (BigDecimal) getValue(FIELD_DFAVMPROPERTYCOPYID);
    }
    public void setDfAVMPropertyCopyId(BigDecimal id) {
        setValue(FIELD_DFAVMPROPERTYCOPYID, id);
    }
}
