package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgIALReviewRepeatedCreditTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedCreditTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doBorrowerEntryCreditModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getTbCreditReferencrDescription().setValue(CHILD_TBCREDITREFERENCRDESCRIPTION_RESET_VALUE);
		getTbInstituationName().setValue(CHILD_TBINSTITUATIONNAME_RESET_VALUE);
		getTbAccountNumber().setValue(CHILD_TBACCOUNTNUMBER_RESET_VALUE);
		getTbCurrentBalance().setValue(CHILD_TBCURRENTBALANCE_RESET_VALUE);
		getBtDeleteCredit().setValue(CHILD_BTDELETECREDIT_RESET_VALUE);
		getCbCreditRefType().setValue(CHILD_CBCREDITREFTYPE_RESET_VALUE);
		getHdCreditBorrowerId().setValue(CHILD_HDCREDITBORROWERID_RESET_VALUE);
		getHdCreditId().setValue(CHILD_HDCREDITID_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_TBCREDITREFERENCRDESCRIPTION,TextField.class);
		registerChild(CHILD_TBINSTITUATIONNAME,TextField.class);
		registerChild(CHILD_TBACCOUNTNUMBER,TextField.class);
		registerChild(CHILD_TBCURRENTBALANCE,TextField.class);
		registerChild(CHILD_BTDELETECREDIT,Button.class);
		registerChild(CHILD_CBCREDITREFTYPE,ComboBox.class);
		registerChild(CHILD_HDCREDITBORROWERID,HiddenField.class);
		registerChild(CHILD_HDCREDITID,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoBorrowerEntryCreditModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;


		// The following code block was migrated from the RepeatedCredit_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("inc @RepeatedCredit "+sumofrowsForCredit);
		sumofrowsForCredit ++;


		/$int rownum = event.getRowIndex(); //get index of the row

				Object index = new Integer(rownum + 1); //number of the row
				CSpRepeated rep = (CSpRepeated)getCommonRepeated("RepeatedIncome");
				rep.setDisplayFieldValue("stIndex2", index); //set value
				$/
		return;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_TBCREDITREFERENCRDESCRIPTION))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBCREDITREFERENCRDESCRIPTION,
				CHILD_TBCREDITREFERENCRDESCRIPTION,
				CHILD_TBCREDITREFERENCRDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBINSTITUATIONNAME))
		{
			TextField child = new TextField(this,
				getdoBorrowerEntryCreditModel(),
				CHILD_TBINSTITUATIONNAME,
				doBorrowerEntryCreditModel.FIELD_DFCREDITREFINSTITUTIONNAME,
				CHILD_TBINSTITUATIONNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBACCOUNTNUMBER))
		{
			TextField child = new TextField(this,
				getdoBorrowerEntryCreditModel(),
				CHILD_TBACCOUNTNUMBER,
				doBorrowerEntryCreditModel.FIELD_DFCREDITREFACCOUNTNUMBER,
				CHILD_TBACCOUNTNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBCURRENTBALANCE))
		{
			TextField child = new TextField(this,
				getdoBorrowerEntryCreditModel(),
				CHILD_TBCURRENTBALANCE,
				doBorrowerEntryCreditModel.FIELD_DFCREDITREFCURRENTBALANCE,
				CHILD_TBCURRENTBALANCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETECREDIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETECREDIT,
				CHILD_BTDELETECREDIT,
				CHILD_BTDELETECREDIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CBCREDITREFTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryCreditModel(),
				CHILD_CBCREDITREFTYPE,
				doBorrowerEntryCreditModel.FIELD_DFCREDITREFTYPEID,
				CHILD_CBCREDITREFTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbCreditRefTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_HDCREDITBORROWERID))
		{
			HiddenField child = new HiddenField(this,
				getdoBorrowerEntryCreditModel(),
				CHILD_HDCREDITBORROWERID,
				doBorrowerEntryCreditModel.FIELD_DFBORROWERID,
				CHILD_HDCREDITBORROWERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDCREDITID))
		{
			HiddenField child = new HiddenField(this,
				getdoBorrowerEntryCreditModel(),
				CHILD_HDCREDITID,
				doBorrowerEntryCreditModel.FIELD_DFCREDITREFID,
				CHILD_HDCREDITID_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public TextField getTbCreditReferencrDescription()
	{
		return (TextField)getChild(CHILD_TBCREDITREFERENCRDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public TextField getTbInstituationName()
	{
		return (TextField)getChild(CHILD_TBINSTITUATIONNAME);
	}


	/**
	 *
	 *
	 */
	public TextField getTbAccountNumber()
	{
		return (TextField)getChild(CHILD_TBACCOUNTNUMBER);
	}


	/**
	 *
	 *
	 */
	public TextField getTbCurrentBalance()
	{
		return (TextField)getChild(CHILD_TBCURRENTBALANCE);
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteCredit()
	{
		return (Button)getChild(CHILD_BTDELETECREDIT);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteCreditDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteCredit_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("@btDeleteCredit_onBeforeHtmlOutputEvent");
		try
		{
			CSpButton buttonHtml =(CSpButton) event.getSource();

			//CSpHtml.sendMessage("Changed2" + buttonHtml);
			String htmlstr = "<INPUT TYPE=IMAGE NAME=\"deleteAction(Credit," + sumofrowsForCredit + ")\" VALUE=\"Delete\" SRC=\"/eNet_images/delete.gif\" ALIGN=TOP  width=52 height=15 alt=\"\" border=\"0\">";

			//CSpHtml.sendMessage("htmlstr " + htmlstr);
			buttonHtml.setHtmlText(htmlstr);
		}
		catch(Exception ex)
		{
			CSpHtml.sendMessage("Excp@html output " + ex.toString());
		}

		return;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbCreditRefType()
	{
		return (ComboBox)getChild(CHILD_CBCREDITREFTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbCreditRefTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbCreditRefTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object CREDITREFTYPEID = results.getObject("CREDITREFTYPEID");
					Object CREDITREFTYPE_CRTDESCRIPTION = results.getObject("CRTDESCRIPTION");

					String label = (CREDITREFTYPE_CRTDESCRIPTION == null?"":CREDITREFTYPE_CRTDESCRIPTION.toString());

					String value = (CREDITREFTYPEID == null?"":CREDITREFTYPEID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT CREDITREFTYPE.CRTDESCRIPTION, CREDITREFTYPE.CREDITREFTYPEID FROM CREDITREFTYPE";

	}



	/**
	 *
	 *
	 */
	public HiddenField getHdCreditBorrowerId()
	{
		return (HiddenField)getChild(CHILD_HDCREDITBORROWERID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdCreditId()
	{
		return (HiddenField)getChild(CHILD_HDCREDITID);
	}


	/**
	 *
	 *
	 */
	public doBorrowerEntryCreditModel getdoBorrowerEntryCreditModel()
	{
		if (doBorrowerEntryCredit == null)
			doBorrowerEntryCredit = (doBorrowerEntryCreditModel) getModel(doBorrowerEntryCreditModel.class);
		return doBorrowerEntryCredit;
	}


	/**
	 *
	 *
	 */
	public void setdoBorrowerEntryCreditModel(doBorrowerEntryCreditModel model)
	{
			doBorrowerEntryCredit = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBCREDITREFERENCRDESCRIPTION="tbCreditReferencrDescription";
	public static final String CHILD_TBCREDITREFERENCRDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_TBINSTITUATIONNAME="tbInstituationName";
	public static final String CHILD_TBINSTITUATIONNAME_RESET_VALUE="";
	public static final String CHILD_TBACCOUNTNUMBER="tbAccountNumber";
	public static final String CHILD_TBACCOUNTNUMBER_RESET_VALUE="";
	public static final String CHILD_TBCURRENTBALANCE="tbCurrentBalance";
	public static final String CHILD_TBCURRENTBALANCE_RESET_VALUE="";
	public static final String CHILD_BTDELETECREDIT="btDeleteCredit";
	public static final String CHILD_BTDELETECREDIT_RESET_VALUE="Delete";
	public static final String CHILD_CBCREDITREFTYPE="cbCreditRefType";
	public static final String CHILD_CBCREDITREFTYPE_RESET_VALUE="";
	private CbCreditRefTypeOptionList cbCreditRefTypeOptions=new CbCreditRefTypeOptionList();
	public static final String CHILD_HDCREDITBORROWERID="hdCreditBorrowerId";
	public static final String CHILD_HDCREDITBORROWERID_RESET_VALUE="";
	public static final String CHILD_HDCREDITID="hdCreditId";
	public static final String CHILD_HDCREDITID_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doBorrowerEntryCreditModel doBorrowerEntryCredit=null;

}

