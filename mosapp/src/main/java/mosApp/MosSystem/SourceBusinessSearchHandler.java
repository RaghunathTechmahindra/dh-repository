package mosApp.MosSystem;

import java.text.*;
import java.io.Serializable;
import java.util.*;

import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.deal.security.*;
import com.basis100.deal.query.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.picklist.BXResources;
import com.basis100.jdbcservices.jdbcexecutor.*;

import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;


//
	// Available from Page Shell Go as a generic search facility (e.g. usage not associated
	// with a deal). When used as a sub page the associated deal is obtained in the normal fashion,
	// e.g.:
	//
	//		let pg be the page entry for the page, then
	//
	//        associated deal id == pg.getPageDealId()
	//
	// When usage is with an associated deal select of a source (from search results) cause the
	// selected source to be recorded as the source for on the deal.
	//

public class SourceBusinessSearchHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	// key for storing fields edit buffer in page state table

	private static final String EDIT_BUFFER="EDIT_BUFFER";
	// Key name to pass the SOBId to sub page(s)

	public static final String SOB_ID_PASSED_PARM="sourceOBIdPassed";


	/**
	 *
	 *
	 */
	public SourceBusinessSearchHandler cloneSS()
	{
		return(SourceBusinessSearchHandler) super.cloneSafeShallow();

	}


	/////////////////////////////////////////////////////////////////////////

	public void saveData(PageEntry currPage, boolean calledInTransaction)
	{
		PageCursorInfo tds = getPageCursorInfo(currPage, "DEFAULT");

		Hashtable pst = currPage.getPageStateTable();

		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(EDIT_BUFFER);

		efb.readFields(getCurrNDPage());


		// we never mark as modified - allow user to walk away without fuss!
		currPage.setModified(false);

	}


	/////////////////////////////////////////////////////////////////////////

	public void preHandlerProtocol(ViewBean currNDPage)
	{
		super.preHandlerProtocol(currNDPage);


		// sefault page load method to display()
		PageEntry pg = theSessionState.getCurrentPage();

		pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);

	}


	/////////////////////////////////////////////////////////////////////////
	// This method is used to populate the fields in the header
	// with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();

		populatePreviousPagesLinks();

		populatePageDealSummarySnapShot();

		revisePrevNextButtonGeneration(pg, tds);

		displayDSSConditional(pg, getCurrNDPage());


		// populate current search criteria
		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(EDIT_BUFFER);

		efb.populateFields(getCurrNDPage());


		// generate tasks displayed display field
		populateRowsDisplayed(getCurrNDPage(), tds);

    //--> This was missing -- Added By Billy 22July2003
    //// JavaScript client side data validation API implementation.
    try
    {
 
      String sobSearchValidPath = Sc.DVALID_SOB_SEARCH_PROPS_FILE_NAME;

      //logger.debug("SFEH@partySearchValidPath: " + partySearchValidPath);

      JSValidationRules sobSearchValidObj = new JSValidationRules(sobSearchValidPath, logger, getCurrNDPage());
      sobSearchValidObj.attachJSValidationRules();
    }
    catch( Exception e )
    {
        logger.error("Exception @SOBSearch.populatePageDisplayFields : Problem encountered in JS data validation API." + e);
    }

	}


	/////////////////////////////////////////////////////////////////////////

	private void populateRowsDisplayed(ViewBean NDPage, PageCursorInfo tds)
	{
		String rowsRpt = "";

		//--Release2.1--//
    if (tds.getTotalRows() > 0) {
    rowsRpt = BXResources.getGenericMsg("DISPLAY_SEARCH_RESULTS_LABEL", theSessionState.getLanguageId()) + " " +
              tds.getFirstDisplayRowNumber() + "-" +
              tds.getLastDisplayRowNumber() + " " +
              BXResources.getGenericMsg("OF_LABEL", theSessionState.getLanguageId()) + " " +
              tds.getTotalRows();
    }

		NDPage.setDisplayFieldValue("rowsDisplayed", new String(rowsRpt));
	}


	/////////////////////////////////////////////////////////////////////////

	private void displayDSSConditional(PageEntry pg, ViewBean thePage)
	{
		String suppressStart = "<!--";

		String suppressEnd = "//-->";

		if (pg == null || pg.getPageDealId() <= 0)
		{
			// suppress
			thePage.setDisplayFieldValue("stIncludeDSSstart", new String(suppressStart));
			thePage.setDisplayFieldValue("stIncludeDSSend", new String(suppressEnd));
			return;
		}


		// display
		thePage.setDisplayFieldValue("stIncludeDSSstart", new String(""));

		thePage.setDisplayFieldValue("stIncludeDSSend", new String(""));

		return;

	}


	/////////////////////////////////////////////////////////////////////////

	////public int populateRepeatedFields(int ndx)
	public boolean populateRepeatedFields(int ndx)
	{
		logger.debug("SOBSH@.populateRepeatedFields: relative page row ndx=" + ndx);

		try
		{
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			if (ndx < 0 || tds.getTotalRows() <= 0)
          return false;

			logger.debug("SOBSH@.populateRepeatedFields:tds:: total currPage  currRel currAbs lastPage :: " + tds.getTotalRows() + " " + tds.getCurrPageNdx() + " " + tds.getRelSelectedRowNdx() + " " + tds.getAbsSelectedRowNdx() + " " + tds.isLastPage());
			ndx = getCursorAbsoluteNdx(tds, ndx);
			logger.debug("SOBSH@.populateRepeatedFields:tds:: currAbs (computed) :: " + ndx);

			Vector results =(Vector) pst.get("RESULTS");
			logger.debug("SOBSH@.populateRepeatedFields:results:: size == " + results.size());

      //--Release2.1--//
      //// langId parameter should be added to the buffer in order to toggle
      //// the language.
      int languageId = theSessionState.getLanguageId();
	  int institutionId = theSessionState.getDealInstitutionId();
	  SourceBusinessSearchResultRow sbsrr = new SourceBusinessSearchResultRow(institutionId, "RROW" + ndx,
                                                                                        logger,
                                                                                        languageId,
                                                                                        (SourceQueryResult) results.elementAt(ndx));
			sbsrr.populateFields(getCurrNDPage());
			return true;
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @SourceBusinessSearchHandler.populateRepeatedFields: populating results row (ndx=" + ndx + ")");
			logger.error(e);
		}

		return false;

	}
  /////////////////////////////////////////////////////////////////////////////

	/*
	 * Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	*/
	public void setupBeforePageGeneration()
	{
		logger.trace("--T--> @SourceBusinessSearchHandler.setupBeforePageGeneration");

		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);


		// -- //
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		setupDealSummarySnapShotDO(pg);

		EditFieldsBuffer efb = null;


		// our (main) page has no DO bindings so row generator (2) was used to
		// create one - because we rely on binding to trigger Object Framework methods
		setRowGenerator2DOForNRows(1);


		// search results display state (corresponds to list of source businesses matching criteria ::
		// this object is the default (named "DEFAULT") for this page
		if (tds == null)
		{
			logger.trace("--T--> @SourceBusinessSearchHandler.setupBeforePageGeneration: first invocation");

			// first invocation!
			// determine number of notes (shown) per display page
			String voName = "rpSourceBusiness";
			////int perPage =(getCurrNDPage().getCommonRepeated(voName)).getMaxDisplayRows();

      pgSourceofBusSearchrpSourceBusinessTiledView vo = ((pgSourceofBusSearchViewBean)getCurrNDPage()).getRpSourceBusiness();
      logger.debug("SOBSH@setupBeforePageGen::Repeated1: " + vo);
      logger.debug("SOBSH@setupBeforePageGen::Repeated1Name: " + vo.getName());

      //// BXTODO: only for testing purposes.
      int perPageNum = vo.getNumTiles();
      logger.debug("SOBSH@setupBeforePageGen:NumTiles: " + perPageNum);

      int perPage =((TiledView)(getCurrNDPage().getChild(voName))).getMaxDisplayTiles();
      logger.debug("SOBSH@setupBeforePageGen:PerPageNum: " + perPage);

			// set up and save task display state object
			tds = new PageCursorInfo("DEFAULT", voName, perPage, - 1);
			tds.setRefresh(true);
			tds.setCriteria("");
			pst.put(tds.getName(), tds);

      // edit buffer for screen fields
			efb = new SourceBusinessSearchEditBuffer("SourceBusinessSearchParms", logger);
			pst.put(EDIT_BUFFER, efb);
		}


		//  get the edit buffer
		efb =(EditFieldsBuffer) pst.get(EDIT_BUFFER);


		// chech for search criteria present
		int numResults = 0;

		if (isSearchCriteria(efb) != false)
		{
			// check for change in search criteria
			tds.setCriteria(efb.getAsCriteria());
			logger.trace("--Toni--> @SourceBusinessSearchHandler.setupBeforePageGeneration: have search criteria");
			numResults = - 1;

			// to keep previous value!
			if (tds.isCriteriaChanged())
			{
				try
				{
					// re-query and store results()
					SourceBusinessQuery sbq = new SourceBusinessQuery(getSessionResourceKit());
					boolean flags [ ] = getSearchCriteriaFlags(efb);
					logger.debug("--D--> Flag1: " + flags [ 0 ] + " flag2: " + flags [ 1 ]);

					// pg.show("******************************************************", logger);
					logger.trace("--Toni--> @SourceBusinessSearchHandler.setupBeforePageGeneration: performing search:: " + flags [ 0 ] + " " + flags [ 1 ]);

					// Modified to search SourceName if it contains in the ful name -- BILLY 07Dec2001
					Vector results = sbq.query(flags, efb.getFieldValue("tbSourceName"),
// FIRST NAME
					efb.getFieldValue("tbSourceName"),
// INITIAL
					efb.getFieldValue("tbSourceName"),
// LASTNAME
					efb.getFieldValue("tbSourceCity"),
          efb.getFieldValue("cbProvince"),


          getPhoneSearchString(efb.getFieldValue("tbSourcePhoneNoPart1"), efb.getFieldValue("tbSourcePhoneNoPart2"), efb.getFieldValue("tbSourcePhoneNoPart3")),
          getPhoneSearchString(efb.getFieldValue("tbSourceFaxPart1"), efb.getFieldValue("tbSourceFaxPart2"), efb.getFieldValue("tbSourceFaxPart3")),
          efb.getFieldValue("tbSourceFirm"));

					logger.debug("--D--> query parms --> " + "\"" + efb.getFieldValue("tbSourceName") + "\", " + "\"" + efb.getFieldValue("tbSourceCity") + "\", " + "\"" + efb.getFieldValue("cbProvince") + "\", " + "\"" + getPhoneSearchString(efb.getFieldValue("tbSourcePhoneNoPart1"), efb.getFieldValue("tbSourcePhoneNoPart2"), efb.getFieldValue("tbSourcePhoneNoPart3")) + "\", " + "\"" + getPhoneSearchString(efb.getFieldValue("tbSourceFaxPart1"), efb.getFieldValue("tbSourceFaxPart2"), efb.getFieldValue("tbSourceFaxPart3")) + "\", " + "\"" + efb.getFieldValue("tbSourceFirm") + "\"");
					logger.debug("--D--> results: " +((results == null) ? "null" :("" + results.size() + "rows")));
					if (results == null || results.size() == 0)
					{
						numResults = 0;
						pst.put("RESULTS", new Vector());
						setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_BUSINESS_SEARCH_NO_RECORD_FOUND", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
					}
					else
					{
						// store search results ...
						numResults = results.size();
						tds.setTotalRows(numResults);
						setRowGeneratorDOForNRows(numResults);
						pst.put("RESULTS", results);

						// ensure display from first page

            //// The following re-written fragment should be seriously tested.
						////CSpDataDrivenVisual vo =(CSpDataDrivenVisual)(getCurrNDPage().getCommonRepeated(tds.getVoName()));
						////tds.setCurrPageNdx(0);
						////vo.goToFirst();

            pgSourceofBusSearchrpSourceBusinessTiledView vo = ((pgSourceofBusSearchViewBean)getCurrNDPage()).getRpSourceBusiness();
            logger.debug("SOBSH@setupBeforePageGen::Repeated1: " + vo);
            logger.debug("SOBSH@setupBeforePageGen::Repeated1Name: " + vo.getName());

            tds.setCurrPageNdx(0);
            ////vo.goToFirst();

            ////Reset the primary model to the "before first" state and resets the display index.
            try
            {
              vo.resetTileIndex();
            }
            catch (ModelControlException mce)
            {
              logger.debug("SOBSH@setupBeforePageGeneration::Exception: " + mce);
            }
					}
				}
				catch(Exception e)
				{
					numResults = 0;
					setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
					logger.error("Exception @SourceBusinessSearchHandler.setupBeforePageGeneration: searching");
					logger.error(e.getMessage());
				}
			}
		}

		if (numResults == 0)
		{
			tds.setTotalRows(0);
			setRowGeneratorDOForNRows(0);
			pst.put("RESULTS", new Vector());
		}


		// special variable for tracking the row being generated (see handleViewSelectButton())
		pst.put("ROWNDX", new Integer(0));

    //-- FXLink Phase II --//
    //--> Add System Type combo-box
    //--> By Billy 29Oct2003
    // Populate the SystemType ComboBox
    populateSystemTypeComboBox();
    //================================
    //BILLYSTOP

	}


	/////////////////////////////////////////////////////////////////////////
	//
	// Determine if select button should be displayed
	//

	////public int handleViewSelectButton()
  public boolean handleViewSelectButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getPageDealId() <= 0) return false;


		// definitely not - usage no associated with a dea
		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");


		// trick to keep track of row being generated (cannot get from CSpHtmlOutputEvent
		// object - event that triggers this call!)

    //// Wrong iMT auto-conversion.
		////int rowNdx =((Integer)TypeConverter.asInt(pst.get("ROWNDX")));
		int rowNdx =(((Integer)pst.get("ROWNDX"))).intValue();

		pst.put("ROWNDX", new Integer(rowNdx + 1));

		rowNdx = getCursorAbsoluteNdx(tds, rowNdx);

		Vector results =(Vector) pst.get("RESULTS");

		int statusId =((SourceQueryResult) results.elementAt(rowNdx)).sourceProfileStatusId;

		if (statusId == Sc.PROFILE_STATUS_ACTIVE || statusId == Sc.PROFILE_STATUS_CAUTION)
        return true;

		return false;

	}


	/////////////////////////////////////////////////////////////////////////

	public void handleForwardButton()
	{
		////SessionState theSession = getTheSession();

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		handleForwardBackwardCommon(pg, tds, - 1, true);

		return;

	}


	/////////////////////////////////////////////////////////////////////////

	public void handleBackwardButton()
	{
		////SessionState theSession = getTheSession();

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		handleForwardBackwardCommon(pg, tds, - 1, false);

		return;

	}


	/////////////////////////////////////////////////////////////////////////
	// called to assign the source business selected (from search results) to the deal (in this page
	// is sub-page to a deal page)
	//
	//  - <srcbusNdx> is relative to the results being displayed on the the page, not absolute.

	public void handleSelectSourceBusiness(int srcbusNdx)
	{
		////SessionState theSession = getTheSession();

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		try
		{
			if (tds.getTotalRows() <= 0) return;
			tds.setRelSelectedRowNdx(srcbusNdx);
			int absNdx = tds.getAbsSelectedRowNdx();
			SourceQueryResult sqr =(SourceQueryResult)((Vector) pst.get("RESULTS")).elementAt(absNdx);
			int dealId = pg.getPageDealId();
			int copyId = pg.getPageDealCID();
			srk.beginTransaction();
			Deal deal = new Deal(srk, null);
			deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
			deal.setSourceOfBusinessProfileId(sqr.sourceOfBusinessProfileId);
			deal.setSourceFirmProfileId(sqr.sourceFirmProfileId);

      //-- FXLink Phase II --//
      //--> By Billy 20Nov2003
      //--> No need to check the Source SystemType
			deal.setSourceSystemMailBoxNBR(sqr.sourceBusinessMailboxNbr);
      deal.setSystemTypeId(sqr.sourceSystemTypeId);
      //==========================================

			deal.ejbStore();

			// navigate back to parent (default for a subpage so not 'next' page setting necessary
			// Call workflow ifSOB changed -- By Billy 29Nov2001
			workflowTrigger(0, srk, pg, null);
			navigateToNextPage();
			srk.commitTransaction();
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceOfBusinessSearchHandler.handleSelectSourceBusiness: assigning source to deal");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	public void handleDetailSourceBusiness(int srcbusNdx)
	{
		try
		{
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			if (tds.getTotalRows() <= 0) return;
			tds.setRelSelectedRowNdx(srcbusNdx);
			int absNdx = tds.getAbsSelectedRowNdx();
			logger.debug("BILLY ==> the absNdx = " + absNdx);
			SourceQueryResult sqr =(SourceQueryResult)((Vector) pst.get("RESULTS")).elementAt(absNdx);

			// Goto review SOB page
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SOB_REVIEW, true);
			Hashtable subPst = pgEntry.getPageStateTable();
			logger.debug("BILLY ==> the SOB_ID_PASSED_PARM = " + sqr.sourceOfBusinessProfileId);
			subPst.put(SOB_ID_PASSED_PARM, new String("" + sqr.sourceOfBusinessProfileId));
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceOfBusinessSearchHandler.handleDetailSourceBusiness: ");
			logger.error(e);
		}

	}


	/////////////////////////////////////////////////////////////////////////

	public void handleAddSourceBusiness()
	{
		try
		{
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SOB_ADD, true);

      //-- FXLink Phase II --//
      //--> Get and pass the selected System Type
      //--> By Billy 04Nov2003
      String systemTypeId = getCurrNDPage().getDisplayFieldValue("cbSystemType").toString();
      pgEntry.getPageStateTable().put(SourceBusinessEditHandler.SYSTYPE_ID_PASSED_PARM, systemTypeId);
      //=======================

			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceOfBusinessSearchHandler.handleAddSourceBusiness()");
			logger.error(e);
		}

	}


	/////////////////////////////////////////////////////////////////////////

	public void handleAddSourceFirm()
	{
		try
		{
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SFIRM_ADD, true);

      //-- FXLink Phase II --//
      //--> Get and pass the selected System Type
      //--> By Billy 04Nov2003
      String systemTypeId = getCurrNDPage().getDisplayFieldValue("cbSystemType").toString();
      pgEntry.getPageStateTable().put(SourceFirmEditHandler.SYSTYPE_ID_PASSED_PARM, systemTypeId);
      //=======================

			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceOfBusinessSearchHandler.handleAddSourceFirm()");
			logger.error(e);
		}

	}


	/////////////////////////////////////////////////////////////////////////

	public void handleBtSearchClickEvent()
	{
		////SessionState theSession = getTheSession();

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(EDIT_BUFFER);

		if (! isInputAvailable())
		{
			setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_BUSINESS_SEARCH_INPUT_REQUIRED", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);

			//pg.setPageStateTable(new Hashtable());
			return;
		}


		// Refresh tds to re-query
		tds.setRefresh(true);

		tds.setCriteria("");

		return;

	}


	/////////////////////////////////////////////////////////////////////////

	private boolean isSearchCriteria(EditFieldsBuffer efb)
	{
		boolean [ ] flags = getSearchCriteriaFlags(efb);

		int lim = flags.length;

		for(int i = 0;
		i < lim;
		++ i)
		{
			if (flags [ i ] == true) return true;
		}

		return false;

	}


	/////////////////////////////////////////////////////////////////////////
	//
	// Returns array as follows:
	//
	//  [0] - true/false - search by source of business criteria present
	//  [1] - true/false - search by source firm present
	//
	//

	private boolean[] getSearchCriteriaFlags(EditFieldsBuffer efb)
	{
		boolean flags [ ] = new boolean [ 2 ];

		flags [ 0 ] = isSourceOfBusinessSearchCriteria(efb);

		flags [ 1 ] = isFirmSearchCriteria(efb);

		return flags;

	}


	/////////////////////////////////////////////////////////////////////////

	private boolean isSourceOfBusinessSearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbSourceName");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbSourceCity");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("cbProvince");

    //--> Bug Fixed : the cbProvince value is IDs : By Billy 01April2003
		if (s != null && s.length() > 0) if (!s.equals("0")) return true;

		s = efb.getFieldValue("tbSourcePhoneNoPart1");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbSourcePhoneNoPart2");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbSourcePhoneNoPart3");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbSourceFaxPart1");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbSourceFaxPart2");

		if (s != null && s.length() > 0) return true;

		s = efb.getFieldValue("tbSourceFaxPart3");

		if (s != null && s.length() > 0) return true;

		return false;

	}


	/////////////////////////////////////////////////////////////////////////

	private boolean isInputAvailable()
	{
		ViewBean currNDPage = getCurrNDPage();

		Object val = null;

		val = currNDPage.getDisplayFieldValue("tbSourceFirm");

		logger.debug(" === Toni === tbSourceFirm == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("tbSourceName");

		logger.debug(" === Toni === tbSourceName == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("tbSourceCity");

		logger.debug(" === Toni === tbSourceCity == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("tbSourcePhoneNoPart1");

		logger.debug(" === Toni === tbSourcePhoneNoPart1 == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("tbSourcePhoneNoPart2");

		logger.debug(" === Toni === tbSourcePhoneNoPart2 == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("tbSourcePhoneNoPart3");

		logger.debug(" === Toni === tbSourcePhoneNoPart3 == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("tbSourceFaxPart1");

		logger.debug(" === Toni === tbSourceFaxPart1 == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("tbSourceFaxPart2");

		logger.debug(" === Toni === tbSourceFaxPart2 == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("tbSourceFaxPart3");

		logger.debug(" === Toni === tbSourceFaxPart3 == " + val.toString());

		if (val != null && val.toString().length() != 0) return true;

		val = currNDPage.getDisplayFieldValue("cbProvince");

		logger.debug(" === Toni === cbProvince == " + val.toString());

    //--> Bug Fixed : the cbProvince value is IDs : By Billy 01April2003
		if (val != null && val.toString().length() != 0) if (! val.toString().equals("0")) return true;

		return false;

	}


	/////////////////////////////////////////////////////////////////////////

	private boolean isFirmSearchCriteria(EditFieldsBuffer efb)
	{
		String s = efb.getFieldValue("tbSourceFirm");

		if (s != null && s.length() > 0) return true;

		return false;

	}

  //--> Add by Billy 17Sept2002
  public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat)
	{
		PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
 		Hashtable pst = pg.getPageStateTable();
 	  PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
    try
    {
        //--> So far we cannot find a way to manually set the Diaplay Offset of the TiledView here.
        //--> The reason is some necessary method is protected (e.g. setWebActionModelOffset()). The
        //--> work around is to create our own method (setCurrentDisplayOffset) in the TiledView class
        //--> (e.g. pgIWorkQueueRepeated1TiledView).  However, this method have to be implemented in all
        //--> TiledView which have Forward/Backward buttons.
        //--> Need to investigate more later  !!!!
        //--> By BILLY 22July2002
        //--Release2.1--//
        //--> Sync it in any case. -- By Billy 17Dec2002
        ((pgSourceofBusSearchrpSourceBusinessTiledView)theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
    }
    catch(Exception mce)
    {
      logger.debug("SOBSearchHandler@checkAndSyncDOWithCursorInfo::Exception: " + mce);
    }
	}

  //-- FXLink Phase II --//
  //--> Add System Type combo-box
  //--> By Billy 29Oct2003
  // Method to populate the SystemType combo Box
  // -- Display only the Types with ChannelMedia <> E
	private void populateSystemTypeComboBox()
	{
		ComboBox comboBox =(ComboBox)(getCurrNDPage().getDisplayField("cbSystemType"));
    OptionList option = (OptionList)comboBox.getOptions();
		option.clear();

		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			String sqlStr = "select systemtypeid, systemtypedescription from systemtype where channelmedia <> 'E'";
			JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sqlStr);

      int languageId = theSessionState.getLanguageId();
	  int institutionId = theSessionState.getDealInstitutionId();
      // Set Non-select to "" and id = -1
      option.add(0, "", "-1");
      //============================================

      String tmpLabel;
      int i=1;
      while (jExec.next(key))
      {
        //--> Get System Type Description from ResourceBundle
        tmpLabel = BXResources.getPickListDescription(institutionId, "SYSTEMTYPE", jExec.getString(key, 1), languageId);
        option.add(i, tmpLabel, jExec.getString(key, 1));
        i++;
			}
      jExec.closeData(key);

		}
		catch(Exception ex)
		{
			logger.error("SOBSearchHandler@populateSystemTypeComboBox::Exception:" + ex);
			return;
		}
	}

}


/**
 *
 *
 */
class SourceBusinessSearchEditBuffer extends EditFieldsBuffer
	implements Serializable
{
	private static String[][] buf={{"TextBox", "tbSourceName"},
                                  {"TextBox", "tbSourceFirm"},
                                  {"TextBox", "tbSourceCity"},
                                  {"ComboBox", "cbProvince"},
                                  {"TextBox", "tbSourcePhoneNoPart1"},
                                  {"TextBox", "tbSourcePhoneNoPart2"},
                                  {"TextBox", "tbSourcePhoneNoPart3"},
                                  {"TextBox", "tbSourceFaxPart1"},
                                  {"TextBox", "tbSourceFaxPart2"},
                                  {"TextBox", "tbSourceFaxPart3"},
                                  };
	private String[][] dataBuffer;


	/**
	 *
	 *
	 */
	public SourceBusinessSearchEditBuffer(String name, SysLogger logger)
	{
		super(name, logger);
		int len = buf.length;

		setNumberOfFields(len);

		dataBuffer = new String [ len ] [ 2 ];

		for(int i = 0;
		i < len;
		++ i)
		{
			for(int j = 0;
			j < 2;
			j ++) dataBuffer [ i ] [ j ] = "";
		}

	}


	/**
	 *
	 *
	 */
	public String getFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 1 ];

	}


	/**
	 *
	 *
	 */
	public String getDOFieldName(int fldNdx)
	{
		return "";

	}


	/**
	 *
	 *
	 */
	public String getFieldType(int fldNdx)
	{
		return buf [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getOriginalValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 1 ];

	}


	// --- //

	public void setFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 1 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setDOFieldName(int fldNdx, String value)
	{
		;

	}


	/**
	 *
	 *
	 */
	public void setFieldType(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setOriginalValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 1 ] = value;

	}

}


/**
 *
 *
 */
class SourceBusinessSearchResultRow extends EditFieldsBuffer
	implements Serializable
{
	private static String[][] buf={{"StaticText", "rpSourceBusiness/stSourceName"},
                                  {"StaticText", "rpSourceBusiness/stSourceFirmResult"},
                                  {"StaticText", "rpSourceBusiness/stStatus"},
                                  {"StaticText", "rpSourceBusiness/stCity"},
                                  {"StaticText", "rpSourceBusiness/stProvince"},
                                  {"StaticText", "rpSourceBusiness/stPhoneNumber"},
                                  {"StaticText", "rpSourceBusiness/stFaxNumber"},
                                  {"StaticText", "rpSourceBusiness/stSourceFirmSysType"},
                                  {"StaticText", "rpSourceBusiness/stSourceCategory"}};

	private String[][] dataBuffer;


	/**
	 *
	 *
	 */
	public SourceBusinessSearchResultRow(int institutionId, String name, SysLogger logger, int languageId, SourceQueryResult sqr)
	{
    //--Release2.1--//
    super(name, logger, languageId);
		int len = buf.length;

		setNumberOfFields(len);

		dataBuffer = new String [ len ] [ 2 ];

		for(int i = 0;
		i < len;
		++ i)
		{
			for(int j = 0;
			j < 2;
			j ++) dataBuffer [ i ] [ j ] = "";
		}

		setFieldValue(0, "" + sqr.source_fullname);

		setFieldValue(1, sqr.sourceFirmFullName);

		//--Release2.1--//
    //// Translation lookup.
    ////setFieldValue(2, sqr.sourceProfileStatus);
    String sbProfileStatus = BXResources.getPickListDescription(institutionId,  "PROFILESTATUS",
                                                              sqr.sourceProfileStatusId,
                                                              languageId);

    setFieldValue(2, sbProfileStatus);

		setFieldValue(3, sqr.source_city);

		//--Release2.1--//
    //// Translation lookup.
    ////setFieldValue(4, sqr.source_province);
    String provinceAbbr = BXResources.getPickListDescription(institutionId,  "PROVINCESHORTNAME",
                                                              sqr.source_province,
                                                              languageId);

    setFieldValue(4, provinceAbbr);

		setFieldValue(5, sqr.source_formphonenumandext);

		setFieldValue(6, sqr.source_formfaxnum);


		//		setFieldValue(5, formatPhoneNumber(sqr.source_phoneNumber));
		//		setFieldValue(6, formatPhoneNumber(sqr.source_faxNumber));
		// New fields added -- Billy 19Nov2001
    //--Release2.1--//
    //// Translation lookup.
    String sourceSystemType = BXResources.getPickListDescription(institutionId,  "SYSTEMTYPE",
                                                              sqr.sourceSystemTypeId,
                                                              languageId);

		setFieldValue(7, sourceSystemType);

    //--Release2.1--//
    //// Translation lookup.
    ////setFieldValue(8, sqr.source_BusinessCategoryDesc);
    String sbBusinessCategoryDesc = BXResources.getPickListDescription(institutionId,  "SOURCEOFBUSINESSCATEGORY",
                                                                      sqr.source_BusinessCategoryId,
                                                                      languageId);

    setFieldValue(8, sbBusinessCategoryDesc);

	}


	/**
	 *
	 *
	 */
	public void setFieldValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public String getFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 1 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldType(int fldNdx)
	{
		return buf [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 0 ];

	}


	// --- //
	// satisfy interface for limited implementation (used for screen population only!) ...

	public String getOriginalValue(int fldNdx)
	{
		return "";

	}


	/**
	 *
	 *
	 */
	public String getDOFieldName(int fldNdx)
	{
		return "";

	}


	/**
	 *
	 *
	 */
	public void setFieldName(int fldNdx, String value)
	{
		;

	}


	/**
	 *
	 *
	 */
	public void setDOFieldName(int fldNdx, String value)
	{
		;

	}


	/**
	 *
	 *
	 */
	public void setFieldType(int fldNdx, String value)
	{
		;

	}


	/**
	 *
	 *
	 */
	public void setOriginalValue(int fldNdx, String value)
	{
		;

	}

}

