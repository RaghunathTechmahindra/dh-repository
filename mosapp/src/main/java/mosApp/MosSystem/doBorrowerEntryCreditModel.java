package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doBorrowerEntryCreditModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCreditRefId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditRefId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCreditRefTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditRefTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCreditRefInstitutionName();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditRefInstitutionName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCreditRefAccountNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditRefAccountNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCreditRefCurrentBalance();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditRefCurrentBalance(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFCREDITREFID="dfCreditRefId";
	public static final String FIELD_DFCREDITREFTYPEID="dfCreditRefTypeId";
	public static final String FIELD_DFCREDITREFINSTITUTIONNAME="dfCreditRefInstitutionName";
	public static final String FIELD_DFCREDITREFACCOUNTNUMBER="dfCreditRefAccountNumber";
	public static final String FIELD_DFCREDITREFCURRENTBALANCE="dfCreditRefCurrentBalance";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

