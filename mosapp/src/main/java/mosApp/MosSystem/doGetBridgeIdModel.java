package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetBridgeIdModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeCopyId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBRIDGEID="dfBridgeId";
	public static final String FIELD_DFBRIDGEDEALID="dfBridgeDealId";
	public static final String FIELD_DFBRIDGECOPYID="dfBridgeCopyId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

