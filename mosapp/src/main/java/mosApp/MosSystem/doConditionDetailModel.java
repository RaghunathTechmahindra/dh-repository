package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doConditionDetailModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDocTrackId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDocTrackId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDocumentText();

	
	/**
	 * 
	 * 
	 */
	public void setDfDocumentText(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDocumentLabel();

	
	/**
	 * 
	 * 
	 */
	public void setDfDocumentLabel(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLanguage();

	
	/**
	 * 
	 * 
	 */
	public void setDfLanguage(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDOCTRACKID="dfDocTrackId";
	public static final String FIELD_DFDOCUMENTTEXT="dfDocumentText";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String FIELD_DFDOCUMENTTEXTLITE="dfDocumentTextLite";
	//================================================================
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFDOCUMENTLABEL="dfDocumentLabel";
	public static final String FIELD_DFLANGUAGE="dfLanguage";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

