package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase; 
/**
 * <p> Title: pgCreditDecisionViewBean</p>
 *
 * <p> Description: ViewBean for pgCreditDecision.jsp & pgCreditDecision_fr.jsp </p>
 *
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public class pgCreditDecisionViewBean extends ExpressViewBeanBase

{
	/**
	 *
	 *
	 */
	public pgCreditDecisionViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * createChild
	 * 	Creates child for JATO framework
	 * 
	 * @param name String<br>
	 * @return View
	 *
	 */
	protected View createChild(String name)
	{
		View superReturn = super.createChild(name);  
		if (superReturn != null) {  
		return superReturn;  
		} else if (name.equals(CHILD_TBDEALID)) 
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);
        //Modified to set NonSelected Label from CHILD_CBPAGENAMES_NONSELECTED_LABEL
        child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
			  child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALID))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALID,
				doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
				CHILD_STDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORRFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUS,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
				CHILD_STDEALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUSDATE,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
				CHILD_STDEALSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEFIRM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCEFIRM,
				doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
				CHILD_STSOURCEFIRM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCE,
				doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
				CHILD_STSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLOB))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STLOB,
				doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
				CHILD_STLOB_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALTYPE,
				doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
				CHILD_STDEALTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALPURPOSE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALPURPOSE,
				doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
				CHILD_STDEALPURPOSE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPURCHASEPRICE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPURCHASEPRICE,
				doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
				CHILD_STPURCHASEPRICE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTTERM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPMTTERM,
				doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
				CHILD_STPMTTERM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESTCLOSINGDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STESTCLOSINGDATE,
				doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
				CHILD_STESTCLOSINGDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSPECIALFEATURE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSPECIALFEATURE,
				doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
				CHILD_STSPECIALFEATURE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTOTALLOANAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STTOTALLOANAMOUNT,
				doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
				CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,

				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
    //***** Change by NBC/PP Implementation Team - Version 1.1 - START *****//
		else
			if (name.equals(CHILD_STVIEWONLYTAG))
			{
				StaticTextField child = new StaticTextField(this,
					getDefaultModel(),
					CHILD_STVIEWONLYTAG,
					CHILD_STVIEWONLYTAG,
					CHILD_STVIEWONLYTAG_RESET_VALUE,
					null);
				return child;
			}		
		else
		if (name.equals(CHILD_HDRESPONSEID)) 
		{
			HiddenField child =
				new HiddenField(this, getdoAdjudicationResponseBNCModel(), 
						CHILD_HDRESPONSEID, 
						doAdjudicationResponseBNCModel.FIELD_DFRESPONSEID, 
						CHILD_HDRESPONSEID_RESET_VALUE, null);
			return child;
		}		
		else
		if (name.equals(CHILD_TXGLOBALCREDITBUREAU)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationResponseBNCModel(), 
					CHILD_TXGLOBALCREDITBUREAU, 
					doAdjudicationResponseBNCModel.FIELD_DFGLOBALUSEDCREDITBUREAUNAME, 
					CHILD_TXGLOBALCREDITBUREAU_RESET_VALUE, null);
			
			return child;
		}
		else
		if (name.equals(CHILD_TXGLOBALINTERNALSCORE)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationResponseBNCModel(), 
					CHILD_TXGLOBALINTERNALSCORE, 
					doAdjudicationResponseBNCModel.FIELD_DFGLOBALINTERNALSCORE, 
					CHILD_TXGLOBALINTERNALSCORE_RESET_VALUE, null);			
			
			return child;
		}
		else
		if (name.equals(CHILD_TXLASTUPDATEDDATE)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXLASTUPDATEDDATE, 
				doAdjudicationResponseBNCModel.FIELD_DFRESPONSEDATE, 
				CHILD_TXLASTUPDATEDDATE_RESET_VALUE, null);				
			
			return child;
		}
		else
		if (name.equals(CHILD_TXGLOBALCREDITBUREAUSCORE)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXGLOBALCREDITBUREAUSCORE, 
				doAdjudicationResponseBNCModel.FIELD_DFGLOBALCREDITBUREAUSCORE, 
				CHILD_TXGLOBALCREDITBUREAUSCORE_RESET_VALUE, null);	
			
			return child;
		}
		else
		if (name.equals(CHILD_TXCREDITDECISIONSTATUS)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXCREDITDECISIONSTATUS, 
				doAdjudicationResponseBNCModel.FIELD_DFREQUESTSTATUSDESC, 
				CHILD_TXCREDITDECISIONSTATUS_RESET_VALUE, null);	
			
			return child;
		}
		else
		if (name.equals(CHILD_HDDEALSTATUSCATEGORYID)) 
		{
			HiddenField child =
				new HiddenField(this, getdoAdjudicationMainBNCModel(), 
						CHILD_HDDEALSTATUSCATEGORYID, 
						doAdjudicationMainBNCModel.FIELD_DFDEALSTATUSCATEGORYID, 
						CHILD_HDDEALSTATUSCATEGORYID_RESET_VALUE, null);
			return child;
		}	
		else
		if (name.equals(CHILD_HDCREDITDECISIONREQUESTSTATUSID)) 
		{
			HiddenField child =
				new HiddenField(this, getdoAdjudicationResponseBNCModel(), 
						CHILD_HDCREDITDECISIONREQUESTSTATUSID, 
						doAdjudicationResponseBNCModel.FIELD_DFREQUESTSTATUSID, 
						CHILD_HDCREDITDECISIONREQUESTSTATUSID_RESET_VALUE, null);
			return child;
		}			
		else
		if (name.equals(CHILD_HDREQUESTID)) 
		{
			HiddenField child =
				new HiddenField(this, getdoAdjudicationResponseBNCModel(), 
						CHILD_HDREQUESTID, 
						doAdjudicationResponseBNCModel.FIELD_DFREQUESTID, 
						CHILD_HDREQUESTID_RESET_VALUE, null);
			return child;
		}	
		else
		if (name.equals(CHILD_TXDECISIONMESSAGES)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationResponseBNCDecisionMessagesModel(), 
					CHILD_TXDECISIONMESSAGES, 
					doAdjudicationResponseBNCDecisionMessagesModel.FIELD_DFDECISIONMESSAGES, 
					CHILD_TXDECISIONMESSAGES_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXGLOBALRISKRATING)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXGLOBALRISKRATING, 
				doAdjudicationResponseBNCModel.FIELD_DFGLOBALRISKRATING, 
				CHILD_TXGLOBALRISKRATING_RESET_VALUE, null);				
			
			return child;
		}
		else
		if (name.equals(CHILD_TXCREDITDECISION)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXCREDITDECISION, 
				doAdjudicationResponseBNCModel.FIELD_DFADJUDICATIONDESC, 
				CHILD_TXCREDITDECISION_RESET_VALUE, null);				
			
			return child;
		}
		else
		if (name.equals(CHILD_TXPOSTEDRATE)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXPOSTEDRATE, 
					doAdjudicationMainBNCModel.FIELD_DFPOSTEDRATE, 
			CHILD_TXPOSTEDRATE_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXMONTHLYPAYMENT)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXMONTHLYPAYMENT, 
					doAdjudicationMainBNCModel.FIELD_DFPANDIPAYMENTAMOUNTMONTHLY, 
			CHILD_TXMONTHLYPAYMENT_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXDOWNPAYMENT)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXDOWNPAYMENT, 
					doAdjudicationMainBNCModel.FIELD_DFDOWNPAYMENTAMOUNT, 
			CHILD_TXDOWNPAYMENT_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXLOCATION)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXLOCATION, 
					doAdjudicationMainBNCModel.FIELD_DFLOCATION, 
			CHILD_TXLOCATION_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXACTUALPAYMENTTERM)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXACTUALPAYMENTTERM, 
					doAdjudicationMainBNCModel.FIELD_DFPAYMENTTERM, 
			CHILD_TXACTUALPAYMENTTERM_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXPRODUCT)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXPRODUCT, 
				doAdjudicationResponseBNCModel.FIELD_DFPRODUCTCODE, 
				CHILD_TXPRODUCT_RESET_VALUE, null);	
			
			return child;
		}
		else
		if (name.equals(CHILD_TXLTV)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXLTV, 
					doAdjudicationMainBNCModel.FIELD_DFCOMBINEDLTV, 
			CHILD_TXLTV_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXOCCUPIEDBY)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXOCCUPIEDBY, 
					doAdjudicationMainBNCModel.FIELD_DFOCCUPANCYTYPE, 
			CHILD_TXOCCUPIEDBY_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXAMORTIZATIONTERM)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXAMORTIZATIONTERM, 
					doAdjudicationMainBNCModel.FIELD_DFAMORTIZATION, 
			CHILD_TXAMORTIZATIONTERM_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYTYPE)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXPROPERTYTYPE, 
					doAdjudicationMainBNCModel.FIELD_DFPROPERTYTYPE, 
			CHILD_TXPROPERTYTYPE_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXMORTGAGEINSURER)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXMORTGAGEINSURER, 
					doAdjudicationMainBNCModel.FIELD_DFMORTGAGEINSURER, 
			CHILD_TXMORTGAGEINSURER_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXPRIMARYAPPLICANTRISK)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXPRIMARYAPPLICANTRISK, 
				doAdjudicationResponseBNCModel.FIELD_DFPRIMARYAPPLICANTRISK, 
				CHILD_TXPRIMARYAPPLICANTRISK_RESET_VALUE, null);	
			
			return child;
		}
		else
		if (name.equals(CHILD_TXMARKETRISK)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXMARKETRISK, 
				doAdjudicationResponseBNCModel.FIELD_DFMARKETRISK, 
				CHILD_TXMARKETRISK_RESET_VALUE, null);	
			
			return child;
		}
		else
		if (name.equals(CHILD_TXMISTATUS)) 
		{
			TextField child =
				new TextField(this, getdoAdjudicationMainBNCModel(), 
					CHILD_TXMISTATUS, 
					doAdjudicationMainBNCModel.FIELD_DFMISTATUS, 
			CHILD_TXMISTATUS_RESET_VALUE, null);
			return child;
		}
		else
		if (name.equals(CHILD_TXSECONDARYAPPLICANTRISK)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXSECONDARYAPPLICANTRISK, 
				doAdjudicationResponseBNCModel.FIELD_DFSECONDARYAPPLICANTRISK, 
				CHILD_TXSECONDARYAPPLICANTRISK_RESET_VALUE, null);	
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYRISK)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXPROPERTYRISK, 
				doAdjudicationResponseBNCModel.FIELD_DFPROPERTYRISK, 
				CHILD_TXPROPERTYRISK_RESET_VALUE, null);	
			return child;
		}
		else
		if (name.equals(CHILD_TXMIDECISION)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXMIDECISION, 
				doAdjudicationResponseBNCModel.FIELD_DFMIDECISION, 
				CHILD_TXMIDECISION_RESET_VALUE, null);	
			return child;
		}
		else
		if (name.equals(CHILD_TXNEIGHBOURHOODRISK)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXNEIGHBOURHOODRISK, 
				doAdjudicationResponseBNCModel.FIELD_DFNEIGHBOURHOODRISK, 
				CHILD_TXNEIGHBOURHOODRISK_RESET_VALUE, null);	
			return child;
		}
		else
		if (name.equals(CHILD_TXAMOUNTINSURED)) 
		{
			TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(), 
				CHILD_TXGLOBALINTERNALSCORE, 
				doAdjudicationResponseBNCModel.FIELD_DFAMOUNTINSURED, 
				CHILD_TXGLOBALINTERNALSCORE_RESET_VALUE, null);	
			return child;
		}

	
	else
	if (name.equals(CHILD_TXCOMBINEDGDS))
	{
		TextField child =
		new TextField(this, getdoAdjudicationMainBNCModel(),
				CHILD_TXCOMBINEDGDS,
				doAdjudicationMainBNCModel.FIELD_DFCOMBINEDGDSBORROWER,
		CHILD_TXCOMBINEDGDS_RESET_VALUE, null);
		return child;
	}
	
	else
	if (name.equals(CHILD_TXCOMBINEDTDS))
	{
		TextField child =
		new TextField(this, getdoAdjudicationMainBNCModel(),
				CHILD_TXCOMBINEDTDS,
				doAdjudicationMainBNCModel.FIELD_DFCOMBINEDTDSBORROWER,
		CHILD_TXCOMBINEDTDS_RESET_VALUE, null);
		return child;
	}
	
	else
	if (name.equals(CHILD_TXTOTALNETWORTH))
	{
		TextField child =
			new TextField(this, getdoAdjudicationResponseBNCModel(),
					CHILD_TXTOTALNETWORTH,
					doAdjudicationResponseBNCModel.FIELD_DFGLOBALNETWORTH,
			CHILD_TXTOTALNETWORTH_RESET_VALUE, null);
		return child;
	}
	
	else
	if (name.equals(CHILD_TXTOTALINCOME))
	{
		TextField child =
		new TextField(this, getdoAdjudicationMainBNCModel(),
				CHILD_TXTOTALINCOME,
				doAdjudicationMainBNCModel.FIELD_DFTOTALANNUALINCOME,
		CHILD_TXTOTALINCOME_RESET_VALUE, null);
		return child;
	}
	
	else
	if (name.equals(CHILD_TXTOTALOTHERINCOME))
	{
		TextField child =
		new TextField(this, getdoAdjudicationMainBNCModel(),
				CHILD_TXTOTALOTHERINCOME,
				doAdjudicationMainBNCModel.FIELD_DFTOTALOTHERINCOME,
		CHILD_TXTOTALOTHERINCOME_RESET_VALUE, null);
		return child;
	}

	else
	if (name.equals(CHILD_TXTOTALCOMBINEDINCOME))
	{
		TextField child =
		new TextField(this, getdoAdjudicationMainBNCModel(),
				CHILD_TXTOTALCOMBINEDINCOME,
				doAdjudicationMainBNCModel.FIELD_DFTOTALCOMBINEDINCOME,
		CHILD_TXTOTALCOMBINEDINCOME_RESET_VALUE, null);
		return child;
	}
	
	else
	if (name.equals(CHILD_BTGETCREDITDECISION))
	{
		Button child =
		new Button(this, getDefaultModel(),
				CHILD_BTGETCREDITDECISION,
				CHILD_BTGETCREDITDECISION,
		CHILD_BTGETCREDITDECISION_RESET_VALUE, null);
		child.setValue( "Get Credit Decision");
		return child;
	}
	else
		if (name.equals(CHILD_REPEATED1))
		{
			pgCreditDecisionRepeated1TiledView child = new pgCreditDecisionRepeated1TiledView(this,
				CHILD_REPEATED1);
			return child;
		}		
    //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//

		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 * resetChildren
	 * 	Reset children values for the JATO framework
	 *
	 * @param none<br>
	 * @return none<br>
	 *
	 */
	public void resetChildren()
	{
		super.resetChildren();
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
		getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
		getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
		getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
		getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
		getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
		getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
		getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
		getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
		getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
		getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
		getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
		getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);

		//-- Credit Decision fields --//
		getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
		getHdResponseId().setValue(CHILD_HDRESPONSEID_RESET_VALUE);
		getTxGlobalCreditBureau().setValue(CHILD_TXGLOBALCREDITBUREAU_RESET_VALUE);
		getTxGlobalInternalScore().setValue(CHILD_TXGLOBALINTERNALSCORE_RESET_VALUE);
		getTxLastUpdatedDate().setValue(CHILD_TXLASTUPDATEDDATE_RESET_VALUE);
		getTxGlobalCreditBureauScore().setValue(CHILD_TXGLOBALCREDITBUREAUSCORE_RESET_VALUE);
		getTxCreditDecisionStatus().setValue(CHILD_TXCREDITDECISIONSTATUS_RESET_VALUE);
		getHdRequestId().setValue(CHILD_HDREQUESTID_RESET_VALUE);
		getHdDealStatusCategoryId().setValue(CHILD_HDDEALSTATUSCATEGORYID_RESET_VALUE);
		getHdCreditDecisionRequestStatusId().setValue(CHILD_HDCREDITDECISIONREQUESTSTATUSID_RESET_VALUE);
		getTxDecisionMessages().setValue(CHILD_TXDECISIONMESSAGES_RESET_VALUE);
		getTxGlobalRiskRating().setValue(CHILD_TXGLOBALRISKRATING_RESET_VALUE);
		getTxCreditDecision().setValue(CHILD_TXCREDITDECISION_RESET_VALUE);
		getTxPostedRate().setValue(CHILD_TXPOSTEDRATE_RESET_VALUE);
		getTxMonthlyPayment().setValue(CHILD_TXMONTHLYPAYMENT_RESET_VALUE);
		getTxDownPayment().setValue(CHILD_TXDOWNPAYMENT_RESET_VALUE);
		getTxLocation().setValue(CHILD_TXLOCATION_RESET_VALUE);
		getTxActualPaymentTerm().setValue(CHILD_TXACTUALPAYMENTTERM_RESET_VALUE);
		getTxProduct().setValue(CHILD_TXPRODUCT_RESET_VALUE);
		getTxLTV().setValue(CHILD_TXLTV_RESET_VALUE);
		getTxOccupiedBy().setValue(CHILD_TXOCCUPIEDBY_RESET_VALUE);
		getTxAmortizationTerm().setValue(CHILD_TXAMORTIZATIONTERM_RESET_VALUE);
		getTxPropertyType().setValue(CHILD_TXPROPERTYTYPE_RESET_VALUE);
		getTxMortgageInsurer().setValue(CHILD_TXMORTGAGEINSURER_RESET_VALUE);
		getTxPrimaryApplicantRisk().setValue(CHILD_TXPRIMARYAPPLICANTRISK_RESET_VALUE);
		getTxMarketRisk().setValue(CHILD_TXMARKETRISK_RESET_VALUE);
		getTxMIStatus().setValue(CHILD_TXMISTATUS_RESET_VALUE);
		getTxSecondaryApplicantRisk().setValue(CHILD_TXSECONDARYAPPLICANTRISK_RESET_VALUE);
		getTxPropertyRisk().setValue(CHILD_TXPROPERTYRISK_RESET_VALUE);
		getTxMIDecision().setValue(CHILD_TXMIDECISION_RESET_VALUE);
		getTxNeighbourhoodRisk().setValue(CHILD_TXNEIGHBOURHOODRISK_RESET_VALUE);
		getTxAmountInsured().setValue(CHILD_TXAMOUNTINSURED_RESET_VALUE);


		getTxCombinedGDS().setValue(CHILD_TXCOMBINEDGDS_RESET_VALUE);

		getTxCombinedTDS().setValue(CHILD_TXCOMBINEDTDS_RESET_VALUE);
		
		getTxTotalNetWorth().setValue(CHILD_TXTOTALNETWORTH_RESET_VALUE);
		
		getTxTotalIncome().setValue(CHILD_TXTOTALINCOME_RESET_VALUE);
		
		getTxTotalOtherIncome().setValue(CHILD_TXTOTALOTHERINCOME_RESET_VALUE);
		
		getTxTotalCombinedIncome().setValue(CHILD_TXTOTALCOMBINEDINCOME_RESET_VALUE);
		
		getBtGetCreditDecision().setValue(CHILD_BTGETCREDITDECISION_RESET_VALUE);
		
		getRepeated1().resetChildren();
		//-- end Credit Decision fields --//

	}


	/**
	 * registerChildren
	 * 	Register children with the JATO framework
	 *
	 * @param none<br>
	 * @return none<br>
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren(); 
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_STDEALID,StaticTextField.class);
		registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
		registerChild(CHILD_STSOURCE,StaticTextField.class);
		registerChild(CHILD_STLOB,StaticTextField.class);
		registerChild(CHILD_STDEALTYPE,StaticTextField.class);
		registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
		registerChild(CHILD_STPURCHASEPRICE,StaticTextField.class);
		registerChild(CHILD_STPMTTERM,StaticTextField.class);
		registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
		registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
    registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG,Button.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);

		//-- Credit decision specific fields --//
		registerChild(CHILD_STVIEWONLYTAG, StaticTextField.class);
		registerChild(CHILD_HDRESPONSEID, HiddenField.class);
		registerChild(CHILD_TXGLOBALCREDITBUREAU, TextField.class);
		registerChild(CHILD_TXGLOBALINTERNALSCORE, TextField.class);
		registerChild(CHILD_TXLASTUPDATEDDATE, TextField.class);
		registerChild(CHILD_TXGLOBALCREDITBUREAUSCORE, TextField.class);
		registerChild(CHILD_TXCREDITDECISIONSTATUS, TextField.class);
		registerChild(CHILD_HDREQUESTID, HiddenField.class);
		registerChild(CHILD_HDDEALSTATUSCATEGORYID, HiddenField.class);
		registerChild(CHILD_HDCREDITDECISIONREQUESTSTATUSID, HiddenField.class);
		registerChild(CHILD_TXDECISIONMESSAGES, TextField.class);
		registerChild(CHILD_TXGLOBALRISKRATING, TextField.class);
		registerChild(CHILD_TXCREDITDECISION, TextField.class);
		registerChild(CHILD_TXPOSTEDRATE, TextField.class);
		registerChild(CHILD_TXMONTHLYPAYMENT, TextField.class);
		registerChild(CHILD_TXDOWNPAYMENT, TextField.class);
		registerChild(CHILD_TXLOCATION, TextField.class);
		registerChild(CHILD_TXACTUALPAYMENTTERM, TextField.class);
		registerChild(CHILD_TXPRODUCT, TextField.class);
		registerChild(CHILD_TXLTV, TextField.class);
		registerChild(CHILD_TXOCCUPIEDBY, TextField.class);
		registerChild(CHILD_TXAMORTIZATIONTERM, TextField.class);
		registerChild(CHILD_TXPROPERTYTYPE, TextField.class);
		registerChild(CHILD_TXMORTGAGEINSURER, TextField.class);
		registerChild(CHILD_TXPRIMARYAPPLICANTRISK, TextField.class);
		registerChild(CHILD_TXMARKETRISK, TextField.class);
		registerChild(CHILD_TXMISTATUS, TextField.class);
		registerChild(CHILD_TXSECONDARYAPPLICANTRISK, TextField.class);
		registerChild(CHILD_TXPROPERTYRISK, TextField.class);
		registerChild(CHILD_TXMIDECISION, TextField.class);
		registerChild(CHILD_TXNEIGHBOURHOODRISK, TextField.class);
		registerChild(CHILD_TXAMOUNTINSURED, TextField.class);

		
		registerChild(CHILD_TXCOMBINEDGDS, TextField.class);
		
		registerChild(CHILD_TXCOMBINEDTDS, TextField.class);
		
		registerChild(CHILD_TXTOTALNETWORTH, TextField.class);

		registerChild(CHILD_TXTOTALINCOME, TextField.class);

		registerChild(CHILD_TXTOTALOTHERINCOME, TextField.class);

		registerChild(CHILD_TXTOTALCOMBINEDINCOME, TextField.class);
		
		registerChild(CHILD_BTGETCREDITDECISION, Button.class);

		registerChild(CHILD_REPEATED1,pgCreditDecisionRepeated1TiledView.class);
		//-- end Credit Decision specificfields --//

	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealSummarySnapShotModel());				
        //// Put your own additional Models if necessary.
				modelList.add(getdoAdjudicationMainBNCModel());
				modelList.add(getdoAdjudicationResponseBNCModel());
//				modelList.add(getdoAdjudicationResponseBNCDecisionMessagesModel());
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * beginDisplay
	 *
	 * @param event Display Event <br>
	 * @return none
	 *
	 * @version 1.1 <br>
	 * Date: 07/26/2006<br>
	 * Author: NBC/PP Implementation Team <br>
	 * Change: <br>
	 * 	Added population of credit bureau option lists
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    cbPageNamesOptions.populate(getRequestContext());
    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    handler.pageSaveState();
    super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatePageDisplayFields();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends OptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//


	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleGoPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(0);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(1);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(2);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(3);

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(4);

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(5);

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(6);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(7);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(8);

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(9);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealId()
	{
		return (StaticTextField)getChild(CHILD_STDEALID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrFirstName()
	{
		return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatus()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceFirm()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSource()
	{
		return (StaticTextField)getChild(CHILD_STSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLOB()
	{
		return (StaticTextField)getChild(CHILD_STLOB);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealType()
	{
		return (StaticTextField)getChild(CHILD_STDEALTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealPurpose()
	{
		return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPurchasePrice()
	{
		return (StaticTextField)getChild(CHILD_STPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmtTerm()
	{
		return (StaticTextField)getChild(CHILD_STPMTTERM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEstClosingDate()
	{
		return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSpecialFeature()
	{
		return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
	}


	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSubmit();

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayWorkQueue();

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleChangePassword();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealHistory();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}



	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealNotes();

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDealSearch();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSignOff();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTotalLoanAmount()
	{
		return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handlePrevTaskPage();

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleNextTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateTaskName();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public boolean beginStDueDateDisplay(ChildDisplayEvent event)
	{
		////Object value = getStDueDate().getValue();

    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.convertToUserTimeZone(this, "stDueDate");
		handler.pageSaveState();

		return true;

	}

	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *

	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}

	/** getStViewOnlyTag
	 * Returns the <code>StViewOnlyTag</code> child View component
	 *
	 * @version 1.0 (Initial Version - 17 Oct 2006)<br>
	 */
	public StaticTextField getStViewOnlyTag() {
	    return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
	}
	
	/** getHdResponseId
	 * Returns the <code>hdResponseId</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public HiddenField getHdResponseId() {
	    return (HiddenField)getChild(CHILD_HDRESPONSEID);
	}
	
	/** getTxGlobalCreditBureau
	 * Returns the <code>txGlobalCreditBureau</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxGlobalCreditBureau() {
	    return (TextField)getChild(CHILD_TXGLOBALCREDITBUREAU);
	}

	/** getTxGlobalInternalScore
	 * Returns the <code>txGlobalInternalScore</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxGlobalInternalScore() {
	    return (TextField)getChild(CHILD_TXGLOBALINTERNALSCORE);
	}

	/** getTxLastUpdatedDate
	 * Returns the <code>txLastUpdatedDate</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxLastUpdatedDate() {
	    return (TextField)getChild(CHILD_TXLASTUPDATEDDATE);
	}

	/** getTxGlobalCreditBureauScore
	 * Returns the <code>txGlobalCreditBureauScore</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxGlobalCreditBureauScore() {
	    return (TextField)getChild(CHILD_TXGLOBALCREDITBUREAUSCORE);
	}

	/** getTxCreditDecisionStatus
	 * Returns the <code>txCreditDecisionStatus</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxCreditDecisionStatus() {
	    return (TextField)getChild(CHILD_TXCREDITDECISIONSTATUS);
	}

	/** getHdCreditDecisionRequestStatusId
	 * Returns the <code>hdCreditDecisionRequestStatusId</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public HiddenField getHdRequestId() {
	    return (HiddenField)getChild(CHILD_HDREQUESTID);
	}
	
	/** getHdCreditDecisionRequestStatusId
	 * Returns the <code>hdCreditDecisionRequestStatusId</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public HiddenField getHdCreditDecisionRequestStatusId() {
	    return (HiddenField)getChild(CHILD_HDCREDITDECISIONREQUESTSTATUSID);
	}

	/** getHdDealStatusCategoryId
	 * Returns the <code>hdDealStatusCategoryId</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public HiddenField getHdDealStatusCategoryId() {
	    return (HiddenField)getChild(CHILD_HDDEALSTATUSCATEGORYID);
	}

	/** getTxDecisionMessages
	 * Returns the <code>txDecisionMessages</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxDecisionMessages() {
	    return (TextField)getChild(CHILD_TXDECISIONMESSAGES);
	}

	/** getTxGlobalRiskRating
	 * Returns the <code>txGlobalRiskRating</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxGlobalRiskRating() {
	    return (TextField)getChild(CHILD_TXGLOBALRISKRATING);
	}

	/** getTxCreditDecision
	 * Returns the <code>txCreditDecision</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxCreditDecision() {
	    return (TextField)getChild(CHILD_TXCREDITDECISION);
	}

	/** getTxPostedRate
	 * Returns the <code>txPostedRate</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 * @version 1.2 <br> 
	 * Date: 18/10/2006 <br>
	 * Author: GCD Implementation Team <br>
	 * Change: <br> 
	 *	Renamed method from getTxPostedInterestRate to getTxPostedRate
	 */
	public TextField getTxPostedRate() {
	    return (TextField)getChild(CHILD_TXPOSTEDRATE);
	}

	/** getTxMonthlyPayment
	 * Returns the <code>txMonthlyPayment</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxMonthlyPayment() {
	    return (TextField)getChild(CHILD_TXMONTHLYPAYMENT);
	}

	/** getTxDownPayment
	 * Returns the <code>txDownPayment</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxDownPayment() {
	    return (TextField)getChild(CHILD_TXDOWNPAYMENT);
	}

	/** getTxLocation
	 * Returns the <code>txLocation</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxLocation() {
	    return (TextField)getChild(CHILD_TXLOCATION);
	}

	/** getTxActualPaymentTerm
	 * Returns the <code>txActualPaymentTerm</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxActualPaymentTerm() {
	    return (TextField)getChild(CHILD_TXACTUALPAYMENTTERM);
	}

	/** getTxProduct
	 * Returns the <code>txProduct</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxProduct() {
	    return (TextField)getChild(CHILD_TXPRODUCT);
	}

	/** getTxLTV
	 * Returns the <code>txLTV</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxLTV() {
	    return (TextField)getChild(CHILD_TXLTV);
	}

	/** getTxOccupiedBy
	 * Returns the <code>txOccupiedBy</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxOccupiedBy() {
	    return (TextField)getChild(CHILD_TXOCCUPIEDBY);
	}

	/** getTxAmortizationTerm
	 * Returns the <code>txAmortizationTerm</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxAmortizationTerm() {
	    return (TextField)getChild(CHILD_TXAMORTIZATIONTERM);
	}

	/** getTxPropertyType
	 * Returns the <code>txPropertyType</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxPropertyType() {
	    return (TextField)getChild(CHILD_TXPROPERTYTYPE);
	}

	/** getTxMortgageInsurer
	 * Returns the <code>txMortgageInsurer</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxMortgageInsurer() {
	    return (TextField)getChild(CHILD_TXMORTGAGEINSURER);
	}

	/** getTxPrimaryApplicantRisk
	 * Returns the <code>txPrimaryApplicantRisk</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxPrimaryApplicantRisk() {
	    return (TextField)getChild(CHILD_TXPRIMARYAPPLICANTRISK);
	}

	/** getTxPrimaryApplicantRisk
	 * Returns the <code>txMarketRisk</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxMarketRisk() {
	    return (TextField)getChild(CHILD_TXMARKETRISK);
	}


	/** getTxMIStatus
	 * Returns the <code>txMIStatus</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxMIStatus() {
	    return (TextField)getChild(CHILD_TXMISTATUS);
	}

	/** getTxSecondaryApplicantRisk
	 * Returns the <code>txSecondaryApplicantRisk</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxSecondaryApplicantRisk() {
	    return (TextField)getChild(CHILD_TXSECONDARYAPPLICANTRISK);
	}

	/** getTxPropertyRisk
	 * Returns the <code>txPropertyRisk</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxPropertyRisk() {
	    return (TextField)getChild(CHILD_TXPROPERTYRISK);
	}

	/** getTxMIDecision
	 * Returns the <code>txMIDecision</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxMIDecision() {
	    return (TextField)getChild(CHILD_TXMIDECISION);
	}

	/** getTxNeighbourhoodRisk
	 * Returns the <code>txNeighbourhoodRisk</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxNeighbourhoodRisk() {
	    return (TextField)getChild(CHILD_TXNEIGHBOURHOODRISK);
	}

	/** getTxAmountInsured
	 * Returns the <code>txAmountInsured</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxAmountInsured() {
	    return (TextField)getChild(CHILD_TXAMOUNTINSURED);
	}

	/** getTxCombinedGDSChild
	 * Returns the <code>txCombinedGDS</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxCombinedGDS() {
	    return (TextField)getChild(CHILD_TXCOMBINEDGDS);
	}

	/** getTxCombinedTDSChild
	 * Returns the <code>txCombinedTDS</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxCombinedTDS() {
	    return (TextField)getChild(CHILD_TXCOMBINEDTDS);
	}



	/** getTxTotalNetWorthChild
	 * Returns the <code>txTotalNetWorth</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxTotalNetWorth() {
	    return (TextField)getChild(CHILD_TXTOTALNETWORTH);
	}


	/** getTxTotalIncomeChild
	 * Returns the <code>txTotalIncome</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxTotalIncome() {
	    return (TextField)getChild(CHILD_TXTOTALINCOME);
	}


	/** getTxTotalOtherIncomeChild
	 * Returns the <code>txTotalOtherIncome</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxTotalOtherIncome() {
	    return (TextField)getChild(CHILD_TXTOTALOTHERINCOME);
	}


	/** getTxTotalCombinedIncomeChild
	 * Returns the <code>txTotalCombinedIncome</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public TextField getTxTotalCombinedIncome() {
	    return (TextField)getChild(CHILD_TXTOTALCOMBINEDINCOME);
	}




	/** getBtGetCreditDecisionChild
	 * Returns the <code>btGetCreditDecision</code> child View component
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public Button getBtGetCreditDecision() {
	    return (Button)getChild(CHILD_BTGETCREDITDECISION);
	}

	/** getBtGetCreditDecisionChild
	 * Handles the <code>btGetCreditDecision</code> request
	 *
	 * @version 1.0 (Initial Version - 26 Jul 2006)<br>
	 */
	public void handleBtGetCreditDecisionRequest(RequestInvocationEvent event)
	                                  throws ServletException, IOException
	{

	    CreditDecisionHandler handler = (CreditDecisionHandler) this.handler.cloneSS();

	    handler.preHandlerProtocol(this);
	    handler.handleGetCreditDecision();
	    handler.postHandlerProtocol();

	}

	/**
	 *
	 *
	 */
	public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
	  	CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		String rc = handler.displayViewOnlyTag();

		handler.pageSaveState();

   return rc;

	}	
	/**
	 * endBtGetCreditDecisionDisplay
	 * 	Hides Get Credit Decision button if request has been submitted
	 * 
	 * @param event
	 * @return
	 */
	public String endBtGetCreditDecisionDisplay(ChildContentDisplayEvent event)
	{
	    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();//
		handler.pageGetState(this.getParentViewBean());

	    boolean rc = true; //default: generate.

	    // check if page is editable 
	    rc = handler.displaySubmitButton();
	    
	    // page is editable, check other conditions 
	    if (rc == true)
	    rc = handler.handleViewRemoveGetCreditDecisionButton();
	    
			handler.pageSaveState();

		if(rc == true)
			return event.getContent();
	    else
	    	return "";
	}
	
	  
		/**
		 *
		 *
		 */
		public pgCreditDecisionRepeated1TiledView getRepeated1()
		{
			return (pgCreditDecisionRepeated1TiledView)getChild(CHILD_REPEATED1);
		}


		
	/**
	 *
	 *
	 */
	public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
	{
		if (doDealSummarySnapShot == null)
			doDealSummarySnapShot = (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
		return doDealSummarySnapShot;
	}


	/**
	 *
	 *
	 */
	public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
	{
			doDealSummarySnapShot = model;
	}


	
	/**
	 *
	 *
	 */
	public doAdjudicationMainBNCModel getdoAdjudicationMainBNCModel()
	{
		if (doAdjudicationMainBNC == null)
			doAdjudicationMainBNC = (doAdjudicationMainBNCModel) getModel(doAdjudicationMainBNCModel.class);
		return doAdjudicationMainBNC;
	}


	/**
	 *
	 *
	 */
	public void setdoAdjudicationMainBNCModel(doAdjudicationMainBNCModel model)
	{
		doAdjudicationMainBNC = model;
	}

	/**
	 *
	 *
	 */
	public doAdjudicationResponseBNCModel getdoAdjudicationResponseBNCModel()
	{
		if (doAdjudicationResponseBNC == null)
			doAdjudicationResponseBNC = (doAdjudicationResponseBNCModel) getModel(doAdjudicationResponseBNCModel.class);
		return doAdjudicationResponseBNC;
	}


	/**
	 *
	 *
	 */
	public void setdoAdjudicationResponseBNCModel(doAdjudicationResponseBNCModel model)
	{
		doAdjudicationResponseBNC = model;
	}

	/**
	 *
	 *
	 */
	public doAdjudicationResponseBNCDecisionMessagesModel getdoAdjudicationResponseBNCDecisionMessagesModel()
	{
		if (doAdjudicationResponseBNCDecisionMessages == null)
			doAdjudicationResponseBNCDecisionMessages = (doAdjudicationResponseBNCDecisionMessagesModel) getModel(doAdjudicationResponseBNCDecisionMessagesModel.class);
		return doAdjudicationResponseBNCDecisionMessages;
	}


	/**
	 *
	 *
	 */
	public void setdoAdjudicationResponseBNCDecisionMessagesModel(doAdjudicationResponseBNCDecisionMessagesModel model)
	{
		doAdjudicationResponseBNCDecisionMessages = model;
	}

  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}

  //// Populate previous links new set of methods to populate Href display Strings.
  public String endHref1Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////



	//]]SPIDER_EVENT<btNextTaskPage_onWebEvent>


  public void handleActMessageOK(String[] args)
	{
		CreditDecisionHandler handler =(CreditDecisionHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

		handler.handleActMessageOK(args);

    handler.postHandlerProtocol();

	}



	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgCreditDecision";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 11Nov2002
  //private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_STDEALID="stDealId";
	public static final String CHILD_STDEALID_RESET_VALUE="";
	public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
	public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUS="stDealStatus";
	public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
	public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STSOURCEFIRM="stSourceFirm";
	public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
	public static final String CHILD_STSOURCE="stSource";
	public static final String CHILD_STSOURCE_RESET_VALUE="";
	public static final String CHILD_STLOB="stLOB";
	public static final String CHILD_STLOB_RESET_VALUE="";
	public static final String CHILD_STDEALTYPE="stDealType";
	public static final String CHILD_STDEALTYPE_RESET_VALUE="";
	public static final String CHILD_STDEALPURPOSE="stDealPurpose";
	public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
	public static final String CHILD_STPURCHASEPRICE="stPurchasePrice";
	public static final String CHILD_STPURCHASEPRICE_RESET_VALUE="";
	public static final String CHILD_STPMTTERM="stPmtTerm";
	public static final String CHILD_STPMTTERM_RESET_VALUE="";
	public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
	public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";
	public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
	public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
	public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
  public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";

	//-- Credit Decision specific fields --//
	public static final String CHILD_STVIEWONLYTAG = "stViewOnlyTag";
	public static final String CHILD_STVIEWONLYTAG_RESET_VALUE = "";
	public static final String CHILD_HDRESPONSEID = "hdResponseId";
	public static final String CHILD_HDRESPONSEID_RESET_VALUE = "";
	public static final String CHILD_TXGLOBALCREDITBUREAU = "txGlobalCreditBureau";
	public static final String CHILD_TXGLOBALCREDITBUREAU_RESET_VALUE = "";
	public static final String CHILD_TXGLOBALINTERNALSCORE = "txGlobalInternalScore";
	public static final String CHILD_TXGLOBALINTERNALSCORE_RESET_VALUE = "";
	public static final String CHILD_TXLASTUPDATEDDATE = "txLastUpdatedDate";
	public static final String CHILD_TXLASTUPDATEDDATE_RESET_VALUE = "";
	public static final String CHILD_TXGLOBALCREDITBUREAUSCORE = "txGlobalCreditBureauScore";
	public static final String CHILD_TXGLOBALCREDITBUREAUSCORE_RESET_VALUE = "";
	public static final String CHILD_TXCREDITDECISIONSTATUS = "txCreditDecisionStatus";
	public static final String CHILD_TXCREDITDECISIONSTATUS_RESET_VALUE = "";
	public static final String CHILD_HDDEALSTATUSCATEGORYID = "hdDealStatusCategoryId";
	public static final String CHILD_HDDEALSTATUSCATEGORYID_RESET_VALUE = "";
	public static final String CHILD_HDCREDITDECISIONREQUESTSTATUSID = "hdCreditDecisionRequestStatusId";
	public static final String CHILD_HDCREDITDECISIONREQUESTSTATUSID_RESET_VALUE = "";
	public static final String CHILD_HDREQUESTID = "hdRequestId";
	public static final String CHILD_HDREQUESTID_RESET_VALUE = "";
	public static final String CHILD_TXDECISIONMESSAGES = "txDecisionMessages";
	public static final String CHILD_TXDECISIONMESSAGES_RESET_VALUE = "";
	public static final String CHILD_TXGLOBALRISKRATING = "txGlobalRiskRating";
	public static final String CHILD_TXGLOBALRISKRATING_RESET_VALUE = "";
	public static final String CHILD_TXCREDITDECISION = "txCreditDecision";
	public static final String CHILD_TXCREDITDECISION_RESET_VALUE = "";
	public static final String CHILD_TXPOSTEDRATE = "txPostedRate";
	public static final String CHILD_TXPOSTEDRATE_RESET_VALUE = "";
	public static final String CHILD_TXMONTHLYPAYMENT = "txMonthlyPayment";
	public static final String CHILD_TXMONTHLYPAYMENT_RESET_VALUE = "";
	public static final String CHILD_TXDOWNPAYMENT = "txDownPayment";
	public static final String CHILD_TXDOWNPAYMENT_RESET_VALUE = "";
	public static final String CHILD_TXLOCATION = "txLocation";
	public static final String CHILD_TXLOCATION_RESET_VALUE = "";
	public static final String CHILD_TXACTUALPAYMENTTERM = "txActualPaymentTerm";
	public static final String CHILD_TXACTUALPAYMENTTERM_RESET_VALUE = "";
	public static final String CHILD_TXPRODUCT = "txProduct";
	public static final String CHILD_TXPRODUCT_RESET_VALUE = "";
	public static final String CHILD_TXLTV = "txLTV";
	public static final String CHILD_TXLTV_RESET_VALUE = "";
	public static final String CHILD_TXOCCUPIEDBY = "txOccupiedBy";
	public static final String CHILD_TXOCCUPIEDBY_RESET_VALUE = "";
	public static final String CHILD_TXAMORTIZATIONTERM = "txAmortizationTerm";
	public static final String CHILD_TXAMORTIZATIONTERM_RESET_VALUE = "";
	public static final String CHILD_TXPROPERTYTYPE = "txPropertyType";
	public static final String CHILD_TXPROPERTYTYPE_RESET_VALUE = "";
	public static final String CHILD_TXMORTGAGEINSURER = "txMortgageInsurer";
	public static final String CHILD_TXMORTGAGEINSURER_RESET_VALUE = "";
	public static final String CHILD_TXPRIMARYAPPLICANTRISK = "txPrimaryApplicantRisk";
	public static final String CHILD_TXPRIMARYAPPLICANTRISK_RESET_VALUE = "";
	public static final String CHILD_TXMARKETRISK = "txMarketRisk";
	public static final String CHILD_TXMARKETRISK_RESET_VALUE = "";
	public static final String CHILD_TXMISTATUS = "txMIStatus";
	public static final String CHILD_TXMISTATUS_RESET_VALUE = "";
	public static final String CHILD_TXSECONDARYAPPLICANTRISK = "txSecondaryApplicantRisk";
	public static final String CHILD_TXSECONDARYAPPLICANTRISK_RESET_VALUE = "";
	public static final String CHILD_TXPROPERTYRISK = "txPropertyRisk";
	public static final String CHILD_TXPROPERTYRISK_RESET_VALUE = "";
	public static final String CHILD_TXMIDECISION = "txMIDecision";
	public static final String CHILD_TXMIDECISION_RESET_VALUE = "";
	public static final String CHILD_TXNEIGHBOURHOODRISK = "txNeighbourhoodRisk";
	public static final String CHILD_TXNEIGHBOURHOODRISK_RESET_VALUE = "";
	public static final String CHILD_TXAMOUNTINSURED = "txAmountInsured";
	public static final String CHILD_TXAMOUNTINSURED_RESET_VALUE = "";
	public static final String CHILD_TXFOURTHAPPLICANTINTERNALSCORE_RESET_VALUE = "";
	public static final String CHILD_TXCOMBINEDGDS = "txCombinedGDS";
	public static final String CHILD_TXCOMBINEDGDS_RESET_VALUE = "";
	public static final String CHILD_TXCOMBINEDTDS = "txCombinedTDS";
	public static final String CHILD_TXCOMBINEDTDS_RESET_VALUE = "";
	public static final String CHILD_TXTOTALNETWORTH = "txTotalNetWorth";
	public static final String CHILD_TXTOTALNETWORTH_RESET_VALUE = "";
	public static final String CHILD_TXTOTALINCOME = "txTotalIncome";
	public static final String CHILD_TXTOTALINCOME_RESET_VALUE = "";
	public static final String CHILD_TXTOTALOTHERINCOME = "txTotalOtherIncome";
	public static final String CHILD_TXTOTALOTHERINCOME_RESET_VALUE = "";
	public static final String CHILD_TXTOTALCOMBINEDINCOME = "txTotalCombinedIncome";
	public static final String CHILD_TXTOTALCOMBINEDINCOME_RESET_VALUE = "";
	public static final String CHILD_BTGETCREDITDECISION = "btGetCreditDecision";
	public static final String CHILD_BTGETCREDITDECISION_RESET_VALUE = " ";
	
	public static final String CHILD_REPEATED1="Repeated1";
	
	//-- end Credit Decision specific fields --//

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealSummarySnapShotModel doDealSummarySnapShot=null;
	private doAdjudicationMainBNCModel doAdjudicationMainBNC=null;
	private doAdjudicationResponseBNCModel doAdjudicationResponseBNC=null;
	private doAdjudicationResponseBNCDecisionMessagesModel doAdjudicationResponseBNCDecisionMessages=null;
	protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgCreditDecision.jsp";

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	private CreditDecisionHandler handler=new CreditDecisionHandler();
	public SysLogger logger;
}

