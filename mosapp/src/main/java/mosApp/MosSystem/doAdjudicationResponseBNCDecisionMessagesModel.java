package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * <p>Title: doAdjudicationMainBNCModel</p>
 * 
 * <p>Description: Interface class for doAdjudicationMainBNCModelImpl</p>
 * 
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public interface doAdjudicationResponseBNCDecisionMessagesModel extends QueryModel, SelectQueryModel
{



	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponseId();


	/**
	 *
	 *
	 */
	public void setDfResponseId(java.math.BigDecimal value);

	/**
	 * 
	 *
	 */
	public String getDfDecisionMessages();

	/**
	 *
	 *
	 */
	public void setDfDecisionMessages(String value);


	/**
	 * 
	 *
	 */
	public java.math.BigDecimal getDfRecommendedCodeId();

	/**
	 *
	 *
	 */
	public void setDfRecommendedCodeId(java.math.BigDecimal value);


	/**
	 * 
	 *
	 */
	public String getDfRecommendCodeDesc();

	/**
	 *
	 *
	 */
	public void setDfRecommendCodeDesc(String value);

	/**
	 *
	 *
	 */
	public String getDfCodeAndReason();

	/**
	 *
	 *
	 */
	public void setDfCodeAndReason(String value);





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFRESPONSEID="dfResponseId";
	
	public static final String FIELD_DFDECISIONMESSAGES="dfDecisionMessages";
	public static final String FIELD_DFRECOMMENDEDCODEID="dfRecommendedCodeId";
	public static final String FIELD_DFRECOMMENDCODEDESC="dfRecommendCodeDesc";
	public static final String FIELD_DFCODEANDREASON="dfCodeAndReason";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

