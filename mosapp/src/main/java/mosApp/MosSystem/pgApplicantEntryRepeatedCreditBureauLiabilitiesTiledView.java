package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);

    // Test by BILLY 01Aug2002
    //DefaultModel dModel = (DefaultModel)getDefaultModel();
    //dModel.setUseDefaultValues(false);

    setPrimaryModelClass( doApplicantCreditBureauLiabilitiesModel.class );
		registerChildren();
		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getTxCreditBureauLiabilityType().setValue(CHILD_TXCREDITBUREAULIABILITYTYPE_RESET_VALUE);
        getChCreditBureauIndicator().setValue(CHILD_CHCREDITBUREAUINDICATOR_RESET_VALUE);
		getTxCreditBureauLiabilityDesc().setValue(CHILD_TXCREDITBUREAULIABILITYDESC_RESET_VALUE);
        getTxCreditBureauLiabilityLimit().setValue(CHILD_TXCREDITBUREAULIABILITYLIMIT_RESET_VALUE);
		getTxCreditBureauLiabilityAmount().setValue(CHILD_TXCREDITBUREAULIABILITYAMOUNT_RESET_VALUE);
		getTxCreditBureauLiabilityMonthlyPayment().setValue(CHILD_TXCREDITBUREAULIABILITYMONTHLYPAYMENT_RESET_VALUE);
		getChCreditBureauLiabilityExport().setValue(CHILD_CHCREDITBUREAULIABILITYEXPORT_RESET_VALUE);
		getChCreditBureauLiabilityDelete().setValue(CHILD_CHCREDITBUREAULIABILITYDELETE_RESET_VALUE);
		getHdCBLiabilityId().setValue(CHILD_HDCBLIABILITYID_RESET_VALUE);
		getHdCBLiabilityCopyId().setValue(CHILD_HDCBLIABILITYCOPYID_RESET_VALUE);
		getHdCBLiabilityPercentageIncludeInGDS().setValue(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE);
		getHdCBLiabilityPercentageIncludeInTDS().setValue(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE);
        getCbMonths().setValue(CHILD_CBMONTHS_RESET_VALUE);
        getTbDay().setValue(CHILD_TBDAY_RESET_VALUE);
        getTbYear().setValue(CHILD_TBYEAR_RESET_VALUE);
        getStTileIndex().setValue(CHILD_STTILEINDEX_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_TXCREDITBUREAULIABILITYTYPE,TextField.class);
        registerChild(CHILD_CHCREDITBUREAUINDICATOR,CheckBox.class);        
		registerChild(CHILD_TXCREDITBUREAULIABILITYDESC,TextField.class);
        registerChild(CHILD_TXCREDITBUREAULIABILITYLIMIT,TextField.class);
		registerChild(CHILD_TXCREDITBUREAULIABILITYAMOUNT,TextField.class);
		registerChild(CHILD_TXCREDITBUREAULIABILITYMONTHLYPAYMENT,TextField.class);
		registerChild(CHILD_CHCREDITBUREAULIABILITYEXPORT,CheckBox.class);
		registerChild(CHILD_CHCREDITBUREAULIABILITYDELETE,CheckBox.class);
		registerChild(CHILD_HDCBLIABILITYID,HiddenField.class);
		registerChild(CHILD_HDCBLIABILITYCOPYID,HiddenField.class);
		registerChild(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS,HiddenField.class);
		registerChild(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS,HiddenField.class);
        registerChild(CHILD_CBMONTHS, ComboBox.class);
        registerChild(CHILD_TBDAY, TextField.class);
        registerChild(CHILD_TBYEAR, TextField.class);
        registerChild(CHILD_STTILEINDEX, StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

   	super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated RepeatedCreditBureauLiabilities_onBeforeRowDisplayEvent code here
      int rowNum = getTileIndex();
		  boolean ret = true;

      ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());

      // Get Data Object
      doApplicantCreditBureauLiabilitiesModelImpl theObj =(doApplicantCreditBureauLiabilitiesModelImpl) this.getdoApplicantCreditBureauLiabilitiesModel();

      if (theObj.getSize() > 0)
      {
        handler.setCreditBureauLiabilitySection(theObj, rowNum);
        //--> Important !! If the field (in TiledView) is not binded to any DataField, we must
        //--> set the defaslt value here in order to populate the value in the Model (PrimaryModel).
        //--> Otherwise, when the page submitted (via button or Herf) the user input will not populate
        //--> Comment by BILLY 02Aug2002
        setDisplayFieldValue(CHILD_CHCREDITBUREAULIABILITYEXPORT,  new String("F"));
        setDisplayFieldValue(CHILD_CHCREDITBUREAULIABILITYDELETE,  new String("F"));
        setDisplayFieldValue(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS,  new String(""));
        setDisplayFieldValue(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS,  new String(""));
        //============================================================================================
      }
      else ret = false;

      handler.pageSaveState();

      return ret;
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
    return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_TXCREDITBUREAULIABILITYTYPE))
		{
			TextField child = new TextField(this,
				getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_TXCREDITBUREAULIABILITYTYPE,
				doApplicantCreditBureauLiabilitiesModel.FIELD_DFLIABILITYTYPEDESCRIPTION,
				CHILD_TXCREDITBUREAULIABILITYTYPE_RESET_VALUE,
				null);
			return child;
		}
        else if (name.equals(CHILD_CHCREDITBUREAUINDICATOR)) {
            CheckBox child = new CheckBox(this,
                getdoApplicantCreditBureauLiabilitiesModel(),
                CHILD_CHCREDITBUREAUINDICATOR,
                doApplicantCreditBureauLiabilitiesModel.FIELD_DFCREDITBUREAUINDICATOR,
                "Y", "N", false, null);
            return child;
        }
		else
		if (name.equals(CHILD_TXCREDITBUREAULIABILITYDESC))
		{
			TextField child = new TextField(this,
				getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_TXCREDITBUREAULIABILITYDESC,
				doApplicantCreditBureauLiabilitiesModel.FIELD_DFLIABILITYDESCRIPTION,
				CHILD_TXCREDITBUREAULIABILITYDESC_RESET_VALUE,
				null);
			return child;
		}
        else
            if (name.equals(CHILD_TXCREDITBUREAULIABILITYLIMIT))
            {
                TextField child = new TextField(this,
                    getdoApplicantCreditBureauLiabilitiesModel(),
                    CHILD_TXCREDITBUREAULIABILITYLIMIT,
                    doApplicantCreditBureauLiabilitiesModel.FIELD_DFCREDITBUREAULIABILITYLIMIT,
                    CHILD_TXCREDITBUREAULIABILITYLIMIT_RESET_VALUE,
                    null);
                return child;
            }
		else
		if (name.equals(CHILD_TXCREDITBUREAULIABILITYAMOUNT))
		{
			TextField child = new TextField(this,
				getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_TXCREDITBUREAULIABILITYAMOUNT,
				doApplicantCreditBureauLiabilitiesModel.FIELD_DFLIABILITYAMOUNT,
				CHILD_TXCREDITBUREAULIABILITYAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXCREDITBUREAULIABILITYMONTHLYPAYMENT))
		{
			TextField child = new TextField(this,
				getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_TXCREDITBUREAULIABILITYMONTHLYPAYMENT,
				doApplicantCreditBureauLiabilitiesModel.FIELD_DFLIABILITYMONTHLYPAYMENT,
				CHILD_TXCREDITBUREAULIABILITYMONTHLYPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CHCREDITBUREAULIABILITYEXPORT))
		{
			CheckBox child = new CheckBox(this,
        //--> Important !! It is not working with the DefaultModel, i.e. field value will not be populated when
        //--> Submit.  The work around is to attached the unbinded field to a Memory field of the PrimaryModel.
        //--> By Billy 02Aug2002
				//getDefaultModel(),
        getdoApplicantCreditBureauLiabilitiesModel(),
        //=====================================================================================================
				CHILD_CHCREDITBUREAULIABILITYEXPORT,
				CHILD_CHCREDITBUREAULIABILITYEXPORT,
				"T","F",
				false,
				null);

			return child;
		}
		else
		if (name.equals(CHILD_CHCREDITBUREAULIABILITYDELETE))
		{
			CheckBox child = new CheckBox(this,
				//getDefaultModel(),
        getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_CHCREDITBUREAULIABILITYDELETE,
				CHILD_CHCREDITBUREAULIABILITYDELETE,
				"T","F",
				false,
				null);

			return child;
		}
		else
		if (name.equals(CHILD_HDCBLIABILITYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_HDCBLIABILITYID,
				doApplicantCreditBureauLiabilitiesModel.FIELD_DFLIABILITYID,
				CHILD_HDCBLIABILITYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDCBLIABILITYCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_HDCBLIABILITYCOPYID,
				doApplicantCreditBureauLiabilitiesModel.FIELD_DFCOPYID,
				CHILD_HDCBLIABILITYCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS,
				CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS,
        CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantCreditBureauLiabilitiesModel(),
				CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS,
				CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS,
				CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE,
				null);
			return child;
		}
        else if (name.equals(CHILD_CBMONTHS)) {
            ComboBox child = new ComboBox(this,
                    getdoApplicantCreditBureauLiabilitiesModel(), CHILD_CBMONTHS,
                    CHILD_CBMONTHS, CHILD_CBMONTHS_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbMonthsOptions);
            return child;
        } else if (name.equals(CHILD_TBDAY)) {
            TextField child = new TextField(this,
                    getdoApplicantCreditBureauLiabilitiesModel(), CHILD_TBDAY, CHILD_TBDAY,
                    CHILD_TBDAY_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBYEAR)) {
            TextField child = new TextField(this,
                    getdoApplicantCreditBureauLiabilitiesModel(), CHILD_TBYEAR, CHILD_TBYEAR,
                    CHILD_TBYEAR_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STTILEINDEX)) {
            StaticTextField child =
                new StaticTextField(this,
                                    getDefaultModel(),
                                    CHILD_STTILEINDEX,
                                    CHILD_STTILEINDEX,
                                    CHILD_STTILEINDEX_RESET_VALUE,
                                    null);
            return child;
        }
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
     * 
     * 
     */
	public TextField getTxCreditBureauLiabilityType()
	{
		return (TextField)getChild(CHILD_TXCREDITBUREAULIABILITYTYPE);
	}
    
    /**
     * 
     * @return
     */
    public CheckBox getChCreditBureauIndicator() {

        return (CheckBox) getChild(CHILD_CHCREDITBUREAUINDICATOR);
    }


	/**
	 *
	 *
	 */
	public TextField getTxCreditBureauLiabilityDesc()
	{
		return (TextField)getChild(CHILD_TXCREDITBUREAULIABILITYDESC);
	}

    /**
     *
     *
     */
    public TextField getTxCreditBureauLiabilityLimit()
    {
        return (TextField)getChild(CHILD_TXCREDITBUREAULIABILITYLIMIT);
    }

	/**
	 *
	 *
	 */
	public TextField getTxCreditBureauLiabilityAmount()
	{
		return (TextField)getChild(CHILD_TXCREDITBUREAULIABILITYAMOUNT);
	}


	/**
	 *
	 *
	 */
	public TextField getTxCreditBureauLiabilityMonthlyPayment()
	{
		return (TextField)getChild(CHILD_TXCREDITBUREAULIABILITYMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public CheckBox getChCreditBureauLiabilityExport()
	{
		return (CheckBox)getChild(CHILD_CHCREDITBUREAULIABILITYEXPORT);
	}


	/**
	 *
	 *
	 */
	public CheckBox getChCreditBureauLiabilityDelete()
	{
		return (CheckBox)getChild(CHILD_CHCREDITBUREAULIABILITYDELETE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdCBLiabilityId()
	{
		return (HiddenField)getChild(CHILD_HDCBLIABILITYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdCBLiabilityCopyId()
	{
		return (HiddenField)getChild(CHILD_HDCBLIABILITYCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdCBLiabilityPercentageIncludeInGDS()
	{
		return (HiddenField)getChild(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdCBLiabilityPercentageIncludeInTDS()
	{
		return (HiddenField)getChild(CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS);
	}


      /**
       *
       *
       */
      public ComboBox getCbMonths()
      {
        return(ComboBox)getChild(CHILD_CBMONTHS);
      }

      /**
       *
       *
       */
      public TextField getTbDay()
      {
        return(TextField)getChild(CHILD_TBDAY);
      }

      /**
       *
       *
       */
      public TextField getTbYear()
      {
        return(TextField)getChild(CHILD_TBYEAR);
      }
      
      /**
      *
      *
      */
      public StaticTextField getStTileIndex() {
          return (StaticTextField) getChild(CHILD_STTILEINDEX);
      }
      
	/**
	 *
	 *
	 */
	public doApplicantCreditBureauLiabilitiesModel getdoApplicantCreditBureauLiabilitiesModel()
	{
		if (doApplicantCreditBureauLiabilities == null)
			doApplicantCreditBureauLiabilities = (doApplicantCreditBureauLiabilitiesModel) getModel(doApplicantCreditBureauLiabilitiesModel.class);
		return doApplicantCreditBureauLiabilities;
	}


	/**
	 *
	 *
	 */
	public void setdoApplicantCreditBureauLiabilitiesModel(doApplicantCreditBureauLiabilitiesModel model)
	{
			doApplicantCreditBureauLiabilities = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TXCREDITBUREAULIABILITYTYPE="txCreditBureauLiabilityType";
	public static final String CHILD_TXCREDITBUREAULIABILITYTYPE_RESET_VALUE="";
    public static final String CHILD_CHCREDITBUREAUINDICATOR="chCreditBureauIndicator";
    public static final String CHILD_CHCREDITBUREAUINDICATOR_RESET_VALUE="";
	public static final String CHILD_TXCREDITBUREAULIABILITYDESC="txCreditBureauLiabilityDesc";
	public static final String CHILD_TXCREDITBUREAULIABILITYDESC_RESET_VALUE="";
    public static final String CHILD_TXCREDITBUREAULIABILITYLIMIT="txCreditBureauLiabilityLimit";
    public static final String CHILD_TXCREDITBUREAULIABILITYLIMIT_RESET_VALUE="";
	public static final String CHILD_TXCREDITBUREAULIABILITYAMOUNT="txCreditBureauLiabilityAmount";
	public static final String CHILD_TXCREDITBUREAULIABILITYAMOUNT_RESET_VALUE="";
	public static final String CHILD_TXCREDITBUREAULIABILITYMONTHLYPAYMENT="txCreditBureauLiabilityMonthlyPayment";
	public static final String CHILD_TXCREDITBUREAULIABILITYMONTHLYPAYMENT_RESET_VALUE="";
	public static final String CHILD_CHCREDITBUREAULIABILITYEXPORT="chCreditBureauLiabilityExport";
	public static final String CHILD_CHCREDITBUREAULIABILITYEXPORT_RESET_VALUE="F";
	public static final String CHILD_CHCREDITBUREAULIABILITYDELETE="chCreditBureauLiabilityDelete";
	public static final String CHILD_CHCREDITBUREAULIABILITYDELETE_RESET_VALUE="F";
	public static final String CHILD_HDCBLIABILITYID="hdCBLiabilityId";
	public static final String CHILD_HDCBLIABILITYID_RESET_VALUE="";
	public static final String CHILD_HDCBLIABILITYCOPYID="hdCBLiabilityCopyId";
	public static final String CHILD_HDCBLIABILITYCOPYID_RESET_VALUE="";
	public static final String CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS="hdCBLiabilityPercentageIncludeInGDS";
	public static final String CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINGDS_RESET_VALUE="";
	public static final String CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS="hdCBLiabilityPercentageIncludeInTDS";
	public static final String CHILD_HDCBLIABILITYPERCENTAGEINCLUDEINTDS_RESET_VALUE="";

    public static final String CHILD_CBMONTHS = "cbMonths";
    public static final String CHILD_CBMONTHS_RESET_VALUE = "";
    private OptionList cbMonthsOptions = new OptionList(new String[] {}
        , new String[] {});
    public static final String CHILD_TBDAY = "tbDay";
    public static final String CHILD_TBDAY_RESET_VALUE = "";
    public static final String CHILD_TBYEAR = "tbYear";
    public static final String CHILD_TBYEAR_RESET_VALUE = "";
    
    // the tile index.
    public static final String CHILD_STTILEINDEX = "stTileIndex";
    public static final String CHILD_STTILEINDEX_RESET_VALUE = "";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doApplicantCreditBureauLiabilitiesModel doApplicantCreditBureauLiabilities=null;

  private ApplicantEntryHandler handler=new ApplicantEntryHandler();

  public SysLogger logger;
}

