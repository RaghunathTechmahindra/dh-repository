package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetPricingProfileIdModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPricingProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPricingProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfRateCode();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateCode(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPRICINGPROFILEID="dfPricingProfileId";
	public static final String FIELD_DFRATECODE="dfRateCode";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

