package mosApp.MosSystem;

import org.apache.log4j.Logger;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.UserProfile;

/**
 * <p>Title: UserActivityLogger</p>
 *
 * <p>Description: To log the user activities, such as login, page navigation in a companct, separate for application log form. <br>
 *                 Currently, we just use it for Performance / Stress testing purpose.
 * </p>
 *
 * <p>Copyright: Filogix 2005</p>
 *
 * <p>Company: Filogix Inc.</p>
 * @author Vladimir Ivanov
 * @version 1.0 (Initial version 20Oct2005)
 */

public class UserActivityLogger {

  public static final Logger log = Logger.getLogger(UserActivityLogger.class.getName());

  // Define the Log Type indicators
  public static final String ACTION = "A"; // User Actions, e.g. Page Navigations
  public static final String TRANSACTION = "T"; // Task change history
  public static final String LOGIN = "SIN"; // On user login
  public static final String LOGOUT = "SOUT"; // On user logout

  public UserActivityLogger() {
  }

  /**
   * Log the User Action (use for Login / Logoff activities)
   *
   * 
   * @param msg String
   */
  public static final void logAction(String msg) {

    log(null, msg, ACTION);
  }
  /**
   * Log the User Action
   *
   * @param up UserProfile
   * @param msg String
   */
  public static final void logAction(UserProfile up, String msg) {
    log(up, msg, ACTION);
  }

  /**
   * Log the User Action
   *
   * @param up UserProfile
   * @param msg String
   */
  public static final void logTransaction(UserProfile up, String msg) {
    log(up, msg, TRANSACTION);
  }
  
  /**
   * Log the User activity in comma delimited format, this way the log file can be import to excel for further process and
   * investigation.
   *
   * @param currentUser UserProfile
   * @param msg String
   * @param type String
   */
  public static final void log(UserProfile up, String msg, String type) {
    log(up, null, msg, type);
  }

  /**
   * Log the User activity in comma delimited format, this way the log file can be import to excel for further process and
   * investigation.
   *
   * @param currentUser UserProfile
   * @param deal Deal
   * @param msg String
   * @param type String
   */
  public static final void log(UserProfile currentUser, Deal deal, String msg, String type) {
    StringBuffer tmpStrBuf = new StringBuffer();

    if(TRANSACTION.equals(type)) {
      tmpStrBuf.append(TRANSACTION).append(", ");
    }
    else {
      tmpStrBuf.append(ACTION).append(", ");
    }

    if(currentUser != null) {
      //Record InstitutionId for multi-lender aka multi-institution environment
      //tmpStrBuf.append("InstitutionId: ");
      //tmpStrBuf.append(currentUser.getInstitutionId()).append(", ");
      //Record UserProfileId
      tmpStrBuf.append("UserProfileId: ");
      tmpStrBuf.append(currentUser.getUserProfileId()).append(", ");
      tmpStrBuf.append("UserLogin: ");
      //Record User Login Id
      tmpStrBuf.append(currentUser.getUserLogin()).append(", ");
      tmpStrBuf.append("User InstitutionProfileId: ");
      //Record User Login Id
      tmpStrBuf.append(currentUser.getInstitutionId()).append(", ");
    }
    else {
      //Record null UserProfileId
      tmpStrBuf.append("?????, ");
      //Record null User Login Id
      tmpStrBuf.append("unknown yet, ");
    }

    if(deal != null) {
      //Record DealId
      tmpStrBuf.append("DealId: ");
      tmpStrBuf.append(deal.getDealId()).append(", ");
      //Record InstitutionId
      tmpStrBuf.append("Deal InstitutionId : ");
      tmpStrBuf.append(deal.getInstitutionProfileId()).append(", ");
      //Record CopyId
      tmpStrBuf.append("CopyId: ");
      tmpStrBuf.append(deal.getCopyId()).append(", ");
      //Record Copy Type
      tmpStrBuf.append("CopyType: ");
      tmpStrBuf.append(deal.getCopyType()).append(", ");
      //Record Deal Status
      tmpStrBuf.append("DealStatus: ");
      tmpStrBuf.append(deal.getStatusId()).append(", ");
      //Record ScenarioRecommended
      tmpStrBuf.append("ScenarioRecommended? ");
      tmpStrBuf.append(deal.getScenarioRecommended()).append(", ");
    }
    else {
      //Record null DealId
      tmpStrBuf.append(", ");
      //Record null Application Id
      tmpStrBuf.append("notDealPage, ");
    }

    //Record Message
    tmpStrBuf.append(msg);
    log.info(tmpStrBuf);
  }

  /**
   * Log the User activity in comma delimited format, this way the log file can be imported to excel for further process and
   * investigation.
   *
   * @param currentUser UserProfile
   * @param msg String
   * @param type String
   */
  public static final void logUser(UserProfile currentUser, String msg, String type) {
    StringBuffer tmpStrBuf = new StringBuffer();

    if(LOGIN.equals(type)) {
      tmpStrBuf.append(LOGIN).append(", ");
    }
    else if (LOGOUT.equals(type)){
      tmpStrBuf.append(LOGOUT).append(", ");
    }

    if(currentUser != null) {
      //Record UserProfileId
      tmpStrBuf.append(currentUser.getUserProfileId()).append(", ");
      //Record User Login Id
      tmpStrBuf.append(currentUser.getUserLogin()).append(", ");
      //Record User Institution Id
      tmpStrBuf.append(currentUser.getInstitutionId()).append(", ");
    }
    else {
      //Record null UserProfileId
      tmpStrBuf.append("?????, ");
      //Record null User Login Id
      tmpStrBuf.append("unknown, ");
    }

    //Record Message
    tmpStrBuf.append(msg);
    log.info(tmpStrBuf);
  }
  
  /**
   * Log the User activity in string format
   *
   * @param msg String
   */
  public static final void log(String msg) {
    StringBuffer tmpStrBuf = new StringBuffer(msg);
    log.info(tmpStrBuf);
  }
  
}
