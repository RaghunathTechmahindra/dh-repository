package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doDealGetReferenceInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
    /**
     * 
     * 
     */
    public java.math.BigDecimal getDfCopyId();

    
    /**
     * 
     * 
     */
    public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfReferenceDealNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfReferenceDealNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfReferenceDealTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfReferenceDealTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfRefDealTypeDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfRefDealTypeDescription(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFREFERENCEDEALNUMBER="dfReferenceDealNumber";
	public static final String FIELD_DFREFERENCEDEALTYPEID="dfReferenceDealTypeId";
	public static final String FIELD_DFREFDEALTYPEDESCRIPTION="dfRefDealTypeDescription";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

