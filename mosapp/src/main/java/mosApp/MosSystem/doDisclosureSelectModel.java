package mosApp.MosSystem;

import com.iplanet.jato.model.sql.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public interface doDisclosureSelectModel extends QueryModel, SelectQueryModel {

  public java.math.BigDecimal getDfDisclosureQuestionId();
  public void setDfDisclosureQuestionId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfDocumentTypeId();
  public void setDfDocumentTypeId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfDealId();
  public void setDfDealId(java.math.BigDecimal value);

  public String getDfQuestionCode();
  public void setDfQuestionCode(String value);

  public String getDfIsResourceBundleKey();
  public void setDfIsResourceBundleKey(String value);

  public String getDfResponse();
  public void setDfResponse(String value);
  
  //cancel btn
  public String getDfInstr();
  public void setDfInstr(String value);

  public static final String FIELD_DFDISCLOSUREQUESTIONID="dfDisclosureQuestionId";
  public static final String FIELD_DFDOCUMENTTYPEID="dfDocumentTypeId";
  public static final String FIELD_DFDEALID="dfDealId";
  public static final String FIELD_DFQUESTIONCODE="dfQuestionCode";
  public static final String FIELD_DFISRESOURCEBUNDLEKEY="dfIsResourceBundleKey";
  public static final String FIELD_DFRESPONSE="dfResponse";
  public static final String FIELD_DF = "dfInstr";	//cancel btn
  
}
