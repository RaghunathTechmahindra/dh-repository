package mosApp.MosSystem;

/**
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 */

import java.sql.Clob;
import java.sql.SQLException;

import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doDocConditionModelImpl extends QueryModelBase
	implements doDocConditionModel
{
	/**
	 *
	 *
	 */
	public doDocConditionModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
		//#DG638 reset clob as string		
		if( queryType != QUERY_TYPE_SELECT)
			return;
		while(next())    {
			String condText;
			//================================================================
			//CLOB Performance Enhancement June 2010
			Object docTextVarchar = getValue(FIELD_DFDOCUMENTTEXTVARCHAR);
			if( docTextVarchar != null) {
				condText = (String)docTextVarchar;
			} else {
		  		try {
		  			Clob condTextObj = (Clob)getValue(FIELD_DFDOCUMENTTEXT);
		  			condText = TypeConverter.stringFromClob(condTextObj);
		  		}
		  		catch (Exception e) {
		  			SysLog.error(getClass(), StringUtil.stack2string(e), "DocConIM");
		  			condText = null;
		  		}
				setDfDocumentText(condText);
			}
			//================================================================
		}
		//Reset Location
		beforeFirst();
		//#DG638 end		
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel()
	{
		return (String)getValue(FIELD_DFDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value)
	{
		setValue(FIELD_DFDOCUMENTLABEL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentText()
	{
		return (String)getValue(FIELD_DFDOCUMENTTEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentText(String value)
	{
		setValue(FIELD_DFDOCUMENTTEXT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTION);
	}


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDefDocTrackId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEFDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void setDfDefDocTrackId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEFDOCTRACKID,value);
	}

  //--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public java.math.BigDecimal getDfLanguagePrederenceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANGUAGEPREFERENCEID);
	}

	public void setDfLanguagePrederenceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCEID,value);
	}


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //--> Added new LanguageId datafield in order to setup Language Criteria when page displayed
  //--> By Billy 26Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID FROM DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, CONDITION  __WHERE__  ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, " +
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, "+
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE, "+
    "DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, DOCUMENTTRACKING.DOCUMENTSTATUSID, "+
    //--> Added LanguagePreferenceId
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID "+
    "FROM DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, CONDITION  "+
    "__WHERE__  "+
    "ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";
//================================================================
	
  public static final String MODIFYING_QUERY_TABLE_NAME="DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE, CONDITION";

  //// SYNCADD.
	////public static final String STATIC_WHERE_CRITERIA=" (((CONDITION.CONDITIONID  =  DOCUMENTTRACKING.CONDITIONID) AND (DOCUMENTTRACKING.DOCUMENTTRACKINGID  =  DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID) AND (DOCUMENTTRACKING.COPYID  =  DOCUMENTTRACKINGVERBIAGE.COPYID)) AND DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0 AND DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 0 AND CONDITION.CONDITIONTYPEID = 1) ";
  //--Release2.1--//
  //--> Take out the HardCoded Criteria, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0
  //public static final String STATIC_WHERE_CRITERIA=" (((CONDITION.CONDITIONID  =  DOCUMENTTRACKING.CONDITIONID) AND (DOCUMENTTRACKING.DOCUMENTTRACKINGID  =  DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID) AND (DOCUMENTTRACKING.COPYID  =  DOCUMENTTRACKINGVERBIAGE.COPYID)) AND DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0 AND DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 0 AND CONDITION.CONDITIONTYPEID = 1 AND CONDITION.CONDITIONID <> 55) ";
	public static final String STATIC_WHERE_CRITERIA=
    " (((CONDITION.CONDITIONID  =  DOCUMENTTRACKING.CONDITIONID) AND "+
    "(DOCUMENTTRACKING.DOCUMENTTRACKINGID  =  DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID) AND "+
    "(DOCUMENTTRACKING.COPYID  =  DOCUMENTTRACKINGVERBIAGE.COPYID) AND " +
    "(CONDITION.INSTITUTIONPROFILEID = DOCUMENTTRACKING.INSTITUTIONPROFILEID) AND " + 
    "(CONDITION.INSTITUTIONPROFILEID = DOCUMENTTRACKINGVERBIAGE.INSTITUTIONPROFILEID)) AND "+
    "(DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 0 AND "+
    "CONDITION.CONDITIONTYPEID = 1 AND "+
    "CONDITION.CONDITIONID <> 55)) ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDOCUMENTLABEL="DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL";
	public static final String COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXT="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT";
	public static final String COLUMN_DFDOCUMENTTEXT="DOCUMENTTEXT";
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXTVARCHAR="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE";
	public static final String COLUMN_DFDOCUMENTTEXTVARCHAR="DOCUMENTTEXTLITE";
	//================================================================
	public static final String QUALIFIED_COLUMN_DFDEALID="DOCUMENTTRACKING.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOCUMENTTRACKING.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFACTION="DOCUMENTTRACKING.DOCUMENTSTATUSID";
	public static final String COLUMN_DFACTION="DOCUMENTSTATUSID";
	public static final String QUALIFIED_COLUMN_DFDEFDOCTRACKID="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID";
	public static final String COLUMN_DFDEFDOCTRACKID="DOCUMENTTRACKINGID";
  //--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID="DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID";
	public static final String COLUMN_DFLANGUAGEPREFERENCEID="LANGUAGEPREFERENCEID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTLABEL,
				COLUMN_DFDOCUMENTLABEL,
				QUALIFIED_COLUMN_DFDOCUMENTLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTTEXT,
				COLUMN_DFDOCUMENTTEXT,
				QUALIFIED_COLUMN_DFDOCUMENTTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	    //================================================================
		//CLOB Performance Enhancement June 2010
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTTEXTVARCHAR,
				COLUMN_DFDOCUMENTTEXTVARCHAR,
				QUALIFIED_COLUMN_DFDOCUMENTTEXTVARCHAR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//================================================================	
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTION,
				COLUMN_DFACTION,
				QUALIFIED_COLUMN_DFACTION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEFDOCTRACKID,
				COLUMN_DFDEFDOCTRACKID,
				QUALIFIED_COLUMN_DFDEFDOCTRACKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Release2.1--//
    //--> Added LanguagePreferenceId DataField
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCEID,
				COLUMN_DFLANGUAGEPREFERENCEID,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

