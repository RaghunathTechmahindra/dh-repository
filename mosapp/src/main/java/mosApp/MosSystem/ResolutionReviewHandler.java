package mosApp.MosSystem;

import java.util.Hashtable;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.Bridge;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.ViewBean;




/**
 *
 *
 */
public class ResolutionReviewHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public ResolutionReviewHandler cloneSS()
	{
		return(ResolutionReviewHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();

		populateTaskNavigator(pg);

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();

	}


	/////////////////////////////////////////////////////////////////////////
	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		try
		{
			PageEntry pg = theSessionState.getCurrentPage();
			if (pg.getSetupBeforeGenerationCalled() == true) return;
			pg.setSetupBeforeGenerationCalled(true);
			setupDealSummarySnapShotDO(pg);
			int dealId = pg.getPageDealId();
			int copyId = pg.getPageDealCID();

      doDealInformationModelImpl doDealInfo = (doDealInformationModelImpl)
                                      (RequestManager.getRequestContext().getModelManager().getModel(doDealInformationModel.class));

			doDealInfo.clearUserWhereCriteria();
			doDealInfo.addUserWhereCriterion("dfDealId", "=", new Integer(dealId));
			doDealInfo.addUserWhereCriterion("dfCopyId", "=", new Integer(copyId));
			int bridgeId = 0;
			try
			{
				Bridge bridge = new Bridge(srk, null);
				if (bridge.findByDeal(new DealPK(dealId, copyId)) == null)
				{
					pg.setPageCondition3(false);
					return;
				}
				bridgeId = bridge.getBridgeId();
				pg.setPageCondition3(true);


				////CSpCriteriaSQLObject getBridgeInfo =(CSpCriteriaSQLObject) getModel(doBridgeDetailsModel.class);
        doBridgeDetailsModelImpl getBridgeInfo = (doBridgeDetailsModelImpl)
                                      (RequestManager.getRequestContext().getModelManager().getModel(doBridgeDetailsModel.class));

				getBridgeInfo.clearUserWhereCriteria();
				getBridgeInfo.addUserWhereCriterion("dfCopyId", "=", new Integer(copyId));
				getBridgeInfo.addUserWhereCriterion("dfBridgeId", "=", new Integer(bridgeId));
			}
			catch(Exception e)
			{
				pg.setPageCondition3(false);
			}
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error(e);
		}

	}


	///////////////////////////////////////////////////////////////////////////

	public void preHandlerProtocol(ViewBean currNDPage)
	{
		super.preHandlerProtocol(currNDPage);


		// default page load method to display()
		PageEntry pg = theSessionState.getCurrentPage();

		pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);

	}


	///////////////////////////////////////////////////////////////////////////

	public void setBeginHideBridgeDetails(DisplayField thefield)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (! pg.getPageCondition3())
		{
			if (thefield.getName().equals("stBeginHideBridgeDetails")) thefield.setValue(new String("<!--"));
			else thefield.setValue(new String("-->"));
			return;
		}

	}


	/////////////////////////////////////////////////////////////////////////

	public void generateEffectiveAmortization(DisplayField thefield)
	{
		String val = (thefield.getValue()).toString();

		if (val != null && val.toString().trim().length() != 0)
		{
			thefield.setValue(convertMonthToYears(new Integer(val).intValue()));
		}

	}

	public String convertMonthToYears(int period)
	{
		String mthsStr = " Mths";

		String yearStr = " Yrs ";

		if (period < 12)
		{
			if (period == 1 || period == 0) mthsStr = " Mth ";
			return(period + mthsStr);
		}
		else
		{
			int yearNum = period / 12;
			int mthNum = period % 12;
			if (yearNum == 1) yearStr = " Yr ";
			if (mthNum == 1 || mthNum == 0) mthsStr = " Mth";
			return(yearNum + yearStr + mthNum + mthsStr);
		}

	}


	///////////////////////////////////////////////////////////////////////

	////public int setYesOrNo(DisplayField thefield)
	public String setYesOrNo(DisplayField thefield)
	{
		////String val = thefield.getHtmlText();
		String val = thefield.stringValue();

		if (val != null && val.trim().length() != 0)
		{
			if (val.equalsIgnoreCase("Y"))
          ////thefield.setHtmlText("Yes");
          thefield.setValue("Yes");
			else
          ////thefield.setHtmlText("No");
          thefield.setValue("No");
		}

		////return CSpPage.PROCEED;
		return val;

	}

	public void handleOKButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			int dealId = pg.getPageDealId();
			int copyId = pg.getPageDealCID();
			srk.beginTransaction();
			standardAdoptTxCopy(pg, true);
			Deal deal = new Deal(srk, null);
			deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
			int resolutioncondid = deal.getResolutionConditionId();
			if (resolutioncondid == Mc.DEAL_RES_CONDITION_HIGHER_APPROVER_DENIED || resolutioncondid == Mc.DEAL_RES_CONDITION_DEAL_COLLAPSED)
			{
				deal.setResolutionConditionId(Mc.DEAL_RES_CONDITION_RESOLUTION_CONFIRMED);
			}
			deal.ejbStore();
			workflowTrigger(2, srk, pg, null);
			srk.commitTransaction();
			navigateToNextPage();

			//handleCancelStandard();  // add business logic later on
			return;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @ResolutionReviewHandler.handleOKButton");
			logger.error(e);
		}
	}

        //--CervusPhaseII--start--//
        public void handleCustomActMessageOk(String[] args)
        {
          logger.trace("--T--> RRH@handleCustomActMessageOk: " +((args != null && args [ 0 ] != null) ? args [ 0 ] : "No args"));

          PageEntry pg = theSessionState.getCurrentPage();
          SessionResourceKit srk = getSessionResourceKit();
          ViewBean thePage = getCurrNDPage();

          if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_YES"))
          {
            logger.trace("RRH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
            provideOverrideDealLockActivity(pg, srk, thePage);
            return;
          }

          if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_NO"))
          {
            logger.trace("RRH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

            theSessionState.setActMessage(null);
            theSessionState.overrideDealLock(true);
            handleCancelStandard(false);

            return;
          }
        }
        //--CervusPhaseII--end--//
}

