package mosApp.MosSystem;


import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;
import MosSystem.Mc;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import com.filogix.express.web.jato.ExpressViewBeanBase; 

/**
 *
 *
 *
 */
public class pgLifeDisabilityInsuranceViewBean extends ExpressViewBeanBase

{
	/**
	 *
	 *
	 */
	public pgLifeDisabilityInsuranceViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		View superReturn = super.createChild(name);  
		if (superReturn != null) {  
		return superReturn;  
		} else if (name.equals(CHILD_TBDEALID)) 
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALID))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALID,
				doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
				CHILD_STDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORRFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUS,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
				CHILD_STDEALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUSDATE,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
				CHILD_STDEALSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEFIRM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCEFIRM,
				doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
				CHILD_STSOURCEFIRM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCE,
				doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
				CHILD_STSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLOB))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STLOB,
				doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
				CHILD_STLOB_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALTYPE,
				doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
				CHILD_STDEALTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALPURPOSE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALPURPOSE,
				doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
				CHILD_STDEALPURPOSE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPURCHASEPRICE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPURCHASEPRICE,
				doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
				CHILD_STPURCHASEPRICE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTTERM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPMTTERM,
				doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
				CHILD_STPMTTERM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESTCLOSINGDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STESTCLOSINGDATE,
				doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
				CHILD_STESTCLOSINGDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBINSPROPORTIONS))
		{
			ComboBox child = new ComboBox( this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_CBINSPROPORTIONS,
				doLifeDisabilityInsProportionsModel.FIELD_DFINSURANCEPROPORTIONSID,
				CHILD_CBINSPROPORTIONS_RESET_VALUE,
				null);
      child.setLabelForNoneSelected("");
			child.setOptions(cbInsProportionsOptions);
			return child;
		}
    // Should be changed upon getting approval of this update from Product
		else
		if (name.equals(CHILD_STMORTGAGEINSURANCEAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
        ////getDefaultModel(),
				CHILD_STMORTGAGEINSURANCEAMOUNT,
				doLifeDisabilityInsProportionsModel.FIELD_DFTOTALLOANAMOUNT,
				////CHILD_STMORTGAGEINSURANCEAMOUNT,
				CHILD_STMORTGAGEINSURANCEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPAYMENTFREQUENCY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STPAYMENTFREQUENCY,
				doLifeDisabilityInsProportionsModel.FIELD_DFPFDESCRIPTION,
				CHILD_STPAYMENTFREQUENCY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;
		}
		else
		if (name.equals(CHILD_REPEATED1))
		{
			pgLifeDisabilityInsuranceRepeated1TiledView child = new pgLifeDisabilityInsuranceRepeated1TiledView(this,
				CHILD_REPEATED1);
			return child;
		}
    //--DJ_CR136--23Nov2004--start--//
    else
    if (name.equals(CHILD_REPEATED2))
    {
      pgLifeDisabilityInsuranceRepeated2TiledView child = new pgLifeDisabilityInsuranceRepeated2TiledView(this,
        CHILD_REPEATED2);
      return child;
    }
    else
    if (name.equals(CHILD_BTADDINSUREONLYAPPLICANT))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDINSUREONLYAPPLICANT,
        CHILD_BTADDINSUREONLYAPPLICANT,
        CHILD_BTADDINSUREONLYAPPLICANT_RESET_VALUE,
        null);
        return child;
    }
    else
    if (name.equals(CHILD_STTARGETINSUREONLYAPPLICANT))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTARGETINSUREONLYAPPLICANT,
        CHILD_STTARGETINSUREONLYAPPLICANT,
        CHILD_STTARGETINSUREONLYAPPLICANT_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_CR136--23Nov2004--end--//
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;
		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;
		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;
		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTOTALLOANAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STTOTALLOANAMOUNT,
				doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
				CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;
		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSPECIALFEATURE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSPECIALFEATURE,
				doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
				CHILD_STSPECIALFEATURE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTOK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTOK,
				CHILD_BTOK,
				CHILD_BTOK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STVIEWONLYTAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG_RESET_VALUE,
				null);
			return child;
		}
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
		else
		if (name.equals(CHILD_STROWSDISPLAYED))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STROWSDISPLAYED,
				CHILD_STROWSDISPLAYED,
				CHILD_STROWSDISPLAYED_RESET_VALUE,
				null);
			return child;
		}
    //--DJ_CR136--start--//
    else
    if (name.equals(CHILD_STROWSDISPLAYEDINSUREONLYAPPL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STROWSDISPLAYEDINSUREONLYAPPL,
        CHILD_STROWSDISPLAYEDINSUREONLYAPPL,
        CHILD_STROWSDISPLAYEDINSUREONLYAPPL_RESET_VALUE,
        null);
      return child;
    }
    //--DJ_CR136--end--//
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
    else
		if (name.equals(CHILD_HDINSURANCEPROPORTIONSID))
		{
			HiddenField child = new HiddenField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_HDINSURANCEPROPORTIONSID,
				getdoLifeDisabilityInsProportionsModel().FIELD_DFINSURANCEPROPORTIONSID,
				CHILD_HDINSURANCEPROPORTIONSID_RESET_VALUE,
				null);
			return child;
		}
    else
		if (name.equals(CHILD_TXLIFEPERCCOVERAGEREQ))
		{
			TextField child = new TextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_TXLIFEPERCCOVERAGEREQ,
				doLifeDisabilityInsProportionsModel.FIELD_DFLIFEPERCENTCOVERAGEREQ,
				CHILD_TXLIFEPERCCOVERAGEREQ_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWPIPAYMENT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STUWPIPAYMENT,
				doLifeDisabilityInsProportionsModel.FIELD_DFPIPAYMENTAMOUNT,
				CHILD_STUWPIPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUWNETRATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STUWNETRATE,
				doLifeDisabilityInsProportionsModel.FIELD_DFNETRATE,
				CHILD_STUWNETRATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLIFEPREMIUM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STLIFEPREMIUM,
				doLifeDisabilityInsProportionsModel.FIELD_DFLIFEPREMIUM,
				CHILD_STLIFEPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDISABILITYPREMIUM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STDISABILITYPREMIUM,
				doLifeDisabilityInsProportionsModel.FIELD_DFDISABILITYPREMIUM,
				CHILD_STDISABILITYPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMBINEDLDPREMIUM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STCOMBINEDLDPREMIUM,
				doLifeDisabilityInsProportionsModel.FIELD_DFCOMBINEDLDPREMIUM,
				CHILD_STCOMBINEDLDPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTRECALCULATE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTRECALCULATE,
				CHILD_BTRECALCULATE,
				CHILD_BTRECALCULATE_RESET_VALUE,
        new CommandFieldDescriptor(OutputFromInfocalCommand.COMMAND_DESCRIPTOR));
				return child;
		}
		else
		if (name.equals(CHILD_STTOTALPREMIUM))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTOTALPREMIUM,
				CHILD_STTOTALPREMIUM,
				CHILD_STTOTALPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDINPUTTORTPCALC))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDINPUTTORTPCALC,
				CHILD_HDINPUTTORTPCALC,
				CHILD_HDINPUTTORTPCALC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDINPUTTOINFOCALCALC))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDINPUTTOINFOCALCALC,
				CHILD_HDINPUTTOINFOCALCALC,
				CHILD_HDINPUTTOINFOCALCALC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDOUTPUTFROMRTPINFOCALCALC))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDOUTPUTFROMRTPINFOCALCALC,
				CHILD_HDOUTPUTFROMRTPINFOCALCALC,
				CHILD_HDOUTPUTFROMRTPINFOCALCALC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMNTINCLUDINGLIFEDISABILITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STPMNTINCLUDINGLIFEDISABILITY,
				doLifeDisabilityInsProportionsModel.FIELD_DFPMNTINCLLIFEDISABILITY,
				CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADDRATEFORLIFEDISABILITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STADDRATEFORLIFEDISABILITY,
				doLifeDisabilityInsProportionsModel.FIELD_DFADDRATEFORLIFEDISABILITY,
				CHILD_STADDRATEFORLIFEDISABILITY,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTOTALRATEINCLLIFEDISABILITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoLifeDisabilityInsProportionsModel(),
				CHILD_STTOTALRATEINCLLIFEDISABILITY,
				doLifeDisabilityInsProportionsModel.FIELD_DFTOTALRATEINCLLIFEDISABILITY,
				CHILD_STTOTALRATEINCLLIFEDISABILITY,
				null);
			return child;
		}
    else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}

	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		super.resetChildren(); 
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
		getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
		getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
		getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
		getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
		getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
		getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
		getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
		getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
		getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
		getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
		getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
		getCbInsProportions().setValue(CHILD_CBINSPROPORTIONS_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getRepeated1().resetChildren();
    //--DJ_CR136--23Nov2004--start--//
    getRepeated2().resetChildren();
    getBtAddInsureOnlyApplicant().setValue(CHILD_BTADDINSUREONLYAPPLICANT_RESET_VALUE);
    getStTargetInsureOnlyApplicant().setValue(CHILD_STTARGETINSUREONLYAPPLICANT_RESET_VALUE);
    //--DJ_CR136--23Nov2004--end--//
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getBtBackwardButton().setValue(CHILD_BTBACKWARDBUTTON_RESET_VALUE);
		getBtForwardButton().setValue(CHILD_BTFORWARDBUTTON_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
		getStRowsDisplayed().setValue(CHILD_STROWSDISPLAYED_RESET_VALUE);
    //--DJ_CR136--start--//
    getStRowsDisplayedInsureOnlyAppl().setValue(CHILD_STROWSDISPLAYEDINSUREONLYAPPL_RESET_VALUE);
    //--DJ_CR136--end--//
		getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    getStMortgageInsuranceAmount().setValue(CHILD_STMORTGAGEINSURANCEAMOUNT_RESET_VALUE);
    getStPaymentFrequency().setValue(CHILD_STPAYMENTFREQUENCY_RESET_VALUE);
		getHdInsuranceProportionsId().setValue(CHILD_HDINSURANCEPROPORTIONSID_RESET_VALUE);
		getBtRecalculate().setValue(CHILD_BTRECALCULATE_RESET_VALUE);
		getStTotalPremium().setValue(CHILD_STTOTALPREMIUM_RESET_VALUE);
		getTxLifePercCoverageReq().setValue(CHILD_TXLIFEPERCCOVERAGEREQ_RESET_VALUE);
		getStUWPIPayment().setValue(CHILD_STUWPIPAYMENT_RESET_VALUE);
		getStUWNetRate().setValue(CHILD_STUWNETRATE_RESET_VALUE);
		getStLifePremium().setValue(CHILD_STLIFEPREMIUM_RESET_VALUE);
		getStDisabilityPremium().setValue(CHILD_STDISABILITYPREMIUM_RESET_VALUE);
		getStCombinedLDPremium().setValue(CHILD_STCOMBINEDLDPREMIUM_RESET_VALUE);
		////getHdInputToRTPCalc().setValue(CHILD_HDINPUTTORTPCALC_RESET_VALUE);
		////getHdInputToInfocalCalc().setValue(CHILD_HDINPUTTOINFOCALCALC_RESET_VALUE);
		getHdOutputFromRTPInfocalCalc().setValue(CHILD_HDOUTPUTFROMRTPINFOCALCALC_RESET_VALUE);
		getStPmntIncludingLifeDisability().setValue(CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE);
		getStTotalRateInclLifeDisability().setValue(CHILD_STTOTALRATEINCLLIFEDISABILITY_RESET_VALUE);
		getStAddRateForLifeDisability().setValue(CHILD_STADDRATEFORLIFEDISABILITY_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren(); 
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_STDEALID,StaticTextField.class);
		registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
		registerChild(CHILD_STSOURCE,StaticTextField.class);
		registerChild(CHILD_STLOB,StaticTextField.class);
		registerChild(CHILD_STDEALTYPE,StaticTextField.class);
		registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
		registerChild(CHILD_STPURCHASEPRICE,StaticTextField.class);
		registerChild(CHILD_STPMTTERM,StaticTextField.class);
		registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
		registerChild(CHILD_CBINSPROPORTIONS,ComboBox.class);
		registerChild(CHILD_TBNOTETEXT,TextField.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_REPEATED1,pgLifeDisabilityInsuranceRepeated1TiledView.class);
    //--DJ_CR136--23Nov2004--start--//
    registerChild(CHILD_REPEATED2,pgLifeDisabilityInsuranceRepeated2TiledView.class);
    registerChild(CHILD_BTADDINSUREONLYAPPLICANT,Button.class);
    registerChild(CHILD_STTARGETINSUREONLYAPPLICANT,StaticTextField.class);
    //--DJ_CR136--23Nov2004--start--//
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_BTBACKWARDBUTTON,Button.class);
		registerChild(CHILD_BTFORWARDBUTTON,Button.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
		registerChild(CHILD_STROWSDISPLAYED,StaticTextField.class);
    //--DJ_CR136--start--//
    registerChild(CHILD_STROWSDISPLAYEDINSUREONLYAPPL,StaticTextField.class);
    //--DJ_CR136--end--//
		registerChild(CHILD_BTOK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
    registerChild(CHILD_BTACTMSG,Button.class);
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
		registerChild(CHILD_STMORTGAGEINSURANCEAMOUNT,StaticTextField.class);
		registerChild(CHILD_STPAYMENTFREQUENCY,StaticTextField.class);
		registerChild(CHILD_HDINSURANCEPROPORTIONSID,HiddenField.class);
		registerChild(CHILD_BTRECALCULATE,Button.class);
		registerChild(CHILD_STTOTALPREMIUM,StaticTextField.class);
		registerChild(CHILD_TXLIFEPERCCOVERAGEREQ,TextField.class);
		registerChild(CHILD_STUWPIPAYMENT,StaticTextField.class);
		registerChild(CHILD_STUWNETRATE,StaticTextField.class);
		registerChild(CHILD_STLIFEPREMIUM,StaticTextField.class);
		registerChild(CHILD_STDISABILITYPREMIUM,StaticTextField.class);
		registerChild(CHILD_STCOMBINEDLDPREMIUM,StaticTextField.class);
		registerChild(CHILD_HDINPUTTORTPCALC,HiddenField.class);
		registerChild(CHILD_HDINPUTTOINFOCALCALC,HiddenField.class);
		registerChild(CHILD_HDOUTPUTFROMRTPINFOCALCALC,HiddenField.class);
		registerChild(CHILD_STPMNTINCLUDINGLIFEDISABILITY,StaticTextField.class);
		registerChild(CHILD_STTOTALRATEINCLLIFEDISABILITY,StaticTextField.class);
		registerChild(CHILD_STADDRATEFORLIFEDISABILITY,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealSummarySnapShotModel()); modelList.add(getdoLifeDisabilityInsProportionsModel());
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}

		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
    logger = SysLog.getSysLogger("LDIP");

    LifeDisabilityInsuranceHandler handler = (LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    //Setup Options for cbInsProportions
    cbInsProportionsOptions.populate(getRequestContext());

    getHdInputToInfocalCalc().setValue(handler.theSessionState.getInputToInfocalCalc());
    getHdInputToRTPCalc().setValue(handler.theSessionState.getInputToRTPCalc());

    handler.theSessionState.setOutputFromInfocalCalc(getHdOutputFromRTPInfocalCalc().getValue().toString());
logger.debug("LDIP@beginDisplay::OutputFromInfocalFromSession: " + getHdOutputFromRTPInfocalCalc().getValue().toString());

    handler.pageSaveState();
    super.beginDisplay(event);
	}

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
    // The following code block was migrated from the this_onBeforeDataObjectExecuteEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.setupBeforePageGeneration();
		handler.pageSaveState();

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

		// The following code block was migrated from the this_onBeforeRowDisplayEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.populatePageDisplayFields();
		handler.pageSaveState();
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btProceed_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleGoPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href1_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(0);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href2_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(1);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href3_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(2);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href4_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(3);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href5_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(4);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href6_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(5);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href7_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(6);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href8_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(7);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href9_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(8);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href10_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(9);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealId()
	{
		return (StaticTextField)getChild(CHILD_STDEALID);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStBorrFirstName()
	{
		return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatus()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceFirm()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSource()
	{
		return (StaticTextField)getChild(CHILD_STSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLOB()
	{
		return (StaticTextField)getChild(CHILD_STLOB);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealType()
	{
		return (StaticTextField)getChild(CHILD_STDEALTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealPurpose()
	{
		return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPurchasePrice()
	{
		return (StaticTextField)getChild(CHILD_STPURCHASEPRICE);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStUWPIPayment()
	{
		return (StaticTextField)getChild(CHILD_STUWPIPAYMENT);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStUWNetRate()
	{
		return (StaticTextField)getChild(CHILD_STUWNETRATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTotalRateInclLifeDisability()
	{
		return (StaticTextField)getChild(CHILD_STTOTALRATEINCLLIFEDISABILITY);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStAddRateForLifeDisability()
	{
		return (StaticTextField)getChild(CHILD_STADDRATEFORLIFEDISABILITY);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStPmtTerm()
	{
		return (StaticTextField)getChild(CHILD_STPMTTERM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEstClosingDate()
	{
		return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
	}

	/**
	 *
	 *
	 */
	public TextField getTxLifePercCoverageReq()
	{
		return (TextField)getChild(CHILD_TXLIFEPERCCOVERAGEREQ);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStLifePremium()
	{
		return (StaticTextField)getChild(CHILD_STLIFEPREMIUM);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStDisabilityPremium()
	{
		return (StaticTextField)getChild(CHILD_STDISABILITYPREMIUM);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStPmntIncludingLifeDisability()
	{
		return (StaticTextField)getChild(CHILD_STPMNTINCLUDINGLIFEDISABILITY);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStCombinedLDPremium()
	{
		return (StaticTextField)getChild(CHILD_STCOMBINEDLDPREMIUM);
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdInputToRTPCalc()
	{
		return (HiddenField)getChild(CHILD_HDINPUTTORTPCALC);
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdInputToInfocalCalc()
	{
		return (HiddenField)getChild(CHILD_HDINPUTTOINFOCALCALC);
	}
	/**
	 *
	 *
	 */
	public HiddenField getHdOutputFromRTPInfocalCalc()
	{
		return (HiddenField)getChild(CHILD_HDOUTPUTFROMRTPINFOCALCALC);
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbInsProportions()
	{
		return (ComboBox)getChild(CHILD_CBINSPROPORTIONS);
	}

	/**
	 *  Since the population of comboboxes on the pages should be done from BXResource
   *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
   *  element if place or it could be done directly from the BXResource. Each approach
   *  has its pros and cons. The most important is: for English and French versions
   *  could be different default values. It forces to use the second approach.
   *
   *  It this case to escape annoying code clone and follow the object oriented
   *  design the following abstact base class should encapsulate the combobox
   *  OptionList population. Each ComboBoxOptionList inner class should extend
   *  this base class and implement the BXResources table name.
   *
	 *
	 */
	abstract static class BaseComboBoxOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		BaseComboBoxOptionList()
		{

		}

		protected final void populate(RequestContext rc, int langId, String tablename)
		{
              String defaultInstanceStateName =
                  rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
              SessionStateModelImpl theSessionState =
                  (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                                                                        defaultInstanceStateName,
                                                                        true);

      try
      {
        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), tablename, langId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
  }

	class CbInsProportionsOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbInsProportionsOptionList()
		{

		}

		/**
		 *
		 *
		 */
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "INSURANCEPROPORTIONS");
      }
	}

	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btSubmit_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleSubmitStandard(true);
		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}

	/**
	 *
	 *
	 */
  /**
	public void handleBtRecalculateRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleRecalculate(true);

    handler.handleActMessageOK();

		handler.postHandlerProtocol();
	}
  **/

	/**
	 *
	 *
	 */
  /**
	public void handleBtSubmitToDLLRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.submitToDLL();

		handler.postHandlerProtocol();
	}
  **/

	/**
	 *
	 *
	 */
	public Button getBtRecalculate()
	{
		return (Button)getChild(CHILD_BTRECALCULATE);
	}

  /**
	 *
	 *
	 */
	public String endBtRecalculateDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btRecalculate_onBeforeHtmlOutputEvent method
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displaySubmitButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}

	/**
	 *
	 *
	 */
	public String endBtSubmitDisplay(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		////boolean rc = handler.displaySubmitLifeDisabilityButton();
		////boolean rc = handler.getTheSessionState().getCurrentPage().getPageCondition1();
    boolean rc = handler.theSessionState.getIsDisplayLifeDisabilitySubmit();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStTotalPremium()
	{
		return (StaticTextField)getChild(CHILD_STTOTALPREMIUM);
	}

	/**
	 *
	 * Original signature.
	 */
  /**
	public boolean beginStTotalPremiumDisplay(ChildDisplayEvent event)
	{
    Object value = getStTotalPremium().getValue();
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.setTotalPremium(this, event);
		handler.pageSaveState();
		return true;
  }
  **/

	/**
	 *
	 *
	 */
  /**
	public boolean beginStMortgageInsuranceAmountDisplay(ChildDisplayEvent event)
	{
    Object value = getStMortgageInsuranceAmount().getValue();
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.setInsuredAmount(this, event);
		handler.pageSaveState();
		return true;
  }
  **/

	/**
	 *
	 *
	 */
	public pgLifeDisabilityInsuranceRepeated1TiledView getRepeated1()
	{
		return (pgLifeDisabilityInsuranceRepeated1TiledView)getChild(CHILD_REPEATED1);
	}

  //--DJ_CR136--23Nov2004--start--//
  /**
   *
   *
   */
  public pgLifeDisabilityInsuranceRepeated2TiledView getRepeated2()
  {
    return (pgLifeDisabilityInsuranceRepeated2TiledView)getChild(CHILD_REPEATED2);
  }

  /**
   *
   *
   */
  public Button getBtAddInsureOnlyApplicant() {
    return (Button) getChild(CHILD_BTADDINSUREONLYAPPLICANT);
  }

  /**
   *
   *
   */
  public void handleBtAddInsureOnlyApplicantRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleAddInsureOnlyApplicant();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public String endBtAddInsureOnlyApplicantDisplay(ChildContentDisplayEvent event)
  {

    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayEditButton();

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";
  }

  /**
   *
   *
   */
  public StaticTextField getStTargetInsureOnlyApplicant()
  {
    return (StaticTextField)getChild(CHILD_STTARGETINSUREONLYAPPLICANT);
  }

  /**
   *
   *
   */
  public String endStTargetInsureOnlyApplicantDisplay(ChildContentDisplayEvent event)
  {

    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.generateLoadTarget(Mc.TARGET_LD_INSUREONLYAPPLICANT);
    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";
  }
  //--DJ_CR136--23Nov2004--end--//

	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btWorkQueueLink_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayWorkQueue();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolHistory_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayDealHistory();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btTooNotes_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayDealNotes();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolSearch_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDealSearch();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolLog_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleSignOff();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public void handleBtBackwardButtonRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btBackwardButton_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    getRepeated1().handleWebAction(WebActions.ACTION_PREV);
		////handler.handleBackwardButton();
		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtBackwardButton()
	{
		return (Button)getChild(CHILD_BTBACKWARDBUTTON);
	}


	/**
	 *
	 *
	 */

   public boolean beginBtBackwardButtonDisplay(ChildDisplayEvent event)  ////BTBACKWARDBUTTON
      throws IOException
  {
      boolean result=((doLifeDisabilityInsuranceInfoModelImpl)(getRepeated1().getdoLifeDisabilityInsuranceInfoModel())).hasPreviousResults();
      return result;
  }

	/**
	 *
	 *
	 */
	public void handleBtForwardButtonRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btForwardButton_onWebEvent method
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
    getRepeated1().handleWebAction(WebActions.ACTION_NEXT);
		////handler.handleForwardButton();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtForwardButton()
	{
		return (Button)getChild(CHILD_BTFORWARDBUTTON);
	}


	/**
	 *
	 *
	 */
  public boolean beginBtForwardButtonDisplay(ChildDisplayEvent event)
      throws IOException
  {
      boolean result=((doLifeDisabilityInsuranceInfoModelImpl)(getRepeated1().getdoLifeDisabilityInsuranceInfoModel())).hasMoreResults();

      return result;
  }

	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTotalLoanAmount()
	{
		return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btPrevTaskPage_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handlePrevTaskPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method

		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generatePrevTaskPage();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generatePrevTaskPage();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateTaskName();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btNextTaskPage_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleNextTaskPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method

		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateNextTaskPage();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCancel_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleCancelStandard(true);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
	public String endBtCancelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btCancel_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		/$ As Product (Joe) requested the DealNote should be always Ediable
				LifeDisabilityInsuranceHandler handler = (LifeDisabilityInsuranceHandler)this.handler.cloneSS();

				handler.pageGetState(this);
				int rc = handler.displayCancelButton();
		        handler.pageSaveState();

				return rc;
				$/
		return PROCEED;
		*/

		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */

	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStMortgageInsuranceAmount()
	{
		return (StaticTextField)getChild(CHILD_STMORTGAGEINSURANCEAMOUNT);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStPaymentFrequency()
	{
		return (StaticTextField)getChild(CHILD_STPAYMENTFREQUENCY);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStSpecialFeature()
	{
		return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStRowsDisplayed()
	{
		return (StaticTextField)getChild(CHILD_STROWSDISPLAYED);
	}

  //--DJ_CR136--start--//
  /**
   *
   *
   */
  public StaticTextField getStRowsDisplayedInsureOnlyAppl()
  {
    return (StaticTextField)getChild(CHILD_STROWSDISPLAYEDINSUREONLYAPPL);
  }
  //--DJ_CR136--end--//

	/**
	 *
	 *
	 */
	public HiddenField getHdInsuranceProportionsId()
	{
		return (HiddenField)getChild(CHILD_HDINSURANCEPROPORTIONSID);
	}

	/**
	 *
	 *
	 */
	public void handleBtOKRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btOK_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleCancelStandard();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtOK()
	{
		return (Button)getChild(CHILD_BTOK);
	}


	/**
	 *
	 *
	 */
	public String endBtOKDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		/$ As Product (Joe) requested the DealNote should be always Ediable
				LifeDisabilityInsuranceHandler handler = (LifeDisabilityInsuranceHandler)this.handler.cloneSS();

				handler.pageGetState(this);
				int rc = handler.displayOKButton();
		        handler.pageSaveState();

				return rc;
				$/
		return SKIP;
		*/

		//return event.getContent();
    //Skip display always
    return "";
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the changePasswordHref_onWebEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleChangePassword();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public StaticTextField getStViewOnlyTag()
	{
		return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
	}


	/**
	 *
	 *
	 */
	public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
	  handler.pageGetState(this);
		String rc = handler.displayViewOnlyTag();
		handler.pageSaveState();

    return rc;
	}

  /**
	public String endHdOutputFromRTPInfocalCalcDisplay(ChildContentDisplayEvent event)
	{
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
	  handler.pageGetState(this);

		String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) this.getChild(fieldName);




    fld.setValue(event.getContent());


		handler.pageSaveState();

    return fld.getValue().toString();
	}
  **/

	/**
	 *
	 *
	 */
	public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
	{
		if (doDealSummarySnapShot == null)
			doDealSummarySnapShot = (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
		return doDealSummarySnapShot;
	}

	/**
	 *
	 *
	 */
	public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
	{
			doDealSummarySnapShot = model;
	}

	/**
	 *
	 *
	 */
	public doLifeDisabilityInsProportionsModel getdoLifeDisabilityInsProportionsModel()
	{
		if (doLifeDisabilityInsProportions == null)
			doLifeDisabilityInsProportions = (doLifeDisabilityInsProportionsModel) getModel(doLifeDisabilityInsProportionsModel.class);
		return doLifeDisabilityInsProportions;
	}

	/**
	 *
	 *
	 */
	public void doLifeDisabilityInsProportionsModel(doLifeDisabilityInsProportionsModel model)
	{
			doLifeDisabilityInsProportions = model;
	}

  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}

  public String endHref1Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }
  //=====================================================
  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

  //// VERY IMPORTANT TEST!!
  /**
  public String getOutFromInfocal()
  {
    logger = SysLog.getSysLogger("LDIP");
    String ret = "";

    RequestContext requestContext = getRequestContext();
    Enumeration tmpEm = requestContext.getRequest().getParameterNames();
    for(int iPara = 0 ; tmpEm.hasMoreElements(); iPara++)
    {
      String currPara = tmpEm.nextElement().toString();
      logger.debug("VLAD ==> The Request ParameterName[" + iPara + "]::" + currPara);
      logger.debug("VLAD ==> The Request ParameterValue[" + iPara + "]::" + requestContext.getRequest().getParameter(currPara));
      if(currPara.equals("pgLifeDisabilityInsurance_hdOutputFromRTPInfocalCalc"))
      {
         ret = requestContext.getRequest().getParameter(currPara).toString();
      }
    }

    return ret;
  }
  **/
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////



	public void handleActMessageOK(String[] args)
	{
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleActMessageOK(args);
		handler.postHandlerProtocol();

		//return;
	}

	public void handleRecalcInfocal(String[] args)
	{
		LifeDisabilityInsuranceHandler handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleRecalcInfocal(args);

		////handler.postHandlerProtocol();
		////handler =(LifeDisabilityInsuranceHandler) this.handler.cloneSS();
    ////handler.preHandlerProtocol(this);
		handler.handleUpdateBorrowers(true, args);
		handler.postHandlerProtocol();

	}

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgLifeDisabilityInsurance";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
	////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgLifeDisabilityInsurance.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //The Option list should not be static, otherwise this will screw up
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

  public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_STDEALID="stDealId";
	public static final String CHILD_STDEALID_RESET_VALUE="";
	public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
	public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUS="stDealStatus";
	public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
	public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STSOURCEFIRM="stSourceFirm";
	public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
	public static final String CHILD_STSOURCE="stSource";
	public static final String CHILD_STSOURCE_RESET_VALUE="";
	public static final String CHILD_STLOB="stLOB";
	public static final String CHILD_STLOB_RESET_VALUE="";
	public static final String CHILD_STDEALTYPE="stDealType";
	public static final String CHILD_STDEALTYPE_RESET_VALUE="";
	public static final String CHILD_STDEALPURPOSE="stDealPurpose";
	public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
	public static final String CHILD_STPURCHASEPRICE="stPurchasePrice";
	public static final String CHILD_STPURCHASEPRICE_RESET_VALUE="";
	public static final String CHILD_STPMTTERM="stPmtTerm";
	public static final String CHILD_STPMTTERM_RESET_VALUE="";
	public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
	public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";

  private CbInsProportionsOptionList cbInsProportionsOptions=new CbInsProportionsOptionList();
	public static final String CHILD_CBINSPROPORTIONS="cbInsProportions";
	public static final String CHILD_CBINSPROPORTIONS_RESET_VALUE="";

	public static final String CHILD_TBNOTETEXT="tbNoteText";
	public static final String CHILD_TBNOTETEXT_RESET_VALUE="";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE="";
	public static final String CHILD_REPEATED1="Repeated1";
  //--DJ_CR136--23Nov2004--start--//
  public static final String CHILD_REPEATED2="Repeated2";
  public static final String CHILD_BTADDINSUREONLYAPPLICANT="btAddInsureOnlyApplicant";
  public static final String CHILD_BTADDINSUREONLYAPPLICANT_RESET_VALUE=" ";
  public static final String CHILD_STTARGETINSUREONLYAPPLICANT="stUWTargetDownPayment";
  public static final String CHILD_STTARGETINSUREONLYAPPLICANT_RESET_VALUE="";
  //--DJ_CR136--23Nov2004--start--//
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_BTBACKWARDBUTTON="btBackwardButton";
	public static final String CHILD_BTBACKWARDBUTTON_RESET_VALUE="Previous";
	public static final String CHILD_BTFORWARDBUTTON="btForwardButton";
	public static final String CHILD_BTFORWARDBUTTON_RESET_VALUE="Next";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
	public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE="Cancel";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
	public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";
	public static final String CHILD_STROWSDISPLAYED="stRowsDisplayed";
	public static final String CHILD_STROWSDISPLAYED_RESET_VALUE="";

  //--DJ_CR136--start--//
  public static final String CHILD_STROWSDISPLAYEDINSUREONLYAPPL="stRowsDisplayedInsureOnlyAppl";
  public static final String CHILD_STROWSDISPLAYEDINSUREONLYAPPL_RESET_VALUE="";
  //--DJ_CR136--end--//

	public static final String CHILD_BTOK="btOK";
	public static final String CHILD_BTOK_RESET_VALUE="OK";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
	public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
 	public static final String CHILD_RPTDEALNOTEWIN="rptDealNoteWin";
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";

	public static final String CHILD_STMORTGAGEINSURANCEAMOUNT="stMortgageInsuranceAmount";
	public static final String CHILD_STMORTGAGEINSURANCEAMOUNT_RESET_VALUE="";

	public static final String CHILD_STPAYMENTFREQUENCY="stPaymentFrequency";
	public static final String CHILD_STPAYMENTFREQUENCY_RESET_VALUE="";

	public static final String CHILD_HDINSURANCEPROPORTIONSID="hdInsuranceProportionsId";
	public static final String CHILD_HDINSURANCEPROPORTIONSID_RESET_VALUE="";

	public static final String CHILD_BTRECALCULATE="btRecalculate";
	public static final String CHILD_BTRECALCULATE_RESET_VALUE="Recalculate";

	public static final String CHILD_STTOTALPREMIUM="stTotalPremium";
	public static final String CHILD_STTOTALPREMIUM_RESET_VALUE="";

	public static final String CHILD_TXLIFEPERCCOVERAGEREQ="txLifePercCoverageReqReq";
	public static final String CHILD_TXLIFEPERCCOVERAGEREQ_RESET_VALUE="";

	public static final String CHILD_STUWPIPAYMENT="stUWPIPayment";
	public static final String CHILD_STUWPIPAYMENT_RESET_VALUE="";

	public static final String CHILD_STUWNETRATE="stUWNetRate";
	public static final String CHILD_STUWNETRATE_RESET_VALUE="";

	public static final String CHILD_STLIFEPREMIUM="stLifePremium";
	public static final String CHILD_STLIFEPREMIUM_RESET_VALUE="";

	public static final String CHILD_STDISABILITYPREMIUM="stDisabilityPremium";
	public static final String CHILD_STDISABILITYPREMIUM_RESET_VALUE="";

	public static final String CHILD_STCOMBINEDLDPREMIUM="stCombinedLDPremium";
	public static final String CHILD_STCOMBINEDLDPREMIUM_RESET_VALUE="";

	public static final String CHILD_HDINPUTTORTPCALC="hdInputToRTPCalc";
	public static final String CHILD_HDINPUTTORTPCALC_RESET_VALUE="";

	public static final String CHILD_HDINPUTTOINFOCALCALC="hdInputToInfocalCalc";
	public static final String CHILD_HDINPUTTOINFOCALCALC_RESET_VALUE="";

	public static final String CHILD_HDOUTPUTFROMRTPINFOCALCALC="hdOutputFromRTPInfocalCalc";
	public static final String CHILD_HDOUTPUTFROMRTPINFOCALCALC_RESET_VALUE="";

	public static final String CHILD_STPMNTINCLUDINGLIFEDISABILITY="stPmntInclLifeDis";
	public static final String CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE="";

	public static final String CHILD_STTOTALRATEINCLLIFEDISABILITY="stTotalRateInclLifeDis";
	public static final String CHILD_STTOTALRATEINCLLIFEDISABILITY_RESET_VALUE="";

	public static final String CHILD_STADDRATEFORLIFEDISABILITY="stAddRateForLifeDisability";
	public static final String CHILD_STADDRATEFORLIFEDISABILITY_RESET_VALUE="";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealSummarySnapShotModel doDealSummarySnapShot=null;
	protected doLifeDisabilityInsProportionsModel doLifeDisabilityInsProportions=null;
	protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgLifeDisabilityInsurance.jsp";
	public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	private LifeDisabilityInsuranceHandler handler=new LifeDisabilityInsuranceHandler();

}

