package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;

/**
 *
 *
 *
 */
public class pgSessionEndedViewBean extends ExpressViewBeanBase
 
{
  /**
   *
   *
   */
  public pgSessionEndedViewBean()
  {
    super(PAGE_NAME);
    setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

    registerChildren();

    initialize();

  }


  /**
   *
   *
   */
  protected void initialize()
  {
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected View createChild(String name)
  {
    if (name.equals(CHILD_STMESSAGE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STMESSAGE,
        CHILD_STMESSAGE,
        CHILD_STMESSAGE_RESET_VALUE,
        null);
      return child;
    }
    else
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
  }


  /**
   *
   *
   */
  public void resetChildren()
  {
	  super.resetChildren(); 
	  getStMessage().setValue(CHILD_STMESSAGE_RESET_VALUE);
  }


  /**
   *
   *
   */
  protected void registerChildren()
  {
	  super.registerChildren(); 
	  registerChild(CHILD_STMESSAGE,StaticTextField.class);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList=new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        ;
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return (Model[])modelList.toArray(new Model[0]);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event)
    throws ModelControlException
  {
    super.beginDisplay(event);
  }


  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {

    // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);

  }


  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {

    // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);

  }


  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {

    // This is the analog of NetDynamics this_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }


  /**
   *
   *
   */
  public StaticTextField getStMessage()
  {
    return (StaticTextField)getChild(CHILD_STMESSAGE);
  }


  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       //// There is no handler for this page: go to SessionStateModel directly.
       String defaultInstanceStateName = this.getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
       SessionStateModelImpl theSessionState = (SessionStateModelImpl)this.getRequestContext().getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);

       int languageId = theSessionState.getLanguageId();
       String url=getDefaultDisplayURL();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }
      return url;
  }


  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String PAGE_NAME="pgSessionEnded";
  public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgSessionEnded.jsp";
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_STMESSAGE="stMessage";
  public static final String CHILD_STMESSAGE_RESET_VALUE="";

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////


}

