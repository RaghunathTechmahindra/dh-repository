package mosApp.MosSystem;

import java.io.*;
import java.util.*;
import com.basis100.log.*;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

import MosSystem.*;

/**
 *
 *
 */
public interface SavedPagesModel extends Model, Sc
{	
// 3.3 ML these fields will never be used.
//	public static final int QUEUE_SIZE_MAX=10;
//	public PageEntry workQueue=new PageEntry();
//	public PageEntry searchPage=null;
//	////public boolean nextPageSet;
//	////public boolean backPageSet;
//	public boolean nextPageSet=false;
//	public boolean backPageSet=false;
//	public PageEntry backPage=new PageEntry();
//	public PageEntry nextPage=new PageEntry();
//	public boolean[] previousPagesLinkFlag=new boolean[QUEUE_SIZE_MAX];
//	public String[] previousPagesLinkText=new String[QUEUE_SIZE_MAX];
//	public PageQueue previousPagesQueue=new PageQueue(QUEUE_SIZE_MAX);
//	public int queueSizeMax=QUEUE_SIZE_MAX;


	/**
	 *
	 *
	 */
	public void addToPreviousPages(PageEntry pe);


	/**
	 *
	 *
	 */
	public void setNextPageAsPreviousPage(int queueNdx);

	/**
	 *
	 *
	 */
	public void setNextPageAsWorkQueue();

	/**
	 *
	 *
	 */
	public void setNextPageAsWorkQueue(boolean refresh);


	/**
	 *
	 *
	 */
	public void setNextPageAsBackPage();

	/**
	 *
	 *
	 */
	public void setNextPageAsDealSearch();

	/**
	 *
	 *
	 */
	public void clearPageSetFlags();


	/**
	 *
	 *
	 */
	public void revisePreviousPagesLinks();

	/**
	 *
	 *
	 */
	////private void initPreviousPagesLinkText();

	/**
	 *
	 *
	 */
	public boolean isNextPageSet();

	/**
	 *
	 *
	 */
	public boolean isBackPageSet();

	/**
	 *
	 *
	 */
	public PageEntry getBackPage();

	/**
	 *

	 */
	public PageEntry getNextPage();

	/**
	 *
	 *
	 */
	public boolean getPreviousPagesLinkFlag(int position);

	/**
	 *
	 *
	 */
	public String getPreviousPagesLinkText(int position);


	/**
	 *
	 *
	 */
	public PageQueue getPreviousPagesQueue();

	/**
	 *
	 *
	 */
	public PageEntry getWorkQueue();

	/**
	 *
	 *
	 */
	public PageEntry getSearchPage();

	/**
	 *
	 *
	 */
	public void setBackPage(PageEntry aBackPage);

	/**
	 *
	 *
	 */
	public void setNextPage(PageEntry aPageEntry);

	/**
	 *
	 *
	 */
	public void setWorkQueue(PageEntry aWorkQueue);

	/**
	 *
	 *
	 */
	public void setSearchPage(PageEntry aSearchPage);

	/**
	 *
	 *
	 */
	public void show(SysLogger logger);

}

