package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *  DJ_CR136.
 *  Insure ONLY type of Borrower implementation
 */
public interface doInsureOnlyApplicantModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();

	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();

	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public void setDfInsureOnlyApplicantId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfInsureOnlyApplicantId();

	/**
	 *
	 *
	 */
	public String getDfFirstInsureOnlyApplicantName();

	/**
	 *
	 *
	 */
	public void setDfInsureOnlyApplicantFirstName(String value);

	/**
	 *
	 *
	 */
	public String getDfLastInsureOnlyApplicantName();

	/**
	 *
	 *
	 */
	public void setDfInsureOnlyApplicantLastName(String value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfLifePercentCoverageReq();

	/**
	 *
	 *
	 */
	public void setDfLifePercentCoverageReq(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfLifeStatusDesc();

	/**
	 *
	 *
	 */
	public void setDfLifeStatusDesc(String value);

	/**
	 *
	 *
	 */
	public String getDfDisabilityStatusDesc();

	/**
	 *
	 *
	 */
	public void setDfDisabilityStatusDesc(String value);

	/**
	 *
	 *
	 */
	public void setDfAge(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfAge();


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfInsureOnlyApplicantBirthDate();

	/**
	 *
	 *
	 */
	public void setDfInsureOnlyApplicantBirthDate(java.sql.Timestamp value);

	/**
	 *
	 *
	 */
	public void setDfCopyID(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfPrimaryInsureOnlyApplicantFlag();

	/**
	 *
	 *
	 */
	public void setDfPrimaryInsureOnlyApplicantFlag(String value);

	/**
	 *
	 *
	 */
	public void setDfInsureOnlyApplicantGenderId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfInsureOnlyApplicantGenderId();

	/**
	 *
	 *
	 */
	public void setDfLifeStatusValue(String value);

	/**
	 *
	 *
	 */
  public String getDfLifeStatusValue();

	/**
	 *
	 *
	 */
	public void setDfDisabilityStatusValue(String value);

	/**
	 *
	 *
	 */
  public String getDfDisabilityStatusValue();

	/**
	 *
	 *
	 */
	public String getDfInsureOnlyApplicantSmoker();

	/**
	 *
	 *
	 */
	public void setDfInsureOnlyApplicantSmoker(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLifeStatusId();

	/**
	 *
	 *
	 */
	public void setDfLifeStatusId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDisabilityStatusId();

	/**
	 *
	 *
	 */
	public void setDfDisabilityStatusId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public void setDfSmokerStatusId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfSmokerStatusId();

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
  public static final String FIELD_DFINSUREONLYAPPLICANTID="dfInsureOnlyApplicantId";
  public static final String FIELD_DFINSUREONLYAPPLICANTFIRSTNAME="dfInsureOnlyApplicantFirstName";
  public static final String FIELD_DFINSUREONLYAPPLICANTLASTNAME="dfInsureOnlyApplicantLastName";
  public static final String FIELD_DFPRIMARYINSUREONLYAPPLICANTFLAG="dfPrimaryInsureOnlyApplicantFlag";
  public static final String FIELD_DFLIFESTATUSDESC="dfLifeStatusDesc";
  public static final String FIELD_DFDISABILITYSTATUSDESC="dfDisabilityStatusDesc";
  public static final String FIELD_DFLIFESTATUSVALUE="dfLifeStatusValue";
  public static final String FIELD_DFLDISABILITYSTATUSVALUE="dfDisabilityStatusValue";
  public static final String FIELD_DFAGE="dfAge";
  public static final String FIELD_DFINSUREONLYAPPLICANTBIRTHDATE="dfInsureOnlyApplicantBirthDate";
  public static final String FIELD_DFINSUREONLYAPPLICANTGENDERID="dfInsureOnlyApplicantGenderId";
  public static final String FIELD_DFSMOKER="dfSmoker";
  public static final String FIELD_DFLIFESTATUSID="dfLifeStatusId";
  public static final String FIELD_DFDISABILITYSTATUSID="dfDisabilityStatusId";
  public static final String FIELD_DFSMOKERSTATUSID="dfSmokerStatusId";
  public static final String FIELD_DFLIFEPERCENTCOVERAGEREQ="dfLifePercentCoverageReq";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

