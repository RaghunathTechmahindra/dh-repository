package mosApp.MosSystem;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

import MosSystem.Sc;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.picklist.PicklistData;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;


//============================================================================================
	//Developer Note:
	//
	//  1) Use PageCondition3 to switch display different Cancel Button
	//      -- false (default, first entry) ==> display 'Cancel' button
	//      -- true (refresh screen by add/delete sub records, e.g. Liability, asset ...
	//            or recalculate button pressed ==> display 'Cancel Current Changes Only button.
	//      -- By BILLY 03May2002
	//============================================================================================

public class PropertyEntryHandler extends DealHandlerCommon
	implements Cloneable
{

	public PropertyEntryHandler()
	{
    super();
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
	}

	public PropertyEntryHandler(String name)
	{
		this(null,name); // No parent
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
	}

	protected PropertyEntryHandler(View parent, String name)
	{
		super(parent,name);
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
	}

  /**
	 *
	 *
	 */
	public PropertyEntryHandler cloneSS()
	{
		return(PropertyEntryHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();
    //logger.debug("PEH@populatePageDisplayFields::after populatePageShellDisplayFields");
		
		// Quick Link Menu display
	    displayQuickLinkMenu();
	    
		populateTaskNavigator(pg);
    //logger.debug("PEH@populatePageDisplayFields::after populateTaskNavigator");

		//// error(?!!) from ND project: double identical call. Should be eliminated.
    ////populateTaskNavigator(pg);

		populatePreviousPagesLinks();
    //logger.debug("PEH@populatePageDisplayFields::after populatePreviousPageLinks");

		ViewBean thePage = getCurrNDPage();

		thePage.setDisplayFieldValue("stTargetPE", new String(TARGET_TAG));

		populateDefaultsVALSData(thePage);
    //logger.debug("PEH@populatePageDisplayFields::after populateDefaultVALSData");

    ////Data validation API.
    try
    {
      String propertyEntryMainValidPath = Sc.DVALID_PROPERTY_MAIN_PROP_FILE_NAME;

      //logger.debug("PEH@dealEntryMainValid: " + propertyEntryMainValidPath);

      JSValidationRules propertyEntryValidObj = new JSValidationRules(propertyEntryMainValidPath, logger, thePage);
      propertyEntryValidObj.attachJSValidationRules();
    }
    catch( Exception e )
    {
        logger.error("Exception PEH@PopulatePageDisplayFields: Problem encountered in JS data validation API." + e.getMessage());
    }

    //logger.debug("PEH@populatePageDisplayFields::after data validation API");

		// Customize html elements: convert the Total Text Boxes to UnEditable
		customizeTextBox("txTotalPropertyExp");

                //--BMO_MI_CR--start//
                //// To provide proper parse of page values to Entity (via db property) for the page.
                //// The basic method parsePageValuesToEntity is totally reconsidered and there is no
                //// need for this setting anymore.
                //pg.setPageCondition9(true);
                //--BMO_MI_CR--end//
	}


  //Method to populate default value data to the HTML
	private void populateDefaultsVALSData(ViewBean thePage)
	{
		String onChange = "";

		// the OnChange string to be set on the ComboBox
		HtmlDisplayFieldBase df;

    //--> Temp fix to avoid getting PickList everytime displaying the screen.
    //--> The valsData is initialized and cached in the static Variable VALS_DATA
    //--> all valsData setup is commented as //>>>
    //--> By Billy 02Dec2003
		//>>>String valsData = "";
    //logger.debug("PEH@populateJSVALSData::ViewBeanIn: " + thePage);
		// The VAL String to be populated on the HTML
		// ========== Set Property Expense Type default values (Begin) ==========
		// Set PropertyExpenseType array
		//>>>valsData += "VALScbPropertyExpenseType = new Array(" + PicklistData.getColumnValuesCDV("PROPERTYEXPENSETYPE", "PROPERTYEXPENSETYPEID") + ");" + "\n";
		// Set PropertyExpenseGDSPercentage array
		//>>>valsData += "VALStxPropertyExpenseGDSPercentage = new Array(" + PicklistData.getColumnValuesCDS("PROPERTYEXPENSETYPE", "GDSINCLUSION") + ");" + "\n";
    //logger.debug("PEH@populateJSVALSData::VALStxPropertyExpenseGDSPercentage: " + valsData);
		// Set Hidden PropertyExpenseGDSPercentage array for double default
		//>>>valsData += "VALShdPropertyExpenseGDSPercentage = new Array(" + PicklistData.getColumnValuesCDS("PROPERTYEXPENSETYPE", "GDSINCLUSION") + ");" + "\n";
		// Set PropertyExpenseIncludeGDS array
		//>>>valsData += "VALScbPropertyExpenseIncludeGDS = new Array(" + PicklistData.getValuesCDS("PROPERTYEXPENSETYPE", "GDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
    //logger.debug("PEH@populateJSVALSData::VALScbPropertyExpenseIncludeGDS: " + valsData);
		// Set PropertyExpenseTDSPercentage array
		//>>>valsData += "VALStxPropertyExpenseTDSPercentage = new Array(" + PicklistData.getColumnValuesCDS("PROPERTYEXPENSETYPE", "TDSINCLUSION") + ");" + "\n";
    //logger.debug("PEH@populateJSVALSData::VALStxPropertyExpenseTDSPercentage: " + valsData);
		// Set hidden PropertyExpenseTDSPercentage array for double default
		//>>>valsData += "VALShdPropertyExpenseTDSPercentage = new Array(" + PicklistData.getColumnValuesCDS("PROPERTYEXPENSETYPE", "TDSINCLUSION") + ");" + "\n";
		// Set PropertyExpenseIncludeTDS array
		//>>>valsData += "VALScbPropertyExpenseIncludeTDS = new Array(" + PicklistData.getValuesCDS("PROPERTYEXPENSETYPE", "TDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
    //logger.debug("PEH@populateJSVALSData::VALScbPropertyExpenseIncludeTDS: " + valsData);
    //======================================================================================

		// Setup OnChange event handling to populate default fields
		////df = thePage.getDisplayField("RepeatedPropertyExpenses.cbPropertyExpenseType");
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPropertyExpenses/cbPropertyExpenseType");

		onChange = "onChange=\"setDefaults('cbPropertyExpenseType', " + "['txPropertyExpenseGDSPercentage','hdPropertyExpenseGDSPercentage','cbPropertyExpenseIncludeGDS','txPropertyExpenseTDSPercentage','hdPropertyExpenseTDSPercentage','cbPropertyExpenseIncludeTDS'], " + "'txPropertyExpenseGDSPercentage');\"";

		((HtmlDisplayFieldBase) df).setExtraHtml(onChange);

		// Setup OnChange event handling to populate default fields for cbPropertyExpenseIncludeGDS
		////df = thePage.getDisplayField("RepeatedPropertyExpenses.cbPropertyExpenseIncludeGDS");
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPropertyExpenses/cbPropertyExpenseIncludeGDS");

    // Handle double default -- by BILLY 19Apr2001
		////onChange = "onChange=\"setValueIfMatched('cbPropertyExpenseIncludeGDS', " + "['txPropertyExpenseGDSPercentage'], " + "'N', " + "'0', "
    //// + "'txPropertyExpenseGDSPercentage');" + "setValueIfMatchedFromOtherField('cbPropertyExpenseIncludeGDS', " +
		////"['txPropertyExpenseGDSPercentage'], " + "'Y', " + "'hdPropertyExpenseGDSPercentage', " + "'txPropertyExpenseGDSPercentage');\"";

		onChange = "onChange=\"setValueIfMatched(['txPropertyExpenseGDSPercentage'], 'N', '0');" +
              "setValueIfMatchedFromOtherField(['txPropertyExpenseGDSPercentage'], 'Y', 'hdPropertyExpenseGDSPercentage');\"";

		((HtmlDisplayFieldBase) df).setExtraHtml(onChange);

		// Setup OnChange event handling to populate default fields for cbPropertyExpenseIncludeTDS
		////df = thePage.getDisplayField("RepeatedPropertyExpenses.cbPropertyExpenseIncludeTDS");
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedPropertyExpenses/cbPropertyExpenseIncludeTDS");

    // Handle double default -- by BILLY 19Apr2001
		////onChange = "onChange=\"setValueIfMatched('cbPropertyExpenseIncludeTDS', " + "['txPropertyExpenseTDSPercentage'], " + "'N', " + "'0', " + "'txPropertyExpenseTDSPercentage');" + "setValueIfMatchedFromOtherField('cbPropertyExpenseIncludeTDS', " +
		////"['txPropertyExpenseTDSPercentage'], " + "'Y', " + "'hdPropertyExpenseTDSPercentage', " + "'txPropertyExpenseTDSPercentage');\"";

		onChange = "onChange=\"setValueIfMatched(['txPropertyExpenseTDSPercentage'], 'N', '0');"
              + "setValueIfMatchedFromOtherField(['txPropertyExpenseTDSPercentage'], 'Y', 'hdPropertyExpenseTDSPercentage');\"";

		((HtmlDisplayFieldBase) df).setExtraHtml(onChange);


		// ========== Set Employment Income Type default values (End) ==========
		// Setup onchange event to populate Purchase Price to Estimated Value -- By BILLY 17Jan2001
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txPurchasePrice");

		////onChange = "onBlur=\"populateValueTo('txPurchasePrice', " + "['txEstimatedValue'], " + "'txPurchasePrice'); " + "if(isFieldInDecRange(0, 99999999999.99))isFieldDecimal(2);\"";
    onChange = "onBlur=\"populateValueTo(['txEstimatedValue']);\""; //// the rest of the line goes
                                                                    //// to the data validation API.

		// Field Validation
		df.setExtraHtml(onChange);

    //--> Temp fix to avoid getting PickList everytime displaying the screen.
    //--> The valsData is initialized and cached in the static Variable VALS_DATA
    //--> all valsData setup is commented as //>>>
    //--> By Billy 02Dec2003
		// Setup Expense Preoid arrays for calculation of TotalExpense (Calc 53)
		//>>valsData += "VALScbExpensePeriod = new Array(" + PicklistData.getColumnValuesCDV("propertyexpenseperiod", "propertyexpenseperiodid") + ");" + "\n";
    //logger.debug("PEH@populateJSVALSData::VALScbExpensePeriod: " + valsData);
		//>>valsData += "VALSExpenseMultiplier = new Array(" + PicklistData.getColumnValuesCDV("propertyexpenseperiod", "annualexpensemultiplier") + ");" + "\n";
    //logger.debug("PEH@populateJSVALSData::VALSExpenseMultiplier: " + valsData);

    //>>>thePage.setDisplayFieldValue("stVALSData", new String(valsData));
    thePage.setDisplayFieldValue("stVALSData", VALS_DATA);
    //==============================================================================

	}

	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);

		// -- //
		int dealId = pg.getPageDealId();

		if (dealId <= 0)
		{
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Problem encountered displaying deal Entry - invalid dealId (" + dealId + ")");
			return;
		}

		Integer propertyId = new Integer(pg.getPageCriteriaId1());

		int copyid = theSessionState.getCurrentPage().getPageDealCID();

		logger.trace("--T--> @PropertyEntryHandler.setupBeforePageGeneration:: propertyId=" +(pg.getPageCriteriaId1() + ", copyId=" + copyid));

		Integer cspPropertyCPId = new Integer(copyid);

		filterPropertyEntryMain(propertyId, cspPropertyCPId);

		filterPropertyEntryPropertyExpense(propertyId, cspPropertyCPId);

	}


	/**
	 *
	 *
	 */
	private void filterPropertyEntryMain(Integer propertyId, Integer copyid)
	{

		doPropertyEntryPropertySelectModel dodealpropentrymain =(doPropertyEntryPropertySelectModel)
                                        RequestManager.getRequestContext().getModelManager().getModel(doPropertyEntryPropertySelectModel.class);

		dodealpropentrymain.clearUserWhereCriteria();

		dodealpropentrymain.addUserWhereCriterion("dfPropertyId", "=", propertyId);

		dodealpropentrymain.addUserWhereCriterion("dfCopyId", "=", copyid);

    try
    {
      ResultSet rs = dodealpropentrymain.executeSelect(null);

      //if(rs == null || dodealpropentrymain.getSize() < 0)
      //{
        //logger.debug("PEH@filterPropertyEntryMain::No row returned!!");
      //}

      //else if(rs != null && dodealpropentrymain.getSize() > 0)
      //{
        //// Do nothing, everything is OK.
        //logger.debug("PEH@filterPropertyEntryMain::Size of the result set: " + dodealpropentrymain.getSize());
      //}
    }
    catch (ModelControlException mce)
    {
      logger.warning("PEH@filterPropertyEntryMain::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("PEH@filterPropertyEntryMain::SQLException: " + sqle);
    }

	}


	/**
	 *
	 *
	 */
	private void filterPropertyEntryPropertyExpense(Integer propertyId, Integer copyid)
	{

		doPropertyExpenseSelectModel dodealpropentryexp =(doPropertyExpenseSelectModel)
                                        RequestManager.getRequestContext().getModelManager().getModel(doPropertyExpenseSelectModel.class);

		dodealpropentryexp.clearUserWhereCriteria();

		dodealpropentryexp.addUserWhereCriterion("dfPropertyId", "=", propertyId);

		dodealpropentryexp.addUserWhereCriterion("dfCopyId", "=", copyid);

    try
    {
      ResultSet rs = dodealpropentryexp.executeSelect(null);

      //if(rs == null || dodealpropentryexp.getSize() < 0)
      //{
      //  logger.debug("PEH@filterPropertyEntryPropertyExpense::No row returned!!");
      //}

      //else if(rs != null && dodealpropentryexp.getSize() > 0)
      //{
      //  //// Do nothing, everything is OK.
      //  logger.debug("PEH@filterPropertyEntryPropertyExpense::Size of the result set: " + dodealpropentryexp.getSize());
      //}
    }
    catch (ModelControlException mce)
    {
      logger.warning("PEH@filterPropertyEntryPropertyExpense::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("PEH@filterPropertyEntryPropertyExpense::SQLException: " + sqle);
    }
	}


	/**
	 *
	 *
	 */
	public void SaveEntity(ViewBean webPage)
		throws Exception
	{
		logger.trace("--T--> @PropertyEntryHandler.SaveEntity");

		CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

		;

		try
		{
			String [ ] propkeys =
			{
				"hdPropertyId", "hdPropertyCopyId"			}
			;
			handleUpdateProperty(PROPERTY_PROP_FILE_NAME, propkeys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] propexpkeys =
			{
				"RepeatedPropertyExpenses/hdPropertyExpenseId", "RepeatedPropertyExpenses/hdPropertyExpenseCopyId"			}
			;
			handleUpdatePropertyExpense(PROPEXP_PROP_FILE_NAME, propexpkeys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			doCalculation(dcm);
		}
		catch(Exception ex)
		{
			logger.error("Exception occurred @doCalculation");
			throw new Exception("Exception @SaveEntity");
		}

	}


	public int getRowDisplaySetting(QueryModelBase theModel)
	{
		//logger.trace("@getRowDisplayStringFor Property IN do "+dobject);
    PageEntry pg = theSessionState.getCurrentPage();
    int numRows = 0;

    Integer cspPropertyId = new Integer(pg.getPageCriteriaId1());
    Integer cspCopyId = new Integer(pg.getPageDealCID());

    //logger.debug("PEH@getRowDisplaySetting::PropertyId: " + cspPropertyId);
    //logger.debug("PEH@getRowDisplaySetting::CopyId: " + cspCopyId);

    ////com.iplanet.jato.model.sql.QueryModelBase chkDoObject =(com.iplanet.jato.model.sql.QueryModelBase) CSpider.getDataObject(dobject);
    ////doPropertyExpenseSelectModel theModel = (doPropertyExpenseSelectModel)
    ////                                      RequestManager.getRequestContext().getModelManager().getModel(doPropertyExpenseSelectModel.class);

    theModel.clearUserWhereCriteria();
    theModel.addUserWhereCriterion("dfPropertyId", "=", cspPropertyId);
    theModel.addUserWhereCriterion("dfCopyId", "=", cspCopyId);


    ////chkDoObject.execute();
    ////int noofrows = chkDoObject.getLastResults().getNumRows();
    ////if (noofrows > 0) return CSpPage.PROCEED;
    ////return CSpPage.SKIP;

    try
    {
      ResultSet rs = theModel.executeSelect(null);

      if(rs == null || theModel.getSize() < 0)
      {
        numRows = 0;
      }

      else if(rs != null && theModel.getSize() > 0)
      {
        //logger.debug("PEH@getRowDisplaySetting::Size of the result set: " + theModel.getSize());
        numRows = theModel.getSize();
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("PEH@getRowDisplaySetting::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("PEH@getRowDisplaySetting::SQLException: " + sqle);
    }

    return numRows;
	}


	//===============================================================
	// Method to return the display status of the Cancel button
	//  Input : type (Button type)
	//            - 1 (Cancel)
	//            - 2 (Cancel current changes only)
	// Note : getPageCondition3() == false means first entry
	//                            == true means refresh within page
	//
	//  By Billy 03May2002
	//================================================================

	////public int displayCancelButton(int type)
	public boolean displayCancelButton(int type)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (type == 1 && pg.getPageCondition3() == true)
		{
			////return CSpPage.SKIP;
      //// SKIP is false in BX.
			return false;
		}

		if (type == 2 && pg.getPageCondition3() == false)
		{
			////return CSpPage.SKIP;
      //// SKIP is false in BX.
			return false;
		}

		return displayCancelButton();

	}

  //=====================================================================================================
  //--> Temp fix to avoid getting PickList everytime displaying the screen.
  //--> By Billy 02Dec2003
  // Define the static variable to store the JavaScript Array values
  private static String VALS_DATA = "";
  // To initial the JavaScript Array values when the Class loaded up the first time user access the screen
  static
  {
System.out.println("BILLY ===> Begin the setup VALS_DATA for PropertyEntryHandler !!");
    VALS_DATA += "VALScbPropertyExpenseType = new Array(" + PicklistData.getColumnValuesCDV("PROPERTYEXPENSETYPE", "PROPERTYEXPENSETYPEID") + ");" + "\n";
		VALS_DATA += "VALStxPropertyExpenseGDSPercentage = new Array(" + PicklistData.getColumnValuesCDS("PROPERTYEXPENSETYPE", "GDSINCLUSION") + ");" + "\n";
    VALS_DATA += "VALShdPropertyExpenseGDSPercentage = new Array(" + PicklistData.getColumnValuesCDS("PROPERTYEXPENSETYPE", "GDSINCLUSION") + ");" + "\n";
		VALS_DATA += "VALScbPropertyExpenseIncludeGDS = new Array(" + PicklistData.getValuesCDS("PROPERTYEXPENSETYPE", "GDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
    VALS_DATA += "VALStxPropertyExpenseTDSPercentage = new Array(" + PicklistData.getColumnValuesCDS("PROPERTYEXPENSETYPE", "TDSINCLUSION") + ");" + "\n";
    VALS_DATA += "VALShdPropertyExpenseTDSPercentage = new Array(" + PicklistData.getColumnValuesCDS("PROPERTYEXPENSETYPE", "TDSINCLUSION") + ");" + "\n";
		VALS_DATA += "VALScbPropertyExpenseIncludeTDS = new Array(" + PicklistData.getValuesCDS("PROPERTYEXPENSETYPE", "TDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
    VALS_DATA += "VALScbExpensePeriod = new Array(" + PicklistData.getColumnValuesCDV("propertyexpenseperiod", "propertyexpenseperiodid") + ");" + "\n";
    VALS_DATA += "VALSExpenseMultiplier = new Array(" + PicklistData.getColumnValuesCDV("propertyexpenseperiod", "annualexpensemultiplier") + ");" + "\n";
System.out.println("BILLY ===> After the setup VALS_DATA for PropertyEntryHandler !!");
  }
}

