package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doDocTrackingCusConditionModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfCusCondition();


	/**
	 *
	 *
	 */
	public void setDfCusCondition(String value);


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel();


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction();


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponsibility();


	/**
	 *
	 *
	 */
	public void setDfResponsibility(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCusDocTrackId();


	/**
	 *
	 *
	 */
	public void setDfCusDocTrackId(java.math.BigDecimal value);


  /**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLanguagePrederenceId();


	/**
	 *
	 *
	 */
	public void setDfLanguagePrederenceId(java.math.BigDecimal value);


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFCUSCONDITION="dfCusCondition";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String FIELD_DFDOCUMENTTEXTLITE="dfDocumentTextLite";
	//================================================================
	public static final String FIELD_DFDOCUMENTLABEL="dfDocumentLabel";
	public static final String FIELD_DFACTION="dfAction";
	public static final String FIELD_DFRESPONSIBILITY="dfResponsibility";
	public static final String FIELD_DFCUSDOCTRACKID="dfCusDocTrackId";
	//--Release2.1--//
  //--> Added LanguagePreferenceId DataField
	public static final String FIELD_DFLANGUAGEPREFERENCEID="dfLanguagePrederenceId";
  public static final String FIELD_DFBYPASS="dfByPass";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

