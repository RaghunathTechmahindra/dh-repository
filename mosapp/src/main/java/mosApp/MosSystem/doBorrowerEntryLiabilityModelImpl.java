package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doBorrowerEntryLiabilityModelImpl extends QueryModelBase
	implements doBorrowerEntryLiabilityModel
{
	/**
	 *
	 *
	 */
	public doBorrowerEntryLiabilityModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYMONTHLYPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityIncludingGDS()
	{
		return (String)getValue(FIELD_DFLIABILITYINCLUDINGGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityIncludingGDS(String value)
	{
		setValue(FIELD_DFLIABILITYINCLUDINGGDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityIncludingTDS()
	{
		return (String)getValue(FIELD_DFLIABILITYINCLUDINGTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityIncludingTDS(String value)
	{
		setValue(FIELD_DFLIABILITYINCLUDINGTDS,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT LIABILITY.BORROWERID, " +
            "LIABILITY.LIABILITYID, LIABILITY.LIABILITYTYPEID, LIABILITY.LIABILITYAMOUNT, " +
            "LIABILITY.LIABILITYMONTHLYPAYMENT, LIABILITY.INCLUDEINGDS, " +
            "LIABILITY.INCLUDEINTDS FROM LIABILITY  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="LIABILITY";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="LIABILITY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYID="LIABILITY.LIABILITYID";
	public static final String COLUMN_DFLIABILITYID="LIABILITYID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYTYPEID="LIABILITY.LIABILITYTYPEID";
	public static final String COLUMN_DFLIABILITYTYPEID="LIABILITYTYPEID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYAMOUNT="LIABILITY.LIABILITYAMOUNT";
	public static final String COLUMN_DFLIABILITYAMOUNT="LIABILITYAMOUNT";
	public static final String QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITY.LIABILITYMONTHLYPAYMENT";
	public static final String COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITYMONTHLYPAYMENT";
	public static final String QUALIFIED_COLUMN_DFLIABILITYINCLUDINGGDS="LIABILITY.INCLUDEINGDS";
	public static final String COLUMN_DFLIABILITYINCLUDINGGDS="INCLUDEINGDS";
	public static final String QUALIFIED_COLUMN_DFLIABILITYINCLUDINGTDS="LIABILITY.INCLUDEINTDS";
	public static final String COLUMN_DFLIABILITYINCLUDINGTDS="INCLUDEINTDS";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYID,
				COLUMN_DFLIABILITYID,
				QUALIFIED_COLUMN_DFLIABILITYID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYTYPEID,
				COLUMN_DFLIABILITYTYPEID,
				QUALIFIED_COLUMN_DFLIABILITYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYAMOUNT,
				COLUMN_DFLIABILITYAMOUNT,
				QUALIFIED_COLUMN_DFLIABILITYAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYMONTHLYPAYMENT,
				COLUMN_DFLIABILITYMONTHLYPAYMENT,
				QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYINCLUDINGGDS,
				COLUMN_DFLIABILITYINCLUDINGGDS,
				QUALIFIED_COLUMN_DFLIABILITYINCLUDINGGDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYINCLUDINGTDS,
				COLUMN_DFLIABILITYINCLUDINGTDS,
				QUALIFIED_COLUMN_DFLIABILITYINCLUDINGTDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

