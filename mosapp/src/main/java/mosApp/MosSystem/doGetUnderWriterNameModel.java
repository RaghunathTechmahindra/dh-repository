package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetUnderWriterNameModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUserProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUnderWriterName();

	
	/**
	 * 
	 * 
	 */
	public void setDfUnderWriterName(String value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFUSERPROFILEID="dfUserProfileId";
	public static final String FIELD_DFUNDERWRITERNAME="dfUnderWriterName";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

