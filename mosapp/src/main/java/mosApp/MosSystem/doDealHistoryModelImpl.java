package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class doDealHistoryModelImpl extends QueryModelBase
	implements doDealHistoryModel
{
	/**
	 *
	 *
	 */
	public doDealHistoryModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By John 25Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    int institutionId = theSessionState.getDealInstitutionId();
    if (institutionId == 0)
    {
        institutionId = theSessionState.getUserInstitutionId();
    }

    while (this.next())
    {
      //  Transaction type
      this.setDfTransactionType(BXResources.getPickListDescription(institutionId,
        "TRANSACTIONTYPE", this.getDfTransactionType(), languageId));

      //  Deal Status
      this.setDfStatus(BXResources.getPickListDescription(institutionId,
        "STATUS", this.getDfStatus(), languageId));
    }
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfApplicationId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAPPLICATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicationId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAPPLICATIONID,value);
	}
    
//    /**
//    *
//    *
//    */
//   public java.math.BigDecimal getDfInstitutionId()
//   {
//       return (java.math.BigDecimal)getValue(FIELD_DFINSTITUTIONID);
//   }
//
//
//   /**
//    *
//    *
//    */
//   public void setDfInstitutionId(java.math.BigDecimal value)
//   {
//       setValue(FIELD_DFINSTITUTIONID,value);
//   }


	/**
	 *
	 *
	 */
	public String getDfStatus()
	{
		return (String)getValue(FIELD_DFSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfStatus(String value)
	{
		setValue(FIELD_DFSTATUS,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfTransactionDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFTRANSACTIONDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfTransactionDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFTRANSACTIONDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTransactionText()
	{
		return (String)getValue(FIELD_DFTRANSACTIONTEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfTransactionText(String value)
	{
		setValue(FIELD_DFTRANSACTIONTEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUser()
	{
		return (String)getValue(FIELD_DFUSER);
	}


	/**
	 *
	 *
	 */
	public void setDfUser(String value)
	{
		setValue(FIELD_DFUSER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTransactionType()
	{
		return (String)getValue(FIELD_DFTRANSACTIONTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfTransactionType(String value)
	{
		setValue(FIELD_DFTRANSACTIONTYPE,value);
	}

	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doDealHistoryModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
   **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT DEALHISTORY.DEALID, DEALHISTORY.APPLICATIONID, STATUS.STATUSDESCRIPTION, DEALHISTORY.TRANSACTIONDATE, DEALHISTORY.TRANSACTIONTEXT, CONTACT.CONTACTLASTNAME || ',' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) USERNAME, SUBSTR(TRANSACTIONTYPE.TTDESCRIPTION,1,15) FROM CONTACT, DEALHISTORY, TRANSACTIONTYPE, USERPROFILE, STATUS  __WHERE__  ORDER BY DEALHISTORY.TRANSACTIONDATE  DESC";

  //--Release2.1--//
/* old
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DEALHISTORY.DEALID, DEALHISTORY.APPLICATIONID, " +
    "STATUS.STATUSDESCRIPTION, DEALHISTORY.TRANSACTIONDATE, " +
    "DEALHISTORY.TRANSACTIONTEXT, " +
    "CONTACT.CONTACTLASTNAME || ',' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) SYNTHETICUSER, " +
    "SUBSTR(TRANSACTIONTYPE.TTDESCRIPTION,1,15) SYNTHETICTRANSACTYPE " +
    "FROM CONTACT, DEALHISTORY, TRANSACTIONTYPE, " +
    "USERPROFILE, STATUS  __WHERE__  ORDER BY DEALHISTORY.TRANSACTIONDATE  DESC";
*/

	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DEALHISTORY.DEALID, DEALHISTORY.APPLICATIONID, " +
    "to_char(DEALHISTORY.STATUSID) STATUSID_STR, DEALHISTORY.TRANSACTIONDATE, " +
    "DEALHISTORY.TRANSACTIONTEXT, " +
    "CONTACT.CONTACTLASTNAME || ',' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) SYNTHETICUSER, " +
    
    "to_char(DEALHISTORY.TRANSACTIONTYPEID) TRANSACTIONTYPEID_STR " +
    "FROM CONTACT, DEALHISTORY, USERPROFILE " +
    "__WHERE__  ORDER BY DEALHISTORY.TRANSACTIONDATE  DESC";

  public static final String MODIFYING_QUERY_TABLE_NAME=
    "CONTACT, DEALHISTORY, USERPROFILE";

	public static final String STATIC_WHERE_CRITERIA=
    " (((DEALHISTORY.USERPROFILEID  =  USERPROFILE.USERPROFILEID) AND " +
    
    " (DEALHISTORY.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) AND " +
    " (DEALHISTORY.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID) AND " +
    " (CONTACT.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID) AND " +
        
    "(USERPROFILE.CONTACTID  =  CONTACT.CONTACTID)) AND CONTACT.COPYID = 1) ";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEALHISTORY.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
    
//    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID="DEALHISTORY.INSTITUTIONPROFILEID";
//    public static final String COLUMN_DFINSTITUTIONID="INSTITUTIONPROFILEID";
    
	public static final String QUALIFIED_COLUMN_DFAPPLICATIONID="DEALHISTORY.APPLICATIONID";
	public static final String COLUMN_DFAPPLICATIONID="APPLICATIONID";

  //--Release2.1--//
	//public static final String QUALIFIED_COLUMN_DFSTATUS="STATUS.STATUSDESCRIPTION";
	//public static final String COLUMN_DFSTATUS="STATUSDESCRIPTION";
	//public static final String QUALIFIED_COLUMN_DFTRANSACTIONTYPE="TRANSACTIONTYPE.SYNTHETICTRANSACTYPE";
	//public static final String COLUMN_DFTRANSACTIONTYPE="SYNTHETICTRANSACTYPE";
  public static final String QUALIFIED_COLUMN_DFSTATUS="STATUS.STATUSID_STR";
	public static final String COLUMN_DFSTATUS="STATUSID_STR";
	public static final String QUALIFIED_COLUMN_DFTRANSACTIONTYPE="TRANSACTIONTYPE.TRANSACTIONTYPEID_STR";
	public static final String COLUMN_DFTRANSACTIONTYPE="TRANSACTIONTYPEID_STR";
  //--------------//

	public static final String QUALIFIED_COLUMN_DFTRANSACTIONDATE="DEALHISTORY.TRANSACTIONDATE";
	public static final String COLUMN_DFTRANSACTIONDATE="TRANSACTIONDATE";
	public static final String QUALIFIED_COLUMN_DFTRANSACTIONTEXT="DEALHISTORY.TRANSACTIONTEXT";
	public static final String COLUMN_DFTRANSACTIONTEXT="TRANSACTIONTEXT";
	////public static final String QUALIFIED_COLUMN_DFUSER=".";
	////public static final String COLUMN_DFUSER="";
	////public static final String QUALIFIED_COLUMN_DFTRANSACTIONTYPE=".";
	////public static final String COLUMN_DFTRANSACTIONTYPE="";
	public static final String QUALIFIED_COLUMN_DFUSER="CONTACT.SYNTHETICUSER";
	public static final String COLUMN_DFUSER="SYNTHETICUSER";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        
//        FIELD_SCHEMA.addFieldDescriptor(
//            new QueryFieldDescriptor(
//                FIELD_DFINSTITUTIONID,
//                COLUMN_DFINSTITUTIONID,
//                QUALIFIED_COLUMN_DFINSTITUTIONID,
//                java.math.BigDecimal.class,
//                false,
//                false,
//                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
//                "",
//                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
//                ""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICATIONID,
				COLUMN_DFAPPLICATIONID,
				QUALIFIED_COLUMN_DFAPPLICATIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUS,
				COLUMN_DFSTATUS,
				QUALIFIED_COLUMN_DFSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTRANSACTIONDATE,
				COLUMN_DFTRANSACTIONDATE,
				QUALIFIED_COLUMN_DFTRANSACTIONDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTRANSACTIONTEXT,
				COLUMN_DFTRANSACTIONTEXT,
				QUALIFIED_COLUMN_DFTRANSACTIONTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFUSER,
		////		COLUMN_DFUSER,
		////		QUALIFIED_COLUMN_DFUSER,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFTRANSACTIONTYPE,
		////		COLUMN_DFTRANSACTIONTYPE,
		////		QUALIFIED_COLUMN_DFTRANSACTIONTYPE,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
    ////	""));

		//// Adjusted FieldDescriptors for the ComputedColumns to fix the iMT tool bug.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSER,
				COLUMN_DFUSER,
				QUALIFIED_COLUMN_DFUSER,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"CONTACT.CONTACTLASTNAME || ',' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"CONTACT.CONTACTLASTNAME || ',' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1)"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTRANSACTIONTYPE,
				COLUMN_DFTRANSACTIONTYPE,
				QUALIFIED_COLUMN_DFTRANSACTIONTYPE,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"SUBSTR(TRANSACTIONTYPE.TTDESCRIPTION,1,15)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"SUBSTR(TRANSACTIONTYPE.TTDESCRIPTION,1,15)"));


	}

}

