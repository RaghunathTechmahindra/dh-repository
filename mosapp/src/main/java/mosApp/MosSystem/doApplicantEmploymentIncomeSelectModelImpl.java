package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doApplicantEmploymentIncomeSelectModelImpl extends QueryModelBase
	implements doApplicantEmploymentIncomeSelectModel
{
	/**
	 *
	 *
	 */
	public doApplicantEmploymentIncomeSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmploymentHistoryId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPLOYMENTHISTORYID);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentHistoryId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPLOYMENTHISTORYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfContactId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONTACTID);
	}


	/**
	 *
	 *
	 */
	public void setDfContactId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONTACTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfContactCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONTACTCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfContactCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONTACTCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAddressId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADDRESSID);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADDRESSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAddrCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADDRCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfAddrCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADDRCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMECOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMECOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerName()
	{
		return (String)getValue(FIELD_DFEMPLOYERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerName(String value)
	{
		setValue(FIELD_DFEMPLOYERNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmploymentStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPLOYMENTSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPLOYMENTSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmploymentTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPLOYMENTTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPLOYMENTTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerPhoneNo()
	{
		return (String)getValue(FIELD_DFEMPLOYERPHONENO);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerPhoneNo(String value)
	{
		setValue(FIELD_DFEMPLOYERPHONENO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfContactPhoneNoExtension()
	{
		return (String)getValue(FIELD_DFCONTACTPHONENOEXTENSION);
	}


	/**
	 *
	 *
	 */
	public void setDfContactPhoneNoExtension(String value)
	{
		setValue(FIELD_DFCONTACTPHONENOEXTENSION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerFaxNo()
	{
		return (String)getValue(FIELD_DFEMPLOYERFAXNO);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerFaxNo(String value)
	{
		setValue(FIELD_DFEMPLOYERFAXNO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerEmailAddress()
	{
		return (String)getValue(FIELD_DFEMPLOYEREMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerEmailAddress(String value)
	{
		setValue(FIELD_DFEMPLOYEREMAILADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerAddressLine1()
	{
		return (String)getValue(FIELD_DFEMPLOYERADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerAddressLine1(String value)
	{
		setValue(FIELD_DFEMPLOYERADDRESSLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerCity()
	{
		return (String)getValue(FIELD_DFEMPLOYERCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerCity(String value)
	{
		setValue(FIELD_DFEMPLOYERCITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProvinceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROVINCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfProvinceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROVINCEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerPostalFSA()
	{
		return (String)getValue(FIELD_DFEMPLOYERPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerPostalFSA(String value)
	{
		setValue(FIELD_DFEMPLOYERPOSTALFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerPostalLDU()
	{
		return (String)getValue(FIELD_DFEMPLOYERPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerPostalLDU(String value)
	{
		setValue(FIELD_DFEMPLOYERPOSTALLDU,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerAddressLine2()
	{
		return (String)getValue(FIELD_DFEMPLOYERADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerAddressLine2(String value)
	{
		setValue(FIELD_DFEMPLOYERADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIndustrySectorId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINDUSTRYSECTORID);
	}


	/**
	 *
	 *
	 */
	public void setDfIndustrySectorId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINDUSTRYSECTORID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfOccupationId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFOCCUPATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfOccupationId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFOCCUPATIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfJobTittle()
	{
		return (String)getValue(FIELD_DFJOBTITTLE);
	}


	/**
	 *
	 *
	 */
	public void setDfJobTittle(String value)
	{
		setValue(FIELD_DFJOBTITTLE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTimeAtJob()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTIMEATJOB);
	}


	/**
	 *
	 *
	 */
	public void setDfTimeAtJob(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTIMEATJOB,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncomeDesc()
	{
		return (String)getValue(FIELD_DFINCOMEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeDesc(String value)
	{
		setValue(FIELD_DFINCOMEDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomePeriodId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEPERIODID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomePeriodId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEPERIODID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludeInGDS()
	{
		return (String)getValue(FIELD_DFINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludeInGDS(String value)
	{
		setValue(FIELD_DFINCLUDEINGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINGDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludeInTDS()
	{
		return (String)getValue(FIELD_DFINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludeInTDS(String value)
	{
		setValue(FIELD_DFINCLUDEINTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfJobTitleID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFJOBTITLEID);
	}


	/**
	 *
	 *
	 */
	public void setDfJobTitleID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFJOBTITLEID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT EMPLOYMENTHISTORY.BORROWERID, " +
            "EMPLOYMENTHISTORY.EMPLOYMENTHISTORYID, EMPLOYMENTHISTORY.COPYID, CONTACT.CONTACTID, " +
            "CONTACT.COPYID, ADDR.ADDRID, ADDR.COPYID, INCOME.INCOMEID, INCOME.COPYID, " +
            "EMPLOYMENTHISTORY.EMPLOYERNAME, EMPLOYMENTHISTORY.EMPLOYMENTHISTORYSTATUSID, " +
            "EMPLOYMENTHISTORY.EMPLOYMENTHISTORYTYPEID, CONTACT.CONTACTPHONENUMBER, " +
            "CONTACT.CONTACTPHONENUMBEREXTENSION, CONTACT.CONTACTFAXNUMBER, " +
            "CONTACT.CONTACTEMAILADDRESS, ADDR.ADDRESSLINE1, ADDR.CITY, " +
            "ADDR.PROVINCEID, ADDR.POSTALFSA, ADDR.POSTALLDU, ADDR.ADDRESSLINE2, " +
            "EMPLOYMENTHISTORY.INDUSTRYSECTORID, EMPLOYMENTHISTORY.OCCUPATIONID, " +
            "EMPLOYMENTHISTORY.JOBTITLE, EMPLOYMENTHISTORY.MONTHSOFSERVICE, " +
            "INCOME.INCOMETYPEID, INCOME.INCOMEDESCRIPTION, INCOME.INCOMEPERIODID, " +
            "INCOME.INCOMEAMOUNT, INCOME.INCINCLUDEINGDS, INCOME.INCPERCENTINGDS, " +
            "INCOME.INCINCLUDEINTDS, INCOME.INCPERCENTINTDS, EMPLOYMENTHISTORY.JOBTITLEID " +
            "FROM CONTACT, ADDR, EMPLOYMENTHISTORY, INCOME  __WHERE__  " +
            "ORDER BY EMPLOYMENTHISTORY.EMPLOYMENTHISTORYID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, ADDR, EMPLOYMENTHISTORY, INCOME";
	public static final String STATIC_WHERE_CRITERIA=
	        "CONTACT.INSTITUTIONPROFILEID = ADDR.INSTITUTIONPROFILEID " +
	        "AND (CONTACT.ADDRID = ADDR.ADDRID) " +
	        "AND CONTACT.INSTITUTIONPROFILEID = EMPLOYMENTHISTORY.INSTITUTIONPROFILEID " +
	        "AND CONTACT.CONTACTID = EMPLOYMENTHISTORY.CONTACTID " +
	        "AND INCOME.INSTITUTIONPROFILEID = EMPLOYMENTHISTORY.INSTITUTIONPROFILEID " +
            "AND INCOME.INCOMEID = EMPLOYMENTHISTORY.INCOMEID ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="EMPLOYMENTHISTORY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYID="EMPLOYMENTHISTORY.EMPLOYMENTHISTORYID";
	public static final String COLUMN_DFEMPLOYMENTHISTORYID="EMPLOYMENTHISTORYID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="EMPLOYMENTHISTORY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFCONTACTID="CONTACT.CONTACTID";
	public static final String COLUMN_DFCONTACTID="CONTACTID";
	public static final String QUALIFIED_COLUMN_DFCONTACTCOPYID="CONTACT.COPYID";
	public static final String COLUMN_DFCONTACTCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFADDRESSID="ADDR.ADDRID";
	public static final String COLUMN_DFADDRESSID="ADDRID";
	public static final String QUALIFIED_COLUMN_DFADDRCOPYID="ADDR.COPYID";
	public static final String COLUMN_DFADDRCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFINCOMEID="INCOME.INCOMEID";
	public static final String COLUMN_DFINCOMEID="INCOMEID";
	public static final String QUALIFIED_COLUMN_DFINCOMECOPYID="INCOME.COPYID";
	public static final String COLUMN_DFINCOMECOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERNAME="EMPLOYMENTHISTORY.EMPLOYERNAME";
	public static final String COLUMN_DFEMPLOYERNAME="EMPLOYERNAME";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTSTATUSID="EMPLOYMENTHISTORY.EMPLOYMENTHISTORYSTATUSID";
	public static final String COLUMN_DFEMPLOYMENTSTATUSID="EMPLOYMENTHISTORYSTATUSID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTTYPEID="EMPLOYMENTHISTORY.EMPLOYMENTHISTORYTYPEID";
	public static final String COLUMN_DFEMPLOYMENTTYPEID="EMPLOYMENTHISTORYTYPEID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERPHONENO="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFEMPLOYERPHONENO="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFCONTACTPHONENOEXTENSION="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFCONTACTPHONENOEXTENSION="CONTACTPHONENUMBEREXTENSION";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERFAXNO="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFEMPLOYERFAXNO="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFEMPLOYEREMAILADDRESS="CONTACT.CONTACTEMAILADDRESS";
	public static final String COLUMN_DFEMPLOYEREMAILADDRESS="CONTACTEMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERADDRESSLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFEMPLOYERADDRESSLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERCITY="ADDR.CITY";
	public static final String COLUMN_DFEMPLOYERCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFPROVINCEID="ADDR.PROVINCEID";
	public static final String COLUMN_DFPROVINCEID="PROVINCEID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERPOSTALFSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFEMPLOYERPOSTALFSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERPOSTALLDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFEMPLOYERPOSTALLDU="POSTALLDU";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERADDRESSLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFEMPLOYERADDRESSLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFINDUSTRYSECTORID="EMPLOYMENTHISTORY.INDUSTRYSECTORID";
	public static final String COLUMN_DFINDUSTRYSECTORID="INDUSTRYSECTORID";
	public static final String QUALIFIED_COLUMN_DFOCCUPATIONID="EMPLOYMENTHISTORY.OCCUPATIONID";
	public static final String COLUMN_DFOCCUPATIONID="OCCUPATIONID";
	public static final String QUALIFIED_COLUMN_DFJOBTITTLE="EMPLOYMENTHISTORY.JOBTITLE";
	public static final String COLUMN_DFJOBTITTLE="JOBTITLE";
	public static final String QUALIFIED_COLUMN_DFTIMEATJOB="EMPLOYMENTHISTORY.MONTHSOFSERVICE";
	public static final String COLUMN_DFTIMEATJOB="MONTHSOFSERVICE";
	public static final String QUALIFIED_COLUMN_DFINCOMETYPEID="INCOME.INCOMETYPEID";
	public static final String COLUMN_DFINCOMETYPEID="INCOMETYPEID";
	public static final String QUALIFIED_COLUMN_DFINCOMEDESC="INCOME.INCOMEDESCRIPTION";
	public static final String COLUMN_DFINCOMEDESC="INCOMEDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFINCOMEPERIODID="INCOME.INCOMEPERIODID";
	public static final String COLUMN_DFINCOMEPERIODID="INCOMEPERIODID";
	public static final String QUALIFIED_COLUMN_DFINCOMEAMOUNT="INCOME.INCOMEAMOUNT";
	public static final String COLUMN_DFINCOMEAMOUNT="INCOMEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFINCLUDEINGDS="INCOME.INCINCLUDEINGDS";
	public static final String COLUMN_DFINCLUDEINGDS="INCINCLUDEINGDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS="INCOME.INCPERCENTINGDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINGDS="INCPERCENTINGDS";
	public static final String QUALIFIED_COLUMN_DFINCLUDEINTDS="INCOME.INCINCLUDEINTDS";
	public static final String COLUMN_DFINCLUDEINTDS="INCINCLUDEINTDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS="INCOME.INCPERCENTINTDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINTDS="INCPERCENTINTDS";
	public static final String QUALIFIED_COLUMN_DFJOBTITLEID="EMPLOYMENTHISTORY.JOBTITLEID";
	public static final String COLUMN_DFJOBTITLEID="JOBTITLEID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTHISTORYID,
				COLUMN_DFEMPLOYMENTHISTORYID,
				QUALIFIED_COLUMN_DFEMPLOYMENTHISTORYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTID,
				COLUMN_DFCONTACTID,
				QUALIFIED_COLUMN_DFCONTACTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTCOPYID,
				COLUMN_DFCONTACTCOPYID,
				QUALIFIED_COLUMN_DFCONTACTCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSID,
				COLUMN_DFADDRESSID,
				QUALIFIED_COLUMN_DFADDRESSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRCOPYID,
				COLUMN_DFADDRCOPYID,
				QUALIFIED_COLUMN_DFADDRCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEID,
				COLUMN_DFINCOMEID,
				QUALIFIED_COLUMN_DFINCOMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMECOPYID,
				COLUMN_DFINCOMECOPYID,
				QUALIFIED_COLUMN_DFINCOMECOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERNAME,
				COLUMN_DFEMPLOYERNAME,
				QUALIFIED_COLUMN_DFEMPLOYERNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTSTATUSID,
				COLUMN_DFEMPLOYMENTSTATUSID,
				QUALIFIED_COLUMN_DFEMPLOYMENTSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTTYPEID,
				COLUMN_DFEMPLOYMENTTYPEID,
				QUALIFIED_COLUMN_DFEMPLOYMENTTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERPHONENO,
				COLUMN_DFEMPLOYERPHONENO,
				QUALIFIED_COLUMN_DFEMPLOYERPHONENO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTPHONENOEXTENSION,
				COLUMN_DFCONTACTPHONENOEXTENSION,
				QUALIFIED_COLUMN_DFCONTACTPHONENOEXTENSION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERFAXNO,
				COLUMN_DFEMPLOYERFAXNO,
				QUALIFIED_COLUMN_DFEMPLOYERFAXNO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYEREMAILADDRESS,
				COLUMN_DFEMPLOYEREMAILADDRESS,
				QUALIFIED_COLUMN_DFEMPLOYEREMAILADDRESS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERADDRESSLINE1,
				COLUMN_DFEMPLOYERADDRESSLINE1,
				QUALIFIED_COLUMN_DFEMPLOYERADDRESSLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERCITY,
				COLUMN_DFEMPLOYERCITY,
				QUALIFIED_COLUMN_DFEMPLOYERCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCEID,
				COLUMN_DFPROVINCEID,
				QUALIFIED_COLUMN_DFPROVINCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERPOSTALFSA,
				COLUMN_DFEMPLOYERPOSTALFSA,
				QUALIFIED_COLUMN_DFEMPLOYERPOSTALFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERPOSTALLDU,
				COLUMN_DFEMPLOYERPOSTALLDU,
				QUALIFIED_COLUMN_DFEMPLOYERPOSTALLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERADDRESSLINE2,
				COLUMN_DFEMPLOYERADDRESSLINE2,
				QUALIFIED_COLUMN_DFEMPLOYERADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINDUSTRYSECTORID,
				COLUMN_DFINDUSTRYSECTORID,
				QUALIFIED_COLUMN_DFINDUSTRYSECTORID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFOCCUPATIONID,
				COLUMN_DFOCCUPATIONID,
				QUALIFIED_COLUMN_DFOCCUPATIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOBTITTLE,
				COLUMN_DFJOBTITTLE,
				QUALIFIED_COLUMN_DFJOBTITTLE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTIMEATJOB,
				COLUMN_DFTIMEATJOB,
				QUALIFIED_COLUMN_DFTIMEATJOB,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMETYPEID,
				COLUMN_DFINCOMETYPEID,
				QUALIFIED_COLUMN_DFINCOMETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEDESC,
				COLUMN_DFINCOMEDESC,
				QUALIFIED_COLUMN_DFINCOMEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEPERIODID,
				COLUMN_DFINCOMEPERIODID,
				QUALIFIED_COLUMN_DFINCOMEPERIODID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEAMOUNT,
				COLUMN_DFINCOMEAMOUNT,
				QUALIFIED_COLUMN_DFINCOMEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDEINGDS,
				COLUMN_DFINCLUDEINGDS,
				QUALIFIED_COLUMN_DFINCLUDEINGDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINGDS,
				COLUMN_DFPERCENTINCLUDEDINGDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDEINTDS,
				COLUMN_DFINCLUDEINTDS,
				QUALIFIED_COLUMN_DFINCLUDEINTDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINTDS,
				COLUMN_DFPERCENTINCLUDEDINTDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOBTITLEID,
				COLUMN_DFJOBTITLEID,
				QUALIFIED_COLUMN_DFJOBTITLEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

