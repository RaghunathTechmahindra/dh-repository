package mosApp.MosSystem;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.commons.lang.NumberUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMessageStateHelper;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.mtgrates.PricingLogic2;
import com.basis100.mtgrates.RateInfo;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.Page;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.express.web.util.service.OSCClosingServiceHelper;
import com.filogix.util.Xc;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.TiledViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.HtmlDisplayField;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;
import com.filogix.express.web.util.service.QualifyingRateHelper;

/**
 * <p>Title: UWorksheetHandler</p>
 *
 * <p>Description: Handler class for UW sheet.</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * @author
 * @version 58
 * Date: 11/20/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	forceDisplayCbProgressAdvanceType(), isForceProgessAdvance () - new methods added
 * call to forceDisplayCbProgressAdvanceType() made from populatePageDisplayFields()
 *
 * @version 59
 * Date: July 14/2008 <br>
 * Author: MCM Implementation Team <br>
 * Change: modified validate method to add validate components against DEC and DCC
 * Change:  <br>
 *  validate: added validate components against DEC and DCC
 * @version 60
 * Date: July 14/2008 <br>
 * Author: MCM Implementation Team <br>
 * Change:  <br>
 *  Bug Fix: Added null check for the tmpRateDate in handleRateTimeStampPassToHtml() method.
 *
 * @version 61
 * Date: July 29/2008 <br>
 * Author: MCM Implementation Team <br>
 * Change:  <br>
 * added saveCompInfoCheckBox in SaveEntity() method
 * July 31, 2008: added JS to control check box enable/disable by miUpfront and taxPayor
 *
 * @version 62 | XS_2.48
 * Date: Aug 06/2008 <br>
 * Author: MCM Implementation Team <br>
 * Change:  <br>
 * Removed the call of DCC- businessrules.
 *
 * @version 63 MCM impl team, artf763316 Aug 21, 2008 : replacing XS_2.60.
 * @version 64 Oct 7, 2008: FXP22741, changed business rule validation for compoents.
 */

// Special usage of Underwriter Worksheet PageEntry members:
    //
    // 1) pageCondition5
    //
    //    Used to indicate if update if MI values is allowed. Values true/false
    //
    //    For clarity the method isMIUpdateAllowed() returns the value of the condition.
    //
    // 2) pageCondition4
    //
    //    Value - true:  Default conditions have been build for view scenario (and no data changes have occured
    //                   since conditions were built)
    //
    //          - false: Default conditions have not been built (and user must visit Conditions Review page to
    //                   review the conditions).
    //
    // 3) pageCondition3
    //
    //    Set during navigation (by framework):
    //
    //    Values - true: deal is in IN_UNDERWRITING status
    //
    //           -false: deal in some other status
    // 4) pageCondition2
    //
    //    Set to indicate page generation condition.
    //
    //    Values - false: external page generation. Page is being displayed for the first time
    //                    after navigation from external page. This flag is used to determine if
    //                    page startup processing (e.g. business rule execution) should be
    //                    performed before the page is displayed.
    //
    //             true:  inner page generation. Page is being displayed for inner-worksheet
    //                    purposes.
    //
    //             ** - In some cases the worksheet logic may set the value to false to force
    //                  the display to behave like first time external navigation display.
    //
    //  5) pageCondition6. Currently Product Team has decided to go with the 'Other Details'
    //     screen always open. This condition is reserved now if the Product Team decides
    //     to customize the application for future client.
    //
    //    Set to indicate is sub-page 'Other Details' open or closed. Values true/false. Along
    //    with the setting of this condition a value of stOtherDetailsFlag ('Y'/'N') is passing
    //    to the HTML page. This value is used by JS showOtherDetails() function that is responsible
    //    for open/close 'Other Details' sub-page based on the value of the stOtherDetailsFlag.
    //
    //
    //  6) pageCondition7
    //
    //    Set to indicate is Rate locked or not. Values true/false. This condition is used
    //    both on the Deal Modidfication and UWorksheet pages.
    //
    //  7) pageCondition8
    //
    //    Used to indicate the origiantion of the deal. Values true:electronic/false:manual.
    //    This condition is used both on the Deal Modification and UWorksheet pages.
    //
  //-- BMO_MI_CR--//
  //
  //   8) pageCondition9
  //
  //    Used to indicate that the MI process is handled outside BXP. In this case client (BMO)
  //    could update all MI related fields directly from the UW or Mortgate Insurance screen.
  //    The . The whole MI module business logic is bypassed in the case and the client has
  //    a direct access to the database and it is their responsibility to provide the data integrity except:
  //
  //    - P&I and total loan amount calcs should update respective values on submit;
  //    - New business rule DE-890 should fire hardstop if MIPremium is < 0 or > (principal * .051);
  //    - 'Pre Qualification MI Certificat #' field is open for editing if and only if the
  //       'MI Indicator' field is set up to 'Mortgage Insurance Pre Qualification'.
  //    - For the Certification fields family if supplier is CMHC the length is =< 10 (softstop)
  //    - If the supplier is GE these lengths should not be less than 10 (softstop).
  //    - If MI Insurance Indicator = 'MI Pre Qualification' and MI status = 'Approved' the
  //      Pre Qualification MI Certificate # must be filled in.
  //    - If MI Indicator = 'Required Std. Guidelines' or 'UW Required' and MI status = 'Approved'
  //      the MI Certificate # must be field in.
  //    - If the MI status is not 'Approved the MI Premium value must be calculated follow standard logic.

public class UWorksheetHandler extends DealHandlerCommon
    implements Cloneable, Xc
{
    private static Log _log = LogFactory.getLog(UWorksheetHandler.class);

	protected static final String BASED_ON_SCENARIO = "Based on Scenario";
	protected static final String BASED_ON_DEAL = "Based on Deal";
	protected static final String BASED = "Based";
	protected static final String WHATEVER = "WHATEVER";
	protected static final String RESOLVE_FROM_UWORKSHEET = "RESOLVE_FROM_UWORKSHEET";
	protected static final String AU_SUBMIT_NO = "AU_SUBMIT_NO";
	protected static final String AU_SUBMIT_YES = "AU_SUBMIT_YES";
	protected static final String CONFIRMDELETE = "CONFIRMDELETE";
	protected static final String SWITCH_DISCARD_CURRENT = "SWITCH_DISCARD_CURRENT";
	protected static final String SWITCH_SAVE_CURRENT = "SWITCH_SAVE_CURRENT";
	protected static final String NEW_DISCARD_CURRENT_FROM_BOTTOM = "NEW_DISCARD_CURRENT_FROM_BOTTOM";
	protected static final String NEW_DISCARD_CURRENT_FROM_TOP = "NEW_DISCARD_CURRENT_FROM_TOP";
	protected static final String NEW_SAVE_CURRENT_FROM_BOTTOM = "NEW_SAVE_CURRENT_FROM_BOTTOM";
	protected static final String NEW_SAVE_CURRENT_FROM_TOP = "NEW_SAVE_CURRENT_FROM_TOP";
	protected static final String OVERRIDE_DEAL_LOCK_NO = "OVERRIDE_DEAL_LOCK_NO";
	protected static final String OVERRIDE_DEAL_LOCK_YES = "OVERRIDE_DEAL_LOCK_YES";
	protected static final String VIEWGOLD = "VIEWGOLD";
	protected static final String NOVIEWGOLD = "NOVIEWGOLD";
	// Product doesn't allow now to re-assign the SOB, but the possiblity to edit
	// the pre-assigned SOB should exist.
	public static final String SOB_ID_PASSED_PARM="sourceOBIdPassed";
	//String homeBaseDealNote = "";		//#DG702

    public UWorksheetHandler()
    {
    super();
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
    }

    public UWorksheetHandler(String name)
    {
        this(null,name); // No parent
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
    }

    protected UWorksheetHandler(View parent, String name)
    {
        super(parent,name);
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
    }


  /**
     *
     *
     */
    public UWorksheetHandler cloneSS()
    {
        return(UWorksheetHandler) super.cloneSafeShallow();
    }


    /**
     *
     *
     */
    public boolean isMIUpdateAllowed(PageEntry pg)
    {
        return pg.getPageCondition5();
    }

  //--BMO_MI_CR--start//
  /**
   *
   *
   */
  //// MI process logic is eliminated for BMO. They can update database either from
  //// UW or MortgageIns screens directly.
  public boolean isMIProcessOutsideBXP(PageEntry pg)
  {
          return pg.getPageCondition9();
  }

  protected boolean getCretarioIsMIStatusOutsideBXP()
  {
    boolean isOutsideBXP = false; // default.
    // regular execution path
    /* #DG702
    if (PropertiesCache.getInstance().getProperty
                    (theSessionState.getDealInstitutionId(),
                     "com.basis100.miinsurance.miprocessinghandledoutsidexpress", "N").equals("N"))
    {
       isOutsideBXP = false;
    }
    // BMO execution path
    else if (PropertiesCache.getInstance().getProperty
                    (theSessionState.getDealInstitutionId(),
                     "com.basis100.miinsurance.miprocessinghandledoutsidexpress", "N").equals("Y"))
    {
       isOutsideBXP = true;
    }*/
    isOutsideBXP = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(), COM_BASIS100_MIINSURANCE_MIPROCESSINGHANDLEDOUTSIDEXPRESS, "N").equals("Y");
    logger.debug("UWH@setCretarioIsMIStatusOutsideBXP::isOutsideXpress? " + isOutsideBXP);

    return isOutsideBXP;
  }
  //--BMO_MI_CR--end//

    /**
     *
     *
     */
    public boolean isRateLocked(PageEntry pg)
    {
        return pg.getPageCondition7();
    }


    /**
     *
     *
     */
    public boolean isDealElectronic(PageEntry pg)
    {
        return pg.getPageCondition8();
    }


    // In event of data change after MI application initiated (or completed!) post a message to the
    // user warning that changes may invalidate the policy.
    //
    // If the deal passed is not the recommended scenario the data changed condition is considered true by default.
    // The rational here is that the MI policy (pending or granted) is associated with the current recommended scenario
    // and hence entering MI with some other scenario constitutes a potential change.

    private void setMIIntegrityMsg(SessionResourceKit srk, PageEntry pg, Deal deal)
    {
        if (isMIUpdateAllowed(pg) == true) return;


        // can't be a problem - since no MI (including no MI pending)
        if (pg.isPageDataModified() == false && "Y".equals(deal.getScenarioRecommended())) return;


        // no change and is recommended scenario - post message
        ActiveMessage am = theSessionState.getActMessage();

        if (am == null)
        {
            setActiveMessageToAlert("", ActiveMsgFactory.ISCUSTOMCONFIRM);
            am = theSessionState.getActMessage();

            if (am == null) return;

            // should never happen ... but
        }

        am.addMsg(BXResources.getSysMsg(MI_DATA_CHANGED_WARNING, theSessionState.getLanguageId()),
        ActiveMessage.NONCRITICAL);
    }
    /**
	 * forceDisplayCbProgressAdvanceType This method sets the hidden field value to 'Y'
	 * based on the property set in the mossys.properties
	 * (if property com.basis100.system.forceprogressadvancetype is set to Y this method sets the hidden feld value to 'Y')
	 *
	 *  @param ViewBean
	 * @version 58
	 *          Date: 11/20/2006
	 *          Author: NBC/PP Implementation Team
	 *
	 */
	protected void forceDisplayCbProgressAdvanceType(ViewBean thePage) {

		boolean displayProgressAdvanceTypeDropDown = isForceProgessAdvance(srk);

		if (displayProgressAdvanceTypeDropDown) {
			thePage.getDisplayField(pgUWorksheetViewBean.CHILD_HDFORCEPROGRESSADVANCE).setValue("Y");
		}
	}

	/**
	 * isForceProgessAdvance This method reads the property from the
	 * mossys.properties and returns true if the property value is Y else
	 * returns false
	 *
	 * @param SessionResourceKit
	 * @return boolean - returns true when the  property com.basis100.system.forceprogressadvancetype is set to Y
	 * @version 58
	 *          Date: 11/20/2006
	 *          Author: NBC/PP Implementation Team
	 *
	 */
	private boolean isForceProgessAdvance(SessionResourceKit srk) {
		boolean returnVal = false;

		try {
			if (PropertiesCache.getInstance().getProperty(
					theSessionState.getDealInstitutionId(),
					COM_BASIS100_SYSTEM_FORCEPROGRESSADVANCETYPE, "N")
					.equalsIgnoreCase("Y")) {
				returnVal = true;
			}
		} catch (Exception e) {
			srk.getSysLogger().error(
					"@isForceProgessAdvance cannot read mossys properties:" + e);		//#DG702
		}

		return returnVal;
	}



/**
 * <p>populatePageDisplayFields</p>
 * This method is used to populate the fields in the header
 * with the appropriate information
 *
 * @version 61: MCM team:  added JS to control check box enable/disable by miUpfront and taxPayor
 */
  public void populatePageDisplayFields()
  {
    //logger.debug("PERFORMANCE CHECK ==> Entered populatePageDisplayFields !!");
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    Hashtable pst = pg.getPageStateTable();

    HashSet skipUWElements = new HashSet();

    populatePageShellDisplayFields();

    // Quick Link Menu display
    displayQuickLinkMenu();

    populateTaskNavigator(pg);

    populatePageDealSummarySnapShot();

    populatePreviousPagesLinks();

    ViewBean thePage = getCurrNDPage();
    thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STUWTARGETDOWNPAYMENT, new String(TARGET_TAG));

    thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STTARGETESCROW, new String(TARGET_TAG));
//  ***** Change by NBC Impl. Team - Version 58 - Start *****//

    forceDisplayCbProgressAdvanceType(thePage);

//  ***** Change by NBC Impl. Team - Version 58 - End*****//

//  Ticket #4113
    DisplayField df = ((pgUWorksheetViewBean)thePage).getDisplayField(pgUWorksheetViewBean.CHILD_STLOCINTERESTONLYMATURITYDATE);
    if(df != null & df.getValue() != null)
    {
        String val = df.getValue().toString();
        if(val.indexOf("1969-12-31") > -1)
        {
            thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STLOCINTERESTONLYMATURITYDATE, "");
        }
    }

//  Ticket #4115
    DisplayField _locAmort = ((pgUWorksheetViewBean)thePage).getDisplayField(pgUWorksheetViewBean.CHILD_STLOCAMORTIZATIONMONTHS);
    if(_locAmort != null & _locAmort.getValue() != null)
    {
        String val = _locAmort.getValue().toString();
        if(val.equals("0"))
        {
            thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STLOCAMORTIZATIONMONTHS, "");
        }
    }

    //// 0. Data validation API.
    try {

      String uWMainValidPath = Sc.DVALID_UW_MAIN_PROP_FILE_NAME;

      //logger.debug("UWH@UWMainJSDataValid: " + uWMainValidPath);

      JSValidationRules uWMainValidObj = new JSValidationRules(uWMainValidPath,
        logger, thePage);
      uWMainValidObj.attachJSValidationRules();
    }
    catch (Exception e) {
      logger.error("Exception UWH@PopulatePageDisplayFields: Problem encountered in JS data validation API." +
                   e.getMessage());
    }

    populateJSVALSData(thePage);

    //1. Customization of the rates block in the Finacial Details section.
    if (isRateLocked(pg) == true) {
      customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWDISCOUNT);
      customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWPREMIUM);
      customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWBUYDOWN);
      skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_TXUWDISCOUNT));
      skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_TXUWPREMIUM));
      skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_TXUWBUYDOWN));
    }
    else if (isRateLocked(pg) == false) {
      customDefaultTextBox(pgUWorksheetViewBean.CHILD_TXUWDISCOUNT);
      customDefaultTextBox(pgUWorksheetViewBean.CHILD_TXUWPREMIUM);
      customDefaultTextBox(pgUWorksheetViewBean.CHILD_TXUWBUYDOWN);

      try {

        String rateValidPath = Sc.DVALID_UW_RATES_PROP_FILE_NAME;

        //logger.debug("UWH@RatesValidationPath: " + rateValidPath);

        JSValidationRules rateValidObj = new JSValidationRules(rateValidPath,
          logger, thePage);
        rateValidObj.attachJSValidationRules();
      }
      catch (Exception e) {
        logger.error("Exception UWH@PopulatePageDisplayFields: Problem encountered in JS data validation API." +
                     e.getMessage());
      }
    }

    //2. Customization of the Reference Source Application field. In case of
    // ingestion (electronic deal) this field is not editable.
    if (isDealElectronic(pg) == true) {
      customizeTextBox(pgUWorksheetViewBean.CHILD_TBUWREFERENCESOURCEAPPLNO);
      //--Ticket#1006--start--//
      // Add electronic media channel disabled field for skipping to db
      // propagation.
      skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_TBUWREFERENCESOURCEAPPLNO));
      //--Ticket#1006--end--//
    }
    else if (isDealElectronic(pg) == false) {
      customDefaultTextBox(pgUWorksheetViewBean.CHILD_TBUWREFERENCESOURCEAPPLNO);
    }

    //3. Customization for the dynamic Lender-->Product-->Rate module
    if (isRateLocked(pg) == true) {
      restrictedDynamicLPRCombos(pgUWorksheetViewBean.CHILD_CBUWLENDER, pgUWorksheetViewBean.CHILD_CBUWPRODUCT,
      		pgUWorksheetViewBean.CHILD_CBUWPOSTEDINTERESTRATE);
      skipUWElements.add(pgUWorksheetViewBean.CHILD_TBUWRATECODE);
    }
    else if (isRateLocked(pg) == false) {
      String formName = "document.forms[0].pgUWorksheet_";

      customizeDynamicLPRCombos(pgUWorksheetViewBean.CHILD_CBUWLENDER, pgUWorksheetViewBean.CHILD_CBUWPRODUCT,
      		pgUWorksheetViewBean.CHILD_CBUWPOSTEDINTERESTRATE, formName);
    }

    customizeTextBox(pgUWorksheetViewBean.CHILD_TBUWRATECODE);
    customizeTextBoxWithAccess(pgUWorksheetViewBean.CHILD_TBUWPAYMENTTERMDESCRIPTION);

    //4. Customization for the calculated fields.
    customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWTOTALDOWN);
    customizeTextBox(pgUWorksheetViewBean.CHILD_TBUWESCROWPAYMENTTOTAL);

    // The data from the following fields should not be read and propagated to the entity
    customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS);
    customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS);

    //5. Customization for the MI module
    //--BMO_MI_CR--start//
    //// Do it only if the process is inside BXP (regular client). For BMO client
    //// open textbox and allow them to edit this value. MIPremium value has been
    //// returned to the underwriterdeal.properties, thus the propagation of this
    //// value to db should be skipped for the regular client.
    //logger.debug("UWH@SkipElementSize:IsOutsideBXP? " + isMIProcessOutsideBXP(pg));

    //--Release2.1--start//
    int indindx = getMIIndicator();
    int typeindx = getMITypeIndex();
    int insurerindx = getMInsurerIndex();
    //logger.debug("UWH@isMIUpdateAllowed? " + isMIUpdateAllowed(pg));

    // restrict MI choiceboxes to current values (if necessary)
    //// Even non-changable combobox labels should be translated via BXResources.
    int langId = theSessionState.getLanguageId();
    int statusindx = getMIStatusIndex();
    if (isMIProcessOutsideBXP(pg) == false) {
      customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWMIPREMIUM);
      customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWMIPREQCERTNUM);

      skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_TXUWMIPREMIUM));
      skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_TXUWMIPREQCERTNUM));
      skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_TXUWMICERTIFICATE));

      if (isMIUpdateAllowed(pg) == false) {
        reduceAccessToComboBox(thePage, pgUWorksheetViewBean.CHILD_CBUWMIINDICATOR, indindx, langId,
                               PKL_MIINDICATOR);
        reduceAccessToComboBox(thePage, pgUWorksheetViewBean.CHILD_CBUWMITYPE, typeindx, langId,
                               PKL_MORTGAGEINSURANCETYPE);
        reduceAccessToComboBox(thePage, pgUWorksheetViewBean.CHILD_CBUWMIINSURER, insurerindx, langId,
                               PKL_MORTGAGEINSURANCECARRIER);
        //// close access to the new editable (if MI process is outside BXP, BMO for example) fields.
        //QC-Ticket-389-Start -- Commented the following code 
        /*reduceAccessToComboBox(thePage, pgUWorksheetViewBean.CHILD_CBUWMISTATUS, statusindx, langId,
                               PKL_MISTATUS);*/
        //QC-Ticket-389-End
        customizeTextBox(pgUWorksheetViewBean.CHILD_TXUWMICERTIFICATE);

        //--Release3.1--begins
        //--by Hiro May 2, 2006
       reduceAccessToComboBox(thePage, pgUWorksheetViewBean.CHILD_CBPROGRESSADVANCETYPE,
            getProgressAdvanceTypeIndex(), langId, PKL_PROGRESSADVANCETYPE);
        reduceAccessToComboBox(thePage, pgUWorksheetViewBean.CHILD_CBLOCREPAYMENTTYPEID,
            getLOCRepaymentTypeIndex(), langId, PKL_LOCREPAYMENTTYPE);

        customizeTextBox(pgUWorksheetViewBean.CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER);
        disableDisplayField(pgUWorksheetViewBean.CHILD_RBPROGRESSADVANCEINSPECTIONBY);
        disableDisplayField(pgUWorksheetViewBean.CHILD_RBSELFDIRECTEDRRSP);
        disableDisplayField(pgUWorksheetViewBean.CHILD_RBREQUESTSTANDARDSERVICE);
        //QC-Ticket-389-Start- Added following method to disable MI Status field in Under Writer Work Sheet
        disableDisplayField(pgUWorksheetViewBean.CHILD_CBUWMISTATUS);
        //QC-Ticket-389-End
        // to avoid making db column null,  add disabled elements to skipped elements.
        skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_RBPROGRESSADVANCEINSPECTIONBY));
        skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_RBSELFDIRECTEDRRSP));
        skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_RBREQUESTSTANDARDSERVICE));
        skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER));
        skipUWElements.add(new String(pgUWorksheetViewBean.CHILD_CBLOCREPAYMENTTYPEID));
        //--Release3.1--ends

      }
      else if (isMIUpdateAllowed(pg) == true) {
        populateJSForMIProcessInsideBXP(thePage);
      }
    }
    else if (isMIProcessOutsideBXP(pg) == true) {
      //logger.debug("UWH@BeforepopulateJSIfMIProcessOutBXP");
      populateJSIfMIProcessOutBXP(thePage);

      String theMIPreQCertNum = "";
      if (indindx == Mc.MI_INDICATOR_PRE_QUALIFICATION) {
        theMIPreQCertNum = (String) thePage.getDisplayFieldValue(pgUWorksheetViewBean.CHILD_TXUWMIPREQCERTNUM);
      }
      else {
        theMIPreQCertNum = (String) thePage.getDisplayFieldValue(pgUWorksheetViewBean.CHILD_HDMIPREQCERTNUM);
      }

      //skipUWElements.add(new String("tbUWReferenceSourceApplNo"));
      logger.debug("UWH@theMIPreQCertNum: " + theMIPreQCertNum);
    }
    
    /***** FXP23234 start *****/
    String autoCalcCommit = (String) thePage
           .getDisplayFieldValue(pgUWorksheetViewBean.CHILD_RBAUTOCALCULATECOMMITMENTDATE);
    if ("Y".equalsIgnoreCase(autoCalcCommit)) {
        skipUWElements
                .add(new String(
                            pgUWorksheetViewBean.CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH));
        skipUWElements
                .add(new String(
                            pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY));
        skipUWElements
               .add(new String(
                        pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR));
    }
    /*****  FXP23234  end *****/
    // FXP31709:  AGF Express 4.4_UAT_The commitment expiry date is changing back to 120 days - Begin
    if (autoCalcCommit.equalsIgnoreCase("N")) {
        skipUWElements
                .remove(new String(
                            pgUWorksheetViewBean.CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH));
        skipUWElements
                .remove(new String(
                            pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY));
        skipUWElements
               .remove(new String(
                        pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR));
    }
    // FXP31709:  AGF Express 4.4_UAT_The commitment expiry date is changing back to 120 days  - End

/*    //Qualify Rate
    String qualifyFlag = (String) thePage
        .getDisplayFieldValue(pgUWorksheetViewBean.CHILD_CHQUALIFYRATEOVERRIDE);
    
    String qualifyRateFlag =  (String) thePage
        .getDisplayFieldValue(pgUWorksheetViewBean.CHILD_CHQUALIFYRATEOVERRIDERATE);
    
    if (qualifyFlag != null && "N".equalsIgnoreCase(qualifyFlag))
    	skipUWElements.add(new String(
                pgUWorksheetViewBean.CHILD_CBQUALIFYPRODUCTTYPE));

   	if (qualifyRateFlag != null && "N".equalsIgnoreCase(qualifyRateFlag)) 
   		skipUWElements.add(new String(
               pgUWorksheetViewBean.CHILD_TXQUALIFYRATE));*/

    logger.debug("UWH@SkipElementSize: " + skipUWElements.size());
    pg.setPageExcludePageVars(skipUWElements);
    //--BMO_MI_CR--end//
    //--Release2.1--end//

    //--DJ_CR_203.1--start--//
    if (this.isDJClient() == true) {
      populateDefaultValues(thePage);
    }
    //--DJ_CR_203.1--end--//

    // SEAN Ticket #1674 June 30, 2005: populate the source of business phone
    // number, including the extension number
/*    doUWSourceDetailsModel sourceInfoModel =
        (doUWSourceDetailsModel) RequestManager.getRequestContext().
        getModelManager().getModel(doUWSourceDetailsModel.class);
    setPhoneNumTextDisplayField(sourceInfoModel, pgUWorksheetViewBean.CHILD_STUWSOURCECONTACTPHONE,
                                sourceInfoModel.FIELD_DFCONTACTPHONENUMBER,
                                sourceInfoModel.FIELD_DFPHONENOEXTENTION,
                                0);*/
    // SEAN Ticket #1674 END


    // MI Up-Front - from main deal data object
    doUWDealSelectMainModel theModel = (doUWDealSelectMainModel)
      RequestManager.getRequestContext().getModelManager().getModel(
      doUWDealSelectMainModel.class);

    Object upfrontflag = theModel.getDfMIUpFrontId();

    if (upfrontflag != null && upfrontflag.toString().equals("Y")) {
      // restricted html output (MI Premium sectin is out).
      displayMIPremiumTitleConditional(pg, getCurrNDPage());
      displayMIPremiumValueConditional(pg, getCurrNDPage());
    }
    else {
      // default html output for the page
      displayMIPremiumTitle(pg, getCurrNDPage());
    }

    // 6. pass JS function to Open/Close 'Other Details' sub-screen. Currently 'Other
    //
    ////        handleJSDetailsScreenFunc("btOtherDetails");
    ////        handleJSDetailsScreenFunc("btCloseOtherDetails");

    //-- ========== SCR#859 begins ========== --//
    setCCAPSServicingMortgageNumber();
    //-- ========== SCR#859 ends ========== --//

    //7. Populate the Deal.ApplicantionDate for the Servlet -- by BILLY 05March2001
    int theDealId = pg.getPageDealId();
    int theCopyId = pg.getPageDealCID();
    SessionResourceKit srk = getSessionResourceKit();

    // Get the Application Date with format MM/dd/yyyy
    DateFormat theFormat = new SimpleDateFormat("MM/dd/yyyy");

    String theAppDate;
    String disRateDate = "";
    java.util.Date disabledRateDate;
    int dealRateStatusId = 0; //default is enable.

    // Get the corresponding Deal Entity
    Deal deal = null;
    try {
        deal = new Deal(srk, null, theDealId, theCopyId);
    } catch (Exception e) {
        throw new ExpressRuntimeException(e);
    }
    
    try
    {
        // Set to NOW if Application date = null
        if (deal.getApplicationDate() != null) theAppDate = theFormat.format(deal.getApplicationDate());
        else theAppDate = theFormat.format(new java.util.Date());

      //--DisableProductRateTicket#570--10Aug2004--start--//
      int dealPricingProfileId = deal.getPricingProfileId();

      dealRateStatusId = getDealRateStatusId(dealPricingProfileId);

      if(dealRateStatusId == Mc.PRICING_STATUS_DISABLED) {
        disabledRateDate = getDealRateDisDate(dealPricingProfileId);
        DateFormat disDateFormat = new SimpleDateFormat("MM/dd/yyyy");

        disRateDate = disDateFormat.format(disabledRateDate);
      } else {
        disabledRateDate = null;
        disRateDate = Mc.NULL_STRING;
      }

logger.debug("UWH@PopulatePageDisplayFields:DisabledRateDate: " + disabledRateDate);
logger.debug("UWH@PopulatePageDisplayFields:DealRateStatusId: " + dealPricingProfileId);
logger.debug("UWH@PopulatePageDisplayFields:DisabledRateDate_Formatted: " + disRateDate);

            //CR-18
            Request request = new Request(srk);
            boolean isReqInProgress =
				request.isRequestForAnotherScenario(theDealId, theCopyId);
            if(isReqInProgress)
            {
                DisplayField _df = thePage.getDisplayField(pgUWorksheetViewBean.CHILD_HDREQINPROGRESS);
                _df.setValue("Y");
            }
            else
            {
                DisplayField _df = thePage.getDisplayField(pgUWorksheetViewBean.CHILD_HDREQINPROGRESS);
                _df.setValue("N");
            }
        }
        catch(Exception e)
        {
          logger.debug("Exception DEH@PopulatePageDisplayFields:ApplicationDate:" + e);		//#DG702
            // Something goes wrong for the deal -- Just set to Current date by default
            theAppDate = theFormat.format(new java.util.Date());
        }

    String isEditable = getPageEditableFlag(pg);

     getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STAPPDATEFORSERVLET, new String(theAppDate));
     getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STDEALRATESTATUSFORSERVLET, (new Integer(dealRateStatusId)).toString());
     getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STRATEDISDATEFORSERVLET, new String(disRateDate));
     getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STISPAGEEDITABLEFORSERVLET, new String(isEditable));
     //--DisableProductRateTicket#570--10Aug2004--end--//

        // Check and set Display or not the FinancingProgram field
        setDisplayFinancingProgram(pg, thePage);

    // SEAN GECF IV Document Type Dorp down: chech the hide setting.
    // IV stands for Income Verification.
    setHideDocumentType(pg, thePage);
    // END GECF IV Document Type Dorp down

    //--DJ_LDI_CR--start//
    //// 1. Customize the GDS/TDS Label with regards to displaying of Life&Disability section.
    //// 2. Hide of display Life&Disability Insurance related info on the screen.
    String gdstdsLabel = "";

    if (PropertiesCache.getInstance().getProperty
                        (theSessionState.getDealInstitutionId(),
                         COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N").equals("N"))
    {
      gdstdsLabel = BXResources.getGenericMsg(GDS_TDS_NO_LIFE_DISABILITY_LABEL,  langId);

      startSupressContent(pgUWorksheetViewBean.CHILD_STINCLUDELIFEDISLABELSSTART, thePage);
      endSupressContent(pgUWorksheetViewBean.CHILD_STINCLUDELIFEDISLABELSEND, thePage);

      startSupressContent("RepeatGDSTDSDetails/stIncludeLifeDisContentStart", thePage);
      endSupressContent("RepeatGDSTDSDetails/stIncludeLifeDisContentEnd", thePage);
    }
    else if (PropertiesCache.getInstance().getProperty
                    (theSessionState.getDealInstitutionId(),
                     COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N").equals("Y"))
    {
      gdstdsLabel = BXResources.getGenericMsg(GDS_TDS_WITH_LIFE_DISABILITY_LABEL,  langId);
    }

    getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STGDSTDSDETAILSLABEL, gdstdsLabel);
    //--DJ_LDI_CR--end//

    //--DJ_CR134--start--27May2004--//
    //--Ticket#692--start--02Nov2004//
    // Open this option for every client, not property driven now.
  	/* #DG702 rewritten
    String homeBASEinfo = "";
    if (getHomeBASEProductRatePmnt() != null && !getHomeBASEProductRatePmnt().equals(""))
    {
      homeBASEinfo = getHomeBASEProductRatePmnt();

      getCurrNDPage().setDisplayFieldValue("stHomeBASERateProductPmnt", convertToHtmString(homeBASEinfo));
logger.debug("UWH@populatePageDisplayFields::HomeBASEProductRateConverted: " + convertToHtmString(homeBASEinfo));
    }
    else
    {
      getCurrNDPage().setDisplayFieldValue("stHomeBASERateProductPmnt",
                                          BXResources.getGenericMsg("NOT_AVAILABLE_LABEL",  theSessionState.getLanguageId()));
    }*/

    // Serghei Feb.03,2009
    // Paradigm ML FXP24081 "Requested Product" and "Rate and Payment" missing in warranty.
    String homeBaseDealNote = "";
    try {
        DealNotes dn = new DealNotes(srk);
    	dn.setSilentMode(true);
    	homeBaseDealNote = dn.findLastNoteTextByDealCategory(theDealId, theSessionState.getLanguageId(), DEAL_NOTES_CATEGORY_DEAL_INGESTION_PRODUCT);
    	homeBaseDealNote = homeBaseDealNote.trim();
	}
	catch (Exception e) {
    	homeBaseDealNote = "";
        logger.error("Exception @DealEntryHandler.setupBeforePageGeneration:homeBaseDealNote:" + e);		//#DG702
	}
	homeBaseDealNote = homeBaseDealNote.equals("") ?
        BXResources.getGenericMsg(NOT_AVAILABLE_LABEL,  langId):
        convertToHtmString(homeBaseDealNote);
    //logger.debug("DEH@populatePageDisplayFields::HomeBASEProductRatePmntConverted: " + convertToHtmString(homeBASEinfo));
    getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STHOMEBASEPRODUCTRATEPMNT, homeBaseDealNote);
   //--Ticket#692--end--02Nov2004//
   //--DJ_CR134--end--//

   //  ***** Change by NBC/PP Implementation Team - GCD - Start *****//

    if (!isShowGCDSumarySection ())
    {
      startSupressContent(pgUWorksheetViewBean.CHILD_ST_INCLUDE_GCDSUM_START, thePage);
      endSupressContent(pgUWorksheetViewBean.CHILD_ST_INCLUDE_GCDSUM_END, thePage);
    }

    //***** Change by NBC/PP Implementation Team - GCD - End *****//
    
        //Qualify Rate
        setHiddenQualifyRateSection(thePage);
        setQualifyRateJSValidation(thePage);
        try 
        {
        	
        	//fill the combobox
	        populateQualifyingProducts("cbQualifyProductType", deal);
	        
	        //set the product
	        int qualProd = deal.getOverrideQualProd();
	        if (qualProd > 0) 
	        {
	        	getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_CBQUALIFYPRODUCTTYPE, qualProd); 
            }
            
/*            //set asterik
            PricingLogic2 pLogic = new PricingLogic2();
            List<RateInfo> rates = pLogic.getLenderProductRates(srk, qualProd, deal.getDealId(), deal.getCopyId());
            RateInfo rateInfo = rates.get(0);
            double postRate = rateInfo.getPostedRate();
            double rate = Double.parseDouble(getCurrNDPage().getDisplayFieldValue(pgUWorksheetViewBean.CHILD_TXQUALIFYRATE).toString());
            if(postRate != rate )
                getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STASTERIK, "*");
            //asterik
	*/        
            Map defaultQualify = QualifyingRateHelper.getDefaultQualify(getSessionResourceKit(), deal);
            for (Iterator i = defaultQualify.keySet().iterator(); i.hasNext(); ) {
                Integer qualifyProduct = (Integer) i.next();
                Integer qualifyRate = (Integer) defaultQualify.get(qualifyProduct);
                getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STHDQUALIFYPRODUCTID, qualifyProduct);
                getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STHDQUALIFYRATE, qualifyRate);
                String prop = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                        "com.filogix.qualoverridechk", "Y");
                if (prop.equals("N")) {
                    HtmlDisplayField qualProdName =(HtmlDisplayField) getCurrNDPage().getDisplayField("stQualifyProductType");
                    String prodName = BXResources.
                    getPickListDescription(deal.getInstitutionProfileId(), "MTGPROD", qualifyProduct+"",
                            theSessionState.getLanguageId());
                    qualProdName.setValue(prodName); 
                    getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STQUALIFYRATE, qualifyRate);
                }
                break;
            }
            
        }
        catch (Exception e)
		{ 
			 logger.error("Exception @UWorksheetHandler.setupBeforePageGeneration:QualifyRate:" + e);
			 getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_TXQUALIFYRATE, pgUWorksheetViewBean.CHILD_TXQUALIFYRATE_RESET_VALUE);
		}
        
        //5.0 MI -- start
        String miresponse = StringUtils.trim(deal.getMortgageInsuranceResponse());
        if(miresponse != null){
            getCurrNDPage().setDisplayFieldValue(
                    pgUWorksheetViewBean.CHILD_TXMIRESPONSE_FIRSTLIEN,
                    StringUtils.substringBefore(miresponse, "\n"));
        }
        //5.0 MI -- end

}

    // create scenarion button(s) displayed only when deal status is "in underwriting"
    public boolean displayCreateScenarioButton()
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        if (pg.getPageCondition3() == true) return true;

        return false;
    }

    /**
     *
     *
     */
    public void setNewScenario1()
    {
    //--Release2.1--start//
    //// The html String should be formed based on the user languageId
    String html = "";
    if (theSessionState.getLanguageId() == 0) { //// English
              html = "<a onclick=\"return IsSubmited();\" href=\"javascript:newScen(1)\"><img id=\"imgNewScenario1\" src=\"../images/create_newscenario.gif\" ALIGN=TOP width=124 height=15 alt=\"\" border=\"0\"></a>";
    }
    if (theSessionState.getLanguageId() == 1) { //// French
              html = "<a onclick=\"return IsSubmited();\" href=\"javascript:newScen(1)\"><img id=\"imgNewScenario1\" src=\"../images/create_newscenario_fr.gif\" ALIGN=TOP width=124 height=15 alt=\"\" border=\"0\"></a>";
    }
    //--Release2.1--end//

        ViewBean currNDPage = getCurrNDPage();

        currNDPage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STNEWSCENARIO1, new String(html));
    }


    /**
     *
     *
     */
    public void setNewScenario2()
    {
    //--Release2.1--start//
    //// The html String should be formed based on the user languageId
    String html = "";
    if (theSessionState.getLanguageId() == 0) { //// English
          html = "<a onclick=\"return IsSubmited();\" href=\"javascript:newScen(2)\"><img id=\"imgNewScenario2\" src=\"../images/create_scenario_lrg.gif\" ALIGN=TOP width=153 height=25 alt=\"\" border=\"0\"></a>";
    }
    if (theSessionState.getLanguageId() == 1) { //// French
          html = "<a onclick=\"return IsSubmited();\" href=\"javascript:newScen(2)\"><img id=\"imgNewScenario2\" src=\"../images/create_scenario_lrg_fr.gif\" ALIGN=TOP width=153 height=25 alt=\"\" border=\"0\"></a>";
    }
    //--Release2.1--end//

    ViewBean currNDPage = getCurrNDPage();

        currNDPage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STNEWSCENARIO2, new String(html));
    }


    //
    // Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
    //

    public void setupBeforePageGeneration()
    {
        //logger.debug("PERFORMANCE CHECK ==> Entered setupBeforePageGeneration !!");
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        if (pg.getSetupBeforeGenerationCalled() == true) return;

        pg.setSetupBeforeGenerationCalled(true);

        // -- //
        setupUWDealSummarySnapShotDO(pg);

        int dealId = pg.getPageDealId();

        try {
            Deal _deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
            Request _req = new Request(srk);

            // CR-24
            boolean _isScenarioRecommended = _deal.getScenarioRecommended().equals("Y");
            boolean _isReqForAnotherScenario = _req.isRequestForAnotherScenario(pg.getPageDealId(), pg.getPageDealCID());
            if(!_isScenarioRecommended && _isReqForAnotherScenario) {
                pg.setEditable(false);
            }
        } catch (Exception e1) {
            logger.error(e1.toString());		//#DG702
			logger.error(e1);
        }

        if (dealId <= 0)
        {
            setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
            logger.error("Problem encountered displaying Under - invalid dealId (" + dealId + ")");
            return;
        }

        SessionResourceKit srk = getSessionResourceKit();

        // gold copy to be viewable if post-dsecision status
        pg.setPageState1(NOVIEWGOLD);

        try
        {
            Deal deal = new Deal(srk, null);
            deal = deal.findByGoldCopy(new DealPK(dealId, - 1));
            DealStatusManager dsm = DealStatusManager.getInstance(srk);
            int sCat = dsm.getStatusCategory(deal.getStatusId(), srk);
            if (sCat == Mc.DEAL_STATUS_CATEGORY_POST_DECISION ||
                sCat == Mc.DEAL_STATUS_CATEGORY_FINAL_DISPOSITION)
              pg.setPageState1("VIEWGOLD");
        }
        catch(Exception e)
        {
            ;
        }

        Integer cspDealId = new Integer(dealId);

        int copyid = theSessionState.getCurrentPage().getPageDealCID();

        Integer cspDealCPId = new Integer(copyid);

    //--DJ_CR134--start--27May2004--//
    // For DJ client execute this model to get HomeBASE product, rate, payment info (if exist)
    // dealnotes table (dealnotescategoryid = 10).
    //--Ticket#692--start--02Nov2004//
    // Open this option for every client, not property driven now.

    /* #DG702 replaced - clean up code
    Integer notesCategoryId = new Integer(Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION_PRODUCT);
    executeCretarioForDealNotesHomeBASEInfo(cspDealId, notesCategoryId);*/
//		try {
//    	DealNotes dn = new DealNotes(srk);
//    	dn.setSilentMode(true);
//    	homeBaseDealNote = dn.findLastNoteTextByDealCategory(dealId, theSessionState.getLanguageId(), DEAL_NOTES_CATEGORY_DEAL_INGESTION_PRODUCT);
//    	homeBaseDealNote = homeBaseDealNote.trim();
//		}
//		catch (Exception e) {
//    	homeBaseDealNote = "";
//      logger.error("Exception @DealEntryHandler.setupBeforePageGeneration:homeBaseDealNote:" + e);		//#DG702
//		}
    //--Ticket#692--end--02Nov2004//
    //--DJ_CR134--end--//

        // call to set criteria for data objects
        
		//4.4 Submission Agent
		doUWSourceDetailsModelImpl douwsourcedetails = (doUWSourceDetailsModelImpl) 
			(RequestManager.getRequestContext().getModelManager().getModel(doUWSourceDetailsModel.class));
		douwsourcedetails.clearUserWhereCriteria();
		douwsourcedetails.addUserWhereCriterion("dfDealId", "=", cspDealId);
		
        executeCretarioForUWMainDeal(cspDealId, cspDealCPId, pg);

    setPageFlagsFromMainDo(pg);

        executeCretarioForUWApplicantFinancial(cspDealId, cspDealCPId);

        executeCretarioForUWApplicantGDSTDS(cspDealId, cspDealCPId);

        //***** Change by NBC/PP Implementation Team - GCD - Start *****//

        executeCretarioForAdjudicationResponseBNCModel(cspDealId, cspDealCPId);
        executeCretarioForDealMIStatusModel(cspDealId, cspDealCPId);

        //***** Change by NBC/PP Implementation Team - GCD - End *****//


        executeCretarioForUWEscrowPayment(cspDealId, cspDealCPId);

        executeCretarioForUWSourceInfo(cspDealId, cspDealCPId);

        executeCretarioForUWPropertyInfo(cspDealId, cspDealCPId);

        executeCretarioForUWBridgeLoan(cspDealId, cspDealCPId);

        executeCretarioForUWDealDownPayment(cspDealId, cspDealCPId);

//        executeCretarioForUWSourceDisplay(cspDealId, cspDealCPId);

    executeCretarioForMIInsurer(cspDealId, cspDealCPId);

    executeCretarioForMIType(cspDealId, cspDealCPId);

    //--Release3.1--begins
    //--by Hiro May 2, 2006
    executeCretarioForProgressAdvanceType(cspDealId,cspDealCPId);
    executeCretarioForLOCRepaymentType(cspDealId,cspDealCPId);
    //--Release3.1--ends

    // to disable MI Fields for Denied and collapsed deals
    boolean miFieldsDisabled = false;
    try {
        Deal _deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
        miFieldsDisabled = disableMIFieldsForDeniedDeal(_deal.getStatusId());
    } catch (Exception e1) {
        logger.error(e1.toString());
		logger.error(e1);
    }
    
    boolean updateMIAllowed = !miFieldsDisabled && executeCretarioForUWMIStatus(cspDealId, cspDealCPId);

    pg.setPageCondition5(updateMIAllowed);

    //--BMO_MI_CR--start//
    //// When MI is not handled outside Xpress close access to MI Status and
    //// MI Certificate fields (close for all clients except BMO).
    boolean isOutsideBXP = getCretarioIsMIStatusOutsideBXP();
        pg.setPageCondition9(isOutsideBXP);

    ViewBean thePage = getCurrNDPage();
    HiddenField theOutsideXpress =(HiddenField) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_HDOUTSIDEXPRESS);

    if (isMIProcessOutsideBXP(pg) == false)
    {
       // Populate hidden for the set of JS functions resposible for open/close access to the fields.
       theOutsideXpress.setValue(new Integer(Mc.MI_HANDLED_OUTSIDE_XPRESS_NO).toString()); // regular execution path
    }
    else if (isMIProcessOutsideBXP(pg) == true)
    {
       // Populate hidden for the set of JS functions resposible for open/close access to the fields.
       theOutsideXpress.setValue(new Integer(Mc.MI_HANDLED_OUTSIDE_XPRESS_YES).toString()); // BMO execution path
    }
    logger.debug("UWH@setupBeforePageGeneration::isOutsideBXP?: " + isMIProcessOutsideBXP(pg));
    //--BMO_MI_CR--end//

    //--DJ_PT_CR--start//
        executeCretarioForUWPartyInfo(cspDealId);
    //--DJ_PT_CR--end//

        double netWorthSum = countNetWorthTotalFromDo();
        pg.setNetWorth(netWorthSum);

    //logger.debug("UWH@setupBeforePageGeneration:: NetWorth: " + pg.getNetWorth());

        preparePageGeneration(srk, pg, pg.getPageDealId(), pg.getPageDealCID());
        /////////////////////
        String prop = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                "com.filogix.qualoverridechk", "Y");
        if (prop.equals("N")) {
            int qualProdId = 0;
            int institutionId = 0;
            try {
                Deal _deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
                qualProdId = _deal.getOverrideQualProd();
                institutionId = _deal.getInstitutionProfileId();
            } catch (Exception e1) {
                logger.error(e1.getMessage());
                logger.error(e1);
            }
            //com.iplanet.jato.view.html.HtmlDisplayField
            
            HtmlDisplayField qualProdName =(HtmlDisplayField) thePage.getDisplayField("stQualifyProductType");
        
            String prodName = BXResources.
            getPickListDescription(institutionId, "MTGPROD", qualProdId,
                    theSessionState.getLanguageId());
            qualProdName.setValue(prodName); 
        }
        /////////////////////
    }

    /**
     *
     *
     */
    private void setPageFlagsFromMainDo(PageEntry pg)
    {
    boolean b = false;
    doUWDealSelectMainModel theModel = (doUWDealSelectMainModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doUWDealSelectMainModel.class);


    // Page Condition 7 ... rate locked ...
    //
    // WARNING BJH :: Previously obtained from doUWDealRateLockModel - but fields already in doUWDealSelectMainModel -
    //                dropped usage of former

    Object isRateLocked = theModel.getDfRateLock();
    b = false;
    if (isRateLocked != null && (isRateLocked.toString()).equals("Y"))
      b = true;

    pg.setPageCondition7(b);

    // Page Condition 8 ... is electronic origination

    // WARNING BJH :: Previously obtained from doDealOriginationModel which obtained DEAL.CHANNELMEDIA. Added
    //                field to doUWDealSelectMainModel so separate data object no longer needed.

    Object channelMedia = theModel.getDfChannelMedia();

    b = false;
    //--Ticket#XXXX--09Feb2005--start--//
    if (channelMedia != null && (channelMedia.toString()).equals("E"))
    //--Ticket#XXXX--09Feb2005--end--//

      b = true;

      pg.setPageCondition8(b);
  }

    //
    // Perform any special activities required immediately before page generation.
    //

    private void preparePageGeneration(SessionResourceKit srk, PageEntry pg, int dealId, int copyId)
    {

        //  data-validation messages, produced only during first-time display (see
        //  pageCondition2 documentation above)
        if (pg.getPageCondition2() == false)
        {
            try
            {

            	Deal deal = DBA.getDeal(srk, dealId, copyId);
            	//4.3GR, Update Total Loan Amount
            	if(displayAMLForUpdatedMIPremium(deal)) return;
            	
                pg.setPageCondition2(true);
                if ((PropertiesCache.getInstance().getProperty
                                (theSessionState.getDealInstitutionId(),
                                 COM_BASIS100_DEAL_UW_RUNBUSINESSRULSWHENENTRY, "Y")).equals("Y"))
                {
                    // ensure selected copy is an unlocked scenario
                    String sLock = deal.getScenarioLocked();
                    if (sLock != null && sLock.equals("N"))
                    {
                        // run data validation business rules
                        PassiveMessage pm = validate(deal, pg, srk);
                        if (pm != null)
                        {
                            pm.setGenerate(true);
                            theSessionState.setPasMessage(pm);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                logger.error("Exception @UWorksheetHandler.preparePageGeneration: - ignored.");
                logger.error(e);
            }
        }

    }


    /**
     *
     *
     */
    public boolean displayUWReadOnlyButton()
    {
    doUWDealSelectMainModel theModel = (doUWDealSelectMainModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doUWDealSelectMainModel.class);

    Object oSLock = theModel.getDfScenarioLocked();

    if (oSLock != null && oSLock.toString().equals("Y"))
      return false;

    return true;
    }


    /**
     *
     *
     */
    private void executeCretarioForUWMainDeal(Integer dealId, Integer cspDealCPId, PageEntry pg)
    {

        ///logger.debug("--D--> @setDealMainDisplayFields: dealId = " +TypeConverter.asInt(dealId) + ", copyId = " +TypeConverter.asInt(cspDealCPId));
        doUWDealSelectMainModel doUWdealentrymain =
                          (doUWDealSelectMainModel) RequestManager.getRequestContext().getModelManager().getModel(doUWDealSelectMainModel.class);

        doUWdealentrymain.clearUserWhereCriteria();

        doUWdealentrymain.addUserWhereCriterion("dfDealId", "=", dealId);

        doUWdealentrymain.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

        doUWdealentrymain.addUserWhereCriterion("dfInstitutionId", "=", pg.getDealInstitutionId());

    executeModel(doUWdealentrymain, "UWH@@executeCretarioForUWMainDeal", true);

        setDealMainDisplayFields(doUWdealentrymain, dealId.intValue(), pg, 0);

        //// SYNCADD. The execution is moved to the TiledView calleer by JATO framework.
        // Execute the DealNote dataObject -- Added button to popup DealNote Window
        //  -- By BILLY 28May2002
        // set the criteria for the populating data object
        doDealNotesInfoModel theDO =
                          (doDealNotesInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doDealNotesInfoModel.class);

        theDO.clearUserWhereCriteria();

        theDO.addUserWhereCriterion("dfDealID", "=", dealId);

        //fxp23512 - add language filter to dealnotes model instance
        final String langStr = "("+ theSessionState.getLanguageId()+ ", "+ LANGUAGE_UNKNOW + ")";
        theDO.addUserWhereCriterion("dfLanguagePreferenceID", "in", langStr);

    logger.debug("UWH@setTaskCountByPriority::DealNotesText: " + theDO.getDfDealNotesText());
    }

  //--DJ_PT_CR--start//
    private void executeCretarioForUWPartyInfo(Integer dealId)
    {
        ///logger.debug("--D--> @executeCretarioForUWMainDeal: dealId = " +TypeConverter.asInt(dealId));

    doPartySummaryModel partyInfoModel = (doPartySummaryModel)
                                RequestManager.getRequestContext().getModelManager().getModel(doPartySummaryModel.class);

    partyInfoModel.clearUserWhereCriteria();
        partyInfoModel.addUserWhereCriterion("dfDealId", "=", dealId);

    //// Display Party Info only for Origination Branch type (it is only one party type could be assigned to a deal.
    partyInfoModel.addUserWhereCriterion("dfPartyTypeId", "=", new Integer(Mc.PARTY_TYPE_ORIGINATION_BRANCH));

    this.executeModel(partyInfoModel, "UWH@executeCretarioForUWPartyInfo", false);
  }
  //--DJ_PT_CR--end//

    /**
     *
     *
     */
    private void setDealMainDisplayFields(doUWDealSelectMainModel dobject, int dealId, PageEntry pg, int rowNum)
    {

        ///logger.debug("--D--> @setDealMainDisplayFields: dealId = " + dealId);
        try
        {
            setTermsDisplayField(dobject, pgUWorksheetViewBean.CHILD_TXUWAMORTIZATIOPERIODYEARS,
            		pgUWorksheetViewBean.CHILD_TXUWAMORTIZATIOPERIODMONTHS,
            		doUWDealSelectMainModel.FIELD_DFAMORTIZATIONTERM, rowNum);
            setTermsDisplayField(dobject, pgUWorksheetViewBean.CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS,
            		pgUWorksheetViewBean.CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS,
            		"dfEffectiveAmortizationInMonths", rowNum);
            setTermsDisplayField(dobject, pgUWorksheetViewBean.CHILD_TXUWACTUALPAYYEARS,
            		pgUWorksheetViewBean.CHILD_TXUWACTUALPAYMONTHS,
            		"dfActualPaymentTerm", rowNum);
            fillupMonths(pgUWorksheetViewBean.CHILD_CBUWESTIMATEDCLOSINGDATEMONTH);
            fillupMonths(pgUWorksheetViewBean.CHILD_CBUWFIRSTPAYMETDATEMONTH);
            fillupMonths(pgUWorksheetViewBean.CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH);

            fillupMonths(pgUWorksheetViewBean.CHILD_CBUWREFIORIGPURCHASEDATEMONTH);
            setDateDisplayField(dobject, pgUWorksheetViewBean.CHILD_CBUWESTIMATEDCLOSINGDATEMONTH,
            		pgUWorksheetViewBean.CHILD_TXUWESTIMATEDCLOSINGDATEDAY,
            		pgUWorksheetViewBean.CHILD_TXUWESTIMATEDCLOSINGDATEYEAR,
            		"dfEstimatedClosingDate", rowNum);
            setDateDisplayField(dobject, pgUWorksheetViewBean.CHILD_CBUWFIRSTPAYMETDATEMONTH,
            		pgUWorksheetViewBean.CHILD_TXUWFIRSTPAYMETDATEDAY,
            		pgUWorksheetViewBean.CHILD_TXUWFIRSTPAYMETDATEYEAR,
            		"dfFirstPaymentDate", rowNum);
            setDateDisplayField(dobject, pgUWorksheetViewBean.CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH,
            		pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY,
            		pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR,
            		"dfReturnDate", rowNum);

            setDateDisplayField(dobject, pgUWorksheetViewBean.CHILD_CBUWREFIORIGPURCHASEDATEMONTH,
            		pgUWorksheetViewBean.CHILD_TXUWREFIORIGPURCHASEDATEDAY,
            		pgUWorksheetViewBean.CHILD_TXUWREFIORIGPURCHASEDATEYEAR,
            		"dfRefiOrigPurchaseDate", rowNum);

            /***** FFATE start *****/
            fillupMonths(pgUWorksheetViewBean.CHILD_CBUWFINANCINGWAIVERDATEMONTH);
            setDateDisplayField(dobject, pgUWorksheetViewBean.CHILD_CBUWFINANCINGWAIVERDATEMONTH,
                    pgUWorksheetViewBean.CHILD_TXUWFINANCINGWAIVERDATEDAY,
                    pgUWorksheetViewBean.CHILD_TXUWFINANCINGWAIVERDATEYEAR,
                    "dfFinancingWaiverDate", rowNum);
            /***** FFATE start *****/
            
            //CR03 start
            fillupMonths(pgUWorksheetViewBean.CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH);
            setDateDisplayField(dobject, pgUWorksheetViewBean.CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH,
            		pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY,
            		pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR,
            		"dfCommitmentExpirationDate", rowNum);

            if(PropertiesCache.getInstance().getProperty
                            (theSessionState.getDealInstitutionId(),
                             COM_BASIS100_CALC_COMMITMENTEXPIRYDATE_USECOMMITMENTTERM, "N").equals("Y")){
                getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_HDMOSPROPERTY, "true");
            }


            Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
            Date commitDate = deal.getCommitmentExpirationDate();
            if (commitDate != null) {
                SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
                String dateString = formatter.format(commitDate);
                if (dateString!=null && dateString.trim().length() > 0)
                    getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_HDCOMMITMENTEXPIRYDATE, dateString);
            }
            //CR03 end


            //Purchase Plus Improvements

            double improvedValue = deal.getTotalPurchasePrice() + deal.getRefiImprovementAmount();
            getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_TXUWIMPROVEDVALUE, new Double(improvedValue));

            switch (deal.getDealPurposeId()) {
                case Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS:
                    getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STDEALPURPOSETYPEHIDDENSTART, "");
                    getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STDEALPURPOSETYPEHIDDENEND, "");
                    break;
                default:
                    getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STDEALPURPOSETYPEHIDDENSTART, supresTagStart);
                    getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STDEALPURPOSETYPEHIDDENEND, supresTagEnd);
                    break;
            }
            //Purchase Plus Improvements

      // fill scenario chioce boxes (cbScenarios - use to support switching viewed scenario)
      //                            (cbScenarioPickListTop, and cbScenarioPickListBottom - both are identival and
      //                             used to support scenario creation.

      // get deals (all selectable scenarios including gold copy and locked scenarios - filter while filling
      // choice box options to avoid unnecessary DB queries)

      deal = new Deal(srk, null);
      Collection deals = deal.findBySelectableScenarios(dealId,     true, true);

            boolean viewGold =VIEWGOLD.equals(pg.getPageState1());  // is gold selectable for viewing

            fillSelectableScenarios(pgUWorksheetViewBean.CHILD_CBSCENARIOS,              viewGold, true,  deals);
            fillSelectableScenarios(pgUWorksheetViewBean.CHILD_CBSCENARIOPICKLISTTOP,    true,     false, deals);
            fillSelectableScenarios(pgUWorksheetViewBean.CHILD_CBSCENARIOPICKLISTBOTTOM, true,     false, deals);
        }
        catch(Exception e)
        {
            logger.error("Exception @setDealMainDisplayFields: - ignored.");
            logger.error(e);
        }
    }

    //
    // Choice boxes for selectable scenarios (switch and create new). Display description,
    // value retrieved is copyId (of the scenario)
    //

    protected void fillSelectableScenarios(String dispName, boolean includeGold, boolean includeLocked, Collection deals)
    {
        int lim = deals.size();
        Object [ ] objs = deals.toArray();

    //logger.debug("--D--> UWH@fillSelectableScenarios: df=" + dispName + ", rows=" + lim);

    String [] labels = new String[lim];
    String [] values = new String[lim];

    int i = 0, j = 0;

        for(; i < lim;  i ++)   {
            Deal deal =(Deal)objs [i];

      if (includeGold == false && "G".equals(deal.getCopyType()))
        continue;

      if (includeLocked == false && "Y".equals(deal.getScenarioLocked()))
        continue;

      labels[j] = formScenarioDescription(deal);
      values[j] = (new Integer(deal.getCopyId())).toString();

      j++;

            //logger.debug("--D--> UWH@fillSelectableScenarios: s.desc = " + formScenarioDescription(deal) + ", s.cid=" + deal.getCopyId());
        }

    // truncate options of some scenarios (deals) skipped
    if (j < lim)
    {
        String [] labs = new String[j]; String [] vals = new String[j];
        for (i = 0; i < j; i++)  {
          labs[i] =labels[i];  vals[i] = values[i];
        }
        labels = labs;  values = vals;
    }

        ComboBox cbxScenarios =(ComboBox) getCurrNDPage().getDisplayField(dispName);

    cbxScenarios.getOptions().setOptions(labels, values);
    }

    /**
     *
     *
     */
    private String formScenarioDescription(Deal deal)
    {
        String desc = "";

        if (deal.getScenarioLocked() != null && deal.getScenarioLocked().equals("Y")) desc = desc + "L";
        else desc = desc + " ";

        desc = desc +("" + deal.getScenarioNumber()) + ". ";

    //--Release2.1--start//
    //// The translation of the scenario description must be done only for the
    //// selected/restricted number of them. Finalize this list, translate and
    //// include into the BXGeneric.properties files.
    ////logger.debug("UWH@formScenarioDescription::Desc: " + desc);

        String d = deal.getScenarioDescription();

    //// Translate pre-selected scenarios descriptions.
    int langId = theSessionState.getLanguageId();

    d = translateScenarioDescription(d, langId);
    //logger.debug("UWH@formScenarioDescription::TranslatedStringD: " + d);
    //--Release2.1--end//

        if (d == null && deal.getCopyType() != null && deal.getCopyType().equals("G"))

    //--Release2.1--start//
    ////d = "Original Deal";
    d = BXResources.getGenericMsg(ORIGINAL_DEAL_LABEL,  theSessionState.getLanguageId());
    //--Release2.1--end//

        desc = desc + d;

        String recommended = deal.getScenarioRecommended();

        if (recommended != null && recommended.equals("Y")) desc = desc + "*";

        return desc;
    }

    /**
     *
     *
     */
    private void executeCretarioForUWApplicantFinancial(Integer dealId, Integer cspDealCPId)
    {
        doUWApplicantFinancialDetailsModel doUWappFinancial =
                          (doUWApplicantFinancialDetailsModel) RequestManager.getRequestContext().getModelManager().getModel(doUWApplicantFinancialDetailsModel.class);

        doUWappFinancial.clearUserWhereCriteria();
        doUWappFinancial.addUserWhereCriterion("dfDealId", "=", dealId);
        doUWappFinancial.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doUWappFinancial, "UWH@executeCretarioForUWApplicantFinancial", false);
    }


    //***** Change by NBC/PP Implementation Team - GCD - Start *****//
    //GCD Summary data retrieval Begin
  /**
     *
     *
     */
    private void executeCretarioForDealMIStatusModel(Integer dealId, Integer cspDealCPId)
    {
        doDealMIStatusModel doDMISDo =
                          (doDealMIStatusModel) RequestManager.getRequestContext().getModelManager().getModel(doDealMIStatusModel.class);

        doDMISDo.clearUserWhereCriteria();
        doDMISDo.addUserWhereCriterion("dfDealId", "=", dealId);
        doDMISDo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doDMISDo, "UWH@executeCretarioFordoDealMIStatusModel", false);
    }

    //***** Change by NBC/PP Implementation Team - GCD - End *****//



    /**
     *
     *
     */
    private void executeCretarioForUWApplicantGDSTDS(Integer dealId, Integer cspDealCPId)
    {
        doUWGDSTDSApplicantDetailsModel doUWappGDSTDS =
                          (doUWGDSTDSApplicantDetailsModel) RequestManager.getRequestContext().getModelManager().getModel(doUWGDSTDSApplicantDetailsModel.class);

        doUWappGDSTDS.clearUserWhereCriteria();
        doUWappGDSTDS.addUserWhereCriterion("dfDealId", "=", dealId);
        doUWappGDSTDS.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doUWappGDSTDS, "UWH@executeCretarioForUWApplicantGDSTDS", false);
    }


    /**
     *
     *
     */
    private void executeCretarioForUWBridgeLoan(Integer cspDealId, Integer cspDealCPId)
    {
        doUWBridgeModel douwbridge =
                          (doUWBridgeModel) RequestManager.getRequestContext().getModelManager().getModel(doUWBridgeModel.class);

        douwbridge.clearUserWhereCriteria();
        douwbridge.addUserWhereCriterion("dfDealId", "=", cspDealId);
        douwbridge.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(douwbridge, "UWH@executeCretarioForUWBridgeLoan", true);
    }


    /**
     *
     *
     */
    private void executeCretarioForUWEscrowPayment(Integer dealId, Integer cspDealCPId)
    {
        doUWEscrowDetailsModel doUWescrowPayment =
                          (doUWEscrowDetailsModel) RequestManager.getRequestContext().getModelManager().getModel(doUWEscrowDetailsModel.class);

        doUWescrowPayment.clearUserWhereCriteria();
        doUWescrowPayment.addUserWhereCriterion("dfDealId", "=", dealId);
        doUWescrowPayment.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doUWescrowPayment, "UWH@executeCretarioForUWEscrowPayment", false);
    }


    /**
     *
     *
     */
    private void executeCretarioForUWDealDownPayment(Integer dealId, Integer cspDealCPId)
    {
        doUWDealDownPaymentModel doUWdownPayment =
                          (doUWDealDownPaymentModel) RequestManager.getRequestContext().getModelManager().getModel(doUWDealDownPaymentModel.class);

        doUWdownPayment.clearUserWhereCriteria();
        doUWdownPayment.addUserWhereCriterion("dfDealId", "=", dealId);
        doUWdownPayment.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doUWdownPayment, "UWH@executeCretarioForUWDealDownPayment", false);
    }


    //
    // Result:
    //
    //  true  - MI values (e.g. MI Indicator, MI Insurer, etc) may be changed by user
    //  false - MI values cannot be changed,
    //

    private boolean executeCretarioForUWMIStatus(Integer dealId, Integer cspDealCPId)
    {
    doUWMIStatusModel doMIStatusObj =
                        (doUWMIStatusModel) RequestManager.getRequestContext().getModelManager().getModel(doUWMIStatusModel.class);

        doMIStatusObj.clearUserWhereCriteria();
        doMIStatusObj.addUserWhereCriterion("dfDealId", "=", dealId);
        doMIStatusObj.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doMIStatusObj, "executeCretarioForUWMIStatus", true);

    Object isMIStatusUpdateAllowed = doMIStatusObj.getDfMIStatusUpdateAllowed();
    if (isMIStatusUpdateAllowed != null && (isMIStatusUpdateAllowed.toString()).equals("Y"))
        return true;

    return false;
    }
    
    /**
     * used to determine that MI fileds should be disabled for denied and collapsed deals - Ticket 267
     * @param dealStatusId
     * @return
     */
    private boolean disableMIFieldsForDeniedDeal(Integer dealStatusId)
    {

    	if (dealStatusId == Mc.DEAL_DENIED || dealStatusId == Mc.DEAL_COLLAPSED )
    	{
    		return true;
    	}
    	return false;
	}

    /**
     *
     *
     */
    private void executeCretarioForUWSourceInfo(Integer dealId, Integer cspDealCPId)
    {
        // here need to get the sourceProfile id for this deal
    String sourceIdInStr = "";

    doUWDealSelectMainModel theModel = (doUWDealSelectMainModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doUWDealSelectMainModel.class);

    Object fieldValue = theModel.getDfSourceFirmProfileId();

    Integer sourceProfId = new Integer(0);

    if(fieldValue != null)
    {
      sourceIdInStr = fieldValue.toString();
      //logger.debug("UWH@executeCretarioForUWSourceInfo::dfValueFromModel: " + sourceIdInStr);

      try
      {
        sourceProfId = new Integer(sourceIdInStr);
      }
      catch (Exception e)
      {
        logger.debug("Exception converting source id <" + sourceIdInStr + "> to integer:" + e);		//#DG702
        sourceProfId = new Integer(0);
      }
    }

    // Obtain source information bases on the sourceProfileId.

        doUWSourceInformationModel doUWSourceInfo =
                      (doUWSourceInformationModel) RequestManager.getRequestContext().getModelManager().getModel(doUWSourceInformationModel.class);

        doUWSourceInfo.clearUserWhereCriteria();
        doUWSourceInfo.addUserWhereCriterion("dfSourceBusinessProfileId", "=", sourceProfId);

    executeModel(doUWSourceInfo, "UWH@executeCretarioForUWSourceInfo", true);
    }

    //***** Change by NBC/PP Implementation Team - GCD - Start *****//
    //GCD Summary data retrieval Begin

    private void executeCretarioForAdjudicationResponseBNCModel(Integer dealId, Integer cspDealCPId)
    {
        doAdjudicationResponseBNCModel doARBNCDo =
                          (doAdjudicationResponseBNCModel) RequestManager.getRequestContext().getModelManager().getModel(doAdjudicationResponseBNCModel.class);

        doARBNCDo.clearUserWhereCriteria();
        doARBNCDo.addUserWhereCriterion("dfDealId", "=", dealId);
        doARBNCDo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doARBNCDo, "UWH@executeCretarioForAdjudicationResponseBNCModel", false);
    }

    //GCD Summary Data Retrieval End
    //***** Change by NBC/PP Implementation Team - GCD - End *****//


    /**
     *
     *
     */
    private void executeCretarioForUWSourceDisplay(Integer dealId, Integer cspDealCPId)
    {
        doUWSourceDetailsModel dodealsourceinfo =
                      (doUWSourceDetailsModel) RequestManager.getRequestContext().getModelManager().getModel(doUWSourceDetailsModel.class);

        dodealsourceinfo.clearUserWhereCriteria();
        dodealsourceinfo.addUserWhereCriterion("dfDealId", "=", dealId);
        dodealsourceinfo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(dodealsourceinfo, "UWH@executeCretarioForUWSourceDisplay", true);
    }


  public void executeCretarioForMIInsurer(Integer dealId, Integer cspDealCPId)
  {
        doUWMIInsurerModel theDo = (doUWMIInsurerModel)(RequestManager.getRequestContext().getModelManager().getModel(doUWMIInsurerModel.class));

        theDo.clearUserWhereCriteria();
        theDo.addUserWhereCriterion("dfDealId", "=", dealId);
        theDo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(theDo, "UWH@executeCretarioForMIInsurer", true);
  }

  public void executeCretarioForMIType(Integer dealId, Integer cspDealCPId)
  {
        doUWMITypeModel theDo = (doUWMITypeModel)(RequestManager.getRequestContext().getModelManager().getModel(doUWMITypeModel.class));

        theDo.clearUserWhereCriteria();
        theDo.addUserWhereCriterion("dfDealId", "=", dealId);
        theDo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(theDo, "UWH@executeCretarioForMITyper", true);
  }


    /**
     *
     *
     */
    private void executeCretarioForUWPropertyInfo(Integer dealId, Integer cspDealCPId)
    {
        doUWPropertyInfosModel doUWproperty =
                      (doUWPropertyInfosModel) RequestManager.getRequestContext().getModelManager().getModel(doUWPropertyInfosModel.class);

        doUWproperty.clearUserWhereCriteria();
        doUWproperty.addUserWhereCriterion("dfDealId", "=", dealId);
        doUWproperty.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(doUWproperty, "UWH@executeCretarioForUWPropertyInfo" , false);
    }

  //--DJ_CR134--start--27May2004--//
    /**
     *
     *
     */
  // New model introduced in order to escape overriding of its execution for different purposes.
  // Also Products wants to enhance the broker information further with other notesTypes.
 	/* #DG702 replaced - clean up code
  private void executeCretarioForDealNotesHomeBASEInfo(Integer dealId, Integer notesCategoryId)    {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    Hashtable pst = pg.getPageStateTable();

        doDealNotesBrokerInfoModel theDealNotes =
                          (doDealNotesBrokerInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doDealNotesBrokerInfoModel.class);

        theDealNotes.clearUserWhereCriteria();
        theDealNotes.addUserWhereCriterion("dfDealID", "=", dealId);
        theDealNotes.addUserWhereCriterion("dfDealNotesCategoryID", "=", notesCategoryId);

    this.executeModel(theDealNotes, "UWH@executeCretarioForDealNotes" , true);
    }*/

  //--Release3.1--begins
  //--by Hiro May 2, 2006
  public void executeCretarioForProgressAdvanceType(Integer dealId,
      Integer cspDealCPId)
  {
    doUWProgressAdvanceTypeModel theDo = (doUWProgressAdvanceTypeModel) (RequestManager
        .getRequestContext().getModelManager()
        .getModel(doUWProgressAdvanceTypeModel.class));

    theDo.clearUserWhereCriteria();
    theDo.addUserWhereCriterion("dfDealId", "=", dealId);
    theDo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(theDo, "UWH@executeCretarioForProgressAdvanceType", true);
  }

  public void executeCretarioForLOCRepaymentType(Integer dealId,
      Integer cspDealCPId)
  {

    doUWLOCRepaymentTypeModel theDo = (doUWLOCRepaymentTypeModel) (RequestManager
        .getRequestContext().getModelManager()
        .getModel(doUWLOCRepaymentTypeModel.class));

    theDo.clearUserWhereCriteria();
    theDo.addUserWhereCriterion("dfDealId", "=", dealId);
    theDo.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);

    this.executeModel(theDo, "UWH@executeCretarioForLOCRepaymentType", true);
  }

  /* #DG702 not used
  protected String getHomeBASEProductRatePmnt()    {
    doDealNotesBrokerInfoModel theDO = (doDealNotesBrokerInfoModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doDealNotesBrokerInfoModel.class);
    String notestext = "";
    Object mNotesText = theDO.getValue(theDO.FIELD_DFDEALNOTESTEXT);
    if(mNotesText != null)    {
      notestext = mNotesText.toString();
    }
    return notestext;
  }*/

    //
    //  Event handlers
    //
    //  MIAction - Values ::: April 26/01 :: No longer used

    public void handleResolveDeal(int MIAction)
    {
        logger.trace("--T--> @UWorksheetHandler.handleResolveDeal:");

        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        SysLogger logger = srk.getSysLogger();

        try
        {
            // first ensure selected scenario is allowed
            logger.trace("--T--> @UWorksheetHandler.handleResolveDeal: ensure resolve allowed (deal status)");
            Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

            DealStatusManager dsm = DealStatusManager.getInstance(srk);
            int cs = deal.getStatusId();
            int statusCat = dsm.getStatusCategory(cs, srk);
            if (statusCat != Mc.DEAL_STATUS_CATEGORY_DECISION || cs == Mc.DEAL_APPROVED)
            {
                setActiveMessageToAlert(BXResources.getSysMsg(UW_DEAL_RESOLUTION_NOT_ALLOWED_STATUS_CATEGORY, theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
                return;
            }
            logger.trace("--T--> @UWorksheetHandler.handleResolveDeal: ensure resolve allowed (scenario unlocked)");

            // selection of a locked scenario only allowed for external approval review/request
            if (deal.getScenarioLocked() != null && deal.getScenarioLocked().equals("Y") &&(!(cs == Mc.DEAL_EXTERNAL_APPROVAL_REVIEW || cs == Mc.DEAL_HIGHER_APPROVAL_REQUESTED)))
            {
                setActiveMessageToAlert(BXResources.getSysMsg(UW_CANNOT_RESOLVE_LOCKED_SCENARIO, theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
                return;
            }
            PassiveMessage pm = null;
            if (deal.getScenarioLocked() != null && deal.getScenarioLocked().equals("N"))
            {
                logger.trace("--T--> @UWorksheetHandler.handleResolveDeal: validate resolution");

                // run validation (except for external approval related)
                pm = validate(deal, pg, srk);
                if (pm != null && pm.getCritical() == true)
                {
                  setActiveMessageToAlert(BXResources.getSysMsg(
                      UW_CANNOT_RESOLVE_VALIDATION_CRITICAL, theSessionState
                      .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
                  pm.setGenerate(true);
                  theSessionState.setPasMessage(pm);
                  return;
                }
            }

            // force user to condition review (if necessary)
            if (pg.getPageCondition4() == false)
            {
                handleConditionsReview(false, true);
                return;
            }

            // post warnings (only) if any - however doesn't stop processing like critical
            if (pm != null && pm.getNumMessages() > 0)
            {
              setActiveMessageToAlert(
                  BXResources.getSysMsg(STANDARD_REVIEW_MESSAGES_WINDOW,
                      theSessionState.getLanguageId()),
                      ActiveMsgFactory.ISCUSTOMCONFIRM);
              pm.setGenerate(true);
              theSessionState.setPasMessage(pm);
            }

            // add message regarding integrity of MI policy.
                       //--BMO_MI_CR--start//
                       // only if regular execution MI process indide BXP, otherwise skip messaging.
                       if (isMIProcessOutsideBXP(pg) == false) {
                         setMIIntegrityMsg(srk, pg, deal);
                       }
                       //--BMO_MI_CR--end//

            // tx ...
            srk.beginTransaction();
            standardAdoptTxCopy(pg, true);
            deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
            if (deal.getScenarioRecommended() == null || ! deal.getScenarioRecommended().equals("Y"))
            {
                // set current scenario as the recommended scenario
                CCM ccm = new CCM(srk);
                ccm.setAsRecommendedScenario(deal);
                deal.ejbStore();
            }
            logger.trace("--T--> @UWorksheetHandler.handleResolveDeal: to deal resolution");

            // navigate to Deal Resolution
            PageEntry dealResolutionPg = setupDealResolutionPageEntry(pg);
            getSavedPages().setNextPage(dealResolutionPg);
            srk.commitTransaction();
            navigateToNextPage(true);
            return;
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleResolveDeal().");
            logger.error(e);
        }

        // an exception has occured - ensure we navigate away from page since we can't guar
        // presence of a tx copy on regeneration
        navigateAwayFromFromPage(true);

    }


    /**
     * <p>validate</p>
     *
     * @version 1.1 July 14, 2008 (XS 2.39/2.47 added component validation for DCC and DEC
     * @auther MCM Team
     *
     */
    public PassiveMessage validate(Deal deal, PageEntry pe, SessionResourceKit srk)
    throws Exception
    {
        PassiveMessage pm = new PassiveMessage();

        BusinessRuleExecutor brExec = new BusinessRuleExecutor();

        Vector v = new Vector();

        logger.debug("BILLY =================== << BusinessRules Validate Begin >> ================");


        // -- //
        v.add(BUZ_RULE_DI);
        v.add(BUZ_RULE_DE);
        v.add(BUZ_RULE_IC);
        v.add(BUZ_RULE_UN);
        logger.debug("before Running BusinessRules (-- BATCH --)");
        brExec.BREValidator(theSessionState, pe, srk, pm, v, true);
        logger.debug("After Running BusinessRules (-- BATCH --)");


        // add MI rules if indicator set to required
        if (deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL &&(deal.getMIIndicatorId() == Mc.MII_UW_REQUIRED || deal.getMIIndicatorId() == Mc.MII_REQUIRED_STD_GUIDELINES))
        {
            // check institution property to ensure this is enabled (note default is Y if property absent)
            if ((PropertiesCache.getInstance().getProperty
                    (theSessionState.getDealInstitutionId(),
                            INSTITUTION_VALIDATE_MI_AT_DEAL_RESOLVE, "Y")).equals("Y"))
            {
                PassiveMessage pmForMI = new PassiveMessage();
                logger.debug("==> before Running BusinessRules (MI-%)");
                brExec.BREValidator(theSessionState, pe, srk, pmForMI, BUZ_RULE_MI, true);
                logger.debug("==> After Running BusinessRules (MI-%)");

                // Check if need to treat all MI Rule results as Non-Critical (note default is N if property absent)
                if ((PropertiesCache.getInstance().getProperty
                        (theSessionState.getDealInstitutionId(),
                                COM_BASIS100_DEAL_UW_TREATMIRULESASNONCRITICAL, "N")).equals("Y"))
                {
                    pmForMI.setAllAsNonCritcal();
                }
                pm.addAllMessages(pmForMI);
            }
        }

        // local scope rules ...
        int lim = 0;
        Object [ ] objs = null;
        // borrower rules
        try
        {
            Collection borrowers = deal.getBorrowers();
            lim = borrowers.size();
            objs = borrowers.toArray();
        }
        catch(Exception e)
        {
            lim = 0;
        }


        // assume no borrowers
        for(int i = 0;
        i < lim;
        ++ i)
        {
            Borrower borr =(Borrower) objs [ i ];
            brExec.setCurrentProperty(- 1);
            //MCM Team Starts: July 14, 2008
            brExec.setCurrentComponent(-1);
            //MCM Team Ends: July 14, 2008
            brExec.setCurrentBorrower(borr.getBorrowerId());
            logger.trace("--T--> @UWorksheetHandler.validate: DEA rules, borrower num=" + i);
            PassiveMessage pmBorr = brExec.BREValidator(theSessionState, pe, srk, null, BUZ_RULE_DEA, true);
            if (pmBorr != null)
            {
                pm.addSeparator();
                //--Release2.1--//
                //// Message should be translated via BXResources.
                ////pm.addMsg("The following message(s) apply to applicant: " + borr.formFullName(), PassiveMessage.INFO);
                String applicantLabel = (BXResources.getGenericMsg(PASSIVE_MSG_APPLIED_TO_APPLICANT_LABEL, theSessionState.getLanguageId())
                        + "<br>");

                pm.addMsg(applicantLabel + borr.formFullName(), PassiveMessage.INFO);

                pm.addAllMessages(pmBorr);
                pm.addSeparator();
            }
        }

        // property rules
        try
        {
            Collection properties = deal.getProperties();
            lim = properties.size();
            objs = properties.toArray();
        }
        catch(Exception e)
        {
            lim = 0;
        }


        // assume no properties
        for(int i = 0;
        i < lim;
        ++ i)
        {
            Property prop =(Property) objs [ i ];
            brExec.setCurrentProperty(prop.getPropertyId());
            brExec.setCurrentBorrower(- 1);
            //MCM Team Starts: July 14, 2008
            brExec.setCurrentComponent(-1);
            //MCM Team Ends: July 14, 2008
            logger.trace("--T--> @UWorksheetHandler.validate: DEP rules, propert num=" + i);
            PassiveMessage pmProp = brExec.BREValidator(theSessionState, pe, srk, null, BUZ_RULE_DEP, true);
            if (pmProp != null)
            {
                pm.addSeparator();
                //--Release2.1--//
                //// Message should be translated via BXResources.
                ////pm.addMsg("The following message(s) apply to property: " + prop.formPropertyAddress(), PassiveMessage.INFO);
                String propLabel = (BXResources.getGenericMsg(PASSIVE_MSG_APPLIED_TO_PRORERTY_LABEL, theSessionState.getLanguageId())
                        + "<br>");
                pm.addMsg(propLabel + prop.formPropertyAddress(), PassiveMessage.INFO);

                pm.addAllMessages(pmProp);
                pm.addSeparator();
            }
        }

        /***************MCM Impl team changes starts - FXP22741*******************/
        PassiveMessage pmComp = validateComponents(pe, srk);
        pm.addAllMessages(pmComp);

//        //MCM Team (XS2.47) Starts: July 14, 2008
//        try
//        {
//            // logger.debug("@DealHandlerCommon.validateDealEntry: collect properties for DEP rules");
//            Collection<Component> components = deal.getComponents();
//            lim = components.size();
//            objs = components.toArray();
//        }
//        catch(Exception e)
//        {
//            lim = 0;
//        }
//        //06-Aug-2008 |XS_2.48| Removed the call of DCC- businessrules.
//        int lastCompTypeid = -1; //FXP22741, changed numbering
//        int compSequence = 0; //FXP22741, changed numbering
//        for(int i = 0; i < lim; ++ i)
//        {
//            Component comp =(Component) objs [ i ];
//            brExec.setCurrentComponent(comp.getComponentId());
//            brExec.setCurrentBorrower(- 1);
//            brExec.setCurrentProperty(- 1);
//
//            //FXP22741, changed numbering
//            compSequence = (lastCompTypeid != comp.getComponentTypeId()) ? 1 : compSequence + 1;
//
//            PassiveMessage pmCompDEC = brExec.BREValidator(theSessionState, pe, srk, null, "DEC-%", true);
//            if (pmCompDEC != null)
//            {
//                pm.addSeparator();
//                pm.addMsg(BXResources.getSysMsg("PASSIVE_MESSAGE_SUBTITLE_COMPONENT_PROBLEM", theSessionState.getLanguageId())
//                        + BXResources.getPickListDescription(-1, "COMPONENTTYPE", comp.getComponentTypeId(), theSessionState.getLanguageId())
//                        + " " + compSequence, PassiveMessage.INFO);
//                pm.addAllMessages(pmCompDEC);
//                pm.addSeparator();
//            }
//            lastCompTypeid = comp.getComponentTypeId(); //FXP22741, changed numbering
//        }
//        //MCM Team (XS2.47) Ends: July 14, 2008
        /***************MCM Impl team changes ends - FXP22741*********************/

        brExec.close();
        logger.debug("=================== << BusinessRules Validate End >> ================");

        if (pm.getNumMessages() > 0)
        {
            // Add sub-title based on Screen spec. -- by BILLY 02Feb2001
            String tmpMsg = BXResources.getSysMsg(PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM,  theSessionState.getLanguageId())
            + "<br>" + "(Deal #: " + deal.getDealId() + ", " + "Scenario #: " + deal.getScenarioNumber() + ")";

            pm.setInfoMsg(tmpMsg);
            return pm;
        }
        return null;
    }


    // MIAction: Values: Apr 26/01 -- no longer used

    public void handleSaveScenario(int MIAction, boolean runBRules)
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        if (disableEditButton(pg) == true) return;
        SessionResourceKit srk = getSessionResourceKit();
        SysLogger logger = srk.getSysLogger();
        try
        {
            // set selected scenarion as recommended (if necessary) and navigate to deal resolution ...
            srk.beginTransaction();

            // For testing by BILLY
            //srk.getJdbcExecutor().startDBPerformanceCheck();
            //logger.debug("BILLY =====> Just start performance Checking !!");
      standardAdoptTxCopy(pg, true);

            Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

            // New requirement ==> Run Business Rule Verification for Verify Button only
            //  -- By BILLY 29Oct2001
            PassiveMessage pm = null;
            if (runBRules == true)
            {
                pm = validate(deal, pg, srk);
            }

            if (pm != null && pm.getNumMessages() > 0)
            {
                setActiveMessageToAlert(BXResources.getSysMsg(UW_SCENARIO_SAVED_WITH_MESSAGES, theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
                pm.setGenerate(true);
                theSessionState.setPasMessage(pm);
            }
            else setActiveMessageToAlert(BXResources.getSysMsg(UW_SCENARIO_SAVED_CLEAN, theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);

            // check change after MI in progress/in place
      //--BMO_MI_CR--start//
      // only if regular execution MI process indide BXP, otherwise skip messaging.
      if (isMIProcessOutsideBXP(pg) == false)
      {
        setMIIntegrityMsg(srk, pg, deal);
      }
      else if (isMIProcessOutsideBXP(pg) == true)
      {
        //// 1. Add the GE, CHMC, PolicyNumber, policy numbers update!!! (in order
        //// to propagate the value to db.
        //// We can't do it via the standard framework method for this screen since we can't
        //// supply the MIInsurerId to the property.
        //// 2. Exclude update of this field if the process is inside BXP (add the value to the
        //// skipUW.... hashtable.
        String miPolicyNbr = (String) getCurrNDPage().getDisplayFieldValue(pgUWorksheetViewBean.CHILD_TXUWMICERTIFICATE);

        ComboBox cbUWMIInsurer =(ComboBox) getCurrNDPage().getDisplayField(pgUWorksheetViewBean.CHILD_CBUWMIINSURER);
        String UWMIInsurer = cbUWMIInsurer.getValue().toString();

        //// temp debug:
        logger.debug("UWH@SaveScenario::MortgageInsFromDeal: " + deal.getMortgageInsurerId());
        logger.debug("UWH@SaveScenario::MortgageInsFromComboBox: " + UWMIInsurer);
        logger.debug("UWH@SaveScenario::MIPolicyNumber: " + miPolicyNbr);

        //// Set
        if(deal.getMortgageInsurerId() == Sc.MI_INSURER_CMHC) {
          //--> Set to CMHC Policy Number
          deal.setMIPolicyNumber(deal.getMIPolicyNumCMHC());
        } else if(deal.getMortgageInsurerId() == Sc.MI_INSURER_GE) {
          //--> Set to GE Policy Number
          deal.setMIPolicyNumber(deal.getMIPolicyNumGE());
        }

        deal.setMIPolicyNumber(miPolicyNbr);
        //// Synchronize GE and CHMC numbers if process is outside the BXP.
        if(deal.getMortgageInsurerId() == Sc.MI_INSURER_CMHC) {
          //// Set to CMHC Policy Number.
          deal.setMIPolicyNumCMHC(miPolicyNbr);
        } else if(deal.getMortgageInsurerId() == Sc.MI_INSURER_GE) {
          //// Or set to GE Policy Number otherwise.
          deal.setMIPolicyNumGE(miPolicyNbr);
        }
        //logger.debug("UWH@SaveScenario::MIPolicyNumber: " + miPolicyNbr);
        //logger.debug("UWH@SaveScenario::MIPolicyNumCHMC " + deal.getMIPolicyNumCMHC());

        deal.ejbStore();
      }  // end else if
      //--BMO_MI_CR--end//

      //logger.debug("BILLY =====> Just stop performance Checking :: Total Time in Millsec = " +
      //    srk.getJdbcExecutor().stopDBPerformanceCheck() + " Total SQL = " + srk.getJdbcExecutor().getDBCount());
      srk.commitTransaction();

      // Must do this before Copy the page entry -- otherwise the modify flag will be incorrect
      srk.setModified(false);
      pg.setModified(false);

      // navigate to self to ensure creation of tx copy on generation
      getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pg));
      navigateToNextPage(true);
      return;
      }
      catch(Exception e)
      {
              srk.cleanTransaction();
              logger.error("Exception @handleSaveScenario().");
              logger.error(e);
      }

      // an exception has occured - ensure we navigate away from page since we can't guar
      // presence of a tx copy on regeneration
      navigateAwayFromFromPage(true);
    }

  //--BMO_MI_CR--start//
    private void populateJSForMIProcessInsideBXP(ViewBean thePage)
    {

    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";
    HtmlDisplayFieldBase df;

    df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_CBUWMISTATUS);
    theExtraHtml = "disabled";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_CBUWMIINDICATOR);
    theExtraHtml = "onChange=\"populateValueTo(['hdMIIndicatorId']);"
                   + " SetMIPreQualifCertNumDisplay();\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_CBUWMIINSURER);
    theExtraHtml = "onChange=\"populateValueTo(['hdMIInsurer']);"
                   + " SetMIPolicyNoDisplay();"
                   + " hideRequestStandardService();" //--Release3.1
                   + " \"";
    df.setExtraHtml(theExtraHtml);

    //--Release3.1--begins
    //--by Hiro May 3, 2006
    df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_CBUWMITYPE);
    theExtraHtml = "onChange=\"hideRequestStandardService();\"";
    df.setExtraHtml(theExtraHtml);

    //--Release3.1--ends
  }

    private void populateJSIfMIProcessOutBXP(ViewBean thePage)
    {

    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";
        HtmlDisplayFieldBase df;

    // the OnChange string to be set on the fields
        // set MIInsurer disable if MIStatusUpdateFlag = N
    //// This logic is moved now to the JS directly on the page (SetMIPolicyNoDisplay() function)
    //// in order to maintain both MI processes: inside BXP (regular) and BMO.
        df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_CBUWMIINSURER);
    theExtraHtml = "onChange=\"populateValueTo(['hdMIInsurer']);"
                  + " SetMIPolicyNoDisplay();\"";
        df.setExtraHtml(theExtraHtml);

        // Populate MIPremium value to Hidden if changed
        df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_TXUWMIPREMIUM);
    theExtraHtml = "onBlur = \"if(isFieldInDecRange(0, 99999999999.99)&& isFieldDecimal(2)) populateValueTo(['hdMIPremium']);\"";
        df.setExtraHtml(theExtraHtml);

        df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_CBUWMIINDICATOR);
    theExtraHtml = "onChange=\"populateValueTo(['hdMIIndicatorId']);"
                   + " SetMIPreQualifCertNumDisplay();\"";
        df.setExtraHtml(theExtraHtml);

    //// The length of the next two fields depends on the Insurer.
    df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_TXUWMIPREQCERTNUM);
    theExtraHtml = "onBlur=\"populateValueTo(['hdMIPreQCertNum']);"
      + " SetMIPreQualifCertNumDisplay();\"";

    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_TXUWMIEXISTINGPOLICY);
        theExtraHtml = "onBlur=\"SetMIExistingPolicyDisplay();\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_TXUWMICERTIFICATE);
        theExtraHtml = "onBlur=\"SetMIPolicyNoDisplay();\"";
    df.setExtraHtml(theExtraHtml);

    df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_CBUWMISTATUS);
    theExtraHtml = "onChange=\"populateValueTo(['hdMIStatusId']);"
                   + " SetMIPreQualifCertNumDisplay();\"";

    df.setExtraHtml(theExtraHtml);
    }
  //--BMO_MI_CR--end//

    /**
     *
     *
     */
    private void handleAddDownpayment(Deal deal, CalcMonitor dcm)
        throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "DownPaymentSource", deal, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleAddDownpayment");
            logger.error(ex);
            throw new Exception("Add entity exception.");
        }

    }


    /**
     *
     *
     */
    @Override
    public void handleCustomActMessageOk(String[] args)
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
                CalcMonitor dcm = CalcMonitor.getMonitor(srk);
        SysLogger logger = srk.getSysLogger();

                //--CervusPhaseII--start--//
                ViewBean thePage = getCurrNDPage();

                if (args [ 0 ].equals(OVERRIDE_DEAL_LOCK_YES))
                {
                  logger.trace("UWH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
                  provideOverrideDealLockActivity(pg, srk, thePage);
                  return;
                }

                if (args [ 0 ].equals(OVERRIDE_DEAL_LOCK_NO))
                {
                  logger.trace("UWH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

                  theSessionState.setActMessage(null);
                  theSessionState.overrideDealLock(true);
                  handleCancelStandard(false);

                  return;
                }
                //--CervusPhaseII--end--//

        if (args [ 0 ].equals(NEW_SAVE_CURRENT_FROM_TOP))
        {
            ActiveMessageStateHelper state =(ActiveMessageStateHelper)((theSessionState.getActMessage()).getResponseObject());
            handleCreateNewScenario(1, state.i1, state.b1, pgUWorksheetViewBean.CHILD_CBSCENARIOPICKLISTTOP);
            return;
        }

        if (args [ 0 ].equals(NEW_SAVE_CURRENT_FROM_BOTTOM))
        {
            ActiveMessageStateHelper state =(ActiveMessageStateHelper)((theSessionState.getActMessage()).getResponseObject());
            handleCreateNewScenario(1, state.i1, state.b1, pgUWorksheetViewBean.CHILD_CBSCENARIOPICKLISTBOTTOM);
            return;
        }

        if (args [ 0 ].equals(NEW_DISCARD_CURRENT_FROM_TOP))
        {
            ActiveMessageStateHelper state =(ActiveMessageStateHelper)((theSessionState.getActMessage()).getResponseObject());
            handleCreateNewScenario(2, state.i1, state.b1, pgUWorksheetViewBean.CHILD_CBSCENARIOPICKLISTTOP);
            return;
        }

        if (args [ 0 ].equals(NEW_DISCARD_CURRENT_FROM_BOTTOM))
        {
            ActiveMessageStateHelper state =(ActiveMessageStateHelper)((theSessionState.getActMessage()).getResponseObject());
            handleCreateNewScenario(2, state.i1, state.b1, pgUWorksheetViewBean.CHILD_CBSCENARIOPICKLISTBOTTOM);
            return;
        }

        if (args [ 0 ].equals(SWITCH_SAVE_CURRENT))
        {
            ActiveMessageStateHelper state =(ActiveMessageStateHelper)((theSessionState.getActMessage()).getResponseObject());
            handleGoScenario(1, state.i1);
            return;
        }

        if (args [ 0 ].equals(SWITCH_DISCARD_CURRENT))
        {
            ActiveMessageStateHelper state =(ActiveMessageStateHelper)((theSessionState.getActMessage()).getResponseObject());
            handleGoScenario(2, state.i1);
            return;
        }

        if (args [ 0 ].equals(CONFIRMDELETE))
        {
            // continue after a delete confirmed (e.g. delete property)
            handleDelete((theSessionState.getActMessage()).getResponseObject());
            return;
        }

        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
        if (args [ 0 ].equals(AU_SUBMIT_YES))
        {
          logger.trace("DHC@handleCustomActMessageOk_AU_SUBMIT_YES: Submit to AU");
          theSessionState.setActMessage(null);
          if ((PropertiesCache.getInstance().getProperty
                          (theSessionState.getDealInstitutionId(),
                           COM_BASIS100_FXP_CLIENTID_GECF, "N")).equals("Y")){
            requestGeMoneyDoc();
          }

          handleExit();
          return;
        }
        if (args [ 0 ].equals(AU_SUBMIT_NO))
        {
          logger.trace("DHC@handleCustomActMessageOk_AU_SUBMIT_NO: Submit to AU");
          theSessionState.setActMessage(null);

          handleExit();
          return;
        }
        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------
        
        
        //4.3GR, if no action was found, go to super method
        super.handleCustomActMessageOk(args);
    }

    /**
     * @version 1.1, FXP24101, Jan 23, 2009, deleted unnesessary code
     */
    public void handleCreateNewScenario(int doCurr, int sourceCopyId, boolean sourceIsGold, String scenarioPickListName)
    {
        PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        // first if doCurr == 0 we retrieve the selected scenario (copy) id
		if (doCurr == 0) {
			ComboBox comboScenarios = (ComboBox) (getCurrNDPage()
					.getDisplayField(scenarioPickListName));
			sourceCopyId = new Integer(comboScenarios.getValue().toString())
					.intValue();

			// determine if is the gold copy
			try {
				Deal deal = DBA.getDeal(srk, pe.getPageDealId(), sourceCopyId);
				sourceIsGold = deal.getCopyType() != null
						&& deal.getCopyType().equals("G");
			} catch (Exception e) {
				sourceIsGold = false;
			}
		}


        // arg: 0 - don't know what to do with current - prompt to user
        // 1 - save current (then create new)
        //  2 - discard current (then create new
        if (doCurr == 0 && pe.isPageDataModified())
        {
            ActiveMessageStateHelper state = new ActiveMessageStateHelper();
            state.i1 = sourceCopyId;
            state.b1 = sourceIsGold;

            // setActiveMessageToAlert(Sc.UW_CREATE_SCENARIO_SAVE_CURRENT, ActiveMsgFactory.ISCUSTOMDIALOG2, "NEW_SAVE_CURRENT", "NEW_DISCARD_CURRENT");
            if (scenarioPickListName.equals(pgUWorksheetViewBean.CHILD_CBSCENARIOPICKLISTTOP))
            {
                // setActiveMessageToAlert(Sc.UW_CREATE_SCENARIO_SAVE_CURRENT, ActiveMsgFactory.ISCUSTOMDIALOG2, "NEW_SAVE_CURRENT_FROM_TOP", "NEW_DISCARD_CURRENT");
                setActiveMessageToAlert(BXResources.getSysMsg(UW_CREATE_SCENARIO_DISCARD_CURRENT, theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMDIALOG, NEW_DISCARD_CURRENT_FROM_TOP);
                //logger.debug("UWH@handleCreateNewScenario::From PickListTop mode");
            }
            else if (scenarioPickListName.equals(pgUWorksheetViewBean.CHILD_CBSCENARIOPICKLISTBOTTOM))
            {
                // setActiveMessageToAlert(Sc.UW_CREATE_SCENARIO_SAVE_CURRENT, ActiveMsgFactory.ISCUSTOMDIALOG2, "NEW_SAVE_CURRENT_FROM_BOTTOM", "NEW_DISCARD_CURRENT");
                setActiveMessageToAlert(BXResources.getSysMsg(UW_CREATE_SCENARIO_DISCARD_CURRENT, theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMDIALOG, NEW_DISCARD_CURRENT_FROM_BOTTOM);
            }

            (theSessionState.getActMessage()).setResponseObject(state);
            return;
        }

        try
        {
            srk.beginTransaction();
            if (doCurr == 1)
            {
                standardAdoptTxCopy(pe, true);
                theSessionState.setDec(null);
            }
            else
                // drop transactional copy (if one) - covers case where create new
                // scenario event arrives and data unchanged (no dialog case)
            {
                standardDropTxCopy(pe, false);
                /*
          DealEditControl dec = theSession.getDec();
          if (dec != null)
          {
              dec.dropCopy(getSessionResourceKit());
              pe.setPageDealCID(dec.getCID());
              theSession.setDec(null);
              pe.setTxCopyLevel(dec.getNumTxCopies());
          }
                 */
            }
            MasterDeal md = new MasterDeal(srk, null);
            md = md.findByPrimaryKey(new MasterDealPK(pe.getPageDealId()));

            //logger.debug("UWH@handleCreateNewScenario::Before make new scenario");

            // make new scenario
            int scenarioCid = md.copy(sourceCopyId);
            Deal dealScenario = new Deal(srk, null);
            dealScenario = dealScenario.findByPrimaryKey(new DealPK(pe.getPageDealId(), scenarioCid));
            dealScenario.setCopyType("S");
            dealScenario.setScenarioLocked("N");
            dealScenario.setScenarioRecommended("N");
            dealScenario.setScenarioDescription(getPageStateHandler().createScenarioDescription(theSessionState, dealScenario, sourceIsGold));
            dealScenario.setScenarioNumber(md.nextScenarioNumber());
            dealScenario.ejbStore();
            md.ejbStore();

            //logger.debug("UWH@handleCreateNewScenario::After ejbStore");

            // set new scenario as focus scenario
            pe.setPageDealCID(scenarioCid);

            // Must do this before Copy the page entry -- otherwise the modify flag will be incorrect
            pe.setModified(false);

            // ensure condition will be rebuild for new scenario
            pe.setPageCondition4(false);

            // ensure treated like initial display condition
            pe.setPageCondition2(false);

            //getSavedPages().setNextPage(pe); // cause navigation to self!!
            // navigate to self to ensure creation of tx copy on generation
            getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pe));
            srk.commitTransaction();
            srk.setModified(false);

            //logger.debug("UWH@handleCreateNewScenario::Before navigateToNextPage");
            navigateToNextPage(true);
            //logger.debug("UWH@handleCreateNewScenario::Before navigateToNextPage");

            return;
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleCreateNewScenario.");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    // assume Deal Resolution is always 'next' page in page links

    public void handleNextTaskPage()
    {
        handleResolveDeal(0);

    }


    /**
     *
     *
     */
    public void handleGoScenario(int doCurr, int selectedScenarioCopyId)
    {
        PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

        int dealId = pe.getPageDealId();

        Integer cspDealId = new Integer(dealId);

        // first if doCurr == 0 we retrieve the selected scenario (copy) id
        if (doCurr == 0)
        {
            ComboBox comboScenarios =(ComboBox)(getCurrNDPage().getDisplayField(pgUWorksheetViewBean.CHILD_CBSCENARIOS));

      selectedScenarioCopyId = (new Integer((comboScenarios.getValue()).toString())).intValue();
      String selectedLabel = comboScenarios.getOptions().getValueLabel((comboScenarios.getValue()).toString());

      //logger.debug("UWH@handleGoScenario::selectedScenarioCopyId: " + selectedScenarioCopyId);
      //logger.debug("UWH@handleGoScenario::selectedLabel: " + selectedLabel);

            Integer cspDealCPId = new Integer(selectedScenarioCopyId);
        }

        // arg: 0 - don't know what to do with current - prompt to user
        //  1 - save current (then create new)
        //  2 - discard current (then create new
        if (doCurr == 0 && pe.isPageDataModified())
        {
            ActiveMessageStateHelper state = new ActiveMessageStateHelper();
            state.i1 = selectedScenarioCopyId;

            // setActiveMessageToAlert(Sc.UW_SWITCH_SCENARIO_SAVE_CURRENT, ActiveMsgFactory.ISCUSTOMDIALOG2, "SWITCH_SAVE_CURRENT", "SWITCH_DISCARD_CURRENT");
            setActiveMessageToAlert(BXResources.getSysMsg(UW_SWITCH_SCENARIO_DISCARD_CURRENT, theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMDIALOG, SWITCH_DISCARD_CURRENT);
            (theSessionState.getActMessage()).setResponseObject(state);
            return;
        }

        SessionResourceKit srk = getSessionResourceKit();

        try
        {
            srk.beginTransaction();
            if (doCurr == 1)
            {
                standardAdoptTxCopy(pe, true);
                theSessionState.setDec(null);
            }
            else
            {
                // drop transactional copy (if one) - covers case where switch
                // scenario event arrives and data unchanged (no dialog case)
                standardDropTxCopy(pe, false);
            }

            // point to selected scenario and navigate to self
            pe.setPageDealCID(selectedScenarioCopyId);
            pe.setModified(false);

            // ensure condition will be rebuild for new scenario
            pe.setPageCondition4(false);

            // ensure treated like initial display condition
            pe.setPageCondition2(false);
            getSavedPages().setNextPage(PageEntry.getPageEntryAsCopy(pe));

            // cause navigation to self!!
            srk.commitTransaction();
            srk.setModified(false);
            navigateToNextPage(true);
            return;
        }
        catch(Exception ex)
        {
            srk.cleanTransaction();
            logger.error("Exception occurred @handleGoScenario.");
            logger.error(ex);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    public void handleConditionsReview(boolean isCheckRules, boolean isResolveButton)
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        SysLogger logger = srk.getSysLogger();

        try
        {
            // check for locked scenario - rebuild conditions only for unlocked scenario
            boolean editable = false;
            Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

            boolean isDisplayResolveButton = isResolveButton;

            // If Check BusinessRule required -- Billy 12Nov2001
            PassiveMessage pm = null;
            if (isCheckRules == true)
            {
                if (deal.getScenarioLocked() != null && deal.getScenarioLocked().equals("N"))
                {
                    logger.trace("--T--> @UWorksheetHandler.handleConditionsReview: validate resolution rules");

                    // run validation (except for external approval related)
                    pm = validate(deal, pg, srk);

                    // Display the Rule message if any
                    if (pm != null && pm.getNumMessages() > 0)
                    {
                        pm.setGenerate(true);
                        theSessionState.setPasMessage(pm);
                        if (pm.getCritical() == true)
                        {
                            // Not display Resolve Deal Button if Critical Error on Rule Checking
                            isDisplayResolveButton = false;
                        }
                    }
                }
            }

            // if Check Rules
            if (deal.getScenarioLocked() == null || deal.getScenarioLocked().equals("N"))
            {
                editable = true;
                srk.beginTransaction();
                ConditionHandler handler = new ConditionHandler(srk);
                logger.debug("Before Calling buildSystemGeneratedDocumentTracking");
                handler.buildSystemGeneratedDocumentTracking(new DealPK(pg.getPageDealId(),
                                                                        pg.getPageDealCID()));
                logger.debug("After Calling buildSystemGeneratedDocumentTracking");
                srk.commitTransaction();
            }

            // to conditions review page
            PageEntry conditionsPg = setupSubPagePageEntry(pg, Mc.PGNM_CONDITIONS_REVIEW);
            conditionsPg.setEditable(editable);
            conditionsPg.setPageCondition3(true);

            // no conditions building on conditions page
            // set if displaying Resolve Deal Button - Billy 12Nov2001
            conditionsPg.setPageCondition5(isDisplayResolveButton);
            getSavedPages().setNextPage(conditionsPg);
            navigateToNextPage(true);
            pg.setPageCondition4(true);

            // conditions built
            return;
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleConditionsReview().");
            logger.error(e);
            setStandardFailMessage();
        }

    }


    /**
     *
     *
     */
    public void handleAppraisalReview()
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        SessionResourceKit srk = getSessionResourceKit();

        SysLogger logger = srk.getSysLogger();

        try
        {
            srk.beginTransaction();

            Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

            // navigate to Appraisal Review
            AppraisalReviewFactory factory = new AppraisalReviewFactory(deal);
            PageEntry appraisalPg = setupSubPagePageEntry(pg, factory.getViewBeanId());
            getSavedPages().setNextPage(appraisalPg);
            navigateToNextPage();
            srk.commitTransaction();
            return;
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleAppraisalReview().");
            logger.error(e);
            setStandardFailMessage();
        }

    }

    // doCurr - Apr 26/01 - no longer used
    public void handleMIReview(int doCurr)
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        SysLogger logger = srk.getSysLogger();
        try
        {
            srk.beginTransaction();

            // adopt transactional copy unconditionally - ensures we enter MI Review screen with saved
            // (non-discardable) scenario data
            standardAdoptTxCopy(pg, true);
            Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

            // check change after MI in progress/in place
      //--BMO_MI_CR--start//
      // Only if regular execution MI process indide BXP, otherwise skip messaging.
      if (isMIProcessOutsideBXP(pg) == false)
      {
              setMIIntegrityMsg(srk, pg, deal);
      }
      //--BMO_MI_CR--end//

            PageEntry miPg = setupSubPagePageEntry(pg, Mc.PGNM_MORTGAGE_INSURANCE);

            // New Requirement -- propulate ScenarioNumber when Requesting MI
            //    Modified by BILLY 29May2001
            miPg.setPageCondition1(true);
            getSavedPages().setNextPage(miPg);
            navigateToNextPage();
            srk.commitTransaction();
            return;
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleMIReview().");
            logger.error(e);
            setStandardFailMessage();
        }

    }

    /**
    private void handleAddLDPremiums(Borrower borrower, CalcMonitor dcm)
        throws Exception
    {
        try
        {
            DBA.createEntity(this.getSessionResourceKit(), "LifeDisabilityPremiums", borrower, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @LifeDisabilityPremiums");
            logger.error(ex);
            throw new Exception("Add entity handler exception");
        }
    }
  **/
  //--DJ_LDI_CR--end--//
  //--DJ_CR_203.1--start--//
    private void populateDefaultValues(ViewBean thePage)
    {

    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";
        HtmlDisplayFieldBase df;

        // Trigger to set MI Premium display if MIType changed
        df = (HtmlDisplayFieldBase) thePage.getDisplayField(pgUWorksheetViewBean.CHILD_RBPROPRIETAIREPLUS);
        theExtraHtml = "onclick=\"SetLineOfCreditDisplay();\"";

    df.setExtraHtml(theExtraHtml);
  }
  //--DJ_CR_203.1--end--//

    /**
     *
     *
     */
    public void handleCreditReport()
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        SysLogger logger = srk.getSysLogger();
        try
        {
            srk.beginTransaction();

            // navigate to Credit Report
            PageEntry creditPg = setupSubPagePageEntry(pg, Mc.PGNM_CREDIT_REVIEW_ID);
            getSavedPages().setNextPage(creditPg);
            navigateToNextPage();
            srk.commitTransaction();
            return;
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception @handleCreditReport().");
            logger.error(e);
            setStandardFailMessage();
        }
    }


    /**
     *
     *
     */
    public String[] getDealKeys()
    {
        String [ ] dealkeys =
        {
        		pgUWorksheetViewBean.CHILD_HDDEALID, pgUWorksheetViewBean.CHILD_HDDEALCOPYID   }
        ;

        return dealkeys;

    }


    /**
     * <p>SaveEntity</p>
     * <p>save data on Page</p>
     *
     * @param webPage ViewBean
     *
     * @version 61 July 29/2008: MCM Implementation Team
     * added saveCompInfoCheckBox before calc
     */
    public void SaveEntity(ViewBean webPage)
        throws Exception
    {
        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

        String [ ] dealkeys = getDealKeys();

        /***** FXP23234 start *****/
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        HashSet skipUWElements = pg.getPageExcludePageVars();
        ViewBean thePage = getCurrNDPage();
        String autoCalcCommit = (String) thePage
               .getDisplayFieldValue(pgUWorksheetViewBean.CHILD_RBAUTOCALCULATECOMMITMENTDATE);
        if ("Y".equalsIgnoreCase(autoCalcCommit)) {
            skipUWElements
                    .add(new String(
                                pgUWorksheetViewBean.CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH));
            skipUWElements
                    .add(new String(
                                pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY));
            skipUWElements
                   .add(new String(
                            pgUWorksheetViewBean.CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR));
        }
        /*****  FXP23234  end *****/
        
        //Qualify Rate
        String qualifyFlag = (String) thePage
            .getDisplayFieldValue(pgUWorksheetViewBean.CHILD_CHQUALIFYRATEOVERRIDE);
        
        String qualifyRateFlag =  (String) thePage
            .getDisplayFieldValue(pgUWorksheetViewBean.CHILD_CHQUALIFYRATEOVERRIDERATE);
        
        Object qualifyRateValue =  thePage
                .getDisplayFieldValue(pgUWorksheetViewBean.CHILD_TXQUALIFYRATE);
  
        if (qualifyFlag != null && "N".equalsIgnoreCase(qualifyFlag))
        	skipUWElements.add(new String(
                    pgUWorksheetViewBean.CHILD_CBQUALIFYPRODUCTTYPE));

       	if ((qualifyRateFlag != null && "N".equalsIgnoreCase(qualifyRateFlag)) ||qualifyRateValue == null) 
       		skipUWElements.add(new String(
                   pgUWorksheetViewBean.CHILD_TXQUALIFYRATE));
        //FXP33510  v5.0_XpS - Dual Qualifying Rate - Qualifying Product was changed with different LTV - begin
        if (qualifyRateValue != null && "Y".equalsIgnoreCase(qualifyRateFlag))
        {
        	skipUWElements.remove(new String(
                    pgUWorksheetViewBean.CHILD_CBQUALIFYPRODUCTTYPE));
        	skipUWElements.remove(new String(
                    pgUWorksheetViewBean.CHILD_TXQUALIFYRATE));
        }
        //FXP33510  v5.0_XpS - Dual Qualifying Rate - Qualifying Product was changed with different LTV - end 
        
       	       	logger.debug("UWH@SkipElementSize: " + skipUWElements.size());
        pg.setPageExcludePageVars(skipUWElements);
        
        try
        {
            handleUpdateDeal(UW_MAIN_DEAL_PROP_FILE_NAME, dealkeys, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @updateDeal of UWHandler " + ex.toString());
            throw new Exception("Exception UWH@SaveEntity");
        }

        try
        {
            String [ ] escrowkeys =
            {
                "RepeatedEscrowDetails/hdEscrowPaymentId", "RepeatedEscrowDetails/hdEscrowPaymentCopyId"
            };
            handleUpdateEscrowPay(UW_ESCROW_PROP_FILE_NAME, escrowkeys, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @updateEscrow of UWHandler " + ex.toString());
            throw new Exception("Exception UWH@SaveEntity");
        }

        try
        {
            String [ ] downpmntkeys =
            {
                "RepeatedUWDownPayment/hdUWDownPaymentSourceId", "RepeatedUWDownPayment/hdUWDownPaymentCopyId"
            };
            handleUpdateDownPay(UW_DOWNPAYMENT_PROP_FILE_NAME, downpmntkeys, dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @updateEscrow of UWHandler " + ex.toString());
            throw new Exception("Exception UWH@SaveEntity");
        }

        // MCM XS 2.60
        saveCompInfoCheckBox(pgUWorksheetViewBean.CHILD_COMPONENTINFOPAGELET, dcm);

        try
        {
            doCalculation(dcm);
        }
        catch(Exception e)
        {
            logger.error("Exception @doCalculation:" + e);		//#DG702
            throw new Exception("Exception @SaveEntity", e);
        }

        saveApplicantPrimaryFlag();
        savePropertyPrimaryFlag();

        // if changes have occurred reset 'default conditions built' flag
        if (getSessionResourceKit().getModified() == true)
        {
            ///logger.debug("**********UW:SaveEntity :: modified --> calling for conditions again!");
            theSessionState.getCurrentPage().setPageCondition4(false);
        }
        else
        {
            ///logger.debug("**********UW:SaveEntity :: not modified!");
        }
    }

  //
    // Implementation
    //

    private void saveApplicantPrimaryFlag()
    {
    	try
    	{
    		int rowcnt = getTotalNoOfRowsForThisPageVar("RepeatApplicantFinancialDetails/hdFinancialApplicantId");
    		int applicantid = 0;
    		int copyid = 0;
    		String primaryflag = "N";
    		if (rowcnt < 0) return;
    		for(int i = 0; i < rowcnt; i ++)
    		{
    			TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild("RepeatApplicantFinancialDetails");

    			tiledView.setTileIndex(i);
    			applicantid = parseStringToInt("" + (tiledView.getDisplayFieldValue(pgUWorksheetRepeatApplicantFinancialDetailsTiledView.CHILD_HDFINANCIALAPPLICANTID)).toString());
    			copyid = parseStringToInt("" + (tiledView.getDisplayFieldValue(pgUWorksheetRepeatApplicantFinancialDetailsTiledView.CHILD_HDFINANCIALAPPLICANCOPYID)).toString());
    			primaryflag = "" + (tiledView.getDisplayFieldValue(pgUWorksheetRepeatApplicantFinancialDetailsTiledView.CHILD_CBUWPRIMARYAPPLICANT)).toString();


    			//4.4 SimpleEntityCache - stopped using direct update sql
    			Borrower borrower = new Borrower(srk, null,applicantid, copyid);
    			borrower.setPrimaryBorrowerFlag(primaryflag);
    			borrower.ejbStore();

    		}
    	}
    	catch(Exception e)
    	{
    		logger.error("Exception @UWorksheetHandler.saveApplicantPrimaryFlag - ignored");
    		logger.error(e);
    	}

    }

    private void savePropertyPrimaryFlag()
    {
    	try
    	{
    		int rowcnt = getTotalNoOfRowsForThisPageVar("RepeatPropertyInformation/hdPropertyId");
    		int propertyid = 0;
    		int copyid = 0;
    		String primaryflag = "N";
    		if (rowcnt < 0) return;
    		for(int i = 0;  i < rowcnt; i ++)
    		{
    			TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild("RepeatPropertyInformation");

    			tiledView.setTileIndex(i);
    			propertyid = parseStringToInt("" + (tiledView.getDisplayFieldValue(pgUWorksheetRepeatPropertyInformationTiledView.CHILD_HDPROPERTYID)).toString());
    			copyid = parseStringToInt("" + (tiledView.getDisplayFieldValue(pgUWorksheetRepeatPropertyInformationTiledView.CHILD_HDPROPERTYCOPYID)).toString());
    			primaryflag = "" + (tiledView.getDisplayFieldValue(pgUWorksheetRepeatPropertyInformationTiledView.CHILD_CBUWPRIMARYPROPERTY)).toString();

    			//4.4 SimpleEntityCache - stopped using direct update sql
    			Property property = new Property(srk, null, propertyid, copyid);
    			property.setPrimaryPropertyFlag(primaryflag);
    			property.ejbStore();

    		}
    	}
    	catch(Exception e)
    	{
    		logger.error("Exception @UWorksheetHandler.savePropertyPrimaryFlag - ignored");
    		logger.error(e);
    	}
    }


    // this method overrides the one from the base class (PHC). Font size must
    // be small on this page.

    protected void keepComboBoxValue(String comboName)
    {
        try
        {
            logger.trace("@UWHandler ---- customizeComboBoxes for population");
            ComboBox cb =(ComboBox) getCurrNDPage().getDisplayField(comboName);
            String onChangeCombo = new String("onChange=\"keepValue('" + comboName + "')\"");
            cb.setExtraHtml(onChangeCombo);
        }
        catch(Exception e)
        {
            setStandardFailMessage();
            logger.error("Exception UWH@keepComboBoxValue: Problem encountered in combobox customization. " + comboName+":" + e);		//#DG702
        }
    }


    /**
     *
     *
     */
    private void displayMIPremiumTitleConditional(PageEntry pg, ViewBean thePage)
    {
    //String fieldTitle = "MI Premium";
		    String fieldTitle = BXResources.getGenericMsg(MI_PREMIUM_LABEL,  theSessionState.getLanguageId());

        //#DG702 int dealId = pg.getPageDealId();
        //#DG702 Integer cspDealId = new Integer(dealId);

        // suppress
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STINCLUDEMIPREMIUMSTART, new String(supresTagStart));
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STMIPREMIUMTITLE, new String(fieldTitle));
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STINCLUDEMIPREMIUMEND, new String(supresTagEnd));
        return;
    }


    /**
     *
     *
     */
    private void displayMIPremiumTitle(PageEntry pg, ViewBean thePage)
    {
    //// The field label should be translated based on languageId.
    //String fieldTitle = "MI Premium";
    String fieldTitle = BXResources.getGenericMsg(MI_PREMIUM_LABEL,  theSessionState.getLanguageId());

        //#DG702 int dealId = pg.getPageDealId();
        //#DG702 Integer cspDealId = new Integer(dealId);

        // display
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STMIPREMIUMTITLE, new String(fieldTitle));
        return;
    }


    /**
     *
     *
     */
    private void displayMIPremiumValueConditional(PageEntry pg, ViewBean thePage)
    {
        //#DG702 int dealId = pg.getPageDealId();
        //#DG702 Integer cspDealId = new Integer(dealId);

        // suppress
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STINCLUDEMIPREMIUMVALUESTART, new String(supresTagStart));
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STINCLUDEMIPREMIUMVALUEEND, new String(supresTagEnd));
        return;

    }


    /**
     *
     *
     */
    protected void customizeTextBox(String tbName)
    {
        try
        {
      TextField textbox =(TextField) getCurrNDPage().getDisplayField(tbName);

            // 1. Customize colors, borders, etc.,
            String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"border-bottom: hidden d1ebff 0\"" + " style=\"border-left: hidden d1ebff 0\"" + " style=\"border-right: hidden d1ebff 0\"" + " style=\"border-top: hidden d1ebff 0\"" + " style=\"font-weight: bold\"" + " style=\"font-size: xx-small\"");

            // 2. Disable edition
            String disableEdition = new String(" disabled");
            textbox.setExtraHtml(changeColorStyle + disableEdition);
        }
        catch(Exception e)
        {
            setStandardFailMessage();
            logger.error("Exception UWHandler@CustomizeTextBox: Problem encountered in combobox customization:" + e);		//#DG702
        }

    }

    //--Release3.1--begins
    //--by Hiro May 2, 2006
    protected void disableDisplayField(String tbName)
    {
      try
      {
        HtmlDisplayFieldBase htmlDispField = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField(tbName);

        // Add disable edition
        htmlDispField.setExtraHtml(new String(" disabled "));

      } catch (Exception e)
      {
        setStandardFailMessage();
        logger.error("Exception UWHandler@disableDisplayField: Problem encountered in html display field customization:" + e);		//#DG702
      }

    }
    //--Release3.1--ends

    /**
     *
     *
     */
    protected void disableControl(String tbName)
    {
        try
        {
            logger.trace("@---T---->UWH ---- disableControl for calculation");
            TextField textbox =(TextField) getCurrNDPage().getDisplayField(tbName);

            //  Disable edition
            String disableEdition = new String(" disabled");
            textbox.setExtraHtml(disableEdition);
        }
        catch(Exception e)
        {
            setStandardFailMessage();
            logger.error("Exception UWH@DisableControl: Problem encountered in disable control:" + e);		//#DG702
        }
    }

    /**
     *
     *
     */
    ////public void handleLenderProfileIdPassToHtml(ViewBean pg, CSpDisplayEvent event, String staticFieldName, String doName, String dataFieldName)
    public void handleLenderProfileIdPassToHtml(ViewBean pg, ChildDisplayEvent event)
    {
    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    //logger.debug("UWH@handleLenderProfileIdPassToHtml::currPg: " + currPg);
    //logger.debug("UWH@handleLenderProfileIdPassToHtml::ViewBean: " + pg);

    doUWDealSelectMainModel theModel = (doUWDealSelectMainModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doUWDealSelectMainModel.class);

    int lenderProfileId = 0;

    Object fieldValue = theModel.getDfLenderProfileId();
    if(fieldValue != null)
    {
      lenderProfileId = new Integer(fieldValue.toString()).intValue();
      //logger.debug("UWH@handleLenderProfileIdPassToHtml::OnBeforeRowDisplayEvent_dfValueFromModel: " + new Integer(fieldValue.toString()).intValue());
    }

        String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

    //logger.debug("UWH@handleLenderProfileIdPassToHtml::fieldName: " + fieldName);
    //logger.debug("UWH@handleLenderProfileIdPassToHtml::fld: " + fld);

    fld.setValue((new Integer(lenderProfileId)).toString());
    }


    /**
     *
     *
     */
    public void handleProductDealIdPassToHtml(ViewBean pg, ChildDisplayEvent event)
    {
    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    doUWDealSelectMainModel theModel = (doUWDealSelectMainModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doUWDealSelectMainModel.class);

    Object fieldValue = theModel.getDfMTGProductId();

    int lenderProductId = 0;

    if(fieldValue != null)
    {
      lenderProductId = new Integer(fieldValue.toString()).intValue();
      //logger.debug("UWH@handleProductDealIdPassToHtml::ProductId: " + new Integer(fieldValue.toString()).intValue());
    }

        String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

    fld.setValue((new Integer(lenderProductId)).toString());
    }


    /**
     *
     *
     */
  public int getMIIndicator()
    {
    doUWDealSelectMainModel theDO = (doUWDealSelectMainModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doUWDealSelectMainModel.class);

        int indx = 0;

    Object mIndicator = theDO.getDfMIIndicatorId();
    if(mIndicator != null)
    {
      indx = new Integer(mIndicator.toString()).intValue();
      //logger.debug("UWH@getMIInsurerIndex:: " + new Integer(mIndicator.toString()).intValue());
    }

    return indx;
  }

  /**
     *
     *
     */
  public int getMITypeIndex()
    {
        doUWMITypeModel theDO = (doUWMITypeModel)(RequestManager.getRequestContext().getModelManager().getModel(doUWMITypeModel.class));

    int indx = 0;
    try
    {
      Object mIType = null;
      if((theDO.getSize() > 0) && (mIType = theDO.getDfMITypeId()) != null)
        indx = new Integer(mIType.toString()).intValue();
    }
    catch (ModelControlException mce)
    {
      logger.warning("UWH@getMITypeIndex::MCEException: " + mce);
      indx = 0;
    }

    return indx;
  }

  /**
     *
     *
     */
  public int getMInsurerIndex()
    {
        doUWMIInsurerModel theDO = (doUWMIInsurerModel)(RequestManager.getRequestContext().getModelManager().getModel(doUWMIInsurerModel.class));

        int indx = 0;
    try
    {
      Object mInsurer = null;

      if(theDO.getSize() > 0 && (mInsurer = theDO.getDfMIInsurerId()) != null)
        indx = new Integer(mInsurer.toString()).intValue();
    }
    catch (ModelControlException mce)
    {
      logger.warning("UWH@getMIInsurerIndex::MCEException: " + mce);
      indx = 0;
    }

    return indx;
  }

  //--BMO_MI_CR--start//
  /**
     *
     *
     */
  public int getMIStatusIndex()
    {
        doUWMIStatusModel theDO = (doUWMIStatusModel)(RequestManager.getRequestContext().getModelManager().getModel(doUWMIStatusModel.class));

        int indx = 0;
    try
    {
      Object mStatus = null;

      if(theDO.getSize() > 0 && (mStatus = theDO.getDfMIStatusId()) != null)
        indx = new Integer(mStatus.toString()).intValue();
    }
    catch (ModelControlException mce)
    {
      logger.warning("UWH@getMIStatusIndex::MCEException: " + mce);
      indx = 0;
    }

    return indx;
  }
  //--BMO_MI_CR--end//

  //--Release3.1--begins
  //--by Hiro Apr 25, 2006
  public int getProgressAdvanceTypeIndex()
  {
    doUWProgressAdvanceTypeModel theDO = (doUWProgressAdvanceTypeModel) (RequestManager
        .getRequestContext().getModelManager()
        .getModel(doUWProgressAdvanceTypeModel.class));

    int indx = 0;
    try
    {
      Object progressAdvanceType = null;

      if (theDO.getSize() > 0
          && (progressAdvanceType = theDO
              .getDfProgressAdvanceTypeId()) != null)
        indx = new Integer(progressAdvanceType.toString()).intValue();
    } catch (ModelControlException mce)
    {
      logger.warning("UWH@getProgressAdvanceTypeIndex::MCEException: " + mce);
      indx = 0;
    }

    return indx;
  }

  public int getLOCRepaymentTypeIndex()
  {
    doUWLOCRepaymentTypeModel theDO = (doUWLOCRepaymentTypeModel) (RequestManager
        .getRequestContext().getModelManager()
        .getModel(doUWLOCRepaymentTypeModel.class));

    int indx = 0;
    try
    {
      Object locRepaymentType = null;

      if (theDO.getSize() > 0
          && (locRepaymentType = theDO
              .getDfLOCRepaymentTypeId()) != null)
        indx = new Integer(locRepaymentType.toString()).intValue();
    } catch (ModelControlException mce)
    {
      logger.warning("UWH@getLOCRepaymentTypeIndex::MCEException: " + mce);
      indx = 0;
    }

    return indx;
  }
  // --Release3.1--ends
    /**
     *
     * getDfBorrowerNetWorth()
     */
    public double countNetWorthTotalFromDo()
    {
        SessionResourceKit srk = getSessionResourceKit();

        SysLogger logger = srk.getSysLogger();

    // determine net worth by using (already executed) financial details dfata object

        doUWApplicantFinancialDetailsModel doUWappFinancial =
                          (doUWApplicantFinancialDetailsModel) RequestManager.getRequestContext().getModelManager().getModel(doUWApplicantFinancialDetailsModel.class);


        double netSum = 0.0;

    try
    {
      doUWappFinancial.beforeFirst();

      while (doUWappFinancial.next())
      {
        Object oNetSum = doUWappFinancial.getDfBorrowerNetWorth();
        if (oNetSum != null)
          netSum += new Double(oNetSum.toString()).doubleValue();
      }

      // restore model to before first state - in case no already used in presentation generation
      doUWappFinancial.beforeFirst();
    }
    catch (ModelControlException mce)
    {
      logger.error("UWH@.countNetWorthTotalFromDo:: Unexpected model control exception, mce = " + mce);
      netSum = 0.0;
    }

    //logger.debug("UWH@countNetWorthTotalFromDo::NetSum: " + netSum);
    return netSum;
  }

    /**
     *
   * WARNING BJH - Nov 18, 2002 :: Dopes comment still apply?
   *
     * BXTODO: Must be synchronized with the identical method from the DealEntryHandler. The
   * temporarily solution is the 'clone' method to the DealEntry one.
   * Both must be joined into one API on the PHC level.
   * @version 60
   * Date: July 14/2008 <br>
   * Author: MCM Implementation Team <br>
   * Change:  <br>
   * Bug Fix: Added null check for the tmpRateDate in handleRateTimeStampPassToHtml() method.
   */

    public void handleRateTimeStampPassToHtml(ViewBean pg, ChildDisplayEvent event)
    {
    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    doUWDealSelectMainModel theModel = (doUWDealSelectMainModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doUWDealSelectMainModel.class);

        GregorianCalendar grRateDate = null;

    Object oRateDate = theModel.getDfRateDate();

    if(oRateDate != null)
    {
//        logger.debug("UWH@handleRateTimeStampPassToHtml::RateDate: " + oRateDate.toString());
//        logger.debug("UWH@handleRateTimeStampPassToHtml::RateDateJATOTypeConvSQLTimestamp: " +
//                      com.iplanet.jato.util.TypeConverter.asType(com.iplanet.jato.util.TypeConverter.TYPE_SQL_DATE, oRateDate));
//
//        logger.debug("UWH@handleRateTimeStampPassToHtml::RateDateJATOTypeConvString: " +
//                com.iplanet.jato.util.TypeConverter.asType(com.iplanet.jato.util.TypeConverter.TYPE_STRING, oRateDate));

      SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd hh:mm:ss.SSS" );

      java.util.Date tmpRateDate = formatter.parse( oRateDate.toString(), new ParsePosition(0));

//      logger.debug( "UWH@handleRateTimeStampPassToHtml::Parsed time: " + ((tmpRateDate == null) ? "NULL" : tmpRateDate.toString()));
      grRateDate = new GregorianCalendar();
      /**************MCM Impl Team Bug Fix :   Added Null check Starts*********************/
      if(tmpRateDate!=null)
      {
          grRateDate.setTime(tmpRateDate);
      }
      /**************MCM Impl Team Bug Fix :   Added Null check Ends*********************/
//      logger.debug( "UWH@handleRateTimeStampPassToHtml::Final Result: " + grRateDate.toString() );
    }
    else
    {
      logger.debug( "UWH@handleRateTimeStampPassToHtml:: Rate Date Object mode");
    }

        String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

        if (grRateDate != null)
        {
            fld.setValue(new String(convertDateToServletFormat(grRateDate)));
        }
        else
        {
            fld.setValue(new String(""));
        }

    }


    /**
     *
     * BXTODO: Obsolete method. JATO takes care on it. Test more and eliminate
   * during the final cleaning of the BX framework classes.
     */
    protected void fullAccessToComboBox(String cbName)
    {
        try
        {
            logger.trace("@---T---->DealEntryHandler ---- full access to the combobox for MI part: " + cbName);
            ComboBox combobox =(ComboBox) getCurrNDPage().getDisplayField(cbName);

            String defaultColorStyle = new String("style=\"background-color:ffffff\"");

            combobox.setExtraHtml(defaultColorStyle);
        }
        catch(Exception e)
        {
            setStandardFailMessage();
            logger.error("Exception DealEntryHandler@fullAccessToComboBox: Problem encountered in combobox customization." + e.getMessage());
        }

    }

    /**
     *
     *
     */
    protected void setNetWorthTotal(ViewBean pg, ChildDisplayEvent event)
    {
    PageEntry currPage = (PageEntry) theSessionState.getCurrentPage();
    //logger.debug("---D---->UWH@setNetWorthTotal::CurrentPage: " + currPage);

        double netWorth = currPage.getNetWorth();
    //logger.debug("---D---->UWH@setNetWorthTotal::NetWorth: " + netWorth);
        ////logger.debug("---D---->UWH@setNetWorthTotal::setting the final value");

        pg.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STUWTOTALNETWORTH, (new Double(netWorth)).toString());
    }


    // The next three methods are reserved for proper realization of open/
    // close 'Other Details' sub-section functionality. Currently the Product Team has decided
    // to go with this sub-section open all time. These methods stay here for possible
    // customization for future clients. In this case the dynamic value "Y"/"N" of stOtherDetailsFlag
    // should be passed to the hdShowOtherDetails parameter of the html page. Currently it goes
    // with the value "Y" always.

    ////protected int passValueOfOtherDetailsFlag(DisplayField theField)
    protected String passValueOfOtherDetailsFlag(DisplayField theField)
    {
        PageEntry currPage = (PageEntry) theSessionState.getCurrentPage();

        if (currPage.getPageCondition6() == false)
        {
            ////theField.setExtraHtml("N");
            logger.debug("---D---->UWH.passInitValueOfOtherDetailsFlag@set up flag value as a default no " + "and SKIP the page");
            return "N";
        }
        else if (currPage.getPageCondition6() == true)
        {
            ////theField.setExtraHtml("Y");
            logger.debug("---D---->UWH.passInitValueOfOtherDetailsFlag@set up flag value to yes and PROCEED the page");
            return "Y";
        }
        return "Y";
    }


    /**
     *
     *
     */
    protected void handleFlagOtherDetails(ViewBean page, String buttonName, String flagName, String flagValue)
    {
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        page.setDisplayFieldValue(flagName, new String(flagValue));

        if (buttonName.equals("btOtherDetails"))
        {
            pg.setPageCondition6(true);
        }
        else if (buttonName.equals("btCloseOtherDetails"))
        {
            pg.setPageCondition6(false);
        }
    }


    /** setupDealResolutionPageEntry:
  *
  * Return a PageEntry for Deal Resolution Screen.
  *
  * Notes: If worksheet used in task orientation an attempt is made to find a Deal Resolution
  *        page entry in the task links (would expect it to immediately follow the worksheet -
  *        however, if not found a new PAgeEntry is created.
  *
  **/
    private PageEntry setupDealResolutionPageEntry(PageEntry pg)
    {
        return setupDealResolutionPageEntry(pg, true);
    }


    /**
     *
     *
     */
    private PageEntry setupDealResolutionPageEntry(PageEntry pg, boolean isForApprove)
    {
        PageEntry dealResPg = null;
        PageEntry pgTemp = pg;

        while(pgTemp.getNextPE() != null)
        {
            pgTemp = pgTemp.getNextPE();
            if (pgTemp.getPageId() == Mc.PGNM_DEAL_RESOLUTION)
            {
                dealResPg = pgTemp;
                break;
            }
        }

        if (dealResPg == null)
        {
            // make it!
            Page pgProperties = getPage(Sc.PGNM_DEAL_RESOLUTION);

            // setup page entry
            dealResPg = new PageEntry();
            dealResPg.setPageId(pgProperties.getPageId());
            dealResPg.setPageName(pgProperties.getPageName());
            dealResPg.setPageLabel(pgProperties.getPageLabel());
            dealResPg.setDealPage(pgProperties.getDealPage());
            dealResPg.setPageTaskId(0);
            dealResPg.setPageTaskSequence(0);
        }

        dealResPg.setPageDealId(pg.getPageDealId());
        dealResPg.setPageDealCID(pg.getPageDealCID());
        dealResPg.setDealInstitutionId(pg.getDealInstitutionId());
        dealResPg.setPageDealApplicationId(pg.getPageDealApplicationId());
        dealResPg.setPageMosUserId(pg.getPageMosUserId());
        dealResPg.setPageMosUserTypeId(pg.getPageMosUserTypeId());

        // mark the page to indicate transfer from undrwriter worksheet
        Hashtable pst = dealResPg.getPageStateTable();

        if (pst == null)
        {
            pst = new Hashtable();
            dealResPg.setPageStateTable(pst);
        }
        // Deal Resolution will not allow an approval resolution unless it finds this key
        if (isForApprove) pst.put(RESOLVE_FROM_UWORKSHEET, WHATEVER);

        return dealResPg;

    }

    //Method to set display or not display the FinancingProgram filed based on the property definition
    private void setDisplayFinancingProgram(PageEntry pg, ViewBean thePage)
    {
        if ((PropertiesCache.getInstance().getProperty
                        (theSessionState.getDealInstitutionId(),
                         COM_BASIS100_DEAL_UW_DISPLAYFINANCINGPROGRAM, "Y")).equals("Y"))
        {
            // Unset mask to display the field
            thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STINCLUDEFINANCINGPROGRAMSTART, new String(""));
            thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STINCLUDEFINANCINGPROGRAMEND, new String(""));
            return;
        }
        // Set Mask to make the filed not dispalyed suppress
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STINCLUDEFINANCINGPROGRAMSTART, new String(supresTagStart));
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STINCLUDEFINANCINGPROGRAMEND, new String(supresTagEnd));
        return;

    }

    // SEAN GECF IV Document type dorp down: dide control.
    private void setHideDocumentType(PageEntry pg, ViewBean thePage) {

        String start = supresTagStart;
        String end = supresTagEnd;

        // get flag from configuration file, default is Y.
        // July2012, QC212 - changed sysproperty controlling this feature
        String flag = PropertiesCache.getInstance().
            getProperty(theSessionState.getDealInstitutionId(),
            		COM_BASIS100_DEAL_UW_DISPLAYINCOMEVERIFICATION, "Y");
        if (flag.equalsIgnoreCase("Y")) {
            start = "";
            end = "";
        }

        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STHIDEIVDOCUMENTTYPESTART, start);
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STHIDEIVDOCUMENTTYPEEND, end);
    }
    // END GECF IV Document type dorp down

    // Method to handle Collapse/Deny Deal Button -- By Billy 04Jan2002
    public void handleCollapseDenyDeal()
    {
        //logger.trace("BILLY ===> @UWorksheetHandler.handleCollapseDenyDeal:");
        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        SysLogger logger = srk.getSysLogger();
        try
        {
            srk.beginTransaction();
            standardAdoptTxCopy(pg, true);
            Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
            
            //QC-Ticket-484-Start Commented below 389 fix and Added method to check the MI Status cancellation when user selected Collapse/Deny button
            checkMICancellation(deal);
            //QC-Ticket-484-End
            
            //QC-Ticket-389-Start Added following code to update the deal table with MI STATUS as cancelled when user selected Cancel/Deny button
            //deal.setMIStatusId(Mc.MI_STATUS_CANCELLATION_SUBMITTED);
            //QC-Ticket-389-End
            
            if (deal.getScenarioRecommended() == null || ! deal.getScenarioRecommended().equals("Y"))
            {
                // set current scenario as the recommended scenario
                CCM ccm = new CCM(srk);
                ccm.setAsRecommendedScenario(deal);
                deal.ejbStore();
            }

            // navigate to Deal Resolution
            PageEntry dealResolutionPg = setupDealResolutionPageEntry(pg, false);
            getSavedPages().setNextPage(dealResolutionPg);
            srk.commitTransaction();
            navigateToNextPage(true);
            return;
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            logger.error("Exception @UWorksheetHandler.handleCollapseDenyDeal()");
            logger.error(e);
        }
        // an exception has occured - ensure we navigate away from page since we can't guar
        // presence of a tx copy on regeneration
        navigateAwayFromFromPage(true);
    }
    
 // Added for ticket 484 - Method to determine MI Status Cancellation when a deal is collapsed/denied
    public void checkMICancellation(Deal deal) throws Exception {
    	
    	 int lStatus = deal.getMIStatusId();
    	 
    	 if(deal.getMIIndicatorId() == Mc.MII_NOT_REQUIRED)
         {
    		 deal.setMIStatusId(Mc.MI_STATUS_BLANK);
         }
    	 else if(deal.getMIIndicatorId() == Mc.MII_UW_WAIVED){
    		 switch(lStatus)
             {
             case Mc.MI_STATUS_BLANK:
             case Mc.MI_STATUS_INITIAL_REQUEST_PENDING:
             case Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED:
             case Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER:
             case Mc.MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER:
             case Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER:
             case Mc.MI_STATUS_ERRORS_RECEIVED_BY_INSURER:
             case Mc.MI_STATUS_ERRORS_CORRECTED_AND_PENDING:
             case Mc.MI_STATUS_CORRECTIONS_SUBMITTED:
             case Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY:
             case Mc.MI_STATUS_MANUAL_UNDERWRITING_REQUIRED:
             case Mc.MI_STATUS_CANCELLATION_PENDING:
             case Mc.MI_STATUS_CANCELLATION_SUBMITTED:
             case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC:
             case Mc.MI_STATUS_CANCELLED:
            	 
             case Mc.MI_STATUS_APPROVAL_RECEIVED:
            	 deal.setMIStatusId(Mc.MI_STATUS_CANCELLATION_SUBMITTED);
            	 break;
            	 
             case Mc.MI_STATUS_APPROVED:
            	 deal.setMIStatusId(Mc.MI_STATUS_CANCELLATION_SUBMITTED);
            	 break;
            	 
             case Mc.MI_STATUS_DENIED_RECEIVED:
             case Mc.MI_STATUS_DENIED:
             case Mc.MI_STATUS_RISKED_RECEIVED:
             case Mc.MI_STATUS_RISKED:
             case Mc.MI_STATUS_CHANGES_PENDING:
             case Mc.MI_STATUS_CHANGES_SUBMITTED:
             case Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED:
            	 
             default : deal.setMIStatusId(Mc.MI_STATUS_BLANK);
             	break;
             }
    	 }
    }
 // End of Method Added for ticket 484 
    
    // New method to populate JS variables for Refinance Section display/hide
    //  -- By BILLY 15Feb2002

    private void populateJSVALSData(ViewBean thePage)
    {
        String valsData = "";
        // The VAL String to be populated on the HTML
        // Set DealPurpose array
        valsData += "VALScbDealPurpose = new Array(" + PicklistData.getColumnValuesCDV(PKL_DEALPURPOSE, "DEALPURPOSEID") + ");" + "\n";

        // Set EmployPercentageIncludeInGDS array
        valsData += "VALSRefinanceFlags = new Array(" + PicklistData.getColumnValuesCDS(PKL_DEALPURPOSE, "REFINANCEFLAG") + ");" + "\n";
        thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STVALSDATA, new String(valsData));
    }

  //// This is a light solution to provide the translation of two basic scenario
  //// descriptions. This field is a free text field and the full scale translation
  //// should take a serious work. The only two basic patterns will be translated:
  //// "Based on Deal on MMM DD YYYY at HH:MM EST" and
  //// "Based on Scenario XX on MMM DD YYYY at HH:MM EST".

  //// Also this is done for Java 1.3 without the RegularExpession extension.
  //// Java 1.4 with precompiled RegExpression facilities
  //// gives much more room for translation. When the JBuilder will be upgrated to Java 1.4
  //// the following method should be re-written to incorporate the RegularExpression
  //// facilities.
    protected String translateScenarioDescription(String scenarioIn, int langId)
    {
        try
        {
      String based = "";
      String scenarioTimeStamp = "";

      // detect if not present or null
            if (scenarioIn == null || scenarioIn.trim().length() == 0)
        return scenarioIn;

      //// 1. Read the first token and go or not to go. This is very non-generic
      //// approach to parse the only approved for translation patterns. This part
      //// (until #2 should be re-done (similar to #2) if product team wants more
      //// translation. Note that in the parsed line fragment there is no chars
      //// 'o' and 'n' that's why (and only) this token can be used.
      scenarioIn = scenarioIn.trim();
      StringTokenizer st = new StringTokenizer(scenarioIn, "on", false);

      StringBuffer resultBuf = new StringBuffer();

      ////start reading the value read up taking the tokens
      based = st.nextToken();

      String deal = st.nextToken();

            //// Go further only if the first word is "Based"
      //// otherwise return the untranslated input String.
      if(scenarioIn.substring(0, 5).trim().equals(BASED))
      {
        String tmp1 = scenarioIn.substring(0, 13).trim();
        String tmp2 = scenarioIn.substring(0, 17).trim();

        if(tmp1.equals(BASED_ON_DEAL))
        {
          String basedOnDeal = BXResources.getGenericMsg(BASED_ON_DEAL_LABEL,  langId);
          resultBuf.append(basedOnDeal);
          resultBuf.append(" ");

          scenarioTimeStamp = scenarioIn.substring(17).trim();
        }

        if(tmp2.equals(BASED_ON_SCENARIO))
        {
          String basedOnScenario = BXResources.getGenericMsg(BASED_ON_SCENARIO_LABEL,  langId);
          resultBuf.append(basedOnScenario);
          resultBuf.append(" ");

          String scenarioNum = scenarioIn.substring(18, 20);
          //logger.debug("UWH@translateScenarioDescription::ScenarioNum: " + scenarioNum);
          resultBuf.append(scenarioNum);
          resultBuf.append(" ");

          scenarioTimeStamp = scenarioIn.substring(21).trim();
        }

        //logger.debug("UWH@translateScenarioDescription::ScenarioTimeStamp: " + scenarioTimeStamp);

        //// 2. Finally parse and translate scenario TimeStamp.
        //// Use the standard tokens, i.e. white spaces now.
        String month = "";
        String day = "";
        String year = "";
        String at = "";
        String hourmin = "";
        String timezone = "";
        int counter = 1;

        TreeMap timeParsedTimestmp = new TreeMap();
        StringTokenizer st1 = new StringTokenizer(scenarioTimeStamp);

        while (st1.hasMoreTokens())
        {
           timeParsedTimestmp.put (new Integer(counter), st1.nextToken());
           counter++;
        }

        String on = BXResources.getGenericMsg(ON_LABEL,  langId);

        resultBuf.append(on);
        resultBuf.append(" ");

        Set keys = timeParsedTimestmp.entrySet();
        Iterator iter = keys.iterator();

        while(iter.hasNext())
        {
          Map.Entry entry = (Map.Entry) iter.next();
          Object key = entry.getKey();
          Object value = entry.getValue();

          int translate = ((Integer)key).intValue();

          switch(translate)
          {
            case 1: month = translateMonth (value.toString(), langId);
            case 2: day = value.toString(); // no translation
            case 3: year = value.toString(); // no translation
            case 4: at = BXResources.getGenericMsg(AT_LABEL,  langId);
            case 5: hourmin = value.toString(); // no translation
            case 6: timezone = value.toString(); // no translation
           }
        }

        //// 3. Finish translation.
        resultBuf.append(month);
        resultBuf.append(" ");
        resultBuf.append(day);
        resultBuf.append(" ");
        resultBuf.append(year);
        resultBuf.append(" ");
        resultBuf.append(at);
        resultBuf.append(" ");
        resultBuf.append(hourmin);
        resultBuf.append(" ");
        resultBuf.append(timezone);

        //logger.debug("UWH@translateScenarioDescription:Buffer: " + resultBuf.toString());

        return resultBuf.toString();
      }
      else
        return scenarioIn;
        }
        catch(Exception e)
        {
            logger.debug("--D--> @translateScenarioDescription: in=<" + scenarioIn + "> - use empty scenario desc:" + e);		//#DG702
        }

    return scenarioIn;
    }

  private String translateMonth(String monthIn, int languageId)
  {
        String transMonth = "";

        try
        {
          if (monthIn.toUpperCase().trim().equals("JAN"))
          {
            transMonth = BXResources.getGenericMsg(JAN_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("FEB"))
          {
            transMonth = BXResources.getGenericMsg(FEB_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("MAR"))
          {
            transMonth = BXResources.getGenericMsg(MAR_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("APR"))
          {
            transMonth = BXResources.getGenericMsg(APR_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("MAY"))
          {
            transMonth = BXResources.getGenericMsg(MAY_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("JUN"))
          {
            transMonth = BXResources.getGenericMsg(JUN_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("JUL"))
          {
            transMonth = BXResources.getGenericMsg(JUL_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("AUG"))
          {
            transMonth = BXResources.getGenericMsg(AUG_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("SEP"))
          {
            transMonth = BXResources.getGenericMsg(SEP_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("OCT"))
          {
            transMonth = BXResources.getGenericMsg(OCT_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("NOV"))
          {
            transMonth = BXResources.getGenericMsg(NOV_LABEL,  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("DEC"))
          {
            transMonth = BXResources.getGenericMsg(DEC_LABEL,  languageId);
          }
        }
        catch(NumberFormatException e)
        {
          throw new IllegalArgumentException("UWH@translateScenarioDescription:Untranslated format", e);		//#DG702
        }
        return transMonth;
  }

  	//--FX_LINK--start--//
    public void handleDetailSourceBusiness()
    {
        try
        {
            // Goto review SOB page
      PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
            PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SOB_REVIEW, true);
            Hashtable subPst = pgEntry.getPageStateTable();
      Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
      int sobProfileId = deal.getSourceOfBusinessProfileId();

logger.debug("UWH@handleDetailSourceBusiness::SOBProfileId: " + sobProfileId);
            subPst.put(SOB_ID_PASSED_PARM, new String("" + sobProfileId));
            getSavedPages().setNextPage(pgEntry);
            navigateToNextPage(true);
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            setStandardFailMessage();
            logger.error("Error @UWorksheetHandler.handleDetailSourceBusiness: ");
            logger.error(e);
        }
    }
  //--FX_LINK--end--//

//4.4 Submission Agent
    public void handleDetailSourceBusiness(String fieldName, int rowIndex)
    {
        try
        {
            // Goto review SOB page
            PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
            PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SOB_REVIEW, true);
            Hashtable subPst = pgEntry.getPageStateTable();

            int sobProfileId = com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField(fieldName, rowIndex));
            
            logger.debug("DEH@handleDetailSourceBusiness::SOBProfileId: " + sobProfileId);
            subPst.put(SOB_ID_PASSED_PARM, new String("" + sobProfileId));
            getSavedPages().setNextPage(pgEntry);
            navigateToNextPage(true);
        }
        catch(Exception e)
        {
            srk.cleanTransaction();
            setStandardFailMessage();
            logger.error("Error @UWorksheetHandler.handleDetailSourceBusiness: ");
            logger.error(e);
        }

    }

    
  //-- ========== SCR#859 begins ========== --//
  // Vlad: UW are using its own SnapShotModel.
  protected void setCCAPSServicingMortgageNumber() {
        doUWDealSummarySnapShotModel myModel = (doUWDealSummarySnapShotModel)(RequestManager.getRequestContext().getModelManager().
                                                getModel(doUWDealSummarySnapShotModel.class));
        String servicingMortgageNumber = null;

        try {
                if (myModel.getSize() > 0) {
                        Object myObj = myModel.getDfServicingMortgageNumber();
                        if (myObj == null) {
                                servicingMortgageNumber = "";
                        } else {
                                servicingMortgageNumber = myObj.toString();
                        }
                } else {
                        logger.error(
                                "UWH@setCCAPSServicingMortgageNumber(): Problem when getting ServicingMortgageNumber info !");
                }

        } catch (ModelControlException mce) {
                String strError = "UWH@setCCAPSServicingMortgageNumber(): " + mce.getMessage();
                logger.error(mce);
                logger.error(strError);
        }

        logger.debug("UWH@setCCAPSServicingMortgageNumber(): servicingMortgageNumber = " + servicingMortgageNumber);

        boolean isBMO = false;
        if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(), COM_BASIS100_FXP_CLIENTID_BMO, "N").equals("Y")) {
                isBMO = true;
        }

        if (isBMO) {
                getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STCCAPS, "CCAPS: ");
                getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STSERVICINGMORTGAGENUMBER, servicingMortgageNumber);
        } else {
                getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STCCAPS, "");
                getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STSERVICINGMORTGAGENUMBER, "");
        }
  }
  //-- ========== SCR#859 ends ========== --//

    //  ***** Change by NBC/PP Implementation Team - GCD - Start *****//
    // GCD Summary Begin

  	/**
  	 * handleCreditDecisionPGButton
  	 *  Setup and send user to the Credit Decision screen
  	 *
  	 *  @version <br>
  	 *  Date: 10/17/2006 <br>
  	 *  Author: GCD Implmentation Team<br>
  	 *  Change:<br>
  	 *  	Added calls to adopt transactional copy before loading Credit Decision screen
  	 */

    public void handleCreditDecisionPGButton(RequestInvocationEvent event) {
  	 PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

     SessionResourceKit srk = getSessionResourceKit();

     SysLogger logger = srk.getSysLogger();

     try
     {
         srk.beginTransaction();

         // adopt transactional copy unconditionally - ensures we enter Credit Decision screen with saved
         // (non-discardable) scenario data
         standardAdoptTxCopy(pg, true);
         srk.commitTransaction();

        // call setNextPage if CD screen is not a subpage of UW worksheet
		PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_CREDIT_DECISION, true);
        getSavedPages().setNextPage(pgEntry);
        navigateToNextPage(true);
     }
     catch(Exception e)
     {
         srk.cleanTransaction();
         logger.error("Exception @handleCreditDecisionPGButton().");
         logger.error(e);
         setStandardFailMessage();
     }
    }

    // GCD Summary End
    //  ***** Change by NBC/PP Implementation Team - GCD - End *****//


 // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
 public void handleReturn(){
   if ((PropertiesCache.getInstance().getProperty
                   (theSessionState.getDealInstitutionId(),
                    COM_BASIS100_FXP_CLIENTID_GECF, "N")).equals("Y"))
   {
     logger.debug("Katya: UWS@handleReturn(): this is GE case" );

     setActiveMessageToAlert(
       BXResources.getSysMsg(CONFIRM_SUBMIT_TO_AU, theSessionState.getLanguageId()),
       ActiveMsgFactory.ISYESNO, AU_SUBMIT_YES, AU_SUBMIT_NO);

   }
   else   // not GE, all other cases are - standard Cancel
   {
     logger.debug("Katya: UWS@handleReturn(): this is not GE case" );
     handleCancelStandard();
   }
 }

	//4.4 Submission Agent
	public boolean setupNumOfRepeatedSOB(TiledView repeated1)
	{
		try
		{
			doUWSourceDetailsModelImpl listOfSOBs =(doUWSourceDetailsModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doUWSourceDetailsModelImpl.class));

			if(listOfSOBs.getSize() == 0) return false;
				repeated1.setMaxDisplayTiles(listOfSOBs.getSize());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealSummaryHandler.setupNumOfRepeatedSOB");
			logger.error(e);
		}
		return true;
	}
    
    /**
     * Qualify Rate 
     * @param thePage
     */
    private void setHiddenQualifyRateSection(ViewBean thePage) {

        String prop = PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),
                "com.filogix.qualoverridechk", "Y");
        if (prop.equals("N")) {
            thePage.setDisplayFieldValue("stQualifyRateHidden", "visible");
            thePage.setDisplayFieldValue("stQualifyRateEdit", "none");

        } else {
            // Set Mask to make the filed not dispalyed
            thePage.setDisplayFieldValue("stQualifyRateHidden", "none");
            thePage.setDisplayFieldValue("stQualifyRateEdit", "visible");
        }
    }
    
    /**
     * Qualify Rate 
     * @param thePage
     */
    private void setQualifyRateJSValidation(ViewBean thePage) {
        
        boolean propRateIsN = PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),
                "com.filogix.qualrateoverride", "Y").equalsIgnoreCase("N");

        if (propRateIsN)
            thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_HDQUALIFYRATEDISABLED, new String("Y"));
        else
        	thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_HDQUALIFYRATEDISABLED, new String("N"));

        boolean propCheckBox = PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),
                "com.filogix.qualoverridechk", "Y").equalsIgnoreCase("N");

        doUWDealSelectMainModel theModel = (doUWDealSelectMainModel) RequestManager
                .getRequestContext().getModelManager().getModel(
                        doUWDealSelectMainModel.class);
        
        Object flag = theModel.getDfQualifyingOverrideFlag();

        HtmlDisplayFieldBase chQualifyRateOverrideDf = (HtmlDisplayFieldBase) getCurrNDPage()
            .getDisplayField("chQualifyRateOverride");

        String extraId = "id='chQualifyRateOverride' ";
        String jsChange = " onclick ='qualifyProdRate();'"; //qualifyChkBoxClicked()
        String disable = " disabled='true'";
    
        chQualifyRateOverrideDf.setExtraHtml(extraId + jsChange);
        
        //new FSD
        HtmlDisplayFieldBase chQualifyRateOverrideRateDf = (HtmlDisplayFieldBase) getCurrNDPage()
                .getDisplayField("chQualifyRateOverrideRate");

        extraId = "id='chQualifyRateOverrideRate' ";
        jsChange = " onclick ='qualifyProdRate();'"; //qualifyRateChkBoxClicked();
        if (propRateIsN) //rate changes are disabled
        	chQualifyRateOverrideRateDf.setExtraHtml(extraId + jsChange + disable);        
        else
        	chQualifyRateOverrideRateDf.setExtraHtml(extraId + jsChange);        
        //end new FSD
        
        HtmlDisplayFieldBase qualifyRateDf = (HtmlDisplayFieldBase) getCurrNDPage()
                .getDisplayField("txQualifyRate");

        extraId = "id='txQualifyRate'";
        String n = "'N'";
        jsChange = " onBlur=\"if(isFieldDecimal(3)) isFieldInDecRange(0, 100); showRatePencil("+ n +");\"";

        String extraHtml = extraId + jsChange;

        if ( flag==null && (!propCheckBox && !propRateIsN)) {
            extraHtml = extraHtml + disable;
        } else if ( propCheckBox || propRateIsN || (flag!=null && "N".equalsIgnoreCase(flag.toString()))) {
            extraHtml = extraHtml + disable;
        }
        qualifyRateDf.setExtraHtml(extraHtml);

        HtmlDisplayFieldBase cbProductType = (HtmlDisplayFieldBase) getCurrNDPage()
                .getDisplayField("cbQualifyProductType");

        extraId = "id='cbQualifyProductType' onChange ='qualifyProductChanged();'";
        extraHtml = extraId;

        if ( flag==null && (!propCheckBox && !propRateIsN)) {
            extraHtml = extraHtml + disable;
        } else if ( propCheckBox || propRateIsN || (flag!=null && "N".equalsIgnoreCase(flag.toString()))) {
            extraHtml = extraHtml + disable;
        }
        cbProductType.setExtraHtml(extraHtml);
    }
    
    /**
     * populate product options.
     */
    protected void populateQualifyingProducts(String fieldName, Deal deal) 
    {

        // let's prepare the options.
    	OptionList theProductOptions = new OptionList();
        Map products = QualifyingRateHelper.getValidQualifyingProducts(getSessionResourceKit(),
                             theSessionState.getLanguageId(),
                             deal);
        
        for (Iterator i = products.keySet().iterator(); i.hasNext(); ) {
            Integer id = (Integer) i.next();
            String label = (String) products.get(id);
            theProductOptions.add(new Option(label, id.toString()));
        }
        // populate the product options.
        ComboBox cbProduct =
            (ComboBox) getCurrNDPage().getChild(fieldName);
        cbProduct.setOptions(theProductOptions);
    }

}

