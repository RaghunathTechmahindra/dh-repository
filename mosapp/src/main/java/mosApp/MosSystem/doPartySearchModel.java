package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doPartySearchModel
  extends QueryModel, SelectQueryModel
{
  /**
   *
   *
   */
  public java.math.BigDecimal getDfPartyType();

  /**
   *
   *
   */
  public void setDfPartyType(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfCompany();

  /**
   *
   *
   */
  public void setDfCompany(String value);

  /**
   *
   *
   */
  public String getDfCity();

  /**
   *
   *
   */
  public void setDfCity(String value);

  /**
   *
   *
   */
  public String getDfPhone();

  /**
   *
   *
   */
  public void setDfPhone(String value);

  /**
   *
   *
   */
  public String getDfFax();

  /**
   *
   *
   */
  public void setDfFax(String value);

  /**
   *
   *
   */
  public String getDfProvinceAbbreviation();

  /**
   *
   *
   */
  public void setDfProvinceAbbreviation(String value);

  /**
   *
   *
   */
  public String getDfPartyTypeDescription();

  /**
   *
   *
   */
  public void setDfPartyTypeDescription(String value);

  /**
   *
   *
   */
  public String getDfProvinceName();

  /**
   *
   *
   */
  public void setDfProvinceName(String value);

  /**
   *
   *
   */
  public String getDfPhoneNumberExt();

  /**
   *
   *
   */
  public void setDfPhoneNumberExt(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPartyStatus();

  /**
   *
   *
   */
  public void setDfPartyStatus(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfPartyShortName();

  /**
   *
   *
   */
  public void setDfPartyShortName(String value);

  /**
   *
   *
   */
  public String getDfClientNumber();

  /**
   *
   *
   */
  public void setDfClientNumber(String value);

  /**
   *
   *
   */
  public String getDfContactFirstName();

  /**
   *
   *
   */
  public void setDfContactFirstName(String value);

  /**
   *
   *
   */
  public String getDfContactIntitial();

  /**
   *
   *
   */
  public void setDfContactIntitial(String value);

  /**
   *
   *
   */
  public String getDfContactLastName();

  /**
   *
   *
   */
  public void setDfContactLastName(String value);

  /**
   *
   *
   */
  public String getDfContactEmail();

  /**
   *
   *
   */
  public void setDfContactEmail(String value);

  /**
   *
   *
   */
  public String getDfAddressLine1();

  /**
   *
   *
   */
  public void setDfAddressLine1(String value);

  /**
   *
   *
   */
  public String getDfAddressLine2();

  /**
   *
   *
   */
  public void setDfAddressLine2(String value);

  /**
   *
   *
   */
  public String getDfPostalFSA();

  /**
   *
   *
   */
  public void setDfPostalFSA(String value);

  /**
   *
   *
   */
  public String getDfPostalLDU();

  /**
   *
   *
   */
  public void setDfPostalLDU(String value);



  /**
   *
   *
   */
  public String getDfContactPhoneExt();

  /**
   *
   *
   */
  public void setDfContactPhoneExt(String value);

  /**
   *
   *
   */
  public String getDfPostalFSA_LDU();

  /**
   *
   *
   */
  public void setDfPostalFSA_LDU(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfProfileStatus();

  /**
   *
   *
   */
  public void setDfProfileStatus(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfProvinceId();

  /**
   *
   *
   */
  public void setDfProvinceId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfPartyProfileStatusDesc();

  /**
   *
   *
   */
  public void setDfPartyProfileStatusDesc(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPartyId();

  /**
   *
   *
   */
  public void setDfPartyId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfPartyName();

  /**
   *
   *
   */
  public void setDfPartyName(String value);

  /**
   *
   *
   */
  public String getDfNotes();

  /**
   *
   *
   */
  public void setDfNotes(String value);

  //--DJ_PT_CR--start//
  public void setDfBranchTransitNumCMHC(String value);

  public String getDfBranchTransitNumCMHC();

  public void setDfBranchTransitNumGE(String value);

  public String getDfBranchTransitNumGE();

  public String getDfPartyNameFirstPart();

  public void setDfPartyNameFirstPart(String value);

  public String getDfPartyNameSecondPart();

  public void setDfPartyNameSecondPart(String value);

  public String getDfPartyNameThirdPart();

  public void setDfPartyNameThirdPart(String value);

  //--DJ_PT_CR--end//

  //--DJ_PREFCOMMETHOD_CR--start--//
  public String getDfPreferredDeliveryMethod();

  public void setDfPreferredDeliveryMethod(String value);

  public void setDfPreferredDeliveryMethodId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfPreferredDeliveryMethodId();
  public java.math.BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(java.math.BigDecimal id);
  //--DJ_PREFCOMMETHOD_CR--end--//

  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
  //--> By Billy 20May2004
  public String getDfBankTransitNum();
  public void setDfBankTransitNum(String value);
  public String getDfBankAccNum();
  public void setDfBankAccNum(String value);
  public String getDfBankName();
  public void setDfBankName(String value);
  //=====================================================================

  //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
  // Add new location field
  public String getDfLocation();
  public void setDfLocation(String value);
  //***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  public static final String FIELD_DFPARTYTYPE = "dfPartyType";
  public static final String FIELD_DFCOMPANY = "dfCompany";
  public static final String FIELD_DFCITY = "dfCity";
  public static final String FIELD_DFPHONE = "dfPhone";
  public static final String FIELD_DFFAX = "dfFax";
  public static final String FIELD_DFPROVINCEABBREVIATION = "dfProvinceAbbreviation";
  public static final String FIELD_DFPARTYTYPEDESCRIPTION = "dfPartyTypeDescription";
  public static final String FIELD_DFPROVINCENAME = "dfProvinceName";
  public static final String FIELD_DFPHONENUMBEREXT = "dfPhoneNumberExt";
  public static final String FIELD_DFPARTYSTATUS = "dfPartyStatus";
  public static final String FIELD_DFPARTYSHORTNAME = "dfPartyShortName";
  public static final String FIELD_DFCLIENTNUMBER = "dfClientNumber";
  public static final String FIELD_DFCONTACTFIRSTNAME = "dfContactFirstName";
  public static final String FIELD_DFCONTACTINTITIAL = "dfContactIntitial";
  public static final String FIELD_DFCONTACTLASTNAME = "dfContactLastName";
  public static final String FIELD_DFCONTACTEMAIL = "dfContactEmail";
  public static final String FIELD_DFADDRESSLINE1 = "dfAddressLine1";
  public static final String FIELD_DFADDRESSLINE2 = "dfAddressLine2";
  public static final String FIELD_DFPOSTALFSA = "dfPostalFSA";
  public static final String FIELD_DFPOSTALLDU = "dfPostalLDU";
  //--> This is not used at all : removed by Billy 21May2004
  //public static final String FIELD_DFPARTYDESCRIPTION = "dfPartyDescription";
  public static final String FIELD_DFCONTACTPHONEEXT = "dfContactPhoneExt";
  public static final String FIELD_DFPOSTALFSA_LDU = "dfPostalFSA_LDU";
  public static final String FIELD_DFPROFILESTATUS = "dfProfileStatus";
  public static final String FIELD_DFPROVINCEID = "dfProvinceId";
  public static final String FIELD_DFPARTYPROFILESTATUSDESC = "dfPartyProfileStatusDesc";
  public static final String FIELD_DFPARTYID = "dfPartyId";
  public static final String FIELD_DFPARTYNAME = "dfPartyName";
  public static final String FIELD_DFNOTES = "dfNotes";

  //--DJ_PT_CR--start//
  public static final String FIELD_DFBRANCHTRANSITNUMCMHC = "dfBranchTransitNumCMHC";
  public static final String FIELD_DFBRANCHTRANSITNUMGE = "dfBranchTransitNumGE";

  public static final String FIELD_DFPARTYNAMEFIRSTPART = "dfPartyNameFirstPart";
  public static final String FIELD_DFPARTYNAMESECONDPART = "dfPartyNameSecondPart";
  public static final String FIELD_DFPARTYNAMETHIRDPART = "dfPartyNameThirdPart";
  //--DJ_PT_CR--end//

  //--DJ_PREFCOMMETHOD_CR--start--//
  public static final String FIELD_DFPREFERREDDELIVERYMETHOD = "dfPrefDeliveryMethod";
  public static final String FIELD_DFPREFERREDDELIVERYMETHODID = "dfPrefDeliveryMethodId";
  //--DJ_PREFCOMMETHOD_CR--end--//

  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
  //--> By Billy 20May2004
  public static final String FIELD_DFBANKTRANSITNUM = "dfBankTransitNum";
  public static final String FIELD_DFBANKACCNUM = "dfBankAccNum";
  public static final String FIELD_DFBANKNAME = "dfBankName";
  //=====================================================================

  //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
  // Add new location field
  public static final String FIELD_DFLOCATION = "dfLocation";
  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
  //***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
  
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

}
