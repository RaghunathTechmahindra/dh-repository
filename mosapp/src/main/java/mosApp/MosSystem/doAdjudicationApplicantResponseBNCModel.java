package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doAdjudicationApplicantResponseBNCModel extends QueryModel, SelectQueryModel
{
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();

	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponseId();


	/**
	 *
	 *
	 */
	public void setDfResponseId(java.math.BigDecimal value);



	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
    public java.math.BigDecimal getDfApplicantNumber();


	/**
	 *
	 *
	 */
	public void setDfApplicantNumber(java.math.BigDecimal value);

	

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditBureauScore();

	/**
	 *
	 *
	 */
	public void setDfCreditBureauScore(java.math.BigDecimal value);



	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfInternalScore();

	/**
	 *
	 *
	 */
	public void setDfInternalScore(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetWorth();

	/**
	 *
	 *
	 */
	public void setDfNetWorth(java.math.BigDecimal value);



	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequestedCreditBureauNameId();

	/**
	 *
	 *
	 */
	public void setDfRequestedCreditBureauNameId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfUsedCreditBureauNameId();


	/**
	 *
	 *
	 */
	public void setDfUsedCreditBureauNameId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
 public String getDfUsedCreditBureauName();


	/**
	 *
	 *
	 */
	public void setDfUsedCreditBureauName(String value);


	/**
	 *
	 *
	 */
    public java.math.BigDecimal getDfRiskRating();


	/**
	 *
	 *
	 */
	public void setDfRiskRating(java.math.BigDecimal value);

	


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFRESPONSEID="dfResponseId";
	public static final String FIELD_DFCREDITBUREAUSCORE="dfCreditBureauScore";
	public static final String FIELD_DFINTERNALSCORE="dfInternalScore";
	public static final String FIELD_DFRISKRATING="dfRiskRating";
	public static final String FIELD_DFNETWORTH="dfNetWorth";
	public static final String FIELD_DFREQUESTEDCREDITBUREAUNAMEID="dfRequestedCreditBureauNameId";
	public static final String FIELD_DFUSEDCREDITBUREAUNAMEID="dfUsedCreditBureauNameId";
	public static final String FIELD_DFUSEDCREDITBUREAUNAME="dfUsedCreditBureauName";
	public static final String FIELD_DFAPPLICANTNUMBER="dfApplicantNumber";
	
	




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

