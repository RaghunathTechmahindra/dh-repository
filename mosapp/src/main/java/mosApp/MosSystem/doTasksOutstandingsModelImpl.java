package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;

import com.basis100.picklist.BXResources;

/**
 *
 *  TD_MWQ_CR.
 *  TD wants to see the new summary section counted by TaskName
 *  (currently only two tasks: all open Decision Deal
 *                         and Source Resubmition.
 *                         
 *  Mar 11, 2008: ML, Introducing InstitutionProfileId. SQL was changed not to return duplicated data.                       
 *
 */
public class doTasksOutstandingsModelImpl extends QueryModelBase
	implements doTasksOutstandingsModel
{
	/**
	 *
	 *
	 */
	public doTasksOutstandingsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

          //logger = SysLog.getSysLogger("DOMWQTOM");
          //logger.debug("DOMWQTOM@PriorityBeforeExecute::SQL: " + sql);

          return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
          //// To override all the Description fields and populate them from BXResource
          //// based on the Language ID from the SessionStateModel
          String defaultInstanceStateName =
              getRequestContext().getModelManager().getDefaultModelInstanceName(
              SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)
              getRequestContext().getModelManager().getModel(
              SessionStateModel.class,
              defaultInstanceStateName,
              true);

          int languageId = theSessionState.getLanguageId();

          while (this.next()) {
            this.setDfTaskNameLabel(BXResources.getPickListDescription(theSessionState.getUserInstitutionId(),
                "TASK741", this.getDfTaskId().intValue(), languageId));
          }

          //Reset Location
          this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfTaskNameLabel()
	{
		return (String)getValue(FIELD_DFTASKNAMELABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfTaskNameLabel(String value)
	{
		setValue(FIELD_DFTASKNAMELABEL,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTaskId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTASKID);
	}


	/**
	 *
	 *
	 */
	public void setDfTaskId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTASKID,value);
	}


	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doTasksOutstandingsModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

        //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
        //// (see detailed description in the desing doc as well).
        //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
        //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
        //// accordingly.

       //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT TASK.TASKID FROM TASK " +
                                                 "__WHERE__  ORDER BY TASK.TASKID  ASC";

	public static final String MODIFYING_QUERY_TABLE_NAME="TASK";

       //// TD currently wants to see only two tasks: all open Decision Deal
      ////                                           and Source Resubmition.

	public static final String STATIC_WHERE_CRITERIA=" TASK.TASKID = 3 OR TASK.TASKID = 258 ";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	// ML, Mar 11, 2008 - we don't need label; it will be created by afterExecute method 
//	public static final String QUALIFIED_COLUMN_DFTASKNAMELABEL="TASK.SYNTHETICTASKNAME";
//	public static final String COLUMN_DFTASKNAMELABEL="SYNTHETICTASKNAME";
	public static final String QUALIFIED_COLUMN_DFTASKID="TASK.TASKID";
	public static final String COLUMN_DFTASKID="TASKID";


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{
	 // ML, Mar 11, 2008 - we don't need label; it will be created by afterExecute method
//		FIELD_SCHEMA.addFieldDescriptor(
//			new QueryFieldDescriptor(
//				FIELD_DFTASKNAMELABEL,
//				COLUMN_DFTASKNAMELABEL,
//				QUALIFIED_COLUMN_DFTASKNAMELABEL,
//				String.class,
//				false,
//				true,
//				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
//				"TASK.TASKNAME",
//				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
//				"TASK.TASKNAME"));


		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKID,
				COLUMN_DFTASKID,
				QUALIFIED_COLUMN_DFTASKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	}
}
