package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * <p>Title: doAdjudicationMainBNCModel</p>
 * 
 * <p>Description: Interface class for doAdjudicationMainBNCModelImpl</p>
 * 
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.2 <br>
 * Date: 10/18/2006 <br>
 * Author: GCD Implementation Team<br>
 * Change: Updated PostedInterestRate field to PostedRate as per spec, defect #1031
 */
public interface doAdjudicationMainBNCModel extends QueryModel, SelectQueryModel
{

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealStatusCategoryId();


	/**
	 *
	 *
	 */
	public void setDfDealStatusCategoryId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPostedRate();
	
	/**
	 *
	 *
	 */
	public void setDfPostedRate(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public void setDfActualPaymentTerm(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualPaymentTerm();

	/**
	 *
	 *
	 */
	public void setDfPaymentTerm(String value);


	/**
	 *
	 *
	 */
	public String getDfPaymentTerm();

	/**
	 *
	 *
	 */
	public void setDfPaymentTermMonths(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaymentTermMonths();

	/**
	 *
	 *
	 */
	public void setDfAmortizationTerm(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAmortizationTerm();

	/**
	 *
	 *
	 */
	public void setDfAmortization(String value);


	/**
	 *
	 *
	 */
	public String getDfAmortization();
	

	/**
	 *
	 *
	 */
	public void setDfAmortizationTermMonths(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAmortizationTermMonths();
	
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPAndIPaymentAmountMonthly();
	
	/**
	 *
	 *
	 */
	public void setDfPAndIPaymentAmountMonthly(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public void setDfDownPaymentAmount(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentAmount();
	
	/**
	 *
	 *
	 */
	public void setDfCombinedLTV(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedLTV();
	
	
	/**
	 *
	 *
	 */
	public void setDfPropertyCity(String value);
	
	/**
	 *
	 *
	 */
	public String getDfPropertyCity();
	
	/**
	 *
	 *
	 */
	public void setDfProvinceId(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProvinceId();
	
	
	/**
	 *
	 *
	 */
	public void setDfProvince(String value);
	
	/**
	 *
	 *
	 */
	public String getDfProvince();
	
	/**
	 *
	 *
	 */
	public void setDfLocation(String value);
	
	/**
	 *
	 *
	 */
	public String getDfLocation();
	
	/**
	 *
	 *
	 */
	public void setDfOccupancyType(String value);
	
	/**
	 *
	 *
	 */
	public String getDfOccupancyType();

	/**
	 *
	 *
	 */
	public void setDfOccupancyTypeId(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfOccupancyTypeId();
	
	/**
	 *
	 *
	 */
	public void setDfPropertyTypeId(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyTypeId();
	
	/**
	 *
	 *
	 */
	public String getDfPropertyType();

	/**
	 *
	 *
	 */
	public void setDfPropertyType(String value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMortgageInsurerId();
	
	/**
	 *
	 *
	 */
	public void setDfMortgageInsurerId(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public String getDfMortgageInsurer();


	/**
	 *
	 *
	 */
	public void setDfMortgageInsurer(String value);
	
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIStatusId();
	
	/**
	 *
	 *
	 */
	public void setDfMIStatusId(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public String getDfMIStatus();


	/**
	 *
	 *
	 */
	public void setDfMIStatus(String value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDSBorrower();
	
	/**
	 *
	 *
	 */
	public void setDfCombinedGDSBorrower(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDSBorrower();
	
	/**
	 *
	 *
	 */
	public void setDfCombinedTDSBorrower(java.math.BigDecimal value);
	

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalCombinedIncome();
	/**
	 *
	 *
	 */
	public void setDfTotalCombinedIncome(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalAnnualIncome();
	/**
	 *
	 *
	 */
	public void setDfTotalAnnualIncome(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalOtherIncome();
	/**
	 *
	 *
	 */
	public void setDfTotalOtherIncome(java.math.BigDecimal value);



	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFDEALSTATUSCATEGORYID="dfDealStatusCategoryId";

	public static final String FIELD_DFPOSTEDRATE="dfPostedRate";
	public static final String FIELD_DFACTUALPAYMENTTERM="dfActualPaymentTerm";
	public static final String FIELD_DFPAYMENTTERM="dfPaymentTerm";
	public static final String FIELD_DFPAYMENTTERMMONTHS="dfPaymentTermMonths";
	public static final String FIELD_DFAMORTIZATIONTERM="dfAmortizationTerm";
	public static final String FIELD_DFAMORTIZATION="dfAmortization";
	public static final String FIELD_DFAMORTIZATIONTERMMONTHS="dfAmortizationTermMonths";
	public static final String FIELD_DFPANDIPAYMENTAMOUNTMONTHLY="dfPAndIPaymentAmountMonthly";
	public static final String FIELD_DFDOWNPAYMENTAMOUNT="dfDownPaymentAmount";
	public static final String FIELD_DFCOMBINEDLTV="dfCombinedLTV";
	public static final String FIELD_DFPROPERTYCITY="dfPropertyCity";
	public static final String FIELD_DFPROVINCEID="dfProvinceId";
	public static final String FIELD_DFPROVINCE="dfProvince";
	public static final String FIELD_DFLOCATION="dfLocation";
	public static final String FIELD_DFOCCUPANCYTYPEID="dfOccupancyTypeId";
	public static final String FIELD_DFOCCUPANCYTYPE="dfOccupancyType";
	public static final String FIELD_DFPROPERTYTYPEID="dfPropertyTypeId";
	public static final String FIELD_DFPROPERTYTYPE="dfPropertyType";
	public static final String FIELD_DFMORTGAGEINSURERID="dfMortgageInsurerId";
	public static final String FIELD_DFMORTGAGEINSURER="dfMortgageInsurer";
	public static final String FIELD_DFMISTATUSID="dfMIStatusId";
	public static final String FIELD_DFMISTATUS="dfMIStatus";
	public static final String FIELD_DFCOMBINEDGDSBORROWER="dfCombinedGDSBorrower";
	public static final String FIELD_DFCOMBINEDTDSBORROWER="dfCombinedTDSBorrower";

	public static final String FIELD_DFTOTALCOMBINEDINCOME="dfTotalCombinedIncome";
	public static final String FIELD_DFTOTALANNUALINCOME="dfTotalAnnualIncome";
	public static final String FIELD_DFTOTALOTHERINCOME="dfTotalOtherIncome";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

