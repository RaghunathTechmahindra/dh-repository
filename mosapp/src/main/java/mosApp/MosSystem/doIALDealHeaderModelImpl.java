package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doIALDealHeaderModelImpl extends QueryModelBase
	implements doIALDealHeaderModel
{
	/**
	 *
	 *
	 */
	public doIALDealHeaderModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLiabilities()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALLIABILITIES);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalLiabilities(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALLIABILITIES,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalAssets()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALASSETS);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalAssets(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALASSETS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalIncome()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALINCOME);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalIncome(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALINCOME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLTV()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALLTV);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalLTV(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALLTV,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.DEALID, DEAL.COMBINEDTOTALLIABILITIES, DEAL.COMBINEDTOTALASSETS, DEAL.COMBINEDTOTALINCOME, DEAL.COMBINEDTDS, DEAL.COMBINEDGDS, DEAL.LTV,DEAL.INSTITUTIONPROFILEID FROM DEAL  __WHERE__  ";
	
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFTOTALLIABILITIES="DEAL.COMBINEDTOTALLIABILITIES";
	public static final String COLUMN_DFTOTALLIABILITIES="COMBINEDTOTALLIABILITIES";
	public static final String QUALIFIED_COLUMN_DFTOTALASSETS="DEAL.COMBINEDTOTALASSETS";
	public static final String COLUMN_DFTOTALASSETS="COMBINEDTOTALASSETS";
	public static final String QUALIFIED_COLUMN_DFTOTALINCOME="DEAL.COMBINEDTOTALINCOME";
	public static final String COLUMN_DFTOTALINCOME="COMBINEDTOTALINCOME";
	public static final String QUALIFIED_COLUMN_DFTOTALTDS="DEAL.COMBINEDTDS";
	public static final String COLUMN_DFTOTALTDS="COMBINEDTDS";
	public static final String QUALIFIED_COLUMN_DFTOTALGDS="DEAL.COMBINEDGDS";
	public static final String COLUMN_DFTOTALGDS="COMBINEDGDS";
	public static final String QUALIFIED_COLUMN_DFTOTALLTV="DEAL.LTV";
	public static final String COLUMN_DFTOTALLTV="LTV";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "DEAL.INSTITUTIONPROFILEID";
  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALLIABILITIES,
				COLUMN_DFTOTALLIABILITIES,
				QUALIFIED_COLUMN_DFTOTALLIABILITIES,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALASSETS,
				COLUMN_DFTOTALASSETS,
				QUALIFIED_COLUMN_DFTOTALASSETS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALINCOME,
				COLUMN_DFTOTALINCOME,
				QUALIFIED_COLUMN_DFTOTALINCOME,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALTDS,
				COLUMN_DFTOTALTDS,
				QUALIFIED_COLUMN_DFTOTALTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALGDS,
				COLUMN_DFTOTALGDS,
				QUALIFIED_COLUMN_DFTOTALGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALLTV,
				COLUMN_DFTOTALLTV,
				QUALIFIED_COLUMN_DFTOTALLTV,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
	}

}

