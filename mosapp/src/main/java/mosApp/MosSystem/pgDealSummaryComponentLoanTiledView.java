package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;


/**
 * Title: pgDealSummaryComponentLoanTiledView
 * <p>
 * Description: This view bean relates the pgDealSummary.jsp.
 * @author MCM Impl Team.
 * @version 1.0 10-June-2008 XS_16.7 Initial version
 * @version 1.1 17-June-2008 XS_16.15
 *  Added new fields for display component loan infomation in Component Details Summary Screen.
 * @version 1.2 25-June-2008 XS_16.15 Updadted nextTile() method:-Added code to display ActualPaymentTerm.
 * @version 1.3 17-July-2008 XS_2.8 Added nextTile(boolean) to avoid handler and fields confliction
 */
public class pgDealSummaryComponentLoanTiledView
    extends RequestHandlingTiledViewBase implements TiledView, RequestHandler {

    public static Map FIELD_DESCRIPTORS;
    public static final String CHILD_STCOMPONENTTYPE = "stComponentType";
    public static final String CHILD_STCOMPONENTTYPE_RESET_VALUE = "";
    public static final String CHILD_STPRODUCTNAME = "stProductName";
    public static final String CHILD_STPRODUCTNAME_RESET_VALUE = "";
    public static final String CHILD_STAMOUNT = "stAmount";
    public static final String CHILD_STAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STNETRATE = "stNetRate";
    public static final String CHILD_STNETRATE_RESET_VALUE = "";
    public static final String CHILD_STDISCOUNT = "stDiscount";
    public static final String CHILD_STDISCOUNT_RESET_VALUE = "";
    public static final String CHILD_STPREMIUM = "stPremium";
    public static final String CHILD_STPREMIUM_RESET_VALUE = "";

    /** ******************MCM Impl Team XS_16.15 ********************** */
    public static final String CHILD_STPAYMENTTERMDESCRIPTION = "stPaymentTermDescription";
    public static final String CHILD_STPAYMENTTERMDESCRIPTION_RESET_VALUE = "";
    public static final String CHILD_STPAYMENTFREQUENCY = "stPaymentFrequency";
    public static final String CHILD_STPAYMENTFREQUENCY_RESET_VALUE = "";
    public static final String CHILD_STACTUALPAYMENTTERMMONTH = "stActualPaymentTermMonth";
    public static final String CHILD_STACTUALPAYMENTTERMMONTH_RESET_VALUE = "";
    public static final String CHILD_STACTUALPAYMENTTERMYEAR = "stActualPaymentTermYear";
    public static final String CHILD_STACTUALPAYMENTTERMYEAR_RESET_VALUE = "";
    public static final String CHILD_STPOSTEDRATE = "stPostedRate";
    public static final String CHILD_STPOSTEDRATE_RESET_VALUE = "";
    public static final String CHILD_STTOTALPAYMENT = "stTotalPayment";
    public static final String CHILD_STTOTALPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STADDITIONALDETAILS = "stAdditionalDetails";
    public static final String CHILD_STADDITIONALDETAILS_RESET_VALUE = "";

    /** *************************************************************** */

    /**
     * Models used by this class to display information.
     */
    private doComponentLoanModel doComponentLoanModel = null;

    /**
     * Handler class used by this class to display information.
     */
    private DealSummaryHandler handler = new DealSummaryHandler();

    /**
     * This Constructor registers the children.
     * @param View
     *            object in which this tile is a part of
     * @param name
     *            of this tiled view
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public pgDealSummaryComponentLoanTiledView (View parent, String name) {
        super(parent, name);
        setMaxDisplayTiles(100);
        setPrimaryModelClass(doComponentLoanModel.class);
        registerChildren();
        initialize();

    }

    /**
     * Initialize.
     */
    protected void initialize () {

    }

    /**
     * Description: This method resets the children which are fields on the
     * corresponding JSP
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 17-June-2008 XS_16.15
     *              Added necessary changes required for the new tiled view for loan infomation section.
     */
    public void resetChildren () {

        getStComponentType().setValue(CHILD_STCOMPONENTTYPE_RESET_VALUE);
        getStProductName().setValue(CHILD_STPRODUCTNAME_RESET_VALUE);
        getStAmount().setValue(CHILD_STAMOUNT_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getStDiscount().setValue(CHILD_STDISCOUNT_RESET_VALUE);
        getStPremium().setValue(CHILD_STPREMIUM_RESET_VALUE);
        getStPaymentTermDescription()
            .setValue(CHILD_STPAYMENTTERMDESCRIPTION_RESET_VALUE);
        getStPaymentFrequency().setValue(CHILD_STPAYMENTFREQUENCY_RESET_VALUE);
        getStPostedRate().setValue(CHILD_STPOSTEDRATE_RESET_VALUE);
        getStActualPaymentTermMonth()
            .setValue(CHILD_STACTUALPAYMENTTERMMONTH_RESET_VALUE);
        getStActualPaymentTermYear()
            .setValue(CHILD_STACTUALPAYMENTTERMYEAR_RESET_VALUE);
        getStTotalPayment().setValue(CHILD_STTOTALPAYMENT_RESET_VALUE);
        getStAdditionalDetails().setValue(CHILD_STADDITIONALDETAILS_RESET_VALUE);

    }

    /**
     * Description: This method registers the children which are fields on the
     * corresponding JSP.
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 17-June-2008 XS_16.15
     *              Added necessary changes required for the new tiled view for loan infomation section.
     */
    protected void registerChildren () {

        registerChild(CHILD_STCOMPONENTTYPE, StaticTextField.class);
        registerChild(CHILD_STPRODUCTNAME, StaticTextField.class);
        registerChild(CHILD_STAMOUNT, StaticTextField.class);
        registerChild(CHILD_STNETRATE, StaticTextField.class);
        registerChild(CHILD_STDISCOUNT, StaticTextField.class);
        registerChild(CHILD_STPREMIUM, StaticTextField.class);
        registerChild(CHILD_STPAYMENTTERMDESCRIPTION, StaticTextField.class);
        registerChild(CHILD_STPAYMENTFREQUENCY, StaticTextField.class);
        registerChild(CHILD_STPOSTEDRATE, StaticTextField.class);
        registerChild(CHILD_STACTUALPAYMENTTERMMONTH, StaticTextField.class);
        registerChild(CHILD_STACTUALPAYMENTTERMYEAR, StaticTextField.class);
        registerChild(CHILD_STTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_STADDITIONALDETAILS, StaticTextField.class);

    }

    /**
     * Description: Adds all models to the model list
     * <p>.
     * @param executionType
     *            the execution type
     * @return model[]- List of models used by this viewbean
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public Model [] getWebActionModels (int executionType) {

        List modelList = new ArrayList();

        switch (executionType) {

        case MODEL_TYPE_RETRIEVE:
            modelList.add(getDoComponentLoanModel());
            ;

            break;

        case MODEL_TYPE_UPDATE:
            ;

            break;

        case MODEL_TYPE_DELETE:
            ;

            break;

        case MODEL_TYPE_INSERT:
            ;

            break;

        case MODEL_TYPE_EXECUTE:
            ;

            break;

        }

        return (Model []) modelList.toArray(new Model[0]);

    }

    /**
     * Description:This method creates the handler objects, saves the page state
     * and also populates the combo box fields. Also resets the tile index.
     * <p>
     * @param event -
     *            DisplayEvent which triggered this call.
     * @return void
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public void beginDisplay (DisplayEvent event) throws ModelControlException {

        if (getPrimaryModel() == null) {

            throw new ModelControlException("Primary model is null");

        }

        super.beginDisplay(event);
        resetTileIndex();

    }

    /**
     * Description: This method moves the control from current tile to next
     * tile.
     * @return boolean - true, if the tile has moved. False otherwise.
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 25-June-2008 XS_16.15 Updadted nextTile() method:-Added code to display ActualPaymentTerm.
     */
    public boolean nextTile () throws ModelControlException {

        boolean movedToRow = super.nextTile();
          if (movedToRow) {
            DealSummaryHandler handler = (DealSummaryHandler)this.handler.cloneSS();
            handler.pageGetState(this.getParentViewBean());
            handler.setLoanCompDisplayFields(getTileIndex());
            handler.pageSaveState();
           }

        return movedToRow;

    }

    /**
     * <p>nextTile</p>
     * <p> to be calles from child to skip this tiled nextTile to avoid confliction
     * @param skipThisTile
     * @return
     * @throws ModelControlException
     * 
     * @version 1.0 July 17, 2008 MCM Team for XS 2.8
     */
    protected boolean nextTile (boolean skipThisTile) throws ModelControlException {
        
        if (skipThisTile) {
            return super.nextTile();
        } else return nextTile();
    }


    /**
     * Description: Creates the children represented by fields on the JSP.
     * @param name -
     *            String name of the child
     * @return View - JATO field representing the child
     * @exception IllegalArgumentException
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 17-June-2008 XS_16.15
     *              Added necessary changes required for the new tiled view for loan infomation section.
     */
    protected View createChild (String name) {

        if (name.equals(CHILD_STCOMPONENTTYPE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STCOMPONENTTYPE,
                                                        doComponentLoanModel.FIELD_DFCOMPONENTTYPE,
                                                        CHILD_STCOMPONENTTYPE_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STPRODUCTNAME)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STPRODUCTNAME,
                                                        doComponentLoanModel.FIELD_DFPRODUCTNAME,
                                                        CHILD_STPRODUCTNAME_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STAMOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STAMOUNT,
                                                        doComponentLoanModel.FIELD_DFLOANAMOUNT,
                                                        CHILD_STAMOUNT_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STNETRATE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STNETRATE,
                                                        doComponentLoanModel.FIELD_DFNETINTRESTRATE,
                                                        CHILD_STNETRATE_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STDISCOUNT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STDISCOUNT,
                                                        doComponentLoanModel.FIELD_DFDISCOUNT,
                                                        CHILD_STDISCOUNT_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STPREMIUM)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STPREMIUM,
                                                        doComponentLoanModel.FIELD_DFPREMIUM,
                                                        CHILD_STPREMIUM_RESET_VALUE,
                                                        null);

            return child;

        }
        /************************MCM Impl Team XS_16.15 changes starts **************************/
         
        else if (name.equals(CHILD_STPAYMENTTERMDESCRIPTION)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STPAYMENTTERMDESCRIPTION,
                                                        doComponentLoanModel.FIELD_DFPAYMENTTERMDESCRIPTION,
                                                        CHILD_STPAYMENTTERMDESCRIPTION_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STPAYMENTFREQUENCY)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STPAYMENTFREQUENCY,
                                                        doComponentLoanModel.FIELD_DFPAYMENTFREQUENCY,
                                                        CHILD_STPAYMENTFREQUENCY_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STPOSTEDRATE)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STPOSTEDRATE,
                                                        doComponentLoanModel.FIELD_DFPOSTEDRATE,
                                                        CHILD_STPOSTEDRATE_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STACTUALPAYMENTTERMMONTH)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDefaultModel(),
                                                        CHILD_STACTUALPAYMENTTERMMONTH,
                                                        CHILD_STACTUALPAYMENTTERMMONTH,
                                                        CHILD_STACTUALPAYMENTTERMMONTH_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STACTUALPAYMENTTERMYEAR)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDefaultModel(),
                                                        CHILD_STACTUALPAYMENTTERMYEAR,
                                                        CHILD_STACTUALPAYMENTTERMYEAR,
                                                        CHILD_STACTUALPAYMENTTERMYEAR_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STTOTALPAYMENT)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STTOTALPAYMENT,
                                                        doComponentLoanModel.FIELD_DFTOTALPAYMENT,
                                                        CHILD_STTOTALPAYMENT_RESET_VALUE,
                                                        null);

            return child;

        } else if (name.equals(CHILD_STADDITIONALDETAILS)) {

            StaticTextField child = new StaticTextField(this,
                                                        getDoComponentLoanModel(),
                                                        CHILD_STADDITIONALDETAILS,
                                                        doComponentLoanModel.FIELD_DFADDITIONALDETAILS,
                                                        CHILD_STADDITIONALDETAILS_RESET_VALUE,
                                                        null);

            return child;

        }
        /** ******************************MCM Impl XS_16.15 changes ends************************************************ */
        else {

            throw new IllegalArgumentException("Invalid child name [" + name +
                                               "]");

        }

    }

    /**
     * Getters and setters for the display fields starts
     */
    public StaticTextField getStDiscount () {

        return (StaticTextField) getChild(CHILD_STDISCOUNT);

    }

    public StaticTextField getStPremium () {

        return (StaticTextField) getChild(CHILD_STPREMIUM);

    }

    public StaticTextField getStComponentType () {

        return (StaticTextField) getChild(CHILD_STCOMPONENTTYPE);

    }

    public StaticTextField getStProductName () {

        return (StaticTextField) getChild(CHILD_STPRODUCTNAME);

    }

    public StaticTextField getStNetRate () {

        return (StaticTextField) getChild(CHILD_STNETRATE);

    }

    public StaticTextField getStAmount () {

        return (StaticTextField) getChild(CHILD_STAMOUNT);

    }

    /** ***********************MCM Impl XS_16.15 changes starts **************************** */
    public StaticTextField getStPaymentTermDescription () {

        return (StaticTextField) getChild(CHILD_STPAYMENTTERMDESCRIPTION);

    }

    public StaticTextField getStPaymentFrequency () {

        return (StaticTextField) getChild(CHILD_STPAYMENTFREQUENCY);

    }

    public StaticTextField getStPostedRate () {

        return (StaticTextField) getChild(CHILD_STPOSTEDRATE);

    }

    public StaticTextField getStActualPaymentTermMonth () {

        return (StaticTextField) getChild(CHILD_STACTUALPAYMENTTERMMONTH);

    }

    public StaticTextField getStActualPaymentTermYear () {

        return (StaticTextField) getChild(CHILD_STACTUALPAYMENTTERMYEAR);

    }

    public StaticTextField getStTotalPayment () {

        return (StaticTextField) getChild(CHILD_STTOTALPAYMENT);

    }

    public StaticTextField getStAdditionalDetails () {

        return (StaticTextField) getChild(CHILD_STADDITIONALDETAILS);

    }

    /** *****************************MCM Impl XS_16.15 changes ends*************************************** */
  
    /**
     * Getters and setters for the display fields ends.
     */

    /**
     * <p>
     * Description: This method returns the model of type doComponentLoanModel
     * used by this class. If the model has not been set, an instance is
     * created.
     * </p>
     * @returns doComponentLoanModel - the model used by this view
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    public doComponentLoanModel getDoComponentLoanModel () {

        if (doComponentLoanModel == null) {

            doComponentLoanModel = (doComponentLoanModel) getModel(doComponentLoanModel.class);

        }

        return doComponentLoanModel;

    }

}
