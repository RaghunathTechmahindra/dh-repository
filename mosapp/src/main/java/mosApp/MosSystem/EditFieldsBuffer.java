package mosApp.MosSystem;

import java.text.*;
import java.util.*;
import java.io.Serializable;

import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

/**
*
*  Utility class for automating collect/population of simple (form like) ND screen - e.g. deal
*  serch, party search, etc.
*
*  Extending class maintains the actual data including field names, etc.
*
*
**/
public abstract class EditFieldsBuffer extends Object
	implements Serializable
{
	protected static final int FT_UNKNOWN=0;
	protected static final int FT_TEXTBOX=1;
	protected static final int FT_COMBOBOX=2;
	protected static final int FT_STATIC=3;
	protected static final int FT_HIDDEN=4;
  protected static final int FT_LISTBOX=5;
	int numberOfFields=0;
	String name;
	SysLogger logger;
  int languageId;


	/**
	 *
	 *
	 */
	public EditFieldsBuffer(String name, SysLogger logger)
	{
		this.name = name;

		this.logger = logger;
	}

  //--Release2.1--//
  //// New constructor in order to translate a buffer element on fly when a user
  //// toggles the toggleLanguage link.
	public EditFieldsBuffer(String name, SysLogger logger, int languageId)
	{
		this.name = name;

		this.logger = logger;

    this.languageId = languageId;
	}

	// -- //

	public abstract String getFieldName(int fldNdx);


	/**
	 *
	 *
	 */
	public abstract String getDOFieldName(int fldNdx);


	/**
	 *
	 *
	 */
	public abstract String getFieldType(int fldNdx);


	/**
	 *
	 *
	 */
	public abstract String getFieldValue(int fldNdx);


	/**
	 *
	 *
	 */
	public abstract String getOriginalValue(int fldNdx);


	// --- //

	public abstract void setFieldName(int fldNdx, String value);


	/**
	 *
	 *
	 */
	public abstract void setDOFieldName(int fldNdx, String value);


	/**
	 *
	 *
	 */
	public abstract void setFieldType(int fldNdx, String value);


	/**
	 *
	 *
	 */
	public abstract void setFieldValue(int fldNdx, String value);


	/**
	 *
	 *
	 */
	public abstract void setOriginalValue(int fldNdx, String value);


	// --- //

	public int getNumberOfFields()
	{
		return numberOfFields;

	}


	/**
	 *
	 *
	 */
	public void setNumberOfFields(int num)
	{
		numberOfFields = num;

	}


	/**
	 *
	 *
	 */
	public void setOriginalAsCurrent()
	{
		int lim = getNumberOfFields();

		for(int i = 0;	i < lim;	++ i)
		{
        setOriginalValue(i, getFieldValue(i));
		}
	}

  // Method to check if field value changed -- Check for all possible matches
  public String getFieldValueIfModified(int ndx)
	{
    String s = null;
    // Check if field changed in String format
    s = getFieldStringValueIfModified(ndx);
    // Check if it matched in Int format
    if(s != null) s = getFieldIntValueIfModified(ndx);
    // Check if it matched in Double format
    if(s != null) s = getFieldDoubleValueIfModified(ndx);

    return s;
  }
  public String getFieldValueIfModified(String fldName)
	{
    int ndx = getFieldNdx(fldName);
    return getFieldValueIfModified(ndx);
  }

	// New method to check if field String value changed
	public String getFieldStringValueIfModified(int fldNdx)
	{
    if (fldNdx == - 1) return null;
		String original = getDFStr(getOriginalValue(fldNdx));
		String current = getDFStr(getFieldValue(fldNdx));
		if (original.equals(current)) return null;

		//logger.debug("BILLY ===> @getFieldValueIfModified : " + getFieldName(fldNdx)
		//    + " Changed : original = " + original + " current = " + current);
		return current;
	}
	public String getFieldStringValueIfModified(String fldName)
	{
		int ndx = getFieldNdx(fldName);
		return getFieldStringValueIfModified(ndx);
	}

  // New method to check if field int value changed
  public String getFieldIntValueIfModified(int ndx)
	{
		if (ndx == - 1) return null;

		// an error actually ... but ignored
		String original = getDFStr(getOriginalValue(ndx));
		String current = getDFStr(getFieldValue(ndx));

    // If either one is not in Integer format
    if(isIntValue(original) && isIntValue(current)&& (getIntValue(original) == getIntValue(current)))
      return null;

		return current;
	}
  public String getFieldIntValueIfModified(String fldName)
	{
		int ndx = getFieldNdx(fldName);
		return getFieldIntValueIfModified(ndx);
	}

  // New method to check if field double value changed
  public String getFieldDoubleValueIfModified(int ndx)
	{
		if (ndx == - 1) return null;

		// an error actually ... but ignored
		String original = getDFStr(getOriginalValue(ndx));
		String current = getDFStr(getFieldValue(ndx));

    // If either one is not in Integer format
    if(isDoubleValue(original) && isDoubleValue(current) &&
        (getDoubleValue(original) == getDoubleValue(current))
      )
      return null;

		return current;
	}
  public String getFieldDoubleValueIfModified(String fldName)
	{
		int ndx = getFieldNdx(fldName);
		return getFieldDoubleValueIfModified(ndx);
	}

	/**
	 *
	 *
	 */
	public String getFieldValue(String fldName)
	{
		int ndx = getFieldNdx(fldName);

		if (ndx == - 1) return null;


		// an error actually ... but ignored
		return getFieldValue(ndx);

	}


	/**
	 *
	 *
	 */
	public String getOriginalValue(String fldName)
	{
		int ndx = getFieldNdx(fldName);

		if (ndx == - 1) return null;


		// an error actually ... but ignored
		return getOriginalValue(ndx);

	}


	/**
	 *
	 *
	 */
	public void setFieldValue(String fldName, String value)
	{
		int ndx = getFieldNdx(fldName);
		if (ndx == - 1) return;
		// an error actually ... but ignored
		setFieldValue(ndx, value);
	}

  public void setOriginalValue(String fldName, String value)
	{
		int ndx = getFieldNdx(fldName);
		if (ndx == - 1) return;
		// an error actually ... but ignored
		setOriginalValue(ndx, value);
	}

	// --- //

	public boolean isModified()
	{
		String s = null;

		int lim = getNumberOfFields();

		for(int i = 0;
		i < lim;
		++ i)
		{
			s = getFieldValueIfModified(i);
			if (s != null) return true;
		}

		return false;

	}


	// return a string that uniquely identifies the current content of the buffer - this
	// is accomplished by concatenating field names and current contents

	public String getAsCriteria()
	{
		StringBuffer s = new StringBuffer();

		int lim = getNumberOfFields();

		for(int i = 0;
		i < lim;
		++ i)
		{
			s.append(getFieldName(i) + getFieldValue(i));
		}

		return s.toString();

	}


	// --- //

	public void populateFields(ViewBean thePage)
	{
		String s = null;

		String fldName = null;

		int lim = getNumberOfFields();

		for(int i = 0; i < lim;	++ i)
		{
			// first check for a mapping to a display field
			fldName = getFieldName(i);
			if (fldName == null || fldName.length() == 0) continue;
			switch(fldTypeNumeric(getFieldType(i)))
			{
				case FT_TEXTBOX :
        // break;
				case FT_HIDDEN :
        // break;
				case FT_STATIC :
        try
				{
					// must be rewritten as the following getDisplayFieldValue(int i).getValue() method could be called
					// only after this
					thePage.setDisplayFieldValue(fldName, new String(getDFStr(getFieldValue(i))));
				}
				catch(Exception e)
				{
					logger.error("Exception @EditFieldsBuffer(name=" + name + ").populateFields: populating field, fileName=" + getFieldName(i) + ", value=<" + getFieldValue(i) + ">, CSpPage=" + thePage + " - ignored");
					logger.error(e);
				}
				break;
				case FT_COMBOBOX :
        try
				{
					s = getDFStr(getFieldValue(i));

          //logger.debug("EFB@populateFields::StringHtmlName: " + fldName);
          //logger.debug("EFB@populateFields::SValue: " + s);
					//ComboBox comboOptions = (ComboBox)thePage.getDisplayField(fldName);
          //logger.debug("EFB@populateFields::ComboOptionsName: " + comboOptions.getName());
          thePage.setDisplayFieldValue(fldName, new String(s));
				}
				catch(Exception e)
				{
					logger.error("Exception @EditFieldsBuffer(name=" + name + ").populateFields: populating field, fileName=" + getFieldName(i) + ", value=<" + getFieldValue(i) + ">, CSpPage=" + thePage + " - ignored");
					logger.error(e);
				}
				break;

        default : logger.error("Warning @EditFieldsBuffer(name=" + name + ").populateFields: no support for for field type = " + getFieldType(i) + ", filedName=" + getFieldName(i) + " - don't know how to populate, input ignored");
				break;
			}
		}

	}


	// --- //

	public void readFields(ViewBean thePage)
	{
		int lim = getNumberOfFields();

		String s = null;

		// working vars

		String fldName = null;

		for(int i = 0; i < lim;	++ i)
		{
			// first check for a mapping to a display field
			fldName = getFieldName(i);
			if (fldName == null || fldName.length() == 0) continue;
			switch(fldTypeNumeric(getFieldType(i)))
			{
				case FT_TEXTBOX :
        //logger.debug("EditDisplayBuffer@FldName(textBox)	:: " + fldName);
				//logger.debug("EditDisplayBuffer@TextBoxValField :: " + thePage.getDisplayFieldValue(fldName));
				//logger.debug("EditDisplayBuffer@TextBoxValue :: " + thePage.getDisplayFieldStringValue(fldName));
				setFieldValue(i, thePage.getDisplayFieldValue(fldName).toString().trim());
				break;

				/**
					try
					{
					 // as the text box is automatically loaded with its value by
					 //looking for an associated web variable (CGI name-value pair)
					 // whose name is equal to this instance's name.

						 Object textBoxValue = getRequestContext().getRequest().getParameter(fldName);
             logger.debug("EditDisplayBuffer@StaticValueFromWebVar :: " + textBoxValue);

					}
					catch (Exception e)
					{
					logger.error("Exception @EditFieldsBuffer(name=" + name +
					            ").readFields: reading text field, fileName=" +
					              fldName + ", CSpPage=" + thePage + ", fielType="
					              + getFieldType(i) + " - field ignored");
					logger.error(e);
					}

					break;
**/
				case FT_HIDDEN :
        // temp commented out
				//logger.debug("EditDisplayBuffer@FldName(hidden)	:: " + fldName);
				//logger.debug("EditDisplayBuffer@HiddenValField :: " + thePage.getDisplayFieldValue(fldName));
				//logger.debug("EditDisplayBuffer@HiddenBoxValue :: " + thePage.getDisplayFieldStringValue(fldName));
				setFieldValue(i, thePage.getDisplayFieldValue(fldName).toString().trim());
				break;
				case FT_STATIC : try
				{
					// orig
					//We cannot get the field value from a StaticText ==> preserve the previous value
					//  -- By BILLY 12April2002
					//setFieldValue(i, thePage.getDisplayFieldStringValue(fldName).trim());
					//logger.debug("EditDisplayBuffer@FldName(static)	:: " + fldName);
					//logger.debug("EditDisplayBuffer@StaticValField :: " + thePage.getDisplayFieldValue(fldName));
					//logger.debug("EditDisplayBuffer@StaticValue :: " + thePage.getDisplayFieldStringValue(fldName));
				}
				catch(Exception e)
				{
					logger.error("Exception @EditFieldsBuffer(name=" + name + ").readFields: reading text field, fileName=" + fldName + ", CSpPage=" + thePage + ", fielType=" + getFieldType(i) + " - field ignored");
					logger.error(e);
				}
				break;
				case FT_COMBOBOX :
        // assume the value we retrieve from the combo box is numeric
				try
				{
					int Id = 0;
					String strVal = "";
					s = thePage.getDisplayFieldValue(getFieldName(i)).toString();
					if (s == null || s.trim().length() == 0) setFieldValue(i, "");

					// ... otherwise we insist on numeric option value
					// Original version is temporary commented out to pick up the values from the composite
					// ComboBox Line
					//						if (thePage.getDisplayFieldValue(getFieldName(i)).isNumeric() == false)
					//							break;
					//                       setFieldValue(i, thePage.getDisplayFieldValue(fldName).toString());
					// This version should be moved to the bridge review handler
					// level or replaced with the generic approach (see below).
					strVal = getStrValCompComboLine(s);
					Id = getIntValCompComboLine(s);

					//temp
					//logger.debug("EditDisplayBuffer@FldName(ComboBox)	:: " + fldName);
					//logger.debug("EditDisplayBuffer@ComboBoxValField :: " + thePage.getDisplayFieldValue(fldName));
					//logger.debug("EditFieldsBuffer@StringValue :: " + strVal);
					//logger.debug("EditFieldsBuffer@IntValue :: " + Id);

					setFieldValue(i, thePage.getDisplayFieldValue(fldName).toString());
				}
				catch(Exception e)
				{
					logger.error("Exception @EditFieldsBuffer(name=" + name + ").readFields: reading combobox field, fileName=" + fldName + ", CSpPage=" + thePage + " - field input ignored");
					logger.error(e);
				}
				break;
				default : logger.error("Warning @EditFieldsBuffer(name=" + name + ").readFields: no support for for field type = " + getFieldType(i) + ", filedName=" + getFieldName(i) + " - input ignored");
				break;
			}
		}

	}


	/**
	*
	* Data object must be prepared for execute (e.g. dynamic criteria set) - only the
	* first row or the result (executed here) is examined.
	*
	**/
	public void readFields(QueryModelBase dataObj)
	{
		//spider.database.CSpDBResultTable resultTab = null;

		try
		{
			dataObj.executeSelect(null);
			//resultTab = dataObj.getLastResults().getResultTable();
		}
		catch(Exception e)
		{
			logger.error("Exception @EditFieldsBuffer(name=" + name + ").readFields: executing data object, DO=" + dataObj + ", - ignored");
			logger.error(e);
			return;
		}

		int lim = getNumberOfFields();

		String doFieldName = null;

		for(int i = 0; i < lim;	++ i)
		{
			// first check for a mapping to a data object field
			doFieldName = getDOFieldName(i);
      //logger.debug("EFB@readFields::doFieldName: " + doFieldName);

			if (doFieldName == null || doFieldName.length() == 0) continue;
			try
			{
				//int doFldNdx = dataObj.getDataFieldPosition(doFieldName);
				//setFieldValue(i, resultTab.getValue(0, doFldNdx).toString());
        setFieldValue(i, dataObj.getValue(doFieldName).toString());
        //logger.debug("EFB@readFields::Value: " + dataObj.getValue(doFieldName).toString());
			}
			catch(Exception e)
			{
				logger.warning("Exception @EditFieldsBuffer(name=" + name + ").readFields: reading from data object, doFileName=" + getDOFieldName(i) + ", DO=" + dataObj + " - field ignored");
				logger.warning(e.toString());
			}
		}

	}


	//
	// Private
	//

	protected int getFieldNdx(String fldName)
	{
		if (fldName == null || fldName.trim().length() == 0) return - 1;

		fldName = fldName.trim();

		int lim = getNumberOfFields();

		for(int i = 0;
		i < lim;
		++ i)
		{
			if (fldName.equals(getFieldName(i))) return i;
		}

		return - 1;

	}


	/**
	 *
	 *
	 */
	protected int fldTypeNumeric(String fldType)
	{
		if (fldType == null || fldType.length() == 0) return FT_UNKNOWN;

		if (fldType.equals("TextBox")) return FT_TEXTBOX;

		if (fldType.equals("ComboBox")) return FT_COMBOBOX;

		if (fldType.equals("StaticText")) return FT_STATIC;

		if (fldType.equals("Hidden")) return FT_HIDDEN;

    if (fldType.equals("ListBox")) return FT_LISTBOX;

		return FT_UNKNOWN;

	}


	/**
	 *
	 *
	 */
	protected String getDFStr(String s)
	{
		if (s == null) return "";

		return s;

	}


	/**
	*
	*  Expected:  aaaxxxxxxx or xxxxxxx
	*
	**/
	protected String formatPhoneNumber(String phStr)
	{
		if (phStr == null || phStr.trim().length() == 0) return "";

		if (phStr.length() <= 7)
		{
			phStr = "   " + phStr;
		}

		phStr = phStr + "                   ";

		return phStr.substring(0, 3) + " " + phStr.substring(3, 6) + " " + phStr.substring(6, 10);

	}


	// this should be adjusted to an arbitrary number of elements for a composite
	// combo box value .... String, int, String, .... e.g., in arbitrary order of them

	private String getStrValCompComboLine(String sCompositeComboLine)
	{
		if (sCompositeComboLine == null) return "";

		sCompositeComboLine = sCompositeComboLine.trim();

		if (sCompositeComboLine.length() == 0) return "";

		try
		{
			// handle the first String element of a composite combo box value
			int ndx = sCompositeComboLine.indexOf(",");
			if (ndx != - 1) sCompositeComboLine = sCompositeComboLine.substring(0, ndx);
			return sCompositeComboLine.trim();
		}
		catch(Exception e)
		{
			;
		}

		return "";

	}


	/**
	 *
	 *
	 */
	private int getIntValCompComboLine(String sCompositeComboLine)
	{
		if (sCompositeComboLine == null) return 0;

		sCompositeComboLine = sCompositeComboLine.trim();

		if (sCompositeComboLine.length() == 0) return 0;

		try
		{
			// handle the second int element of a composite combo box value
			int ndx = sCompositeComboLine.indexOf(",");
			if (ndx != - 1) sCompositeComboLine = sCompositeComboLine.substring(ndx + 1);
			return getIntValue(sCompositeComboLine.trim());
		}
		catch(Exception e)
		{
			;
		}

		return 0;

	}


	/**
	 *
	 *
	 */
	protected int getIntValue(String sNum)
	{
		if (sNum == null) return 0;

		sNum = sNum.trim();

		if (sNum.length() == 0) return 0;

		try
		{
			// handle decimal (e.g. float type) representation
			int ndx = sNum.indexOf(".");
			if (ndx != - 1) sNum = sNum.substring(0, ndx);
			return Integer.parseInt(sNum.trim());
		}
		catch(Exception e)
		{
			;
		}

		return 0;
	}

  protected double getDoubleValue(String sNum)
	{
		if (sNum == null) return 0.0;
		sNum = sNum.trim();
		if (sNum.length() == 0) return 0.0;

		try
		{
			return Double.parseDouble(sNum.trim());
		}
		catch(Exception e)
		{
			;
		}

		return 0.0;
	}

  //New method to check if Field value is Integer
  protected boolean isIntValue(String sNum)
	{
    //Treated Null and Null String as Int
		if (sNum == null) return true;
		sNum = sNum.trim();
		if (sNum.length() == 0) return true;

		try
		{
			Integer.parseInt(sNum.trim());
		}
		catch(Exception e)
		{
			return false;
		}

		return true;
	}

  //New method to check if Field value is Double
  protected boolean isDoubleValue(String sNum)
	{
    //Treated Null and Null String as Double 0.0
		if (sNum == null) return true;
		sNum = sNum.trim();
		if (sNum.length() == 0) return true;

		try
		{
			Double.parseDouble(sNum.trim());
		}
		catch(Exception e)
		{
			return false;
		}

		return true;
	}


	// Function for reading of an arbitrary number of nameValue, nameId pairs from
	// the composite ComboBox line (nameValue1, nameId1, nameValue2, nameId2, etc.)
	// NetDynamics forms this composite combobox line (AutoFillComboBox line in the
	// wizard. The real format of this line is as follows:
	//   [ Bridge Secured,1.0,ING,3.0,5 Years Closed,7.0,.....]
	// Comma is the separator for each element (NetD default) and the Ids are represented
	// by an integer 1.0, etc.
	// IMPORTANT: We are assuming here that nameId must follow by the an appropriate
	// nameValue. Output is a hash containg the Ids as keys. This approach is useful
	// when we need to add/update an entity evolving a nubmer of description tables.
	// It gives the Ids for entity without extra processing as NetD treats them behind
	// the scene anyway.

	public Hashtable splitCompositeComboBoxLine(String linein)
	{
		Hashtable hashout = new Hashtable();

		StringTokenizer st = new StringTokenizer(linein, ",", false);

		while(st.hasMoreElements())
		{
			//start reading the value
			//read up to the first comma
			String value = st.nextToken();
			Integer key = null;

			//start reading past comma
			String tmp = null;
			while((tmp = value) != null)
			{
				//read next token, analyze it
				if (tmp == null)
				{
					//we've got a problem - throw an exception
					//	...
				}
				try
				{
					//if we succeed here, this means we have a key, immediately start reading a new pair
					key = new Integer(getIntValue(tmp));
					break;
				}
				catch(Exception e)
				{
					//if exception is thrown, it must have been an internal comma, so append the token
					value += tmp;
				}
			}

			//hash that needs to be updated with the new key-value pair
			hashout.put(key, value);
		}


		//outer while
		return hashout;

	}

}

