package mosApp.MosSystem;

import java.math.BigDecimal;
import java.util.*;
import java.sql.*;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

public interface doFixedPriceClosingInfoModel extends QueryModel,
        SelectQueryModel {

    
    /*
     * Field bound definition
     */
    public final static String FIELD_DFFPCDEALID =
        "dfFPCDealId";

    public final static String FIELD_DFFPCCOPYID =
        "dfFPCCopyId";

    public final static String FIELD_DFFPCREQUESTID =
        "dfFPCRequestId";
    
    public final static String FIELD_DFFPCCONTACTID =
        "dfFPCContactId";

    public final static String FIELD_DFFPCPROVIDERID =
        "dfFPCProviderId";

    public final static String FIELD_DFFPCPRODUCTID =
        "dfFPCProductId";
    
    public static final String FIELD_DFFPCREQUESTSTATUS =
        "dfFPCRequestStatus";

	public static final String FIELD_DFFPCREQUESTSTATUSID =
        "dfFPCRequestStatusId";
    
    public final static String FIELD_DFFPCREQUESTDATE =
        "dfFPCRequestDate";

        
    public final static String FIELD_DFFPCCONTACTFIRSTNAME =
        "dfFPCContactFirstName";
    
    public final static String FIELD_DFFPCCONTACTLASTNAME = 
        "dfFPCContactLastName";
    
    public final static String FIELD_DFFPCEMAIL =
        "dfFPCEmail";
    
    public final static String FIELD_DFFPCWORKPHONEAREACODE =
        "dfFPCWorkPhoneAreaCode";
        
    public final static String FIELD_DFFPCWORKPHONEFIRSTTHREEDIGITS =
        "dfFPCWorkPhoneFirstThreeDigits";

    public final static String FIELD_DFFPCWORKPHONELASTFOURDIGITS =
        "dfFPCWorkPhoneLastFourDigits";
    
    public final static String FIELD_DFFPCWORKPHONENUMEXTENSION =
        "dfFPCWorkPhoneNumExtension";
    
    public final static String FIELD_DFFPCCELLPHONEAREACODE =
        "dfFPCCellPhoneAreaCode";
        
    public final static String FIELD_DFFPCCELLPHONEFIRSTTHREEDIGITS =
        "dfFPCCellPhoneFirstThreeDigits";
    
    public final static String FIELD_DFFPCCELLPHONELASTFOURDIGITS =
        "dfFPCCellPhoneLastFourDigits";
    
    public final static String FIELD_DFFPCHOMEPHONEAREACODE =
        "dfFPCHomePhoneAreaCode";
    
    public final static String FIELD_DFFPCHOMEPHONEFIRSTTHREEDIGITS =
        "dfFPCHomePhoneFirstThreeDigits";
    
    public final static String FIELD_DFFPCHOMEPHONELASTFOURDIGITS =
        "dfFPCHomePhoneLastFourDigits";

    public final static String FIELD_DFFPCCOMMENTS = "dfFPCComments";
    
    public final static String FIELD_DFFPCBORROWERID = "dfFPCBorrowerId";
    
    public final static String FIELD_DFFPCREQUESTMESSAGE = "dfFPCRequestMessage";
    
    
    public final static String FIELD_DFFPCPROVIDERREFNO ="dfFPCProviderRefNo";
    
    /*
     * Getter and Setter
     */
    
    public BigDecimal getDfFPCProviderId();
    public void setDfFPCProviderId(BigDecimal id);
    
    public BigDecimal getDfFPCProductId();
    public void setDfFPCProductId(BigDecimal id);

    public String getDfFPCRequestStatus();
    public void setDfFPCRequestStatus(String dfFPCRequestStatus);

	public BigDecimal getDfFPCRequestStatusId();
    public void setDfFPCRequestStatusId(BigDecimal dfFPCRequestStatusId);
    
    public Timestamp getDfFPCRequestDate();
    public void setDfFPCRequestDate(Timestamp date);
    
    public String getDfFPCContactFirstName();
    public void setDfFPCContactFirstName(String dfFPCContactFirstName);
    
    public String getDfFPCContactLastName();
    public void setDfFPCContactLastName(String dfFPCContactLastName);

    public String getDfFPCEmail();
    public void setDfFPCEmail(String dfFPCEmail);

    public String getDfFPCWorkPhoneAreaCode();
    public void setDfFPCWorkPhoneAreaCode(String tbWorkPhoneAreaCode);
        
    public String getDfFPCWorkPhoneFirstThreeDigits();
    public void setDfFPCWorkPhoneFirstThreeDigits(String tbWorkPhoneFirstThreeDigits);

    public String getDfFPCWorkPhoneLastFourDigits();
    public void setDfFPCWorkPhoneLastFourDigits(String tbWorkPhoneLastFourDigits);
    
    public String getDfFPCWorkPhoneNumExtension();
    public void setDfFPCWorkPhoneNumExtension(String tbWorkPhoneNumExtension);
    
    public String getDfFPCCellPhoneAreaCode();
    public void setDfFPCCellPhoneAreaCode(String tbCellPhoneAreaCode);
        
    public String getDfFPCCellPhoneFirstThreeDigits();
    public void setDfFPCCellPhoneFirstThreeDigits(String tbCellPhoneFirstThreeDigits);

    public String getDfFPCCellPhoneLastFourDigits();
    public void setDfFPCCellPhoneLastFourDigits(String tbCellPhoneLastFourDigits);

    public String getDfFPCHomePhoneAreaCode();
    public void setDfFPCHomePhoneAreaCode(String dfFPCHomePhoneAreaCode);
    
    public String getDfFPCHomePhoneFirstThreeDigits();
    public void setDfFPCHomePhoneFirstThreeDigits(String dfFPCHomePhoneFirstThreeDigits);

    public String getDfFPCHomePhoneLastFourDigits();
    public void setDfFPCHomePhoneLastFourDigits(String dfFPCHomePhoneLastFourDigits);

    
    public String getDfFPCComments();
    public void setDfFPCComments(String dfFPCComments);
    
    public BigDecimal getDfFPCBorrowerId();
    public void setDfFPCBorrowerId(BigDecimal dfFPCBorrowerId);

    public String getDfFPCRequestMessage();
    public void setDfFPCRequestMessage(String dfFPCRequestMessage);
    
    public BigDecimal getDfFPCRequestId();
    public void setDfFPCRequestId(BigDecimal id);
    
    public BigDecimal getDfFPCCopyId();
    public void setDfFPCCopyId(BigDecimal id);

    public BigDecimal getDfFPCContactId();
    public void setDfFPCContactId(BigDecimal id);


}
