package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doGetBranchNameModelImpl extends QueryModelBase
	implements doGetBranchNameModel
{
	/**
	 *
	 *
	 */
	public doGetBranchNameModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert Branch name.
      this.setDfBranchName(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "BRANCHPROFILE", this.getDfBranchProfileId().intValue(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBranchProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRANCHPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRANCHPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBranchName()
	{
		return (String)getValue(FIELD_DFBRANCHNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBranchName(String value)
	{
		setValue(FIELD_DFBRANCHNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBranchBusinessId()
	{
		return (String)getValue(FIELD_DFBRANCHBUSINESSID);
	}


	/**
	 *
	 *
	 */
	public void setDfBranchBusinessId(String value)
	{
		setValue(FIELD_DFBRANCHBUSINESSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBranchShortName()
	{
		return (String)getValue(FIELD_DFBRANCHSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBranchShortName(String value)
	{
		setValue(FIELD_DFBRANCHSHORTNAME,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Not need to modify the SQL and the Multilingual Desc. is populated in afterExecute() method
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL BRANCHPROFILE.BRANCHPROFILEID, BRANCHPROFILE.BRANCHNAME, BRANCHPROFILE.BPBUSINESSID, BRANCHPROFILE.BPSHORTNAME,BRANCHPROFILE.INSTITUTIONPROFILEID FROM BRANCHPROFILE  __WHERE__  ";
  
	public static final String MODIFYING_QUERY_TABLE_NAME="BRANCHPROFILE";
	
	public static final String STATIC_WHERE_CRITERIA="";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	
	public static final String QUALIFIED_COLUMN_DFBRANCHPROFILEID="BRANCHPROFILE.BRANCHPROFILEID";
	public static final String COLUMN_DFBRANCHPROFILEID="BRANCHPROFILEID";
	public static final String QUALIFIED_COLUMN_DFBRANCHNAME="BRANCHPROFILE.BRANCHNAME";
	public static final String COLUMN_DFBRANCHNAME="BRANCHNAME";
	public static final String QUALIFIED_COLUMN_DFBRANCHBUSINESSID="BRANCHPROFILE.BPBUSINESSID";
	public static final String COLUMN_DFBRANCHBUSINESSID="BPBUSINESSID";
	public static final String QUALIFIED_COLUMN_DFBRANCHSHORTNAME="BRANCHPROFILE.BPSHORTNAME";
	public static final String COLUMN_DFBRANCHSHORTNAME="BPSHORTNAME";


    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "BRANCHPROFILE.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHPROFILEID,
				COLUMN_DFBRANCHPROFILEID,
				QUALIFIED_COLUMN_DFBRANCHPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHNAME,
				COLUMN_DFBRANCHNAME,
				QUALIFIED_COLUMN_DFBRANCHNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHBUSINESSID,
				COLUMN_DFBRANCHBUSINESSID,
				QUALIFIED_COLUMN_DFBRANCHBUSINESSID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHSHORTNAME,
				COLUMN_DFBRANCHSHORTNAME,
				QUALIFIED_COLUMN_DFBRANCHSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	}

}

