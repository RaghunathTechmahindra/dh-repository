package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>Title: pgComponentDetailsOverdraftTiledView</p>
 * <p>Description: TiledView Class for Overdraft Component section on Component Details Screen</p>
 *
 * @author MCM Team
 * @version 1.0 (Initial Version � July 2, 2008)
 * @version 1.1 delete Component section - XS_2.32 Modified - handleBtDeleteRequest()
 * @version 1.2 July 25, 2008 set MAXCOMPTILE_SIZE = 10 for artf750856 
 */
public class pgComponentDetailsOverdraftTiledView extends RequestHandlingTiledViewBase
implements TiledView, RequestHandler {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(pgComponentDetailsOverdraftTiledView.class);

    /**
     * <p>constructor</p>
     */
    public pgComponentDetailsOverdraftTiledView(View parent, String name) {
        super(parent, name);
        // MCM Team for artf750856
        setMaxDisplayTiles(pgComponentDetailsViewBean.MAXCOMPTILE_SIZE);
        setPrimaryModelClass(doComponentOverDraftModel.class);
        registerChildren();
        initialize();
    }

    /**
     * <p>initializer</p>
     */
    protected void initialize() {
    }

    /**
     * <p>createChild</p>
     * <p>Description: createChild method for Jato Framework</p>
     * 
     * @param name:String - child name
     * @return View 
     */
    protected View createChild(String name) {

        if (name.equals(CHILD_HDCOMPONENTID)) {
            HiddenField child 
            = new HiddenField(this,
                    getdoComponentOverDraftModel(),
                    CHILD_HDCOMPONENTID,
                    doComponentOverDraftModel.FIELD_DFCOMPONENTID,
                    CHILD_HDCOMPONENTID_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_HDCOPYID)) {
            HiddenField child 
            = new HiddenField(this,
                    getdoComponentOverDraftModel(),
                    CHILD_HDCOPYID,
                    doComponentOverDraftModel.FIELD_DFCOPYID,
                    CHILD_HDCOPYID_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_CBCOMPONENTTYPE)) {
            ComboBox child 
            = new ComboBox(this,
                    getdoComponentOverDraftModel(),
                    CHILD_CBCOMPONENTTYPE,
                    doComponentOverDraftModel.FIELD_DFCOMPONENTTYPE,
                    CHILD_CBCOMPONENTTYPE_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_TXOVERDRAFTAMOUNT)){
            TextField child 
            = new TextField(this,
                    getdoComponentOverDraftModel(),
                    CHILD_TXOVERDRAFTAMOUNT,
                    doComponentOverDraftModel.FIELD_DFOVERDRAFTAMOUNT,
                    CHILD_TXOVERDRAFTAMOUNT_RESET_VALUE,
                    null);            
            return child;
        } else if (name.equals(CHILD_TXADDITIONALINFO)) {
            TextField child =
                new TextField(this, getdoComponentOverDraftModel(),
                        CHILD_TXADDITIONALINFO,
                        doComponentOverDraftModel.FIELD_DFADDITIONALINFORMATION,
                        CHILD_TXADDITIONALINFO_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_CBPRODUCT)) {
            ComboBox child = new ComboBox(this, 
                    getdoComponentOverDraftModel(),
                    CHILD_CBPRODUCT,
                    doComponentOverDraftModel.FIELD_DFMTGPRODID,
                    CHILD_CBPRODUCT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_CBINTERESTRATE)) {
            ComboBox child = new ComboBox(this, 
                    getdoComponentOverDraftModel(),
                    CHILD_CBINTERESTRATE, 
                    doComponentOverDraftModel.FIELD_DFPRICINGRATEINVENTORYID,
                    CHILD_CBINTERESTRATE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_BTRECALC)) {
            Button child = new Button(
                    this, getDefaultModel(), CHILD_BTRECALC,
                    CHILD_BTRECALC, CHILD_BTRECALC_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_BTDELETE)) {
            Button child = new Button(
                    this, getDefaultModel(), CHILD_BTDELETE,
                    CHILD_BTDELETE, CHILD_BTDELETE_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_STTITLE)) {
            StaticTextField child 
            = new StaticTextField(this,
                    getDefaultModel(),
                    CHILD_STTITLE,
                    CHILD_STTITLE,
                    CHILD_STTITLE,
                    null);
            return child;
        } else if (name.equals(CHILD_IMSECTIONDEVIDER)) {
            ImageField child 
            = new ImageField(this,
                    getDefaultModel(),
                    CHILD_IMSECTIONDEVIDER,
                    CHILD_IMSECTIONDEVIDER,
                    CHILD_IMSECTIONDEVIDER,
                    null);
            return child;
        }
        /***************MCM Impl team changes starts - XS_2.28*******************/

        else if (name.equals(CHILD_STMTGPRODUCT)) {
            TextField child = new TextField(this, 
                    getDefaultModel(),
                    CHILD_STMTGPRODUCT, 
                    CHILD_STMTGPRODUCT,
                    CHILD_STMTGPRODUCT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STINTERESTRATE)) {
            TextField child = new TextField(this, 
                    getDefaultModel(),
                    CHILD_STINTERESTRATE, 
                    CHILD_STINTERESTRATE,
                    CHILD_STINTERESTRATE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), 
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TXPOSTEDRATE)) {
            TextField child =
                new TextField(this, getdoComponentOverDraftModel(),
                        CHILD_TXPOSTEDRATE,
                        doComponentOverDraftModel.FIELD_DFPOSTEDRATE,
                        CHILD_TXPOSTEDRATE_RESET_VALUE, null);
            return child;
        }
        /** *************MCM Impl team changes ends - XS_2.28******************** */

        else throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }

    /**
     * <p>
     * registerChildren
     * </p>
     * <p>
     * Description: registerChildren method for Jato Framework
     * </p>
     * 
     * @param
     * @return
     */
    protected void registerChildren() {

        registerChild(CHILD_HDCOMPONENTID, HiddenField.class);
        registerChild(CHILD_HDCOPYID, HiddenField.class);
        registerChild(CHILD_CBCOMPONENTTYPE, ComboBox.class);
        registerChild(CHILD_TXOVERDRAFTAMOUNT, TextField.class);
        registerChild(CHILD_TXADDITIONALINFO, TextField.class);
        registerChild(CHILD_CBPRODUCT, ComboBox.class);
        registerChild(CHILD_CBINTERESTRATE, ComboBox.class);
        registerChild(CHILD_BTRECALC, Button.class);
        registerChild(CHILD_BTDELETE, Button.class);
        registerChild(CHILD_STTITLE, StaticTextField.class);
        registerChild(CHILD_IMSECTIONDEVIDER, ImageField.class);
        /***************MCM Impl team changes starts - XS_2.28*******************/
        registerChild(CHILD_STINTERESTRATE, TextField.class);
        registerChild(CHILD_STMTGPRODUCT, TextField.class);
        registerChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT, StaticTextField.class);
        registerChild(CHILD_TXPOSTEDRATE, TextField.class);
        /***************MCM Impl team changes ends - XS_2.28*********************/
    }

    /**
     * <p>resetChildren</p>
     * <p>Description: resetChildren method for Jato Framework</p>
     * 
     * @param 
     * @return
     */
    public void resetChildren() {

        super.resetChildren();

        getHdMtgComponentId().setValue(CHILD_HDCOMPONENTID_RESET_VALUE);
        getHdMtgCopyId().setValue(CHILD_HDCOMPONENTID_RESET_VALUE);

        getCbComponentType().setValue(CHILD_CBCOMPONENTTYPE_RESET_VALUE);
        getTxMortgageAmount().setValue(CHILD_TXOVERDRAFTAMOUNT_RESET_VALUE);
        getCbPostedInterestRate().setValue(CHILD_CBINTERESTRATE_RESET_VALUE);
        getBtRecalc().setValue(CHILD_BTRECALC_RESET_VALUE);
        getBtDelete().setValue(CHILD_BTDELETE_RESET_VALUE);
        
        getStTitle().setValue(CHILD_STTITLE_RESET_VALUE);
        getImSectionDevider().setValue(CHILD_IMSECTIONDEVIDER_RESET_VALUE);
        
        /***************MCM Impl team changes starts - XS_2.28*******************/
        getStPostedInterestRate().setValue(CHILD_STINTERESTRATE_RESET_VALUE);
        getStMtgProduct().setValue(CHILD_STMTGPRODUCT_RESET_VALUE);
        getStInitDyanmicListJavaScript().setValue(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE);
        getTxPostedRate().setValue(CHILD_TXPOSTEDRATE_RESET_VALUE);
        /***************MCM Impl team changes ends - XS_2.28*********************/

    }

    /**
     * <p>nextTile</p>
     * <p>Description: display next mortgage section tiled view </p>
     */
    public boolean nextTile() throws ModelControlException {

        boolean movedToRow = super.nextTile();
        if (movedToRow) {
            ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
            .cloneSS();
            handler.pageGetState(this.getParentViewBean());
            handler.setOverdraftCompDisplayFields(getTileIndex());
            handler.pageSaveState();
        }
        return movedToRow;
    }

    public HiddenField getHdMtgComponentId() {
        return (HiddenField) getChild(CHILD_HDCOMPONENTID);
    }

    public HiddenField getHdMtgCopyId() {
        return (HiddenField) getChild(CHILD_HDCOPYID);
    }

    public ComboBox getCbComponentType() {
        return (ComboBox) getChild(CHILD_CBCOMPONENTTYPE);
    }

    public TextField getTxMortgageAmount() {
        return (TextField) getChild(CHILD_TXOVERDRAFTAMOUNT);
    }

    public TextField getTxAdditionalInfo() {
        return (TextField) getChild(CHILD_TXADDITIONALINFO);
    }

    public ComboBox getMtgProduct() {
        return (ComboBox) getChild(CHILD_CBPRODUCT);
    }

    public ComboBox getCbPostedInterestRate() {
        return (ComboBox) getChild(CHILD_CBINTERESTRATE);
    }

    public Button getBtRecalc() {
        return (Button) getChild(CHILD_BTRECALC);
    }

    public Button getBtDelete() {
        return (Button) getChild(CHILD_BTDELETE);
    }
    // geters ends    

    /**
     * <p>handleBtRecalcRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @author MCM Team
     * -- XS 2.38
     */
    public void handleBtRecalcRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.debug("Web event invoked: "+getClass().getName()+".handleBtRecalcRequest");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        handler.handleRecalc();
        handler.postHandlerProtocol();    
    }

    /**
     * <p>handleBtDeleteRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @version 1.0 -- XS 2.38
     * @version 1.1 XS 2.32 delete component
     * @author MCM Team
     */
    public void handleBtDeleteRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.debug("Web event invoked: "+getClass().getName()+".handleBtDeleteRequest");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
        handler.handleDeleteOverdraftComp(tileIndx);
        handler.postHandlerProtocol();    
    }

    public StaticTextField getStTitle() {
        return (StaticTextField) getChild(CHILD_STTITLE);
    }
    
    public ImageField getImSectionDevider() {
        return (ImageField) getChild(CHILD_IMSECTIONDEVIDER);
    }
    
    /***************MCM Impl team changes starts - XS_2.28*******************/

    public TextField getStPostedInterestRate() {
        return (TextField) getChild(CHILD_STINTERESTRATE);
    }

    public TextField getStMtgProduct() {
        return (TextField) getChild(CHILD_STMTGPRODUCT);
    }

    public StaticTextField getStInitDyanmicListJavaScript() {
        return (StaticTextField) getChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT);
    }

    public TextField getTxPostedRate() {
        return (TextField) getChild(CHILD_TXPOSTEDRATE);
    }
    
    /***************MCM Impl team changes ends - XS_2.28*********************/

    /**
     * <p>getWebActionModels</p>
     * <p>Description: get Auto executed models</p>
     *
     * @param int: executionType
     * @return Model[]
     */
    public Model[] getWebActionModels(int executionType) {
        List modelList = new ArrayList();
        switch (executionType) {
        case MODEL_TYPE_RETRIEVE:
            modelList.add(getdoComponentOverDraftModel());
            ;
            break;

        case MODEL_TYPE_UPDATE:
            ;
            break;

        case MODEL_TYPE_DELETE:
            ;
            break;

        case MODEL_TYPE_INSERT:
            ;
            break;

        case MODEL_TYPE_EXECUTE:
            ;
            break;
        }
        return (Model[]) modelList.toArray(new Model[0]);
    }

    /**
     * <p>beginDisplay</p>
     * <p>Description: beginDisplay method for Jato Framework</p>
     * 
     * @param event: DisplayEvent
     * @return
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {

        ComponentDetailsHandler handler 
        = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());

        if (getPrimaryModel() == null) {
            throw new ModelControlException("Primary model is null");
        }

        super.beginDisplay(event);
        resetTileIndex();
    }

    /**
     * <p>beforeModelExecutes</p>
     * <p>Description: beforeModelExecutes method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @return boolean
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {
        return super.beforeModelExecutes(model, executionContext);
    }

    /**
     * <p>afterModelExecutes</p>
     * <p>Description: beforeModelExecutes method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @return
     */
    public void afterModelExecutes(Model model, int executionContext) {
        super.afterModelExecutes(model, executionContext);
    }

    /**
     * <p>afterModelExecutes</p>
     * <p>Description: afterModelExecutes method for Jato Framework</p>
     * 
     * @param executionContext: int
     * @return
     */
    public void afterAllModelsExecute(int executionContext) {
        super.afterAllModelsExecute(executionContext);
    }

    /**
     * <p>onModelError</p>     * 
     * <p>Description: onModelError method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @param exception: ModelControlException
     * @return
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception) throws ModelControlException {
        super.onModelError(model, executionContext, exception);
    }

    public void setdoComponentOverDraftModel( doComponentOverDraftModel model) {
        doComponentOverDraftModelInstance = model;
    }

    public doComponentOverDraftModel getdoComponentOverDraftModel() {
        if (doComponentOverDraftModelInstance == null) {
            doComponentOverDraftModelInstance = 
                (doComponentOverDraftModel) getModel(doComponentOverDraftModel.class);
        }
        return doComponentOverDraftModelInstance;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // Class variables
    // //////////////////////////////////////////////////////////////////////////////

//  public static Map FIELD_DESCRIPTORS;


    public static final String CHILD_HDCOMPONENTID = "hdComponentId";
    public static final String CHILD_HDCOMPONENTID_RESET_VALUE = "";

    public static final String CHILD_HDCOPYID = "hdCopyId";
    public static final String CHILD_HDCOPYID_RESET_VALUE = "";

    public static final String CHILD_CBCOMPONENTTYPE = "cbComponentType";
    public static final String CHILD_CBCOMPONENTTYPE_RESET_VALUE = "";
    public static final String CHILD_TXOVERDRAFTAMOUNT = "txOverdraftAmount";
    public static final String CHILD_TXOVERDRAFTAMOUNT_RESET_VALUE = "";

    public static final String CHILD_TXADDITIONALINFO = "txAdditionalInfo";
    public static final String CHILD_TXADDITIONALINFO_RESET_VALUE = "";

    public static final String CHILD_CBPRODUCT = "cbProduct";
    public static final String CHILD_CBPRODUCT_RESET_VALUE = "";

    public static final String CHILD_CBINTERESTRATE = "cbInterestRate";
    public static final String CHILD_CBINTERESTRATE_RESET_VALUE = "";

    public static final String CHILD_BTRECALC = "btRecalc";
    public static final String CHILD_BTRECALC_RESET_VALUE = "";

    public static final String CHILD_BTDELETE = "btDelete";
    public static final String CHILD_BTDELETE_RESET_VALUE = "";

    public static final String CHILD_STTITLE = "stTitle";
    public static final String CHILD_STTITLE_RESET_VALUE = "";    
    
    public static final String CHILD_IMSECTIONDEVIDER = "imSectionDevider";
    public static final String CHILD_IMSECTIONDEVIDER_RESET_VALUE = "";
    
    /***************MCM Impl team changes starts - XS_2.28*******************/

    public static final String CHILD_STMTGPRODUCT = "stMtgProduct";
    public static final String CHILD_STMTGPRODUCT_RESET_VALUE = "";

    public static final String CHILD_STINTERESTRATE = "stInterestRate";
    public static final String CHILD_STINTERESTRATE_RESET_VALUE = "";

    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT = "stInitJavaScript"; 
    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE = "";
    
    public static final String CHILD_TXPOSTEDRATE = "txPostedRate";
    public static final String CHILD_TXPOSTEDRATE_RESET_VALUE = "";
    
    /***************MCM Impl team changes ends - XS_2.28*********************/

    // //////////////////////////////////////////////////////////////////////////
    // Instance variables
    // //////////////////////////////////////////////////////////////////////////

    private doComponentOverDraftModel doComponentOverDraftModelInstance = null;
    private ComponentDetailsHandler handler = new ComponentDetailsHandler();

}
