package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedEscrowPaymentsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedEscrowPaymentsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doMainViewEscrowModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStEscrowType().setValue(CHILD_STESCROWTYPE_RESET_VALUE);
		getStEscrowPaymentDesc().setValue(CHILD_STESCROWPAYMENTDESC_RESET_VALUE);
		getStEscrowPayment().setValue(CHILD_STESCROWPAYMENT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STESCROWTYPE,StaticTextField.class);
		registerChild(CHILD_STESCROWPAYMENTDESC,StaticTextField.class);
		registerChild(CHILD_STESCROWPAYMENT,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoMainViewEscrowModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STESCROWTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoMainViewEscrowModel(),
				CHILD_STESCROWTYPE,
				doMainViewEscrowModel.FIELD_DFESCROWTYPE,
				CHILD_STESCROWTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESCROWPAYMENTDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getdoMainViewEscrowModel(),
				CHILD_STESCROWPAYMENTDESC,
				doMainViewEscrowModel.FIELD_DFESCROWPAYMENTDESC,
				CHILD_STESCROWPAYMENTDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESCROWPAYMENT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoMainViewEscrowModel(),
				CHILD_STESCROWPAYMENT,
				doMainViewEscrowModel.FIELD_DFESCROWPAYMENT,
				CHILD_STESCROWPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEscrowType()
	{
		return (StaticTextField)getChild(CHILD_STESCROWTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEscrowPaymentDesc()
	{
		return (StaticTextField)getChild(CHILD_STESCROWPAYMENTDESC);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEscrowPayment()
	{
		return (StaticTextField)getChild(CHILD_STESCROWPAYMENT);
	}


	/**
	 *
	 *
	 */
	public doMainViewEscrowModel getdoMainViewEscrowModel()
	{
		if (doMainViewEscrow == null)
			doMainViewEscrow = (doMainViewEscrowModel) getModel(doMainViewEscrowModel.class);
		return doMainViewEscrow;
	}


	/**
	 *
	 *
	 */
	public void setdoMainViewEscrowModel(doMainViewEscrowModel model)
	{
			doMainViewEscrow = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STESCROWTYPE="stEscrowType";
	public static final String CHILD_STESCROWTYPE_RESET_VALUE="";
	public static final String CHILD_STESCROWPAYMENTDESC="stEscrowPaymentDesc";
	public static final String CHILD_STESCROWPAYMENTDESC_RESET_VALUE="";
	public static final String CHILD_STESCROWPAYMENT="stEscrowPayment";
	public static final String CHILD_STESCROWPAYMENT_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doMainViewEscrowModel doMainViewEscrow=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

