package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doUWSourceInformationModelImpl extends QueryModelBase
	implements doUWSourceInformationModel
{
	/**
	 *
	 *
	 */
	public doUWSourceInformationModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSourceBusinessProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSOURCEBUSINESSPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceBusinessProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSOURCEBUSINESSPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceName()
	{
		return (String)getValue(FIELD_DFSOURCENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceName(String value)
	{
		setValue(FIELD_DFSOURCENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceFirm()
	{
		return (String)getValue(FIELD_DFSOURCEFIRM);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceFirm(String value)
	{
		setValue(FIELD_DFSOURCEFIRM,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceAddress1()
	{
		return (String)getValue(FIELD_DFSOURCEADDRESS1);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceAddress1(String value)
	{
		setValue(FIELD_DFSOURCEADDRESS1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceCity()
	{
		return (String)getValue(FIELD_DFSOURCECITY);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceCity(String value)
	{
		setValue(FIELD_DFSOURCECITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceProvince()
	{
		return (String)getValue(FIELD_DFSOURCEPROVINCE);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceProvince(String value)
	{
		setValue(FIELD_DFSOURCEPROVINCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceContactPhone()
	{
		return (String)getValue(FIELD_DFSOURCECONTACTPHONE);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceContactPhone(String value)
	{
		setValue(FIELD_DFSOURCECONTACTPHONE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceContactPhoneExt()
	{
		return (String)getValue(FIELD_DFSOURCECONTACTPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceContactPhoneExt(String value)
	{
		setValue(FIELD_DFSOURCECONTACTPHONEEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceContactFax()
	{
		return (String)getValue(FIELD_DFSOURCECONTACTFAX);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceContactFax(String value)
	{
		setValue(FIELD_DFSOURCECONTACTFAX,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //---------------- FXLink Phase II ------------------//
  //--> Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> By Billy 17Nov2003
	public static final String SELECT_SQL_TEMPLATE=
  "SELECT distinct SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID, SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME, " +
  "SOURCEFIRMPROFILE.SFSHORTNAME, ADDR.ADDRESSLINE1, ADDR.CITY, PROVINCE.PROVINCENAME, " +
  "CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTPHONENUMBEREXTENSION, CONTACT.CONTACTFAXNUMBER " +
  "FROM SOURCEOFBUSINESSPROFILE, SOURCEFIRMPROFILE, CONTACT, " +
  "ADDR, PROVINCE " +
  " __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME=
  "SOURCEOFBUSINESSPROFILE, SOURCEFIRMPROFILE, CONTACT, ADDR, PROVINCE";
	public static final String STATIC_WHERE_CRITERIA=
  " (SOURCEOFBUSINESSPROFILE.SOURCEFIRMPROFILEID  =  SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID) AND " +
  "(SOURCEOFBUSINESSPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND " +
  "(CONTACT.ADDRID  =  ADDR.ADDRID) AND " +
  "(ADDR.PROVINCEID  =  PROVINCE.PROVINCEID) " +
  "AND (SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID = SOURCEFIRMPROFILE.INSTITUTIONPROFILEID) " +
  "AND (SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID = CONTACT.INSTITUTIONPROFILEID) " +
  "AND (SOURCEOFBUSINESSPROFILE.INSTITUTIONPROFILEID = ADDR.INSTITUTIONPROFILEID) ";
  //=====================================================
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFSOURCEBUSINESSPROFILEID="SOURCEOFBUSINESSPROFILE.SOURCEOFBUSINESSPROFILEID";
	public static final String COLUMN_DFSOURCEBUSINESSPROFILEID="SOURCEOFBUSINESSPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSOURCENAME="SOURCEOFBUSINESSPROFILE.SOBPSHORTNAME";
	public static final String COLUMN_DFSOURCENAME="SOBPSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFSOURCEFIRM="SOURCEFIRMPROFILE.SFSHORTNAME";
	public static final String COLUMN_DFSOURCEFIRM="SFSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFSOURCEADDRESS1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFSOURCEADDRESS1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFSOURCECITY="ADDR.CITY";
	public static final String COLUMN_DFSOURCECITY="CITY";
	public static final String QUALIFIED_COLUMN_DFSOURCEPROVINCE="PROVINCE.PROVINCENAME";
	public static final String COLUMN_DFSOURCEPROVINCE="PROVINCENAME";
	public static final String QUALIFIED_COLUMN_DFSOURCECONTACTPHONE="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFSOURCECONTACTPHONE="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFSOURCECONTACTPHONEEXT="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFSOURCECONTACTPHONEEXT="CONTACTPHONENUMBEREXTENSION";
	public static final String QUALIFIED_COLUMN_DFSOURCECONTACTFAX="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFSOURCECONTACTFAX="CONTACTFAXNUMBER";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCEBUSINESSPROFILEID,
				COLUMN_DFSOURCEBUSINESSPROFILEID,
				QUALIFIED_COLUMN_DFSOURCEBUSINESSPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCENAME,
				COLUMN_DFSOURCENAME,
				QUALIFIED_COLUMN_DFSOURCENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCEFIRM,
				COLUMN_DFSOURCEFIRM,
				QUALIFIED_COLUMN_DFSOURCEFIRM,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCEADDRESS1,
				COLUMN_DFSOURCEADDRESS1,
				QUALIFIED_COLUMN_DFSOURCEADDRESS1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCECITY,
				COLUMN_DFSOURCECITY,
				QUALIFIED_COLUMN_DFSOURCECITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCEPROVINCE,
				COLUMN_DFSOURCEPROVINCE,
				QUALIFIED_COLUMN_DFSOURCEPROVINCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCECONTACTPHONE,
				COLUMN_DFSOURCECONTACTPHONE,
				QUALIFIED_COLUMN_DFSOURCECONTACTPHONE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCECONTACTPHONEEXT,
				COLUMN_DFSOURCECONTACTPHONEEXT,
				QUALIFIED_COLUMN_DFSOURCECONTACTPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCECONTACTFAX,
				COLUMN_DFSOURCECONTACTFAX,
				QUALIFIED_COLUMN_DFSOURCECONTACTFAX,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

