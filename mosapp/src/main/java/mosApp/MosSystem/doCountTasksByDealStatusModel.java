package mosApp.MosSystem;

import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import java.io.*;

import java.sql.*;

import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;


/**
 * SCR#750/BMO 1.5 BMO wants to see the new summary section counted by DealStatus
 *
 * @author Neil on Dec/22/2004
 */
public interface doCountTasksByDealStatusModel extends QueryModel, SelectQueryModel
{
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  public static final String FIELD_DFTASKNAMECOUNT = "dfTaskNameCount";

  /**
   *
   *
   */
  public Integer getDfTaskNameCount();

  /**
   *
   *
   */
  public void setDfTaskNameCount(Integer value);

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
}
