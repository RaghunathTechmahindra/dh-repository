package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetLivingSpaceUnitOfMeasureModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLivingSpaceUnitOfMeasureId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLivingSpaceUnitOfMeasureId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLivingSpaceUnitMeasureDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfLivingSpaceUnitMeasureDesc(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFLIVINGSPACEUNITOFMEASUREID="dfLivingSpaceUnitOfMeasureId";
	public static final String FIELD_DFLIVINGSPACEUNITMEASUREDESC="dfLivingSpaceUnitMeasureDesc";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

