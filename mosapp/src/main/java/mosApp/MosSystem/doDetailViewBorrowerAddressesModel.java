package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewBorrowerAddressesModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfProvinceName();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvinceName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfMonthsAtAddress();

	
	/**
	 * 
	 * 
	 */
	public void setDfMonthsAtAddress(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressStatus();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressStatus(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfResidentialStatus();

	
	/**
	 * 
	 * 
	 */
	public void setDfResidentialStatus(String value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFADDRESSLINE1="dfAddressLine1";
	public static final String FIELD_DFADDRESSLINE2="dfAddressLine2";
	public static final String FIELD_DFCITY="dfCity";
	public static final String FIELD_DFPROVINCENAME="dfProvinceName";
	public static final String FIELD_DFPOSTALFSA="dfPostalFSA";
	public static final String FIELD_DFPOSTALLDU="dfPostalLDU";
	public static final String FIELD_DFMONTHSATADDRESS="dfMonthsAtAddress";
	public static final String FIELD_DFADDRESSSTATUS="dfAddressStatus";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFRESIDENTIALSTATUS="dfResidentialStatus";
	
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

