package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * <p>
 * Title: doIdentificationTypeModel
 * </p>
 * 
 * <p>
 * Description: Model class for Identification tile screen
 * 
 * 
 * @author
 * @version 1.0 Date: 8/01/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          New class
 *          
 *          @version 1.1 Date: 8/11/2006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: Added borrower id and copy id fields -  Fix for defect 4215 <br>
 *          
 * 
 */
public interface doIdentificationTypeModel extends QueryModel, SelectQueryModel {
	public static final String FIELD_DFIDENTIFICATIONNUMBER = "dfIdentificationNumber";

	public static final String FIELD_DFIDENTIFICATIONTYPE = "dfIdentificationType";

	public static final String FIELD_DFIDENTIFICATIONSOURCECOUNTRY = "dfIdentificationSourceCountry";
//	***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final String FIELD_DFBORROWERID = "dfBorrowerId";

	public static final String FIELD_DFCOPYID = "dfCopyId";
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  
	/**
	 * <p>
	 * returns borrower Id
	 * </p>
	 * 
	 * @return - returns borrower id
	 */
	public java.math.BigDecimal getDfBorrowerId();

	/**
	 * <p>
	 * sets borrower id
	 * </p>
	 * 
	 * @param -
	 *            sets borrower id
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	/**
	 * <p>
	 * returns copy Id
	 * </p>
	 * 
	 * @return - returns copy id
	 */
	public java.math.BigDecimal getDfCopyId();

	/**
	 * <p>
	 * sets copy id
	 * </p>
	 * 
	 * @param -
	 *            sets copy id
	 */
	public void setDfCopyId(java.math.BigDecimal value);


//	***** Change by NBC Impl. Team - Version 1.1 - End*****//

	/**
	 * getDfIdentificationNumber
	 * 
	 * @param <br>
	 * 
	 * @return String : the result of getDfIdentificationNumber <br>
	 */
	public String getDfIdentificationNumber();

	/**
	 * setDfIdentificationNumber
	 * 
	 * @param String
	 *            <br>
	 * 
	 * @return void : the result of setDfIdentificationNumber <br>
	 */
	public void setDfIdentificationNumber(String value);

	/**
	 * getDfIdentificationType
	 * 
	 * @param <br>
	 * 
	 * @return String : the result of getDfIdentificationType <br>
	 */
	public String getDfIdentificationType();

	/**
	 * setDfIdentificationType
	 * 
	 * @param String
	 *            <br>
	 * 
	 * @return void : the result of setDfIdentificationType <br>
	 */
	public void setDfIdentificationType(String value);

	/**
	 * getDfIdentificationNumber
	 * 
	 * @param <br>
	 * 
	 * @return String : the result of getDfIdentificationNumber <br>
	 */
	public String getDfIdentificationSourceCountry();

	/**
	 * setDfIdentificationSourceCountry
	 * 
	 * @param String
	 *            <br>
	 * 
	 * @return void : the result of setDfIdentificationSourceCountry <br>
	 */
	public void setDfIdentificationSourceCountry(String value);
	
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
}
