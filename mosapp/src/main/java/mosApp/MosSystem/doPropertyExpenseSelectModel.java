package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doPropertyExpenseSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyExpenseId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpenseId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyExpenseTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpenseTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyExpenseDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpenseDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyExpensePeriodId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpensePeriodId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyExpenseAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpenseAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyExpenseIncludeInGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpenseIncludeInGDS(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyExpensePercentageIncludedGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpensePercentageIncludedGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyExpenseIncludeTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpenseIncludeTDS(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyExpensePercentageIncludedTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyExpensePercentageIncludedTDS(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPROPERTYEXPENSEID="dfPropertyExpenseId";
	public static final String FIELD_DFPROPERTYID="dfPropertyId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFPROPERTYEXPENSETYPEID="dfPropertyExpenseTypeId";
	public static final String FIELD_DFPROPERTYEXPENSEDESC="dfPropertyExpenseDesc";
	public static final String FIELD_DFPROPERTYEXPENSEPERIODID="dfPropertyExpensePeriodId";
	public static final String FIELD_DFPROPERTYEXPENSEAMOUNT="dfPropertyExpenseAmount";
	public static final String FIELD_DFPROPERTYEXPENSEINCLUDEINGDS="dfPropertyExpenseIncludeInGDS";
	public static final String FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS="dfPropertyExpensePercentageIncludedGDS";
	public static final String FIELD_DFPROPERTYEXPENSEINCLUDETDS="dfPropertyExpenseIncludeTDS";
	public static final String FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS="dfPropertyExpensePercentageIncludedTDS";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

