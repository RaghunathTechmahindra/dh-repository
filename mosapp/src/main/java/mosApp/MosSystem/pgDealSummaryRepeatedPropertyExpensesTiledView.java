package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedPropertyExpensesTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedPropertyExpensesTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDetailViewPropertyExpensesModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStExpenseType().setValue(CHILD_STEXPENSETYPE_RESET_VALUE);
		getStExpenseDescription().setValue(CHILD_STEXPENSEDESCRIPTION_RESET_VALUE);
		getStExpensePeriod().setValue(CHILD_STEXPENSEPERIOD_RESET_VALUE);
		getStExpenseAmount().setValue(CHILD_STEXPENSEAMOUNT_RESET_VALUE);
		getStIncludeGDS().setValue(CHILD_STINCLUDEGDS_RESET_VALUE);
		getStIncludeTDS().setValue(CHILD_STINCLUDETDS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STEXPENSETYPE,StaticTextField.class);
		registerChild(CHILD_STEXPENSEDESCRIPTION,StaticTextField.class);
		registerChild(CHILD_STEXPENSEPERIOD,StaticTextField.class);
		registerChild(CHILD_STEXPENSEAMOUNT,StaticTextField.class);
		registerChild(CHILD_STINCLUDEGDS,StaticTextField.class);
		registerChild(CHILD_STINCLUDETDS,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewPropertyExpensesModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedPropertyExpenses_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedPropertyExpenses(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;


		// The following code block was migrated from the RepeatedPropertyExpenses_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//		DealSummaryHandler handler = (DealSummaryHandler)this.handler.cloneSS();
		//
		//		handler.pageGetState(this);
		//		handler.setPropertyExpenses(event.getRowIndex());
		//		handler.pageSaveState();
		//
		return;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STEXPENSETYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertyExpensesModel(),
				CHILD_STEXPENSETYPE,
				doDetailViewPropertyExpensesModel.FIELD_DFPROPERTYEXPENSETYPE,
				CHILD_STEXPENSETYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXPENSEDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertyExpensesModel(),
				CHILD_STEXPENSEDESCRIPTION,
				doDetailViewPropertyExpensesModel.FIELD_DFPROPERTYEXPENSEDESC,
				CHILD_STEXPENSEDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXPENSEPERIOD))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertyExpensesModel(),
				CHILD_STEXPENSEPERIOD,
				doDetailViewPropertyExpensesModel.FIELD_DFPROPERTYEXPENSEPERIODDESC,
				CHILD_STEXPENSEPERIOD_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXPENSEAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertyExpensesModel(),
				CHILD_STEXPENSEAMOUNT,
				doDetailViewPropertyExpensesModel.FIELD_DFPROPERTYEXPENSEAMOUNT,
				CHILD_STEXPENSEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDEGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertyExpensesModel(),
				CHILD_STINCLUDEGDS,
				doDetailViewPropertyExpensesModel.FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,
				CHILD_STINCLUDEGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDETDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewPropertyExpensesModel(),
				CHILD_STINCLUDETDS,
				doDetailViewPropertyExpensesModel.FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,
				CHILD_STINCLUDETDS_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStExpenseType()
	{
		return (StaticTextField)getChild(CHILD_STEXPENSETYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStExpenseDescription()
	{
		return (StaticTextField)getChild(CHILD_STEXPENSEDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStExpensePeriod()
	{
		return (StaticTextField)getChild(CHILD_STEXPENSEPERIOD);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStExpenseAmount()
	{
		return (StaticTextField)getChild(CHILD_STEXPENSEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeGDS()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDEGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeTDS()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDETDS);
	}


	/**
	 *
	 *
	 */
	public doDetailViewPropertyExpensesModel getdoDetailViewPropertyExpensesModel()
	{
		if (doDetailViewPropertyExpenses == null)
			doDetailViewPropertyExpenses = (doDetailViewPropertyExpensesModel) getModel(doDetailViewPropertyExpensesModel.class);
		return doDetailViewPropertyExpenses;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewPropertyExpensesModel(doDetailViewPropertyExpensesModel model)
	{
			doDetailViewPropertyExpenses = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STEXPENSETYPE="stExpenseType";
	public static final String CHILD_STEXPENSETYPE_RESET_VALUE="";
	public static final String CHILD_STEXPENSEDESCRIPTION="stExpenseDescription";
	public static final String CHILD_STEXPENSEDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_STEXPENSEPERIOD="stExpensePeriod";
	public static final String CHILD_STEXPENSEPERIOD_RESET_VALUE="";
	public static final String CHILD_STEXPENSEAMOUNT="stExpenseAmount";
	public static final String CHILD_STEXPENSEAMOUNT_RESET_VALUE="";
	public static final String CHILD_STINCLUDEGDS="stIncludeGDS";
	public static final String CHILD_STINCLUDEGDS_RESET_VALUE="";
	public static final String CHILD_STINCLUDETDS="stIncludeTDS";
	public static final String CHILD_STINCLUDETDS_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewPropertyExpensesModel doDetailViewPropertyExpenses=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

}

