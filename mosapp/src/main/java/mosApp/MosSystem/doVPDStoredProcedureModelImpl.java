package mosApp.MosSystem;

import java.sql.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.basis100.log.*;

/**
 *
 *
 *
 */
public class doVPDStoredProcedureModelImpl extends StoredProcModelBase
	implements doVPDStoredProcedureModel
{
	/**
	 *
	 *
	 */
	public doVPDStoredProcedureModelImpl()
	{
		super();

		setDataSourceName(DATA_SOURCE_NAME);
                setProcedureName(STORED_PROCEDURE_NAME);
                setProcedureSchema(STORED_PROCEDURE_SCHEMA);

		initialize();
	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{
          logger = SysLog.getSysLogger("VPDModel");
          logger.debug("VLAD ===> VPDModel@SQLBeforeExecute: " + sql);

          return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
          logger = SysLog.getSysLogger("VPDModel");
          logger.debug("VLAD ===> VPDModel@SQLAfterExecute");

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
          logger = SysLog.getSysLogger("VPDModel");
          logger.debug("VLAD ===> VPDModel@onDatabaseErrorQueryType: " + queryType);
          if (context != null)
          {
            logger.debug("VLAD ===> VPDModel@onDatabaseErrorQueryType::ExecuteWithOperation: " + context.getOperationName());
            logger.debug("VLAD ===> VPDModel@onDatabaseErrorQueryType::ExecuteContext: " + context.toString());
          }
          logger.debug("VLAD ===> VPDModel@onDatabaseErrorException: " + exception.toString());
	}

        /**
         *
         *
         */
        public Object execute(ModelExecutionContext context)
        {
             logger = SysLog.getSysLogger("VPDModel");
             logger.debug("VLAD ===> VPDModel@Execute");

             if (context != null)
             {
               logger.debug("VLAD ===> VPDModel@ExecuteWithOperation: " + context.getOperationName());
               logger.debug("VLAD ===> VPDModel@ExecuteContext: " + context.toString());
             }

             ResultSet rs = null;
             return rs;
        }

        /**
         *
         *
         */
        public Object executeProcedure(ModelExecutionContext context)
            throws SQLException, ModelControlException
        {
          logger = SysLog.getSysLogger("VPDModel");
          logger.debug("VLAD ===> VPDModel@ExecuteProcedure");

         // Just dirty repeat the base code: MUST BE Refactored
         // We try to use the statement from the context if one is available.
         // In general, we'll have either a statement, a connection, or neither.
         CallableStatement statement=null;
         Connection connection=null;
         boolean usedDefaultConnection=false;
         boolean usedDefaultStatement=false;

         // Construct the SQL
         String sql=constructProcedureSQL();


         //try{
         sql=beforeExecute(context,sql);
         if (sql==null)
                 return null;
         //}
         //catch (ModelControlException mce)
         //{

         //}
         try
         {
                 if (context!=null && context instanceof SQLModelExecutionContext)
                 {
                         if (((SQLModelExecutionContext)context).getStatement()!=null)
                         {
                                 if (((SQLModelExecutionContext)context).getStatement()
                                         instanceof CallableStatement)
                                 {
                                         statement=(CallableStatement)
                                                 ((SQLModelExecutionContext)context).getStatement();
                                 }
                                 else
                                 {
                                         throw new IllegalArgumentException("If a statement "+
                                                 "is provided in the execution context it must be "+
                                                 "of type "+CallableStatement.class.getName());
                                 }

                                 connection=
                                         ((SQLModelExecutionContext)context).getConnection();
                         }
                 }

                 // If we didn't get a statement, then we need to get a connection
                 // (if one wasn't already provided) and use it to get a statement.
                 if (statement==null)
                 {
                         // Get the connection from the context.  If one is not
                         // available, obtain the default connection.
                         if (connection==null)
                         {
                                 connection=getDefaultConnection();
                                 usedDefaultConnection=true;
                         }

                         // TODO: Is there any possible optimzation here?
                         statement=connection.prepareCall(sql);
                         usedDefaultStatement=true;
                 }

                 // Register all the procedure's parameters
                 registerParameters(statement);

                 // Clear the underlying model of any existing data. Ideally we
                 // are only clearing the result set and any cached model data here.
                 // Parameters are left untouched, the assumption being that clients
                 // of the model wish to preserve the input parameters.  Note that
                 // this won't have any effect between requests because the model
                 // will be created anew.
                 // TODO: This call is currently just resetting the result set and
                 // the maxInitializedRow index, but not clearing any result set data
                 // that might be cached.  This might be just fine, the idea being
                 // that the maxInitializedRow index will keep us from seeing stale
                 // data; but, we should figure out a way to clear this data without
                 // clearing the input param data.  One solution might be to override
                 // setValue(...) so that it sets values in the param list instead
                 // of in the default model data structure.  What consequences would
                 // this have?
                 super.clearResultSet();

                 // Execute the procedure
                 boolean moreResults=statement.execute();

                 // Note that we only support return of a single result set
                 ResultSet resultSet=obtainResultSet(statement);
                 if (resultSet!=null)
                 {
                         if (context!=null && !(context instanceof DatasetModelExecutionContext))
                                 throw new IllegalArgumentException("Context must be a valid "+
                                         "instance of type "+DatasetModelExecutionContext.class.getName()+
                                         " when a result set is returned from a procedure (context = "+
                                         context+")");

                         setResultSet(resultSet);
                 }

                 // Now we can position the result set
                 if (resultSet!=null)
                         positionResultSet((DatasetModelExecutionContext)context);

                 // Map the output parameters into the model
                 Object result=mapOutputParameters(statement);

                 // Fire the after execution event
                 afterExecute(context, statement);
                 return result;
         }
         catch (SQLException e)
         {
                 onDatabaseError(context,e);
                 throw e;
         }
         finally
         {
                 if (usedDefaultStatement && statement!=null)
                         statement.close();

                 if (usedDefaultConnection && connection!=null)
                         connection.close();
         }

        }

        /**
         *
         *
         */
        public java.math.BigDecimal getDfUSERProfileID()
        {
                return (java.math.BigDecimal)getValue(FIELD_DFUSERPROFILEID);
        }


        /**
         *
         *
         */
        public void setDfUSERPROFILEID(java.math.BigDecimal value)
        {
                setValue(FIELD_DFUSERPROFILEID,value);
        }

        /**
         *
         *
         */
        public java.math.BigDecimal getDfInstitutionID()
        {
                return (java.math.BigDecimal)getValue(FIELD_DFINSTITUTIONID);
        }


        /**
         *
         *
         */
        public void setDfInstitutionID(java.math.BigDecimal value)
        {
                setValue(FIELD_DFINSTITUTIONID,value);
        }

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
        public SysLogger logger;

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
        public static StoredProcSchema STORED_PROCEDURE_SCHEMA = new StoredProcSchema();
        //public static StoredProcSchema schema = getProcedureSchema();

        public static final String STORED_PROCEDURE_NAME = "vpdadmin.vpd_security_context.set_app_context";
        public static final String VPN_PROC_PARAM1 = "p_userprofileid";
        public static final String VPN_PROC_PARAM2 = "p_institutionid";

        public static final int PARAM_IN = 1;

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

        static { // Begin Model Field declaration

         com.iplanet.jato.model.sql.StoredProcParameterDescriptor
             VPD_Proc_Descriptor_Param1 = new com.iplanet.jato.model.sql.StoredProcParameterDescriptor(
                                     FIELD_DFUSERPROFILEID,
                                     VPN_PROC_PARAM1,
                                     PARAM_IN,
                                     java.sql.Types.NUMERIC,
                                     java.math.BigDecimal.class);

         STORED_PROCEDURE_SCHEMA.addParameterDescriptor(VPD_Proc_Descriptor_Param1);

         com.iplanet.jato.model.sql.StoredProcParameterDescriptor
            VPD_Proc_Descriptor_Param2 = new com.iplanet.jato.model.sql.StoredProcParameterDescriptor(
                                    FIELD_DFINSTITUTIONID,
                                    VPN_PROC_PARAM2,
                                    PARAM_IN,
                                    java.sql.Types.NUMERIC,
                                    java.math.BigDecimal.class);

         STORED_PROCEDURE_SCHEMA.addParameterDescriptor(VPD_Proc_Descriptor_Param2);

       } // End Model Field declaration
}

