package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doSourceUserModelImpl extends QueryModelBase
	implements doSourceUserModel
{
	/**
	 *
	 *
	 */
	public doSourceUserModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUserProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUsername()
	{
		return (String)getValue(FIELD_DFUSERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfUsername(String value)
	{
		setValue(FIELD_DFUSERNAME,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }
	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
   			super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
              	        logger.debug("Testing MetaData Object for new synthetic fields for" +
                 	            " doSourceUserModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////


  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, CONTACT.CONTACTFIRSTNAME ||   '  ' || CONTACT.CONTACTLASTNAME || '  ' || USERPROFILE.USERLOGIN USERNAME FROM CONTACT, USERPROFILE  __WHERE__  ORDER BY CONTACT.CONTACTFIRSTNAME  ASC, CONTACT.CONTACTLASTNAME  ASC, USERPROFILE.USERLOGIN  ASC";
	
  public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, CONTACT.CONTACTFIRSTNAME ||   '  ' || CONTACT.CONTACTLASTNAME || '  ' || USERPROFILE.USERLOGIN SYNTETIC_USERNAME,USERPROFILE.INSTITUTIONPROFILEID FROM CONTACT, USERPROFILE  __WHERE__  ORDER BY CONTACT.CONTACTFIRSTNAME  ASC, CONTACT.CONTACTLASTNAME  ASC, USERPROFILE.USERLOGIN  ASC";
	
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, USERPROFILE";
	public static final String STATIC_WHERE_CRITERIA=" (CONTACT.CONTACTID  =  USERPROFILE.CONTACTID) AND (CONTACT.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID) ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID="USERPROFILE.USERPROFILEID";
	public static final String COLUMN_DFUSERPROFILEID="USERPROFILEID";
	////public static final String QUALIFIED_COLUMN_DFUSERNAME=".";
	////public static final String COLUMN_DFUSERNAME="";
	public static final String QUALIFIED_COLUMN_DFUSERNAME="CONTACT.SYNTETIC_USERNAME";
	public static final String COLUMN_DFUSERNAME="SYNTETIC_USERNAME";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "USERPROFILE.INSTITUTIONPROFILEID";
  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//// Improper converted fragment by iMT.
                ////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFUSERNAME,
		////		COLUMN_DFUSERNAME,
		////		QUALIFIED_COLUMN_DFUSERNAME,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERNAME,
				COLUMN_DFUSERNAME,
				QUALIFIED_COLUMN_DFUSERNAME,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"CONTACT.CONTACTFIRSTNAME ||   '  ' || CONTACT.CONTACTLASTNAME || '  ' || USERPROFILE.USERLOGIN",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"CONTACT.CONTACTFIRSTNAME ||   '  ' || CONTACT.CONTACTLASTNAME || '  ' || USERPROFILE.USERLOGIN"));

        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
	}

}

