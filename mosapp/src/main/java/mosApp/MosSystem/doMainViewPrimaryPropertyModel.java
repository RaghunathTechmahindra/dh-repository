package mosApp.MosSystem;

import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doMainViewPrimaryPropertyModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryPropertyFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryPropertyFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetName();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfProvince();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvince(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalCode1();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalCode1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalCode2();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalCode2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPorpertyUsage();

	
	/**
	 * 
	 * 
	 */
	public void setDfPorpertyUsage(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfOccupancy();

	
	/**
	 * 
	 * 
	 */
	public void setDfOccupancy(String value);

	
	/**
	 * 
	 * 
	 */
	public String getPropertyType();

	
	/**
	 * 
	 * 
	 */
	public void setPropertyType(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEstimatedAppraisalValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfEstimatedAppraisalValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfActualAppraisalValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfActualAppraisalValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLandValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfLandValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetTypeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetDirectionDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetDirectionDesc(String value);
	
	
	public java.math.BigDecimal getDfInstitutionId();
	public void setDfInstitutionId(java.math.BigDecimal id);
	  
	public String getDfUnitNumber();
	public void setDfUnitNumber(String value);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFPRIMARYPROPERTYFLAG="dfPrimaryPropertyFlag";
	public static final String FIELD_DFSTREETNUMBER="dfStreetNumber";
	public static final String FIELD_DFSTREETNAME="dfStreetName";
	public static final String FIELD_DFADDRESSLINE2="dfAddressLine2";
	public static final String FIELD_DFCITY="dfCity";
	public static final String FIELD_DFPROVINCE="dfProvince";
	public static final String FIELD_DFPOSTALCODE1="dfPostalCode1";
	public static final String FIELD_DFPOSTALCODE2="dfPostalCode2";
	public static final String FIELD_DFPORPERTYUSAGE="dfPorpertyUsage";
	public static final String FIELD_DFOCCUPANCY="dfOccupancy";
	public static final String FIELD_PROPERTYTYPE="PropertyType";
	public static final String FIELD_DFPURCHASEPRICE="dfPurchasePrice";
	public static final String FIELD_DFESTIMATEDAPPRAISALVALUE="dfEstimatedAppraisalValue";
	public static final String FIELD_DFACTUALAPPRAISALVALUE="dfActualAppraisalValue";
	public static final String FIELD_DFLANDVALUE="dfLandValue";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFSTREETTYPEDESC="dfStreetTypeDesc";
	public static final String FIELD_DFSTREETDIRECTIONDESC="dfStreetDirectionDesc";
	public static final String FIELD_DFUNITNUMBER="dfUnitNumber";
	
	
	  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

