package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doMainViewBorrowerModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryBorrowerFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryBorrowerFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfLastName(String value);

	/**
	 * 
	 * 
	 */
	public String getDfSuffix();

	
	/**
	 * 
	 * 
	 */
	public void setDfSuffix(String value);
	
	/**
	 * 
	 * 
	 */
	public String getDfMiddleInitial();

	
	/**
	 * 
	 * 
	 */
	public void setDfMiddleInitial(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLanguagePreference();

	
	/**
	 * 
	 * 
	 */
	public void setDfLanguagePreference(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfExisitingCleint();

	
	/**
	 * 
	 * 
	 */
	public void setDfExisitingCleint(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfRefCleintNo();

	
	/**
	 * 
	 * 
	 */
	public void setDfRefCleintNo(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfHomePhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfHomePhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfWorkPhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfWorkPhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCreditScore();

	
	/**
	 * 
	 * 
	 */
	public void setDfCreditScore(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfWorkPhoneExt();

	
	/**
	 * 
	 * 
	 */
	public void setDfWorkPhoneExt(String value);
	
	  public java.math.BigDecimal getDfInstitutionId();
	  public void setDfInstitutionId(java.math.BigDecimal id);
	  	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFBORROWERTYPEID="dfBorrowerTypeId";
	public static final String FIELD_DFPRIMARYBORROWERFLAG="dfPrimaryBorrowerFlag";
	public static final String FIELD_DFFIRSTNAME="dfFirstName";
	public static final String FIELD_DFLASTNAME="dfLastName";
	public static final String FIELD_DFSUFFIX="dfSuffix";
	public static final String FIELD_DFMIDDLEINITIAL="dfMiddleInitial";
	public static final String FIELD_DFLANGUAGEPREFERENCE="dfLanguagePreference";
	public static final String FIELD_DFEXISITINGCLEINT="dfExisitingCleint";
	public static final String FIELD_DFREFCLEINTNO="dfRefCleintNo";
	public static final String FIELD_DFHOMEPHONENUMBER="dfHomePhoneNumber";
	public static final String FIELD_DFWORKPHONENUMBER="dfWorkPhoneNumber";
	public static final String FIELD_DFCREDITSCORE="dfCreditScore";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFWORKPHONEEXT="dfWorkPhoneExt";
  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

