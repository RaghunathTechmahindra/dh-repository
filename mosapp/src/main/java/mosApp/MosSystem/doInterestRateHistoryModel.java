package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doInterestRateHistoryModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEffectiveDate();


	/**
	 *
	 *
	 */
	public void setDfEffectiveDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getStPostedRate();


	/**
	 *
	 *
	 */
	public void setStPostedRate(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public Double getDfBestRate();


	/**
	 *
	 *
	 */
	public void setDfBestRate(Double value);


	/**
	 *
	 *
	 */
	public String getDfRateCode();


	/**
	 *
	 *
	 */
	public void setDfRateCode(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaxDiscountRate();


	/**
	 *
	 *
	 */
	public void setDfMaxDiscountRate(java.math.BigDecimal value);


  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  /**
   *
   *
   */
  public java.math.BigDecimal getDfSyntheticTeaserRate();

  /**
   *
   *
   */
  public void setDfSyntheticTeaserRate(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPrimeIndexRateProfileId();

  /**
   *
   *
   */
  public void setDfPrimeIndexRateProfileId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public Double getDfPrimeIndexRate();

  /**
   *
   *
   */
  public void setDfPrimeIndexRate(Double value);

  /**
   *
   *
   */
  public Double getDfPrimeBaseAdj();

  /**
   *
   *
   */
  public void setDfPrimeBaseAdj(Double value);

  /**
   *
   *
   */
  public Double getDfTeaserDiscount();

  /**
   *
   *
   */
  public void setDfTeaserDiscount(Double value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTeaserTerm();

  /**
   *
   *
   */
  public void setDfTeaserTerm(java.math.BigDecimal value);


  /**
   *
   *
   */
  public String getDfPrimeIndexName();

  /**
   *
   *
   */
  public void setDfPrimeIndexName(String value);
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFEFFECTIVEDATE="dfEffectiveDate";
	public static final String FIELD_STPOSTEDRATE="stPostedRate";
	public static final String FIELD_DFBESTRATE="dfBestRate";
	public static final String FIELD_DFRATECODE="dfRateCode";
	public static final String FIELD_DFMAXDISCOUNTRATE="dfMaxDiscountRate";

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  public static final String FIELD_DFSYNTHETICTEASERRATE = "dfSyntheticTeaserRate";
  public static final String FIELD_DFPRIMEINDEXRATEPROFILEID = "dfPrimeIndexProfileId";
  public static final String FIELD_DFPRIMEINDEXNAME = "dfPrimeIndexName";
  public static final String FIELD_DFPRIMEINDEXRATE = "dfPrimeIndexRate";
  public static final String FIELD_DFPRIMEBASEADJ = "dfPrimeBaseAdj";
  public static final String FIELD_DFTEASERDISCOUNT = "dfTeaserDiscount";
  public static final String FIELD_DFTEASERTERM = "dfTeaserTerm";
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

