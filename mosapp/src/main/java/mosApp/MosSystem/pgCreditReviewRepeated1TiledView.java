package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;
/**
 *
 *
 *
 */
public class pgCreditReviewRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgCreditReviewRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doCreditReviewModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdBorrowerId().setValue(CHILD_HDBORROWERID_RESET_VALUE);
		getStBorrowerName().setValue(CHILD_STBORROWERNAME_RESET_VALUE);
		getStPrimaryBorrowerFlag().setValue(CHILD_STPRIMARYBORROWERFLAG_RESET_VALUE);
		getStBorrowerType().setValue(CHILD_STBORROWERTYPE_RESET_VALUE);
		getStExistingClient().setValue(CHILD_STEXISTINGCLIENT_RESET_VALUE);
		getTbExistingClientComments().setValue(CHILD_TBEXISTINGCLIENTCOMMENTS_RESET_VALUE);
		getStGDS().setValue(CHILD_STGDS_RESET_VALUE);
		getStGDS3Year().setValue(CHILD_STGDS3YEAR_RESET_VALUE);
		getStTDS().setValue(CHILD_STTDS_RESET_VALUE);
		getStTDS3Year().setValue(CHILD_STTDS3YEAR_RESET_VALUE);
		getStNetWorth().setValue(CHILD_STNETWORTH_RESET_VALUE);
		getCbBureauName().setValue(CHILD_CBBUREAUNAME_RESET_VALUE);
		getCbBureauMonth().setValue(CHILD_CBBUREAUMONTH_RESET_VALUE);
		getTbBureauDay().setValue(CHILD_TBBUREAUDAY_RESET_VALUE);
		getTbBureauYear().setValue(CHILD_TBBUREAUYEAR_RESET_VALUE);
		getCbBureauPulledMonth().setValue(CHILD_CBBUREAUPULLEDMONTH_RESET_VALUE);
		getTbBureauPulledDay().setValue(CHILD_TBBUREAUPULLEDDAY_RESET_VALUE);
		getTbBureauPulledYear().setValue(CHILD_TBBUREAUPULLEDYEAR_RESET_VALUE);
		getCbAuthorizationMonth().setValue(CHILD_CBAUTHORIZATIONMONTH_RESET_VALUE);
		getTbAuthorizationDay().setValue(CHILD_TBAUTHORIZATIONDAY_RESET_VALUE);
		getTbAuthorizationYear().setValue(CHILD_TBAUTHORIZATIONYEAR_RESET_VALUE);
		getTbAuthorizationMethod().setValue(CHILD_TBAUTHORIZATIONMETHOD_RESET_VALUE);
		getTbBureauSummary().setValue(CHILD_TBBUREAUSUMMARY_RESET_VALUE);
		getTbCreditScore().setValue(CHILD_TBCREDITSCORE_RESET_VALUE);
		getTbTimesBankrupt().setValue(CHILD_TBTIMESBANKRUPT_RESET_VALUE);
		getCbBankruptStatus().setValue(CHILD_CBBANKRUPTSTATUS_RESET_VALUE);
		getCbNSFHistory().setValue(CHILD_CBNSFHISTORY_RESET_VALUE);
		getCbPaymentHistory().setValue(CHILD_CBPAYMENTHISTORY_RESET_VALUE);
		getCbGeneralStatus().setValue(CHILD_CBGENERALSTATUS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDBORROWERID,HiddenField.class);
		registerChild(CHILD_STBORROWERNAME,StaticTextField.class);
		registerChild(CHILD_STPRIMARYBORROWERFLAG,StaticTextField.class);
		registerChild(CHILD_STBORROWERTYPE,StaticTextField.class);
		registerChild(CHILD_STEXISTINGCLIENT,StaticTextField.class);
		registerChild(CHILD_TBEXISTINGCLIENTCOMMENTS,TextField.class);
		registerChild(CHILD_STGDS,StaticTextField.class);
		registerChild(CHILD_STGDS3YEAR,StaticTextField.class);
		registerChild(CHILD_STTDS,StaticTextField.class);
		registerChild(CHILD_STTDS3YEAR,StaticTextField.class);
		registerChild(CHILD_STNETWORTH,StaticTextField.class);
		registerChild(CHILD_CBBUREAUNAME,ComboBox.class);
		registerChild(CHILD_CBBUREAUMONTH,ComboBox.class);
		registerChild(CHILD_TBBUREAUDAY,TextField.class);
		registerChild(CHILD_TBBUREAUYEAR,TextField.class);
		registerChild(CHILD_CBBUREAUPULLEDMONTH,ComboBox.class);
		registerChild(CHILD_TBBUREAUPULLEDDAY,TextField.class);
		registerChild(CHILD_TBBUREAUPULLEDYEAR,TextField.class);
		registerChild(CHILD_CBAUTHORIZATIONMONTH,ComboBox.class);
		registerChild(CHILD_TBAUTHORIZATIONDAY,TextField.class);
		registerChild(CHILD_TBAUTHORIZATIONYEAR,TextField.class);
		registerChild(CHILD_TBAUTHORIZATIONMETHOD,TextField.class);
		registerChild(CHILD_TBBUREAUSUMMARY,TextField.class);
		registerChild(CHILD_TBCREDITSCORE,TextField.class);
		registerChild(CHILD_TBTIMESBANKRUPT,TextField.class);
		registerChild(CHILD_CBBANKRUPTSTATUS,ComboBox.class);
		registerChild(CHILD_CBNSFHISTORY,ComboBox.class);
		registerChild(CHILD_CBPAYMENTHISTORY,ComboBox.class);
		registerChild(CHILD_CBGENERALSTATUS,ComboBox.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoCreditReviewModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		resetTileIndex();

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By John 26Nov2002
    cbBureauNameOptions.populate(getRequestContext());
    cbBureauMonthOptions.populate(getRequestContext());
    cbBureauPulledMonthOptions.populate(getRequestContext());
    cbAuthorizationMonthOptions.populate(getRequestContext());
    cbBankruptStatusOptions.populate(getRequestContext());
    cbNSFHistoryOptions.populate(getRequestContext());
    cbPaymentHistoryOptions.populate(getRequestContext());
    cbGeneralStatusOptions.populate(getRequestContext());
    //=====================================

    super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
      // The following code block was migrated from the Repeated1_onBeforeRowDisplayEvent method
			CreditReviewHandler handler =(CreditReviewHandler) this.handler.cloneSS();
		  handler.pageGetState(this.getParentViewBean());
  		handler.populateBureauDates(getTileIndex());
  		handler.populateFlags(getTileIndex());
  		handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

		// The following code block was migrated from the Repeated1_onAfterDataObjectExecuteEvent method
    //--> It is not necessary to save the Model.  As we shpuld get the Model from RequestManager
    //--> Commented out by Billy 30Aug2002
    /*
    CreditReviewHandler handler =(CreditReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.saveDataObject(event.getDataObject());
		handler.pageSaveState();
    */
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDBORROWERID))
		{
			HiddenField child = new HiddenField(this,
				getdoCreditReviewModel(),
				CHILD_HDBORROWERID,
				doCreditReviewModel.FIELD_DFBORROWERID,
				CHILD_HDBORROWERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORROWERNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoCreditReviewModel(),
				CHILD_STBORROWERNAME,
				doCreditReviewModel.FIELD_DFBORROWERNAME,
				CHILD_STBORROWERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPRIMARYBORROWERFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPRIMARYBORROWERFLAG,
				CHILD_STPRIMARYBORROWERFLAG,
				CHILD_STPRIMARYBORROWERFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORROWERTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoCreditReviewModel(),
				CHILD_STBORROWERTYPE,
				doCreditReviewModel.FIELD_DFBORROWERTYPE,
				CHILD_STBORROWERTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEXISTINGCLIENT))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STEXISTINGCLIENT,
				CHILD_STEXISTINGCLIENT,
				CHILD_STEXISTINGCLIENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBEXISTINGCLIENTCOMMENTS))
		{
			TextField child = new TextField(this,
				getdoCreditReviewModel(),
				CHILD_TBEXISTINGCLIENTCOMMENTS,
				doCreditReviewModel.FIELD_DFEXISTCLIENTCOMMENTS,
				CHILD_TBEXISTINGCLIENTCOMMENTS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoCreditReviewModel(),
				CHILD_STGDS,
				doCreditReviewModel.FIELD_DFGTS,
				CHILD_STGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STGDS3YEAR))
		{
			StaticTextField child = new StaticTextField(this,
				getdoCreditReviewModel(),
				CHILD_STGDS3YEAR,
				doCreditReviewModel.FIELD_DFGTS3YEAR,
				CHILD_STGDS3YEAR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoCreditReviewModel(),
				CHILD_STTDS,
				doCreditReviewModel.FIELD_DFTDS,
				CHILD_STTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTDS3YEAR))
		{
			StaticTextField child = new StaticTextField(this,
				getdoCreditReviewModel(),
				CHILD_STTDS3YEAR,
				doCreditReviewModel.FIELD_DFTDS3YEAR,
				CHILD_STTDS3YEAR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STNETWORTH))
		{
			StaticTextField child = new StaticTextField(this,
				getdoCreditReviewModel(),
				CHILD_STNETWORTH,
				doCreditReviewModel.FIELD_DFNETWORTH,
				CHILD_STNETWORTH_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBBUREAUNAME))
		{
			ComboBox child = new ComboBox( this,
				getdoCreditReviewModel(),
				CHILD_CBBUREAUNAME,
				doCreditReviewModel.FIELD_DFBUREAUNAMEID,
				CHILD_CBBUREAUNAME_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbBureauNameOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBBUREAUMONTH))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBBUREAUMONTH,
				CHILD_CBBUREAUMONTH,
				CHILD_CBBUREAUMONTH_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbBureauMonthOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBBUREAUDAY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBUREAUDAY,
				CHILD_TBBUREAUDAY,
				CHILD_TBBUREAUDAY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBUREAUYEAR))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBUREAUYEAR,
				CHILD_TBBUREAUYEAR,
				CHILD_TBBUREAUYEAR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBBUREAUPULLEDMONTH))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBBUREAUPULLEDMONTH,
				CHILD_CBBUREAUPULLEDMONTH,
				CHILD_CBBUREAUPULLEDMONTH_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbBureauPulledMonthOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBBUREAUPULLEDDAY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBUREAUPULLEDDAY,
				CHILD_TBBUREAUPULLEDDAY,
				CHILD_TBBUREAUPULLEDDAY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBUREAUPULLEDYEAR))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBUREAUPULLEDYEAR,
				CHILD_TBBUREAUPULLEDYEAR,
				CHILD_TBBUREAUPULLEDYEAR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBAUTHORIZATIONMONTH))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBAUTHORIZATIONMONTH,
				CHILD_CBAUTHORIZATIONMONTH,
				CHILD_CBAUTHORIZATIONMONTH_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbAuthorizationMonthOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBAUTHORIZATIONDAY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBAUTHORIZATIONDAY,
				CHILD_TBAUTHORIZATIONDAY,
				CHILD_TBAUTHORIZATIONDAY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBAUTHORIZATIONYEAR))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBAUTHORIZATIONYEAR,
				CHILD_TBAUTHORIZATIONYEAR,
				CHILD_TBAUTHORIZATIONYEAR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBAUTHORIZATIONMETHOD))
		{
			TextField child = new TextField(this,
				getdoCreditReviewModel(),
				CHILD_TBAUTHORIZATIONMETHOD,
				doCreditReviewModel.FIELD_DFAUTHORIZATIONMETHOD,
				CHILD_TBAUTHORIZATIONMETHOD_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBUREAUSUMMARY))
		{
			TextField child = new TextField(this,
				getdoCreditReviewModel(),
				CHILD_TBBUREAUSUMMARY,
				doCreditReviewModel.FIELD_DFCREDITBUREAUSUMMARY,
				CHILD_TBBUREAUSUMMARY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBCREDITSCORE))
		{
			TextField child = new TextField(this,
				getdoCreditReviewModel(),
				CHILD_TBCREDITSCORE,
				doCreditReviewModel.FIELD_DFCREDITSCORE,
				CHILD_TBCREDITSCORE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBTIMESBANKRUPT))
		{
			TextField child = new TextField(this,
				getdoCreditReviewModel(),
				CHILD_TBTIMESBANKRUPT,
				doCreditReviewModel.FIELD_DFTIMESBANKRUPT,
				CHILD_TBTIMESBANKRUPT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBBANKRUPTSTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoCreditReviewModel(),
				CHILD_CBBANKRUPTSTATUS,
				doCreditReviewModel.FIELD_DFBANKRUPTSTATUSID,
				CHILD_CBBANKRUPTSTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbBankruptStatusOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBNSFHISTORY))
		{
			ComboBox child = new ComboBox( this,
				getdoCreditReviewModel(),
				CHILD_CBNSFHISTORY,
				doCreditReviewModel.FIELD_NSFOCCURENCESTATUSID,
				CHILD_CBNSFHISTORY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbNSFHistoryOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAYMENTHISTORY))
		{
			ComboBox child = new ComboBox( this,
				getdoCreditReviewModel(),
				CHILD_CBPAYMENTHISTORY,
				doCreditReviewModel.FIELD_DFPAYMENTHISTORYTYPEID,
				CHILD_CBPAYMENTHISTORY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPaymentHistoryOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBGENERALSTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoCreditReviewModel(),
				CHILD_CBGENERALSTATUS,
				doCreditReviewModel.FIELD_DFBORROWERGENERALSTATUSID,
				CHILD_CBGENERALSTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbGeneralStatusOptions);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdBorrowerId()
	{
		return (HiddenField)getChild(CHILD_HDBORROWERID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerName()
	{
		return (StaticTextField)getChild(CHILD_STBORROWERNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrimaryBorrowerFlag()
	{
		return (StaticTextField)getChild(CHILD_STPRIMARYBORROWERFLAG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerType()
	{
		return (StaticTextField)getChild(CHILD_STBORROWERTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStExistingClient()
	{
		return (StaticTextField)getChild(CHILD_STEXISTINGCLIENT);
	}


	/**
	 *
	 *
	 */
	public TextField getTbExistingClientComments()
	{
		return (TextField)getChild(CHILD_TBEXISTINGCLIENTCOMMENTS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStGDS()
	{
		return (StaticTextField)getChild(CHILD_STGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStGDS3Year()
	{
		return (StaticTextField)getChild(CHILD_STGDS3YEAR);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTDS()
	{
		return (StaticTextField)getChild(CHILD_STTDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTDS3Year()
	{
		return (StaticTextField)getChild(CHILD_STTDS3YEAR);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNetWorth()
	{
		return (StaticTextField)getChild(CHILD_STNETWORTH);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbBureauName()
	{
		return (ComboBox)getChild(CHILD_CBBUREAUNAME);
	}

	/**
	 *
	 *
	 */
	static class CbBureauNameOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbBureauNameOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "CREDITBUREAUNAME", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbBureauMonth()
	{
		return (ComboBox)getChild(CHILD_CBBUREAUMONTH);
	}

	/**
	 *
	 *
	 */
	static class CbBureauMonthOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbBureauMonthOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "MONTHS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public TextField getTbBureauDay()
	{
		return (TextField)getChild(CHILD_TBBUREAUDAY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBureauYear()
	{
		return (TextField)getChild(CHILD_TBBUREAUYEAR);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbBureauPulledMonth()
	{
		return (ComboBox)getChild(CHILD_CBBUREAUPULLEDMONTH);
	}

	/**
	 *
	 *
	 */
	static class CbBureauPulledMonthOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbBureauPulledMonthOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                                            "MONTHS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public TextField getTbBureauPulledDay()
	{
		return (TextField)getChild(CHILD_TBBUREAUPULLEDDAY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBureauPulledYear()
	{
		return (TextField)getChild(CHILD_TBBUREAUPULLEDYEAR);
	}

	
	/**
	 *
	 *
	 */
	public ComboBox getCbAuthorizationMonth()
	{
		return (ComboBox)getChild(CHILD_CBBUREAUPULLEDMONTH);
	}

	/**
	 *
	 *
	 */
	static class CbAuthorizationMonthOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbAuthorizationMonthOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
     try
     {
       //Get Language from SessionState
       //Get the Language ID from the SessionStateModel
       String defaultInstanceStateName =
         rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
       SessionStateModelImpl theSessionState =
           (SessionStateModelImpl)rc.getModelManager().getModel(
                           SessionStateModel.class,
                           defaultInstanceStateName,
                           true);

       int languageId = theSessionState.getLanguageId();

       //Get IDs and Labels from BXResources
       Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                                           "MONTHS", languageId);
       Iterator<String[]> l = c.iterator();
       String[] theVal = new String[2];

       while(l.hasNext())
       {
         theVal = (String[])(l.next());
         add(theVal[1], theVal[0]);
       }
			}
			catch (Exception ex)
     {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public TextField getTbAuthorizationDay()
	{
		return (TextField)getChild(CHILD_TBAUTHORIZATIONDAY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbAuthorizationYear()
	{
		return (TextField)getChild(CHILD_TBAUTHORIZATIONYEAR);
	}


	/**
	 *
	 *
	 */
	public TextField getTbAuthorizationMethod()
	{
		return (TextField)getChild(CHILD_TBAUTHORIZATIONMETHOD);
	}
	
	/**
	 *
	 *
	 */
	public TextField getTbBureauSummary()
	{
		return (TextField)getChild(CHILD_TBBUREAUSUMMARY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbCreditScore()
	{
		return (TextField)getChild(CHILD_TBCREDITSCORE);
	}


	/**
	 *
	 *
	 */
	public TextField getTbTimesBankrupt()
	{
		return (TextField)getChild(CHILD_TBTIMESBANKRUPT);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbBankruptStatus()
	{
		return (ComboBox)getChild(CHILD_CBBANKRUPTSTATUS);
	}

	/**
	 *
	 *
	 */
	static class CbBankruptStatusOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbBankruptStatusOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                                            "BANKRUPTCYSTATUS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbNSFHistory()
	{
		return (ComboBox)getChild(CHILD_CBNSFHISTORY);
	}

	/**
	 *
	 *
	 */
	static class CbNSFHistoryOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbNSFHistoryOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "NSFOCCURENCETYPE", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbPaymentHistory()
	{
		return (ComboBox)getChild(CHILD_CBPAYMENTHISTORY);
	}

	/**
	 *
	 *
	 */
	static class CbPaymentHistoryOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbPaymentHistoryOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                                            "PAYMENTHISTORYTYPE", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbGeneralStatus()
	{
		return (ComboBox)getChild(CHILD_CBGENERALSTATUS);
	}

	/**
	 *
	 *
	 */
	static class CbGeneralStatusOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbGeneralStatusOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                                            "BORROWERGENERALSTATUS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];

        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
		}
	}



	/**
	 *
	 *
	 */
	public doCreditReviewModel getdoCreditReviewModel()
	{
		if (doCreditReview == null)
			doCreditReview = (doCreditReviewModel) getModel(doCreditReviewModel.class);
		return doCreditReview;
	}


	/**
	 *
	 *
	 */
	public void setdoCreditReviewModel(doCreditReviewModel model)
	{
			doCreditReview = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDBORROWERID="hdBorrowerId";
	public static final String CHILD_HDBORROWERID_RESET_VALUE="";
	public static final String CHILD_STBORROWERNAME="stBorrowerName";
	public static final String CHILD_STBORROWERNAME_RESET_VALUE="";
	public static final String CHILD_STPRIMARYBORROWERFLAG="stPrimaryBorrowerFlag";
	public static final String CHILD_STPRIMARYBORROWERFLAG_RESET_VALUE="";
	public static final String CHILD_STBORROWERTYPE="stBorrowerType";
	public static final String CHILD_STBORROWERTYPE_RESET_VALUE="";
	public static final String CHILD_STEXISTINGCLIENT="stExistingClient";
	public static final String CHILD_STEXISTINGCLIENT_RESET_VALUE="";
	public static final String CHILD_TBEXISTINGCLIENTCOMMENTS="tbExistingClientComments";
	public static final String CHILD_TBEXISTINGCLIENTCOMMENTS_RESET_VALUE="";
	public static final String CHILD_STGDS="stGDS";
	public static final String CHILD_STGDS_RESET_VALUE="";
	public static final String CHILD_STGDS3YEAR="stGDS3Year";
	public static final String CHILD_STGDS3YEAR_RESET_VALUE="";
	public static final String CHILD_STTDS="stTDS";
	public static final String CHILD_STTDS_RESET_VALUE="";
	public static final String CHILD_STTDS3YEAR="stTDS3Year";
	public static final String CHILD_STTDS3YEAR_RESET_VALUE="";
	public static final String CHILD_STNETWORTH="stNetWorth";
	public static final String CHILD_STNETWORTH_RESET_VALUE="";
	public static final String CHILD_CBBUREAUNAME="cbBureauName";
	public static final String CHILD_CBBUREAUNAME_RESET_VALUE="0";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 26Nov2002
	//private static CbBureauNameOptionList cbBureauNameOptions=new CbBureauNameOptionList();
  private CbBureauNameOptionList cbBureauNameOptions=new CbBureauNameOptionList();
  //==========================================================================

	public static final String CHILD_CBBUREAUMONTH="cbBureauMonth";
	public static final String CHILD_CBBUREAUMONTH_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 26Nov2002
	//private static CbBureauMonthOptionList cbBureauMonthOptions=new CbBureauMonthOptionList();
	private CbBureauMonthOptionList cbBureauMonthOptions=new CbBureauMonthOptionList();
  //==========================================================================

	public static final String CHILD_TBBUREAUDAY="tbBureauDay";
	public static final String CHILD_TBBUREAUDAY_RESET_VALUE="";
	public static final String CHILD_TBBUREAUYEAR="tbBureauYear";
	public static final String CHILD_TBBUREAUYEAR_RESET_VALUE="";
	public static final String CHILD_CBBUREAUPULLEDMONTH="cbBureauPulledMonth";
	public static final String CHILD_CBBUREAUPULLEDMONTH_RESET_VALUE="";
	
  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 26Nov2002
	//private static CbBureauPulledMonthOptionList cbBureauPulledMonthOptions=new CbBureauPulledMonthOptionList();
	private CbBureauPulledMonthOptionList cbBureauPulledMonthOptions=new CbBureauPulledMonthOptionList();
  //==========================================================================

	public static final String CHILD_TBBUREAUPULLEDDAY="tbBureauPulledDay";
	public static final String CHILD_TBBUREAUPULLEDDAY_RESET_VALUE="";
	public static final String CHILD_TBBUREAUPULLEDYEAR="tbBureauPulledYear";
	public static final String CHILD_TBBUREAUPULLEDYEAR_RESET_VALUE="";
	public static final String CHILD_TBBUREAUSUMMARY="tbBureauSummary";
	public static final String CHILD_TBBUREAUSUMMARY_RESET_VALUE="";
	public static final String CHILD_TBCREDITSCORE="tbCreditScore";
	public static final String CHILD_TBCREDITSCORE_RESET_VALUE="";
	public static final String CHILD_TBTIMESBANKRUPT="tbTimesBankrupt";
	public static final String CHILD_TBTIMESBANKRUPT_RESET_VALUE="";
	public static final String CHILD_CBBANKRUPTSTATUS="cbBankruptStatus";
	public static final String CHILD_CBBANKRUPTSTATUS_RESET_VALUE="0";

	public static final String CHILD_CBAUTHORIZATIONMONTH="cbAuthorizationMonth";
	public static final String CHILD_CBAUTHORIZATIONMONTH_RESET_VALUE="";
	
	private CbAuthorizationMonthOptionList cbAuthorizationMonthOptions = new CbAuthorizationMonthOptionList();

	public static final String CHILD_TBAUTHORIZATIONDAY="tbAuthorizationDay";
	public static final String CHILD_TBAUTHORIZATIONDAY_RESET_VALUE="";
	public static final String CHILD_TBAUTHORIZATIONYEAR="tbAuthorizationYear";
	public static final String CHILD_TBAUTHORIZATIONYEAR_RESET_VALUE="";
	
	public static final String CHILD_TBAUTHORIZATIONMETHOD="tbAuthorizationMethod";
	public static final String CHILD_TBAUTHORIZATIONMETHOD_RESET_VALUE="";
  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 26Nov2002
	//private static CbBankruptStatusOptionList cbBankruptStatusOptions=new CbBankruptStatusOptionList();
	private CbBankruptStatusOptionList cbBankruptStatusOptions=new CbBankruptStatusOptionList();
  //==========================================================================

	public static final String CHILD_CBNSFHISTORY="cbNSFHistory";
	public static final String CHILD_CBNSFHISTORY_RESET_VALUE="0";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 26Nov2002
	//private static CbNSFHistoryOptionList cbNSFHistoryOptions=new CbNSFHistoryOptionList();
  private CbNSFHistoryOptionList cbNSFHistoryOptions=new CbNSFHistoryOptionList();
  //==========================================================================

	public static final String CHILD_CBPAYMENTHISTORY="cbPaymentHistory";
	public static final String CHILD_CBPAYMENTHISTORY_RESET_VALUE="0";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 26Nov2002
	//private static CbPaymentHistoryOptionList cbPaymentHistoryOptions=new CbPaymentHistoryOptionList();
  private CbPaymentHistoryOptionList cbPaymentHistoryOptions=new CbPaymentHistoryOptionList();
  //==========================================================================

	public static final String CHILD_CBGENERALSTATUS="cbGeneralStatus";
	public static final String CHILD_CBGENERALSTATUS_RESET_VALUE="0";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 26Nov2002
	//private static CbGeneralStatusOptionList cbGeneralStatusOptions=new CbGeneralStatusOptionList();
	private CbGeneralStatusOptionList cbGeneralStatusOptions=new CbGeneralStatusOptionList();
  //==========================================================================


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doCreditReviewModel doCreditReview=null;
  private CreditReviewHandler handler=new CreditReviewHandler();

}

