package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doUWPropertyInfosModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryPropertyFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryPropertyFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyStreetNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyStreetName();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyStreetType();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyStreetDirection();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetDirection(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyUnitNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyUnitNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyProvince();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyProvince(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyPostalFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyPostalFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyPostalLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyPostalLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyTotalEstimatedValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyTotalEstimatedValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyActualAppraisalValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyActualAppraisalValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyTypeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyOccupancyTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyOccupancyTypeDesc(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFPROPERTYID="dfPropertyId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFPRIMARYPROPERTYFLAG="dfPrimaryPropertyFlag";
	public static final String FIELD_DFPROPERTYSTREETNUMBER="dfPropertyStreetNumber";
	public static final String FIELD_DFPROPERTYSTREETNAME="dfPropertyStreetName";
	public static final String FIELD_DFPROPERTYSTREETTYPE="dfPropertyStreetType";
	public static final String FIELD_DFPROPERTYSTREETDIRECTION="dfPropertyStreetDirection";
	public static final String FIELD_DFPROPERTYUNITNUMBER="dfPropertyUnitNumber";
	public static final String FIELD_DFPROPERTYCITY="dfPropertyCity";
	public static final String FIELD_DFPROPERTYPROVINCE="dfPropertyProvince";
	public static final String FIELD_DFPROPERTYPOSTALFSA="dfPropertyPostalFSA";
	public static final String FIELD_DFPROPERTYPOSTALLDU="dfPropertyPostalLDU";
	public static final String FIELD_DFPROPERTYPURCHASEPRICE="dfPropertyPurchasePrice";
	public static final String FIELD_DFPROPERTYTOTALESTIMATEDVALUE="dfPropertyTotalEstimatedValue";
	public static final String FIELD_DFPROPERTYACTUALAPPRAISALVALUE="dfPropertyActualAppraisalValue";
	public static final String FIELD_DFPROPERTYTYPEDESC="dfPropertyTypeDesc";
	public static final String FIELD_DFPROPERTYOCCUPANCYTYPEDESC="dfPropertyOccupancyTypeDesc";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

