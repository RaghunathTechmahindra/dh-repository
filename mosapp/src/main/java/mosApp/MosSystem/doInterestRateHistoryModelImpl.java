package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.picklist.BXResources;

import com.basis100.log.*;


/**
 *
 *
 *
 */
public class doInterestRateHistoryModelImpl extends QueryModelBase
	implements doInterestRateHistoryModel
{
	/**
	 *
	 *
	 */
	public doInterestRateHistoryModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

    logger = SysLog.getSysLogger("DOIRHM");
    logger.debug("DOIRHM@BeforeExecute: " + sql);
		return sql;

	}

	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //To override all the Description fields by populating from BXResource
    String defaultInstanceStateName = getRequestContext().getModelManager().getDefaultModelInstanceName(
                                      SessionStateModel.class);

    SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext().getModelManager().getModel(
                                             SessionStateModel.class,
                                             defaultInstanceStateName,
                                             true
                                             );
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    while (this.next())
    {
        this.setDfPrimeIndexName(BXResources.getPickListDescription(
                                institutionId, "PRIMEINDEXRATEPROFILE",
                                this.getDfPrimeIndexRateProfileId().intValue(),
                                languageId)
                                );
	   }
	}

	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEffectiveDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFEFFECTIVEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfEffectiveDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFEFFECTIVEDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getStPostedRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_STPOSTEDRATE);
	}


	/**
	 *
	 *
	 */
	public void setStPostedRate(java.math.BigDecimal value)
	{
		setValue(FIELD_STPOSTEDRATE,value);
	}


	/**
	 *
	 *
	 */
	public Double getDfBestRate()
	{
		return (Double)getValue(FIELD_DFBESTRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfBestRate(Double value)
	{
		setValue(FIELD_DFBESTRATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRateCode()
	{
		return (String)getValue(FIELD_DFRATECODE);
	}


	/**
	 *
	 *
	 */
	public void setDfRateCode(String value)
	{
		setValue(FIELD_DFRATECODE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaxDiscountRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMAXDISCOUNTRATE);
	}

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  /**
   *
   *
   */
  public java.math.BigDecimal getDfSyntheticTeaserRate()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFSYNTHETICTEASERRATE);
  }

  /**
   *
   *
   */
  public void setDfSyntheticTeaserRate(java.math.BigDecimal value)
  {
    setValue(FIELD_DFSYNTHETICTEASERRATE,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPrimeIndexRateProfileId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPRIMEINDEXRATEPROFILEID);
  }

  /**
   *
   *
   */
  public void setDfPrimeIndexRateProfileId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPRIMEINDEXRATEPROFILEID,value);
  }

  /**
   *
   *
   */
  public Double getDfPrimeIndexRate()
  {
    return (Double)getValue(FIELD_DFPRIMEINDEXRATE);
  }

  /**
   *
   *
   */
  public void setDfPrimeIndexRate(Double value)
  {
    setValue(FIELD_DFPRIMEINDEXRATE,value);
  }

  /**
   *
   *
   */
  public Double getDfPrimeBaseAdj()
  {
    return (Double)getValue(FIELD_DFPRIMEBASEADJ);
  }

  /**
   *
   *
   */
  public void setDfPrimeBaseAdj(Double value)
  {
    setValue(FIELD_DFPRIMEBASEADJ,value);
  }



  /**
   *
   *
   */
  public Double getDfTeaserDiscount()
  {
    return (Double)getValue(FIELD_DFTEASERDISCOUNT);
  }

  /**
   *
   *
   */
  public void setDfTeaserDiscount(Double value)
  {
    setValue(FIELD_DFTEASERDISCOUNT,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTeaserTerm()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFTEASERTERM);
  }

  /**
   *
   *
   */
  public void setDfTeaserTerm(java.math.BigDecimal value)
  {
    setValue(FIELD_DFTEASERTERM,value);
  }

    /**
     *
     *
     */
    public String getDfPrimeIndexName()
    {
      return (String)getValue(FIELD_DFPRIMEINDEXNAME);
    }

    /**
     *
     *
     */
    public void setDfPrimeIndexName(String value)
    {
      setValue(FIELD_DFPRIMEINDEXNAME,value);
    }
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	/**
	 *
	 *
	 */
	public void setDfMaxDiscountRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMAXDISCOUNTRATE,value);
	}


	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doInterestRateHistoryModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL PRICINGRATEINVENTORY.INDEXEFFECTIVEDATE, PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE, PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE - PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED  BESTRATE, PRICINGPROFILE.RATECODE, PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED FROM PRICINGPROFILE, PRICINGRATEINVENTORY  __WHERE__  ORDER BY PRICINGRATEINVENTORY.INDEXEFFECTIVEDATE  DESC";
  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PRICINGRATEINVENTORY.INDEXEFFECTIVEDATE, " +
                                                 "PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE, " +
                                                 "PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE - PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED SYNTHETICBESTRATE, " +
                                                 "PRICINGPROFILE.RATECODE, PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED, " +

                                                 //--Ticket#1736--18July2005--start--//
                                                 //"PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE - MTGPROD.TEASERDISCOUNT SYNTHETICTEASERRATE, " +
                                                 "PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE - PRICINGRATEINVENTORY.TEASERDISCOUNT SYNTHETICTEASERRATE, " +
                                                 //--Ticket#1736--18July2005--end--//

                                                 "PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATEPROFILEID, PRIMEINDEXRATEPROFILE.PRIMEINDEXNAME," +

                                                 "PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATE, PRICINGRATEINVENTORY.PRIMEBASEADJ, " +
                                                 //--Ticket#1736--18July2005--start--//
                                                 //"MTGPROD.TEASERDISCOUNT, MTGPROD.TEASERTERM  " +
                                                 "PRICINGRATEINVENTORY.TEASERDISCOUNT, PRICINGRATEINVENTORY.TEASERTERM  " +
                                                 //--Ticket#1736--18July2005--end--//

                                                 "FROM PRICINGPROFILE, PRICINGRATEINVENTORY,  " +
                                                 "MTGPROD, PRIMEINDEXRATEPROFILE, PRIMEINDEXRATEINVENTORY " +
                                                 "__WHERE__  ORDER BY PRICINGRATEINVENTORY.INDEXEFFECTIVEDATE  DESC";
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	public static final String MODIFYING_QUERY_TABLE_NAME="PRICINGPROFILE, PRICINGRATEINVENTORY";

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
	public static final String STATIC_WHERE_CRITERIA=" (PRICINGRATEINVENTORY.PRICINGPROFILEID  =  PRICINGPROFILE.PRICINGPROFILEID) AND" +
                                                   " (MTGPROD.PRICINGPROFILEID  =  PRICINGPROFILE.PRICINGPROFILEID) AND" +
                                                   " (PRICINGPROFILE.PRIMEINDEXRATEPROFILEID  =  PRIMEINDEXRATEPROFILE.PRIMEINDEXRATEPROFILEID) AND" +
                                                   " (PRICINGRATEINVENTORY.PRIMEINDEXRATEINVENTORYID  =  PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATEINVENTORYID) ";
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFEFFECTIVEDATE="PRICINGRATEINVENTORY.INDEXEFFECTIVEDATE";
	public static final String COLUMN_DFEFFECTIVEDATE="INDEXEFFECTIVEDATE";
	public static final String QUALIFIED_COLUMN_STPOSTEDRATE="PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE";
	public static final String COLUMN_STPOSTEDRATE="INTERNALRATEPERCENTAGE";
	////public static final String QUALIFIED_COLUMN_DFBESTRATE=".";
	////public static final String COLUMN_DFBESTRATE="";
	public static final String QUALIFIED_COLUMN_DFBESTRATE="PRICINGRATEINVENTORY.SYNTHETICBESTRATE";
	public static final String COLUMN_DFBESTRATE="SYNTHETICBESTRATE";
	public static final String QUALIFIED_COLUMN_DFRATECODE="PRICINGPROFILE.RATECODE";
	public static final String COLUMN_DFRATECODE="RATECODE";
	public static final String QUALIFIED_COLUMN_DFMAXDISCOUNTRATE="PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED";
	public static final String COLUMN_DFMAXDISCOUNTRATE="MAXIMUMDISCOUNTALLOWED";

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  //--Ticket#1736--18July2005--start--//
  //public static final String QUALIFIED_COLUMN_DFSYNTHETICTEASERRATE="MTGPROD.SYNTHETICTEASERRATE";
  public static final String QUALIFIED_COLUMN_DFSYNTHETICTEASERRATE="PRICINGRATEINVENTORY.SYNTHETICTEASERRATE";
  public static final String COLUMN_DFSYNTHETICTEASERRATE="SYNTHETICTEASERRATE";
  //--Ticket#1736--18July2005--end--//

  public static final String QUALIFIED_COLUMN_DFPRIMEINDEXRATEPROFILEID="PRIMEINDEXRATEPROFILE.PRIMEINDEXRATEPROFILEID";
  public static final String COLUMN_DFPRIMEINDEXRATEPROFILEID="PRIMEINDEXRATEPROFILEID";

  public static final String QUALIFIED_COLUMN_DFPRIMEINDEXNAME="PRIMEINDEXRATEPROFILE.PRIMEINDEXNAME";
  public static final String COLUMN_DFPRIMEINDEXNAME="PRIMEINDEXNAME";

  public static final String QUALIFIED_COLUMN_DFPRIMEINDEXRATE="PRIMEINDEXRATEINVENTORY.PRIMEINDEXRATE";
  public static final String COLUMN_DFPRIMEINDEXRATE="PRIMEINDEXRATE";

  public static final String QUALIFIED_COLUMN_DFPRIMEBASEADJ="PRICINGRATEINVENTORY.PRIMEBASEADJ";
  public static final String COLUMN_DFPRIMEBASEADJ="PRIMEBASEADJ";

  //--Ticket#1736--18July2005--start--//
  //public static final String QUALIFIED_COLUMN_DFTEASERDISCOUNT="MTGPROD.TEASERDISCOUNT";
  public static final String QUALIFIED_COLUMN_DFTEASERDISCOUNT="PRICINGRATEINVENTORY.TEASERDISCOUNT";
  public static final String COLUMN_DFTEASERDISCOUNT="TEASERDISCOUNT";

  //public static final String QUALIFIED_COLUMN_DFTEASERTERM="MTGPROD.TEASERTERM";
  public static final String QUALIFIED_COLUMN_DFTEASERTERM="PRICINGRATEINVENTORY.TEASERTERM";
  public static final String COLUMN_DFTEASERTERM="TEASERTERM";
  //--Ticket#1736--18July2005--start--//
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEFFECTIVEDATE,
				COLUMN_DFEFFECTIVEDATE,
				QUALIFIED_COLUMN_DFEFFECTIVEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_STPOSTEDRATE,
				COLUMN_STPOSTEDRATE,
				QUALIFIED_COLUMN_STPOSTEDRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFBESTRATE,
		////		COLUMN_DFBESTRATE,
		////		QUALIFIED_COLUMN_DFBESTRATE,
		////		Double.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjustment for the FieldDescriptor improper converted by iMT tool for the
    //// ComputedColumn.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBESTRATE,
				COLUMN_DFBESTRATE,
				QUALIFIED_COLUMN_DFBESTRATE,
        java.math.BigDecimal.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE - PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE - PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATECODE,
				COLUMN_DFRATECODE,
				QUALIFIED_COLUMN_DFRATECODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMAXDISCOUNTRATE,
				COLUMN_DFMAXDISCOUNTRATE,
				QUALIFIED_COLUMN_DFMAXDISCOUNTRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    // ComputedColumn: Special treatment
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFSYNTHETICTEASERRATE,
      COLUMN_DFSYNTHETICTEASERRATE,
      QUALIFIED_COLUMN_DFSYNTHETICTEASERRATE,
      java.math.BigDecimal.class,
      false,
      true,
      QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
      "PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE - PRICINGRATEINVENTORY.TEASERDISCOUNT",
      QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
      "PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE - PRICINGRATEINVENTORY.TEASERDISCOUNT"));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPRIMEINDEXRATEPROFILEID,
        COLUMN_DFPRIMEINDEXRATEPROFILEID,
        QUALIFIED_COLUMN_DFPRIMEINDEXRATEPROFILEID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFPRIMEINDEXRATE,
        COLUMN_DFPRIMEINDEXRATE,
        QUALIFIED_COLUMN_DFPRIMEINDEXRATE,
        java.math.BigDecimal.class,
        false,
        true,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFPRIMEBASEADJ,
        COLUMN_DFPRIMEBASEADJ,
        QUALIFIED_COLUMN_DFPRIMEBASEADJ,
        java.math.BigDecimal.class,
        false,
        true,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFTEASERDISCOUNT,
        COLUMN_DFTEASERDISCOUNT,
        QUALIFIED_COLUMN_DFTEASERDISCOUNT,
        java.math.BigDecimal.class,
        false,
        true,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFTEASERTERM,
          COLUMN_DFTEASERTERM,
          QUALIFIED_COLUMN_DFTEASERTERM,
          java.math.BigDecimal.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));

      FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
          FIELD_DFPRIMEINDEXNAME,
          COLUMN_DFPRIMEINDEXNAME,
          QUALIFIED_COLUMN_DFPRIMEINDEXNAME,
          String.class,
          false,
          false,
          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
          "",
          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
          ""));
 //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
	}

}

