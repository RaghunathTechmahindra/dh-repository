package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;


//--Release2.1--//
//// Important!! This model is obsolete now.
//// paymentTermId is passing now via the new hdPaymentTermId field
//// in order to propagate the paymentTermId from the screen
//// to the database via dbprop framework method. The JS families on UW
//// and DE pages have been adjusted correspondingly.

/**
 *
 *
 *
 */
public interface doGetPaymentTermIdModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaymentTermId();


	/**
	 *
	 *
	 */
	public void setDfPaymentTermId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfPaymentTermDescription();


	/**
	 *
	 *
	 */
	public void setDfPaymentTermDescription(String value);




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFPAYMENTTERMID="dfPaymentTermId";
	public static final String FIELD_DFPAYMENTTERMDESCRIPTION="dfPaymentTermDescription";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

