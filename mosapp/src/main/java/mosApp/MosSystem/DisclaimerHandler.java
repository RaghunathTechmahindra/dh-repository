package mosApp.MosSystem;

/**
 *  Disclaimer Handler 
 */

import java.util.Hashtable;


import com.basis100.resources.PropertiesCache;
import com.filogix.util.Xc;

/**
 * <p>
 * Title: DisclaimerHandler.java
 * </p>
 * <p>
 * Description: Handler Class for Quick Link screen with Jato Framework
 * </p>
 * 
 * @author
 * @version 1.0 (Initial Version � May 25, 2011)
 */
public class DisclaimerHandler extends PageHandlerCommon
  implements Cloneable, Xc
{

  /**
   * <p>
   * cloneSS
   * </p>
   */
  public DisclaimerHandler cloneSS()
  {
    return(DisclaimerHandler) super.cloneSafeShallow();

  }
  
  /**
   * <p>
   * populatePageDisplayFields
   * </p>
   * *
   * <p>
   * Description: populatePageDisplayFields method for Jato Framework
   * </p>
   * 
   */
  public void populatePageDisplayFields()
  {
    SessionStateModelImpl theSessionState = getTheSessionState();

    PageEntry pg = getTheSessionState().getCurrentPage();

   // Hashtable pst = pg.getPageStateTable();

    // set the disclaimer duration 
    PropertiesCache cache = PropertiesCache.getInstance();
	
    getCurrNDPage().setDisplayFieldValue("hdDisclaimerDuration", 
    		cache.getInstanceProperty("com.filogix.ingestion.quicklinks.disclaimer.duration", "3000")); 

  }


  /**
   * <p>
   * setupBeforePageGeneration
   * </p>
   * *
   * <p>
   * Description: setupBeforePageGeneration method for Jato Framework
   * </p>
   * 
   */
  public void setupBeforePageGeneration()
  {

    PageEntry pg = getTheSessionState().getCurrentPage();

    if (pg.getSetupBeforeGenerationCalled() == true) return;

    pg.setSetupBeforeGenerationCalled(true);

  }
}

