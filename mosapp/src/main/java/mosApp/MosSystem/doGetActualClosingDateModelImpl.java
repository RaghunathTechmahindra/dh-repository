package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doGetActualClosingDateModelImpl extends QueryModelBase
	implements doGetActualClosingDateModel
{
	/**
	 *
	 *
	 */
	public doGetActualClosingDateModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfActualClosingDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFACTUALCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfActualClosingDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFACTUALCLOSINGDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfServicingMortgNum()
	{
		return (String)getValue(FIELD_DFSERVICINGMORTGNUM);
	}


	/**
	 *
	 *
	 */
	public void setDfServicingMortgNum(String value)
	{
		setValue(FIELD_DFSERVICINGMORTGNUM,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBankABANumber()
	{
		return (String)getValue(FIELD_DFBANKABANUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfBankABANumber(String value)
	{
		setValue(FIELD_DFBANKABANUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBankAccountNumber()
	{
		return (String)getValue(FIELD_DFBANKACCOUNTNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfBankAccountNumber(String value)
	{
		setValue(FIELD_DFBANKACCOUNTNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBankName()
	{
		return (String)getValue(FIELD_DFBANKNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBankName(String value)
	{
		setValue(FIELD_DFBANKNAME,value);
	}

  //--> Ticket#127 : Change request for BMO
  //--> Add check box to indicate if the Broker Commission paid or not
  //--> By Billy 25Nov2003
  public String getDfBrokerCommPaid()
	{
		return (String)getValue(FIELD_DFBROKERCOMMPAID);
	}

	public void setDfBrokerCommPaid(String value)
	{
		setValue(FIELD_DFBROKERCOMMPAID,value);
	}
  //===================================================================

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--> Ticket#127 : Change request for BMO
  //--> Add check box to indicate if the Broker Commission paid or not
  //--> By Billy 25Nov2003
	public static final String SELECT_SQL_TEMPLATE=
  "SELECT ALL DEAL.DEALID, DEAL.COPYID, DEAL.ACTUALCLOSINGDATE, DEAL.SERVICINGMORTGAGENUMBER, " +
  "DEAL.BANKABANUMBER, DEAL.BANKACCOUNTNUMBER, DEAL.BANKNAME, DEAL.BROKERCOMMPAID " +
  "FROM DEAL " +
  " __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFACTUALCLOSINGDATE="DEAL.ACTUALCLOSINGDATE";
	public static final String COLUMN_DFACTUALCLOSINGDATE="ACTUALCLOSINGDATE";
	public static final String QUALIFIED_COLUMN_DFSERVICINGMORTGNUM="DEAL.SERVICINGMORTGAGENUMBER";
	public static final String COLUMN_DFSERVICINGMORTGNUM="SERVICINGMORTGAGENUMBER";
	public static final String QUALIFIED_COLUMN_DFBANKABANUMBER="DEAL.BANKABANUMBER";
	public static final String COLUMN_DFBANKABANUMBER="BANKABANUMBER";
	public static final String QUALIFIED_COLUMN_DFBANKACCOUNTNUMBER="DEAL.BANKACCOUNTNUMBER";
	public static final String COLUMN_DFBANKACCOUNTNUMBER="BANKACCOUNTNUMBER";
	public static final String QUALIFIED_COLUMN_DFBANKNAME="DEAL.BANKNAME";
	public static final String COLUMN_DFBANKNAME="BANKNAME";
  public static final String QUALIFIED_COLUMN_DFBROKERCOMMPAID="DEAL.BROKERCOMMPAID";
	public static final String COLUMN_DFBROKERCOMMPAID="BROKERCOMMPAID";
  //====================================================================


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTUALCLOSINGDATE,
				COLUMN_DFACTUALCLOSINGDATE,
				QUALIFIED_COLUMN_DFACTUALCLOSINGDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSERVICINGMORTGNUM,
				COLUMN_DFSERVICINGMORTGNUM,
				QUALIFIED_COLUMN_DFSERVICINGMORTGNUM,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBANKABANUMBER,
				COLUMN_DFBANKABANUMBER,
				QUALIFIED_COLUMN_DFBANKABANUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBANKACCOUNTNUMBER,
				COLUMN_DFBANKACCOUNTNUMBER,
				QUALIFIED_COLUMN_DFBANKACCOUNTNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBANKNAME,
				COLUMN_DFBANKNAME,
				QUALIFIED_COLUMN_DFBANKNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--> Ticket#127 : Change request for BMO
    //--> Add check box to indicate if the Broker Commission paid or not
    //--> By Billy 25Nov2003
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBROKERCOMMPAID,
				COLUMN_DFBROKERCOMMPAID,
				QUALIFIED_COLUMN_DFBROKERCOMMPAID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //===================================================================
	}

}

