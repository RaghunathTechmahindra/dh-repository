package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doApplicantEntryAddressSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerAddressId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerAddressId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAddrId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddrId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAddrCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddrCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfCity(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfProvinceId();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvinceId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAddressStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfMonthsAtAddress();

	
	/**
	 * 
	 * 
	 */
	public void setDfMonthsAtAddress(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfResidentialStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfResidentialStatusId(java.math.BigDecimal value);
	
	
	/**
	 * setDfStreetNumber
	 * @param String <br>
	 * 	streetNumber to be set
	 */
	public void setDfStreetNumber(String value);
	
	/**
	 * getDfStreetNumber
	 * @return String <br>
	 * 	streetNumber
	 */
	public String getDfStreetNumber();

	/**
	 * setDfStreetName
	 * @param String <br>
	 * 	value to be set
	 */
	public void setDfStreetName(String value);

	
	/**
	 * getDfStreetName
	 * @return String <br>
	 * 	streetName
	 */
	public String getDfStreetName();

	/**
	 * getDfStreetTypeId
	 * @return int <br>
	 * 	streetTypeId
	 */
	public java.math.BigDecimal getDfStreetTypeId();

	
	/**
	 * setDfStreetTypeId
	 * @return int <br>
	 * 	streetTypeId
	 */
	public void setDfStreetTypeId(java.math.BigDecimal value);

	/**
	 * getDfDirectionId
	 * @return int <br>
	 * 	StreetDirectionId
	 */
	public java.math.BigDecimal getDfStreetDirectionId();

	
	/**
	 * setDfStreetDirectionId
	 * @return int <br>
	 * 	streetDirectionId
	 */
	public void setDfStreetDirectionId(java.math.BigDecimal value);

	/**
	 * setDfUnitNumber
	 * @return String <br>
	 * 	unitNumber
	 */
	public void setDfUnitNumber(String value);

	/**
	 * getDfUnitNumber
	 * @return String <br>
	 * 	unitNumber
	 */
	public String getDfUnitNumber();
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFBORROWERADDRESSID="dfBorrowerAddressId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFADDRID="dfAddrId";
	public static final String FIELD_DFADDRCOPYID="dfAddrCopyId";
	public static final String FIELD_DFADDRESSLINE1="dfAddressLine1";
	public static final String FIELD_DFCITY="dfCity";
	public static final String FIELD_DFPROVINCEID="dfProvinceId";
	public static final String FIELD_DFPOSTALFSA="dfPostalFSA";
	public static final String FIELD_DFPOSTALLDU="dfPostalLDU";
	public static final String FIELD_DFADDRESSLINE2="dfAddressLine2";
	public static final String FIELD_DFADDRESSSTATUSID="dfAddressStatusId";
	public static final String FIELD_DFMONTHSATADDRESS="dfMonthsAtAddress";
	public static final String FIELD_DFRESIDENTIALSTATUSID="dfResidentialStatusId";
	
	public static final String FIELD_DFSTREETNUMBER="dfStreetNumber";
	public static final String FIELD_DFSTREETNAME="dfStreetName";
	public static final String FIELD_DFSTREETTYPEID="dfStreetTypeId";
	public static final String FIELD_DFDIRECTIONTYPEID="dfStreetDirectionId";
	public static final String FIELD_DFUNITNUMBER="dfUnitNumber";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

