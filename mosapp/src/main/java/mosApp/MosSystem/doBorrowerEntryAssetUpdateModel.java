package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.UpdateQueryModel;


/**
 *
 *
 *
 */
public interface doBorrowerEntryAssetUpdateModel extends QueryModel, UpdateQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssetId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAssetDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssetValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludingGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludingGDS(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludingTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludingTDS(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFASSETID="dfAssetId";
	public static final String FIELD_DFASSETDESCRIPTION="dfAssetDescription";
	public static final String FIELD_DFASSETVALUE="dfAssetValue";
	public static final String FIELD_DFINCLUDINGGDS="dfIncludingGDS";
	public static final String FIELD_DFINCLUDINGTDS="dfIncludingTDS";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

