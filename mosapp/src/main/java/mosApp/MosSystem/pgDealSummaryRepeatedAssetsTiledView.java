package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedAssetsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedAssetsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewAssetsModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStADBAssetType().setValue(CHILD_STADBASSETTYPE_RESET_VALUE);
		getStADBDescription().setValue(CHILD_STADBDESCRIPTION_RESET_VALUE);
		getStADBValue().setValue(CHILD_STADBVALUE_RESET_VALUE);
		getStADBPercentageIncInNetworthASDtls().setValue(CHILD_STADBPERCENTAGEINCINNETWORTHASDTLS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STADBASSETTYPE,StaticTextField.class);
		registerChild(CHILD_STADBDESCRIPTION,StaticTextField.class);
		registerChild(CHILD_STADBVALUE,StaticTextField.class);
		registerChild(CHILD_STADBPERCENTAGEINCINNETWORTHASDTLS,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewAssetsModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedAssets_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedAssets(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STADBASSETTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewAssetsModel(),
				CHILD_STADBASSETTYPE,
				doDetailViewAssetsModel.FIELD_DFASSETTYPE,
				CHILD_STADBASSETTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewAssetsModel(),
				CHILD_STADBDESCRIPTION,
				doDetailViewAssetsModel.FIELD_DFASSETDESC,
				CHILD_STADBDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBVALUE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewAssetsModel(),
				CHILD_STADBVALUE,
				doDetailViewAssetsModel.FIELD_DFASSETVALUE,
				CHILD_STADBVALUE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBPERCENTAGEINCINNETWORTHASDTLS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewAssetsModel(),
				CHILD_STADBPERCENTAGEINCINNETWORTHASDTLS,
				doDetailViewAssetsModel.FIELD_DFPERCENTINCLUDEDINNETWORTH,
				CHILD_STADBPERCENTAGEINCINNETWORTHASDTLS_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBAssetType()
	{
		return (StaticTextField)getChild(CHILD_STADBASSETTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBDescription()
	{
		return (StaticTextField)getChild(CHILD_STADBDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBValue()
	{
		return (StaticTextField)getChild(CHILD_STADBVALUE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBPercentageIncInNetworthASDtls()
	{
		return (StaticTextField)getChild(CHILD_STADBPERCENTAGEINCINNETWORTHASDTLS);
	}


	/**
	 *
	 *
	 */
	public doDetailViewAssetsModel getdoDetailViewAssetsModel()
	{
		if (doDetailViewAssets == null)
			doDetailViewAssets = (doDetailViewAssetsModel) getModel(doDetailViewAssetsModel.class);
		return doDetailViewAssets;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewAssetsModel(doDetailViewAssetsModel model)
	{
			doDetailViewAssets = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STADBASSETTYPE="stADBAssetType";
	public static final String CHILD_STADBASSETTYPE_RESET_VALUE="";
	public static final String CHILD_STADBDESCRIPTION="stADBDescription";
	public static final String CHILD_STADBDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_STADBVALUE="stADBValue";
	public static final String CHILD_STADBVALUE_RESET_VALUE="";
	public static final String CHILD_STADBPERCENTAGEINCINNETWORTHASDTLS="stADBPercentageIncInNetworthASDtls";
	public static final String CHILD_STADBPERCENTAGEINCINNETWORTHASDTLS_RESET_VALUE="";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewAssetsModel doDetailViewAssets=null;
  private DealSummaryHandler handler=new DealSummaryHandler();
}

