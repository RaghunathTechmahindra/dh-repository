package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doPageNameLabelModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPageID();

	
	/**
	 * 
	 * 
	 */
	public void setDfPageID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPageName();

	
	/**
	 * 
	 * 
	 */
	public void setDfPageName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPageLabel();

	
	/**
	 * 
	 * 
	 */
	public void setDfPageLabel(String value);
	
	  public java.math.BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(java.math.BigDecimal id);
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPAGEID="dfPageID";
	public static final String FIELD_DFPAGENAME="dfPageName";
	public static final String FIELD_DFPAGELABEL="dfPageLabel";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

