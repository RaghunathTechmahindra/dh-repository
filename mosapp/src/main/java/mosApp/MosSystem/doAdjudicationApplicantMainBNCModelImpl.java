package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import MosSystem.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 * <p>Title: doAdjudicationApplicantMainBNCModelImpl</p>
 * 
 * <p>Description: Implementation class for doAdjudicationApplicantMainBNCModel</p>
 * 
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.1 <br>
 * Date: 10/18/2006 <br>
 * Author: GCD Implementation Team <br>
 * Change:<br>
 * 	Added sort by borrowerId asc to the end of the SQL select statement, defect 1362
 */
public class doAdjudicationApplicantMainBNCModelImpl extends QueryModelBase
	implements doAdjudicationApplicantMainBNCModel
{
	/**
	 *
	 *
	 */
	public doAdjudicationApplicantMainBNCModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

//   logger = SysLog.getSysLogger("AdjudicationApplicantMainBNCModel");
//   logger.debug("AdjudicationApplicantMainBNCModel@afterExecute::Size: " + sql);

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
//	    logger = SysLog.getSysLogger("AdjudicationApplicantMainBNC");
//	    logger.debug("AdjudicationApplicantMainBNC@afterExecute::Size: " + this.getSize());
//	    logger.debug("AdjudicationApplicantMainBNC@afterExecute::SQLTemplate: " + this.getSelectSQL());
	
	    //--Release2.1--//
	    //To override all the Description fields by populating from BXResource
	    //Get the Language ID from the SessionStateModel
	    String defaultInstanceStateName =
				getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	    SessionStateModelImpl theSessionState =
	        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
	                        SessionStateModel.class,
	                        defaultInstanceStateName,
	                        true);
	    int languageId = theSessionState.getLanguageId();
		int institutionId = theSessionState.getDealInstitutionId();

	    while(this.next())
	    {
	      //  BorrowerType
	      this.setDfBorrowerType(BXResources.getPickListDescription(
	        institutionId, "BORROWERTYPE", this.getDfBorrowerType(), languageId));
	      
	    }

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

	/**	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERTYPEID,value);
	}

	/**
	 *
	 *
	 */
	public String getDfBorrowerType()
	{
		return (String)getValue(FIELD_DFBORROWERTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerType(String value)
	{
		setValue(FIELD_DFBORROWERTYPE,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERGDS,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERTDS,value);
	}

	/**
	 *
	 *
	 */
	public String getDfPrimaryBorrowerFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYBORROWERFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value)
	{
		setValue(FIELD_DFPRIMARYBORROWERFLAG,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAnnualIncomeAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFANNUALINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfAnnualIncomeAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFANNUALINCOMEAMOUNT,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfOtherAnnualIncome()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFOTHERANNUALINCOME);
	}


	/**
	 *
	 *
	 */
	public void setDfOtherAnnualIncome(java.math.BigDecimal value)
	{
		setValue(FIELD_DFOTHERANNUALINCOME,value);
	}	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalAnnualIncome()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALANNUALINCOME);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalAnnualIncome(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALANNUALINCOME,value);
	}


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE=
	  "SELECT DISTINCT D.DEALID, D.COPYID, SUM(I0.ANNUALINCOMEAMOUNT) TOTALANNUALINCOME, SUM( I.ANNUALINCOMEAMOUNT ) ANNUALINCOMEAMOUNT, " +
	  "SUM( I2.ANNUALINCOMEAMOUNT ) OTHERANNUALINCOME , B.BORROWERID , B.BORROWERTYPEID, " + 
	  "to_char(B.BORROWERTYPEID) BORROWERTYPEID_STR, B.GDS, B.TDS, B.PRIMARYBORROWERFLAG " + 
	  "FROM DEAL D , BORROWER B " +
	  "LEFT OUTER JOIN INCOME I0 ON B.BORROWERID = I0.BORROWERID AND B.COPYID = I0.COPYID " + 
	  "LEFT OUTER JOIN INCOME I2 ON I0.INCOMEID  = I2.INCOMEID AND I0.COPYID = I2.COPYID AND I2.INCOMETYPEID IN ( " +
	  Mc.INCOME_INTEREST_INCOME + "," + Mc.INCOME_CHILD_SUPPORT + "," + Mc.INCOME_PENSION + "," + Mc.INCOME_ALIMONY + "," + Mc.INCOME_OTHER + "," + Mc.INCOME_RENTAL_ADD + "," + Mc.INCOME_RENTAL_SUBTRACT + " ) " +
	  "LEFT OUTER JOIN INCOME I ON I0.INCOMEID = I.INCOMEID AND I0.COPYID = I.COPYID AND I.INCOMETYPEID IN ( " + 
	  Mc.INCOME_SALARY + "," + Mc.INCOME_HOURLY + "," + Mc.INCOME_HOURLY_PLUS_COMMISSION + "," + Mc.INCOME_COMMISSION + "," + Mc.INCOME_SELF_EMP + "," + Mc.INCOME_OTHER_EMPLOYMENT_INCOME + " ) " +
	  "__WHERE__ GROUP BY B.BORROWERID,B.BORROWERTYPEID, B.GDS, B.TDS, D.DEALID, D.COPYID, B.PRIMARYBORROWERFLAG " +
	  "ORDER BY B.PRIMARYBORROWERFLAG DESC, B.BORROWERID";


  public static final String MODIFYING_QUERY_TABLE_NAME="DEAL D , BORROWER B ";
  
  public static final String STATIC_WHERE_CRITERIA="D.DEALID = B.DEALID AND D.COPYID = B.COPYID";
  
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFCOPYID="D.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDEALID="D.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFBORROWERID="B.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFBORROWERTYPEID="B.BORROWERTYPEID";
	public static final String COLUMN_DFBORROWERTYPEID="BORROWERTYPEID";
	public static final String QUALIFIED_COLUMN_DFBORROWERTYPE="B.BORROWERTYPEID_STR";
	public static final String COLUMN_DFBORROWERTYPE="BORROWERTYPEID_STR";
	public static final String QUALIFIED_COLUMN_DFBORROWERGDS="B.GDS";
	public static final String COLUMN_DFBORROWERGDS="GDS";
	public static final String QUALIFIED_COLUMN_DFBORROWERTDS="B.TDS";
	public static final String COLUMN_DFBORROWERTDS="TDS";
	public static final String QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG="B.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFPRIMARYBORROWERFLAG="PRIMARYBORROWERFLAG";
	public static final String QUALIFIED_COLUMN_DFANNUALINCOMEAMOUNT="I.ANNUALINCOMEAMOUNT";
	public static final String COLUMN_DFANNUALINCOMEAMOUNT="ANNUALINCOMEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFOTHERANNUALINCOME="I2.OTHERANNUALINCOME";
	public static final String COLUMN_DFOTHERANNUALINCOME="OTHERANNUALINCOME";
	public static final String QUALIFIED_COLUMN_DFTOTALANNUALINCOME="I0.TOTALANNUALINCOME";
	public static final String COLUMN_DFTOTALANNUALINCOME="TOTALANNUALINCOME";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFBORROWERTYPEID,
				COLUMN_DFBORROWERTYPEID,
				QUALIFIED_COLUMN_DFBORROWERTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFBORROWERTYPE,
				COLUMN_DFBORROWERTYPE,
				QUALIFIED_COLUMN_DFBORROWERTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFBORROWERGDS,
				COLUMN_DFBORROWERGDS,
				QUALIFIED_COLUMN_DFBORROWERGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFBORROWERTDS,
				COLUMN_DFBORROWERTDS,
				QUALIFIED_COLUMN_DFBORROWERTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFPRIMARYBORROWERFLAG,
				COLUMN_DFPRIMARYBORROWERFLAG,
				QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));			
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFANNUALINCOMEAMOUNT,
				COLUMN_DFANNUALINCOMEAMOUNT,
				QUALIFIED_COLUMN_DFANNUALINCOMEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFOTHERANNUALINCOME,
				COLUMN_DFOTHERANNUALINCOME,
				QUALIFIED_COLUMN_DFOTHERANNUALINCOME,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFTOTALANNUALINCOME,
				COLUMN_DFTOTALANNUALINCOME,
				QUALIFIED_COLUMN_DFTOTALANNUALINCOME,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
  
	}

}

