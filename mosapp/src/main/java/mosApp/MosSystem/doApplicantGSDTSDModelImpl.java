package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doApplicantGSDTSDModelImpl extends QueryModelBase
	implements doApplicantGSDTSDModel
{
	/**
	 *
	 *
	 */
	public doApplicantGSDTSDModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDSBorrower()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDGDSBORROWER);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedGDSBorrower(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDGDSBORROWER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDSBorrower()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDTDSBORROWER);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedTDSBorrower(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDTDSBORROWER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDS3Year()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDGDS3YEAR);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedGDS3Year(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDGDS3YEAR,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDS3Year()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDTDS3YEAR);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedTDS3Year(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDTDS3YEAR,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	
	//FXP23719 - MCM Nov 28, 2008 - start
	/*
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT DEAL.DEALID, DEAL.COPYID, " +
	"DEAL.COMBINEDTDS, DEAL.COMBINEDGDS, DEAL.COMBINEDGDSBORROWER, " +
	"DEAL.COMBINEDTDSBORROWER, DEAL.COMBINEDGDS3YEAR, " +
	"DEAL.COMBINEDTDS3YEAR FROM DEAL, BORROWER  __WHERE__  ";
	*/
	public static final String SELECT_SQL_TEMPLATE="SELECT DEAL.DEALID, DEAL.COPYID, " +
	"DEAL.COMBINEDTDS, DEAL.COMBINEDGDS, DEAL.COMBINEDGDSBORROWER, " +
	"DEAL.COMBINEDTDSBORROWER, DEAL.COMBINEDGDS3YEAR, " +
	"DEAL.COMBINEDTDS3YEAR FROM DEAL  __WHERE__  ";

	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL";
    //public static final String STATIC_WHERE_CRITERIA="DEAL.INSTITUTIONPROFILEID = " +
    //"BORROWER.INSTITUTIONPROFILEID";
    public static final String STATIC_WHERE_CRITERIA="";
	//FXP23719 - MCM Nov 28, 2008 - end
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDTDS="DEAL.COMBINEDTDS";
	public static final String COLUMN_DFCOMBINEDTDS="COMBINEDTDS";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDGDS="DEAL.COMBINEDGDS";
	public static final String COLUMN_DFCOMBINEDGDS="COMBINEDGDS";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDGDSBORROWER="DEAL.COMBINEDGDSBORROWER";
	public static final String COLUMN_DFCOMBINEDGDSBORROWER="COMBINEDGDSBORROWER";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDTDSBORROWER="DEAL.COMBINEDTDSBORROWER";
	public static final String COLUMN_DFCOMBINEDTDSBORROWER="COMBINEDTDSBORROWER";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDGDS3YEAR="DEAL.COMBINEDGDS3YEAR";
	public static final String COLUMN_DFCOMBINEDGDS3YEAR="COMBINEDGDS3YEAR";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDTDS3YEAR="DEAL.COMBINEDTDS3YEAR";
	public static final String COLUMN_DFCOMBINEDTDS3YEAR="COMBINEDTDS3YEAR";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDTDS,
				COLUMN_DFCOMBINEDTDS,
				QUALIFIED_COLUMN_DFCOMBINEDTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDGDS,
				COLUMN_DFCOMBINEDGDS,
				QUALIFIED_COLUMN_DFCOMBINEDGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDGDSBORROWER,
				COLUMN_DFCOMBINEDGDSBORROWER,
				QUALIFIED_COLUMN_DFCOMBINEDGDSBORROWER,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDTDSBORROWER,
				COLUMN_DFCOMBINEDTDSBORROWER,
				QUALIFIED_COLUMN_DFCOMBINEDTDSBORROWER,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDGDS3YEAR,
				COLUMN_DFCOMBINEDGDS3YEAR,
				QUALIFIED_COLUMN_DFCOMBINEDGDS3YEAR,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDTDS3YEAR,
				COLUMN_DFCOMBINEDTDS3YEAR,
				QUALIFIED_COLUMN_DFCOMBINEDTDS3YEAR,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

