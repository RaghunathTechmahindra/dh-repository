package mosApp.MosSystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.GroupProfile;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.picklist.BXResources;
import com.filogix.express.legacy.mosapp.ISourceBusinessReviewHandler;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ViewBean;

// Special usage of Conditions:
	//
	// 1) pageCondition3
	//
	//    Value - true:  Run trigger workflow when submit
	//
	//          - false: Default - not calling workflow

public class SourceBusinessReviewHandler extends PageHandlerCommon
	implements Cloneable, Sc, ISourceBusinessReviewHandler
{
	// Key name to pass the SOB Id

	public static final String SFIRM_ID_PASSED_PARM="sourceFirmIdPassed";
	// following used as key in page state table to indicate if user has edit permission

	public static final String USER_CAN_EDIT="USER_CAN_EDIT";


	/**
	 *
	 *
	 */
	public SourceBusinessReviewHandler cloneSS()
	{
		return(SourceBusinessReviewHandler) super.cloneSafeShallow();

	}


	/////////////////////////////////////////////////////////////////////////

	public void preHandlerProtocol(ViewBean currNDPage)
	{
		super.preHandlerProtocol(currNDPage);


		// sefault page load method to display()
		PageEntry pg = theSessionState.getCurrentPage();

		pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);

	}


	/////////////////////////////////////////////////////////////////////////
	// This method is used to populate the fields in the header
	// with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		ViewBean theNDPage = getCurrNDPage();


		//////////// manually populate display fields /////////////////
		////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doSourceOfBusinessModel.class);

     doSourceOfBusinessModel theDO = (doSourceOfBusinessModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doSourceOfBusinessModel.class);

		//// By default for non-repeatable fields should be passed rowNum value equals 0.
    //// See detailed explanation on the issue in the PHC and DEH classes.
    // Set PhoneNum with extension
		setPhoneNumTextDisplayField(theDO, "stWorkPhoneNumber", theDO.FIELD_DFSBPPHONE, theDO.FIELD_DFSBPPHONEEXT, 0);

		// Set Fax # without extension
		setPhoneNumTextDisplayField(theDO, "stFaxNumber", theDO.FIELD_DFSBPFAX, "", 0);

		// Set Length of Service
		setTermsDisplayField(theDO, "stLengthOfSerYrs", "stLengthOfSerMths", theDO.FIELD_DFSBPSERVLEN, 0);

		// Set SourceFirm Name
    ////1. Get the dfSBPFirmId field value from theDO model first.
    String sSBFirmId = "";

    Object oSBFirmId = theDO.getValue(theDO.FIELD_DFSBPFIRMID);
    if (oSBFirmId == null)
    {
      sSBFirmId = "";
    }
    else if((oSBFirmId != null))
    {
      sSBFirmId = oSBFirmId.toString();
      //--> Bug Fix by Billy 08April2003
      //logger.debug("SBRH@populatePageDisplayFields::FirmIdFromModel: " + sSBFirmId);
    }

    //--> Bug Fix by Billy 08April2003
    Integer sbFirmId;
    try{
      sbFirmId = new Integer(sSBFirmId);
    }
    catch(Exception e)
    {
      //--> Force to invalid id
      sbFirmId = new Integer(-1);
    }
    //==================================

    //// 2. Execute theFirmDO model with pre-defined criteria.
		////CSpCriteriaSQLObject theFirmDO =(CSpCriteriaSQLObject) getModel(doSourceFirmModel.class);
    doSourceFirmModel theFirmDO = (doSourceFirmModel)
        RequestManager.getRequestContext().getModelManager().getModel(doSourceFirmModel.class);

		theFirmDO.clearUserWhereCriteria();

		////theFirmDO.addUserWhereCriterion("dfSBPFirmProfileId", "=", theDO.getValue(0, "dfSBPFirmId"));
		theFirmDO.addUserWhereCriterion("dfSBPFirmProfileId", "=", sbFirmId);

    String sbFirmName = "";

		////theFirmDO.execute();

   try
   {
      ResultSet rs = theFirmDO.executeSelect(null);

      ////((QueryModelBase)theModel).setResultSet(rs);
		  if (rs != null && theFirmDO.getSize() > 0)
      {
        Object oFirmName = theFirmDO.getValue(theFirmDO.FIELD_DFSBPFIRMNAME);
        if (oFirmName == null)
        {
          sbFirmName = "";
        }
        else if((oFirmName != null))
        {
          sbFirmName = oFirmName.toString();
          //logger.debug("SBRH@populatePageDisplayFields::FirmNameFromModel: " + oFirmName.toString());
        }

        ////theNDPage.setDisplayFieldValue("stSourceFirmName", theFirmDO.getValue(0, "dfSBPFirmName"));
        theNDPage.setDisplayFieldValue("stSourceFirmName", sbFirmName);
      }
      else
      {
          //logger.debug("SBRH@populatePageDisplayFields::FirmName none mode");
          String none = new String("--None--");
          theNDPage.setDisplayFieldValue("stSourceFirmName", none);
      }

    }
    catch (ModelControlException mce)
    {
      logger.warning("SBRH@populatePageDisplayFields::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("SBRH@populatePageDisplayFields::SQLException: " + sqle);
    }

		// Set Associated User Name
    ////1. Get the dfSBPFirmId field value from theDO model first.
    String sSBPUserId = "";

    Object oSBPUserId  = theDO.getValue(theDO.FIELD_DFSBPUSERID);
    if (oSBPUserId  == null)
    {
      sSBPUserId = "";
    }
    else if((oSBPUserId  != null))
    {
      sSBPUserId  = oSBPUserId .toString();
      //logger.debug("SBRH@populatePageDisplayFields::SBPUserIdFromModel: " + oSBPUserId .toString());
    }

    if (sSBPUserId != null && !sSBPUserId.equals(""))
    {
      Integer sBPUserId = new Integer(sSBPUserId);

      //// 2. Execute theFirmDO model with pre-defined criteria.
      ////CSpCriteriaSQLObject theUserDO =(CSpCriteriaSQLObject) getModel(doSourceUserModel.class);
      doSourceUserModel theUserDO = (doSourceUserModel)
                                              RequestManager.getRequestContext().getModelManager().getModel(doSourceUserModel.class);

      theUserDO.clearUserWhereCriteria();

      ////theUserDO.addUserWhereCriterion("dfUserProfileId", "=", theDO.getValue(0, "dfSBPUserId"));
      theUserDO.addUserWhereCriterion("dfUserProfileId", "=", sBPUserId);

      //logger.debug("SBRH@populatePageDisplayFields::UserIdBeforeExecute: " + sBPUserId);

      ////theUserDO.execute();
       String userName = "";

       try
       {
          ResultSet rsUser = theUserDO.executeSelect(null);
          //logger.debug("SBRH@populatePageDisplayFields::ResultSet: " + rsUser);

          ////((QueryModelBase)theModel).setResultSet(rs);
          if (rsUser != null && theUserDO.getSize() > 0)
          {
            Object oUserName = theUserDO.getValue(theUserDO.FIELD_DFUSERNAME);
            if (oUserName == null)
            {
              userName = "";
            }
            else if((oUserName != null))
            {
              userName = oUserName.toString();
              //logger.debug("SBRH@populatePageDisplayFields::UserNameFromModel: " + userName.toString());
            }

            theNDPage.setDisplayFieldValue("stSourceUserName", userName);
          }
          else
          {
              //logger.debug("SBRH@populatePageDisplayFields::UserName none mode");
              String none = new String("--None--");
              theNDPage.setDisplayFieldValue("stSourceUserName", none);
          }
        }
        catch (ModelControlException mce)
        {
          logger.warning("SBRH@populatePageDisplayFields::MCEException: " + mce);
        }

        catch (SQLException sqle)
        {
          logger.warning("SBRH@populatePageDisplayFields::SQLException: " + sqle);
        }
    }

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Added new fields to handle Source to Group association
    //--> By Billy 19Aug2003
    // Set Associated Group Name
    String sSBPGroupId = "";

    Object oSBPGroupId  = theDO.getValue(theDO.FIELD_DFSBPGROUPID);
    if (oSBPGroupId  == null)
    {
      sSBPGroupId = "";
    }
    else
    {
      sSBPGroupId  = oSBPGroupId.toString();
      //logger.debug("SBRH@populatePageDisplayFields::SBPGroupId From Model: " + oSBPGroupId.toString());
    }

    if(sSBPGroupId != null && !sSBPGroupId.equals(""))
    {
      try
      {
        int sBPGroupId = Integer.parseInt(sSBPGroupId);

        GroupProfile theGp = new GroupProfile(srk, sBPGroupId);
        theNDPage.setDisplayFieldValue("stSourceGroupName", theGp.getGroupName());
      }
      catch(Exception eGp)
      {
        //logger.debug("SBRH@populatePageDisplayFields::Exception when setting up Group Name: " + eGp);
      }
    }
    //==================================================
		///////////// End manual population of DisplayFields ////////////////
		populatePageShellDisplayFields();

		populatePageDealSummarySnapShot();

		displayDSSConditional(pg, getCurrNDPage());

	}

	private void displayDSSConditional(PageEntry pg, ViewBean thePage)
	{
		String suppressStart = "<!--";

		String suppressEnd = "//-->";

		if (pg == null || pg.getPageDealId() <= 0)
		{
			// suppress
			thePage.setDisplayFieldValue("stIncludeDSSstart", new String(suppressStart));
			thePage.setDisplayFieldValue("stIncludeDSSend", new String(suppressEnd));
			return;
		}


		// display
		thePage.setDisplayFieldValue("stIncludeDSSstart", new String(""));

		thePage.setDisplayFieldValue("stIncludeDSSend", new String(""));

		return;

	}


	/////////////////////////////////////////////////////////////////////////

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		// get SOB profile id
		String pId =(String) pst.get(SOB_ID_PASSED_PARM);

    //logger.debug("SBRH@setupBeforePageGeneration::ProfileId: " + pId);

		// determine if user can modify existing party information
		if (getUserAccessType(Mc.PGNM_SOB_ADD) == Sc.PAGE_ACCESS_EDIT) pst.put(USER_CAN_EDIT, USER_CAN_EDIT);

		///CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doSourceOfBusinessModel.class);
    doSourceOfBusinessModelImpl theDO = (doSourceOfBusinessModelImpl)
                                            RequestManager.getRequestContext().getModelManager().getModel(doSourceOfBusinessModel.class);


		theDO.clearUserWhereCriteria();

		if (pId != null)
        theDO.addUserWhereCriterion("dfSBProfileId", "=", new String(pId));

		setupDealSummarySnapShotDO(pg);

	}


	/**
	 *
	 *
	 */
	public void handleAddSourceBusiness()
	{
		try
		{
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SOB_ADD, true);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceBusinessReviewHandler.handleAddSourceBusiness()");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	public void handleDetailSourceFirm()
	{
		try
		{
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			ViewBean thisNDPage = this.getCurrNDPage();
			String theSorceFirmId = thisNDPage.getDisplayFieldValue("hdSourceFirmId").toString();
			if (theSorceFirmId == null || theSorceFirmId.trim().equals("") || theSorceFirmId.equals("0"))
			{
				setActiveMessageToAlert(BXResources.getSysMsg("SOURCE_OF_BUSINESS_SOURCE_FIRM_NOT_SET", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			// Goto review SourceFirm page
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SFIRM_REVIEW, true);
			Hashtable subPst = pgEntry.getPageStateTable();
			subPst.put(SFIRM_ID_PASSED_PARM, theSorceFirmId);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Error @SourceOfBusinessSearchHandler.handleSelectSourceBusiness: ");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	////public int handleViewModifyButton()
	public boolean handleViewModifyButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			if (pst.get(USER_CAN_EDIT) != null)
          return true;
		}
		catch(Exception e)
		{
			;
		}

		return false;
	}


	/**
	 *
	 *
	 */
	public void handleModifySOB()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageEntry editSOBPage = setupSubPagePageEntry(pg, Mc.PGNM_SOB_ADD, true);

		Hashtable subPst = editSOBPage.getPageStateTable();

		String SOBProfileId = getCurrNDPage().getDisplayFieldValue("hdSOBPId").toString();

		subPst.put(SOB_ID_PASSED_PARM, SOBProfileId);

		getSavedPages().setNextPage(editSOBPage);

		navigateToNextPage(true);

	}


	/**
	 *
	 *
	 */
	public void handleOKSpecial()
	{
		PageEntry pg = theSessionState.getCurrentPage();


		// Check if PageCondition 3 set ==> call workflow
		if (pg.getPageCondition3() == true)
		{
			try
			{
				srk.beginTransaction();
				workflowTrigger(2, srk, pg, null);
				srk.commitTransaction();
			}
			catch(Exception e)
			{
				srk.cleanTransaction();
				logger.error("Error @SourceOfBusinessSearchHandler.handleOKSpecial: ");
				logger.error(e);
			}
		}

		this.handleCancelStandard();

	}

}

