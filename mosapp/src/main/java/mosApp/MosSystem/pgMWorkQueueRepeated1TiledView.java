package mosApp.MosSystem;

import com.basis100.log.*;

import com.basis100.resources.PropertiesCache;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import mosApp.*;

import java.io.*;

import java.lang.reflect.*;

import java.sql.*;

import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;


/**
 *
 *
 *
 */
public class pgMWorkQueueRepeated1TiledView extends RequestHandlingTiledViewBase
  implements TiledView, RequestHandler
{
  //===================================================================================
  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Child accessors
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Child rendering methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Repeated event methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_STBORROWER = "stBorrower";
  public static final String CHILD_STBORROWER_RESET_VALUE = "";
  public static final String CHILD_STASSIGNEDTO = "stAssignedTo";
  public static final String CHILD_STASSIGNEDTO_RESET_VALUE = "";
  public static final String CHILD_STLOGIN = "stLogin";
  public static final String CHILD_STLOGIN_RESET_VALUE = "";
  public static final String CHILD_HDDEALID = "hdDealId";
  public static final String CHILD_HDDEALID_RESET_VALUE = "";
  public static final String CHILD_STDEALNUMBER = "stDealNumber";
  public static final String CHILD_STDEALNUMBER_RESET_VALUE = "";
  public static final String CHILD_HDWQID = "hdWqID";
  public static final String CHILD_HDWQID_RESET_VALUE = "";
  public static final String CHILD_STTASKDESCRIPTION = "stTaskDescription";
  public static final String CHILD_STTASKDESCRIPTION_RESET_VALUE = "";
  public static final String CHILD_STTASKPRIORITY = "stTaskPriority";
  public static final String CHILD_STTASKPRIORITY_RESET_VALUE = "";
  public static final String CHILD_STTASKDUEDATE = "stTaskDueDate";
  public static final String CHILD_STTASKDUEDATE_RESET_VALUE = "";
  public static final String CHILD_STTASKSTATUS = "stTaskStatus";
  public static final String CHILD_STTASKSTATUS_RESET_VALUE = "";
  public static final String CHILD_STTASKWARNING = "stTaskWarning";
  public static final String CHILD_STTASKWARNING_RESET_VALUE = "";
  public static final String CHILD_BGCOLOR = "bgColor";
  public static final String CHILD_BGCOLOR_RESET_VALUE = "";
  public static final String CHILD_BTSNAPSHOT = "btSnapShot";
  public static final String CHILD_BTSNAPSHOT_RESET_VALUE = "Snapshot";
  public static final String CHILD_BTREASSIGN = "btReassign";
  public static final String CHILD_BTREASSIGN_RESET_VALUE = "Snapshot";

  //--Release2.1--//
  //// In order to translate the UserTypeDescription part inside the ComputedColumn
  //// the UserTypeDescription should be separated into the distinct field
  //// and translated on fly (see doMasterWorkQueueImpl detail description).
  public static final String CHILD_STUTDESCRIPTION = "stUtDescription";
  public static final String CHILD_STUTDESCRIPTION_RESET_VALUE = "";
  public static final String CHILD_HDUSERTYPEID = "hdUserTypeId";
  public static final String CHILD_HDUSERTYPEID_RESET_VALUE = "";

  //--TD_MWQ_CR--start--//
  //// Added Source Firm and Source to the Borrower info.
  public static final String CHILD_STSOURCENAME = "stBPSource";
  public static final String CHILD_STSOURCENAME_RESET_VALUE = "";
  public static final String CHILD_STSFSHORTNAME = "stBPSourceFirm";
  public static final String CHILD_STSFSHORTNAME_RESET_VALUE = "";

  //--TD_MWQ_CR--end--//
  //--> Performance enhancement : elimination of the 2nd call of the DataObject
  //--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
  //--> the hidden values on each row.
  //--> By Billy 16Jan2004
  public static final String CHILD_HDCOPYID = "hdCopyId";
  public static final String CHILD_HDCOPYID_RESET_VALUE = "";
  public static final String CHILD_HDAPPLICATIONID = "hdApplicationId";
  public static final String CHILD_HDAPPLICATIONID_RESET_VALUE = "";

  //-- ========== SCR#750 begins ============================================ --//
  //-- by Neil on Dec/20/2004
  public static final String CHILD_STDEALSTATUS = "stDealStatus";
  public static final String CHILD_STDEALSTATUS_RESET_VALUE = "";
  public static final String CHILD_STHOLDREASON = "stHoldReason";
  public static final String CHILD_STHOLDREASON_RESET_VALUE = "";

  //-- these field holds the style format for TaskStatus/HoldReason textbox.
  //-- TaskStatus and HoldReason use the same textbox according to SCR#750.
  public static final String CHILD_TASKSTATUSORHOLDREASONSTYLE = "taskStatusOrHoldReasonStyle";
  public static final String CHILD_TASKSTATUSORHOLDREASONSTYLE_RESET_VALUE = "";

  //-- ========== SCR#750 ends ============================================== --//
  
  public static final String CHILD_STINSTITUTIONID = "stInstitutionId";
  public static final String CHILD_STINSTITUTIONID_RESET_VALUE ="";
  
  public static final String CHILD_HDINSTITUTIONID = "hdInstitutionId";
  public static final String CHILD_HDINSTITUTIONID_RESET_VALUE ="";
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
  private doMasterWorkQueueModel doMasterWorkQueue = null;
  private MasterWorkQueueHandler handler = new MasterWorkQueueHandler();
  public SysLogger logger;
  
  public static final int MAX_NUMBER_OF_MASTERWORKQUEUE_PER_PAGE = 200;
  public static final int MIN_NUMBER_OF_MASTERWORKQUEUE_PER_PAGE = 10;
  
  /**
   *
   *
   */
  public pgMWorkQueueRepeated1TiledView(View parent, String name)
  {
    super(parent, name);

    //--> Performance enhancement : make the num of rows to display configurable
    //--> By Billy 22Jan2004
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    int institutionId = handler.getTheSessionState().getCurrentPage().dealPage ? handler.getTheSessionState().getDealInstitutionId():handler.getTheSessionState().getUserInstitutionId();
    int numOfRows = TypeConverter.asInt(PropertiesCache.getInstance().getProperty(institutionId, "com.basis100.masterworkqueue.numofrows", String.valueOf(MAX_NUMBER_OF_MASTERWORKQUEUE_PER_PAGE)), MAX_NUMBER_OF_MASTERWORKQUEUE_PER_PAGE);
    if (numOfRows < MIN_NUMBER_OF_MASTERWORKQUEUE_PER_PAGE) {
      numOfRows = MIN_NUMBER_OF_MASTERWORKQUEUE_PER_PAGE;
    } else if (numOfRows > MAX_NUMBER_OF_MASTERWORKQUEUE_PER_PAGE) {
      numOfRows = MAX_NUMBER_OF_MASTERWORKQUEUE_PER_PAGE;
    }

    setMaxDisplayTiles(numOfRows);

    //============================================================================
    setPrimaryModelClass(doMasterWorkQueueModel.class);
    registerChildren();
    initialize();
  }

  /**
   *
   *
   */
  protected void initialize()
  {
  }

  /**
   *
   *
   */
  public void resetChildren()
  {
    getStBorrower().setValue(CHILD_STBORROWER_RESET_VALUE);
    getStAssignedTo().setValue(CHILD_STASSIGNEDTO_RESET_VALUE);
    getStLogin().setValue(CHILD_STLOGIN_RESET_VALUE);
    getHdDealId().setValue(CHILD_HDDEALID_RESET_VALUE);
    getStDealNumber().setValue(CHILD_STDEALNUMBER_RESET_VALUE);
    getHdWqID().setValue(CHILD_HDWQID_RESET_VALUE);
    getStTaskDescription().setValue(CHILD_STTASKDESCRIPTION_RESET_VALUE);
    getStTaskPriority().setValue(CHILD_STTASKPRIORITY_RESET_VALUE);
    getStTaskDueDate().setValue(CHILD_STTASKDUEDATE_RESET_VALUE);
    getStTaskStatus().setValue(CHILD_STTASKSTATUS_RESET_VALUE);
    getStTaskWarning().setValue(CHILD_STTASKWARNING_RESET_VALUE);
    getBgColor().setValue(CHILD_BGCOLOR_RESET_VALUE);
    getBtSnapShot().setValue(CHILD_BTSNAPSHOT_RESET_VALUE);
    getBtReassign().setValue(CHILD_BTREASSIGN_RESET_VALUE);

    //--Release2.1--//
    //// In order to translate the UserTypeDescription part inside the ComputedColumn
    //// the UserTypeDescription should be separated into the distinct field
    //// and translated on fly (see doMasterWorkQueueImpl detail description).
    getStUtDescription().setValue(CHILD_STUTDESCRIPTION_RESET_VALUE);
    getHdUserTypeId().setValue(CHILD_HDUSERTYPEID_RESET_VALUE);

    //--TD_MWQ_CR--start--//
    //// Added Source Firm and Source to the Borrower info.
    getStSourceName().setValue(CHILD_STSOURCENAME_RESET_VALUE);
    getStSFShortName().setValue(CHILD_STSFSHORTNAME_RESET_VALUE);

    //--> Performance enhancement : elimination of the 2nd call of the DataObject
    //--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
    //--> the hidden values on each row.
    //--> By Billy 16Jan2004
    getHdCopyId().setValue(CHILD_HDCOPYID_RESET_VALUE);
    getHdApplicationId().setValue(CHILD_HDAPPLICATIONID_RESET_VALUE);

    //===================================================================================
    //-- ========== SCR#750 begins ============================================ --//
    //-- by Neil on Dec/20/2004
    getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
    getStHoldReason().setValue(CHILD_STHOLDREASON_RESET_VALUE);
    getTaskStatusOrHoldReasonStyle().setValue(CHILD_TASKSTATUSORHOLDREASONSTYLE_RESET_VALUE);

    //-- ========== SCR#750 ends ============================================== --//
    
    getStInstitutionId().setValue(CHILD_STINSTITUTIONID_RESET_VALUE);
    getHdInstitutionId().setValue(CHILD_HDINSTITUTIONID_RESET_VALUE);

  }

  /**
   *
   *
   */
  protected void registerChildren()
  {
    registerChild(CHILD_STBORROWER, StaticTextField.class);
    registerChild(CHILD_STASSIGNEDTO, StaticTextField.class);
    registerChild(CHILD_STLOGIN, StaticTextField.class);
    registerChild(CHILD_HDDEALID, HiddenField.class);
    registerChild(CHILD_STDEALNUMBER, StaticTextField.class);
    registerChild(CHILD_HDWQID, HiddenField.class);
    registerChild(CHILD_STTASKDESCRIPTION, StaticTextField.class);
    registerChild(CHILD_STTASKPRIORITY, StaticTextField.class);
    registerChild(CHILD_STTASKDUEDATE, StaticTextField.class);
    registerChild(CHILD_STTASKSTATUS, StaticTextField.class);
    registerChild(CHILD_STTASKWARNING, StaticTextField.class);
    registerChild(CHILD_BGCOLOR, StaticTextField.class);
    registerChild(CHILD_BTSNAPSHOT, Button.class);
    registerChild(CHILD_BTREASSIGN, Button.class);

    //--Release2.1--//
    //// In order to translate the UserTypeDescription part inside the ComputedColumn
    //// the UserTypeDescription should be separated into the distinct field
    //// and translated on fly (see doMasterWorkQueueImpl detail description).
    registerChild(CHILD_STUTDESCRIPTION, StaticTextField.class);
    registerChild(CHILD_HDUSERTYPEID, HiddenField.class);

    //--TD_MWQ_CR--start--//
    //// Added Source Firm and Source to the Borrower info.
    registerChild(CHILD_STSOURCENAME, StaticTextField.class);
    registerChild(CHILD_STSFSHORTNAME, StaticTextField.class);

    //--> Performance enhancement : elimination of the 2nd call of the DataObject
    //--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
    //--> the hidden values on each row.
    //--> By Billy 16Jan2004
    registerChild(CHILD_HDCOPYID, HiddenField.class);
    registerChild(CHILD_HDAPPLICATIONID, HiddenField.class);

    //==================================================================================
    //-- ========== SCR#750 begins ============================================ --//
    //-- by Neil on Dec/20/2004
    registerChild(CHILD_STDEALSTATUS, StaticTextField.class);
    registerChild(CHILD_STHOLDREASON, StaticTextField.class);
    registerChild(CHILD_TASKSTATUSORHOLDREASONSTYLE, StaticTextField.class);

    //-- ========== SCR#750 ends ============================================== --//
    registerChild(CHILD_STINSTITUTIONID, StaticTextField.class);
    registerChild(CHILD_HDINSTITUTIONID, HiddenField.class);

  }

  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList = new ArrayList();

    switch (executionType)
    {
    case MODEL_TYPE_RETRIEVE:
      modelList.add(getdoMasterWorkQueueModel());
      ;

      break;

    case MODEL_TYPE_UPDATE:

      ;

      break;

    case MODEL_TYPE_DELETE:
      ;

      break;

    case MODEL_TYPE_INSERT:
      ;

      break;

    case MODEL_TYPE_EXECUTE:
      ;

      break;
    }

    return (Model[]) modelList.toArray(new Model[0]);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
    //--> BILLYDEBUG 15Jan2004
    //logger = SysLog.getSysLogger("pgMWQRTV");
    //logger.debug("pgRepeated1@beginDisplay ::start !!");
    //==================================
    // This is the analog of NetDynamics repeated_onBeforeDisplayEvent
    // Ensure the primary model is non-null; if null, it would cause havoc
    // with the various methods dependent on the primary model
    if (getPrimaryModel() == null)
    {
      throw new ModelControlException("Primary model is null");
    }

    // The following code block was migrated from the Repeated1_onBeforeDisplayEvent method
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    handler.checkAndSyncDOWithCursorInfo(this);
    handler.pageSaveState();

    super.beginDisplay(event);
    resetTileIndex();
  }

  /**
   *
   *
   */
  public boolean nextTile() throws ModelControlException
  {
    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
    boolean movedToRow = super.nextTile();

    if (movedToRow)
    {
      // Put migrated repeated_onBeforeRowDisplayEvent code here
      int rowNum = this.getTileIndex();
      boolean ret = true;

      MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      handler.populatePickListFields(rowNum);
      ret = true;

      //-- ========== SCR#750 begins ============================================ --//
      //-- by Neil on Dec/20/2004
      try
      {
        handler.setTaskStatusWithHoldReason();
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }

      //-- ========== SCR#750   ends ============================================ --//
      handler.pageSaveState();

      return ret;
    }

    return movedToRow;
  }

  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
    ////logger = SysLog.getSysLogger("MWQH");
    ////logger.debug("MWQH@:beforeModelExecutes "+getClass().getName()
    ////    + " :: the Model Name = " + model.getName() + " :: ExecutionContext = " + executionContext);
    // This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
    // This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {
    // This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);
  }

  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {
    ////logger.debug("MWQH@:beforeModelExecutes "+getClass().getName()
    ////    + " :: the Model Name = " + model.getName() + " :: ExecutionContext = " + executionContext +
    ////    " :: Exception: " + exception);
    // This is the analog of NetDynamics repeated_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  protected View createChild(String name)
  {
    if (name.equals(CHILD_STBORROWER))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STBORROWER,
          doMasterWorkQueueModel.FIELD_DFBORROWER, CHILD_STBORROWER_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STASSIGNEDTO))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STASSIGNEDTO,
          doMasterWorkQueueModel.FIELD_DFASSIGNEDUSERNAME, CHILD_STASSIGNEDTO_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STLOGIN))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STLOGIN,
          doMasterWorkQueueModel.FIELD_DFLOGIN, CHILD_STLOGIN_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDDEALID))
    {
      HiddenField child =
        new HiddenField(this, getdoMasterWorkQueueModel(), CHILD_HDDEALID,
          doMasterWorkQueueModel.FIELD_DFDEALID, CHILD_HDDEALID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STDEALNUMBER))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STDEALNUMBER,
          /***** FXP23189, FXP23192 change to DealId from ApplicationId starts *****/
          doMasterWorkQueueModel.FIELD_DFDEALID, CHILD_STDEALNUMBER_RESET_VALUE, null);
          /***** FXP23189, FXP23192 change to DealId from ApplicationId ends *****/
      return child;
    }
    else if (name.equals(CHILD_HDWQID))
    {
      HiddenField child =
        new HiddenField(this, getdoMasterWorkQueueModel(), CHILD_HDWQID,
          doMasterWorkQueueModel.FIELD_DFASSIGNTASKID, CHILD_HDWQID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKDESCRIPTION))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STTASKDESCRIPTION,
          doMasterWorkQueueModel.FIELD_DFTASKNAME, CHILD_STTASKDESCRIPTION_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKPRIORITY))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STTASKPRIORITY,
          doMasterWorkQueueModel.FIELD_DFPRIORITYDESCRIPTION, CHILD_STTASKPRIORITY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKDUEDATE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STTASKDUEDATE,
          doMasterWorkQueueModel.FIELD_DFDUETIMESTAMP, CHILD_STTASKDUEDATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKSTATUS))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STTASKSTATUS,
          doMasterWorkQueueModel.FIELD_DFSTATUSDESC, CHILD_STTASKSTATUS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKWARNING))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STTASKWARNING,
          doMasterWorkQueueModel.FIELD_DFTMDESCRIPTION, CHILD_STTASKWARNING_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BGCOLOR))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_BGCOLOR, CHILD_BGCOLOR,
          CHILD_BGCOLOR_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTSNAPSHOT))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTSNAPSHOT, CHILD_BTSNAPSHOT,
          CHILD_BTSNAPSHOT_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTREASSIGN))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTREASSIGN, CHILD_BTREASSIGN,
          CHILD_BTREASSIGN_RESET_VALUE, null);

      return child;
    }

    //--Release2.1--//
    //// In order to translate the UserTypeDescription part inside the ComputedColumn
    //// the UserTypeDescription should be separated into the distinct field
    //// and translated on fly (see doMasterWorkQueueImpl detail description).
    else if (name.equals(CHILD_STUTDESCRIPTION))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STUTDESCRIPTION,
          doMasterWorkQueueModel.FIELD_DFUTDESCRIPTION, CHILD_STUTDESCRIPTION_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDUSERTYPEID))
    {
      HiddenField child =
        new HiddenField(this, getdoMasterWorkQueueModel(), CHILD_HDUSERTYPEID,
          doMasterWorkQueueModel.FIELD_DFUSERTYPEID, CHILD_HDUSERTYPEID_RESET_VALUE, null);

      return child;
    }

    //--TD_MWQ_CR--start--//
    //// Added Source Firm and Source to the Borrower info.
    else if (name.equals(CHILD_STSOURCENAME))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STSOURCENAME,
          doMasterWorkQueueModel.FIELD_DFSOURCE, CHILD_STSOURCENAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STSFSHORTNAME))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STSFSHORTNAME,
          doMasterWorkQueueModel.FIELD_SFSHORTNAME, CHILD_STSFSHORTNAME_RESET_VALUE, null);

      return child;
    }

    //=================================================================================
    //--> Performance enhancement : elimination of the 2nd call of the DataObject
    //--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
    //--> the hidden values on each row.
    //--> By Billy 16Jan2004
    else if (name.equals(CHILD_HDCOPYID))
    {
      HiddenField child =
        new HiddenField(this, getdoMasterWorkQueueModel(), CHILD_HDCOPYID,
          doMasterWorkQueueModel.FIELD_DFCOPYID, CHILD_HDCOPYID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDAPPLICATIONID))
    {
      HiddenField child =
        new HiddenField(this, getdoMasterWorkQueueModel(), CHILD_HDAPPLICATIONID,
          doMasterWorkQueueModel.FIELD_DFAPPLICATIONID, CHILD_HDAPPLICATIONID_RESET_VALUE, null);

      return child;

      //-- ========== SCR#750 begins ============================================ --//
      //-- by Neil on Dec/20/2004
    }
    else if (name.equals(CHILD_TASKSTATUSORHOLDREASONSTYLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_TASKSTATUSORHOLDREASONSTYLE,
          CHILD_TASKSTATUSORHOLDREASONSTYLE, CHILD_TASKSTATUSORHOLDREASONSTYLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STDEALSTATUS))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STDEALSTATUS,
          doMasterWorkQueueModel.FIELD_DFDEALSTATUS, CHILD_STDEALSTATUS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STHOLDREASON))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STHOLDREASON,
          doMasterWorkQueueModel.FIELD_DFHOLDREASONDESCRIPTION, CHILD_STHOLDREASON_RESET_VALUE, null);

      return child;

      //-- ========== SCR#750 ends ============================================== --//
    }
    else if (name.equals(CHILD_STINSTITUTIONID))
    {
      StaticTextField child =
        new StaticTextField(this, getdoMasterWorkQueueModel(), CHILD_STINSTITUTIONID,
          doMasterWorkQueueModel.FIELD_DFINSTITUTIONNAME, CHILD_STINSTITUTIONID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDINSTITUTIONID))
    {
      HiddenField child =
        new HiddenField(this, getdoMasterWorkQueueModel(), CHILD_HDINSTITUTIONID,
          doMasterWorkQueueModel.FIELD_DFINSTITUTIONID, CHILD_HDINSTITUTIONID_RESET_VALUE, null);

      return child;
    }
    else
    {
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStBorrower()
  {
    return (StaticTextField) getChild(CHILD_STBORROWER);
  }

  /**
   *
   *
   */
  public StaticTextField getStAssignedTo()
  {
    return (StaticTextField) getChild(CHILD_STASSIGNEDTO);
  }

  /**
   *
   *
   */
  public StaticTextField getStLogin()
  {
    return (StaticTextField) getChild(CHILD_STLOGIN);
  }

  /**
   *
   *
   */
  public HiddenField getHdDealId()
  {
    return (HiddenField) getChild(CHILD_HDDEALID);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealNumber()
  {
    return (StaticTextField) getChild(CHILD_STDEALNUMBER);
  }

  /**
   *
   *
   */
  public HiddenField getHdWqID()
  {
    return (HiddenField) getChild(CHILD_HDWQID);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskDescription()
  {
    return (StaticTextField) getChild(CHILD_STTASKDESCRIPTION);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskPriority()
  {
    return (StaticTextField) getChild(CHILD_STTASKPRIORITY);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskDueDate()
  {
    return (StaticTextField) getChild(CHILD_STTASKDUEDATE);
  }

  //--TD_MWQ_CR--start--//
  //// Added Source Firm and Source to the Borrower info.
  public StaticTextField getStSourceName()
  {
    return (StaticTextField) getChild(CHILD_STSOURCENAME);
  }

  public StaticTextField getStSFShortName()
  {
    return (StaticTextField) getChild(CHILD_STSFSHORTNAME);
  }

  /**
   *
   *
   */
  public boolean beginStTaskDueDateDisplay(ChildDisplayEvent event)
  {
    ////Object value = getStTaskDueDate().getValue();
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    handler.convertToUserTimeZone(this.getParentViewBean(), "Repeated1/stTaskDueDate");
    handler.pageSaveState();

    return true;

    // The following code block was migrated from the stTaskDueDate_onBeforeDisplayEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.convertToUserTimeZone(this, "*stTaskDueDate");

       handler.pageSaveState();

       return;
     */
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskStatus()
  {
    return (StaticTextField) getChild(CHILD_STTASKSTATUS);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskWarning()
  {
    return (StaticTextField) getChild(CHILD_STTASKWARNING);
  }

  /**
   *
   *
   */
  public StaticTextField getBgColor()
  {
    return (StaticTextField) getChild(CHILD_BGCOLOR);
  }

  //--Release2.1--//
  //// In order to translate the UserTypeDescription part inside the ComputedColumn
  //// the UserTypeDescription should be separated into the distinct field
  //// and translated on fly (see doMasterWorkQueueImpl detail description).

  /**
   *
   *
   */
  public StaticTextField getStUtDescription()
  {
    return (StaticTextField) getChild(CHILD_STUTDESCRIPTION);
  }

  /**
   *
   *
   */
  public HiddenField getHdUserTypeId()
  {
    return (HiddenField) getChild(CHILD_HDUSERTYPEID);
  }

  /**
   *
   *
   */
  public void handleBtSnapShotRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    //logger = SysLog.getSysLogger("MWQR1TV");
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this.getParentViewBean());

    //--> Must call handleWebAction with refresh option in order to refresh screen without
    //--> resetting the current display row index.
    this.handleWebAction(WebActions.ACTION_REFRESH);

    //logger.debug("MWQR1TV@handleBtSnapShotRequest::RowIndx: " + handler.getRowNdxFromWebEventMethod(event));
    ////handler.handleSnapShotButton(handler.getRowNdxFromWebEventMethod(event));
    //// IMPORTANT!! Verify this number!!
    handler.handleSnapShotButton(((TiledViewRequestInvocationEvent) (event)).getTileNumber());

    //logger.debug("MWQR1TV@handleBtSnapShotRequest::RowIndex FROM InvocatoionEvent: " + ((TiledViewRequestInvocationEvent)(event)).getTileNumber());
    handler.postHandlerProtocol();

    // The following code block was migrated from the btSnapShot_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleSnapShotButton(handler.getRowNdxFromWebEventMethod(event));

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtSnapShot()
  {
    return (Button) getChild(CHILD_BTSNAPSHOT);
  }

  /**
   *
   *
   */
  public void handleBtReassignRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this.getParentViewBean());
    handler.handleReassign(((TiledViewRequestInvocationEvent) (event)).getTileNumber());
    handler.postHandlerProtocol();
  }

  //// New method to manually set the current Display Offset.
  //// There is no analog in JATO framework, so it should be done here.
  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_REFRESH);
  }

  //-- ========== SCR#750 begins ============================================ --//
  //-- by Neil on Dec/20/2004

  /**
   * DOCUMENT ME!
   *
   * @return StaticTextField
   */
  public StaticTextField getStDealStatus()
  {
    return (StaticTextField) getChild(CHILD_STDEALSTATUS);
  }

  public StaticTextField getStHoldReason()
  {
    return (StaticTextField) getChild(CHILD_STHOLDREASON);
  }

  public StaticTextField getTaskStatusOrHoldReasonStyle()
  {
    return (StaticTextField) getChild(CHILD_TASKSTATUSORHOLDREASONSTYLE);
  }

  //-- ========== SCR#750 ends ============================================== --//

  /**
   *
   *
   */
  public Button getBtReassign()
  {
    return (Button) getChild(CHILD_BTREASSIGN);
  }

  /**
   *
   *
   */
  public doMasterWorkQueueModel getdoMasterWorkQueueModel()
  {
    if (doMasterWorkQueue == null)
    {
      doMasterWorkQueue = (doMasterWorkQueueModel) getModel(doMasterWorkQueueModel.class);
    }

    return doMasterWorkQueue;
  }

  /**
   *
   *
   */
  public void setdoMasterWorkQueueModel(doMasterWorkQueueModel model)
  {
    doMasterWorkQueue = model;
  }

  //--> Performance enhancement : elimination of the 2nd call of the DataObject
  //--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
  //--> the hidden values on each row.
  //--> By Billy 16Jan2004
  public HiddenField getHdCopyId()
  {
    return (HiddenField) getChild(CHILD_HDCOPYID);
  }

  public HiddenField getHdApplicationId()
  {
    return (HiddenField) getChild(CHILD_HDAPPLICATIONID);
  }
  
  public StaticTextField getStInstitutionId()
  {
    return (StaticTextField) getChild(CHILD_STINSTITUTIONID);
  }
  
  public HiddenField getHdInstitutionId()
  {
    return (HiddenField) getChild(CHILD_HDINSTITUTIONID);
  }
  
}
