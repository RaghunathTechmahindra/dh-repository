package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doDealEntryDownPaymentModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDownPaymentSourceId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDownPaymentSourceId(java.math.BigDecimal value);

	
    /**
     * 
     * 
     */
    public java.math.BigDecimal getDfCopyId();

    
    /**
     * 
     * 
     */
    public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDownPaymentSourceTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDownPaymentSourceTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDownPaymentAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfDownPaymentAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDPSDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfDPSDescription(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFDOWNPAYMENTSOURCEID="dfDownPaymentSourceId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFDOWNPAYMENTSOURCETYPEID="dfDownPaymentSourceTypeId";
	public static final String FIELD_DFDOWNPAYMENTAMOUNT="dfDownPaymentAmount";
	public static final String FIELD_DFDPSDESCRIPTION="dfDPSDescription";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

