package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;
/**
 *
 *
 *
 */
public class pgMortgageInsViewBean extends ExpressViewBeanBase

{
	/**
	 *
	 *
	 */
	public pgMortgageInsViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * @version 20.0 <br> Date: 11/23/2006 <br> Author: NBC/PP Implementation
	 *          Team <br> Change: Created a Hidden variable to Access the
	 *          mossys.properties value for Mortgage Insurance Review Screen.<br>
	 *          modified method createChild() to Access the mossys.properties
	 *          value for Mortgage Insurance Review Screen to define the
	 *          visibility of the fields in the screen <br>
	 */
	protected View createChild(String name)
	{
		View superReturn = super.createChild(name);  
		if (superReturn != null) {  
			return superReturn; 
		} else if (name.equals(CHILD_STBORRFIRSTNAME)) 
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUS,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
				CHILD_STDEALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUSDATE,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
				CHILD_STDEALSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEFIRM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCEFIRM,
				doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
				CHILD_STSOURCEFIRM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCE,
				doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
				CHILD_STSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLOB))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STLOB,
				doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
				CHILD_STLOB_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALTYPE,
				doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
				CHILD_STDEALTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALPURPOSE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALPURPOSE,
				doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
				CHILD_STDEALPURPOSE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPURCHASEPRICE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPURCHASEPRICE,
				doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
				CHILD_STPURCHASEPRICE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTTERM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPMTTERM,
				doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
				CHILD_STPMTTERM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESTCLOSINGDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STESTCLOSINGDATE,
				doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
				CHILD_STESTCLOSINGDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTOTALLOANAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STTOTALLOANAMOUNT,
				doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
				CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALID))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALID,
				doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
				CHILD_STDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;
		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else if (name.equals(CHILD_TBDEALID)) 
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);
      //--Release2.1--//
      ////Modified to set NonSelected Label as a CHILD_CBPAGENAMES_NONSELECTED_LABEL
			////child.setLabelForNoneSelected("Choose a Page");
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBMIINDICATOR))
		{
			ComboBox child = new ComboBox( this,
				getdoMIReviewModel(),
				CHILD_CBMIINDICATOR,
				doMIReviewModel.FIELD_DFMIINDICATORID,
				CHILD_CBMIINDICATOR_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMIIndicatorOptions);
			return child;
		}
    //--BMO_MI_CR--start//
    //// This field should be editable under certain circustances (BMO direct
    //// db update: MI process outside BXP.
		else
		if (name.equals(CHILD_CBMISTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoMIReviewModel(),
				CHILD_CBMISTATUS,
				doMIReviewModel.FIELD_DFMISTATUSID,
				CHILD_CBMISTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMIStatusOptions);
			return child;
		}


    //// This field should be editable under certain circustances.
		else
		if (name.equals(CHILD_TXMIPREQCERTNUM))
		{
			TextField child = new TextField(this,
				getdoMIReviewModel(),
				CHILD_TXMIPREQCERTNUM,
				doMIReviewModel.FIELD_DFPREQUALIFICATIONMICERTNUM,
				CHILD_TXMIPREQCERTNUM_RESET_VALUE,
				null);
			return child;
		}
    //--BMO_MI_CR--end//
		else
		if (name.equals(CHILD_HDMISTATUSID))
		{
			HiddenField child = new HiddenField(this,
				getdoMIReviewModel(),
				CHILD_HDMISTATUSID,
				doMIReviewModel.FIELD_DFMISTATUSID,
				CHILD_HDMISTATUSID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDMISTATUSUPDATEFLAG))
		{
			HiddenField child = new HiddenField(this,
				getdoMIReviewModel(),
				CHILD_HDMISTATUSUPDATEFLAG,
				doMIReviewModel.FIELD_DFMISTATUSUPDATEFLAG,
				CHILD_HDMISTATUSUPDATEFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXMIEXISTINGPOLICY))
		{
			TextField child = new TextField(this,
				getdoMIReviewModel(),
				CHILD_TXMIEXISTINGPOLICY,
				doMIReviewModel.FIELD_DFMIEXISTINGPOLICYNUMBER,
				CHILD_TXMIEXISTINGPOLICY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBMITYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoMIReviewModel(),
				CHILD_CBMITYPE,
				doMIReviewModel.FIELD_DFMITYPEID,
				CHILD_CBMITYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			return child;
		}
		else
		if (name.equals(CHILD_CBMIINSURER))
		{
			ComboBox child = new ComboBox( this,
				getdoMIReviewModel(),
				CHILD_CBMIINSURER,
				doMIReviewModel.FIELD_DFMIINSURERID,
				CHILD_CBMIINSURER_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMIInsurerOptions);
			return child;
		}

        // SEAN DJ SPEC-Progress Advance Type July 22, 2005: Create the child...
        else if (name.equals(CHILD_CBPROGRESSADVANCETYPE))
        {
          ComboBox child =
            new ComboBox(this, getdoMIReviewModel(), CHILD_CBPROGRESSADVANCETYPE,
                         doMIReviewModel.FIELD_DFPROGRESSADVANCETYPEID,
                         CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE, null);
          child.setLabelForNoneSelected("");
          child.setOptions(cbProgressAdvanceTypeOptions);

          return child;
        }
        // SEAN DJ SPEC-PAT END
		else
		if (name.equals(CHILD_CBMIPAYOR))
		{
			ComboBox child = new ComboBox( this,
				getdoMIReviewModel(),
				CHILD_CBMIPAYOR,
				doMIReviewModel.FIELD_DFMIPAYORID,
				CHILD_CBMIPAYOR_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMIPayorOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXMIPREMIUM))
		{
			TextField child = new TextField(this,
				getdoMIReviewModel(),
				CHILD_TXMIPREMIUM,
				doMIReviewModel.FIELD_DFMIPREMIUM,
				CHILD_TXMIPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDMIPREMIUM))
		{
			HiddenField child = new HiddenField(this,
				getdoMIReviewModel(),
				CHILD_HDMIPREMIUM,
				doMIReviewModel.FIELD_DFMIPREMIUM,
				CHILD_HDMIPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDLIENPOSITION))
		{
			HiddenField child = new HiddenField(this,
				getdoMIReviewModel(),
				CHILD_HDLIENPOSITION,
				doMIReviewModel.FIELD_DFLIENPOSITION,
				CHILD_HDLIENPOSITION_RESET_VALUE,
				null);
			return child;
		}
		else
    // Changed to TextBox in order to display the Policy dynamically based on the MI-Insurer
    // -- by Billy 16July2003
		if (name.equals(CHILD_TXMIPOLICYNO))
		{
			TextField child = new TextField(this,
				getdoMIReviewModel(),
				CHILD_TXMIPOLICYNO,
				doMIReviewModel.FIELD_DFMIPOLICYNUMBER,
				CHILD_TXMIPOLICYNO_RESET_VALUE,
				null);
			return child;
		}
    //=======================================================================================
		else
		if (name.equals(CHILD_RBMIUPFRONT))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoMIReviewModel(),
				CHILD_RBMIUPFRONT,
				doMIReviewModel.FIELD_DFMIUPFRONT,
				CHILD_RBMIUPFRONT_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(rbMIUpfrontOptions);
			return child;
		}
		else
		if (name.equals(CHILD_RBMIRUINTERVENTION))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getdoMIReviewModel(),
				CHILD_RBMIRUINTERVENTION,
				doMIReviewModel.FIELD_DFMIRUINTERVENTION,
				CHILD_RBMIRUINTERVENTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(rbMIRUInterventionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXMICOMMENTS))
		{
			TextField child = new TextField(this,
				getdoMIReviewModel(),
				CHILD_TXMICOMMENTS,
				doMIReviewModel.FIELD_DFMICOMMENTS,
				CHILD_TXMICOMMENTS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXMIRESPONSE))
		{
			TextField child = new TextField(this,
				getdoMIReviewModel(),
				CHILD_TXMIRESPONSE,
				doMIReviewModel.FIELD_DFMIRESPONSE,
				CHILD_TXMIRESPONSE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTTODEALMOD))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTODEALMOD,
				CHILD_BTTODEALMOD,
				CHILD_BTTODEALMOD_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSPECIALFEATURE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSPECIALFEATURE,
				doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
				CHILD_STSPECIALFEATURE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STVIEWONLYTAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTOK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTOK,
				CHILD_BTOK,
				CHILD_BTOK_RESET_VALUE,
				null);
				return child;

		}
    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //// SYNCADD
    else
 		if (name.equals(CHILD_STSOURCEFIRMNAME))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOURCEFIRMNAME,
 				doMIReviewModel.FIELD_DFSOURCEFIRMNAME,
 				CHILD_STSOURCEFIRMNAME_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBFIRSTNAME))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBFIRSTNAME,
 				doMIReviewModel.FIELD_DFSOBFIRSTNAME,
 				CHILD_STSOBFIRSTNAME_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBLASTNAME))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBLASTNAME,
 				doMIReviewModel.FIELD_DFSOBLASTNAME,
 				CHILD_STSOBLASTNAME_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBADDRESSLINE1))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBADDRESSLINE1,
 				doMIReviewModel.FIELD_DFSOBADDRESSLINE1,
 				CHILD_STSOBADDRESSLINE1_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBADDRESSLINE2))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBADDRESSLINE2,
 				doMIReviewModel.FIELD_DFSOBADDRESSLINE2,
 				CHILD_STSOBADDRESSLINE2_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBCITY))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBCITY,
 				doMIReviewModel.FIELD_DFSOBCITY,
 				CHILD_STSOBCITY_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBPROVINCE))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBPROVINCE,
 				doMIReviewModel.FIELD_DFSOBPROVINCE,
 				CHILD_STSOBPROVINCE_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBPHONENUMBER))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBPHONENUMBER,
 				doMIReviewModel.FIELD_DFSOBPHONENUM,
 				CHILD_STSOBPHONENUMBER_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBPHONEEXT))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBPHONEEXT,
 				doMIReviewModel.FIELD_DFSOBPHONEEXT,
 				CHILD_STSOBPHONEEXT_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBFAXNUMBER))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBFAXNUMBER,
 				doMIReviewModel.FIELD_DFSOBFAXNUM,
 				CHILD_STSOBFAXNUMBER_RESET_VALUE,
 				null);
 			return child;
 		}
 		else
 		if (name.equals(CHILD_STSOBREFNUM))
 		{
 			StaticTextField child = new StaticTextField(this,
 				getdoMIReviewModel(),
 				CHILD_STSOBREFNUM,
 				doMIReviewModel.FIELD_DFSOURCEAPPID,
 				CHILD_STSOBREFNUM_RESET_VALUE,
 				null);
 			return child;
 		}
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
    //===============================================================
    // New fields to handle MIPolicyNum problem :
    //  the number lost if user cancel MI and re-send again later.
    // -- Modified by Billy 14July2003
    else
    if (name.equals(CHILD_HDMIPOLICYNOCMHC))
		{
			HiddenField child = new HiddenField(this,
				getdoMIReviewModel(),
				CHILD_HDMIPOLICYNOCMHC,
				doMIReviewModel.FIELD_DFMIPOLICYNUMBERCMHC,
				CHILD_HDMIPOLICYNOCMHC_RESET_VALUE,
				null);
			return child;
		}
    else
    if (name.equals(CHILD_HDMIPOLICYNOGE))
		{
			HiddenField child = new HiddenField(this,
				getdoMIReviewModel(),
				CHILD_HDMIPOLICYNOGE,
				doMIReviewModel.FIELD_DFMIPOLICYNUMBERGE,
				CHILD_HDMIPOLICYNOGE_RESET_VALUE,
				null);
			return child;
		}
    else
    	if (name.equals(CHILD_HDMIPOLICYNOAIGUG))
		{
			HiddenField child = new HiddenField(this,
				getdoMIReviewModel(),
				CHILD_HDMIPOLICYNOAIGUG,
				doMIReviewModel.FIELD_DFMIPOLICYNUMBERAIGUG,
				CHILD_HDMIPOLICYNOAIGUG_RESET_VALUE,
				null);
			return child;
		}
    else
	    if (name.equals(CHILD_HDMIPOLICYNOPMI))
		{
			HiddenField child = new HiddenField(this,
				getdoMIReviewModel(),
				CHILD_HDMIPOLICYNOPMI,
				doMIReviewModel.FIELD_DFMIPOLICYNUMBERPMI,
				CHILD_HDMIPOLICYNOPMI_RESET_VALUE,
				null);
			return child;
		}
	//MI additions
	// Modified by Serghei 17 Nov 2006
    else
    	if(name.equals(CHILD_HDDEALSTATUSID)) {
    		HiddenField child = new HiddenField(this,
    				getdoMIReviewModel(),
    				CHILD_HDDEALSTATUSID,
    				doMIReviewModel.FIELD_DFDEALSTATUSID,
    				CHILD_HDDEALSTATUSID_RESET_VALUE,
    				null);
    			return child;
    	}
    else
    if(name.equals(CHILD_CBLOCREPAYMENTTYPEID))
    {
        ComboBox child = new ComboBox(this,
            getdoMIReviewModel(),
            CHILD_CBLOCREPAYMENTTYPEID,
            doMIReviewModel.FIELD_DFLOCREPAYMENTTYPEID,
            CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE,
            null);
        child.setLabelForNoneSelected("");
        child.setOptions(cbLOCRepaymentTypeOptions);
        return child;
    }
    else
    if(name.equals(CHILD_RBSELFDIRECTEDRRSP))
    {
        RadioButtonGroup child = new RadioButtonGroup(this,
            getdoMIReviewModel(),
            CHILD_RBSELFDIRECTEDRRSP,
            doMIReviewModel.FIELD_DFSELFDIRECTEDRRSP,
            CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE,
            null);
        child.setLabelForNoneSelected("");
        child.setOptions(rbSelfDirectedRRSPOptions);
        return child;
    }
    else
    if(name.equals(CHILD_RBREQUESTSTANDARDSERVICE))
    {
        RadioButtonGroup child = new RadioButtonGroup(this,
            getdoMIReviewModel(),
            CHILD_RBREQUESTSTANDARDSERVICE,
            doMIReviewModel.FIELD_DFREQUESTSTANDARDSERVICE,
            CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE,
            null);
        child.setLabelForNoneSelected("");
        child.setOptions(rbRequestStandardServiceOptions);
        return child;
    }
    else
    if(name.equals(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER))
    {
        TextField child = new TextField(this,
            getdoMIReviewModel(),
            CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER,
            doMIReviewModel.FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER,
            CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE,
            null);
        return child;
    }
    else
    if(name.equals(CHILD_STLOCAMORTIZATIONMONTHS))
    {
        StaticTextField child = new StaticTextField(this,
            getdoMIReviewModel(),
            CHILD_STLOCAMORTIZATIONMONTHS,
            doMIReviewModel.FIELD_DFLOCAMORTIZATIONMONTHS,
            CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE,
            null);
        return child;
    }
    else
    if(name.equals(CHILD_STLOCINTERESTONLYMATURITYDATE))
    {
        StaticTextField child = new StaticTextField(this,
            getdoMIReviewModel(),
            CHILD_STLOCINTERESTONLYMATURITYDATE,
            doMIReviewModel.FIELD_DFLOCINTERESTONLYMATURITYDATE,
            CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE,
            null);
        return child;
    }
    else
    if(name.equals(CHILD_HDPROGRESSADVANCE))
    {
        HiddenField child = new HiddenField(this,
            getdoMIReviewModel(),
            CHILD_HDPROGRESSADVANCE,
            doMIReviewModel.FIELD_DFPROGRESSADVANCE,
            CHILD_HDPROGRESSADVANCE_RESET_VALUE,
            null);
        return child;
    }
	//Added new Hidden Value for reading Mossys.properties value - start - bug# 1679 - start	
    else
	if(name.equals(CHILD_HDPROGRESSADVANCEHIDDEN))
	{
	     HiddenField child = new HiddenField(this,
	    		getDefaultModel(),
	            CHILD_HDPROGRESSADVANCEHIDDEN,
	            CHILD_HDPROGRESSADVANCEHIDDEN,
	            CHILD_HDPROGRESSADVANCEHIDDEN_RESET_VALUE,
	            null);
	        return child;
	    }
	else
	// code fix for bug# 1679 - end	
    if(name.equals(CHILD_RBPROGRESSADVANCEINSPECTIONBY))
    {
        RadioButtonGroup child = new RadioButtonGroup(this,
            getdoMIReviewModel(),
            CHILD_RBPROGRESSADVANCEINSPECTIONBY,
            doMIReviewModel.FIELD_DFPROGRESSADVANCEINSPECTIONBY,
            CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE,
            null);
        child.setLabelForNoneSelected("");
        child.setOptions(rbProgressAdvanceInspectionByOptions);
        return child;
    }
	// Serghei MIReview 27 July 2006
    else if (name.equals(CHILD_STHDDEALID))
    {
      HiddenField child =
        new HiddenField(this, getdoMIReviewModel(), CHILD_STHDDEALID,
        		        doMIReviewModel.FIELD_DFDEALID,
                        CHILD_STHDDEALID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STHDDEALCOPYID))
    {
      HiddenField child =
        new HiddenField(this, getdoMIReviewModel(), CHILD_STHDDEALCOPYID,
        			    doMIReviewModel.FIELD_DFCOPYID,
                        CHILD_STHDDEALCOPYID_RESET_VALUE, null);

      return child;
    }
		
	// Serghei MIReview end
    //================================================================
		// 5.0 MI -- start
        else if (name.equals(CHILD_BTPROCESSMI)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTPROCESSMI,
                CHILD_BTPROCESSMI,
                CHILD_BTPROCESSMI_RESET_VALUE,
                null);
            return child;
    
		} else if (name.equals(CHILD_HDPREVIOUSMIINDICATORID)) {
			HiddenField child = new HiddenField(
					this, 
					getdoMIReviewModel(),
					CHILD_HDPREVIOUSMIINDICATORID,
					doMIReviewModel.FIELD_DFPREVIOUSMIINDICATORID,
					CHILD_HDPREVIOUSMIINDICATORID_RESET_VALUE, null);
			return child;

        // 5.0 MI -- end
        } 
		
		//QC-Ticket-268- Start
		else if (name.equals(CHILD_HDSPECIALFEATUREID)) {
			HiddenField child = new HiddenField(
					this, 
					getdoMIReviewModel(),
					CHILD_HDSPECIALFEATUREID,
					doMIReviewModel.FIELD_DFSPECIALFEATUREID,
					CHILD_HDSPECIALFEATUREID_RESET_VALUE, null);
			return child;
        } 
		//QC-Ticket-268- Start
		
		
		else {
            throw new IllegalArgumentException("Invalid child name [" + name + "]");
        }
	}

	public void resetChildren()
	{
		super.resetChildren(); 
		getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
		getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
		getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
		getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
		getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
		getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
		getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
		getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
		getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
		getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
		getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
		getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
		getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getCbMIIndicator().setValue(CHILD_CBMIINDICATOR_RESET_VALUE);
		getHdMIStatusId().setValue(CHILD_HDMISTATUSID_RESET_VALUE);
		getHdMIStatusUpdateFlag().setValue(CHILD_HDMISTATUSUPDATEFLAG_RESET_VALUE);
		getTxMIExistingPolicy().setValue(CHILD_TXMIEXISTINGPOLICY_RESET_VALUE);
		getCbMIType().setValue(CHILD_CBMITYPE_RESET_VALUE);
		getCbMIInsurer().setValue(CHILD_CBMIINSURER_RESET_VALUE);
        // SEAN DJ SPEC-Progress Advance Type July 22, 2005: Reset value.
        getCbProgressAdvanceType().setValue(CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE);
        // SEAN DJ SPEC-PAT END
		getCbMIPayor().setValue(CHILD_CBMIPAYOR_RESET_VALUE);
		getTxMIPremium().setValue(CHILD_TXMIPREMIUM_RESET_VALUE);
		getHdMIPremium().setValue(CHILD_HDMIPREMIUM_RESET_VALUE);
		getHdLienPosition().setValue(CHILD_HDLIENPOSITION_RESET_VALUE);
		getTxMIPolicyNo().setValue(CHILD_TXMIPOLICYNO_RESET_VALUE);
		getTxMIPreQCertNum().setValue(CHILD_TXMIPREQCERTNUM_RESET_VALUE);
		getRbMIUpfront().setValue(CHILD_RBMIUPFRONT_RESET_VALUE);
		getRbMIRUIntervention().setValue(CHILD_RBMIRUINTERVENTION_RESET_VALUE);
		getTxMIComments().setValue(CHILD_TXMICOMMENTS_RESET_VALUE);
		getTxMIResponse().setValue(CHILD_TXMIRESPONSE_RESET_VALUE);
		getBtToDealMod().setValue(CHILD_BTTODEALMOD_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
		getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
		getBtOk().setValue(CHILD_BTOK_RESET_VALUE);
    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //// SYNCADD.
 		getStSourceFirmName().setValue(CHILD_STSOURCEFIRMNAME_RESET_VALUE);
 		getStSOBFirstName().setValue(CHILD_STSOBFIRSTNAME_RESET_VALUE);
 		getStSOBLastName().setValue(CHILD_STSOBLASTNAME_RESET_VALUE);
 		getStSOBAddressLine1().setValue(CHILD_STSOBADDRESSLINE1_RESET_VALUE);
 		getStSOBAddressLine2().setValue(CHILD_STSOBADDRESSLINE2_RESET_VALUE);
 		getStSOBCity().setValue(CHILD_STSOBCITY_RESET_VALUE);
 		getStSOBProvince().setValue(CHILD_STSOBPROVINCE_RESET_VALUE);
 		getStSOBPhoneNumber().setValue(CHILD_STSOBPHONENUMBER_RESET_VALUE);
 		getStSOBPhoneExt().setValue(CHILD_STSOBPHONEEXT_RESET_VALUE);
 		getStSOBFaxNumber().setValue(CHILD_STSOBFAXNUMBER_RESET_VALUE);
 		getStSOBRefNum().setValue(CHILD_STSOBREFNUM_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
    //===============================================================
    // New fields to handle MIPolicyNum problem :
    //  the number lost if user cancel MI and re-send again later.
    // -- Modified by Billy 14July2003
    getHdMIPolicyNoCMHC().setValue(CHILD_HDMIPOLICYNOCMHC_RESET_VALUE);
    getHdMIPolicyNoGE().setValue(CHILD_HDMIPOLICYNOGE_RESET_VALUE);
    getHdMIPolicyNoAIGUG().setValue(CHILD_HDMIPOLICYNOAIGUG_RESET_VALUE);
    getHdMIPolicyNoPMI().setValue(CHILD_HDMIPOLICYNOPMI_RESET_VALUE);
    //--BMO_MI_CR--start//
    // Modified by Serghei 17 Nov 2006
    getHdDealStatusId().setValue(CHILD_HDDEALSTATUSID_RESET_VALUE);
    //// This field should be editable under certain circustances (BMO direct
    //// db update: MI process outside BXP.
		getCbMIStatus().setValue(CHILD_CBMISTATUS_RESET_VALUE);
		
		//MI additions, resetChildren()
		getCbLOCRepaymentType().setValue(CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE);
		getRbSelfDirectedRRSP().setValue(CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE);
		getRbRequestStandardService().setValue(CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE);
		getTxCMHCProductTrackerIdentifier().setValue(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE);
		getStLOCAmortizationMonths().setValue(CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE);
		getStLOCInterestOnlyMaturityDate().setValue(CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE);
		getHdProgressAdvance().setValue(CHILD_HDPROGRESSADVANCE_RESET_VALUE);
		//		***** Change by NBC Impl. Team - Version 1.15.1.12.2 - Start *****//
		//code fix for bug# 1679 - to set the reset_value for the Hidden field
		getHdProgressAdvanceHidden().setValue(CHILD_HDPROGRESSADVANCEHIDDEN_RESET_VALUE);
		//code fix for bug# 1679 - end
		//		***** Change by NBC Impl. Team - Version 1.15.1.12.2 - End *****//
		getRbProgressAdvanceInspectionBy().setValue(CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE);
		// Serghei MIReview 27 July 2006
		getStHDDealId().setValue(CHILD_STHDDEALID_RESET_VALUE);
		getStHDDealCopyId().setValue(CHILD_STHDDEALCOPYID_RESET_VALUE);
		// Serghei MIReview end
		getBtProcessMI().setValue(CHILD_BTPROCESSMI_RESET_VALUE); //5.0 MI
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren(); 
		registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
		registerChild(CHILD_STSOURCE,StaticTextField.class);
		registerChild(CHILD_STLOB,StaticTextField.class);
		registerChild(CHILD_STDEALTYPE,StaticTextField.class);
		registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
		registerChild(CHILD_STPURCHASEPRICE,StaticTextField.class);
		registerChild(CHILD_STPMTTERM,StaticTextField.class);
		registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
		registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
		registerChild(CHILD_STDEALID,StaticTextField.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_CBMIINDICATOR,ComboBox.class);
		registerChild(CHILD_HDMISTATUSID,HiddenField.class);
		registerChild(CHILD_HDMISTATUSUPDATEFLAG,HiddenField.class);
		registerChild(CHILD_TXMIEXISTINGPOLICY,TextField.class);
		registerChild(CHILD_CBMITYPE,ComboBox.class);
		registerChild(CHILD_CBMIINSURER,ComboBox.class);
        // SEAN DJ SPEC-Progress Advance Type July 22, 2005: Register Child.
        registerChild(CHILD_CBPROGRESSADVANCETYPE, ComboBox.class);
        // SEAN DJ SPEC-PAT END
		registerChild(CHILD_CBMIPAYOR,ComboBox.class);
		registerChild(CHILD_TXMIPREMIUM,TextField.class);
		registerChild(CHILD_HDMIPREMIUM,HiddenField.class);
		registerChild(CHILD_HDLIENPOSITION,HiddenField.class);
		registerChild(CHILD_TXMIPOLICYNO,TextField.class);
		registerChild(CHILD_RBMIUPFRONT,RadioButtonGroup.class);
		registerChild(CHILD_RBMIRUINTERVENTION,RadioButtonGroup.class);
		registerChild(CHILD_TXMICOMMENTS,TextField.class);
		registerChild(CHILD_TXMIRESPONSE,TextField.class);
		registerChild(CHILD_BTTODEALMOD,Button.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
		registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
		registerChild(CHILD_BTOK,Button.class);
    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG,Button.class);
    //// SYNCADD.
 		registerChild(CHILD_STSOURCEFIRMNAME,StaticTextField.class);
 		registerChild(CHILD_STSOBFIRSTNAME,StaticTextField.class);
 		registerChild(CHILD_STSOBLASTNAME,StaticTextField.class);
 		registerChild(CHILD_STSOBADDRESSLINE1,StaticTextField.class);
 		registerChild(CHILD_STSOBADDRESSLINE2,StaticTextField.class);
 		registerChild(CHILD_STSOBCITY,StaticTextField.class);
 		registerChild(CHILD_STSOBPROVINCE,StaticTextField.class);
 		registerChild(CHILD_STSOBPHONENUMBER,StaticTextField.class);
 		registerChild(CHILD_STSOBPHONEEXT,StaticTextField.class);
 		registerChild(CHILD_STSOBFAXNUMBER,StaticTextField.class);
 		registerChild(CHILD_STSOBREFNUM,StaticTextField.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
    //===============================================================
    // New fields to handle MIPolicyNum problem :
    //  the number lost if user cancel MI and re-send again later.
    // -- Modified by Billy 14July2003
    registerChild(CHILD_HDMIPOLICYNOCMHC,HiddenField.class);
    registerChild(CHILD_HDMIPOLICYNOGE,HiddenField.class);
    registerChild(CHILD_HDMIPOLICYNOAIGUG,HiddenField.class);
    registerChild(CHILD_HDMIPOLICYNOPMI,HiddenField.class);
    //--BMO_MI_CR--start//
    // Modified by Serghei 17 Nov 2006
    registerChild(CHILD_HDDEALSTATUSID, HiddenField.class);
    //// This field should be editable under certain circustances (BMO direct
    //// db update: MI process outside BXP.
		registerChild(CHILD_CBMISTATUS,TextField.class);
    // It is a textfield now.
		registerChild(CHILD_TXMIPREQCERTNUM,TextField.class);
		
		//MI additions
		registerChild(CHILD_CBLOCREPAYMENTTYPEID, ComboBox.class);
		registerChild(CHILD_RBSELFDIRECTEDRRSP, RadioButtonGroup.class);
		registerChild(CHILD_RBREQUESTSTANDARDSERVICE, RadioButtonGroup.class);
		registerChild(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER, TextField.class);
		registerChild(CHILD_STLOCAMORTIZATIONMONTHS, StaticTextField.class);
		registerChild(CHILD_STLOCINTERESTONLYMATURITYDATE, StaticTextField.class);
		registerChild(CHILD_HDPROGRESSADVANCE, HiddenField.class);
		//		***** Change by NBC Impl. Team - Version 1.15.1.12.2 - Start *****//
		//code fix for bug# 1679 - Register the new Hidden field
		registerChild(CHILD_HDPROGRESSADVANCEHIDDEN, HiddenField.class);
		//code fix for bug# 1679 - Register the new Hidden field
		//		***** Change by NBC Impl. Team - Version 1.15.1.12.2 - end *****//

		registerChild(CHILD_RBPROGRESSADVANCEINSPECTIONBY, RadioButtonGroup.class);
		// Serghei MIReview 27 July 2006
		registerChild(CHILD_STHDDEALID, StaticTextField.class);
		registerChild(CHILD_STHDDEALCOPYID, StaticTextField.class);
		// Serghei MIReview end
		registerChild(CHILD_BTPROCESSMI, Button.class); //5.0
		registerChild(CHILD_HDPREVIOUSMIINDICATORID, HiddenField.class); //5.0
		
		//QC-Ticket-268- Start
		registerChild(CHILD_HDSPECIALFEATUREID, HiddenField.class);
		//QC-Ticket-268- End
		
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealSummarySnapShotModel());;modelList.add(getdoMIReviewModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

    //--Release2.1--start//
    ////Populate all ComboBoxes manually here
    //// 1. Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    ////Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
        BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    ////Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    //// 2-... Repopulate all Comboboxes on the page.
    cbMIIndicatorOptions.populate(getRequestContext());
    cbMIInsurerOptions.populate(getRequestContext());
    cbMIPayorOptions.populate(getRequestContext());
    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: populate the options.
    cbProgressAdvanceTypeOptions.populate(getRequestContext());
    // SEAN DJ SPEC-PAT END

    //--BMO_MI_CR--//
    //// MIStatus is combobox to handle MI Processing outside BXP
    cbMIStatusOptions.populate(getRequestContext());

    // 3-... Setup the Yes/No ComboBox and Radio Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());

    rbMIUpfrontOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    rbMIRUInterventionOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});

    //MI additions
    cbLOCRepaymentTypeOptions.populate(getRequestContext());
    rbSelfDirectedRRSPOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    rbRequestStandardServiceOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    
    String lenderStr = BXResources.getGenericMsg("LENDER_LABEL", handler
            .getTheSessionState().getLanguageId());
    String insurerStr = BXResources.getGenericMsg("INSURER_LABEL", handler
    		.getTheSessionState().getLanguageId());
        
    rbProgressAdvanceInspectionByOptions.setOptions(new String[]{lenderStr, insurerStr},new String[]{"L", "I"});
    
		handler.pageSaveState();

    super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatePageDisplayFields();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrFirstName()
	{
		return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatus()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceFirm()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSource()
	{
		return (StaticTextField)getChild(CHILD_STSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLOB()
	{
		return (StaticTextField)getChild(CHILD_STLOB);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealType()
	{
		return (StaticTextField)getChild(CHILD_STDEALTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealPurpose()
	{
		return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPurchasePrice()
	{
		return (StaticTextField)getChild(CHILD_STPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmtTerm()
	{
		return (StaticTextField)getChild(CHILD_STPMTTERM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEstClosingDate()
	{
		return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTotalLoanAmount()
	{
		return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealId()
	{
		return (StaticTextField)getChild(CHILD_STDEALID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


  //--BMO_MI_CR--start//
  //// MI Status field is combobox now in order to handel the MI process outside BXP
	class CbMIStatusOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbMIStatusOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "MISTATUS", theSessionState.getDealInstitutionId());
      }
	}

	//MI additions
	class CbLOCRepaymentTypeOptionsList extends BaseComboBoxOptionList {

        CbLOCRepaymentTypeOptionsList(){}

        public void populate(RequestContext rc) {
            // Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
                    .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                    .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populate(rc, languageId, "LOCREPAYMENTTYPE", theSessionState.getDealInstitutionId());
        }
    }
	
	public ComboBox getCbLOCRepaymentType()
    {
        return (ComboBox)getChild(CHILD_CBLOCREPAYMENTTYPEID);
    }
	
	public RadioButtonGroup getRbSelfDirectedRRSP()
    {
        return (RadioButtonGroup)getChild(CHILD_RBSELFDIRECTEDRRSP);
    }
	
	public RadioButtonGroup getRbRequestStandardService()
    {
        return (RadioButtonGroup)getChild(CHILD_RBREQUESTSTANDARDSERVICE);
    }
	
	public TextField getTxCMHCProductTrackerIdentifier()
    {
        return (TextField)getChild(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER);
    }
	
	public StaticTextField getStLOCAmortizationMonths()
    {
        return (StaticTextField)getChild(CHILD_STLOCAMORTIZATIONMONTHS);
    }
	
	public StaticTextField getStLOCInterestOnlyMaturityDate()
    {
        return (StaticTextField)getChild(CHILD_STLOCINTERESTONLYMATURITYDATE);
    }
	
	public HiddenField getHdProgressAdvance()
    {
        return (HiddenField)getChild(CHILD_HDPROGRESSADVANCE);
    }
	//		***** Change by NBC Impl. Team - Version 1.15.1.12.2 - Start *****//
	//  code fix bug #1679 added method to access the value of Hidden field
	public HiddenField getHdProgressAdvanceHidden()
    {
        return (HiddenField)getChild(CHILD_HDPROGRESSADVANCEHIDDEN);
    }
	//		***** Change by NBC Impl. Team - Version 1.15.1.12.2 - Start *****//
	public RadioButtonGroup getRbProgressAdvanceInspectionBy()
    {
        return (RadioButtonGroup)getChild(CHILD_RBPROGRESSADVANCEINSPECTIONBY);
    }
	//END MI additions	


  //--BMO_MI_CR--end//

	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayWorkQueue();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleChangePassword();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealHistory();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealNotes();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDealSearch();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSignOff();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(0);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(1);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(2);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(3);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(4);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(5);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(6);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(7);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(8);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(9);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleGoPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbMIIndicator()
	{
		return (ComboBox)getChild(CHILD_CBMIINDICATOR);
	}

  //--Release2.1--//
	/**
	 *  Since the population of comboboxes on the pages should be done from BXResource
   *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
   *  element if place or it could be done directly from the BXResource. Each approach
   *  has its pros and cons. The most important is: for English and French versions
   *  could be different default values. It forces to use the second approach.
   *
   *  It this case to escape annoying code clone and follow the object oriented
   *  design the following abstact base class should encapsulate the combobox
   *  OptionList population. Each ComboBoxOptionList inner class should extend
   *  this base class and implement the BXResources table name.
   *
	 *
	 */
	abstract static class BaseComboBoxOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		BaseComboBoxOptionList()
		{

		}

		protected final void populate(RequestContext rc, int langId, String tablename, int institutionProfileId)
		{
      try
      {
        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(institutionProfileId, tablename, langId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
  }


	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbMIIndicatorOptionList extends OptionList
	class CbMIIndicatorOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbMIIndicatorOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "MIINDICATOR", theSessionState.getDealInstitutionId());
      }
	}


	/**
	 *
	 *
	 */
	public StaticTextField getCbMIStatus()
	{
		return (StaticTextField)getChild(CHILD_CBMISTATUS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdMIStatusId()
	{
		return (HiddenField)getChild(CHILD_HDMISTATUSID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdMIStatusUpdateFlag()
	{
		return (HiddenField)getChild(CHILD_HDMISTATUSUPDATEFLAG);
	}


	/**
	 *
	 *
	 */
	public TextField getTxMIExistingPolicy()
	{
		return (TextField)getChild(CHILD_TXMIEXISTINGPOLICY);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbMIType()
	{
		return (ComboBox)getChild(CHILD_CBMITYPE);
	}

    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: The getter methods.
    public ComboBox getCbProgressAdvanceType()
    {
      return (ComboBox) getChild(CHILD_CBPROGRESSADVANCETYPE);
    }
    // SEAN DJ SPEC-PAT END

    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: define the inner option list class
    class CbProgressAdvanceTypeOptionList extends BaseComboBoxOptionList
    {
      /**
       *
       *
       */
      CbProgressAdvanceTypeOptionList()
      {
      }

      public void populate(RequestContext rc)
      {
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                                                                defaultInstanceStateName,
                                                                true);

        int languageId = theSessionState.getLanguageId();

        super.populate(rc, languageId, "PROGRESSADVANCETYPE", theSessionState.getDealInstitutionId());
      }
    }
    // SEAN DJ SPEC-PAT END

	/**
	 *
	 *
	 */
	public ComboBox getCbMIInsurer()
	{
		return (ComboBox)getChild(CHILD_CBMIINSURER);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbMIInsurerOptionList extends OptionList
	class CbMIInsurerOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbMIInsurerOptionList()
		{

		}

		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "MORTGAGEINSURANCECARRIER", theSessionState.getDealInstitutionId());
      }
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbMIPayor()
	{
		return (ComboBox)getChild(CHILD_CBMIPAYOR);
	}

	/**
	 *
	 *
	 */
  //--Release2.1--//
	////static class CbMIPayorOptionList extends OptionList
	class CbMIPayorOptionList extends BaseComboBoxOptionList
	{
		/**
		 *
		 *
		 */
		CbMIPayorOptionList()
		{

		}


		/**
		 *
		 *
		 */
     //--Release2.1--//
     //// See detailed comment in DealEntryViewBean.
      public void populate(RequestContext rc)
      {
          //Get the Language ID from the SessionStateModel
          String defaultInstanceStateName = rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
          SessionStateModelImpl theSessionState = (SessionStateModelImpl)rc.getModelManager().getModel(
                                                  SessionStateModel.class,
                                                  defaultInstanceStateName,
                                                  true);

          int languageId = theSessionState.getLanguageId();

          super.populate(rc, languageId, "MORTGAGEINSURANCEPAYOR", theSessionState.getDealInstitutionId());
      }
	}

	/**
	 *
	 *
	 */
	public TextField getTxMIPremium()
	{
		return (TextField)getChild(CHILD_TXMIPREMIUM);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdMIPremium()
	{
		return (HiddenField)getChild(CHILD_HDMIPREMIUM);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdLienPosition()
	{
		return (HiddenField)getChild(CHILD_HDLIENPOSITION);
	}


	/**
	 *
	 *
	 */
  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 14July2003
	public TextField getTxMIPolicyNo()
	{
		return (TextField)getChild(CHILD_TXMIPOLICYNO);
	}

   public HiddenField getHdMIPolicyNoCMHC()
	{
		return (HiddenField)getChild(CHILD_HDMIPOLICYNOCMHC);
	}

  public HiddenField getHdMIPolicyNoGE()
	{
		return (HiddenField)getChild(CHILD_HDMIPOLICYNOGE);
	}
  
  public HiddenField getHdMIPolicyNoAIGUG()
	{
		return (HiddenField)getChild(CHILD_HDMIPOLICYNOAIGUG);
	}
  public HiddenField getHdMIPolicyNoPMI()
	{
		return (HiddenField)getChild(CHILD_HDMIPOLICYNOPMI);
	}
  //==============================================================

  // Modified by Serghei 17 N0v. 2006
  public HiddenField getHdDealStatusId() {
	  return (HiddenField)getChild(CHILD_HDDEALSTATUSID);
  }

	/**
	 *
	 *
	 */
  //--BMO_MI_CR--//
  // It is textfield now.
	public TextField getTxMIPreQCertNum()
	{
		return (TextField)getChild(CHILD_TXMIPREQCERTNUM);
	}


	/**
	 *
	 *
	 */
	public RadioButtonGroup getRbMIUpfront()
	{
		return (RadioButtonGroup)getChild(CHILD_RBMIUPFRONT);
	}


	/**
	 *
	 *
	 */
	public RadioButtonGroup getRbMIRUIntervention()
	{
		return (RadioButtonGroup)getChild(CHILD_RBMIRUINTERVENTION);
	}


	/**
	 *
	 *
	 */
	public TextField getTxMIComments()
	{
		return (TextField)getChild(CHILD_TXMICOMMENTS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxMIResponse()
	{
		return (TextField)getChild(CHILD_TXMIRESPONSE);
	}

	// Serghei MIReview 27 July 2006
	public StaticTextField getStHDDealId()
	{
		return (StaticTextField) getChild(CHILD_STHDDEALID);
	}

	 /**
	  *
	  *
	  */
	public StaticTextField getStHDDealCopyId()
	{
		return (StaticTextField) getChild(CHILD_STHDDEALCOPYID);
	}
	 //	 Serghei MIReview

	/**
	 *
	 *
	 */
	public void handleBtToDealModRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleToDealMod();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToDealMod()
	{
		return (Button)getChild(CHILD_BTTODEALMOD);
	}


	/**
	 *
	 *
	 */
	public String endBtToDealModDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btToDealMod_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displayToDealModButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}

	// 5.0 MI -- start
    public void handleBtProcessMIRequest(RequestInvocationEvent event)
        throws ServletException, IOException {

        MorgageInsHandler handler = (MorgageInsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        //just execute workflow
        handler.handleWorkFlow();
        handler.postHandlerProtocol();
    }

    public Button getBtProcessMI() {
        return (Button) getChild(CHILD_BTPROCESSMI);
    }
    
    public HiddenField getHdPreviousMIIndicatorId() {
  	  return (HiddenField)getChild(CHILD_HDPREVIOUSMIINDICATORID);
    }
    
    //QC-Ticket-268- Start
    public HiddenField getHdSpecialFeatureIdId() {
    	  return (HiddenField)getChild(CHILD_HDSPECIALFEATUREID);
      }
    //QC-Ticket-268- End
	// 5.0 MI -- end

	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleMortgageInsuranceSubmit();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}


	/**
	 *
	 *
	 */
	public String endBtSubmitDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btSubmit_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displaySubmitButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleCancelStandard();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
	public String endBtCancelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btCancel_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displayCancelButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handlePrevTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleNextTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.generateTaskName();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSpecialFeature()
	{
		return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStViewOnlyTag()
	{
		return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
	}


	/**
	 *
	 *
	 */
	public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		String rc = handler.displayViewOnlyTag();

		handler.pageSaveState();

    return rc;

	}

	public String endStLOCAmortizationMonthsDisplay(ChildContentDisplayEvent event)
	  {
		MorgageInsHandler handler = (MorgageInsHandler) this.handler.cloneSS();
	    handler.pageGetState(this);

	    String rc = handler.setMonthsToYearsMonths(event.getContent());
	    handler.pageSaveState();

	    return rc;
	  }

	/**
	 *
	 *
	 */
	public void handleBtOkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

        /***** AIG UG Code change for "Cancel MI" task start *****/
        handler.handleWorkFlow();
        /***** AIG UG Code change for "Cancel MI" task end *****/

		handler.handleCancelStandard();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtOk()
	{
		return (Button)getChild(CHILD_BTOK);
	}


	/**
	 *
	 *
	 */
	public String endBtOkDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btOk_onBeforeHtmlOutputEvent method
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		boolean rc = handler.displayOKButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
	{
		if (doDealSummarySnapShot == null)
			doDealSummarySnapShot = (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
		return doDealSummarySnapShot;
	}


	/**
	 *
	 *
	 */
	public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
	{
			doDealSummarySnapShot = model;
	}


	/**
	 *
	 *
	 */
	public doMIReviewModel getdoMIReviewModel()
	{
		if (doMIReview == null)
			doMIReview = (doMIReviewModel) getModel(doMIReviewModel.class);
		return doMIReview;
	}


	/**
	 *
	 *
	 */
	public void setdoMIReviewModel(doMIReviewModel model)
	{
			doMIReview = model;
	}

  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}

  //// SYNCADD.
  /**
	 *
	 *
   */
  public StaticTextField getStSourceFirmName()
 	{
 		return (StaticTextField)getChild(CHILD_STSOURCEFIRMNAME);
  }

 	/**
 	 *
 	 *
 	 */
  public StaticTextField getStSOBFirstName()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBFIRSTNAME);
  }

 	/**
 	 *
 	 *
 	 */
  public StaticTextField getStSOBAddressLine1()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBADDRESSLINE1);
 	}

 	/**
 	 *
 	 *
 	 */
 	public StaticTextField getStSOBLastName()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBLASTNAME);
  }

 	/**
 	 *
 	 *
 	 */
 	public StaticTextField getStSOBAddressLine2()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBADDRESSLINE2);
 	}


 	/**
 	 *
 	 *
 	 */
 	public StaticTextField getStSOBCity()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBCITY);
 	}


 	/**
 	 *
 	 *
 	 */
 	public StaticTextField getStSOBProvince()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBPROVINCE);
 	}


 	/**
 	 *
 	 *
 	 */
 	public StaticTextField getStSOBPhoneNumber()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBPHONENUMBER);
 	}


 	/**
 	 *
 	 *
 	 */
 	public StaticTextField getStSOBPhoneExt()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBPHONEEXT);
 	}


 	/**
 	 *
 	 *
 	 */
 	public StaticTextField getStSOBFaxNumber()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBFAXNUMBER);
 	}


 	/**
 	 *
 	 *
 	 */
 	public StaticTextField getStSOBRefNum()
 	{
 		return (StaticTextField)getChild(CHILD_STSOBREFNUM);
 	}
  ////======================================================================

  //// Additional set of methods to populate Href display String
  public String endHref1Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }

 //--Release2.1--//
 //// This method overrides the getDefaultURL() framework JATO method and it is located
 //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
 //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
 //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
 //// The full method is still in PHC base class. It should care the
 //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
 public String getDisplayURL()
 {
       MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();
       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////


  public void handleActMessageOK(String[] args)
	{
		MorgageInsHandler handler =(MorgageInsHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

		handler.handleActMessageOK(args);

    handler.postHandlerProtocol();

	}

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgMortgageIns";
	public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgMortgageIns.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
	public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUS="stDealStatus";
	public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
	public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STSOURCEFIRM="stSourceFirm";
	public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
	public static final String CHILD_STSOURCE="stSource";
	public static final String CHILD_STSOURCE_RESET_VALUE="";
	public static final String CHILD_STLOB="stLOB";
	public static final String CHILD_STLOB_RESET_VALUE="";
	public static final String CHILD_STDEALTYPE="stDealType";
	public static final String CHILD_STDEALTYPE_RESET_VALUE="";
	public static final String CHILD_STDEALPURPOSE="stDealPurpose";
	public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
	public static final String CHILD_STPURCHASEPRICE="stPurchasePrice";
	public static final String CHILD_STPURCHASEPRICE_RESET_VALUE="";
	public static final String CHILD_STPMTTERM="stPmtTerm";
	public static final String CHILD_STPMTTERM_RESET_VALUE="";
	public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
	public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";
	public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
	public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
	public static final String CHILD_STDEALID="stDealId";
	public static final String CHILD_STDEALID_RESET_VALUE=" ";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";
	
	

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screw up the population.
	private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  ////private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  ////Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_CBMIINDICATOR="cbMIIndicator";
	public static final String CHILD_CBMIINDICATOR_RESET_VALUE="0.0";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the
	////private static CbMIIndicatorOptionList cbMIIndicatorOptions=new CbMIIndicatorOptionList();
	private CbMIIndicatorOptionList cbMIIndicatorOptions=new CbMIIndicatorOptionList();

	public static final String CHILD_HDMISTATUSID="hdMIStatusId";
	public static final String CHILD_HDMISTATUSID_RESET_VALUE="";
	public static final String CHILD_HDMISTATUSUPDATEFLAG="hdMIStatusUpdateFlag";
	public static final String CHILD_HDMISTATUSUPDATEFLAG_RESET_VALUE="";
	public static final String CHILD_TXMIEXISTINGPOLICY="txMIExistingPolicy";
	public static final String CHILD_TXMIEXISTINGPOLICY_RESET_VALUE="";
	public static final String CHILD_CBMITYPE="cbMIType";
	public static final String CHILD_CBMITYPE_RESET_VALUE="1.0";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources

    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: constants for new fields.
    public static final String CHILD_CBPROGRESSADVANCETYPE = "cbProgressAdvanceType";
    public static final String CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE = "";
    // SEAN DJ SPEC-PAT END

    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: prepare the options.
    private CbProgressAdvanceTypeOptionList cbProgressAdvanceTypeOptions =
        new CbProgressAdvanceTypeOptionList();
    // SEAN DJ SPEC-PAT END

	public static final String CHILD_CBMIINSURER="cbMIInsurer";
	public static final String CHILD_CBMIINSURER_RESET_VALUE="1.0";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static CbMIInsurerOptionList cbMIInsurerOptions=new CbMIInsurerOptionList();
	private CbMIInsurerOptionList cbMIInsurerOptions=new CbMIInsurerOptionList();

	public static final String CHILD_CBMIPAYOR="cbMIPayor";
	public static final String CHILD_CBMIPAYOR_RESET_VALUE="1.0";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static CbMIPayorOptionList cbMIPayorOptions=new CbMIPayorOptionList();
	private CbMIPayorOptionList cbMIPayorOptions=new CbMIPayorOptionList();

	public static final String CHILD_TXMIPREMIUM="txMIPremium";
	public static final String CHILD_TXMIPREMIUM_RESET_VALUE="";
	public static final String CHILD_HDMIPREMIUM="hdMIPremium";
	public static final String CHILD_HDMIPREMIUM_RESET_VALUE="";
	public static final String CHILD_HDLIENPOSITION="hdLienPosition";
	public static final String CHILD_HDLIENPOSITION_RESET_VALUE="";
	public static final String CHILD_TXMIPOLICYNO="txMIPolicyNo";
	public static final String CHILD_TXMIPOLICYNO_RESET_VALUE="";
	public static final String CHILD_TXMIPREQCERTNUM="txMIPreQCertNum";
	public static final String CHILD_TXMIPREQCERTNUM_RESET_VALUE="";
	public static final String CHILD_RBMIUPFRONT="rbMIUpfront";
	public static final String CHILD_RBMIUPFRONT_RESET_VALUE="N";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList rbMIUpfrontOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList rbMIUpfrontOptions=new OptionList();

	public static final String CHILD_RBMIRUINTERVENTION="rbMIRUIntervention";
	public static final String CHILD_RBMIRUINTERVENTION_RESET_VALUE="N";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList rbMIRUInterventionOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList rbMIRUInterventionOptions=new OptionList();

	public static final String CHILD_TXMICOMMENTS="txMIComments";
	public static final String CHILD_TXMICOMMENTS_RESET_VALUE="";
	public static final String CHILD_TXMIRESPONSE="txMIResponse";
	public static final String CHILD_TXMIRESPONSE_RESET_VALUE="";
	public static final String CHILD_BTTODEALMOD="btToDealMod";
	public static final String CHILD_BTTODEALMOD_RESET_VALUE=" ";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
	public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";
	public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
	public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";
	public static final String CHILD_BTOK="btOk";
	public static final String CHILD_BTOK_RESET_VALUE=" ";
  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //// SYNCADD.
 	public static final String CHILD_STSOURCEFIRMNAME="stSourceFirmName";
 	public static final String CHILD_STSOURCEFIRMNAME_RESET_VALUE="";
 	public static final String CHILD_STSOBFIRSTNAME="stSOBFirstName";
 	public static final String CHILD_STSOBFIRSTNAME_RESET_VALUE="";
 	public static final String CHILD_STSOBLASTNAME="stSOBLastName";
 	public static final String CHILD_STSOBLASTNAME_RESET_VALUE="";
 	public static final String CHILD_STSOBADDRESSLINE1="stSOBAddressLine1";
 	public static final String CHILD_STSOBADDRESSLINE1_RESET_VALUE="";
 	public static final String CHILD_STSOBADDRESSLINE2="stSOBAddressLine2";
 	public static final String CHILD_STSOBADDRESSLINE2_RESET_VALUE="";
 	public static final String CHILD_STSOBCITY="stSOBCity";
 	public static final String CHILD_STSOBCITY_RESET_VALUE="";
 	public static final String CHILD_STSOBPROVINCE="stSOBProvince";
 	public static final String CHILD_STSOBPROVINCE_RESET_VALUE="";
 	public static final String CHILD_STSOBPHONENUMBER="stSOBPhoneNumber";
 	public static final String CHILD_STSOBPHONENUMBER_RESET_VALUE="";
 	public static final String CHILD_STSOBPHONEEXT="stSOBPhoneExt";
 	public static final String CHILD_STSOBPHONEEXT_RESET_VALUE="";
 	public static final String CHILD_STSOBFAXNUMBER="stSOBFaxNumber";
 	public static final String CHILD_STSOBFAXNUMBER_RESET_VALUE="";
 	public static final String CHILD_STSOBREFNUM="stSOBRefNum";
 	public static final String CHILD_STSOBREFNUM_RESET_VALUE="";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";
  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 14July2003
  public static final String CHILD_HDMIPOLICYNOCMHC="hdMIPolicyNoCMHC";
	public static final String CHILD_HDMIPOLICYNOCMHC_RESET_VALUE="";
  public static final String CHILD_HDMIPOLICYNOGE="hdMIPolicyNoGE";
	public static final String CHILD_HDMIPOLICYNOGE_RESET_VALUE="";
  public static final String CHILD_HDMIPOLICYNOAIGUG="hdMIPolicyNoAIGUG";
  public static final String CHILD_HDMIPOLICYNOAIGUG_RESET_VALUE="";
  public static final String CHILD_HDMIPOLICYNOPMI="hdMIPolicyNoPMI";
  public static final String CHILD_HDMIPOLICYNOPMI_RESET_VALUE="";
	
  // Modified by Serghei 17 Nov 2006
  public static final String CHILD_HDDEALSTATUSID = "hdDealStatusId";
  public static final String CHILD_HDDEALSTATUSID_RESET_VALUE="";
  

  //--BMO_MI_CR--start//

	public static final String CHILD_CBMISTATUS="cbMIStatus";
	public static final String CHILD_CBMISTATUS_RESET_VALUE="";
	private CbMIStatusOptionList cbMIStatusOptions=new CbMIStatusOptionList();

	
	public static final String CHILD_BTPROCESSMI="btProcessMI";
	public static final String CHILD_BTPROCESSMI_RESET_VALUE=" ";

	public static final String CHILD_HDPREVIOUSMIINDICATORID="hdPreviousMIIndicatorId";
	public static final String CHILD_HDPREVIOUSMIINDICATORID_RESET_VALUE="";
	
	//QC-Ticket-268- Start
	public static final String CHILD_HDSPECIALFEATUREID="hdSpecialFeatureId";
	public static final String CHILD_HDSPECIALFEATUREID_RESET_VALUE="";
	//QC-Ticket-268- End

	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealSummarySnapShotModel doDealSummarySnapShot=null;
	private doMIReviewModel doMIReview=null;




	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	MorgageInsHandler handler=new MorgageInsHandler();
	
	//MI additions
	public static final String CHILD_CBLOCREPAYMENTTYPEID = "cbLOCRepaymentType";
    public static final String CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE = "";
    private CbLOCRepaymentTypeOptionsList cbLOCRepaymentTypeOptions = new CbLOCRepaymentTypeOptionsList();

    public static final String CHILD_RBSELFDIRECTEDRRSP = "rbSelfDirectedRRSP";
    public static final String CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE = "";
    private OptionList rbSelfDirectedRRSPOptions = new OptionList();
    
    public static final String CHILD_RBREQUESTSTANDARDSERVICE = "rbRequestStandardService";
    public static final String CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE = "";
    private OptionList rbRequestStandardServiceOptions = new OptionList();
    
    public static final String CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER = "txCMHCProductTrackerIdentifier";
    public static final String CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE = "";
    
    public static final String CHILD_STLOCAMORTIZATIONMONTHS = "stLOCAmortizationMonths";
    public static final String CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE = "";
    
    public static final String CHILD_STLOCINTERESTONLYMATURITYDATE = "stLOCInterestOnlyMaturityDate";
    public static final String CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE = "";
    
    public static final String CHILD_HDPROGRESSADVANCE = "hdProgressAdvance";
    public static final String CHILD_HDPROGRESSADVANCE_RESET_VALUE = "";
	// ***** Change by NBC Impl. Team - Version 1.15.1.12.2 - Start *****//
	// code fix bug #1679 added new String Constant for MI Screen. To access the
	// mossys.properties value to define the visibility of Progress Advance Type
	// Progress Advance Inspection By
    public static final String CHILD_HDPROGRESSADVANCEHIDDEN = "hdProgressAdvanceHidden";
	//		***** Change by NBC Impl. Team - Version 1.15.1.12.2 -End *****//
    
    public static final String CHILD_HDPROGRESSADVANCEHIDDEN_RESET_VALUE = "";
    
    public static final String CHILD_RBPROGRESSADVANCEINSPECTIONBY = "rbProgressAdvanceInspectionBy";
    public static final String CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE = "";
    //Serghei MIReview July 27 2006
    public static final String CHILD_STHDDEALID = "stHDDealId";
    public static final String CHILD_STHDDEALID_RESET_VALUE = "";
    public static final String CHILD_STHDDEALCOPYID = "stHDDealCopyId";
    public static final String CHILD_STHDDEALCOPYID_RESET_VALUE = "";
    // Serghei MIReview end
    
    private OptionList rbProgressAdvanceInspectionByOptions = new OptionList();
}

