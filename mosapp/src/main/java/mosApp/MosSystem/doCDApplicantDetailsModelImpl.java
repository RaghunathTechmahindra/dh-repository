/**
 * 
 */
package mosApp.MosSystem;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * @author NBC Implementation Team
 * 
 */
public class doCDApplicantDetailsModelImpl extends QueryModelBase implements
		doCDApplicantDetailsModel {

	public SysLogger logger;
	
	public static final String DATA_SOURCE_NAME = "jdbc/orcl";

	public static final String SELECT_SQL_TEMPLATE = "SELECT ALL BORROWER.BORROWERID, BORROWER.DEALID, BORROWER.COPYID, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERLASTNAME, BORROWER.BORROWERFIRSTNAME AS APPLICANTNAME, " +
			"ADJUDICATIONAPPLICANTRESPONSE.RESPONSEID, ADJUDICATIONAPPLICANTRESPONSE.APPLICANTNUMBER, " +
			"ADJUDICATIONAPPLICANTRESPONSE.CREDITBUREAUREPORT, ADJUDICATIONAPPLICANTRESPONSE.CREDITBUREAUREPORT as CBREPORT FROM BORROWER, BORROWERRESPONSEASSOC, " +
			"ADJUDICATIONAPPLICANTRESPONSE __WHERE__";

	public static final String MODIFYING_QUERY_TABLE_NAME = "BORROWER, BORROWERRESPONSEASSOC, ADJUDICATIONAPPLICANTRESPONSE";

	public static final String STATIC_WHERE_CRITERIA = "BORROWER.BORROWERID = BORROWERRESPONSEASSOC.BORROWERID AND BORROWERRESPONSEASSOC.RESPONSEID = ADJUDICATIONAPPLICANTRESPONSE.RESPONSEID"; //BORROWER.COPYID = BORROWERRESPONSEASSOC.COPYID AND 

	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFBORROWERFIRSTNAME = "BORROWER.BORROWERFIRSTNAME";

	public static final String COLUMN_DFBORROWERFIRSTNAME = "BORROWERFIRSTNAME";

	public static final String QUALIFIED_COLUMN_DFBORROWERLASTNAME = "BORROWER.BORROWERLASTNAME";

	public static final String COLUMN_DFBORROWERLASTNAME = "BORROWERLASTNAME";

	public static final String QUALIFIED_COLUMN_DFBORROWERID = "BORROWER.BORROWERID";

	public static final String COLUMN_DFBORROWERID = "BORROWERID";
	
	public static final String QUALIFIED_COLUMN_DFDEALID = "BORROWER.DEALID";
	
	public static final String COLUMN_DFDEALID = "DEALID";
	
	public static final String QUALIFIED_COLUMN_DFCOPYID = "BORROWER.COPYID";
	
	public static final String COLUMN_DFCOPYID = "COPYID";

	public static final String QUALIFIED_COLUMN_DFRESPONSEID = "ADJUDICATIONAPPLICANTRESPONSE.RESPONSEID";

	public static final String COLUMN_DFRESPONSEID = "RESPONSEID";

	public static final String QUALIFIED_COLUMN_DFCREDITBUREAUREPORT = "ADJUDICATIONAPPLICANTRESPONSE.CREDITBUREAUREPORT";

	public static final String COLUMN_DFCREDITBUREAUREPORT = "CREDITBUREAUREPORT";

	public static final String QUALIFIED_COLUMN_DFAPPLICANTNUMBER = "ADJUDICATIONAPPLICANTRESPONSE.APPLICANTNUMBER";

	public static final String COLUMN_DFAPPLICANTNUMBER = "APPLICANTNUMBER";
	
	public static final String QUALIFIED_COLUMN_DFAPPLICANTNAME = "BORROWER.BORROWERFIRSTNAME";

	public static final String COLUMN_DFAPPLICANTNAME = "BORROWERFIRSTNAME";
	
	public static final String QUALIFIED_COLUMN_DFCBREPORT = "ADJUDICATIONAPPLICANTRESPONSE.CREDITBUREAUREPORT";

	public static final String COLUMN_DFCBREPORT = "CBREPORT";
	/**
	 * 
	 */
	public doCDApplicantDetailsModelImpl() {
		super();
		setDataSourceName(DATA_SOURCE_NAME);
		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
		setFieldSchema(FIELD_SCHEMA);
		initialize();
	}

	public void initialize() {
		
	}

	protected String beforeExecute(ModelExecutionContext context,
			int queryType, String sql) throws ModelControlException {

		return sql;
	}

	protected void afterExecute(ModelExecutionContext context, int queryType)
			throws ModelControlException {
		
	    logger = SysLog.getSysLogger("CDApplicantDetailsModel");
	    logger.debug("CDApplicantDetailsModel@afterExecute::Size: " + this.getSize());
	    logger.debug("CDApplicantDetailsModel@afterExecute::SQLTemplate: " + this.getSelectSQL());
	    
	    while(this.next())
		{
	    	
				String applicantName = this.getDfBorrowerFirstName() + ", " + this.getDfBorrowerLastName();
				this.setDfApplicantName(applicantName);
			try {	
				//CLOB handle code begin
				
				java.sql.Clob clob = this.getDfCBReport();
				int i = (int)clob.length();
				StringBuffer stringbuffer = new StringBuffer(i);
				try
		        {
		            Reader reader = clob.getCharacterStream();	            
		            char ac[] = new char[i];
		            for(int j = 0; (j = reader.read(ac)) != -1;)
		            {
		            	for(int k = 0; k < j; k++){
		                       stringbuffer.append(ac[k]);
		                }
		            }
		            reader.close();
		        }
		        catch(Exception ex)
		        {
					logger.error("Exception when handle CLOB");
					logger.error(ex);
		        }
		        String clobString = stringbuffer.toString();

				this.setDfCreditBureauReport(clobString);
			} catch (SQLException e) {
				logger.error("SQLException when handle CLOB");
				logger.error(e);
			}

		}
	
	}

	protected void onDatabaseError(ModelExecutionContext context,
			int queryType, SQLException exception) {
	}

	public String getDfApplicantName() {
		return (String)getValue(FIELD_DFAPPLICANTNAME);
	}

	public void setDfApplicantName(String value) {
		setValue(FIELD_DFAPPLICANTNAME,value);

	}
	
	public String getDfBorrowerFirstName() {
		return (String)getValue(FIELD_DFBORROWERFIRSTNAME);
	}

	public void setDfBorrowerFirstName(String value) {
		setValue(FIELD_DFBORROWERFIRSTNAME,value);

	}

	public String getDfBorrowerLastName() {
		return (String)getValue(FIELD_DFBORROWERLASTNAME);
	}

	public void setDfBorrowerLastName(String value) {
		setValue(FIELD_DFBORROWERLASTNAME,value);

	}

	public BigDecimal getDfBorrowerId() {
		
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}

	public void setDfBorrowerId(BigDecimal value) {
		setValue(FIELD_DFBORROWERID,value);

	}

	
	public BigDecimal getDfDealId() {
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}

	public void setDfDealId(BigDecimal value) {
		setValue(FIELD_DFCOPYID,value);

	}
	
	public BigDecimal getDfCopyId() {
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}

	public void setDfCopyId(BigDecimal value) {
		setValue(FIELD_DFCOPYID,value);

	}

	public String getDfCreditBureauReport() {
		
		return (String) getValue(FIELD_DFCREDITBUREAUREPORT);
	}

	public void setDfCreditBureauReport(String value) {
		setValue(FIELD_DFCREDITBUREAUREPORT,value);

	}
	
	public java.sql.Clob getDfCBReport() {
		
		return (java.sql.Clob) getValue(FIELD_DFCBREPORT);
	}

	public void setDfCBReport(java.sql.Clob value) {
		setValue(FIELD_DFCBREPORT,value);

	}

	public BigDecimal getDfResponseId() {
		return (java.math.BigDecimal)getValue(FIELD_DFRESPONSEID);
	}

	public void setDfResponseId(BigDecimal value) {
		setValue(FIELD_DFRESPONSEID,value);

	}

	public BigDecimal getDfApplicantNumber() {
		return (java.math.BigDecimal)getValue(FIELD_DFAPPLICANTNUMBER);
	}

	public void setDfApplicantNumber(BigDecimal value) {
		setValue(FIELD_DFAPPLICANTNUMBER,value);

	}

	////////////////////////////////////////////////////////////////////////////
	// FieldDescriptor variables
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERFIRSTNAME,
				COLUMN_DFBORROWERFIRSTNAME,
				QUALIFIED_COLUMN_DFBORROWERFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERLASTNAME,
				COLUMN_DFBORROWERLASTNAME,
				QUALIFIED_COLUMN_DFBORROWERLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDEALID,
					COLUMN_DFDEALID,
					QUALIFIED_COLUMN_DFDEALID,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITBUREAUREPORT,
				COLUMN_DFCREDITBUREAUREPORT,
				QUALIFIED_COLUMN_DFCREDITBUREAUREPORT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRESPONSEID,
				COLUMN_DFRESPONSEID,
				QUALIFIED_COLUMN_DFRESPONSEID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFAPPLICANTNUMBER,
					COLUMN_DFAPPLICANTNUMBER,
					QUALIFIED_COLUMN_DFAPPLICANTNUMBER,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFAPPLICANTNAME,
					COLUMN_DFAPPLICANTNAME,
					QUALIFIED_COLUMN_DFAPPLICANTNAME,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFCBREPORT,
					COLUMN_DFCBREPORT,
					QUALIFIED_COLUMN_DFCBREPORT,
					java.sql.Clob.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
	}
	
}
