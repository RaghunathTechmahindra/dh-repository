package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Sc;

import com.basis100.deal.entity.QuickLinks;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.util.service.QuickLinkFaviconFinder;
import com.filogix.externallinks.services.ServiceInfo;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>
 * Title: QuickLinksHandler.java
 * </p>
 * <p>
 * Description: Handler Class for Quick Link screen with Jato Framework
 * </p>
 * 
 * @author
 * @version 1.0 (Initial Version � May 25, 2011)
 */
public class QuickLinksHandler extends PageHandlerCommon implements Sc {

	private final static Logger  logger = LoggerFactory.getLogger(QuickLinksHandler.class);
	
	public static String quickLinkEnabledForAllBraches = "Y";
	
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final String NULL  = "NULL";
	public static final String HTTP_PREFIX  = "http://";
	public static final String HTTPS_PREFIX  = "https://";
	public static final String FAVICON_NAME  = "/favicon.ico";
	

	/**
	 * <p>
	 * cloneSS
	 * </p>
	 */
	public QuickLinksHandler cloneSS() {
		return (QuickLinksHandler) super.cloneSafeShallow();

	}

	
	/**
	 * <P>
	 * setQuickLinkDisplayFields
	 * 
	 * set the values from database tables to quick link screen
	 * 
	 * @param tileIndex
	 *            - index of tile fromDataModel - indicates to populate data
	 *            from model or empty fields.
	 * 
	 *            flag = true populate from data model
	 * 
	 *            flag = false populate from empty remaining fields
	 * 
	 * @author MCM team
	 */
	protected void setQuickLinkDisplayFields(int tileIndex,
			boolean fromDataModel) {

		String tileViewPrefix = pgQuickLinksViewBean.CHILD_REPEATED1 + "/";

		doQuickLinkModel model = (doQuickLinkModel) (RequestManager
				.getRequestContext().getModelManager()
				.getModel(doQuickLinkModel.class));

		// setting QuickLinkID data of quick links
		HiddenField stQuickLinkID = (HiddenField) getCurrNDPage()
				.getDisplayField(
						tileViewPrefix + pgQuickLinkTiledView.CHILD_HDQID);
		
		if (fromDataModel)
			stQuickLinkID.setValue(model
					.getValue(doQuickLinkModel.FIELD_DFQUICKLINKID));
		else
			stQuickLinkID.setValue("");

		// setting LinkNameEnglish data of quick links
		TextField stLinkNameEnglish = (TextField) getCurrNDPage()
				.getDisplayField(
						tileViewPrefix
								+ pgQuickLinkTiledView.CHILD_TBDESCRIPTIONENGLISH);
		stLinkNameEnglish.setExtraHtml("id=\"descEnglish["+tileIndex+"]\" ");
		
		if (fromDataModel)
			stLinkNameEnglish.setValue(model
					.getValue(doQuickLinkModel.FIELD_DFLINKNAMEENGLISH));
		else
			stLinkNameEnglish.setValue("");

		// setting LinkNameFrench to quick links
		TextField stLinkNameFrench = (TextField) getCurrNDPage()
				.getDisplayField(
						tileViewPrefix
								+ pgQuickLinkTiledView.CHILD_TBDESCRIPTIONFRENCH);
		stLinkNameFrench.setExtraHtml("id=\"descFrench["+tileIndex+"]\" ");
		
		if (fromDataModel)
			stLinkNameFrench.setValue(model
					.getValue(doQuickLinkModel.FIELD_DFLINKNAMEFRENCH));
		else
			stLinkNameFrench.setValue("");

		// setting UrlLink to quick links
		TextField stUrlLink = (TextField) getCurrNDPage().getDisplayField(
				tileViewPrefix + pgQuickLinkTiledView.CHILD_TBURLADDRESS);
		
		stUrlLink.setExtraHtml("id=\"urlAddress["+tileIndex+"]\" ");
		
		if (fromDataModel)
			stUrlLink.setValue(model.getValue(doQuickLinkModel.FIELD_URLLINK));
		else
			stUrlLink.setValue("");

		// setting Sort order to quick links
		HiddenField stSortOrder = (HiddenField) getCurrNDPage()
				.getDisplayField(
						tileViewPrefix + pgQuickLinkTiledView.CHILD_HDSORTID);
		if (fromDataModel)
			stSortOrder.setValue(model
					.getValue(doQuickLinkModel.FIELD_DFSORTORDER));
		else
			stSortOrder.setValue("");

		String quickList = tileViewPrefix
				+ pgQuickLinkTiledView.CHILD_STQUICKLINKLIST;
		
		logger.debug("Institution profile id from model : "+ model.getValue(doQuickLinkModel.FIELD_DFINSTITUTIONPROFILEID));
		logger.debug("Branch profile id from model : "+ model.getValue(doQuickLinkModel.FIELD_DFBRANCHPROFILEID));
		
		populateStaticTextData(quickList, tileIndex);
	}

	/**
	 * <p>
	 * populateStaticTextData
	 * </p>
	 * 
	 * <p>
	 * Description: To generate static text after 3rd quick link row.
	 * </p>
	 * 
	 * @param childQuickLinkList
	 *            : StaticTextField
	 * @param tileIndex
	 *            : int
	 * 
	 */
	private void populateStaticTextData(String childQuickLinkList, int tileIndex) {
		
		if (tileIndex == 2) {
			// Load French/English based on language selection
			StaticTextField quickLinkListField = (StaticTextField) getCurrNDPage()
			.getDisplayField(childQuickLinkList);
			quickLinkListField.setValue(BXResources.getGenericMsg(
					"QL_LIST_BUTTONS_LABEL", this.getTheSessionState()
					.getLanguageId()));
		} else {
			StaticTextField quickLinkListField = (StaticTextField) getCurrNDPage()
			.getDisplayField(childQuickLinkList);
			quickLinkListField.setValue("");
		}
			
	}

		
	/*
	 * Setup data objects associated with page - e.g. set criteria for deal snap
	 * shot, etc.
	 */
	public void setupBeforePageGeneration() {
		logger.trace("--T--> @QuickLinkHandler.setupBeforePageGeneration:");

		PageEntry pg = theSessionState.getCurrentPage();
		
		if (pg.getSetupBeforeGenerationCalled() == true)
			return;
				
		pg.setSetupBeforeGenerationCalled(true);

	}
	
	/**
	 * <p>
	 * handleBtSaveonWebEvent
	 * </p>
	 * <p>
	 * Description: Save the quick links to database table
	 * </p>
	 * 
	 * 
	 */
	public void handleBtSaveonWebEvent()  throws Exception {
		SessionResourceKit srk = getSessionResourceKit();
		
		String copyFromBranchProfileId = "";
		String branchProfileId = "";
		int oldQuickLinksBranchId = 0;
		// Read from branch drop down
		PropertiesCache cache = PropertiesCache.getInstance();
		quickLinkEnabledForAllBraches = (cache.getProperty(srk.getExpressState().getUserInstitutionId(),
				"com.filogix.ingestion.quicklinks.enabled.ALLBranches", "Y"));
	
		
		if (quickLinkEnabledForAllBraches.equals(NO)) {
			copyFromBranchProfileId = (getCurrNDPage().getDisplayFieldValue(
					pgQuickLinksViewBean.CHILD_CBCOPYFROMBRANCHNAMES)
					.toString());
			branchProfileId = (getCurrNDPage().getDisplayFieldValue(
					pgQuickLinksViewBean.CHILD_CBBRANCHNAMES).toString());
			
			if(branchProfileId.length() == 0) {
				
				ActiveMessage am = setActiveMessageToAlert(BXResources.getGenericMsg(
						 "QL_CBBRANCHNAMES_NONSELECTED_LABEL", theSessionState.getLanguageId()),
			              ActiveMsgFactory.ISCUSTOMCONFIRM, "NA");
				
				return;
			} else if(copyFromBranchProfileId.length() != 0){
				oldQuickLinksBranchId = Integer.parseInt(branchProfileId);
			}
					
		}

		logger.debug("@QuickLinkHamdler.handleBtSaveonWebEvent Branch Profile ID:"
						+ branchProfileId);

		List<QuickLinks> quicklist = readUserEnteredQuickLinks(branchProfileId,
				copyFromBranchProfileId);
		// Save the updated quick links data to table
		QuickLinks link = null;
		try {
			
			link = new QuickLinks(srk);
			srk.beginTransaction();
			link.updateQuickLinks(quicklist,oldQuickLinksBranchId);
			srk.commitTransaction();
		} 
		catch (Exception e) {
			String strError = "QuickLinkHandler@handleBtSaveonWebEvent(): "
					+ e.getMessage();
			logger.error(strError);
			srk.rollbackTransaction();
		}
		finally {
			srk.freeResources();
		}
	}

	/**
	 * <p>
	 * handleBtSaveonWebEvent
	 * </p>
	 * <p>
	 * Description: Reads user entered quick links from front end
	 * </p>
	 * 
	 * @param branchProfileId
	 *            : String
	 * @param copyFromBranchProfileId
	 *            : String
	 * 
	 ** @return List: Quick links rows from user interface
	 */

	private List<QuickLinks> readUserEnteredQuickLinks(String branchProfileId,
			String copyFromBranchProfileId) {
		QuickLinks quicklink = null;
		String value = "";

		List<QuickLinks> quicklist = new ArrayList<QuickLinks>();
		String tileViewPrefix = pgQuickLinksViewBean.CHILD_REPEATED1 + "/";

		ViewBean ViewBean = getCurrNDPage();
		
		int instId = srk.getExpressState().getDealInstitutionId();
		logger.debug("@QuickLinkHamdler.handleBtSaveonWebEvent Deal Institution Id:" + instId);
		
		// get all the fields data
		for (int i = 0; i < pgQuickLinksViewBean.MAXCOMPTILE_SIZE; i++) {
			quicklink = new QuickLinks();

			value = readFromPage(tileViewPrefix
					+ pgQuickLinkTiledView.CHILD_HDQID, i, ViewBean);

			// if copying quick links from other branch - set the quick link -0
			// for insertion
			
			if (quickLinkEnabledForAllBraches.equals(NO)
					&& copyFromBranchProfileId.length() != 0
					&& branchProfileId.length() != 0) {
				quicklink.setQuickLinkId(0);
			} else if (value.length() != 0)
				quicklink.setQuickLinkId((Integer.parseInt(value)));
			else
				quicklink.setQuickLinkId(0);

			quicklink.setLinkNameEnglish(readFromPage(tileViewPrefix
					+ pgQuickLinkTiledView.CHILD_TBDESCRIPTIONENGLISH, i,
					ViewBean));

			quicklink.setLinkNameFrench(readFromPage(tileViewPrefix
					+ pgQuickLinkTiledView.CHILD_TBDESCRIPTIONFRENCH, i,
					ViewBean));

			quicklink.setUrlAddress(readFromPage(tileViewPrefix
					+ pgQuickLinkTiledView.CHILD_TBURLADDRESS, i, ViewBean));

			quicklink.setSortOrder(i + 1);

			// code to get Favicon from URL
			String url = readFromPage(tileViewPrefix
					+ pgQuickLinkTiledView.CHILD_TBURLADDRESS, i, ViewBean);
			// if URL does not contain http:// or https:// than add http:// to url
			if ( url != null &&  url.length() != 0 && url.startsWith(HTTP_PREFIX) == false  && url.startsWith(HTTPS_PREFIX) == false ) 
					url = HTTP_PREFIX + url; 
			
			// FXP33253
			// find favicon location from URL
			QuickLinkFaviconFinder finder = new QuickLinkFaviconFinder();
			String faviconURL = finder.getFaviconURL(url);
			if (faviconURL == null || url.trim() == "") {
	          if ((url.startsWith((HTTP_PREFIX)) && (url.indexOf('/', 8) != -1)))
	              url = url.substring(0, url.indexOf('/', 8)) + FAVICON_NAME;
	          else if ((url.startsWith((HTTPS_PREFIX)) && (url.indexOf('/', 9) != -1)))
	              url = url.substring(0, url.indexOf('/', 8)) + FAVICON_NAME;
	          else if (url.length() != 0)
	              url = url + FAVICON_NAME;
	          else
	              url = "";
			}

			logger.debug("@QuickLinkHamdler.handleBtSaveonWebEvent Favicon:"
					+ url);

			quicklink.setFavicon(url);

			if (quickLinkEnabledForAllBraches.equals(YES))
				quicklink.setBranchProfileId(-1);
			else
				quicklink.setBranchProfileId(Integer.parseInt(branchProfileId));

			quicklink.setInstitutionProfileId(instId);

			quicklist.add(quicklink);
		}

		return quicklist;
	}

	/**
	 * <p>
	 * populatePageDisplayFields
	 * </p>
	 * <p>
	 * Description: Populate Quick Links - default display
	 * </p>
	 * 
	 * 
	 */
	public void populatePageDisplayFields() {
		logger.debug("QuickLinkHandler:populatePageDisplayFields() method called");
		PageEntry pg = getTheSessionState().getCurrentPage();

		populatePageShellDisplayFields();

		// populate Task navigator
		populateTaskNavigator(pg);
		// populate page links
		populatePreviousPagesLinks();

		String selectedBranch = this.getCurrNDPage().getDisplayFieldValue(
				pgQuickLinksViewBean.CHILD_CBBRANCHNAMES).toString();
		String copyFromBranch = this.getCurrNDPage().getDisplayFieldValue(
				pgQuickLinksViewBean.CHILD_CBCOPYFROMBRANCHNAMES).toString();
		// Fetch quick Links for all the branches
		PropertiesCache cache = PropertiesCache.getInstance();
		quickLinkEnabledForAllBraches = (cache.getProperty(srk.getExpressState().getUserInstitutionId(),
				"com.filogix.ingestion.quicklinks.enabled.ALLBranches", "Y"));
		
		logger.debug("QuickLinkHandler.populatePageDisplayFields(): before vpd status " + srk.getActualVPDStateForDebug());
		
		if (quickLinkEnabledForAllBraches.equalsIgnoreCase(YES))
			executeCriteriaQuickLinkModel(pg);
		else {
			// Branch option selected
			if (selectedBranch.length() == 0 && copyFromBranch.length() == 0)
				executeCriteriaQuickLinkModel(pg, -1);
			else if (selectedBranch.length() != 0
					&& copyFromBranch.length() == 0)
				executeCriteriaQuickLinkModel(pg, Integer
						.parseInt(selectedBranch));
			else if (selectedBranch.length() != 0
					&& copyFromBranch.length() != 0)
				executeCriteriaQuickLinkModel(pg, Integer
						.parseInt(copyFromBranch));
		}
		setCbxDisplayFields();
		logger.debug("QuickLinkHandler.populatePageDisplayFields: after vpd status " + srk.getActualVPDStateForDebug());
	}

	/**
	 * <p>
	 * populateQuickLinkForSelectedBranch
	 * </p>
	 * <p>
	 * Description: Populate Quick Links - when user change branch drop down
	 * </p>
	 * 
	 */
	public void populateQuickLinkForSelectedBranch() {
		logger.debug("QuickLinkHandler:populatePageDisplayFields(int copyFromBranchProfileId) method called");
	}

	/**
	 * <p>
	 * populateQuickLnksForCopyFromBranch
	 * </p>
	 * <p>
	 * Description: Populate Quick Links from Selected Copy From Branch
	 * </p>
	 * 
	 */
	public void populateQuickLnksForCopyFromBranch() {
		logger.debug("QuickLinkHandler:populateQuickLnksForCopyFromBranch() method called");	
	}

	/**
	 * <p>
	 * executeCriteriaQuickLinkModel
	 * </p>
	 * <p>
	 * Description: fetch Quick Links URLs based on branch and institution id.
	 * </p>
	 * 
	 * @param pg
	 *            : PageEntry
	 * @param branchProfileId
	 *            : int
	 * 
	 */
	protected void executeCriteriaQuickLinkModel(PageEntry pg,
			int branchProfileId) {

		doQuickLinkModel theDO = (doQuickLinkModel) (RequestManager
				.getRequestContext().getModelManager()
				.getModel(doQuickLinkModel.class));

		theDO.clearUserWhereCriteria();

		theDO.addUserWhereCriterion(doQuickLinkModel.FIELD_DFBRANCHPROFILEID,
				"=", String.valueOf(branchProfileId));
		theDO.addUserWhereCriterion(doQuickLinkModel.FIELD_DFINSTITUTIONPROFILEID,
				" = ", srk.getExpressState().getDealInstitutionId());
		executeModel(theDO, "CDH@@executeCriteriaQuickLinkModel", false);
	}

	/**
	 * <p>
	 * executeCriteriaQuickLinkModel
	 * </p>
	 * <p>
	 * Description: fetch Quick Links URLs based on institution id is NULL.
	 * </p>
	 * 
	 * @param pg
	 *            : PageEntry
	 * 
	 */
	protected void executeCriteriaQuickLinkModel(PageEntry pg) {

		doQuickLinkModel theDO = (doQuickLinkModel) (RequestManager
				.getRequestContext().getModelManager()
				.getModel(doQuickLinkModel.class));

		theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(doQuickLinkModel.FIELD_DFBRANCHPROFILEID,
				" is ", NULL);
		theDO.addUserWhereCriterion(doQuickLinkModel.FIELD_DFINSTITUTIONPROFILEID,
				" = ", srk.getExpressState().getDealInstitutionId());
		executeModel(theDO, "CDH@@executeCriteriaQuickLinkModel", false);
	}

	/**
	 * <p>
	 * setCbxDisplayFields
	 * </p>
	 * <p>
	 * Description: enable /disable drop down
	 * </p>
	 * 
	 * 
	 */
	public void setCbxDisplayFields() {

		ViewBean currNDPage = getCurrNDPage();
		// Branch
		ComboBox cbbranches = null;
		cbbranches = (ComboBox) currNDPage
				.getChild(pgQuickLinksViewBean.CHILD_CBBRANCHNAMES);

		// Copy From Branch
		ComboBox cbcopyfrombranches = null;
		cbcopyfrombranches = (ComboBox) currNDPage
				.getChild(pgQuickLinksViewBean.CHILD_CBCOPYFROMBRANCHNAMES);
		
		StaticTextField stAllBranches = null;
		stAllBranches = (StaticTextField) currNDPage
				.getChild(pgQuickLinksViewBean.CHILD_STALLBRANCHES);
		
		if (quickLinkEnabledForAllBraches.equals(YES)) {
			cbbranches.setVisible(false);
			cbcopyfrombranches.setVisible(false);
			// French 
			stAllBranches.setValue(BXResources.getGenericMsg(
					"QL_ALL_BRANCHES", this.getTheSessionState()
					.getLanguageId()));
		} else
			stAllBranches.setVisible(true);
	}
}
