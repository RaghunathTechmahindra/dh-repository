package mosApp.MosSystem;

/**
 * 23/May/2006 DVG #DG426 #3242  apostrophe quadruples when used in notes
 */

import java.util.Hashtable;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.Parameters;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import MosSystem.Mc;


//===================================================================================
//
//  PageCondition5 : true - if user need is coming from deal resolution screen and need to navigate to MI screen
//===================================================================================

/**
 *
 *
 */
public class DealNotesHandler extends PageHandlerCommon
  implements Cloneable, Xc
{
  ///////////////////////////////////////////////////////////////////////////

  public DealNotesHandler cloneSS()
  {
    return(DealNotesHandler) super.cloneSafeShallow();

  }


  ///////////////////////////////////////////////////////////////////////////
  //This method is used to populate the fields in the header
  //with the appropriate information

  public void populatePageDisplayFields()
  {
    SessionStateModelImpl theSessionState = getTheSessionState();

    PageEntry pg = getTheSessionState().getCurrentPage();

    Hashtable pst = pg.getPageStateTable();

    PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

    populatePageShellDisplayFields();

    // Quick Link Menu display
    displayQuickLinkMenu();
    
    populateTaskNavigator(pg);

    populatePageDealSummarySnapShot();

    populatePreviousPagesLinks();

    revisePrevNextButtonGeneration(pg, tds);


    // note category and text
    getCurrNDPage().setDisplayFieldValue("tbNoteText", new String(pg.getPageState1()));

    //--> Use the get/set Value instead
    //--> Modified by BILLY 06Aug2002
    //int noteCategoryNdx = getSelectedNoteCategoryNdx(pg);
    String noteCategoryId = getSelectedNoteCategoryNdx(pg);
    if (noteCategoryId != null && !noteCategoryId.trim().equals(""))
    {
      getCurrNDPage().setDisplayFieldValue("cbNoteCategory", noteCategoryId);
    }
    //=====================================================

    populateRowsDisplayed(getCurrNDPage(), tds);

  }


  /////////////////////////////////////////////////////////////////////////

  private void populateRowsDisplayed(ViewBean NDPage, PageCursorInfo tds)
  {
    String rowsRpt = "";

    if (tds.getTotalRows() > 0)
    {
      rowsRpt = BXResources.getSysMsg("DP_DEALNOTESROWSDISPLAYED", theSessionState.getLanguageId());

      try
      {
        rowsRpt = com.basis100.deal.util.Format.printf(rowsRpt,
                new Parameters(tds.getFirstDisplayRowNumber())
                .add(tds.getLastDisplayRowNumber())
                .add(tds.getTotalRows()));
      }
      catch (Exception e)
      {
        logger.warning("String Format error @ DealNotesHandler.populateRowsDisplayed: " + e);
      }
    }

    NDPage.setDisplayFieldValue("stRowsDisplayed", new String(rowsRpt));

  }


  ///////////////////////////////////////////////////////////////////////////
  //
  // Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
  //

  public void setupBeforePageGeneration()
  {

    //// testPassiveMessage();
    //// testActiveMessage();
    PageEntry pg = getTheSessionState().getCurrentPage();

    if (pg.getSetupBeforeGenerationCalled() == true) return;

    pg.setSetupBeforeGenerationCalled(true);


    // -- //
    setupDealSummarySnapShotDO(pg);


    // set the criteria for the populating data object
    String dealIdStr = "" + pg.getPageDealId();

    //// SYNCADD.
    //CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doDealNotesInfoModel.class);
    doDealNotesInfoModelImpl theDO =(doDealNotesInfoModelImpl)
      (RequestManager.getRequestContext().getModelManager().getModel(doDealNotesInfoModel.class));

    theDO.clearUserWhereCriteria();

    theDO.addUserWhereCriterion("dfDealID", "=", new String(dealIdStr));
    // bilingual deal notes project
    final String langStr = "("+ theSessionState.getLanguageId()+ ", "+ LANGUAGE_UNKNOW + ")";
    theDO.addUserWhereCriterion("dfLanguagePreferenceID", "in", langStr);      

    ////===============================================================

    PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

    Hashtable pst = pg.getPageStateTable();

    if (tds == null)
    {
      // first invocation ...
      // clear 'check deal notes flag' if necessary
      checkAcknowledgeNotes(getSessionResourceKit(), pg);

      // determine number of notes (shown) per display page
      String voName = "Repeated1";
      int perPage =((TiledView)getCurrNDPage().getChild(voName)).getMaxDisplayTiles();
      int numRows = countDealNotes(srk, pg);

      // set up and save task display state object
      tds = new PageCursorInfo("DEFAULT", voName, perPage, numRows);
      tds.setRefresh(true);
      pst.put(tds.getName(), tds);
      pg.setPageState1("");

      // maintains note text
      pg.setPageState2("0");

      // maintains note category id (string rep., 0 = none selected
    }

    // execute notes data object to determine number of notes
    try
    {
      theDO.executeSelect(null);
      tds.setTotalRows(theDO.getSize());
    }
    catch(Exception ex)
    {
      setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
      logger.error("Problem encountered getting Deal Notes");
      logger.error(ex);
      return;
    }


    // determine if we will force display of first page
    if (tds.isTotalRowsChanged()|| getTheSessionState().getLanguageChanged())
    {
      // number of rows has changed - unconditional display of first page
      TiledView vo =(TiledView)(getCurrNDPage().getChild(tds.getVoName()));
      tds.setCurrPageNdx(0);
      //vo.goToFirst();
      try
      {
        vo.resetTileIndex();
      }
      catch (ModelControlException mce)
      {
        logger.warning("DealNoteHandler@setupBeforePageGeneration::Exception when resetTileIndex: " + mce);
      }

      tds.setTotalRowsChanged(false);
      getTheSessionState().setLanguageChanged(false);
      
    }

  }


  /**
   *
   *
   */
  public void checkAcknowledgeNotes(SessionResourceKit srk, PageEntry pg)
  {
    boolean inTx = false;

    try
    {
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), null);

      String chkNotesFlag = deal.getCheckNotesFlag() + "N";
      if (deal.getCheckNotesFlag() == null || deal.getCheckNotesFlag().equals("N"))
         return;

      inTx = srk.isInTransaction();
      if (inTx == false) srk.beginTransaction();
      deal.setCheckNotesFlag("N");
      deal.ejbStore();
      workflowTrigger(2, srk, pg, null);
      if (inTx == false) srk.commitTransaction();
    }
    catch(Exception e)
    {
      if (inTx == false) srk.cleanTransaction();
      logger.error("Exception @DealNotestHandler.checkAcknowledgeNotes: clearing check notes flag - ignored.");
      logger.error(e);
    }

  }


  ///////////////////////////////////////////////////////////////////////////

  public void handleSubmitStandard(boolean isExit)
  {
    PageEntry pe = theSessionState.getCurrentPage();

    SessionResourceKit srk = getSessionResourceKit();


    // validate data ....
    if (validateData(pe, srk) == false) return;


    // active and/or passive message set in this case
    try
    {
      srk.beginTransaction();
      updateData(pe, srk);
      standardAdoptTxCopy(pe, true);
      //--> New requirement from Product to Trigger Workflow un-conditionally
      //--> By Billy 12Jan2004
      workflowTrigger(2, srk, pe, null);
      //=====================================================================
      srk.commitTransaction();
      if (isExit == false)
      {
    	  // fixed bug by Midori 18 April 2007
    	  // if user came from UWWS, then go back to UW
    	  // Copyid must be maitained
    	  if (!pe.isSubPage())
    	      pe.setPageDealCID(- 1);

        // ensure copy selection activated - cannot ensure current copy still exists
        // (since no lock during deal notes usage)!
        dealNoteNavigateToNext(pe);
      }
      else
      {
    	  // condition evaluates to true if MI request is pending - Ticket 250
    	  if (pe.getPageCondition5())
          {
    		  setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_INIT_REQ_PENDING_MSG", theSessionState.getLanguageId()),
                      ActiveMsgFactory.ISCUSTOMDIALOG2, "M", "N"); 
    		  return;
          }
    	  
    	  navigateToNextPage(true);
      }
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      logger.error("Exception occurred @handleSubmit()");
      logger.error(e);
      setStandardFailMessage();
      return;
    }

  }
  
  /** Ticket 250
  * added new method for exit button on deal notes screen as need to navigate to 
  * Mortgage screen on deal approval if MI is not requested before
  */
  public void handleExitDealNotes()
  {
	  PageEntry pe = theSessionState.getCurrentPage();
	  
	  // condition evaluates to true if MI request is pending - Ticket 250
	  if (pe.getPageCondition5())
      {
		  setActiveMessageToAlert(BXResources.getSysMsg("MI_REVIEW_RCMSTATUS_INIT_REQ_PENDING_MSG", theSessionState.getLanguageId()),
                  ActiveMsgFactory.ISCUSTOMDIALOG2, "M", "N"); 
		  return;
      }
	  handleExitStandard(false);
  }
  
  /**
   * this method handles the Active message ok and cancle button
   */
  public void handleCustomActMessageOk(String[] args)
  {
      PageEntry pg = theSessionState.getCurrentPage();

      if (args[0].equals("M") || args[0].equals("N"))
      {
    	  if (args[0].equals("M"))
    	  {
    		  // navigate to MI screen - Commented below code for QC-Ticket-427
    		  //PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_MORTGAGE_INSURANCE, true);
    		  
    		  //QC-Ticket-427- Start
    		  //Setting up MI screen as main page instead of sub page so that after deal approval cancel button on MI screen takes user to IWQ 
    		  PageEntry pgEntry = setupMainPagePageEntry(
    				  				Mc.PGNM_MORTGAGE_INSURANCE, 
    				  				pg.getPageDealId(), 
    				  				-1, 
    				  				theSessionState.getDealInstitutionId());
    		  //QC-Ticket-427- End	
    		  
		      getSavedPages().setNextPage(pgEntry);
    	  }
		  pg.setPageCondition5(false);
		  navigateToNextPage(true);
    	  return;
      }
  }


  ///////////////////////////////////////////////////////////////////////////

  public void dealNoteNavigateToNext(PageEntry pg)
  {
    try
    {
      //getCurrNDPage().setDisplayFieldValue("cbNoteCategory",new String());
      getCurrNDPage().setDisplayFieldValue("tbNoteText", new String(""));
      getSavedPages().setNextPage(pg);
      navigateToNextPage(true);
    }
    catch(Exception e)
    {
      logger.error("Exception occurred @dealNoteNavigateToNext()");
      logger.error(e);
      setStandardFailMessage();
      return;
    }

  }


  ///////////////////////////////////////////////////////////////////////////

  public void saveData(PageEntry pg, boolean calledInTransaction)
  {

    // get note text and store in page state 1
    // get not category and store in page state 2
    String msgText =((String) getCurrNDPage().getDisplayFieldValue("tbNoteText")).toString();

    pg.setPageState1(msgText);


    //  consider page modified if note text present (only! - we don't care about note category)
    if (msgText != null && msgText.length() > 0) pg.setModified(true);
    else pg.setModified(false);


    //get note category and note text
    pg.setPageState2("" +(getCurrNDPage().getDisplayFieldValue("cbNoteCategory")));

  }


  ///////////////////////////////////////////////////////////////////////////

  public boolean validateData(PageEntry pe, SessionResourceKit srk)
  {
    String noteCategoryIndex = getSelectedNoteCategoryNdx(pe);

    if (noteCategoryIndex == null || noteCategoryIndex.trim().equals(""))
    {
      setActiveMessageToAlert(BXResources.getSysMsg("DEAL_NOTE_CATEGORY_INVALID", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      return false;
    }

    String noteText = pe.getPageState1();

    if (! ClientUtils.isValidTextValue(noteText))
    {
      setActiveMessageToAlert(BXResources.getSysMsg("DEAL_NOTE_NOTE_MISSING", theSessionState.getLanguageId()),
        ActiveMsgFactory.ISCUSTOMCONFIRM);
      return false;
    }

    return true;

  }


  ///////////////////////////////////////////////////////////////////////////

  public void updateData(PageEntry pe, SessionResourceKit srk)
    throws Exception
  {
    String noteText = pe.getPageState1();    //#DG426

    // trim text if too long
    //Toni
    //if (noteText.length() > 249 )
    //	noteText = noteText.substring(0,249);
    // insert note
    int dealId = pe.getPageDealId();

    int userId = getTheSessionState().getSessionUserId();

    DealPK dealPk = new DealPK(dealId, 1);


    // only needed to pass deal id
    DealNotes dealNotes = new DealNotes(srk);

    //#DG426 noteText = StringUtil.makeQuoteSafe(noteText);

    //noteText.replace('\'','\"');
    dealNotes.create(dealPk, dealId, getSelectedNoteCategoryID(), noteText, null, userId);

    dealNotes.setDealNotesDate(new java.util.Date());

    pe.setPageState1("");

    pe.setPageState2("0");

  }

  ///////////////////////////////////////////////////////////////////////////
  //DJ_Ticket#665--start--//
  //// IMPORTANT!! This trivial routine solves the multiplying of quote
  //// by JavaScript.
  //// Pro: doesn't take regression testing and keep up all functionalities.
  //// Con: for full solution not only from the screens should be called from
  //// the entity as well. DocPrep/Ingestion still running on JVM 1.3 and should
  //// be ported first to JVM 1.4.

  //// Commented our to escape conflict with Ingestion/DocPrep until their
  //// porting to JVM 1.4
  /**
  private String suppressQuotes(String in)
  {
    Pattern p = Pattern.compile("\'\'");

    Matcher m = p.matcher(in);
    StringBuffer sb = new StringBuffer();
    while (m.find()) {
      m.appendReplacement(sb, "\'");
    }

    m.appendTail(sb);
    String ret = sb.toString();

    return ret;
  }
  **/
  //DJ_Ticket#665--end--//

  ///////////////////////////////////////////////////////////////////////////

  public void handleForwardButton()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

    int numDealNotes = countDealNotes(srk, pg);

    handleForwardBackwardCommon(pg, tds, numDealNotes, true);

    return;

  }


  ///////////////////////////////////////////////////////////////////////////

  public void handleBackwardButton()
  {
    PageEntry pg = getTheSessionState().getCurrentPage();
    Hashtable pst = pg.getPageStateTable();
    PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

    int numDealNotes = countDealNotes(srk, pg);

    handleForwardBackwardCommon(pg, tds, numDealNotes, false);

    return;

  }

  ///////////////////////////////////////////////////////////////////////////

  private int countDealNotes(SessionResourceKit srk, PageEntry pg)
  {
    int count = 1;
    String sql = "SELECT count(*) from ( select ALL DEALNOTES.DEALNOTESID, contact.contactlastname " + 
                 " FROM DEALNOTES, USERPROFILE, CONTACT, DEALNOTESCATEGORY " + 
                 " WHERE  (USERPROFILE.CONTACTID  =  CONTACT.CONTACTID) " + 
                 " AND (USERPROFILE.USERPROFILEID  =  DEALNOTES.DEALNOTESUSERID) " + 
                 " AND (DEALNOTES.DEALNOTESCATEGORYID  =  DEALNOTESCATEGORY.DEALNOTESCATEGORYID) " +
                 " AND DEALNOTES.DEALID  = " + pg.getPageDealId() +
                 " AND DEALNOTES.LanguagePreferenceID in ( -1, " + theSessionState.getLanguageId() + "))" ;

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sql);
      while(jExec.next(key))
      {
        count = jExec.getInt(key, 1);
        break;
      }
      jExec.closeData(key);
    }
    catch(Exception e)
    {
      ;
    }

    return count;

  }


  ///////////////////////////////////////////////////////////////////////////
  //
  // Implementation
  //

  private String getSelectedNoteCategoryNdx(PageEntry pg)
  {
    return(pg.getPageState2());
  }


  ///////////////////////////////////////////////////////////////////////////

  private int getSelectedNoteCategoryID()
  {
    ComboBox cb =(ComboBox) getCurrNDPage().getDisplayField("cbNoteCategory");

    return(com.iplanet.jato.util.TypeConverter.asInt(cb.getValue()));

  }


  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////

  private void testPassiveMessage()
  {
    PassiveMessage pm = theSessionState.getPasMessage();

    if (pm == null)
    {
      pm = new PassiveMessage();
      pm.setGenerate(true);
      pm.setTitle("Just Testing");
      pm.setInfoMsg("Information message. This message is for your information only. Please don't tell anyone you say this.");
      pm.addMsg("First Critical message - you did something really bad", pm.CRITICAL);
      pm.addMsg("That wansn't so bad but don't do it again!", pm.NONCRITICAL);
      pm.addMsg("Second Critical message - you did something really bad - that's twice now - what the hell do you think you're doing?", pm.CRITICAL);
      pm.addMsg("Third Critical message.", pm.CRITICAL);
      pm.addMsg("Fourth Critical message.", pm.CRITICAL);
      pm.addMsg("Not so bad 2.", pm.NONCRITICAL);
      pm.addMsg("Not so bad 3.", pm.NONCRITICAL);
      pm.addMsg("Not so bad 4.", pm.NONCRITICAL);
      pm.addMsg("Fifth Critical message.", pm.CRITICAL);
    }
    else if (pm.getTitle() != null) pm.setTitle(null);
    else if (pm.getInfoMsg() != null) pm.setInfoMsg(null);
    else pm = null;

    theSessionState.setPasMessage(pm);

    if (pm != null) pm.setGenerate(true);

  }


  ///////////////////////////////////////////////////////////////////////////

  private void testActiveMessage()
  {
    ActiveMessage am = theSessionState.getActMessage();

    if (am == null)
    {
      setActiveMessageToAlert("Oh Yeah! I forgot to mention", ActiveMsgFactory.ISPAGECONFIRMCANCEL);
      am = theSessionState.getActMessage();
      am.setGenerate(true);
      am.setTitle("Just Testing - Active Message");
      am.setInfoMsg("Information active message. This message is for your information only. Please don't tell anyone you say this.");
      am.addMsg("AM. First Critical message - you did something really bad", am.CRITICAL);
      am.addMsg("AM. That wansn't so bad but don't do it again!", am.NONCRITICAL);
      am.addMsg("AM. Second Critical message - you did something really bad - that's twice now - what the hell do you think you're doing?", am.CRITICAL);
      am.addMsg("AM. Third Critical message.", am.CRITICAL);
      am.addMsg("AM. Fourth Critical message.", am.CRITICAL);
      am.addMsg("AM. Not so bad 2.", am.NONCRITICAL);
      am.addMsg("AM. Not so bad 3.", am.NONCRITICAL);
      am.addMsg("AM. Not so bad 4.", am.NONCRITICAL);
      am.addMsg("AM. Fifth Critical message.", am.CRITICAL);
    }
    else if (am.getTitle() != null)
    {
      am.setTitle(null);
      am.clearMessages();
    }
    else if (am.getInfoMsg() != null) am.setInfoMsg(null);
    else am = null;

    theSessionState.setActMessage(am);

    if (am != null) am.setGenerate(true);
  }

  //// SYNCADD
  // Added new button to popup DealNote window -- By BILLY 28May2002
  //// This adjusted method is moved to the PHC base class.
  /**
  public int populateDealNotesWin(int ind)
  {
    CSpDataObject theObj =(CSpDataObject) getModel(doDealNotesInfoModel.class);

    if (theObj.getNumRows() <= 0) return CSpPage.SKIP;


    // ALERT :: Commented this oout to not doing anything before we fixing the Data problem of the Bureau from Morty
    //          The Report content may contain some hidden char e.g. Char(0) ==> caused JavaScript problems.
    //getCurrNDPage().setDisplayFieldValue("Repeated2.stCreditBureauReport", new String( (String)(reports.get(ind)) ) );
    String tmpStr = theObj.getValue(ind, "dfDealNotesText").toString();

    tmpStr = convertToHtmString2(tmpStr);

    getCurrNDPage().setDisplayFieldValue("rptDealNoteWin.stNoteText1", new String(tmpStr));

    return CSpPage.PROCEED;
  }
  **/

}

