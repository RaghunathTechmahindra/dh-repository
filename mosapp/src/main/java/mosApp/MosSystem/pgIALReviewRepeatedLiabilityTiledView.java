package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgIALReviewRepeatedLiabilityTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedLiabilityTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doBorrowerEntryLiabilityModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getCbLiabilityType().setValue(CHILD_CBLIABILITYTYPE_RESET_VALUE);
		getTbliabTotalAmount().setValue(CHILD_TBLIABTOTALAMOUNT_RESET_VALUE);
		getTbliabMonyhlyPayment().setValue(CHILD_TBLIABMONYHLYPAYMENT_RESET_VALUE);
		getBtDeleteLiability().setValue(CHILD_BTDELETELIABILITY_RESET_VALUE);
		getStIndex1().setValue(CHILD_STINDEX1_RESET_VALUE);
		getHdLiabliltyBorrowerId().setValue(CHILD_HDLIABLILTYBORROWERID_RESET_VALUE);
		getHdLiabilityId().setValue(CHILD_HDLIABILITYID_RESET_VALUE);
		getCbliabIncudedInGDS().setValue(CHILD_CBLIABINCUDEDINGDS_RESET_VALUE);
		getCbliabIncudedInTDS().setValue(CHILD_CBLIABINCUDEDINTDS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_CBLIABILITYTYPE,ComboBox.class);
		registerChild(CHILD_TBLIABTOTALAMOUNT,TextField.class);
		registerChild(CHILD_TBLIABMONYHLYPAYMENT,TextField.class);
		registerChild(CHILD_BTDELETELIABILITY,Button.class);
		registerChild(CHILD_STINDEX1,StaticTextField.class);
		registerChild(CHILD_HDLIABLILTYBORROWERID,HiddenField.class);
		registerChild(CHILD_HDLIABILITYID,HiddenField.class);
		registerChild(CHILD_CBLIABINCUDEDINGDS,ComboBox.class);
		registerChild(CHILD_CBLIABINCUDEDINTDS,ComboBox.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoBorrowerEntryLiabilityModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;


		// The following code block was migrated from the RepeatedLiability_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("inc @RepeatedLiability "+sumofrowsForLiliability);
		sumofrowsForLiliability ++;


		/$int rownum = event.getRowIndex();

				Object index = new Integer(rownum + 1);
				CSpRepeated rep = (CSpRepeated)getCommonRepeated("RepeatedLiability");
				rep.setDisplayFieldValue("stIndex1", index);$/
		return;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_CBLIABILITYTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryLiabilityModel(),
				CHILD_CBLIABILITYTYPE,
				doBorrowerEntryLiabilityModel.FIELD_DFLIABILITYTYPEID,
				CHILD_CBLIABILITYTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbLiabilityTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBLIABTOTALAMOUNT))
		{
			TextField child = new TextField(this,
				getdoBorrowerEntryLiabilityModel(),
				CHILD_TBLIABTOTALAMOUNT,
				doBorrowerEntryLiabilityModel.FIELD_DFLIABILITYAMOUNT,
				CHILD_TBLIABTOTALAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBLIABMONYHLYPAYMENT))
		{
			TextField child = new TextField(this,
				getdoBorrowerEntryLiabilityModel(),
				CHILD_TBLIABMONYHLYPAYMENT,
				doBorrowerEntryLiabilityModel.FIELD_DFLIABILITYMONTHLYPAYMENT,
				CHILD_TBLIABMONYHLYPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETELIABILITY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETELIABILITY,
				CHILD_BTDELETELIABILITY,
				CHILD_BTDELETELIABILITY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STINDEX1))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINDEX1,
				CHILD_STINDEX1,
				CHILD_STINDEX1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDLIABLILTYBORROWERID))
		{
			HiddenField child = new HiddenField(this,
				getdoBorrowerEntryLiabilityModel(),
				CHILD_HDLIABLILTYBORROWERID,
				doBorrowerEntryLiabilityModel.FIELD_DFBORROWERID,
				CHILD_HDLIABLILTYBORROWERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDLIABILITYID))
		{
			HiddenField child = new HiddenField(this,
				getdoBorrowerEntryLiabilityModel(),
				CHILD_HDLIABILITYID,
				doBorrowerEntryLiabilityModel.FIELD_DFLIABILITYID,
				CHILD_HDLIABILITYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBLIABINCUDEDINGDS))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryLiabilityModel(),
				CHILD_CBLIABINCUDEDINGDS,
				doBorrowerEntryLiabilityModel.FIELD_DFLIABILITYINCLUDINGGDS,
				CHILD_CBLIABINCUDEDINGDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbliabIncudedInGDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBLIABINCUDEDINTDS))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryLiabilityModel(),
				CHILD_CBLIABINCUDEDINTDS,
				doBorrowerEntryLiabilityModel.FIELD_DFLIABILITYINCLUDINGTDS,
				CHILD_CBLIABINCUDEDINTDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbliabIncudedInTDSOptions);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbLiabilityType()
	{
		return (ComboBox)getChild(CHILD_CBLIABILITYTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbLiabilityTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbLiabilityTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object LIABILITYTYPE_LDESCRIPTION = results.getObject("LDESCRIPTION");
					Object LIABILITYTYPEID = results.getObject("LIABILITYTYPEID");

					String label = (LIABILITYTYPE_LDESCRIPTION == null?"":LIABILITYTYPE_LDESCRIPTION.toString());

					String value = (LIABILITYTYPEID == null?"":LIABILITYTYPEID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT LIABILITYTYPE.LDESCRIPTION, LIABILITYTYPE.LIABILITYTYPEID FROM LIABILITYTYPE";

	}



	/**
	 *
	 *
	 */
	public TextField getTbliabTotalAmount()
	{
		return (TextField)getChild(CHILD_TBLIABTOTALAMOUNT);
	}


	/**
	 *
	 *
	 */
	public TextField getTbliabMonyhlyPayment()
	{
		return (TextField)getChild(CHILD_TBLIABMONYHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteLiability()
	{
		return (Button)getChild(CHILD_BTDELETELIABILITY);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteLiabilityDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteLiability_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("@btDeleteLiability_onBeforeHtmlOutputEvent");
		try
		{
			CSpButton buttonHtml =(CSpButton) event.getSource();

			//CSpHtml.sendMessage("Changed2" + buttonHtml);
			String htmlstr = "<INPUT TYPE=IMAGE NAME=\"deleteAction(Liability," + sumofrowsForLiliability + ")\" VALUE=\"Delete\" SRC=\"/eNet_images/delete.gif\" ALIGN=TOP  width=52 height=15 alt=\"\" border=\"0\">";

			//CSpHtml.sendMessage("htmlstr " + htmlstr);
			buttonHtml.setHtmlText(htmlstr);
		}
		catch(Exception ex)
		{
			CSpHtml.sendMessage("Excp@html output " + ex.toString());
		}

		return;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIndex1()
	{
		return (StaticTextField)getChild(CHILD_STINDEX1);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdLiabliltyBorrowerId()
	{
		return (HiddenField)getChild(CHILD_HDLIABLILTYBORROWERID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdLiabilityId()
	{
		return (HiddenField)getChild(CHILD_HDLIABILITYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbliabIncudedInGDS()
	{
		return (ComboBox)getChild(CHILD_CBLIABINCUDEDINGDS);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbliabIncudedInTDS()
	{
		return (ComboBox)getChild(CHILD_CBLIABINCUDEDINTDS);
	}


	/**
	 *
	 *
	 */
	public doBorrowerEntryLiabilityModel getdoBorrowerEntryLiabilityModel()
	{
		if (doBorrowerEntryLiability == null)
			doBorrowerEntryLiability = (doBorrowerEntryLiabilityModel) getModel(doBorrowerEntryLiabilityModel.class);
		return doBorrowerEntryLiability;
	}


	/**
	 *
	 *
	 */
	public void setdoBorrowerEntryLiabilityModel(doBorrowerEntryLiabilityModel model)
	{
			doBorrowerEntryLiability = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_CBLIABILITYTYPE="cbLiabilityType";
	public static final String CHILD_CBLIABILITYTYPE_RESET_VALUE="";
	private CbLiabilityTypeOptionList cbLiabilityTypeOptions=new CbLiabilityTypeOptionList();
	public static final String CHILD_TBLIABTOTALAMOUNT="tbliabTotalAmount";
	public static final String CHILD_TBLIABTOTALAMOUNT_RESET_VALUE="";
	public static final String CHILD_TBLIABMONYHLYPAYMENT="tbliabMonyhlyPayment";
	public static final String CHILD_TBLIABMONYHLYPAYMENT_RESET_VALUE="";
	public static final String CHILD_BTDELETELIABILITY="btDeleteLiability";
	public static final String CHILD_BTDELETELIABILITY_RESET_VALUE="Delete";
	public static final String CHILD_STINDEX1="stIndex1";
	public static final String CHILD_STINDEX1_RESET_VALUE="";
	public static final String CHILD_HDLIABLILTYBORROWERID="hdLiabliltyBorrowerId";
	public static final String CHILD_HDLIABLILTYBORROWERID_RESET_VALUE="";
	public static final String CHILD_HDLIABILITYID="hdLiabilityId";
	public static final String CHILD_HDLIABILITYID_RESET_VALUE="";
	public static final String CHILD_CBLIABINCUDEDINGDS="cbliabIncudedInGDS";
	public static final String CHILD_CBLIABINCUDEDINGDS_RESET_VALUE="N";
	private OptionList cbliabIncudedInGDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	public static final String CHILD_CBLIABINCUDEDINTDS="cbliabIncudedInTDS";
	public static final String CHILD_CBLIABINCUDEDINTDS_RESET_VALUE="N";
	private OptionList cbliabIncudedInTDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doBorrowerEntryLiabilityModel doBorrowerEntryLiability=null;

}

