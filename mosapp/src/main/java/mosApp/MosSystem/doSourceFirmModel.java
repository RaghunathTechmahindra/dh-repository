package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doSourceFirmModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public String getDfSBPFirstName();


	/**
	 *
	 *
	 */
	public void setDfSBPFirstName(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPMidInit();


	/**
	 *
	 *
	 */
	public void setDfSBPMidInit(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPLastName();


	/**
	 *
	 *
	 */
	public void setDfSBPLastName(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPPhone();


	/**
	 *
	 *
	 */
	public void setDfSBPPhone(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPFax();


	/**
	 *
	 *
	 */
	public void setDfSBPFax(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPEmail();


	/**
	 *
	 *
	 */
	public void setDfSBPEmail(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPCity();


	/**
	 *
	 *
	 */
	public void setDfSBPCity(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPProvinceId();


	/**
	 *
	 *
	 */
	public void setDfSBPProvinceId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfSBP_FSA();


	/**
	 *
	 *
	 */
	public void setDfSBP_FSA(String value);


	/**
	 *
	 *
	 */
	public String getDfSBP_LDU();


	/**
	 *
	 *
	 */
	public void setDfSBP_LDU(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPAddrLine1();


	/**
	 *
	 *
	 */
	public void setDfSBPAddrLine1(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPAddrLine2();


	/**
	 *
	 *
	 */
	public void setDfSBPAddrLine2(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPPhoneExt();


	/**
	 *
	 *
	 */
	public void setDfSBPPhoneExt(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPShortName();


	/**
	 *
	 *
	 */
	public void setDfSBPShortName(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPStatusId();


	/**
	 *
	 *
	 */
	public void setDfSBPStatusId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPSystemTypeId();


	/**
	 *
	 *
	 */
	public void setDfSBPSystemTypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfSBPClientId();


	/**
	 *
	 *
	 */
	public void setDfSBPClientId(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPAlternativeId();


	/**
	 *
	 *
	 */
	public void setDfSBPAlternativeId(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPFirmName();


	/**
	 *
	 *
	 */
	public void setDfSBPFirmName(String value);

	/**
	 *
	 *
	 */
	public String getDfLicenseNumber();


	/**
	 *
	 *
	 */
	public void setDfLicenseNumber(String value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPFirmProfileId();


	/**
	 *
	 *
	 */
	public void setDfSBPFirmProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPUserProfileId();


	/**
	 *
	 *
	 */
	public void setDfSBPUserProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfSBPUserEffectiveDate();


	/**
	 *
	 *
	 */
	public void setDfSBPUserEffectiveDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfSFPStatusDesc();


	/**
	 *
	 *
	 */
	public void setDfSFPStatusDesc(String value);


	/**
	 *
	 *
	 */
	public String getDfSFPSystemTypeDesc();


	/**
	 *
	 *
	 */
	public void setDfSFPSystemTypeDesc(String value);


	/**
	 *
	 *
	 */
	public String getDfSFPProvinceName();


	/**
	 *
	 *
	 */
	public void setDfSFPProvinceName(String value);

  /**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPGroupProfileId();

	/**
	 *
	 *
	 */
	public void setDfSBPGroupProfileId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfSBPGroupEffectiveDate();


	/**
	 *
	 *
	 */
	public void setDfSBPGroupEffectiveDate(java.sql.Timestamp value);

  //---------------- FXLink Phase II ------------------//
  //--> Add new field ==> Firm System Code
  //--> By Billy 14Nov2003
  public String getDfSFPSystemCode();
	public void setDfSFPSystemCode(String value);
  //=====================================================

  //--DJ_PREFCOMMETHOD_CR--start--//
  public String getDfPreferredDeliveryMethod();
  public void setDfPreferredDeliveryMethod(String value);

  public void setDfPreferredDeliveryMethodId(java.math.BigDecimal value);
  public java.math.BigDecimal getDfPreferredDeliveryMethodId();
  //--DJ_PREFCOMMETHOD_CR--end--//
  public java.math.BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(java.math.BigDecimal id);
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFSBPFIRSTNAME="dfSBPFirstName";
	public static final String FIELD_DFSBPMIDINIT="dfSBPMidInit";
	public static final String FIELD_DFSBPLASTNAME="dfSBPLastName";
	public static final String FIELD_DFSBPPHONE="dfSBPPhone";
	public static final String FIELD_DFSBPFAX="dfSBPFax";
	public static final String FIELD_DFSBPEMAIL="dfSBPEmail";
	public static final String FIELD_DFSBPCITY="dfSBPCity";
	public static final String FIELD_DFSBPPROVINCEID="dfSBPProvinceId";
	public static final String FIELD_DFSBP_FSA="dfSBP_FSA";
	public static final String FIELD_DFSBP_LDU="dfSBP_LDU";
	public static final String FIELD_DFSBPADDRLINE1="dfSBPAddrLine1";
	public static final String FIELD_DFSBPADDRLINE2="dfSBPAddrLine2";
	public static final String FIELD_DFSBPPHONEEXT="dfSBPPhoneExt";
	public static final String FIELD_DFSBPSHORTNAME="dfSBPShortName";
	public static final String FIELD_DFSBPSTATUSID="dfSBPStatusId";
	public static final String FIELD_DFSBPSYSTEMTYPEID="dfSBPSystemTypeId";
	public static final String FIELD_DFSBPCLIENTID="dfSBPClientId";
	public static final String FIELD_DFSBPALTERNATIVEID="dfSBPAlternativeId";
	public static final String FIELD_DFSBPFIRMNAME="dfSBPFirmName";
	public static final String FIELD_DFLICENSENUMBER="dfLicenseNumber";
	public static final String FIELD_DFSBPFIRMPROFILEID="dfSBPFirmProfileId";
	public static final String FIELD_DFSBPUSERPROFILEID="dfSBPUserProfileId";
	public static final String FIELD_DFSBPUSEREFFECTIVEDATE="dfSBPUserEffectiveDate";
	public static final String FIELD_DFSFPSTATUSDESC="dfSFPStatusDesc";
	public static final String FIELD_DFSFPSYSTEMTYPEDESC="dfSFPSystemTypeDesc";
	public static final String FIELD_DFSFPPROVINCENAME="dfSFPProvinceName";
	//=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Added new fields to handle SourceFirm to Group association
  //--> By Billy 19Aug2003
	public static final String FIELD_DFSBPGROUPPROFILEID="dfSBPGroupProfileId";
	public static final String FIELD_DFSBPGROUPEFFECTIVEDATE="dfSBPGroupEffectiveDate";
	//=================================================

  //---------------- FXLink Phase II ------------------//
  //--> Add new field ==> Firm System Code
  //--> By Billy 14Nov2003
  public static final String FIELD_DFSFPSYSTEMCODE="dfSFPSystemCode";
  //====================================================

  //--DJ_PREFCOMMETHOD_CR--start--//
	public static final String FIELD_DFPREFERREDDELIVERYMETHOD="dfPrefDeliveryMethod";
	public static final String FIELD_DFPREFERREDDELIVERYMETHODID="dfPrefDeliveryMethodId";
  //--DJ_PREFCOMMETHOD_CR--end--//
	 public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

