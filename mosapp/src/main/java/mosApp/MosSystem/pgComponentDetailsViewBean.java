package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>
 * Title: pgComponentDetailsViewBean
 * </p>
 * <p>
 * Description: ViewBean Class for Component Details screen with Jato Framework</p>
 * </p>
 * 
 * @author: MCM Impl Team.
 * @version 1.0 June 6 2008
 * 
 * @version 1.1 <br>
 * Date: June 17 2008<br>
 * @author: MCM Implementation Team<br>
 * Change:  <br>
 *  added implementation for handleBtSubmitRequest<br> 
 *
 * @version 1.2 <br>
 * Date: June ?? 2008<br>
 * @author: MCM Implementation Team<br>
 * Change:  <br>
 *  added Component Loc section - XS_2.33 <br> 
 *  
 * @version 1.3 <br>
 * Date: June 22 2008<br>
 * @author: MCM Implementation Team<br>
 * Change:  <br>
 *  added ComponentMortgage section - XS_2.27<br> 
 *  
 * @version 1.4 <br>
 * Date: June 02 2008<br>
 * @author: MCM Impl Team<br>
 * Change:  <br>
 * XS_2.25 Added Deal Summary and Component Summary Section fields.
 * 
 * @version 1.5 July 3, 2008: adding XS_2.38 (Overdraft)
 * @version 1.6 JUly 08,2008 Bug Fix artf738619: Updated biginDisplay() method, moved super.beginDisplay(event) to end of the method
 * @version 1.7 July 4, 2008: adding XS_2.37 (Loan)
 * @version 1.8 July 4, 2008: adding XS_2.36 (Credit Card)
 * @version 1.9 added Add Component section - XS_2.26 Modified
 * @version 1.10 July 15, 2008 modified handleBtCancel/CurrentCancelRequest for XS 2.31
 * @version 1.11 July 21, 2008 add getDisplayURL() for XS 2.54
 * @version 1.12 July 25, 2008 added MAXCOMPTILE_SIZE = 10 for artf750856 
 * @version 1.13 July 25, 2008 fixed artf751260 added 2nd parameter into prehandlerProtocol to skip save
 * @version 1.14 July 30, 2008 fixed artf751756 changed handleActMessageOK to call handleCustomActMessageOk
 * @version 1.15 Oct 20,2008 Bugfix FXP23027 : Added missing code for preivious link.
 */
public class pgComponentDetailsViewBean extends ExpressViewBeanBase {

    // MCM Team July 25, 2008 STARTS
    // this is temporaly value.
    // BA confirmation is Required
    public static final int MAXCOMPTILE_SIZE = 10;
    // MCM Team July 25, 2008 ENDS

    // The logger
    private final static Log _log = 
        LogFactory.getLog(pgComponentDetailsViewBean.class);

    /**
     * <p>constructor</p>
     */
    public pgComponentDetailsViewBean() {
        super(PAGE_NAME);
        setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
        registerChildren();
        initialize();
    }


    /**
     * <p>initializer</p>
     */
    protected void initialize() {
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Child manipulation methods
    ////////////////////////////////////////////////////////////////////////////////
    /**
     * <p>createChild</p>
     * <p>Description: createChild method for Jato Framework</p>
     * 
     * @param name:String - child name
     * @return View 
     * @version 1.4 03-July-2008 XS_2.25 Added nesseccery changes for the new fields :
     * Mi Premium,Product Name,combinedLTV, Tax Escrow ,Total Amount,Unallocated Amount,LOB.
     */
    protected View createChild(String name) {
        View superReturn = super.createChild(name);
        if (superReturn != null) {
            return superReturn;
        } else if (name.equals(CHILD_TBDEALID)) {
            TextField child = new TextField(this,
                getDefaultModel(),
                CHILD_TBDEALID,
                CHILD_TBDEALID,
                CHILD_TBDEALID_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_CBPAGENAMES)) {
            ComboBox child = new ComboBox( this,
                    getDefaultModel(),
                    CHILD_CBPAGENAMES,
                    CHILD_CBPAGENAMES,
                    CHILD_CBPAGENAMES_RESET_VALUE,
                    null);
          //--Release2.1--//
          ////Modified to set NonSelected Label as a CHILD_CBPAGENAMES_NONSELECTED_LABEL
                ////child.setLabelForNoneSelected("Choose a Page");
          child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
                child.setOptions(cbPageNamesOptions);
                return child;
        } else if (name.equals(CHILD_BTACTMSG)) {
            Button child = new Button(this, getDefaultModel(), CHILD_BTACTMSG,
                    CHILD_BTACTMSG, CHILD_BTACTMSG_RESET_VALUE,
                    new CommandFieldDescriptor(
                            ActMessageCommand.COMMAND_DESCRIPTOR));

            return child;
        } else if (name.equals(CHILD_TOGGLELANGUAGEHREF)) {
            HREF child = new HREF(this, getDefaultModel(),
                    CHILD_TOGGLELANGUAGEHREF, CHILD_TOGGLELANGUAGEHREF,
                    CHILD_TOGGLELANGUAGEHREF_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTPROCEED)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTPROCEED,
                CHILD_BTPROCEED,
                CHILD_BTPROCEED_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF1)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF1,
                CHILD_HREF1,
                CHILD_HREF1_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF2)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF2,
                CHILD_HREF2,
                CHILD_HREF2_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF3)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF3,
                CHILD_HREF3,
                CHILD_HREF3_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF4)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF4,
                CHILD_HREF4,
                CHILD_HREF4_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF5)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF5,
                CHILD_HREF5,
                CHILD_HREF5_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF6)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF6,
                CHILD_HREF6,
                CHILD_HREF6_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF7)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF7,
                CHILD_HREF7,
                CHILD_HREF7_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF8)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF8,
                CHILD_HREF8,
                CHILD_HREF8_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF9)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF9,
                CHILD_HREF9,
                CHILD_HREF9_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_HREF10)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_HREF10,
                CHILD_HREF10,
                CHILD_HREF10_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_STPAGELABEL)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPAGELABEL,
                CHILD_STPAGELABEL,
                CHILD_STPAGELABEL_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STCOMPANYNAME)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STCOMPANYNAME,
                CHILD_STCOMPANYNAME,
                CHILD_STCOMPANYNAME_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STTODAYDATE)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STTODAYDATE,
                CHILD_STTODAYDATE,
                CHILD_STTODAYDATE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STUSERNAMETITLE)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STUSERNAMETITLE,
                CHILD_STUSERNAMETITLE,
                CHILD_STUSERNAMETITLE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STDEALID)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STDEALID,
                doUWDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
                CHILD_STDEALID_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STBORRFIRSTNAME)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(), CHILD_STBORRFIRSTNAME,
                CHILD_STBORRFIRSTNAME, CHILD_STBORRFIRSTNAME_RESET_VALUE,
                null);

            return child;
        } else  if (name.equals(CHILD_STDEALSTATUS)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STDEALSTATUS,
                doUWDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
                CHILD_STDEALSTATUS_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STDEALSTATUSDATE)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STDEALSTATUSDATE,
                doUWDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
                CHILD_STDEALSTATUSDATE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STSOURCEFIRM)){
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STSOURCEFIRM,
                doUWDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
                CHILD_STSOURCEFIRM_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STSOURCE)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STSOURCE,
                doUWDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
                CHILD_STSOURCE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STUNDERWRITERLASTNAME)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STUNDERWRITERLASTNAME,
                doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERLASTNAME,
                CHILD_STUNDERWRITERLASTNAME_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STUNDERWRITERFIRSTNAME)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STUNDERWRITERLASTNAME,
                doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERFIRSTNAME,
                CHILD_STUNDERWRITERLASTNAME_RESET_VALUE,
                    null);
                return child;
        } else if (name.equals(CHILD_STUNDERWRITERMIDDLEINITIAL)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STUNDERWRITERLASTNAME,
                doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERMIDDLEINITIAL,
                CHILD_STUNDERWRITERLASTNAME_RESET_VALUE,
                    null);
                return child;
        } else if (name.equals(CHILD_STDEALTYPE)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STDEALTYPE,
                doUWDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
                CHILD_STDEALTYPE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STDEALPURPOSE)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STDEALPURPOSE,
                doUWDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
                CHILD_STDEALPURPOSE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPURCHASEPRICE)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STPURCHASEPRICE,
                doUWDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
                CHILD_STPURCHASEPRICE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMTTERM)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STPMTTERM,
                doUWDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
                CHILD_STPMTTERM_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STESTCLOSINGDATE)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STESTCLOSINGDATE,
                doUWDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
                CHILD_STESTCLOSINGDATE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STVIEWONLYTAG)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STVIEWONLYTAG,
                CHILD_STVIEWONLYTAG,
                CHILD_STVIEWONLYTAG_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_BTCHECKRULE)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTCHECKRULE,
                CHILD_BTCHECKRULE,
                CHILD_BTCHECKRULE_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTRECALC)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTRECALC,
                CHILD_BTRECALC,
                CHILD_BTRECALC_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTSUBMIT)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTSUBMIT,
                CHILD_BTSUBMIT,
                CHILD_BTSUBMIT_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_BTCANCEL)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTCANCEL,
                CHILD_BTCANCEL,
                CHILD_BTCANCEL_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTCURRENTCANCEL)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTCURRENTCANCEL,
                CHILD_BTCURRENTCANCEL,
                CHILD_BTCANCEL_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTOK)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTOK,
                CHILD_BTOK,
                CHILD_BTCANCEL_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTWORKQUEUELINK)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTWORKQUEUELINK,
                CHILD_BTWORKQUEUELINK,
                CHILD_BTWORKQUEUELINK_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_CHANGEPASSWORDHREF)) {
            HREF child = new HREF(
                this,
                getDefaultModel(),
                CHILD_CHANGEPASSWORDHREF,
                CHILD_CHANGEPASSWORDHREF,
                CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTTOOLHISTORY)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOOLHISTORY,
                CHILD_BTTOOLHISTORY,
                CHILD_BTTOOLHISTORY_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTTOONOTES)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOONOTES,
                CHILD_BTTOONOTES,
                CHILD_BTTOONOTES_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTTOOLSEARCH)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOOLSEARCH,
                CHILD_BTTOOLSEARCH,
                CHILD_BTTOOLSEARCH_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTTOOLHELP)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOOLHELP,
                CHILD_BTTOOLHELP,
                CHILD_BTTOOLHELP_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_BTTOOLLOG)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTTOOLLOG,
                CHILD_BTTOOLLOG,
                CHILD_BTTOOLLOG_RESET_VALUE,
                null);
                return child;
        } else if (name.equals(CHILD_STERRORFLAG)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STERRORFLAG,
                CHILD_STERRORFLAG,
                CHILD_STERRORFLAG_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_DETECTALERTTASKS)) {
            HiddenField child = new HiddenField(this,
                getDefaultModel(),
                CHILD_DETECTALERTTASKS,
                CHILD_DETECTALERTTASKS,
                CHILD_DETECTALERTTASKS_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STTOTALLOANAMOUNT)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STTOTALLOANAMOUNT,
                doUWDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
                CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAPPLICATIONDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STAPPLICATIONDATE,
                    doUWDealSummarySnapShotModel.FIELD_DFAPPLICATIONDATE,
                    CHILD_STAPPLICATIONDATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTCANCEL)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTCANCEL,
                CHILD_BTCANCEL,
                CHILD_BTCANCEL_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_BTPREVTASKPAGE)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTPREVTASKPAGE,
                CHILD_BTPREVTASKPAGE,
                CHILD_BTPREVTASKPAGE_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_STPREVTASKPAGELABEL)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPREVTASKPAGELABEL,
                CHILD_STPREVTASKPAGELABEL,
                CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_BTNEXTTASKPAGE)){
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTNEXTTASKPAGE,
                CHILD_BTNEXTTASKPAGE,
                CHILD_BTNEXTTASKPAGE_RESET_VALUE,
                null);
                return child;

        } else if (name.equals(CHILD_STNEXTTASKPAGELABEL)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STNEXTTASKPAGELABEL,
                CHILD_STNEXTTASKPAGELABEL,
                CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STTASKNAME)){
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STTASKNAME,
                CHILD_STTASKNAME,
                CHILD_STTASKNAME_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_SESSIONUSERID)){
            HiddenField child = new HiddenField(this,
                getDefaultModel(),
                CHILD_SESSIONUSERID,
                CHILD_SESSIONUSERID,
                CHILD_SESSIONUSERID_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMGENERATE)){
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMGENERATE,
                CHILD_STPMGENERATE,
                CHILD_STPMGENERATE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMHASTITLE)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMHASTITLE,
                CHILD_STPMHASTITLE,
                CHILD_STPMHASTITLE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMHASINFO)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMHASINFO,
                CHILD_STPMHASINFO,
                CHILD_STPMHASINFO_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMHASTABLE)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMHASTABLE,
                CHILD_STPMHASTABLE,
                CHILD_STPMHASTABLE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMHASOK)){
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMHASOK,
                CHILD_STPMHASOK,
                CHILD_STPMHASOK_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMTITLE)){
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMTITLE,
                CHILD_STPMTITLE,
                CHILD_STPMTITLE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMINFOMSG)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMINFOMSG,
                CHILD_STPMINFOMSG,
                CHILD_STPMINFOMSG_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMONOK)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMONOK,
                CHILD_STPMONOK,
                CHILD_STPMONOK_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMMSGS)){
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMMSGS,
                CHILD_STPMMSGS,
                CHILD_STPMMSGS_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STPMMSGTYPES)){
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STPMMSGTYPES,
                CHILD_STPMMSGTYPES,
                CHILD_STPMMSGTYPES_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMGENERATE)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMGENERATE,
                CHILD_STAMGENERATE,
                CHILD_STAMGENERATE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMHASTITLE)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMHASTITLE,
                CHILD_STAMHASTITLE,
                CHILD_STAMHASTITLE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMHASINFO)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMHASINFO,
                CHILD_STAMHASINFO,
                CHILD_STAMHASINFO_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMHASTABLE)){
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMHASTABLE,
                CHILD_STAMHASTABLE,
                CHILD_STAMHASTABLE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMTITLE)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMTITLE,
                CHILD_STAMTITLE,
                CHILD_STAMTITLE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMINFOMSG)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMINFOMSG,
                CHILD_STAMINFOMSG,
                CHILD_STAMINFOMSG_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMMSGS)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMMSGS,
                CHILD_STAMMSGS,
                CHILD_STAMMSGS_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMMSGTYPES)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMMSGTYPES,
                CHILD_STAMMSGTYPES,
                CHILD_STAMMSGTYPES_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMDIALOGMSG)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMDIALOGMSG,
                CHILD_STAMDIALOGMSG,
                CHILD_STAMDIALOGMSG_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STAMBUTTONSHTML)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STAMBUTTONSHTML,
                CHILD_STAMBUTTONSHTML,
                CHILD_STAMBUTTONSHTML_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STSPECIALFEATURE)) {
            StaticTextField child = new StaticTextField(this,
                getdoUWDealSummarySnapShotModel(),
                CHILD_STSPECIALFEATURE,
                doUWDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
                CHILD_STSPECIALFEATURE_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STVALSDATA)) {
            StaticTextField child = new StaticTextField(this,
                getDefaultModel(),
                CHILD_STVALSDATA,
                CHILD_STVALSDATA,
                CHILD_STVALSDATA_RESET_VALUE,
                null);
            return child;
        } else if (name.equals(CHILD_STSERVICINGMORTGAGENUMBER)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STSERVICINGMORTGAGENUMBER,
                    doUWDealSummarySnapShotModel.FIELD_DFSERVICINGMORTGAGENUMBER,
                    CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STCCAPS)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STCCAPS, CHILD_STCCAPS,
                    CHILD_STCCAPS_RESET_VALUE, null);
            return child;

        // ***************MCM Impl team changes stars - XS 2.27 ******************* //
        } else if (name.equals(CHILD_TILEDMTGCOMP)) {
            pgComponentDetailsMTGTiledView child = new pgComponentDetailsMTGTiledView(this,
                    CHILD_TILEDMTGCOMP);
            return child;
        }       
        // ***************MCM Impl team changes ends - XS 2.27 ******************* //
        
        // ***************MCM Impl team changes starts - XS 2.33 ******************* //
        else if (name.equals(CHILD_REPEATEDLOC)) {
          pgComponentDetailsLOCTiledView child = new pgComponentDetailsLOCTiledView(
                  this, CHILD_REPEATEDLOC);
          return child;
        } 
        // ***************MCM Impl team changes ends - XS 2.33 ******************* //

        // ***************MCM Impl team changes stars - XS 2.38 ******************* //
        else if (name.equals(CHILD_REPEATEDOVERDRAFT)) {
            pgComponentDetailsOverdraftTiledView child 
                = new pgComponentDetailsOverdraftTiledView(this, CHILD_REPEATEDOVERDRAFT);
            return child;
        }       
        // ***************MCM Impl team changes ends - XS 2.38 ******************* //


        // ***************MCM Impl team changes stars - XS 2.36 ******************* //
        else if (name.equals(CHILD_TILEDCCCOMP)) {
            pgComponentCreditCardTiledView child = new pgComponentCreditCardTiledView(this,
                    CHILD_TILEDCCCOMP);
            return child;
        }       
    // ***************MCM Impl team changes ends - XS 2.36 ******************* //

        /***************MCM Impl team changes starts - XS_2.28*******************/
        else if (name.equals(CHILD_STISPAGEEDITABLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), 
                    CHILD_STISPAGEEDITABLE,
                    CHILD_STISPAGEEDITABLE, 
                    CHILD_STISPAGEEDITABLE_RESET_VALUE,
                    null);
            return child;
        } 
        else if (name.equals(CHILD_STLENDERPROFILEID)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), 
                    CHILD_STLENDERPROFILEID,
                    CHILD_STLENDERPROFILEID, 
                    CHILD_STLENDERPROFILEID_RESET_VALUE,
                    null);
            return child;
        } 
        /***************MCM Impl team changes ends - XS_2.28*********************/
        
        /***************MCM Impl Team XS_2.25 changes starts*********************/
        else if (name.equals(CHILD_STLOB)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STLOB,
                    doUWDealSummarySnapShotModel.FIELD_DFLOB,
                    CHILD_STLOB_RESET_VALUE, null);
            return child;
        }
        
        else if (name.equals(CHILD_STPRODUCTNAME)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STPRODUCTNAME,
                    doUWDealSummarySnapShotModel.FIELD_DFPRODUCTNAME,
                    CHILD_STPRODUCTNAME_RESET_VALUE, null);
            return child;
        }
        
        else if (name.equals(CHILD_STMIPREMIUM)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STMIPREMIUM,
                    doUWDealSummarySnapShotModel.FIELD_DFMIPREMIUM,
                    CHILD_STMIPREMIUM_RESET_VALUE, null);
            return child;
        }
        
        else if (name.equals(CHILD_STTAXESCROW)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STTAXESCROW,
                    doUWDealSummarySnapShotModel.FIELD_DFTAXESCROW,
                    CHILD_STTAXESCROW_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDLTV)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STCOMBINEDLTV,
                    doUWDealSummarySnapShotModel.FIELD_DFCOMBINEDLTV,
                    CHILD_STCOMBINEDLTV_RESET_VALUE, null);
            return child;
        }
        
        else if (name.equals(CHILD_STTOTALAMOUNT)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoComponentSummaryModel(),
                    CHILD_STTOTALAMOUNT,
                    doComponentSummaryModel.FIELD_DFTOTALAMOUNT,
                    CHILD_STTOTALAMOUNT_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STTOTALCASHBACKAMOUNT)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoComponentSummaryModel(),
                    CHILD_STTOTALCASHBACKAMOUNT,
                    doComponentSummaryModel.FIELD_DFTOTALCASHBACKAMOUNT,
                    CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STUNALLOCATEDAMOUNT)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoComponentSummaryModel(),
                    CHILD_STUNALLOCATEDAMOUNT,
                    doComponentSummaryModel.FIELD_DFUNALLOCATEDAMOUNT,
                    CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE, null);
            return child;
        }

        /***************MCM Impl Team XS_2.25 changes ends*********************/
        
        // ***************MCM Impl team changes starts - XS 2.37 ******************* //
        else if (name.equals(CHILD_REPEATEDLOAN)) {
          pgComponentDetailsLoanTiledView child = new pgComponentDetailsLoanTiledView(
                  this, CHILD_REPEATEDLOAN);
          return child;
        } 
        // ***************MCM Impl team changes ends - XS 2.37 ******************* //
        
        /***************MCM Impl Team XS_2.26 changes starts******************/
        else if (name.equals(CHILD_CBCOMPONENTTYPE)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBCOMPONENTTYPE,
                    CHILD_CBCOMPONENTTYPE,
                    CHILD_CBCOMPONENTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbComponentTypeOptions);
            return child;
        } 
        else if (name.equals(CHILD_CBPRODUCTTYPE)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBPRODUCTTYPE,
                    CHILD_CBPRODUCTTYPE,
                    CHILD_CBPRODUCTTYPE_RESET_VALUE, null);
            //child.setLabelForNoneSelected("");
            //child.setOptions(cbProductTypeOptions);
            return child;
        }
        else if (name.equals(CHILD_BTADDCOMPONENT)) {
            Button child = new Button(
                this,
                getDefaultModel(),
                CHILD_BTADDCOMPONENT,
                CHILD_BTADDCOMPONENT,
                CHILD_BTADDCOMPONENT_RESET_VALUE,
                null);
                return child;
        }
        /***************MCM Impl Team XS_2.26 changes ends*******************/
        //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - start.
        else if (name.equals(CHILD_TXWINDOWSCROLLPOSITION)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXWINDOWSCROLLPOSITION, CHILD_TXWINDOWSCROLLPOSITION,
					CHILD_TXWINDOWSCROLLPOSITION_RESET_VALUE, null);
			return child;
		}
        // FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - end.
        else throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }

    /**
     * <p>resetChildren</p>
     * <p>Description: resetChildren method for Jato Framework</p>
     * 
     * @param 
     * @return
     * @version 1.4 03-July-2008 XS_2.25 Added nesseccery changes for the new fields :
     * Mi Premium,Product Name,combinedLTV, Tax Escrow ,Total Amount,Unallocated Amount,LOB.
     */
    public void resetChildren()
    {
        super.resetChildren();
        getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
        getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
        getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
        getHref1().setValue(CHILD_HREF1_RESET_VALUE);
        getHref2().setValue(CHILD_HREF2_RESET_VALUE);
        getHref3().setValue(CHILD_HREF3_RESET_VALUE);
        getHref4().setValue(CHILD_HREF4_RESET_VALUE);
        getHref5().setValue(CHILD_HREF5_RESET_VALUE);
        getHref6().setValue(CHILD_HREF6_RESET_VALUE);
        getHref7().setValue(CHILD_HREF7_RESET_VALUE);
        getHref8().setValue(CHILD_HREF8_RESET_VALUE);
        getHref9().setValue(CHILD_HREF9_RESET_VALUE);
        getHref10().setValue(CHILD_HREF10_RESET_VALUE);
        getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
        getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
        getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
        getStUnderWriterLastName().setValue(CHILD_STUNDERWRITERLASTNAME_RESET_VALUE);
        getStUnderWriterFirstName().setValue(CHILD_STUNDERWRITERFIRSTNAME_RESET_VALUE);
        getStUnderWriterMiddleInitial().setValue(CHILD_STUNDERWRITERMIDDLEINITIAL_RESET_VALUE);
        getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
        getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
        getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
        getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
        getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
        getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
        getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
        getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
        getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
        getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
        getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
        getStApplicationDate().setValue(CHILD_STAPPLICATIONDATE_RESET_VALUE);
        getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
        getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
        getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
        getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
        getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
        getBtCheckRule().setValue(CHILD_BTCHECKRULE_RESET_VALUE);
        getBtRecalc().setValue(CHILD_BTRECALC_RESET_VALUE);
        getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
        getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
        getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
        getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
        getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
        getBtToolHelp().setValue(CHILD_BTTOOLHELP_RESET_VALUE);
        getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
        getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
        getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
        getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
        getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
        getBtCurrentCancel().setValue(CHILD_BTCURRENTCANCEL_RESET_VALUE);
        getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
        getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
        getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
        getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
        getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
        getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
        getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
        getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
        getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
        getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
        getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
        getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
        getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
        getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
        getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
        getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
        getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
        getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
        getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
        getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
        getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
        getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
        getStVALSData().setValue(CHILD_STVALSDATA_RESET_VALUE);
        getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
        getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
        getStServicingMortgageNumber().setValue(
                CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE);
        getStCCAPS().setValue(CHILD_STCCAPS_RESET_VALUE);

        //***************MCM Impl team changes starts - XS 2.33 ******************* //
        getRepeatedLOC().resetChildren();
        //***************MCM Impl team changes ends - XS 2.33 ******************* //

        //***************MCM Impl team changes starts - XS 2.27 ******************* //
        getTiledMtgComp().resetChildren();
        //***************MCM Impl team changes ends - XS 2.27 ******************* //
        
        //***************MCM Impl team changes starts - XS 2.36 ******************* //
        getTiledCrecitCardComp().resetChildren();
        //***************MCM Impl team changes ends - XS 2.36 ******************* //
        
        /***************MCM Impl team changes starts - XS_2.28*******************/
        getStIsPageEditable().setValue(CHILD_STISPAGEEDITABLE_RESET_VALUE);
        getStLenderProfileId().setValue(CHILD_STLENDERPROFILEID_RESET_VALUE);
        /***************MCM Impl team changes ends - XS_2.28*********************/
        
        /***********************MCM Impl Team XS_2.25 changes starts *****************/
        getStProductName().setValue(CHILD_STPRODUCTNAME_RESET_VALUE);
        getStMIPremium().setValue(CHILD_STMIPREMIUM_RESET_VALUE);
        getStTaxEscrow().setValue(CHILD_STTAXESCROW_RESET_VALUE);
        getStCombinedLTV().setValue(CHILD_STCOMBINEDLTV_RESET_VALUE);
        getStTotalAmount().setValue(CHILD_STTOTALAMOUNT_RESET_VALUE);
        getStUnAllocatedAmount().setValue(CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE);
        getStTotalCashBackAmount().setValue(CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE);
        /***********************MCM Impl Team XS_2.25 changes ends ******************/
        
        //***************MCM Impl team changes starts - XS 2.37 ******************* //
        getRepeatedLoan().resetChildren();
        //***************MCM Impl team changes ends - XS 2.37 ******************* //
        
        //***************MCM Impl team changes starts - XS 2.26 ******************* //
        getCbComponentType().setValue(CHILD_CBCOMPONENTTYPE_RESET_VALUE);
        getCbProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);
        getBtAddComponent().setValue(CHILD_BTADDCOMPONENT_RESET_VALUE);
        //***************MCM Impl team changes ends - XS 2.26 ******************* //
        
        //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - start.
        getTxWindowScrollPosition().setValue(CHILD_TXWINDOWSCROLLPOSITION_RESET_VALUE);
        //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - end.
    }

    /**
     * <p>registerChildren</p>
     * <p>Description: registerChildren method for Jato Framework</p>
     * 
     * @param 
     * @return
     * @version 1.4 03-July-2008 XS_2.25 Added nesseccery changes for the new fields :
     * Mi Premium,Product Name,combinedLTV, Tax Escrow ,Total Amount,Unallocated Amount,LOB.
     */
    protected void registerChildren()
    {
        super.registerChildren();
        registerChild(CHILD_TBDEALID,TextField.class);
        registerChild(CHILD_CBPAGENAMES,ComboBox.class);
        registerChild(CHILD_BTPROCEED,Button.class);
        registerChild(CHILD_HREF1,HREF.class);
        registerChild(CHILD_HREF2,HREF.class);
        registerChild(CHILD_HREF3,HREF.class);
        registerChild(CHILD_HREF4,HREF.class);
        registerChild(CHILD_HREF5,HREF.class);
        registerChild(CHILD_HREF6,HREF.class);
        registerChild(CHILD_HREF7,HREF.class);
        registerChild(CHILD_HREF8,HREF.class);
        registerChild(CHILD_HREF9,HREF.class);
        registerChild(CHILD_HREF10,HREF.class);
        registerChild(CHILD_STPAGELABEL,StaticTextField.class);
        registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
        registerChild(CHILD_STTODAYDATE,StaticTextField.class);
        registerChild(CHILD_STUNDERWRITERLASTNAME,StaticTextField.class);
        registerChild(CHILD_STUNDERWRITERFIRSTNAME,StaticTextField.class);
        registerChild(CHILD_STUNDERWRITERMIDDLEINITIAL,StaticTextField.class);
        registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
        registerChild(CHILD_STDEALID,StaticTextField.class);
        registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
        registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
        registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
        registerChild(CHILD_STSOURCE,StaticTextField.class);
        registerChild(CHILD_STLOB,StaticTextField.class);
        registerChild(CHILD_STDEALTYPE,StaticTextField.class);
        registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
        registerChild(CHILD_STPURCHASEPRICE,StaticTextField.class);
        registerChild(CHILD_STPMTTERM,StaticTextField.class);
        registerChild(CHILD_STAPPLICATIONDATE, StaticTextField.class);
        registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
        registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
        registerChild(CHILD_BTOK,Button.class);
        registerChild(CHILD_BTSUBMIT,Button.class);
        registerChild(CHILD_BTCHECKRULE,Button.class);
        registerChild(CHILD_BTRECALC,Button.class);
        registerChild(CHILD_BTWORKQUEUELINK,Button.class);
        registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
        registerChild(CHILD_BTTOOLHISTORY,Button.class);
        registerChild(CHILD_BTTOONOTES,Button.class);
        registerChild(CHILD_BTTOOLSEARCH,Button.class);
        registerChild(CHILD_BTTOOLHELP,Button.class);
        registerChild(CHILD_BTTOOLLOG,Button.class);
        registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);
        registerChild(CHILD_STERRORFLAG,StaticTextField.class);
        registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
        registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
        registerChild(CHILD_BTCANCEL,Button.class);
        registerChild(CHILD_BTCURRENTCANCEL,Button.class);
        registerChild(CHILD_BTPREVTASKPAGE,Button.class);
        registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
        registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
        registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
        registerChild(CHILD_STTASKNAME,StaticTextField.class);
        registerChild(CHILD_SESSIONUSERID,HiddenField.class);
        registerChild(CHILD_STPMGENERATE,StaticTextField.class);
        registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
        registerChild(CHILD_STPMHASINFO,StaticTextField.class);
        registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
        registerChild(CHILD_STPMHASOK,StaticTextField.class);
        registerChild(CHILD_STPMTITLE,StaticTextField.class);
        registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
        registerChild(CHILD_STPMONOK,StaticTextField.class);
        registerChild(CHILD_STPMMSGS,StaticTextField.class);
        registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
        registerChild(CHILD_STAMGENERATE,StaticTextField.class);
        registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
        registerChild(CHILD_STAMHASINFO,StaticTextField.class);
        registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
        registerChild(CHILD_STAMTITLE,StaticTextField.class);
        registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
        registerChild(CHILD_STAMMSGS,StaticTextField.class);
        registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
        registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
        registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
        registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
        registerChild(CHILD_STVALSDATA,StaticTextField.class);
        registerChild(CHILD_STSERVICINGMORTGAGENUMBER, StaticTextField.class);
        registerChild(CHILD_STCCAPS, StaticTextField.class);
        registerChild(CHILD_BTACTMSG, Button.class);

        //***************MCM Impl team changes ends - XS 2.33 ******************* //
        registerChild(CHILD_REPEATEDLOC, pgComponentDetailsLOCTiledView.class);
        //***************MCM Impl team changes ends - XS 2.33 ******************* //

        //***************MCM Impl team changes ends - XS 2.27 ******************* //
        registerChild(CHILD_TILEDMTGCOMP, pgComponentDetailsMTGTiledView.class);
        //***************MCM Impl team changes ends - XS 2.27 ******************* //

        //***************MCM Impl team changes ends - XS 2.38 ******************* //
        registerChild(CHILD_REPEATEDOVERDRAFT, pgComponentDetailsOverdraftTiledView.class);
        //***************MCM Impl team changes ends - XS 2.38 ******************* //

        /***************MCM Impl team changes starts - XS_2.28*******************/
        registerChild(CHILD_STISPAGEEDITABLE, StaticTextField.class );
        registerChild(CHILD_STLENDERPROFILEID, StaticTextField.class);
        /***************MCM Impl team changes ends - XS_2.28*********************/
        //***************MCM Impl team changes ends - XS 2.36 ******************* //
        registerChild(CHILD_TILEDCCCOMP, pgComponentCreditCardTiledView.class);
        //***************MCM Impl team changes ends - XS 2.36 ******************* //
        
        /***************MCM Impl team changes starts - XS_2.25*******************/
        registerChild(CHILD_STPRODUCTNAME, StaticTextField.class );
        registerChild(CHILD_STMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_STTAXESCROW, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDLTV, StaticTextField.class);
        registerChild(CHILD_STTOTALAMOUNT, StaticTextField.class);
        registerChild(CHILD_STUNALLOCATEDAMOUNT, StaticTextField.class);
        registerChild(CHILD_STTOTALCASHBACKAMOUNT, StaticTextField.class);
        /***************MCM Impl team changes ends - XS_2.25*********************/
        
        //***************MCM Impl team changes ends - XS 2.37 ******************* //
        registerChild(CHILD_REPEATEDLOAN, pgComponentDetailsLoanTiledView.class);
        //***************MCM Impl team changes ends - XS 2.37 ******************* //
        
        //***************MCM Impl team changes ends - XS 2.26 ******************* //
        registerChild(CHILD_CBCOMPONENTTYPE, ComboBox.class);
        registerChild(CHILD_CBPRODUCTTYPE, ComboBox.class);
        registerChild(CHILD_BTADDCOMPONENT,Button.class);
        //***************MCM Impl team changes ends - XS 2.26 ******************* //
        
        //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - start.
        registerChild(CHILD_TXWINDOWSCROLLPOSITION, TextField.class);
        //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - end.
    }


    ///////////////////////////////////////////////////////////////////////////
    // Model management methods
    ///////////////////////////////////////////////////////////////////////////
    /**
     * <p>getWebActionModels</p>
     * <p>Description: getWebActionModels method for Jato Framework</p>
     * 
     * @param executionType: int
     * @return Model[]
     * @version 1.4 02-July-2008 XS_2.25 Added doComponentSummaryModel for the model list.
     */
    public Model[] getWebActionModels(int executionType) {
        List<Model> modelList=new ArrayList<Model>();
        switch(executionType) {
            case MODEL_TYPE_RETRIEVE:
                modelList.add(getdoUWDealSummarySnapShotModel());
                /***************MCM Impl Team XS_2.25 changes starts***************/
                modelList.add(getdoComponentSummaryModel());
                /***************MCM Impl Team XS_2.25 changes ends***************/
                break;

            case MODEL_TYPE_UPDATE:
                ;
                break;

            case MODEL_TYPE_DELETE:
                ;
                break;

            case MODEL_TYPE_INSERT:
                ;
                break;

            case MODEL_TYPE_EXECUTE:
                ;
                break;
        }
        return (Model[])modelList.toArray(new Model[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // View flow control methods
    ////////////////////////////////////////////////////////////////////////////////
    /**
     * <p>beginDisplay</p>
     * <p>Description: beginDisplay method for Jato Framework</p>
     * 
     * @param event: DisplayEvent
     * @return
     *  @version 1.6 JUly 08,2008 Bug Fix artf738619: Updated biginDisplay() method, moved super.beginDisplay(event) to end of the method
     */
    public void beginDisplay(DisplayEvent event)
        throws ModelControlException {
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        /***************MCM Impl Team XS_2.26 changes starts******************/
        cbComponentTypeOptions.populate(getRequestContext());

        /***************MCM Impl Team XS_2.26 changes ends******************/
        
        int langId = handler.getTheSessionState().getLanguageId();
        cbPageNamesOptions.populate(getRequestContext(), langId);

        ////Setup NoneSelected Label for cbPageNames
        CHILD_CBPAGENAMES_NONSELECTED_LABEL =
            BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", 
                    handler.getTheSessionState().getLanguageId());
        ////Check if the cbPageNames is already created
        if(getCbPageNames() != null)
          getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
        
        handler.setupBeforePageGeneration();
        super.beginDisplay(event);
    }

    /**
     * <p>beforeModelExecutes</p>
     * <p>Description: beforeModelExecutes method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @return boolean
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {

        return super.beforeModelExecutes(model, executionContext);
    }

    /**
     * <p>afterModelExecutes</p>
     * <p>Description: beforeModelExecutes method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @return
     */
    public void afterModelExecutes(Model model, int executionContext) {
        super.afterModelExecutes(model, executionContext);
    }

    /**
     * <p>afterModelExecutes</p>
     * <p>Description: afterModelExecutes method for Jato Framework</p>
     * 
     * @param executionContext: int
     * @return
     */
    public void afterAllModelsExecute(int executionContext) {
        super.afterAllModelsExecute(executionContext);
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.populatePageDisplayFields();
        handler.pageSaveState();
        return;
    }

    /**
     * <p>onModelError</p>     * 
     * <p>Description: onModelError method for Jato Framework</p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @param exception: ModelControlException
     * @return
     */
    public void onModelError(Model model, int executionContext, ModelControlException exception)
        throws ModelControlException {
        super.onModelError(model, executionContext, exception);
    }

////////////////////////////////////////////////////////////////////////////////
// Display control and call handlers
////////////////////////////////////////////////////////////////////////////////

    public StaticTextField getStErrorFlag() {
        return (StaticTextField)getChild(CHILD_STERRORFLAG);
    }

    public Button getBtActMsg() {
        return (Button) getChild(CHILD_BTACTMSG);
    }

    /**
     * <p>handleActMessageOK</p>     * 
     * <p>Description: Handle clicking OK on Active Message </p>
     * 
     * @param model: Model
     * @param executionContext: int
     * @param exception: ModelControlException
     * 
     * @version 1.14 fixed for artf751756: change to call handleCustomActMessageOk
     *
     */
    public void handleActMessageOK(String[] args){
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        //MCM Team changed
        handler.handleActMessageOK(args);
        handler.postHandlerProtocol();
        return;
    }

// START PM and AM    
    public StaticTextField getStPmGenerate() {
        return (StaticTextField)getChild(CHILD_STPMGENERATE);
    }

    public StaticTextField getStPmHasTitle() {
        return (StaticTextField)getChild(CHILD_STPMHASTITLE);
    }

    public StaticTextField getStPmHasInfo() {
        return (StaticTextField)getChild(CHILD_STPMHASINFO);
    }

    public StaticTextField getStPmHasTable() {
        return (StaticTextField)getChild(CHILD_STPMHASTABLE);
    }

    public StaticTextField getStPmHasOk() {
        return (StaticTextField)getChild(CHILD_STPMHASOK);
    }

    public StaticTextField getStPmTitle() {
        return (StaticTextField)getChild(CHILD_STPMTITLE);
    }

    public StaticTextField getStPmInfoMsg() {
        return (StaticTextField)getChild(CHILD_STPMINFOMSG);
    }

    public StaticTextField getStPmOnOk() {
        return (StaticTextField)getChild(CHILD_STPMONOK);
    }

    public StaticTextField getStPmMsgs() {
        return (StaticTextField)getChild(CHILD_STPMMSGS);
    }

    public StaticTextField getStPmMsgTypes() {
        return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
    }

    public StaticTextField getStAmGenerate() {
        return (StaticTextField)getChild(CHILD_STAMGENERATE);
    }
    
    public StaticTextField getStAmHasTitle() {
        return (StaticTextField)getChild(CHILD_STAMHASTITLE);
    }

    public StaticTextField getStAmHasInfo() {
        return (StaticTextField)getChild(CHILD_STAMHASINFO);
    }

    public StaticTextField getStAmHasTable() {
        return (StaticTextField)getChild(CHILD_STAMHASTABLE);
    }

    public StaticTextField getStAmTitle() {
        return (StaticTextField)getChild(CHILD_STAMTITLE);
    }

    public StaticTextField getStAmInfoMsg() {
        return (StaticTextField)getChild(CHILD_STAMINFOMSG);
    }

    public StaticTextField getStAmMsgs() {
        return (StaticTextField)getChild(CHILD_STAMMSGS);
    }

    public StaticTextField getStAmMsgTypes(){
        return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
    }

    public StaticTextField getStAmDialogMsg(){
        return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
    }

    public StaticTextField getStAmButtonsHtml() {
        return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
    }

    public HiddenField getSessionUserId() {
        return (HiddenField)getChild(CHILD_SESSIONUSERID);
    }

// END PM/AM

// START COMMON HEADER FIELDS     
     public StaticTextField getStPageLabel(){
         return (StaticTextField)getChild(CHILD_STPAGELABEL);
     }

     public StaticTextField getStCompanyName() {
         return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
     }

     public StaticTextField getStTodayDate() {
         return (StaticTextField)getChild(CHILD_STTODAYDATE);
     }
     
     public StaticTextField getStUserNameTitle() {
         return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
     }
     
     public HiddenField getDetectAlertTasks()
     {
         return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
     }

// END COMMON HEADER FIELDS
     
     /***************MCM Impl Team XS_2.26 changes starts******************/
     public ComboBox getCbProductType() {
         return (ComboBox) getChild(CHILD_CBCOMPONENTTYPE);
     }
     
     public ComboBox getCbComponentType() {
         return (ComboBox) getChild(CHILD_CBPRODUCTTYPE);
     }
     
     public Button getBtAddComponent() {
         return (Button) getChild(CHILD_BTADDCOMPONENT);
     }
     
     /***************MCM Impl Team XS_2.26 changes ends******************/
     
    public Button getStViewOnlyTag() {
        return (Button)getChild(CHILD_STVIEWONLYTAG);
    }
    
    /**
     * <p>endStViewOnlyTagDisplay</p>
     * <p>Description: display control for "View Only mode"</p>
     * @param event: ChildContentDisplayEvent
     * @return String
     */
    public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event) {
        
        ComponentDetailsHandler handler = (ComponentDetailsHandler)this.handler.cloneSS();
        handler.pageGetState(this);
        String rc = handler.displayViewOnlyTag();
        handler.pageSaveState();
        return rc;
   }
     
// START COMMON TOOLBAR FIELDS
     public TextField getTbDealId() {
         return (TextField)getChild(CHILD_TBDEALID);
     }

     public ComboBox getCbPageNames() {
         return (ComboBox)getChild(CHILD_CBPAGENAMES);
     }

    /**
     * <p> CBPageNamesOptionList </p>
     * <p>Description: inner class for page dropdown option list in toolbar</p>
     * <P>this method is exactly same as other ViewBean classes</p>
     * 
     */
    static class CbPageNamesOptionList extends OptionList {

        /**
         * <p>constructor</p> 
         */
        CbPageNamesOptionList() {

        }

        /**
         * <p>populate</p> 
         * 
         * @param rc: RequestContext
         * @param langId: int (0:English, 1:French)
         */
        public void populate(RequestContext rc, int langId) {
            clear();
            String[][] pageLabelValue = getPageOption(rc, langId);
            
            for (int i = 0; i < pageLabelValue.length; i++)
                add(pageLabelValue[i][0], pageLabelValue[i][1]);
          }
        /**
         * Get page's label&value pair for GOTO menu dropdown list.
         * @param rc
         * @param langId
         * @return String[][]
         */
        String[][] getPageOption(RequestContext rc, int langId) {
            Connection c = null;
            String[][] pageLabelValue = null;
            try {
              SelectQueryModel m = null;

              SelectQueryExecutionContext eContext = 
                  new SelectQueryExecutionContext((Connection)null,
                          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
                          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

              if (rc == null) {
                m = new doPageNameLabelModelImpl();
                c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
                eContext.setConnection(c);
              }
              else {
                m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
              }

              m.retrieve(eContext);
              m.beforeFirst();
              
              pageLabelValue = new String[m.getSize()][2];  
              int _i = 0;
              
              while (m.next()) {
                Object dfPageLabel = m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
                String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
                Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
                String value = (dfPageId == null ? "" : dfPageId.toString());
                
                pageLabelValue[_i][0] = label;
                pageLabelValue[_i][1] = value;
                _i++;
              }
              
              pageLabelValue = JatoInputUtils.sortOption(pageLabelValue, langId);

            }
            catch (Exception ex) {
                ex.printStackTrace();
            } 
            finally {
                try {
                    if (c != null)
                        c.close();
                } catch (SQLException ex) {
                }
            }
            return pageLabelValue;
          }
    }
// END inner class CbPageNamesOptionList
    

// START PREVIOUS SCREEN on tool bar CONTROL    
    public HREF getHref1() {
        return (HREF)getChild(CHILD_HREF1);
    }

    public void handleHref1Request(RequestInvocationEvent event)
        throws ServletException, IOException {

        _log.trace("Web event invoked: "+getClass().getName()+".Href1");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(0);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref2() {
        return (HREF)getChild(CHILD_HREF2);
    }

    public void handleHref2Request(RequestInvocationEvent event)
        throws ServletException, IOException {
    	
        _log.trace("Web event invoked: "+getClass().getName()+".Href2");
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(1);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref3() {
        return (HREF)getChild(CHILD_HREF3);
    }

    public void handleHref3Request(RequestInvocationEvent event)
        throws ServletException, IOException {

        _log.trace("Web event invoked: "+getClass().getName()+".Href3");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(2);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref4() {
        return (HREF)getChild(CHILD_HREF4);
    }
    
    public void handleHref4Request(RequestInvocationEvent event)
        throws ServletException, IOException {

        _log.trace("Web event invoked: "+getClass().getName()+".Href4");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(3);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref5() {
        return (HREF)getChild(CHILD_HREF5);
    }

    public void handleHref5Request(RequestInvocationEvent event)
        throws ServletException, IOException {

        _log.trace("Web event invoked: "+getClass().getName()+".Href5");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(4);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref6() {
        return (HREF)getChild(CHILD_HREF6);
    }

    public void handleHref6Request(RequestInvocationEvent event)
        throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".Href6");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(5);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref7() {
        return (HREF)getChild(CHILD_HREF7);
    }

    public void handleHref7Request(RequestInvocationEvent event)
        throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".Href7");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(6);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref8() {
        return (HREF)getChild(CHILD_HREF8);
    }

    public void handleHref8Request(RequestInvocationEvent event)
        throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".Href8");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(7);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref9() {
        return (HREF)getChild(CHILD_HREF9);
    }

    public void handleHref9Request(RequestInvocationEvent event)
        throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".Href9");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(8);
        handler.postHandlerProtocol();
        return;
    }

    public HREF getHref10() {
        return (HREF)getChild(CHILD_HREF10);
    }

    public void handleHref10Request(RequestInvocationEvent event)
        throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".Href10");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(9);
        handler.postHandlerProtocol();
        return;
    }
// END PREVIOUS SCREEN on tool bar CONTROL    

    public Button getBtToolSearch() {
        return (Button)getChild(CHILD_BTTOOLSEARCH);
    }

    public void handleBtToolSearchRequest(RequestInvocationEvent event)
    	throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".BtToolSearch");
	    ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
	    handler.preHandlerProtocol(this);
	    handler.handleDealSearch();
	    handler.postHandlerProtocol();
	    return;
    }

    public Button getBtToolHelp() {
    	return (Button)getChild(CHILD_BTTOOLHELP);
    }

    /**
     * <p>handleBtToolHelpRequest</p>
     * <p>Description: Handle clicking "help" buttons at tool bar</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtToolHelpRequest(RequestInvocationEvent event)
		throws ServletException, IOException {
    	_log.trace("Web event invoked: "+getClass().getName()+".BtToolHelp");

	    ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
	    handler.preHandlerProtocol(this);
	    handler.handleUnSupportedCalls();
	    handler.postHandlerProtocol();
	    return;
	}

    public Button getBtToolLog() {
        return (Button)getChild(CHILD_BTTOOLLOG);
    }

    /**
     * <p>handleBtToolLogRequest</p>
     * <p>Description: Handle clicking "log off" buttons at tool bar</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtToolLogRequest(RequestInvocationEvent event)
    	throws ServletException, IOException {

		_log.trace("Web event invoked: "+getClass().getName()+".BtToolLog");
	    ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
	    handler.preHandlerProtocol(this);
	    handler.handleSignOff();
	    handler.postHandlerProtocol();
	    return;
    }

    public HREF getChangePasswordHref() {
        return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
    }
    
    /**
     * <p>handleBtToolLogRequest</p>
     * <p>Description: Handle clicking "Change password" buttons in  "Tool" at tool bar</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
    	throws ServletException, IOException {

		_log.trace("Web event invoked: "+getClass().getName()+".ChangePasswordHref");
		ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleChangePassword();
		handler.postHandlerProtocol();
		return;
    }

    public HREF getToggleLanguageHref() {
        return (HREF) getChild(CHILD_TOGGLELANGUAGEHREF);
    }

    /**
     * <p>handleToggleLanguageHrefRequest</p>
     * <p>Description: Handle clicking "Toggle Language" buttons in  "Tool" at tool bar</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
        throws ServletException, IOException {

		_log.trace("Web event invoked: "+getClass().getName()+".ToggleLanguageHref");
    	ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
    	handler.preHandlerProtocol(this);
    	handler.handleToggleLanguage();
    	handler.postHandlerProtocol();
    }

    public Button getBtToolHistory() {
	    return (Button)getChild(CHILD_BTTOOLHISTORY);
	}

    /**
     * <p>handleToggleLanguageHrefRequest</p>
     * <p>Description: Handle clicking "History" buttons at tool bar</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtToolHistoryRequest(RequestInvocationEvent event)
    	throws ServletException, IOException {
		_log.trace("Web event invoked: "+getClass().getName()+".BtToolHistory");
	    ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
	    handler.preHandlerProtocol(this);
	    handler.handleDisplayDealHistory();
	    handler.postHandlerProtocol();
	    return;
    }

    public Button getBtTooNotes() {
        return (Button)getChild(CHILD_BTTOONOTES);
    }

    /**
     * <p>handleBtTooNotesRequest</p>
     * <p>Description: Handle clicking "Deal Notes" buttons at tool bar</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtTooNotesRequest(RequestInvocationEvent event)
    	throws ServletException, IOException {

		_log.trace("Web event invoked: "+getClass().getName()+".BtTooNotes");
	    ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
	    handler.preHandlerProtocol(this);
	    handler.handleDisplayDealNotes();
	    handler.postHandlerProtocol();
	    return ;
	}
    
    public Button getBtWorkQueueLink() {
        return (Button)getChild(CHILD_BTWORKQUEUELINK);
    }

    /**
     * <p>handleBtWorkQueueLinkRequest</p>
     * <p>Description: Handle clicking "Work Queue" buttons at tool bar</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
    	throws ServletException, IOException {

		_log.trace("Web event invoked: "+getClass().getName()+".BtWorkQueueLink");
		ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayWorkQueue();
		handler.postHandlerProtocol();
		return;
	}
// END COMMON TOOLBAR FIELDS

    public StaticTextField getStVALSData() {
        return (StaticTextField)getChild(CHILD_STVALSDATA);
    }

    public Button getBtProceed() {
        return (Button)getChild(CHILD_BTPROCEED);
    }

    /**
     * <p>handleBtProceedRequest</p>
     * <p>Description: Handle any proceed event, such as submit, AML OK, AML Cancel</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtProceedRequest(RequestInvocationEvent event)
        throws ServletException, IOException {

		_log.trace("Web event invoked: "+getClass().getName()+".BtProceed");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleGoPage();
        handler.postHandlerProtocol();
        return;
    }

// START DEAL SUMMARY SNAPSHOT FIELD 
    public StaticTextField getStDealId() {
        return (StaticTextField)getChild(CHILD_STDEALID);
    }

    public StaticTextField getStBorrFirstName() {
        return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
    }

    public StaticTextField getStDealStatus() {
        return (StaticTextField)getChild(CHILD_STDEALSTATUS);
    }

    public StaticTextField getStDealStatusDate() {
        return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
    }

    public StaticTextField getStSourceFirm() {
        return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
    }

    public StaticTextField getStSource() {
        return (StaticTextField)getChild(CHILD_STSOURCE);
    }

    public StaticTextField getStUnderWriterLastName() {
        return (StaticTextField)getChild(CHILD_STUNDERWRITERLASTNAME);
    }

    public StaticTextField getStUnderWriterFirstName() {
        return (StaticTextField)getChild(CHILD_STUNDERWRITERFIRSTNAME);
    }

    public StaticTextField getStUnderWriterMiddleInitial() {
        return (StaticTextField)getChild(CHILD_STUNDERWRITERMIDDLEINITIAL);
    }

    public StaticTextField getStCCAPS() {
        return (StaticTextField) getChild(CHILD_STCCAPS);
    }

    public StaticTextField getStServicingMortgageNumber() {
        return (StaticTextField) getChild(CHILD_STSERVICINGMORTGAGENUMBER);
    }

    public StaticTextField getStDealType() {
        return (StaticTextField)getChild(CHILD_STDEALTYPE);
    }

    public StaticTextField getStDealPurpose() {
        return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
    }

    public StaticTextField getStPurchasePrice() {
        return (StaticTextField)getChild(CHILD_STPURCHASEPRICE);
    }

    public StaticTextField getStApplicationDate() {
        return (StaticTextField)getChild(CHILD_STAPPLICATIONDATE);
    }

    public StaticTextField getStPmtTerm() {
        return (StaticTextField)getChild(CHILD_STPMTTERM);
    }

    public StaticTextField getStEstClosingDate() {
        return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
    }

    public StaticTextField getStSpecialFeature()
    {
        return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
    }


    public StaticTextField getStLOB() {
        return (StaticTextField)getChild(CHILD_STLOB);
    }
// END DEAL SUMMARY SNAPSHOT FIELD 
    
    /**
     * For LOC component
     * MCM Impl team changes  - XS 2.33
     */
    public pgComponentDetailsLOCTiledView getRepeatedLOC() {
        return (pgComponentDetailsLOCTiledView) getChild(CHILD_REPEATEDLOC);
    }
    

    /**
     * XS_2.38
     * @return instance of pgComponentDetailsOverdraftTiledView
     */
    public pgComponentDetailsOverdraftTiledView getRepeatedOverdraft() {
        return (pgComponentDetailsOverdraftTiledView) getChild(CHILD_REPEATEDOVERDRAFT);
    }
    

    
    /**
     * Loan Component TileView
     * MCM Impl team changes  - XS 2.37
     */
    public pgComponentDetailsLoanTiledView getRepeatedLoan() {
        return (pgComponentDetailsLoanTiledView) getChild(CHILD_REPEATEDLOAN);
    }
    

//  START BUTTONS
    public Button getBtCheckRule() {
        return (Button)getChild(CHILD_BTCHECKRULE);
    }

    /**
     * <p>handleBtCheckRuleRequest</p>
     * <p>Description: Handle clicking Check Rule</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtCheckRuleRequest(RequestInvocationEvent event)
    throws ServletException, IOException {

    	_log.trace("Web event invoked: "+getClass().getName()+".BtCheckRule");
		ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleCheckRule();
		handler.postHandlerProtocol();
		return;
    }

    public Button getBtRecalc() {
        return (Button)getChild(CHILD_BTRECALC);
    }

    /**
     * <p>handleBtRecalcRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtRecalcRequest(RequestInvocationEvent event)
        throws ServletException, IOException {
        _log.trace("Web event invoked: "+getClass().getName()+".BtRecalc");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleRecalc();
        handler.postHandlerProtocol();    
        return;
    }


    public Button getBtSubmit() {
        return (Button)getChild(CHILD_BTSUBMIT);
    }
    
    /**
     * <p>endBtSubmitDisplay</p>
     * <p>Description: control if display submit button. if not view only then display
     * @param event: ChildContentDisplayEvent
     * @return String
     */
    public String endBtSubmitDisplay(ChildContentDisplayEvent event) {

    	ComponentDetailsHandler handler = (ComponentDetailsHandler)this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.displaySubmitButton();
        handler.pageSaveState();
    
        if(rc == true) {
          return event.getContent();
        } else {
          return "";
        }
    }

    /**
     * <p>handleBtAddComponentRequest</p>
     * <p>Description: Handle clicking Add Component</p>
     * @param event: RequestInvocationEvent
     * @return 
     * XS_2.26 add component
     */
    public void handleBtAddComponentRequest(RequestInvocationEvent event)
        throws ServletException, Exception {
        _log.trace("Web event invoked: "+getClass().getName()+".BtSubmit");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleAddComponent();
        handler.postHandlerProtocol();

        return;
    }
    
    
    /**
     * <p>handleBtSubmitRequest</p>
     * <p>Description: Handle clicking submit</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtSubmitRequest(RequestInvocationEvent event)
        throws ServletException, IOException {
    	_log.trace("Web event invoked: "+getClass().getName()+".BtSubmit");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.forceRecalc();//FXP24256, Feb 5, 2009 added
        handler.handleDECSubmit();
        handler.postHandlerProtocol();

        return;
    }

    public Button getBtOK() {
        return (Button)getChild(CHILD_BTOK);
    }
    
    /**
     * <p>endBtOKDisplay</p>
     * <p>Description: control if display OK button. if view only then display
     * @param event: ChildContentDisplayEvent
     * @return String
     */
    public String endBtOKDisplay(ChildContentDisplayEvent event) {
        
        ComponentDetailsHandler handler = (ComponentDetailsHandler)this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = !handler.displaySubmitButton(); // OK is 
        handler.pageSaveState();
    
        if(rc == true) {
          return event.getContent();
        } else {
          return "";
        }
    }

    /**
     * <p>handleBtOKRequest</p>
     * <p>Description: Handle clicking cancel</p>
     * @param event: RequestInvocationEvent
     * @return 
     */
    public void handleBtOKRequest(RequestInvocationEvent event)
        throws ServletException, IOException {
    	_log.trace("Web event invoked: "+getClass().getName()+".BtOK");

        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleCancelStandard();
        handler.postHandlerProtocol();
        return;
    }

    public Button getBtCancel() {
        return (Button)getChild(CHILD_BTCANCEL);
    }

    /**
     * <p>endBtCancelDisplay</p>
     * <p>Description: control if display cancel button. 
     * @param event: ChildContentDisplayEvent
     * @return String
     * 
     * @see ComponentDetailsHandler.displayCancelButton
     */
    public String endBtCancelDisplay(ChildContentDisplayEvent event) {
        
        ComponentDetailsHandler handler = (ComponentDetailsHandler)this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.displayCancelButton(1);
        handler.pageSaveState();

        if(rc == true) {
          return event.getContent();
        } else {
          return "";
        }
    }

    /**
     * <p>handleBtCancelRequest</p>
     * <p>Description: Handle clicking cancel</p>
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @version 1.1 set parameter of handleCancelStandard for XS 2.31
     * @version 1.13 July 25, 2008 fixed artf751260 added 2nd parameter into prehandlerProtocol to skip save
     */
    public void handleBtCancelRequest(RequestInvocationEvent event)
        throws ServletException, IOException    {

        _log.trace("Web event invoked: "+getClass().getName()+".BtCancel");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        //MCM Team: artf751260 added 2nd parameter to skip save
        handler.preHandlerProtocol(this, true);
        // set parameter false for XS 2.31
        handler.handleCancelStandard(false);
        handler.postHandlerProtocol();

        return;
    }

    public Button getBtCurrentCancel() {
        return (Button)getChild(CHILD_BTCURRENTCANCEL);
    }

    /**
     * <p>endBtCurrentCancelDisplay</p>
     * <p>Description: control if display Cancel Current change Only button. 
     * @param event: ChildContentDisplayEvent
     * @return String
     * 
     * @see ComponentDetailsHandler.displayCancelButton
     */
    public String endBtCurrentCancelDisplay(ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler)this.handler.cloneSS();
        handler.pageGetState(this);
        boolean rc = handler.displayCancelButton(2);
        handler.pageSaveState();

        if(rc == true) {
          return event.getContent();
        } else {
          return "";
        }
      }
    
    /**
     * <p>handleBtCancelRequest</p>
     * <p>Description: Handle clicking cancel</p>
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @see ComponentDetailsHandler.handleCancelStandard
     * 
     * @version 1.1 set parameter of handleCancelStandard for XS 2.31
     * @version 1.13 July 25, 2008 fixed artf751260 added 2nd parameter into prehandlerProtocol to skip save
     */
    public void handleBtCurrentCancelRequest(RequestInvocationEvent event)
        throws ServletException, IOException {

        _log.trace("Web event invoked: "+getClass().getName()+".BtCurrentCancel");
        ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
        //MCM Team: artf751260 added 2nd parameter to skip save
        handler.preHandlerProtocol(this, true);
        // set parameter false for XS 2.31
        handler.handleCancelStandard(false);
        handler.postHandlerProtocol();
        
        return;
    }

    public StaticTextField getStTotalLoanAmount()
    {
        return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
    }

    public Button getBtPrevTaskPage() {
        return (Button)getChild(CHILD_BTPREVTASKPAGE);
    }
    
    /***************MCM Impl team changes starts - XS_2.28*******************/
    public StaticTextField getStIsPageEditable() {
        return (StaticTextField)getChild(CHILD_STISPAGEEDITABLE);
    }
    
    public StaticTextField getStLenderProfileId() {
        return (StaticTextField)getChild(CHILD_STLENDERPROFILEID);
    }
    /***************MCM Impl team changes ends - XS_2.28*********************/
    
    /**
     * <p>pgComponentDetailsMTGTiledView</p>
     * <p>Description: ComponentMortgage Section Tiled View</p>
     * @param
     * @return pgComponentDetailsMTGTiledView
     * 
     * @see pgComponentDetailsMTGTiledView
     * 
     * XS_2.27
     */
    public pgComponentDetailsMTGTiledView getTiledMtgComp()
    {
        return (pgComponentDetailsMTGTiledView)getChild(CHILD_TILEDMTGCOMP);
    }

    /**
     * <p>pgComponentCreditCardTiledView</p>
     * <p>Description: ComponentCreditCard Section Tiled View</p>
     * @param
     * @return pgComponentCreditCardTiledView
     * 
     * @see pgComponentCreditCardTiledView
     * 
     * XS_2.27
     */
    public pgComponentCreditCardTiledView getTiledCrecitCardComp()
    {
        return (pgComponentCreditCardTiledView)getChild(CHILD_TILEDCCCOMP);
    }

// END DISPLAY CONTROL
    
//MODEALS    
    public doUWDealSummarySnapShotModel getdoUWDealSummarySnapShotModel() {
        if (doUWDealSummarySnapShot == null) {
            doUWDealSummarySnapShot = (doUWDealSummarySnapShotModel) getModel(doUWDealSummarySnapShotModel.class);
        }

        return doUWDealSummarySnapShot;
    }

    public void setdoUWDealSummarySnapShotModel(
            doUWDealSummarySnapShotModel model) {
        doUWDealSummarySnapShot = model;
    }
//END MODELS
/******************************MCM Impl Team XS_2.25 changes starts*****************/
    public StaticTextField getStProductName() {
        return (StaticTextField)getChild(CHILD_STPRODUCTNAME);
    }
    public StaticTextField getStMIPremium() {
        return (StaticTextField)getChild(CHILD_STMIPREMIUM);
    }
    
    public StaticTextField getStTaxEscrow() {
        return (StaticTextField)getChild(CHILD_STTAXESCROW);
    }
    
    public StaticTextField getStCombinedLTV() {
        return (StaticTextField)getChild(CHILD_STCOMBINEDLTV);
    }
    
    public StaticTextField getStTotalAmount() {
        return (StaticTextField)getChild(CHILD_STTOTALAMOUNT);
    }
    public StaticTextField getStUnAllocatedAmount() {
        return (StaticTextField)getChild(CHILD_STUNALLOCATEDAMOUNT);
    }
    
    public StaticTextField getStTotalCashBackAmount() {
        return (StaticTextField)getChild(CHILD_STTOTALCASHBACKAMOUNT);
    }
    
   
    public doComponentSummaryModel getdoComponentSummaryModel() {
        if (doComponentSummaryModel == null) {
            doComponentSummaryModel = (doComponentSummaryModel) getModel(doComponentSummaryModel.class);
        }

        return doComponentSummaryModel;
    }

    public void setdoComponentSummaryModel(
            doComponentSummaryModel model) {
        doComponentSummaryModel = model;
    }
    
    //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - start.
    public TextField getTxWindowScrollPosition() {
        return (TextField) getChild(CHILD_TXWINDOWSCROLLPOSITION);
    }
    //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - end.
    
/******************************MCM Impl Team XS_2.25 changes ends*******************/    
    /**
     * Since the population of comboboxes on the pages should be done from
     * BXResource bundle, either the iMT converted methods could be adjusted
     * with the FETCH_DATA_STATEMENT element if place or it could be done
     * directly from the BXResource. Each approach has its pros and cons. The
     * most important is: for English and French versions could be different
     * default values. It forces to use the second approach.
     * 
     * It this case to escape annoying code clone and follow the object oriented
     * design the following abstact base class should encapsulate the combobox
     * OptionList population. Each ComboBoxOptionList inner class should extend
     * this base class and implement the BXResources table name.
     * 
     *  
     */
    static abstract class BaseComboBoxOptionList extends OptionList {
        BaseComboBoxOptionList() {
        }

        protected final void populateOptionList(RequestContext rc, int langId,
                String tablename, OptionList name) {
            try {
                String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName
                    (SessionStateModel.class);
                SessionStateModelImpl theSessionState =
                    (SessionStateModelImpl)rc.getModelManager().getModel(
                    SessionStateModel.class,
                    defaultInstanceStateName,
                    true);

                //Get IDs and Labels from BXResources
                Collection<String[]> c = BXResources.getPickListValuesAndDesc
                    (theSessionState.getDealInstitutionId(), tablename, langId);
                int tableSize = BXResources.getPickListTableSize
                    (theSessionState.getDealInstitutionId(), tablename, langId);
                Iterator l = c.iterator();

                String[] vals = new String[tableSize];
                String[] labels = new String[tableSize];
                String[] theVal = new String[2];

                int i = 0;

                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    vals[i] = theVal[1];
                    labels[i] = theVal[0];
                    i++;
                }

                name.setOptions(vals, labels);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////////

    public static final String PAGE_NAME="pgComponentDetails";
    public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgComponentDetails.jsp";  
    public static Map FIELD_DESCRIPTORS;
    public static final String CHILD_BTACTMSG = "btActMsg";
    public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";
    public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";
    public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";
    public static final String CHILD_TBDEALID="tbDealId";
    public static final String CHILD_TBDEALID_RESET_VALUE="";
    public static final String CHILD_CBPAGENAMES="cbPageNames";
    public static final String CHILD_CBPAGENAMES_RESET_VALUE="";
    private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
    private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
    public static final String CHILD_BTPROCEED="btProceed";
    public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
    public static final String CHILD_HREF1="Href1";
    public static final String CHILD_HREF1_RESET_VALUE="";
    public static final String CHILD_HREF2="Href2";
    public static final String CHILD_HREF2_RESET_VALUE="";
    public static final String CHILD_HREF3="Href3";
    public static final String CHILD_HREF3_RESET_VALUE="";
    public static final String CHILD_HREF4="Href4";
    public static final String CHILD_HREF4_RESET_VALUE="";
    public static final String CHILD_HREF5="Href5";
    public static final String CHILD_HREF5_RESET_VALUE="";
    public static final String CHILD_HREF6="Href6";
    public static final String CHILD_HREF6_RESET_VALUE="";
    public static final String CHILD_HREF7="Href7";
    public static final String CHILD_HREF7_RESET_VALUE="";
    public static final String CHILD_HREF8="Href8";
    public static final String CHILD_HREF8_RESET_VALUE="";
    public static final String CHILD_HREF9="Href9";
    public static final String CHILD_HREF9_RESET_VALUE="";
    public static final String CHILD_HREF10="Href10";
    public static final String CHILD_HREF10_RESET_VALUE="";
    public static final String CHILD_STPAGELABEL="stPageLabel";
    public static final String CHILD_STPAGELABEL_RESET_VALUE="";
    public static final String CHILD_STCOMPANYNAME="stCompanyName";
    public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
    public static final String CHILD_STTODAYDATE="stTodayDate";
    public static final String CHILD_STTODAYDATE_RESET_VALUE="";
    public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
    public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
    
    public static final String CHILD_STDEALID="stDealId";
    public static final String CHILD_STDEALID_RESET_VALUE="";
    public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
    public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
    public static final String CHILD_STDEALSTATUS="stDealStatus";
    public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
    public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
    public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
    public static final String CHILD_STSOURCEFIRM="stSourceFirm";
    public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
    public static final String CHILD_STSOURCE="stSource";
    public static final String CHILD_STSOURCE_RESET_VALUE="";

    public static final String CHILD_STDEALTYPE="stDealType";
    public static final String CHILD_STDEALTYPE_RESET_VALUE="";
    public static final String CHILD_STDEALPURPOSE="stDealPurpose";
    public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
    public static final String CHILD_STPURCHASEPRICE="stPurchasePrice";
    public static final String CHILD_STPURCHASEPRICE_RESET_VALUE="";
    public static final String CHILD_STPMTTERM="stPmtTerm";
    public static final String CHILD_STPMTTERM_RESET_VALUE="";
    public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
    public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";
    public static final String CHILD_STUNDERWRITERLASTNAME = "stUnderwriterLastName";
    public static final String CHILD_STUNDERWRITERLASTNAME_RESET_VALUE = "";
    public static final String CHILD_STUNDERWRITERFIRSTNAME = "stUnderwriterFirstName";
    public static final String CHILD_STUNDERWRITERFIRSTNAME_RESET_VALUE = "";
    public static final String CHILD_STUNDERWRITERMIDDLEINITIAL = "stUnderwriterMiddleInitial";
    public static final String CHILD_STUNDERWRITERMIDDLEINITIAL_RESET_VALUE = "";
    
    public static final String CHILD_STVALSDATA="stVALSData";
    public static final String CHILD_STVALSDATA_RESET_VALUE="";

    public static final String CHILD_BTSUBMIT="btSubmit";
    public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
    public static final String CHILD_BTOK="btOK";
    public static final String CHILD_BTOK_RESET_VALUE=" ";
    public static final String CHILD_BTCHECKRULE="btCheckRule";
    public static final String CHILD_BTCHECKRULE_RESET_VALUE=" ";
    public static final String CHILD_BTRECALC="btRecalc";
    public static final String CHILD_BTRECALC_RESET_VALUE=" ";
    
    public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
    public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";

    public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
    public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
    public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
    public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
    public static final String CHILD_BTTOOLHISTORY="btToolHistory";
    public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
    public static final String CHILD_BTTOONOTES="btTooNotes";
    public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
    public static final String CHILD_BTTOOLSEARCH="btToolSearch";
    public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
    public static final String CHILD_BTTOOLHELP="btToolHelp";
    public static final String CHILD_BTTOOLHELP_RESET_VALUE=" ";
    public static final String CHILD_BTTOOLLOG="btToolLog";
    public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
    public static final String CHILD_STERRORFLAG="stErrorFlag";
    public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
    public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
    public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
    public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
    public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
    public static final String CHILD_STAPPLICATIONDATE = "stApplicationDate";
    public static final String CHILD_STAPPLICATIONDATE_RESET_VALUE = "";
    
    public static final String CHILD_BTCANCEL="btCancel";
    public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
    public static final String CHILD_BTCURRENTCANCEL="btCurrentCancel";
    public static final String CHILD_BTCURRENTCANCEL_RESET_VALUE=" ";
    
    public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
    public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
    public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
    public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
    public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
    public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
    public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
    public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
    public static final String CHILD_STTASKNAME="stTaskName";
    public static final String CHILD_STTASKNAME_RESET_VALUE="";
    public static final String CHILD_SESSIONUSERID="sessionUserId";
    public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
    public static final String CHILD_STPMGENERATE="stPmGenerate";
    public static final String CHILD_STPMGENERATE_RESET_VALUE="";
    public static final String CHILD_STPMHASTITLE="stPmHasTitle";
    public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
    public static final String CHILD_STPMHASINFO="stPmHasInfo";
    public static final String CHILD_STPMHASINFO_RESET_VALUE="";
    public static final String CHILD_STPMHASTABLE="stPmHasTable";
    public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
    public static final String CHILD_STPMHASOK="stPmHasOk";
    public static final String CHILD_STPMHASOK_RESET_VALUE="";
    public static final String CHILD_STPMTITLE="stPmTitle";
    public static final String CHILD_STPMTITLE_RESET_VALUE="";
    public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
    public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
    public static final String CHILD_STPMONOK="stPmOnOk";
    public static final String CHILD_STPMONOK_RESET_VALUE="";
    public static final String CHILD_STPMMSGS="stPmMsgs";
    public static final String CHILD_STPMMSGS_RESET_VALUE="";
    public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
    public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
    public static final String CHILD_STAMGENERATE="stAmGenerate";
    public static final String CHILD_STAMGENERATE_RESET_VALUE="";
    public static final String CHILD_STAMHASTITLE="stAmHasTitle";
    public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
    public static final String CHILD_STAMHASINFO="stAmHasInfo";
    public static final String CHILD_STAMHASINFO_RESET_VALUE="";
    public static final String CHILD_STAMHASTABLE="stAmHasTable";
    public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
    public static final String CHILD_STAMTITLE="stAmTitle";
    public static final String CHILD_STAMTITLE_RESET_VALUE="";
    public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
    public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
    public static final String CHILD_STAMMSGS="stAmMsgs";
    public static final String CHILD_STAMMSGS_RESET_VALUE="";
    public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
    public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
    public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
    public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
    public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
    public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
    public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
    public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";

    public static final String CHILD_STCCAPS = "stCCAPS";
    public static final String CHILD_STCCAPS_RESET_VALUE = "";
    public static final String CHILD_STSERVICINGMORTGAGENUMBER = "stServicingMortgageNumber";
    public static final String CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE = "";
    
    // ***************MCM Impl team changes starts - XS 2.33 ***************** //
    public static final String CHILD_REPEATEDLOC = "RepeatedLOC";
    // ***************MCM Impl team changes ends - XS 2.33 ******************* //

    // ***************MCM Impl team changes starts - XS 2.27 ***************** //
    public static final String CHILD_TILEDMTGCOMP = "MTGComp";
    public static final String CHILD_TILEDMTGCOMP_RESET_VALUE = "";
    // ***************MCM Impl team changes ends - XS 2.27 ***************** //
    
    // ***************MCM Impl team changes starts - XS 2.38 ***************** //
    public static final String CHILD_REPEATEDOVERDRAFT = "RepeatedOverdraft";
    // ***************MCM Impl team changes ends - XS 2.38 ******************* //

    
    /***************MCM Impl team changes starts - XS_2.28*******************/
    public static final String CHILD_STISPAGEEDITABLE = "stIsPageEditable";
    public static final String CHILD_STISPAGEEDITABLE_RESET_VALUE = "Y";
    
    public static final String CHILD_STLENDERPROFILEID = "stLenderProfileId";
    public static final String CHILD_STLENDERPROFILEID_RESET_VALUE = "";

    /***************MCM Impl team changes ends - XS_2.28*********************/
    // ***************MCM Impl team changes starts - XS 2.36 ***************** //
    public static final String CHILD_TILEDCCCOMP = "CreditCardComp";
    public static final String CHILD_TILEDCCGCOMP_RESET_VALUE = "";
    // ***************MCM Impl team changes ends - XS 2.36 ***************** //
    /****************MCM Impl Team XS_2.25 changes starts********************/
    public static final String CHILD_STLOB="stLOB";
    public static final String CHILD_STLOB_RESET_VALUE="";
    public static final String CHILD_STPRODUCTNAME = "stProductName";
    public static final String CHILD_STPRODUCTNAME_RESET_VALUE = "";
    
    public static final String CHILD_STMIPREMIUM = "stMIPremium";
    public static final String CHILD_STMIPREMIUM_RESET_VALUE = "";
    
    public static final String CHILD_STTAXESCROW = "stTaxEscrow";
    public static final String CHILD_STTAXESCROW_RESET_VALUE = "";
    
    public static final String CHILD_STCOMBINEDLTV = "stCombinedLTV";
    public static final String CHILD_STCOMBINEDLTV_RESET_VALUE = "";
    
    public static final String CHILD_STTOTALAMOUNT = "stTotalAmount";
    public static final String CHILD_STTOTALAMOUNT_RESET_VALUE = "";
    
    public static final String CHILD_STUNALLOCATEDAMOUNT = "stUnAllocatedAmount";
    public static final String CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE = "";
    
    public static final String CHILD_STTOTALCASHBACKAMOUNT = "stTotalCashBackAmount";
    public static final String CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE = "";
    
  
    // ***************MCM Impl team changes starts - XS 2.37 ***************** //
    public static final String CHILD_REPEATEDLOAN = "RepeatedLoan";
    // ***************MCM Impl team changes ends - XS 2.37 ******************* //
    
    private doComponentSummaryModel doComponentSummaryModel=null;
    /****************MCM Impl Team XS_2.25 changes ends********************/
    
    
    /***************MCM Impl Team XS_2.26 changes starts******************/
    public static final String CHILD_CBCOMPONENTTYPE = "cbComponentType";
    public static final String CHILD_CBCOMPONENTTYPE_RESET_VALUE = "";

    public static final String CHILD_CBPRODUCTTYPE = "cbComponentProduct";
    public static final String CHILD_CBPRODUCTTYPE_RESET_VALUE = "";
    
    public static final String CHILD_BTADDCOMPONENT = "btAddComponent";
    public static final String CHILD_BTADDCOMPONENT_RESET_VALUE = "";
    
    private CbComponentTypeOptionList cbComponentTypeOptions = new CbComponentTypeOptionList();

    //private CbProductTypeOptionList cbProductTypeOptions = new CbProductTypeOptionList();
    /***************MCM Impl Team XS_2.26 changes endss******************/
    
    //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - start.
    public static final String CHILD_TXWINDOWSCROLLPOSITION = "txWindowScrollPosition";
    public static final String CHILD_TXWINDOWSCROLLPOSITION_RESET_VALUE = "";
    //FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position - start.
    
    ////////////////////////////////////////////////////////////////////////////
    // Instance variables
    ////////////////////////////////////////////////////////////////////////////

    private doUWDealSummarySnapShotModel doUWDealSummarySnapShot=null;




    ////////////////////////////////////////////////////////////////////////////
    // Custom Members - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////

    private ComponentDetailsHandler handler=new ComponentDetailsHandler();
    public SysLogger logger;

    /****************MCM Impl Team XS_2.26 changes starts********************/
    /**
     * CbComponentTypeOptionList
     * inner Class for COMPONENTTYPE comboBox
     * 
     */
    static class CbComponentTypeOptionList extends OptionList {

        CbComponentTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            try {
                String defaultInstanceStateName = rc.getModelManager()
                        .getDefaultModelInstanceName(SessionStateModel.class);
                SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                        .getModelManager().getModel(SessionStateModel.class,
                                defaultInstanceStateName, true);
                int languageId = theSessionState.getLanguageId();

                // Get IDs and Labels from BXResources
                Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                        "COMPONENTTYPE", languageId);
                Iterator l = c.iterator();
                String[] theVal = new String[2];
                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /****************MCM Impl Team XS_2.26 changes ends********************/
    
    /*
     * method handle french version jsp
     * July 21, 2008 MCM Impl Team XS_2.54 
     */
    public String getDisplayURL() {
         ComponentDetailsHandler handler =(ComponentDetailsHandler) this.handler.cloneSS();
         handler.pageGetState(this);

         String url=getDefaultDisplayURL();

         int languageId = handler.theSessionState.getLanguageId();

         if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
            url = BXResources.getBXUrl(url, languageId);
         }
         else {
            url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
         }

        return url;
    }
/************Bugfix:FXP23027 Added missing code for previous link Starts********************************/
    /**
     * <p>handleBtPrevTaskPageRequest</p>
     * <p>Description: Handle clicking "Previous Task " event</p>
     * @param event: RequestInvocationEvent
     * @throws ServletException
     * @thwoes IOException
     * @return
     * 
     * */
    public void handleBtPrevTaskPageRequest (RequestInvocationEvent event)
    throws ServletException, IOException {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handlePrevTaskPage();

        handler.postHandlerProtocol();

    }

    /**
     * <p>endBtPrevTaskPageDisplay</p>
     * <p>Description: Description: display control for "Previous Task Page Display"</p>
     * @param event: ChildContentDisplayEvent
     * @return
     * @version 1.0 13-June-2008 XS_16.12 Intial Version
     */
    public String endBtPrevTaskPageDisplay (ChildContentDisplayEvent event) {

        // The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generatePrevTaskPage();

        handler.pageSaveState();

        if (rc == true) {

            return event.getContent();

        } else {

            return "";

        }

    }

    public String endHref1Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                0);
        handler.pageSaveState();

        return rc;

    }

    public String endHref2Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                1);
        handler.pageSaveState();

        return rc;

    }

    public String endHref3Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                2);
        handler.pageSaveState();

        return rc;

    }

    public String endHref4Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                3);
        handler.pageSaveState();

        return rc;

    }

    public String endHref5Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                4);
        handler.pageSaveState();

        return rc;

    }

    public String endHref6Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                5);
        handler.pageSaveState();

        return rc;

    }

    public String endHref7Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                6);
        handler.pageSaveState();

        return rc;

    }

    public String endHref8Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                7);
        handler.pageSaveState();

        return rc;

    }

    public String endHref9Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                8);
        handler.pageSaveState();

        return rc;

    }

    public String endHref10Display (ChildContentDisplayEvent event) {

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(),
                9);
        handler.pageSaveState();

        return rc;

    }
    /************Bugfix:FXP23027 Added missing code for previous link Ends********************************/   
}
