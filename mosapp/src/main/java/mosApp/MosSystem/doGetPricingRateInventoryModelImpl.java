package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doGetPricingRateInventoryModelImpl extends QueryModelBase
	implements doGetPricingRateInventoryModel
{
	/**
	 *
	 *
	 */
	public doGetPricingRateInventoryModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPricingRateInventoryId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPRICINGRATEINVENTORYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPricingRateInventoryId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPRICINGRATEINVENTORYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfInternalRatePercentage()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINTERNALRATEPERCENTAGE);
	}


	/**
	 *
	 *
	 */
	public void setDfInternalRatePercentage(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINTERNALRATEPERCENTAGE,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfIndexEffectiveDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFINDEXEFFECTIVEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfIndexEffectiveDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFINDEXEFFECTIVEDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaximumDiscountAllowed()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMAXIMUMDISCOUNTALLOWED);
	}


	/**
	 *
	 *
	 */
	public void setDfMaximumDiscountAllowed(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMAXIMUMDISCOUNTALLOWED,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PRICINGRATEINVENTORY.PRICINGRATEINVENTORYID, PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE, PRICINGRATEINVENTORY.INDEXEFFECTIVEDATE, PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED FROM PRICINGRATEINVENTORY, PRICINGPROFILE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="PRICINGRATEINVENTORY, PRICINGPROFILE";
	public static final String STATIC_WHERE_CRITERIA=" (PRICINGRATEINVENTORY.PRICINGPROFILEID  =  PRICINGPROFILE.PRICINGPROFILEID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID="PRICINGRATEINVENTORY.PRICINGRATEINVENTORYID";
	public static final String COLUMN_DFPRICINGRATEINVENTORYID="PRICINGRATEINVENTORYID";
	public static final String QUALIFIED_COLUMN_DFINTERNALRATEPERCENTAGE="PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE";
	public static final String COLUMN_DFINTERNALRATEPERCENTAGE="INTERNALRATEPERCENTAGE";
	public static final String QUALIFIED_COLUMN_DFINDEXEFFECTIVEDATE="PRICINGRATEINVENTORY.INDEXEFFECTIVEDATE";
	public static final String COLUMN_DFINDEXEFFECTIVEDATE="INDEXEFFECTIVEDATE";
	public static final String QUALIFIED_COLUMN_DFMAXIMUMDISCOUNTALLOWED="PRICINGRATEINVENTORY.MAXIMUMDISCOUNTALLOWED";
	public static final String COLUMN_DFMAXIMUMDISCOUNTALLOWED="MAXIMUMDISCOUNTALLOWED";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRICINGRATEINVENTORYID,
				COLUMN_DFPRICINGRATEINVENTORYID,
				QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINTERNALRATEPERCENTAGE,
				COLUMN_DFINTERNALRATEPERCENTAGE,
				QUALIFIED_COLUMN_DFINTERNALRATEPERCENTAGE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINDEXEFFECTIVEDATE,
				COLUMN_DFINDEXEFFECTIVEDATE,
				QUALIFIED_COLUMN_DFINDEXEFFECTIVEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMAXIMUMDISCOUNTALLOWED,
				COLUMN_DFMAXIMUMDISCOUNTALLOWED,
				QUALIFIED_COLUMN_DFMAXIMUMDISCOUNTALLOWED,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

