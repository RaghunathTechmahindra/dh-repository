package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

//--Release2.1--TD_DTS_CR//
//// TD requests to add ConditionReview functionalities to the DocumentTracking.
/**
 *
 *
 *
 */
public class pgDocTrackingRepeated5TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDocTrackingRepeated5TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(200);
		setPrimaryModelClass( doDocTrackingCusConditionModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getTbCusCondition().setValue(CHILD_TBCUSCONDITION_RESET_VALUE);
		getBtCusDeleteCondition().setValue(CHILD_BTCUSDELETECONDITION_RESET_VALUE);
		getTbCusDocumentLabel().setValue(CHILD_TBCUSDOCUMENTLABEL_RESET_VALUE);
		getCbCusAction().setValue(CHILD_CBCUSACTION_RESET_VALUE);
		getCbCusResponsibility().setValue(CHILD_CBCUSRESPONSIBILITY_RESET_VALUE);
		getHdCusDocTrackId().setValue(CHILD_HDCUSDOCTRACKID_RESET_VALUE);
		getStCusSignByPass().setValue(CHILD_STCUSSIGNBYPASS_RESET_VALUE);
		getHdByPass().setValue(CHILD_HDBYPASS_RESET_VALUE);
    getHdCusCopyId().setValue(CHILD_HDCUSCOPYID_RESET_VALUE);
    getStIncludeCustomTileStart().setValue(CHILD_STINCLUDECUSTTILESTART_RESET_VALUE);
    getStIncludeCustomTileEnd().setValue(CHILD_STINCLUDECUSTTILEEND_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_TBCUSCONDITION,TextField.class);
		registerChild(CHILD_BTCUSDELETECONDITION,Button.class);
		registerChild(CHILD_TBCUSDOCUMENTLABEL,TextField.class);
		registerChild(CHILD_CBCUSACTION,ComboBox.class);
		registerChild(CHILD_CBCUSRESPONSIBILITY,ComboBox.class);
		registerChild(CHILD_HDCUSDOCTRACKID,HiddenField.class);
		registerChild(CHILD_STCUSSIGNBYPASS,StaticTextField.class);
		registerChild(CHILD_HDCUSCOPYID,HiddenField.class);
		registerChild(CHILD_HDBYPASS,HiddenField.class);
		registerChild(CHILD_STINCLUDECUSTTILESTART,StaticTextField.class);
		registerChild(CHILD_STINCLUDECUSTTILEEND,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDocTrackingCusConditionModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    cbCusActionOptions.populate(getRequestContext());
    cbCusResponsibilityOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();
    boolean ret = true;

    int rowNum = this.getTileIndex();

		if (movedToRow)
		{
      // Put migrated repeated_onBeforeRowDisplayEvent code here
      DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

      handler.pageGetState(this.getParentViewBean());

      logger = SysLog.getSysLogger("pgDTR5TV");
      logger.debug("pgDTR5TV@nextTile::rowNum: " + rowNum);

      ret = handler.checkDisplayThisRow("Repeated5");
      logger.debug("pgDTR5TV@nextTile::RetValue: " + ret);

      doDocTrackingCusConditionModelImpl theObj =(doDocTrackingCusConditionModelImpl) getdoDocTrackingCusConditionModel();

      //logger.debug("pgDTR5TV@nextTile::Size: " + theObj.getSize());
      //logger.debug("pgDTR5TV@nextTile::Location: " + theObj.getLocation());

      if (theObj.getNumRows() > 0 && ret == true)
      {
          handler.populateDisplayRow(rowNum,
                                      "Repeated5",
                                      doDocTrackingCusConditionModel.class,
                                      DocTrackingHandler.SECTION_CUSTOM_CONDITION);
      }

      handler.pageSaveState();

      return ret;
		}
		return movedToRow;

    /**
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			boolean retval = handler.checkDisplayThisRow("Repeated5");
			handler.pageSaveState();
			return retval;
		}
		return movedToRow;
**/

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_TBCUSCONDITION))
		{
			TextField child = new TextField(this,
				getdoDocTrackingCusConditionModel(),
				CHILD_TBCUSCONDITION,
				doDocTrackingCusConditionModel.FIELD_DFCUSCONDITION,
				CHILD_TBCUSCONDITION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCUSDELETECONDITION))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCUSDELETECONDITION,
				CHILD_BTCUSDELETECONDITION,
				CHILD_BTCUSDELETECONDITION_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_TBCUSDOCUMENTLABEL))
		{
			TextField child = new TextField(this,
				getdoDocTrackingCusConditionModel(),
				CHILD_TBCUSDOCUMENTLABEL,
				doDocTrackingCusConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_TBCUSDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBCUSACTION))
		{
			ComboBox child = new ComboBox( this,
				getdoDocTrackingCusConditionModel(),
				CHILD_CBCUSACTION,
				doDocTrackingCusConditionModel.FIELD_DFACTION,
				CHILD_CBCUSACTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbCusActionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBCUSRESPONSIBILITY))
		{
			ComboBox child = new ComboBox( this,
				getdoDocTrackingCusConditionModel(),
				CHILD_CBCUSRESPONSIBILITY,
				doDocTrackingCusConditionModel.FIELD_DFRESPONSIBILITY,
				CHILD_CBCUSRESPONSIBILITY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbCusResponsibilityOptions);
			return child;
		}
		else
		if (name.equals(CHILD_HDCUSDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackingCusConditionModel(),
				CHILD_HDCUSDOCTRACKID,
				doDocTrackingCusConditionModel.FIELD_DFCUSDOCTRACKID,
				CHILD_HDCUSDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCUSSIGNBYPASS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCUSSIGNBYPASS,
				CHILD_STCUSSIGNBYPASS,
				CHILD_STCUSSIGNBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDCUSCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackingCusConditionModel(),
				CHILD_HDCUSCOPYID,
				doDocTrackingCusConditionModel.FIELD_DFCOPYID,
				CHILD_HDCUSCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDBYPASS))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackingCusConditionModel(),
				CHILD_HDBYPASS,
				doDocTrackingCusConditionModel.FIELD_DFBYPASS,
				CHILD_HDBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDECUSTTILESTART))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINCLUDECUSTTILESTART,
				CHILD_STINCLUDECUSTTILESTART,
				CHILD_STINCLUDECUSTTILESTART_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDECUSTTILEEND))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINCLUDECUSTTILEEND,
				CHILD_STINCLUDECUSTTILEEND,
				CHILD_STINCLUDECUSTTILEEND_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCusSignByPass()
	{
		return (StaticTextField)getChild(CHILD_STCUSSIGNBYPASS);
	}

  /**
	 *
	 *
	 */
	public TextField getTbCusCondition()
	{
		return (TextField)getChild(CHILD_TBCUSCONDITION);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeCustomTileStart()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDECUSTTILESTART);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeCustomTileEnd()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDECUSTTILEEND);
	}

	/**
	 *
	 *
	 */
	public void handleBtCusDeleteConditionRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCusDeleteCondition_onWebEvent method
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleDelete(handler.getRowNdxFromWebEventMethod(event), "btCusDeleteCondition");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtCusDeleteCondition()
	{
		return (Button)getChild(CHILD_BTCUSDELETECONDITION);
	}


	/**
	 *
	 *
	 */
	public String endBtCusDeleteConditionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btCusDeleteCondition_onBeforeHtmlOutputEvent method
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public TextField getTbCusDocumentLabel()
	{
		return (TextField)getChild(CHILD_TBCUSDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbCusAction()
	{
		return (ComboBox)getChild(CHILD_CBCUSACTION);
	}

	/**
	 *
	 *
	 */
	static class CbCusActionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbCusActionOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "DOCUMENTSTATUS", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbCusResponsibility()
	{
		return (ComboBox)getChild(CHILD_CBCUSRESPONSIBILITY);
	}

	/**
	 *
	 *
	 */
	static class CbCusResponsibilityOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbCusResponsibilityOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    ////Convert to populate the Options from ResourceBundle
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "CONDITIONRESPONSIBILITYROLE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}

	/**
	 *
	 *
   *
	 */
	public HiddenField getHdByPass()
	{
		return (HiddenField)getChild(CHILD_HDBYPASS);
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdCusDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDCUSDOCTRACKID);
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdCusCopyId()
	{
		return (HiddenField)getChild(CHILD_HDCUSCOPYID);
	}

	/**
	 *
	 *
	 */
	public doDocTrackingCusConditionModel getdoDocTrackingCusConditionModel()
	{
		if (doDocTrackingCusCondition == null)
			doDocTrackingCusCondition = (doDocTrackingCusConditionModel) getModel(doDocTrackingCusConditionModel.class);
		return doDocTrackingCusCondition;
	}


	/**
	 *
	 *
	 */
	public void setdoDocTrackingCusConditionModel(doDocTrackingCusConditionModel model)
	{
			doDocTrackingCusCondition = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBCUSCONDITION="tbCusCondition";
	public static final String CHILD_TBCUSCONDITION_RESET_VALUE="";
	public static final String CHILD_BTCUSDELETECONDITION="btCusDeleteCondition";
	public static final String CHILD_BTCUSDELETECONDITION_RESET_VALUE="Delete Condition";
	public static final String CHILD_TBCUSDOCUMENTLABEL="tbCusDocumentLabel";
	public static final String CHILD_TBCUSDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_CBCUSACTION="cbCusAction";
	public static final String CHILD_CBCUSACTION_RESET_VALUE="";
  //--Release2.1--//
	//private static CbCusActionOptionList cbCusActionOptions=new CbCusActionOptionList();
  private CbCusActionOptionList cbCusActionOptions=new CbCusActionOptionList();
	public static final String CHILD_CBCUSRESPONSIBILITY="cbCusResponsibility";
	public static final String CHILD_CBCUSRESPONSIBILITY_RESET_VALUE="";
  //--Release2.1--//
	//private static CbCusResponsibilityOptionList cbCusResponsibilityOptions=new CbCusResponsibilityOptionList();
	private CbCusResponsibilityOptionList cbCusResponsibilityOptions=new CbCusResponsibilityOptionList();
	public static final String CHILD_HDCUSDOCTRACKID="hdCusDocTrackId";
	public static final String CHILD_HDCUSDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_STCUSSIGNBYPASS="stCusSignByPass";
	public static final String CHILD_STCUSSIGNBYPASS_RESET_VALUE="";
	public static final String CHILD_HDCUSCOPYID="hdCusCopyId";
	public static final String CHILD_HDCUSCOPYID_RESET_VALUE="";
	public static final String CHILD_HDBYPASS="hdByPass";
	public static final String CHILD_HDBYPASS_RESET_VALUE="";

  public static final String CHILD_STINCLUDECUSTTILESTART="stIncludeCustTileStart";
	public static final String CHILD_STINCLUDECUSTTILESTART_RESET_VALUE="";
	public static final String CHILD_STINCLUDECUSTTILEEND="stIncludeCustTileEnd";
	public static final String CHILD_STINCLUDECUSTTILEEND_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDocTrackingCusConditionModel doDocTrackingCusCondition=null;
  private DocTrackingHandler handler=new DocTrackingHandler();
  public SysLogger logger;

}

