package mosApp.MosSystem;

import java.math.BigDecimal;

import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

public class doCountTasksbyPriorityModel2Impl extends QueryModelBase implements
		doCountTasksbyPriorityModel2 {

	private static final long serialVersionUID = 6212701445445649575L;
	public static final String DATA_SOURCE_NAME = "jdbc/orcl";

	public static final String SELECT_SQL_TEMPLATE = "SELECT priority.priorityid, "
			+ "priority.pdescription, COUNT(aq.priorityid) AS PRIORITYCOUNT FROM priority LEFT JOIN ("
			+ "SELECT ASSIGNEDTASKSWORKQUEUE.PRIORITYID "
			+ "FROM ASSIGNEDTASKSWORKQUEUE, CONTACT c1, USERPROFILE, "
			+ "DEAL, BORROWER, BRANCHPROFILE, GROUPPROFILE, WORKFLOWTASK  __WHERE__  "
			+ ") aq ON (aq.priorityid=priority.priorityid) WHERE priority.priorityid > 0 "
			+ " GROUP BY (priority.priorityid, priority.pdescription) ORDER BY priority.priorityid";
	public static final String STATIC_WHERE_CRITERIA = " (((ASSIGNEDTASKSWORKQUEUE.USERPROFILEID  =  USERPROFILE.USERPROFILEID) "
			+ "AND (BRANCHPROFILE.BRANCHPROFILEID  =  GROUPPROFILE.BRANCHPROFILEID) "
			+ "AND (USERPROFILE.GROUPPROFILEID  =  GROUPPROFILE.GROUPPROFILEID) "
			+ "AND (ASSIGNEDTASKSWORKQUEUE.WORKFLOWID  =  WORKFLOWTASK.WORKFLOWID) "
			+ "AND (ASSIGNEDTASKSWORKQUEUE.TASKID  =  WORKFLOWTASK.TASKID) "
			+ "AND (c1.CONTACTID  =  USERPROFILE.CONTACTID) "
			+ "AND (ASSIGNEDTASKSWORKQUEUE.DEALID  =  DEAL.DEALID) "
			+ "AND (DEAL.COPYID  =  BORROWER.COPYID) "
			+ "AND (DEAL.DEALID  =  BORROWER.DEALID)) "
			+ "AND c1.COPYID = 1 "
			+ "AND ASSIGNEDTASKSWORKQUEUE.TASKID < 50000 "
			+ "AND DEAL.SCENARIORECOMMENDED = 'Y' "
			+ "AND BORROWER.PRIMARYBORROWERFLAG = 'Y' "
			+ "AND DEAL.COPYTYPE <> 'T')";

	public static final String COLUMN_DFPRIORITYID = "PRIORITYID";
	public static final String COLUMN_DFPRIORITYLABEL = "PDESCRIPTION";
	public static final String COLUMN_DFPRIORITYCOUNT = "PRIORITYCOUNT";

	public static final String QUALIFIED_COLUMN_DFPRIORITYID = "PRIORITY.PRIORITYID";
	public static final String QUALIFIED_COLUMN_DFPRIORITYLABEL = "PRIORITY.PDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPRIORITYCOUNT = "PRIORITYCOUNT";

	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
	static {
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFPRIORITYID, COLUMN_DFPRIORITYID,
				QUALIFIED_COLUMN_DFPRIORITYID, BigDecimal.class, true, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFPRIORITYLABEL, COLUMN_DFPRIORITYLABEL,
				QUALIFIED_COLUMN_DFPRIORITYLABEL, String.class, false, false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
		FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
				FIELD_DFPRIORITYCOUNT, COLUMN_DFPRIORITYCOUNT,
				QUALIFIED_COLUMN_DFPRIORITYCOUNT, Integer.class, false, true,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
	}

	public doCountTasksbyPriorityModel2Impl() {
		setDataSourceName(DATA_SOURCE_NAME);
		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
		setFieldSchema(FIELD_SCHEMA);
	}

}
