package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgApplicantEntryRepeatedApplicantAddressTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgApplicantEntryRepeatedApplicantAddressTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doApplicantEntryAddressSelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getAppAddressId().setValue(CHILD_APPADDRESSID_RESET_VALUE);
		getAppAddressCopyId().setValue(CHILD_APPADDRESSCOPYID_RESET_VALUE);
		getAppAddressAddrId().setValue(CHILD_APPADDRESSADDRID_RESET_VALUE);
		getAppAddressAddrCopyId().setValue(CHILD_APPADDRESSADDRCOPYID_RESET_VALUE);
		getTxApplicantAddressLine1().setValue(CHILD_TXAPPLICANTADDRESSLINE1_RESET_VALUE);
		getTxApplicantCity().setValue(CHILD_TXAPPLICANTCITY_RESET_VALUE);
		getCbApplicantAddressProvince().setValue(CHILD_CBAPPLICANTADDRESSPROVINCE_RESET_VALUE);
		getTxApplicantPostalCodeFSA().setValue(CHILD_TXAPPLICANTPOSTALCODEFSA_RESET_VALUE);
		getTxApplicantLDU().setValue(CHILD_TXAPPLICANTLDU_RESET_VALUE);
		getTxApplicantAddressLine2().setValue(CHILD_TXAPPLICANTADDRESSLINE2_RESET_VALUE);
		getCbApplicantAddressStatus().setValue(CHILD_CBAPPLICANTADDRESSSTATUS_RESET_VALUE);
		getTxApplicantTimeYears().setValue(CHILD_TXAPPLICANTTIMEYEARS_RESET_VALUE);
		getTxApplicantTimeMonths().setValue(CHILD_TXAPPLICANTTIMEMONTHS_RESET_VALUE);
		getCbApplicantAddResidentialStatus().setValue(CHILD_CBAPPLICANTADDRESIDENTIALSTATUS_RESET_VALUE);
		getBtDeleteApplicantAddress().setValue(CHILD_BTDELETEAPPLICANTADDRESS_RESET_VALUE);
		//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
		getTxStreetNumber().setValue(CHILD_TXSTREETNUMBER_RESET_VALUE);
		getTxStreetName().setValue(CHILD_TXSTREETNAME_RESET_VALUE);
		getCbStreetType().setValue(CHILD_CBSTREETTYPE_RESET_VALUE);
		getCbStreetDirection().setValue(CHILD_CBSTREETDIRECTION_RESET_VALUE);
		getTxUnitNumber().setValue(CHILD_TXUNITNUMBER_RESET_VALUE);		
		//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_APPADDRESSID,HiddenField.class);
		registerChild(CHILD_APPADDRESSCOPYID,HiddenField.class);
		registerChild(CHILD_APPADDRESSADDRID,HiddenField.class);
		registerChild(CHILD_APPADDRESSADDRCOPYID,HiddenField.class);
		registerChild(CHILD_TXAPPLICANTADDRESSLINE1,TextField.class);
		registerChild(CHILD_TXAPPLICANTCITY,TextField.class);
		registerChild(CHILD_CBAPPLICANTADDRESSPROVINCE,ComboBox.class);
		registerChild(CHILD_TXAPPLICANTPOSTALCODEFSA,TextField.class);
		registerChild(CHILD_TXAPPLICANTLDU,TextField.class);
		registerChild(CHILD_TXAPPLICANTADDRESSLINE2,TextField.class);
		registerChild(CHILD_CBAPPLICANTADDRESSSTATUS,ComboBox.class);
		registerChild(CHILD_TXAPPLICANTTIMEYEARS,TextField.class);
		registerChild(CHILD_TXAPPLICANTTIMEMONTHS,TextField.class);
		registerChild(CHILD_CBAPPLICANTADDRESIDENTIALSTATUS,ComboBox.class);
		registerChild(CHILD_BTDELETEAPPLICANTADDRESS,Button.class);
		//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
		registerChild(CHILD_TXSTREETNUMBER,TextField.class);
		registerChild(CHILD_TXSTREETNAME,TextField.class);
		registerChild(CHILD_CBSTREETTYPE,ComboBox.class);
		registerChild(CHILD_CBSTREETDIRECTION,ComboBox.class);
		registerChild(CHILD_TXUNITNUMBER,TextField.class);
		//		***** Change by NBC Impl. Team - Version 1.2 - End *****//
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				; // modelList.add(getdoApplicantEntryAddressSelectModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 05Nov2002
    ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Set up all ComboBox options
    cbApplicantAddressProvinceOptions.populate(getRequestContext());
    cbApplicantAddressStatusOptions.populate(getRequestContext());
    cbApplicantAddResidentialStatusOptions.populate(getRequestContext());
	//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
    cbStreetTypeOptions.populate(getRequestContext());
    cbStreetDirectionOptions.populate(getRequestContext());
	//		***** Change by NBC Impl. Team - Version 1.2 - End *****//  
    handler.pageSaveState();
		//========================================================================

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated RepeatedApplicantAddress_onBeforeRowDisplayEvent code here
      // Get Row Index
      int rowNum = this.getTileIndex();
      boolean ret = true;

      ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());

      //logger = SysLog.getSysLogger("pgAERAATV");

      //logger.debug("pgAERAATV@nextTile::rowNum: " + rowNum);

      // Modified by BILLY 16Jan2001
      // Get Data Object
      doApplicantEntryAddressSelectModelImpl theObj =(doApplicantEntryAddressSelectModelImpl) getdoApplicantEntryAddressSelectModel();
      //logger.debug("pgAERAATV@nextTile::Size: " + theObj.getSize());

      if (theObj.getNumRows() > 0)
      {
        //logger.debug("pgAERAATV@nextTile::Location: " + theObj.getLocation());
        handler.setApplicantAddressDisplayFields(theObj, rowNum);
      }
      else
        ret = false;

      handler.pageSaveState();

      return ret;
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
    return super.beforeModelExecutes(model, executionContext);
/*
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
    // The following code block was migrated from the RepeatedApplicantAddress_onBeforeDataObjectExecuteEvent method
    ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		//CSpCriteriaSQLObject theObj =(CSpCriteriaSQLObject) event.getDataObject();

		boolean retval = true;

    //--> Not need to check in Jato -- By Billy 24July2002
		//if (model.getName().equals("mosApp.MosSystem.doApplicantEntryAddressSelectModel"))
		//{
    retval = handler.filterApplicantEntryAddressDataSet((QueryModelBase)model);
		//}

		handler.pageSaveState();

		if(retval == true)
		  return super.beforeModelExecutes(model, executionContext);
    else
      return false;
*/
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_APPADDRESSID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_APPADDRESSID,
				doApplicantEntryAddressSelectModel.FIELD_DFBORROWERADDRESSID,
				CHILD_APPADDRESSID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_APPADDRESSCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_APPADDRESSCOPYID,
				doApplicantEntryAddressSelectModel.FIELD_DFCOPYID,
				CHILD_APPADDRESSCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_APPADDRESSADDRID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_APPADDRESSADDRID,
				doApplicantEntryAddressSelectModel.FIELD_DFADDRID,
				CHILD_APPADDRESSADDRID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_APPADDRESSADDRCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_APPADDRESSADDRCOPYID,
				doApplicantEntryAddressSelectModel.FIELD_DFADDRCOPYID,
				CHILD_APPADDRESSADDRCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXAPPLICANTADDRESSLINE1))
		{
			TextField child = new TextField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_TXAPPLICANTADDRESSLINE1,
				doApplicantEntryAddressSelectModel.FIELD_DFADDRESSLINE1,
				CHILD_TXAPPLICANTADDRESSLINE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXAPPLICANTCITY))
		{
			TextField child = new TextField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_TXAPPLICANTCITY,
				doApplicantEntryAddressSelectModel.FIELD_DFCITY,
				CHILD_TXAPPLICANTCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBAPPLICANTADDRESSPROVINCE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_CBAPPLICANTADDRESSPROVINCE,
				doApplicantEntryAddressSelectModel.FIELD_DFPROVINCEID,
				CHILD_CBAPPLICANTADDRESSPROVINCE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbApplicantAddressProvinceOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXAPPLICANTPOSTALCODEFSA))
		{
			TextField child = new TextField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_TXAPPLICANTPOSTALCODEFSA,
				doApplicantEntryAddressSelectModel.FIELD_DFPOSTALFSA,
				CHILD_TXAPPLICANTPOSTALCODEFSA_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXAPPLICANTLDU))
		{
			TextField child = new TextField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_TXAPPLICANTLDU,
				doApplicantEntryAddressSelectModel.FIELD_DFPOSTALLDU,
				CHILD_TXAPPLICANTLDU_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXAPPLICANTADDRESSLINE2))
		{
			TextField child = new TextField(this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_TXAPPLICANTADDRESSLINE2,
				doApplicantEntryAddressSelectModel.FIELD_DFADDRESSLINE2,
				CHILD_TXAPPLICANTADDRESSLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBAPPLICANTADDRESSSTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_CBAPPLICANTADDRESSSTATUS,
				doApplicantEntryAddressSelectModel.FIELD_DFADDRESSSTATUSID,
				CHILD_CBAPPLICANTADDRESSSTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbApplicantAddressStatusOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXAPPLICANTTIMEYEARS))
		{
			TextField child = new TextField(this,
				////getDefaultModel(),
        getdoApplicantEntryAddressSelectModel(),
				CHILD_TXAPPLICANTTIMEYEARS,
				CHILD_TXAPPLICANTTIMEYEARS,
				CHILD_TXAPPLICANTTIMEYEARS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXAPPLICANTTIMEMONTHS))
		{
			TextField child = new TextField(this,
				////getDefaultModel(),
        getdoApplicantEntryAddressSelectModel(),
				CHILD_TXAPPLICANTTIMEMONTHS,
				CHILD_TXAPPLICANTTIMEMONTHS,
				CHILD_TXAPPLICANTTIMEMONTHS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBAPPLICANTADDRESIDENTIALSTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantEntryAddressSelectModel(),
				CHILD_CBAPPLICANTADDRESIDENTIALSTATUS,
				doApplicantEntryAddressSelectModel.FIELD_DFRESIDENTIALSTATUSID,
				CHILD_CBAPPLICANTADDRESIDENTIALSTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbApplicantAddResidentialStatusOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEAPPLICANTADDRESS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEAPPLICANTADDRESS,
				CHILD_BTDELETEAPPLICANTADDRESS,
				CHILD_BTDELETEAPPLICANTADDRESS_RESET_VALUE,
				null);
				return child;

		}
		//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
		else
				if (name.equals(CHILD_TXSTREETNUMBER))
				{
					TextField child = new TextField(this,
						////getDefaultModel(),
		        getdoApplicantEntryAddressSelectModel(),
		        CHILD_TXSTREETNUMBER,
		        doApplicantEntryAddressSelectModel.FIELD_DFSTREETNUMBER,
		        CHILD_TXSTREETNUMBER_RESET_VALUE,
						null);
					return child;
				}
		else
			if (name.equals(CHILD_TXSTREETNAME))
			{
				TextField child = new TextField(this,
					////getDefaultModel(),
	        getdoApplicantEntryAddressSelectModel(),
	        CHILD_TXSTREETNAME,
	        doApplicantEntryAddressSelectModel.FIELD_DFSTREETNAME,
	        CHILD_TXSTREETNAME_RESET_VALUE,
					null);
				return child;
			}
		else
			if (name.equals(CHILD_CBSTREETTYPE))
			{
				ComboBox child = new ComboBox( this,
					getdoApplicantEntryAddressSelectModel(),
					CHILD_CBSTREETTYPE,
					doApplicantEntryAddressSelectModel.FIELD_DFSTREETTYPEID,
					CHILD_CBSTREETTYPE_RESET_VALUE,
					null);
				child.setLabelForNoneSelected("");
				child.setOptions(cbStreetTypeOptions);
				return child;
			}		
		else
			if (name.equals(CHILD_CBSTREETDIRECTION))
			{
				ComboBox child = new ComboBox( this,
					getdoApplicantEntryAddressSelectModel(),
					CHILD_CBSTREETDIRECTION,
					doApplicantEntryAddressSelectModel.FIELD_DFDIRECTIONTYPEID,
					CHILD_CBSTREETDIRECTION_RESET_VALUE,
					null);
				child.setLabelForNoneSelected("");
				child.setOptions(cbStreetDirectionOptions);
				return child;
			}		
		else
			if (name.equals(CHILD_TXUNITNUMBER))
			{
				TextField child = new TextField(this,
					////getDefaultModel(),
	        getdoApplicantEntryAddressSelectModel(),
	        CHILD_TXUNITNUMBER,
	        doApplicantEntryAddressSelectModel.FIELD_DFUNITNUMBER,
	        CHILD_TXUNITNUMBER_RESET_VALUE,
					null);
				return child;
			}
		//		***** Change by NBC Impl. Team - Version 1.2 - End *****//
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getAppAddressId()
	{
		return (HiddenField)getChild(CHILD_APPADDRESSID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getAppAddressCopyId()
	{
		return (HiddenField)getChild(CHILD_APPADDRESSCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getAppAddressAddrId()
	{
		return (HiddenField)getChild(CHILD_APPADDRESSADDRID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getAppAddressAddrCopyId()
	{
		return (HiddenField)getChild(CHILD_APPADDRESSADDRCOPYID);
	}


	/**
	 *
	 *
	 */
	public TextField getTxApplicantAddressLine1()
	{
		return (TextField)getChild(CHILD_TXAPPLICANTADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public TextField getTxApplicantCity()
	{
		return (TextField)getChild(CHILD_TXAPPLICANTCITY);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbApplicantAddressProvince()
	{
		return (ComboBox)getChild(CHILD_CBAPPLICANTADDRESSPROVINCE);
	}

	/**
	 *
	 *
	 */
	static class CbApplicantAddressProvinceOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbApplicantAddressProvinceOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "PROVINCE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxApplicantPostalCodeFSA()
	{
		return (TextField)getChild(CHILD_TXAPPLICANTPOSTALCODEFSA);
	}


	/**
	 *
	 *
	 */
	public TextField getTxApplicantLDU()
	{
		return (TextField)getChild(CHILD_TXAPPLICANTLDU);
	}


	/**
	 *
	 *
	 */
	public TextField getTxApplicantAddressLine2()
	{
		return (TextField)getChild(CHILD_TXAPPLICANTADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbApplicantAddressStatus()
	{
		return (ComboBox)getChild(CHILD_CBAPPLICANTADDRESSSTATUS);
	}
	//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	/**
	 * getTxStreetNumber <br>
	 * @return TextField <br>
	 * 	 txStreetNumber
	 */
	public TextField getTxStreetNumber()
	{
		return (TextField)getChild(CHILD_TXSTREETNUMBER);
	}
	/**
	 * getTxStreetName <br>
	 * @return TextField <br>
	 * 	 txStreetName
	 */
	public TextField getTxStreetName()
	{
		return (TextField)getChild(CHILD_TXSTREETNAME);
	}
		
	/**
	 * getCbStreetType <br>
	 * @return ComboBox <br>
	 * 	 CbStreetType
	 */
	public ComboBox getCbStreetType()
	{
		return (ComboBox)getChild(CHILD_CBSTREETTYPE);
	}

	/**
	 * getCbStreetDirection <br>
	 * @return ComboBox <br>
	 * 	 CbStreetDirection
	 */
	public ComboBox getCbStreetDirection()
	{
		return (ComboBox)getChild(CHILD_CBSTREETDIRECTION);
	}
		
	/**
	 * getTxUnitNumber <br>
	 * @return TextField <br>
	 * 	 txUnitNumber
	 */
	public TextField getTxUnitNumber()
	{
		return (TextField)getChild(CHILD_TXUNITNUMBER);
	}	
	//	***** Change by NBC Impl. Team - Version 1.2 - End*****//

	/**
	 *
	 *
	 */
	static class CbApplicantAddressStatusOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbApplicantAddressStatusOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                                   "BORROWERADDRESSTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}
//	***** Change by NBC Impl. Team - Version 1.2 - Start*****//

	/**
	 *
	 *
	 */
	static class CbStreetTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbStreetTypeOptionList()
		{

		}


		/**
		 * populate
		 * This method sets the streettype picklist values in session
		 * @param RequestContext
		 * 
		 */
		public void populate(RequestContext rc)
		{
		 try
		 {
		   //Get Language from SessionState
		   //Get the Language ID from the SessionStateModel
		   String defaultInstanceStateName =
		               rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
		   SessionStateModelImpl theSessionState =
		                                         (SessionStateModelImpl)rc.getModelManager().getModel(
		                                         SessionStateModel.class,
		                                         defaultInstanceStateName,
		                                         true);
		   int languageId = theSessionState.getLanguageId();
		
		   //Get IDs and Labels from BXResources
		   Collection c 
               = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                      "STREETTYPE", languageId);
		       Iterator l = c.iterator();
		       String[] theVal = new String[2];
		       while(l.hasNext())
		       {
		         theVal = (String[])(l.next());
		         add(theVal[1], theVal[0]);
		       }
					}
					catch (Exception ex) {
						ex.printStackTrace();
					}
		   }
	}
	/**
	 *
	 *
	 */
	static class CbStreetDirectionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbStreetDirectionOptionList()
		{

		}


		/**
		 * populate
		 * This method sets the stretdirection picklist values in session
		 * @param RequestContext
		 * 
		 */
		public void populate(RequestContext rc)
		{
		 try
		 {
		   //Get Language from SessionState
		   //Get the Language ID from the SessionStateModel
		   String defaultInstanceStateName =
		               rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
		   SessionStateModelImpl theSessionState =
		                                         (SessionStateModelImpl)rc.getModelManager().getModel(
		                                         SessionStateModel.class,
		                                         defaultInstanceStateName,
		                                         true);
		   int languageId = theSessionState.getLanguageId();
		
		   //Get IDs and Labels from BXResources
		   Collection c 
               = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                      "STREETDIRECTION", languageId);
		       Iterator l = c.iterator();
		       String[] theVal = new String[2];
		       while(l.hasNext())
		       {
		         theVal = (String[])(l.next());
		         add(theVal[1], theVal[0]);
		       }
					}
					catch (Exception ex) {
						ex.printStackTrace();
					}
		   }
	}

//	***** Change by NBC Impl. Team - Version 1.2 - End*****//

	/**
	 *
	 *
	 */
	public TextField getTxApplicantTimeYears()
	{
		return (TextField)getChild(CHILD_TXAPPLICANTTIMEYEARS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxApplicantTimeMonths()
	{
		return (TextField)getChild(CHILD_TXAPPLICANTTIMEMONTHS);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbApplicantAddResidentialStatus()
	{
		return (ComboBox)getChild(CHILD_CBAPPLICANTADDRESIDENTIALSTATUS);
	}

	/**
	 *
	 *
	 */
	static class CbApplicantAddResidentialStatusOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbApplicantAddResidentialStatusOptionList()
		{

		}



		/**
		 *
		 *
		 */
    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "RESIDENTIALSTATUS", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public void handleBtDeleteApplicantAddressRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btDeleteApplicantAddress_onWebEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
    handler.handleDelete(false, 6, handler.getRowNdxFromWebEventMethod(event), "RepeatedApplicantAddress/appAddressId", "borrowerAddressId", "RepeatedApplicantAddress/appAddressCopyId", "BorrowerAddress");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteApplicantAddress()
	{
		return (Button)getChild(CHILD_BTDELETEAPPLICANTADDRESS);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteApplicantAddressDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btDeleteApplicantAddress_onBeforeHtmlOutputEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
      return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public doApplicantEntryAddressSelectModel getdoApplicantEntryAddressSelectModel()
	{
		if (doApplicantEntryAddressSelect == null)
			doApplicantEntryAddressSelect = (doApplicantEntryAddressSelectModel) getModel(doApplicantEntryAddressSelectModel.class);
		return doApplicantEntryAddressSelect;
	}


	/**
	 *
	 *
	 */
	public void setdoApplicantEntryAddressSelectModel(doApplicantEntryAddressSelectModel model)
	{
			doApplicantEntryAddressSelect = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_APPADDRESSID="appAddressId";
	public static final String CHILD_APPADDRESSID_RESET_VALUE="";
	public static final String CHILD_APPADDRESSCOPYID="appAddressCopyId";
	public static final String CHILD_APPADDRESSCOPYID_RESET_VALUE="";
	public static final String CHILD_APPADDRESSADDRID="appAddressAddrId";
	public static final String CHILD_APPADDRESSADDRID_RESET_VALUE="";
	public static final String CHILD_APPADDRESSADDRCOPYID="appAddressAddrCopyId";
	public static final String CHILD_APPADDRESSADDRCOPYID_RESET_VALUE="";
	public static final String CHILD_TXAPPLICANTADDRESSLINE1="txApplicantAddressLine1";
	public static final String CHILD_TXAPPLICANTADDRESSLINE1_RESET_VALUE="";
	public static final String CHILD_TXAPPLICANTCITY="txApplicantCity";
	public static final String CHILD_TXAPPLICANTCITY_RESET_VALUE="";
	public static final String CHILD_CBAPPLICANTADDRESSPROVINCE="cbApplicantAddressProvince";
	public static final String CHILD_CBAPPLICANTADDRESSPROVINCE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbApplicantAddressProvinceOptionList cbApplicantAddressProvinceOptions=new CbApplicantAddressProvinceOptionList();
	private CbApplicantAddressProvinceOptionList cbApplicantAddressProvinceOptions=new CbApplicantAddressProvinceOptionList();
	public static final String CHILD_TXAPPLICANTPOSTALCODEFSA="txApplicantPostalCodeFSA";
	public static final String CHILD_TXAPPLICANTPOSTALCODEFSA_RESET_VALUE="";
	public static final String CHILD_TXAPPLICANTLDU="txApplicantLDU";
	public static final String CHILD_TXAPPLICANTLDU_RESET_VALUE="";
	public static final String CHILD_TXAPPLICANTADDRESSLINE2="txApplicantAddressLine2";
	public static final String CHILD_TXAPPLICANTADDRESSLINE2_RESET_VALUE="";
	public static final String CHILD_CBAPPLICANTADDRESSSTATUS="cbApplicantAddressStatus";
	public static final String CHILD_CBAPPLICANTADDRESSSTATUS_RESET_VALUE="";
  //--Release2.1--//
	//private static CbApplicantAddressStatusOptionList cbApplicantAddressStatusOptions=new CbApplicantAddressStatusOptionList();
	private CbApplicantAddressStatusOptionList cbApplicantAddressStatusOptions=new CbApplicantAddressStatusOptionList();
	public static final String CHILD_TXAPPLICANTTIMEYEARS="txApplicantTimeYears";
	public static final String CHILD_TXAPPLICANTTIMEYEARS_RESET_VALUE="";
	public static final String CHILD_TXAPPLICANTTIMEMONTHS="txApplicantTimeMonths";
	public static final String CHILD_TXAPPLICANTTIMEMONTHS_RESET_VALUE="";
	public static final String CHILD_CBAPPLICANTADDRESIDENTIALSTATUS="cbApplicantAddResidentialStatus";
	public static final String CHILD_CBAPPLICANTADDRESIDENTIALSTATUS_RESET_VALUE="";
  //--Release2.1--//
	//private static CbApplicantAddResidentialStatusOptionList cbApplicantAddResidentialStatusOptions=new CbApplicantAddResidentialStatusOptionList();
	private CbApplicantAddResidentialStatusOptionList cbApplicantAddResidentialStatusOptions=new CbApplicantAddResidentialStatusOptionList();
	public static final String CHILD_BTDELETEAPPLICANTADDRESS="btDeleteApplicantAddress";
	public static final String CHILD_BTDELETEAPPLICANTADDRESS_RESET_VALUE=" ";

	//	***** Change by NBC Impl. Team - Version 1.2 - Start*****//
	private CbStreetTypeOptionList cbStreetTypeOptions= new CbStreetTypeOptionList();
	private CbStreetDirectionOptionList cbStreetDirectionOptions= new CbStreetDirectionOptionList();
	
	public static final String CHILD_TXSTREETNUMBER = "txStreetNumber";
	public static final String CHILD_TXSTREETNUMBER_RESET_VALUE = "";
	
	public static final String CHILD_TXSTREETNAME = "txStreetName";
	public static final String CHILD_TXSTREETNAME_RESET_VALUE = "";
	
	public static final String CHILD_CBSTREETTYPE="cbStreetType";
	public static final String CHILD_CBSTREETTYPE_RESET_VALUE="";
	
	public static final String CHILD_CBSTREETDIRECTION="cbStreetDirection";
	public static final String CHILD_CBSTREETDIRECTION_RESET_VALUE="";
	
	public static final String CHILD_TXUNITNUMBER = "txUnitNumber";
	public static final String CHILD_TXUNITNUMBER_RESET_VALUE = "";
//	***** Change by NBC Impl. Team - Version 1.2 - End*****//

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doApplicantEntryAddressSelectModel doApplicantEntryAddressSelect=null;

  private ApplicantEntryHandler handler=new ApplicantEntryHandler();

  public SysLogger logger;
}

