package mosApp.MosSystem;

import static mosApp.MosSystem.pgMWorkQueueViewBean.CHILD_CBTASKEXPIRYDATEENDFILTER;
import static mosApp.MosSystem.pgMWorkQueueViewBean.CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE;
import static mosApp.MosSystem.pgMWorkQueueViewBean.CHILD_CBTASKEXPIRYDATESTARTFILTER;
import static mosApp.MosSystem.pgMWorkQueueViewBean.CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.GroupProfile;
import com.basis100.deal.entity.RegionProfile;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.ListBox;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 */

//==================================================================
// Use PageCondition3 to indicate if display Hidden Task or not:
//    false (default) : Not display
//    true : Dispaly
// Modified by Billy 08Oct2002
//==================================================================
//--TD_MWQ_CR--//
//==================================================================
// Use PageCondition5 to narrow task display based on the braches' selection:
//    false (default) : do not narrow
//    true : narrow based on the user's selection in the listbox.
//==================================================================
//==================================================================
// Use PageCondition6 to narrow task display based on the group's selection:
//    false (default) : do not narrow
//    true : narrow based on the user's selection in the listbox.
//==================================================================
//==================================================================
// Use PageCondition7 in conjunction with the option to allow displaying
// of the tasks on the MasterWorkqueue screen narrowed by region, branch, or group.
//    false (default) : do not apply narrowing
//    true : narrow based on the mossys.properties (see options below).
//    System should also cancel the narrowing if any filter option for branch or
//    group is applied.
//    -- "R" (narrow by group)
//    -- "B" (narrow by branch)
//    -- "G" (narrow by group)
//    -- default = N (display all tasks)
//==================================================================
//==================================================================
//Use PageCondition4 was used for taskName index for filter 
// however, since ML, task Id is not common among institutions, 
// so that pageEntry.mwqTaskNameFilter was added instead.
// bug fixed for FXP26606
//==================================================================

public class MasterWorkQueueHandler
	extends PageHandlerCommon
	implements Cloneable, Sc {

  private static final Log logger = LogFactory.getLog(MasterWorkQueueHandler.class);

	static final int REALM_TYPE_ANY = 0;
	static final int REALM_TYPE_REGION = 1;
	static final int REALM_TYPE_BRANCH = 2;
	static final int REALM_TYPE_GROUP = 3;
	static final int TRANCATED_LOCATION_LENGTH = 13;
	static final int TRANCATED_TASKNAME_LENGTH = 9;

	////////////////////////////////////////////////////////////////////////
	// on this page the 10th (index 9) filter option on the cursor object
	// is used to hold boolean for priority tasks displayed
	private static final int DISP_PRIORITY_TASKS = 9;
	
	private static final String PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_START = "MWQByExpiryDateStart";
	private static final String PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_END = "MWQByExpiryDateEnd";
	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    // the instution where clause,

	/**
	 *
	 *
	 */
	public MasterWorkQueueHandler cloneSS() {
		return (MasterWorkQueueHandler) super.cloneSafeShallow();
	}

	////////////////////////////////////////////////////////////////////////
	// OVERRIDE
	public void preHandlerProtocol(ViewBean currNDPage) {
		super.preHandlerProtocol(currNDPage);

		// sefault page load method to display()
		PageEntry pg = theSessionState.getCurrentPage();
		pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);
	}

	public void setupBeforePageGeneration() {
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) {
			return;
		}

		pg.setSetupBeforeGenerationCalled(true);

		// -- //
		// cursor object for work queue tasks list (named "DEFAULT" for this page)
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		//// Debug tds parameters if it is not null.
		//if(tds != null) {
		//  logger.debug("MWQH@setupBeforePageGen::PstName: " + tds.getName());
		//  logger.debug("MWQ@setupBeforePageGen::TotalNumberOfRows: " + tds.getTotalRows());
		//  logger.debug("MWQ@setupBeforePageGen::RelIndex: " + tds.getRelSelectedRowNdx());
		//  logger.debug("MWQ@setupBeforePageGen::CurrPageNdx: " + tds.getCurrPageNdx() + " RowsPerPage:" + tds.getRowsPerPage());
		//  logger.debug("MWQ@setupBeforePageGen::AbsIndex: " + tds.getAbsSelectedRowNdx());
		//}
		Hashtable pst = pg.getPageStateTable();

		//// Check if need to refresh the PageStateHandler.
		boolean refresh = false;
		Boolean theRefreshFlag = (Boolean) pst.get("REFRESH");

		if (theRefreshFlag != null) {
			refresh = theRefreshFlag.booleanValue();
		}

		//// Set all parameters is tds is null.
		if ((tds == null) || (refresh == true)) {
			//logger.debug("MWQ@setupBeforePageGen::tds equals null mode");
			//logger.debug("MWQ@setupBeforePageGen::tds: " + tds);
			//logger.debug("MWQ@setupBeforePageGen::refresh: " + refresh);
			//// Reset Refresh flag from PST first, new feature.
			pst.put("REFRESH", new Boolean(false));

			//logger.debug("MWQH@setupBeforePageGen:: tds == null :: Create new tds !!");
			// determine number of task (shown) per display page
			String voName = "Repeated1";

			int tasksPerPage =
				((TiledView) (getCurrNDPage().getChild(voName)))
					.getMaxDisplayTiles();

			//logger.debug("MWQ@setupBeforePageGen::TasksPerPageTds: " + tasksPerPage);
			// set up and save task display state object
			tds = new PageCursorInfo("DEFAULT", voName, tasksPerPage, 0);

			// last (0) - don't know total number yet!
			tds.setRefresh(true);
			tds.setSortOption(Mc.MWQ_SORT_DUE_DATE);
			pg.setPageCriteriaId2(0);
			pg.setPageCriteriaId1(-1);
			pg.setPageCriteriaId3(0);
            // FXP26606
			pg.setMWQTaskNameFilter("");

			//-- ========== SCR#750 begins ========== --//
			//-- by Neil on Dec/22/2004
			//-- new filter: by Deal Status (PageCriteriaId6)
            // SEAN Dec 19, 2005: should be -1 for all status.
            pg.setPageCriteriaId6(-1);

			//-- ========== SCR#750   ends ========== --//
			//--TD_MWQ_CR--start//
			//// New filter options: by Task Name (PageCriteriaId4) and by Region (PageCriteriaId5).
			//// Default 'All' option.
			pg.setPageCondition5(false);
			pg.setPageCondition6(false);
			pg.setPageCondition7(false);

			//--TD_MWQ_CR--end//
			// Added checkbox to display Hidden tasks or not
			// By Billy 08Oct2002
			pg.setPageCondition3(false);
			setFilterCriteria();

			//==============================================
			tds.setFilterOption(0, false);
			pst.put(tds.getName(), tds);

			//--TD_MWQ_CR--start//
			//// The logic of getLocationCriteria(pst) filters if current user is a manager of his/her
			//// R)egion, (B)ranch, (G)roup. In case he a manager of some outer RBG, for now there is no filtering
			getLocationCriteria(pst);

			//--TD_MWQ_CR--end//
		}

		String voName1 = "Repeated2";
		PageCursorInfo tds1 = getPageCursorInfo(pg, voName1);

		if (tds1 == null) {
			//logger.debug("MWQ@setupBeforePageGen::tds1 equals null mode");
			//logger.debug("MWQ@setupBeforePageGen::tds1: " + tds);
			//logger.debug("MWQ@setupBeforePageGen::refresh: " + refresh);
			// determine number of task (shown) per display page
			int tasksPerPage = ((TiledView) (getCurrNDPage().getChild(voName1))).getMaxDisplayTiles();
			logger.debug("MWQ@setupBeforePageGen::TaskPerPageTds1: " + tasksPerPage);

			// set up and save task display state object
			tds1 = new PageCursorInfo(voName1, voName1, tasksPerPage, 0);

			// last (0) - don't know total number yet!
			tds1.setRefresh(true);
			pst.put(tds1.getName(), tds1);
		}

		int numOfRows = 0;

		doPriorityModel outstandDO = (doPriorityModel) (RequestManager.getRequestContext().getModelManager().getModel(doPriorityModel.class));

		try {
			ResultSet rs = outstandDO.executeSelect(null);

			if ((rs == null) || (outstandDO.getSize() < 0)) {
				logger.debug("MWQ@setupBeforePageGen::No row returned!!");
				numOfRows = 0;
			} else if ((rs != null) && (outstandDO.getSize() > 0)) {
//logger.debug("MWQ@setupBeforePageGen::Size of the result set: " + outstandDO.getSize());
				numOfRows = outstandDO.getSize();
			}
		} catch (ModelControlException mce) {
			logger.debug("MWQ@setupBeforePageGen::MCEException: " + mce);
		} catch (SQLException sqle) {
			logger.debug("MWQ@setupBeforePageGen::SQLException: " + sqle);
		}

		tds1.setTotalRows(numOfRows);

		//--TD_MWQ_CR--start--//
		//// TD wants to display outstanding tasks counted by Task Name
		//// currently for two tasks:
		//// Decusion Deal (taskId = 3) and
		//// Source Resubmission (taskId = 258).
		String voName3 = "Repeated3";
		PageCursorInfo tds3 = getPageCursorInfo(pg, voName3);

		if (tds3 == null) {
			logger.debug("MWQ@setupBeforePageGen::tds3 equals null mode");

			// determine number of task (shown) per display page
			int tasksPerPage =
				((TiledView) (getCurrNDPage().getChild(voName3)))
					.getMaxDisplayTiles();
			logger.debug(
				"MWQ@setupBeforePageGen::TaskPerPageTds3: " + tasksPerPage);

			// set up and save task display state object
			tds3 = new PageCursorInfo(voName3, voName3, tasksPerPage, 0);

			// last (0) - don't know total number yet!
			tds3.setRefresh(true);
			logger.debug(
				"MWQ@setupBeforePageGen::Tds3::name: " + tds3.getName());

			pst.put(tds3.getName(), tds3);
		}

		doTasksOutstandingsModel outstandTaskByName =
			(doTasksOutstandingsModel) (RequestManager
				.getRequestContext()
				.getModelManager()
				.getModel(doTasksOutstandingsModel.class));

		try {
			ResultSet rs = outstandTaskByName.executeSelect(null);

			if ((rs == null) || (outstandTaskByName.getSize() < 0)) {
				logger.debug("MWQ@setupBeforePageGen::No row returned!!");
				numOfRows = 0;
			} else if ((rs != null) && (outstandTaskByName.getSize() > 0)) {
				logger.debug(
					"MWQ@setupBeforePageGen::Size of the result set: "
						+ outstandTaskByName.getSize());
				numOfRows = outstandTaskByName.getSize();
			}
		} catch (ModelControlException mce) {
			logger.debug("MWQ@setupBeforePageGen::MCEException: " + mce);
		} catch (SQLException sqle) {
			logger.debug("MWQ@setupBeforePageGen::SQLException: " + sqle);
		}

		tds3.setTotalRows(numOfRows);

		//--TD_MWQ_CR--end--//
		//--> Performance enhancement : elimination of the 2nd call of the DataObject
		//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
		//--> the hidden values on each row.
		//--> By Billy 16Jan2004
		// ensure data objects have correct criteria ...
		doMasterWorkQueueModelImpl wqDO = (doMasterWorkQueueModelImpl) (RequestManager.getRequestContext().getModelManager().getModel(doMasterWorkQueueModel.class));
		doMasterWorkQueueCountModelImpl wqCountDO = (doMasterWorkQueueCountModelImpl) (RequestManager.getRequestContext().getModelManager().getModel(doMasterWorkQueueCountModel.class));

		try
                {
			// Set both DOs up based on the filters user picked
			buildDataObject(pg, tds, wqDO, wqCountDO);

			// Here execute only the Count DataObject
			ResultSet rs = wqCountDO.executeSelect(null);

			if ((rs != null) && (wqCountDO.getSize() > 0))
                        {
				int tmpCount = getIntValue(wqCountDO.getValue(doMasterWorkQueueCountModel.FIELD_DFWQNUMOFROWS).toString());
				tds.setTotalRows(tmpCount);
//logger.debug("MWQH@ The Total Rows : " + tmpCount);
			}
                        else
                        {
				tds.setTotalRows(0);
			}
		} catch (Exception ex) {
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("MWQH@Problem encountered getting work queue tasks");
			logger.error(ex);

			return;
		}

		// determine if we will force display of first page
		if (tds.isTotalRowsChanged() || tds.isCriteriaChanged()) {
			// number of rows has changed - unconditional display of first page
			pgMWorkQueueRepeated1TiledView vo =
				((pgMWorkQueueViewBean) getCurrNDPage()).getRepeated1();

			//logger.debug("MWH@setupBeforePageGen::Repeated1Name: " + vo.getName());
			tds.setCurrPageNdx(0);

			////Reset the primary model to the "before first" state and reset the display index.
			try {
        vo.resetTileIndex();
      } catch (ModelControlException mce1) {
        logger.error("MWQH@setupBeforePageGeneration::Exception: ", mce1);
      }

			tds.setTotalRowsChanged(false);
			tds.setCriteriaChanged(false);

			//--> Reset selected info.
			//--> Bug fix By Billy 09Feb2004
			pst.put("DEALID", new Integer(0));
			pst.put("COPYID", new Integer(0));
			pst.put("DEALAPPID", "0");
            pst.put("INSTITUTIONID", new Integer(-1));

            pst.put("MULTIACCESS", "" + theSessionState.isMultiAccess());
			tds.setRelSelectedRowNdx(-1);

		}

		// set application Id for deal summary snapshot
		logger.debug("MWQH@setupBeforePageGeneration PageEntry dealid: " + pg.getPageDealId());
		setDealForSnapshot(pg, tds);
		logger.debug("MWQH@setupBeforePageGeneration set after snapshot: " + pg.getPageDealId());

		setupDealSummarySnapShotDO(pg);

		logger.debug("MWQH@setupBeforePageGeneration::FINISHED!!!");
	}

	/**
	 *
	 *
	 */
	public void setTaskCountByPriority(int rowNdx, Integer priorityId) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		String taskCount = "0";
		String sqlStr = "";
		String strCrit = "";

		try
                {
			doCountTasksbyPriorityModel doPSum = (doCountTasksbyPriorityModel) (RequestManager
					.getRequestContext()
					.getModelManager()
					.getModel(doCountTasksbyPriorityModel.class));
			((doCountTasksbyPriorityModelImpl) doPSum).clearUserWhereCriteria();

			//// IMPORTANT: AddDynamicStrCriterion() method has no analog in JATO
			//// Model search criteria. To simulate it we need first to create/execute
			//// the appropriate sql and add the regular JATO addUserWhereCriterion
			//// after this. This approach should be implemented in the
			//// PartySearchHandler as well.
			////doPSum.addDynamicStrCriterion(" ASSIGNEDTASKSWORKQUEUE.PRIORITYID = " + priorityId + " ");
			sqlStr += ("(ASSIGNEDTASKSWORKQUEUE.PRIORITYID = " + priorityId.toString()+ ")");

			//logger.debug("MWQH@setTaskCountByPriority::PriorityId: " + priorityId.toString());
			//// Because we could not to append the theModel.STATIC_WHERE_CRITERIA
			//// this method is eliminated and filter criteria is set up right here.
			////applyFilterCriteria(doPSum);
			sqlStr += " AND DEAL.COPYTYPE <> 'T'";

			strCrit = (String) pst.get("FilterCriteria");
//logger.debug("MWQH@setTaskCountByPriority::FilterCriteria = " + strCrit);

			if ((strCrit != null) && (strCrit.trim().length() > 0)) {
				sqlStr += (" AND " + strCrit);
			}
//logger.debug("MWQH@setTaskCountByPriority::StrCriteria before PageConditions = " + sqlStr);

			//--TD_MWQ_CR--start//
			if (!sqlStr.trim().equals("")) {
				sqlStr += (" AND " + getPageConditions(pg, pst));
			} else {
				sqlStr += getPageConditions(pg, pst);
			}

			//--TD_MWQ_CR--end//
			String str = doCountTasksbyPriorityModelImpl.STATIC_WHERE_CRITERIA + " AND " + sqlStr;
			doCountTasksbyPriorityModelImpl doPSumImpl = (doCountTasksbyPriorityModelImpl) doPSum;
			doPSumImpl.setStaticWhereCriteriaString(str);

logger.debug("MWQH@setTaskCountByPriority::SQL before execute = " + doPSumImpl.getSelectSQL());

			ResultSet rs = doPSum.executeSelect(null);

			if ((rs != null) && (doPSum.getSize() > 0))
                        {
				taskCount = doPSum.getValue(doCountTasksbyPriorityModelImpl.FIELD_DFPRIORITYCOUNT).toString();

//logger.debug("MWQH@setTaskCountByPriority::taskCount: " + taskCount.toString());
//logger.debug("MWQH@setTaskCountByPriority::Size of the taskCount result set: " + doPSum.getSize());
			}
                        else
                        {
				logger.error("MWQH@setTaskCountByPriority::Problem when getting the TaskCount info !!");
			}
		}
                catch (Exception e)
                {
			logger.error(e);
			logger.error("MWQH@setTaskCountByPriority::Index = " + rowNdx + ", PriorityId = "+ priorityId);
		}

		getCurrNDPage().setDisplayFieldValue("Repeated2/stPriorityCount", taskCount);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param rowNdx
	 * @param taskId
	 */
	public void setTaskCountByTaskName(int rowNdx, Integer taskId)
        {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		String taskCount = "0";
		String sqlStr = "";
		String strCrit = "";

		try
                {
			doCountTasksByTaskNameModel mTaskNum = (doCountTasksByTaskNameModel) (RequestManager.getRequestContext().getModelManager().getModel(doCountTasksByTaskNameModel.class));

			doCountTasksByTaskNameModelImpl mTaskNumImpl = (doCountTasksByTaskNameModelImpl) mTaskNum;
			mTaskNumImpl.clearUserWhereCriteria();

//logger.debug("MWQH@setTaskCountByTaskName::Template_AfterClear = " + ((doCountTasksByTaskNameModelImpl) mTaskNum).getSelectSQL());

			StringBuffer sqlBuff =
				new StringBuffer("(ASSIGNEDTASKSWORKQUEUE.TASKID = ");
			sqlBuff.append(taskId);
			sqlBuff.append(") ");
//logger.debug("MWQH@setTaskCountByTaskName::TaskId = " + taskId.toString());

			sqlBuff.append(" AND DEAL.COPYTYPE <> 'T'");

			strCrit = (String) pst.get("FilterCriteria");
//logger.debug("MWQH@setTaskCountByTaskName::FilterCriteria = " + strCrit);

			if ((strCrit != null) && (strCrit.trim().length() > 0)) {
				sqlBuff.append(" AND ");
				sqlBuff.append(strCrit);
			}

			sqlStr += sqlBuff.toString();

//logger.debug("MWQH@setTaskCountByTaskName::StrCriteria before Page Conditions: " + sqlStr);

			//--TD_MWQ_CR--start//
			if (!sqlStr.trim().equals("")) {
				sqlStr += (" AND " + getPageConditions(pg, pst));
			} else {
				sqlStr += getPageConditions(pg, pst);
			}

			//--TD_MWQ_CR--end//
//logger.debug("MWQH@setTaskCountByTaskName::StaticWhereCriteria = " + doCountTasksByTaskNameModelImpl.STATIC_WHERE_CRITERIA);

			mTaskNumImpl.setStaticWhereCriteriaString(doCountTasksByTaskNameModelImpl.STATIC_WHERE_CRITERIA
					                          + " AND "
					                          + sqlStr);

logger.debug("MWQH@setTaskCountByTaskName::SQL before execute = " + mTaskNumImpl.getSelectSQL());

			ResultSet rs = mTaskNum.executeSelect(null);

			if ((rs != null) && (mTaskNum.getSize() > 0))
                        {
				taskCount = mTaskNum.getValue(doCountTasksByTaskNameModelImpl.FIELD_DFTASKNAMECOUNT).toString();
//logger.debug("MWQH@setTaskCountByTaskName::TaskCount = " + taskCount.toString());
//logger.debug("MWQH@setTaskCountByTaskName::Size of the taskCount result set = " + mTaskNum.getSize());
			}
                        else
                        {
				logger.error("MWQH@setTaskCountByTaskName::Problem when getting the TaskCount info !!");
			}
		}
                catch (Exception e)
                {
			logger.error(e);
			logger.error("MWQH@setTaskCountByTaskName::Index = " + rowNdx + ", TaskId = " + taskId);
		}

		getCurrNDPage().setDisplayFieldValue("Repeated3/stTaskNameCount", taskCount);
	}

	/**
	 * Extracted from setTaskCountByXXX.
	 *
	 * @param pg
	 * @param pst
	 *
	 * @return
	 */
	String getPageConditions(PageEntry pg, Hashtable pst)
        {
		StringBuffer sqlBuff = null;

		//		if (!sqlStr.trim().equals(""))
		//			sqlStr += " AND ";
		//
		//		sqlStr += " ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1";
		// I. Display or not display hidden tasks.
		String str = null;

		if (pg.getPageCondition3() == false) {
			str = " ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1 ";
			sqlBuff = new StringBuffer(str);
		}
                else if (pg.getPageCondition3() == true) {
                        str = " (ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0 OR ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1)";
                        sqlBuff = new StringBuffer(str);
                }

		// II. TD requests to set up two extra filters: by Task Name and by Region/Branch/Group.
		// 1. By Task Name.
		// fixed for FXP26606
		if (pg.getMWQTaskNameFilter().length() > 0) {

            sqlBuff.append(getTaskNameCriteria(pg.getMWQTaskNameFilter()));
		}

		// 2. By Branch(es).
		if (pg.getPageCondition5() == true) {

            String branchCriteria = getInstitutionCriteria((Map<Integer,Integer>)pst.get("branches"));
            sqlBuff.append( branchCriteria);

		}

		// 3. By Groups.
		if (pg.getPageCondition6() == true) {
			Vector groups = (Vector) pst.get("groups");

			if (groups != null) {
				str = "MWH@getPageConditions::NumberOfGroupsSelected = ";
				logger.debug(str + groups.size());

				if (groups.size() == 1) {
					str = " AND (GROUPPROFILE.GROUPPROFILEID = ";
					sqlBuff.append(str);
					sqlBuff.append(groups.elementAt(0));
					sqlBuff.append(") ");
				} else if (groups.size() > 1) {
					for (int i = 0; i < groups.size(); i++) {
						str = "MWH@getPageConditions::GroupIdAdded: ";
						logger.debug(str + groups.elementAt(i));

						if (i == 0) {
							str = " AND (GROUPPROFILE.GROUPPROFILEID = ";
						} else {
							str = " OR (GROUPPROFILE.GROUPPROFILEID = ";
						}

						sqlBuff.append(str);
						sqlBuff.append(groups.elementAt(i));
					}

					sqlBuff.append(") ");
				}
			}
		}

		//-- ========== SCR#750 begins ========== --//
		//-- by Neil on Dec/21/2004
		//-- BMO 1.5 wants to filter by Deal Status
		//-- 4.By Deal Status.

//logger.debug("MWH@getPageConditions::pg.getPageCondition5 = " + pg.getPageCondition5());
//logger.debug("MWH@getPageConditions::pg.getPageCondition6 = " + pg.getPageCondition6());
//logger.debug("MWH@getPageConditions::pg.getPageCondition7 = " + pg.getPageCondition7());
               //--Ticket#1196--11Apr2005--start--//
               // Deal Status Filtering has to include boundary case 0: Incomplete status
		if (pg.getPageCriteriaId6() >= 0)
                //--Ticket#1196--11Apr2005--end--//
                {
			str = " AND (DEAL.STATUSID = ";
			sqlBuff.append(str);
			sqlBuff.append(pg.getPageCriteriaId6());
			sqlBuff.append(") ");
		}
		//-- ========== SCR#750   ends ========== --//
		// TD requests to display tasks narrowed by branch.
		// This option is implemented for region
		// and group as well for other clients.
		int realmId = getRealm();
logger.debug("MWH@getPageConditions::SqlStrBeforeRealm = " + sqlBuff.toString());

		logger.debug("MWH@getPageConditions::RealmId = " + realmId);

		// If a narrow criteria exists and both branch and group filtering
		// criteria are false the locationCriteria should be applied.
		if ((realmId > 0) && (pg.getPageCondition5() == false)
                    && (pg.getPageCondition6() == false))
                {
			pg.setPageCondition7(true);
		}

		if (pg.getPageCondition7() == true) {
			String locationCriteria = addLocationCriteria(pst, realmId);
			logger.debug("MWH@getPageConditions:LocationCriteria = " + locationCriteria);
			sqlBuff.append(locationCriteria);
		}

		String sql = sqlBuff.toString();
logger.debug("MWH@getPageConditions::SqlStrBefore_STATIC_WHERE_CRIT = " + sql);

		return sql;
	}

	//--TD_MWQ_CR--end--//
	//-- ========== SCR#750 begins ========== --//
	//-- by Neil on Dec/21/2004

	/**
	 * DOCUMENT ME!
	 *
	 * @param rowNdx
	 * @param dealStatusId
	 */
	public void setTaskCountByDealStatus(int rowNdx, Integer dealStatusId) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		String taskCount = "0";
		String sqlStr = "";
		String strCrit = "";

		try {
			doCountTasksByTaskNameModel mTaskNum =
				(doCountTasksByTaskNameModel) (RequestManager
					.getRequestContext()
					.getModelManager()
					.getModel(doCountTasksByTaskNameModel.class));

			doCountTasksByTaskNameModelImpl mTaskNumImpl =
				(doCountTasksByTaskNameModelImpl) mTaskNum;
			mTaskNumImpl.clearUserWhereCriteria();

//logger.debug("MWH@setTaskCountByDealStatus::Template_AfterClear = " + ((doCountTasksByTaskNameModelImpl) mTaskNum).getSelectSQL());

			StringBuffer sqlBuff =
				new StringBuffer("(ASSIGNEDTASKSWORKQUEUE.DEALID = ");
			sqlBuff.append("DEAL.DEALID)");
			sqlBuff.append(" AND (DEAL.STATUSID = ");
			sqlBuff.append(dealStatusId);
			sqlBuff.append(") ");
//logger.debug("MWH@setTaskCountByDealStatus::DealStatusId = " + dealStatusId.toString());

			sqlBuff.append(" AND DEAL.COPYTYPE <> 'T'");

			strCrit = (String) pst.get("FilterCriteria");

//logger.debug("MWH@setTaskCountByDealStatus::FilterCriteria = " + strCrit);

			if ((strCrit != null) && (strCrit.trim().length() > 0)) {
				sqlBuff.append(" AND ");
				sqlBuff.append(strCrit);
			}

			sqlStr += sqlBuff.toString();

//logger.debug("MWH@setTaskCountByDealStatus::StrCriteria before PageConditions: " + sqlStr);

			//--TD_MWQ_CR--start//
			if (!sqlStr.trim().equals("")) {
				sqlStr += (" AND " + getPageConditions(pg, pst));
			} else {
				sqlStr += getPageConditions(pg, pst);
			}

			//--TD_MWQ_CR--end//
			logger.debug("MWH@setTaskCountByDealStatus::StaticWhereCriteria = "
					+ doCountTasksByTaskNameModelImpl.STATIC_WHERE_CRITERIA);

			mTaskNumImpl.setStaticWhereCriteriaString(
				doCountTasksByTaskNameModelImpl.STATIC_WHERE_CRITERIA
					+ " AND "
					+ sqlStr);

logger.debug("MWH@setTaskCountByDealStatus::SQL before execute = " + mTaskNumImpl.getSelectSQL());

			ResultSet rs = mTaskNum.executeSelect(null);

			if ((rs != null) && (mTaskNum.getSize() > 0)) {
				taskCount = mTaskNum.getValue(doCountTasksByTaskNameModelImpl.FIELD_DFTASKNAMECOUNT).toString();
//logger.debug("MWH@setTaskCountByDealStatus::TaskCount = " + taskCount.toString());
//logger.debug("MWH@setTaskCountByDealStatus::Size of the taskCount result set = " + mTaskNum.getSize());
			}
                        else
                        {
				logger.error("MWH@setTaskCountByDealStatus::Problem when getting the TaskCount info !!");
			}
		} catch (Exception e) {
			logger.error(e);
			logger.error("MWH@setTaskCountByDealStatus::Index = " + rowNdx+ ", DealStatusId = " + dealStatusId);
		}

		//		TODO
		//		getCurrNDPage().setDisplayFieldValue(
		//			"Repeated4/stDealStatusCount",
		//			taskCount);
	}
	//-- ========== SCR#750   ends ========== --//


	private void resetFilterCriteria() {
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		pst.put("FilterCriteria", " ");

		// SEAN Ticket #2457 Nov 30, 2005
		// for the filter by branches.
		pst.put("branches", (new HashMap<Integer, Integer>()));
		// for the filter by users.
		((ComboBox) (getCurrNDPage().getChild("cbUserFilter"))).setValue("", true);
		// for deal status.
		((ComboBox) getCurrNDPage().getDisplayField("cbDealStatusFilter")).setValue("", true);
		// SEAN Ticket #2457 END
		
		((TextField) getCurrNDPage().getChild(CHILD_CBTASKEXPIRYDATESTARTFILTER)).setValue(CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE);
		((TextField) getCurrNDPage().getChild(CHILD_CBTASKEXPIRYDATEENDFILTER)).setValue(CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE);
		pst.remove(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_START);
		pst.remove(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_END);
	}

	////////////////////////////////////////////////////////////////////////
	private void setFilterCriteria()
        {
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		String sqlStr = "";

		if (pg.getPageCriteriaId1() >= 0)
                {
                  doUnderwriter1Model dtDo = (doUnderwriter1Model) (RequestManager.getRequestContext().getModelManager().getModel(doUnderwriter1Model.class));

                  dtDo.clearUserWhereCriteria();
                  dtDo.addUserWhereCriterion("dfUserProfileId", "=", new Integer(pg.getPageCriteriaId1()));

                  String firstName = null;
                  String lastName = null;

                  try
                  {
                          ResultSet rs = dtDo.executeSelect(null);

                          if ((rs != null) && (dtDo.getSize() > 0))
                          {
                              firstName = dtDo.getValue(doUnderwriter1ModelImpl.FIELD_DFFIRSTNAME).toString();
                              lastName = dtDo.getValue(doUnderwriter1ModelImpl.FIELD_DFLASTNAME).toString();
                          }
                          else
                          {
                              logger.error("MWQH@setFilterCriteria::Problem when getting the Underwriter info !!");
                          }
                          
                          // FXP27748 - changed this to use the correct contact table.
                          sqlStr = " C1.CONTACTLASTNAME LIKE '"+ lastName
                                  + "' AND SUBSTR(C1.CONTACTFIRSTNAME,1,1) LIKE '"
                                  + firstName.substring(0, 1) + "' ";

                          logger.debug("MWQH@setFilterCriteria::sqlStr: " + sqlStr);
                  }
                  catch (Exception e)
                  {
                          logger.error(e);
                          logger.error("MasterWorkQueueHandler@getFilterCriteria=");
                          sqlStr = "";
                  }
		}

               //----SCR#1149--start--23Mar2005--//
               // Now based on filter options it should be dynamically setup based on
               // selected option.
               int taskStatusMapper = MWQ_STATUS_TASK_MAPPER[pg.getPageCriteriaId2()];
               logger.debug("MWQH@setFilterCriteria::iTaskStatusMapper: " + taskStatusMapper);

               if (!sqlStr.trim().equals("")) {
                       sqlStr += " AND ";
               }

                switch(taskStatusMapper)
                {
                    case Sc.TASK_OPEN: // open, 1
                    sqlStr += " assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
                    logger.debug("MWQH@setFilterCriteria::Filter Option = by [OPEN]" + sqlStr);
                    break;

                  case Sc.TASK_COMPLETE: // complete, 3
                    sqlStr += " assignedtasksworkqueue.taskstatusid = " + Sc.TASK_COMPLETE;
                    logger.debug("MWQH@setFilterCriteria::Filter Option = by [COMPLETE]" + sqlStr);
                    break;

                  case Mc.MWQ_TASK_NOT_ONHOLD: // on hold, 10
                      sqlStr += " assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
                      sqlStr += " AND DEAL.HOLDREASONID <> " + Mc.HOLD_REASON_NONE;
                      logger.debug("MWQH@setFilterCriteria::Filter Option = by [ON HOLD]" + sqlStr);
                    break;

                  case Mc.MWQ_STATUS_TASK_ALL: // all, 9
                      sqlStr += " ((assignedtasksworkqueue.taskstatusid = " + Sc.TASK_COMPLETE + ") " +
                      " OR (assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN + ") )";
                      logger.debug("MWQH@setFilterCriteria::Filter Option = by [ALL]" + sqlStr);
                    break;

                  default: // OPEN
                    sqlStr += " assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
                    logger.debug("MWQH@setFilterCriteria::Filter Option = by [OPEN]" + sqlStr);
                    break;
                 }
                //----SCR#1149--end--23Mar2005--//

		// Set DealId Filter -- By BILLY 06Dec2001
		if (pg.getPageCriteriaId3() > 0)
        {
			logger.debug("MWQH@setFilterCriteria::PageConditionId3 > 0 mode");

			if (!sqlStr.trim().equals("")) {
				sqlStr += " AND ";
			}

			sqlStr += "( assignedtasksworkqueue.dealid = " + pg.getPageCriteriaId3() + ") ";
			//logger.debug("MWQH@setFilterCriteria::SqlStrPgCond3>0: " + sqlStr);
		}
		
		Date expiryDateStart = (Date) pst.get(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_START);
		Date expiryDateEnd = (Date) pst.get(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_END);
		if (expiryDateStart != null)
			sqlStr += (" AND assignedtasksworkqueue.endtimestamp>='" + TypeConverter.stringTypeFrom(expiryDateStart)) + "' ";
		if (expiryDateEnd != null)
			sqlStr += (" AND assignedtasksworkqueue.endtimestamp<='" + TypeConverter.stringTypeFrom(expiryDateEnd)) + "' ";

		//-- ========== SCR#750 begins ========== --//
		//-- by Neil on Dec/22/2004
		//-- filter by Deal Status
        //--Ticket#1196--11Apr2005--start--//
        // Deal Status Filtering has to include boundary case 0: Incomplete status
        if (pg.getPageCriteriaId6() >= 0)
        //--Ticket#1196--11Apr2005--end--//
        {
			logger.debug("MWQH@setFilterCriteria::Not MQS_DEAL_STATUS_ALL mode");

			if (!sqlStr.trim().equals("")) {
				sqlStr += " AND ";
			}

			sqlStr += ("DEAL.STATUSID = " + pg.getPageCriteriaId6());
			logger.debug("MWQH@setFilterCriteria::SqlStr NOT_DEAL_STATUS_ALL: " + sqlStr);
		}
		//-- ========== SCR#750   ends ========== --//

		//--TD_MWQ_CR--start//
		//// TD requests to set up two extra filters: by Task Name and by Region.
		//// Set up Task Name filter.
		//// 1. New filter by Task Name.
		if (pg.getMWQTaskNameFilter().length() > 0)
        {
            sqlStr += getTaskNameCriteria(pg.getMWQTaskNameFilter());
        }

		// // 2. New filter by location criteria (Region, Branch, Group).
		if ((pst != null) && ((pg.getPageCondition5() == true) || (pg.getPageCondition6() == true))
                    && (pg.getPageCondition7() == false))
                {
                  logger.trace("MWQH@setFilterCriteria::addVariousLocationCriteria()");

//                  LocationWrapper locationWrapper = (LocationWrapper) (pst.get("locationWrapper"));
                  Map<Integer, LocationWrapper> locationMap = (Map<Integer, LocationWrapper>) pst.get("locationWrapper");

                  Iterator locationIter = locationMap.keySet().iterator();

                  while (locationIter.hasNext())  {

                      int instId = Integer.parseInt(locationIter.next()
                              .toString());
                      LocationWrapper locationWrapper = (LocationWrapper)(locationMap.get(instId));

                  if (locationWrapper != null) {
                          //// 1. Add Region Filter Criteria.
                          /*
                             if(!sqlStr.trim().equals(""))
                                 sqlStr += " AND ";

                             ////BX: Reorganize the location wrapper class!!!!
                             sqlStr += "(BRANCHPROFILE.REGIONPROFILEID = " + locationWrapper.getRegionProfileId() + ")";
                             logger.debug("MWQH@setFilterCriteria::SQL_RegionCase: " + sqlStr);
                           */

                          //// 2. Add Bransh(es) Filter Criteria.
                          if (pg.getPageCondition5() == true) {

                              String branchCriteria = getInstitutionCriteria((Map<Integer,Integer>)pst.get("branches"));
                              sqlStr +=  branchCriteria;

                          }

                          //// 3. Add Group(s) Filter Criteria.
                          if (pg.getPageCondition6() == true) {
                                  Vector groups = (Vector) pst.get("groups");
                                  String andOr = " AND "; // default

                                  if (groups != null) {
                                          logger.debug(
                                                  "MWQH@setFilterCriteria::SQL_GroupCase:NumberOfGroupsSelected = "
                                                          + groups.size());

                                          if (groups.size() == 1) {
                                                  sqlStr
                                                          += (" AND (GROUPPROFILE.GROUPPROFILEID = "
                                                                  + groups.elementAt(0));
                                                  sqlStr += ") ";
                                          } else if (groups.size() > 1) {
                                                  for (int i = 0; i < groups.size(); i++) {
                                                          logger.debug(
                                                                  "MWQH@MWQH@@setFilterCriteria::SQL_GroupCase:GroupIdAdded: "
                                                                          + groups.elementAt(i));
                                                          andOr = (i == 0) ? " AND (" : " OR ";

                                                          sqlStr
                                                                  += (andOr
                                                                          + "GROUPPROFILE.GROUPPROFILEID = "
                                                                          + groups.elementAt(i));
                                                  }

                                                  sqlStr += ") ";
                                          }

                                          logger.debug(
                                                  "MWQH@setFilterCriteria::SQL_GroupCase: " + sqlStr);
                                  }
                          }
                  }
                }
                  // end of inner if
		}

        /***** MCM Impl Team. FXP 22027 22263 ticket merge start *****/
        if (pg.getPageCondition3() == false) {
            sqlStr += " AND ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1";
        } else if (pg.getPageCondition3() == true) {
            sqlStr += " AND (ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0 OR ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1)";
        }
       /***** MCM Impl Team. FXP 22027 22263 ticket merge end *****/

		//========================================================
		logger.debug("MWQH@setFilterCriteria::SQL put into FilterCriteria: " + sqlStr);

		pst.put("FilterCriteria", sqlStr);
	}

	////////////////////////////////////////////////////////////////////////
	//This method is used to populate the fields in the header
	//with the appropriate information
	public void populatePageDisplayFields() {
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");

		populatePageShellDisplayFields();

		// Quick Link Menu display
		displayQuickLinkMenu();

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();

		revisePrevNextButtonGeneration(pg, tds);

		//// Populate sort and Task Status options
		populateSortOptions(tds);
		populateTaskStatusOptions();

		//logger.debug("MWQH@PopulatePageDisplayFields::After populate TaskStatusOptions");
		// generate <first>-<last> lines to display on screen and total number lines
		populateRowsDisplayed(getCurrNDPage(), tds);

		populateSortFilterParams(pg, tds);
		
		// FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- start
		applyWindowScrollPosition(); 
		// FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- end
		
		logger.debug("MWQH@PopulatePageDisplayFields::FINISHED!!!");
	}

	public void handleForwardButton() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");
		handleForwardBackwardCommon(pg, tds, -1, true);

		//--> Reset selected info.
		//--> Bug fix By Billy 09Feb2004
		pst.put("DEALID", new Integer(0));
		pst.put("COPYID", new Integer(0));
		pst.put("DEALAPPID", "0");
		tds.setRelSelectedRowNdx(-1);

		//==============================
		return;
	}

	////////////////////////////////////////////////////////////////////////
	public void handleBackwardButton() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");
		handleForwardBackwardCommon(pg, tds, -1, false);

		//--> Reset selected info.
		//--> Bug fix By Billy 09Feb2004
		pst.put("DEALID", new Integer(0));
		pst.put("COPYID", new Integer(0));
		pst.put("DEALAPPID", "0");
		tds.setRelSelectedRowNdx(-1);

		//==============================
		return;
	}

	////////////////////////////////////////////////////////////////////////
	//I had to split the main DataObject doMasterWorkQueue and stem a silly doTaskStatus d.o.
	// because Oracle failed on the doMasterWorkQueue having 12 tables relation
	public void populatePickListFields(int taskNdx) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");

		try {

			// highligt line if current row - else standard background color
			String colorStr = "FFFFCC";

			if (tds.getRelSelectedRowNdx() == taskNdx) {
				colorStr = "FFFFFF";
			}

			// "99CCFF" - blue - ugly!
			getCurrNDPage().setDisplayFieldValue(
				"Repeated1/bgColor",
				new String(colorStr));

			//logger.debug("MWQH@populatePickListFields::wqId: " + wqId);
			//logger.debug("MWQH@populatePickListFields::statIdsVectorSize: " + statIds.size());
			//--> Combined with the fileds from doMWQPickListModel for performance optimization
			//--> By Billy 17July2003
			//doMWQPickListModel statDo = (doMWQPickListModel)
			//         (RequestManager.getRequestContext().getModelManager().getModel(doMWQPickListModel.class));
			//--> Performance Enhancement : should integrated with the DataObject
			//--> and not necessary to manually setup here
			//--> By BILLY 16Jan2004
			//doMasterWorkQueueModel statDo = (doMasterWorkQueueModel)
			//         (RequestManager.getRequestContext().getModelManager().getModel(doMasterWorkQueueModel.class));
			//==================================================================================

			/*
			   String statusName = "";
			   String mileStoneName = "";
			   String taskName = "";
			   String priorityDescription = "";

			   try
			   {
			     //ResultSet rs = statDo.executeSelect(null);
			     if(statDo.getSize() > 0)
			     {
			       statusName = statDo.getValue(doMasterWorkQueueModel.FIELD_DFSTATUSDESC).toString();
			       mileStoneName = statDo.getValue(doMasterWorkQueueModel.FIELD_DFTMDESCRIPTION).toString();
			       taskName = statDo.getValue(doMasterWorkQueueModel.FIELD_DFTASKNAME).toString();
			       priorityDescription = statDo.getValue(doMasterWorkQueueModel.FIELD_DFPRIORITYDESCRIPTION).toString();
			     }
			     else
			     {
			       logger.info("MWQH@populatePickListFields::There is no info for PickList !!");
			     }
			   }
			   catch(Exception e)
			   {
			     logger.error("MasterWorkQueueHandler@populatePickListFields" + e);
			   }

			   getCurrNDPage().setDisplayFieldValue("Repeated1/stTaskStatus", statusName);
			   getCurrNDPage().setDisplayFieldValue("Repeated1/stTaskWarning", mileStoneName);
			   getCurrNDPage().setDisplayFieldValue("Repeated1/stTaskDescription", taskName);
			   getCurrNDPage().setDisplayFieldValue("Repeated1/stTaskPriority", priorityDescription);
			 */
			modifyTaskInstitutionName();
			//===============================================================================================
		} catch (Exception e) {
			setActiveMessageToAlert(
				BXResources.getSysMsg(
					"TASK_RELATED_UNEXPECTED_FAILURE",
					theSessionState.getLanguageId()),
				ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error(
				"Exception @MWorkQueueHandler.populatePickListFields()");
			logger.error(e);
		}
	}

	//--> Performance enhancement : elimination of the 2nd call of the DataObject
	//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
	//--> the hidden values on each row.
	//--> By Billy 21Jan2004
	public void handleReassign(int taskNdx) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");

		//logger.debug("MWQH@HandleReassign::tds: " + tds);
		//int numPages = 0;
		try
                {
			tds.setRelSelectedRowNdx(taskNdx);
            String institutionId = getRepeatedField("Repeated1/hdInstitutionId", taskNdx);
			//int absNdx = tds.getAbsSelectedRowNdx();
			//--> Performance enhancement : elimination of the 2nd call of the DataObject
			//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
			//--> the hidden values on each row.
			//--> By Billy 21Jan2004
			// Set the Ids in PST
			pst.put("DEALID",new Integer(getIntValue(getRepeatedField("Repeated1/hdDealId", taskNdx))));
			pst.put("COPYID",new Integer(getIntValue(getRepeatedField("Repeated1/hdCopyId", taskNdx))));
			pst.put("DEALAPPID", getRepeatedField("Repeated1/hdCopyId", taskNdx));
            pst.put("INSTITUTIONID", new Integer(institutionId));
			//=====================================================================================
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_TASK_REASSIGN, true);
			int WQTaskId = getIntValue(getRepeatedField("Repeated1/hdWqID", taskNdx));
			int WQCopyId = getIntValue(getRepeatedField("Repeated1/hdCopyId", taskNdx));
            int WQDealId = getIntValue(getRepeatedField("Repeated1/hdDealId", taskNdx));

			logger.debug("MWQH@HandleReassign:: WQTaskId = " + WQTaskId + " :: WQCopyId = "+ WQCopyId);

			pgEntry.setPageCriteriaId1(WQTaskId);
			pgEntry.setPageCriteriaId2(WQCopyId);
			pgEntry.setPageCondition3(true);
			int intInstitutionId = Integer.parseInt(institutionId);
            pgEntry.setDealInstitutionId(intInstitutionId);
            pgEntry.setPageMosUserId(theSessionState.getUserProfileId(intInstitutionId));
            pgEntry.setPageMosUserTypeId(theSessionState.getUserTypeId(intInstitutionId));

            pgEntry.setPageDealId(WQDealId);

            logger.debug("MWQHandler Handle Reassign Vpd institutionid: " + institutionId);
            
            //FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position
            saveWindowScrollPosition();
            
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);

		} catch (Exception e) {
			setActiveMessageToAlert(
				BXResources.getSysMsg(
					"TASK_RELATED_UNEXPECTED_FAILURE",
					theSessionState.getLanguageId()),
				ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @MasterWorkQueueHandler.handleReassign()");
			logger.error(e);
		}
	}

	//=================================================================================
	////////////////////////////////////////////////////////////////////////
	public void handleSnapShotButton(int relTaskNdx) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");
		tds.setRelSelectedRowNdx(relTaskNdx);

		//--> Performance enhancement : elimination of the 2nd call of the DataObject
		//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
		//--> the hidden values on each row.
		//--> By Billy 21Jan2004
		// Set the Ids in PST
		pst.put(
			"DEALID",
			new Integer(
				getIntValue(
					getRepeatedField("Repeated1/hdDealId", relTaskNdx))));
		pst.put(
			"COPYID",
			new Integer(
				getIntValue(
					getRepeatedField("Repeated1/hdCopyId", relTaskNdx))));
		pst.put(
			"DEALAPPID",
			getRepeatedField("Repeated1/hdCopyId", relTaskNdx));

    logger.debug("MasterWorkQueue DealId Repeated1: " +
        getRepeatedField("Repeated1/stDealNumber", relTaskNdx));

        logger.debug("MasterWorkQueue institutionid Repeated1: " +
            getRepeatedField("Repeated1/hdInstitutionId", relTaskNdx));

        pst.put(
          "INSTITUTIONID",
          new Integer(
              getIntValue(
              getRepeatedField("Repeated1/hdInstitutionId", relTaskNdx))));

		//=====================================================================================
        //FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position
        saveWindowScrollPosition();
	}

	////////////////////////////////////////////////////////////////////////
	private void setDealForSnapshot(PageEntry pg, PageCursorInfo tds) {
		if (tds.getTotalRows() <= 0) {
			pg.setPageDealApplicationId("0");
			pg.setPageDealId(0);

			return;
		}

		try {
			//--> Performance enhancement : elimination of the 2nd call of the DataObject
			//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
			//--> the hidden values on each row.
			//--> By Billy 21Jan2004
			Hashtable pst = pg.getPageStateTable();
			int dealId = ((Integer) pst.get("DEALID")).intValue();
			int copyId = ((Integer) pst.get("COPYID")).intValue();
			String appId = (String) pst.get("DEALAPPID");
            int institutionId = ((Integer) pst.get("INSTITUTIONID")).intValue();

            //FXP21616, do not set deal info for the first time
            if ( dealId != 0 ){
			   pg.setPageDealId(dealId);
			   pg.setPageDealCID(copyId);
			   pg.setPageDealApplicationId(appId);
               pg.setDealInstitutionId(institutionId);
               pg.setPageMosUserId(theSessionState.getUserProfileId(institutionId));
               pg.setPageMosUserTypeId(theSessionState.getUserTypeId(institutionId));
            }
			//===================================================================================
		} catch (Exception e) {
			pg.setPageDealApplicationId("0");
			logger.error("Error setting deal id for summary snap shot", e);
		}
	}

	private void buildDataObject(PageEntry pg, PageCursorInfo tds, doMasterWorkQueueModel wqDo, doMasterWorkQueueCountModel wqCountDo)
		throws Exception
       {
		wqDo.clearUserWhereCriteria();
		wqCountDo.clearUserWhereCriteria();

		Hashtable pst = pg.getPageStateTable();
		String sqlStr = "";

        /***** MCM Impl Team. FXP 22072 22263 ticket merge *****/

        String orderBy = getSortOrder(tds.getSortOption());
        Boolean toggleVisiblity = (Boolean) pst.get("toggleVisiblity");
        String filterCriteria = (String) pst.get("FilterCriteria");
        if (toggleVisiblity != null && toggleVisiblity) {
            pst.remove("toggleVisiblity");
            if (filterCriteria != null && filterCriteria.trim().length() > 0)
                sqlStr += " AND " + filterCriteria;
        } else {
		//// temp debug
		logger.debug("MWQH@buildDataObject::PageCriteria2: " + pg.getPageCriteriaId2());
		logger.debug("MWQH@buildDataObject::PageCriteria3: " + pg.getPageCriteriaId3());
		logger.debug("MWQH@buildDataObject::PageCondition5: " + pg.getPageCondition5());
		logger.debug("MWQH@buildDataObject::PageCondition6: " + pg.getPageCondition6());
		logger.debug("MWQH@buildDataObject::PageCondition7: " + pg.getPageCondition7());

		int iTaskStatusMapper = MWQ_STATUS_TASK_MAPPER[pg.getPageCriteriaId2()];

               logger.debug("MWQH@buildDataObject::iTaskStatusMapper: " + iTaskStatusMapper);

               switch(iTaskStatusMapper)
               {
                   case Sc.TASK_OPEN: // open, 1
                   sqlStr += " AND assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
                   logger.debug("MWQH@buildDataObject::Filter Option = by [OPEN]" + sqlStr);

                   break;
                 case Sc.TASK_COMPLETE: // complete, 3
                   sqlStr += " AND assignedtasksworkqueue.taskstatusid = " + Sc.TASK_COMPLETE;
                   logger.debug("MWQH@buildDataObject::Filter Option = by [COMPLETE]" + sqlStr);

                   break;
                 case Mc.MWQ_TASK_NOT_ONHOLD: // on hold, 10
                     sqlStr += " AND assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
                     sqlStr += " AND DEAL.HOLDREASONID <> " + Mc.HOLD_REASON_NONE;
                     logger.debug("MWQH@buildDataObject::Filter Option = by [ON HOLD]" + sqlStr);

                   break;
                 case Mc.MWQ_STATUS_TASK_ALL: // all, 9
                     sqlStr += " AND ((assignedtasksworkqueue.taskstatusid = " + Sc.TASK_COMPLETE + ") " +
                     " OR (assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN + ") )";
                     logger.debug("MWQH@buildDataObject::Filter Option = by [ALL]" + sqlStr);

                   break;
                 default: // OPEN
                   sqlStr += " AND assignedtasksworkqueue.taskstatusid = " + Sc.TASK_OPEN;
                   logger.debug("MWQH@buildDataObject::Filter Option = by [OPEN]" + sqlStr);

                   break;
                }
		//-- ========== SCR#750   ends ========== --//

		if (pg.getPageCriteriaId3() > 0)
                {
			//logger.debug("MWQH@buildDataObject::PageCriteria3 > 0 mode");
			//logger.debug("MWQH@buildDataObject::PageCriteria3: " + pg.getPageCriteriaId3());
			sqlStr += " AND (assignedtasksworkqueue.dealid = " + pg.getPageCriteriaId3() +") ";
		}
		
		Date expiryDateStart = (Date) pst.get(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_START);
		Date expiryDateEnd = (Date) pst.get(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_END);
		if (expiryDateStart != null)
			sqlStr += (" AND assignedtasksworkqueue.endtimestamp>='" + TypeConverter.stringTypeFrom(expiryDateStart)) + "' ";
		if (expiryDateEnd != null)
			sqlStr += (" AND assignedtasksworkqueue.endtimestamp<='" + TypeConverter.stringTypeFrom(expiryDateEnd)) + "' ";

		//--TD_MWQ_CR--start//
		//// I. TD requests to set up two extra filters: by Task Name and by Region/Branch/Group.
		//// 1. By Task Name.
        //FXP26606
		if (pg.getMWQTaskNameFilter().length() > 0)
        {
            sqlStr += getTaskNameCriteria(pg.getMWQTaskNameFilter());
		}

		//-- ========== SCR#750 begins ========== --//
		//-- by Neil on Dec/22/2004
		//-- BMO 1.5
		//-- added filter by "Deal Status" (PageCriteriaId6)
                //--Ticket#1196--11Apr2005--start--//
                // Deal Status Filtering has to include boundary case 0: Incomplete status
                if (pg.getPageCriteriaId6() >= 0)
                //--Ticket#1196--11Apr2005--end--//
                {
			sqlStr += (" AND DEAL.STATUSID = " + pg.getPageCriteriaId6());
		}
		//-- ========== SCR#750   ends ========== --//

		//// 2. By Branch(es).
		if (pg.getPageCondition5() == true) {
            // logger.debug("MWQH@buildDataObject::Not Task Name All mode");

            String branchCriteria = getInstitutionCriteria((Map<Integer,Integer>)pst.get("branches"));


            sqlStr += branchCriteria;
        }

		//// 3. By Groups.
		if (pg.getPageCondition6() == true) {
			//logger.debug("MWQH@buildDataObject::Not Task Name All mode");
			Vector groups = (Vector) pst.get("groups");
			String andOr = " AND "; // default

			if (groups != null) {
				//logger.debug("MWQH@buildDataObject::NumberOfGroupsSelected = " + groups.size());
				if (groups.size() == 1) {
					//logger.debug("MWQH@buildDataObject::Group_Element_0 = " + groups.elementAt(0));
					sqlStr += (" AND (GROUPPROFILE.GROUPPROFILEID = " + groups.elementAt(0));
					sqlStr += ") ";
				} else if (groups.size() > 1) {
					for (int i = 0; i < groups.size(); i++) {
						//logger.debug("MWQH@buildDataObject::GroupIdAdded: " + groups.elementAt(i));
						andOr = (i == 0) ? " AND (" : " OR ";

						sqlStr += (andOr + "GROUPPROFILE.GROUPPROFILEID = " + groups.elementAt(i));
					}

					sqlStr += ") ";
				}
			}
		}

		//--TD_MWQ_CR--end//
		if (pg.getPageCriteriaId1() >= 0) {
			//logger.debug("MWQH@buildDataObject::PageCriteria1 > 0 mode");
			doUnderwriter1Model dtDo =
				(doUnderwriter1Model) (RequestManager
					.getRequestContext()
					.getModelManager()
					.getModel(doUnderwriter1Model.class));

			dtDo.clearUserWhereCriteria();
			dtDo.addUserWhereCriterion(
				"dfUserProfileId",
				"=",
				new Integer(pg.getPageCriteriaId1()));

			//// BX: separate the following fragment into the private routine in
			//// this class (3 times occurance).
			String firstName = null;
			String lastName = null;

			try {
				ResultSet rs = dtDo.executeSelect(null);

				if ((rs != null) && (dtDo.getSize() > 0)) {
					firstName = dtDo.getValue(doUnderwriter1ModelImpl.FIELD_DFFIRSTNAME).toString();
					lastName = dtDo.getValue(doUnderwriter1ModelImpl.FIELD_DFLASTNAME).toString();

					//logger.debug("MWQH@buildDataObject::FirstName: " + firstName.toString());
					//logger.debug("MWQH@buildDataObject::LastName: " + lastName.toString());
					//logger.debug("MWQH@buildDataObject::Size of the taskCount result set: " + dtDo.getSize());
				}
                                else
                                {
					logger.error("MWQH@buildDataObject::Problem when getting the Underwriter info !!");

					////never happens, but return just in case.
					return;
				}

				if (firstName.equals("")
					|| (firstName == null)
					|| lastName.equals("")
					|| (lastName == null)) {
					////for bad user profiles like 0 ID or something else wrong.
					return;
				}

				//--TD_MWQ_CR--//
				//// IMPORTANT!!
				//// TD wants to see the Source Firm/Source of Business along with primary Borrower.
				//// As a result the 'CONTACT' table is aliased in doMasterWorkQueueModel and,
				//// therefore, should be alised (to C1 'CONTACT' instance) in this additional
				//// search criteria to escape an SQL invalid identifier exception in the joint query.
				////sqlStr += " AND CONTACT.CONTACTLASTNAME LIKE '" + lastName + "' AND SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) LIKE '"
				////          + firstName.substring(0, 1) + "'";
				sqlStr
					+= (" AND C1.CONTACTLASTNAME LIKE '"
						+ lastName
						+ "' AND SUBSTR(C1.CONTACTFIRSTNAME,1,1) LIKE '"
						+ firstName.substring(0, 1)
						+ "'");

				logger.debug(
					"MWQH@buildDataObject::FINISHED PageCriteria1 > 0 mode!!!"
						+ sqlStr);
			} catch (Exception e) {
				logger.error(e);
			}
		}

		//// end pgCriteriaId1 > 0.
		if (pg.getPageCondition3() == false) {
			sqlStr += " AND ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1";
		}
                else if (pg.getPageCondition3() == true) {
                        sqlStr += " AND (ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0 OR ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1)";
                }

		//========================================================
		logger.debug("MWQH@buildDataObject::orderBy: " + orderBy);

		//// TD--MWQ_CR--start//
		//// TD requests to display tasks narrowed by branch. This option is implemented for region
		//// and group as well for other clients.
		int realmId = getRealm();
		logger.debug("MWQH@buildDataObject::RealmId: " + realmId);

		//// If a narrow criteria exists and both branch and group filtering criteria
		//// are false the locationCriteria should be applied.
		if ((realmId > 0)
			&& (pg.getPageCondition5() == false)
			&& (pg.getPageCondition6() == false)) {
			pg.setPageCondition7(true);
		}

		if (pg.getPageCondition7() == true) {
			String locationCriteria = addLocationCriteria(pst, realmId);

			//logger.debug("MWQH@buildDataObject::LocationCriteria: " + locationCriteria);
			sqlStr = sqlStr + locationCriteria;

			//logger.debug("MWQH@buildDataObject::SqlStrBefore_STATIC_WHERE_CRIT: " + sqlStr);
		}
        }   //MCM Impl Team. FXP 22072 ticket merge
        logger.debug("MWQH@buildDataObject::orderBy: " + orderBy);

		//// TD--MWQ_CR--end//
		if (sqlStr.equals("") || (sqlStr == null))
                {
			((doMasterWorkQueueModelImpl) wqDo).setStaticWhereCriteriaString(
				        doMasterWorkQueueModelImpl.STATIC_WHERE_CRITERIA
					+ " order by "
					+ orderBy);

			//// order by only...
			((doMasterWorkQueueCountModelImpl) wqCountDo).setStaticWhereCriteriaString(doMasterWorkQueueCountModelImpl.STATIC_WHERE_CRITERIA);

			//// order by only...
		} else if (!sqlStr.equals("") && (sqlStr != null)) {
			((doMasterWorkQueueModelImpl) wqDo).setStaticWhereCriteriaString(
				        doMasterWorkQueueModelImpl.STATIC_WHERE_CRITERIA
					+ sqlStr
					+ " order by "
					+ orderBy);
			((doMasterWorkQueueCountModelImpl) wqCountDo).setStaticWhereCriteriaString(
				        doMasterWorkQueueCountModelImpl.STATIC_WHERE_CRITERIA
					+ sqlStr);

		}

	}

	public void handleFilterButton()
        {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

        /***** MCM Impl Team. FXP 22072 22263 ticket merge starts *****/
        String sqlStr = "";
        boolean toggleVisiblity;
        boolean currentValue = getCurrNDPage().getDisplayFieldValue("chIsDisplayHidden").toString().equals("T");
        String storedCriteria = (String) pst.get("FilterCriteria");
        String displayInVisible = "ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0";
        if (storedCriteria != null && storedCriteria.trim().length() > 0)
            toggleVisiblity = (currentValue && !storedCriteria.contains(displayInVisible))
                                || (!currentValue && storedCriteria.contains(displayInVisible));
        else
            toggleVisiblity = currentValue;
        /***** MCM Impl Team. FXP 22072 22263 ticket merge ends*****/
        pst.put("toggleVisiblity", new Boolean(toggleVisiblity));

        if (toggleVisiblity) {
            pg.setPageCondition3(currentValue);
        /***** MCM Impl Team. FXP 22072 22263 ticket merge starts *****/
            if (storedCriteria != null && storedCriteria.trim().length() > 0) {
                sqlStr = storedCriteria.substring(0, storedCriteria.lastIndexOf(" AND ")) + " AND ";
            }

            if (currentValue) {
                sqlStr += " (ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 0 OR ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1)";
            } else {
                sqlStr += " ASSIGNEDTASKSWORKQUEUE.VISIBLEFLAG = 1";
            }

            logger.debug("MWQH@setFilterCriteria::SQL put into FilterCriteria: " + sqlStr);

            pst.put("FilterCriteria", sqlStr);
        } else {
        /***** MCM Impl Team. FXP 22072 ticket merge ends*****/

		ComboBox userFilterBox = (ComboBox) getCurrNDPage().getDisplayField("cbUserFilter");
		String sFilterValue = (String) userFilterBox.getValue();
//logger.debug("MWQH@handleFilterButton::UserFilterValue: " + sFilterValue);

		int userFltrNdx = 0;

		try
                {
                  userFltrNdx = (new Integer(sFilterValue)).intValue();
logger.debug("MWQH@handleFilterButton::UserFilterBoxIndex: " + sFilterValue);

                  pg.setPageCriteriaId1(userFltrNdx);
		}
                catch (Exception e)
                {
                  //// Any Problem default to 0 (default ioption)
                  userFltrNdx = 0;
                  logger.debug("MWQH@handleFilterButton::::Default filter option");
                  pg.setPageCriteriaId1(-1);
		}

//logger.debug("MWQH@handleFilterButton::PageCriteria1: " + pg.getPageCriteriaId1());

		ComboBox statusFilterBox = (ComboBox) getCurrNDPage().getDisplayField("cbStatusFilter");
		String statusFilterValue = (String) statusFilterBox.getValue();
//logger.debug("MWQH@handleFilterButton::StatusFilterValue: " + statusFilterValue);

		int statusFltrNdx = 0;

		try
                {
                  statusFltrNdx = (new Integer(statusFilterValue)).intValue();
//logger.debug("MWQH@handleFilterButton::StatusBoxIndex: " + statusFltrNdx);

                  pg.setPageCriteriaId2(statusFltrNdx);
		}
                catch (Exception e)
                {
                  //// Any Problem default to 0 (default ioption)
                  statusFltrNdx = 0;
                  logger.debug("MWQH@handleFilterButton::Default sort option");
                  pg.setPageCriteriaId2(-1);
		}

		//-- ========== SCR#750 begins ========== --//
		//-- by Neil on Dec/22/2004
		//-- BMO 1.5
		//-- added new filter by "Deal Status"
		ComboBox dealStatusFilterBox =
			(ComboBox) getCurrNDPage().getDisplayField("cbDealStatusFilter");
		String dealStatusFilterValue = (String) dealStatusFilterBox.getValue();
//logger.debug("MWQH@handleFilterButton::DealStatusFilterValue: " + dealStatusFilterValue);

		int dealStatusFltrNdx = 0;

		try
                {
			dealStatusFltrNdx = (new Integer(dealStatusFilterValue)).intValue();
//logger.debug("MWQH@handleFilterButton::DealStatusBoxIndex: " + dealStatusFltrNdx);

			pg.setPageCriteriaId6(dealStatusFltrNdx);
		}
                catch (Exception e)
                {
                    //// Any Problem default to 0 (default ioption)
                    dealStatusFltrNdx = 0;
                    logger.debug("MWQH@handleFilterButton::Default sort option");
                    pg.setPageCriteriaId6(-1);
		}

		logger.debug("MWQH@handleFilterButton::PageCriteria6: " + pg.getPageCriteriaId6());

		//-- ========== SCR#750   ends ========== --//
		// Add Deal Id Filter -- BILLY 06Dec2001
		TextField dealIdFilter = (TextField) getCurrNDPage().getDisplayField("tbDealIdFilter");
		String dealIdFilterValue = (String) dealIdFilter.getValue();

		//logger.debug("MWQH@handleFilterButton::DealIdFilterOptions: " + dealIdFilter.getValue());
		if (!dealIdFilterValue.equals("") && (dealIdFilterValue != null)) {
			pg.setPageCriteriaId3((new Integer(dealIdFilter.getValue().toString())).intValue());
		} else {
			pg.setPageCriteriaId3(0);
		}

		//logger.debug("MWQH@handleFilterButton::PageCriteria3: " + pg.getPageCriteriaId3());
		// Added checkbox to display Hidden tasks or not
		// By Billy 08Oct2002
		if (getCurrNDPage().getDisplayFieldValue("chIsDisplayHidden").toString().equals("T")) {
			pg.setPageCondition3(true);
		} else {
			pg.setPageCondition3(false);
		}

		//===============================================
		//--TD--MWQ_CR--start//
		//// 1. New Filter by Task Name.
		ComboBox taskNameFilterBox = (ComboBox) getCurrNDPage().getDisplayField("cbTaskNameFilter");
		String taskNameFilterValue = (String) taskNameFilterBox.getValue();
		logger.debug("MWQH@handleFilterButton::TaskNameFilterValue: " + taskNameFilterValue);
		// fixed for FXP26606
		try
                {
                  logger.debug("MWQH@handleFilterButton::TaskNameBoxIndex: " + taskNameFilterValue);
                  //FXP26606
                  pg.setMWQTaskNameFilter(taskNameFilterValue);
		}
                catch (Exception e)
                {
                    //// Any Problem default to 0 (default option)
                    logger.debug(
                            "MWQH@handleFilterButton::Default Task Name filter option");
                    //FXP26606
                    pg.setMWQTaskNameFilter("");
		}

		//// 2. New Filter by Branch.
		ListBox theBranches =
			(ListBox) (getCurrNDPage().getChild("lbMWQBranches"));
		Object[] theSelectedBranches = theBranches.getValues();



		if (theSelectedBranches.length > 0) {
			int branchId = 0;

            Map<Integer, Integer> branchMap
            = new HashMap<Integer, Integer>();

			//logger.debug("MWQH@handleFilterButton::NumberOfBrancesSelected = " + theSelectedBranches.length);
			//logger.debug("MWQH@handleFilterButton::BranchOptionsSize: " + theBranches.getOptions().size());
			for (int i = 0; i < theSelectedBranches.length; i++) {
				//logger.debug("MWQH@handleFilterButton::Index: " + theSelectedBranches[i].toString());
				if (!theSelectedBranches[i].toString().equals("")) {

                    String branchStr = theSelectedBranches[i].toString();

                    String[] result = branchStr.split(", ");

                    branchId = Integer.valueOf(result[0]).intValue();
                    int instId = Integer.valueOf(result[1]).intValue();

					logger.debug(
						"MWQH@handleFilterButton::BranchId: " + branchId);
                    logger.debug(
                            "MWQH@handleFilterButton::institutionId: " + branchId);

					branchMap.put(new Integer(branchId), new Integer(instId));

				}
			}

			pg.setPageCondition5(true);
			pst.put("branches", branchMap);
		}

		//// 3. New Filter by Group.
		ListBox theGroups = (ListBox) (getCurrNDPage().getChild("lbMWQGroups"));
		Object[] theSelectedGroups = theGroups.getValues();

		if (theSelectedGroups.length > 0) {
			int groupId = 0;
			Vector groups = new Vector();

			//logger.debug("MWQH@handleFilterButton::NumberOfGroupsSelected = " + theSelectedGroups.length);
			//logger.debug("MWQH@handleFilterButton::GroupOptionsSize: " + theGroups.getOptions().size());
			for (int i = 0; i < theSelectedGroups.length; i++) {
				//logger.debug("MWQH@handleFilterButton::Index: " + theSelectedGroups[i].toString());
				if (!theSelectedGroups[i].toString().equals("")) {
					groupId =
						com.iplanet.jato.util.TypeConverter.asInt(
							theSelectedGroups[i],
							0);
					logger.debug(
						"MWQH@handleFilterButton::GroupId: " + groupId);
					groups.add(new Integer(groupId));
				}
			}

			pg.setPageCondition6(true);
			pg.setPageCondition7(false);
			pst.put("groups", groups);
		}

		//--TD_MWQ_CR--end//
		// expiry date range
		TextField expiryDateStartTextFeild = (TextField) (getCurrNDPage().getChild(CHILD_CBTASKEXPIRYDATESTARTFILTER));
		String expiryDateStartString = (expiryDateStartTextFeild == null) ? null : (String) (expiryDateStartTextFeild.getValue());
		Date expiryDateStart;
		try {
			expiryDateStart = dateFormat.parse(expiryDateStartString);
			expiryDateStartString = dateFormat.format(expiryDateStart);
			pst.put(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_START, expiryDateStart);
		} catch (Exception e) { //NullPointerException and ParseException
			expiryDateStart = null;
			expiryDateStartString = CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE;
			pst.remove(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_START);
		}
		if (expiryDateStartTextFeild != null) {
			expiryDateStartTextFeild.setValue(expiryDateStartString);
		}

		TextField expiryDateEndTextFeild = (TextField) (getCurrNDPage().getChild(CHILD_CBTASKEXPIRYDATEENDFILTER));
		String expiryDateEndString = (expiryDateEndTextFeild == null) ? null : (String) (expiryDateEndTextFeild.getValue());
		Date expiryDateEnd;
		try {
			expiryDateEnd = dateFormat.parse(expiryDateEndString);
			expiryDateEndString = dateFormat.format(expiryDateEnd);
			pst.put(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_END, expiryDateEnd);
		} catch (Exception e) { //NullPointerException and ParseException
			expiryDateEnd = null;
			expiryDateEndString = CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE;
			pst.remove(PAGE_STATE_TABLE_KEY_MWQ_BY_EXPIRY_DATE_END);
		}
		if (expiryDateEndTextFeild != null)
			expiryDateEndTextFeild.setValue(expiryDateEndString);
		
		setFilterCriteria();
        } /***** MCM Impl Team. FXP 22072 ticket merge *****/
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
		tds.setCriteriaChanged(true);
	}

	////////////////////////////////////////////////////////////////////////////
	public void handleSortButton()
        {
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		ComboBox sortBox =
			(ComboBox) getCurrNDPage().getDisplayField("cbSortOptions");
		String sortBoxValue = (String) sortBox.getValue();

		//logger.debug("MWQH@handleSortButton::SortBoxValue: " + sortBoxValue);
		int sortNdx = 0;

		try {
			sortNdx = (new Integer(sortBoxValue)).intValue();

			//logger.debug("MWQH@handleSortButton::SortBoxIndex: " + sortNdx);
			tds.setSortOption(sortNdx);
		} catch (Exception e) {
			//// Any Problem default to 0 (default ioption)
			sortNdx = 0;

			//logger.debug("MWQH@handleSortButton::Default sort option");
		}

		tds.setCriteriaChanged(true);
	}

	public void handleResetButton()
        {
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		pg.setPageCriteriaId1(-1);

		resetFilterCriteria();

		tds.setSortOption(0);

		pg.setPageCriteriaId2(0);

		pg.setPageCriteriaId3(0);
		
		// FXP26606
		pg.setMWQTaskNameFilter("");

		//--TD_MWQ_CR--start//
		//// TD requires to filter tasks by two other options: by Task Name and by Region.

		//-- ========== SCR#750 begins ========== --//
		//-- by Neil on Dec/22/2004
		//-- new filter by Deal Status (PageCriteriaId6)
        // SEAN Dec 19, 2005: should be -1 for all status.
		pg.setPageCriteriaId6(-1);

		//-- ========== SCR#750   ends ========== --//
		pg.setPageCondition5(false);
		pg.setPageCondition6(false);
		pg.setPageCondition7(false);

		//--TD_MWQ_CR--end//
		// Added checkbox to display Hidden tasks or not
		// By Billy 08Oct2002
		pg.setPageCondition3(false);

		//==============================================
		tds.setCriteriaChanged(true);

		//--MCM Nov 18 2008, FXP22355 start
		Hashtable pst = pg.getPageStateTable();
		pst.put("REFRESH", new Boolean(true));
		//--MCM Nov 18 2008, FXP22355 end

	}

	private void populateSortOptions(PageCursorInfo tds)
       {

	    if (theSessionState.isMultiAccess())
	        getCurrNDPage().setDisplayFieldValue("multiAccess", "Y");
	    else
            getCurrNDPage().setDisplayFieldValue("multiAccess", "N");
	    ComboBox sortBox =
			(ComboBox) getCurrNDPage().getDisplayField("cbSortOptions");
		Option[] optionsOrig = sortBox.getOptions().toOptionArray();

		//logger.debug("MWQH@populateSortOptions::Size: " + optionsOrig.length); //// not populated yet.
		//// Manual population of the Sort Combobox.
		//--Release2.1--start//
		////Modified to get the SortOrderOption Labels from Resource Bundle
		////int lim = Mc.MWQ_SORT_ORDER_OPTIONS_LENGTH;
		Collection theSList =
			BXResources.getPickListValuesAndDesc(this.theSessionState.getUserInstitutionId(),
				"MWORKQUEUESORTORDERS",
				this.theSessionState.getLanguageId());
		int lim = theSList.size();
		Iterator theList = theSList.iterator();
		String[] theVal = new String[2];

		//logger.debug("MWQH@populateSortOptions::Limit: " + lim);
		//OptionList options = new OptionList();

		String[] labels = new String[lim];
		String[] values = new String[lim];

		for (int i = 0; i < lim; i++) {
			theVal = (String[]) (theList.next());
			labels[i] = theVal[1];
			values[i] = theVal[0];
		}

		//--Release2.1--end//
		OptionList sortOption = pgMWorkQueueViewBean.cbSortOptionsOptions;

		sortOption.setOptions(labels, values);

		int selectedNdx = tds.getSortOption();

		//logger.debug("MWQH@populateSortOptions::SelectedIndex: " + selectedNdx);
		if ((selectedNdx < 0) || (selectedNdx >= lim)) {
			selectedNdx = 0;
		}

		sortBox.setValue((new Integer(selectedNdx)).toString(), true);

		//logger.debug("MWQH@populateSortOptions::ComboBox value: " + (sortBox.getValue()).toString());
		if (sortBox.getValue() != null) {
			String selectedValue = sortBox.getValue().toString();
			String selectedLabel =
				sortBox.getOptions().getValueLabel(selectedValue);

			//logger.debug("MWQH@populateSortOptions::selectedLabel: " + selectedLabel);
			//logger.debug("MWQH@populateSortOptions::selectedValue: " + selectedValue);
		}
	}

	private void populateTaskStatusOptions()
        {
		PageEntry pg = theSessionState.getCurrentPage();

		ComboBox statFilter =
			(ComboBox) getCurrNDPage().getDisplayField("cbStatusFilter");
		Option[] optionsOrig = statFilter.getOptions().toOptionArray();

		//logger.debug("MWQH@populateTaskStatusOptions::Size: " + optionsOrig.length); //// not populated yet.
		//// Manual population of the TaskStatus Combobox.
		int lim = Mc.MWQ_STATUS_TASK_OPTIONS_LENGTH;

		////logger.debug("MWQH@populateTaskStatusOptions::Limit: " + lim);
		//OptionList options = new OptionList();

		String[] labels = new String[lim];
		String[] values = new String[lim];

		//--Release2.1--start//
		////Modified to get the Option Labels from the GenericMessage.properties file
		//		String openTasksLbl =
		//			BXResources.getGenericMsg(
		//				"MWQ_STATUS_TASK_CBOPTIONS_OPEN_LABEL",
		//				theSessionState.getLanguageId());
		//		String completedTasksLbl =
		//			BXResources.getGenericMsg(
		//				"MWQ_STATUS_TASK_CBOPTIONS_COMPLITE_LABEL",
		//				theSessionState.getLanguageId());
		//		String allTasksLbl =
		//			BXResources.getGenericMsg(
		//				"MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL",
		//				theSessionState.getLanguageId());
		//
		//		String[] MWQ_STATUS_TASK_OPTIONS =
		//			{ openTasksLbl, completedTasksLbl, allTasksLbl //Always goes last
		//		};
		//-- ========== SCR#750 begins ========== --//
		//-- by Neil on Dec/21/2004
		//-- BMO 1.5
		//-- added for new Task Status, "On Hold"
		String onholdTasksLbl =
			BXResources.getGenericMsg(
				"MWQ_STATUS_TASK_CBOPTIONS_ONHOLD_LABEL",
				theSessionState.getLanguageId());
		String openTasksLbl =
			BXResources.getGenericMsg(
				"MWQ_STATUS_TASK_CBOPTIONS_OPEN_LABEL",
				theSessionState.getLanguageId());
		String completedTasksLbl =
			BXResources.getGenericMsg(
				"MWQ_STATUS_TASK_CBOPTIONS_COMPLITE_LABEL",
				theSessionState.getLanguageId());
		String allTasksLbl =
			BXResources.getGenericMsg(
				"MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL",
				theSessionState.getLanguageId());

		String[] MWQ_STATUS_TASK_OPTIONS = { openTasksLbl, completedTasksLbl,
			// newly added fields should not
			// change value of other existing field value.
			onholdTasksLbl, allTasksLbl //Always goes last
		};

		//-- ========== SCR#750   ends ========== --//
		for (int i = 0; i < lim; i++) {
			labels[i] = MWQ_STATUS_TASK_OPTIONS[i];
			values[i] = (new Integer(i)).toString();
		}

		//--Release2.1--end//
		OptionList statusTaskOption =
			pgMWorkQueueViewBean.cbStatusFilterOptions;

		statusTaskOption.setOptions(labels, values);

		int selectedNdx = pg.getPageCriteriaId2();
		logger.debug("MWQH@populateTaskStatusOptions::SelectedIndex: " + selectedNdx);

		if ((selectedNdx < 0) || (selectedNdx >= lim)) {
			selectedNdx = 0;
		}

		statFilter.setValue((new Integer(selectedNdx)).toString(), true);

		//logger.debug("MWQH@populateTaskStatusOptions::ComboBox value: " + (statFilter.getValue()).toString());
		if (statFilter.getValue() != null) {
			String selectedValue = statFilter.getValue().toString();
			String selectedLabel = statFilter.getOptions().getValueLabel(selectedValue);

			logger.debug("MWQH@populateTaskStatusOptions::selectedLabel: " + selectedLabel);
			logger.debug("MWQH@populateTaskStatusOptions::selectedValue: " + selectedValue);
		}
	}

	private String getSortOrder(int sortType) {
		String dueDatePriority = "ASSIGNEDTASKSWORKQUEUE.dueTimeStamp, ASSIGNEDTASKSWORKQUEUE.priorityId";

		//logger.debug("MWQH@getSortOrder::sortType: " + sortType);
		String sql = null;

		switch (sortType) {
			case Mc.MWQ_SORT_ORDER_PRIORITY :
				sql = " ASSIGNEDTASKSWORKQUEUE.priorityId, ASSIGNEDTASKSWORKQUEUE.dueTimeStamp ";
				break;

			case Mc.MWQ_SORT_DUE_DATE :
				sql = dueDatePriority;
				break;

			case Mc.MWQ_SORT_BORROWER :
				sql = "BORROWER.borrowerLastName, " + dueDatePriority;
				break;

			case Mc.MWQ_SORT_DEAL_NUMBER :
				sql = "ASSIGNEDTASKSWORKQUEUE.dealId ASC, " + dueDatePriority;
				break;

			case Mc.MWQ_SORT_DEAL_NUMBER_DSC :
				//Descending order
				sql = "ASSIGNEDTASKSWORKQUEUE.dealId DESC, " + dueDatePriority;
				break;

			case Mc.MWQ_SORT_TASK_DESC :
				sql = "WORKFLOWTASK.taskname, " + dueDatePriority;
				break;

			case Mc.MWQ_SORT_TASK_STATUS :
				sql = "ASSIGNEDTASKSWORKQUEUE.taskStatusID, " + dueDatePriority;
				break;

			case Mc.MWQ_SORT_WARNING :
				sql = "ASSIGNEDTASKSWORKQUEUE.timeMilestoneID, " + dueDatePriority;
				break;

				//-- ========== SCR#750 begins ========== --//
				//-- by Neil on Dec/20/2004
			case Mc.MWQ_SORT_DEAL_STATUS :
				//-- ========== SCR#885 begins ========== --//
				//-- by Neil on Jan/25/2005 --//
				// sort by DealStatus
				sql = "DEAL.STATUSID, " + dueDatePriority;
				//sql = "STATUS.STATUSDESCRIPTION, " + dueDatePriority;
				//-- ========== SCR#885   ends ========= --//
				break;

				//-- ========== SCR#750   ends ========== --//
			default :
				sql = dueDatePriority;

				break;
		}

		//logger.debug("MWQH@getSortOrder::sql: " + sql);
		return setSchemaName(sql);
	}

	////////////////////////////////////////////////////////////////////////
	private void populateRowsDisplayed(ViewBean NDPage, PageCursorInfo tds) {
		String rowsRpt = "";

		//logger.debug("MWQH@PopulateRowsDisplayed::TotalRowsInTDS: " + tds.getTotalRows());
		if (tds.getTotalRows() > 0) {
			rowsRpt =
				tds.getFirstDisplayRowNumber()
					+ "-"
					+ tds.getLastDisplayRowNumber()
					+ " of ["
					+ tds.getTotalRows()
					+ "]";
		}

		//logger.debug("MWQH@PopulateRowsDisplayed::rowRptString: " + rowsRpt);
		NDPage.setDisplayFieldValue("rowsDisplayed", new String(rowsRpt));

		//logger.debug("MWQH@PopulateRowsDisplayed::FINISHED!!");
		//--> Added hidden fields for Scrolling JavaScript Function
		//--> By Billy 21July2003
		NDPage.setDisplayFieldValue(
			"hdSelectedRowID",
			"" + tds.getRelSelectedRowNdx());
		NDPage.setDisplayFieldValue(
			"hdCurrentFirstRowID",
			"" + tds.getFirstDisplayRowNumber());
	}

	// If priority tasks have been displayed set the acknowledged flag in work queue
	// records.
	////////////////////////////////////////////////////////////////////////
	////public int checkDisplayThisRow(String cursorName)
	public boolean checkDisplayThisRow(String cursorName) {
		try {
			////SessionState theSession = getTheSession();
			PageEntry pg = theSessionState.getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds = (PageCursorInfo) pst.get(cursorName);

			if (tds.getTotalRows() <= 0) {
				return false;
			}

			return true;
		} catch (Exception e) {
			logger.error(
				"Exception @checkDisplayThisRow: Section " + cursorName);
			logger.error(e);

			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////
	public void handleSubmit() {
		handleCancelStandard();
	}

	/**
	 *
	 *
	 */
      private void populateSortFilterParams(PageEntry pg, PageCursorInfo tds)
      {
        try
        {
                int filterOptValue = pg.getPageCriteriaId1();
                int sortOptValue = tds.getSortOption();
                int statusFilterValue = pg.getPageCriteriaId2();
                int dealIdFilterValue = pg.getPageCriteriaId3();

                //--TD_MWQ_CR--start//
                Hashtable pst = pg.getPageStateTable();
                Map<Integer, Integer> branches = (Map<Integer,Integer>)pst.get("branches");
                Vector groups = (Vector) pst.get("groups");

                String stTaskName = "";

                String branchesFilterDesc = getBranchFilterDesc();
                String groupsFilterDesc = getGroupFilterDesc();

                ListBox theBranches =
                        (ListBox) (getCurrNDPage().getChild("lbMWQBranches"));
                ListBox theGroups =
                        (ListBox) (getCurrNDPage().getChild("lbMWQGroups"));

                //--TD_MWQ_CR--end//
                //// Temp debug info.
                //logger.debug("MWQH@populateSortFilterParams::FilterOptValue: " + filterOptValue);
                //logger.debug("MWQH@populateSortFilterParams::SortOptValue: " + sortOptValue);
                //logger.debug("MWQH@populateSortFilterParams::StatusFilterValue: " + statusFilterValue);
                //logger.debug("MWQH@populateSortFilterParams::DealIdFilterValue: " + dealIdFilterValue);
                //logger.debug("MWQH@populateSortFilterParams::BranchSize: " + branches.size());
                //logger.debug("MWQH@populateSortFilterParams::GroupSize: " + groups.size());
                //logger.debug("MWQH@populateSortFilterParams::SelBranches: " + branchesFilterDesc);
                //logger.debug("MWQH@populateSortFilterParams::SelGroups: " + groupsFilterDesc);
                //logger.debug("MWQH@populateSortFilterParams::TaskNameOptValue_PageCriteriaId4: " + taskNameOptValue);
                ComboBox filterBox =
                        (ComboBox) (getCurrNDPage().getDisplayField("cbUserFilter"));
                ComboBox sortBox =
                        (ComboBox) (getCurrNDPage().getDisplayField("cbSortOptions"));
                ComboBox statusFilterBox =
                        (ComboBox) (getCurrNDPage().getDisplayField("cbStatusFilter"));

                //--TD_MWQ_CR--start--//
                ////1. Prepare the Summary Task Description static text for setup.
                ComboBox taskNameFilterBox =
                        (ComboBox) getCurrNDPage().getDisplayField("cbTaskNameFilter");

                ////int taskNameFltrNdx;
                String taskNameFilterValue = (String) taskNameFilterBox.getValue();

                if (taskNameFilterValue.length()>1) {

                    String taskName = taskNameFilterValue.substring(2,
                        taskNameFilterValue.length() - 2);
                    String[] result = taskName.split("\\}, \\{");

                    String[] inner = result[0].split("=");
                    int instId = Integer.valueOf(inner[0]).intValue();
                    int taskId = Integer.valueOf(inner[1]).intValue();
                    stTaskName = BXResources.getPickListDescription(instId,
                            "TASK741",
                            taskId,
                            theSessionState.getLanguageId());

                } else {
                        stTaskName = "";
                }

				// SEAN Ticket #2457 Nov 30, 2005: populate the branhes filter.
				if (branches != null) {

                    Iterator instIds = branches.keySet().iterator();
                    int size = branches.size();

                    Object[] listVector = new Object[size];

                    for (int i = 0; i < size; i++) {
                        Integer branchId = (Integer) instIds.next();
                        int branchIdVal = branchId.intValue();
                        int instId = ((Integer) branches.get(branchId))
                                .intValue();

                        listVector[i] = new String(branchIdVal + ", " + instId);

                    }
                    theBranches.setValues(listVector, true);

                }
				// SEAN Ticket #2457 END

                //// 2. Populate Location Filter Part if com.basis100.masterworkqueue.filtertasks property
                //// narrowing initial task display by region, branch, or group.
                int realmId = getRealm();
                logger.debug("MWQH@populateSortFilterParams::RealmId: " + realmId);

                if (realmId > 0) {
                        String locationCriteria =
                                getLocationPathLabel(srk, realmId, pst);
                        logger.debug(
                                "MWQH@populateSortFilterParams::locationCriteria: "
                                        + locationCriteria);

                        getCurrNDPage().setDisplayFieldValue(
                                "stUserFilter",
                                new String(locationCriteria));
                        getCurrNDPage().setDisplayFieldValue(
                                "stLocationFull",
                                new String(locationCriteria));
                } else {
                        getCurrNDPage().setDisplayFieldValue(
                                "stUserFilter",
                                new String(""));
                        getCurrNDPage().setDisplayFieldValue(
                                "stLocationFull",
                                new String(""));
                }

                //--TD_MWQ_CR--end--//
                // ensure selected user is current selection in choice list and populate
                // selected user name field
                if (filterOptValue < 0)
                {
                        //--Release2.1--//
                        //// Deal word should be converted to the LABEL in order to translate on fly
                        // Set if DealId filter applied -- By Billy 06Dev2001
                        String dealLabel =
                                BXResources.getGenericMsg(
                                        "MWQ_DEAL_LABEL",
                                        theSessionState.getLanguageId());
                        String numberLabel =
                                BXResources.getGenericMsg(
                                        "MWQ_NUMBER_LABEL",
                                        theSessionState.getLanguageId());

                        //--TD_MWQ_CR--start//
                        //// 1. Populate the rest of the filter's parts.
                   		// FXP26606
                        if (pg.getMWQTaskNameFilter().length() > 0)
                        {
                            //// Assign the whole content to the ToolTip.
                            getCurrNDPage().setDisplayFieldValue("stTaskName",
                                                                 new String(stTaskName));

                            //// Truncate the text for displaying on the screen.
                            if (stTaskName.length() > TRANCATED_TASKNAME_LENGTH)
                            {
                                    String trancTaskName = stTaskName.substring(0, TRANCATED_TASKNAME_LENGTH)
                                                           + "...";
                                    getCurrNDPage().setDisplayFieldValue("stTaskName", new String(trancTaskName));
                                    getCurrNDPage().setDisplayFieldValue("stTaskNameFull", new String(stTaskName));
                            }
                            else
                            {
                                    getCurrNDPage().setDisplayFieldValue("stTaskName", new String(stTaskName));
                                    getCurrNDPage().setDisplayFieldValue("stTaskNameFull", new String(stTaskName));
                            }
                        }
                        else
                        {
                                getCurrNDPage().setDisplayFieldValue(
                                        "stTaskName",
                                        new String(
                                                BXResources.getGenericMsg(
                                                        "MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL",
                                                        theSessionState.getLanguageId())));
                                getCurrNDPage().setDisplayFieldValue(
                                        "stTaskNameFull",
                                        new String(
                                                BXResources.getGenericMsg(
                                                        "MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL",
                                                        theSessionState.getLanguageId())));
                        }

                        if (dealIdFilterValue > 0) {
                                getCurrNDPage().setDisplayFieldValue(
                                        "stUserFilter",
                                        new String(
                                                ""
                                                        + "("
                                                        + dealLabel
                                                        + " "
                                                        + numberLabel
                                                        + " "
                                                        + dealIdFilterValue
                                                        + ")"));
                        } else if (
                                (pg.getPageCondition5() == true)
                                        || (pg.getPageCondition6() == true)) {
                                //// Assign the whole content to the ToolTip.
                                getCurrNDPage().setDisplayFieldValue(
                                        "stLocationFull",
                                        new String(branchesFilterDesc + groupsFilterDesc));

                                //// Truncate the text for displaying on the screen.
                                if ((branchesFilterDesc.length()
                                        + groupsFilterDesc.length())
                                        > TRANCATED_LOCATION_LENGTH) {
                                        String temp = branchesFilterDesc + groupsFilterDesc;
                                        String trancLocation =
                                                temp.substring(0, TRANCATED_LOCATION_LENGTH)
                                                        + "...";
                                        getCurrNDPage().setDisplayFieldValue(
                                                "stUserFilter",
                                                new String(trancLocation));
                                } else {
                                        getCurrNDPage().setDisplayFieldValue(
                                                "stUserFilter",
                                                new String(branchesFilterDesc + groupsFilterDesc));
                                }
                        } else if (realmId == 0) {
                                getCurrNDPage().setDisplayFieldValue(
                                        "stUserFilter",
                                        new String(""));
                                getCurrNDPage().setDisplayFieldValue(
                                        "stLocationFull",
                                        new String(""));
                        }

                //--TD_MWQ_CR--end//
                }
                else
                {
                        filterBox.setValue(new Integer(filterOptValue));

                        //--TD_MWQ_CR--start--//
                        //FIXME:
                        //taskNameFilterBox.setValue(new Integer(taskNameOptValue));

                        //--TD_MWQ_CR_end--//
                        // selected user field
                        try
                        {
                          doUnderwriter1ModelImpl theDO = (doUnderwriter1ModelImpl) (RequestManager.getRequestContext()
                                                          .getModelManager()
                                                          .getModel(doUnderwriter1Model.class));

                          theDO.clearUserWhereCriteria();
                          theDO.addUserWhereCriterion("dfUserProfileId", "=", new Integer(filterOptValue));

                          String userName = null;

                          try
                          {
                              ResultSet rs = theDO.executeSelect(null);

                              if ((rs != null) && (theDO.getSize() > 0))
                              {
                                userName = theDO.getValue(doUnderwriter1ModelImpl.FIELD_DFUSERNAME).toString();

                                //logger.debug("MWQH@populateSortFilterParams::LastName: " + userName.toString());
                                //logger.debug("MWQH@populateSortFilterParams::Size of the taskCount result set: " + theDO.getSize());
                              } else
                              {
                                logger.error("MWQH@populateSortFilterParams::Problem when getting the userName !!");
                              }
                          }
                          catch (Exception e)
                          {
                                  logger.error("MWQH@populateSortFilterParams:: " + e);
                          }

                          if (dealIdFilterValue > 0)
                          {
                            getCurrNDPage().setDisplayFieldValue("stUserFilter",
                                                                new String(userName
                                                                 + "(Deal # "
                                                                 + dealIdFilterValue + ")"));
                          }
                          else
                          {
                                  //// Assign the whole content to the ToolTip.
                                  getCurrNDPage().setDisplayFieldValue(
                                          "stLocationFull",
                                          new String(
                                                  branchesFilterDesc
                                                          + groupsFilterDesc
                                                          + userName));

                                  //// Truncate the text for displaying on the screen.
                                  if ((branchesFilterDesc.length()
                                          + groupsFilterDesc.length()
                                          + userName.length())
                                          > TRANCATED_LOCATION_LENGTH) {
                                          String temp =
                                                  branchesFilterDesc
                                                          + groupsFilterDesc
                                                          + userName;
                                          String trancLocation =
                                                  temp.substring(0, TRANCATED_LOCATION_LENGTH)
                                                          + "...";
                                          getCurrNDPage().setDisplayFieldValue(
                                                  "stUserFilter",
                                                  new String(trancLocation));
                                  } else {
                                          getCurrNDPage().setDisplayFieldValue(
                                                  "stUserFilter",
                                                  new String(
                                                          branchesFilterDesc
                                                                  + groupsFilterDesc
                                                                  + userName));
                                  }
                          }
                        } catch (Exception e) {
                                // can't match on selected user - should never happen but ...
                                getCurrNDPage().setDisplayFieldValue(
                                        "stUserFilter",
                                        new String(" "));
                                logger.error(
                                        "Exception MWQH@populateSortFilterParams: getting selected user name - ignored");
                                logger.error(e);
                        }
                }

                //--Release2.1--start//
                String defaultLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_DEFAULT_LABEL",
                                theSessionState.getLanguageId());
                String priorityLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_PRIORITY_LABEL",
                                theSessionState.getLanguageId());
                String dealASCLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_DEAL_NUMBER_ASC_LABEL",
                                theSessionState.getLanguageId());
                String dealDECLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_DEAL_NUMBER_DEC_LABEL",
                                theSessionState.getLanguageId());
                String statusLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_STATUS_LABEL",
                                theSessionState.getLanguageId());
                String borrNameLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_BORROWER_NAME_LABEL",
                                theSessionState.getLanguageId());
                String warningLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_WARNING_LABEL",
                                theSessionState.getLanguageId());
                String taskDescLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_TASK_DESCRIPTION_LABEL",
                                theSessionState.getLanguageId());

                //-- ========== SCR#876 begins ========== --//
                //-- by Neil on Jan 20, 2005
                String dealStatusLabel =
                        BXResources.getGenericMsg(
                                "MWQ_SORT_ORDER_OPTIONS_DEAL_STATUS_LABEL",
                                theSessionState.getLanguageId());

                //String[] MWQ_SORT_ORDER_OPTIONS =
                //	{
                //		defaultLabel,
                //		priorityLabel,
                //		dealASCLabel,
                //		dealDECLabel,
                //		statusLabel,
                //		borrNameLabel,
                //		warningLabel,
                //		taskDescLabel };
                String[] MWQ_SORT_ORDER_OPTIONS =
                        {
                                defaultLabel,
                                priorityLabel,
                                dealASCLabel,
                                dealDECLabel,
                                statusLabel,
                                borrNameLabel,
                                warningLabel,
                                taskDescLabel,
                                dealStatusLabel };

                //-- ========== SCR#876   ends ========== --//
                //========================================================
                String openTasksLbl =
                        BXResources.getGenericMsg(
                                "MWQ_STATUS_TASK_CBOPTIONS_OPEN_LABEL",
                                theSessionState.getLanguageId());
                String completedTasksLbl =
                        BXResources.getGenericMsg(
                                "MWQ_STATUS_TASK_CBOPTIONS_COMPLITE_LABEL",
                                theSessionState.getLanguageId());
                String allTasksLbl =
                        BXResources.getGenericMsg(
                                "MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL",
                                theSessionState.getLanguageId());

                //-- ========== SCR#876 begins ========== --//
                //-- by Neil on Jan/19/2005
                //String[] MWQ_STATUS_TASK_OPTIONS =
                //{ openTasksLbl, completedTasksLbl, allTasksLbl  //Always goes last
                //};
                String onholdTasksLbl =
                        BXResources.getGenericMsg(
                                "MWQ_STATUS_TASK_CBOPTIONS_ONHOLD_LABEL",
                                theSessionState.getLanguageId());
                String[] MWQ_STATUS_TASK_OPTIONS =
                        {
                                openTasksLbl,
                                completedTasksLbl,
                                onholdTasksLbl,
                                allTasksLbl //Always goes last
                };

                //-- ========== SCR#876   ends ========== --//
                // sort option
                if (sortOptValue == 0) {
                        sortOptValue = Mc.MWQ_SORT_DUE_DATE;
                }

                sortBox.setValue(new Integer(sortOptValue));
                statusFilterBox.setValue(new Integer(statusFilterValue));

                logger.debug(
                        "MWQH@populateSortFilterParams::SortOptValue: " + sortOptValue);
                logger.debug(
                        "MWQH@populateSortFilterParams::StatusFilterValue: "
                                + statusFilterValue);

                // populate sort option selected
                String s = new String(MWQ_SORT_ORDER_OPTIONS[sortOptValue]);

                //getCurrNDPage().setDisplayFieldValue("stSortOption",
                //  new String(MWQ_SORT_ORDER_OPTIONS[sortOptValue]));
                getCurrNDPage().setDisplayFieldValue("stSortOption", s);
                s = new String(MWQ_STATUS_TASK_OPTIONS[statusFilterValue]);
                getCurrNDPage().setDisplayFieldValue("stStatusFilter", s);

                // populate DealId Filter value
                if (dealIdFilterValue > 0) {
                        getCurrNDPage().setDisplayFieldValue(
                                "tbDealIdFilter",
                                new Integer(dealIdFilterValue));
                } else {
                        getCurrNDPage().setDisplayFieldValue(
                                "tbDealIdFilter",
                                new String(""));
                }

                // Added checkbox to display Hidden tasks or not
                // By Billy 08Oct2002
                if (pg.getPageCondition3() == true) {
                        getCurrNDPage().setDisplayFieldValue(
                                "chIsDisplayHidden",
                                new String("T"));
                } else {
                        getCurrNDPage().setDisplayFieldValue(
                                "chIsDisplayHidden",
                                new String("F"));
                }

                //--TD_MWQ_CR--//
                //// Populate TaskName filter.
        		// FXP26606
                if (pg.getMWQTaskNameFilter().length() >0 ) {
                        //// Assign the whole content to the ToolTip.
                        getCurrNDPage().setDisplayFieldValue(
                                "stTaskName",
                                new String(stTaskName));

                        //// Truncate the text for displaying on the screen.
                        if (stTaskName.length() > TRANCATED_TASKNAME_LENGTH) {
                                String trancTaskName =
                                        stTaskName.substring(0, TRANCATED_TASKNAME_LENGTH)
                                                + "...";
                                getCurrNDPage().setDisplayFieldValue(
                                        "stTaskName",
                                        new String(trancTaskName));
                                getCurrNDPage().setDisplayFieldValue(
                                        "stTaskNameFull",
                                        new String(stTaskName));
                        } else {
                                getCurrNDPage().setDisplayFieldValue(
                                        "stTaskName",
                                        new String(stTaskName));
                                getCurrNDPage().setDisplayFieldValue(
                                        "stTaskNameFull",
                                        new String(stTaskName));
                        }
                } else {
                        getCurrNDPage().setDisplayFieldValue(
                                "stTaskName",
                                new String(
                                        BXResources.getGenericMsg(
                                                "MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL",
                                                theSessionState.getLanguageId())));
                        getCurrNDPage().setDisplayFieldValue(
                                "stTaskNameFull",
                                new String(
                                        BXResources.getGenericMsg(
                                                "MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL",
                                                theSessionState.getLanguageId())));
                }
        } catch (Exception e) {
                logger.error("Exception @populateSortFilterParams: ignored");
                logger.error(StringUtil.stack2string(e));
        }
	}

	//--TD_MWQ_CR--start//
	private String getBranchFilterDesc() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		//// 1. Form the branch filter criteria.
		ListBox theBranches =
			(ListBox) (getCurrNDPage().getChild("lbMWQBranches"));
		Object[] theSelectedBranches = theBranches.getValues();
		String selBranches = "";

		if (theSelectedBranches.length > 0) {
			int branchId = 0;

			////logger.debug("MWQH@populateSortFilterParams::NumberOfBrancesSelected = " + theSelectedBranches.length);
			for (int i = 0; i < theSelectedBranches.length; i++) {
				if (!theSelectedBranches[i].toString().equals("")) {

                    String branchStr = theSelectedBranches[i].toString();
                    String[] result = branchStr.split(", ");

                    branchId = Integer.valueOf(result[0]).intValue();
                    int instId = Integer.valueOf(result[1]).intValue();

                    theSelectedBranches[i].toString();

					selBranches += BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),
							"BRANCHPROFILE",
							branchId,
							theSessionState.getLanguageId());

					if (theSelectedBranches.length == 1) {
						selBranches += "/";
					} else if (theSelectedBranches.length >= 1) {
						if (i < (theSelectedBranches.length - 1)) {
							selBranches += "+";
						} else {
							selBranches += "/";
						}
					}
				}
			}

			// end for
		}

		// end branch if
		return selBranches;
	}

	private String getGroupFilterDesc() {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		//// 1. Form the group filter criteria.
		ListBox theGroups = (ListBox) (getCurrNDPage().getChild("lbMWQGroups"));
		Object[] theSelectedGroups = theGroups.getValues();
		String selGroups = "";

		if (theSelectedGroups.length > 0) {
			int groupId = 0;

			for (int i = 0; i < theSelectedGroups.length; i++) {
				if (!theSelectedGroups[i].toString().equals("")) {
					groupId =
						com.iplanet.jato.util.TypeConverter.asInt(
							theSelectedGroups[i],
							0);

					selGroups
						+= BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),
							"GROUPPROFILE",
							groupId,
							theSessionState.getLanguageId());

					if (theSelectedGroups.length == 1) {
						selGroups += "/";
					} else if (theSelectedGroups.length >= 1) {
						if (i < (theSelectedGroups.length - 1)) {
							selGroups += "+";
						} else {
							selGroups += "/";
						}
					}
				}
			}

			// end for
		}

		// end group if
		return selGroups;
	}

	private String getLocationPathLabel(
		SessionResourceKit srk,
		int locationLevel,
		Hashtable pst) {
		logger.trace("getLocationPathLabel");

		if (pst == null) {
			return "";
		}

		LocationWrapper locationWrapper =
			(LocationWrapper) (pst.get("locationWrapper"));
		String locationLabel = "";

		if (locationWrapper == null) {
			return locationLabel;
		}

		switch (locationLevel) {
			case REALM_TYPE_ANY :
				return locationLabel;

			case REALM_TYPE_REGION :
				{
					locationLabel =
						BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),
							"REGIONPROFILE",
							locationWrapper.getRegionProfileId(),
							theSessionState.getLanguageId());

					//logger.debug("MWQH@@getLocationPathLabel::RegionCase: " + locationLabel);
					return locationLabel;
				}

			case REALM_TYPE_BRANCH :
				{
					String region =
						BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),
							"REGIONPROFILE",
							locationWrapper.getRegionProfileId(),
							theSessionState.getLanguageId());
					String branch =
						BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),
							"BRANCHPROFILE",
							locationWrapper.getBranchProfileId(),
							theSessionState.getLanguageId());

					//// Currently we are using only branch label. Down the road when the group part
					//// will be used it could be customized based on clients' desire.
					////locationLabel = region + "/" + branch;
					locationLabel = branch;

					//logger.debug("MWQH@@getLocationPathLabel::BranchCase: " + locationLabel);
					return locationLabel;
				}

			case REALM_TYPE_GROUP :
				{
					String region =
						BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),
							"REGIONPROFILE",
							locationWrapper.getRegionProfileId(),
							theSessionState.getLanguageId());
					String branch =
						BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),
							"BRANCHPROFILE",
							locationWrapper.getBranchProfileId(),
							theSessionState.getLanguageId());
					String group =
						BXResources.getPickListDescription(this.theSessionState.getUserInstitutionId(),
							"GROUPPROFILE",
							locationWrapper.getGroupProfileId(),
							theSessionState.getLanguageId());

					locationLabel = region + "/" + branch + "/" + group;

					//logger.debug("MWQH@@getLocationPathLabel::GroupCase: " + locationLabel);
					return locationLabel;
				}

			default :
				return locationLabel;
		}
	}

	//This is a implementation of the Task view accessibility logic required by TD.
	//
	//		If UserProfile.UserType = 'System Administrator'
	//			All tasks/deals are available.
	//		ElseIf UserProfile = ManagerID in one of the Region Profiles
	//			All tasks/deals for that region are available.
	//		ElseIf UserProfile = ManagerID in one of the Branch Profiles
	//			All tasks/deals for that branch are available.
	//		ElseIf UserProfile = SuperviserID in one of the Group Profiles
	//			All tasks/deals for that group are available.
	//		Else
	//			nothing can be displayed.
	private String addLocationCriteria(Hashtable pst, int narrower) {
		logger.trace("MasterWorkQueueHandler@addLocationCriteria");

		if (pst == null) {
			return "";
		}

		LocationWrapper locationWrapper =
			(LocationWrapper) (pst.get("locationWrapper"));
		String sqlStr = "";

		if (locationWrapper == null) {
			return sqlStr;
		}

		switch (narrower)
                {
                  case REALM_TYPE_ANY :
                          return sqlStr;

                  case REALM_TYPE_REGION :
                  {
                      sqlStr = " AND ";

                      sqlStr += ("(BRANCHPROFILE.REGIONPROFILEID = "
                                + locationWrapper.getRegionProfileId() + ")");

                      logger.debug("MWQH@@addLocationCriteria::SQL_RegionCase: " + sqlStr);

                      return sqlStr;
                  }

                  case REALM_TYPE_BRANCH :
                  {
                      sqlStr = " AND ";

                      sqlStr += ("(BRANCHPROFILE.REGIONPROFILEID = "
                                      + locationWrapper.getRegionProfileId() + ")");
                      sqlStr += " AND ";

                      sqlStr += ("(BRANCHPROFILE.BRANCHPROFILEID = "
                                      + locationWrapper.getBranchProfileId() + ")");

                      logger.debug("MWQH@@addLocationCriteria::SQL_BranchCase: " + sqlStr);

                      return sqlStr;
                  }

                  case REALM_TYPE_GROUP :
                  {
                      sqlStr = " AND ";

                      sqlStr += ("(BRANCHPROFILE.REGIONPROFILEID = "
                                      + locationWrapper.getRegionProfileId() + ")");
                      sqlStr += " AND ";

                      sqlStr += ("(BRANCHPROFILE.BRANCHPROFILEID = "
                                      + locationWrapper.getBranchProfileId() + ")");
                      sqlStr += " AND ";

                      sqlStr += ("(GROUPPROFILE.GROUPPROFILEID = "
                                      + locationWrapper.getGroupProfileId() + ")");

                      logger.debug("MWQH@@addLocationCriteria::SQL_GroupCase: " + sqlStr);

                      return sqlStr;
                  }

                  default :
                          return sqlStr;
		}
	}

	/**
	 *
	 *
	 */
	private void getLocationCriteria(Hashtable pst) {

		PageEntry pg = theSessionState.getCurrentPage();

		SessionResourceKit srk = getSessionResourceKit();

		int userId = pg.getPageMosUserId();

		logger.trace("MasterWorkQueueHandler@getLocationCriteria()");

		try {
            Map<Integer, LocationWrapper> locationWrappers =
                new HashMap<Integer, LocationWrapper>();
            Map<Integer, LocationCriteriaHelper> locationCriteriaHelpers =
                new HashMap<Integer, LocationCriteriaHelper>();


            for(Integer institutionId: theSessionState.institutionList()){

                int groupProfileId = theSessionState.getGroupProfileId(institutionId);
                GroupProfile group = new GroupProfile(srk, groupProfileId);
                int branchProfileId = group.getBranchProfileId();
                BranchProfile branch = new BranchProfile(srk, branchProfileId);
                int regionProfileId = branch.getRegionProfileId();
                RegionProfile region = new RegionProfile(srk, regionProfileId);

                //// Create wrapper object.
                LocationWrapper lw = new LocationWrapper();
                lw.setRegionProfileId(regionProfileId);
                lw.setBranchProfileId(branchProfileId);
                lw.setGroupProfileId(groupProfileId);
                locationWrappers.put(institutionId, lw);

                LocationCriteriaHelper location =
                    new LocationCriteriaHelper(REALM_TYPE_ANY, 0);
                //// BX: finalize with the Product Team the userId relationship for this
                //// CR: whether it is user dependant or not.
                if (region.getRegionManagerUserId() == userId) {
                    location =
                        new LocationCriteriaHelper(
                            REALM_TYPE_REGION,
                            regionProfileId);
                    logger.trace(
                        "MWQ@getLocationCriteria::CaseRegionManagerUserId");
                } else if (branch.getBranchManagerUserId() == userId) {
                    location =
                        new LocationCriteriaHelper(
                            REALM_TYPE_BRANCH,
                            branchProfileId);
                    logger.trace(
                        "MWQ@getLocationCriteria::CaseBranchManagerUserId");
                } else if (group.getSupervisorUserId() == userId) {
                    location =
                        new LocationCriteriaHelper(
                            REALM_TYPE_GROUP,
                            groupProfileId);
                    logger.trace(
                        "MWQ@getLocationCriteria::CaseSupervisorUserId");

                }
                locationCriteriaHelpers.put(institutionId, location);
            }
            pst.put("locationWrapper", locationWrappers);
	        pst.put("locationCriteria", locationCriteriaHelpers);
		} catch (Exception e) {
			logger.error(
				"Exception MasterWorkQueueHandler@getLocationCriteria()");
			logger.error(e);
			logger.error(StringUtil.stack2string(e));
			setStandardFailMessage();
		}

		return;
	}

	//Comment for ML:
	// if user can access more than Institutions,
	// then property com.basis100.masterworkqueue.filtertasks should be same?
	private int getRealm() {

	    String filterTasks
	        = PropertiesCache.getInstance().getProperty(theSessionState.getUserInstitutionId(),
	                                                    "com.basis100.masterworkqueue.filtertasks",
	                                                    "N");

		if ( "R".equals(filterTasks)) {
			return 1;
		}
		if ( "B".equals(filterTasks)) {
			return 2;
		}
		if ( "G".equals(filterTasks)) {
			return 3;
		}

		return 0;
	}

	//--TD_MWQ_CR--end//
	// Method to sync the Current Displayed Row between the Repeated Object and the CursorInfo.
	//  -- By Billy 24Jan2002
	public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get("DEFAULT");

		try {
			//--> So far we cannot find a way to manually set the Diaplay Offset of the TiledView here.
			//--> The reason is some necessary method is protected (e.g. setWebActionModelOffset()). The
			//--> work around is to create our own method (setCurrentDisplayOffset) in the TiledView class
			//--> (e.g. pgIWorkQueueRepeated1TiledView).  However, this method have to be implemented in all
			//--> TiledView which have Forward/Backward buttons.
			//--> Need to investigate more later  !!!!
			//--Release2.1--//
			//--> Sync it in any case. -- By Billy 17Dec2002
			((pgMWorkQueueRepeated1TiledView) theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
		} catch (Exception mce) {
			logger.error(
				"MWQH@checkAndSyncDOWithCursorInfo::Exception: " , mce);
		}
	}

	//// BX: Move to the base class later.
	private String cleanNumber(String strNum) {
		strNum = strNum.trim();

		int ndx = strNum.indexOf(".");

		if (ndx > 0) {
			return strNum.substring(0, ndx);
		}

		return strNum;
	}

	//-- ========== SCR#750 begins ============================================ --//
	//-- by Neil on Dec/20/2004

	/**
	 * According to SCR#750, both <i>TaskStatus</i> and <i>HoldReason</i> should share the same
	 * TextField component for displaying. Usually, the TextField is used to display TaskStatus.
	 * When the DEAL.HOLDREASONID is not <i>0</i>,  however, it will be used to display
	 * HoldReason description. This method changes the binding for that TextField to HoldReason
	 * instead of TaskStatus.
	 *
	 * @throws Exception
	 */
	protected void setTaskStatusWithHoldReason() throws Exception {
		doMasterWorkQueueModel doMWQ = (doMasterWorkQueueModel) (RequestManager.getRequestContext().getModelManager().getModel(doMasterWorkQueueModel.class));
		String holdReason = null;
		int holdReasonId = 0;
		String taskStatus = null;
		BigDecimal instId = doMWQ.getDfInstitutionId();
		try {
			// do not need to check if rs==null
			// because the result is the existing one not new.
			// if (rs != null && doIWQ.getSize() > 0) {
			if (doMWQ.getSize() > 0)
                        {
				Object dfHoldReasonValue = doMWQ.getValue(
						doMasterWorkQueueModelImpl.FIELD_DFHOLDREASONDESCRIPTION);
				Object dfHoldReasonIdValue = doMWQ.getValue(
						doMasterWorkQueueModelImpl.FIELD_DFHOLDREASONID);
				Object dfTaskStatusValue = doMWQ.getValue(
						doMasterWorkQueueModelImpl.FIELD_DFSTATUSDESC);

				if (dfHoldReasonValue == null)
                                {
					holdReason = null;
				}
                                else
                                {
					holdReason = dfHoldReasonValue.toString();
				}

				if (dfHoldReasonIdValue == null)
                                {
					String strErr = "@MasterWorkQueueHandler.setTaskStatusWithHoldReason: "
							+ "Invalide HOLDREASONID found.";
					throw new ModelControlException(strErr);
				}
                                else
                                {
					holdReasonId = new Integer(dfHoldReasonIdValue.toString()).intValue();
				}

				if (dfTaskStatusValue == null)
                                {
					String strErr = "@MasterWorkQueueHandler.setTaskStatusWithHoldReason: "
							+ "Invalide TASKSTATUS found.";
					throw new ModelControlException(strErr);
				}
                                else
                                {
					taskStatus = dfTaskStatusValue.toString();
				}
			}
                        else
                        {
				// SEAN Ticket #1979 Sept 16, 2005: change from error level to info level.
				logger.info( "@MasterWorkQueueHandler.setTaskStatusWithHoldReason: There is no task for this user.");
				// SEAN Ticket #1979 END
			}

		}
                catch (ModelControlException mce)
                {
			String strError =
				"@MasterWorkQueueHandler.setTaskStatusWithHoldReason: " + mce.getMessage();
			logger.error(mce);
			logger.error(strError);
			throw new Exception(strError);

			//		} catch (SQLException sqle) {
			//			String strError =
			//				"@MasterWorkQueueHandler.setTaskStatusWithHoldReason(): "
			//					+ sqle.getMessage();
			//			logger.error(sqle);
			//			logger.error(strError);
			//			throw new Exception(strError);
		}


		// set TaskStatus with HoldReason if holdReasonId is not 0.
		if ((holdReasonId == 0) || (holdReason == null))
                {
			// display with TaskStatus
			getCurrNDPage().setDisplayFieldValue("Repeated1/stTaskStatus", taskStatus);

			// keep the default style.
			getCurrNDPage().setDisplayFieldValue("Repeated1/taskStatusOrHoldReasonStyle", "");
		}
                else
                {
			// display with HoldReason
                        logger.debug("@MasterWorkQueueHandler.setTaskStatusWithHoldReason(): holdReason=" + holdReason);

                        /***** GR 3.2.2 On Hold Functionality start *****/
                        long currentTime = System.currentTimeMillis();
                        Timestamp dfDueTime = (Timestamp)doMWQ
                                        .getValue(doMasterWorkQueueModelImpl.FIELD_DFDUETIMESTAMP);
                        long dueTime = dfDueTime.getTime();

                        if (!onHoldNotDisplay(instId.intValue()) || dueTime > currentTime ) {
                            // display with HoldReason
                            String strOnHold =
                              BXResources.getGenericMsg(
                              "ON_HOLD_LABEL",
                              theSessionState.getLanguageId());
                            getCurrNDPage().setDisplayFieldValue(
                                    "Repeated1/stTaskStatus",
                                    strOnHold + " / " + holdReason);
                            // change style.
                            getCurrNDPage().setDisplayFieldValue(
                              "Repeated1/taskStatusOrHoldReasonStyle",
                              "color: #FF0000;");
                        }
                        /***** GR 3.2.2 On Hold Functionality end *****/

		}
	}

	/**
	 *
	 *
	 */
	public class LocationCriteriaHelper extends Vector {
		public LocationCriteriaHelper(int realmType, int realmId) {
			clear();
			addElement(new Integer(realmType));
			addElement(new Integer(realmId));
		}

		public int getRealmType() {
			logger.debug("LCH@getRealmType() -> " + new Integer(get(0).toString()).intValue());

			return new Integer(get(0).toString()).intValue();
		}

		public int getRealmId() {
			logger.debug("LCH@getRealmId() -> " + new Integer(get(1).toString()).intValue());

			return new Integer(get(1).toString()).intValue();
		}
	}

	/**
	 *
	 *
	 *
	 */
	public class LocationWrapper {
		private int regionProfileId;
		private int branchProfileId;
		private int groupProfileId;

		// "getters" methods.
		public int getRegionProfileId() {
			return regionProfileId;
		}

		public int getBranchProfileId() {
			return branchProfileId;
		}

		public int getGroupProfileId() {
			return groupProfileId;
		}

		// "setters" methods.
		public void setRegionProfileId(int aRegionProfileId) {
			regionProfileId = aRegionProfileId;
		}

		public void setBranchProfileId(int aBranchProfileId) {
			branchProfileId = aBranchProfileId;
		}

		public void setGroupProfileId(int aGroupProfileId) {
			groupProfileId = aGroupProfileId;
		}
	}

	//-- ========== SCR#750   ends ============================================ --//


    /*
     * method clear instution name in the task list
     * if user only access one institution
     */
    public void modifyTaskInstitutionName() {

        if (theSessionState.isMultiAccess() == false) {
            getCurrNDPage().setDisplayFieldValue("Repeated1/stInstitutionId",
                    new String(""));
        }
    }


    /*
     * method to generate criteria for branch
     */
    public String getInstitutionCriteria(Map<Integer, Integer> branchMap) {

        Iterator institutionIds = branchMap.keySet().iterator();
        int size = branchMap.size();
        StringBuffer userBuffer = new StringBuffer();

        for (int i = 0; i < size; i++) {
            Integer branchId = (Integer) institutionIds.next();
            int branchIdVal = branchId.intValue();
            int instId = ((Integer) branchMap.get(branchId))
                    .intValue();
            if (i > 0)
                userBuffer.append(" OR ");
                userBuffer.append("(BRANCHPROFILE.branchProfileID = "
                    + branchIdVal
                    + " AND BRANCHPROFILE.institutionProfileID = "
                    + instId + ")");
        }
        if( size > 1) {
            userBuffer.insert(0, "(");
            userBuffer.append(")");
        }
        if( size > 0)
            userBuffer.insert(0, " AND ");

        return userBuffer.toString();
    }

    /*
     * method to generate criteria string for taskname
     * fixed for FXP26606
     */
    public String getTaskNameCriteria(String taskNameFilter) {

        Map<Integer, Integer> taskMap = new HashMap<Integer, Integer>();
        String taskName = taskNameFilter.substring(2,
        		taskNameFilter.length() - 2);
        String[] result = taskName.split("\\}, \\{");
        for (int x = 0; x < result.length; x++) {
            String[] inner = result[x].split("=");
            for (int i = 0; i < inner.length; i++) {
                taskMap.put(new Integer(inner[0]), new Integer(inner[1]));
            }
        }

        Iterator institutionIds = taskMap.keySet().iterator();
        int size = taskMap.size();
        StringBuffer userBuffer = new StringBuffer();

        for (int i = 0; i < size; i++) {
            Integer institutionId = (Integer) institutionIds.next();
            int instId = institutionId.intValue();
            int taskId = ((Integer) taskMap.get(institutionId)).intValue();
            if (i > 0)
                userBuffer.append(" OR ");
            userBuffer.append("(ASSIGNEDTASKSWORKQUEUE.taskID = " + taskId
                    + " AND ASSIGNEDTASKSWORKQUEUE.institutionProfileID = "
                    + instId + ")");
        }
        if (size > 1) {
            userBuffer.insert(0, "(");
            userBuffer.append(")");
        }
        if (size > 0)
            userBuffer.insert(0, " AND ");

        return userBuffer.toString();
    }

    /**
     * Instituion's property setting for On Hold message display
     * GR 3.2.2 On Hold Functionality
     * @return boolean: true not display, false display
     */
    protected boolean onHoldNotDisplay(int instId) {
        boolean notDisplay = false;
        PageEntry pg = (PageEntry)theSessionState.getCurrentPage();
        String property = PropertiesCache.getInstance().getProperty(instId,
                "deal.onhold.stop.message.display.when.task.expires", "N");
        if (property.equalsIgnoreCase("Y"))
        	notDisplay = true;
        return notDisplay;
    }

    
    //FXP30170, 4.4, Oct 08, 2010, saving/resetting screen scroll position  -- start
    private void saveWindowScrollPosition(){
    	
    	PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		if (pst == null)
		{
			pst = new Hashtable();
			pg.setPageStateTable(pst);
		}
    	pst.put(pgMWorkQueueViewBean.CHILD_TXWINDOWSCROLLPOSITION, "");
    	
    	ViewBean thePage = getCurrNDPage();
    	HttpServletRequest request = thePage.getRequestContext().getRequest();
    	String scrollPos = request.getParameter(pgMWorkQueueViewBean.CHILD_TXWINDOWSCROLLPOSITION);
    	
    	if(scrollPos == null || scrollPos.length() < 1)return; // do nothing
    	pst.put(pgMWorkQueueViewBean.CHILD_TXWINDOWSCROLLPOSITION, scrollPos);
    }
    
    private void applyWindowScrollPosition(){
    	    	
    	PageEntry pg = theSessionState.getCurrentPage();
    	Hashtable pst = pg.getPageStateTable();
    	if (pst == null) return; // do nothing
    	
    	String scrollPos = (String)pst.get(pgMWorkQueueViewBean.CHILD_TXWINDOWSCROLLPOSITION);
    	pst.put(pgMWorkQueueViewBean.CHILD_TXWINDOWSCROLLPOSITION, ""); // clean
    	if (scrollPos == null || scrollPos.length() < 1) return; //do nothing
    	
    	ViewBean thePage = getCurrNDPage();
    	HttpServletRequest request = thePage.getRequestContext().getRequest();
    	request.setAttribute(pgMWorkQueueViewBean.CHILD_TXWINDOWSCROLLPOSITION, scrollPos);
    	
    }
    //FXP30170, 4.4, Oct 08, 2010, saving/resetting screen scroll position  -- start
    
}
