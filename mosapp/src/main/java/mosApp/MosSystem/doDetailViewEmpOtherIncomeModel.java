package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewEmpOtherIncomeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfIncomeTypeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeTypeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncomeEmpRelated();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeEmpRelated(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncomePeriodDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomePeriodDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomePercentageInGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomePercentageInGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomePercentageInTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomePercentageInTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncomeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFINCOMETYPEDESC="dfIncomeTypeDesc";
	public static final String FIELD_DFINCOMEEMPRELATED="dfIncomeEmpRelated";
	public static final String FIELD_DFINCOMEPERIODDESC="dfIncomePeriodDesc";
	public static final String FIELD_DFINCOMEAMOUNT="dfIncomeAmount";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFINCOMEPERCENTAGEINGDS="dfIncomePercentageInGDS";
	public static final String FIELD_DFINCOMEPERCENTAGEINTDS="dfIncomePercentageInTDS";
	public static final String FIELD_DFINCOMEDESC="dfIncomeDesc";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

