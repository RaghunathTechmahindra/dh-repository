package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doApplicantGSDTSDModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCombinedTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfCombinedTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCombinedGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfCombinedGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCombinedGDSBorrower();

	
	/**
	 * 
	 * 
	 */
	public void setDfCombinedGDSBorrower(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCombinedTDSBorrower();

	
	/**
	 * 
	 * 
	 */
	public void setDfCombinedTDSBorrower(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCombinedGDS3Year();

	
	/**
	 * 
	 * 
	 */
	public void setDfCombinedGDS3Year(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCombinedTDS3Year();

	
	/**
	 * 
	 * 
	 */
	public void setDfCombinedTDS3Year(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFCOMBINEDTDS="dfCombinedTDS";
	public static final String FIELD_DFCOMBINEDGDS="dfCombinedGDS";
	public static final String FIELD_DFCOMBINEDGDSBORROWER="dfCombinedGDSBorrower";
	public static final String FIELD_DFCOMBINEDTDSBORROWER="dfCombinedTDSBorrower";
	public static final String FIELD_DFCOMBINEDGDS3YEAR="dfCombinedGDS3Year";
	public static final String FIELD_DFCOMBINEDTDS3YEAR="dfCombinedTDS3Year";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

