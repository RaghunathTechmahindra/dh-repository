package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgApplicantEntryRepeatedOtherIncomeTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgApplicantEntryRepeatedOtherIncomeTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doApplicantOtherIncomesSelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdOtherIncomeId().setValue(CHILD_HDOTHERINCOMEID_RESET_VALUE);
		getHdOtherIncomeCopyId().setValue(CHILD_HDOTHERINCOMECOPYID_RESET_VALUE);
		getCbOtherIncomeType().setValue(CHILD_CBOTHERINCOMETYPE_RESET_VALUE);
		getTxOtherIncomeDesc().setValue(CHILD_TXOTHERINCOMEDESC_RESET_VALUE);
		getCbOtherIncomePeriod().setValue(CHILD_CBOTHERINCOMEPERIOD_RESET_VALUE);
		getTxOtherIncomeAmount().setValue(CHILD_TXOTHERINCOMEAMOUNT_RESET_VALUE);
		getCbOtherIncomeIncludeInGDS().setValue(CHILD_CBOTHERINCOMEINCLUDEINGDS_RESET_VALUE);
		getTxOtherIncomePercentIncludeInGDS().setValue(CHILD_TXOTHERINCOMEPERCENTINCLUDEINGDS_RESET_VALUE);
		getHdOtherIncomePercentIncludeInGDS().setValue(CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS_RESET_VALUE);
		getCbOtherIncomeIncludeInTDS().setValue(CHILD_CBOTHERINCOMEINCLUDEINTDS_RESET_VALUE);
		getTxOtherIncomePercentIncludeInTDS().setValue(CHILD_TXOTHERINCOMEPERCENTINCLUDEINTDS_RESET_VALUE);
		getHdOtherIncomePercentIncludeInTDS().setValue(CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS_RESET_VALUE);
		getBtDeleteOtherIncome().setValue(CHILD_BTDELETEOTHERINCOME_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDOTHERINCOMEID,HiddenField.class);
		registerChild(CHILD_HDOTHERINCOMECOPYID,HiddenField.class);
		registerChild(CHILD_CBOTHERINCOMETYPE,ComboBox.class);
		registerChild(CHILD_TXOTHERINCOMEDESC,TextField.class);
		registerChild(CHILD_CBOTHERINCOMEPERIOD,ComboBox.class);
		registerChild(CHILD_TXOTHERINCOMEAMOUNT,TextField.class);
		registerChild(CHILD_CBOTHERINCOMEINCLUDEINGDS,ComboBox.class);
		registerChild(CHILD_TXOTHERINCOMEPERCENTINCLUDEINGDS,TextField.class);
		registerChild(CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS,HiddenField.class);
		registerChild(CHILD_CBOTHERINCOMEINCLUDEINTDS,ComboBox.class);
		registerChild(CHILD_TXOTHERINCOMEPERCENTINCLUDEINTDS,TextField.class);
		registerChild(CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS,HiddenField.class);
		registerChild(CHILD_BTDELETEOTHERINCOME,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 05Nov2002
    ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Set up all ComboBox options
    cbOtherIncomeTypeOptions.populate(getRequestContext());
    cbOtherIncomePeriodOptions.populate(getRequestContext());

    //Setup the Yes/No Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());
    cbOtherIncomeIncludeInGDSOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});
    cbOtherIncomeIncludeInTDSOptions.setOptions(new String[]{yesStr, noStr},new String[]{"Y", "N"});

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated RepeatedOtherIncome_onBeforeRowDisplayEvent code here
      doApplicantOtherIncomesSelectModelImpl theObj =(doApplicantOtherIncomesSelectModelImpl)this.getdoApplicantOtherIncomesSelectModel();

		  if (theObj.getSize() > 0)
      {
        //--> Important !! If the field (in TiledView) is not binded to any DataField, we must
        //--> set the defaslt value here in order to populate the value in the Model (PrimaryModel).
        //--> Otherwise, when the page submitted (via button or Herf) the user input will not populate
        //--> Comment by BILLY 02Aug2002
        setDisplayFieldValue(CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS,
              new String(CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS_RESET_VALUE));
        setDisplayFieldValue(CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS,
              new String(CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS_RESET_VALUE));
        //============================================================================================
        return true;
      }
		  else return false;
		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
    return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDOTHERINCOMEID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_HDOTHERINCOMEID,
				doApplicantOtherIncomesSelectModel.FIELD_DFINCOMEID,
				CHILD_HDOTHERINCOMEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDOTHERINCOMECOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_HDOTHERINCOMECOPYID,
				doApplicantOtherIncomesSelectModel.FIELD_DFCOPYID,
				CHILD_HDOTHERINCOMECOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBOTHERINCOMETYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_CBOTHERINCOMETYPE,
				doApplicantOtherIncomesSelectModel.FIELD_DFINCOMETYPEID,
				CHILD_CBOTHERINCOMETYPE_RESET_VALUE,
				null);


			child.setLabelForNoneSelected("");
			child.setOptions(cbOtherIncomeTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXOTHERINCOMEDESC))
		{
			TextField child = new TextField(this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_TXOTHERINCOMEDESC,
				doApplicantOtherIncomesSelectModel.FIELD_DFINCOMEDESC,
				CHILD_TXOTHERINCOMEDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBOTHERINCOMEPERIOD))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_CBOTHERINCOMEPERIOD,
				doApplicantOtherIncomesSelectModel.FIELD_DFINCOMEPERIODID,
				CHILD_CBOTHERINCOMEPERIOD_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbOtherIncomePeriodOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXOTHERINCOMEAMOUNT))
		{
			TextField child = new TextField(this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_TXOTHERINCOMEAMOUNT,
				doApplicantOtherIncomesSelectModel.FIELD_DFINCOMEAMT,
				CHILD_TXOTHERINCOMEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBOTHERINCOMEINCLUDEINGDS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_CBOTHERINCOMEINCLUDEINGDS,
				doApplicantOtherIncomesSelectModel.FIELD_DFINCLUDEINGDS,
				CHILD_CBOTHERINCOMEINCLUDEINGDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbOtherIncomeIncludeInGDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXOTHERINCOMEPERCENTINCLUDEINGDS))
		{
			TextField child = new TextField(this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_TXOTHERINCOMEPERCENTINCLUDEINGDS,
				doApplicantOtherIncomesSelectModel.FIELD_DFPERCENTINCLUDEDINGDS,
				CHILD_TXOTHERINCOMEPERCENTINCLUDEINGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantOtherIncomesSelectModel(),
				CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS,
				CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS,
				CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBOTHERINCOMEINCLUDEINTDS))
		{
			ComboBox child = new ComboBox( this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_CBOTHERINCOMEINCLUDEINTDS,
				doApplicantOtherIncomesSelectModel.FIELD_DFINCLUDEINTDS,
				CHILD_CBOTHERINCOMEINCLUDEINTDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbOtherIncomeIncludeInTDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXOTHERINCOMEPERCENTINCLUDEINTDS))
		{
			TextField child = new TextField(this,
				getdoApplicantOtherIncomesSelectModel(),
				CHILD_TXOTHERINCOMEPERCENTINCLUDEINTDS,
				doApplicantOtherIncomesSelectModel.FIELD_DFPERCENTINCLUDEDINTDS,
				CHILD_TXOTHERINCOMEPERCENTINCLUDEINTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS))
		{
			HiddenField child = new HiddenField(this,
				//getDefaultModel(),
        getdoApplicantOtherIncomesSelectModel(),
				CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS,
				CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS,
				CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEOTHERINCOME))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEOTHERINCOME,
				CHILD_BTDELETEOTHERINCOME,
				CHILD_BTDELETEOTHERINCOME_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOtherIncomeId()
	{
		return (HiddenField)getChild(CHILD_HDOTHERINCOMEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOtherIncomeCopyId()
	{
		return (HiddenField)getChild(CHILD_HDOTHERINCOMECOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbOtherIncomeType()
	{
		return (ComboBox)getChild(CHILD_CBOTHERINCOMETYPE);
	}

	/**
	 *
	 *
	 */
	static class CbOtherIncomeTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbOtherIncomeTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
			try {
				clear();
				SelectQueryModel m = null;

				SelectQueryExecutionContext eContext=new SelectQueryExecutionContext(
					(Connection)null,
					DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
					DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

				if(rc == null) {
					m = new doApplicantOtherIncomeTypeModelImpl();
					c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
					eContext.setConnection(c);
				}
				else {
					m = (SelectQueryModel) rc.getModelManager().getModel(doApplicantOtherIncomeTypeModel.class);
				}

				m.retrieve(eContext);
				m.beforeFirst();

				while (m.next()) {
					Object MOS_INCOMETYPE_INCOMETYPEID = m.getValue(doApplicantOtherIncomeTypeModel.FIELD_MOS_INCOMETYPE_INCOMETYPEID);
					Object MOS_INCOMETYPE_ITDESCRIPTION = m.getValue(doApplicantOtherIncomeTypeModel.FIELD_MOS_INCOMETYPE_ITDESCRIPTION);

					String label = (MOS_INCOMETYPE_ITDESCRIPTION == null?"":MOS_INCOMETYPE_ITDESCRIPTION.toString());

					String value = (MOS_INCOMETYPE_INCOMETYPEID == null?"":MOS_INCOMETYPE_INCOMETYPEID.toString());

					add(label, value);
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null)
						c.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}

	}



	/**
	 *
	 *
	 */
	public TextField getTxOtherIncomeDesc()
	{
		return (TextField)getChild(CHILD_TXOTHERINCOMEDESC);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbOtherIncomePeriod()
	{
		return (ComboBox)getChild(CHILD_CBOTHERINCOMEPERIOD);
	}

	/**
	 *
	 *
	 */
	static class CbOtherIncomePeriodOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbOtherIncomePeriodOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 21Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "INCOMEPERIOD", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTxOtherIncomeAmount()
	{
		return (TextField)getChild(CHILD_TXOTHERINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbOtherIncomeIncludeInGDS()
	{
		return (ComboBox)getChild(CHILD_CBOTHERINCOMEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxOtherIncomePercentIncludeInGDS()
	{
		return (TextField)getChild(CHILD_TXOTHERINCOMEPERCENTINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOtherIncomePercentIncludeInGDS()
	{
		return (HiddenField)getChild(CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbOtherIncomeIncludeInTDS()
	{
		return (ComboBox)getChild(CHILD_CBOTHERINCOMEINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxOtherIncomePercentIncludeInTDS()
	{
		return (TextField)getChild(CHILD_TXOTHERINCOMEPERCENTINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdOtherIncomePercentIncludeInTDS()
	{
		return (HiddenField)getChild(CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteOtherIncomeRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btDeleteOtherIncome_onWebEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleDelete(false, 8, handler.getRowNdxFromWebEventMethod(event), "RepeatedOtherIncome/hdOtherIncomeId", "incomeId", "RepeatedOtherIncome/hdOtherIncomeCopyId", "Income");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteOtherIncome()
	{
		return (Button)getChild(CHILD_BTDELETEOTHERINCOME);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteOtherIncomeDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btDeleteOtherIncome_onBeforeHtmlOutputEvent method
		ApplicantEntryHandler handler =(ApplicantEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public doApplicantOtherIncomesSelectModel getdoApplicantOtherIncomesSelectModel()
	{
		if (doApplicantOtherIncomesSelect == null)
			doApplicantOtherIncomesSelect = (doApplicantOtherIncomesSelectModel) getModel(doApplicantOtherIncomesSelectModel.class);
		return doApplicantOtherIncomesSelect;
	}


	/**
	 *
	 *
	 */
	public void setdoApplicantOtherIncomesSelectModel(doApplicantOtherIncomesSelectModel model)
	{
			doApplicantOtherIncomesSelect = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDOTHERINCOMEID="hdOtherIncomeId";
	public static final String CHILD_HDOTHERINCOMEID_RESET_VALUE="";
	public static final String CHILD_HDOTHERINCOMECOPYID="hdOtherIncomeCopyId";
	public static final String CHILD_HDOTHERINCOMECOPYID_RESET_VALUE="";
	public static final String CHILD_CBOTHERINCOMETYPE="cbOtherIncomeType";
	public static final String CHILD_CBOTHERINCOMETYPE_RESET_VALUE="";
  //--Release2.1--//
	//private static CbOtherIncomeTypeOptionList cbOtherIncomeTypeOptions=new CbOtherIncomeTypeOptionList();
	private CbOtherIncomeTypeOptionList cbOtherIncomeTypeOptions=new CbOtherIncomeTypeOptionList();
	public static final String CHILD_TXOTHERINCOMEDESC="txOtherIncomeDesc";
	public static final String CHILD_TXOTHERINCOMEDESC_RESET_VALUE="";
	public static final String CHILD_CBOTHERINCOMEPERIOD="cbOtherIncomePeriod";
	public static final String CHILD_CBOTHERINCOMEPERIOD_RESET_VALUE="";
  //--Release2.1--//
	//private static CbOtherIncomePeriodOptionList cbOtherIncomePeriodOptions=new CbOtherIncomePeriodOptionList();
	private CbOtherIncomePeriodOptionList cbOtherIncomePeriodOptions=new CbOtherIncomePeriodOptionList();
	public static final String CHILD_TXOTHERINCOMEAMOUNT="txOtherIncomeAmount";
	public static final String CHILD_TXOTHERINCOMEAMOUNT_RESET_VALUE="";
	public static final String CHILD_CBOTHERINCOMEINCLUDEINGDS="cbOtherIncomeIncludeInGDS";
	public static final String CHILD_CBOTHERINCOMEINCLUDEINGDS_RESET_VALUE="Y";
  //--Release2.1--//
	//private static OptionList cbOtherIncomeIncludeInGDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList cbOtherIncomeIncludeInGDSOptions=new OptionList();
	public static final String CHILD_TXOTHERINCOMEPERCENTINCLUDEINGDS="txOtherIncomePercentIncludeInGDS";
	public static final String CHILD_TXOTHERINCOMEPERCENTINCLUDEINGDS_RESET_VALUE="";
	public static final String CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS="hdOtherIncomePercentIncludeInGDS";
	public static final String CHILD_HDOTHERINCOMEPERCENTINCLUDEINGDS_RESET_VALUE="";
	public static final String CHILD_CBOTHERINCOMEINCLUDEINTDS="cbOtherIncomeIncludeInTDS";
	public static final String CHILD_CBOTHERINCOMEINCLUDEINTDS_RESET_VALUE="Y";
  //--Release2.1--//
	//private static OptionList cbOtherIncomeIncludeInTDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList cbOtherIncomeIncludeInTDSOptions=new OptionList();
	public static final String CHILD_TXOTHERINCOMEPERCENTINCLUDEINTDS="txOtherIncomePercentIncludeInTDS";
	public static final String CHILD_TXOTHERINCOMEPERCENTINCLUDEINTDS_RESET_VALUE="";
	public static final String CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS="hdOtherIncomePercentIncludeInTDS";
	public static final String CHILD_HDOTHERINCOMEPERCENTINCLUDEINTDS_RESET_VALUE="";
	public static final String CHILD_BTDELETEOTHERINCOME="btDeleteOtherIncome";
	public static final String CHILD_BTDELETEOTHERINCOME_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doApplicantOtherIncomesSelectModel doApplicantOtherIncomesSelect=null;

  private ApplicantEntryHandler handler=new ApplicantEntryHandler();

  public SysLogger logger;

}

