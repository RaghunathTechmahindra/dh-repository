package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doUserAdminGetBranchesModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBranchProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBranchName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBranchName(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfRegionProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfRegionProfileId(java.math.BigDecimal value);
	
    /**
     * 
     * 
     */
    public java.math.BigDecimal getDfInstitutionProfileId();

    
    /**
     * 
     * 
     */
    public void setDfInstitutionProfileId(java.math.BigDecimal value);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBRANCHPROFILEID="dfBranchProfileId";
	public static final String FIELD_DFBRANCHNAME="dfBranchName";
	public static final String FIELD_DFREGIONPROFILEID="dfRegionProfileId";
    public static final String FIELD_DFINSTITUTIONPROFILEID = "dfInstitutionProfileId";

	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

