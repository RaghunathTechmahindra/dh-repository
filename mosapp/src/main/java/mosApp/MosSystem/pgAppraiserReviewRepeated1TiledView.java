package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

import mosApp.MosSystem.models.AVMOrderModel;
import mosApp.MosSystem.models.AVMOrderModelImpl;

/**
 *
 *
 *
 */
public class pgAppraiserReviewRepeated1TiledView
  extends RequestHandlingTiledViewBase
  implements TiledView, RequestHandler
{
  /**
   *
   *
   */
  public pgAppraiserReviewRepeated1TiledView(View parent, String name)
  {
    super(parent, name);
    setMaxDisplayTiles(10);
    setPrimaryModelClass(doAppraisalPropertyModel.class);
    registerChildren();
    initialize();
  }

  /**
   *
   *
   */
  protected void initialize()
  {
  }

  /**
   *
   *
   */
  public void resetChildren()
  {
    getStPrimaryProperty().setValue(CHILD_STPRIMARYPROPERTY_RESET_VALUE);
    getStPropertyAddress().setValue(CHILD_STPROPERTYADDRESS_RESET_VALUE);
    getStPropertyOccurence().setValue(CHILD_STPROPERTYOCCURENCE_RESET_VALUE);
    getStAppraiserName().setValue(CHILD_STAPPRAISERNAME_RESET_VALUE);
    getStAppraiserShortName().setValue(CHILD_STAPPRAISERSHORTNAME_RESET_VALUE);
    getStAppraiserCompanyName().setValue(CHILD_STAPPRAISERCOMPANYNAME_RESET_VALUE);
    getStAppraiserAddress().setValue(CHILD_STAPPRAISERADDRESS_RESET_VALUE);
    getStPhone().setValue(CHILD_STPHONE_RESET_VALUE);
    getStFax().setValue(CHILD_STFAX_RESET_VALUE);
    getStEmail().setValue(CHILD_STEMAIL_RESET_VALUE);
    getStPropertyPurchasePrice().setValue(CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE);
    getTbEstimatedAppraisal().setValue(CHILD_TBESTIMATEDAPPRAISAL_RESET_VALUE);
    getTbActualAppraisal().setValue(CHILD_TBACTUALAPPRAISAL_RESET_VALUE);
    getCbMonths().setValue(CHILD_CBMONTHS_RESET_VALUE);
    getTbDay().setValue(CHILD_TBDAY_RESET_VALUE);
    getTbYear().setValue(CHILD_TBYEAR_RESET_VALUE);
    getCbAppraisalSource().setValue(CHILD_CBAPPRAISALSOURCE_RESET_VALUE);
	getCbAppraisalProvider().setValue(CHILD_CBAPPRAISALPROVIDER_RESET_VALUE);
    getCbAppraisalProduct().setValue(CHILD_CBAPPRAISALPRODUCT_RESET_VALUE);
    getTbAppraiserNotes().setValue(CHILD_TBAPPRAISERNOTES_RESET_VALUE);
    getBtChangeAppraiser().setValue(CHILD_BTCHANGEAPPRAISER_RESET_VALUE);
    getBtAddAppraiser().setValue(CHILD_BTADDAPPRAISER_RESET_VALUE);
    getHbPropertyId().setValue(CHILD_HBPROPERTYID_RESET_VALUE);
    getHdAppraisalDate().setValue(CHILD_HDAPPRAISALDATE_RESET_VALUE);
    getStAppraiserExtension().setValue(CHILD_STAPPRAISEREXTENSION_RESET_VALUE);
    //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    //--> By Billy 16Dec2003
    //New display fields for AppraisalOrder
    getStAppraisalStatus().setValue(CHILD_STAPPRAISALSTATUS_RESET_VALUE);
    getChUseBorrowerInfo().setValue(CHILD_CHUSEBORROWERINFO_RESET_VALUE);
    getTbPropertyOwnerName().setValue(CHILD_TBPROPERTYOWNERNAME_RESET_VALUE);
    getTbAOContactName().setValue(CHILD_TBAOCONTACTNAME_RESET_VALUE);
    getTbAOContactWorkPhoneNum1().setValue(CHILD_TBAOCONTACTWORKPHONENUM1_RESET_VALUE);
    getTbAOContactWorkPhoneNum2().setValue(CHILD_TBAOCONTACTWORKPHONENUM2_RESET_VALUE);
    getTbAOContactWorkPhoneNum3().setValue(CHILD_TBAOCONTACTWORKPHONENUM3_RESET_VALUE);
    getTbAOContactWorkPhoneNumExt().setValue(CHILD_TBAOCONTACTWORKPHONENUMEXT_RESET_VALUE);
    getTbAOContactCellPhoneNum1().setValue(CHILD_TBAOCONTACTCELLPHONENUM1_RESET_VALUE);
    getTbAOContactCellPhoneNum2().setValue(CHILD_TBAOCONTACTCELLPHONENUM2_RESET_VALUE);
    getTbAOContactCellPhoneNum3().setValue(CHILD_TBAOCONTACTCELLPHONENUM3_RESET_VALUE);
    getTbAOContactHomePhoneNum1().setValue(CHILD_TBAOCONTACTHOMEPHONENUM1_RESET_VALUE);
    getTbAOContactHomePhoneNum2().setValue(CHILD_TBAOCONTACTHOMEPHONENUM2_RESET_VALUE);
    getTbAOContactHomePhoneNum3().setValue(CHILD_TBAOCONTACTHOMEPHONENUM3_RESET_VALUE);
    getTbCommentsForAppraiser().setValue(CHILD_TBCOMMENTSFORAPPRAISER_RESET_VALUE);
    getStPropertyId().setValue(CHILD_STPROPERTYID_RESET_VALUE);
    //=================================================================

    // SEAN AVM Service Feb 13, 2006: reset the default value.
    getCbAVMProvider().setValue(CHILD_CBAVMPROVIDER_RESET_VALUE);
    getCbAVMProduct().setValue(CHILD_CBAVMPRODUCT_RESET_VALUE);
    getStAVMRequestStatus().setValue(CHILD_STAVMREQUESTSTATUS_RESET_VALUE);
    getStAVMRequestStatusMsg().
        setValue(CHILD_STAVMREQUESTSTATUSMSG_RESET_VALUE);
    getStAVMRequestDate().setValue(CHILD_STAVMREQUESTDATE_RESET_VALUE);
    getStTileIndex().setValue(CHILD_STTILEINDEX_RESET_VALUE);
    getStAVMReports().setValue(CHILD_STAVMREPORTS_RESET_VALUE);
	getStAppraisalHiddenStart().setValue(CHILD_STAPPRAISALHIDDENSTART_RESET_VALUE);
	getStAppraisalHiddenEnd().setValue(CHILD_STAPPRAISALHIDDENEND_RESET_VALUE);
	
	//START: hidden controls for enabling/disabling Appraisal Contact Information section
	getStAppraisalContactInfoHiddenStart().setValue(CHILD_STAPPRAISALCONTINFOHIDDENSTART_RESET_VALUE);
	getStAppraisalContactInfoHiddenEnd().setValue(CHILD_STAPPRAISALCONTINFOHIDDENEND_RESET_VALUE);
	//END: hidden controls for enabling/disabling Appraisal Contact Information section
	getStAppraisalTypeHidden().setValue(CHILD_STAPPRAISALTYPEHIDDEN_RESET_VALUE);
	//FIX
	getStAppraisalSourceStart().setValue(CHILD_STAPPRAISALSOURCESTART_RESET_VALUE);
	getStAppraisalSourceEnd().setValue(CHILD_STAPPRAISALSOURCEEND_RESET_VALUE);
	//FIX
	getStAVMOrderHidden().setValue(CHILD_STAVMORDERHIDDEN_RESET_VALUE);
	getStAVMOrderHiddenEnd().setValue(CHILD_STAVMORDERHIDDENEND_RESET_VALUE);

    getStAppraisalSummary().setValue(CHILD_STAPPRAISALSUMMARY_RESET_VALUE);
    getStAOContactFirstName().setValue(CHILD_STAOCONTACTFIRSTNAME_RESET_VALUE);
    getStAOContactLastName().setValue(CHILD_STAOCONTACTLASTNAME_RESET_VALUE);
	getTbAOEmail().setValue(CHILD_TBAOEMAIL_RESET_VALUE);
    getTbAOWorkPhoneAreaCode().setValue(CHILD_TBAOWORKPHONEAREACODE_RESET_VALUE);
    getTbAOWorkPhoneFirstThreeDigits().setValue(CHILD_TBAOWORKPHONEFIRSTTHREEDIGITS_RESET_VALUE);
    getTbAOWorkPhoneLastFourDigits().setValue(CHILD_TBAOWORKPHONELASTFOURDIGITS_RESET_VALUE);
    getTbAOWorkPhoneNumExtension().setValue(CHILD_TBAOWORKPHONENUMEXTENSION_RESET_VALUE);
    getTbAOCellPhoneAreaCode().setValue(CHILD_TBAOCELLPHONEAREACODE_RESET_VALUE);
    getTbAOCellPhoneFirstThreeDigits().setValue(CHILD_TBAOCELLPHONEFIRSTTHREEDIGITS_RESET_VALUE);
    getTbAOCellPhoneLastFourDigits().setValue(CHILD_TBAOCELLPHONELASTFOURDIGITS_RESET_VALUE);
    getTbAOHomePhoneAreaCode().setValue(CHILD_TBAOHOMEPHONEAREACODE_RESET_VALUE);
    getTbAOHomePhoneFirstThreeDigits().setValue(CHILD_TBAOHOMEPHONEFIRSTTHREEDIGITS_RESET_VALUE);
    getTbAOHomePhoneLastFourDigits().setValue(CHILD_TBAOHOMEPHONELASTFOURDIGITS_RESET_VALUE);
    getChAOPrimaryBorrowerInfo().setValue(CHILD_CHAOPRIMARYBORROWERINFO_RESET_VALUE);
    getStAOPropertyOwnerName().setValue(CHILD_STAOPROPERTYOWNERNAME_RESET_VALUE);
    getStAppraisalBorrowerId().setValue(CHILD_STAPPRAISALBORROWERID_RESET_VALUE);
    getStAppraisalRequestId().setValue(CHILD_STAPPRAISALREQUESTID_RESET_VALUE);
    getStAppraisalStatusId().setValue(CHILD_STAPPRAISALSTATUSID_RESET_VALUE);
    //chPrimaryBorrowerInfo
    // SEAN AVM Service END.
    // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
    getStRead().setValue(CHILD_STREAD_RESET_VALUE);
    //  ***** Change by NBC/PP Implementation Team, Defect #1419 - END *****//
  }

  /**
   *
   *
   */
  protected void registerChildren()
  {
    registerChild(CHILD_STPRIMARYPROPERTY, StaticTextField.class);
    registerChild(CHILD_STPROPERTYADDRESS, StaticTextField.class);
    registerChild(CHILD_STPROPERTYOCCURENCE, StaticTextField.class);
    registerChild(CHILD_STAPPRAISERNAME, StaticTextField.class);
    registerChild(CHILD_STAPPRAISERSHORTNAME, StaticTextField.class);
    registerChild(CHILD_STAPPRAISERCOMPANYNAME, StaticTextField.class);
    registerChild(CHILD_STAPPRAISERADDRESS, StaticTextField.class);
    registerChild(CHILD_STPHONE, StaticTextField.class);
    registerChild(CHILD_STFAX, StaticTextField.class);
    registerChild(CHILD_STEMAIL, StaticTextField.class);
    registerChild(CHILD_STPROPERTYPURCHASEPRICE, StaticTextField.class);
    registerChild(CHILD_TBESTIMATEDAPPRAISAL, TextField.class);
    registerChild(CHILD_TBACTUALAPPRAISAL, TextField.class);
    registerChild(CHILD_CBMONTHS, ComboBox.class);
    registerChild(CHILD_TBDAY, TextField.class);
    registerChild(CHILD_TBYEAR, TextField.class);
    registerChild(CHILD_CBAPPRAISALSOURCE, ComboBox.class);
	registerChild(CHILD_CBAPPRAISALPROVIDER, ComboBox.class); //Appraisal
    registerChild(CHILD_CBAPPRAISALPRODUCT, ComboBox.class);
    registerChild(CHILD_TBAPPRAISERNOTES, TextField.class);
    registerChild(CHILD_BTCHANGEAPPRAISER, Button.class);
    registerChild(CHILD_BTADDAPPRAISER, Button.class);
    registerChild(CHILD_HBPROPERTYID, HiddenField.class);
    registerChild(CHILD_HDAPPRAISALDATE, HiddenField.class);
    registerChild(CHILD_STAPPRAISEREXTENSION, StaticTextField.class);
    //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    //--> By Billy 16Dec2003
    //New display fields for AppraisalOrder
    registerChild(CHILD_STAPPRAISALSTATUS, StaticTextField.class);
    registerChild(CHILD_CHUSEBORROWERINFO, CheckBox.class);
    registerChild(CHILD_TBPROPERTYOWNERNAME, TextField.class);
    registerChild(CHILD_TBAOCONTACTNAME, TextField.class);
    registerChild(CHILD_TBAOCONTACTWORKPHONENUM1, TextField.class);
    registerChild(CHILD_TBAOCONTACTWORKPHONENUM2, TextField.class);
    registerChild(CHILD_TBAOCONTACTWORKPHONENUM3, TextField.class);
    registerChild(CHILD_TBAOCONTACTWORKPHONENUMEXT, TextField.class);
    registerChild(CHILD_TBAOCONTACTCELLPHONENUM1, TextField.class);
    registerChild(CHILD_TBAOCONTACTCELLPHONENUM2, TextField.class);
    registerChild(CHILD_TBAOCONTACTCELLPHONENUM3, TextField.class);
    registerChild(CHILD_TBAOCONTACTHOMEPHONENUM1, TextField.class);
    registerChild(CHILD_TBAOCONTACTHOMEPHONENUM2, TextField.class);
    registerChild(CHILD_TBAOCONTACTHOMEPHONENUM3, TextField.class);
    registerChild(CHILD_TBCOMMENTSFORAPPRAISER, TextField.class);
    registerChild(CHILD_STPROPERTYID, StaticTextField.class);
    //================================================================

    // SEAN AVM Service Feb 13, 2006: register the children.
    registerChild(CHILD_CBAVMPROVIDER, ComboBox.class);
    registerChild(CHILD_CBAVMPRODUCT, ComboBox.class);
    registerChild(CHILD_STAVMREQUESTSTATUS, StaticTextField.class);
    registerChild(CHILD_STAVMREQUESTSTATUSMSG, StaticTextField.class);
    registerChild(CHILD_STAVMREQUESTDATE, StaticTextField.class);
    registerChild(CHILD_STTILEINDEX, StaticTextField.class);
    registerChild(CHILD_STAVMREPORTS, StaticTextField.class);
	registerChild(CHILD_STAPPRAISALHIDDENSTART, StaticTextField.class);
	registerChild(CHILD_STAPPRAISALHIDDENEND, StaticTextField.class);
	//START: hidden controls for enabling/disabling Appraisal Contact Information section
	registerChild(CHILD_STAPPRAISALCONTINFOHIDDENSTART, StaticTextField.class);
	registerChild(CHILD_STAPPRAISALCONTINFOHIDDENEND, StaticTextField.class);
	//END: hidden controls for enabling/disabling Appraisal Contact Information section
	registerChild(CHILD_STAPPRAISALTYPEHIDDEN, StaticTextField.class);
	//FIX
	registerChild(CHILD_STAPPRAISALSOURCESTART, StaticTextField.class);
	registerChild(CHILD_STAPPRAISALSOURCEEND, StaticTextField.class);
	//FIX
	registerChild(CHILD_STAVMORDERHIDDEN, StaticTextField.class);
	registerChild(CHILD_STAVMORDERHIDDENEND, StaticTextField.class);

    registerChild(CHILD_STAPPRAISALSUMMARY, StaticTextField.class);
    registerChild(CHILD_STAOCONTACTFIRSTNAME, TextField.class);
    registerChild(CHILD_STAOCONTACTLASTNAME, TextField.class);
	registerChild(CHILD_TBAOEMAIL, TextField.class);
    registerChild(CHILD_TBAOWORKPHONEAREACODE, TextField.class);
    registerChild(CHILD_TBAOWORKPHONEFIRSTTHREEDIGITS, TextField.class);
    registerChild(CHILD_TBAOWORKPHONELASTFOURDIGITS, TextField.class);
    registerChild(CHILD_TBAOWORKPHONENUMEXTENSION, TextField.class);
    registerChild(CHILD_TBAOCELLPHONEAREACODE, TextField.class);
    registerChild(CHILD_TBAOCELLPHONEFIRSTTHREEDIGITS, TextField.class);
    registerChild(CHILD_TBAOCELLPHONELASTFOURDIGITS, TextField.class);
    registerChild(CHILD_TBAOHOMEPHONEAREACODE, TextField.class);
    registerChild(CHILD_TBAOHOMEPHONEFIRSTTHREEDIGITS, TextField.class);
	registerChild(CHILD_TBAOHOMEPHONELASTFOURDIGITS, TextField.class);
    registerChild(CHILD_CHAOPRIMARYBORROWERINFO, CheckBox.class);
    registerChild(CHILD_STAOPROPERTYOWNERNAME, TextField.class);
    registerChild(CHILD_STAPPRAISALBORROWERID, HiddenField.class);
    registerChild(CHILD_STAPPRAISALREQUESTID, HiddenField.class);
    registerChild(CHILD_STAPPRAISALCONTACTID, HiddenField.class);
    registerChild(CHILD_STAPPRAISALSTATUSID, HiddenField.class);
    // SEAN AVM Service END.
    // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
    registerChild(CHILD_STREAD, HiddenField.class);
    // ***** Change by NBC/PP Implementation Team, Defect #1419 - END *****//

  }

  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList = new ArrayList();
    switch(executionType)
    {
      case MODEL_TYPE_RETRIEVE:
        modelList.add(getdoAppraisalPropertyModel());
        modelList.add(getdoAppraisalProviderModel());
        // SEAN AVM SERVICE Feb 13, 2006: add the model to the model list.
        //modelList.add(getAVMOrderModel());
        // SEAN AVM SERVICE end.
        break;

      case MODEL_TYPE_UPDATE:
        ;
        break;

      case MODEL_TYPE_DELETE:
        ;
        break;

      case MODEL_TYPE_INSERT:
        ;
        break;

      case MODEL_TYPE_EXECUTE:
        ;
        break;
    }
    return(Model[])modelList.toArray(new Model[0]);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
    // This is the analog of NetDynamics repeated_onBeforeDisplayEvent
    // Ensure the primary model is non-null; if null, it would cause havoc
    // with the various methods dependent on the primary model
    if(getPrimaryModel() == null)
    {
      throw new ModelControlException("Primary model is null");
    }

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    AppraisalHandler handler = (AppraisalHandler)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());

    //Set up all ComboBox options
    cbAppraisalSourceOptions.populate(getRequestContext());

    //cbAppraisalProviderOptions.populate(getRequestContext());
	
    //=======================================================

    // SEAN AVM Service Feb 13, 2006: populate the options for combobox.
    _cbAVMProviderOptions.populate(getRequestContext());

    //_cbAVMProductOptions.populate(getRequestContext());
    // SEAN AVM Service END.

    super.beginDisplay(event);
    resetTileIndex();
  }

  /**
   *
   *
   */
  public boolean nextTile() throws ModelControlException
  {
    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
    boolean movedToRow = super.nextTile();

    if(movedToRow)
    {
      AppraisalHandler handler = (AppraisalHandler)this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      handler.displayAppraiser(getTileIndex());
      handler.pageSaveState();
    }

    return movedToRow;

  }

  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
    // This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
    // This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {
    // This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);
  }

  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception) throws ModelControlException
  {

    // This is the analog of NetDynamics repeated_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);

  }

  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected View createChild(String name)
  {
    if(name.equals(CHILD_STPRIMARYPROPERTY))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPRIMARYPROPERTY,
        doAppraisalPropertyModel.FIELD_DFPRIMARYPROPERTY,
        CHILD_STPRIMARYPROPERTY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROPERTYADDRESS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPROPERTYADDRESS,
        doAppraisalPropertyModel.FIELD_DFPROPERTYADDRESS,
        CHILD_STPROPERTYADDRESS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROPERTYOCCURENCE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPROPERTYOCCURENCE,
        doAppraisalPropertyModel.FIELD_DFPROPERTYOCCURENCE,
        CHILD_STPROPERTYOCCURENCE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISERNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISERNAME,
        CHILD_STAPPRAISERNAME,
        CHILD_STAPPRAISERNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISERSHORTNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISERSHORTNAME,
        CHILD_STAPPRAISERSHORTNAME,
        CHILD_STAPPRAISERSHORTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISERCOMPANYNAME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISERCOMPANYNAME,
        CHILD_STAPPRAISERCOMPANYNAME,
        CHILD_STAPPRAISERCOMPANYNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISERADDRESS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISERADDRESS,
        CHILD_STAPPRAISERADDRESS,
        CHILD_STAPPRAISERADDRESS_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPHONE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPHONE,
        CHILD_STPHONE,
        CHILD_STPHONE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STFAX))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STFAX,
        CHILD_STFAX,
        CHILD_STFAX_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STEMAIL))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STEMAIL,
        CHILD_STEMAIL,
        CHILD_STEMAIL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROPERTYPURCHASEPRICE))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPROPERTYPURCHASEPRICE,
        doAppraisalPropertyModel.FIELD_DFPURCHASEPRICE,
        CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBESTIMATEDAPPRAISAL))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBESTIMATEDAPPRAISAL,
        doAppraisalPropertyModel.FIELD_DFESTIMATEDAPPRAISAL,
        CHILD_TBESTIMATEDAPPRAISAL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBACTUALAPPRAISAL))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBACTUALAPPRAISAL,
        doAppraisalPropertyModel.FIELD_DFACTUALAPPRAISAL,
        CHILD_TBACTUALAPPRAISAL_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBMONTHS))
    {
      ComboBox child = new ComboBox(this,
        //getDefaultModel(),
        getdoAppraisalPropertyModel(),
        CHILD_CBMONTHS,
        CHILD_CBMONTHS,
        CHILD_CBMONTHS_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbMonthsOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TBDAY))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoAppraisalPropertyModel(),
        CHILD_TBDAY,
        CHILD_TBDAY,
        CHILD_TBDAY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBYEAR))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoAppraisalPropertyModel(),
        CHILD_TBYEAR,
        CHILD_TBYEAR,
        CHILD_TBYEAR_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_CBAPPRAISALSOURCE))
    {
      ComboBox child = new ComboBox(this,
        getdoAppraisalPropertyModel(),
        CHILD_CBAPPRAISALSOURCE,
        doAppraisalPropertyModel.FIELD_DFAPPRAISALSOURCEID,
        CHILD_CBAPPRAISALSOURCE_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbAppraisalSourceOptions);
      return child;
    }
	else
    if(name.equals(CHILD_CBAPPRAISALPROVIDER))
    {
      ComboBox child = new ComboBox(this,
        getdoAppraisalProviderModel(),
        CHILD_CBAPPRAISALPROVIDER,
        doAppraisalProviderModel.FIELD_DFAPPRAISALPROVIDERID,
        CHILD_CBAPPRAISALPROVIDER_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbAppraisalProviderOptions);
      return child;
    }
    else
    if(name.equals(CHILD_CBAPPRAISALPRODUCT))
    {
      ComboBox child = new ComboBox(this,
        getdoAppraisalProviderModel(),
        CHILD_CBAPPRAISALPRODUCT,
        doAppraisalProviderModel.FIELD_DFAPPRAISALPRODUCTID,
        CHILD_CBAPPRAISALPRODUCT_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      return child;
    }
    else
    if(name.equals(CHILD_TBAPPRAISERNOTES))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAPPRAISERNOTES,
        CHILD_TBAPPRAISERNOTES,
        CHILD_TBAPPRAISERNOTES_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_BTCHANGEAPPRAISER))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTCHANGEAPPRAISER,
        CHILD_BTCHANGEAPPRAISER,
        CHILD_BTCHANGEAPPRAISER_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_BTADDAPPRAISER))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTADDAPPRAISER,
        CHILD_BTADDAPPRAISER,
        CHILD_BTADDAPPRAISER_RESET_VALUE,
        null);
      return child;

    }
    else
    if(name.equals(CHILD_HBPROPERTYID))
    {
      HiddenField child = new HiddenField(this,
        getdoAppraisalPropertyModel(),
        CHILD_HBPROPERTYID,
        doAppraisalPropertyModel.FIELD_DFPROPERTYID,
        CHILD_HBPROPERTYID_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_HDAPPRAISALDATE))
    {
      HiddenField child = new HiddenField(this,
        getdoAppraisalPropertyModel(),
        CHILD_HDAPPRAISALDATE,
        doAppraisalPropertyModel.FIELD_DFAPPRAISALDATE,
        CHILD_HDAPPRAISALDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STAPPRAISEREXTENSION))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STAPPRAISEREXTENSION,
        CHILD_STAPPRAISEREXTENSION,
        CHILD_STAPPRAISEREXTENSION_RESET_VALUE,
        null);
      return child;
    }
    //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    //--> By Billy 16Dec2003
    //New display fields for AppraisalOrder
    else
    if(name.equals(CHILD_STAPPRAISALSTATUS))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STAPPRAISALSTATUS,
        doAppraisalPropertyModel.FIELD_DFAPPRAISALSTATUSDESC,
        CHILD_STAPPRAISALSTATUS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_CHUSEBORROWERINFO))
    {
      CheckBox child = new CheckBox(this,
        getdoAppraisalPropertyModel(),
        CHILD_CHUSEBORROWERINFO,
        doAppraisalPropertyModel.FIELD_DFUSEBORROWERINFO,
        "Y","N",
        false,
        null);

      return child;
    }
    else
    if(name.equals(CHILD_TBPROPERTYOWNERNAME))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBPROPERTYOWNERNAME,
        doAppraisalProviderModel.FIELD_DFAOPROPERTYOWNERNAME,
        CHILD_TBPROPERTYOWNERNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTNAME))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBAOCONTACTNAME,
        doAppraisalPropertyModel.FIELD_DFAOCONTACTNAME,
        CHILD_TBAOCONTACTNAME_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTWORKPHONENUM1))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTWORKPHONENUM1,
        CHILD_TBAOCONTACTWORKPHONENUM1,
        CHILD_TBAOCONTACTWORKPHONENUM1_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTWORKPHONENUM2))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTWORKPHONENUM2,
        CHILD_TBAOCONTACTWORKPHONENUM2,
        CHILD_TBAOCONTACTWORKPHONENUM2_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTWORKPHONENUM3))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTWORKPHONENUM3,
        CHILD_TBAOCONTACTWORKPHONENUM3,
        CHILD_TBAOCONTACTWORKPHONENUM3_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTWORKPHONENUMEXT))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBAOCONTACTWORKPHONENUMEXT,
        doAppraisalPropertyModel.FIELD_DFAOCONTACTWORKPHONENUMEXT,
        CHILD_TBAOCONTACTWORKPHONENUMEXT_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTCELLPHONENUM1))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTCELLPHONENUM1,
        CHILD_TBAOCONTACTCELLPHONENUM1,
        CHILD_TBAOCONTACTCELLPHONENUM1_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTCELLPHONENUM2))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTCELLPHONENUM2,
        CHILD_TBAOCONTACTCELLPHONENUM2,
        CHILD_TBAOCONTACTCELLPHONENUM2_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTCELLPHONENUM3))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTCELLPHONENUM3,
        CHILD_TBAOCONTACTCELLPHONENUM3,
        CHILD_TBAOCONTACTCELLPHONENUM3_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTHOMEPHONENUM1))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTHOMEPHONENUM1,
        CHILD_TBAOCONTACTHOMEPHONENUM1,
        CHILD_TBAOCONTACTHOMEPHONENUM1_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTHOMEPHONENUM2))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTHOMEPHONENUM2,
        CHILD_TBAOCONTACTHOMEPHONENUM2,
        CHILD_TBAOCONTACTHOMEPHONENUM2_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBAOCONTACTHOMEPHONENUM3))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TBAOCONTACTHOMEPHONENUM3,
        CHILD_TBAOCONTACTHOMEPHONENUM3,
        CHILD_TBAOCONTACTHOMEPHONENUM3_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TBCOMMENTSFORAPPRAISER))
    {
      TextField child = new TextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_TBCOMMENTSFORAPPRAISER,
        doAppraisalPropertyModel.FIELD_DFCOMMENTSFORAPPRAISER,
        CHILD_TBCOMMENTSFORAPPRAISER_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPROPERTYID))
    {
      StaticTextField child = new StaticTextField(this,
        getdoAppraisalPropertyModel(),
        CHILD_STPROPERTYID,
        doAppraisalPropertyModel.FIELD_DFPROPERTYID,
        CHILD_STPROPERTYID_RESET_VALUE,
        null);
      return child;
    }
    // SEAN AVM Service Feb 13, 2006: Creating the children.
    else if (name.equals(CHILD_CBAVMPROVIDER)) {
        ComboBox child = new ComboBox(this,
                                      getAVMOrderModel(),
                                      CHILD_CBAVMPROVIDER,
                                      AVMOrderModel.FIELD_DFAVMPROVIDERID,
                                      CHILD_CBAVMPROVIDER_RESET_VALUE,
                                      null);
        child.setLabelForNoneSelected("");
        child.setOptions(_cbAVMProviderOptions);

        return child;
    }
    else if (name.equals(CHILD_CBAVMPRODUCT)) {
        ComboBox child = new ComboBox(this,
                                      getAVMOrderModel(),
                                      CHILD_CBAVMPRODUCT,
                                      AVMOrderModel.FIELD_DFAVMPRODUCTID,
                                      CHILD_CBAVMPRODUCT_RESET_VALUE,
                                      null);
        child.setLabelForNoneSelected("");
        //child.setOptions(_cbAVMProductOptions);
        return child;
    }
    else if (name.equals(CHILD_STAVMREQUESTSTATUS)) {
        StaticTextField child =
            new StaticTextField(this,
                                getAVMOrderModel(),
                                CHILD_STAVMREQUESTSTATUS,
                                AVMOrderModel.FIELD_DFAVMREQUESTSTATUS,
                                CHILD_STAVMREQUESTSTATUS_RESET_VALUE,
                                null);
        return child;
    }
    else if (name.equals(CHILD_STAVMREQUESTSTATUSMSG)) {
        StaticTextField child =
            new StaticTextField(this,
                                getAVMOrderModel(),
                                CHILD_STAVMREQUESTSTATUSMSG,
                                AVMOrderModel.FIELD_DFAVMREQUESTSTATUSMSG,
                                CHILD_STAVMREQUESTSTATUSMSG_RESET_VALUE,
                                null);
        return child;
    } else if (name.equals(CHILD_STAVMREQUESTDATE)) {
        StaticTextField child =
            new StaticTextField(this,
                                getAVMOrderModel(),
                                CHILD_STAVMREQUESTDATE,
                                AVMOrderModel.FIELD_DFAVMREQUESTDATE,
                                CHILD_STAVMREQUESTDATE_RESET_VALUE,
                                null);
        return child;
    } else if (name.equals(CHILD_STTILEINDEX)) {
        StaticTextField child =
            new StaticTextField(this,
                                getDefaultModel(),
                                CHILD_STTILEINDEX,
                                CHILD_STTILEINDEX,
                                CHILD_STTILEINDEX_RESET_VALUE,
                                null);
        return child;
    } else if (name.equals(CHILD_STAVMREPORTS)) {
        StaticTextField child =
            new StaticTextField(this,
                                getDefaultModel(),
                                CHILD_STAVMREPORTS,
                                CHILD_STAVMREPORTS,
                                CHILD_STAVMREPORTS_RESET_VALUE,
                                null);
        return child;
    } else if (name.equals(CHILD_STAPPRAISALSUMMARY)) {
        StaticTextField child =
            new StaticTextField(this,
                                getDefaultModel(),
                                CHILD_STAPPRAISALSUMMARY,
                                CHILD_STAPPRAISALSUMMARY,
                                CHILD_STAPPRAISALSUMMARY_RESET_VALUE,
                                null);
        return child;
    } else if (name.equals(CHILD_STAPPRAISALHIDDENSTART)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAPPRAISALHIDDENSTART,
								CHILD_STAPPRAISALHIDDENSTART,
								CHILD_STAPPRAISALHIDDENSTART_RESET_VALUE,
								null);
		return child;
	} else if (name.equals(CHILD_STAPPRAISALHIDDENEND)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAPPRAISALHIDDENEND,
								CHILD_STAPPRAISALHIDDENEND,
								CHILD_STAPPRAISALHIDDENEND_RESET_VALUE,
								null);
		return child;
	}
    //START: hidden controls for enabling/disabling Appraisal Contact Information section
	else if (name.equals(CHILD_STAPPRAISALCONTINFOHIDDENSTART)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAPPRAISALCONTINFOHIDDENSTART,
								CHILD_STAPPRAISALCONTINFOHIDDENSTART,
								CHILD_STAPPRAISALCONTINFOHIDDENSTART_RESET_VALUE,
								null);
		return child;
	} else if (name.equals(CHILD_STAPPRAISALCONTINFOHIDDENEND)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAPPRAISALCONTINFOHIDDENEND,
								CHILD_STAPPRAISALCONTINFOHIDDENEND,
								CHILD_STAPPRAISALCONTINFOHIDDENEND_RESET_VALUE,
								null);
		return child;
	}  
    //END: hidden controls for enabling/disabling Appraisal Contact Information section
	else if (name.equals(CHILD_STAPPRAISALTYPEHIDDEN)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAPPRAISALTYPEHIDDEN,
								CHILD_STAPPRAISALTYPEHIDDEN,
								CHILD_STAPPRAISALTYPEHIDDEN_RESET_VALUE,
								null);
		return child;
	} 
    //FIX
	else if (name.equals(CHILD_STAPPRAISALSOURCESTART)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAPPRAISALSOURCESTART,
								CHILD_STAPPRAISALSOURCESTART,
								CHILD_STAPPRAISALSOURCESTART_RESET_VALUE,
								null);
		return child;
	} 
	else if (name.equals(CHILD_STAPPRAISALSOURCEEND)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAPPRAISALSOURCEEND,
								CHILD_STAPPRAISALSOURCEEND,
								CHILD_STAPPRAISALSOURCEEND_RESET_VALUE,
								null);
		return child;
	} 
	//FIX
	else if (name.equals(CHILD_STAVMORDERHIDDEN)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAVMORDERHIDDEN,
								CHILD_STAVMORDERHIDDEN,
								CHILD_STAVMORDERHIDDEN_RESET_VALUE,
								null);
		return child;
	} else if (name.equals(CHILD_STAVMORDERHIDDENEND)) {
		StaticTextField child =
			new StaticTextField(this,
								getDefaultModel(),
								CHILD_STAVMORDERHIDDENEND,
								CHILD_STAVMORDERHIDDENEND,
								CHILD_STAVMORDERHIDDENEND_RESET_VALUE,
								null);
		return child;
	}
    // SEAN AVM Service END.

    else if (name.equals(CHILD_CBAPPRAISALPROVIDER)) {
        ComboBox child = new ComboBox(this,
                                      getdoAppraisalProviderModel(),
                                      CHILD_CBAPPRAISALPROVIDER,
                                      doAppraisalProviderModel.FIELD_DFAPPRAISALPROVIDERID,
                                      CHILD_CBAPPRAISALPROVIDER_RESET_VALUE,
                                      null);
        child.setLabelForNoneSelected("");
        return child;
    }
    else if (name.equals(CHILD_TBAPPRAISALSPECIALINST)) {
        StaticTextField child =
            new StaticTextField(this,
            					getdoAppraisalProviderModel(),
                                CHILD_TBAPPRAISALSPECIALINST,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALSPECIALINST,
                                CHILD_TBAPPRAISALSPECIALINST_RESET_VALUE,
                                null);
        return child;
    }
    else if (name.equals(CHILD_STAPPRAISALBORROWERID)) {
        HiddenField child =
            new HiddenField(this,
                                getdoAppraisalProviderModel(),
                                CHILD_STAPPRAISALBORROWERID,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALBORROWERID,
                                CHILD_STAPPRAISALBORROWERID_RESET_VALUE,
                                null);
        return child;
    }

    else if (name.equals(CHILD_STAPPRAISALREQUESTID)) {
        HiddenField child =
            new HiddenField(this,
                                getdoAppraisalProviderModel(),
                                CHILD_STAPPRAISALREQUESTID,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALREQUESTID,
                                CHILD_STAPPRAISALREQUESTID_RESET_VALUE,
                                null);
        return child;
    }    
    
    else if (name.equals(CHILD_STAPPRAISALCONTACTID)) {
        HiddenField child =
            new HiddenField(this,
                                getdoAppraisalProviderModel(),
                                CHILD_STAPPRAISALCONTACTID,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALCONTACTID,
                                CHILD_STAPPRAISALCONTACTID_RESET_VALUE,
                                null);
        return child;
    }    
    
    else if (name.equals(CHILD_STAPPRAISALSTATUSID)) {
        HiddenField child =
            new HiddenField(this,
                                getdoAppraisalProviderModel(),
                                CHILD_STAPPRAISALSTATUSID,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALSTATUSID,
                                CHILD_STAPPRAISALSTATUSID_RESET_VALUE,
                                null);
        return child;
    }    
    
    else if (name.equals(CHILD_STAPPRAISALREQUESTSTATUS)) {
        StaticTextField child =
            new StaticTextField(this,
            					getdoAppraisalProviderModel(),
                                CHILD_STAPPRAISALREQUESTSTATUS,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALREQUESTSTATUS,
                                CHILD_STAPPRAISALREQUESTSTATUS_RESET_VALUE,
                                null);
        return child;
    }
    else if (name.equals(CHILD_STAPPRAISALREQUESTSTATUSDATE)) {
        StaticTextField child =
            new StaticTextField(this,
            					getdoAppraisalProviderModel(),
                                CHILD_STAPPRAISALREQUESTSTATUSDATE,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALREQUESTSTATUSDATE,
                                CHILD_STAPPRAISALREQUESTSTATUSDATE_RESET_VALUE,
                                null);
        return child;
    }
    else if (name.equals(CHILD_STAPPRAISALREQUESTREFNUM)) {
        StaticTextField child =
            new StaticTextField(this,
            					getdoAppraisalProviderModel(),
                                CHILD_STAPPRAISALREQUESTREFNUM,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALREQUESTREFNUM,
                                CHILD_STAPPRAISALREQUESTREFNUM_RESET_VALUE,
                                null);
        return child;
    }
    if(name.equals(CHILD_BTAPPRAISALCANCEL))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTAPPRAISALCANCEL,
        CHILD_BTAPPRAISALCANCEL,
        CHILD_BTAPPRAISALCANCEL_RESET_VALUE,
        null);
      return child;

    }
   else if (name.equals(CHILD_TBAPPRAISALSTATUSMESSAGES)) {
        StaticTextField child =
            new StaticTextField(this,
            					getdoAppraisalProviderModel(),
                                CHILD_TBAPPRAISALSTATUSMESSAGES,
                                doAppraisalProviderModel.FIELD_DFAPPRAISALREQUESTSTATUSMSG,
                                CHILD_TBAPPRAISALSTATUSMESSAGES_RESET_VALUE,
                                null);
        return child;
    }
    
   else if (name.equals(CHILD_STAOCONTACTFIRSTNAME)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_STAOCONTACTFIRSTNAME,
                               doAppraisalProviderModel.FIELD_DFAOCONTACTFIRSTNAME,
                               CHILD_STAOCONTACTFIRSTNAME_RESET_VALUE,
                               null);
           return child;
   }
   else if (name.equals(CHILD_STAOCONTACTLASTNAME)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_STAOCONTACTLASTNAME,
                               doAppraisalProviderModel.FIELD_DFAOCONTACTLASTNAME,
                               CHILD_STAOCONTACTLASTNAME_RESET_VALUE,
                               null);
           return child;
   }
	else if (name.equals(CHILD_TBAOEMAIL)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOEMAIL,
                               doAppraisalProviderModel.FIELD_DFAOEMAIL,
                               CHILD_TBAOEMAIL_RESET_VALUE,
                               null);
           return child;
   }   
	else if (name.equals(CHILD_TBAOWORKPHONEAREACODE)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOWORKPHONEAREACODE,
                               doAppraisalProviderModel.FIELD_DFAOWORKPHONEAREACODE,
                               CHILD_TBAOWORKPHONEAREACODE_RESET_VALUE,
                               null);
           return child;
   }        
   else if (name.equals(CHILD_TBAOWORKPHONEFIRSTTHREEDIGITS)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOWORKPHONEFIRSTTHREEDIGITS,
                               doAppraisalProviderModel.FIELD_DFAOWORKPHONEFIRSTTHREEDIGITS,
                               CHILD_TBAOWORKPHONEFIRSTTHREEDIGITS_RESET_VALUE,
                               null);
           return child;
   }  
	else if (name.equals(CHILD_TBAOWORKPHONELASTFOURDIGITS)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOWORKPHONELASTFOURDIGITS,
                               doAppraisalProviderModel.FIELD_DFAOWORKPHONELASTFOURDIGITS,
                               CHILD_TBAOWORKPHONELASTFOURDIGITS_RESET_VALUE,
                               null);
           return child;
   }  


   else if (name.equals(CHILD_TBAOWORKPHONENUMEXTENSION)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOWORKPHONENUMEXTENSION,
                               doAppraisalProviderModel.FIELD_DFAOWORKPHONENUMEXTENSION,
                               CHILD_TBAOWORKPHONENUMEXTENSION_RESET_VALUE,
                               null);
           return child;
   }
   else if (name.equals(CHILD_TBAOCELLPHONEAREACODE)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOCELLPHONEAREACODE,
                               doAppraisalProviderModel.FIELD_DFAOCELLPHONEAREACODE,
                               CHILD_TBAOCELLPHONEAREACODE_RESET_VALUE,
                               null);
           return child;
   }        
   else if (name.equals(CHILD_TBAOCELLPHONEFIRSTTHREEDIGITS)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOCELLPHONEFIRSTTHREEDIGITS,
                               doAppraisalProviderModel.FIELD_DFAOCELLPHONEFIRSTTHREEDIGITS,
                               CHILD_TBAOCELLPHONEFIRSTTHREEDIGITS_RESET_VALUE,
                               null);
           return child;
   }

	else if (name.equals(CHILD_TBAOCELLPHONELASTFOURDIGITS)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOCELLPHONELASTFOURDIGITS,
                               doAppraisalProviderModel.FIELD_DFAOCELLPHONELASTFOURDIGITS,
                               CHILD_TBAOCELLPHONELASTFOURDIGITS_RESET_VALUE,
                               null);
           return child;
   }  
   else if (name.equals(CHILD_TBAOHOMEPHONEAREACODE)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOHOMEPHONEAREACODE,
                               doAppraisalProviderModel.FIELD_DFAOHOMEPHONEAREACODE,
                               CHILD_TBAOHOMEPHONEAREACODE_RESET_VALUE,
                               null);
           return child;
   }        
   else if (name.equals(CHILD_TBAOHOMEPHONEFIRSTTHREEDIGITS)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOHOMEPHONEFIRSTTHREEDIGITS,
                               doAppraisalProviderModel.FIELD_DFAOHOMEPHONEFIRSTTHREEDIGITS,
                               CHILD_TBAOHOMEPHONEFIRSTTHREEDIGITS_RESET_VALUE,
                               null);
           return child;
   }   

	else if (name.equals(CHILD_TBAOHOMEPHONELASTFOURDIGITS)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_TBAOHOMEPHONELASTFOURDIGITS,
                               doAppraisalProviderModel.FIELD_DFAOHOMEPHONELASTFOURDIGITS,
                               CHILD_TBAOHOMEPHONELASTFOURDIGITS_RESET_VALUE,
                               null);
           return child;
   }  
    
    else if (name.equals(CHILD_CHAOPRIMARYBORROWERINFO)) {
        CheckBox child = new CheckBox(this,
								  getDefaultModel(),
								  CHILD_CHAOPRIMARYBORROWERINFO,
								  CHILD_CHAOPRIMARYBORROWERINFO,
								  "Y", "N", false, null);
        return child;
    }
    else if (name.equals(CHILD_STAOPROPERTYOWNERNAME)) {
		TextField child =
           new TextField(this,
                               getdoAppraisalProviderModel(),
                               CHILD_STAOPROPERTYOWNERNAME,
                               doAppraisalProviderModel.FIELD_DFAOPROPERTYOWNERNAME,
                               CHILD_STAOPROPERTYOWNERNAME_RESET_VALUE,
                               null);
           return child;
   }   
  // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
   else if (name.equals(CHILD_STREAD)) {
		StaticTextField child =
         new StaticTextField(this,
                             getDefaultModel(),
                             CHILD_STREAD,
                             CHILD_STREAD,
                             CHILD_STREAD_RESET_VALUE,
                             null);
         return child;
   }
    // ***** Change by NBC/PP Implementation Team, Defect #1419 - END *****//    
    //==================================================================
    else
    {
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStPrimaryProperty()
  {
    return(StaticTextField)getChild(CHILD_STPRIMARYPROPERTY);
  }

  /**
   *
   *
   */
  public String endStPrimaryPropertyDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the stPrimaryProperty_onBeforeHtmlOutputEvent method
    AppraisalHandler handler = (AppraisalHandler)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    String rc = handler.setYesOrNo(event.getContent());
    handler.pageSaveState();

    return rc;
  }

  /**
   *
   *
   */
  public StaticTextField getStPropertyAddress()
  {
    return(StaticTextField)getChild(CHILD_STPROPERTYADDRESS);
  }

  /**
   *
   *
   */
  public StaticTextField getStPropertyOccurence()
  {
    return(StaticTextField)getChild(CHILD_STPROPERTYOCCURENCE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserName()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISERNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserShortName()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISERSHORTNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserCompanyName()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISERCOMPANYNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserAddress()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISERADDRESS);
  }

  /**
   *
   *
   */
  public StaticTextField getStPhone()
  {
    return(StaticTextField)getChild(CHILD_STPHONE);
  }

  /**
   *
   *
   */
  public StaticTextField getStFax()
  {
    return(StaticTextField)getChild(CHILD_STFAX);
  }

  /**
   *
   *
   */
  public StaticTextField getStEmail()
  {
    return(StaticTextField)getChild(CHILD_STEMAIL);
  }

  /**
   *
   *
   */
  public StaticTextField getStPropertyPurchasePrice()
  {
    return(StaticTextField)getChild(CHILD_STPROPERTYPURCHASEPRICE);
  }

  /**
   *
   *
   */
  public TextField getTbEstimatedAppraisal()
  {
    return(TextField)getChild(CHILD_TBESTIMATEDAPPRAISAL);
  }

  /**
   *
   *
   */
  public TextField getTbActualAppraisal()
  {
    return(TextField)getChild(CHILD_TBACTUALAPPRAISAL);
  }

  /**
   *
   *
   */
  public ComboBox getCbMonths()
  {
    return(ComboBox)getChild(CHILD_CBMONTHS);
  }

  /**
   *
   *
   */
  public TextField getTbDay()
  {
    return(TextField)getChild(CHILD_TBDAY);
  }

  /**
   *
   *
   */
  public TextField getTbYear()
  {
    return(TextField)getChild(CHILD_TBYEAR);
  }


  
  
  
  public ComboBox getCbAppraisalSource()
  {
    return(ComboBox)getChild(CHILD_CBAPPRAISALSOURCE);
  }

	public ComboBox getCbAppraisalProvider() {
		return(ComboBox)getChild(CHILD_CBAPPRAISALPROVIDER);
	}

	public ComboBox getCbAppraisalProduct() {
		return (ComboBox) getChild(CHILD_CBAPPRAISALPRODUCT);
	}
  

  

    // SEAN AVM Service Feb 13, 2006: getter for children.
    public ComboBox getCbAVMProvider() {
        return (ComboBox) getChild(CHILD_CBAVMPROVIDER);
    }

    public ComboBox getCbAVMProduct() {
        return (ComboBox) getChild(CHILD_CBAVMPRODUCT);
    }

    public StaticTextField getStAVMRequestStatus() {
        return (StaticTextField) getChild(CHILD_STAVMREQUESTSTATUS);
    }

    public StaticTextField getStAVMRequestDate() {
        return (StaticTextField) getChild(CHILD_STAVMREQUESTDATE);
    }

    public StaticTextField getStAVMRequestStatusMsg() {
        return (StaticTextField) getChild(CHILD_STAVMREQUESTSTATUSMSG);
    }

    public StaticTextField getStTileIndex() {
        return (StaticTextField) getChild(CHILD_STTILEINDEX);
    }

    public StaticTextField getStAVMReports() {
        return (StaticTextField) getChild(CHILD_STAVMREPORTS);
    }

    public StaticTextField getStAppraisalSummary() {
        return (StaticTextField) getChild(CHILD_STAPPRAISALSUMMARY);
    }


	public StaticTextField getStAppraisalContactInfoHiddenStart() {
		return (StaticTextField) getChild(CHILD_STAPPRAISALHIDDENSTART);
	}
	/**
	 * getStAppraisalContactInfoHiddenEnd
	 * this method returns static textfield used for hidden control 
	 * which is used to enable/disable Appraisal Contact Information section
	 * 
	 *
	 * @return StaticTextField : the result of getStAppraisalContactInfoHiddenEnd <br>
	 *   
	 */
	public StaticTextField getStAppraisalContactInfoHiddenEnd() {
		return (StaticTextField) getChild(CHILD_STAPPRAISALHIDDENEND);
	}
	
	/**
	 * getStAppraisalHiddenStart
	 * this method returns static textfield used for hidden control 
	 * which is used to enable/disable Appraisal Contact Information section
	 * 
	 *
	 * @return StaticTextField : the result of getStAppraisalHiddenStart <br>
	 *   
	 */	
	public StaticTextField getStAppraisalHiddenStart() {
		return (StaticTextField) getChild(CHILD_STAPPRAISALCONTINFOHIDDENSTART);
	}

	public StaticTextField getStAppraisalHiddenEnd() {
		return (StaticTextField) getChild(CHILD_STAPPRAISALCONTINFOHIDDENEND);
	}

	public StaticTextField getStAppraisalTypeHidden() {
		return (StaticTextField) getChild(CHILD_STAPPRAISALTYPEHIDDEN);
	}
	
	//FIX
	public StaticTextField getStAppraisalSourceStart() {
		return (StaticTextField) getChild(CHILD_STAPPRAISALSOURCESTART);
	}
	public StaticTextField getStAppraisalSourceEnd() {
		return (StaticTextField) getChild(CHILD_STAPPRAISALSOURCEEND);
	}
	//FIX

	public StaticTextField getStAVMOrderHidden() {
		return (StaticTextField) getChild(CHILD_STAVMORDERHIDDEN);
	}
	public StaticTextField getStAVMOrderHiddenEnd() {
		return (StaticTextField) getChild(CHILD_STAVMORDERHIDDENEND);
	}
    // SEAN AVM SERVICE end.

    public TextField getStAOContactFirstName() {
    	return(TextField) getChild(CHILD_STAOCONTACTFIRSTNAME);
    }
    
    public TextField getStAOContactLastName() {
    	return(TextField) getChild(CHILD_STAOCONTACTLASTNAME);
    }   
    
    public HiddenField getStAppraisalBorrowerId() {
        return(HiddenField) getChild(CHILD_STAPPRAISALBORROWERID);
    }
    
    public HiddenField getStAppraisalRequestId() {
        return(HiddenField) getChild(CHILD_STAPPRAISALREQUESTID);
    }   

    public HiddenField getStAppraisalContactId() {
        return(HiddenField) getChild(CHILD_STAPPRAISALCONTACTID);
    }
    
    public HiddenField getStAppraisalStatusId() {
        return(HiddenField) getChild(CHILD_STAPPRAISALSTATUSID);
    }
    
	public TextField getTbAOEmail() {
		return(TextField) getChild(CHILD_TBAOEMAIL);
	}


    public TextField getTbAOWorkPhoneAreaCode() {
    	return (TextField) getChild(CHILD_TBAOWORKPHONEAREACODE);
    }
    
    public TextField getTbAOWorkPhoneFirstThreeDigits() {
    	return (TextField) getChild(CHILD_TBAOWORKPHONEFIRSTTHREEDIGITS);
    }
    
    public TextField getTbAOWorkPhoneLastFourDigits() {
    	return (TextField) getChild(CHILD_TBAOWORKPHONELASTFOURDIGITS);
    }

    public TextField getTbAOWorkPhoneNumExtension() {
    	return (TextField) getChild(CHILD_TBAOWORKPHONENUMEXTENSION);
    }
    
    public TextField getTbAOCellPhoneAreaCode() {
    	return (TextField) getChild(CHILD_TBAOCELLPHONEAREACODE);
    }
    
    public TextField getTbAOCellPhoneFirstThreeDigits() {
    	return (TextField) getChild(CHILD_TBAOCELLPHONEFIRSTTHREEDIGITS);
    }

	public TextField getTbAOCellPhoneLastFourDigits() {
    	return (TextField) getChild(CHILD_TBAOCELLPHONELASTFOURDIGITS);
    }

    public TextField getTbAOHomePhoneAreaCode() {
    	return (TextField) getChild(CHILD_TBAOHOMEPHONEAREACODE);
    }
    
    public TextField getTbAOHomePhoneFirstThreeDigits() {
    	return (TextField) getChild(CHILD_TBAOHOMEPHONEFIRSTTHREEDIGITS);
    }
    
    public TextField getTbAOHomePhoneLastFourDigits() {
    	return (TextField) getChild(CHILD_TBAOHOMEPHONELASTFOURDIGITS);
    }

    public CheckBox getChAOPrimaryBorrowerInfo() {

        return (CheckBox) getChild(CHILD_CHAOPRIMARYBORROWERINFO);
    }
    
    public TextField getStAOPropertyOwnerName() {
    	return (TextField) getChild(CHILD_STAOPROPERTYOWNERNAME);
    }
	
    // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
    public StaticTextField getStRead() {
      return (StaticTextField) getChild(CHILD_STREAD);
    }
    // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
    
    // SEAN AVM SERVICE Feb 13, 2006: getter and setter for models.
    public AVMOrderModel getAVMOrderModel() {
        if (_avmOrderModel == null) 
            _avmOrderModel = (AVMOrderModel) getModel(AVMOrderModel.class);

        return _avmOrderModel;
    }

    public void setAVMOrderModel(AVMOrderModel model) {

        _avmOrderModel = model;
    }
    // SEAN AVM SERVICE end

  /**
   *
   *
   */
  static class CbAppraisalSourceOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbAppraisalSourceOptionList()
    {

    }

    /**
     *
     *
     */
    //--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 22Nov2002
    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"APPRAISALSOURCE", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }


  static class CbAppraisalProviderOptionList
    extends OptionList
  {
    /**
     *
     *
     */
    CbAppraisalProviderOptionList()
    {

    }

    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl)rc.getModelManager().getModel(
          SessionStateModel.class,
          defaultInstanceStateName,
          true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"SERVICEPROVIDER", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   *
   *
   */
  public TextField getTbAppraiserNotes()
  {
    return(TextField)getChild(CHILD_TBAPPRAISERNOTES);
  }

  /**
   *
   *
   */
  public void handleBtChangeAppraiserRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btChangeAppraiser_onWebEvent method
	    AppraisalHandler handler = (AppraisalHandler)this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());
	    handler.preHandlerProtocol(this.getParentViewBean());
    handler.changeAppraiser(handler.getRowNdxFromWebEventMethod(event), "btChangeAppraiser");
        handler.postHandlerProtocol();  
  }

  /**
   *
   *
   */
  public Button getBtChangeAppraiser()
  {
    return(Button)getChild(CHILD_BTCHANGEAPPRAISER);
  }

  /**
   *
   *
   */
  public String endBtChangeAppraiserDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btChangeAppraiser_onBeforeHtmlOutputEvent method
    AppraisalHandler handler = (AppraisalHandler)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    boolean rc = handler.setChangeAppraiserStatus();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public void handleBtAddAppraiserRequest(RequestInvocationEvent event) throws ServletException, IOException
  {
    // The following code block was migrated from the btAddAppraiser_onWebEvent method
	    AppraisalHandler handler = (AppraisalHandler)this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());

	    handler.preHandlerProtocol(this.getParentViewBean());
	        handler.changeAppraiser(handler.getRowNdxFromWebEventMethod(event), "btAddAppraiser");
        handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtAddAppraiser()
  {
    return(Button)getChild(CHILD_BTADDAPPRAISER);
  }

  /**
   *
   *
   */
  public String endBtAddAppraiserDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btAddAppraiser_onBeforeHtmlOutputEvent method
    AppraisalHandler handler = (AppraisalHandler)this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    boolean rc = handler.setAddAppraiserStatus();
    handler.pageSaveState();

    if(rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }
  }

  /**
   *
   *
   */
  public HiddenField getHbPropertyId()
  {
    return(HiddenField)getChild(CHILD_HBPROPERTYID);
  }

  /**
   *
   *
   */
  public HiddenField getHdAppraisalDate()
  {
    return(HiddenField)getChild(CHILD_HDAPPRAISALDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAppraiserExtension()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISEREXTENSION);
  }

  /**
   *
   *
   */
  public doAppraisalPropertyModel getdoAppraisalPropertyModel()
  {
    if(doAppraisalProperty == null)
    {
      doAppraisalProperty = (doAppraisalPropertyModel)getModel(doAppraisalPropertyModel.class);
    }
    return doAppraisalProperty;
  }

  /**
   *
   *
   */
  public void setdoAppraisalPropertyModel(doAppraisalPropertyModel model)
  {
    doAppraisalProperty = model;
  }

  /**
  *
  *
  */
 public doAppraisalProviderModel getdoAppraisalProviderModel()
 {
   if(doAppraisalProvider == null)
   {
     doAppraisalProvider = (doAppraisalProviderModel)getModel(doAppraisalProviderModel.class);
   }
   return doAppraisalProvider;
 }

 /**
  *
  *
  */
 public void setdoAppraisalProviderModel(doAppraisalProviderModel model)
 {
   doAppraisalProvider = model;
 }
  
  
  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  //New display fields for AppraisalOrder
  public StaticTextField getStAppraisalStatus()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISALSTATUS);
  }

  public CheckBox getChUseBorrowerInfo()
  {
    return(CheckBox)getChild(CHILD_CHUSEBORROWERINFO);
  }

  public TextField getTbPropertyOwnerName()
  {
    return(TextField)getChild(CHILD_TBPROPERTYOWNERNAME);
  }

  public TextField getTbAOContactName()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTNAME);
  }

  public TextField getTbAOContactWorkPhoneNum1()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTWORKPHONENUM1);
  }

  public TextField getTbAOContactWorkPhoneNum2()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTWORKPHONENUM2);
  }

  public TextField getTbAOContactWorkPhoneNum3()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTWORKPHONENUM3);
  }

  public TextField getTbAOContactWorkPhoneNumExt()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTWORKPHONENUMEXT);
  }

  public TextField getTbAOContactCellPhoneNum1()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTCELLPHONENUM1);
  }

  public TextField getTbAOContactCellPhoneNum2()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTCELLPHONENUM2);
  }

  public TextField getTbAOContactCellPhoneNum3()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTCELLPHONENUM3);
  }

  public TextField getTbAOContactHomePhoneNum1()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTHOMEPHONENUM1);
  }

  public TextField getTbAOContactHomePhoneNum2()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTHOMEPHONENUM2);
  }

  public TextField getTbAOContactHomePhoneNum3()
  {
    return(TextField)getChild(CHILD_TBAOCONTACTHOMEPHONENUM3);
  }

  public TextField getTbCommentsForAppraiser()
  {
    return(TextField)getChild(CHILD_TBCOMMENTSFORAPPRAISER);
  }

  public StaticTextField getStPropertyId()
 {
   return(StaticTextField)getChild(CHILD_STPROPERTYID);
 }

  
  public TextField getTbAppraisalSpecialInst()
  {
    return(TextField)getChild(CHILD_TBAPPRAISALSPECIALINST);
  }
  
  public StaticTextField getStAppraisalRequestStatus()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISALREQUESTSTATUS);
  }
  
  public StaticTextField getStAppraisalRequestStatusDate()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISALREQUESTSTATUSDATE);
  }
    
  
  public StaticTextField getStAppraisalRequestRefNum()
  {
    return(StaticTextField)getChild(CHILD_STAPPRAISALREQUESTREFNUM);
  }
  
  
  public TextField getTbAppraisalStatusMessages()
  {
    return(TextField)getChild(CHILD_TBAPPRAISALSTATUSMESSAGES);
  }
  
  
  //==================================================================

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////
  // Child accessors
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Child rendering methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Repeated event methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_STPRIMARYPROPERTY = "stPrimaryProperty";
  public static final String CHILD_STPRIMARYPROPERTY_RESET_VALUE = "";
  public static final String CHILD_STPROPERTYADDRESS = "stPropertyAddress";
  public static final String CHILD_STPROPERTYADDRESS_RESET_VALUE = "";
  public static final String CHILD_STPROPERTYOCCURENCE = "stPropertyOccurence";
  public static final String CHILD_STPROPERTYOCCURENCE_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISERNAME = "stAppraiserName";
  public static final String CHILD_STAPPRAISERNAME_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISERSHORTNAME = "stAppraiserShortName";
  public static final String CHILD_STAPPRAISERSHORTNAME_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISERCOMPANYNAME = "stAppraiserCompanyName";
  public static final String CHILD_STAPPRAISERCOMPANYNAME_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISERADDRESS = "stAppraiserAddress";
  public static final String CHILD_STAPPRAISERADDRESS_RESET_VALUE = "";
  public static final String CHILD_STPHONE = "stPhone";
  public static final String CHILD_STPHONE_RESET_VALUE = "";
  public static final String CHILD_STFAX = "stFax";
  public static final String CHILD_STFAX_RESET_VALUE = "";
  public static final String CHILD_STEMAIL = "stEmail";
  public static final String CHILD_STEMAIL_RESET_VALUE = "";
  public static final String CHILD_STPROPERTYPURCHASEPRICE = "stPropertyPurchasePrice";
  public static final String CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE = "";
  public static final String CHILD_TBESTIMATEDAPPRAISAL = "tbEstimatedAppraisal";
  public static final String CHILD_TBESTIMATEDAPPRAISAL_RESET_VALUE = "";
  public static final String CHILD_TBACTUALAPPRAISAL = "tbActualAppraisal";
  public static final String CHILD_TBACTUALAPPRAISAL_RESET_VALUE = "";
  public static final String CHILD_CBMONTHS = "cbMonths";
  public static final String CHILD_CBMONTHS_RESET_VALUE = "";
  private OptionList cbMonthsOptions = new OptionList(new String[]
    {}
    , new String[]
    {});
  public static final String CHILD_TBDAY = "tbDay";
  public static final String CHILD_TBDAY_RESET_VALUE = "";
  public static final String CHILD_TBYEAR = "tbYear";
  public static final String CHILD_TBYEAR_RESET_VALUE = "";
  public static final String CHILD_CBAPPRAISALSOURCE = "cbAppraisalSource";
  public static final String CHILD_CBAPPRAISALSOURCE_RESET_VALUE = "";

  public static final String CHILD_CBAPPRAISALPROVIDER = "cbAppraisalProvider";
  public static final String CHILD_CBAPPRAISALPROVIDER_RESET_VALUE = "";
  public static final String CHILD_CBAPPRAISALPRODUCT = "cbAppraisalProduct";
  public static final String CHILD_CBAPPRAISALPRODUCT_RESET_VALUE = "";

  //--Release2.1--//
  //private static CbAppraisalSourceOptionList cbAppraisalSourceOptions=new CbAppraisalSourceOptionList();
  private CbAppraisalSourceOptionList cbAppraisalSourceOptions = new CbAppraisalSourceOptionList();
  private CbAppraisalProviderOptionList cbAppraisalProviderOptions = new CbAppraisalProviderOptionList();

  public static final String CHILD_TBAPPRAISERNOTES = "tbAppraiserNotes";
  public static final String CHILD_TBAPPRAISERNOTES_RESET_VALUE = "";
  public static final String CHILD_BTCHANGEAPPRAISER = "btChangeAppraiser";
  public static final String CHILD_BTCHANGEAPPRAISER_RESET_VALUE = " ";
  public static final String CHILD_BTADDAPPRAISER = "btAddAppraiser";
  public static final String CHILD_BTADDAPPRAISER_RESET_VALUE = " ";
  public static final String CHILD_HBPROPERTYID = "hbPropertyId";
  public static final String CHILD_HBPROPERTYID_RESET_VALUE = "";
  public static final String CHILD_HDAPPRAISALDATE = "hdAppraisalDate";
  public static final String CHILD_HDAPPRAISALDATE_RESET_VALUE = "";
  public static final String CHILD_STAPPRAISEREXTENSION = "stAppraiserExtension";
  public static final String CHILD_STAPPRAISEREXTENSION_RESET_VALUE = "";
  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  //New display fields for AppraisalOrder
  public static final String CHILD_STAPPRAISALSTATUS = "stAppraisalStatus";
  public static final String CHILD_STAPPRAISALSTATUS_RESET_VALUE = "";
  public static final String CHILD_CHUSEBORROWERINFO = "chUseBorrowerInfo";
  public static final String CHILD_CHUSEBORROWERINFO_RESET_VALUE = "";
  public static final String CHILD_TBPROPERTYOWNERNAME = "tbPropertyOwnerName";
  public static final String CHILD_TBPROPERTYOWNERNAME_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTNAME = "tbAOContactName";
  public static final String CHILD_TBAOCONTACTNAME_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM1 = "tbAOContactWorkPhoneNum1";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM1_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM2 = "tbAOContactWorkPhoneNum2";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM2_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM3 = "tbAOContactWorkPhoneNum3";
  public static final String CHILD_TBAOCONTACTWORKPHONENUM3_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTWORKPHONENUMEXT = "tbAOContactWorkPhoneNumExt";
  public static final String CHILD_TBAOCONTACTWORKPHONENUMEXT_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM1 = "tbAOContactCellPhoneNum1";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM1_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM2 = "tbAOContactCellPhoneNum2";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM2_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM3 = "tbAOContactCellPhoneNum3";
  public static final String CHILD_TBAOCONTACTCELLPHONENUM3_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM1 = "tbAOContactHomePhoneNum1";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM1_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM2 = "tbAOContactHomePhoneNum2";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM2_RESET_VALUE = "";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM3 = "tbAOContactHomePhoneNum3";
  public static final String CHILD_TBAOCONTACTHOMEPHONENUM3_RESET_VALUE = "";
  public static final String CHILD_TBCOMMENTSFORAPPRAISER = "tbCommentsForAppraiser";
  public static final String CHILD_TBCOMMENTSFORAPPRAISER_RESET_VALUE = "";
  public static final String CHILD_STPROPERTYID = "stPropertyId";
  public static final String CHILD_STPROPERTYID_RESET_VALUE = "";
  //============================================================

    // SEAN AVM SERVICE Feb 13, 2006: child name definition.
    private CbAVMProviderOptions _cbAVMProviderOptions =
        new CbAVMProviderOptions();
    public static final String CHILD_CBAVMPROVIDER = "cbAVMProvider";
    public static final String CHILD_CBAVMPROVIDER_RESET_VALUE = "";
	// the product options are setted on the fly.
    public static final String CHILD_CBAVMPRODUCT = "cbAVMProduct";
    public static final String CHILD_CBAVMPRODUCT_RESET_VALUE = "";
    public static final String CHILD_STAVMREQUESTSTATUS =
        "stAVMRequestStatus";
    public static final String CHILD_STAVMREQUESTSTATUS_RESET_VALUE = "";
    public static final String CHILD_STAVMREQUESTDATE =
        "stAVMRequestDate";
    public static final String CHILD_STAVMREQUESTDATE_RESET_VALUE = "";
    public static final String CHILD_STAVMREQUESTSTATUSMSG =
        "stAVMRequestStatusMsg";
    public static final String CHILD_STAVMREQUESTSTATUSMSG_RESET_VALUE = "";
    // the tile index.
    public static final String CHILD_STTILEINDEX = "stTileIndex";
    public static final String CHILD_STTILEINDEX_RESET_VALUE = "";
    // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
    public static final String CHILD_STREAD = "stRead";
    public static final String CHILD_STREAD_RESET_VALUE = "";
    // ***** Change by NBC/PP Implementation Team, Defect #1419 - START *****//
    // the existing response.
    public static final String CHILD_STAVMREPORTS = "stAVMReports";
    public static final String CHILD_STAVMREPORTS_RESET_VALUE = "";

	public static final String CHILD_STAPPRAISALSUMMARY = "stAppraisalSummary";
    public static final String CHILD_STAPPRAISALSUMMARY_RESET_VALUE = "";

    // hidden control.
    public static final String CHILD_STAPPRAISALHIDDENSTART = "stAppraisalHiddenStart";
    public static final String CHILD_STAPPRAISALHIDDENSTART_RESET_VALUE = "";
    public static final String CHILD_STAPPRAISALHIDDENEND = "stAppraisalHiddenEnd";
    public static final String CHILD_STAPPRAISALHIDDENEND_RESET_VALUE = "";
    //START: hidden controls for enabling/disabling Appraisal Contact Information section
    public static final String CHILD_STAPPRAISALCONTINFOHIDDENSTART = "stAppraisalContactInfoHiddenStart";
    public static final String CHILD_STAPPRAISALCONTINFOHIDDENSTART_RESET_VALUE = "";
    public static final String CHILD_STAPPRAISALCONTINFOHIDDENEND = "stAppraisalContactInfoHiddenEnd";
    public static final String CHILD_STAPPRAISALCONTINFOHIDDENEND_RESET_VALUE = "";
    //END: hidden controls for enabling/disabling Appraisal Contact Information section
    public static final String CHILD_STAPPRAISALTYPEHIDDEN = "stAppraisalTypeHidden";
    public static final String CHILD_STAPPRAISALTYPEHIDDEN_RESET_VALUE = "hidden";
    
    //FIX
    public static final String CHILD_STAPPRAISALSOURCESTART = "stAppraisalSourceStart";
    public static final String CHILD_STAPPRAISALSOURCESTART_RESET_VALUE = "";
    
    public static final String CHILD_STAPPRAISALSOURCEEND = "stAppraisalSourceEnd";
    public static final String CHILD_STAPPRAISALSOURCEEND_RESET_VALUE = "";
    //FIX
	public static final String CHILD_STAVMORDERHIDDEN = "stAVMOrderHidden";
	public static final String CHILD_STAVMORDERHIDDEN_RESET_VALUE = "";
	public static final String CHILD_STAVMORDERHIDDENEND = "stAVMOrderHiddenEnd";
	public static final String CHILD_STAVMORDERHIDDENEND_RESET_VALUE = "";
    
    public static final String CHILD_STAPPRAISALPROVIDER = "cbAppraisalProvider";
    public static final String CHILD_STAPPRAISALPROVIDER_RESET_VALUE = "";
    
    public static final String CHILD_STAPPRAISALSOURCE = "cbAppraisalSource";
    public static final String CHILD_STAPPRAISALSOURCE_RESET_VALUE = "";
    
    public static final String CHILD_TBAPPRAISALSPECIALINST = "tbAppraisalSpecialInst";
    public static final String CHILD_TBAPPRAISALSPECIALINST_RESET_VALUE = "";
    
    public static final String CHILD_STAPPRAISALREQUESTSTATUS = "stAppraisalRequestStatus";
    public static final String CHILD_STAPPRAISALREQUESTSTATUS_RESET_VALUE = "";
    
    public static final String CHILD_STAPPRAISALREQUESTSTATUSDATE = "stAppraisalRequestStatusDate";
    public static final String CHILD_STAPPRAISALREQUESTSTATUSDATE_RESET_VALUE = "";
    
    public static final String CHILD_STAPPRAISALREQUESTREFNUM = "stAppraisalRequestRefNum";
    public static final String CHILD_STAPPRAISALREQUESTREFNUM_RESET_VALUE = ""; 
    
    public static final String CHILD_BTAPPRAISALCANCEL = "btAppraisalCancel";
    public static final String CHILD_BTAPPRAISALCANCEL_RESET_VALUE = "";
    
    public static final String CHILD_TBAPPRAISALSTATUSMESSAGES = "tbAppraisalStatusMessages";
    public static final String CHILD_TBAPPRAISALSTATUSMESSAGES_RESET_VALUE = "";
    public static final String CHILD_STAOCONTACTFIRSTNAME = "stAOContactFirstName";
    public static final String CHILD_STAOCONTACTFIRSTNAME_RESET_VALUE = "";
    
    public static final String CHILD_STAOCONTACTLASTNAME = "stAOContactLastName";
    public static final String CHILD_STAOCONTACTLASTNAME_RESET_VALUE = "";

    public static final String CHILD_TBAOEMAIL = "tbAOEmail";
    public static final String CHILD_TBAOEMAIL_RESET_VALUE ="";
    
    public static final String CHILD_TBAOWORKPHONEAREACODE = "tbAOWorkPhoneAreaCode";
    public static final String CHILD_TBAOWORKPHONEAREACODE_RESET_VALUE ="";
        
    public static final String CHILD_TBAOWORKPHONEFIRSTTHREEDIGITS = "tbAOWorkPhoneFirstThreeDigits";
    public static final String CHILD_TBAOWORKPHONEFIRSTTHREEDIGITS_RESET_VALUE ="";

    public static final String CHILD_TBAOWORKPHONELASTFOURDIGITS = "tbAOWorkPhoneLastFourDigits";
    public static final String CHILD_TBAOWORKPHONELASTFOURDIGITS_RESET_VALUE ="";

    public static final String CHILD_TBAOWORKPHONENUMEXTENSION = "tbAOWorkPhoneNumExtension";
    public static final String CHILD_TBAOWORKPHONENUMEXTENSION_RESET_VALUE ="";
    
    public static final String CHILD_TBAOCELLPHONEAREACODE = "tbAOCellPhoneAreaCode";
    public static final String CHILD_TBAOCELLPHONEAREACODE_RESET_VALUE ="";
        
    public static final String CHILD_TBAOCELLPHONEFIRSTTHREEDIGITS = "tbAOCellPhoneFirstThreeDigits";
    public static final String CHILD_TBAOCELLPHONEFIRSTTHREEDIGITS_RESET_VALUE ="";

	public static final String CHILD_TBAOCELLPHONELASTFOURDIGITS = "tbAOCellPhoneLastFourDigits";
    public static final String CHILD_TBAOCELLPHONELASTFOURDIGITS_RESET_VALUE ="";
    
    public static final String CHILD_TBAOHOMEPHONEAREACODE = "tbAOHomePhoneAreaCode";
    public static final String CHILD_TBAOHOMEPHONEAREACODE_RESET_VALUE = "";
        
    public static final String CHILD_TBAOHOMEPHONEFIRSTTHREEDIGITS = "tbAOHomePhoneFirstThreeDigits";
    public static final String CHILD_TBAOHOMEPHONEFIRSTTHREEDIGITS_RESET_VALUE = "";

	public static final String CHILD_TBAOHOMEPHONELASTFOURDIGITS = "tbAOHomePhoneLastFourDigits";
	public static final String CHILD_TBAOHOMEPHONELASTFOURDIGITS_RESET_VALUE = "";
	
	public static final String CHILD_CHAOPRIMARYBORROWERINFO_RESET_VALUE ="";
	public static final String CHILD_CHAOPRIMARYBORROWERINFO = "chPrimaryBorrowerInfo";
	
	public static final String CHILD_STAOPROPERTYOWNERNAME_RESET_VALUE ="";
	public static final String CHILD_STAOPROPERTYOWNERNAME = "stAOPropertyOwnerName";
    
    public static final String CHILD_STAPPRAISALBORROWERID_RESET_VALUE ="";
    public static final String CHILD_STAPPRAISALBORROWERID = "stAppraisalBorrowerId";
    
    public static final String CHILD_STAPPRAISALREQUESTID_RESET_VALUE ="";
    public static final String CHILD_STAPPRAISALREQUESTID = "stAppraisalRequestId";
    
    public static final String CHILD_STAPPRAISALCONTACTID_RESET_VALUE ="";
    public static final String CHILD_STAPPRAISALCONTACTID = "stAppraisalContactId";
    
    public static final String CHILD_STAPPRAISALSTATUSID_RESET_VALUE ="";
    public static final String CHILD_STAPPRAISALSTATUSID = "stAppraisalStatusId";
    //Appraisal end

	//stAOPropertyOwnerName

    // SEAN AVM SERVICE END.


  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  private doAppraisalPropertyModel doAppraisalProperty = null;
  private doAppraisalProviderModel doAppraisalProvider = null;
    // SEAN AVM SERVICE Feb 13, 2006: define the model.
    private AVMOrderModel _avmOrderModel = null;
    // SEAN AVM SERVICE end.
    
  private AppraisalHandler handler = new AppraisalHandler();

    // SEAN AVM Service Feb 16, 2005:  preparing the combo box options.
    /**
     * Option list for the closing service provider - outsourced closing service.
     */
    class CbAVMProviderOptions
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {

        /**
         * constructor.
         */
        CbAVMProviderOptions() {
        }

        /**
         * populate the options from the bxresources bundle.
         */
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
              rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
              (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                                                                    defaultInstanceStateName,
                                                                    true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "SERVICEPROVIDER", _cbAVMProviderOptions);
        }
    }

    class CbAVMProductOptions
        extends pgUWorksheetViewBean.BaseComboBoxOptionList {

        /**
         * constructor.
         */
        CbAVMProductOptions() {
        }

        /**
         * populate the options from the bxresources bundle.
         */
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
              rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
              (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                                                                    defaultInstanceStateName,
                                                                    true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "SERVICEPRODUCT", new OptionList());
        }
    }
    // SEAN AVM Service END
}
