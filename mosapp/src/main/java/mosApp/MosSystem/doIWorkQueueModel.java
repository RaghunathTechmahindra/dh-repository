package mosApp.MosSystem;

import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import java.io.*;

import java.math.BigDecimal;

import java.sql.*;

import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;


/**
 *
 *
 *
 */
public interface doIWorkQueueModel extends QueryModel, SelectQueryModel
{
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  public static final String FIELD_DFCONTACTLASTNAME = "dfCONTACTLASTNAME";
  public static final String FIELD_DFSFSHORTNAME = "dfSFSHORTNAME";
  public static final String FIELD_DFASSIGNEDTASKSWORKQUEUEID = "dfASSIGNEDTASKSWORKQUEUEID";
  public static final String FIELD_DFDEALID = "dfDEALID";
  public static final String FIELD_DFAPPLICATIONID = "dfAPPLICATIONID";
  public static final String FIELD_DFTASKID = "dfTASKID";
  public static final String FIELD_DFTASK_TASKLABEL = "dfTASK_TASKLABEL";
  public static final String FIELD_DFTASKSTATUS_TSDESCRIPTION = "dfTASKSTATUS_TSDESCRIPTION";
  public static final String FIELD_DFPRIORITY_PDESCRIPTION = "dfPRIORITY_PDESCRIPTION";
  public static final String FIELD_DFDUETIMESTAMP = "dfDUETIMESTAMP";
  public static final String FIELD_DFTMDESCRIPTION = "dfTMDESCRIPTION";
  public static final String FIELD_DFUSERPROFILEID = "dfUSERPROFILEID";
  public static final String FIELD_DFCOPYTYPE = "dfCOPYTYPE";
  public static final String FIELD_DFCOPYID = "dfCOPYID";

  //-- ========== SCR#750 begins ======================================== --//
  //-- by Neil on Dec/20/2004
  public static final String FIELD_DFHOLDREASONID = "dfHoldReasonId";
  public static final String FIELD_DFHOLDREASONDESCRIPTION = "dfHoldReasonDescription";
  public static final String FIELD_DFDEALSTATUS = "dfDealStatus";
  
  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
  public static final String FIELD_DFINSTITUTIONNAME = "dfInstitutionName";

  /**
   *
   *
   */
  public String getDfCONTACTLASTNAME();

  /**
   *
   *
   */
  public void setDfCONTACTLASTNAME(String value);

  /**
   *
   *
   */
  public String getDfSFSHORTNAME();

  /**
   *
   *
   */
  public void setDfSFSHORTNAME(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfASSIGNEDTASKSWORKQUEUEID();

  /**
   *
   *
   */
  public void setDfASSIGNEDTASKSWORKQUEUEID(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDEALID();

  /**
   *
   *
   */
  public void setDfDEALID(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfAPPLICATIONID();

  /**
   *
   *
   */
  public void setDfAPPLICATIONID(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTASKID();

  /**
   *
   *
   */
  public void setDfTASKID(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfTASK_TASKLABEL();

  /**
   *
   *
   */
  public void setDfTASK_TASKLABEL(String value);

  /**
   *
   *
   */
  public String getDfTASKSTATUS_TSDESCRIPTION();

  /**
   *
   *
   */
  public void setDfTASKSTATUS_TSDESCRIPTION(String value);

  /**
   *
   *
   */
  public String getDfPRIORITY_PDESCRIPTION();

  /**
   *
   *
   */
  public void setDfPRIORITY_PDESCRIPTION(String value);

  /**
   *
   *
   */
  public java.sql.Timestamp getDfDUETIMESTAMP();

  /**
   *
   *
   */
  public void setDfDUETIMESTAMP(java.sql.Timestamp value);

  /**
   *
   *
   */
  public String getDfTMDESCRIPTION();

  /**
   *
   *
   */
  public void setDfTMDESCRIPTION(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfUSERPROFILEID();

  /**
   *
   *
   */
  public void setDfUSERPROFILEID(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfCOPYTYPE();

  /**
   *
   *
   */
  public void setDfCOPYTYPE(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCOPYID();

  /**
   *
   *
   */
  public void setDfCOPYID(java.math.BigDecimal value);

  public BigDecimal getDfHoldReasonId();

  public void setDfHoldReasonId(BigDecimal value);

  public void setDfHoldReasonDescription(String value);

  public String getDfHoldReasonDescription();

  public String getDfDealStatus();

  public void setDfDealStatus(String value);

  //-- ========== SCR#750 ends ========================================== --//
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
  
  
  public BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(BigDecimal id);
  
  
  public String getDfInstitutionName();
  public void setDfInstitutionName(String id);
}
