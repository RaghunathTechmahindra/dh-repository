package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewTDSModelImpl extends QueryModelBase
	implements doDetailViewTDSModel
{
	/**
	 *
	 *
	 */
	public doDetailViewTDSModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert BORROWERTYPE Desc.
      this.setDfApplicantType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "BORROWERTYPE", this.getDfApplicantType(), languageId));
      this.setDfSuffix(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(), "SUFFIX", this.getDfSuffix(), languageId)); 

    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFirstName()
	{
		return (String)getValue(FIELD_DFFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstName(String value)
	{
		setValue(FIELD_DFFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMiddleName()
	{
		return (String)getValue(FIELD_DFMIDDLENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfMiddleName(String value)
	{
		setValue(FIELD_DFMIDDLENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLastName()
	{
		return (String)getValue(FIELD_DFLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfLastName(String value)
	{
		setValue(FIELD_DFLASTNAME,value);
	}

    /**
     *
     *
     */
    public String getDfSuffix()
    {
        return (String)getValue(FIELD_DFSUFFIX);
    }


    /**
     *
     *
     */
    public void setDfSuffix(String value)
    {
        setValue(FIELD_DFSUFFIX,value);
    }
    
	/**
	 *
	 *
	 */
	public String getDfApplicantType()
	{
		return (String)getValue(FIELD_DFAPPLICANTTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicantType(String value)
	{
		setValue(FIELD_DFAPPLICANTTYPE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTDS3Yr()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTDS3YR);
	}


	/**
	 *
	 *
	 */
	public void setDfTDS3Yr(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTDS3YR,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	  public java.math.BigDecimal getDfInstitutionId() {
		    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
		  }

		  public void setDfInstitutionId(java.math.BigDecimal value) {
		    setValue(FIELD_DFINSTITUTIONID, value);
		  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BORROWER.BORROWERID, BORROWER.DEALID, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, BORROWERTYPE.BTDESCRIPTION, BORROWER.TDS, BORROWER.TDS3YEAR, BORROWER.COPYID FROM BORROWER, BORROWERTYPE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT BORROWER.BORROWERID, BORROWER.DEALID, BORROWER.BORROWERFIRSTNAME, "+
    "BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, to_char(BORROWER.BORROWERTYPEID) BORROWERTYPEID_STR, "+
    "BORROWER.TDS, BORROWER.TDS3YEAR, BORROWER.COPYID, to_char(BORROWER.SUFFIXID) BORROWERSUFFIXID_STR, "+
    "BORROWER.INSTITUTIONPROFILEID "+
    " FROM BORROWER  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER, BORROWERTYPE";
  public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER";
  //--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
	//public static final String STATIC_WHERE_CRITERIA=" (BORROWER.BORROWERTYPEID  =  BORROWERTYPE.BORROWERTYPEID)";
	public static final String STATIC_WHERE_CRITERIA=
    "";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
	public static final String COLUMN_DFFIRSTNAME="BORROWERFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFMIDDLENAME="BORROWER.BORROWERMIDDLEINITIAL";
	public static final String COLUMN_DFMIDDLENAME="BORROWERMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFLASTNAME="BORROWER.BORROWERLASTNAME";
	public static final String COLUMN_DFLASTNAME="BORROWERLASTNAME";
	public static final String QUALIFIED_COLUMN_DFTDS="BORROWER.TDS";
	public static final String COLUMN_DFTDS="TDS";
	public static final String QUALIFIED_COLUMN_DFTDS3YR="BORROWER.TDS3YEAR";
	public static final String COLUMN_DFTDS3YR="TDS3YEAR";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWER.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
    public static final String QUALIFIED_COLUMN_DFSUFFIX="BORROWER.BORROWERSUFFIXID_STR";
    public static final String COLUMN_DFSUFFIX="BORROWERSUFFIXID_STR";
    
	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFAPPLICANTTYPE="BORROWERTYPE.BTDESCRIPTION";
	//public static final String COLUMN_DFAPPLICANTTYPE="BTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFAPPLICANTTYPE="BORROWER.BORROWERTYPEID_STR";
	public static final String COLUMN_DFAPPLICANTTYPE="BORROWERTYPEID_STR";
	  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
		    "BORROWER.INSTITUTIONPROFILEID";
	  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTNAME,
				COLUMN_DFFIRSTNAME,
				QUALIFIED_COLUMN_DFFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIDDLENAME,
				COLUMN_DFMIDDLENAME,
				QUALIFIED_COLUMN_DFMIDDLENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLASTNAME,
				COLUMN_DFLASTNAME,
				QUALIFIED_COLUMN_DFLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFSUFFIX,
                COLUMN_DFSUFFIX,
                QUALIFIED_COLUMN_DFSUFFIX,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
		
        FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICANTTYPE,
				COLUMN_DFAPPLICANTTYPE,
				QUALIFIED_COLUMN_DFAPPLICANTTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTDS,
				COLUMN_DFTDS,
				QUALIFIED_COLUMN_DFTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTDS3YR,
				COLUMN_DFTDS3YR,
				QUALIFIED_COLUMN_DFTDS3YR,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		   FIELD_SCHEMA.addFieldDescriptor(
			        new QueryFieldDescriptor(
			          FIELD_DFINSTITUTIONID,
			          COLUMN_DFINSTITUTIONID,
			          QUALIFIED_COLUMN_DFINSTITUTIONID,
			          java.math.BigDecimal.class,
			          false,
			          false,
			          QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
			          "",
			          QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
			          ""));
	}

}

