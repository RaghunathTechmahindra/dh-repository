package mosApp.MosSystem;

import java.sql.SQLException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doSourceFirmModelImpl extends QueryModelBase
	implements doSourceFirmModel
{
	/**
	 *
	 *
	 */
	public doSourceFirmModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--> The following were missed in Multi-Lingual conversion
    //--> By Billy 14Nov2003

    ////logger = SysLog.getSysLogger("DOSFM");
    ////logger.debug("DOSFM@afterExecute::SQLTemplate: " + this.getSelectSQL());
    ////logger.debug("DOSFM@afterExecute::Size: " + this.getSize());

    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

    int languageId = theSessionState.getLanguageId();

    ////logger.debug("DOSFM@afterExecute::SYSTEMTYPE: " + this.getDfSFPSystemTypeDesc());
    ////logger.debug("DOSFM@afterExecute::SYSTEMTYPETransl: " + BXResources.getPickListDescription( "SYSTEMTYPE", this.getDfSFPSystemTypeDesc(), languageId));

    ////logger.debug("DOSFM@afterExecute::PrefDelMethod: " + this.getDfPreferredDeliveryMethod());
    ////logger.debug("DOSFM@afterExecute::PrefDelMethodTransl: " + BXResources.getPickListDescription("PREFDELIVERYMETHOD", this.getDfPreferredDeliveryMethod(), languageId));

    while(this.next())
    {
      // Profile Status.
      this.setDfSFPStatusDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PROFILESTATUS", this.getDfSFPStatusDesc(), languageId));

      // Province
      this.setDfSFPProvinceName(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PROVINCE", this.getDfSFPProvinceName(), languageId));

      // get SystemType description
      this.setDfSFPSystemTypeDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "SYSTEMTYPE", this.getDfSFPSystemTypeDesc(), languageId));

      //--DJ_PREFCOMMETHOD_CR--start--//
      this.setDfPreferredDeliveryMethod(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
        "PREFERREDDELIVERYMETHOD", this.getDfPreferredDeliveryMethod(), languageId));
      //--DJ_PREFCOMMETHOD_CR--end--//
    }

   //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfSBPFirstName()
	{
		return (String)getValue(FIELD_DFSBPFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPFirstName(String value)
	{
		setValue(FIELD_DFSBPFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPMidInit()
	{
		return (String)getValue(FIELD_DFSBPMIDINIT);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPMidInit(String value)
	{
		setValue(FIELD_DFSBPMIDINIT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPLastName()
	{
		return (String)getValue(FIELD_DFSBPLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPLastName(String value)
	{
		setValue(FIELD_DFSBPLASTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPPhone()
	{
		return (String)getValue(FIELD_DFSBPPHONE);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPPhone(String value)
	{
		setValue(FIELD_DFSBPPHONE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPFax()
	{
		return (String)getValue(FIELD_DFSBPFAX);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPFax(String value)
	{
		setValue(FIELD_DFSBPFAX,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPEmail()
	{
		return (String)getValue(FIELD_DFSBPEMAIL);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPEmail(String value)
	{
		setValue(FIELD_DFSBPEMAIL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPCity()
	{
		return (String)getValue(FIELD_DFSBPCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPCity(String value)
	{
		setValue(FIELD_DFSBPCITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPProvinceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPPROVINCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPProvinceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPPROVINCEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBP_FSA()
	{
		return (String)getValue(FIELD_DFSBP_FSA);
	}


	/**
	 *
	 *
	 */
	public void setDfSBP_FSA(String value)
	{
		setValue(FIELD_DFSBP_FSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBP_LDU()
	{
		return (String)getValue(FIELD_DFSBP_LDU);
	}


	/**
	 *
	 *
	 */
	public void setDfSBP_LDU(String value)
	{
		setValue(FIELD_DFSBP_LDU,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPAddrLine1()
	{
		return (String)getValue(FIELD_DFSBPADDRLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPAddrLine1(String value)
	{
		setValue(FIELD_DFSBPADDRLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPAddrLine2()
	{
		return (String)getValue(FIELD_DFSBPADDRLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPAddrLine2(String value)
	{
		setValue(FIELD_DFSBPADDRLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPPhoneExt()
	{
		return (String)getValue(FIELD_DFSBPPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPPhoneExt(String value)
	{
		setValue(FIELD_DFSBPPHONEEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPShortName()
	{
		return (String)getValue(FIELD_DFSBPSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPShortName(String value)
	{
		setValue(FIELD_DFSBPSHORTNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPSystemTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPSYSTEMTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPSystemTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPSYSTEMTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPClientId()
	{
		return (String)getValue(FIELD_DFSBPCLIENTID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPClientId(String value)
	{
		setValue(FIELD_DFSBPCLIENTID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPAlternativeId()
	{
		return (String)getValue(FIELD_DFSBPALTERNATIVEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPAlternativeId(String value)
	{
		setValue(FIELD_DFSBPALTERNATIVEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSBPFirmName()
	{
		return (String)getValue(FIELD_DFSBPFIRMNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPFirmName(String value)
	{
		setValue(FIELD_DFSBPFIRMNAME,value);
	}

	/**
	 *
	 *
	 */
	public String getDfLicenseNumber()
	{
		return (String)getValue(FIELD_DFLICENSENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfLicenseNumber(String value)
	{
		setValue(FIELD_DFLICENSENUMBER,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPFirmProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPFIRMPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPFirmProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPFIRMPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPUserProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPUSERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPUserProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPUSERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfSBPUserEffectiveDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFSBPUSEREFFECTIVEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfSBPUserEffectiveDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFSBPUSEREFFECTIVEDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSFPStatusDesc()
	{
		return (String)getValue(FIELD_DFSFPSTATUSDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfSFPStatusDesc(String value)
	{
		setValue(FIELD_DFSFPSTATUSDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSFPSystemTypeDesc()
	{
		return (String)getValue(FIELD_DFSFPSYSTEMTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfSFPSystemTypeDesc(String value)
	{
		setValue(FIELD_DFSFPSYSTEMTYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSFPProvinceName()
	{
		return (String)getValue(FIELD_DFSFPPROVINCENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfSFPProvinceName(String value)
	{
		setValue(FIELD_DFSFPPROVINCENAME,value);
	}

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Added new fields to handle SourceFirm to Group association
  //--> By Billy 19Aug2003
  public java.math.BigDecimal getDfSBPGroupProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSBPGROUPPROFILEID);
	}

	public void setDfSBPGroupProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSBPGROUPPROFILEID,value);
	}

	public java.sql.Timestamp getDfSBPGroupEffectiveDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFSBPGROUPEFFECTIVEDATE);
	}

	public void setDfSBPGroupEffectiveDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFSBPGROUPEFFECTIVEDATE,value);
	}
  //======================================================

  //---------------- FXLink Phase II ------------------//
  //--> Add new field ==> Firm System Code
  //--> By Billy 14Nov2003
  public String getDfSFPSystemCode()
	{
		return (String)getValue(FIELD_DFSFPSYSTEMCODE);
	}

	public void setDfSFPSystemCode(String value)
	{
		setValue(FIELD_DFSFPSYSTEMCODE,value);
	}
  //====================================================

  //--DJ_PREFCOMMETHOD_CR--start--//
  /**
   *
   *
   */
  public String getDfPreferredDeliveryMethod()
  {
    return (String)getValue(FIELD_DFPREFERREDDELIVERYMETHOD);
  }

  /**
   *
   *
   */
  public void setDfPreferredDeliveryMethod(String value)
  {
    setValue(FIELD_DFPREFERREDDELIVERYMETHOD,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPreferredDeliveryMethodId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPREFERREDDELIVERYMETHODID);
  }

  /**
   *
   *
   */
  public void setDfPreferredDeliveryMethodId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPREFERREDDELIVERYMETHODID,value);
  }
  //--DJ_PREFCOMMETHOD_CR--end--//
  public java.math.BigDecimal getDfInstitutionId() {
      return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
    }

    public void setDfInstitutionId(java.math.BigDecimal value) {
      setValue(FIELD_DFINSTITUTIONID, value);
    }
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  public SysLogger logger;

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Added new fields to handle SourceFirm to Group association
  //--> By Billy 19Aug2003
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME, " +
    "CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTEMAILADDRESS, " +
    "ADDR.CITY, ADDR.PROVINCEID, ADDR.POSTALFSA, ADDR.POSTALLDU, ADDR.ADDRESSLINE1, " +
    "ADDR.ADDRESSLINE2, CONTACT.CONTACTPHONENUMBEREXTENSION, SOURCEFIRMPROFILE.SFSHORTNAME, " +
    "SOURCEFIRMPROFILE.PROFILESTATUSID, SOURCEFIRMPROFILE.SYSTEMTYPEID, " +
    "SOURCEFIRMPROFILE.SFBUSINESSID, SOURCEFIRMPROFILE.ALTERNATIVEID, " +
    "SOURCEFIRMPROFILE.SOURCEFIRMNAME, SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID, " +
    "SOURCEFIRMPROFILE.SFLICENSEREGISTRATIONNUMBER, " +
    "SOURCEFIRMTOUSERASSOC.USERPROFILEID, SOURCEFIRMTOUSERASSOC.EFFECTIVEDATE, " +
    "SOURCEFIRMTOGROUPASSOC.GROUPPROFILEID, SOURCEFIRMTOGROUPASSOC.EFFECTIVEDATE EFFECTIVEDATE_GROUP, " +
    //--> The following were missed in Multi-Lingual conversion
    //--> By Billy 14Nov2003
    //"PROFILESTATUS.PSDESCRIPTION, SYSTEMTYPE.SYSTEMTYPEDESCRIPTION, PROVINCE.PROVINCENAME " +
    "to_char(SOURCEFIRMPROFILE.PROFILESTATUSID) PROFILESTATUSID_STR, " +
    "to_char(SOURCEFIRMPROFILE.SYSTEMTYPEID) SYSTEMTYPEID_STR, " +
    "to_char(ADDR.PROVINCEID) PROVINCEID_STR, " +
    //---------------- FXLink Phase II ------------------//
    //--> Add new field ==> Firm System Code
    //--> By Billy 14Nov2003
    "SOURCEFIRMPROFILE.SOURCEFIRMCODE, " +
    //=====================================================
    //--DJ_PREFCOMMETHOD_CR--start--//
    "to_char(CONTACT.PREFERREDDELIVERYMETHODID) PREFERREDDELIVERYMETHODID_STR, " +
    "CONTACT.PREFERREDDELIVERYMETHODID,CONTACT.INSTITUTIONPROFILEID " +
    //--DJ_PREFCOMMETHOD_CR--end--//
    "FROM " +
    "CONTACT, ADDR, SOURCEFIRMPROFILE, SOURCEFIRMTOUSERASSOC, SOURCEFIRMTOGROUPASSOC, " +
    //=========================================================
    //--DJ_PREFCOMMETHOD_CR--start--//
    "PREFERREDDELIVERYMETHOD " +
    //--DJ_PREFCOMMETHOD_CR--end--//
    " __WHERE__  " +
    "ORDER BY SOURCEFIRMPROFILE.SOURCEFIRMNAME  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, ADDR, SOURCEFIRMPROFILE, " +
                                                        "SOURCEFIRMTOUSERASSOC, " +
                                                        //--DJ_PREFCOMMETHOD_CR--start--//
                                                        "PREFERREDDELIVERYMETHOD ";
                                                        //--DJ_PREFCOMMETHOD_CR--end--//
	//--> The following were missed in Multi-Lingual conversion
  //--> By Billy 14Nov2003
  public static final String STATIC_WHERE_CRITERIA=
    " SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID = SOURCEFIRMTOUSERASSOC.SOURCEFIRMPROFILEID(+) AND " +
    " SOURCEFIRMPROFILE.INSTITUTIONPROFILEID = SOURCEFIRMTOUSERASSOC.INSTITUTIONPROFILEID(+) AND " +
    " SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID = SOURCEFIRMTOGROUPASSOC.SOURCEFIRMPROFILEID(+) AND " +
    " SOURCEFIRMPROFILE.INSTITUTIONPROFILEID = SOURCEFIRMTOGROUPASSOC.INSTITUTIONPROFILEID(+) AND " +
    " SOURCEFIRMPROFILE.CONTACTID  =  CONTACT.CONTACTID (+) AND " +
    " SOURCEFIRMPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID (+) AND " +
    " CONTACT.ADDRID = ADDR.ADDRID (+) AND " +
    " CONTACT.INSTITUTIONPROFILEID = ADDR.INSTITUTIONPROFILEID (+) AND " +
    //--DJ_PREFCOMMETHOD_CR--start--//
    " CONTACT.PREFERREDDELIVERYMETHODID  =  PREFERREDDELIVERYMETHOD.PREFERREDDELIVERYMETHODID ";
    //--DJ_PREFCOMMETHOD_CR--end--//

    //" ADDR.PROVINCEID  =  PROVINCE.PROVINCEID (+) AND " +
    //" SOURCEFIRMPROFILE.SYSTEMTYPEID  =  SYSTEMTYPE.SYSTEMTYPEID (+) AND " +
    //" SOURCEFIRMPROFILE.PROFILESTATUSID  =  PROFILESTATUS.PROFILESTATUSID (+)  ";
  //=========================================================
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFSBPFIRSTNAME="CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_DFSBPFIRSTNAME="CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFSBPMIDINIT="CONTACT.CONTACTMIDDLEINITIAL";
	public static final String COLUMN_DFSBPMIDINIT="CONTACTMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFSBPLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFSBPLASTNAME="CONTACTLASTNAME";
	public static final String QUALIFIED_COLUMN_DFSBPPHONE="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFSBPPHONE="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFSBPFAX="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFSBPFAX="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFSBPEMAIL="CONTACT.CONTACTEMAILADDRESS";
	public static final String COLUMN_DFSBPEMAIL="CONTACTEMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFSBPCITY="ADDR.CITY";
	public static final String COLUMN_DFSBPCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFSBPPROVINCEID="ADDR.PROVINCEID";
	public static final String COLUMN_DFSBPPROVINCEID="PROVINCEID";
	public static final String QUALIFIED_COLUMN_DFSBP_FSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFSBP_FSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFSBP_LDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFSBP_LDU="POSTALLDU";
	public static final String QUALIFIED_COLUMN_DFSBPADDRLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFSBPADDRLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFSBPADDRLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFSBPADDRLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFSBPPHONEEXT="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFSBPPHONEEXT="CONTACTPHONENUMBEREXTENSION";
	public static final String QUALIFIED_COLUMN_DFSBPSHORTNAME="SOURCEFIRMPROFILE.SFSHORTNAME";
	public static final String COLUMN_DFSBPSHORTNAME="SFSHORTNAME";
	public static final String QUALIFIED_COLUMN_DFSBPSTATUSID="SOURCEFIRMPROFILE.PROFILESTATUSID";
	public static final String COLUMN_DFSBPSTATUSID="PROFILESTATUSID";
	public static final String QUALIFIED_COLUMN_DFSBPSYSTEMTYPEID="SOURCEFIRMPROFILE.SYSTEMTYPEID";
	public static final String COLUMN_DFSBPSYSTEMTYPEID="SYSTEMTYPEID";
	public static final String QUALIFIED_COLUMN_DFSBPCLIENTID="SOURCEFIRMPROFILE.SFBUSINESSID";
	public static final String COLUMN_DFSBPCLIENTID="SFBUSINESSID";
	public static final String QUALIFIED_COLUMN_DFSBPALTERNATIVEID="SOURCEFIRMPROFILE.ALTERNATIVEID";
	public static final String COLUMN_DFSBPALTERNATIVEID="ALTERNATIVEID";
	public static final String QUALIFIED_COLUMN_DFSBPFIRMNAME="SOURCEFIRMPROFILE.SOURCEFIRMNAME";
	public static final String COLUMN_DFSBPFIRMNAME="SOURCEFIRMNAME";
	public static final String QUALIFIED_COLUMN_DFLICENSENUMBER="SOURCEFIRMPROFILE.SFLICENSEREGISTRATIONNUMBER";
	public static final String COLUMN_DFLICENSENUMBER="SFLICENSEREGISTRATIONNUMBER";
	public static final String QUALIFIED_COLUMN_DFSBPFIRMPROFILEID="SOURCEFIRMPROFILE.SOURCEFIRMPROFILEID";
	public static final String COLUMN_DFSBPFIRMPROFILEID="SOURCEFIRMPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSBPUSERPROFILEID="SOURCEFIRMTOUSERASSOC.USERPROFILEID";
	public static final String COLUMN_DFSBPUSERPROFILEID="USERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSBPUSEREFFECTIVEDATE="SOURCEFIRMTOUSERASSOC.EFFECTIVEDATE";
	public static final String COLUMN_DFSBPUSEREFFECTIVEDATE="EFFECTIVEDATE";
	//=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Added new fields to handle SourceFirm to Group association
  //--> By Billy 19Aug2003
  public static final String QUALIFIED_COLUMN_DFSBPGROUPPROFILEID="SOURCEFIRMTOGROUPASSOC.GROUPPROFILEID";
	public static final String COLUMN_DFSBPGROUPPROFILEID="GROUPPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSBPGROUPEFFECTIVEDATE="SOURCEFIRMTOGROUPASSOC.EFFECTIVEDATE";
	public static final String COLUMN_DFSBPGROUPEFFECTIVEDATE="EFFECTIVEDATE_GROUP";

  //--> The following were missed in Multi-Lingual conversion
  //--> By Billy 14Nov2003
  public static final String QUALIFIED_COLUMN_DFSFPSTATUSDESC="SOURCEFIRMTOGROUPASSOC.PROFILESTATUSID_STR";
	public static final String COLUMN_DFSFPSTATUSDESC="PROFILESTATUSID_STR";
	public static final String QUALIFIED_COLUMN_DFSFPSYSTEMTYPEDESC="SOURCEFIRMPROFILE.SYSTEMTYPEID_STR";
	public static final String COLUMN_DFSFPSYSTEMTYPEDESC="SYSTEMTYPEID_STR";
	public static final String QUALIFIED_COLUMN_DFSFPPROVINCENAME="ADDR.PROVINCEID_STR";
	public static final String COLUMN_DFSFPPROVINCENAME="PROVINCEID_STR";
  //=========================================================

  //---------------- FXLink Phase II ------------------//
  //--> Add new field ==> Firm System Code
  //--> By Billy 14Nov2003
  public static final String QUALIFIED_COLUMN_DFSFPSYSTEMCODE="SOURCEFIRMPROFILE.SOURCEFIRMCODE";
	public static final String COLUMN_DFSFPSYSTEMCODE="SOURCEFIRMCODE";
  //====================================================

  //--DJ_PREFCOMMETHOD_CR--start--//
	public static final String QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHOD="PREFERREDDELIVERYMETHOD.PREFERREDDELIVERYMETHODID_STR";
	public static final String COLUMN_DFPREFERREDDELIVERYMETHOD="PREFERREDDELIVERYMETHODID_STR";

  public static final String QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHODID="CONTACT.PREFERREDDELIVERYMETHODID";
	public static final String COLUMN_DFPREFERREDDELIVERYMETHODID="PREFERREDDELIVERYMETHODID";
  //--DJ_PREFCOMMETHOD_CR--end--//
	  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
	      "CONTACT.INSTITUTIONPROFILEID";
	public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPFIRSTNAME,
				COLUMN_DFSBPFIRSTNAME,
				QUALIFIED_COLUMN_DFSBPFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPMIDINIT,
				COLUMN_DFSBPMIDINIT,
				QUALIFIED_COLUMN_DFSBPMIDINIT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPLASTNAME,
				COLUMN_DFSBPLASTNAME,
				QUALIFIED_COLUMN_DFSBPLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPHONE,
				COLUMN_DFSBPPHONE,
				QUALIFIED_COLUMN_DFSBPPHONE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPFAX,
				COLUMN_DFSBPFAX,
				QUALIFIED_COLUMN_DFSBPFAX,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPEMAIL,
				COLUMN_DFSBPEMAIL,
				QUALIFIED_COLUMN_DFSBPEMAIL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPCITY,
				COLUMN_DFSBPCITY,
				QUALIFIED_COLUMN_DFSBPCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPROVINCEID,
				COLUMN_DFSBPPROVINCEID,
				QUALIFIED_COLUMN_DFSBPPROVINCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBP_FSA,
				COLUMN_DFSBP_FSA,
				QUALIFIED_COLUMN_DFSBP_FSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBP_LDU,
				COLUMN_DFSBP_LDU,
				QUALIFIED_COLUMN_DFSBP_LDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPADDRLINE1,
				COLUMN_DFSBPADDRLINE1,
				QUALIFIED_COLUMN_DFSBPADDRLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPADDRLINE2,
				COLUMN_DFSBPADDRLINE2,
				QUALIFIED_COLUMN_DFSBPADDRLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPPHONEEXT,
				COLUMN_DFSBPPHONEEXT,
				QUALIFIED_COLUMN_DFSBPPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSHORTNAME,
				COLUMN_DFSBPSHORTNAME,
				QUALIFIED_COLUMN_DFSBPSHORTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSTATUSID,
				COLUMN_DFSBPSTATUSID,
				QUALIFIED_COLUMN_DFSBPSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPSYSTEMTYPEID,
				COLUMN_DFSBPSYSTEMTYPEID,
				QUALIFIED_COLUMN_DFSBPSYSTEMTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPCLIENTID,
				COLUMN_DFSBPCLIENTID,
				QUALIFIED_COLUMN_DFSBPCLIENTID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPALTERNATIVEID,
				COLUMN_DFSBPALTERNATIVEID,
				QUALIFIED_COLUMN_DFSBPALTERNATIVEID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPFIRMNAME,
				COLUMN_DFSBPFIRMNAME,
				QUALIFIED_COLUMN_DFSBPFIRMNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLICENSENUMBER,
				COLUMN_DFLICENSENUMBER,
				QUALIFIED_COLUMN_DFLICENSENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPFIRMPROFILEID,
				COLUMN_DFSBPFIRMPROFILEID,
				QUALIFIED_COLUMN_DFSBPFIRMPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPUSERPROFILEID,
				COLUMN_DFSBPUSERPROFILEID,
				QUALIFIED_COLUMN_DFSBPUSERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPUSEREFFECTIVEDATE,
				COLUMN_DFSBPUSEREFFECTIVEDATE,
				QUALIFIED_COLUMN_DFSBPUSEREFFECTIVEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSFPSTATUSDESC,
				COLUMN_DFSFPSTATUSDESC,
				QUALIFIED_COLUMN_DFSFPSTATUSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSFPSYSTEMTYPEDESC,
				COLUMN_DFSFPSYSTEMTYPEDESC,
				QUALIFIED_COLUMN_DFSFPSYSTEMTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSFPPROVINCENAME,
				COLUMN_DFSFPPROVINCENAME,
				QUALIFIED_COLUMN_DFSFPPROVINCENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //=================================================
    //--> CRF#37 :: Source/Firm to Group routing for TD
    //=================================================
    //--> Added new fields to handle SourceFirm to Group association
    //--> By Billy 19Aug2003
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPGROUPPROFILEID,
				COLUMN_DFSBPGROUPPROFILEID,
				QUALIFIED_COLUMN_DFSBPGROUPPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSBPGROUPEFFECTIVEDATE,
				COLUMN_DFSBPGROUPEFFECTIVEDATE,
				QUALIFIED_COLUMN_DFSBPUSEREFFECTIVEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //---------------- FXLink Phase II ------------------//
    //--> Add new field ==> Firm System Code
    //--> By Billy 14Nov2003
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSFPSYSTEMCODE,
				COLUMN_DFSFPSYSTEMCODE,
				QUALIFIED_COLUMN_DFSFPSYSTEMCODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //=====================================================
    //--DJ_PREFCOMMETHOD_CR--start--//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREFERREDDELIVERYMETHOD,
				COLUMN_DFPREFERREDDELIVERYMETHOD,
				QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHOD,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREFERREDDELIVERYMETHODID,
				COLUMN_DFPREFERREDDELIVERYMETHODID,
				QUALIFIED_COLUMN_DFPREFERREDDELIVERYMETHODID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
    //--DJ_PREFCOMMETHOD_CR--end--//
	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	}
}

