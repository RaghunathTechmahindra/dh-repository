package mosApp.MosSystem;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *  <p>Title: doQualifyingDetailsModelImpl</p> 
 *  <p>Description: Implementation of doQualifyingDetailsModel Interface 
 *  </p>
 *
 * @author MCM Impl team
 * @version 1.0 06-JUN-2008 XS_2.13 Initial Version 
 * 
 */
public class doQualifyingDetailsModelImpl extends QueryModelBase
	implements doQualifyingDetailsModel
{
	/**
	 * Description: This constructor initialises an instance of this model, setting basic attributes
	 * @version 1.0 06-JUN-2008 XS_2.13 Initial version
	 */
	public doQualifyingDetailsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}
	/**
	  * Description: This method returns the sql statement as a string
	 * @param context  ModelExecutionContext
	 * @param queryType  int
	 * @param sql String
	 * @return String
	 * @version 1.0 06-JUN-2008 XS_2.13 Initial version
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{
		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 * Description: This method is called after query execution.
	 *    
	 * @param context  ModelExecutionContext
	 * @param queryType int
	 * @version 1.0 06-JUN-2008 XS_2.13 Initial version
	 * @return void
	 * @exception ModelControlException
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
		
		String defaultInstanceStateName = getRequestContext().getModelManager()
		.getDefaultModelInstanceName(SessionStateModel.class);

		SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
		.getModelManager().getModel(SessionStateModel.class,
				defaultInstanceStateName, true);

		int languageId = theSessionState.getLanguageId();

		//Get institution id
		int institutionId = theSessionState.getDealInstitutionId();

		//Populate Repayment type description from pick list properties based on institution and language
		if (getDfRepaymentTypeId() != null) {
			this.setDfRepaymentTypeId(BXResources.getPickListDescription(
					institutionId, "REPAYMENTTYPE",
					this.getDfRepaymentTypeId(), languageId));
		}

		//Populate Compound period months from pick list properties based on institution and language
		if (getDfInterestCompoundingId() != null) {
			this.setDfInterestCompoundingId(BXResources.getPickListDescription(
					institutionId, "COMPOUNDPERIOD", this
					.getDfInterestCompoundingId(), languageId));
		}

	}


	public static final String DATA_SOURCE_NAME="jdbc/orcl";


 	public static final String SELECT_SQL_TEMPLATE="SELECT QUALIFYDETAIL.INSTITUTIONPROFILEID, QUALIFYDETAIL.DEALID, " +
 			"QUALIFYDETAIL.QUALIFYRATE, to_char(QUALIFYDETAIL.INTERESTCOMPOUNDINGID)INTERESTCOMPOUNDINGID_STR, QUALIFYDETAIL.AMORTIZATIONTERM, " + 
			"to_char(QUALIFYDETAIL.REPAYMENTTYPEID) REPAYMENTTYPEID_STR, QUALIFYDETAIL.PANDIPAYMENTAMOUNTQUALIFY,  " +
 			"QUALIFYDETAIL.QUALIFYGDS, QUALIFYDETAIL.QUALIFYTDS " +
 			"FROM QUALIFYDETAIL __WHERE__  ";

 	public static final String MODIFYING_QUERY_TABLE_NAME=" QUALIFYDETAIL ";
 	
 	public static final String STATIC_WHERE_CRITERIA=" ";
 	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_FIELD_DFINSTITUTIONPROFILEID="QUALIFYDETAIL.INSTITUTIONPROFILEID";
	public static final String COLUMN_DFINSTITUTIONPROFILEID="INSTITUTIONPROFILEID";
	
	public static final String QUALIFIED_COLUMN_DFDEALID="QUALIFYDETAIL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
			
	public static final String QUALIFIED_COLUMN_DFQUALIFYRATE="QUALIFYDETAIL.QUALIFYRATE";
	public static final String COLUMN_DFQUALIFYRATE="QUALIFYRATE";
	
	public static final String QUALIFIED_COLUMN_DFINTERESTCOMPOUNDINGID="QUALIFYDETAIL.INTERESTCOMPOUNDINGID_STR";
	public static final String COLUMN_DFINTERESTCOMPOUNDINGID="INTERESTCOMPOUNDINGID_STR";
	
	public static final String QUALIFIED_COLUMN_DFAMORTIZATIONTERM="QUALIFYDETAIL.AMORTIZATIONTERM";
	public static final String COLUMN_DFAMORTIZATIONTERM="AMORTIZATIONTERM";

	public static final String QUALIFIED_COLUMN_DFREPAYMENTTYPEID="QUALIFYDETAIL.REPAYMENTTYPEID_STR";
	public static final String COLUMN_DFREPAYMENTTYPEID="REPAYMENTTYPEID_STR";
	
	public static final String QUALIFIED_COLUMN_DFPANDIPAYMENTAMOUNTQUALIFY="QUALIFYDETAIL.PANDIPAYMENTAMOUNTQUALIFY";
	public static final String COLUMN_DFPANDIPAYMENTAMOUNTQUALIFY="PANDIPAYMENTAMOUNTQUALIFY";
	
	public static final String QUALIFIED_COLUMN_DFQUALIFYGDS="QUALIFYDETAIL.QUALIFYGDS";
	public static final String COLUMN_DFQUALIFYGDS="QUALIFYGDS";
	
	public static final String QUALIFIED_COLUMN_DFQUALIFYTDS="QUALIFYDETAIL.QUALIFYTDS";
	public static final String COLUMN_DFQUALIFYTDS="QUALIFYTDS";
	
	/**
	 * Description: This is static block for mapping the model varibles to the database columns.
	 * 
	 * @param context   ModelExecutionContext
	 * @param queryType int
	 * @param exception SQLException
	 * @return void
	 * @version 1.0 06-JUN-2008 XS_2.13 Initial version
	 */

	static
	{
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFDEALID,
						COLUMN_DFDEALID,
						QUALIFIED_COLUMN_DFDEALID,
						java.math.BigDecimal.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFINSTITUTIONPROFILEID,
						COLUMN_DFINSTITUTIONPROFILEID,
						QUALIFIED_COLUMN_FIELD_DFINSTITUTIONPROFILEID,
						java.math.BigDecimal.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFQUALIFYRATE,
						COLUMN_DFQUALIFYRATE,
						QUALIFIED_COLUMN_DFQUALIFYRATE,
						java.math.BigDecimal.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFINTERESTCOMPOUNDINGID,
						COLUMN_DFINTERESTCOMPOUNDINGID,
						QUALIFIED_COLUMN_DFINTERESTCOMPOUNDINGID,
						String.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFAMORTIZATIONTERM,
						COLUMN_DFAMORTIZATIONTERM,
						QUALIFIED_COLUMN_DFAMORTIZATIONTERM,
						java.math.BigDecimal.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFREPAYMENTTYPEID,
						COLUMN_DFREPAYMENTTYPEID,
						QUALIFIED_COLUMN_DFREPAYMENTTYPEID,
						String.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFPANDIPAYMENTAMOUNTQUALIFY,
						COLUMN_DFPANDIPAYMENTAMOUNTQUALIFY,
						QUALIFIED_COLUMN_DFPANDIPAYMENTAMOUNTQUALIFY,
						java.math.BigDecimal.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFQUALIFYGDS,
						COLUMN_DFQUALIFYGDS,
						QUALIFIED_COLUMN_DFQUALIFYGDS,
						java.math.BigDecimal.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
						FIELD_DFQUALIFYTDS,
						COLUMN_DFQUALIFYTDS,
						QUALIFIED_COLUMN_DFQUALIFYTDS,
						java.math.BigDecimal.class,
						false,
						false,
						QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
						"",
						QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	  

	}
	
	/**
	 * Getters and setters for the fields 
	 */
	
	public java.math.BigDecimal getDfQualifyRate()
	{
		return (java.math.BigDecimal) getValue(FIELD_DFQUALIFYRATE);
	}

	public void setDfQualifyRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFQUALIFYRATE, value);
	}
	public String getDfInterestCompoundingId()
	{
		return (String)getValue(FIELD_DFINTERESTCOMPOUNDINGID);
	}

	public void setDfInterestCompoundingId(String value)
	{
		setValue(FIELD_DFINTERESTCOMPOUNDINGID,value);
	}

	public java.math.BigDecimal getDfAmortizationTerm()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAMORTIZATIONTERM);
	}

	public void setDfAmortizationTerm(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAMORTIZATIONTERM,value);
	}

	public String getDfRepaymentTypeId()
	{
		return (String)getValue(FIELD_DFREPAYMENTTYPEID);
	}

	public void setDfRepaymentTypeId(String value)
	{
		setValue(FIELD_DFREPAYMENTTYPEID,value);
	}

	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}

	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}

	public java.math.BigDecimal getDfInstitutionProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINSTITUTIONPROFILEID);
	}

	public void setDfInstitutionProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINSTITUTIONPROFILEID,value);
	}

	public java.math.BigDecimal getDfQualifyGds() {
		return ((java.math.BigDecimal) getValue(FIELD_DFQUALIFYGDS));
	}

	public void setDfQualifyGds(java.math.BigDecimal value) {
		setValue(FIELD_DFQUALIFYGDS,value);
	}

	public java.math.BigDecimal getDfPAndIPaymentAmountQualify() {
		return (java.math.BigDecimal) getValue(FIELD_DFPANDIPAYMENTAMOUNTQUALIFY);
	}
	public void setDfPAndIPaymentAmountQualify(java.math.BigDecimal value) {
		setValue(FIELD_DFPANDIPAYMENTAMOUNTQUALIFY,value);
	}

	public java.math.BigDecimal getDfQualifyTds() {
		return (java.math.BigDecimal) getValue(FIELD_DFQUALIFYTDS);
	}
	public void setDfQualifyTds(java.math.BigDecimal value) {
		setValue(FIELD_DFQUALIFYTDS,value);
	}
		
}

