package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doPropertyEntryPropertySelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetName();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetName(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfStreetTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyStreetDirectionId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetDirectionId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUnitNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfUnitNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfCity(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyProvinceId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyProvinceId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLegalLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfLegalLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLegalLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfLegalLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLegalLine3();

	
	/**
	 * 
	 * 
	 */
	public void setDfLegalLine3(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyDwellingTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyDwellingTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDwellilngStyleId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDwellilngStyleId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfStructureAge();

	
	/**
	 * 
	 * 
	 */
	public void setDfStructureAge(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNoOfUnits();

	
	/**
	 * 
	 * 
	 */
	public void setDfNoOfUnits(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNumberOfBedRooms();

	
	/**
	 * 
	 * 
	 */
	public void setDfNumberOfBedRooms(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLivingSpace();

	
	/**
	 * 
	 * 
	 */
	public void setDfLivingSpace(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyLivingSpaceMeasureId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyLivingSpaceMeasureId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLotSize();

	
	/**
	 * 
	 * 
	 */
	public void setDfLotSize(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLotsizeDepth();

	
	/**
	 * 
	 * 
	 */
	public void setDfLotsizeDepth(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLotsizeFrontPage();

	
	/**
	 * 
	 * 
	 */
	public void setDfLotsizeFrontPage(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLotSizeUnitmeasuredId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLotSizeUnitmeasuredId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyHeatTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyHeatTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUFFIInsulation();

	
	/**
	 * 
	 * 
	 */
	public void setDfUFFIInsulation(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyWaterTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyWaterTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertySwegeTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertySwegeTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyUsageId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyUsageId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyOccupancyTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyOccupancyTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyTypeId(java.math.BigDecimal value);

//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfRequestAppraisalId();

	
	/**
	 * 
	 * 
	 */
	public void setDfRequestAppraisalId(java.math.BigDecimal value);
	
//	***** Change by NBC Impl. Team - Version 1.2 - End *****//
	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyLocationId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyLocationId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfZoning();

	
	/**
	 * 
	 * 
	 */
	public void setDfZoning(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLandValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfLandValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEquityAvailable();

	
	/**
	 * 
	 * 
	 */
	public void setDfEquityAvailable(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEstimateValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfEstimateValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLTV();

	
	/**
	 * 
	 * 
	 */
	public void setDfLTV(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalPropertyExp();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalPropertyExp(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMLSListingFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfMLSListingFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfGarageSizeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfGarageSizeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfGarageTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfGarageTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBuilderName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBuilderName(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNewConstructionId();

	
	/**
	 * 
	 * 
	 */
	public void setDfNewConstructionId(java.math.BigDecimal value);
	
	
	//Begin MI additions
	public String getDfSubdivisionDiscount();
	
	public void setDfSubdivisionDiscount(String value);
	
	public java.math.BigDecimal getDfTenureTypeId();

	public void setDfTenureTypeId(java.math.BigDecimal value);
	
//	public java.math.BigDecimal getDfOnReserveTrustAgreementNumber();
//	public void setDfOnReserveTrustAgreementNumber(java.math.BigDecimal value);
	
	public String getDfOnReserveTrustAgreementNumber();
	public void setDfOnReserveTrustAgreementNumber(String value);
	
	public String getDfMIEnergyEfficiency();
    public void setDfMIEnergyEfficiency(String value);
    
    public String getDfMortgageInsurerId();
    public void setDfMortgageInsurerId(String value);
	//End MI additions
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFPROPERTYID="dfPropertyId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFSTREETNUMBER="dfStreetNumber";
	public static final String FIELD_DFSTREETNAME="dfStreetName";
	public static final String FIELD_DFSTREETTYPEID="dfStreetTypeId";
	public static final String FIELD_DFPROPERTYSTREETDIRECTIONID="dfPropertyStreetDirectionId";
	public static final String FIELD_DFUNITNUMBER="dfUnitNumber";
	public static final String FIELD_DFADDRESSLINE2="dfAddressLine2";
	public static final String FIELD_DFCITY="dfCity";
	public static final String FIELD_DFPROPERTYPROVINCEID="dfPropertyProvinceId";
	public static final String FIELD_DFPOSTALFSA="dfPostalFSA";
	public static final String FIELD_DFPOSTALLDU="dfPostalLDU";
	public static final String FIELD_DFLEGALLINE1="dfLegalLine1";
	public static final String FIELD_DFLEGALLINE2="dfLegalLine2";
	public static final String FIELD_DFLEGALLINE3="dfLegalLine3";
	public static final String FIELD_DFPROPERTYDWELLINGTYPEID="dfPropertyDwellingTypeId";
	public static final String FIELD_DFDWELLILNGSTYLEID="dfDwellilngStyleId";
	public static final String FIELD_DFSTRUCTUREAGE="dfStructureAge";
	public static final String FIELD_DFNOOFUNITS="dfNoOfUnits";
	public static final String FIELD_DFNUMBEROFBEDROOMS="dfNumberOfBedRooms";
	public static final String FIELD_DFLIVINGSPACE="dfLivingSpace";
	public static final String FIELD_DFPROPERTYLIVINGSPACEMEASUREID="dfPropertyLivingSpaceMeasureId";
	public static final String FIELD_DFLOTSIZE="dfLotSize";
	public static final String FIELD_DFLOTSIZEDEPTH="dfLotsizeDepth";
	public static final String FIELD_DFLOTSIZEFRONTPAGE="dfLotsizeFrontPage";
	public static final String FIELD_DFLOTSIZEUNITMEASUREDID="dfLotSizeUnitmeasuredId";
	public static final String FIELD_DFPROPERTYHEATTYPEID="dfPropertyHeatTypeId";
	public static final String FIELD_DFUFFIINSULATION="dfUFFIInsulation";
	public static final String FIELD_DFPROPERTYWATERTYPEID="dfPropertyWaterTypeId";
	public static final String FIELD_DFPROPERTYSWEGETYPEID="dfPropertySwegeTypeId";
	public static final String FIELD_DFPROPERTYUSAGEID="dfPropertyUsageId";
	public static final String FIELD_DFPROPERTYOCCUPANCYTYPEID="dfPropertyOccupancyTypeId";
	public static final String FIELD_DFPROPERTYTYPEID="dfPropertyTypeId";
//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	public static final String FIELD_DFREQUESTAPPRAISALID="dfRequestAppraisalId";
//	***** Change by NBC Impl. Team - Version 1.2 - End *****//	
	public static final String FIELD_DFPROPERTYLOCATIONID="dfPropertyLocationId";
	public static final String FIELD_DFZONING="dfZoning";
	public static final String FIELD_DFPURCHASEPRICE="dfPurchasePrice";
	public static final String FIELD_DFLANDVALUE="dfLandValue";
	public static final String FIELD_DFEQUITYAVAILABLE="dfEquityAvailable";
	public static final String FIELD_DFESTIMATEVALUE="dfEstimateValue";
	public static final String FIELD_DFLTV="dfLTV";
	public static final String FIELD_DFTOTALPROPERTYEXP="dfTotalPropertyExp";
	public static final String FIELD_DFMLSLISTINGFLAG="dfMLSListingFlag";
	public static final String FIELD_DFGARAGESIZEID="dfGarageSizeId";
	public static final String FIELD_DFGARAGETYPEID="dfGarageTypeId";
	public static final String FIELD_DFBUILDERNAME="dfBuilderName";
	public static final String FIELD_DFNEWCONSTRUCTIONID="dfNewConstructionId";
	
	
	//MI addition
	public static final String FIELD_DFSUBDIVISIONDISCOUNT="dfSubdivisionDiscount";
	public static final String FIELD_DFTENURETYPEID="dfTenureTypeId";
	public static final String FIELD_DFONRESERVETRUSTAGREEMENTNUMBER = "dfOnReserveTrustAgreementNumber";
	public static final String FIELD_DFMIENERGYEFFICIENCY = "dfMIEnergyEfficiency";
	public static final String FIELD_DFMORTGAGEINSURERID = "dfMortgageInsurerId";
	//End MI addition
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

