package mosApp.MosSystem;

import java.io.IOException;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>
 * Title: pgDisclosureNLViewBean
 * <p>
 * Description: View bean for Disclosure NL PAGE
 * <p>
 * Company: Filogix Inc. <br>
 * Date: 11/28/2006
 * @author NBC Implementation Team
 * @version 1.0
 */
public class pgDisclosureNLViewBean extends pgDisclosureViewBean implements
	ViewBean
{
  /**
    * pgDisclosureNLViewBean <br>
    * Constructor for class pgDisclosureNLViewBean. <br>
    * Date: 11/28/2006
    */
  public pgDisclosureNLViewBean()
  {
	super(PAGE_NAME);
	setDefaultDisplayURL(DISPLAY_URL);
	setDefaultDisplayUrl(DISPLAY_URL);
	registerChildren();
  }
 
  /**
   * initialize <br>
   * Method used for initialization <br>
   * Creation Date: 11/28/2006
   */
  protected void initialize()
  {
  }
  
  // //////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  // //////////////////////////////////////////////////////////////////////////////
  
  /**
   * createChild <br>
   * This method covers creating form elements specific to NL. The method calls
   * createChild() of the parent base view bean to load the base elements
   * common to the page <br>
   * Creation Date: 11/28/2006
   * @param name
   *            The name of the child form element to instantiate.
   * @return The named child view instance
   * @throws java.lang.IllegalArgumentException
   */
  protected View createChild(String name) throws IllegalArgumentException
  {
	View childFromParent = super.createChild(name);

	if (childFromParent != null)
	{
	  return childFromParent;
	}
	if (name.equals(CHILD_TXANNPERCRATE_NL))
	{
	  TextField child = new TextField(this, getDefaultModel(),
		  CHILD_TXANNPERCRATE_NL, CHILD_TXANNPERCRATE_NL,
		  CHILD_TXANNPERCRATE_RESET_VALUE_NL, null);
	  return child;
	}
	else
	  if (name.equals(CHILD_TXPAYVAR))
	  {
		TextField child = new TextField(this, getDefaultModel(),
			CHILD_TXPAYVAR, CHILD_TXPAYVAR, CHILD_TXPAYVAR_RESET_VALUE, null);
		return child;
	  }
	  else
		if (name.equals(CHILD_TXARIGHTREPAY))
		{
		  TextField child = new TextField(this, getDefaultModel(),
			  CHILD_TXARIGHTREPAY, CHILD_TXARIGHTREPAY,
			  CHILD_TXARIGHTREPAY_RESET_VALUE, null);
		  return child;
		}
		else
		{
		  throw new IllegalArgumentException("Invalid child name [" + name
			  + "]");
		}
  }

  /**
   * resetChildren <br>
   * This method covers reseting form element values specific to NL The method
   * also calls resetChildren() from the parent base view bean to reset the
   * base elements common to the page <br>
   * Creation Date: 11/28/2006
   */
  public void resetChildren()
  {
	super.resetChildren();
	getTxAnnualPercentNL().setValue(CHILD_TXANNPERCRATE_RESET_VALUE_NL);
	getTxPaymentVar().setValue(CHILD_TXPAYVAR_RESET_VALUE);
	getTxRightRepay().setValue(CHILD_TXARIGHTREPAY_RESET_VALUE);
  }

  /**
   * registerChildren <br>
   * This method covers regestering form elements specific to NL The method
   * calls registerChildren() from the parent base view bean to register the
   * base elements common to the page <br>
   * Creation Date: 11/28/2006
   */
  protected void registerChildren()
  {
	super.registerChildren();
	registerChild(CHILD_TXANNPERCRATE_NL, TextField.class);
	registerChild(CHILD_TXPAYVAR, TextField.class);
	registerChild(CHILD_TXARIGHTREPAY, TextField.class);
  }

  /**
   * beginDisplay <br>
   * This method renders the jsp <br>
   * Creation Date: 11/28/2006
   * @Param event
   *            The DisplayEvent.
   */
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
	if (getdoDisclosureSelectModel() == null)
	  throw new ModelControlException("Primary model is null");

	DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
	handler.pageGetState(this.getParentViewBean());
	handler.overridePageLabel(62);
	super.beginDisplay(event);
  }

  /**
   * beforeModelExecutes <br>
   * This method executes any application logic required before rendering the jsp <br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that is about to be auto-executed
   * @Param executionContext
   *            The context under which this model is being executed
   * @return True if the execution of the model should proceed, false to skip
   *         execution of the model
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.pageGetState(this);
	handler.setupBeforePageGeneration();
	handler.pageSaveState();
	return super.beforeModelExecutes(model, executionContext);
  }

  /**
   * afterModelExecutes <br>
   * This method executes any application logic required after rendering the
   * jsp <br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that is about to be auto-executed
   * @Param executionContext
   *            The context under which this model is being executed
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
	super.afterModelExecutes(model, executionContext);
  }

  /**
   * afterModelExecutes <br>
   * This method executes after all models have finished execution <br>
   * Creation Date: 11/28/2006
   * @Param executionContext
   *            The context under which this model is being executed
   */
  public void afterAllModelsExecute(int executionContext)
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.pageGetState(this);
	handler.populatePageDisplayFields();
	handler.pageSaveState();
	super.afterAllModelsExecute(executionContext);
  }

  /**
   * onModelError <br>
   * Invoked as notification that a given model threw a <code>
   * ModelControlException</code>
   * during auto-execution.<br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that which caused an execution error
   * @Param executionContext
   *            The context under which this model is being executed
   * @param exception
   *            The exception thrown during auto-execution
   */
  public void onModelError(Model model, int executionContext,
	  ModelControlException exception) throws ModelControlException
  {
	super.onModelError(model, executionContext, exception);
  }

  /**
   * handleBtProceedRequest <br>
   * This method handles the go button click <br>
   * Creation Date: 11/28/2006
   * @Param event
   *            The RequestInvocationEvent.
   */
  public void handleBtProceedRequest(RequestInvocationEvent event)
	  throws ServletException, IOException
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.preHandlerProtocol(this);
	handler.handleGoPage();
	handler.postHandlerProtocol();
  }

  /**
   * getDisplayURL <br>
   * This method overrides the getDefaultURL() framework JATO method and it is
   * located in each ViewBean. This allows not to stay with the JATO ViewBean
   * extension, otherwise each ViewBean a.k.a page should extend its Handler
   * (to be called by the framework) and it is not good. DEFAULT_DISPLAY_URL
   * String is a variable now. The full method is still in PHC base class. It
   * should care the the url="/" case (non-initialized defaultURL) by the BX
   * framework methods. <br>
   * Creation Date: 11/28/2006
   * @return Url for the jsp which is displayed
   */
  public String getDisplayURL()
  {
	DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
	handler.pageGetState(this);
	String url = getDefaultDisplayURL();
	int languageId = handler.theSessionState.getLanguageId();
	if (url != null && !url.trim().equals("") && !url.trim().equals("/"))
	{
	  url = BXResources.getBXUrl(url, languageId);
	}
	else
	{
	  url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
	}
	return url;
  }
  
  /**
   * getTxAnnualPercentNL <br>
   * Returns an reference of the "txAnnualPercentage" child element <br>
   * Creation Date: 11/28/2006
   * @return reference of the "txAnnualPercentage" text field
   */
  public TextField getTxAnnualPercentNL()
  {
	return (TextField) getChild(CHILD_TXANNPERCRATE_NL);
  }

  /**
   * getTxPaymentVar <br>
   * Returns an reference of the "txPaymentVar" child element <br>
   * Creation Date: 11/28/2006
   * @return reference of the "txPaymentVar" text field
   */
  public TextField getTxPaymentVar()
  {
	return (TextField) getChild(CHILD_TXPAYVAR);
  }

  /**
   * getTxRightRepay <br>
   * Returns an reference of the "txRightRepay" child element <br>
   * Creation Date: 11/28/2006
   * @return reference of the "txRightRepay" text field
   */
  public TextField getTxRightRepay()
  {
	return (TextField) getChild(CHILD_TXARIGHTREPAY);
  }

  // //////////////////////////////////////////////////////////////////////////////
  // Class variables
  // //////////////////////////////////////////////////////////////////////////////

  /**
   * Constant defining the page name
   * @value "pgDisclosureNL"
   */
  public static final String PAGE_NAME = "pgDisclosureNL";

  /**
   * Constant defining the question id for annual percentage rate
   * @value "103Q050U"
   */
  public static final String CHILD_TXANNPERCRATE_NL = "103Q050U";

  /**
   * Constant defining the reset value for annual percentage rate
   * @value ""
   */
  public static final String CHILD_TXANNPERCRATE_RESET_VALUE_NL = "";

  /**
   * Constant defining the question id for payment variations
   * @value "103Q080X"
   */
  public static final String CHILD_TXPAYVAR = "103Q080X";

  /**
   * Constant defining the reset value for payment variations
   * @value ""
   */
  public static final String CHILD_TXPAYVAR_RESET_VALUE = "";

  /**
   * Constant defining the question id for rights to repay
   * @value "103Q12AE"
   */
  public static final String CHILD_TXARIGHTREPAY = "103Q12AE";

  /**
   * Constant defining the reset value for rights to repay
   * @value ""
   */
  public static final String CHILD_TXARIGHTREPAY_RESET_VALUE = "";

  // //////////////////////////////////////////////////////////////////////////
  // Instance variables
  // //////////////////////////////////////////////////////////////////////////

  /**
   * Instance variable defining the page display URL
   * @value "/mosApp/MosSystem/pgDisclosureNL.jsp"
   */
  private String DISPLAY_URL = "/mosApp/MosSystem/pgDisclosureNL.jsp";
}
