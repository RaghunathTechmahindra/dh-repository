package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 * <p>
 * Title: pgIdentificationTypeRepeated1TiledView
 * </p>
 * 
 * <p>
 * Description: View class for borrower identification repeated tile
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 *  
 * @author NBC/PP Implementation Team
 * @version 1.0
 * 
 * @version 1.1
 * Date: 08/11/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Added new method nextTile() - Fix for defect 4215
 * 
 */
public class pgIdentificationTypeRepeated1TiledView extends
		RequestHandlingTiledViewBase implements TiledView, RequestHandler {

	public pgIdentificationTypeRepeated1TiledView(View parent, String name) {
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass(doIdentificationTypeModel.class);
		registerChildren();
		initialize();
	}

	protected void initialize() {
	}
//	***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	/**
	 * nextTile - Bring up multiple identifications for a borrower associated to a deal
	 * 
	 * @param None
	 * @return boolean : the result of nextTile() <br>
	 */

	public boolean nextTile() throws ModelControlException {

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow) {
			/*DealSummaryHandler handler = (DealSummaryHandler) this.handler
					.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			handler.setBorrowerIndentifications(this.getTileIndex());
			handler.pageSaveState();*/
		}
		return movedToRow;
	}
//	***** Change by NBC Impl. Team - Version 1.1 - End *****//
	/**
	 * resetChildren
	 * 
	 * @see com.iplanet.jato.view.ContainerView#resetChildren()
	 * @param None
	 *            <br>
	 * 
	 * @return void : the result of resetChildren <br>
	 */

	public void resetChildren() {
		getStIdentificationNumber().setValue(
				CHILD_STIDENTIFICATIONNUMBER_RESET_VALUE);
		getStIdentificationType().setValue(
				CHILD_STIDENTIFICATIONTYPE_RESET_VALUE);
		getStIdentificationSourceCountry().setValue(
				CHILD_STIDENTIFICATIONSOURCECOUNTRY_RESET_VALUE);

	}

	/**
	 * registerChildren
	 * 
	 * @param None
	 *            <br>
	 * 
	 * @return void : the result of registerChildren <br>
	 */
	protected void registerChildren() {
		registerChild(CHILD_STIDENTIFICATIONNUMBER, StaticTextField.class);
		registerChild(CHILD_STIDENTIFICATIONTYPE, StaticTextField.class);
		registerChild(CHILD_STIDENTIFICATIONSOURCECOUNTRY,
				StaticTextField.class);

	}

	// //////////////////////////////////////////////////////////////////////////////
	// Model management methods
	// //////////////////////////////////////////////////////////////////////////////

	public Model[] getWebActionModels(int executionType) {
		List modelList = new ArrayList();
		switch (executionType) {
		case MODEL_TYPE_RETRIEVE:
			modelList.add(getdoIdentificationTypeModel());
			;
			break;

		case MODEL_TYPE_UPDATE:
			;
			break;

		case MODEL_TYPE_DELETE:
			;
			break;

		case MODEL_TYPE_INSERT:
			;
			break;

		case MODEL_TYPE_EXECUTE:
			;
			break;
		}
		return (Model[]) modelList.toArray(new Model[0]);
	}

	// //////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	// //////////////////////////////////////////////////////////////////////////////

	public void beginDisplay(DisplayEvent event) throws ModelControlException {
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null)
			throw new ModelControlException("Primary model is null");

		DealSummaryHandler handler = (DealSummaryHandler) this.handler
				.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedApplicantIdentifications(this);
		handler.pageSaveState();
		super.beginDisplay(event);
		resetTileIndex();
	}

	public boolean beforeModelExecutes(Model model, int executionContext) {

		// This is the analog of NetDynamics
		// repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}

	public void afterModelExecutes(Model model, int executionContext) {

		// This is the analog of NetDynamics
		// repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}

	public void afterAllModelsExecute(int executionContext) {

		// This is the analog of NetDynamics
		// repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}

	public void onModelError(Model model, int executionContext,
			ModelControlException exception) throws ModelControlException {

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}

	// //////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	// //////////////////////////////////////////////////////////////////////////////
	/**
	 * createChild
	 * 
	 * @param String
	 *            :name of field <br>
	 * 
	 * @return View : the result of createChild <br>
	 */
	protected View createChild(String name) {

		if (name.equals(CHILD_STIDENTIFICATIONNUMBER)) {
			StaticTextField child = new StaticTextField(this,
					getdoIdentificationTypeModel(),
					CHILD_STIDENTIFICATIONNUMBER,
					doIdentificationTypeModel.FIELD_DFIDENTIFICATIONNUMBER,
					CHILD_STIDENTIFICATIONNUMBER_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_STIDENTIFICATIONTYPE)) {
			StaticTextField child = new StaticTextField(this,
					getdoIdentificationTypeModel(), CHILD_STIDENTIFICATIONTYPE,
					doIdentificationTypeModel.FIELD_DFIDENTIFICATIONTYPE,
					CHILD_STIDENTIFICATIONTYPE_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_STIDENTIFICATIONSOURCECOUNTRY)) {
			StaticTextField child = new StaticTextField(
					this,
					getdoIdentificationTypeModel(),
					CHILD_STIDENTIFICATIONSOURCECOUNTRY,
					doIdentificationTypeModel.FIELD_DFIDENTIFICATIONSOURCECOUNTRY,
					CHILD_STIDENTIFICATIONSOURCECOUNTRY_RESET_VALUE, null);
			return child;
		} else
			throw new IllegalArgumentException("Invalid child name [" + name
					+ "]");
	}

	/**
	 * getStIdentificationNumber
	 * 
	 * @param <br>
	 * 
	 * @return StaticTextField : the result of getStIdentificationNumber <br>
	 */
	public StaticTextField getStIdentificationNumber() {
		return (StaticTextField) getChild(CHILD_STIDENTIFICATIONNUMBER);
	}

	/**
	 * getStIdentificationType
	 * 
	 * @param <br>
	 * 
	 * @return StaticTextField : the result of getStIdentificationType <br>
	 */
	public StaticTextField getStIdentificationType() {
		return (StaticTextField) getChild(CHILD_STIDENTIFICATIONTYPE);
	}

	/**
	 * getStIdentificationSourceCountry
	 * 
	 * @param <br>
	 * 
	 * @return StaticTextField : the result of getStIdentificationSourceCountry
	 *         <br>
	 */
	public StaticTextField getStIdentificationSourceCountry() {
		return (StaticTextField) getChild(CHILD_STIDENTIFICATIONTYPE);
	}

	public doIdentificationTypeModel getdoIdentificationTypeModel() {
		if (doIdentificationType == null)
			doIdentificationType = (doIdentificationTypeModel) getModel(doIdentificationTypeModel.class);
		return doIdentificationType;
	}

	public void setdoIdentificationTypeModel(doIdentificationTypeModel impl) {
		// TODO Auto-generated method stub
		doIdentificationType = impl;

	}

	public void setCurrentDisplayOffset(int offset) throws Exception {
		setWebActionModelOffset(offset);
		handleWebAction(WebActions.ACTION_REFRESH);
		// Have to re-execute the Model here. Otherwise, not working
		executeAutoRetrievingModels();
	}

	// //////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	// //////////////////////////////////////////////////////////////////////////

	// //////////////////////////////////////////////////////////////////////////
	// Class variables
	// //////////////////////////////////////////////////////////////////////////

	// //////////////////////////////////////////////////////////////////////////////
	// Child accessors
	// //////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	// //////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	// //////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////
	// Class variables
	// //////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;

	public static final String CHILD_STIDENTIFICATIONNUMBER = "stIdentificationNumber";

	public static final String CHILD_STIDENTIFICATIONNUMBER_RESET_VALUE = "";

	public static final String CHILD_STIDENTIFICATIONTYPE = "stIdentificationType";

	public static final String CHILD_STIDENTIFICATIONTYPE_RESET_VALUE = "";

	public static final String CHILD_STIDENTIFICATIONSOURCECOUNTRY = "stIdentificationSourceCountry";

	public static final String CHILD_STIDENTIFICATIONSOURCECOUNTRY_RESET_VALUE = "";

	private doIdentificationTypeModel doIdentificationType = null;

	private DealSummaryHandler handler = new DealSummaryHandler();

	// //////////////////////////////////////////////////////////////////////////
	// Instance variables
	// //////////////////////////////////////////////////////////////////////////

}
