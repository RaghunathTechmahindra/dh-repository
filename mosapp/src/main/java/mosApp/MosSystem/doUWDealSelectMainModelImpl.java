package mosApp.MosSystem;

import java.sql.SQLException;
import java.sql.Timestamp;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *  BJH - Nov 20, 2002 : Add field for MI Indicator Description (to eliminate need for doUWMIIndicatorModel -
 *                       translation of description can be accomplished via resourse bundle in normal fashion later)
 *
 *                       Note, field could be safely added since is non-null with appropriate integrity constraint.
 * @version 1.2  <br>
 * Date: 06/26/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	added getDfRateGuaranteePeriod(), setDfRateGuaranteePeriod() <br>
 *	added 2 constants QUALIFIED_COLUMN_DFRATEGUARANTEEPERIOD,COLUMN_DFRATEGUARANTEEPERIOD <br>
 *	updated SELECT_SQL_TEMPLATE to include RATEGUARANTEEPERIOD <br>
 *	updated static block for RATEGUARANTEEPERIOD <br>
 *
 * @version 1.3  <br>
 * Date: 07/28/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Modified to add field for affiliation program Id<br>
 *	- added declaration for getter/setter for affiliation program id<br>
 *  - added 2 constants QUALIFIED_COLUMN_DFAFFILIATIONPROGRAMID,COLUMN_DFAFFILIATIONPROGRAMID <br>
 *	- updated SELECT_SQL_TEMPLATE to include AFFILIATIONPROGRAMID <br>
 *	- updated static block for AFFILIATIONPROGRAMID <br>
 *	
 *	@Version 1.4 <br>
 *	Date: 06/09/2008
 *	Author: MCM Impl Team <br>
 *	Change Log:  <br>
 *	XS_2.46 -- 06/06/2008 -- Modified to add field for RefiAdditionalInformation<br>
 *		- added declaration for getter/setter for RefiAdditionalInformation<br>
 * @Version 1.5 <br>
 *  Date: 16/07/2008
 *  Author: MCM Impl Team <br>
 *  Change Log:  <br>
 *  Bug Fix: Modified data type for getDfRateDate() from timestamp to Date data type.
 *	
 */

public class doUWDealSelectMainModelImpl extends QueryModelBase
	implements doUWDealSelectMainModel
{
	/**
	 *
	 *
	 */
	public doUWDealSelectMainModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
//	 ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	/**
	 * getDfCashBackOverrideInDollars
	 * 
	 * @param None
	 *            <br>
	 * 
	 * @return String : the result of getDfCashBackOverrideInDollars <br>
	 */
	public String getDfCashBackOverrideInDollars() {
		return (String) getValue(FIELD_DFCASHBACKOVERRIDEINDOLLARS);
	}

	/**
	 * setDfCashBackOverrideInDollars
	 * 
	 * @param String
	 *            <br>
	 * 
	 * @return void : the result of setDfCashBackOverrideInDollars <br>
	 */
	public void setDfCashBackOverrideInDollars(String value) {
		setValue(FIELD_DFCASHBACKOVERRIDEINDOLLARS, value);

	}

	/**
	 * getDfCashBackInDollars
	 * 
	 * @param None
	 *            <br>
	 * 
	 * @return BigDecimal : the result of getDfCashBackInDollars <br>
	 */

	public java.math.BigDecimal getDfCashBackInDollars() {
		return (java.math.BigDecimal) getValue(FIELD_DFCASHBACKINDOLLARS);
	}

	/**
	 * setDfCashBackInDollars
	 * 
	 * @param BigDecimal
	 *            <br>
	 * 
	 * @return Void : the result of setDfCashBackInDollars <br>
	 */
	public void setDfCashBackInDollars(java.math.BigDecimal value) {
		setValue(FIELD_DFCASHBACKINDOLLARS, value);
	}

	/**
	 * getDfCashBackInPercentage
	 * 
	 * @param None
	 *            <br>
	 * 
	 * @return BigDecimal : the result of getDfCashBackInPercentage <br>
	 */

	public java.math.BigDecimal getDfCashBackInPercentage() {
		return (java.math.BigDecimal) getValue(FIELD_DFCASHBACKINPERCENTAGE);
	}

	/**
	 * setDfCashBackInPercentage
	 * 
	 * @param BigDecimal
	 *            <br>
	 * 
	 * @return void : the result of setDfCashBackInPercentage <br>
	 */
	public void setDfCashBackInPercentage(java.math.BigDecimal value) {
		setValue(FIELD_DFCASHBACKINPERCENTAGE, value);
	}


// ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	public static final String QUALIFIED_COLUMN_DFCASHBACKINPERCENTAGE = "DEAL.CASHBACKPERCENT";

	public static final String COLUMN_DFCASHBACKINPERCENTAGE = "CASHBACKPERCENT";

	public static final String QUALIFIED_COLUMN_DFCASHBACKINDOLLARS = "DEAL.CASHBACKAMOUNT";

	public static final String COLUMN_DFCASHBACKINDOLLARS = "CASHBACKAMOUNT";

	public static final String QUALIFIED_COLUMN_DFCASHBACKOVERRIDEINDOLLARS = "DEAL.CASHBACKAMOUNTOVERRIDE";

	public static final String COLUMN_DFCASHBACKOVERRIDEINDOLLARS = "CASHBACKAMOUNTOVERRIDE";

	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{
    logger = SysLog.getSysLogger("UWDSMM");
    logger.debug("UWDSMM@beforeExecute::SQLTemplate: " + this.getSelectSQL());
		return sql;
	}

	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    logger = SysLog.getSysLogger("UWDSMM");
    logger.debug("UWDSMM@afterExecute::SQLTemplate: " + this.getSelectSQL());

    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.handler);

    //// 6. Override all the Description fields (see #1-5 below in SQL_TEMPLATE section
    //// and translate them based on the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			              getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                            SessionStateModel.class,
                                            defaultInstanceStateName,
                                            true);

    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    //logger.debug("UWDSMM@afterExecute::LanguageId: " + languageId);

    while(this.next())
    {
      //Translate the UWScenarioDescription textbox content.
      this.setDfScenarioDescTranslated(handler.translateScenarioDescription(
                                      this.getDfScenarioDesc(), languageId));

      //logger.debug("UWDSMM@afterExecute::Before this.getDfScenarioDesc = " + this.getDfScenarioDesc());

      //===============================================================
      // New fields to handle MIPolicyNum problem :
      //  the number lost if user cancel MI and re-send again later.
      //--> Need to set to NULL string if the field is null, otherwise, the Hidden Value field binding to this
      //--> will not be generated by JATO.
      //--> By Billy 17July2003
      if(this.getDfMIPolicyNumberCMHC() == null)
        this.setDfMIPolicyNumberCMHC("");
      if(this.getDfMIPolicyNumberGE() == null)
        this.setDfMIPolicyNumberGE("");
      if(this.getDfMIPolicyNumberAIGUG() == null)
    	  this.setDfMIPolicyNumberAIGUG("");
      if(this.getDfMIPolicyNumberPMI() == null)
    	  this.setDfMIPolicyNumberPMI("");
      //=====================================================================================================

      //-- FXLink Phase II --//
      //--> New Field for Market Type Indicator
      //--> By Billy 18Nov2003
      if(this.getDfMccMarketType() != null)
        this.setDfMccMarketType(BXResources.getPickListDescription(
          institutionId, "MCCMARKETTYPE", this.getDfMccMarketType(), languageId));
      else
        this.setDfMccMarketType("");
      //======================================
      
      
      //cr03
      if(this.getDfAutoCalcCommitExpiryDate()==null)
          this.setDfAutoCalcCommitExpiryDate("Y");
      //cr03
     }
     
    if(this.getDfOverrideQualProd()!=null) {
        this.setDfOverrideQualProd(BXResources.getPickListDescription(
            institutionId, "MTGPROD", this.getDfOverrideQualProd(),
            languageId));
    }
		handler.pageSaveState();

   //Reset Location
    this.beforeFirst();

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}
	/**
	 * getDfRateGuaranteePeriod
	 *
	 * @param  <br>
	 *
	 * @return String : the result of getDfRateGuaranteePeriod <br>
	 * @version 1.2  <br>
	 * Date: 6/26/006 <br>
	 * Author: NBC/PP Implementation Team <br>
	 * Change:  <br>
	 *	<br>
	 */
	public String getDfRateGuaranteePeriod()
	  {
		
		  return (String)getValue(FIELD_DFRATEGUARANTEEPERIOD);
	  }

	/**
	 * setDfRateGuaranteePeriod
	 *
	 * @param String  <br>
	 *
	 * @return void : the result of setDfRateGuaranteePeriod <br>
	 * @version 1.2  <br>
	 * Date: 6/26/006 <br>
	 * Author: NBC/PP Implementation Team <br>
	 * Change:  <br>
	 *	<br>
	 */
	public void setDfRateGuaranteePeriod(String value)
	  {

		  setValue(FIELD_DFRATEGUARANTEEPERIOD, value);
		  
	  }

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}

    /**
     *
     *
     */
    public java.math.BigDecimal getDfCopyId()
    {
        return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
    }

	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


    /**
    *
    *
    */
   public java.math.BigDecimal getDfInstitutionId()
   {
       return (java.math.BigDecimal)getValue(FIELD_DFINSTITUTIONID);
   }
   /**
   *
   *
   */
  public void setDfInstitutionId(java.math.BigDecimal value)
  {
        setValue(FIELD_DFINSTITUTIONID,value);
  }

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSourceFirmProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSOURCEFIRMPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceFirmProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSOURCEFIRMPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfScenarioNumber()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSCENARIONUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfScenarioNumber(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSCENARIONUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfScenarioDesc()
	{
		return (String)getValue(FIELD_DFSCENARIODESC);
	}


	/**
	 *
	 *
	 */
	public void setDfScenarioDesc(String value)
	{
		setValue(FIELD_DFSCENARIODESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfScenarioRecomended()
	{
		return (String)getValue(FIELD_DFSCENARIORECOMENDED);
	}


	/**
	 *
	 *
	 */
	public void setDfScenarioRecomended(String value)
	{
		setValue(FIELD_DFSCENARIORECOMENDED,value);
	}


	/**
	 *
	 *
	 */
	public String getDfScenarioLocked()
	{
		return (String)getValue(FIELD_DFSCENARIOLOCKED);
	}


	/**
	 *
	 *
	 */
	public void setDfScenarioLocked(String value)
	{
		setValue(FIELD_DFSCENARIOLOCKED,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombined3YearGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINED3YEARGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombined3YearGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINED3YEARGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedBorrowerGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDBORROWERGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedBorrowerGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDBORROWERGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombined3YearTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINED3YEARTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombined3YearTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINED3YEARTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedBorrowerTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDBORROWERTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedBorrowerTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDBORROWERTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLienPositionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIENPOSITIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfLienPositionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIENPOSITIONID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealDownPaymentAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALDOWNPAYMENTAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfDealDownPaymentAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALDOWNPAYMENTAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLOBId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLOBID);
	}


	/**
	 *
	 *
	 */
	public void setDfLOBId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLOBID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLenderProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLENDERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfLenderProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLENDERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMTGProductId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMTGPRODUCTID);
	}


	/**
	 *
	 *
	 */
	public void setDfMTGProductId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMTGPRODUCTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedLTV()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDLTV);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedLTV(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDLTV,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPremiumAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMIPREMIUMAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfMIPremiumAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMIPREMIUMAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealDiscount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALDISCOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfDealDiscount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALDISCOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLoanAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalLoanAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALLOANAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealPremium()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALPREMIUM);
	}


	/**
	 *
	 *
	 */
	public void setDfDealPremium(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALPREMIUM,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAmortizationTerm()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAMORTIZATIONTERM);
	}


	/**
	 *
	 *
	 */
	public void setDfAmortizationTerm(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAMORTIZATIONTERM,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBuyDownRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBUYDOWNRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfBuyDownRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBUYDOWNRATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEffectiveAmortizationInMonths()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS);
	}


	/**
	 *
	 *
	 */
	public void setDfEffectiveAmortizationInMonths(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNETRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfNetRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNETRATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPaymentTermDesc()
	{
		return (String)getValue(FIELD_DFPAYMENTTERMDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTermDesc(String value)
	{
		setValue(FIELD_DFPAYMENTTERMDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPIPaymentAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPIPAYMENTAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfPIPaymentAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPIPAYMENTAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualPaymentTerm()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTUALPAYMENTTERM);
	}


	/**
	 *
	 *
	 */
	public void setDfActualPaymentTerm(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTUALPAYMENTTERM,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaymentFrequencyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPAYMENTFREQUENCYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentFrequencyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPAYMENTFREQUENCYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEscrowPaymentAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFESCROWPAYMENTAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfEscrowPaymentAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFESCROWPAYMENTAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPaymentAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPAYMENTAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPaymentAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPAYMENTAMOUNT,value);
	}
  //--DJ_LDI_CR--start--//

	/**
	 *
	 *
	 */
	public void setDfPmntPlusLifeDisability(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPMNTPLUSLIFEDISABILITY,value);
	}

	/**
	 *
	 *
	 */

  public java.math.BigDecimal getDfPmntPlusLifeDisability()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPMNTPLUSLIFEDISABILITY);
	}
  //--DJ_LDI_CR--end--//

  //--DJ_CR010--start//
	/**
	 *
	 *
	 */
	public String getDfCommisionCode()
	{
		return (String)getValue(FIELD_DFCOMMISIONCODE);
	}

	/**
	 *
	 *
	 */
	public void setDfCommisionCode(String value)
	{
		setValue(FIELD_DFCOMMISIONCODE,value);
	}
  //--DJ_CR010--end//

  //--DJ_CR203.1--start//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProprietairePlusLOC()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPRIETAIREPLUSLOC);
	}

	/**
	 *
	 *
	 */
	public void setDfProprietairePlusLOC(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPRIETAIREPLUSLOC,value);
	}

	/**
	 *
	 *
	 */
	public String getDfMultiProject()
	{
		return (String)getValue(FIELD_DFMULTIPROJECT);
	}

	/**
	 *
	 *
	 */
	public void setDfMultiProject(String value)
	{
		setValue(FIELD_DFMULTIPROJECT,value);
	}

	/**
	 *
	 *
	 */
	public String getDfProprietairePlus()
	{
		return (String)getValue(FIELD_DFPROPRIETAIREPLUS);
	}

	/**
	 *
	 *
	 */
	public void setDfProprietairePlus(String value)
	{
		setValue(FIELD_DFPROPRIETAIREPLUS,value);
	}
  //--DJ_CR203.1--end//

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPrivilagePaymentId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPRIVILAGEPAYMENTID);
	}


	/**
	 *
	 *
	 */
	public void setDfPrivilagePaymentId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPRIVILAGEPAYMENTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfExistingLoanAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEXISTINGLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfExistingLoanAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEXISTINGLOANAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealPurposeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALPURPOSEID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealPurposeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALPURPOSEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBranchProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRANCHPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRANCHPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCrossellProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCROSSELLPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfCrossellProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCROSSELLPROFILEID,value);
	}

	// SEAN GECF IV Document Type drop down: dfIVDocumentTypeId getter setter implementation.
	public java.math.BigDecimal getDfIVDocumentTypeId() {

		return (java.math.BigDecimal) getValue(FIELD_DFIVDOCUMENTTYPEID);
	}

	public void setDfIVDocumentTypeId(java.math.BigDecimal value) {

		setValue(FIELD_DFIVDOCUMENTTYPEID, value);
	}
	// END GECF Document Type drop down

	// SEAN DJ SPEC-Progress Advance Type July 21, 2005: getter and setter implementation.
	public java.math.BigDecimal getDfProgressAdvanceTypeId() {

		return (java.math.BigDecimal) getValue(FIELD_DFPROGRESSADVANCETYPEID);
	}

	public void setDfProgressAdvanceTypeId(java.math.BigDecimal value) {

		setValue(FIELD_DFPROGRESSADVANCETYPEID, value);
	}
	// SEAN DJ SPEC-PAT END

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSpecialFeatureId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSPECIALFEATUREID);
	}


	/**
	 *
	 *
	 */
	public void setDfSpecialFeatureId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSPECIALFEATUREID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfExistingMTGNumber()
	{
		return (String)getValue(FIELD_DFEXISTINGMTGNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfExistingMTGNumber(String value)
	{
		setValue(FIELD_DFEXISTINGMTGNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRateLock()
	{
		return (String)getValue(FIELD_DFRATELOCK);
	}


	/**
	 *
	 *
	 */
	public void setDfRateLock(String value)
	{
		setValue(FIELD_DFRATELOCK,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfReferenceDealTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREFERENCEDEALTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceDealTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREFERENCEDEALTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRePaymentTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREPAYMENTTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfRePaymentTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREPAYMENTTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfReferenceDealNumber()
	{
		return (String)getValue(FIELD_DFREFERENCEDEALNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceDealNumber(String value)
	{
		setValue(FIELD_DFREFERENCEDEALNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSecondaryFinancing()
	{
		return (String)getValue(FIELD_DFSECONDARYFINANCING);
	}


	/**
	 *
	 *
	 */
	public void setDfSecondaryFinancing(String value)
	{
		setValue(FIELD_DFSECONDARYFINANCING,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEstimatedClosingDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFESTIMATEDCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfEstimatedClosingDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFESTIMATEDCLOSINGDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfFinanceProgramId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFFINANCEPROGRAMID);
	}


	/**
	 *
	 *
	 */
	public void setDfFinanceProgramId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFFINANCEPROGRAMID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfFirstPaymentDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFFIRSTPAYMENTDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstPaymentDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFFIRSTPAYMENTDATE,value);
	}

    /**
     * FFATE
     *
     */
    public java.sql.Timestamp getDfFinancingWaiverDate()
    {
        return (java.sql.Timestamp)getValue(FIELD_DFFINANCINGWAIVERDATE);
    }


    /**
     * CR03
     *
     */
    public void setDfFinancingWaiveDate(java.sql.Timestamp value)
    {
        setValue(FIELD_DFFINANCINGWAIVERDATE,value);
    }

	/**
	 * CR03
	 *
	 */
	public java.sql.Timestamp getDfCommitmentExpirationDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFCOMMITMENTEXPIRATIONDATE);
	}


	/**
	 * CR03
	 *
	 */
	public void setDfCommitmentExpirationDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFCOMMITMENTEXPIRATIONDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealTaxPayorId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALTAXPAYORID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealTaxPayorId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALTAXPAYORID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfMaturityId()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFMATURITYID);
	}


	/**
	 *
	 *
	 */
	public void setDfMaturityId(java.sql.Timestamp value)
	{
		setValue(FIELD_DFMATURITYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSourceApplicationId()
	{
		return (String)getValue(FIELD_DFSOURCEAPPLICATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfSourceApplicationId(String value)
	{
		setValue(FIELD_DFSOURCEAPPLICATIONID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTotalIncome()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDTOTALINCOME);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedTotalIncome(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDTOTALINCOME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTotalLiabilities()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDTOTALLIABILITIES);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedTotalLiabilities(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDTOTALLIABILITIES,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTotalAssets()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDTOTALASSETS);
	}


	/**
	 *
	 *
	 */
	public void setDfCombinedTotalAssets(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBINEDTOTALASSETS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPropertyExpenses()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPROPERTYEXPENSES);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPropertyExpenses(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPROPERTYEXPENSES,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaximumPricipalAllowed()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMAXIMUMPRICIPALALLOWED);
	}


	/**
	 *
	 *
	 */
	public void setDfMaximumPricipalAllowed(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMAXIMUMPRICIPALALLOWED,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMinimumIncomeRequired()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMINIMUMINCOMEREQUIRED);
	}


	/**
	 *
	 *
	 */
	public void setDfMinimumIncomeRequired(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMINIMUMINCOMEREQUIRED,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaximumTDSExpenseAllowed()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMAXIMUMTDSEXPENSEALLOWED);
	}


	/**
	 *
	 *
	 */
	public void setDfMaximumTDSExpenseAllowed(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMAXIMUMTDSEXPENSEALLOWED,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetLoanAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNETLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfNetLoanAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNETLOANAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdvanceHold()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADVANCEHOLD);
	}


	/**
	 *
	 *
	 */
	public void setDfAdvanceHold(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADVANCEHOLD,value);
	}


	/**
	 *
	 **
   * Bug Fix:artf749317 :This is the temp fix for the converting date to time stamp.we need to find out the permanent fix for this bug
   */
	 
  
	public java.sql.Timestamp getDfRateDate()
	{
		Object dfRateDateObj = getValue(FIELD_DFRATEDATE);
    if(dfRateDateObj!=null)
    {
    if( dfRateDateObj instanceof Timestamp)
        return (java.sql.Timestamp) dfRateDateObj;
        else
          return new java.sql.Timestamp(((java.sql.Date)dfRateDateObj).getTime());
    }       
       return (Timestamp) dfRateDateObj;  
	}


	/**
	 *
	 *
	 */
	public void setDfRateDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFRATEDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPAPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPAPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfPAPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPAPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIStatus()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMISTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfMIStatus(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMISTATUS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPayorId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMIPAYORID);
	}


	/**
	 *
	 *
	 */
	public void setDfMIPayorId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMIPAYORID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMITypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMITYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfMITypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMITYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIUpFrontId()
	{
		return (String)getValue(FIELD_DFMIUPFRONTID);
	}


	/**
	 *
	 *
	 */
	public void setDfMIUpFrontId(String value)
	{
		setValue(FIELD_DFMIUPFRONTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIIndicatorId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMIINDICATORID);
	}


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMIINDICATORID,value);
	}

	/**
	 *
	 *
	 */
	public String getDfMIIndicatorDescription()
	{
		return (String)getValue(FIELD_DFMIINDICATORDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorDescription(String value)
	{
		setValue(FIELD_DFMIINDICATORDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMInsurerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMINSURERID);
	}


	/**
	 *
	 *
	 */
	public void setDfMInsurerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMINSURERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIPolicyNumber()
	{
		return (String)getValue(FIELD_DFMIPOLICYNUMBER);
	}

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 17July2003
  public String getDfMIPolicyNumberCMHC()
	{
		return (String)getValue(FIELD_DFMIPOLICYNUMBERCMHC);
	}

  public String getDfMIPolicyNumberGE()
	{
		return (String)getValue(FIELD_DFMIPOLICYNUMBERGE);
	}
  public String getDfMIPolicyNumberAIGUG()
	{
		return (String)getValue(FIELD_DFMIPOLICYNUMBERAIGUG);
	}
  public String getDfMIPolicyNumberPMI()
	{
		return (String)getValue(FIELD_DFMIPOLICYNUMBERPMI);
	}
  //================================================================

	/**
	 *
	 *
	 */
	public void setDfMIPolicyNumber(String value)
	{
		setValue(FIELD_DFMIPOLICYNUMBER,value);
	}

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 17July2003
  public void setDfMIPolicyNumberCMHC(String value)
	{
		setValue(FIELD_DFMIPOLICYNUMBERCMHC,value);
	}
  public void setDfMIPolicyNumberGE(String value)
	{
		setValue(FIELD_DFMIPOLICYNUMBERGE,value);
	}
  public void setDfMIPolicyNumberAIGUG(String value)
	{
		setValue(FIELD_DFMIPOLICYNUMBERAIGUG,value);
	}
  public void setDfMIPolicyNumberPMI(String value)
	{
		setValue(FIELD_DFMIPOLICYNUMBERPMI,value);
	}
  //================================================================

	/**
	 *
	 *
	 */
	public String getDfMIExistingPolicyNumber()
	{
		return (String)getValue(FIELD_DFMIEXISTINGPOLICYNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfMIExistingPolicyNumber(String value)
	{
		setValue(FIELD_DFMIEXISTINGPOLICYNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdditionalPrincipal()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADDITIONALPRINCIPAL);
	}


	/**
	 *
	 *
	 */
	public void setDfAdditionalPrincipal(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADDITIONALPRINCIPAL,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPrePaymentOptionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPREPAYMENTOPTIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfPrePaymentOptionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPREPAYMENTOPTIONID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequiredDownpayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREQUIREDDOWNPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfRequiredDownpayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREQUIREDDOWNPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombainedTotalPropertyExpence()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOMBAINEDTOTALPROPERTYEXPENCE);
	}


	/**
	 *
	 *
	 */
	public void setDfCombainedTotalPropertyExpence(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOMBAINEDTOTALPROPERTYEXPENCE,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfReturnDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFRETURNDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfReturnDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFRETURNDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProgressAdvance()
	{
		return (String)getValue(FIELD_DFPROGRESSADVANCE);
	}


	/**
	 *
	 *
	 */
	public void setDfProgressAdvance(String value)
	{
		setValue(FIELD_DFPROGRESSADVANCE,value);
	}

	/**
	 *CR03
	 *
	 */
	public String getDfAutoCalcCommitExpiryDate()
	{
		return (String)getValue(FIELD_DFAUTOCALCCOMMITEXPIRYDATE);
	}


	/**
	 *CR03
	 *
	 */
	public void setDfAutoCalcCommitExpiryDate(String value)
	{
		setValue(FIELD_DFAUTOCALCCOMMITEXPIRYDATE,value);
	}



	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdvanceToDateAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADVANCETODATEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfAdvanceToDateAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADVANCETODATEAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdvanceNumber()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADVANCENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfAdvanceNumber(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADVANCENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIRUIntervention()
	{
		return (String)getValue(FIELD_DFMIRUINTERVENTION);
	}


	/**
	 *
	 *
	 */
	public void setDfMIRUIntervention(String value)
	{
		setValue(FIELD_DFMIRUINTERVENTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPreQualificationCertNum()
	{
		return (String)getValue(FIELD_DFPREQUALIFICATIONCERTNUM);
	}


	/**
	 *
	 *
	 */
	public void setDfPreQualificationCertNum(String value)
	{
		setValue(FIELD_DFPREQUALIFICATIONCERTNUM,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRefiPurpose()
	{
		return (String)getValue(FIELD_DFREFIPURPOSE);
	}


	/**
	 *
	 *
	 */
	public void setDfRefiPurpose(String value)
	{
		setValue(FIELD_DFREFIPURPOSE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiOrigPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREFIORIGPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfRefiOrigPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREFIORIGPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRefiBlendedAmortization()
	{
		return (String)getValue(FIELD_DFREFIBLENDEDAMORTIZATION);
	}


	/**
	 *
	 *
	 */
	public void setDfRefiBlendedAmortization(String value)
	{
		setValue(FIELD_DFREFIBLENDEDAMORTIZATION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiOrigMtgAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREFIORIGMTGAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfRefiOrigMtgAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREFIORIGMTGAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRefiImprovementsDesc()
	{
		return (String)getValue(FIELD_DFREFIIMPROVEMENTSDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfRefiImprovementsDesc(String value)
	{
		setValue(FIELD_DFREFIIMPROVEMENTSDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiImprovementValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREFIIMPROVEMENTVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfRefiImprovementValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREFIIMPROVEMENTVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNextAdvanceAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNEXTADVANCEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfNextAdvanceAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNEXTADVANCEAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRefiOrigPurchaseDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFREFIORIGPURCHASEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfRefiOrigPurchaseDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFREFIORIGPURCHASEDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRefiCurMortgageHolder()
	{
		return (String)getValue(FIELD_DFREFICURMORTGAGEHOLDER);
	}


	/**
	 *
	 *
	 */
	public void setDfRefiCurMortgageHolder(String value)
	{
		setValue(FIELD_DFREFICURMORTGAGEHOLDER,value);
	}

  //--Release2.1--start//
	/**
	 *
	 *
	 */
	public String getDfScenarioDescTranslated()
	{
		return (String)getValue(FIELD_DFSCENARIODESCTRANSLATED);
	}


	/**
	 *
	 *
	 */
	public void setDfScenarioDescTranslated(String value)
	{
		setValue(FIELD_DFSCENARIODESCTRANSLATED,value);
	}
  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public String getDfChannelMedia()
	{
		return (String)getValue(FIELD_DFCHANNELMEDIA);
	}


	/**
	 *
	 *
	 */
	public void setDfChannelMedia(String value)
	{
		setValue(FIELD_DFCHANNELMEDIA,value);
	}

  //-- FXLink Phase II --//
  //--> New Field for Market Type Indicator
  //--> By Billy 18Nov2003
  public String getDfMccMarketType()
	{
		return (String)getValue(FIELD_DFMCCMARKETTYPE);
	}

	public void setDfMccMarketType(String value)
	{
		setValue(FIELD_DFMCCMARKETTYPE,value);
	}
  //======================================

  //--Release3.1--begins
  //--by Hiro Apr 13, 2006
  public void setDfProductTypeId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPRODUCTTYPEID,value);
  }

  public java.math.BigDecimal getDfProductTypeId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPRODUCTTYPEID);
  }

  public void setDfRefiProductTypeId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFREFIPRODUCTTYPEID,value);
  }

  public java.math.BigDecimal getDfRefiProductTypeId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFREFIPRODUCTTYPEID);
  }

  public String getDfCmhcProductTrackerIdentifier()
  {
    return (String)getValue(FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER);
  }

  public void setDfCmhcProductTrackerIdentifier(String value)
  {
    setValue(FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER,value);
  }

  public java.math.BigDecimal getDfLOCAmortizationMonths()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLOCAMORTIZATIONMONTHS);
  }

  public void setDfLOCAmortizationMonths(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLOCAMORTIZATIONMONTHS,value);
  }

  public java.sql.Timestamp getDfLOCInterestOnlyMaturityDate()
  {
    return (java.sql.Timestamp)getValue(FIELD_DFLOCINTERESTONLYMATURITYDATE);
  }

  public void setDfLOCInterestOnlyMaturityDate(java.sql.Timestamp value)
  {
    setValue(FIELD_DFLOCINTERESTONLYMATURITYDATE,value);
  }

  public java.math.BigDecimal getDfLOCRepaymentTypeId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLOCREPAYMENTTYPEID);
  }

  public void setDfLOCRepaymentTypeId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLOCREPAYMENTTYPEID,value);
  }

  public String getDfProgressAdvanceInspectionBy()
  {
    return (String)getValue(FIELD_DFPROGRESSADVANCEINSPECTIONBY);
  }

  public void setDfProgressAdvanceInspectionBy(String value)
  {
    setValue(FIELD_DFPROGRESSADVANCEINSPECTIONBY,value);
  }

  public String getDfRequestStandardService()
  {
    return (String)getValue(FIELD_DFREQUESTSTANDARDSERVICE);
  }

  public void setDfRequestStandardService(String value)
  {
    setValue(FIELD_DFREQUESTSTANDARDSERVICE,value);
  }

  public String getDfSelfDirectedRRSP()
  {
    return (String)getValue(FIELD_DFSELFDIRECTEDRRSP);
  }

  public void setDfSelfDirectedRRSP(String value)
  {
    setValue(FIELD_DFSELFDIRECTEDRRSP,value);
  }

  
  //--Release3.1--ends
  
	 //    ***** Change by NBC Impl. Team - Version 1.3 - Start *****//	
	
	public void setDfAffiliationProgramId(java.math.BigDecimal value )
	{
		setValue(FIELD_DFAFFILIATIONPROGRAMID,value);
	}
	
	public java.math.BigDecimal getDfAffiliationProgramId()
	{
		return (java.math.BigDecimal) getValue(FIELD_DFAFFILIATIONPROGRAMID);
	}
	
	 //    ***** Change by NBC Impl. Team - Version 1.3 - End *****//
	
	/***************MCM Impl team changes starts - XS_2.46 *******************/  
		/**
		 * This method returns DfRefiAdditionalInformation value
		 *
		 * @param None <br>
		 *  
		 * @return String : the result of getDfRefiAdditionalInformation <br>
		 */
		public String getDfRefiAdditionalInformation()
		{
			return (String)getValue(FIELD_DFREFIADDITIONALINFORMATION);
		}

		/**
		 * This method sets value for DfRefiAdditionalInformation
		 *
		 * @param String <br>
		 *  
		 */
		public void setDfRefiAdditionalInformation(String value)
		{
			setValue(FIELD_DFREFIADDITIONALINFORMATION,value);
		}
		/***************MCM Impl team changes ends - XS_2.46 *********************/  
        
        
        //***** Qualify Rate *****//
        public String getDfQualifyingOverrideFlag() {
            return (String) getValue(FIELD_DFQUALIFYINGOVERRIDEFLAG);
        }

        public void setDfQualifyingOverrideFlag(String value) {
            setValue(FIELD_DFQUALIFYINGOVERRIDEFLAG, value);

        }
        
        public java.math.BigDecimal getDfQualifyRate() {
            return (java.math.BigDecimal) getValue(FIELD_DFQUALIFYRATE);
        }

        public void setDfQualifyRate(java.math.BigDecimal value) {
            setValue(FIELD_DFQUALIFYRATE, value);

        }
        
        public String getDfOverrideQualProd() {
            return (String) getValue(FIELD_DFOVERRIDEQUALPROD);
        }

        public void setDfOverrideQualProd(String value) {
            setValue(FIELD_DFOVERRIDEQUALPROD, value);
        }
        
        public String getDfQualifyingOverrideRateFlag() {
            return (String) getValue(FIELD_DFQUALIFYINGOVERRIDERATEFLAG);
        }

        public void setDfQualifyingOverrideRateFlag(String value) {
            setValue(FIELD_DFQUALIFYINGOVERRIDERATEFLAG, value);

        }
        // ***** Qualify Rate *****//
        
        // Ticket 267
        public String getDfDealStatusId() {
      	  return (String)getValue(FIELD_DFDEALSTATUSID);
        }
        
        public void setDfDealStatusId(String value) {
      	  setValue(FIELD_DFDEALSTATUSID, value);
        }
        
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
  public SysLogger logger;
	private UWorksheetHandler handler=new UWorksheetHandler();

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE=

  //--Release2.1--//
  /**
  "SELECT DISTINCT DEAL.DEALID, DEAL.COPYID, DEAL.SOURCEFIRMPROFILEID, DEAL.SCENARIONUMBER, " +
  "DEAL.SCENARIODESCRIPTION, DEAL.SCENARIORECOMMENDED, DEAL.SCENARIOLOCKED, DEAL.COMBINEDGDS, " +
  "DEAL.COMBINEDGDS3YEAR, DEAL.COMBINEDGDSBORROWER, DEAL.COMBINEDTDS, DEAL.COMBINEDTDS3YEAR, " +
  "DEAL.COMBINEDTDSBORROWER, DEAL.TOTALPURCHASEPRICE, DEAL.LIENPOSITIONID, " +
  "DEAL.DOWNPAYMENTAMOUNT, DEAL.LINEOFBUSINESSID, DEAL.LENDERPROFILEID, DEAL.MTGPRODID, " +
  "DEAL.COMBINEDLTV, DEAL.MIPREMIUMAMOUNT, DEAL.DISCOUNT, DEAL.TOTALLOANAMOUNT, " +
  "DEAL.PREMIUM, DEAL.AMORTIZATIONTERM, DEAL.BUYDOWNRATE, DEAL.EFFECTIVEAMORTIZATIONMONTHS, " +
  "DEAL.NETINTERESTRATE, PAYMENTTERM.PTDESCRIPTION, DEAL.PANDIPAYMENTAMOUNT, " +
  "DEAL.ACTUALPAYMENTTERM, DEAL.PAYMENTFREQUENCYID, DEAL.ESCROWPAYMENTAMOUNT, " +
  "DEAL.TOTALPAYMENTAMOUNT,   DEAL.PRIVILEGEPAYMENTID, DEAL.EXISTINGLOANAMOUNT, " +
  "DEAL.DEALPURPOSEID, DEAL.BRANCHPROFILEID, DEAL.DEALTYPEID, DEAL.CROSSSELLPROFILEID, " +
  "DEAL.SPECIALFEATUREID, DEAL.REFEXISTINGMTGNUMBER, DEAL.RATELOCK, DEAL.REFERENCEDEALTYPEID, " +
  "DEAL.REPAYMENTTYPEID, DEAL.REFERENCEDEALNUMBER, DEAL.SECONDARYFINANCING, " +
  "DEAL.ESTIMATEDCLOSINGDATE, DEAL.FINANCEPROGRAMID, DEAL.FIRSTPAYMENTDATE, " +
  "DEAL.TAXPAYORID, DEAL.MATURITYDATE, DEAL.SOURCEAPPLICATIONID, DEAL.COMBINEDTOTALINCOME, " +
  "DEAL.COMBINEDTOTALLIABILITIES, DEAL.COMBINEDTOTALASSETS, DEAL.TOTALPROPERTYEXPENSES, " +
  "DEAL.MAXIMUMPRINCIPALAMOUNT, DEAL.MINIMUMINCOMEREQUIRED, DEAL.MAXIMUMTDSEXPENSESALLOWED, " +
  "DEAL.NETLOANAMOUNT, DEAL.ADVANCEHOLD, DEAL.RATEDATE, DEAL.PAPURCHASEPRICE, " +
  "DEAL.MISTATUSID, DEAL.MIPAYORID, DEAL.MITYPEID, DEAL.MIUPFRONT, DEAL.MIINDICATORID, MIINDICATOR.MIIDESCRIPTION, " +
  "DEAL.MORTGAGEINSURERID,  DEAL.MIPOLICYNUMBER, DEAL.MIEXISTINGPOLICYNUMBER, " +
  "DEAL.ADDITIONALPRINCIPAL, DEAL.PREPAYMENTOPTIONSID, DEAL.REQUIREDDOWNPAYMENT, " +
  "DEAL.COMBINEDTOTALPROPERTYEXP, DEAL.RETURNDATE, DEAL.PROGRESSADVANCE, " +
  "DEAL.ADVANCETODATEAMOUNT, DEAL.ADVANCENUMBER, DEAL.MIRUINTERVENTION, " +
  "DEAL.PREQUALIFICATIONMICERTNUM, DEAL.REFIPURPOSE, DEAL.REFIORIGPURCHASEPRICE, " +
  "DEAL.REFIBLENDEDAMORTIZATION, DEAL.REFIORIGMTGAMOUNT, DEAL.REFIIMPROVEMENTSDESC, " +
  "DEAL.REFIIMPROVEMENTAMOUNT, DEAL.NEXTADVANCEAMOUNT, DEAL.REFIORIGPURCHASEDATE, " +
  "DEAL.REFICURMORTGAGEHOLDER, DEAL.CHANNELMEDIA  FROM DEAL, PAYMENTTERM, MIINDICATOR  __WHERE__  ";
  **/

  "SELECT ALL DEAL.DEALID, DEAL.COPYID, DEAL.INSTITUTIONPROFILEID, DEAL.SOURCEFIRMPROFILEID, DEAL.SCENARIONUMBER, " +
  "DEAL.SCENARIODESCRIPTION, to_char(DEAL.SCENARIODESCRIPTION) SCENARIODESCRIPTION_TRANS, " +
  "DEAL.SCENARIORECOMMENDED, DEAL.SCENARIOLOCKED, DEAL.COMBINEDGDS, " +
  "DEAL.COMBINEDGDS3YEAR, DEAL.COMBINEDGDSBORROWER, DEAL.COMBINEDTDS, DEAL.COMBINEDTDS3YEAR, " +
  "DEAL.COMBINEDTDSBORROWER, DEAL.TOTALPURCHASEPRICE, DEAL.LIENPOSITIONID, " +
  "DEAL.DOWNPAYMENTAMOUNT, DEAL.LINEOFBUSINESSID, DEAL.LENDERPROFILEID, DEAL.MTGPRODID, " +
  "DEAL.COMBINEDLTV, DEAL.MIPREMIUMAMOUNT, DEAL.DISCOUNT, DEAL.TOTALLOANAMOUNT, " +
  "DEAL.PREMIUM, DEAL.AMORTIZATIONTERM, DEAL.BUYDOWNRATE, DEAL.EFFECTIVEAMORTIZATIONMONTHS, " +
  "DEAL.NETINTERESTRATE, PAYMENTTERM.PTDESCRIPTION, DEAL.PANDIPAYMENTAMOUNT, " +
  "DEAL.ACTUALPAYMENTTERM, DEAL.PAYMENTFREQUENCYID, DEAL.ESCROWPAYMENTAMOUNT, " +
  "DEAL.TOTALPAYMENTAMOUNT,   DEAL.PRIVILEGEPAYMENTID, DEAL.EXISTINGLOANAMOUNT, " +
  "DEAL.DEALPURPOSEID, DEAL.BRANCHPROFILEID, DEAL.DEALTYPEID, DEAL.CROSSSELLPROFILEID, " +
  "DEAL.SPECIALFEATUREID, DEAL.REFEXISTINGMTGNUMBER, DEAL.RATELOCK, DEAL.REFERENCEDEALTYPEID, " +
  "DEAL.REPAYMENTTYPEID, DEAL.REFERENCEDEALNUMBER, DEAL.SECONDARYFINANCING, " +
  "DEAL.ESTIMATEDCLOSINGDATE, DEAL.FINANCEPROGRAMID, DEAL.FIRSTPAYMENTDATE, DEAL.COMMITMENTEXPIRATIONDATE, " +
  "DEAL.TAXPAYORID, DEAL.MATURITYDATE, DEAL.SOURCEAPPLICATIONID, DEAL.COMBINEDTOTALINCOME, " +
  "DEAL.COMBINEDTOTALLIABILITIES, DEAL.COMBINEDTOTALASSETS, DEAL.TOTALPROPERTYEXPENSES, " +
  "DEAL.MAXIMUMPRINCIPALAMOUNT, DEAL.MINIMUMINCOMEREQUIRED, DEAL.MAXIMUMTDSEXPENSESALLOWED, " +
  "DEAL.NETLOANAMOUNT, DEAL.ADVANCEHOLD, DEAL.RATEDATE, DEAL.PAPURCHASEPRICE, " +
  "DEAL.MISTATUSID, DEAL.MIPAYORID, DEAL.MITYPEID, DEAL.MIUPFRONT, DEAL.MIINDICATORID, MIINDICATOR.MIIDESCRIPTION, DEAL.STATUSID, " +
  "DEAL.MORTGAGEINSURERID,  DEAL.MIPOLICYNUMBER, DEAL.MIEXISTINGPOLICYNUMBER, " +
  "DEAL.ADDITIONALPRINCIPAL, DEAL.PREPAYMENTOPTIONSID, DEAL.REQUIREDDOWNPAYMENT, " +
  "DEAL.COMBINEDTOTALPROPERTYEXP, DEAL.RETURNDATE, DEAL.PROGRESSADVANCE, " +
  "DEAL.ADVANCETODATEAMOUNT, DEAL.ADVANCENUMBER, DEAL.MIRUINTERVENTION, " +
  "DEAL.PREQUALIFICATIONMICERTNUM, DEAL.REFIPURPOSE, DEAL.REFIORIGPURCHASEPRICE, " +
  "DEAL.REFIBLENDEDAMORTIZATION, DEAL.REFIORIGMTGAMOUNT, DEAL.REFIIMPROVEMENTSDESC, " +
  "DEAL.REFIIMPROVEMENTAMOUNT, DEAL.NEXTADVANCEAMOUNT, DEAL.REFIORIGPURCHASEDATE, " +
  "DEAL.REFICURMORTGAGEHOLDER, DEAL.FINANCINGWAIVERDATE, DEAL.CHANNELMEDIA, " +
  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 17July2003
  "DEAL.MIPOLICYNUMCMHC, DEAL.MIPOLICYNUMGE, DEAL.MIPOLICYNUMAIGUG, DEAL.MIPOLICYNUMPMI, " +
  //===============================================================
  //-- FXLink Phase II --//
  //--> New Field for Market Type Indicator
  //--> By Billy 18Nov2003
  "DEAL.MCCMARKETTYPE, " +
  //===============================================================
  //--DJ_LDI_CR--start--//
  "DEAL.PMNTPLUSLIFEDISABILITY, " +
  //--DJ_LDI_CR--end--//
   // SEAN GECF Document Type drop down: add to the SQL template.
   "DEAL.INCOMEVERIFICATIONTYPEID, " +
   // END GECF Document Type drop down
  // SEAN DJ SPEC-Progress Advance Type July 20, 2005: update the template.
  "DEAL.PROGRESSADVANCETYPEID, " +
  // SEAN DJ SPEC-PAT END.
  //--DJ_CR010--start//
  "DEAL.COMMISIONCODE, " +
  //--DJ_CR010--end//
  //--DJ_CR203.1--start//
  "DEAL.MULTIPROJECT, DEAL.PROPRIETAIREPLUS, DEAL.PROPRIETAIREPLUSLOC " +
  //--DJ_CR203.1--end//
  
  //--Release3.1--begins
  //--by Hiro Apr 13, 2006
  ",DEAL.PRODUCTTYPEID " +
  ",DEAL.REFIPRODUCTTYPEID " + 
  ",DEAL.CMHCPRODUCTTRACKERIDENTIFIER " +
  ",DEAL.LOCAMORTIZATIONMONTHS " +
  ",DEAL.LOCINTERESTONLYMATURITYDATE " +
  ",DEAL.LOCREPAYMENTTYPEID " +
  ",DEAL.PROGRESSADVANCEINSPECTIONBY " +
  ",DEAL.REQUESTSTANDARDSERVICE " +
  ",DEAL.SELFDIRECTEDRRSP " +
  //--Release3.1--ends
  
  //***** Change by NBC Impl. Team - Version 1.2 - Start *****//
  ",DEAL.RATEGUARANTEEPERIOD " +
  ",DEAL.CASHBACKAMOUNT , DEAL.CASHBACKPERCENT , DEAL.CASHBACKAMOUNTOVERRIDE " +
  // ***** Change by NBC Impl. Team - Version 1.2 - End*****//

  //	***** Change by NBC Impl. Team - Version 1.3 - Start *****//
  ",DEAL.AFFILIATIONPROGRAMID " +
  //	***** Change by NBC Impl. Team - Version 1.3 - End*****//

  //CR03
  /***************MCM Impl team changes starts - XS_2.46 *******************/
  ",DEAL.AUTOCALCCOMMITEXPIRYDATE,DEAL.REFIADDITIONALINFORMATION " +
  /***************MCM Impl team changes ends - XS_2.46 *******************/
  //CR03

  //***** Qualify Rate *****//
  ", DEAL.QUALIFYINGOVERRIDEFLAG, DEAL.QUALIFYINGRATEOVERRIDEFLAG, DEAL.GDSTDS3YEARRATE, to_char(DEAL.OVERRIDEQUALPROD) OVERRIDEQUALPROD_STR " +
  //***** Qualify Rate *****//
  
  //QC702
  ", DEAL.COMBINEDLENDINGVALUE " +
  
  " FROM DEAL, PAYMENTTERM, MIINDICATOR  " +
  "__WHERE__  ";

	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, PAYMENTTERM, MIINDICATOR";

	public static final String STATIC_WHERE_CRITERIA=" (DEAL.PAYMENTTERMID  =  PAYMENTTERM.PAYMENTTERMID)" +
                                                   " AND (DEAL.MIINDICATORID  =  MIINDICATOR.MIINDICATORID) " +
                                                   " AND (DEAL.INSTITUTIONPROFILEID = PAYMENTTERM.INSTITUTIONPROFILEID)";
    
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
    public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
    public static final String COLUMN_DFCOPYID="COPYID";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID="DEAL.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFINSTITUTIONID="INSTITUTIONPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSOURCEFIRMPROFILEID="DEAL.SOURCEFIRMPROFILEID";
	public static final String COLUMN_DFSOURCEFIRMPROFILEID="SOURCEFIRMPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSCENARIONUMBER="DEAL.SCENARIONUMBER";
	public static final String COLUMN_DFSCENARIONUMBER="SCENARIONUMBER";
	public static final String QUALIFIED_COLUMN_DFSCENARIODESC="DEAL.SCENARIODESCRIPTION";
	public static final String COLUMN_DFSCENARIODESC="SCENARIODESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFSCENARIORECOMENDED="DEAL.SCENARIORECOMMENDED";
	public static final String COLUMN_DFSCENARIORECOMENDED="SCENARIORECOMMENDED";
	public static final String QUALIFIED_COLUMN_DFSCENARIOLOCKED="DEAL.SCENARIOLOCKED";
	public static final String COLUMN_DFSCENARIOLOCKED="SCENARIOLOCKED";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDGDS="DEAL.COMBINEDGDS";
	public static final String COLUMN_DFCOMBINEDGDS="COMBINEDGDS";
	public static final String QUALIFIED_COLUMN_DFCOMBINED3YEARGDS="DEAL.COMBINEDGDS3YEAR";
	public static final String COLUMN_DFCOMBINED3YEARGDS="COMBINEDGDS3YEAR";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDBORROWERGDS="DEAL.COMBINEDGDSBORROWER";
	public static final String COLUMN_DFCOMBINEDBORROWERGDS="COMBINEDGDSBORROWER";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDTDS="DEAL.COMBINEDTDS";
	public static final String COLUMN_DFCOMBINEDTDS="COMBINEDTDS";
	public static final String QUALIFIED_COLUMN_DFCOMBINED3YEARTDS="DEAL.COMBINEDTDS3YEAR";
	public static final String COLUMN_DFCOMBINED3YEARTDS="COMBINEDTDS3YEAR";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDBORROWERTDS="DEAL.COMBINEDTDSBORROWER";
	public static final String COLUMN_DFCOMBINEDBORROWERTDS="COMBINEDTDSBORROWER";
	public static final String QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE="DEAL.TOTALPURCHASEPRICE";
	public static final String COLUMN_DFTOTALPURCHASEPRICE="TOTALPURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFLIENPOSITIONID="DEAL.LIENPOSITIONID";
	public static final String COLUMN_DFLIENPOSITIONID="LIENPOSITIONID";
	public static final String QUALIFIED_COLUMN_DFDEALDOWNPAYMENTAMOUNT="DEAL.DOWNPAYMENTAMOUNT";
	public static final String COLUMN_DFDEALDOWNPAYMENTAMOUNT="DOWNPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFLOBID="DEAL.LINEOFBUSINESSID";
	public static final String COLUMN_DFLOBID="LINEOFBUSINESSID";
	public static final String QUALIFIED_COLUMN_DFLENDERPROFILEID="DEAL.LENDERPROFILEID";
	public static final String COLUMN_DFLENDERPROFILEID="LENDERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFMTGPRODUCTID="DEAL.MTGPRODID";
	public static final String COLUMN_DFMTGPRODUCTID="MTGPRODID";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDLTV="DEAL.COMBINEDLTV";
	public static final String COLUMN_DFCOMBINEDLTV="COMBINEDLTV";
	public static final String QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT="DEAL.MIPREMIUMAMOUNT";
	public static final String COLUMN_DFMIPREMIUMAMOUNT="MIPREMIUMAMOUNT";
	public static final String QUALIFIED_COLUMN_DFDEALDISCOUNT="DEAL.DISCOUNT";
	public static final String COLUMN_DFDEALDISCOUNT="DISCOUNT";
	public static final String QUALIFIED_COLUMN_DFTOTALLOANAMOUNT="DEAL.TOTALLOANAMOUNT";
	public static final String COLUMN_DFTOTALLOANAMOUNT="TOTALLOANAMOUNT";
	public static final String QUALIFIED_COLUMN_DFDEALPREMIUM="DEAL.PREMIUM";
	public static final String COLUMN_DFDEALPREMIUM="PREMIUM";
	public static final String QUALIFIED_COLUMN_DFAMORTIZATIONTERM="DEAL.AMORTIZATIONTERM";
	public static final String COLUMN_DFAMORTIZATIONTERM="AMORTIZATIONTERM";
	public static final String QUALIFIED_COLUMN_DFBUYDOWNRATE="DEAL.BUYDOWNRATE";
	public static final String COLUMN_DFBUYDOWNRATE="BUYDOWNRATE";
	public static final String QUALIFIED_COLUMN_DFEFFECTIVEAMORTIZATIONINMONTHS="DEAL.EFFECTIVEAMORTIZATIONMONTHS";
	public static final String COLUMN_DFEFFECTIVEAMORTIZATIONINMONTHS="EFFECTIVEAMORTIZATIONMONTHS";
	public static final String QUALIFIED_COLUMN_DFNETRATE="DEAL.NETINTERESTRATE";
	public static final String COLUMN_DFNETRATE="NETINTERESTRATE";
	public static final String QUALIFIED_COLUMN_DFPAYMENTTERMDESC="PAYMENTTERM.PTDESCRIPTION";
	public static final String COLUMN_DFPAYMENTTERMDESC="PTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPIPAYMENTAMOUNT="DEAL.PANDIPAYMENTAMOUNT";
	public static final String COLUMN_DFPIPAYMENTAMOUNT="PANDIPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFACTUALPAYMENTTERM="DEAL.ACTUALPAYMENTTERM";
	public static final String COLUMN_DFACTUALPAYMENTTERM="ACTUALPAYMENTTERM";
	public static final String QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID="DEAL.PAYMENTFREQUENCYID";
	public static final String COLUMN_DFPAYMENTFREQUENCYID="PAYMENTFREQUENCYID";
	public static final String QUALIFIED_COLUMN_DFESCROWPAYMENTAMOUNT="DEAL.ESCROWPAYMENTAMOUNT";
	public static final String COLUMN_DFESCROWPAYMENTAMOUNT="ESCROWPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFTOTALPAYMENTAMOUNT="DEAL.TOTALPAYMENTAMOUNT";
	public static final String COLUMN_DFTOTALPAYMENTAMOUNT="TOTALPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFPRIVILAGEPAYMENTID="DEAL.PRIVILEGEPAYMENTID";
	public static final String COLUMN_DFPRIVILAGEPAYMENTID="PRIVILEGEPAYMENTID";
	public static final String QUALIFIED_COLUMN_DFEXISTINGLOANAMOUNT="DEAL.EXISTINGLOANAMOUNT";
	public static final String COLUMN_DFEXISTINGLOANAMOUNT="EXISTINGLOANAMOUNT";
	public static final String QUALIFIED_COLUMN_DFDEALPURPOSEID="DEAL.DEALPURPOSEID";
	public static final String COLUMN_DFDEALPURPOSEID="DEALPURPOSEID";
	public static final String QUALIFIED_COLUMN_DFBRANCHPROFILEID="DEAL.BRANCHPROFILEID";
	public static final String COLUMN_DFBRANCHPROFILEID="BRANCHPROFILEID";
	// SEAN GECF IV Document Type drop down: define the COLUMN and qualified column.
	public static final String QUALIFIED_COLUMN_DFIVDOCUMENTTYPEID="DEAL.INCOMEVERIFICATIONTYPEID";
	public static final String COLUMN_DFIVDOCUMENTTYPEID="INCOMEVERIFICATIONTYPEID";
	// END GECF IV Document Type drop down
	// SEAN DJ SPEC-Progress Advance Type July 21, 2005: define the column...
	public static final String QUALIFIED_COLUMN_DFPROGRESSADVANCETYPEID="DEAL.PROGRESSADVANCETYPEID";
	public static final String COLUMN_DFPROGRESSADVANCETYPEID="PROGRESSADVANCETYPEID";
	// SEAN DJ SPEC-PAT END
	public static final String QUALIFIED_COLUMN_DFDEALTYPEID="DEAL.DEALTYPEID";
	public static final String COLUMN_DFDEALTYPEID="DEALTYPEID";
	public static final String QUALIFIED_COLUMN_DFCROSSELLPROFILEID="DEAL.CROSSSELLPROFILEID";
	public static final String COLUMN_DFCROSSELLPROFILEID="CROSSSELLPROFILEID";
	public static final String QUALIFIED_COLUMN_DFSPECIALFEATUREID="DEAL.SPECIALFEATUREID";
	public static final String COLUMN_DFSPECIALFEATUREID="SPECIALFEATUREID";
	public static final String QUALIFIED_COLUMN_DFEXISTINGMTGNUMBER="DEAL.REFEXISTINGMTGNUMBER";
	public static final String COLUMN_DFEXISTINGMTGNUMBER="REFEXISTINGMTGNUMBER";
	public static final String QUALIFIED_COLUMN_DFRATELOCK="DEAL.RATELOCK";
	public static final String COLUMN_DFRATELOCK="RATELOCK";
	public static final String QUALIFIED_COLUMN_DFREFERENCEDEALTYPEID="DEAL.REFERENCEDEALTYPEID";
	public static final String COLUMN_DFREFERENCEDEALTYPEID="REFERENCEDEALTYPEID";
	public static final String QUALIFIED_COLUMN_DFREPAYMENTTYPEID="DEAL.REPAYMENTTYPEID";
	public static final String COLUMN_DFREPAYMENTTYPEID="REPAYMENTTYPEID";
	public static final String QUALIFIED_COLUMN_DFREFERENCEDEALNUMBER="DEAL.REFERENCEDEALNUMBER";
	public static final String COLUMN_DFREFERENCEDEALNUMBER="REFERENCEDEALNUMBER";
	public static final String QUALIFIED_COLUMN_DFSECONDARYFINANCING="DEAL.SECONDARYFINANCING";
	public static final String COLUMN_DFSECONDARYFINANCING="SECONDARYFINANCING";
	public static final String QUALIFIED_COLUMN_DFESTIMATEDCLOSINGDATE="DEAL.ESTIMATEDCLOSINGDATE";
	public static final String COLUMN_DFESTIMATEDCLOSINGDATE="ESTIMATEDCLOSINGDATE";
	public static final String QUALIFIED_COLUMN_DFFINANCEPROGRAMID="DEAL.FINANCEPROGRAMID";
	public static final String COLUMN_DFFINANCEPROGRAMID="FINANCEPROGRAMID";
	public static final String QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE="DEAL.FIRSTPAYMENTDATE";
    //FFATE
    public static final String QUALIFIED_COLUMN_DFFINANCINGWAIVERDATE="DEAL.FINANCINGWAIVERDATE";
    public static final String COLUMN_DFFINANCINGWAIVERDATE="FINANCINGWAIVERDATE";
    //FFATE
    //CR03
	public static final String QUALIFIED_COLUMN_DFCOMMITMENTEXPIRATIONDATE="DEAL.COMMITMENTEXPIRATIONDATE";
	public static final String COLUMN_DFCOMMITMENTEXPIRATIONDATE="COMMITMENTEXPIRATIONDATE";
    //CR03
	public static final String COLUMN_DFFIRSTPAYMENTDATE="FIRSTPAYMENTDATE";
	public static final String QUALIFIED_COLUMN_DFDEALTAXPAYORID="DEAL.TAXPAYORID";
	public static final String COLUMN_DFDEALTAXPAYORID="TAXPAYORID";
	public static final String QUALIFIED_COLUMN_DFMATURITYID="DEAL.MATURITYDATE";
	public static final String COLUMN_DFMATURITYID="MATURITYDATE";
	public static final String QUALIFIED_COLUMN_DFSOURCEAPPLICATIONID="DEAL.SOURCEAPPLICATIONID";
	public static final String COLUMN_DFSOURCEAPPLICATIONID="SOURCEAPPLICATIONID";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDTOTALINCOME="DEAL.COMBINEDTOTALINCOME";
	public static final String COLUMN_DFCOMBINEDTOTALINCOME="COMBINEDTOTALINCOME";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDTOTALLIABILITIES="DEAL.COMBINEDTOTALLIABILITIES";
	public static final String COLUMN_DFCOMBINEDTOTALLIABILITIES="COMBINEDTOTALLIABILITIES";
	public static final String QUALIFIED_COLUMN_DFCOMBINEDTOTALASSETS="DEAL.COMBINEDTOTALASSETS";
	public static final String COLUMN_DFCOMBINEDTOTALASSETS="COMBINEDTOTALASSETS";
	public static final String QUALIFIED_COLUMN_DFTOTALPROPERTYEXPENSES="DEAL.TOTALPROPERTYEXPENSES";
	public static final String COLUMN_DFTOTALPROPERTYEXPENSES="TOTALPROPERTYEXPENSES";
	public static final String QUALIFIED_COLUMN_DFMAXIMUMPRICIPALALLOWED="DEAL.MAXIMUMPRINCIPALAMOUNT";
	public static final String COLUMN_DFMAXIMUMPRICIPALALLOWED="MAXIMUMPRINCIPALAMOUNT";
	public static final String QUALIFIED_COLUMN_DFMINIMUMINCOMEREQUIRED="DEAL.MINIMUMINCOMEREQUIRED";
	public static final String COLUMN_DFMINIMUMINCOMEREQUIRED="MINIMUMINCOMEREQUIRED";
	public static final String QUALIFIED_COLUMN_DFMAXIMUMTDSEXPENSEALLOWED="DEAL.MAXIMUMTDSEXPENSESALLOWED";
	public static final String COLUMN_DFMAXIMUMTDSEXPENSEALLOWED="MAXIMUMTDSEXPENSESALLOWED";
	public static final String QUALIFIED_COLUMN_DFNETLOANAMOUNT="DEAL.NETLOANAMOUNT";
	public static final String COLUMN_DFNETLOANAMOUNT="NETLOANAMOUNT";
	public static final String QUALIFIED_COLUMN_DFADVANCEHOLD="DEAL.ADVANCEHOLD";
	public static final String COLUMN_DFADVANCEHOLD="ADVANCEHOLD";
	public static final String QUALIFIED_COLUMN_DFRATEDATE="DEAL.RATEDATE";
	public static final String COLUMN_DFRATEDATE="RATEDATE";
	public static final String QUALIFIED_COLUMN_DFPAPURCHASEPRICE="DEAL.PAPURCHASEPRICE";
	public static final String COLUMN_DFPAPURCHASEPRICE="PAPURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFMISTATUS="DEAL.MISTATUSID";
	public static final String COLUMN_DFMISTATUS="MISTATUSID";
	public static final String QUALIFIED_COLUMN_DFMIPAYORID="DEAL.MIPAYORID";
	public static final String COLUMN_DFMIPAYORID="MIPAYORID";
	public static final String QUALIFIED_COLUMN_DFMITYPEID="DEAL.MITYPEID";
	public static final String COLUMN_DFMITYPEID="MITYPEID";
	public static final String QUALIFIED_COLUMN_DFMIUPFRONTID="DEAL.MIUPFRONT";
	public static final String COLUMN_DFMIUPFRONTID="MIUPFRONT";
	public static final String QUALIFIED_COLUMN_DFMIINDICATORID="DEAL.MIINDICATORID";
	public static final String COLUMN_DFMIINDICATORID="MIINDICATORID";
	public static final String QUALIFIED_COLUMN_DFMIINDICATORDESCRIPTION="MIINDICATOR.MIIDESCRIPTION";
	public static final String COLUMN_DFMIINDICATORDESCRIPTION="MIIDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFMINSURERID="DEAL.MORTGAGEINSURERID";
	public static final String COLUMN_DFMINSURERID="MORTGAGEINSURERID";
	public static final String QUALIFIED_COLUMN_DFMIPOLICYNUMBER="DEAL.MIPOLICYNUMBER";
	public static final String COLUMN_DFMIPOLICYNUMBER="MIPOLICYNUMBER";
	public static final String QUALIFIED_COLUMN_DFMIEXISTINGPOLICYNUMBER="DEAL.MIEXISTINGPOLICYNUMBER";
	public static final String COLUMN_DFMIEXISTINGPOLICYNUMBER="MIEXISTINGPOLICYNUMBER";
	public static final String QUALIFIED_COLUMN_DFADDITIONALPRINCIPAL="DEAL.ADDITIONALPRINCIPAL";
	public static final String COLUMN_DFADDITIONALPRINCIPAL="ADDITIONALPRINCIPAL";
	public static final String QUALIFIED_COLUMN_DFPREPAYMENTOPTIONID="DEAL.PREPAYMENTOPTIONSID";
	public static final String COLUMN_DFPREPAYMENTOPTIONID="PREPAYMENTOPTIONSID";
	public static final String QUALIFIED_COLUMN_DFREQUIREDDOWNPAYMENT="DEAL.REQUIREDDOWNPAYMENT";
	public static final String COLUMN_DFREQUIREDDOWNPAYMENT="REQUIREDDOWNPAYMENT";
	public static final String QUALIFIED_COLUMN_DFCOMBAINEDTOTALPROPERTYEXPENCE="DEAL.COMBINEDTOTALPROPERTYEXP";
	public static final String COLUMN_DFCOMBAINEDTOTALPROPERTYEXPENCE="COMBINEDTOTALPROPERTYEXP";
	public static final String QUALIFIED_COLUMN_DFRETURNDATE="DEAL.RETURNDATE";
	public static final String COLUMN_DFRETURNDATE="RETURNDATE";
	public static final String QUALIFIED_COLUMN_DFPROGRESSADVANCE="DEAL.PROGRESSADVANCE";
	public static final String COLUMN_DFPROGRESSADVANCE="PROGRESSADVANCE";
    
    //CR03
    public static final String QUALIFIED_COLUMN_DFAUTOCALCCOMMITEXPIRYDATE="DEAL.AUTOCALCCOMMITEXPIRYDATE";
	public static final String COLUMN_DFAUTOCALCCOMMITEXPIRYDATE="AUTOCALCCOMMITEXPIRYDATE";
    //CR03

	public static final String QUALIFIED_COLUMN_DFADVANCETODATEAMOUNT="DEAL.ADVANCETODATEAMOUNT";
	public static final String COLUMN_DFADVANCETODATEAMOUNT="ADVANCETODATEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFADVANCENUMBER="DEAL.ADVANCENUMBER";
	public static final String COLUMN_DFADVANCENUMBER="ADVANCENUMBER";
	public static final String QUALIFIED_COLUMN_DFMIRUINTERVENTION="DEAL.MIRUINTERVENTION";
	public static final String COLUMN_DFMIRUINTERVENTION="MIRUINTERVENTION";
	public static final String QUALIFIED_COLUMN_DFPREQUALIFICATIONCERTNUM="DEAL.PREQUALIFICATIONMICERTNUM";
	public static final String COLUMN_DFPREQUALIFICATIONCERTNUM="PREQUALIFICATIONMICERTNUM";
	public static final String QUALIFIED_COLUMN_DFREFIPURPOSE="DEAL.REFIPURPOSE";
	public static final String COLUMN_DFREFIPURPOSE="REFIPURPOSE";
	public static final String QUALIFIED_COLUMN_DFREFIORIGPURCHASEPRICE="DEAL.REFIORIGPURCHASEPRICE";
	public static final String COLUMN_DFREFIORIGPURCHASEPRICE="REFIORIGPURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFREFIBLENDEDAMORTIZATION="DEAL.REFIBLENDEDAMORTIZATION";
	public static final String COLUMN_DFREFIBLENDEDAMORTIZATION="REFIBLENDEDAMORTIZATION";
	public static final String QUALIFIED_COLUMN_DFREFIORIGMTGAMOUNT="DEAL.REFIORIGMTGAMOUNT";
	public static final String COLUMN_DFREFIORIGMTGAMOUNT="REFIORIGMTGAMOUNT";
	public static final String QUALIFIED_COLUMN_DFREFIIMPROVEMENTSDESC="DEAL.REFIIMPROVEMENTSDESC";
	public static final String COLUMN_DFREFIIMPROVEMENTSDESC="REFIIMPROVEMENTSDESC";
	public static final String QUALIFIED_COLUMN_DFREFIIMPROVEMENTVALUE="DEAL.REFIIMPROVEMENTAMOUNT";
	public static final String COLUMN_DFREFIIMPROVEMENTVALUE="REFIIMPROVEMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFNEXTADVANCEAMOUNT="DEAL.NEXTADVANCEAMOUNT";
	public static final String COLUMN_DFNEXTADVANCEAMOUNT="NEXTADVANCEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFREFIORIGPURCHASEDATE="DEAL.REFIORIGPURCHASEDATE";
	public static final String COLUMN_DFREFIORIGPURCHASEDATE="REFIORIGPURCHASEDATE";
	public static final String QUALIFIED_COLUMN_DFREFICURMORTGAGEHOLDER="DEAL.REFICURMORTGAGEHOLDER";
	public static final String COLUMN_DFREFICURMORTGAGEHOLDER="REFICURMORTGAGEHOLDER";
	public static final String QUALIFIED_COLUMN_DFCHANNELMEDIA="DEAL.CHANNELMEDIA";
	public static final String COLUMN_DFCHANNELMEDIA="CHANNELMEDIA";

  //	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	public static final String QUALIFIED_COLUMN_DFRATEGUARANTEEPERIOD="DEAL.RATEGUARANTEEPERIOD";
	public static final String COLUMN_DFRATEGUARANTEEPERIOD="RATEGUARANTEEPERIOD";
  //	***** Change by NBC Impl. Team - Version 1.2 - End*****//

  //-Release2.1--//
	public static final String QUALIFIED_COLUMN_DFSCENARIODESCTRANSLATED="DEAL.SCENARIODESCRIPTION_TRANS";
	public static final String COLUMN_DFSCENARIODESCTRANSLATED="SCENARIODESCRIPTION_TRANS";

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 17July2003
  public static final String QUALIFIED_COLUMN_DFMIPOLICYNUMBERCMHC="DEAL.MIPOLICYNUMCMHC";
	public static final String COLUMN_DFMIPOLICYNUMBERCMHC="MIPOLICYNUMCMHC";
  public static final String QUALIFIED_COLUMN_DFMIPOLICYNUMBERGE="DEAL.MIPOLICYNUMGE";
	public static final String COLUMN_DFMIPOLICYNUMBERGE="MIPOLICYNUMGE";
  public static final String QUALIFIED_COLUMN_DFMIPOLICYNUMBERAIGUG="DEAL.MIPOLICYNUMAIGUG";
	public static final String COLUMN_DFMIPOLICYNUMBERAIGUG="MIPOLICYNUMAIGUG";
  public static final String QUALIFIED_COLUMN_DFMIPOLICYNUMBERPMI="DEAL.MIPOLICYNUMPMI";
	public static final String COLUMN_DFMIPOLICYNUMBERPMI="MIPOLICYNUMPMI";
  //================================================================

  //-- FXLink Phase II --//
  //--> New Field for Market Type Indicator
  //--> By Billy 18Nov2003
  public static final String QUALIFIED_COLUMN_DFMCCMARKETTYPE="DEAL.MCCMARKETTYPE";
	public static final String COLUMN_DFMCCMARKETTYPE="MCCMARKETTYPE";
  //================================================================
  //--DJ_LDI_CR--start--//
	public static final String QUALIFIED_COLUMN_DFPMNTPLUSLIFEDISABILITY="DEAL.PMNTPLUSLIFEDISABILITY";
	public static final String COLUMN_DFPMNTPLUSLIFEDISABILITY="PMNTPLUSLIFEDISABILITY";
  //--DJ_LDI_CR--end--//

  //--DJ_CR010--start//
	public static final String QUALIFIED_COLUMN_DFCOMMISIONCODE="DEAL.COMMISIONCODE";
	public static final String COLUMN_DFCOMMISIONCODE="COMMISIONCODE";
  //--DJ_CR010--end//

  //--DJ_CR203.1--start//
	public static final String QUALIFIED_COLUMN_DFPROPRIETAIREPLUSLOC="DEAL.PROPRIETAIREPLUSLOC";
	public static final String COLUMN_DFPROPRIETAIREPLUSLOC="PROPRIETAIREPLUSLOC";

	public static final String QUALIFIED_COLUMN_DFPROPRIETAIREPLUS="DEAL.PROPRIETAIREPLUS";
	public static final String COLUMN_DFPROPRIETAIREPLUS="PROPRIETAIREPLUS";

	public static final String QUALIFIED_COLUMN_DFMULTIPROJECT="DEAL.MULTIPROJECT";
	public static final String COLUMN_DFMULTIPROJECT="MULTIPROJECT";
  //--DJ_CR203.1--end//

  //--Release3.1--begins
  //--by Hiro Apr 13, 2006
  public static final String QUALIFIED_COLUMN_DFPRODUCTTYPEID="DEAL.PRODUCTTYPEID";
  public static final String COLUMN_DFPRODUCTTYPEID="PRODUCTTYPEID";

  public static final String QUALIFIED_COLUMN_DFREFIPRODUCTTYPEID="DEAL.REFIPRODUCTTYPEID";
  public static final String COLUMN_DFREFIPRODUCTTYPEID="REFIPRODUCTTYPEID";

  public static final String QUALIFIED_COLUMN_DFCMHCPRODUCTTRACKERIDENTIFIER="DEAL.CMHCPRODUCTTRACKERIDENTIFIER";
  public static final String COLUMN_DFCMHCPRODUCTTRACKERIDENTIFIER="CMHCPRODUCTTRACKERIDENTIFIER";
  
  public static final String QUALIFIED_COLUMN_DFLOCAMORTIZATIONMONTHS="DEAL.LOCAMORTIZATIONMONTHS";
  public static final String COLUMN_DFLOCAMORTIZATIONMONTHS="LOCAMORTIZATIONMONTHS";
  
  public static final String QUALIFIED_COLUMN_DFLOCINTERESTONLYMATURITYDATE="DEAL.LOCINTERESTONLYMATURITYDATE";
  public static final String COLUMN_DFLOCINTERESTONLYMATURITYDATE="LOCINTERESTONLYMATURITYDATE";
  
  public static final String QUALIFIED_COLUMN_DFLOCREPAYMENTTYPEID="DEAL.LOCREPAYMENTTYPEID";
  public static final String COLUMN_DFLOCREPAYMENTTYPEID="LOCREPAYMENTTYPEID";
  
  public static final String QUALIFIED_COLUMN_DFPROGRESSADVANCEINSPECTIONBY="DEAL.PROGRESSADVANCEINSPECTIONBY";
  public static final String COLUMN_DFPROGRESSADVANCEINSPECTIONBY="PROGRESSADVANCEINSPECTIONBY";
  
  public static final String QUALIFIED_COLUMN_DFREQUESTSTANDARDSERVICE="DEAL.REQUESTSTANDARDSERVICE";
  public static final String COLUMN_DFREQUESTSTANDARDSERVICE="REQUESTSTANDARDSERVICE";
  
  public static final String QUALIFIED_COLUMN_DFSELFDIRECTEDRRSP="DEAL.SELFDIRECTEDRRSP";
  public static final String COLUMN_DFSELFDIRECTEDRRSP="SELFDIRECTEDRRSP";
  
  //--Release3.1--ends

  //    ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
  public static final String QUALIFIED_COLUMN_DFAFFILIATIONPROGRAMID = "DEAL.AFFILIATIONPROGRAMID";
  public static final String COLUMN_DFAFFILIATIONPROGRAMID = "AFFILIATIONPROGRAMID";
  //    ***** Change by NBC Impl. Team - Version 1.3 - End *****//
  
  /***************MCM Impl team changes starts - XS_2.46 *******************/ 
	
	public static final String QUALIFIED_COLUMN_DFREFIADDITIONALINFORMATION="DEAL.REFIADDITIONALINFORMATION";
	public static final String COLUMN_DFREFIADDITIONALINFORMATION="REFIADDITIONALINFORMATION";
	
	/***************MCM Impl team changes ends - XS_2.46 *******************/
	
	// Ticket 267
	public static final String QUALIFIED_COLUMN_DFDEALSTATUSID = "DEAL.STATUSID";
	public static final String COLUMN_DFDEALSTATUSID = "STATUSID";
    
    // ***** Qualifying Rate ***** //
    public static final String QUALIFIED_COLUMN_DFQUALIFYINGOVERRIDEFLAG = "DEAL.QUALIFYINGOVERRIDEFLAG";
    public static final String COLUMN_DFQUALIFYINGOVERRIDEFLAG = "QUALIFYINGOVERRIDEFLAG";
    
    public static final String QUALIFIED_COLUMN_DFQUALIFYRATE = "DEAL.GDSTDS3YEARRATE";
    public static final String COLUMN_DFQUALIFYRATE = "GDSTDS3YEARRATE";
    
    public static final String QUALIFIED_COLUMN_DFOVERRIDEQUALPRODSTR = "DEAL.OVERRIDEQUALPROD_STR";
    public static final String COLUMN_DFOVERRIDEQUALPRODSTR = "OVERRIDEQUALPROD_STR";

    public static final String QUALIFIED_COLUMN_DFQUALIFYINGOVERRIDERATEFLAG = "DEAL.QUALIFYINGRATEOVERRIDEFLAG";
    public static final String COLUMN_DFQUALIFYINGOVERRIDERATEFLAG = "QUALIFYINGRATEOVERRIDEFLAG";
    
    //QC702
	public static final String QUALIFIED_COLUMN_DFCOMBINEDLENDINGVALUE="DEAL.COMBINEDLENDINGVALUE";
	public static final String COLUMN_DFCOMBINEDLENDINGVALUE="COMBINEDLENDINGVALUE";
    
    //  ***** Qualifying Rate ***** //
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFCOPYID,
                COLUMN_DFCOPYID,
                QUALIFIED_COLUMN_DFCOPYID,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFINSTITUTIONID,
                COLUMN_DFINSTITUTIONID,
                QUALIFIED_COLUMN_DFINSTITUTIONID,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCEFIRMPROFILEID,
				COLUMN_DFSOURCEFIRMPROFILEID,
				QUALIFIED_COLUMN_DFSOURCEFIRMPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSCENARIONUMBER,
				COLUMN_DFSCENARIONUMBER,
				QUALIFIED_COLUMN_DFSCENARIONUMBER,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSCENARIODESC,
				COLUMN_DFSCENARIODESC,
				QUALIFIED_COLUMN_DFSCENARIODESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSCENARIORECOMENDED,
				COLUMN_DFSCENARIORECOMENDED,
				QUALIFIED_COLUMN_DFSCENARIORECOMENDED,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSCENARIOLOCKED,
				COLUMN_DFSCENARIOLOCKED,
				QUALIFIED_COLUMN_DFSCENARIOLOCKED,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDGDS,
				COLUMN_DFCOMBINEDGDS,
				QUALIFIED_COLUMN_DFCOMBINEDGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINED3YEARGDS,
				COLUMN_DFCOMBINED3YEARGDS,
				QUALIFIED_COLUMN_DFCOMBINED3YEARGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDBORROWERGDS,
				COLUMN_DFCOMBINEDBORROWERGDS,
				QUALIFIED_COLUMN_DFCOMBINEDBORROWERGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDTDS,
				COLUMN_DFCOMBINEDTDS,
				QUALIFIED_COLUMN_DFCOMBINEDTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINED3YEARTDS,
				COLUMN_DFCOMBINED3YEARTDS,
				QUALIFIED_COLUMN_DFCOMBINED3YEARTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDBORROWERTDS,
				COLUMN_DFCOMBINEDBORROWERTDS,
				QUALIFIED_COLUMN_DFCOMBINEDBORROWERTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPURCHASEPRICE,
				COLUMN_DFTOTALPURCHASEPRICE,
				QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIENPOSITIONID,
				COLUMN_DFLIENPOSITIONID,
				QUALIFIED_COLUMN_DFLIENPOSITIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALDOWNPAYMENTAMOUNT,
				COLUMN_DFDEALDOWNPAYMENTAMOUNT,
				QUALIFIED_COLUMN_DFDEALDOWNPAYMENTAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOBID,
				COLUMN_DFLOBID,
				QUALIFIED_COLUMN_DFLOBID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLENDERPROFILEID,
				COLUMN_DFLENDERPROFILEID,
				QUALIFIED_COLUMN_DFLENDERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMTGPRODUCTID,
				COLUMN_DFMTGPRODUCTID,
				QUALIFIED_COLUMN_DFMTGPRODUCTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDLTV,
				COLUMN_DFCOMBINEDLTV,
				QUALIFIED_COLUMN_DFCOMBINEDLTV,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPREMIUMAMOUNT,
				COLUMN_DFMIPREMIUMAMOUNT,
				QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALDISCOUNT,
				COLUMN_DFDEALDISCOUNT,
				QUALIFIED_COLUMN_DFDEALDISCOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALLOANAMOUNT,
				COLUMN_DFTOTALLOANAMOUNT,
				QUALIFIED_COLUMN_DFTOTALLOANAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALPREMIUM,
				COLUMN_DFDEALPREMIUM,
				QUALIFIED_COLUMN_DFDEALPREMIUM,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAMORTIZATIONTERM,
				COLUMN_DFAMORTIZATIONTERM,
				QUALIFIED_COLUMN_DFAMORTIZATIONTERM,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBUYDOWNRATE,
				COLUMN_DFBUYDOWNRATE,
				QUALIFIED_COLUMN_DFBUYDOWNRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS,
				COLUMN_DFEFFECTIVEAMORTIZATIONINMONTHS,
				QUALIFIED_COLUMN_DFEFFECTIVEAMORTIZATIONINMONTHS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNETRATE,
				COLUMN_DFNETRATE,
				QUALIFIED_COLUMN_DFNETRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTTERMDESC,
				COLUMN_DFPAYMENTTERMDESC,
				QUALIFIED_COLUMN_DFPAYMENTTERMDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPIPAYMENTAMOUNT,
				COLUMN_DFPIPAYMENTAMOUNT,
				QUALIFIED_COLUMN_DFPIPAYMENTAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTUALPAYMENTTERM,
				COLUMN_DFACTUALPAYMENTTERM,
				QUALIFIED_COLUMN_DFACTUALPAYMENTTERM,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTFREQUENCYID,
				COLUMN_DFPAYMENTFREQUENCYID,
				QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESCROWPAYMENTAMOUNT,
				COLUMN_DFESCROWPAYMENTAMOUNT,
				QUALIFIED_COLUMN_DFESCROWPAYMENTAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPAYMENTAMOUNT,
				COLUMN_DFTOTALPAYMENTAMOUNT,
				QUALIFIED_COLUMN_DFTOTALPAYMENTAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIVILAGEPAYMENTID,
				COLUMN_DFPRIVILAGEPAYMENTID,
				QUALIFIED_COLUMN_DFPRIVILAGEPAYMENTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEXISTINGLOANAMOUNT,
				COLUMN_DFEXISTINGLOANAMOUNT,
				QUALIFIED_COLUMN_DFEXISTINGLOANAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALPURPOSEID,
				COLUMN_DFDEALPURPOSEID,
				QUALIFIED_COLUMN_DFDEALPURPOSEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHPROFILEID,
				COLUMN_DFBRANCHPROFILEID,
				QUALIFIED_COLUMN_DFBRANCHPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALTYPEID,
				COLUMN_DFDEALTYPEID,
				QUALIFIED_COLUMN_DFDEALTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		// SEAN GECF IV Document Type drop down: make association.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFIVDOCUMENTTYPEID,
				COLUMN_DFIVDOCUMENTTYPEID,
				QUALIFIED_COLUMN_DFIVDOCUMENTTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		// END GECF IV Document Type drop down

		// SEAN DJ SPEC-Progress Advance Type July 21, 2005: make association.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROGRESSADVANCETYPEID,
				COLUMN_DFPROGRESSADVANCETYPEID,
				QUALIFIED_COLUMN_DFPROGRESSADVANCETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		// SEAN DJ SPEC-PAT END

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCROSSELLPROFILEID,
				COLUMN_DFCROSSELLPROFILEID,
				QUALIFIED_COLUMN_DFCROSSELLPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSPECIALFEATUREID,
				COLUMN_DFSPECIALFEATUREID,
				QUALIFIED_COLUMN_DFSPECIALFEATUREID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEXISTINGMTGNUMBER,
				COLUMN_DFEXISTINGMTGNUMBER,
				QUALIFIED_COLUMN_DFEXISTINGMTGNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATELOCK,
				COLUMN_DFRATELOCK,
				QUALIFIED_COLUMN_DFRATELOCK,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCEDEALTYPEID,
				COLUMN_DFREFERENCEDEALTYPEID,
				QUALIFIED_COLUMN_DFREFERENCEDEALTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREPAYMENTTYPEID,
				COLUMN_DFREPAYMENTTYPEID,
				QUALIFIED_COLUMN_DFREPAYMENTTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCEDEALNUMBER,
				COLUMN_DFREFERENCEDEALNUMBER,
				QUALIFIED_COLUMN_DFREFERENCEDEALNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSECONDARYFINANCING,
				COLUMN_DFSECONDARYFINANCING,
				QUALIFIED_COLUMN_DFSECONDARYFINANCING,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESTIMATEDCLOSINGDATE,
				COLUMN_DFESTIMATEDCLOSINGDATE,
				QUALIFIED_COLUMN_DFESTIMATEDCLOSINGDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFINANCEPROGRAMID,
				COLUMN_DFFINANCEPROGRAMID,
				QUALIFIED_COLUMN_DFFINANCEPROGRAMID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTPAYMENTDATE,
				COLUMN_DFFIRSTPAYMENTDATE,
				QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFFINANCINGWAIVERDATE,
                COLUMN_DFFINANCINGWAIVERDATE,
                QUALIFIED_COLUMN_DFFINANCINGWAIVERDATE,
                java.sql.Timestamp.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
    
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMMITMENTEXPIRATIONDATE,
				COLUMN_DFCOMMITMENTEXPIRATIONDATE,
				QUALIFIED_COLUMN_DFCOMMITMENTEXPIRATIONDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALTAXPAYORID,
				COLUMN_DFDEALTAXPAYORID,
				QUALIFIED_COLUMN_DFDEALTAXPAYORID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMATURITYID,
				COLUMN_DFMATURITYID,
				QUALIFIED_COLUMN_DFMATURITYID,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSOURCEAPPLICATIONID,
				COLUMN_DFSOURCEAPPLICATIONID,
				QUALIFIED_COLUMN_DFSOURCEAPPLICATIONID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDTOTALINCOME,
				COLUMN_DFCOMBINEDTOTALINCOME,
				QUALIFIED_COLUMN_DFCOMBINEDTOTALINCOME,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDTOTALLIABILITIES,
				COLUMN_DFCOMBINEDTOTALLIABILITIES,
				QUALIFIED_COLUMN_DFCOMBINEDTOTALLIABILITIES,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDTOTALASSETS,
				COLUMN_DFCOMBINEDTOTALASSETS,
				QUALIFIED_COLUMN_DFCOMBINEDTOTALASSETS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPROPERTYEXPENSES,
				COLUMN_DFTOTALPROPERTYEXPENSES,
				QUALIFIED_COLUMN_DFTOTALPROPERTYEXPENSES,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMAXIMUMPRICIPALALLOWED,
				COLUMN_DFMAXIMUMPRICIPALALLOWED,
				QUALIFIED_COLUMN_DFMAXIMUMPRICIPALALLOWED,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMINIMUMINCOMEREQUIRED,
				COLUMN_DFMINIMUMINCOMEREQUIRED,
				QUALIFIED_COLUMN_DFMINIMUMINCOMEREQUIRED,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMAXIMUMTDSEXPENSEALLOWED,
				COLUMN_DFMAXIMUMTDSEXPENSEALLOWED,
				QUALIFIED_COLUMN_DFMAXIMUMTDSEXPENSEALLOWED,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNETLOANAMOUNT,
				COLUMN_DFNETLOANAMOUNT,
				QUALIFIED_COLUMN_DFNETLOANAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADVANCEHOLD,
				COLUMN_DFADVANCEHOLD,
				QUALIFIED_COLUMN_DFADVANCEHOLD,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATEDATE,
				COLUMN_DFRATEDATE,
				QUALIFIED_COLUMN_DFRATEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAPURCHASEPRICE,
				COLUMN_DFPAPURCHASEPRICE,
				QUALIFIED_COLUMN_DFPAPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMISTATUS,
				COLUMN_DFMISTATUS,
				QUALIFIED_COLUMN_DFMISTATUS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPAYORID,
				COLUMN_DFMIPAYORID,
				QUALIFIED_COLUMN_DFMIPAYORID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMITYPEID,
				COLUMN_DFMITYPEID,
				QUALIFIED_COLUMN_DFMITYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIUPFRONTID,
				COLUMN_DFMIUPFRONTID,
				QUALIFIED_COLUMN_DFMIUPFRONTID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIINDICATORID,
				COLUMN_DFMIINDICATORID,
				QUALIFIED_COLUMN_DFMIINDICATORID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIINDICATORDESCRIPTION,
				COLUMN_DFMIINDICATORDESCRIPTION,
				QUALIFIED_COLUMN_DFMIINDICATORDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		// Ticket 267
	    FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDEALSTATUSID,
					COLUMN_DFDEALSTATUSID,
					QUALIFIED_COLUMN_DFDEALSTATUSID,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMINSURERID,
				COLUMN_DFMINSURERID,
				QUALIFIED_COLUMN_DFMINSURERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPOLICYNUMBER,
				COLUMN_DFMIPOLICYNUMBER,
				QUALIFIED_COLUMN_DFMIPOLICYNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIEXISTINGPOLICYNUMBER,
				COLUMN_DFMIEXISTINGPOLICYNUMBER,
				QUALIFIED_COLUMN_DFMIEXISTINGPOLICYNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDITIONALPRINCIPAL,
				COLUMN_DFADDITIONALPRINCIPAL,
				QUALIFIED_COLUMN_DFADDITIONALPRINCIPAL,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREPAYMENTOPTIONID,
				COLUMN_DFPREPAYMENTOPTIONID,
				QUALIFIED_COLUMN_DFPREPAYMENTOPTIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREQUIREDDOWNPAYMENT,
				COLUMN_DFREQUIREDDOWNPAYMENT,
				QUALIFIED_COLUMN_DFREQUIREDDOWNPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBAINEDTOTALPROPERTYEXPENCE,
				COLUMN_DFCOMBAINEDTOTALPROPERTYEXPENCE,
				QUALIFIED_COLUMN_DFCOMBAINEDTOTALPROPERTYEXPENCE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRETURNDATE,
				COLUMN_DFRETURNDATE,
				QUALIFIED_COLUMN_DFRETURNDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROGRESSADVANCE,
				COLUMN_DFPROGRESSADVANCE,
				QUALIFIED_COLUMN_DFPROGRESSADVANCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        //CR03
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAUTOCALCCOMMITEXPIRYDATE,
				COLUMN_DFAUTOCALCCOMMITEXPIRYDATE,
				QUALIFIED_COLUMN_DFAUTOCALCCOMMITEXPIRYDATE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        //CR03

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADVANCETODATEAMOUNT,
				COLUMN_DFADVANCETODATEAMOUNT,
				QUALIFIED_COLUMN_DFADVANCETODATEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADVANCENUMBER,
				COLUMN_DFADVANCENUMBER,
				QUALIFIED_COLUMN_DFADVANCENUMBER,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIRUINTERVENTION,
				COLUMN_DFMIRUINTERVENTION,
				QUALIFIED_COLUMN_DFMIRUINTERVENTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREQUALIFICATIONCERTNUM,
				COLUMN_DFPREQUALIFICATIONCERTNUM,
				QUALIFIED_COLUMN_DFPREQUALIFICATIONCERTNUM,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFIPURPOSE,
				COLUMN_DFREFIPURPOSE,
				QUALIFIED_COLUMN_DFREFIPURPOSE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFIORIGPURCHASEPRICE,
				COLUMN_DFREFIORIGPURCHASEPRICE,
				QUALIFIED_COLUMN_DFREFIORIGPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFIBLENDEDAMORTIZATION,
				COLUMN_DFREFIBLENDEDAMORTIZATION,
				QUALIFIED_COLUMN_DFREFIBLENDEDAMORTIZATION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFIORIGMTGAMOUNT,
				COLUMN_DFREFIORIGMTGAMOUNT,
				QUALIFIED_COLUMN_DFREFIORIGMTGAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFIIMPROVEMENTSDESC,
				COLUMN_DFREFIIMPROVEMENTSDESC,
				QUALIFIED_COLUMN_DFREFIIMPROVEMENTSDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFIIMPROVEMENTVALUE,
				COLUMN_DFREFIIMPROVEMENTVALUE,
				QUALIFIED_COLUMN_DFREFIIMPROVEMENTVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNEXTADVANCEAMOUNT,
				COLUMN_DFNEXTADVANCEAMOUNT,
				QUALIFIED_COLUMN_DFNEXTADVANCEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFIORIGPURCHASEDATE,
				COLUMN_DFREFIORIGPURCHASEDATE,
				QUALIFIED_COLUMN_DFREFIORIGPURCHASEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFICURMORTGAGEHOLDER,
				COLUMN_DFREFICURMORTGAGEHOLDER,
				QUALIFIED_COLUMN_DFREFICURMORTGAGEHOLDER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCHANNELMEDIA,
				COLUMN_DFCHANNELMEDIA,
				QUALIFIED_COLUMN_DFCHANNELMEDIA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Release2.1--start//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSCENARIODESCTRANSLATED,
				COLUMN_DFSCENARIODESCTRANSLATED,
				QUALIFIED_COLUMN_DFSCENARIODESCTRANSLATED,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--Release2.1--end//

    //===============================================================
    // New fields to handle MIPolicyNum problem :
    //  the number lost if user cancel MI and re-send again later.
    // -- Modified by Billy 17July2003
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPOLICYNUMBERCMHC,
				COLUMN_DFMIPOLICYNUMBERCMHC,
				QUALIFIED_COLUMN_DFMIPOLICYNUMBERCMHC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPOLICYNUMBERGE,
				COLUMN_DFMIPOLICYNUMBERGE,
				QUALIFIED_COLUMN_DFMIPOLICYNUMBERGE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPOLICYNUMBERAIGUG,
				COLUMN_DFMIPOLICYNUMBERAIGUG,
				QUALIFIED_COLUMN_DFMIPOLICYNUMBERAIGUG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPOLICYNUMBERPMI,
				COLUMN_DFMIPOLICYNUMBERPMI,
				QUALIFIED_COLUMN_DFMIPOLICYNUMBERPMI,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //===========================================================
    //-- FXLink Phase II --//
    //--> New Field for Market Type Indicator
    //--> By Billy 18Nov2003
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMCCMARKETTYPE,
				COLUMN_DFMCCMARKETTYPE,
				QUALIFIED_COLUMN_DFMCCMARKETTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //===========================================================
    //--DJ_LDI_CR--start--//
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPMNTPLUSLIFEDISABILITY,
				COLUMN_DFPMNTPLUSLIFEDISABILITY,
				QUALIFIED_COLUMN_DFPMNTPLUSLIFEDISABILITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--DJ_LDI_CR--end--//

    //--DJ_CR010--start//
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMMISIONCODE,
				COLUMN_DFCOMMISIONCODE,
				QUALIFIED_COLUMN_DFCOMMISIONCODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--DJ_CR010--end//

    //--DJ_CR203.1--start//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPRIETAIREPLUSLOC,
				COLUMN_DFPROPRIETAIREPLUSLOC,
				QUALIFIED_COLUMN_DFPROPRIETAIREPLUSLOC,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPRIETAIREPLUS,
				COLUMN_DFPROPRIETAIREPLUS,
				QUALIFIED_COLUMN_DFPROPRIETAIREPLUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMULTIPROJECT,
				COLUMN_DFMULTIPROJECT,
				QUALIFIED_COLUMN_DFMULTIPROJECT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--DJ_CR203.1--end//
    
    //--Release3.1--begins
    //--by Hiro Apr 13, 2006
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFPRODUCTTYPEID,
        COLUMN_DFPRODUCTTYPEID,
        QUALIFIED_COLUMN_DFPRODUCTTYPEID, 
        java.math.BigDecimal.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        ""));   
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFREFIPRODUCTTYPEID,
        COLUMN_DFREFIPRODUCTTYPEID,
        QUALIFIED_COLUMN_DFREFIPRODUCTTYPEID, 
        java.math.BigDecimal.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        ""));   
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFPROGRESSADVANCEINSPECTIONBY,
        COLUMN_DFPROGRESSADVANCEINSPECTIONBY,
        QUALIFIED_COLUMN_DFPROGRESSADVANCEINSPECTIONBY, 
        String.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        "")); 
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFSELFDIRECTEDRRSP,
        COLUMN_DFSELFDIRECTEDRRSP,
        QUALIFIED_COLUMN_DFSELFDIRECTEDRRSP, 
        String.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        ""));
    
    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER,
        COLUMN_DFCMHCPRODUCTTRACKERIDENTIFIER,
        QUALIFIED_COLUMN_DFCMHCPRODUCTTRACKERIDENTIFIER, 
        String.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        ""));   

    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFLOCAMORTIZATIONMONTHS,
        COLUMN_DFLOCAMORTIZATIONMONTHS,
        QUALIFIED_COLUMN_DFLOCAMORTIZATIONMONTHS, 
        java.math.BigDecimal.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        ""));   

    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFLOCINTERESTONLYMATURITYDATE,
        COLUMN_DFLOCINTERESTONLYMATURITYDATE,
        QUALIFIED_COLUMN_DFLOCINTERESTONLYMATURITYDATE, 
        java.sql.Timestamp.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        ""));   

    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFLOCREPAYMENTTYPEID,
        COLUMN_DFLOCREPAYMENTTYPEID,
        QUALIFIED_COLUMN_DFLOCREPAYMENTTYPEID, 
        java.math.BigDecimal.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        ""));   

    FIELD_SCHEMA.addFieldDescriptor(
        new QueryFieldDescriptor(
        FIELD_DFREQUESTSTANDARDSERVICE,
        COLUMN_DFREQUESTSTANDARDSERVICE,
        QUALIFIED_COLUMN_DFREQUESTSTANDARDSERVICE, 
        String.class, 
        false,
        false, 
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, 
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, 
        ""));   

    //--Release3.1--ends

    //  ***** Change by NBC Impl. Team - Version 1.2 - Start *****//    
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATEGUARANTEEPERIOD,
				COLUMN_DFRATEGUARANTEEPERIOD,
				QUALIFIED_COLUMN_DFRATEGUARANTEEPERIOD,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));  
    //  ***** Change by NBC Impl. Team - Version 1.2 - End*****//

    //  ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
			FIELD_DFCASHBACKINDOLLARS, COLUMN_DFCASHBACKINDOLLARS,
			QUALIFIED_COLUMN_DFCASHBACKINDOLLARS,
			java.math.BigDecimal.class, false, false,
			QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
			QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
			FIELD_DFCASHBACKINPERCENTAGE,// logicalName
			COLUMN_DFCASHBACKINPERCENTAGE,// columnName
			QUALIFIED_COLUMN_DFCASHBACKINPERCENTAGE,// qualifiedColumnName
			java.math.BigDecimal.class,// fieldClass
			false,// isKey
			false,// isComputedField
			QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, // insertValueSource
																	// The
																	// application
																	// provides
																	// the
																	// inserted
																	// value
																	// (default)
			"",// insertFormula
			QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, // onEmptyValuePolicy
															// If no value
															// is provided,
															// don't supply
															// a value
															// (default)
			"")); // emptyFormula

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
			FIELD_DFCASHBACKOVERRIDEINDOLLARS,
			COLUMN_DFCASHBACKOVERRIDEINDOLLARS,
			QUALIFIED_COLUMN_DFCASHBACKOVERRIDEINDOLLARS, String.class,
			false, false,
			QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
			QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

    // ***** Change by NBC Impl. Team - Version 1.2 - End*****//
    //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//    
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAFFILIATIONPROGRAMID,
				COLUMN_DFAFFILIATIONPROGRAMID,
				QUALIFIED_COLUMN_DFAFFILIATIONPROGRAMID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));  
    //  ***** Change by NBC Impl. Team - Version 1.3 - End*****//
    
    /***************MCM Impl team changes starts - XS_2.46 *******************/ 
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFIADDITIONALINFORMATION,
				COLUMN_DFREFIADDITIONALINFORMATION,
				QUALIFIED_COLUMN_DFREFIADDITIONALINFORMATION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
	/***************MCM Impl team changes ends - XS_2.46 *******************/ 

    //***** Qualify Rate ******//
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
            FIELD_DFQUALIFYINGOVERRIDEFLAG,
            COLUMN_DFQUALIFYINGOVERRIDEFLAG,
            QUALIFIED_COLUMN_DFQUALIFYINGOVERRIDEFLAG, String.class,
            false, false,
            QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
            QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFQUALIFYRATE,
                COLUMN_DFQUALIFYRATE,
                QUALIFIED_COLUMN_DFQUALIFYRATE,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFOVERRIDEQUALPROD,
              COLUMN_DFOVERRIDEQUALPRODSTR,
              QUALIFIED_COLUMN_DFOVERRIDEQUALPRODSTR,
              String.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
            FIELD_DFQUALIFYINGOVERRIDERATEFLAG,
            COLUMN_DFQUALIFYINGOVERRIDERATEFLAG,
            QUALIFIED_COLUMN_DFQUALIFYINGOVERRIDERATEFLAG, String.class,
            false, false,
            QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
            QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
    
	FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOMBINEDLENDINGVALUE,
				COLUMN_DFTOTALPURCHASEPRICE,
				QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}
}

