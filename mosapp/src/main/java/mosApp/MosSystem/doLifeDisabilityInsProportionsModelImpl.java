package mosApp.MosSystem;



import java.sql.SQLException;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doLifeDisabilityInsProportionsModelImpl extends QueryModelBase
  implements doLifeDisabilityInsProportionsModel
{
  /**
   *
   *
   */
  public doLifeDisabilityInsProportionsModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);

    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

    setFieldSchema(FIELD_SCHEMA);

    initialize();

  }


  /**
   *
   *
   */
  public void initialize()
  {
  }


  /**
   *
   *
   */
  protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
    throws ModelControlException
  {
    //logger = SysLog.getSysLogger("LDIP");
    //logger.debug("VLAD ===> LDIP@SQLBeforeExecute: " + sql);

    return sql;

  }


  /**
   *
   *
   */
  protected void afterExecute(ModelExecutionContext context, int queryType)
    throws ModelControlException
  {
    //logger = SysLog.getSysLogger("LDII");
    //logger.debug("DOMWQPA@afterExecute::Size: " + this.getSize());
    //logger.debug("DOMWQPA@afterExecute::SQLTemplate: " + this.getSelectSQL());

    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
      getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      //Convert PAYMENTFREQUENCY Desc.
      this.setDfPFDescription(BXResources.getPickListDescription(
          theSessionState.getDealInstitutionId(),
          "PAYMENTFREQUENCY", this.getDfPFDescription(), languageId));
    }
  }

  /**
   *
   *
   */
  protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
  {
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
  }


  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDEALID,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
  }


  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID,value);
  }

  /**
   *
   *
   */
  public void setDfBorrowerId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFBORROWERID,value);
  }


  /**
   *
   *
   */

  public java.math.BigDecimal getDfBorrowerId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
  }

  /**
   *
   *
   */
  public String getDfFirstBorrowerName()
  {
    return (String)getValue(FIELD_DFBORROWERFIRSTNAME);
  }

  /**
   *
   *
   */
  public void setDfBorrowerFirstName(String value)
  {
    setValue(FIELD_DFBORROWERFIRSTNAME,value);
  }

  /**
   *
   *
   */
  public String getDfLastBorrowerName()
  {
    return (String)getValue(FIELD_DFBORROWERLASTNAME);
  }

  /**
   *
   *
   */
  public void setDfBorrowerLastName(String value)
  {
    setValue(FIELD_DFBORROWERLASTNAME,value);
  }

  /**
   *
   *
   */
  public String getDfCopyType()
  {
    return (String)getValue(FIELD_DFCOPYTYPE);
  }

  /**
   *
   *
   */
  public void setDfCopyType(String value)
  {
    setValue(FIELD_DFCOPYTYPE,value);
  }

  /**
   *
   *
   */
  public void setDfCopyID(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID,value);
  }

  /**
   *
   *
   */
  public String getDfIPDesc()
  {
    return (String)getValue(FIELD_DFIPDESC);
  }

  /**
   *
   *
   */
  public void setDfIPDesc(String value)
  {
    setValue(FIELD_DFIPDESC,value);
  }

  /**
   *
   *
   */
  public void setDfTotalLoanAmount(java.math.BigDecimal value)
  {
    setValue(FIELD_DFTOTALLOANAMOUNT,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfTotalLoanAmount()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFTOTALLOANAMOUNT);
  }

  /**
   *
   *
   */
  public void setDfInsuranceProportionsId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFINSURANCEPROPORTIONSID,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfInsuranceProportionsId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFINSURANCEPROPORTIONSID);
  }

  /**
   *
   *
   */
  public void setDfPaymentFrequencyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPAYMENTFREQUENCYID,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfPaymentFrequencyId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPAYMENTFREQUENCYID);
  }

  /**
   *
   *
   */
  public String getDfPFDescription()
  {
    return (String)getValue(FIELD_DFPFDESCRIPTION);
  }

  /**
   *
   *
   */
  public void setDfPFDescription(String value)
  {
    setValue(FIELD_DFPFDESCRIPTION,value);
  }

  /**
   *
   *
   */
  public void setDfPremiumAmount(java.math.BigDecimal value)
  {
    setValue(FIELD_DFMIPREMIUMAMOUNT,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfPremiumAmount()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFMIPREMIUMAMOUNT);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfLifePercentCoverageReq()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLIFEPERCENTCOVERAGEREQ);
  }

  /**
   *
   *
   */
  public void setDfLifePercentCoverageReq(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFEPERCENTCOVERAGEREQ,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPIPaymentAmount()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPIPAYMENTAMOUNT);
  }

  /**
   *
   *
   */
  public void setDfPIPaymentAmount(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPIPAYMENTAMOUNT,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfNetRate()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFNETRATE);
  }


  /**
   *
   *
   */
  public void setDfNetRate(java.math.BigDecimal value)
  {
    setValue(FIELD_DFNETRATE,value);
  }

  /**
   *
   *
   */
  public void setDfLifePremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLIFEPREMIUM,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfLifePremium()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFLIFEPREMIUM);
  }

  /**
   *
   *
   */
  public void setDfDisabilityPremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDISABILITYPREMIUM,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDisabilityPremium()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFDISABILITYPREMIUM);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPmntInclLifeDisability()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPMNTINCLLIFEDISABILITY);
  }

  /**
   *
   *
   */
  public void setDfPmntInclLifeDisability(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPMNTINCLLIFEDISABILITY,value);
  }

  /**
   *
   *
   */
  public void setDfTotalRateInclLifeDisability(java.math.BigDecimal value)
  {
    setValue(FIELD_DFTOTALRATEINCLLIFEDISABILITY,value);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTotalRateInclLifeDisability()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFTOTALRATEINCLLIFEDISABILITY);
  }

  /**
   *
   *
   */
  public java.math.BigDecimal getDfInterestRateIncrement()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFADDRATEFORLIFEDISABILITY);
  }

  /**
   *
   *
   */
  public void stDfInterestRateIncrement(java.math.BigDecimal value)
  {
    setValue(FIELD_DFADDRATEFORLIFEDISABILITY,value);
  }

  /**
   *
   *
   */
  public void setDfCombinedLDPremium(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOMBINEDLDPREMIUM,value);
  }

  /**
   *
   *
   */

  public java.math.BigDecimal getDfCombindedLDPremium()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFCOMBINEDLDPREMIUM);
  }

  /**
   * MetaData object test method to verify the SYNTHETIC new
   * field for the reogranized SQL_TEMPLATE query.
   *
   */
  /**
  public void setResultSet(ResultSet value)
  {
      logger = SysLog.getSysLogger("METADATA");

      try
      {
         super.setResultSet(value);
         if( value != null)
         {
             ResultSetMetaData metaData = value.getMetaData();
             int columnCount = metaData.getColumnCount();
             logger.debug("===============================================");
                        logger.debug("Testing MetaData Object for new synthetic fields for" +
                               " doDealEntrySourceInfoModel");
             logger.debug("NumberColumns: " + columnCount);
             for(int i = 0; i < columnCount ; i++)
              {
                   logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
              }
              logger.debug("================================================");
          }
      }
      catch (SQLException e)
      {
            logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
      }
   }
  **/

  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

  //////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  public static final String DATA_SOURCE_NAME="jdbc/orcl";

  public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL BORROWER.BORROWERID, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERLASTNAME, " +
    "DEAL.DEALID, DEAL.COPYID, DEAL.COPYTYPE, DEAL.TOTALLOANAMOUNT, " +
    "BORROWER.INSURANCEPROPORTIONSID, INSURANCEPROPORTIONS.IPDESC, " +
    "DEAL.PAYMENTFREQUENCYID, to_char(DEAL.PAYMENTFREQUENCYID) PAYMENTFREQUENCYID_STR, DEAL.MIPREMIUMAMOUNT, " +
    "BORROWER.LIFEPERCENTCOVERAGEREQ, DEAL.PANDIPAYMENTAMOUNT, DEAL.NETINTERESTRATE, " +
    "DEAL.LIFEPREMIUM, DEAL.DISABILITYPREMIUM, DEAL.COMBINEDPREMIUM, " +
    "DEAL.PMNTPLUSLIFEDISABILITY,  DEAL.INTERESTRATEINCLIFEDISABILITY, DEAL.INTERESTRATEINCREMENT " +
    "FROM BORROWER, DEAL, PAYMENTFREQUENCY, " +
    "INSURANCEPROPORTIONS __WHERE__ ";

  public static final String MODIFYING_QUERY_TABLE_NAME=
    "BORROWER, DEAL, PAYMENTFREQUENCY, " +
    "INSURANCEPROPORTIONS ";

  public static final String STATIC_WHERE_CRITERIA=
    "(DEAL.COPYID  =  BORROWER.COPYID) AND " +
    "(DEAL.DEALID = BORROWER.DEALID) AND " +
    "(INSURANCEPROPORTIONS.INSURANCEPROPORTIONSID = BORROWER.INSURANCEPROPORTIONSID) AND " +
    "(DEAL.PAYMENTFREQUENCYID = PAYMENTFREQUENCY.PAYMENTFREQUENCYID) AND " +
    "(DEAL.INSTITUTIONPROFILEID = BORROWER.INSTITUTIONPROFILEID) AND " +
    "(BORROWER.PRIMARYBORROWERFLAG = 'Y') ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
  public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
  public static final String COLUMN_DFDEALID="DEALID";

  public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
  public static final String COLUMN_DFCOPYID="COPYID";

  public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
  public static final String COLUMN_DFBORROWERID="BORROWERID";

  public static final String QUALIFIED_COLUMN_DFBORROWERFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
  public static final String COLUMN_DFBORROWERFIRSTNAME="BORROWERFIRSTNAME";

  public static final String QUALIFIED_COLUMN_DFBORROWERLASTNAME="BORROWER.BORROWERLASTNAME";
  public static final String COLUMN_DFBORROWERLASTNAME="BORROWERLASTNAME";

  public static final String QUALIFIED_COLUMN_COPYID="DEAL.COPYID";
  public static final String COLUMN_COPYID="COPYID";

  public static final String QUALIFIED_COLUMN_COPYTYPE="DEAL.COPYTYPE";
  public static final String COLUMN_COPYTYPE="COPYTYPE";

  public static final String QUALIFIED_COLUMN_DFTOTALLOANAMOUNT="DEAL.TOTALLOANAMOUNT";
  public static final String COLUMN_DFTOTALLOANAMOUNT="TOTALLOANAMOUNT";

  public static final String QUALIFIED_COLUMN_DFINSURANCEPROPORTIONSID="BORROWER.INSURANCEPROPORTIONSID";
  public static final String COLUMN_DFINSURANCEPROPORTIONSID="INSURANCEPROPORTIONSID";

  public static final String QUALIFIED_COLUMN_DFIPDESC="INSURANCEPROPORTIONS.IPDESC";
  public static final String COLUMN_DFIPDESC="IPDESC";

  public static final String QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID="DEAL.PAYMENTFREQUENCYID";
  public static final String COLUMN_DFPAYMENTFREQUENCYID="PAYMENTFREQUENCYID";

  public static final String QUALIFIED_COLUMN_DFPFDESCRIPTION="DEAL.PAYMENTFREQUENCYID_STR";
  public static final String COLUMN_DFPFDESCRIPTION="PAYMENTFREQUENCYID_STR";

  public static final String QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT="DEAL.MIPREMIUMAMOUNT";
  public static final String COLUMN_DFMIPREMIUMAMOUNT="MIPREMIUMAMOUNT";

  public static final String QUALIFIED_COLUMN_DFLIFEPERCENTCOVERAGEREQ="BORROWER.LIFEPERCENTCOVERAGEREQ";
  public static final String COLUMN_DFLIFEPERCENTCOVERAGEREQ="LIFEPERCENTCOVERAGEREQ";

  public static final String QUALIFIED_COLUMN_DFPIPAYMENTAMOUNT="DEAL.PANDIPAYMENTAMOUNT";
  public static final String COLUMN_DFPIPAYMENTAMOUNT="PANDIPAYMENTAMOUNT";

  public static final String QUALIFIED_COLUMN_DFNETRATE="DEAL.NETINTERESTRATE";
  public static final String COLUMN_DFNETRATE="NETINTERESTRATE";

  public static final String QUALIFIED_COLUMN_DFLIFEPREMIUM="DEAL.LIFEPREMIUM";
  public static final String COLUMN_DFLIFEPREMIUM="LIFEPREMIUM";

  public static final String QUALIFIED_COLUMN_DFDISABILITYPREMIUM="DEAL.DISABILITYPREMIUM";
  public static final String COLUMN_DFDISABILITYPREMIUM="DISABILITYPREMIUM";

  public static final String QUALIFIED_COLUMN_DFCOMBINEDLDPREMIUM="DEAL.COMBINEDPREMIUM";
  public static final String COLUMN_DFCOMBINEDLDPREMIUM="COMBINEDPREMIUM";

  public static final String QUALIFIED_COLUMN_DFPMNTINCLLIFEDISABILITY="DEAL.PMNTPLUSLIFEDISABILITY";
  public static final String COLUMN_DFPMNTINCLLIFEDISABILITY="PMNTPLUSLIFEDISABILITY";

  public static final String QUALIFIED_COLUMN_DFTOTALRATEINCLLIFEDISABILITY="DEAL.INTERESTRATEINCLIFEDISABILITY";
  public static final String COLUMN_DFTOTALRATEINCLLIFEDISABILITY="INTERESTRATEINCLIFEDISABILITY";

  public static final String QUALIFIED_COLUMN_DFADDRATEFORLIFEDISABILITY="DEAL.INTERESTRATEINCREMENT";
  public static final String COLUMN_DFADDRATEFORLIFEDISABILITY="INTERESTRATEINCREMENT";

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  static
  {
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDEALID,
        COLUMN_DFDEALID,
        QUALIFIED_COLUMN_DFDEALID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOPYID,
        COLUMN_DFCOPYID,
        QUALIFIED_COLUMN_DFCOPYID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERID,
        COLUMN_DFBORROWERID,
        QUALIFIED_COLUMN_DFBORROWERID,
        java.sql.Timestamp.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERFIRSTNAME,
        COLUMN_DFBORROWERFIRSTNAME,
        QUALIFIED_COLUMN_DFBORROWERFIRSTNAME,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFBORROWERLASTNAME,
        COLUMN_DFBORROWERLASTNAME,
        QUALIFIED_COLUMN_DFBORROWERLASTNAME,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOPYID,
        COLUMN_COPYID,
        QUALIFIED_COLUMN_COPYID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOPYTYPE,
        COLUMN_COPYTYPE,
        QUALIFIED_COLUMN_COPYTYPE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFTOTALLOANAMOUNT,
        COLUMN_DFTOTALLOANAMOUNT,
        QUALIFIED_COLUMN_DFTOTALLOANAMOUNT,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

      FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFINSURANCEPROPORTIONSID,
        COLUMN_DFINSURANCEPROPORTIONSID,
        QUALIFIED_COLUMN_DFINSURANCEPROPORTIONSID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFIPDESC,
        COLUMN_DFIPDESC,
        QUALIFIED_COLUMN_DFIPDESC,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPAYMENTFREQUENCYID,
        COLUMN_DFPAYMENTFREQUENCYID,
        QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPFDESCRIPTION,
        COLUMN_DFPFDESCRIPTION,
        QUALIFIED_COLUMN_DFPFDESCRIPTION,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFMIPREMIUMAMOUNT,
        COLUMN_DFMIPREMIUMAMOUNT,
        QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFEPERCENTCOVERAGEREQ,
        COLUMN_DFLIFEPERCENTCOVERAGEREQ,
        QUALIFIED_COLUMN_DFLIFEPERCENTCOVERAGEREQ,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPIPAYMENTAMOUNT,
        COLUMN_DFPIPAYMENTAMOUNT,
        QUALIFIED_COLUMN_DFPIPAYMENTAMOUNT,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFNETRATE,
        COLUMN_DFNETRATE,
        QUALIFIED_COLUMN_DFNETRATE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFLIFEPREMIUM,
        COLUMN_DFLIFEPREMIUM,
        QUALIFIED_COLUMN_DFLIFEPREMIUM,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDISABILITYPREMIUM,
        COLUMN_DFDISABILITYPREMIUM,
        QUALIFIED_COLUMN_DFDISABILITYPREMIUM,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPMNTINCLLIFEDISABILITY,
        COLUMN_DFPMNTINCLLIFEDISABILITY,
        QUALIFIED_COLUMN_DFPMNTINCLLIFEDISABILITY,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOMBINEDLDPREMIUM,
        COLUMN_DFCOMBINEDLDPREMIUM,
        QUALIFIED_COLUMN_DFCOMBINEDLDPREMIUM,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFTOTALRATEINCLLIFEDISABILITY,
        COLUMN_DFTOTALRATEINCLLIFEDISABILITY,
        QUALIFIED_COLUMN_DFTOTALRATEINCLLIFEDISABILITY,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFADDRATEFORLIFEDISABILITY,
        COLUMN_DFADDRATEFORLIFEDISABILITY,
        QUALIFIED_COLUMN_DFADDRATEFORLIFEDISABILITY,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));
  }
}
