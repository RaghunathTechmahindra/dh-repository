package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doApplicantEntryAddressSelectModelImpl extends QueryModelBase
	implements doApplicantEntryAddressSelectModel
{
	/**
	 *
	 *
	 */
	public doApplicantEntryAddressSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerAddressId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERADDRESSID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerAddressId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERADDRESSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAddrId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADDRID);
	}


	/**
	 *
	 *
	 */
	public void setDfAddrId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADDRID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAddrCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADDRCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfAddrCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADDRCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine1()
	{
		return (String)getValue(FIELD_DFADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine1(String value)
	{
		setValue(FIELD_DFADDRESSLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProvinceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROVINCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfProvinceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROVINCEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalFSA()
	{
		return (String)getValue(FIELD_DFPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalFSA(String value)
	{
		setValue(FIELD_DFPOSTALFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalLDU()
	{
		return (String)getValue(FIELD_DFPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalLDU(String value)
	{
		setValue(FIELD_DFPOSTALLDU,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine2()
	{
		return (String)getValue(FIELD_DFADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine2(String value)
	{
		setValue(FIELD_DFADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAddressStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADDRESSSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADDRESSSTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMonthsAtAddress()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMONTHSATADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfMonthsAtAddress(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMONTHSATADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResidentialStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFRESIDENTIALSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfResidentialStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFRESIDENTIALSTATUSID,value);
	}

//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//


	/**
	 * getDfStreetNumber
	 * @return  String <br>
	 * 	StreetNumber
	 */
	public String getDfStreetNumber()
	{
		return (String)getValue(FIELD_DFSTREETNUMBER);
	}


	/**
	 * setDfStreetNumber
	 * @param  int <br>
	 * 	streetNumber
	 */
	public void setDfStreetNumber(String value)
	{
		setValue(FIELD_DFSTREETNUMBER,value);
	}

	/**
	 * getDfStreetName
	 * @return  String <br>
	 * 	streetName
	 */
	public String getDfStreetName()
	{
		return (String)getValue(FIELD_DFSTREETNAME);
	}

	/**
	 * setDfStreetName
	 * @param  String <br>
	 * 	streetName
	 */
	public void setDfStreetName(String value)
	{
		setValue(FIELD_DFSTREETNAME,value);
	}

	
	/**
	 * getDfStreetTypeId
	 * @return  int <br>
	 * 	streetTypeId
	 */
	public java.math.BigDecimal getDfStreetTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSTREETTYPEID);
	}


	/**
	 * setDfStreetTypeId
	 * @param  int <br>
	 * 	streetTypeId
	 */
	public void setDfStreetTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSTREETTYPEID,value);
	}

	/**
	 * getDfStreetDirectionId
	 * @return  int <br>
	 * streetDirectionId
	 */
	
	public java.math.BigDecimal getDfStreetDirectionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDIRECTIONTYPEID);
	}


	/**
	 * setDfStreetDirectionId
	 * @param  int <br>
	 * 	streetDirectionId
	 */
	public void setDfStreetDirectionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDIRECTIONTYPEID,value);
	}
		
	
	/**
	 * getDfUnitNumber
	 * @return  String <br>
	 * unitNumber
	 */
	public String getDfUnitNumber()
	{
		return (String)getValue(FIELD_DFUNITNUMBER);
	}


	/**
	 * setDfStreetDirectionId
	 * @param  String <br>
	 * 	unitNumber
	 */
	public void setDfUnitNumber(String value)
	{
		setValue(FIELD_DFUNITNUMBER,value);
	}
	
//	***** Change by NBC Impl. Team - Version 1.2 - End *****//



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BORROWERADDRESS.BORROWERID, BORROWERADDRESS.BORROWERADDRESSID, BORROWERADDRESS.COPYID, ADDR.ADDRID, ADDR.COPYID, ADDR.ADDRESSLINE1, ADDR.CITY, ADDR.PROVINCEID, ADDR.POSTALFSA, ADDR.POSTALLDU, ADDR.ADDRESSLINE2, ADDR.STREETNUMBER, ADDR.STREETNAME, ADDR.STREETTYPEID, ADDR.STREETDIRECTIONID, ADDR.UNITNUMBER, BORROWERADDRESS.BORROWERADDRESSTYPEID, BORROWERADDRESS.MONTHSATADDRESS, BORROWERADDRESS.RESIDENTIALSTATUSID FROM BORROWERADDRESS, ADDR  __WHERE__  ORDER BY BORROWERADDRESS.BORROWERADDRESSID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="BORROWERADDRESS, ADDR";
	public static final String STATIC_WHERE_CRITERIA=
	    " borroweraddress.institutionprofileid = addr.institutionprofileid " + 
	    " AND BORROWERADDRESS.ADDRID  =  ADDR.ADDRID ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWERADDRESS.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFBORROWERADDRESSID="BORROWERADDRESS.BORROWERADDRESSID";
	public static final String COLUMN_DFBORROWERADDRESSID="BORROWERADDRESSID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWERADDRESS.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFADDRID="ADDR.ADDRID";
	public static final String COLUMN_DFADDRID="ADDRID";
	public static final String QUALIFIED_COLUMN_DFADDRCOPYID="ADDR.COPYID";
	public static final String COLUMN_DFADDRCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFADDRESSLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFCITY="ADDR.CITY";
	public static final String COLUMN_DFCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFPROVINCEID="ADDR.PROVINCEID";
	public static final String COLUMN_DFPROVINCEID="PROVINCEID";
	public static final String QUALIFIED_COLUMN_DFPOSTALFSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFPOSTALFSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFPOSTALLDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFPOSTALLDU="POSTALLDU";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFADDRESSLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFADDRESSSTATUSID="BORROWERADDRESS.BORROWERADDRESSTYPEID";
	public static final String COLUMN_DFADDRESSSTATUSID="BORROWERADDRESSTYPEID";
	public static final String QUALIFIED_COLUMN_DFMONTHSATADDRESS="BORROWERADDRESS.MONTHSATADDRESS";
	public static final String COLUMN_DFMONTHSATADDRESS="MONTHSATADDRESS";
	public static final String QUALIFIED_COLUMN_DFRESIDENTIALSTATUSID="BORROWERADDRESS.RESIDENTIALSTATUSID";
	public static final String COLUMN_DFRESIDENTIALSTATUSID="RESIDENTIALSTATUSID";

	//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	public static final String QUALIFIED_COLUMN_DFSTREETNUMBER="ADDR.STREETNUMBER";
	public static final String COLUMN_DFSTREETNUMBER="STREETNUMBER";

	public static final String QUALIFIED_COLUMN_DFSTREETNAME="ADDR.STREETNAME";
	public static final String COLUMN_DFSTREETNAME="STREETNAME";
	
	public static final String QUALIFIED_COLUMN_DFSTREETTYPEID="ADDR.STREETTYPEID";
	public static final String COLUMN_DFSTREETTYPEID="STREETTYPEID";

	public static final String QUALIFIED_COLUMN_DFDIRECTIONTYPEID="ADDR.STREETDIRECTIONID";
	public static final String COLUMN_DFDIRECTIONTYPEID="STREETDIRECTIONID";

	public static final String QUALIFIED_COLUMN_DFUNITNUMBER="ADDR.UNITNUMBER";
	public static final String COLUMN_DFUNITNUMBER="UNITNUMBER";
//	***** Change by NBC Impl. Team - Version 1.2 - End *****//


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERADDRESSID,
				COLUMN_DFBORROWERADDRESSID,
				QUALIFIED_COLUMN_DFBORROWERADDRESSID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRID,
				COLUMN_DFADDRID,
				QUALIFIED_COLUMN_DFADDRID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRCOPYID,
				COLUMN_DFADDRCOPYID,
				QUALIFIED_COLUMN_DFADDRCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE1,
				COLUMN_DFADDRESSLINE1,
				QUALIFIED_COLUMN_DFADDRESSLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCEID,
				COLUMN_DFPROVINCEID,
				QUALIFIED_COLUMN_DFPROVINCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALFSA,
				COLUMN_DFPOSTALFSA,
				QUALIFIED_COLUMN_DFPOSTALFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALLDU,
				COLUMN_DFPOSTALLDU,
				QUALIFIED_COLUMN_DFPOSTALLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE2,
				COLUMN_DFADDRESSLINE2,
				QUALIFIED_COLUMN_DFADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSSTATUSID,
				COLUMN_DFADDRESSSTATUSID,
				QUALIFIED_COLUMN_DFADDRESSSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMONTHSATADDRESS,
				COLUMN_DFMONTHSATADDRESS,
				QUALIFIED_COLUMN_DFMONTHSATADDRESS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRESIDENTIALSTATUSID,
				COLUMN_DFRESIDENTIALSTATUSID,
				QUALIFIED_COLUMN_DFRESIDENTIALSTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFSTREETNUMBER,
					COLUMN_DFSTREETNUMBER,
					QUALIFIED_COLUMN_DFSTREETNUMBER,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFSTREETNAME,
					COLUMN_DFSTREETNAME,
					QUALIFIED_COLUMN_DFSTREETNAME,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFSTREETTYPEID,
					COLUMN_DFSTREETTYPEID,
					QUALIFIED_COLUMN_DFSTREETTYPEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDIRECTIONTYPEID,
					COLUMN_DFDIRECTIONTYPEID,
					QUALIFIED_COLUMN_DFDIRECTIONTYPEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFUNITNUMBER,
					COLUMN_DFUNITNUMBER,
					QUALIFIED_COLUMN_DFUNITNUMBER,
					//NBC - team 6: ticket# 3880 changed the type to String from BigDecimal
					String.class,  
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
//		***** Change by NBC Impl. Team - Version 1.2 - End *****//


	}

}

