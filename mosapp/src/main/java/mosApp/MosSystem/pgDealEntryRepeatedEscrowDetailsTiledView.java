package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.picklist.BXResources;
import MosSystem.*;


/**
 *
 *
 *
 */
public class pgDealEntryRepeatedEscrowDetailsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealEntryRepeatedEscrowDetailsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDealEntryEscrowModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdEscrowPaymentId().setValue(CHILD_HDESCROWPAYMENTID_RESET_VALUE);
		getHdEscrowCopyId().setValue(CHILD_HDESCROWCOPYID_RESET_VALUE);
		getCbEscrowType().setValue(CHILD_CBESCROWTYPE_RESET_VALUE);
		getStEscrowPaymentDesc().setValue(CHILD_STESCROWPAYMENTDESC_RESET_VALUE);
		getStEscrowPayment().setValue(CHILD_STESCROWPAYMENT_RESET_VALUE);
		getBtDeleteEscrow().setValue(CHILD_BTDELETEESCROW_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDESCROWPAYMENTID,HiddenField.class);
		registerChild(CHILD_HDESCROWCOPYID,HiddenField.class);
		registerChild(CHILD_CBESCROWTYPE,ComboBox.class);
		registerChild(CHILD_STESCROWPAYMENTDESC,TextField.class);
		registerChild(CHILD_STESCROWPAYMENT,TextField.class);
		registerChild(CHILD_BTDELETEESCROW,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealEntryEscrowModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    ////Populate EscrowType ComboBoxes manually here
    DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    //Setup EscrowType ComboBox options
    cbEscrowTypeOptions.populate(getRequestContext());

    handler.pageSaveState();
    //--Release2.1--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
        // Put migrated repeated_onBeforeRowDisplayEvent code here
        validJSFromTiledView();
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDESCROWPAYMENTID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealEntryEscrowModel(),
				CHILD_HDESCROWPAYMENTID,
				doDealEntryEscrowModel.FIELD_DFESCROWPAYMENTID,
				CHILD_HDESCROWPAYMENTID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDESCROWCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealEntryEscrowModel(),
				CHILD_HDESCROWCOPYID,
				doDealEntryEscrowModel.FIELD_DFCOPYID,
				CHILD_HDESCROWCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBESCROWTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoDealEntryEscrowModel(),
				CHILD_CBESCROWTYPE,
				doDealEntryEscrowModel.FIELD_DFESCROWTYPEID,
				CHILD_CBESCROWTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbEscrowTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STESCROWPAYMENTDESC))
		{
			TextField child = new TextField(this,
				getdoDealEntryEscrowModel(),
				CHILD_STESCROWPAYMENTDESC,
				doDealEntryEscrowModel.FIELD_DFESCROWPAYMENTDESC,
				CHILD_STESCROWPAYMENTDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESCROWPAYMENT))
		{
			TextField child = new TextField(this,
				getdoDealEntryEscrowModel(),
				CHILD_STESCROWPAYMENT,
				doDealEntryEscrowModel.FIELD_DFESCROWPAYMENT,
				CHILD_STESCROWPAYMENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEESCROW))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEESCROW,
				CHILD_BTDELETEESCROW,
				CHILD_BTDELETEESCROW_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEscrowPaymentId()
	{
		return (HiddenField)getChild(CHILD_HDESCROWPAYMENTID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEscrowCopyId()
	{
		return (HiddenField)getChild(CHILD_HDESCROWCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbEscrowType()
	{
		return (ComboBox)getChild(CHILD_CBESCROWTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbEscrowTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbEscrowTypeOptionList()
		{

		}


   //--Release2.1--//
   //// Populate the comboBox from the ResourceBundle.
		public void populate(RequestContext rc)
		{
      try
      {
        ////Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"ESCROWTYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }

	}



	/**
	 *
	 *
	 */
	public TextField getStEscrowPaymentDesc()
	{
		return (TextField)getChild(CHILD_STESCROWPAYMENTDESC);
	}


	/**
	 *
	 *
	 */
	public TextField getStEscrowPayment()
	{
		return (TextField)getChild(CHILD_STESCROWPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteEscrowRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

    //logger = SysLog.getSysLogger("PGDEREDTV");

    DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

    //logger.debug("pgDealEntry@handleBtDeleteEscrowRequest::Before PreHandler");
    //logger.debug("pgDealEntry@handleBtDeleteEscrowRequest::Parent: " + (pgDealEntryViewBean) this.getParent());

    handler.preHandlerProtocol((ViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgDealEntry@handleBtDeleteEscrowRequest::TileIndexNew: " + tileIndx);

		handler.handleDelete(false,
                         4,
                         ////handler.getRowNdxFromWebEventMethod(event),
                          tileIndx,
                         "RepeatedEscrowDetails/hdEscrowPaymentId",
                         "escrowpaymentId",
                         "RepeatedEscrowDetails/hdEscrowCopyId",
                         "EscrowPayment");

    handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteEscrow()
	{
		return (Button)getChild(CHILD_BTDELETEESCROW);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteEscrowDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteEscrow_onBeforeHtmlOutputEvent method

		DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public doDealEntryEscrowModel getdoDealEntryEscrowModel()
	{
		if (doDealEntryEscrow == null)
			doDealEntryEscrow = (doDealEntryEscrowModel) getModel(doDealEntryEscrowModel.class);
		return doDealEntryEscrow;
	}


	/**
	 *
	 *
	 */
	public void setdoDealEntryEscrowModel(doDealEntryEscrowModel model)
	{
			doDealEntryEscrow = model;
	}

  public void validJSFromTiledView()
  {
          logger = SysLog.getSysLogger("PGDEREDTV");

          //// Currently it is called from the page. Should be moved to the
          //// TiledView utility classes to generalize the parent (ViewBean name/casting).
          //// ValidationPath is temporarily hardcoded as well. Should be replaced with
          //// the picklist mechanism along with the move to the utility class.

          pgDealEntryViewBean viewBean = (pgDealEntryViewBean) getParentViewBean();

          ////logger.debug("validJSFromTiledView@ParentBean: " + viewBean);

          try
          {
            String escrowValidPath = Sc.DVALID_DEAL_TILE_ESCROWPAY_PROP_FILE_NAME;

            ////logger.debug("PGDEREDTV@EscrowValidPath: " + escrowValidPath);

            JSValidationRules escrowValidObj = new JSValidationRules(escrowValidPath, logger, viewBean);
            escrowValidObj.attachJSValidationRules();
          }
          catch( Exception e )
          {
              logger.debug("DataValidJSException: " + e);
              e.printStackTrace();
          }
  }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDESCROWPAYMENTID="hdEscrowPaymentId";
	public static final String CHILD_HDESCROWPAYMENTID_RESET_VALUE="";
	public static final String CHILD_HDESCROWCOPYID="hdEscrowCopyId";
	public static final String CHILD_HDESCROWCOPYID_RESET_VALUE="";
	public static final String CHILD_CBESCROWTYPE="cbEscrowType";
	public static final String CHILD_CBESCROWTYPE_RESET_VALUE="";

  //--Release2.1--//
	////private static CbEscrowTypeOptionList cbEscrowTypeOptions=new CbEscrowTypeOptionList();
	private CbEscrowTypeOptionList cbEscrowTypeOptions=new CbEscrowTypeOptionList();
	public static final String CHILD_STESCROWPAYMENTDESC="stEscrowPaymentDesc";
	public static final String CHILD_STESCROWPAYMENTDESC_RESET_VALUE="";
	public static final String CHILD_STESCROWPAYMENT="stEscrowPayment";
	public static final String CHILD_STESCROWPAYMENT_RESET_VALUE="";
	public static final String CHILD_BTDELETEESCROW="btDeleteEscrow";
	public static final String CHILD_BTDELETEESCROW_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealEntryEscrowModel doDealEntryEscrow=null;
  private DealEntryHandler handler=new DealEntryHandler();
  public SysLogger logger;

}

