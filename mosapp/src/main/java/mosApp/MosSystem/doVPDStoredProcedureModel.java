package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doVPDStoredProcedureModel extends QueryModel, StoredProcModel
{
    /**
     *
     *
     */
    public java.math.BigDecimal getDfUSERProfileID();

    /**
     *
     *
     */
    public java.math.BigDecimal getDfInstitutionID();

    public static final String FIELD_DFUSERPROFILEID="dfUSERPROFILEID";
    public static final String FIELD_DFINSTITUTIONID="dfINSTITUTIONID";

}

