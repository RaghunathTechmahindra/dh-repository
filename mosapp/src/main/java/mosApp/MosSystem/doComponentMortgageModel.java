package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * Title: doComponentMortgageModel
 * <p>
 * Description: This is the interface class for doComponentMortgageModelImpl
 * used to retrieve data from componentMortgage table.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 * @version 1.1 18-June-2008 XS_2.27/28a added fields for componentMortgage section
 * @version 1.2 22-June-2008 XS_2.27/28a bug fixed: RateLockIn type to String
 * @version 1.3 24-June-2008 XS_16.13 Added new fields to get picklist description for repayment,payment frequency,privilege payment option.
 * @version 1.3 14-Oct-2008 Bugfix FXP22958: Added new fields to display MI Premium amount in Deal Summary Screen
 */
public interface doComponentMortgageModel extends QueryModel, SelectQueryModel
{
    // Variables representing fields retrieved using this model
    public static final String FIELD_DFCOMPONENTID = "dfComponentId";

    public static final String FIELD_DFCOPYID = "dfCopyId";

    public static final String FIELD_DFDEALID = "dfDealId";

    public static final String FIELD_DFISNTITUTIONID = "dfInstitutionId";

    public static final String FIELD_DFTOTALMORTGAGEAMOUNT = "dfTotalMortgageAmount";

    public static final String FIELD_DFMIALLOCAGTEFLAG = "dfMiAllocateFlag";

    public static final String FIELD_DFTOTALPAYMENTAMOUNT = "dfTotalPaymentAmount";

    public static final String FIELD_DFPROPERTYTAXALLOCATEFLAG = "dfPropertyTaxAllocateFlag";

    public static final String FIELD_DFNETINTRESTRATE = "dfNetIntrestRate";

    public static final String FIELD_DFDISCOUNT = "dfDiscount";

    public static final String FIELD_DFPREMIUM = "dfPremium";

    public static final String FIELD_DFBUYDOWNRATE = "dfBuyDownRate";

    public static final String FIELD_DFCOMPONENTTYPE = "dfComponentType";
    
    public static final String FIELD_DFPRODUCTNAME = "dfProductName";
    
    /***************MCM Impl team changes starts - XS_2.27, 2.28 *******************/
    
    public static final String FIELD_DFMIPREMIUM = "dfMIPremium";
    public static final String FIELD_DFMORTGAGEAMOUNT = "dfMortgageAmount";
    public static final String FIELD_DFAMORTIZATIONTERM="dfAmortizationTerm";
    public static final String FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS="dfEffectiveAmortizationInMonths";
    public static final String FIELD_DFPAYMENTTERMDESC="dfPaymentTermDesc";
    public static final String FIELD_DFPAYMENTTERMID="dfPaymentTermId";
    public static final String FIELD_DFACTUALPAYMENTTERM="dfActualPaymentTerm";
    public static final String FIELD_DFPAYMENTFREQUENCYID="dfPaymentFrequencyId";
    public static final String FIELD_DFPREPAYMENTOPTIONSID="dfPrepaymentOptionsId";
    public static final String FIELD_DFPRIVILEGEPAYMENTID="dfPrivilegePaymentId";
    public static final String FIELD_DFCOMMISSIONCODE="dfCommissionCode";
    public static final String FIELD_DFCASHBACKPERCENTAGE="dfCashbackPercentage";
    public static final String FIELD_DFCASHBACKAMOUNT="dfCashbackAmount";
    public static final String FIELD_DFCASHBACKAMOUNTOVERRIDE="dfCashbackAmountOverride";
    public static final String FIELD_DFREPAYMENTTYPEID="dfRepaymentTypeId";    
    public static final String FIELD_DFRATELOCK = "dfRateLock";  
    public static final String FIELD_DFADDITIONALINFORMATION = "dfAdditionalInformation";
    public static final String FIELD_DFMTGPRODTID = "dfMtgProdId";
    public static final String FIELD_DFPRICINGRATEINVENTORYID = "dfPricingRateInventoryId";
    
    public static final String FIELD_DFPIPAYMENTAMOUNT = "dfPIPaymentAmount";
    public static final String FIELD_DFADDITIONALPRINCIPAL = "dfAdditionalPrincipal";
    public static final String FIELD_DFESCROWPAYMENTAMOUNT = "dfEscrowPaymentAmount";
    
    public static final String FIELD_DFADVANCEHOLD = "dfAdvancedHold";
    public static final String FIELD_DFRATEGUARANTEEPERIOD = "dfRateGuaranteePeriod";
    public static final String FIELD_DFEXISTINGACOUNTINDICATOR = "dfExsitingAccountIndicator";
    public static final String FIELD_DFEXISTINGACOUNTNUMBER = "dfExsitingAccountNumber";
    public static final String FIELD_DFFIRSTPAYMENTDATE = "dfFirstPaymentDate";
    public static final String FIELD_DFMATURITYDATE = "dfMaturityDate";
    
    public static final String FIELD_DFTAXPAYORID = "dfTaxPayorId";
    public static final String FIELD_DFMIUPFRONT = "dfMIUpfront";    
    
    /***************MCM Impl team changes ends - XS_2.27, 2.28 *********************/
    /***********************MCM Impl team changes starts - XS_16.13*****************/
    public static final String FIELD_DFREPAYMENTTYPE = "dfRePaymentType";
    public static final String FIELD_DFPAYMENTFREQUENCY = "dfPaymentFrequency"; 
    public static final String FIELD_DFPREPAYMENTOPTION = "dfPrePaymentOption";
    public static final String FIELD_DFPRIVILEGEPAYMENTOPTION = "dfPrivilegePaymentOption";
    public static final String FIELD_DFPOSTEDRATE = "dfPostedRate";
    public static final String FIELD_DFPROPERTYTAXESCOWAMOUNT = "dfPropertyTaxEscrowAmount";
    /***********************MCM Impl team changes starts - XS_16.13*****************/
    
   /**Bug Fix:22957 Added new field for MI Premium Amount***/
    
    public static final String FIELD_DFDEALSUMMARYMIPREMIUMAMOUNT = "dfSummaryMiPremiumAmount";
    public static final String FIELD_DFDEALSUMMARYTAXESCROWAMOUNT = "dfSummaryTaxEscrowAmount";
    /**
     * This method declaration returns the value of field MI Premium Amount.
     * @return BigDecimal MI Premium Amount from the model.
     */
    public java.math.BigDecimal getDfSummaryMiPremiumAmount();

    /**
     * This method declaration sets the value of field MI Premium Amount
     * @param value -
     *            new value of the field
     */
    public void setDfSummaryMiPremiumAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field TaxEscrowAmount.
     * @return BigDecimal TaxEscrowAmount from the model.
     */
    public java.math.BigDecimal getDfSummaryTaxEscrowAmount();

    /**
     * This method declaration sets the value of field TaxEscrowAmount
     * @param value -
     *            new value of the field
     */
    public void setDfSummaryTaxEscrowAmount(java.math.BigDecimal value);
    /************************************************************************/
   
    /**
     * This method declaration returns the value of field ComponentId.
     * @return BigDecimal componentId from the model.
     */
    public java.math.BigDecimal getDfComponentId();

    /**
     * This method declaration sets the value of field ComponentId
     * @param value -
     *            new value of the field
     */
    public void setDfComponentId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field copyId.
     * @return BigDecimal copyId from the model.
     */
    public java.math.BigDecimal getDfCopyId();

    /**
     * This method declaration sets the value of field copyId
     * @param value -
     *            new value of the field
     */
    public void setDfCopyId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field InstitutionId.
     * @return BigDecimal copyId from the model.
     */
    public java.math.BigDecimal getDfInstitutionId();

    /**
     * This method declaration sets the value of field InstitutionId
     * @param value -
     *            new InstitutionId.
     */
    public void setDfInstitutionId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field dfTotalMortgageAmount.
     * @return BigDecimal totalMortgageAmount from the model
     */
    public java.math.BigDecimal getDfTotalMortgageAmount();

    /**
     * This method declaration sets the dfTotalMortgageAmount.
     * @param value
     *            the new dfTotalMortgageAmount
     */
    public void setDfTotalMortgageAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field ComponentType.
     * @return String componentType from the model
     */
    public String getDfComponentType();

    /**
     * This method declaration sets the dfComponentType.
     * @param value
     *            the new dfComponentType
     */
    public void setDfComponentType(String value);

    /**
     * This method declaration returns the value of field MiAllocateFlag.
     * @return the MiAllocateFlag
     */
    public String getDfMiAllocateFlag();

    /**
     * This method declaration sets the MiAllocateFlag.
     * @param value
     *            the new MiAllocateFlag
     */
    public void setDfMiAllocateFlag(String value);

    /**
     * This method declaration returns the value of field the
     * TotalPaymentAmount.
     * @return the TotalPaymentAmount
     */
    public java.math.BigDecimal getDfTotalPaymentAmount();

    /**
     * This method declaration sets the TotalPaymentAmount.
     * @param value
     *            the new dfTotalPaymentAmount
     */
    public void setDfTotalPaymentAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field PropertyTaxFlag.
     * @return the PropertyTaxFlag
     */
    public String getDfPropertyTaxFlag();

    /**
     * This method declaration sets the PropertyTaxFlag.
     * @param value
     *            the new PropertyTaxFlag
     */
    public void setDfPropertyTaxFlag(String value);

    /**
     * This method declaration returns the value of field the NetInstrestRate.
     * @return the NetInstrestRate
     */
    public java.math.BigDecimal getDfNetInstrestRate();

    /**
     * This method declaration sets the NetInstrestRate.
     * @param value
     *            the new NetInstrestRate
     */
    public void setDfNetInstrestRate(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the Discount.
     * @return the Discount.
     */
    public java.math.BigDecimal getDfDiscount();

    /**
     * This method declaration sets the Discount.
     * @param value
     *            the new dfDiscount
     */
    public void setDfDiscount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the Premium.
     * @return the Premium
     */
    public java.math.BigDecimal getDfPremium();

    /**
     * This method declaration sets the Premium.
     * @param value
     *            the new Premium
     */
    public void setDfPremium(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of fieldthe BuyDownRate.
     * @return the BuyDownRate.
     */
    public java.math.BigDecimal getDfBuyDownRate();

    /**
     * This method declaration sets the BuyDownRate.
     * @param value
     *            the new BuyDownRate.
     */
    public void setDfBuyDownRate(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the DealId.
     * @return the DealId
     */
    public java.math.BigDecimal getDfDealId();

    /**
     * This method declaration sets the dealId.
     * @param value
     *            the new dealId
     */
    public void setDfDealId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ProductType.
     * @return the ProductType
     */
    public String getDfProductName();

    /**
     * This method declaration sets the dfProductType.
     * @param value
     *            the new dfProductType
     */
    public void setDfProductName(String value);
    
    /***************MCM Impl team changes starts - XS_2.27, 2.28 *******************/
    /**
     * This method declaration returns the value of field the MIPremium.
     * @return the dfMIPremium
     */
    public java.math.BigDecimal getDfMIPremium();

    /**
     * This method declaration sets the MIPremium.
     * @param value
     *            the new dfMIPremium
     */
    public void setDfMIPremium(java.math.BigDecimal value);
    /**
     * This method declaration returns the value of field the MortgageAmount.
     * @return the dfMortgageAmount
     */
    public java.math.BigDecimal getDfMortgageAmount();

    /**
     * This method declaration sets the MortgageAmount.
     * @param value
     *            the new dfMortgageAmount
     */
    public void setDfMortgageAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the AmortizationTerm.
     * @return the dfAmortizationTerm
     */
    public java.math.BigDecimal getDfAmortizationTerm();

    /**
     * This method declaration sets the AmortizationTerm.
     * @param value
     *            the new dfAmortizationTerm
     */
    public void setDfAmortizationTerm(java.math.BigDecimal value);
        
    /**
     * This method declaration returns the value of field the EffectiveAmortizationInMonths.
     * @return the dfEffectiveAmortizationInMonths
     */
    public java.math.BigDecimal getDfEffectiveAmortizationInMonths();

    /**
     * This method declaration sets the EffectiveAmortizationInMonths.
     * @param value
     *            the new dfEffectiveAmortizationInMonths
     */
    public void setDfEffectiveAmortizationInMonths(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the PaymentTermId.
     * @return the dfPaymentTermId
     */
    public java.math.BigDecimal getDfPaymentTermId();

    /**
     * This method declaration sets the PaymentTermDesc.
     * @param value
     *            the new dfPaymentTermId
     */
    public void setDfPaymentTermId(java.math.BigDecimal value);
    /**
     * This method declaration returns the value of field the PaymentTermDesc.
     * @return the dfPaymentTermDesc
     */
    public String getDfPaymentTermDesc();

    /**
     * This method declaration sets the PaymentTermDesc.
     * @param value
     *            the new dfPaymentTermDesc
     */
    public void setDfPaymentTermDesc(String value);
    
    /**
     * This method declaration returns the value of field the ActualPaymentTerm.
     * @return the dfActualPaymentTerm
     */
    public java.math.BigDecimal getDfActualPaymentTerm();

    /**
     * This method declaration sets the ActualPaymentTerm.
     * @param value: the new dfActualPaymentTerm
     */
    public void setDfActualPaymentTerm(java.math.BigDecimal value);
    
    /**
     * This method declaration returns the value of field the PaymentFrequencyId.
     * @return dfPaymentFrequencyId
     */
    public java.math.BigDecimal getDfPaymentFrequencyId();

    /**
     * This method declaration sets the PaymentFrequencyId.
     * @param value: the new dfPaymentFrequencyId
     */
    public void setDfPaymentFrequencyId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the PrepaymentOptionsId.
     * @return dfPrepaymentOptionsId
     */
    public java.math.BigDecimal getDfPrepaymentOptionsId();

    /**
     * This method declaration sets the PrepaymentOptionsId.
     * @param value: the new dfPrepaymentOptionsId
     */
    public void setDfPrepaymentOptionsId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the PrivilegePaymentId.
     * @return dfPrivilegePaymentId
     */
    public java.math.BigDecimal getDfPrivilegePaymentId();

    /**
     * This method declaration sets the PrivilegePaymentId.
     * @param value: the new dfPrivilegePaymentId
     */
    public void setDfPrivilegePaymentId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the CommissionCode.
     * @return dfCommissionCode
     */
    public String getDfCommissionCode();

    /**
     * This method declaration sets the CommissionCode.
     * @param value: the new dfCommissionCode
     */
    public void setDfCommissionCode(String value);

    /**
     * This method declaration returns the value of field the CashbackPercentage.
     * @return dfCashbackPercentage
     */
    public java.math.BigDecimal getDfCashbackPercentage();

    /**
     * This method declaration sets the CashbackPercentage.
     * @param value: the new dfCashbackPercentage
     */
    public void setDfCashbackPercentage(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the CashbackAmount.
     * @return dfCashbackAmount
     */
    public java.math.BigDecimal getDfCashbackAmount();

    /**
     * This method declaration sets the CashbackAmount.
     * @param value: the new dfCashbackAmount
     */
    public void setDfCashbackAmount(java.math.BigDecimal value);
    
    /**
     * This method declaration returns the value of field the CashbackAmountOverride.
     * @return dfCashbackAmountOverride
     */
    public String getDfCashbackAmountOverride();

    /**
     * This method declaration sets the CashbackAmountOverride.
     * @param value: the new dfCashbackAmountOverride
     */
    public void setDfCashbackAmountOverride(String value);
    
    /**
     * This method declaration returns the value of field the RepaymentTypeId.
     * @return dfRepaymentTypeId
     */
    public java.math.BigDecimal getDfRepaymentTypeId();

    /**
     * This method declaration sets the RepaymentTypeId.
     * @param value: the new dfRepaymentTypeId
     */
    public void setDfRepaymentTypeId(java.math.BigDecimal value);
    
    /**
     * This method declaration returns the value of field the RateLockedIn.
     * @return dfRateLockedIn
     */
    public String getDfRateLockedIn();

    /**
     * This method declaration sets the RateLockedIn.
     * @param value: the new dfRateLockedIn
     */
    public void setDfRateLockedIn(String value);

    /**
     * This method declaration returns the value of field the AdditionalInformation.
     * @return dfAdditionalInformation
     */
    public String getDfAdditionalInformation();

    /**
     * This method declaration sets the AdditionalInformation.
     * @param value: the new dfAdditionalInformation
     */
    public void setDfAdditionalInformation(String value);

    /**
     * This method declaration returns the value of field the MtgProdId.
     * @return dfMtgProdId
     */
    public java.math.BigDecimal getDfMtgProdId();

    /**
     * This method declaration sets the MtgProdId.
     * @param value: the new dfMtgProdId
     */
    public void setDfMtgProdId(java.math.BigDecimal value);
    
    /**
     * This method declaration returns the value of field the PricingRateInventoryId.
     * @return dfPricingRateInventoryId
     */
    public java.math.BigDecimal getDfPricingRateInventoryId();

    /**
     * This method declaration sets the PricingRateInventoryId.
     * @param value: the new dfPricingRateInventoryRateId
     */
    public void setDfPricingRateInventoryId(java.math.BigDecimal value);
    
    /**
     * This method declaration returns the value of field the PandIPaymentAmount.
     * @return dfPIPaymentAmount
     */
    public java.math.BigDecimal getDfPIPaymentAmount();

    /**
     * This method declaration sets the PandIPaymentAmount.
     * @param value: the new dfPIPaymentAmount
     */
    public void setDfPIPaymentAmount(java.math.BigDecimal value);
    
    /**
     * This method declaration returns the value of field the AdditionalPrincipal.
     * @return dfAdditionalPrincipal
     */
    public java.math.BigDecimal getDfAdditionalPrincipal();

    /**
     * This method declaration sets the AdditionalPrincipal.
     * @param value: the new dfAdditionalPrincipal
     */
    public void setDfAdditionalPrincipal(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the EscrowPaymentAmount.
     * @return EscrowPaymentAmount
     */
    public java.math.BigDecimal getDfEscrowPaymentAmount();

    /**
     * This method declaration sets the AdvancedHold.
     * @param value: the new EscrowPaymentAmount
     */
    public void setDfEscrowPaymentAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the AdvancedHold.
     * @return dfAdvancedHold
     */
    public java.math.BigDecimal getDfAdvancedHold();

    /**
     * This method declaration sets the RateGuaranteePeriod.
     * @param value: the new DfAdvancedHold
     */
    public void setDfAdvancedHold(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the RateGuaranteePeriod.
     * @return dfRateGuaranteePeriod
     */
    public java.math.BigDecimal getDfRateGuaranteePeriod();

    /**
     * This method declaration sets the ExsitingAccountIndicator.
     * @param value: the new dfRateGuaranteePeriod
     */
    public void setDfRateGuaranteePeriod(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ExsitingAccountIndicator.
     * @return dfExsitingAccountIndicator
     */
    public String getDfExsitingAccountIndicator();

    /**
     * This method declaration sets the ExsitingAccountNumber.
     * @param value: the new dfExsitingAccountIndicator
     */
    public void setDfExsitingAccountIndicator(String value);

    /**
     * This method declaration returns the value of field the ExsitingAccountNumber.
     * @return dfExsitingAccountNumber
     */
    public String getDfExsitingAccountNumber();

    /**
     * This method declaration sets the ExsitingAccountNumber.
     * @param value: the new dfExsitingAccountNumber
     */
    public void setDfExsitingAccountNumber(String value);

    /**
     * This method declaration returns the value of field the FirstPaymentDate.
     * @return dfFirstPaymentDate
     */
    public  java.sql.Timestamp getDfFirstPaymentDate();

    /**
     * This method declaration sets the FirstPaymentDate.
     * @param value: the new dfFirstPaymentDate
     */
    public void setDfFirstPaymentDate(java.sql.Timestamp value);

    /**
     * This method declaration returns the value of field the MaturityDate.
     * @return dfMaturityDate
     */
    public  java.sql.Timestamp getDfMaturityDate();

    /**
     * This method declaration sets the MaturityDate.
     * @param value: the new dfMaturityDate
     */
    public void setDfMaturityDate(java.sql.Timestamp value);
    /**
     * This method declaration returns the value of field the taxPayorId on Deal.
     * @return dfTaxPayorId
     */
    public  java.math.BigDecimal getDfTaxPayorId();

    /**
     * This method declaration sets the taxPayorId from Deal.
     * @param value: the new dfTaxPayorId
     */
    public void setDfTaxPayorId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the taxPayorId on Deal.
     * @return dfTaxPayorId
     */
    public  String getDfMIUpfront();
    /**
     * This method declaration sets the miUpfront from Deal.
     * @param value: the new miUpfront
     */
    public void setDfMIUpfront(String value);
    /***************MCM Impl team changes ends - XS_2.27, 2.28 *******************/
    
    
    /***************MCM Impl team changes starts - XS_16.13 *******************/
    /**
     * This method declaration returns the value of field the DfRePaymentType.
     * @return DfRePaymentType
     */
    public  String getDfRePaymentType();
    /**
     * This method declaration sets the DfRePaymentType.
     * @param value: the new DfRePaymentType
     */
    public void setDfRePaymentType(String value);
    
    /**
     * This method declaration returns the value of field the DfPaymentFrequency.
     * @return DfPaymentFrequency
     */
    public  String getDfPaymentFrequency();
    /**
     * This method declaration sets the DfPaymentFrequency.
     * @param value: the new DfPaymentFrequency
     */
    public void setDfPaymentFrequency(String value);
    
    /**
     * This method declaration returns the value of field the DfPrePaymentOption.
     * @return DfPrePaymentOption
     */
    public  String getDfPrePaymentOption();
    /**
     * This method declaration sets the DfPrePaymentOption.
     * @param value: the new DfPrePaymentOption
     */
    public void setDfPrePaymentOption(String value);
    
    /**
     * This method declaration returns the value of field the DfPrivilegePaymentOption.
     * @return DfPrivilegePaymentOption
     */
    public  String getDfPrivilegePaymentOption();
    /**
     * This method declaration sets the DfPrivilegePaymentOption.
     * @param value: the new DfPrivilegePaymentOption
     */
    public void setDfPrivilegePaymentOption(String value);
    
    /**
     * This method declaration returns the value of field the DfPostedRate.
     * @return DfPostedRate
     */
    public  java.math.BigDecimal getDfPostedRate();
    /**
     * This method declaration sets the DfPostedRate.
     * @param value: the new DfPostedRate
     */
    public void setDfPostedRate(java.math.BigDecimal value);
    
      /***************MCM Impl team changes ends - XS_16.13*******************/
}
