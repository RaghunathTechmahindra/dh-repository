package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import MosSystem.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 * <p>Title: doAdjudicationApplicantResponseBNCModelImpl</p>
 *
 * <p>Description: Implementation class for doAdjudicationApplicantResponseBNCModel, used on Credit Decision screen</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006 </p
 * 
 * <p>Company: Filogix Inc. </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.0
 * @version 1.1 <br>
 * Date: 10/18/2006 <br>
 * Author: GCD Implementation Team <br>
 * Change: Changed Used Credit Bureau field to use CREDITBUREAUNAME.properties 
 * file as source for values, defect #1325 
 * @version 1.2 <br>
 * Date: 10/18/2006 <br>
 * Author: GCD Implementation Team <br>
 * Change: Changed incorrect table names in constants to read from correct table (BNCADJUDICATIONAPPLRESPONSE),
 * defect #1223 
 *
 */
public class doAdjudicationApplicantResponseBNCModelImpl extends QueryModelBase
	implements doAdjudicationApplicantResponseBNCModel
{
	/**
	 *
	 *
	 */
	public doAdjudicationApplicantResponseBNCModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

//   logger = SysLog.getSysLogger("AdjudicationApplicantResponseBNCModel");
//   logger.debug("AdjudicationApplicantResponseBNCModel@afterExecute::Size: " + sql);

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
//	    logger = SysLog.getSysLogger("AdjudicationApplicantResponseBNCModel");
//	    logger.debug("AdjudicationApplicantResponseBNCModel@afterExecute::Size: " + this.getSize());
//	    logger.debug("AdjudicationApplicantResponseBNCModel@afterExecute::SQLTemplate: " + this.getSelectSQL());
	
	    //To override all the Description fields by populating from BXResource
	    //Get the Language ID from the SessionStateModel
	    String defaultInstanceStateName =
				getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	    SessionStateModelImpl theSessionState =
	        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
	                        SessionStateModel.class,
	                        defaultInstanceStateName,
	                        true);
	    int languageId = theSessionState.getLanguageId();
		int institutionId = theSessionState.getDealInstitutionId();

	    while(this.next())
	    {
	      //  Credit Bureau Name
	      if (this.getDfUsedCreditBureauName() != null) 
	      {
    	  this.setDfUsedCreditBureauName(BXResources.getPickListDescription(
    			  institutionId, "CREDITBUREAUNAME", this.getDfUsedCreditBureauName(), languageId));
	      }
	    	  
	    }

	    // reset cursor location
	    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponseId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFRESPONSEID);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicantNumber(java.math.BigDecimal value)
	{
		setValue(FIELD_DFAPPLICANTNUMBER,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfApplicantNumber()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFAPPLICANTNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfResponseId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFRESPONSEID,value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditBureauScore()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITBUREAUSCORE);
	}

	/**
	 *
	 *
	 */
	public void setDfCreditBureauScore(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITBUREAUSCORE, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfInternalScore()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINTERNALSCORE);
	}

	/**
	 *
	 *
	 */
	public void setDfInternalScore(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINTERNALSCORE, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRiskRating()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFRISKRATING);
	}

	/**
	 *
	 *
	 */
	public void setDfRiskRating(java.math.BigDecimal value)
	{
		setValue(FIELD_DFRISKRATING, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetWorth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNETWORTH);
	}

	/**
	 *
	 *
	 */
	public void setDfNetWorth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNETWORTH, value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUsedCreditBureauNameId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSEDCREDITBUREAUNAMEID);
	}

	/**
	 *
	 *
	 */
	public void setDfUsedCreditBureauNameId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSEDCREDITBUREAUNAMEID, value);
	}
	
	/**
	 *
	 *
	 */
	public String getDfUsedCreditBureauName()
	{
		return (String)getValue(FIELD_DFUSEDCREDITBUREAUNAME);
	}

	/**
	 *
	 *
	 */
	public void setDfUsedCreditBureauName(String value)
	{
		setValue(FIELD_DFUSEDCREDITBUREAUNAME, value);
	}
	

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequestedCreditBureauNameId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREQUESTEDCREDITBUREAUNAMEID);
	}

	/**
	 *
	 *
	 */
	public void setDfRequestedCreditBureauNameId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREQUESTEDCREDITBUREAUNAMEID, value);
	}
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE=" SELECT DISTINCT DEAL.DEALID, DEAL.COPYID, RESPONSE.RESPONSEID, ADJUDICATIONAPPLICANTREQUEST.APPLICANTNUMBER, " +
	  "to_char(BNCADJUDICATIONAPPLRESPONSE.USEDCREDITBUREAUNAMEDID) USEDCREDITBUREAUNAMEDID_STR, " +
	  "BNCADJUDICATIONAPPLRESPONSE.USEDCREDITBUREAUNAMEDID, " + 
  	  "ADJUDICATIONAPPLICANTRESPONSE.CREDITBUREAUSCORE, " +
	  "ADJUDICATIONAPPLICANTRESPONSE.RISKRATING, ADJUDICATIONAPPLICANTRESPONSE.INTERNALSCORE, " + 
	  "BNCADJUDICATIONAPPLRESPONSE.NETWORTH, BNCADJUDICATIONAPPLREQUEST.REQUESTEDCREDITBUREAUNAMEID " +
	  "FROM ADJUDICATIONAPPLICANTRESPONSE , BNCADJUDICATIONAPPLRESPONSE, ADJUDICATIONAPPLICANTREQUEST, BNCADJUDICATIONAPPLREQUEST, RESPONSE, REQUEST, DEAL " +
	  "__WHERE__ ORDER BY ADJUDICATIONAPPLICANTREQUEST.APPLICANTNUMBER";

  public static final String MODIFYING_QUERY_TABLE_NAME=
    " ADJUDICATIONAPPLICANTRESPONSE , BNCADJUDICATIONAPPLRESPONSE, ADJUDICATIONAPPLICANTREQUEST, BNCADJUDICATIONAPPLREQUEST, RESPONSE, REQUEST, DEAL "; 

  // Removed REQUEST.COPYID = DEAL.COPYID
  public static final String STATIC_WHERE_CRITERIA=" REQUEST.DEALID = DEAL.DEALID " +
    "AND REQUEST.SERVICEPRODUCTID = " + Mc.SERVICE_PRODUCT_ID_GCD + " " +
    "AND RESPONSE.REQUESTID = REQUEST.REQUESTID " + 
    "AND REQUEST.REQUESTID = ADJUDICATIONAPPLICANTREQUEST.REQUESTID " +
    "AND RESPONSE.RESPONSEID = ADJUDICATIONAPPLICANTRESPONSE.RESPONSEID " +
    "AND ADJUDICATIONAPPLICANTRESPONSE.RESPONSEID = BNCADJUDICATIONAPPLRESPONSE.RESPONSEID AND ADJUDICATIONAPPLICANTRESPONSE.APPLICANTNUMBER = BNCADJUDICATIONAPPLRESPONSE.APPLICANTNUMBER " +
    "AND ADJUDICATIONAPPLICANTREQUEST.REQUESTID = BNCADJUDICATIONAPPLREQUEST.REQUESTID AND ADJUDICATIONAPPLICANTREQUEST.APPLICANTNUMBER = BNCADJUDICATIONAPPLREQUEST.APPLICANTNUMBER " +
    "AND ADJUDICATIONAPPLICANTREQUEST.BORROWERID = ADJUDICATIONAPPLICANTRESPONSE.BORROWERID ";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFRESPONSEID="RESPONSE.RESPONSEID";
	public static final String COLUMN_DFRESPONSEID="RESPONSEID";		
	public static final String QUALIFIED_COLUMN_DFCREDITBUREAUSCORE="BNCADJUDICATIONAPPLRESPONSE.CREDITBUREAUSCORE";
	public static final String COLUMN_DFCREDITBUREAUSCORE="CREDITBUREAUSCORE";
	public static final String QUALIFIED_COLUMN_DFINTERNALSCORE="BNCADJUDICATIONAPPLRESPONSE.INTERNALSCORE";
	public static final String COLUMN_DFINTERNALSCORE="INTERNALSCORE";
	public static final String QUALIFIED_COLUMN_DFRISKRATING="BNCADJUDICATIONAPPLRESPONSE.RISKRATING";
	public static final String COLUMN_DFRISKRATING="RISKRATING";
	public static final String QUALIFIED_COLUMN_DFNETWORTH="BNCADJUDICATIONAPPLRESPONSE.NETWORTH";
	public static final String COLUMN_DFNETWORTH="NETWORTH";
	public static final String QUALIFIED_COLUMN_DFREQUESTEDCREDITBUREAUNAMEID="BNCADJUDICATIONAPPLRESPONSE.REQUESTEDCREDITBUREAUNAMEID";
	public static final String COLUMN_DFREQUESTEDCREDITBUREAUNAMEID="REQUESTEDCREDITBUREAUNAMEID";
	public static final String QUALIFIED_COLUMN_DFUSEDCREDITBUREAUNAMEID="BNCADJUDICATIONAPPLRESPONSE.USEDCREDITBUREAUNAMEDID";
	public static final String COLUMN_DFUSEDCREDITBUREAUNAMEID="USEDCREDITBUREAUNAMEDID";
	public static final String QUALIFIED_COLUMN_DFUSEDCREDITBUREAUNAME="BNCADJUDICATIONAPPLRESPONSE.USEDCREDITBUREAUNAMEDID_STR";
	public static final String COLUMN_DFUSEDCREDITBUREAUNAME="USEDCREDITBUREAUNAMEDID_STR";
	public static final String QUALIFIED_COLUMN_DFAPPLICANTNUMBER="ADJUDICATIONAPPLICANTREQUEST.APPLICANTNUMBER";
	public static final String COLUMN_DFAPPLICANTNUMBER="APPLICANTNUMBER";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{


		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDEALID,
					COLUMN_DFDEALID,
					QUALIFIED_COLUMN_DFDEALID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFRESPONSEID,
					COLUMN_DFRESPONSEID,
					QUALIFIED_COLUMN_DFRESPONSEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
			

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFCREDITBUREAUSCORE,
				COLUMN_DFCREDITBUREAUSCORE,
				QUALIFIED_COLUMN_DFCREDITBUREAUSCORE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFINTERNALSCORE,
				COLUMN_DFINTERNALSCORE,
				QUALIFIED_COLUMN_DFINTERNALSCORE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFRISKRATING,
				COLUMN_DFRISKRATING,
				QUALIFIED_COLUMN_DFRISKRATING,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFNETWORTH,
				COLUMN_DFNETWORTH,
				QUALIFIED_COLUMN_DFNETWORTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFUSEDCREDITBUREAUNAME,
				COLUMN_DFUSEDCREDITBUREAUNAME,
				QUALIFIED_COLUMN_DFUSEDCREDITBUREAUNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFUSEDCREDITBUREAUNAMEID,
				COLUMN_DFUSEDCREDITBUREAUNAMEID,
				QUALIFIED_COLUMN_DFUSEDCREDITBUREAUNAMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFREQUESTEDCREDITBUREAUNAMEID,
				COLUMN_DFREQUESTEDCREDITBUREAUNAMEID,
				QUALIFIED_COLUMN_DFREQUESTEDCREDITBUREAUNAMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

			FIELD_SCHEMA.addFieldDescriptor(
					new QueryFieldDescriptor(
					FIELD_DFAPPLICANTNUMBER,
					COLUMN_DFAPPLICANTNUMBER,
					QUALIFIED_COLUMN_DFAPPLICANTNUMBER,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
			
	}

}

