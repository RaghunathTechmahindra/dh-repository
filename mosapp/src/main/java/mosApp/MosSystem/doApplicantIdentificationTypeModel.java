package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * <p>
 * Title: doApplicantIdentificationTypeModel
 * </p>
 * 
 * <p>
 * Description: Model for Identification tile on Applicant entry screen
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.0
 * 
 */
public interface doApplicantIdentificationTypeModel extends QueryModel,
		SelectQueryModel {
	/**
	 * <p>returns borrower Id</p>
	 * @return - returns borrower id
	 */
	public java.math.BigDecimal getDfBorrowerId();
	
	/**
	 * <p>sets borrower id</p>
	 * @param - sets borrower id
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);
	
	/**
	 * <p>returns Identification Id</p>
	 * @return - returns Identificationt id
	 */
	public java.math.BigDecimal getDfIdentificationId();

	/**
	 * <p>sets Identification id</p>
	 * @param - sets Identification id
	 */
	public void setDfIdentificationId(java.math.BigDecimal value);
	
	/**
	 * <p>returns copy Id</p>
	 * @return - returns copy id
	 */
	public java.math.BigDecimal getDfCopyId();

	/**
	 * <p>sets copy id</p>
	 * @param - sets copy id
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	/**
	 * <p>returns Identification Type </p>
	 * @return - returns Identification Type
	 */
	public java.math.BigDecimal getDfIdentificationTypeId();

	/**
	 * <p>sets  Identification Type</p>
	 * @param - sets  Identification Type
	 */
	public void setDfIdentificationTypeId(java.math.BigDecimal value);

	/**
	 * <p>returns  Identification Number</p>
	 * @return - returns Identification Number
	 */
	public String getDfIdentificationNumber();

	/**
	 * <p>sets Identification Number</p>
	 * @param - sets Identification Number
	 */	
	public void setDfIdentificationNumber(String value);

	/**
	 * <p>returns Identification Country Id</p>
	 * @return - returns Identification Country id
	 */
	public String getDfIdentificationCountry();

	/**
	 * <p>sets Identification Country id</p>
	 * @param - sets Identification Country id
	 */
	public void setDfIdentificationCountry(String value);

	public static final String FIELD_DFIDENTIFICATIONID = 
			"dfIdentificationId";

	public static final String FIELD_DFIDENTIFICATIONNUMBER = 
			"dfIdentificationNumber";

	public static final String FIELD_DFIDENTIFICATIONTYPEID = 
			"dfIdentificationTypeId";

	public static final String FIELD_DFBORROWERID = 
			"dfBorrowerId";

	public static final String FIELD_DFCOPYID = "dfCopyId";

	public static final String FIELD_DFIDENTIFICATIONCOUNTRY = 
			"dfIdentificationCountry";
}
