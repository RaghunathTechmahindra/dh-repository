package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doBridgeDescriptionModelImpl extends QueryModelBase
	implements doBridgeDescriptionModel
{
	/**
	 *
	 *
	 */
	public doBridgeDescriptionModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgePurposeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGEPURPOSEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgePurposeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGEPURPOSEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBPDescription()
	{
		return (String)getValue(FIELD_DFBPDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfBPDescription(String value)
	{
		setValue(FIELD_DFBPDESCRIPTION,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL BRIDGEPURPOSE.BRIDGEPURPOSEID, BRIDGEPURPOSE.BPDESCRIPTION FROM BRIDGEPURPOSE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="BRIDGEPURPOSE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBRIDGEPURPOSEID="BRIDGEPURPOSE.BRIDGEPURPOSEID";
	public static final String COLUMN_DFBRIDGEPURPOSEID="BRIDGEPURPOSEID";
	public static final String QUALIFIED_COLUMN_DFBPDESCRIPTION="BRIDGEPURPOSE.BPDESCRIPTION";
	public static final String COLUMN_DFBPDESCRIPTION="BPDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEPURPOSEID,
				COLUMN_DFBRIDGEPURPOSEID,
				QUALIFIED_COLUMN_DFBRIDGEPURPOSEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBPDESCRIPTION,
				COLUMN_DFBPDESCRIPTION,
				QUALIFIED_COLUMN_DFBPDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

