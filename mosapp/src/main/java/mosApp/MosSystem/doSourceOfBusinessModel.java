package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doSourceOfBusinessModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBProfileId();


	/**
	 *
	 *
	 */
	public void setDfSBProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfSBPShortName();


	/**
	 *
	 *
	 */
	public void setDfSBPShortName(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPStatusId();


	/**
	 *
	 *
	 */
	public void setDfSBPStatusId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPCompFactor();


	/**
	 *
	 *
	 */
	public void setDfSBPCompFactor(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfSBPClientId();


	/**
	 *
	 *
	 */
	public void setDfSBPClientId(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPAlternativeId();


	/**
	 *
	 *
	 */
	public void setDfSBPAlternativeId(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPServLen();


	/**
	 *
	 *
	 */
	public void setDfSBPServLen(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPCategoryId();


	/**
	 *
	 *
	 */
	public void setDfSBPCategoryId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPTierLevel();


	/**
	 *
	 *
	 */
	public void setDfSBPTierLevel(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
  //---------------- FXLink Phase II ------------------//
  //--> By Billy 06Nov2003
  //--> Rename DFSBPMAILBOXNUM ==> DFSBPSYSTEMCODE
	public String getDfSBPSystemCode();
  //=====================================================

	/**
	 *
	 *
	 */
  //---------------- FXLink Phase II ------------------//
  //--> By Billy 06Nov2003
  //--> Rename DFSBPMAILBOXNUM ==> DFSBPSYSTEMCODE
	public void setDfSBPSystemCode(String value);
  //====================================================


	/**
	 *
	 *
	 */
	public String getDfSBPFirstName();


	/**
	 *
	 *
	 */
	public void setDfSBPFirstName(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPMidInit();


	/**
	 *
	 *
	 */
	public void setDfSBPMidInit(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPLastName();


	/**
	 *
	 *
	 */
	public void setDfSBPLastName(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPPhone();


	/**
	 *
	 *
	 */
	public void setDfSBPPhone(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPFax();


	/**
	 *
	 *
	 */
	public void setDfSBPFax(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPEmail();


	/**
	 *
	 *
	 */
	public void setDfSBPEmail(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPCity();


	/**
	 *
	 *
	 */
	public void setDfSBPCity(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPProvinceId();


	/**
	 *
	 *
	 */
	public void setDfSBPProvinceId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfSBP_FSA();


	/**
	 *
	 *
	 */
	public void setDfSBP_FSA(String value);


	/**
	 *
	 *
	 */
	public String getDfSBP_LDU();


	/**
	 *
	 *
	 */
	public void setDfSBP_LDU(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPAddrLine1();


	/**
	 *
	 *
	 */
	public void setDfSBPAddrLine1(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPAddrLine2();


	/**
	 *
	 *
	 */
	public void setDfSBPAddrLine2(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPFirmId();


	/**
	 *
	 *
	 */
	public void setDfSBPFirmId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPUserId();


	/**
	 *
	 *
	 */
	public void setDfSBPUserId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfSBPUserDate();


	/**
	 *
	 *
	 */
	public void setDfSBPUserDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfSBPPhoneExt();


	/**
	 *
	 *
	 */
	public void setDfSBPPhoneExt(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPStatusDesc();


	/**
	 *
	 *
	 */
	public void setDfSBPStatusDesc(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPCategoryDesc();


	/**
	 *
	 *
	 */
	public void setDfSBPCategoryDesc(String value);


	/**
	 *
	 *
	 */
	public String getDfSBPProvinceName();


	/**
	 *
	 *
	 */
	public void setDfSBPProvinceName(String value);


	/**
	 *
	 *
	 */
	public String getDfNotes();


	/**
	 *
	 *
	 */
	public void setDfNotes(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSOBRegionId();


	/**
	 *
	 *
	 */
	public void setDfSOBRegionId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfSOBRegionDesc();


	/**
	 *
	 *
	 */
	public void setDfSOBRegionDesc(String value);

  /**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSBPGroupId();


	/**
	 *
	 *
	 */
	public void setDfSBPGroupId(java.math.BigDecimal value);

  /**
	 *
	 *
	 */
	public java.sql.Timestamp getDfSBPGroupDate();


	/**
	 *
	 *
	 */
	public void setDfSBPGroupDate(java.sql.Timestamp value);

  //---------------- FXLink Phase II ------------------//
  //--> By Billy 06Nov2003
  //--> Add new fields for SystemType
  public java.math.BigDecimal getDfSBPSystemTypeId();
	public void setDfSBPSystemTypeId(java.math.BigDecimal value);
  public String getDfSBPSystemTypeDesc();
	public void setDfSBPSystemTypeDesc(String value);
  //====================================================

  //========================================================
  //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
  //--> Add new field to indicate the Priority of the SOB
  //--> By Billy 28Nov2003
  public java.math.BigDecimal getDfSBPPriorityId();
	public void setDfSBPPriorityId(java.math.BigDecimal value);
  public String getDfSBPPriorityDesc();
	public void setDfSBPPriorityDesc(String value);
  //====================================================

  //--DJ_PREFCOMMETHOD_CR--start--//
  public String getDfPreferredDeliveryMethod();
  public void setDfPreferredDeliveryMethod(String value);

  public void setDfPreferredDeliveryMethodId(java.math.BigDecimal value);
  public java.math.BigDecimal getDfPreferredDeliveryMethodId();
  //--DJ_PREFCOMMETHOD_CR--end--//
  public java.math.BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(java.math.BigDecimal id);
  
    public String getDfLicenseNumber();
	public void setDfLicenseNumber(String value);

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFSBPROFILEID="dfSBProfileId";
	public static final String FIELD_DFSBPSHORTNAME="dfSBPShortName";
	public static final String FIELD_DFSBPSTATUSID="dfSBPStatusId";
	public static final String FIELD_DFSBPCOMPFACTOR="dfSBPCompFactor";
	public static final String FIELD_DFSBPCLIENTID="dfSBPClientId";
	public static final String FIELD_DFSBPALTERNATIVEID="dfSBPAlternativeId";
	public static final String FIELD_DFSBPSERVLEN="dfSBPServLen";
	public static final String FIELD_DFSBPCATEGORYID="dfSBPCategoryId";
	public static final String FIELD_DFSBPTIERLEVEL="dfSBPTierLevel";
	public static final String FIELD_DFSBPFIRSTNAME="dfSBPFirstName";
	public static final String FIELD_DFSBPMIDINIT="dfSBPMidInit";
	public static final String FIELD_DFSBPLASTNAME="dfSBPLastName";
	public static final String FIELD_DFSBPPHONE="dfSBPPhone";
	public static final String FIELD_DFSBPFAX="dfSBPFax";
	public static final String FIELD_DFSBPEMAIL="dfSBPEmail";
	public static final String FIELD_DFSBPCITY="dfSBPCity";
	public static final String FIELD_DFSBPPROVINCEID="dfSBPProvinceId";
	public static final String FIELD_DFSBP_FSA="dfSBP_FSA";
	public static final String FIELD_DFSBP_LDU="dfSBP_LDU";
	public static final String FIELD_DFSBPADDRLINE1="dfSBPAddrLine1";
	public static final String FIELD_DFSBPADDRLINE2="dfSBPAddrLine2";
	public static final String FIELD_DFSBPFIRMID="dfSBPFirmId";
	public static final String FIELD_DFSBPUSERID="dfSBPUserId";
	public static final String FIELD_DFSBPUSERDATE="dfSBPUserDate";
	public static final String FIELD_DFSBPPHONEEXT="dfSBPPhoneExt";
	public static final String FIELD_DFSBPSTATUSDESC="dfSBPStatusDesc";
	public static final String FIELD_DFSBPCATEGORYDESC="dfSBPCategoryDesc";
	public static final String FIELD_DFSBPPROVINCENAME="dfSBPProvinceName";
	public static final String FIELD_DFNOTES="dfNotes";
	public static final String FIELD_DFSOBREGIONID="dfSOBRegionId";
	public static final String FIELD_DFSOBREGIONDESC="dfSOBRegionDesc";

  //--Release2.1--//
  //// New field to translate Profile Status Description
  public static final String FIELD_DFSBPROFILESTATUSDESC="dfSBProfileStatDesc";

  //=================================================
  //--> CRF#37 :: Source/Firm to Group routing for TD
  //=================================================
  //--> Added new fields to handle Source to Group association
  //--> By Billy 19Aug2003
  public static final String FIELD_DFSBPGROUPID="dfSBPGroupId";
	public static final String FIELD_DFSBPGROUPDATE="dfSBPGroupDate";

  //---------------- FXLink Phase II ------------------//
  //--> 1) Modified to use SOB.sourceFirmProfileId instead of SOURCETOFIRMASSOC table
  //--> 2) Modified to use SOB.sourceOfBusinessCode instead of SOURCEOFBUSINESSMAILBOX table
  //--> 3) Added new filed : systemTypeId
  //--> By Billy 06Nov2003
  //--> Rename DFSBPMAILBOXNUM ==> DFSBPSYSTEMCODE
  public static final String FIELD_DFSBPSYSTEMCODE="dfSBPSystemCode";
  //--> Remove Firm Effective Date
  //public static final String FIELD_DFSBPFIRMDATE="dfSBPFirmDate";
  //--> Add new fields for SystemType
  public static final String FIELD_DFSBPSYSTEMTYPEID="dfSBPSystemTypeId";
  public static final String FIELD_DFSBPSYSTEMTYPEDESC="dfSBPSystemTypeDesc";
  public static final String FIELD_DFLICENSENUMBER="dfLicenseNumber";
  //========================================================
  //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
  //--> Add new field to indicate the Priority of the SOB
  //--> By Billy 28Nov2003
  public static final String FIELD_DFSBPPRIORITYID="dfSBPPriorityId";
  public static final String FIELD_DFSBPPRIORITYDESC="dfSBPPriorityDesc";
  //========================================================

  //--DJ_PREFCOMMETHOD_CR--start--//
	public static final String FIELD_DFPREFERREDDELIVERYMETHOD="dfPrefDeliveryMethod";
	public static final String FIELD_DFPREFERREDDELIVERYMETHODID="dfPrefDeliveryMethodId";
  //--DJ_PREFCOMMETHOD_CR--end--//
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

