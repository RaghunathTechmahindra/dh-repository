package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * Title: doComponentLocModel
 * <p>
 * Description: This is the interface class for doComponentMortgageModelImpl
 * used to retrieve data from componentloc table.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 * @version 1.1 25-June-2008 XS_16.14 
 * - Added dfNetRate,dfPostedRate
 * @version 1.2 14-Oct-2008 Bugfix FXP22958: Added new fields to display MI Premium amount in Deal Summary Screen
 */
public interface doComponentLocModel extends QueryModel, SelectQueryModel
{

    public static final String FIELD_DFCOMPONENTID = "dfComponentId";

    public static final String FIELD_DFCOPYID = "dfCopyId";

    public static final String FIELD_DFDEALID = "dfDealId";

    public static final String FIELD_DFISNTITUTIONID = "dfInstitutionId";

    public static final String FIELD_DFTOTALLOCAMOUNT = "dfTotalLocAmount";

    public static final String FIELD_DFMIALLOCAGTEFLAG = "dfMiAllocateFlag";

    public static final String FIELD_DFTOTALPAYMENTAMOUNT = "dfTotalPaymentAmount";

    public static final String FIELD_DFPROPERTYTAXALLOCATEFLAG = "dfPropertyTaxAllocateFlag";

    public static final String FIELD_DFNETINTRESTRATE = "dfNetIntrestRate";

    public static final String FIELD_DFDISCOUNT = "dfDiscount";

    public static final String FIELD_DFPREMIUM = "dfPremium";

    public static final String FIELD_DFCOMPONENTTYPE = "dfComponentType";

    public static final String FIELD_DFPRODUCTNAME = "dfProductName";

    public static final String FIELD_DFMIPREMIUMAMOUNT = "dfMiPremiumAmount";

    public static final String FIELD_DFPAYMENTFREQUENCYID = "dfPaymentFrequencyId";

    public static final String FIELD_DFCOMMISSIONCODE = "dfCommissionCode";

    public static final String FIELD_DFPANDIPAYMENTAMOUNT = "dfPandIPaymentAmount";

    public static final String FIELD_DFADVANCEHOLD = "dfAdvanceHold";

    public static final String FIELD_DFEXISTINGACCOUNTNUMBER = "dfExistingAccountNumber";

    public static final String FIELD_DFFIRSTPAYMENTDATE = "dfFirstPaymentDate";

    public static final String FIELD_DFREPAYMENTTYPEID = "dfRepaymentTypeId";

    public static final String FIELD_DFCOMPONENTTYPEID = "dfComponentTypeId";

    public static final String FIELD_DFPRODUCTID = "dfProductId";

    public static final String FIELD_DFEXISTINGACCOUNTINDICATOR = "dfExistingAccountIndicator";

    public static final String FIELD_DFLOCAMOUNT = "dfLocAmount";

    public static final String FIELD_DFADDITIONALINFORMATION = "dfAdditionalInformation";

    public static final String FIELD_DFPROPERTYTAXESCROWAMOUNT = "dfPropertyTaxEscrowAmount";

    public static final String FIELD_DFMIFLAG = "dfMIFlag";

    public static final String FIELD_DFPROPERTYTAXFLAG = "dfPropertyTaxFlag";


    public static final String FIELD_DFTAXPAYORID = "dfTaxPayorId";

    public static final String FIELD_DFMIUPFRONT = "dfMIUpfront";    

    /** ******************XS_16.14 STARTS****************************************** */
    
    public static final String FIELD_DFREPAYMENTTYPE = "dfRePaymentType";
    
    public static final String FIELD_DFPAYMENTFREQUENCY = "dfPaymentFrequency"; 
    
    public static final String FIELD_DFNETRATE = "dfNetRate";
    
    public static final String FIELD_DFPOSTEDRATE = "dfPostedRate";
    
    /** ******************XS_16.14 ENDS****************************************** */
    
    public static final String FIELD_DFPRICINGRATEINVENTORYID = "dfPricingRateInventoryId";
    
    
    /**Bug Fix:22957 Added new field for MI Premium Amount***/
    
    public static final String FIELD_DFDEALSUMMARYMIPREMIUMAMOUNT = "dfSummaryMiPremiumAmount";
    public static final String FIELD_DFDEALSUMMARYTAXESCROWAMOUNT = "dfSummaryTaxEscrowAmount";
    /**
     * This method declaration returns the value of field MI Premium Amount.
     * @return BigDecimal MI Premium Amount from the model.
     */
    public java.math.BigDecimal getDfSummaryMiPremiumAmount();

    /**
     * This method declaration sets the value of field MI Premium Amount
     * @param value -
     *            new value of the field
     */
    public void setDfSummaryMiPremiumAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field TaxEscrowAmount.
     * @return BigDecimal TaxEscrowAmount from the model.
     */
    public java.math.BigDecimal getDfSummaryTaxEscrowAmount();

    /**
     * This method declaration sets the value of field TaxEscrowAmount
     * @param value -
     *            new value of the field
     */
    public void setDfSummaryTaxEscrowAmount(java.math.BigDecimal value);
    /************************************************************************/
    /**
     * This method declaration returns the value of field ComponentId.
     * @return BigDecimal componentId from the model.
     */
    public java.math.BigDecimal getDfComponentId();

    /**
     * This method declaration sets the value of field ComponentId
     * @param value -
     *            new value of the field
     */
    public void setDfComponentId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field copyId.
     * @return BigDecimal copyId from the model.
     */
    public java.math.BigDecimal getDfCopyId();

    /**
     * This method declaration sets the value of field copyId
     * @param value -
     *            new value of the field
     */
    public void setDfCopyId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field InstitutionId.
     * @return BigDecimal InstitutionId from the model.
     */
    public java.math.BigDecimal getDfInstitutionId();

    /**
     * This method declaration sets the value of field InstitutionId
     * @param value -
     *            new InstitutionId.
     */
    public void setDfInstitutionId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field TotalLocAmount.
     * @return BigDecimal TotalLocAmount from the model
     */
    public java.math.BigDecimal getDfTotalLocAmount();

    /**
     * This method declaration sets the TotalLocAmount.
     * @param value
     *            the new TotalLocAmount
     */
    public void setDfTotalLocAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field ComponentType.
     * @return String componentType from the model
     */
    public String getDfComponentType();

    /**
     * This method declaration sets the componentType.
     * @param value
     *            the new componentType
     */
    public void setDfComponentType(String value);

    /**
     * This method declaration returns the value of field MiAllocateFlag.
     * @return the MiAllocateFlag
     */
    public String getDfMiAllocateFlag();

    /**
     * This method declaration sets the MiAllocateFlag.
     * @param value
     *            the new MiAllocateFlag
     */
    public void setDfMiAllocateFlag(String value);

    /**
     * This method declaration returns the value of field the
     * TotalPaymentAmount.
     * @return the TotalPaymentAmount
     */
    public java.math.BigDecimal getDfTotalPaymentAmount();

    /**
     * This method declaration sets the TotalPaymentAmount.
     * @param value
     *            the new dfTotalPaymentAmount
     */
    public void setDfTotalPaymentAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field PropertyTaxFlag.
     * @return the PropertyTaxFlag
     */
    public String getDfPropertyTaxFlag();

    /**
     * This method declaration sets the PropertyTaxFlag.
     * @param value
     *            the new PropertyTaxFlag
     */
    public void setDfPropertyTaxFlag(String value);

    /**
     * This method declaration returns the value of field the NetInstrestRate.
     * @return the NetInstrestRate
     */
    public java.math.BigDecimal getDfNetInstrestRate();

    /**
     * This method declaration sets the NetInstrestRate.
     * @param value
     *            the new NetInstrestRate
     */
    public void setDfNetInstrestRate(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the Discount.
     * @return the Discount.
     */
    public java.math.BigDecimal getDfDiscount();

    /**
     * This method declaration sets the Discount.
     * @param value
     *            the new dfDiscount
     */
    public void setDfDiscount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the Premium.
     * @return the Premium
     */
    public java.math.BigDecimal getDfPremium();

    /**
     * This method declaration sets the the Premium.
     * @param value
     *            the new Premium
     */
    public void setDfPremium(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of fieldthe DealId.
     * @return the DealId.
     */
    public java.math.BigDecimal getDfDealId();

    /**
     * This method declaration sets the the DealId.
     * @param value
     *            the new DealId.
     */
    public void setDfDealId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ProductType.
     * @return the ProductType
     */
    public String getDfProductName();

    /**
     * This method declaration sets the the ProductType.
     * @param value
     *            the new ProductType
     */
    public void setDfProductName(String value);

    //XS2.33

    /**
     * This method declaration returns the value of field the MiPremiumAmount.
     * @return the MiPremiumAmount.
     */
    public java.math.BigDecimal getDfMiPremiumAmount();

    /**
     * This method declaration sets the the MiPremiumAmount.
     * @param value
     *            the new MiPremiumAmount.
     */
    public void setDfMiPremiumAmount(java.math.BigDecimal value);


    public java.math.BigDecimal getDfPaymentFrequencyId();

    public void setDfPaymentFrequencyId(java.math.BigDecimal value);


    /**
     * This method declaration returns the value of field the CommissionCode.
     * @return the CommissionCode
     */
    public String getDfCommissionCode();

    /**
     * This method declaration sets the the CommissionCode.
     * @param value
     *            the new CommissionCode
     */
    public void setDfCommissionCode(String value);

    /**
     * This method declaration returns the value of field the dfPandIPaymentAmount
     * @return the dfPandIPaymentAmount
     */
    public java.math.BigDecimal getDfPandIPaymentAmount();

    /**
     * This method declaration sets the the PandIPaymentAmount.
     * @param value
     *            the new PandIPaymentAmount
     */
    public void setDfPandIPaymentAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the AdvanceHold
     * @return the AdvanceHold
     */
    public java.math.BigDecimal getDfAdvanceHold();

    /**
     * This method declaration sets the the AdvanceHold
     * @param value
     *            the new AdvanceHold
     */
    public void setDfAdvanceHold(java.math.BigDecimal value);


    /**
     * This method declaration returns the value of field the ExistingAccountNumber
     * @return the ExistingAccountNumber
     */
    public String  getDfExistingAccountNumber();

    
    /**
     * This method declaration sets the the ExistingAccountNumber
     * @param value
     *            the new ExistingAccountNumber
     */
    public void setDfExistingAccountNumber(String value);

    
    /**
     * This method declaration returns the value of field the FirstPaymentDate
     * @return the FirstPaymentDate
     */
    public java.sql.Timestamp getDfFirstPaymentDate();

    
    /**
     * This method declaration sets the the FirstPaymentDate
     * @param value
     *            the new FirstPaymentDate
     */
    public void setDfFirstPaymentDate(java.sql.Timestamp value);

    
    /**
     * This method declaration returns the value of field the RepaymentTypeId
     * @return the RepaymentTypeId
     */
    public java.math.BigDecimal getDfRepaymentTypeId();
    
    
    /**
     * This method declaration sets the the RepaymentTypeId
     * @param value
     *            the new RepaymentTypeId
     */
    public void setDfRepaymentTypeId(java.math.BigDecimal value);

    
    /**
     * This method declaration returns the value of field the ComponentTypeId
     * @return the ComponentTypeId
     */
    public java.math.BigDecimal getDfComponentTypeId();
    
    
    /**
     * This method declaration sets the the ComponentTypeId
     * @param value
     *            the new ComponentTypeId
     */
    public void setDfComponentTypeId(java.math.BigDecimal value);

    
    /**
     * This method declaration returns the value of field the ProductId
     * @return the ProductId
     */
    public java.math.BigDecimal getDfProductId();
    
    /**
     * This method declaration sets the the ProductId
     * @param value
     *            the new ProductId
     */
    public void setDfProductId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ExistingAccountIndicator
     * @return the ExistingAccountIndicator
     */
    public String getDfExistingAccountIndicator();
    
    /**
     * This method declaration sets the the ExistingAccountIndicator
     * @param value
     *            the new ExistingAccountIndicator
     */
    public void setDfExistingAccountIndicator(String value);

    /**
     * This method declaration returns the value of field LocAmount.
     * @return BigDecimal LocAmount from the model
     */
    public java.math.BigDecimal getDfLocAmount();

    /**
     * This method declaration sets the LocAmount.
     * @param value new LocAmount
     */
    public void setDfLocAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the AdditionalInformation.
     * @return the AdditionalInformation
     */
    public String getDfAdditionalInformation();

    /**
     * This method declaration sets the the AdditionalInformation.
     * @param value
     *            the new AdditionalInformation
     */
    public void setDfAdditionalInformation(String value);

    /**
     * This method declaration returns the value of field PropertyTaxEscrowAmount.
     * @return BigDecimal LocAmount from the model
     */
    public java.math.BigDecimal getDfPropertyTaxEscrowAmount();

    /**
     * This method declaration sets the PropertyTaxEscrowAmount.
     * @param value new PropertyTaxEscrowAmount
     */
    public void setDfPropertyTaxEscrowAmount(java.math.BigDecimal value);


    /**
     * This method declaration returns the value of field the MIAllocateFlag.
     * @return the MIAllocateFlag
     */
    public String getDfMIFlag();

    /**
     * This method declaration sets the the MIAllocateFlag.
     * @param value
     *            the new MIAllocateFlag
     */
    public void setDfMIFlag(String value);

    /**
     * This method declaration returns the value of field the PropertyTaxFlag.
     * @return the PropertyTaxFlag
     */
    public String getDfPropertyTaxAllocateFlag();

    /**
     * This method declaration sets the the PropertyTaxFlag.
     * @param value
     *            the new PropertyTaxFlag
     */
    public void setDfPropertyTaxAllocateFlag(String value);

    /** ******************XS_16.14 STARTS****************************************** */
   
    /**
     * This method declaration returns the value of field the DfRePaymentType.
     * @return DfRePaymentType
     */
    public  String getDfRePaymentType();
    /**
     * This method declaration sets the DfRePaymentType.
     * @param value: the new DfRePaymentType
     */
    public void setDfRePaymentType(String value);

    /**
     * This method declaration returns the value of field the DfPaymentFrequency.
     * @return DfPaymentFrequency
     */
    public  String getDfPaymentFrequency();
    /**
     * This method declaration sets the DfPaymentFrequency.
     * @param value: the new DfPaymentFrequency
     */
    public void setDfPaymentFrequency(String value);
    /**
     * This method declaration returns the value of field the PropertyTaxFlag.
     * @return the PropertyTaxFlag
     */
    public java.math.BigDecimal getDfNetRate();

    /**
     * This method declaration sets the the PropertyTaxFlag.
     * @param value
     *            the new PropertyTaxFlag
     */
    public void setDfNetRate(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the PropertyTaxFlag.
     * @return the PropertyTaxFlag
     */
    public java.math.BigDecimal getDfPostedRate();

    /**
     * This method declaration sets the the PropertyTaxFlag.
     * @param value
     *            the new PropertyTaxFlag
     */
    public void setDfPostedRate(java.math.BigDecimal value);

    /** ******************XS_16.14 ENDS****************************************** */

    /**
     * This method declaration returns the value of field the MIUpfront.
     * @return MIUpfront
     */
    public  String getDfMIUpfront();
    /**
     * This method declaration sets the MIUpfront.
     * @param value: the new miUpfront
     */
    public void setDfMIUpfront(String value);

    /**
     * This method declaration returns the value of field the PricingRateInventoryId.
     * @return dfPricingRateInventoryId
     */
    public java.math.BigDecimal getDfPricingRateInventoryId();

    /**
     * This method declaration sets the PricingRateInventoryId.
     * @param value: the new dfPricingRateInventoryRateId
     */
    public void setDfPricingRateInventoryId(java.math.BigDecimal value);
    
}
