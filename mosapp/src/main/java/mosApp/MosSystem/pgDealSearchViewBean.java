package mosApp.MosSystem;


import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;

import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import com.basis100.picklist.BXResources;
import MosSystem.Mc;

/**
 *
 *
 *
 */

public class pgDealSearchViewBean extends ExpressViewBeanBase 
{
	/**
	 *
	 *
	 */
	public pgDealSearchViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
	    View superReturn = super.createChild(name);
        if (superReturn != null) {
            return superReturn;
        } else 
	    if (name.equals(CHILD_TBDEALID))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);

      //--Release2.1--//
      //Modified to set NonSelected Label from CHILD_CBPAGENAMES_NONSELECTED_LABEL
      //--> By John 08Nov2002
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      //==========================================================================

			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBDEALNUMBER))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALNUMBER,
				CHILD_TBDEALNUMBER,
				CHILD_TBDEALNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERFIRSTNAME))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERFIRSTNAME,
				CHILD_TBBORROWERFIRSTNAME,
				CHILD_TBBORROWERFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERLASTNAME))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERLASTNAME,
				CHILD_TBBORROWERLASTNAME,
				CHILD_TBBORROWERLASTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_RBPRIMARYBORROWERONLY))
		{
			RadioButtonGroup child = new RadioButtonGroup( this,
				getDefaultModel(),
				CHILD_RBPRIMARYBORROWERONLY,
				CHILD_RBPRIMARYBORROWERONLY,
				CHILD_RBPRIMARYBORROWERONLY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(rbPrimaryBorrowerOnlyOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERMAILINGADDRESS))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERMAILINGADDRESS,
				CHILD_TBBORROWERMAILINGADDRESS,
				CHILD_TBBORROWERMAILINGADDRESS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERHOMEPHONENUMBER1))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERHOMEPHONENUMBER1,
				CHILD_TBBORROWERHOMEPHONENUMBER1,
				CHILD_TBBORROWERHOMEPHONENUMBER1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERHOMEPHONENUMBER2))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERHOMEPHONENUMBER2,
				CHILD_TBBORROWERHOMEPHONENUMBER2,
				CHILD_TBBORROWERHOMEPHONENUMBER2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERHOMEPHONENUMBER3))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERHOMEPHONENUMBER3,
				CHILD_TBBORROWERHOMEPHONENUMBER3,
				CHILD_TBBORROWERHOMEPHONENUMBER3_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERWORKPHONENUMBER1))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERWORKPHONENUMBER1,
				CHILD_TBBORROWERWORKPHONENUMBER1,
				CHILD_TBBORROWERWORKPHONENUMBER1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERWORKPHONENUMBER2))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERWORKPHONENUMBER2,
				CHILD_TBBORROWERWORKPHONENUMBER2,
				CHILD_TBBORROWERWORKPHONENUMBER2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBORROWERWORKPHONENUMBER3))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBORROWERWORKPHONENUMBER3,
				CHILD_TBBORROWERWORKPHONENUMBER3,
				CHILD_TBBORROWERWORKPHONENUMBER3_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPROPERTYSTREETNUMBER))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPROPERTYSTREETNUMBER,
				CHILD_TBPROPERTYSTREETNUMBER,
				CHILD_TBPROPERTYSTREETNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPROPERTYSTREETNAME))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPROPERTYSTREETNAME,
				CHILD_TBPROPERTYSTREETNAME,
				CHILD_TBPROPERTYSTREETNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPROPERTYCITY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPROPERTYCITY,
				CHILD_TBPROPERTYCITY,
				CHILD_TBPROPERTYCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBSOURCENAME))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBSOURCENAME,
				CHILD_TBSOURCENAME,
				CHILD_TBSOURCENAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBSOURCEAPPID))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBSOURCEAPPID,
				CHILD_TBSOURCEAPPID,
				CHILD_TBSOURCEAPPID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBUNDERWRITER))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBUNDERWRITER,
				CHILD_TBUNDERWRITER,
				CHILD_TBUNDERWRITER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBADMINISTRATOR))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBADMINISTRATOR,
				CHILD_TBADMINISTRATOR,
				CHILD_TBADMINISTRATOR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBFUNDER))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBFUNDER,
				CHILD_TBFUNDER,
				CHILD_TBFUNDER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSEARCH,
				CHILD_BTSEARCH,
				CHILD_BTSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_REPEATED1))
		{
			pgDealSearchRepeated1TiledView child = new pgDealSearchRepeated1TiledView(this,
				CHILD_REPEATED1);
			return child;
		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTBACKWARDBUTTON))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTBACKWARDBUTTON,
				CHILD_BTBACKWARDBUTTON,
				CHILD_BTBACKWARDBUTTON_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTFORWARDBUTTON))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTFORWARDBUTTON,
				CHILD_BTFORWARDBUTTON,
				CHILD_BTFORWARDBUTTON_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_ROWSDISPLAYED))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_ROWSDISPLAYED,
				CHILD_ROWSDISPLAYED,
				CHILD_ROWSDISPLAYED_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDROWNUM2))
		{
			HiddenField child = new HiddenField(this,
				getdoRowGenerator2Model(),
				CHILD_HDROWNUM2,
				doRowGenerator2Model.FIELD_DFROWNDX,
				CHILD_HDROWNUM2_RESET_VALUE,
				null);
			return child;
		}
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> by BILLY 15July2002
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //--Release2.1--//
    //// Link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    else
		if (name.equals(CHILD_TBSERVICINGNUM))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBSERVICINGNUM,
				CHILD_TBSERVICINGNUM,
				CHILD_TBSERVICINGNUM_RESET_VALUE,
				null);
			return child;
		}
    //========================================================
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
	    super.resetChildren();
	    getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getTbDealNumber().setValue(CHILD_TBDEALNUMBER_RESET_VALUE);
		getTbBorrowerFirstName().setValue(CHILD_TBBORROWERFIRSTNAME_RESET_VALUE);
		getTbBorrowerLastName().setValue(CHILD_TBBORROWERLASTNAME_RESET_VALUE);
		getRbPrimaryBorrowerOnly().setValue(CHILD_RBPRIMARYBORROWERONLY_RESET_VALUE);
		getTbBorrowerMailingAddress().setValue(CHILD_TBBORROWERMAILINGADDRESS_RESET_VALUE);
		getTbBorrowerHomePhoneNumber1().setValue(CHILD_TBBORROWERHOMEPHONENUMBER1_RESET_VALUE);
		getTbBorrowerHomePhoneNumber2().setValue(CHILD_TBBORROWERHOMEPHONENUMBER2_RESET_VALUE);
		getTbBorrowerHomePhoneNumber3().setValue(CHILD_TBBORROWERHOMEPHONENUMBER3_RESET_VALUE);
		getTbBorrowerWorkPhoneNumber1().setValue(CHILD_TBBORROWERWORKPHONENUMBER1_RESET_VALUE);
		getTbBorrowerWorkPhoneNumber2().setValue(CHILD_TBBORROWERWORKPHONENUMBER2_RESET_VALUE);
		getTbBorrowerWorkPhoneNumber3().setValue(CHILD_TBBORROWERWORKPHONENUMBER3_RESET_VALUE);
		getTbPropertyStreetNumber().setValue(CHILD_TBPROPERTYSTREETNUMBER_RESET_VALUE);
		getTbPropertyStreetName().setValue(CHILD_TBPROPERTYSTREETNAME_RESET_VALUE);
		getTbPropertyCity().setValue(CHILD_TBPROPERTYCITY_RESET_VALUE);
		getTbSourceName().setValue(CHILD_TBSOURCENAME_RESET_VALUE);
		getTbSourceAppId().setValue(CHILD_TBSOURCEAPPID_RESET_VALUE);
		getTbUnderwriter().setValue(CHILD_TBUNDERWRITER_RESET_VALUE);
		getTbAdministrator().setValue(CHILD_TBADMINISTRATOR_RESET_VALUE);
		getTbFunder().setValue(CHILD_TBFUNDER_RESET_VALUE);
		getBtSearch().setValue(CHILD_BTSEARCH_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getRepeated1().resetChildren();
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getBtBackwardButton().setValue(CHILD_BTBACKWARDBUTTON_RESET_VALUE);
		getBtForwardButton().setValue(CHILD_BTFORWARDBUTTON_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getRowsDisplayed().setValue(CHILD_ROWSDISPLAYED_RESET_VALUE);
		getHdRowNum2().setValue(CHILD_HDROWNUM2_RESET_VALUE);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
    getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    getTbServicingNum().setValue(CHILD_TBSERVICINGNUM_RESET_VALUE);
    //========================================================
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
	    super.registerChildren();
	    registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_TBDEALNUMBER,TextField.class);
		registerChild(CHILD_TBBORROWERFIRSTNAME,TextField.class);
		registerChild(CHILD_TBBORROWERLASTNAME,TextField.class);
		registerChild(CHILD_RBPRIMARYBORROWERONLY,RadioButtonGroup.class);
		registerChild(CHILD_TBBORROWERMAILINGADDRESS,TextField.class);
		registerChild(CHILD_TBBORROWERHOMEPHONENUMBER1,TextField.class);
		registerChild(CHILD_TBBORROWERHOMEPHONENUMBER2,TextField.class);
		registerChild(CHILD_TBBORROWERHOMEPHONENUMBER3,TextField.class);
		registerChild(CHILD_TBBORROWERWORKPHONENUMBER1,TextField.class);
		registerChild(CHILD_TBBORROWERWORKPHONENUMBER2,TextField.class);
		registerChild(CHILD_TBBORROWERWORKPHONENUMBER3,TextField.class);
		registerChild(CHILD_TBPROPERTYSTREETNUMBER,TextField.class);
		registerChild(CHILD_TBPROPERTYSTREETNAME,TextField.class);
		registerChild(CHILD_TBPROPERTYCITY,TextField.class);
		registerChild(CHILD_TBSOURCENAME,TextField.class);
		registerChild(CHILD_TBSOURCEAPPID,TextField.class);
		registerChild(CHILD_TBUNDERWRITER,TextField.class);
		registerChild(CHILD_TBADMINISTRATOR,TextField.class);
		registerChild(CHILD_TBFUNDER,TextField.class);
		registerChild(CHILD_BTSEARCH,Button.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_REPEATED1,pgDealSearchRepeated1TiledView.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_BTBACKWARDBUTTON,Button.class);
		registerChild(CHILD_BTFORWARDBUTTON,Button.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_ROWSDISPLAYED,StaticTextField.class);
		registerChild(CHILD_HDROWNUM2,HiddenField.class);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    registerChild(CHILD_BTACTMSG,Button.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
    //--> Ticket#129 -- Support search by Servicing Mortgage #
    //--> By Billy 26Nov2003
    registerChild(CHILD_TBSERVICINGNUM,TextField.class);
    //========================================================
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoRowGenerator2Model());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:

				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By John 08Nov2002
    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
    BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    rbPrimaryBorrowerOnlyOptions.populate(getRequestContext());

    //=====================================

    handler.pageSaveState();
    super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// The following code block was migrated from the this_onBeforeDataObjectExecuteEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.setupBeforePageGeneration();
		handler.pageSaveState();

    return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// The following code block was migrated from the this_onBeforeRowDisplayEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.populatePageDisplayFields();
		handler.pageSaveState();

    super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

	/**
	 *
	 *
	 */
	static class RbPrimaryBorrowerOnlyOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		RbPrimaryBorrowerOnlyOptionList()
		{

		}

    public void populate(RequestContext rc)
    {
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
            (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

        String label1 = BXResources.getSysMsg("DEAL_SEARCH_PRIMARY_BORROWER", theSessionState.getLanguageId());
        String label2 = BXResources.getSysMsg("DEAL_SEARCH_ALL_BORROWERS", theSessionState.getLanguageId());

        add(label1, "Y");
        add(label2, "N");
			}
			catch (Exception ex)
      {
				ex.printStackTrace();
			}
    }
	}


	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btProceed_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleGoPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href1_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(0);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href2_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(1);
    handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href3_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(2);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href4_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(3);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href5_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(4);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href6_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(5);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href7_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(6);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href8_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(7);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href9_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(8);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href10_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(9);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public TextField getTbDealNumber()
	{
		return (TextField)getChild(CHILD_TBDEALNUMBER);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerFirstName()
	{
		return (TextField)getChild(CHILD_TBBORROWERFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerLastName()
	{
		return (TextField)getChild(CHILD_TBBORROWERLASTNAME);
	}


	/**
	 *
	 *
	 */
	public RadioButtonGroup getRbPrimaryBorrowerOnly()
	{
		return (RadioButtonGroup)getChild(CHILD_RBPRIMARYBORROWERONLY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerMailingAddress()
	{
		return (TextField)getChild(CHILD_TBBORROWERMAILINGADDRESS);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerHomePhoneNumber1()
	{
		return (TextField)getChild(CHILD_TBBORROWERHOMEPHONENUMBER1);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerHomePhoneNumber2()
	{
		return (TextField)getChild(CHILD_TBBORROWERHOMEPHONENUMBER2);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerHomePhoneNumber3()
	{
		return (TextField)getChild(CHILD_TBBORROWERHOMEPHONENUMBER3);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerWorkPhoneNumber1()
	{
		return (TextField)getChild(CHILD_TBBORROWERWORKPHONENUMBER1);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerWorkPhoneNumber2()
	{
		return (TextField)getChild(CHILD_TBBORROWERWORKPHONENUMBER2);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBorrowerWorkPhoneNumber3()
	{
		return (TextField)getChild(CHILD_TBBORROWERWORKPHONENUMBER3);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPropertyStreetNumber()
	{
		return (TextField)getChild(CHILD_TBPROPERTYSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPropertyStreetName()
	{
		return (TextField)getChild(CHILD_TBPROPERTYSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPropertyCity()
	{
		return (TextField)getChild(CHILD_TBPROPERTYCITY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbSourceName()
	{
		return (TextField)getChild(CHILD_TBSOURCENAME);
	}


	/**
	 *
	 *
	 */
	public TextField getTbSourceAppId()
	{
		return (TextField)getChild(CHILD_TBSOURCEAPPID);
	}


	/**
	 *
	 *
	 */
	public TextField getTbUnderwriter()
	{
		return (TextField)getChild(CHILD_TBUNDERWRITER);
	}


	/**
	 *
	 *
	 */
	public TextField getTbAdministrator()
	{
		return (TextField)getChild(CHILD_TBADMINISTRATOR);
	}


	/**
	 *
	 *
	 */
	public TextField getTbFunder()
	{
		return (TextField)getChild(CHILD_TBFUNDER);
	}


	/**
	 *
	 *
	 */
	public void handleBtSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btSearch_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleBtSearchClickEvent();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSearch()
	{
		return (Button)getChild(CHILD_BTSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCancel_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleCancelButton();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
	public boolean beginBtCancelDisplay(ChildDisplayEvent event)
	{
		// The following code block was migrated from the btCancel_onBeforeDisplayEvent method
		boolean rc = true;
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		rc = handler.checkPassiveUser();
		handler.pageSaveState();

		return(rc);
	}


	/**
	 *
	 *
	 */
	public pgDealSearchRepeated1TiledView getRepeated1()
	{
		return (pgDealSearchRepeated1TiledView)getChild(CHILD_REPEATED1);
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btWorkQueueLink_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayWorkQueue();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the changePasswordHref_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleChangePassword();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolHistory_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayDealHistory();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btTooNotes_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayDealNotes();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolSearch_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDealSearch();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolLog_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleSignOff();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public void handleBtBackwardButtonRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btBackwardButton_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
    getRepeated1().handleWebAction(WebActions.ACTION_PREV);
		handler.handleBackwardButton();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtBackwardButton()
	{
		return (Button)getChild(CHILD_BTBACKWARDBUTTON);
	}


	/**
	 *
	 *
	 */
	public String endBtBackwardButtonDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btBackwardButton_onBeforeHtmlOutputEvent method
		boolean result=((doRowGeneratorModelImpl)(getRepeated1().getdoRowGeneratorModel())).hasPreviousResults();

		if(result == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtForwardButtonRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btForwardButton_onWebEvent method
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
    getRepeated1().handleWebAction(WebActions.ACTION_NEXT);
		handler.handleForwardButton();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtForwardButton()
	{
		return (Button)getChild(CHILD_BTFORWARDBUTTON);
	}


	/**
	 *
	 *
	 */
	public String endBtForwardButtonDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btForwardButton_onBeforeHtmlOutputEvent method
		boolean result=((doRowGeneratorModelImpl)(getRepeated1().getdoRowGeneratorModel())).hasMoreResults();

		if(result == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getRowsDisplayed()
	{
		return (StaticTextField)getChild(CHILD_ROWSDISPLAYED);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdRowNum2()
	{
		return (HiddenField)getChild(CHILD_HDROWNUM2);
	}


	/**
	 *
	 *
	 */
	public doRowGenerator2Model getdoRowGenerator2Model()
	{
		if (doRowGenerator2 == null)
			doRowGenerator2 = (doRowGenerator2Model) getModel(doRowGenerator2Model.class);
		return doRowGenerator2;
	}


	/**
	 *
	 *
	 */
	public void setdoRowGenerator2Model(doRowGenerator2Model model)
	{
			doRowGenerator2 = model;
	}

  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}
  //===========================================

  //--> Addition methods to propulate Href display String
  //--> Test by BILLY 07Aug2002
  public String endHref1Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }
  //=====================================================
  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> By Billy 26Nov2003
  public TextField getTbServicingNum()
	{
		return (TextField)getChild(CHILD_TBSERVICINGNUM);
	}
  //========================================================

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	public void handleActMessageOK(String[] args)
	{
		DealSearchHandler handler =(DealSearchHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this, true);
		handler.handleActMessageOK(args);
		handler.postHandlerProtocol();
	}


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgDealSearch";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
	////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDealSearch.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //--Release2.1--//
  //The Option list should not be Static, otherwise this will screw up
  //--> By John 08Nov2002
  //private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
  //==================================================================

	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_TBDEALNUMBER="tbDealNumber";
	public static final String CHILD_TBDEALNUMBER_RESET_VALUE="";
	public static final String CHILD_TBBORROWERFIRSTNAME="tbBorrowerFirstName";
	public static final String CHILD_TBBORROWERFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_TBBORROWERLASTNAME="tbBorrowerLastName";
	public static final String CHILD_TBBORROWERLASTNAME_RESET_VALUE="";
	public static final String CHILD_RBPRIMARYBORROWERONLY="rbPrimaryBorrowerOnly";
	public static final String CHILD_RBPRIMARYBORROWERONLY_RESET_VALUE="Y";
	//private static OptionList rbPrimaryBorrowerOnlyOptions=new OptionList(new String[]{"Primary Borrower on Deal", "All Borrowers"},new String[]{"Y", "N"});
  private RbPrimaryBorrowerOnlyOptionList rbPrimaryBorrowerOnlyOptions=new RbPrimaryBorrowerOnlyOptionList();
	public static final String CHILD_TBBORROWERMAILINGADDRESS="tbBorrowerMailingAddress";
	public static final String CHILD_TBBORROWERMAILINGADDRESS_RESET_VALUE="";
	public static final String CHILD_TBBORROWERHOMEPHONENUMBER1="tbBorrowerHomePhoneNumber1";
	public static final String CHILD_TBBORROWERHOMEPHONENUMBER1_RESET_VALUE="";
	public static final String CHILD_TBBORROWERHOMEPHONENUMBER2="tbBorrowerHomePhoneNumber2";
	public static final String CHILD_TBBORROWERHOMEPHONENUMBER2_RESET_VALUE="";
	public static final String CHILD_TBBORROWERHOMEPHONENUMBER3="tbBorrowerHomePhoneNumber3";
	public static final String CHILD_TBBORROWERHOMEPHONENUMBER3_RESET_VALUE="";
	public static final String CHILD_TBBORROWERWORKPHONENUMBER1="tbBorrowerWorkPhoneNumber1";
	public static final String CHILD_TBBORROWERWORKPHONENUMBER1_RESET_VALUE="";
	public static final String CHILD_TBBORROWERWORKPHONENUMBER2="tbBorrowerWorkPhoneNumber2";
	public static final String CHILD_TBBORROWERWORKPHONENUMBER2_RESET_VALUE="";
	public static final String CHILD_TBBORROWERWORKPHONENUMBER3="tbBorrowerWorkPhoneNumber3";
	public static final String CHILD_TBBORROWERWORKPHONENUMBER3_RESET_VALUE="";
	public static final String CHILD_TBPROPERTYSTREETNUMBER="tbPropertyStreetNumber";
	public static final String CHILD_TBPROPERTYSTREETNUMBER_RESET_VALUE="";
	public static final String CHILD_TBPROPERTYSTREETNAME="tbPropertyStreetName";
	public static final String CHILD_TBPROPERTYSTREETNAME_RESET_VALUE="";
	public static final String CHILD_TBPROPERTYCITY="tbPropertyCity";
	public static final String CHILD_TBPROPERTYCITY_RESET_VALUE="";
	public static final String CHILD_TBSOURCENAME="tbSourceName";
	public static final String CHILD_TBSOURCENAME_RESET_VALUE="";
	public static final String CHILD_TBSOURCEAPPID="tbSourceAppId";
	public static final String CHILD_TBSOURCEAPPID_RESET_VALUE="";
	public static final String CHILD_TBUNDERWRITER="tbUnderwriter";
	public static final String CHILD_TBUNDERWRITER_RESET_VALUE="";
	public static final String CHILD_TBADMINISTRATOR="tbAdministrator";
	public static final String CHILD_TBADMINISTRATOR_RESET_VALUE="";
	public static final String CHILD_TBFUNDER="tbFunder";
	public static final String CHILD_TBFUNDER_RESET_VALUE="";
	public static final String CHILD_BTSEARCH="btSearch";
	public static final String CHILD_BTSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE="Cancel";
	public static final String CHILD_REPEATED1="Repeated1";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_BTBACKWARDBUTTON="btBackwardButton";
	public static final String CHILD_BTBACKWARDBUTTON_RESET_VALUE="Previous";
	public static final String CHILD_BTFORWARDBUTTON="btForwardButton";
	public static final String CHILD_BTFORWARDBUTTON_RESET_VALUE="Next";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_ROWSDISPLAYED="rowsDisplayed";
	public static final String CHILD_ROWSDISPLAYED_RESET_VALUE="";
	public static final String CHILD_HDROWNUM2="hdRowNum2";
	public static final String CHILD_HDROWNUM2_RESET_VALUE="";
  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";
  //--> Ticket#129 -- Support search by Servicing Mortgage #
  //--> By Billy 26Nov2003
  public static final String CHILD_TBSERVICINGNUM="tbServicingNum";
	public static final String CHILD_TBSERVICINGNUM_RESET_VALUE="";
  //========================================================


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doRowGenerator2Model doRowGenerator2=null;
	protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDealSearch.jsp";

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	private DealSearchHandler handler=new DealSearchHandler();

}

