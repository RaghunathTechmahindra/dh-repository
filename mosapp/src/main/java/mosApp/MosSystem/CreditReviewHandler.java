package mosApp.MosSystem;


import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Vector;


import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.DealCalcUtil;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.CreditBureauReport;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.StaticTextField;

/**
 *
 *
 */
public class CreditReviewHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public CreditReviewHandler cloneSS()
	{
		return(CreditReviewHandler) super.cloneSafeShallow();
	}


	////////////////////////////////////////////////////////////////////////////
	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			populatePageShellDisplayFields();
			// Quick Link Menu display
			displayQuickLinkMenu();
			populateTaskNavigator(pg);
			populatePageDealSummarySnapShot();
			populatePreviousPagesLinks();
			generateBureauViewButton(pg);
			setJSValidation();
		}
		catch(Exception e)
		{
			logger.error("Exception @CreditReviewHandler.populatePageDisplayFields()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	/////////////////////////////////////////////////////////////////////

	public void setJSValidation() throws FinderException, RemoteException
	{
		// Validate # of Dependants
		HtmlDisplayFieldBase df = null;
		String theExtraHtml = null;
		ViewBean thePage = getCurrNDPage();

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbCreditScore");
		theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
		
		// Add by Anna for PMI2 -- set Credit Score Field as read only
		// under the condition: 1.From GoTo button/Task/Link History
		// 2.The Deal is Mortgage Insured. 3.MI type coded as Extended Payload = 'Y'
		PageEntry pg = getTheSessionState().getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();
		Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
		if(!(getTheSessionState().getCurrentPage().isSubPage())&&(deal.getMortgageInsurerId()!=0)&&
				(DealCalcUtil.checkIfMIPremiumIsExtended(deal)))			 
		{
			// 1. Customize color
		    String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"border-bottom: hidden d1ebff 0\"" + " style=\"border-left: hidden d1ebff 0\"" + " style=\"border-right: hidden d1ebff 0\"" + " style=\"border-top: hidden d1ebff 0\"" + " style=\"font-weight: bold\"");

		    // 2. Disable edition
		    String disableEdition = new String(" disabled");
		    
		    theExtraHtml = theExtraHtml + changeColorStyle + disableEdition;
		}	
		df.setExtraHtml(theExtraHtml);

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbTimesBankrupt");
		theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
		df.setExtraHtml(theExtraHtml);

		// Set the compare date to today
		//  Validate Month
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/cbBureauMonth");
		theExtraHtml = "onBlur=\"isFieldValidDate('tbBureauYear', 'cbBureauMonth', 'tbBureauDay');\"";
		df.setExtraHtml(theExtraHtml);

		//  Validate Day
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbBureauDay");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('tbBureauYear', 'cbBureauMonth', 'tbBureauDay');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Year
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbBureauYear");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('tbBureauYear', 'cbBureauMonth', 'tbBureauDay');\"";
		df.setExtraHtml(theExtraHtml);

		// Set the compare date to today
		//  Validate Month
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/cbBureauPulledMonth");
		theExtraHtml = "onBlur=\"isFieldValidDate('tbBureauPulledYear', 'cbBureauPulledMonth', 'tbBureauPulledDay');\"";
		df.setExtraHtml(theExtraHtml);

		//  Validate Day
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbBureauPulledDay");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('tbBureauPulledYear', 'cbBureauPulledMonth', 'tbBureauPulledDay');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Year
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbBureauPulledYear");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('tbBureauPulledYear', 'cbBureauPulledMonth', 'tbBureauPulledDay');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate CreditBureau Summary
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbBureauSummary");
		theExtraHtml = "onBlur=\"isFieldMaxLength(2000);\"";
		df.setExtraHtml(theExtraHtml);

		//  ffate Validate Month
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/cbAuthorizationMonth");
		theExtraHtml = "onBlur=\"isFieldValidDate('tbAuthorizationYear', 'cbAuthorizationMonth', 'tbAuthorizationDay');\"";
		df.setExtraHtml(theExtraHtml);

		//  ffate Validate Day
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAuthorizationDay");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('tbAuthorizationYear', 'cbAuthorizationMonth', 'tbAuthorizationDay');\"";
		df.setExtraHtml(theExtraHtml);

		// ffate Validate Year
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("Repeated1/tbAuthorizationYear");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('tbAuthorizationYear', 'cbAuthorizationMonth', 'tbAuthorizationDay');\"";
		df.setExtraHtml(theExtraHtml);
	}

	//--Release2.1--start//
  //// This button image must be generated now based on the languageId. The String
  //// generation is moved inside each method.
  ////final static String bureauViewButton="<td align=left><a href=\"javascript:createBureauWindow();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"/images/BureauViewButton.gif\" width=170 height=16 alt=\"\" border=\"0\"></a></td>";

  //// This button has not been used yet.
	final static String pdfViewButton="<td align=left><a href=\"javascript:createPDFWindow();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"../images/BureauViewButton.gif\" width=170 height=16 alt=\"\" border=\"0\"></a></td>";
	//--Release2.1--end//

	private void generateBureauViewButton(PageEntry pg)
	{
		StaticTextField button =(StaticTextField) getCurrNDPage().getDisplayField("stBureauViewButton");

    //--Release2.1--start//
    //// This button image must be generated now based on the languageId.
    String bureauViewButton = "";
    if (theSessionState.getLanguageId() == 0) { //// English
		  bureauViewButton = "<td align=left><a href=\"javascript:createBureauWindow();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"../images/BureauViewButton.gif\" width=170 height=16 alt=\"\" border=\"0\"></a></td>";
    }
    if (theSessionState.getLanguageId() == 1) { //// French
		  bureauViewButton = "<td align=left><a href=\"javascript:createBureauWindow();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"../images/BureauViewButton_fr.gif\" width=220 height=16 alt=\"\" border=\"0\"></a></td>";
    }
    //--Release2.1--end//

		button.setValue(new String(bureauViewButton));
	}


	////////////////////////////////////////////////////////////////////////////
	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		try
		{
			SysLogger logger = getSessionResourceKit().getSysLogger();

			//logger.debug("****** CR - setupBefore....");
			//getSavedPages().show(logger);
			PageEntry pg = getTheSessionState().getCurrentPage();
			if (pg.getSetupBeforeGenerationCalled() == true) return;
			pg.setSetupBeforeGenerationCalled(true);

			// -- //
			setupDealSummarySnapShotDO(pg);

			// set the criteria for the populating data object
			QueryModelBase theDO =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doCreditReviewModel.class));
			theDO.clearUserWhereCriteria();
			theDO.addUserWhereCriterion("dfDealId", "=", new String("" + pg.getPageDealId()));
			theDO.addUserWhereCriterion("dfCopyId", "=", new String("" + pg.getPageDealCID()));
			theDO.executeSelect(null);
			int numRows = theDO.getSize();
			PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
			Hashtable pst = pg.getPageStateTable();
			if (tds == null)
			{
				tds = new PageCursorInfo("DEFAULT", "Repeated1", numRows, numRows);
				tds.setRefresh(true);
				pst.put(tds.getName(), tds);
			}
			else
			{
				tds.setTotalRows(numRows);
				tds.setRowsPerPage(numRows);
			}
			setupRowGenerator(pst);
		}
		catch(Exception e)
		{
			logger.error("Exception @CreditReviewHandler.setupBeforePageGeneration()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	////////////////////////////////////////////////////////////////////////////

	private void setupRowGenerator(Hashtable pst)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			CreditBureauReport report = new CreditBureauReport(srk);
			Vector reports = new Vector();
			Vector vect =(Vector)(report.findByDeal(new DealPK(pg.getPageDealId(), 
                pg.getPageDealCID())));
			for(int i = 0; i < vect.size();	++ i)
        reports.add(((CreditBureauReport)(vect.get(i))).getCreditReport());
			setRowGeneratorDOForNRows(reports.size());
			pst.put("reportVector", reports);
		}
		catch(Exception e)
		{
			logger.error("Exception @CreditReviewHandler.setupRowGenerator()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	////////////////////////////////////////////////////////////////////////////

	public boolean populateBureauReport(int ind)
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		Vector reports =(Vector) pst.get("reportVector");

		if (reports.size() < 1)
		{
			return false;
		}


		// ALERT :: Commented this oout to not doing anything before we fixing the Data problem of the Bureau from Morty
		//          The Report content may contain some hidden char e.g. Char(0) ==> caused JavaScript problems.
		//getCurrNDPage().setDisplayFieldValue("Repeated2.stCreditBureauReport", new String( (String)(reports.get(ind)) ) );
		String tmpStr =(String)(reports.get(ind));

		//tmpStr = this.replaceString("\0", tmpStr, "\\0");
		//logger.debug("BILLY ==> The Orig Str = " + tmpStr);
		//tmpStr = StringTokenizer2.replace(tmpStr, "\0", "\\0");
		tmpStr = convertToHtmString(tmpStr);

		//logger.debug("BILLY ==> The Str After convert = " + tmpStr);
		getCurrNDPage().setDisplayFieldValue("Repeated2/stCreditBureauReport", new String(tmpStr));

		return true;
	}


	////////////////////////////////////////////////////////////////////////////
//--> This is not necessary to store the Model.  As we should get the model from
//--> RequestManager.
//--> Commneted out by Billy 29Aug2002
/*
	public void saveDataObject(CSpDataObject data)
	{
		try
		{
			if (! data.getName().equals("doCreditReview")) return;
			if (! data.succeeded())
			{
				logger.error("ERROR IN THE DATAOBJECT!!");
				return;
			}
			PageEntry pg = getTheSessionState().getCurrentPage();
			Hashtable pst = pg.getPageStateTable();
			pst.put("result", data);
			return;
		}
		catch(Exception e)
		{
			logger.error("Exception @CreditReviewHandler.saveDataObject()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}
*/
//=========================================================================


	////////////////////////////////////////////////////////////////////////////

	public void populateBureauDates(int index)
	{
		populateBureauDate(index, "dfBureauOnfile", "Bureau");
		populateBureauDate(index, "dfBureauProfile", "BureauPulled");
		populateBureauDate(index, "dfAuthorization", "Authorization");
	}


	////////////////////////////////////////////////////////////////////////////

	private void populateBureauDate(int index, String dfBureau, String bureau)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			//Hashtable pst = pg.getPageStateTable();
			//CSpDataObject data =(CSpDataObject) pst.get("result");
      QueryModelBase data =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doCreditReviewModel.class));

      //Object val = data.getValue(dfBureau);
  		java.util.Date dt = (java.util.Date)data.getValue(dfBureau);

			if (dt != null)
			{
				getCurrNDPage().setDisplayFieldValue("Repeated1/cb" + bureau + "Month", new Integer(dt.getMonth() + 1));
				getCurrNDPage().setDisplayFieldValue("Repeated1/tb" + bureau + "Day", new String("" + dt.getDate()));
				getCurrNDPage().setDisplayFieldValue("Repeated1/tb" + bureau + "Year", new String("" + (dt.getYear()+1900)));
			}
		}
		catch(Exception e)
		{
			logger.error("Exception @CreditReviewHandler.populateBureauDate() : Index = " + index);
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	public void populateFlags(int index)
	{
		sayYesOrNo(index, "dfPrimaryBorrowerFlag", "Repeated1/stPrimaryBorrowerFlag");
		sayYesOrNo(index, "dfExistingClient", "Repeated1/stExistingClient");
	}


	////////////////////////////////////////////////////////////////////////////
	private void sayYesOrNo(int index, String dfFieldName, String fieldName)
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
      QueryModelBase data =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doCreditReviewModel.class));
      			Object o = data.getValue(dfFieldName);
      			String strField = null;
      			if ( o != null)
      			    strField = data.getValue(dfFieldName).toString();
      			if (strField == null || strField.length() == 0) return;

      int lang = theSessionState.getLanguageId();

			getCurrNDPage().setDisplayFieldValue(fieldName,
        new String(Character.toUpperCase(
            strField.charAt(0)) == 'Y' ? BXResources.getGenericMsg("YES_LABEL", lang) :
                                         BXResources.getGenericMsg("NO_LABEL", lang)));
		}
		catch(Exception e)
		{
			logger.error("Exception @CreditReviewHandler.populateFlags() : Index = " + index);
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	public void handleSubmitStandard()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getPageCondition3() == true)
		{
			pg.setPageCondition3(false);
			return;
		}

		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			SysLogger logger = getSessionResourceKit().getSysLogger();

			//logger.debug("****** CR - handleSubmitStandard :: IN");
			//getSavedPages().show(logger);
			srk.beginTransaction();
			standardAdoptTxCopy(pg, true);

			//logger.debug("****** CR - handleSubmitStandard :: BEFORE NAVIGATE");
			//getSavedPages().show(logger);
			navigateToNextPage();

			//logger.debug("****** CR - handleSubmitStandard :: AFTER NAVIGATE");
			//getSavedPages().show(logger);
			srk.commitTransaction();
		}
		catch(Exception e)
		{
			//logger.debug("****** CR - handleSubmitStandard :: ON EXCEPTION");
			//getSavedPages().show(logger);
			srk.cleanTransaction();
			logger.error("Exception @CreditReviewHandler.handleSubmitStandard()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	public void saveData(PageEntry pg, boolean calledInTransaction)
	{
		try
		{
			SessionResourceKit srk = getSessionResourceKit();
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);
			if (calledInTransaction == false) srk.beginTransaction();
			if (updateBorrowers(pg, srk, dcm) < 0)
			{
				if (calledInTransaction == false) srk.cleanTransaction();
				pg.setPageCondition3(true);
				return;
			}
			dcm.calc();
			if (calledInTransaction == false) srk.commitTransaction();
			//FXP26405: Removed unnecessary logging around the line below.
			pg.setModified(true);
		}
		catch(Exception e)
		{
			if (calledInTransaction == false) srk.cleanTransaction();
			logger.error("Exception @CreditReviewHandler.saveData()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	////////////////////////////////////////////////////////////////////////////
	private int updateBorrowers(PageEntry pg, SessionResourceKit srk, CalcMonitor dcm)
		throws Exception
	{
		int borrowerId = 0;
		int copyId = pg.getPageDealCID();

		Vector hdBorrowerId = getRepeatedFieldValues("Repeated1/hdBorrowerId");
		Vector tbExistingClientComments = getRepeatedFieldValues("Repeated1/tbExistingClientComments");
		Vector tbAuthorizationMethod = getRepeatedFieldValues("Repeated1/tbAuthorizationMethod");
		Vector cbBureauName = getRepeatedFieldValues("Repeated1/cbBureauName");
		Vector bureauMonth = getRepeatedFieldValues("Repeated1/cbBureauMonth");
		Vector bureauDay = getRepeatedFieldValues("Repeated1/tbBureauDay");
		Vector bureauYear = getRepeatedFieldValues("Repeated1/tbBureauYear");
		Vector bureauPulledMonth = getRepeatedFieldValues("Repeated1/cbBureauPulledMonth");
		Vector bureauPulledDay = getRepeatedFieldValues("Repeated1/tbBureauPulledDay");
		Vector bureauPulledYear = getRepeatedFieldValues("Repeated1/tbBureauPulledYear");
		Vector AuthorizationMonth = getRepeatedFieldValues("Repeated1/cbAuthorizationMonth");
		Vector AuthorizationDay = getRepeatedFieldValues("Repeated1/tbAuthorizationDay");
		Vector AuthorizationYear = getRepeatedFieldValues("Repeated1/tbAuthorizationYear");
		Vector tbBureauSummary = getRepeatedFieldValues("Repeated1/tbBureauSummary");
		Vector tbCreditScore = getRepeatedFieldValues("Repeated1/tbCreditScore");
		Vector tbTimesBankrupt = getRepeatedFieldValues("Repeated1/tbTimesBankrupt");
		Vector cbBankruptStatus = getRepeatedFieldValues("Repeated1/cbBankruptStatus");
		Vector cbNSFHistory = getRepeatedFieldValues("Repeated1/cbNSFHistory");
		Vector cbPaymentHistory = getRepeatedFieldValues("Repeated1/cbPaymentHistory");
		Vector cbGeneralStatus = getRepeatedFieldValues("Repeated1/cbGeneralStatus");

		if (hdBorrowerId != null && hdBorrowerId.size() > 0)
		{
			// There are multiple row values in this display field
			int vectorSize = hdBorrowerId.size();
			for(int i = 0;	i < vectorSize;	i ++)
			{
        borrowerId = com.iplanet.jato.util.TypeConverter.asInt(hdBorrowerId.get(i), -1);
				if (borrowerId >= 0)
				{
					Borrower borrower = new Borrower(srk, dcm, borrowerId, copyId);
					borrower.setExistingClientComments(tbExistingClientComments.get(i).toString());
					borrower.setCbAuthorizationMethod(tbAuthorizationMethod.get(i).toString());

         borrower.setCreditBureauNameId(
          com.iplanet.jato.util.TypeConverter.asInt(cbBureauName.get(i), Sc.CREDIT_BUREAU_NONE));

					GregorianCalendar calendar = new GregorianCalendar();
					Date dt = null;
					//			   		if( ((Vector)bureauMonth).get(i).isNumeric() /*&& bureauDay.isNumeric() && bureauYear.isNumeric()*/)
					//					{
					try
					{
						calendar.set(
                com.iplanet.jato.util.TypeConverter.asInt(bureauYear.get(i)),
                com.iplanet.jato.util.TypeConverter.asInt(bureauMonth.get(i)) - 1,
                com.iplanet.jato.util.TypeConverter.asInt(bureauDay.get(i)));
						dt = calendar.getTime();
						borrower.setCreditBureauOnFileDate(dt);
					}
					catch(Exception e)
					{
						logger.error("CreditReview:Warning:Incorrect Bureau Pulled Date()");

						//setActiveMessageToAlert(Sc.BUREAU_DATE_INVALID,
						//	ActiveMsgFactory.ISCUSTOMCONFIRM);
						//return -1;
						borrower.setCreditBureauOnFileDate(null);
					}

					//					}
					dt = null;

					//					if(  ((Vector)bureauPulledMonth).get(i).isNumeric() /*&& bureauPulledDay.isNumeric() && bureauPulledYear.isNumeric()*/)
					//					{
					try
					{
						calendar.set(
                com.iplanet.jato.util.TypeConverter.asInt(bureauPulledYear.get(i)),
                com.iplanet.jato.util.TypeConverter.asInt(bureauPulledMonth.get(i)) - 1,
                com.iplanet.jato.util.TypeConverter.asInt(bureauPulledDay.get(i)));
						dt = calendar.getTime();
						borrower.setDateOfCreditBureauProfile(dt);
					}
					catch(Exception e)
					{
						logger.error("CreditReview:Warning:Incorrect Bureau Onfile Date()");

						//setActiveMessageToAlert(Sc.BUREAU_PULLED_DATE_INVALID,
						//	ActiveMsgFactory.ISCUSTOMCONFIRM);
						//return -1;
						borrower.setDateOfCreditBureauProfile(null);
					}

					try
					{
						calendar.set(
								com.iplanet.jato.util.TypeConverter.asInt(AuthorizationYear.get(i)),
								com.iplanet.jato.util.TypeConverter.asInt(AuthorizationMonth.get(i)) - 1,
								com.iplanet.jato.util.TypeConverter.asInt(AuthorizationDay.get(i)));
								dt = calendar.getTime();
						borrower.setCbAuthorizationDate(dt);
					}
					catch(Exception e)
					{
						logger.error("CreditReview:Warning:Incorrect Bureau Onfile Date()");

						//setActiveMessageToAlert(Sc.BUREAU_PULLED_DATE_INVALID,
						//	ActiveMsgFactory.ISCUSTOMCONFIRM);
						//return -1;
						borrower.setCbAuthorizationDate(null);
					}
					//					}
					// Modified to usethe new field -- By BILLY 22Jan2002
					//borrower.setCreditSummary(((Vector)tbBureauSummary).get(i).toString());
					borrower.setCreditBureauSummary(tbBureauSummary.get(i).toString());
					borrower.setCreditScore(com.iplanet.jato.util.TypeConverter.asInt(tbCreditScore.get(i), 0));
					borrower.setNumberOfTimesBankrupt(com.iplanet.jato.util.TypeConverter.asInt(tbTimesBankrupt.get(i), 0));
					borrower.setBankruptcyStatusId(com.iplanet.jato.util.TypeConverter.asInt(cbBankruptStatus.get(i), Sc.BANKRUPTCY_STATUS_NEVER_BANKRUPT));
					borrower.setNSFOccurenceTypeId(com.iplanet.jato.util.TypeConverter.asInt(cbNSFHistory.get(i), Sc.NSF_OCCURENCE_NO_NSF));
					borrower.setPaymentHistoryTypeId(com.iplanet.jato.util.TypeConverter.asInt(cbPaymentHistory.get(i), Sc.PAYMENT_HISTORY_EXCELLENT));
					borrower.setBorrowerGeneralStatusId(com.iplanet.jato.util.TypeConverter.asInt(cbGeneralStatus.get(i), Sc.BORROWER_GENERAL_STATUS_NEITHER_DELQ_NSF));

					borrower.ejbStore();
				}
			}
		}
		return 0;
	}
}

