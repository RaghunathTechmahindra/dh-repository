package mosApp.MosSystem;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.text.*;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.entity.*;

import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

public class JSValidationRules implements Serializable, Sc
{
	String 		propertyFileName;
	SysLogger 	logger;
	ViewBean thePage;

	Vector	calls;
	
	private static final Log _log = LogFactory.getLog(JSValidationRules.class);

	public JSValidationRules( String propertyFileName, SysLogger logger, ViewBean thePage)
	throws IOException
	{
		this.propertyFileName = propertyFileName;
		this.logger = logger;
		this.thePage = thePage;

		calls = new Vector();
		load( propertyFileName );
	}

	public JSValidationRules( String propertyFileName, ViewBean thePage)
	throws IOException
	{
		this.propertyFileName = propertyFileName;
		this.thePage = thePage;

		calls = new Vector();
		load( propertyFileName );
	}

	private void load( String propertyFileName )
	throws IOException, IllegalArgumentException
	{
        String simpleClassName = this.getClass().getSimpleName();
        _log.info("JSValidation className: " + simpleClassName);

        URL lsLocation = this.getClass()
                .getResource(simpleClassName + ".class");

        String dir = lsLocation.toString();

        int index = dir.indexOf("mosApp/MosSystem");
        
        String value = dir.substring(0, index) + "com/filogix/express/web/dvalidprops/";

        _log.info("CalcSet lsLocation: " + lsLocation);
        String filename=(value+propertyFileName);
        LineNumberReader reader = null;
        try {
    //      reader = new LineNumberReader( new FileReader( (value+propertyFileName).substring("file:/".length()) ) );
            reader = new LineNumberReader( new FileReader( new File(new URI(value+propertyFileName))) );
        } catch (URISyntaxException uris) {
            _log.error("fileName: " + filename + " - couldn't find validation file for JSValidation" );
            throw new IllegalArgumentException(uris);
        }
        
        
		String currCall = null;
		while( ( currCall = reader.readLine() ) != null ) {
			 calls.add( parse( currCall ) );
		}
	}

	private JSCall parse( String call )
	throws IllegalArgumentException
	{
		StringTokenizer tokenizer = new StringTokenizer( call, new String( new char[]{ JS_PROPERTY_DELIMITER } ) );

		int numTokens = tokenizer.countTokens();

		if( numTokens < 3 ) {
			throw new IllegalArgumentException("A JS call string = " + call + " has too few parameters.");
		}

    boolean append = true;
		String func_name = tokenizer.nextToken();
		String attach_field = tokenizer.nextToken();
		String event_type = tokenizer.nextToken();
		String[] arguments = new String[numTokens - 3];

		if (numTokens == 3)
		    append = false;
		else
		    append = true;

	        for( int i = 0; i < numTokens - 3; i++ ) {
			arguments[i] = tokenizer.nextToken();
			if(arguments[i].equals("FIRST_DATE_COMPARE"))
			{
			    DateFormat firstFormat = new SimpleDateFormat("MMM dd yyyy");
                            String firstDateCompare = firstFormat.format(new Date());
                            arguments[i] = firstDateCompare;
////temp
////logger.debug("JSVR@TheDateCompare: " + firstDateCompare);
////logger.debug("JSVR@Argument[" + i + "]: " + arguments[i]);
			}
			else if(arguments[i].equals("SECOND_DATE_COMPARE"))
			{
			    DateFormat secondFormat = new SimpleDateFormat("MMM dd yyyy");
                            String secondDateCompare = secondFormat.format(new Date());
                            arguments[i] = secondDateCompare;
////temp
////logger.debug("JSVR@TheDateCompare: " + secondDateCompare);
////logger.debug("JSVR@Argument[" + i + "]: " + arguments[i]);
			}
			else if(arguments[i].equals("EMPTY_COMPARE"))
			{
                            arguments[i] = "";
////temp
////logger.debug("JSVR@Argument[" + i + "]: " + arguments[i]);
			}
		}

		return new JSCall( func_name, attach_field, event_type, arguments, append );
	}


	public void attachJSValidationRules()
	{
		if( calls != null ) {
			for( int i = 0; i < calls.size(); i++ ) {
				JSCall currCall = (JSCall)( calls.elementAt( i ) );

////logger.debug("JSVR@CurrentCall: " + currCall.toString());

                                HtmlDisplayFieldBase disp_field = (HtmlDisplayFieldBase)thePage.getDisplayField( currCall.attach_field );

////logger.debug("JSVR@DisplayFieldMainBean: " + disp_field);

                                disp_field.setExtraHtml( currCall.toString() );
			}
		}
	}
}


/**
*	This class represents a basic data structure for recording information about a single JavaScript function call
*
* @author	Vladimir Ivanov
*/

class JSCall
{
	String	func_name;
	String	attach_field;
	String	event_type;
	String	arguments[];
	boolean appendArgs;

	JSCall( String func_name, String attach_field, String event_type, String[] arguments, boolean appendArgs)
	{
		this.func_name = func_name;
		this.attach_field = attach_field;
		this.event_type = event_type;
		this.arguments = arguments;
		this.appendArgs = appendArgs;
	}

	public String toString()
	{
		StringBuffer result = new StringBuffer();

		result.append( event_type + "=" );
		result.append( "\"" + func_name);
		if (appendArgs == true)
		{
     		result.append( "(" );
			for( int i = 0; i < arguments.length; i++ ) {
				result.append("\'" + arguments[i] + "\'");
				if( i != arguments.length - 1 ) {
					result.append(", ");
				}
			}
			result.append(")");
        }
		result.append( ";\"" );

		return result.toString();
        }

}