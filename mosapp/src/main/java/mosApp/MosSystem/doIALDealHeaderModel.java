package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doIALDealHeaderModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalLiabilities();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalLiabilities(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalAssets();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalAssets(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalIncome();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalIncome(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalLTV();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalLTV(java.math.BigDecimal value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFTOTALLIABILITIES="dfTotalLiabilities";
	public static final String FIELD_DFTOTALASSETS="dfTotalAssets";
	public static final String FIELD_DFTOTALINCOME="dfTotalIncome";
	public static final String FIELD_DFTOTALTDS="dfTotalTDS";
	public static final String FIELD_DFTOTALGDS="dfTotalGDS";
	public static final String FIELD_DFTOTALLTV="dfTotalLTV";
	 public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

