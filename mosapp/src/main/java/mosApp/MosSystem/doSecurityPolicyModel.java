package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doSecurityPolicyModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfSECURITYPOLICYID();

	
	/**
	 * 
	 * 
	 */
	public void setDfSECURITYPOLICYID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSECPOLDESCRIPTION();

	
	/**
	 * 
	 * 
	 */
	public void setDfSECPOLDESCRIPTION(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfVALUE();

	
	/**
	 * 
	 * 
	 */
	public void setDfVALUE(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfCHANGEDATE();

	
	/**
	 * 
	 * 
	 */
	public void setDfCHANGEDATE(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCHANGEDBYUSERID();

	
	/**
	 * 
	 * 
	 */
	public void setDfCHANGEDBYUSERID(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFSECURITYPOLICYID="dfSECURITYPOLICYID";
	public static final String FIELD_DFSECPOLDESCRIPTION="dfSECPOLDESCRIPTION";
	public static final String FIELD_DFVALUE="dfVALUE";
	public static final String FIELD_DFCHANGEDATE="dfCHANGEDATE";
	public static final String FIELD_DFCHANGEDBYUSERID="dfCHANGEDBYUSERID";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

