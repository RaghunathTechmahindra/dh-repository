package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
// MigrationToDo : Still Needed? import java.awt.event.*;
// MigrationToDo : Still Needed? import spider.event.*;
// MigrationToDo : Still Needed? import spider.database.*;
// MigrationToDo : Still Needed? import spider.visual.*;
// MigrationToDo : Still Needed? import spider.*;
// MigrationToDo : Still Needed? import spider.session.*;
// MigrationToDo : Still Needed? import spider.html.*;
// MigrationToDo : Still Needed? import spider.util.*;

/**
 *
 *
 *
 */
public class pgBridgeReviewViewBean extends ViewBeanBase
	implements ViewBean
{
	/**
	 *
	 *
	 */
	public pgBridgeReviewViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_TBDEALID))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("Choose a Page");
			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALID))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALID,
				doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
				CHILD_STDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORRFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME,
				CHILD_STBORRFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUS,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
				CHILD_STDEALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALSTATUSDATE,
				doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
				CHILD_STDEALSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCEFIRM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCEFIRM,
				doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
				CHILD_STSOURCEFIRM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSOURCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSOURCE,
				doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
				CHILD_STSOURCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLOB))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STLOB,
				doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
				CHILD_STLOB_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALTYPE,
				doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
				CHILD_STDEALTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALPURPOSE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STDEALPURPOSE,
				doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
				CHILD_STDEALPURPOSE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTOTALLOANAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STTOTALLOANAMOUNT,
				doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
				CHILD_STTOTALLOANAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPURCHASEPRICE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPURCHASEPRICE,
				doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
				CHILD_STPURCHASEPRICE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTTERM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STPMTTERM,
				doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
				CHILD_STPMTTERM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STESTCLOSINGDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STESTCLOSINGDATE,
				doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
				CHILD_STESTCLOSINGDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSPECIALFEATURE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealSummarySnapShotModel(),
				CHILD_STSPECIALFEATURE,
				doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
				CHILD_STSPECIALFEATURE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBBRIDGEPURPOSE))
		{
			ComboBox child = new ComboBox( this,
				getdoBridgeReviewNewModel(),
				CHILD_CBBRIDGEPURPOSE,
				doBridgeReviewNewModel.FIELD_DFBRIDGEPURPOSE,
				CHILD_CBBRIDGEPURPOSE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbBridgePurposeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBLENDER))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBLENDER,
				CHILD_CBLENDER,
				CHILD_CBLENDER_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbLenderOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBPRODUCT))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPRODUCT,
				CHILD_CBPRODUCT,
				CHILD_CBPRODUCT_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbProductOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBPOSTEDINTERESTRATE))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPOSTEDINTERESTRATE,
				CHILD_CBPOSTEDINTERESTRATE,
				CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPostedInterestRateOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBDISCOUNT))
		{
			TextField child = new TextField(this,
				getdoBridgeReviewNewModel(),
				CHILD_TBDISCOUNT,
				doBridgeReviewNewModel.FIELD_DFDISCOUNT,
				CHILD_TBDISCOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPREMIUM))
		{
			TextField child = new TextField(this,
				getdoBridgeReviewNewModel(),
				CHILD_TBPREMIUM,
				doBridgeReviewNewModel.FIELD_DFPREMIUM,
				CHILD_TBPREMIUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STNETRATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoBridgeReviewNewModel(),
				CHILD_STNETRATE,
				doBridgeReviewNewModel.FIELD_DFNETRATE,
				CHILD_STNETRATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBRIDGELOANAMOUNT))
		{
			TextField child = new TextField(this,
				getdoBridgeReviewNewModel(),
				CHILD_TBBRIDGELOANAMOUNT,
				doBridgeReviewNewModel.FIELD_DFBRIDGELOANAMOUNT,
				CHILD_TBBRIDGELOANAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBMATURITYDATEMONTH))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBMATURITYDATEMONTH,
				CHILD_CBMATURITYDATEMONTH,
				CHILD_CBMATURITYDATEMONTH_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMaturityDateMonthOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBMATURITYDATEDAY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBMATURITYDATEDAY,
				CHILD_TBMATURITYDATEDAY,
				CHILD_TBMATURITYDATEDAY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBMATURITYDATEYEAR))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBMATURITYDATEYEAR,
				CHILD_TBMATURITYDATEYEAR,
				CHILD_TBMATURITYDATEYEAR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCALCULATION))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCALCULATION,
				CHILD_BTCALCULATION,
				CHILD_BTCALCULATION_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTDELETE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETE,
				CHILD_BTDELETE,
				CHILD_BTDELETE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_RPSALEADDRESS))
		{
			pgBridgeReviewrpSaleAddressTiledView child = new pgBridgeReviewrpSaleAddressTiledView(this,
				CHILD_RPSALEADDRESS);
			return child;
		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDEDSSSTART))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINCLUDEDSSSTART,
				CHILD_STINCLUDEDSSSTART,
				CHILD_STINCLUDEDSSSTART_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDEDSSEND))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINCLUDEDSSEND,
				CHILD_STINCLUDEDSSEND,
				CHILD_STINCLUDEDSSEND_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_ROWSDISPLAYED))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_ROWSDISPLAYED,
				CHILD_ROWSDISPLAYED,
				CHILD_ROWSDISPLAYED_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLENDERPROFILEID))
		{
			StaticTextField child = new StaticTextField(this,
				getdoFetchLenderProfileIdModel(),
				CHILD_STLENDERPROFILEID,
				doFetchLenderProfileIdModel.FIELD_DFLENDERPROFILEID,
				CHILD_STLENDERPROFILEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBRATECODE))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBRATECODE,
				CHILD_TBRATECODE,
				CHILD_TBRATECODE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPAYMENTTERMDESCRIPTION))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPAYMENTTERMDESCRIPTION,
				CHILD_TBPAYMENTTERMDESCRIPTION,
				CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STRATETIMESTAMP))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STRATETIMESTAMP,
				CHILD_STRATETIMESTAMP,
				CHILD_STRATETIMESTAMP_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDLONGPOSTEDDATE))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDLONGPOSTEDDATE,
				CHILD_HDLONGPOSTEDDATE,
				CHILD_HDLONGPOSTEDDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLENDERPRODUCTID))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STLENDERPRODUCTID,
				CHILD_STLENDERPRODUCTID,
				CHILD_STLENDERPRODUCTID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTOK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTOK,
				CHILD_BTOK,
				CHILD_BTOK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STAPPDATEFORSERVLET))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPDATEFORSERVLET,
				CHILD_STAPPDATEFORSERVLET,
				CHILD_STAPPDATEFORSERVLET_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STVIEWONLYTAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
		getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
		getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
		getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
		getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
		getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
		getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
		getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
		getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
		getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
		getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
		getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
		getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
		getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getCbBridgePurpose().setValue(CHILD_CBBRIDGEPURPOSE_RESET_VALUE);
		getCbLender().setValue(CHILD_CBLENDER_RESET_VALUE);
		getCbProduct().setValue(CHILD_CBPRODUCT_RESET_VALUE);
		getCbPostedInterestRate().setValue(CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE);
		getTbDiscount().setValue(CHILD_TBDISCOUNT_RESET_VALUE);
		getTbPremium().setValue(CHILD_TBPREMIUM_RESET_VALUE);
		getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
		getTbBridgeLoanAmount().setValue(CHILD_TBBRIDGELOANAMOUNT_RESET_VALUE);
		getCbMaturityDateMonth().setValue(CHILD_CBMATURITYDATEMONTH_RESET_VALUE);
		getTbMaturityDateDay().setValue(CHILD_TBMATURITYDATEDAY_RESET_VALUE);
		getTbMaturityDateYear().setValue(CHILD_TBMATURITYDATEYEAR_RESET_VALUE);
		getBtCalculation().setValue(CHILD_BTCALCULATION_RESET_VALUE);
		getBtDelete().setValue(CHILD_BTDELETE_RESET_VALUE);
		getRpSaleAddress().resetChildren();
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getStIncludeDSSstart().setValue(CHILD_STINCLUDEDSSSTART_RESET_VALUE);
		getStIncludeDSSend().setValue(CHILD_STINCLUDEDSSEND_RESET_VALUE);
		getRowsDisplayed().setValue(CHILD_ROWSDISPLAYED_RESET_VALUE);
		getStLenderProfileId().setValue(CHILD_STLENDERPROFILEID_RESET_VALUE);
		getTbRateCode().setValue(CHILD_TBRATECODE_RESET_VALUE);
		getTbPaymentTermDescription().setValue(CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE);
		getStRateTimeStamp().setValue(CHILD_STRATETIMESTAMP_RESET_VALUE);
		getHdLongPostedDate().setValue(CHILD_HDLONGPOSTEDDATE_RESET_VALUE);
		getStLenderProductId().setValue(CHILD_STLENDERPRODUCTID_RESET_VALUE);
		getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
		getStAppDateForServlet().setValue(CHILD_STAPPDATEFORSERVLET_RESET_VALUE);
		getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_STDEALID,StaticTextField.class);
		registerChild(CHILD_STBORRFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUS,StaticTextField.class);
		registerChild(CHILD_STDEALSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STSOURCEFIRM,StaticTextField.class);
		registerChild(CHILD_STSOURCE,StaticTextField.class);
		registerChild(CHILD_STLOB,StaticTextField.class);
		registerChild(CHILD_STDEALTYPE,StaticTextField.class);
		registerChild(CHILD_STDEALPURPOSE,StaticTextField.class);
		registerChild(CHILD_STTOTALLOANAMOUNT,StaticTextField.class);
		registerChild(CHILD_STPURCHASEPRICE,StaticTextField.class);
		registerChild(CHILD_STPMTTERM,StaticTextField.class);
		registerChild(CHILD_STESTCLOSINGDATE,StaticTextField.class);
		registerChild(CHILD_STSPECIALFEATURE,StaticTextField.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_CBBRIDGEPURPOSE,ComboBox.class);
		registerChild(CHILD_CBLENDER,ComboBox.class);
		registerChild(CHILD_CBPRODUCT,ComboBox.class);
		registerChild(CHILD_CBPOSTEDINTERESTRATE,ComboBox.class);
		registerChild(CHILD_TBDISCOUNT,TextField.class);
		registerChild(CHILD_TBPREMIUM,TextField.class);
		registerChild(CHILD_STNETRATE,StaticTextField.class);
		registerChild(CHILD_TBBRIDGELOANAMOUNT,TextField.class);
		registerChild(CHILD_CBMATURITYDATEMONTH,ComboBox.class);
		registerChild(CHILD_TBMATURITYDATEDAY,TextField.class);
		registerChild(CHILD_TBMATURITYDATEYEAR,TextField.class);
		registerChild(CHILD_BTCALCULATION,Button.class);
		registerChild(CHILD_BTDELETE,Button.class);
		registerChild(CHILD_RPSALEADDRESS,pgBridgeReviewrpSaleAddressTiledView.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_STINCLUDEDSSSTART,StaticTextField.class);
		registerChild(CHILD_STINCLUDEDSSEND,StaticTextField.class);
		registerChild(CHILD_ROWSDISPLAYED,StaticTextField.class);
		registerChild(CHILD_STLENDERPROFILEID,StaticTextField.class);
		registerChild(CHILD_TBRATECODE,TextField.class);
		registerChild(CHILD_TBPAYMENTTERMDESCRIPTION,TextField.class);
		registerChild(CHILD_STRATETIMESTAMP,StaticTextField.class);
		registerChild(CHILD_HDLONGPOSTEDDATE,HiddenField.class);
		registerChild(CHILD_STLENDERPRODUCTID,StaticTextField.class);
		registerChild(CHILD_BTOK,Button.class);
		registerChild(CHILD_STAPPDATEFORSERVLET,StaticTextField.class);
		registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealSummarySnapShotModel());;modelList.add(getdoBridgeReviewNewModel());;modelList.add(getdoFetchLenderProfileIdModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);


		// The following code block was migrated from the this_onBeforeDataObjectExecuteEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

		handler.pageSaveState();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);


		// The following code block was migrated from the this_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		int rc = PROCEED;

		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatePageDisplayFields();

		handler.pageSaveState();

		return(rc);
		*/
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
	static class CbPageNamesOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbPageNamesOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
			try {
				clear();
				SelectQueryModel m = null;

				SelectQueryExecutionContext eContext=new SelectQueryExecutionContext(
					(Connection)null,
					DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
					DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

				if(rc == null) {
					m = new doPageNameLabelModelImpl();
					c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
					eContext.setConnection(c);
				}
				else {
					m = (SelectQueryModel) rc.getModelManager().getModel(doPageNameLabelModel.class);
				}

				m.retrieve(eContext);
				m.beforeFirst();

				while (m.next()) {
					Object dfPageLabel = m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);

					String label = (dfPageLabel == null?"":dfPageLabel.toString());

					String value = (dfPageLabel == null?"":dfPageLabel.toString());

					add(label, value);
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null)
						c.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}

	}



	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btProceed_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleGoPage();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href1_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(0);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href2_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(1);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href3_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(2);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href4_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(3);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href5_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(4);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href6_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(5);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href7_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(6);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href8_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(7);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href9_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(8);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href10_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(9);

		return handler.postHandlerProtocol();
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealId()
	{
		return (StaticTextField)getChild(CHILD_STDEALID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrFirstName()
	{
		return (StaticTextField)getChild(CHILD_STBORRFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatus()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STDEALSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSourceFirm()
	{
		return (StaticTextField)getChild(CHILD_STSOURCEFIRM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSource()
	{
		return (StaticTextField)getChild(CHILD_STSOURCE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLOB()
	{
		return (StaticTextField)getChild(CHILD_STLOB);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealType()
	{
		return (StaticTextField)getChild(CHILD_STDEALTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealPurpose()
	{
		return (StaticTextField)getChild(CHILD_STDEALPURPOSE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTotalLoanAmount()
	{
		return (StaticTextField)getChild(CHILD_STTOTALLOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPurchasePrice()
	{
		return (StaticTextField)getChild(CHILD_STPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmtTerm()
	{
		return (StaticTextField)getChild(CHILD_STPMTTERM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEstClosingDate()
	{
		return (StaticTextField)getChild(CHILD_STESTCLOSINGDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSpecialFeature()
	{
		return (StaticTextField)getChild(CHILD_STSPECIALFEATURE);
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btWorkQueueLink_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayWorkQueue();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the changePasswordHref_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleChangePassword();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolHistory_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealHistory();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btTooNotes_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealNotes();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolSearch_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDealSearch();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolLog_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSignOff();

		handler.postHandlerProtocol();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btPrevTaskPage_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handlePrevTaskPage();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btNextTaskPage_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleNextTaskPage();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.generateNextTaskPage();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.generateNextTaskPage();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.generateTaskName();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbBridgePurpose()
	{
		return (ComboBox)getChild(CHILD_CBBRIDGEPURPOSE);
	}

	/**
	 *
	 *
	 */
	static class CbBridgePurposeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbBridgePurposeOptionList()
		{

		}


		/**
		 *
		 *
		 */
    //--> Modified to fix the Open Cursor problem :
    //--> Have to close the statement and ResultSet otherwise it will cause
    //--> "ORA-01000 maximum open cursors exceeded" Exception
    //--> Modified by BILLY 06June2003
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object BPDESCRIPTION = results.getObject("BPDESCRIPTION");
					Object BRIDGEPURPOSEID = results.getObject("BRIDGEPURPOSEID");
					Object BRIDGEPURPOSE_BPDESCRIPTION = results.getObject("BPDESCRIPTION");

					String label = (BRIDGEPURPOSE_BPDESCRIPTION == null?"":BRIDGEPURPOSE_BPDESCRIPTION.toString());

					String value = (BPDESCRIPTION == null?"":BPDESCRIPTION.toString()) + "," +
					(BRIDGEPURPOSEID == null?"":BRIDGEPURPOSEID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT BRIDGEPURPOSE.BPDESCRIPTION, BRIDGEPURPOSE.BPDESCRIPTION, BRIDGEPURPOSE.BRIDGEPURPOSEID FROM BRIDGEPURPOSE";

	}



	/**
	 *
	 *
	 */
	public ComboBox getCbLender()
	{
		return (ComboBox)getChild(CHILD_CBLENDER);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbProduct()
	{
		return (ComboBox)getChild(CHILD_CBPRODUCT);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPostedInterestRate()
	{
		return (ComboBox)getChild(CHILD_CBPOSTEDINTERESTRATE);
	}


	/**
	 *
	 *
	 */
	public TextField getTbDiscount()
	{
		return (TextField)getChild(CHILD_TBDISCOUNT);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPremium()
	{
		return (TextField)getChild(CHILD_TBPREMIUM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNetRate()
	{
		return (StaticTextField)getChild(CHILD_STNETRATE);
	}


	/**
	 *
	 *
	 */
	public boolean beginStNetRateDisplay(ChildDisplayEvent event)
	{
		Object value = getStNetRate().getValue();
		return true;

		// The following code block was migrated from the stNetRate_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.handleStaticFieldSetup(this, event, "stNetRate", "doBridgeReviewNew", "dfNetRate");

		handler.pageSaveState();


		//		handler.staticPostHandlerProtocol();
		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public TextField getTbBridgeLoanAmount()
	{
		return (TextField)getChild(CHILD_TBBRIDGELOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbMaturityDateMonth()
	{
		return (ComboBox)getChild(CHILD_CBMATURITYDATEMONTH);
	}


	/**
	 *
	 *
	 */
	public TextField getTbMaturityDateDay()
	{
		return (TextField)getChild(CHILD_TBMATURITYDATEDAY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbMaturityDateYear()
	{
		return (TextField)getChild(CHILD_TBMATURITYDATEYEAR);
	}


	/**
	 *
	 *
	 */
	public void handleBtCalculationRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCalculation_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.updateData(true);

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtCalculation()
	{
		return (Button)getChild(CHILD_BTCALCULATION);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btDelete_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDelete(false);

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtDelete()
	{
		return (Button)getChild(CHILD_BTDELETE);
	}


	/**
	 *
	 *
	 */
	public pgBridgeReviewrpSaleAddressTiledView getRpSaleAddress()
	{
		return (pgBridgeReviewrpSaleAddressTiledView)getChild(CHILD_RPSALEADDRESS);
	}


	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCancel_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleCancel();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
	public String endBtCancelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btCancel_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.displayCancelButton();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btSubmit_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleBridgeReviewSubmit();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}


	/**
	 *
	 *
	 */
	public String endBtSubmitDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btSubmit_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.displaySubmitButton();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeDSSstart()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDEDSSSTART);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeDSSend()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDEDSSEND);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getRowsDisplayed()
	{
		return (StaticTextField)getChild(CHILD_ROWSDISPLAYED);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLenderProfileId()
	{
		return (StaticTextField)getChild(CHILD_STLENDERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public boolean beginStLenderProfileIdDisplay(ChildDisplayEvent event)
	{
		Object value = getStLenderProfileId().getValue();
		return true;

		// The following code block was migrated from the stLenderProfileId_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.handleStaticPassToHtml(this, event, "stLenderProfileId", "doFetchLenderProfileId", "dfLenderProfileId");

		handler.pageSaveState();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public TextField getTbRateCode()
	{
		return (TextField)getChild(CHILD_TBRATECODE);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPaymentTermDescription()
	{
		return (TextField)getChild(CHILD_TBPAYMENTTERMDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStRateTimeStamp()
	{
		return (StaticTextField)getChild(CHILD_STRATETIMESTAMP);
	}


	/**
	 *
	 *
	 */
	public boolean beginStRateTimeStampDisplay(ChildDisplayEvent event)
	{
		Object value = getStRateTimeStamp().getValue();
		return true;

		// The following code block was migrated from the stRateTimeStamp_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.handleRateDatePassToHtml(this, event, "stRateTimeStamp", "doGetRateDate", "dfRateDate");

		handler.pageSaveState();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdLongPostedDate()
	{
		return (HiddenField)getChild(CHILD_HDLONGPOSTEDDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLenderProductId()
	{
		return (StaticTextField)getChild(CHILD_STLENDERPRODUCTID);
	}


	/**
	 *
	 *
	 */
	public boolean beginStLenderProductIdDisplay(ChildDisplayEvent event)
	{
		Object value = getStLenderProductId().getValue();
		return true;

		// The following code block was migrated from the stLenderProductId_onBeforeDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.handleProductIdPassToHtml(this, event, "stLenderProductId", "doBridgeReviewNew", "dfProductId");

		handler.pageSaveState();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public void handleBtOKRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btOK_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleCancelStandard();

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtOK()
	{
		return (Button)getChild(CHILD_BTOK);
	}


	/**
	 *
	 *
	 */
	public String endBtOKDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.displayOKButton();

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAppDateForServlet()
	{
		return (StaticTextField)getChild(CHILD_STAPPDATEFORSERVLET);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStViewOnlyTag()
	{
		return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
	}


	/**
	 *
	 *
	 */
	public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		int rc = handler.displayViewOnlyTag((DisplayField) event.getSource());

		handler.pageSaveState();

		return rc;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
	{
		if (doDealSummarySnapShot == null)
			doDealSummarySnapShot = (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
		return doDealSummarySnapShot;
	}


	/**
	 *
	 *
	 */
	public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
	{
			doDealSummarySnapShot = model;
	}


	/**
	 *
	 *
	 */
	public doBridgeReviewNewModel getdoBridgeReviewNewModel()
	{
		if (doBridgeReviewNew == null)
			doBridgeReviewNew = (doBridgeReviewNewModel) getModel(doBridgeReviewNewModel.class);
		return doBridgeReviewNew;
	}


	/**
	 *
	 *
	 */
	public void setdoBridgeReviewNewModel(doBridgeReviewNewModel model)
	{
			doBridgeReviewNew = model;
	}


	/**
	 *
	 *
	 */
	public doFetchLenderProfileIdModel getdoFetchLenderProfileIdModel()
	{
		if (doFetchLenderProfileId == null)
			doFetchLenderProfileId = (doFetchLenderProfileIdModel) getModel(doFetchLenderProfileIdModel.class);
		return doFetchLenderProfileId;
	}


	/**
	 *
	 *
	 */
	public void setdoFetchLenderProfileIdModel(doFetchLenderProfileIdModel model)
	{
			doFetchLenderProfileId = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////



	//]]SPIDER_EVENT<btNextTaskPage_onWebEvent>

	/* MigrationToDo : Migrate custom method
	public int handleActMessageOK(String[] args)
	{
		BridgeReviewHandler handler =(BridgeReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleActMessageOK(args);

		handler.postHandlerProtocol();

		return;
	}
	 */




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgBridgeReview";
	public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgBridgeReview.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";
	private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_STDEALID="stDealId";
	public static final String CHILD_STDEALID_RESET_VALUE="";
	public static final String CHILD_STBORRFIRSTNAME="stBorrFirstName";
	public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUS="stDealStatus";
	public static final String CHILD_STDEALSTATUS_RESET_VALUE="";
	public static final String CHILD_STDEALSTATUSDATE="stDealStatusDate";
	public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STSOURCEFIRM="stSourceFirm";
	public static final String CHILD_STSOURCEFIRM_RESET_VALUE="";
	public static final String CHILD_STSOURCE="stSource";
	public static final String CHILD_STSOURCE_RESET_VALUE="";
	public static final String CHILD_STLOB="stLOB";
	public static final String CHILD_STLOB_RESET_VALUE="";
	public static final String CHILD_STDEALTYPE="stDealType";
	public static final String CHILD_STDEALTYPE_RESET_VALUE="";
	public static final String CHILD_STDEALPURPOSE="stDealPurpose";
	public static final String CHILD_STDEALPURPOSE_RESET_VALUE="";
	public static final String CHILD_STTOTALLOANAMOUNT="stTotalLoanAmount";
	public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE="";
	public static final String CHILD_STPURCHASEPRICE="stPurchasePrice";
	public static final String CHILD_STPURCHASEPRICE_RESET_VALUE="";
	public static final String CHILD_STPMTTERM="stPmtTerm";
	public static final String CHILD_STPMTTERM_RESET_VALUE="";
	public static final String CHILD_STESTCLOSINGDATE="stEstClosingDate";
	public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE="";
	public static final String CHILD_STSPECIALFEATURE="stSpecialFeature";
	public static final String CHILD_STSPECIALFEATURE_RESET_VALUE="";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_CBBRIDGEPURPOSE="cbBridgePurpose";
	public static final String CHILD_CBBRIDGEPURPOSE_RESET_VALUE="";
	private static CbBridgePurposeOptionList cbBridgePurposeOptions=new CbBridgePurposeOptionList();
	public static final String CHILD_CBLENDER="cbLender";
	public static final String CHILD_CBLENDER_RESET_VALUE="";
	private OptionList cbLenderOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_CBPRODUCT="cbProduct";
	public static final String CHILD_CBPRODUCT_RESET_VALUE="";
	private OptionList cbProductOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_CBPOSTEDINTERESTRATE="cbPostedInterestRate";
	public static final String CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE="";
	private OptionList cbPostedInterestRateOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_TBDISCOUNT="tbDiscount";
	public static final String CHILD_TBDISCOUNT_RESET_VALUE="";
	public static final String CHILD_TBPREMIUM="tbPremium";
	public static final String CHILD_TBPREMIUM_RESET_VALUE="";
	public static final String CHILD_STNETRATE="stNetRate";
	public static final String CHILD_STNETRATE_RESET_VALUE="";
	public static final String CHILD_TBBRIDGELOANAMOUNT="tbBridgeLoanAmount";
	public static final String CHILD_TBBRIDGELOANAMOUNT_RESET_VALUE="";
	public static final String CHILD_CBMATURITYDATEMONTH="cbMaturityDateMonth";
	public static final String CHILD_CBMATURITYDATEMONTH_RESET_VALUE="";
	private static OptionList cbMaturityDateMonthOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_TBMATURITYDATEDAY="tbMaturityDateDay";
	public static final String CHILD_TBMATURITYDATEDAY_RESET_VALUE="";
	public static final String CHILD_TBMATURITYDATEYEAR="tbMaturityDateYear";
	public static final String CHILD_TBMATURITYDATEYEAR_RESET_VALUE="";
	public static final String CHILD_BTCALCULATION="btCalculation";
	public static final String CHILD_BTCALCULATION_RESET_VALUE=" ";
	public static final String CHILD_BTDELETE="btDelete";
	public static final String CHILD_BTDELETE_RESET_VALUE="Custom";
	public static final String CHILD_RPSALEADDRESS="rpSaleAddress";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_STINCLUDEDSSSTART="stIncludeDSSstart";
	public static final String CHILD_STINCLUDEDSSSTART_RESET_VALUE="";
	public static final String CHILD_STINCLUDEDSSEND="stIncludeDSSend";
	public static final String CHILD_STINCLUDEDSSEND_RESET_VALUE="";
	public static final String CHILD_ROWSDISPLAYED="rowsDisplayed";
	public static final String CHILD_ROWSDISPLAYED_RESET_VALUE="";
	public static final String CHILD_STLENDERPROFILEID="stLenderProfileId";
	public static final String CHILD_STLENDERPROFILEID_RESET_VALUE="";
	public static final String CHILD_TBRATECODE="tbRateCode";
	public static final String CHILD_TBRATECODE_RESET_VALUE="";
	public static final String CHILD_TBPAYMENTTERMDESCRIPTION="tbPaymentTermDescription";
	public static final String CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_STRATETIMESTAMP="stRateTimeStamp";
	public static final String CHILD_STRATETIMESTAMP_RESET_VALUE="";
	public static final String CHILD_HDLONGPOSTEDDATE="hdLongPostedDate";
	public static final String CHILD_HDLONGPOSTEDDATE_RESET_VALUE="";
	public static final String CHILD_STLENDERPRODUCTID="stLenderProductId";
	public static final String CHILD_STLENDERPRODUCTID_RESET_VALUE="";
	public static final String CHILD_BTOK="btOK";
	public static final String CHILD_BTOK_RESET_VALUE="OK";
	public static final String CHILD_STAPPDATEFORSERVLET="stAppDateForServlet";
	public static final String CHILD_STAPPDATEFORSERVLET_RESET_VALUE="";
	public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
	public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";

	static
	{

		/*MigrationToDo  - developers should review this code
		We are setting up these option lists for static initialization.
		If left intact, this code will cause the option lists to be
		populated once and only once - before their first use.
		Such an implementation is optimized for performance when you wish
		all users to see exactly the same list contents.

		However, if you wish to repopulate the lists on every request,
		because you wish each user to see user specific options,
		then you should change the option list member variable definitions
		from static to non-static.
		You can then call "optionListRef.populate(getRequestContext());"
		whenever you wish to repopulate the list.
		(Either way, LEAVE THE OptionList class definitions static)
		*/

			cbPageNamesOptions.populate(null);
			cbBridgePurposeOptions.populate(null);
	}


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealSummarySnapShotModel doDealSummarySnapShot=null;
	private doBridgeReviewNewModel doBridgeReviewNew=null;
	private doFetchLenderProfileIdModel doFetchLenderProfileId=null;




	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	// private BridgeReviewHandler handler=new BridgeReviewHandler();

}

