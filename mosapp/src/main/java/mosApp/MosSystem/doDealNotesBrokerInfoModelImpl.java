package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
//--DJ_CR134--19Oct2004--//
// New model introduced in order to escape overriding of its execution for different purposes.
// Also Products wants to enhance the broker information further with other notesTypes.
// This Model is bigger than necessary now, but gives a flexibility to add extra functionalities
// from Product.
public class doDealNotesBrokerInfoModelImpl extends QueryModelBase
	implements doDealNotesBrokerInfoModel
{
	/**
	 *
	 *
	 */
	public doDealNotesBrokerInfoModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

   logger = SysLog.getSysLogger("DNIM");
   logger.debug("DNIM@afterExecute::Size: " + sql);

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    logger = SysLog.getSysLogger("DNIM");
    logger.debug("DNIM@afterExecute::Size: " + this.getSize());
    logger.debug("DNIM@afterExecute::SQLTemplate: " + this.getSelectSQL());

    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    while(this.next())
    {
      //  Category
      this.setDfDealNotesCategory(BXResources.getPickListDescription(
        institutionId, "DEALNOTESCATEGORY", this.getDfDealNotesCategory(), languageId));
    }
	this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**	 *
	 *
	 */
	public java.sql.Timestamp getDfDealNotesDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFDEALNOTESDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFDEALNOTESDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDealNotesCategory()
	{
		return (String)getValue(FIELD_DFDEALNOTESCATEGORY);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesCategory(String value)
	{
		setValue(FIELD_DFDEALNOTESCATEGORY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALNOTESID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALNOTESID,value);
	}


	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfDealNotesCategoryID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALNOTESCATEGORYID);
	}

	/**
	 *
	 *
	 */
	public void setDfDealNotesCategoryID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALNOTESCATEGORYID,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesUserID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALNOTESUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesUserID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALNOTESUSERID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Clob getDfDealNotesText()
	{
		return (java.sql.Clob)getValue(FIELD_DFDEALNOTESTEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfDealNotesText(java.sql.Clob value)
	{
		setValue(FIELD_DFDEALNOTESTEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUserName()
	{
		return (String)getValue(FIELD_DFUSERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfUserName(String value)
	{
		setValue(FIELD_DFUSERNAME,value);
	}

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DEALNOTES.DEALNOTESDATE, " +
    "to_char(DEALNOTES.DEALNOTESCATEGORYID) CATEGORYID_STR, " +
    "DEALNOTES.DEALNOTESID, " +
    "DEALNOTES.DEALNOTESCATEGORYID, " +
    "DEALNOTES.DEALID, DEALNOTES.DEALNOTESUSERID, " +
    "DEALNOTES.DEALNOTESTEXT,  " +
    "contact.contactlastname || ',' || substr(contact.CONTACTFIRSTNAME,1,1) SYNTETIC_USERNAME " +
    "FROM DEALNOTES, USERPROFILE, CONTACT,  " +
    "DEALNOTESCATEGORY " +
    "__WHERE__  ORDER BY DEALNOTES.DEALNOTESDATE  DESC";

  public static final String MODIFYING_QUERY_TABLE_NAME=
    "DEALNOTES, USERPROFILE, CONTACT, " +
    "DEALNOTESCATEGORY";

  public static final String STATIC_WHERE_CRITERIA=
      " (USERPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID) AND " +
      " (USERPROFILE.CONTACTID  =  CONTACT.CONTACTID) AND " +
      " (DEALNOTES.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID) AND " +
      "(DEALNOTES.DEALNOTESUSERID = USERPROFILE.USERPROFILEID ) AND " +
      "(DEALNOTES.DEALNOTESCATEGORYID  =  DEALNOTESCATEGORY.DEALNOTESCATEGORYID) ";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALNOTESDATE="DEALNOTES.DEALNOTESDATE";
	public static final String COLUMN_DFDEALNOTESDATE="DEALNOTESDATE";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESCATEGORY="DEALNOTESCATEGORY.CATEGORYID_STR";
	public static final String COLUMN_DFDEALNOTESCATEGORY="CATEGORYID_STR";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESID="DEALNOTES.DEALNOTESID";
	public static final String COLUMN_DFDEALNOTESID="DEALNOTESID";

  public static final String QUALIFIED_COLUMN_DFDEALNOTESCATEGORYID="DEALNOTES.DEALNOTESCATEGORYID";
  public static final String COLUMN_DFDEALNOTESCATEGORYID="DEALNOTESCATEGORYID";

	public static final String QUALIFIED_COLUMN_DFDEALID="DEALNOTES.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESUSERID="DEALNOTES.DEALNOTESUSERID";
	public static final String COLUMN_DFDEALNOTESUSERID="DEALNOTESUSERID";
	public static final String QUALIFIED_COLUMN_DFDEALNOTESTEXT="DEALNOTES.DEALNOTESTEXT";
	public static final String COLUMN_DFDEALNOTESTEXT="DEALNOTESTEXT";
	public static final String QUALIFIED_COLUMN_DFUSERNAME="CONTACT.SYNTETIC_USERNAME";
	public static final String COLUMN_DFUSERNAME="SYNTETIC_USERNAME";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESDATE,
				COLUMN_DFDEALNOTESDATE,
				QUALIFIED_COLUMN_DFDEALNOTESDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESCATEGORY,
				COLUMN_DFDEALNOTESCATEGORY,
				QUALIFIED_COLUMN_DFDEALNOTESCATEGORY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESID,
				COLUMN_DFDEALNOTESID,
				QUALIFIED_COLUMN_DFDEALNOTESID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--DJ_CR134--start--27May2004--//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESCATEGORYID,
				COLUMN_DFDEALNOTESCATEGORYID,
				QUALIFIED_COLUMN_DFDEALNOTESCATEGORYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--DJ_CR134--end--//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESUSERID,
				COLUMN_DFDEALNOTESUSERID,
				QUALIFIED_COLUMN_DFDEALNOTESUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALNOTESTEXT,
				COLUMN_DFDEALNOTESTEXT,
				QUALIFIED_COLUMN_DFDEALNOTESTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERNAME,
				COLUMN_DFUSERNAME,
				QUALIFIED_COLUMN_DFUSERNAME,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"contact.contactlastname || ',' || substr(contact.CONTACTFIRSTNAME,1,1)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"contact.contactlastname || ',' || substr(contact.CONTACTFIRSTNAME,1,1)"));

	}

}

