package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewBorrowerAddressesModelImpl extends QueryModelBase
	implements doDetailViewBorrowerAddressesModel
{
	/**
	 *
	 *
	 */
	public doDetailViewBorrowerAddressesModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
		//--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 05Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert Province Name
      this.setDfProvinceName(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROVINCE", this.getDfProvinceName(), languageId));
      //Convert Address type
      this.setDfAddressStatus(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "BORROWERADDRESSTYPE", this.getDfAddressStatus(), languageId));
      //Convert Residential status desc.
      this.setDfResidentialStatus(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "RESIDENTIALSTATUS", this.getDfResidentialStatus(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine1()
	{
		return (String)getValue(FIELD_DFADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine1(String value)
	{
		setValue(FIELD_DFADDRESSLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine2()
	{
		return (String)getValue(FIELD_DFADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine2(String value)
	{
		setValue(FIELD_DFADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProvinceName()
	{
		return (String)getValue(FIELD_DFPROVINCENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfProvinceName(String value)
	{
		setValue(FIELD_DFPROVINCENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalFSA()
	{
		return (String)getValue(FIELD_DFPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalFSA(String value)
	{
		setValue(FIELD_DFPOSTALFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalLDU()
	{
		return (String)getValue(FIELD_DFPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalLDU(String value)
	{
		setValue(FIELD_DFPOSTALLDU,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMonthsAtAddress()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMONTHSATADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfMonthsAtAddress(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMONTHSATADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressStatus()
	{
		return (String)getValue(FIELD_DFADDRESSSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressStatus(String value)
	{
		setValue(FIELD_DFADDRESSSTATUS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfResidentialStatus()
	{
		return (String)getValue(FIELD_DFRESIDENTIALSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfResidentialStatus(String value)
	{
		setValue(FIELD_DFRESIDENTIALSTATUS,value);
	}


    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 15Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BORROWER.BORROWERID, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, ADDR.CITY, PROVINCE.PROVINCENAME, ADDR.POSTALFSA, ADDR.POSTALLDU, BORROWERADDRESS.MONTHSATADDRESS, BORROWERADDRESSTYPE.BATDESCRIPTION, BORROWERADDRESS.COPYID, RESIDENTIALSTATUS.RESIDENTIALSTATUSNAME FROM BORROWERADDRESS, PROVINCE, ADDR, BORROWER, BORROWERADDRESSTYPE, RESIDENTIALSTATUS  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT BORROWER.BORROWERID, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, "+
    "ADDR.CITY, to_char(ADDR.PROVINCEID) PROVINCEID_STR, ADDR.POSTALFSA, ADDR.POSTALLDU, "+
    "BORROWERADDRESS.MONTHSATADDRESS, to_char(BORROWERADDRESS.BORROWERADDRESSTYPEID) BORROWERADDRESSTYPEID_STR, "+
    "BORROWERADDRESS.COPYID, to_char(BORROWERADDRESS.RESIDENTIALSTATUSID) RESIDENTIALSTATUSID_STR, "+
    "BORROWER.INSTITUTIONPROFILEID "+
    "FROM BORROWERADDRESS, ADDR, BORROWER  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="BORROWERADDRESS, PROVINCE, ADDR, BORROWER, BORROWERADDRESSTYPE, RESIDENTIALSTATUS";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "BORROWERADDRESS, ADDR, BORROWER ";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (BORROWERADDRESS.ADDRID  =  ADDR.ADDRID) AND (ADDR.PROVINCEID  =  PROVINCE.PROVINCEID) AND (BORROWER.BORROWERID  =  BORROWERADDRESS.BORROWERID) AND (BORROWERADDRESS.BORROWERADDRESSTYPEID  =  BORROWERADDRESSTYPE.BORROWERADDRESSTYPEID) AND (ADDR.COPYID  =  BORROWERADDRESS.COPYID) AND (BORROWER.COPYID  =  BORROWERADDRESS.COPYID) AND (BORROWERADDRESS.RESIDENTIALSTATUSID  =  RESIDENTIALSTATUS.RESIDENTIALSTATUSID)";
	public static final String STATIC_WHERE_CRITERIA=
    " (BORROWERADDRESS.ADDRID  =  ADDR.ADDRID) AND "+
    "(BORROWER.BORROWERID  =  BORROWERADDRESS.BORROWERID) AND "+
    "(ADDR.COPYID  =  BORROWERADDRESS.COPYID) AND "+
    "(BORROWER.COPYID  =  BORROWERADDRESS.COPYID) AND "+
    " (BORROWERADDRESS.INSTITUTIONPROFILEID  =  ADDR.INSTITUTIONPROFILEID) AND "+
    "(BORROWER.INSTITUTIONPROFILEID  =  BORROWERADDRESS.INSTITUTIONPROFILEID)";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFADDRESSLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFADDRESSLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFCITY="ADDR.CITY";
	public static final String COLUMN_DFCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFPOSTALFSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFPOSTALFSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFPOSTALLDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFPOSTALLDU="POSTALLDU";
	public static final String QUALIFIED_COLUMN_DFMONTHSATADDRESS="BORROWERADDRESS.MONTHSATADDRESS";
	public static final String COLUMN_DFMONTHSATADDRESS="MONTHSATADDRESS";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWERADDRESS.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFPROVINCENAME="PROVINCE.PROVINCENAME";
	//public static final String COLUMN_DFPROVINCENAME="PROVINCENAME";
  public static final String QUALIFIED_COLUMN_DFPROVINCENAME="ADDR.PROVINCEID_STR";
	public static final String COLUMN_DFPROVINCENAME="PROVINCEID_STR";
  //public static final String QUALIFIED_COLUMN_DFADDRESSSTATUS="BORROWERADDRESSTYPE.BATDESCRIPTION";
	//public static final String COLUMN_DFADDRESSSTATUS="BATDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFADDRESSSTATUS="BORROWERADDRESS.BORROWERADDRESSTYPEID_STR";
	public static final String COLUMN_DFADDRESSSTATUS="BORROWERADDRESSTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFRESIDENTIALSTATUS="RESIDENTIALSTATUS.RESIDENTIALSTATUSNAME";
	//public static final String COLUMN_DFRESIDENTIALSTATUS="RESIDENTIALSTATUSNAME";
  public static final String QUALIFIED_COLUMN_DFRESIDENTIALSTATUS="BORROWERADDRESS.RESIDENTIALSTATUSID_STR";
	public static final String COLUMN_DFRESIDENTIALSTATUS="RESIDENTIALSTATUSID_STR";

    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "BORROWER.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	//[[SPIDER_EVENTS BEGIN

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE1,
				COLUMN_DFADDRESSLINE1,
				QUALIFIED_COLUMN_DFADDRESSLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE2,
				COLUMN_DFADDRESSLINE2,
				QUALIFIED_COLUMN_DFADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCENAME,
				COLUMN_DFPROVINCENAME,
				QUALIFIED_COLUMN_DFPROVINCENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALFSA,
				COLUMN_DFPOSTALFSA,
				QUALIFIED_COLUMN_DFPOSTALFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALLDU,
				COLUMN_DFPOSTALLDU,
				QUALIFIED_COLUMN_DFPOSTALLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMONTHSATADDRESS,
				COLUMN_DFMONTHSATADDRESS,
				QUALIFIED_COLUMN_DFMONTHSATADDRESS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSSTATUS,
				COLUMN_DFADDRESSSTATUS,
				QUALIFIED_COLUMN_DFADDRESSSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRESIDENTIALSTATUS,
				COLUMN_DFRESIDENTIALSTATUS,
				QUALIFIED_COLUMN_DFRESIDENTIALSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	}

}

