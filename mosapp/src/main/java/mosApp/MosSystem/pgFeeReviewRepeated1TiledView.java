package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class pgFeeReviewRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgFeeReviewRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(50);
		setPrimaryModelClass( doDealFeeModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getCbFeeDescription().setValue(CHILD_CBFEEDESCRIPTION_RESET_VALUE);
		getTbFeeAmount().setValue(CHILD_TBFEEAMOUNT_RESET_VALUE);
		getCbMonth().setValue(CHILD_CBMONTH_RESET_VALUE);
		getTbDay().setValue(CHILD_TBDAY_RESET_VALUE);
		getTbYear().setValue(CHILD_TBYEAR_RESET_VALUE);
		getCbPaymentMethod().setValue(CHILD_CBPAYMENTMETHOD_RESET_VALUE);
		getCbFeeStatus().setValue(CHILD_CBFEESTATUS_RESET_VALUE);
		getHdDealFeeId().setValue(CHILD_HDDEALFEEID_RESET_VALUE);
		getBtDelete().setValue(CHILD_BTDELETE_RESET_VALUE);
		getStYearLabel().setValue(CHILD_STYEARLABEL_RESET_VALUE);
		getStDayLabel().setValue(CHILD_STDAYLABEL_RESET_VALUE);
		getStMonthLabel().setValue(CHILD_STMONTHLABEL_RESET_VALUE);
		getStDollarSign().setValue(CHILD_STDOLLARSIGN_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_CBFEEDESCRIPTION,ComboBox.class);
		registerChild(CHILD_TBFEEAMOUNT,TextField.class);
		registerChild(CHILD_CBMONTH,ComboBox.class);
		registerChild(CHILD_TBDAY,TextField.class);
		registerChild(CHILD_TBYEAR,TextField.class);
		registerChild(CHILD_CBPAYMENTMETHOD,ComboBox.class);
		registerChild(CHILD_CBFEESTATUS,ComboBox.class);
		registerChild(CHILD_HDDEALFEEID,HiddenField.class);
		registerChild(CHILD_BTDELETE,Button.class);
		registerChild(CHILD_STYEARLABEL,StaticTextField.class);
		registerChild(CHILD_STDAYLABEL,StaticTextField.class);
		registerChild(CHILD_STMONTHLABEL,StaticTextField.class);
		registerChild(CHILD_STDOLLARSIGN,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealFeeModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 27Nov2002
    cbMonthOptions.populate(getRequestContext());
    cbPaymentMethodOptions.populate(getRequestContext());
    cbFeeStatusOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			FeeReviewHandler handler =(FeeReviewHandler) this.handler.cloneSS();
		  handler.pageGetState(this.getParentViewBean());
  		handler.displayFeeDate(this.getTileIndex());
    	handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
    //--> This is not necessary to save the Model. As we can get the model from Model manager.
    //--> Commented out by Billy 12Sept2002
    /*
		// The following code block was migrated from the Repeated1_onAfterDataObjectExecuteEvent method
		FeeReviewHandler handler =(FeeReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.saveDataObject(event.getDataObject());
		handler.pageSaveState();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_CBFEEDESCRIPTION))
		{
			ComboBox child = new ComboBox( this,
				getdoDealFeeModel(),
				CHILD_CBFEEDESCRIPTION,
				doDealFeeModel.FIELD_DFFEETYPEID,
				CHILD_CBFEEDESCRIPTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbFeeDescriptionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBFEEAMOUNT))
		{
			TextField child = new TextField(this,
				getdoDealFeeModel(),
				CHILD_TBFEEAMOUNT,
				doDealFeeModel.FIELD_DFFEEAMOUNT,
				CHILD_TBFEEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBMONTH))
		{
			ComboBox child = new ComboBox( this,
				//getDefaultModel(),
        getdoDealFeeModel(),
				CHILD_CBMONTH,
				CHILD_CBMONTH,
				CHILD_CBMONTH_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMonthOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBDAY))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoDealFeeModel(),
				CHILD_TBDAY,
				CHILD_TBDAY,
				CHILD_TBDAY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBYEAR))
		{
			TextField child = new TextField(this,
				//getDefaultModel(),
        getdoDealFeeModel(),
				CHILD_TBYEAR,
				CHILD_TBYEAR,
				CHILD_TBYEAR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAYMENTMETHOD))
		{
			ComboBox child = new ComboBox( this,
				getdoDealFeeModel(),
				CHILD_CBPAYMENTMETHOD,
				doDealFeeModel.FIELD_DFFEEPAYMENTMETHODID,
				CHILD_CBPAYMENTMETHOD_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPaymentMethodOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBFEESTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoDealFeeModel(),
				CHILD_CBFEESTATUS,
				doDealFeeModel.FIELD_DFFEESTATUSID,
				CHILD_CBFEESTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbFeeStatusOptions);
			return child;
		}
		else
		if (name.equals(CHILD_HDDEALFEEID))
		{
			HiddenField child = new HiddenField(this,
				getdoDealFeeModel(),
				CHILD_HDDEALFEEID,
				doDealFeeModel.FIELD_DFDEALFEEID,
				CHILD_HDDEALFEEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETE,
				CHILD_BTDELETE,
				CHILD_BTDELETE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STYEARLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STYEARLABEL,
				CHILD_STYEARLABEL,
				CHILD_STYEARLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDAYLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDAYLABEL,
				CHILD_STDAYLABEL,
				CHILD_STDAYLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STMONTHLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STMONTHLABEL,
				CHILD_STMONTHLABEL,
				CHILD_STMONTHLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDOLLARSIGN))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDOLLARSIGN,
				CHILD_STDOLLARSIGN,
				CHILD_STDOLLARSIGN_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbFeeDescription()
	{
		return (ComboBox)getChild(CHILD_CBFEEDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public TextField getTbFeeAmount()
	{
		return (TextField)getChild(CHILD_TBFEEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbMonth()
	{
		return (ComboBox)getChild(CHILD_CBMONTH);
	}

	/**
	 *
	 *
	 */
	static class CbMonthOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbMonthOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 27Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"MONTHS", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public TextField getTbDay()
	{
		return (TextField)getChild(CHILD_TBDAY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbYear()
	{
		return (TextField)getChild(CHILD_TBYEAR);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPaymentMethod()
	{
		return (ComboBox)getChild(CHILD_CBPAYMENTMETHOD);
	}

	/**
	 *
	 *
	 */
	static class CbPaymentMethodOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbPaymentMethodOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 27Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"FEEPAYMENTMETHOD", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbFeeStatus()
	{
		return (ComboBox)getChild(CHILD_CBFEESTATUS);
	}

	/**
	 *
	 *
	 */
	static class CbFeeStatusOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbFeeStatusOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 27Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"FEESTATUS", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public HiddenField getHdDealFeeId()
	{
		return (HiddenField)getChild(CHILD_HDDEALFEEID);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    // The following code block was migrated from the btDelete_onWebEvent method
		FeeReviewHandler handler =(FeeReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.deleteDealFee(event);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDelete()
	{
		return (Button)getChild(CHILD_BTDELETE);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btDelete_onBeforeHtmlOutputEvent method
		FeeReviewHandler handler =(FeeReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStYearLabel()
	{
		return (StaticTextField)getChild(CHILD_STYEARLABEL);
	}


	/**
	 *
	 *
	 */
	public String endStYearLabelDisplay(ChildContentDisplayEvent event)
	{
    // The following code block was migrated from the stYearLabel_onBeforeHtmlOutputEvent method
		FeeReviewHandler handler =(FeeReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateLabels("stYearLabel");
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDayLabel()
	{
		return (StaticTextField)getChild(CHILD_STDAYLABEL);
	}


	/**
	 *
	 *
	 */
	public String endStDayLabelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stDayLabel_onBeforeHtmlOutputEvent method
		FeeReviewHandler handler =(FeeReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateLabels("stDayLabel");
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStMonthLabel()
	{
		return (StaticTextField)getChild(CHILD_STMONTHLABEL);
	}


	/**
	 *
	 *
	 */
	public String endStMonthLabelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stMonthLabel_onBeforeHtmlOutputEvent method
		FeeReviewHandler handler =(FeeReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateLabels("stMonthLabel");
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDollarSign()
	{
		return (StaticTextField)getChild(CHILD_STDOLLARSIGN);
	}


	/**
	 *
	 *
	 */
	public String endStDollarSignDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stDollarSign_onBeforeHtmlOutputEvent method
		FeeReviewHandler handler =(FeeReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateLabels("stDollarSign");
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public doDealFeeModel getdoDealFeeModel()
	{
		if (doDealFee == null)
			doDealFee = (doDealFeeModel) getModel(doDealFeeModel.class);
		return doDealFee;
	}


	/**
	 *
	 *
	 */
	public void setdoDealFeeModel(doDealFeeModel model)
	{
			doDealFee = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_CBFEEDESCRIPTION="cbFeeDescription";
	public static final String CHILD_CBFEEDESCRIPTION_RESET_VALUE="";
	private OptionList cbFeeDescriptionOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_TBFEEAMOUNT="tbFeeAmount";
	public static final String CHILD_TBFEEAMOUNT_RESET_VALUE="";
	public static final String CHILD_CBMONTH="cbMonth";
	public static final String CHILD_CBMONTH_RESET_VALUE="0";
  //--Release2.1--//
	//private static CbMonthOptionList cbMonthOptions=new CbMonthOptionList();
  private CbMonthOptionList cbMonthOptions=new CbMonthOptionList();
	public static final String CHILD_TBDAY="tbDay";
	public static final String CHILD_TBDAY_RESET_VALUE="";
	public static final String CHILD_TBYEAR="tbYear";
	public static final String CHILD_TBYEAR_RESET_VALUE="";
	public static final String CHILD_CBPAYMENTMETHOD="cbPaymentMethod";
	public static final String CHILD_CBPAYMENTMETHOD_RESET_VALUE="0";
  //--Release2.1--//
	//private static CbPaymentMethodOptionList cbPaymentMethodOptions=new CbPaymentMethodOptionList();
  private CbPaymentMethodOptionList cbPaymentMethodOptions=new CbPaymentMethodOptionList();
	public static final String CHILD_CBFEESTATUS="cbFeeStatus";
	public static final String CHILD_CBFEESTATUS_RESET_VALUE="0";
  //--Release2.1--//
	//private static CbFeeStatusOptionList cbFeeStatusOptions=new CbFeeStatusOptionList();
  private CbFeeStatusOptionList cbFeeStatusOptions=new CbFeeStatusOptionList();
	public static final String CHILD_HDDEALFEEID="hdDealFeeId";
	public static final String CHILD_HDDEALFEEID_RESET_VALUE="";
	public static final String CHILD_BTDELETE="btDelete";
	public static final String CHILD_BTDELETE_RESET_VALUE=" ";
	public static final String CHILD_STYEARLABEL="stYearLabel";
	public static final String CHILD_STYEARLABEL_RESET_VALUE="";
	public static final String CHILD_STDAYLABEL="stDayLabel";
	public static final String CHILD_STDAYLABEL_RESET_VALUE="";
	public static final String CHILD_STMONTHLABEL="stMonthLabel";
	public static final String CHILD_STMONTHLABEL_RESET_VALUE="";
	public static final String CHILD_STDOLLARSIGN="stDollarSign";
	public static final String CHILD_STDOLLARSIGN_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealFeeModel doDealFee=null;
  private FeeReviewHandler handler=new FeeReviewHandler();

}

