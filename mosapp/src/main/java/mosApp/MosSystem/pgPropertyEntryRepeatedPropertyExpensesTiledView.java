package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;
import com.basis100.resources.*;
import com.basis100.picklist.BXResources;

import MosSystem.*;

/**
 *
 *
 *
 */
public class pgPropertyEntryRepeatedPropertyExpensesTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgPropertyEntryRepeatedPropertyExpensesTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass( doPropertyExpenseSelectModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdPropertyExpensePropId().setValue(CHILD_HDPROPERTYEXPENSEPROPID_RESET_VALUE);
		getHdPropertyExpenseId().setValue(CHILD_HDPROPERTYEXPENSEID_RESET_VALUE);
		getHdPropertyExpenseCopyId().setValue(CHILD_HDPROPERTYEXPENSECOPYID_RESET_VALUE);
		getCbPropertyExpenseType().setValue(CHILD_CBPROPERTYEXPENSETYPE_RESET_VALUE);
		getTxPropertyExpenseDesc().setValue(CHILD_TXPROPERTYEXPENSEDESC_RESET_VALUE);
		getCbExpensePeriod().setValue(CHILD_CBEXPENSEPERIOD_RESET_VALUE);
		getTxExpenseAmount().setValue(CHILD_TXEXPENSEAMOUNT_RESET_VALUE);
		getCbPropertyExpenseIncludeGDS().setValue(CHILD_CBPROPERTYEXPENSEINCLUDEGDS_RESET_VALUE);
		getTxPropertyExpenseGDSPercentage().setValue(CHILD_TXPROPERTYEXPENSEGDSPERCENTAGE_RESET_VALUE);
		getHdPropertyExpenseGDSPercentage().setValue(CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE_RESET_VALUE);
		getCbPropertyExpenseIncludeTDS().setValue(CHILD_CBPROPERTYEXPENSEINCLUDETDS_RESET_VALUE);
		getTxPropertyExpenseTDSPercentage().setValue(CHILD_TXPROPERTYEXPENSETDSPERCENTAGE_RESET_VALUE);
		getHdPropertyExpenseTDSPercentage().setValue(CHILD_HDPROPERTYEXPENSETDSPERCENTAGE_RESET_VALUE);
		getBtDeleteExpense().setValue(CHILD_BTDELETEEXPENSE_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDPROPERTYEXPENSEPROPID,HiddenField.class);
		registerChild(CHILD_HDPROPERTYEXPENSEID,HiddenField.class);
		registerChild(CHILD_HDPROPERTYEXPENSECOPYID,HiddenField.class);
		registerChild(CHILD_CBPROPERTYEXPENSETYPE,ComboBox.class);
		registerChild(CHILD_TXPROPERTYEXPENSEDESC,TextField.class);
		registerChild(CHILD_CBEXPENSEPERIOD,ComboBox.class);
		registerChild(CHILD_TXEXPENSEAMOUNT,TextField.class);
		registerChild(CHILD_CBPROPERTYEXPENSEINCLUDEGDS,ComboBox.class);
		registerChild(CHILD_TXPROPERTYEXPENSEGDSPERCENTAGE,TextField.class);
		registerChild(CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE,HiddenField.class);
		registerChild(CHILD_CBPROPERTYEXPENSEINCLUDETDS,ComboBox.class);
		registerChild(CHILD_TXPROPERTYEXPENSETDSPERCENTAGE,TextField.class);
		registerChild(CHILD_HDPROPERTYEXPENSETDSPERCENTAGE,HiddenField.class);
		registerChild(CHILD_BTDELETEEXPENSE,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoPropertyExpenseSelectModel());
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    // 1. Setup the Yes/No Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());

    cbPropertyExpenseIncludeGDSOptions.setOptions(new String[]{noStr, yesStr},new String[]{"N", "Y"});
    cbPropertyExpenseIncludeTDSOptions.setOptions(new String[]{noStr, yesStr},new String[]{"N", "Y"});
    //--Release2.1--end//

    //// 2-... Repopulate all Comboboxes on the TiledView part of the page.
    cbPropertyExpenseTypeOptions.populate(getRequestContext());
    cbExpensePeriodOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

    //--Temp for debug by Billy 15May2003
//    doPropertyExpenseSelectModelImpl theObj =(doPropertyExpenseSelectModelImpl)this.getdoPropertyExpenseSelectModel();

		if (movedToRow)
    {
      validJSFromTiledView();
    }
		return movedToRow;
	}

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDPROPERTYEXPENSEPROPID))
		{
			HiddenField child = new HiddenField(this,
				getdoPropertyExpenseSelectModel(),
				CHILD_HDPROPERTYEXPENSEPROPID,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYID,
				CHILD_HDPROPERTYEXPENSEPROPID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYEXPENSEID))
		{
			HiddenField child = new HiddenField(this,
				getdoPropertyExpenseSelectModel(),
				CHILD_HDPROPERTYEXPENSEID,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSEID,
				CHILD_HDPROPERTYEXPENSEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYEXPENSECOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoPropertyExpenseSelectModel(),
				CHILD_HDPROPERTYEXPENSECOPYID,
				doPropertyExpenseSelectModel.FIELD_DFCOPYID,
				CHILD_HDPROPERTYEXPENSECOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROPERTYEXPENSETYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyExpenseSelectModel(),
				CHILD_CBPROPERTYEXPENSETYPE,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSETYPEID,
				CHILD_CBPROPERTYEXPENSETYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPropertyExpenseTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYEXPENSEDESC))
		{
			TextField child = new TextField(this,
				getdoPropertyExpenseSelectModel(),
				CHILD_TXPROPERTYEXPENSEDESC,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSEDESC,
				CHILD_TXPROPERTYEXPENSEDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBEXPENSEPERIOD))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyExpenseSelectModel(),
				CHILD_CBEXPENSEPERIOD,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSEPERIODID,
				CHILD_CBEXPENSEPERIOD_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbExpensePeriodOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXEXPENSEAMOUNT))
		{
			TextField child = new TextField(this,
				getdoPropertyExpenseSelectModel(),
				CHILD_TXEXPENSEAMOUNT,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSEAMOUNT,
				CHILD_TXEXPENSEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROPERTYEXPENSEINCLUDEGDS))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyExpenseSelectModel(),
				CHILD_CBPROPERTYEXPENSEINCLUDEGDS,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSEINCLUDEINGDS,
				CHILD_CBPROPERTYEXPENSEINCLUDEGDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPropertyExpenseIncludeGDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYEXPENSEGDSPERCENTAGE))
		{
			TextField child = new TextField(this,
				getdoPropertyExpenseSelectModel(),
				CHILD_TXPROPERTYEXPENSEGDSPERCENTAGE,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,
				CHILD_TXPROPERTYEXPENSEGDSPERCENTAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE,
				CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE,
				CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPROPERTYEXPENSEINCLUDETDS))
		{
			ComboBox child = new ComboBox( this,
				getdoPropertyExpenseSelectModel(),
				CHILD_CBPROPERTYEXPENSEINCLUDETDS,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSEINCLUDETDS,
				CHILD_CBPROPERTYEXPENSEINCLUDETDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbPropertyExpenseIncludeTDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TXPROPERTYEXPENSETDSPERCENTAGE))
		{
			TextField child = new TextField(this,
				getdoPropertyExpenseSelectModel(),
				CHILD_TXPROPERTYEXPENSETDSPERCENTAGE,
				doPropertyExpenseSelectModel.FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,
				CHILD_TXPROPERTYEXPENSETDSPERCENTAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPROPERTYEXPENSETDSPERCENTAGE))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDPROPERTYEXPENSETDSPERCENTAGE,
				CHILD_HDPROPERTYEXPENSETDSPERCENTAGE,
				CHILD_HDPROPERTYEXPENSETDSPERCENTAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEEXPENSE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEEXPENSE,
				CHILD_BTDELETEEXPENSE,
				CHILD_BTDELETEEXPENSE_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyExpensePropId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYEXPENSEPROPID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyExpenseId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYEXPENSEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyExpenseCopyId()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYEXPENSECOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPropertyExpenseType()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTYEXPENSETYPE);
	}

	/**
	 *
	 *
	 */
	static class CbPropertyExpenseTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbPropertyExpenseTypeOptionList()
		{

		}

		/**
		 *
		 *
		 */
		//--Release2.1--//
    ////Convert to populate the Options from ResourceBundle
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "PROPERTYEXPENSETYPE", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}

	/**
	 *
	 *
	 */
	public TextField getTxPropertyExpenseDesc()
	{
		return (TextField)getChild(CHILD_TXPROPERTYEXPENSEDESC);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbExpensePeriod()
	{
		return (ComboBox)getChild(CHILD_CBEXPENSEPERIOD);
	}

	/**
	 *
	 *
	 */
	static class CbExpensePeriodOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbExpensePeriodOptionList()
		{

		}

		//--Release2.1--//
    ////Convert to populate the Options from ResourceBundle
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c 
            = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                   "PROPERTYEXPENSEPERIOD", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}

	/**
	 *
	 *
	 */
	public TextField getTxExpenseAmount()
	{
		return (TextField)getChild(CHILD_TXEXPENSEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPropertyExpenseIncludeGDS()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTYEXPENSEINCLUDEGDS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxPropertyExpenseGDSPercentage()
	{
		return (TextField)getChild(CHILD_TXPROPERTYEXPENSEGDSPERCENTAGE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyExpenseGDSPercentage()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPropertyExpenseIncludeTDS()
	{
		return (ComboBox)getChild(CHILD_CBPROPERTYEXPENSEINCLUDETDS);
	}


	/**
	 *
	 *
	 */
	public TextField getTxPropertyExpenseTDSPercentage()
	{
		return (TextField)getChild(CHILD_TXPROPERTYEXPENSETDSPERCENTAGE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPropertyExpenseTDSPercentage()
	{
		return (HiddenField)getChild(CHILD_HDPROPERTYEXPENSETDSPERCENTAGE);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteExpenseRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    //logger = SysLog.getSysLogger("PgPERPETV");

    PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

    //logger.debug("PgPERPETV@handleBtDeleteExpenseRequest::Before PreHandler");
    //logger.debug("PgPERPETV@handleBtDeleteExpenseRequest::Parent: " + (ViewBean) this.getParent());

    handler.preHandlerProtocol((ViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("PgPERPETV@handleBtDeleteExpenseRequest::TileIndexNew: " + tileIndx);

		handler.handleDelete(false,
                        5,
                        ////handler.getRowNdxFromWebEventMethod(event),
                        tileIndx,
                        "RepeatedPropertyExpenses/hdPropertyExpenseId",
                        "propertyExpenseId",
                        "RepeatedPropertyExpenses/hdPropertyExpenseCopyId",
                        "PropertyExpense");

    handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteExpense()
	{
		return (Button)getChild(CHILD_BTDELETEEXPENSE);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteExpenseDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteExpense_onBeforeHtmlOutputEvent method
		PropertyEntryHandler handler =(PropertyEntryHandler) this.handler.cloneSS();

		handler.pageGetState(this.getParentViewBean());

    boolean rc = handler.displayEditButton();

		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public doPropertyExpenseSelectModel getdoPropertyExpenseSelectModel()
	{
		if (doPropertyExpenseSelect == null)
			doPropertyExpenseSelect = (doPropertyExpenseSelectModel) getModel(doPropertyExpenseSelectModel.class);
		return doPropertyExpenseSelect;
	}


	/**
	 *
	 *
	 */
	public void setdoPropertyExpenseSelectModel(doPropertyExpenseSelectModel model)
	{
			doPropertyExpenseSelect = model;
	}

  public void validJSFromTiledView()
  {
      logger = SysLog.getSysLogger("PGDEREDTV");

      pgPropertyEntryViewBean viewBean = (pgPropertyEntryViewBean) getParentViewBean();

      try
      {
        String expensesValidPath = Sc.DVALID_PROPERTY_EXPENSE_PROP_FILE_NAME;

        JSValidationRules expensesValidObj = new JSValidationRules(expensesValidPath, logger, viewBean);
        expensesValidObj.attachJSValidationRules();
      }
      catch( Exception e )
      {
          logger.warning("PGPERPETVTV@DataValidJSException: " + e.getMessage());
          e.printStackTrace();
      }
  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDPROPERTYEXPENSEPROPID="hdPropertyExpensePropId";
	public static final String CHILD_HDPROPERTYEXPENSEPROPID_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYEXPENSEID="hdPropertyExpenseId";
	public static final String CHILD_HDPROPERTYEXPENSEID_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYEXPENSECOPYID="hdPropertyExpenseCopyId";
	public static final String CHILD_HDPROPERTYEXPENSECOPYID_RESET_VALUE="";
	public static final String CHILD_CBPROPERTYEXPENSETYPE="cbPropertyExpenseType";
	public static final String CHILD_CBPROPERTYEXPENSETYPE_RESET_VALUE="";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static CbPropertyExpenseTypeOptionList cbPropertyExpenseTypeOptions=new CbPropertyExpenseTypeOptionList();
	private CbPropertyExpenseTypeOptionList cbPropertyExpenseTypeOptions=new CbPropertyExpenseTypeOptionList();

	public static final String CHILD_TXPROPERTYEXPENSEDESC="txPropertyExpenseDesc";
	public static final String CHILD_TXPROPERTYEXPENSEDESC_RESET_VALUE="";
	public static final String CHILD_CBEXPENSEPERIOD="cbExpensePeriod";
	public static final String CHILD_CBEXPENSEPERIOD_RESET_VALUE="";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static CbExpensePeriodOptionList cbExpensePeriodOptions=new CbExpensePeriodOptionList();
	private CbExpensePeriodOptionList cbExpensePeriodOptions=new CbExpensePeriodOptionList();

	public static final String CHILD_TXEXPENSEAMOUNT="txExpenseAmount";
	public static final String CHILD_TXEXPENSEAMOUNT_RESET_VALUE="";
	public static final String CHILD_CBPROPERTYEXPENSEINCLUDEGDS="cbPropertyExpenseIncludeGDS";
	public static final String CHILD_CBPROPERTYEXPENSEINCLUDEGDS_RESET_VALUE="Y";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList cbPropertyExpenseIncludeGDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList cbPropertyExpenseIncludeGDSOptions=new OptionList();

	public static final String CHILD_TXPROPERTYEXPENSEGDSPERCENTAGE="txPropertyExpenseGDSPercentage";
	public static final String CHILD_TXPROPERTYEXPENSEGDSPERCENTAGE_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE="hdPropertyExpenseGDSPercentage";
	public static final String CHILD_HDPROPERTYEXPENSEGDSPERCENTAGE_RESET_VALUE="";
	public static final String CHILD_CBPROPERTYEXPENSEINCLUDETDS="cbPropertyExpenseIncludeTDS";
	public static final String CHILD_CBPROPERTYEXPENSEINCLUDETDS_RESET_VALUE="Y";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList cbPropertyExpenseIncludeTDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	private OptionList cbPropertyExpenseIncludeTDSOptions=new OptionList();

	public static final String CHILD_TXPROPERTYEXPENSETDSPERCENTAGE="txPropertyExpenseTDSPercentage";
	public static final String CHILD_TXPROPERTYEXPENSETDSPERCENTAGE_RESET_VALUE="";
	public static final String CHILD_HDPROPERTYEXPENSETDSPERCENTAGE="hdPropertyExpenseTDSPercentage";
	public static final String CHILD_HDPROPERTYEXPENSETDSPERCENTAGE_RESET_VALUE="";
	public static final String CHILD_BTDELETEEXPENSE="btDeleteExpense";
	public static final String CHILD_BTDELETEEXPENSE_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

  private PropertyEntryHandler handler=new PropertyEntryHandler();
  private doPropertyExpenseSelectModel doPropertyExpenseSelect=null;
  public SysLogger logger;

}

