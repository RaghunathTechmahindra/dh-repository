package mosApp.MosSystem;

import java.text.*;
import java.util.*;
import java.sql.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.security.*;

import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;
import com.basis100.deal.docprep.Dc;

/**
 * <p>Title: AdjudicationRequestHandler.java </p>
 *
 * <p>Description: Handler logic implementation </p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: filogix</p>
 *
 * @author Vlad Ivanov
 * @version 1.0
 */
public class AdjudicationRequestHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
        private Deal deal;

        public AdjudicationRequestHandler() {
          super();
        }

        public AdjudicationRequestHandler(String name) {
            super(name);
          }

            public AdjudicationRequestHandler(PageHandlerCommon other) {
              super();
              initFromOther(other);
            }

            public AdjudicationRequestHandler cloneSS() {
              return (AdjudicationRequestHandler)super.cloneSafeShallow();
            }

            /**
             * Generic method to bind disclsoure questions to pgViewBean elements
             * When adding new disclosure types (provinces), you do not have to modify this method.  Simply extend DisclosureType
             * @param pg PageEntry
             */
            private void showPageFields(PageEntry pg) {
              // determine the document type constant from a factory method, using the page name as the parameter
              ViewBean currentViewBean = getCurrNDPage();

              logger.error("ARH@showPageFields:ViewBeanName: " + currentViewBean.getName());

              //currentViewBean.setDisplayFieldValue("hdCopyId", String.valueOf(pg.getPageDealCID()));
              logger.error("ARH@showPageFields:hdCopyId: " + String.valueOf(pg.getPageDealCID()));

              // force the PHC to display the confirmation dialog.
              pg.setModified(true);

              // use reflection to set the element on the ViewBean if necessary for customization
            }

            //This method is used to populate the fields in the header
            //with the appropriate information
            public void populatePageDisplayFields() {
              PageEntry pg = theSessionState.getCurrentPage();

              Hashtable pst = pg.getPageStateTable();

              populatePageShellDisplayFields();

              populateTaskNavigator(pg);

              populatePageDealSummarySnapShot();

              populatePreviousPagesLinks();

              /// CUSTOM POPULATION GOES HERE (e.g. set page specific display fields, etc)
              showPageFields(pg);
            }

	/**
	 *
	 *
	 */
	public void setupBeforePageGeneration()
	{
          PageEntry pg = theSessionState.getCurrentPage();

          try {
            if (pg.getSetupBeforeGenerationCalled() == true)return;
            pg.setSetupBeforeGenerationCalled(true);
            setupDealSummarySnapShotDO(pg);
            // Add any custom model binding here
            //or and any field customization logic here
            // Setup cursor parameters here, if necessary
            //........................................
          }
          catch (Exception e) {
            logger.error(
                "Exception @AdjudicationRequestHandler.setupBeforePageGeneration()");
            logger.error(e);
            setStandardFailMessage();
            return;
          }
	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
              PageEntry pg = theSessionState.getCurrentPage();

              SessionResourceKit srk = getSessionResourceKit();

              try
              {
                      // complete the task - make it go away
                      // Example to close task on 'Submit' action
                      srk.beginTransaction();
                      //AssignedTask aTask = new AssignedTask(srk);
                      //aTask.findByPrimaryKey(new AssignedTaskBeanPK(pg.getPageCriteriaId1()));
                      //aTask.setTaskStatusId(Sc.TASK_COMPLETE);
                      //aTask.ejbStore();
                      srk.commitTransaction();
                      navigateToNextPage(true);
              }
              catch(Exception e)
              {
                      logger.error("Exception @AdjudicationRequestHandler.handleSubmit()");
                      logger.error(e);
                      setStandardFailMessage();
                      return;
              }
        }
    }

