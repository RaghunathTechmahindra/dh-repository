package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doSaveNewPasswordModel extends QueryModel, UpdateQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUSERID();

	
	/**
	 * 
	 * 
	 */
	public void setDfUSERID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfLASTPASSWORDCHANGE();

	
	/**
	 * 
	 * 
	 */
	public void setDfLASTPASSWORDCHANGE(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCURRENTGRACECOUNT();

	
	/**
	 * 
	 * 
	 */
	public void setDfCURRENTGRACECOUNT(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPASSWORD();

	
	/**
	 * 
	 * 
	 */
	public void setDfPASSWORD(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFUSERID="dfUSERID";
	public static final String FIELD_DFLASTPASSWORDCHANGE="dfLASTPASSWORDCHANGE";
	public static final String FIELD_DFCURRENTGRACECOUNT="dfCURRENTGRACECOUNT";
	public static final String FIELD_DFPASSWORD="dfPASSWORD";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

