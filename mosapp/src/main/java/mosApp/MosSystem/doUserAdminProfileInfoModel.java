package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doUserAdminProfileInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUserProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUserLogin();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserLogin(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUserTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfGroupProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfGroupProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfJointApproverUserId();

	
	/**
	 * 
	 * 
	 */
	public void setDfJointApproverUserId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfContactId();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfProfileStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfProfileStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBilingual();

	
	/**
	 * 
	 * 
	 */
	public void setDfBilingual(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPartnerUserId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPartnerUserId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfStandInUserId();

	
	/**
	 * 
	 * 
	 */
	public void setDfStandInUserId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfManagerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfManagerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfHigherApproverId();

	
	/**
	 * 
	 * 
	 */
	public void setDfHigherApproverId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUPBusinessId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUPBusinessId(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfInitial();

	
	/**
	 * 
	 * 
	 */
	public void setDfInitial(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfLastName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfPhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfFaxNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfFaxNumber(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFUSERPROFILEID="dfUserProfileId";
	public static final String FIELD_DFUSERLOGIN="dfUserLogin";
	public static final String FIELD_DFUSERTYPEID="dfUserTypeId";
	public static final String FIELD_DFGROUPPROFILEID="dfGroupProfileId";
	public static final String FIELD_DFJOINTAPPROVERUSERID="dfJointApproverUserId";
	public static final String FIELD_DFCONTACTID="dfContactId";
	public static final String FIELD_DFPROFILESTATUSID="dfProfileStatusId";
	public static final String FIELD_DFBILINGUAL="dfBilingual";
	public static final String FIELD_DFPARTNERUSERID="dfPartnerUserId";
	public static final String FIELD_DFSTANDINUSERID="dfStandInUserId";
	public static final String FIELD_DFMANAGERID="dfManagerId";
	public static final String FIELD_DFHIGHERAPPROVERID="dfHigherApproverId";
	public static final String FIELD_DFUPBUSINESSID="dfUPBusinessId";
	public static final String FIELD_DFFIRSTNAME="dfFirstName";
	public static final String FIELD_DFINITIAL="dfInitial";
	public static final String FIELD_DFLASTNAME="dfLastName";
	public static final String FIELD_DFPHONENUMBER="dfPhoneNumber";
	public static final String FIELD_DFFAXNUMBER="dfFaxNumber";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

