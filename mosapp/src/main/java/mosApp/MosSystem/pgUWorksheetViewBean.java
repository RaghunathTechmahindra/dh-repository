package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mosApp.SQLConnectionManagerImpl;
import MosSystem.Mc;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import java.math.BigDecimal;

import com.filogix.express.web.jato.ExpressViewBeanBase;

/**
 * <p>
 * Title: pgUWorksheetViewBean
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author @
 * @version 1.2 <br>
 *          Date: 06/26/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: added 2 constants
 *          CHILD_TXRATEGUARANTEEPERIOD,CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE
 *          modified registerChildren(),resetChildren() ,createChild(String)
 *          
 * @version 55 <br>
 * Date: 11/20/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change: added 2 constants
 * CHILD_HDFORCEPROGRESSADVANCE  ,CHILD_HDFORCEPROGRESSADVANCE_RESET_VALUE
 * modified registerChildren(),resetChildren() ,createChild(String)
 *  
 *	@Version 1.3 <br>
 *	Date: 06/09/2008
 *	Author: MCM Impl Team <br>
 *	Change Log:  <br>
 *	XS_2.46 -- 06/06/2008 -- added 2 fields in Refinance section<br>
 *		- CHILD_TXREFIADDITIONALINFORMATION,CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE,
 * 		  CHILD_TXREFEXISTINGMTGNUMBER,CHILD_TXREFEXISTINGMTGNUMBER_RESET_VALUE
 *		- modified registerChildren(),resetChildren() ,createChild(String)	
 *
 * @version 1.4 06-JUN-2008 XS_2.13 Included pgQualifyingDetailsPageletView
 * - Added CHILD_STQUALIFYINGDETAILSPAGELET
 * - Modified registerChildren(),createChild(String)
 * 
 * @version 1.5 22-JULY-2008 XS_2.44 added handler as a pgComponentInfoPageletView constructor parameter
 * @version 1.6 Aug 08, 2008 XS 2.43 added field : stDefaultRepaymentTypeId
 * @version 1.7 Jan 23, 2009 FXP24101, deleted "static" from 2 Option lists   
 */
public class pgUWorksheetViewBean extends ExpressViewBeanBase {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(pgUWorksheetViewBean.class);

    ////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////////
    public static final String PAGE_NAME = "pgUWorksheet";

    //// It is a variable now (see explanation in the getDisplayURL() method above.
    ////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgUWorksheet.jsp";
    public static Map FIELD_DESCRIPTORS;

    public static final String CHILD_TBDEALID = "tbDealId";

    public static final String CHILD_TBDEALID_RESET_VALUE = "";

    public static final String CHILD_CBPAGENAMES = "cbPageNames";

    public static final String CHILD_CBPAGENAMES_RESET_VALUE = "";

    public static final String CHILD_BTPROCEED = "btProceed";

    public static final String CHILD_BTPROCEED_RESET_VALUE = "Proceed";

    public static final String CHILD_HREF1 = "Href1";

    public static final String CHILD_HREF1_RESET_VALUE = "";

    public static final String CHILD_HREF2 = "Href2";

    public static final String CHILD_HREF2_RESET_VALUE = "";

    public static final String CHILD_HREF3 = "Href3";

    public static final String CHILD_HREF3_RESET_VALUE = "";

    public static final String CHILD_HREF4 = "Href4";

    public static final String CHILD_HREF4_RESET_VALUE = "";

    public static final String CHILD_HREF5 = "Href5";

    public static final String CHILD_HREF5_RESET_VALUE = "";

    public static final String CHILD_HREF6 = "Href6";

    public static final String CHILD_HREF6_RESET_VALUE = "";

    public static final String CHILD_HREF7 = "Href7";

    public static final String CHILD_HREF7_RESET_VALUE = "";

    public static final String CHILD_HREF8 = "Href8";

    public static final String CHILD_HREF8_RESET_VALUE = "";

    public static final String CHILD_HREF9 = "Href9";

    public static final String CHILD_HREF9_RESET_VALUE = "";

    public static final String CHILD_HREF10 = "Href10";

    public static final String CHILD_HREF10_RESET_VALUE = "";

    public static final String CHILD_STPAGELABEL = "stPageLabel";

    public static final String CHILD_STPAGELABEL_RESET_VALUE = "";

    public static final String CHILD_STCOMPANYNAME = "stCompanyName";

    public static final String CHILD_STCOMPANYNAME_RESET_VALUE = "";

    public static final String CHILD_STTODAYDATE = "stTodayDate";

    public static final String CHILD_STTODAYDATE_RESET_VALUE = "";

    public static final String CHILD_STUSERNAMETITLE = "stUserNameTitle";

    public static final String CHILD_STUSERNAMETITLE_RESET_VALUE = "";

    public static final String CHILD_BTWORKQUEUELINK = "btWorkQueueLink";

    public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE = " ";

    public static final String CHILD_CHANGEPASSWORDHREF = "changePasswordHref";

    public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE = "";

    public static final String CHILD_BTTOOLHISTORY = "btToolHistory";

    public static final String CHILD_BTTOOLHISTORY_RESET_VALUE = " ";

    public static final String CHILD_BTTOONOTES = "btTooNotes";

    public static final String CHILD_BTTOONOTES_RESET_VALUE = " ";

    public static final String CHILD_BTTOOLSEARCH = "btToolSearch";

    public static final String CHILD_BTTOOLSEARCH_RESET_VALUE = " ";

    public static final String CHILD_BTTOOLLOG = "btToolLog";

    public static final String CHILD_BTTOOLLOG_RESET_VALUE = " ";

    public static final String CHILD_STERRORFLAG = "stErrorFlag";

    public static final String CHILD_STERRORFLAG_RESET_VALUE = "N";

    public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";

    public static final String CHILD_DETECTALERTTASKS_RESET_VALUE = "";

    public static final String CHILD_HDDEALID = "hdDealId";

    public static final String CHILD_HDDEALID_RESET_VALUE = "";

    public static final String CHILD_HDDEALCOPYID = "hdDealCopyId";

    public static final String CHILD_HDDEALCOPYID_RESET_VALUE = "";

    public static final String CHILD_HDDEALINSTITUTIONID = "hdDealInstitutionId";

    public static final String CHILD_HDDEALINSTITUTIONID_RESET_VALUE = "";

    public static final String CHILD_STDEALID = "stDealId";

    public static final String CHILD_STDEALID_RESET_VALUE = "";

    public static final String CHILD_STBORRFIRSTNAME = "stBorrFirstName";

    public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE = "";

    public static final String CHILD_STDEALSTATUS = "stDealStatus";

    public static final String CHILD_STDEALSTATUS_RESET_VALUE = "";

    public static final String CHILD_STDEALSTATUSDATE = "stDealStatusDate";

    public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE = "";

    public static final String CHILD_STSOURCEFIRM = "stSourceFirm";

    public static final String CHILD_STSOURCEFIRM_RESET_VALUE = "";

    public static final String CHILD_STSOURCE = "stSource";

    public static final String CHILD_STSOURCE_RESET_VALUE = "";

    public static final String CHILD_STDEALTYPE = "stDealType";

    public static final String CHILD_STDEALTYPE_RESET_VALUE = "";

    public static final String CHILD_STDEALPURPOSE = "stDealPurpose";

    public static final String CHILD_STDEALPURPOSE_RESET_VALUE = "";

    public static final String CHILD_STPMTTERM = "stPmtTerm";

    public static final String CHILD_STPMTTERM_RESET_VALUE = "";

    public static final String CHILD_STESTCLOSINGDATE = "stEstClosingDate";

    public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE = "";

    public static final String CHILD_STSPECIALFEATURE = "stSpecialFeature";

    public static final String CHILD_STSPECIALFEATURE_RESET_VALUE = "";

    public static final String CHILD_STSCENARIONO = "stScenarioNo";

    public static final String CHILD_STSCENARIONO_RESET_VALUE = "";

    public static final String CHILD_TXUWSCENERIODESC = "txUWScenerioDesc";

    public static final String CHILD_TXUWSCENERIODESC_RESET_VALUE = "";

    public static final String CHILD_STRECOEMENDSCENARIO = "stRecoemendScenario";

    public static final String CHILD_STRECOEMENDSCENARIO_RESET_VALUE = "";

    public static final String CHILD_STLOCKED = "stLocked";

    public static final String CHILD_STLOCKED_RESET_VALUE = "";

    public static final String CHILD_CBSCENARIOS = "cbScenarios";

    public static final String CHILD_CBSCENARIOS_RESET_VALUE = "";

    private OptionList cbScenariosOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_CBSCENARIOPICKLISTTOP = "cbScenarioPickListTop";

    public static final String CHILD_CBSCENARIOPICKLISTTOP_RESET_VALUE = "";
    
    //FXP24101, deleted "static", Jan 23, 2009
    private OptionList cbScenarioPickListTopOptions = new OptionList(
            new String[] {}, new String[] {});
    
    public static final String CHILD_BTPROCEEDTOCREATSCENARIOTOP = "btProceedToCreatScenarioTop";

    public static final String CHILD_BTPROCEEDTOCREATSCENARIOTOP_RESET_VALUE = " ";

    public static final String CHILD_CBSCENARIOPICKLISTBOTTOM = "cbScenarioPickListBottom";

    public static final String CHILD_CBSCENARIOPICKLISTBOTTOM_RESET_VALUE = "";

    //FXP24101, deleted "static", Jan 23, 2009
    private OptionList cbScenarioPickListBottomOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM = "btProceedToCreateScenarioBottom";

    public static final String CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM_RESET_VALUE = " ";

    public static final String CHILD_STCOMBINEDGDS = "stCombinedGDS";

    public static final String CHILD_STCOMBINEDGDS_RESET_VALUE = "";

    public static final String CHILD_STCOMBINED3YRGDS = "stCombined3YrGDS";

    public static final String CHILD_STCOMBINED3YRGDS_RESET_VALUE = "";

    public static final String CHILD_STCOMBINEDBORROWERGDS = "stCombinedBorrowerGDS";

    public static final String CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE = "";

    public static final String CHILD_STCOMBINEDTDS = "stCombinedTDS";

    public static final String CHILD_STCOMBINEDTDS_RESET_VALUE = "";

    public static final String CHILD_STCOMBINED3YRTDS = "stCombined3YrTDS";

    public static final String CHILD_STCOMBINED3YRTDS_RESET_VALUE = "";

    public static final String CHILD_STCOMBINEDBORROWERTDS = "stCombinedBorrowerTDS";

    public static final String CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE = "";

    public static final String CHILD_REPEATGDSTDSDETAILS = "RepeatGDSTDSDetails";

    public static final String CHILD_STUWPURCHASEPRICE = "stUWPurchasePrice";

    public static final String CHILD_STUWPURCHASEPRICE_RESET_VALUE = "";

    public static final String CHILD_HDUWPURCHASEPRICE = "hdUWPurchasePrice";

    public static final String CHILD_HDUWPURCHASEPRICE_RESET_VALUE = "";

    //CR03

    public static final String CHILD_HDCOMMITMENTEXPIRYDATE = "hdCommitmentExpiryDate";

    public static final String CHILD_HDCOMMITMENTEXPIRYDATE_RESET_VALUE = "";

    public static final String CHILD_HDMOSPROPERTY = "hdMosProperty";

    public static final String CHILD_HDMOSPROPERTY_RESET_VALUE = "";
    //CR03

    public static final String CHILD_CBUWCHARGE = "cbUWCharge";

    public static final String CHILD_CBUWCHARGE_RESET_VALUE = "";

    public static final String CHILD_STUWDOWNPAYMENT = "stUWDownPayment";

    public static final String CHILD_STUWDOWNPAYMENT_RESET_VALUE = "";

    public static final String CHILD_CBUWLOB = "cbUWLOB";

    public static final String CHILD_CBUWLOB_RESET_VALUE = "";

    public static final String CHILD_CBUWLENDER = "cbUWLender";

    public static final String CHILD_CBUWLENDER_RESET_VALUE = "";

    private OptionList cbUWLenderOptions = new OptionList(
            new String[] { " " }, new String[] { " " });

    public static final String CHILD_STUWBRIDGELOAN = "stUWBridgeLoan";

    public static final String CHILD_STUWBRIDGELOAN_RESET_VALUE = "";

    public static final String CHILD_CBUWPRODUCT = "cbUWProduct";

    public static final String CHILD_CBUWPRODUCT_RESET_VALUE = "";

    private OptionList cbUWProductOptions = new OptionList(
            new String[] { " " }, new String[] { " " });

    public static final String CHILD_TXUWLOANAMOUNT = "txUWLoanAmount";

    public static final String CHILD_TXUWLOANAMOUNT_RESET_VALUE = "";

    public static final String CHILD_TXUWLTV = "txUWLTV";

    public static final String CHILD_TXUWLTV_RESET_VALUE = "";

    public static final String CHILD_HDUWORIGLTV = "hdUWOrigLTV";

    public static final String CHILD_HDUWORIGLTV_RESET_VALUE = "";

    public static final String CHILD_BTRECALCULATELTV = "btRecalculateLTV";

    public static final String CHILD_BTRECALCULATELTV_RESET_VALUE = " ";

    public static final String CHILD_CBUWPOSTEDINTERESTRATE = "cbUWPostedInterestRate";

    public static final String CHILD_CBUWPOSTEDINTERESTRATE_RESET_VALUE = "";

    private OptionList cbUWPostedInterestRateOptions = new OptionList(
            new String[] { " " }, new String[] { " " });

    public static final String CHILD_STMIPREMIUM = "stMIPremium";

    public static final String CHILD_STMIPREMIUM_RESET_VALUE = "";

    public static final String CHILD_TXUWDISCOUNT = "txUWDiscount";

    public static final String CHILD_TXUWDISCOUNT_RESET_VALUE = "";

    public static final String CHILD_STUWTOTALLOAN = "stUWTotalLoan";

    public static final String CHILD_STUWTOTALLOAN_RESET_VALUE = "";

    public static final String CHILD_TXUWPREMIUM = "txUWPremium";

    public static final String CHILD_TXUWPREMIUM_RESET_VALUE = "";

    public static final String CHILD_TXUWAMORTIZATIOPERIODYEARS = "txUWAmortizatioPeriodYears";

    public static final String CHILD_TXUWAMORTIZATIOPERIODYEARS_RESET_VALUE = "";

    public static final String CHILD_TXUWAMORTIZATIOPERIODMONTHS = "txUWAmortizatioPeriodMonths";

    public static final String CHILD_TXUWAMORTIZATIOPERIODMONTHS_RESET_VALUE = "";

    public static final String CHILD_TXUWBUYDOWN = "txUWBuyDown";

    public static final String CHILD_TXUWBUYDOWN_RESET_VALUE = "";

    public static final String CHILD_STUWNETRATE = "stUWNetRate";

    public static final String CHILD_STUWNETRATE_RESET_VALUE = "";
    
//    public static final String CHILD_STASTERIK = "stAsterik";
//    public static final String CHILD_STASTERIK_RESET_VALUE = "";

    public static final String CHILD_STUWPIPAYMENT = "stUWPIPayment";

    public static final String CHILD_STUWPIPAYMENT_RESET_VALUE = "";

    public static final String CHILD_TXUWACTUALPAYYEARS = "txUWActualPayYears";

    public static final String CHILD_TXUWACTUALPAYYEARS_RESET_VALUE = "";

    public static final String CHILD_TXUWACTUALPAYMONTHS = "txUWActualPayMonths";

    public static final String CHILD_TXUWACTUALPAYMONTHS_RESET_VALUE = "";

    public static final String CHILD_TXUWADDITIONALPIPAY = "txUWAdditionalPIPay";

    public static final String CHILD_TXUWADDITIONALPIPAY_RESET_VALUE = "";

    public static final String CHILD_CBUWPAYMENTFREQUENCY = "cbUWPaymentFrequency";

    public static final String CHILD_CBUWPAYMENTFREQUENCY_RESET_VALUE = "";

    public static final String CHILD_STUWTOTALESCROW = "stUWTotalEscrow";

    public static final String CHILD_STUWTOTALESCROW_RESET_VALUE = "";

    public static final String CHILD_CBUWPREPAYMENTPENALTY = "cbUWPrePaymentPenalty";

    public static final String CHILD_CBUWPREPAYMENTPENALTY_RESET_VALUE = "";

    public static final String CHILD_STUWTOTALPAYMENT = "stUWTotalPayment";

    public static final String CHILD_STUWTOTALPAYMENT_RESET_VALUE = "";

    public static final String CHILD_CBUWPRIVILEGEPAYMENTOPTION = "cbUWPrivilegePaymentOption";

    public static final String CHILD_CBUWPRIVILEGEPAYMENTOPTION_RESET_VALUE = "";

    public static final String CHILD_TXUWADVANCEHOLD = "txUWAdvanceHold";

    public static final String CHILD_TXUWADVANCEHOLD_RESET_VALUE = "";

    public static final String CHILD_STUWMAXIMUMPRINCIPALBALANCEALLOWED = "stUWMaximumPrincipalBalanceAllowed";

    public static final String CHILD_STUWMAXIMUMPRINCIPALBALANCEALLOWED_RESET_VALUE = "";

    public static final String CHILD_STUWMINIMUMINCOMEREQUIRED = "stUWMinimumIncomeRequired";

    public static final String CHILD_STUWMINIMUMINCOMEREQUIRED_RESET_VALUE = "";

    public static final String CHILD_STUWMAXIMUMTDSEXPENSEALLOWED = "stUWMaximumTDSExpenseAllowed";

    public static final String CHILD_STUWMAXIMUMTDSEXPENSEALLOWED_RESET_VALUE = "";

    public static final String CHILD_CBUWDEALPURPOSE = "cbUWDealPurpose";

    public static final String CHILD_CBUWDEALPURPOSE_RESET_VALUE = "";

    public static final String CHILD_CBUWBRANCH = "cbUWBranch";

    public static final String CHILD_CBUWBRANCH_RESET_VALUE = "";

    public static final String CHILD_CBUWDEALTYPE = "cbUWDealType";

    public static final String CHILD_CBUWDEALTYPE_RESET_VALUE = "";

    public static final String CHILD_CBUWCROSSSELL = "cbUWCrossSell";

    public static final String CHILD_CBUWCROSSSELL_RESET_VALUE = "";

    public static final String CHILD_CBUWSPECIALFEATURE = "cbUWSpecialFeature";

    public static final String CHILD_CBUWSPECIALFEATURE_RESET_VALUE = "";

    public static final String CHILD_TXUWREFERENCEEXISTINGMTGNO = "txUWReferenceExistingMTGNo";

    public static final String CHILD_TXUWREFERENCEEXISTINGMTGNO_RESET_VALUE = "";

    public static final String CHILD_CBUWRATELOCKEDIN = "cbUWRateLockedIn";

    public static final String CHILD_CBUWRATELOCKEDIN_RESET_VALUE = "N";

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the
    // BXResources
    ////private static OptionList cbUWRateLockedInOptions=new OptionList(new
    // String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList cbUWRateLockedInOptions = new OptionList();

    public static final String CHILD_CBUWREFERENCETYPE = "cbUWReferenceType";

    public static final String CHILD_CBUWREFERENCETYPE_RESET_VALUE = "";

    public static final String CHILD_CBUWREPAYMENTTYPE = "cbUWRepaymentType";

    public static final String CHILD_CBUWREPAYMENTTYPE_RESET_VALUE = "";

    /***************MCM Impl team changes starts - XS_2.43 *******************/
    public static final String CHILD_STDEFAULTREPAYMENTTYPEID = "stDefaultRepaymentTypeId";
    public static final String CHILD_STDEFAULTREPAYMENTTYPEID_RESET_VALUE = "";
    /***************MCM Impl team changes starts - XS_2.43 *******************/
    
    public static final String CHILD_TXUWREFERENCEDEALNO = "txUWReferenceDealNo";

    public static final String CHILD_TXUWREFERENCEDEALNO_RESET_VALUE = "";

    public static final String CHILD_CBUWSECONDMORTGAGEEXIST = "cbUWSecondMortgageExist";

    public static final String CHILD_CBUWSECONDMORTGAGEEXIST_RESET_VALUE = "N";

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the
    // BXResources
    ////private static OptionList cbUWSecondMortgageExistOptions=new
    // OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList cbUWSecondMortgageExistOptions = new OptionList();

    public static final String CHILD_CBUWESTIMATEDCLOSINGDATEMONTH = "cbUWEstimatedClosingDateMonth";

    public static final String CHILD_CBUWESTIMATEDCLOSINGDATEMONTH_RESET_VALUE = "";

    private OptionList cbUWEstimatedClosingDateMonthOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_TXUWESTIMATEDCLOSINGDATEDAY = "txUWEstimatedClosingDateDay";

    public static final String CHILD_TXUWESTIMATEDCLOSINGDATEDAY_RESET_VALUE = "";

    public static final String CHILD_TXUWESTIMATEDCLOSINGDATEYEAR = "txUWEstimatedClosingDateYear";

    public static final String CHILD_TXUWESTIMATEDCLOSINGDATEYEAR_RESET_VALUE = "";

    public static final String CHILD_STINCLUDEFINANCINGPROGRAMSTART = "stIncludeFinancingProgramStart";

    public static final String CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE = "";

    public static final String CHILD_CBUWFINANCINGPROGRAM = "cbUWFinancingProgram";

    public static final String CHILD_CBUWFINANCINGPROGRAM_RESET_VALUE = "";

    public static final String CHILD_STINCLUDEFINANCINGPROGRAMEND = "stIncludeFinancingProgramEnd";

    public static final String CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE = "";

    public static final String CHILD_CBUWFIRSTPAYMETDATEMONTH = "cbUWFirstPaymetDateMonth";

    public static final String CHILD_CBUWFIRSTPAYMETDATEMONTH_RESET_VALUE = "";

    private OptionList cbUWFirstPaymetDateMonthOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_TXUWFIRSTPAYMETDATEDAY = "txUWFirstPaymetDateDay";

    public static final String CHILD_TXUWFIRSTPAYMETDATEDAY_RESET_VALUE = "";

    public static final String CHILD_TXUWFIRSTPAYMETDATEYEAR = "txUWFirstPaymetDateYear";

    public static final String CHILD_TXUWFIRSTPAYMETDATEYEAR_RESET_VALUE = "";

    public static final String CHILD_CBUWTAXPAYOR = "cbUWTaxPayor";

    public static final String CHILD_CBUWTAXPAYOR_RESET_VALUE = "";

    public static final String CHILD_STUWMATURITYDATE = "stUWMaturityDate";

    public static final String CHILD_STUWMATURITYDATE_RESET_VALUE = "";
/*
    public static final String CHILD_STUWSOURCEFIRM = "stUWSourceFirm";

    public static final String CHILD_STUWSOURCEFIRM_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCEADDRESSLINE1 = "stUWSourceAddressLine1";

    public static final String CHILD_STUWSOURCEADDRESSLINE1_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCECITY = "stUWSourceCity";

    public static final String CHILD_STUWSOURCECITY_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCEPROVINCE = "stUWSourceProvince";

    public static final String CHILD_STUWSOURCEPROVINCE_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCECONTACTPHONE = "stUWSourceContactPhone";

    public static final String CHILD_STUWSOURCECONTACTPHONE_RESET_VALUE = "";

    public static final String CHILD_STUWSOURCEFAX = "stUWSourceFax";

    public static final String CHILD_STUWSOURCEFAX_RESET_VALUE = "";
*/
    public static final String CHILD_REPEATAPPLICANTFINANCIALDETAILS = "RepeatApplicantFinancialDetails";

    public static final String CHILD_BTADDAPPLICANT = "btAddApplicant";

    public static final String CHILD_BTADDAPPLICANT_RESET_VALUE = " ";

    public static final String CHILD_BTDUPAPPLICANT = "btDupApplicant";

    public static final String CHILD_BTDUPAPPLICANT_RESET_VALUE = " ";

    public static final String CHILD_BTCREDITREPORT = "btCreditReport";

    public static final String CHILD_BTCREDITREPORT_RESET_VALUE = " ";

    public static final String CHILD_STUWCOMBINEDTOTALINCOME = "stUWCombinedTotalIncome";

    public static final String CHILD_STUWCOMBINEDTOTALINCOME_RESET_VALUE = "";

    public static final String CHILD_STUWCOMBINEDTOTALLIABILITIES = "stUWCombinedTotalLiabilities";

    public static final String CHILD_STUWCOMBINEDTOTALLIABILITIES_RESET_VALUE = "";

    public static final String CHILD_STUWTOTALNETWORTH = "stUWTotalNetWorth";

    public static final String CHILD_STUWTOTALNETWORTH_RESET_VALUE = "";

    public static final String CHILD_STUWCOMBINEDTOTALASSETS = "stUWCombinedTotalAssets";

    public static final String CHILD_STUWCOMBINEDTOTALASSETS_RESET_VALUE = "";

    public static final String CHILD_REPEATPROPERTYINFORMATION = "RepeatPropertyInformation";

    public static final String CHILD_BTADDPROPERTY = "btAddProperty";

    public static final String CHILD_BTADDPROPERTY_RESET_VALUE = " ";

    public static final String CHILD_STTOTALPROPERTYEXPENSES = "stTotalPropertyExpenses";

    public static final String CHILD_STTOTALPROPERTYEXPENSES_RESET_VALUE = "";

    public static final String CHILD_BTAPPRAISALREVIEW = "btAppraisalReview";

    public static final String CHILD_BTAPPRAISALREVIEW_RESET_VALUE = " ";

    public static final String CHILD_REPEATEDUWDOWNPAYMENT = "RepeatedUWDownPayment";

    public static final String CHILD_REPEATEDESCROWDETAILS = "RepeatedEscrowDetails";

    public static final String CHILD_STUWTARGETDOWNPAYMENT = "stUWTargetDownPayment";

    public static final String CHILD_STUWTARGETDOWNPAYMENT_RESET_VALUE = "";

    public static final String CHILD_STUWDOWNPAYMENTREQUIRED = "stUWDownPaymentRequired";

    public static final String CHILD_STUWDOWNPAYMENTREQUIRED_RESET_VALUE = "";

    public static final String CHILD_TXUWTOTALDOWN = "txUWTotalDown";

    public static final String CHILD_TXUWTOTALDOWN_RESET_VALUE = "";

    public static final String CHILD_BTADDDOWNPAYMENT = "btAddDownPayment";

    public static final String CHILD_BTADDDOWNPAYMENT_RESET_VALUE = " ";

    public static final String CHILD_BTADDESCROW = "btAddEscrow";

    public static final String CHILD_BTADDESCROW_RESET_VALUE = " ";

    public static final String CHILD_BTSAVESCENARIO = "btSaveScenario";

    public static final String CHILD_BTSAVESCENARIO_RESET_VALUE = " ";

    public static final String CHILD_BTVERIFYSCENARIO = "btVerifyScenario";

    public static final String CHILD_BTVERIFYSCENARIO_RESET_VALUE = " ";

    public static final String CHILD_BTCONDITIONS = "btConditions";

    public static final String CHILD_BTCONDITIONS_RESET_VALUE = " ";

    public static final String CHILD_BTRESOLVEDEAL = "btResolveDeal";

    public static final String CHILD_BTRESOLVEDEAL_RESET_VALUE = " ";

    public static final String CHILD_BTCOLLAPSEDENYDEAL = "btCollapseDenyDeal";

    public static final String CHILD_BTCOLLAPSEDENYDEAL_RESET_VALUE = "Collapse/Deny Deal";

    public static final String CHILD_BTUWCANCEL = "btUWCancel";

    public static final String CHILD_BTUWCANCEL_RESET_VALUE = " ";

    public static final String CHILD_BTPREVTASKPAGE = "btPrevTaskPage";

    public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE = " ";

    public static final String CHILD_STPREVTASKPAGELABEL = "stPrevTaskPageLabel";

    public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE = "";

    public static final String CHILD_BTNEXTTASKPAGE = "btNextTaskPage";

    public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE = " ";

    public static final String CHILD_STNEXTTASKPAGELABEL = "stNextTaskPageLabel";

    public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE = "";

    public static final String CHILD_STTASKNAME = "stTaskName";

    public static final String CHILD_STTASKNAME_RESET_VALUE = "";

    public static final String CHILD_SESSIONUSERID = "sessionUserId";

    public static final String CHILD_SESSIONUSERID_RESET_VALUE = "";

    public static final String CHILD_STPMGENERATE = "stPmGenerate";

    public static final String CHILD_STPMGENERATE_RESET_VALUE = "";

    public static final String CHILD_STPMHASTITLE = "stPmHasTitle";

    public static final String CHILD_STPMHASTITLE_RESET_VALUE = "";

    public static final String CHILD_STPMHASINFO = "stPmHasInfo";

    public static final String CHILD_STPMHASINFO_RESET_VALUE = "";

    public static final String CHILD_STPMHASTABLE = "stPmHasTable";

    public static final String CHILD_STPMHASTABLE_RESET_VALUE = "";

    public static final String CHILD_STPMHASOK = "stPmHasOk";

    public static final String CHILD_STPMHASOK_RESET_VALUE = "";

    public static final String CHILD_STPMTITLE = "stPmTitle";

    public static final String CHILD_STPMTITLE_RESET_VALUE = "";

    public static final String CHILD_STPMINFOMSG = "stPmInfoMsg";

    public static final String CHILD_STPMINFOMSG_RESET_VALUE = "";

    public static final String CHILD_STPMONOK = "stPmOnOk";

    public static final String CHILD_STPMONOK_RESET_VALUE = "";

    public static final String CHILD_STPMMSGS = "stPmMsgs";

    public static final String CHILD_STPMMSGS_RESET_VALUE = "";

    public static final String CHILD_STPMMSGTYPES = "stPmMsgTypes";

    public static final String CHILD_STPMMSGTYPES_RESET_VALUE = "";

    public static final String CHILD_STAMGENERATE = "stAmGenerate";

    public static final String CHILD_STAMGENERATE_RESET_VALUE = "";

    public static final String CHILD_STAMHASTITLE = "stAmHasTitle";

    public static final String CHILD_STAMHASTITLE_RESET_VALUE = "";

    public static final String CHILD_STAMHASINFO = "stAmHasInfo";

    public static final String CHILD_STAMHASINFO_RESET_VALUE = "";

    public static final String CHILD_STAMHASTABLE = "stAmHasTable";

    public static final String CHILD_STAMHASTABLE_RESET_VALUE = "";

    public static final String CHILD_STAMTITLE = "stAmTitle";

    public static final String CHILD_STAMTITLE_RESET_VALUE = "";

    public static final String CHILD_STAMINFOMSG = "stAmInfoMsg";

    public static final String CHILD_STAMINFOMSG_RESET_VALUE = "";

    public static final String CHILD_STAMMSGS = "stAmMsgs";

    public static final String CHILD_STAMMSGS_RESET_VALUE = "";

    public static final String CHILD_STAMMSGTYPES = "stAmMsgTypes";

    public static final String CHILD_STAMMSGTYPES_RESET_VALUE = "";

    public static final String CHILD_STAMDIALOGMSG = "stAmDialogMsg";

    public static final String CHILD_STAMDIALOGMSG_RESET_VALUE = "";

    public static final String CHILD_STAMBUTTONSHTML = "stAmButtonsHtml";

    public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE = "";

    public static final String CHILD_BTFEEREVIEW = "btFeeReview";

    public static final String CHILD_BTFEEREVIEW_RESET_VALUE = " ";

    public static final String CHILD_BTGOSCENARIO = "btGoScenario";

    public static final String CHILD_BTGOSCENARIO_RESET_VALUE = " ";

    public static final String CHILD_BTRECALCULATE = "btRecalculate";

    public static final String CHILD_BTRECALCULATE_RESET_VALUE = " ";

    public static final String CHILD_BTCHANGESOURCE = "btChangeSource";

    public static final String CHILD_BTCHANGESOURCE_RESET_VALUE = " ";

    public static final String CHILD_BTBRIDGEREVIEW = "btBridgeReview";

    public static final String CHILD_BTBRIDGEREVIEW_RESET_VALUE = " ";

    public static final String CHILD_STTARGETESCROW = "stTargetEscrow";

    public static final String CHILD_STTARGETESCROW_RESET_VALUE = "";

    public static final String CHILD_HDLONGPOSTEDDATE = "hdLongPostedDate";

    public static final String CHILD_HDLONGPOSTEDDATE_RESET_VALUE = "";

    public static final String CHILD_TBUWRATECODE = "tbUWRateCode";

    public static final String CHILD_TBUWRATECODE_RESET_VALUE = "";

    public static final String CHILD_TBUWPAYMENTTERMDESCRIPTION = "tbUWPaymentTermDescription";

    public static final String CHILD_TBUWPAYMENTTERMDESCRIPTION_RESET_VALUE = "";

    public static final String CHILD_STLENDERPRODUCTID = "stLenderProductId";

    public static final String CHILD_STLENDERPRODUCTID_RESET_VALUE = "";

    public static final String CHILD_STRATETIMESTAMP = "stRateTimeStamp";

    public static final String CHILD_STRATETIMESTAMP_RESET_VALUE = "";

    public static final String CHILD_STLENDERPROFILEID = "stLenderProfileId";

    public static final String CHILD_STLENDERPROFILEID_RESET_VALUE = "";

    public static final String CHILD_TBUWREFERENCESOURCEAPPLNO = "tbUWReferenceSourceApplNo";

    public static final String CHILD_TBUWREFERENCESOURCEAPPLNO_RESET_VALUE = "";

    public static final String CHILD_TBUWESCROWPAYMENTTOTAL = "tbUWEscrowPaymentTotal";

    public static final String CHILD_TBUWESCROWPAYMENTTOTAL_RESET_VALUE = "";

    public static final String CHILD_BTMIREVIEW = "btMIReview";

    public static final String CHILD_BTMIREVIEW_RESET_VALUE = " ";

    public static final String CHILD_BTUWPARTYSUMMARY = "btUWPartySummary";

    public static final String CHILD_BTUWPARTYSUMMARY_RESET_VALUE = " ";

    public static final String CHILD_STNEWSCENARIO1 = "stNewScenario1";

    public static final String CHILD_STNEWSCENARIO1_RESET_VALUE = "";

    public static final String CHILD_STNEWSCENARIO2 = "stNewScenario2";

    public static final String CHILD_STNEWSCENARIO2_RESET_VALUE = "";

    public static final String CHILD_BTOK = "btOK";

    public static final String CHILD_BTOK_RESET_VALUE = "Custom";

    public static final String CHILD_TXUWPREAPPESTPURCHASEPRICE = "txUWPreAppEstPurchasePrice";

    public static final String CHILD_TXUWPREAPPESTPURCHASEPRICE_RESET_VALUE = "";

    public static final String CHILD_CBUWMIINDICATOR = "cbUWMIIndicator";

    public static final String CHILD_CBUWMIINDICATOR_RESET_VALUE = "";

    public static final String CHILD_CBUWMITYPE = "cbUWMIType";

    public static final String CHILD_CBUWMITYPE_RESET_VALUE = "";

    public static final String CHILD_CBUWMIINSURER = "cbUWMIInsurer";

    public static final String CHILD_CBUWMIINSURER_RESET_VALUE = "";

    public static final String CHILD_RBUWMIRUINTERVENTION = "rbUWMIRUIntervention";

    public static final String CHILD_RBUWMIRUINTERVENTION_RESET_VALUE = "N";

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList rbUWMIRUInterventionOptions=new
    // OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList rbUWMIRUInterventionOptions = new OptionList();

    public static final String CHILD_TXUWMIEXISTINGPOLICY = "txUWMIExistingPolicy";

    public static final String CHILD_TXUWMIEXISTINGPOLICY_RESET_VALUE = "";

    public static final String CHILD_CBUWMIPAYOR = "cbUWMIPayor";

    public static final String CHILD_CBUWMIPAYOR_RESET_VALUE = "";

    ////public static final String CHILD_STUWMIPREQCERTNUM="stUWMIPreQCertNum";
    ////public static final String CHILD_STUWMIPREQCERTNUM_RESET_VALUE="";
    ////public static final String CHILD_STUWMISTATUS="stUWMIStatus";
    ////public static final String CHILD_STUWMISTATUS_RESET_VALUE="";
    public static final String CHILD_RBUWMIUPFRONT = "rbUWMIUpfront";

    public static final String CHILD_RBUWMIUPFRONT_RESET_VALUE = "N";

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList rbUWMIUpfrontOptions=new OptionList(new
    // String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList rbUWMIUpfrontOptions = new OptionList();

    public static final String CHILD_TXUWMIPREMIUM = "txUWMIPremium";

    public static final String CHILD_TXUWMIPREMIUM_RESET_VALUE = "";

    public static final String CHILD_TXUWMICERTIFICATE = "txUWMICertificate";

    public static final String CHILD_TXUWMICERTIFICATE_RESET_VALUE = "";

    public static final String CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS = "txUWEffectiveAmortizationYears";

    public static final String CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE = "";

    public static final String CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS = "txUWEffectiveAmortizationMonths";

    public static final String CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE = "";

    public static final String CHILD_STTOTALLOANAMOUNT = "stTotalLoanAmount";

    public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STAPPDATEFORSERVLET = "stAppDateForServlet";

    public static final String CHILD_STAPPDATEFORSERVLET_RESET_VALUE = "";

    public static final String CHILD_STMIPREMIUMTITLE = "stMIPremiumTitle";

    public static final String CHILD_STMIPREMIUMTITLE_RESET_VALUE = "";

    public static final String CHILD_STINCLUDEMIPREMIUMSTART = "stIncludeMIPremiumStart";

    public static final String CHILD_STINCLUDEMIPREMIUMSTART_RESET_VALUE = "";

    public static final String CHILD_STINCLUDEMIPREMIUMEND = "stIncludeMIPremiumEnd";

    public static final String CHILD_STINCLUDEMIPREMIUMEND_RESET_VALUE = "";

    public static final String CHILD_STINCLUDEMIPREMIUMVALUESTART = "stIncludeMIPremiumValueStart";

    public static final String CHILD_STINCLUDEMIPREMIUMVALUESTART_RESET_VALUE = "";

    public static final String CHILD_STINCLUDEMIPREMIUMVALUEEND = "stIncludeMIPremiumValueEnd";

    public static final String CHILD_STINCLUDEMIPREMIUMVALUEEND_RESET_VALUE = "";

    public static final String CHILD_STUWPHONENOEXTENSION = "stUWPhoneNoExtension";

    public static final String CHILD_STUWPHONENOEXTENSION_RESET_VALUE = "";

    public static final String CHILD_STAPPLICATIONDATE = "stApplicationDate";

    public static final String CHILD_STAPPLICATIONDATE_RESET_VALUE = "";

    public static final String CHILD_STUNDERWRITERLASTNAME = "stUnderwriterLastName";

    public static final String CHILD_STUNDERWRITERLASTNAME_RESET_VALUE = "";

    public static final String CHILD_STUNDERWRITERFIRSTNAME = "stUnderwriterFirstName";

    public static final String CHILD_STUNDERWRITERFIRSTNAME_RESET_VALUE = "";

    public static final String CHILD_STUNDERWRITERMIDDLEINITIAL = "stUnderwriterMiddleInitial";

    public static final String CHILD_STUNDERWRITERMIDDLEINITIAL_RESET_VALUE = "";

    public static final String CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH = "cbUWCommitmentExpectedDateMonth";

    public static final String CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH_RESET_VALUE = "";

    private OptionList cbUWCommitmentExpectedDateMonthOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR = "txUWCommitmentExpectedDateYear";

    public static final String CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR_RESET_VALUE = "";

    public static final String CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY = "txUWCommitmentExpectedDateDay";

    public static final String CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY_RESET_VALUE = "";

    /***** FFATE start *****/
    public static final String CHILD_CBUWFINANCINGWAIVERDATEMONTH = "cbUWFinancingWaiverDateMonth";

    public static final String CHILD_CBUWFINANCINGWAIVERDATEMONTH_RESET_VALUE = "";

    private OptionList cbUWFinancingWaiverDateMonthOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_TXUWFINANCINGWAIVERDATEYEAR = "txUWFinancingWaiverDateYear";

    public static final String CHILD_TXUWFINANCINGWAIVERDATEYEAR_RESET_VALUE = "";

    public static final String CHILD_TXUWFINANCINGWAIVERDATEDAY = "txUWFinancingWaiverDateDay";

    public static final String CHILD_TXUWFINANCINGWAIVERDATEDAY_RESET_VALUE = "";
    /***** FFATE END  *****/
    
    //CR03
    public static final String CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH = "cbUWCommitmentExpirationDateMonth";

    public static final String CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH_RESET_VALUE = "";

    private OptionList cbUWCommitmentExpirationDateMonthOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR = "txUWCommitmentExpirationDateYear";

    public static final String CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR_RESET_VALUE = "";

    public static final String CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY = "txUWCommitmentExpirationDateDay";

    public static final String CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY_RESET_VALUE = "";
    //CR03

    public static final String CHILD_STRATELOCK = "stRateLock";

    public static final String CHILD_STRATELOCK_RESET_VALUE = "";

    public static final String CHILD_STVIEWONLYTAG = "stViewOnlyTag";

    public static final String CHILD_STVIEWONLYTAG_RESET_VALUE = "";

    public static final String CHILD_STSOURCEFIRSTNAME = "stSourceFirstName";

    public static final String CHILD_STSOURCEFIRSTNAME_RESET_VALUE = "";

    public static final String CHILD_STSOURCELASTNAME = "stSourceLastName";

    public static final String CHILD_STSOURCELASTNAME_RESET_VALUE = "";

    public static final String CHILD_RBPROGRESSADVANCE = "rbProgressAdvance";

    public static final String CHILD_RBPROGRESSADVANCE_RESET_VALUE = "N";

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList rbProgressAdvanceOptions=new OptionList(new
    // String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList rbProgressAdvanceOptions = new OptionList();

    public static final String CHILD_TXNEXTADVANCEAMOUNT = "txNextAdvanceAmount";

    public static final String CHILD_TXNEXTADVANCEAMOUNT_RESET_VALUE = "";

    public static final String CHILD_TXADVANCETODATEAMT = "txAdvanceToDateAmt";

    public static final String CHILD_TXADVANCETODATEAMT_RESET_VALUE = "";

    public static final String CHILD_TXADVANCENUMBER = "txAdvanceNumber";

    public static final String CHILD_TXADVANCENUMBER_RESET_VALUE = "";

    public static final String CHILD_CBUWREFIORIGPURCHASEDATEMONTH = "cbUWRefiOrigPurchaseDateMonth";

    public static final String CHILD_CBUWREFIORIGPURCHASEDATEMONTH_RESET_VALUE = "";

    private OptionList cbUWRefiOrigPurchaseDateMonthOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_TXUWREFIORIGPURCHASEDATEYEAR = "txUWRefiOrigPurchaseDateYear";

    public static final String CHILD_TXUWREFIORIGPURCHASEDATEYEAR_RESET_VALUE = "";

    public static final String CHILD_TXUWREFIORIGPURCHASEDATEDAY = "txUWRefiOrigPurchaseDateDay";

    public static final String CHILD_TXUWREFIORIGPURCHASEDATEDAY_RESET_VALUE = "";

    public static final String CHILD_TXUWREFIPURPOSE = "txUWRefiPurpose";

    public static final String CHILD_TXUWREFIPURPOSE_RESET_VALUE = "";

    public static final String CHILD_TXUWREFIMORTGAGEHOLDER = "txUWRefiMortgageHolder";

    public static final String CHILD_TXUWREFIMORTGAGEHOLDER_RESET_VALUE = "";

    public static final String CHILD_RBBLENDEDAMORTIZATION = "rbBlendedAmortization";

    public static final String CHILD_RBBLENDEDAMORTIZATION_RESET_VALUE = "N";

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList rbBlendedAmortizationOptions=new
    // OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList rbBlendedAmortizationOptions = new OptionList();

    public static final String CHILD_TXUWREFIORIGPURCHASEPRICE = "txUWRefiOrigPurchasePrice";

    public static final String CHILD_TXUWREFIORIGPURCHASEPRICE_RESET_VALUE = "";

    public static final String CHILD_TXUWREFIIMPROVEMENTVALUE = "txUWRefiImprovementValue";

    public static final String CHILD_TXUWREFIIMPROVEMENTVALUE_RESET_VALUE = "";

    public static final String CHILD_TXUWREFIORIGMTGAMOUNT = "txUWRefiOrigMtgAmount";

    public static final String CHILD_TXUWREFIORIGMTGAMOUNT_RESET_VALUE = "";

    public static final String CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT = "txUWRefiOutstandingMtgAmount";

    public static final String CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE = "";

    public static final String CHILD_TXUWREFIIMPROVEMENTSDESC = "txUWRefiImprovementsDesc";

    public static final String CHILD_TXUWREFIIMPROVEMENTSDESC_RESET_VALUE = "";

    public static final String CHILD_STVALSDATA = "stVALSData";

    public static final String CHILD_STVALSDATA_RESET_VALUE = "";

    //// New hidden button implementing the 'fake' one from the ActiveMessage class.
    public static final String CHILD_BTACTMSG = "btActMsg";

    public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";

    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value throughout all modulus.
    public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";

    public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

    ////--Release2.1--//
    public static final String CHILD_STRATEINVENTORYID = "stRateInventoryId";

    public static final String CHILD_STRATEINVENTORYID_RESET_VALUE = "";

    ////--Release2.1--//
    //// New HiddenField to get the paymentTermId from JavaScript and propagate into
    //// the database.
    public static final String CHILD_HDPAYMENTTERMID = "hdPaymentTermId";

    public static final String CHILD_HDPAYMENTTERMID_RESET_VALUE = "";

    //===============================================================
    // New fields to handle MIPolicyNum problem :
    //  the number lost if user cancel MI and re-send again later.
    // -- Modified by Billy 14July2003
    public static final String CHILD_HDMIPOLICYNOCMHC = "hdMIPolicyNoCMHC";

    public static final String CHILD_HDMIPOLICYNOCMHC_RESET_VALUE = "";

    public static final String CHILD_HDMIPOLICYNOGE = "hdMIPolicyNoGE";

    public static final String CHILD_HDMIPOLICYNOGE_RESET_VALUE = "";
    public static final String CHILD_HDMIPOLICYNOAIGUG = "hdMIPolicyNoAIGUG";
    public static final String CHILD_HDMIPOLICYNOAIGUG_RESET_VALUE = "";
	public static final String CHILD_HDMIPOLICYNOPMI = "hdMIPolicyNoPMI";
	public static final String CHILD_HDMIPOLICYNOPMI_RESET_VALUE = "";

    //===============================================================
    //--BMO_MI_CR--start//
    public static final String CHILD_HDOUTSIDEXPRESS = "hdOutsideXpress";

    public static final String CHILD_HDOUTSIDEXPRESS_RESET_VALUE = "";

    public static final String CHILD_TXUWMIPREQCERTNUM = "txUWMIPreQCertNum";

    public static final String CHILD_TXUWMIPREQCERTNUM_RESET_VALUE = "";

    public static final String CHILD_CBUWMISTATUS = "cbUWMIStatus";

    public static final String CHILD_CBUWMISTATUS_RESET_VALUE = "";

    public static final String CHILD_HDMISTATUSID = "hdMIStatusId";

    public static final String CHILD_HDMISTATUSID_RESET_VALUE = "0";

    public static final String CHILD_HDMIINDICATORID = "hdMIIndicatorId";

    public static final String CHILD_HDMIINDICATORID_RESET_VALUE = "0";

    public static final String CHILD_HDMIINSURER = "hdMIInsurer";

    public static final String CHILD_HDMIINSURER_RESET_VALUE = "";

    public static final String CHILD_HDMIPREMIUM = "hdMIPremium";

    public static final String CHILD_HDMIPREMIUM_RESET_VALUE = "";

    public static final String CHILD_HDMIPREQCERTNUM = "hdMIPreQCertNum";

    public static final String CHILD_HDMIPREQCERTNUM_RESET_VALUE = "";

    //--BMO_MI_CR--end//
    //-- FXLink Phase II --//
    //--> New Field for Market Type Indicator
    //--> By Billy 18Nov2003
    public static final String CHILD_STMARKETTYPELABEL = "stMarketTypeLabel";

    public static final String CHILD_STMARKETTYPELABEL_RESET_VALUE = "";

    public static final String CHILD_STMARKETTYPE = "stMarketType";

    public static final String CHILD_STMARKETTYPE_RESET_VALUE = "";

    //--DJ_PT_CR--start//
    public static final String CHILD_STBRANCHTRANSITLABEL = "stBranchTransitLabel";

    public static final String CHILD_STBRANCHTRANSITLABEL_RESET_VALUE = "";

    public static final String CHILD_STPARTYNAME = "stPartyName";

    public static final String CHILD_STPARTYNAME_RESET_VALUE = "";

    public static final String CHILD_STBRANCHTRANSITNUM = "stBranchTransitNumber";

    public static final String CHILD_STBRANCHTRANSITNUM_RESET_VALUE = "";

    public static final String CHILD_STADDRESSLINE1 = "stAddressLine1";

    public static final String CHILD_STADDRESSLINE1_RESET_VALUE = "";

    public static final String CHILD_STCITY = "stCity";

    public static final String CHILD_STCITY_RESET_VALUE = "";

    //--DJ_PT_CR--end//
    //--DJ_LDI_CR--start--//
    public static final String CHILD_BTLDINSURANCEDETAILS = "btLDInsuranceDetails";

    public static final String CHILD_BTLDINSURANCEDETAILS_RESET_VALUE = " ";

    public static final String CHILD_STINCLUDELIFEDISLABELSSTART = "stIncludeLifeDisLabelsStart";

    public static final String CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE = "";

    public static final String CHILD_STINCLUDELIFEDISLABELSEND = "stIncludeLifeDisLabelsEnd";

    public static final String CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE = "";

    public static final String CHILD_STGDSTDSDETAILSLABEL = "stGdsTdsDetailsLabel";

    public static final String CHILD_STGDSTDSDETAILSLABEL_RESET_VALUE = "";

    public static final String CHILD_STPMNTINCLUDINGLIFEDISABILITY = "stPmntInclLifeDis";

    public static final String CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE = "";

    //--DJ_LDI_CR--end--//
    //--FX_LINK--start--//
    public static final String CHILD_BTSOURCEOFBUSINESSDETAILS = "btSOBDetails";

    public static final String CHILD_BTSOURCEOFBUSINESSDETAILS_RESET_VALUE = " ";

    //--FX_LINK--end--//
    //--DJ_CR010--start//
    public static final String CHILD_TXCOMMISIONCODE = "txCommisionCode";

    public static final String CHILD_TXCOMMISIONCODE_RESET_VALUE = "";

    //--DJ_CR010--end//
    //--DJ_CR203.1--start//
    public static final String CHILD_TXPROPRIETAIREPLUSLOC = "txProprietairePlusLOC";

    public static final String CHILD_TXPROPRIETAIREPLUSLOC_RESET_VALUE = "";

    protected OptionList rbMultiProjectOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_RBMULTIPROJECT = "rbMultiProject";

    public static final String CHILD_RBMULTIPROJECT_RESET_VALUE = "";

    protected OptionList rbProprietairePlusOptions = new OptionList(
            new String[] {}, new String[] {});

    public static final String CHILD_RBPROPRIETAIREPLUS = "rbProprietairePlus";

    public static final String CHILD_RBPROPRIETAIREPLUS_RESET_VALUE = "N";

    //--DJ_CR203.1--end//
    //--DJ_CR134--start--27May2004--//
    public static final String CHILD_STHOMEBASEPRODUCTRATEPMNT = "stHomeBASERateProductPmnt";

    public static final String CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE = "";

    //--DJ_CR134--end--//
    //--DisableProductRateTicket#570--10Aug2004--start--//
    public static final String CHILD_STDEALRATESTATUSFORSERVLET = "stDealRateStatusForServlet";

    public static final String CHILD_STDEALRATESTATUSFORSERVLET_RESET_VALUE = "";

    public static final String CHILD_STRATEDISDATEFORSERVLET = "stRateDisDateForServlet";

    public static final String CHILD_STRATEDISDATEFORSERVLET_RESET_VALUE = "";

    public static final String CHILD_STISPAGEEDITABLEFORSERVLET = "stIsPageEditableForServlet";

    public static final String CHILD_STISPAGEEDITABLEFORSERVLET_RESET_VALUE = "";

    //--DisableProductRateTicket#570--10Aug2004--end--//
    //--Ticket#575--16Sep2004--start--//
    public static final String CHILD_HDINTERESTTYPEID = "hdInterestTypeId";

    public static final String CHILD_HDINTERESTTYPEID_RESET_VALUE = "";
    
    //--Ticket#575--16Sep2004--end--//
    //-- ========== DJ#725 Begins ========== --//
    //-- By Neil at Nov/29/2004
    public static final String CHILD_STCONTACTEMAILADDRESS = "stContactEmailAddress";

    public static final String CHILD_STCONTACTEMAILADDRESS_RESET_VALUE = "";

    public static final String CHILD_STPSDESCRIPTION = "stPsDescription";

    public static final String CHILD_STPSDESCRIPTION_RESET_VALUE = "";

    public static final String CHILD_STSYSTEMTYPEDESCRIPTION = "stSystemTypeDescription";

    public static final String CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE = "";

    //-- ========== DJ#725 Ends ========== --//

    //-- ========== SCR#859 begins ========== --//
    //-- by Neil on Feb 14, 2005
    public static final String CHILD_STSERVICINGMORTGAGENUMBER = "stServicingMortgageNumber";

    public static final String CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE = "";

    public static final String CHILD_STCCAPS = "stCCAPS";

    public static final String CHILD_STCCAPS_RESET_VALUE = "";

    //-- ========== SCR#859 ends ========== --//

    // SEAN GECF Document Type drop down Define constants.
    public static final String CHILD_CBIVDOCUMENTTYPE = "cbIVDocumentType";

    public static final String CHILD_CBIVDOCUMENTTYPE_RESET_VALUE = "";

    public static final String CHILD_STHIDEIVDOCUMENTTYPESTART = "stHideIVDocumentTypeStart";

    public static final String CHILD_STHIDEIVDOCUMENTTYPESTART_RESET_VALUE = "";

    public static final String CHILD_STHIDEIVDOCUMENTTYPEEND = "stHideIVDocumentTypeEnd";

    public static final String CHILD_STHIDEIVDOCUMENTTYPEEND_RESET_VALUE = "";

    // END GECF Document Type drop down

    // SEAN DJ SPEC-Progress Advance Type July 20, 2005: added a drop down combo
    // box, it will be hidden for non-DJ client.
    public static final String CHILD_CBPROGRESSADVANCETYPE = "cbProgressAdvanceType";
    public static final String CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE = "";
    // SEAN DJ SPEC-PAT END
    public static final String CHILD_STHIDEPROGRESSADVANCETYPESTART = "stHideProgressAdvanceTypeStart";

    public static final String CHILD_STHIDEPROGRESSADVANCETYPESTART_RESET_VALUE = "";

    public static final String CHILD_STHIDEPROGRESSADVANCETYPEEND = "stHideProgressAdvanceTypeEnd";

    public static final String CHILD_STHIDEPROGRESSADVANCETYPEEND_RESET_VALUE = "";

    public static final String CHILD_TXRATEGUARANTEEPERIOD = "txRateGuaranteePeriod";

    public static final String CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE = "";


    //--Release3.1--begins
    //--by Hiro Apr 13, 2006
    public static final String CHILD_CBPRODUCTTYPE = "cbProductType";

    public static final String CHILD_CBPRODUCTTYPE_RESET_VALUE = " ";

    public static final String CHILD_CBREFIPRODUCTTYPE = "cbRefiProductType";

    public static final String CHILD_CBREFIPRODUCTTYPE_RESET_VALUE = " ";

    public static final String CHILD_RBPROGRESSADVANCEINSPECTIONBY = "rbProgressAdvanceInspectionBy";

    public static final String CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE = "";

    private OptionList rbProgressAdvanceInspectionByOptions = new OptionList();

    public static final String CHILD_RBSELFDIRECTEDRRSP = "rbSelfDirectedRRSP";

    public static final String CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE = "";

    private OptionList rbSelfDirectedRRSPOptions = new OptionList();

    //CR03

    public static final String CHILD_RBAUTOCALCULATECOMMITMENTDATE = "rbAutoCalculateCommitmentDate";

    public static final String CHILD_RBAUTOCALCULATECOMMITMENTDATE_RESET_VALUE = "Y";

    private OptionList rbAutoCalculateCommitmentDateOptions = new OptionList();
    //CR03

    //Purchase Plus Improvements
    public static final String CHILD_TXUWIMPROVEDVALUE = "txUWImprovedValue";

    public static final String CHILD_TXUWIMPROVEDVALUE_RESET_VALUE = "";

    public static final String CHILD_STDEALPURPOSETYPEHIDDENSTART = "stDealPurposeTypeHiddenStart";

    public static final String CHILD_STDEALPURPOSETYPEHIDDENSTART_RESET_VALUE = "";

    public static final String CHILD_STDEALPURPOSETYPEHIDDENEND = "stDealPurposeTypeHiddenEnd";

    public static final String CHILD_STDEALPURPOSETYPEHIDDENEND_RESET_VALUE = "";
    //Purchase Plus Improvements

    public static final String CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER = "txCMHCProductTrackerIdentifier";

    public static final String CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE = "";

    public static final String CHILD_CBLOCREPAYMENTTYPEID = "cbLOCRepaymentTypeId";

    public static final String CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE = " ";

    public static final String CHILD_RBREQUESTSTANDARDSERVICE = "rbRequestStandardService";

    public static final String CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE = "";

    private OptionList rbRequestStandardServiceOptions = new OptionList();

    public static final String CHILD_STLOCAMORTIZATIONMONTHS = "stLOCAmortizationMonths";

    public static final String CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE = "";

    public static final String CHILD_STLOCINTERESTONLYMATURITYDATE = "stLOCInterestOnlyMaturityDate";

    public static final String CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE = "";

    //CR-18
    public static final String CHILD_HDREQINPROGRESS = "hdReqInProgress";
    public static final String CHILD_HDREQINPROGRESS_RESET_VALUE = "";
    public static final String CHILD_HDREQINPROGRESS_FOR_ANOTHER_SCENARIO = "hdReqInProgressForAnotherScenario";
    public static final String CHILD_HDREQINPROGRESS_FOR_ANOTHER_SCENARIO_RESET_VALUE = "";

//  ***** Change by NBC Impl. Team - Version 55 - Start *****//

    public static final String CHILD_HDFORCEPROGRESSADVANCE = "hdForceProgressAdvance";
    public static final String CHILD_HDFORCEPROGRESSADVANCE_RESET_VALUE = "";
   
    //***** Qualifying Rate *****/
    public static final String CHILD_CBQUALIFYPRODUCTTYPE = "cbQualifyProductType";
    public static final String CHILD_CBQUALIFYPRODUCTTYPE_RESET_VALUE = " ";
    
    public static final String CHILD_TXQUALIFYRATE = "txQualifyRate";
    public static final String CHILD_TXQUALIFYRATE_RESET_VALUE = "";
    
    public static final String CHILD_CHQUALIFYRATEOVERRIDE = "chQualifyRateOverride";
    public static final String CHILD_CHQUALIFYRATEOVERRIDE_RESET_VALUE = "N";
    
    public static final String CHILD_CHQUALIFYRATEOVERRIDERATE = "chQualifyRateOverrideRate";
    public static final String CHILD_CHQUALIFYRATEOVERRIDERATE_RESET_VALUE = "N";

    public static final String CHILD_BTRECALCULATEGDS = "btRecalculateGDS";
    public static final String CHILD_BTRECALCULATEGDS_RESET_VALUE = " ";
    
    public static final String CHILD_STQUALIFYRATE = "stQualifyRate";
    public static final String CHILD_STQUALIFYRATE_RESET_VALUE = "";
    
    public static final String CHILD_STQUALIFYPRODUCTTYPE = "stQualifyProductType";
    public static final String CHILD_STQUALIFYPRODUCTTYPE_RESET_VALUE = "";
    
    public static final String CHILD_STQUALIFYRATEHIDDEN = "stQualifyRateHidden";
    public static final String CHILD_STQUALIFYRATEHIDDEN_RESET_VALUE = "none";
    
    public static final String CHILD_STQUALIFYRATEEDIT = "stQualifyRateEdit";
    public static final String CHILD_STQUALIFYRATEEDIT_RESET_VALUE = "none";
    
    public static final String CHILD_STHDQUALIFYRATE = "sthdQualifyRate";
    public static final String CHILD_STHDQUALIFYRATE_RESET_VALUE = "";
    
    public static final String CHILD_STHDQUALIFYPRODUCTID = "sthdQualifyProductId";
    public static final String CHILD_STHDQUALIFYPRODUCTID_RESET_VALUE = "";
    
    public static final String CHILD_HDQUALIFYRATEDISABLED = "hdQualifyRateDisabled";
    public static final String CHILD_HDQUALIFYRATEDISABLED_RESET_VALUE = "";
    //***** Qualifying Rate *****/
    
    public HiddenField getHdForceProgressAdvance()
    {
        return (HiddenField)getChild(CHILD_HDFORCEPROGRESSADVANCE);
    }

//  ***** Change by NBC Impl. Team - Version 55 - End*****//
    public HiddenField getHdReqInProgress()
    {
        return (HiddenField)getChild(CHILD_HDREQINPROGRESS);
    }
    public HiddenField getHdReqInProgressForAnotherScenario()
    {
        return (HiddenField)getChild(CHILD_HDREQINPROGRESS_FOR_ANOTHER_SCENARIO);
    }

    //--Release3.1--ends

    // ***** Change by NBC Impl. Team - Version 1.3 - Start*****//
    public static final String CHILD_CBAFFILIATIONPROGRAM = "cbAffiliationProgram";

    public static final String CHILD_CBAFFILIATIONPROGRAM_RESET_VALUE = "";
    // ***** Change by NBC Impl. Team - Version 1.3 - End*****//

    //***** Change by NBC/PP Implementation Team - GCD - Start *****//

    public static final String CHILD_TX_CREDIT_DECISION_STATUS = "txCreditDecisionStatus";
    public static final String CHILD_TX_CREDIT_DECISION = "txCreditDecision";
    public static final String CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE = "txGlobalCreditBureauScore";
    public static final String CHILD_TX_GLOBAL_RISK_RATING = "txGlobalRiskRating";
    public static final String CHILD_TX_GLOBAL_INTERNAL_SCORE = "txGlobalInternalScore";

    //Use this one to get MSStatus as a static field
    public static final String CHILD_TX_MISTATUS = "txMIStatus";
    public static final String CHILD_BT_CREDIT_DECISION_PG = "btCreditDecisionPG";
    public static final String CHILD_HD_DEAL_ID = "hdDealID";

    public static final String CHILD_ST_INCLUDE_GCDSUM_START = "stIncludeGCDSumStart";
    public static final String CHILD_ST_INCLUDE_GCDSUM_END = "stIncludeGCDSumEnd";

    public static final String CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE = "";
    public static final String CHILD_TX_CREDIT_DECISION_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE = "";
    public static final String CHILD_TX_MISTATUS_RESET_VALUE = "";
    public static final String CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE = "";
    public static final String CHILD_HD_DEAL_ID_RESET_VALUE = "";

    public static final String CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE = "";
    public static final String CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE = "";

    private doAdjudicationResponseBNCModel doAdjudicationResponseBNCModel= null;

    private doDealMIStatusModel doDealMIStatusModel= null;

    //***** Change by NBC/PP Implementation Team - GCD - End *****//

    /***************MCM Impl team changes starts - XS_2.46 *******************/

    public static final String CHILD_TXREFIADDITIONALINFORMATION ="txRefiAdditionalInformation";
    public static final String CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE = "";

    /***************MCM Impl team changes ends - XS_2.46 *********************/
    
    // Modified for Ticket 267 by Neha
    public static final String CHILD_HDDEALSTATUSID = "hdDealStatusId";
    public static final String CHILD_HDDEALSTATUSID_RESET_VALUE="";

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    private CbPageNamesOptionList cbPageNamesOptions = new CbPageNamesOptionList();

    ////Added the Variable for NonSelected Label
    private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

    private CbUWChargeOptionList cbUWChargeOptions = new CbUWChargeOptionList();

    private CbUWLOBOptionList cbUWLOBOptions = new CbUWLOBOptionList();

    private CbUWPaymentFrequencyOptionList cbUWPaymentFrequencyOptions = new CbUWPaymentFrequencyOptionList();

    private CbUWPrePaymentPenaltyOptionList cbUWPrePaymentPenaltyOptions = new CbUWPrePaymentPenaltyOptionList();

    private CbUWPrivilegePaymentOptionOptionList cbUWPrivilegePaymentOptionOptions = new CbUWPrivilegePaymentOptionOptionList();

    private CbUWDealPurposeOptionList cbUWDealPurposeOptions = new CbUWDealPurposeOptionList();

    private CbUWBranchOptionList cbUWBranchOptions = new CbUWBranchOptionList();

    // SEAN DJ SPEC-Progress Advance Type July 20, 2005: Define the ComboBox option list.
    private CbProgressAdvanceTypeOptionList cbProgressAdvanceTypeOptions = new CbProgressAdvanceTypeOptionList();

    // SEAN DJ SPEC-PAT END

    // SEAN GECF IV Document Type Drop Down define the option list.
    private CbIVDocumentTypeOptionList cbIVDocumentTypeOptions = new CbIVDocumentTypeOptionList();

    // END GECF IV Document Type Drop Down

    //--Release2.1--//
    private CbUWDealTypeOptionList cbUWDealTypeOptions = new CbUWDealTypeOptionList();

    private CbUWCrossSellOptionList cbUWCrossSellOptions = new CbUWCrossSellOptionList();

    private CbUWSpecialFeatureOptionList cbUWSpecialFeatureOptions = new CbUWSpecialFeatureOptionList();

    private CbUWReferenceTypeOptionList cbUWReferenceTypeOptions = new CbUWReferenceTypeOptionList();

    private CbUWRepaymentTypeOptionList cbUWRepaymentTypeOptions = new CbUWRepaymentTypeOptionList();

    private CbUWFinancingProgramOptionList cbUWFinancingProgramOptions = new CbUWFinancingProgramOptionList();

    private CbUWTaxPayorOptionList cbUWTaxPayorOptions = new CbUWTaxPayorOptionList();

    private CbUWMIIndicatorOptionList cbUWMIIndicatorOptions = new CbUWMIIndicatorOptionList();

    // private CbUWMITypeOptionList cbUWMITypeOptions = new CbUWMITypeOptionList(); //5.0 MI deleted

    private CbUWMIInsurerOptionList cbUWMIInsurerOptions = new CbUWMIInsurerOptionList();

    private CbUWMIPayorOptionList cbUWMIPayorOptions = new CbUWMIPayorOptionList();

    private CbUWMIStatusOptionList cbUWMIStatusOptions = new CbUWMIStatusOptionList();

    //--Release3.1--begins
    //--by Hiro Apr 13, 2006
    private CbProductTypeOptionList cbProductTypeOptions = new CbProductTypeOptionList();

    private CbRefiProductTypeOptionList cbRefiProductTypeOptions = new CbRefiProductTypeOptionList();

    private CbLOCRepaymentTypeOptionList cbLOCRepaymentTypeOptions = new CbLOCRepaymentTypeOptionList();

    //--Release3.1--ends

    //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
    private CbAffiliationProgramOptionList cbAffiliationProgramOptions = new CbAffiliationProgramOptionList();
    //  ***** Change by NBC Impl. Team - Version 1.3 - End *****//

    ////////////////////////////////////////////////////////////////////////////
    // Instance variables
    ////////////////////////////////////////////////////////////////////////////
    private doUWDealSelectMainModel doUWDealSelectMain = null;

    private doUWDealSummarySnapShotModel doUWDealSummarySnapShot = null;

    private doUWBridgeModel doUWBridge = null;

    private doUWSourceDetailsModel doUWSourceDetails = null;

    private doUWMIStatusModel doUWMIStatus = null;

    private doUWSourceInformationModel doUWSourceInformation = null;

    private String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgUWorksheet.jsp"; ////
    // default
    // is
    // English
//  ***** Change by NBC Impl. Team - Version 1.5 - Start *****//

    public static final String CHILD_TXCASHBACKINDOLLARS = "txCashBackInDollars";

    public static final String CHILD_TXCASHBACKINDOLLARS_RESET_VALUE = "";

    public static final String CHILD_TXCASHBACKINPERCENTAGE = "txCashBackInPercentage";

    public static final String CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE = "";

    public static final String CHILD_CHCASHBACKOVERRIDEINDOLLARS = "chCashBackOverrideInDollars";

    public static final String CHILD_CHCASHBACKOVERRIDEINDOLLARS_RESET_VALUE = "N";

    //4.4 Submission Agent
    public static final String CHILD_REPEATEDSOB = "RepeatedSOB";
    public static final String CHILD_REPEATEDSOB_RESET_VALUE = "";
    
    // ***** Change by NBC Impl. Team - Version 1.5 - End*****//

    /***************MCM Impl team changes starts - XS_2.13*******************/
    public static final String CHILD_STQUALIFYINGDETAILSPAGELET = "qualifyingDetailsPagelet";
    /***************MCM Impl team changes ends - XS_2.13*******************/

    /***************MCM Impl team changes starts - XS_2.44*******************/
    public static final String CHILD_COMPONENTINFOPAGELET = "componentInfoPagelet";
    /***************MCM Impl team changes ends - XS_2.44*******************/

    //5.0 MI -- start
    public static final String CHILD_TXMIRESPONSE_FIRSTLIEN = "txMIResponseFirstLine";
    public static final String CHILD_TXMIRESPONSE_FIRSTLIEN_RESET_VALUE = "";
    public static final String CHILD_BTEXPAND_MIRESPONSE = "btExpandMIResponse";
    public static final String CHILD_BTEXPAND_MIRESPONSE_RESET_VALUE = "";
    
    public static final String CHILD_HDUWCOMBINEDLENDINGVALUE = "hdUWCombinedLendingValue";

    public static final String CHILD_HDUWCOMBINEDLENDINGVALUE_RESET_VALUE = "";
    //5.0 MI -- end
    
    //--DJ_PT_CR--start//
    private doPartySummaryModel doPartySummary = null;

    //--DJ_PT_CR--end//
    ////////////////////////////////////////////////////////////////////////////
    // Custom Members - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    // MigrationToDo : Migrate custom member
    protected UWorksheetHandler handler = new UWorksheetHandler();

    public SysLogger logger;

    /**
     * 
     *  
     */
    public pgUWorksheetViewBean() {
        super(PAGE_NAME);
        setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

        registerChildren();

        initialize();
    }

    /**
     * 
     *  
     */
    protected void initialize() {
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Child manipulation methods
    ////////////////////////////////////////////////////////////////////////////////

    /**
     * createChild
     * 
     * @param oneInput
     *            String <br>
     *  
     * @return View : the result of createChild <br>
     * @version 1.2 <br>
     *          Date: 6/26/006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: <br>
     *          added block - else if (name.equals(CHILD_TXRATEGUARANTEEPERIOD))
     *@version 55 
     * Date: 11/20/2006 
     * Author: NBC/PP Implementation Team 
     * added create child for
     * CHILD_HDFORCEPROGRESSADVANCE  
     *
     *  @Version 1.3 <br>
     *  Date: 06/09/2008
     *  Author: MCM Impl Team <br>
     *  Change Log:  <br>
     *  XS_2.46 -- 06/06/2008 -- Added necessary changes required for the new fields added in Refinance section<br>
     *  @Version 1.4 <br>
     *  Date: July 21, 2008
     *  Author: MCM Impl Team <br>
     *  Change Log:  <br>
     *  XS_2.44 -- Added ComponentInfo section<br>
     *  
     * @version 1.5 22-JULY-2008 XS_2.44 added handler as a pgComponentInfoPageletView constractor parameter
     */
    protected View createChild(String name) {

        View superReturn = super.createChild(name);
        if (superReturn != null) {
            return superReturn;
        } else if (name.equals(CHILD_TBDEALID)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TBDEALID, CHILD_TBDEALID, CHILD_TBDEALID_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_CBPAGENAMES)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBPAGENAMES, CHILD_CBPAGENAMES,
                    CHILD_CBPAGENAMES_RESET_VALUE, null);

            //--Release2.1--//
            ////Modified to set NonSelected Label as a
            // CHILD_CBPAGENAMES_NONSELECTED_LABEL
            ////child.setLabelForNoneSelected("Choose a Page");
            child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
            child.setOptions(cbPageNamesOptions);

            return child;
        } 
//      ***** Change by NBC Impl. Team - Version 55 - Start *****//

        else if (name.equals(CHILD_HDFORCEPROGRESSADVANCE))

        {
            HiddenField child = new HiddenField(this, getDefaultModel(),CHILD_HDFORCEPROGRESSADVANCE, CHILD_HDFORCEPROGRESSADVANCE, CHILD_HDFORCEPROGRESSADVANCE_RESET_VALUE, null);

            return child;
        }

//      ***** Change by NBC Impl. Team - Version 55 - End*****//

//      ***** Change by NBC Impl. Team - Version 1.5 - Start *****//
        else if (name.equals(CHILD_TXCASHBACKINDOLLARS)) {

            TextField child = new TextField(this,
                    getdoUWDealSelectMainModel(), CHILD_TXCASHBACKINDOLLARS,
                    doUWDealSelectMainModel.FIELD_DFCASHBACKINDOLLARS,
                    CHILD_TXCASHBACKINDOLLARS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXCASHBACKINPERCENTAGE)) {

            TextField child = new TextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_TXCASHBACKINPERCENTAGE,
                    doUWDealSelectMainModel.FIELD_DFCASHBACKINPERCENTAGE,
                    CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CHCASHBACKOVERRIDEINDOLLARS)) {
            CheckBox child = new CheckBox(
                    this,
                    getdoUWDealSelectMainModel(),
                    CHILD_CHCASHBACKOVERRIDEINDOLLARS,
                    // CHILD_CHCASHBACKOVERRIDEINDOLLARS,
                    doUWDealSelectMainModel.FIELD_DFCASHBACKOVERRIDEINDOLLARS,
                    "Y", // Checkbox should be checked when the value in
                    // database is Y
                    "N", // Checkbox should be unchecked when the value in
                    // database is N
                    false, // 
                    null);

            return child;

        }

//      ***** Change by NBC Impl. Team - Version 1.5 - End*****//

        else if (name.equals(CHILD_BTPROCEED)) {
            Button child = new Button(this, getDefaultModel(), CHILD_BTPROCEED,
                    CHILD_BTPROCEED, CHILD_BTPROCEED_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF1)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF1,
                    CHILD_HREF1, CHILD_HREF1_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF2)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF2,
                    CHILD_HREF2, CHILD_HREF2_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF3)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF3,
                    CHILD_HREF3, CHILD_HREF3_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF4)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF4,
                    CHILD_HREF4, CHILD_HREF4_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF5)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF5,
                    CHILD_HREF5, CHILD_HREF5_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF6)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF6,
                    CHILD_HREF6, CHILD_HREF6_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF7)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF7,
                    CHILD_HREF7, CHILD_HREF7_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF8)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF8,
                    CHILD_HREF8, CHILD_HREF8_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF9)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF9,
                    CHILD_HREF9, CHILD_HREF9_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HREF10)) {
            HREF child = new HREF(this, getDefaultModel(), CHILD_HREF10,
                    CHILD_HREF10, CHILD_HREF10_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPAGELABEL)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPAGELABEL, CHILD_STPAGELABEL,
                    CHILD_STPAGELABEL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCOMPANYNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STCOMPANYNAME,
                    CHILD_STCOMPANYNAME, CHILD_STCOMPANYNAME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STTODAYDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STTODAYDATE, CHILD_STTODAYDATE,
                    CHILD_STTODAYDATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUSERNAMETITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STUSERNAMETITLE,
                    CHILD_STUSERNAMETITLE, CHILD_STUSERNAMETITLE_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_BTWORKQUEUELINK)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTWORKQUEUELINK, CHILD_BTWORKQUEUELINK,
                    CHILD_BTWORKQUEUELINK_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CHANGEPASSWORDHREF)) {
            HREF child = new HREF(this, getDefaultModel(),
                    CHILD_CHANGEPASSWORDHREF, CHILD_CHANGEPASSWORDHREF,
                    CHILD_CHANGEPASSWORDHREF_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTTOOLHISTORY)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTTOOLHISTORY, CHILD_BTTOOLHISTORY,
                    CHILD_BTTOOLHISTORY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTTOONOTES)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTTOONOTES, CHILD_BTTOONOTES,
                    CHILD_BTTOONOTES_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTTOOLSEARCH)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTTOOLSEARCH, CHILD_BTTOOLSEARCH,
                    CHILD_BTTOOLSEARCH_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTTOOLLOG)) {
            Button child = new Button(this, getDefaultModel(), CHILD_BTTOOLLOG,
                    CHILD_BTTOOLLOG, CHILD_BTTOOLLOG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STERRORFLAG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STERRORFLAG, CHILD_STERRORFLAG,
                    CHILD_STERRORFLAG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_DETECTALERTTASKS)) {
            HiddenField child = new HiddenField(this, getDefaultModel(),
                    CHILD_DETECTALERTTASKS, CHILD_DETECTALERTTASKS,
                    CHILD_DETECTALERTTASKS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDDEALID)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDDEALID,
                    doUWDealSelectMainModel.FIELD_DFDEALID,
                    CHILD_HDDEALID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDDEALCOPYID)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDDEALCOPYID,
                    doUWDealSelectMainModel.FIELD_DFCOPYID,
                    CHILD_HDDEALCOPYID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDDEALINSTITUTIONID)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDDEALINSTITUTIONID,
                    doUWDealSelectMainModel.FIELD_DFINSTITUTIONID,
                    CHILD_HDDEALINSTITUTIONID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALID)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STDEALID,
                    doUWDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,
                    CHILD_STDEALID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STBORRFIRSTNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STBORRFIRSTNAME,
                    CHILD_STBORRFIRSTNAME, CHILD_STBORRFIRSTNAME_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_STDEALSTATUS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STDEALSTATUS,
                    doUWDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
                    CHILD_STDEALSTATUS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALSTATUSDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STDEALSTATUSDATE,
                    doUWDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
                    CHILD_STDEALSTATUSDATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSOURCEFIRM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STSOURCEFIRM,
                    doUWDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
                    CHILD_STSOURCEFIRM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSOURCE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STSOURCE,
                    doUWDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
                    CHILD_STSOURCE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALTYPE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STDEALTYPE,
                    doUWDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
                    CHILD_STDEALTYPE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STDEALPURPOSE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STDEALPURPOSE,
                    doUWDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
                    CHILD_STDEALPURPOSE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMTTERM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STPMTTERM,
                    doUWDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
                    CHILD_STPMTTERM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STESTCLOSINGDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STESTCLOSINGDATE,
                    doUWDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
                    CHILD_STESTCLOSINGDATE_RESET_VALUE, null);

            return child;
        } 


        else if (name.equals(CHILD_TXRATEGUARANTEEPERIOD)) {

            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXRATEGUARANTEEPERIOD,
                    doUWDealSelectMainModel.FIELD_DFRATEGUARANTEEPERIOD,
                    CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE, null);

            return child;
        } 

        // ***** Change by NBC Impl. Team - Version 1.2 - End*****//
        else if (name.equals(CHILD_STSPECIALFEATURE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STSPECIALFEATURE,
                    doUWDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
                    CHILD_STSPECIALFEATURE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSCENARIONO)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STSCENARIONO,
                    doUWDealSelectMainModel.FIELD_DFSCENARIONUMBER,
                    CHILD_STSCENARIONO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWSCENERIODESC)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWSCENERIODESC,

                    //--Release2.1--start//
                    ////doUWDealSelectMainModel.FIELD_DFSCENARIODESC,
                    doUWDealSelectMainModel.FIELD_DFSCENARIODESCTRANSLATED,
                    CHILD_TXUWSCENERIODESC_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STRECOEMENDSCENARIO)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STRECOEMENDSCENARIO,
                    doUWDealSelectMainModel.FIELD_DFSCENARIORECOMENDED,
                    CHILD_STRECOEMENDSCENARIO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STLOCKED)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STLOCKED,
                    doUWDealSelectMainModel.FIELD_DFSCENARIOLOCKED,
                    CHILD_STLOCKED_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBSCENARIOS)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBSCENARIOS, CHILD_CBSCENARIOS,
                    CHILD_CBSCENARIOS_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbScenariosOptions);

            return child;
        } else if (name.equals(CHILD_CBSCENARIOPICKLISTTOP)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBSCENARIOPICKLISTTOP, CHILD_CBSCENARIOPICKLISTTOP,
                    CHILD_CBSCENARIOPICKLISTTOP_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbScenarioPickListTopOptions);

            return child;
        } else if (name.equals(CHILD_BTPROCEEDTOCREATSCENARIOTOP)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTPROCEEDTOCREATSCENARIOTOP,
                    CHILD_BTPROCEEDTOCREATSCENARIOTOP,
                    CHILD_BTPROCEEDTOCREATSCENARIOTOP_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBSCENARIOPICKLISTBOTTOM)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBSCENARIOPICKLISTBOTTOM,
                    CHILD_CBSCENARIOPICKLISTBOTTOM,
                    CHILD_CBSCENARIOPICKLISTBOTTOM_RESET_VALUE, null);

            //--Release2.1--//
            //// "None selected" label should be eliminated.
            child.setLabelForNoneSelected("");
            child.setOptions(cbScenarioPickListBottomOptions);

            return child;
        } else if (name.equals(CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM,
                    CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM,
                    CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCOMBINEDGDS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STCOMBINEDGDS,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDGDS,
                    CHILD_STCOMBINEDGDS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCOMBINED3YRGDS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STCOMBINED3YRGDS,
                    doUWDealSelectMainModel.FIELD_DFCOMBINED3YEARGDS,
                    CHILD_STCOMBINED3YRGDS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCOMBINEDBORROWERGDS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STCOMBINEDBORROWERGDS,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDBORROWERGDS,
                    CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCOMBINEDTDS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STCOMBINEDTDS,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDTDS,
                    CHILD_STCOMBINEDTDS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCOMBINED3YRTDS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STCOMBINED3YRTDS,
                    doUWDealSelectMainModel.FIELD_DFCOMBINED3YEARTDS,
                    CHILD_STCOMBINED3YRTDS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCOMBINEDBORROWERTDS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STCOMBINEDBORROWERTDS,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDBORROWERTDS,
                    CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_REPEATGDSTDSDETAILS)) {
            pgUWorksheetRepeatGDSTDSDetailsTiledView child = new pgUWorksheetRepeatGDSTDSDetailsTiledView(
                    this, CHILD_REPEATGDSTDSDETAILS);

            return child;
        } else if (name.equals(CHILD_STUWPURCHASEPRICE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STUWPURCHASEPRICE,
                    doUWDealSelectMainModel.FIELD_DFTOTALPURCHASEPRICE,
                    CHILD_STUWPURCHASEPRICE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDUWPURCHASEPRICE)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDUWPURCHASEPRICE,
                    doUWDealSelectMainModel.FIELD_DFTOTALPURCHASEPRICE,
                    CHILD_HDUWPURCHASEPRICE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWCHARGE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWCHARGE,
                    doUWDealSelectMainModel.FIELD_DFLIENPOSITIONID,
                    CHILD_CBUWCHARGE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWChargeOptions);

            return child;
        } else if (name.equals(CHILD_STUWDOWNPAYMENT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STUWDOWNPAYMENT,
                    doUWDealSelectMainModel.FIELD_DFREQUIREDDOWNPAYMENT,
                    CHILD_STUWDOWNPAYMENT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWLOB)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWLOB, doUWDealSelectMainModel.FIELD_DFLOBID,
                    CHILD_CBUWLOB_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWLOBOptions);

            return child;
        } else if (name.equals(CHILD_CBUWLENDER)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWLENDER,
                    doUWDealSelectMainModel.FIELD_DFLENDERPROFILEID,
                    CHILD_CBUWLENDER_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWLenderOptions);

            return child;
        } else if (name.equals(CHILD_STUWBRIDGELOAN)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWBridgeModel(), CHILD_STUWBRIDGELOAN,
                    doUWBridgeModel.FIELD_DFNETLOANAMT,
                    CHILD_STUWBRIDGELOAN_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWPRODUCT)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWPRODUCT,
                    doUWDealSelectMainModel.FIELD_DFMTGPRODUCTID,
                    CHILD_CBUWPRODUCT_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWProductOptions);

            return child;
        } else if (name.equals(CHILD_TXUWLOANAMOUNT)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWLOANAMOUNT,
                    doUWDealSelectMainModel.FIELD_DFNETLOANAMOUNT,
                    CHILD_TXUWLOANAMOUNT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWLTV)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWLTV, doUWDealSelectMainModel.FIELD_DFCOMBINEDLTV,
                    CHILD_TXUWLTV_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDUWORIGLTV)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDUWORIGLTV,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDLTV,
                    CHILD_HDUWORIGLTV_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTRECALCULATELTV)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTRECALCULATELTV, CHILD_BTRECALCULATELTV,
                    CHILD_BTRECALCULATELTV_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWPOSTEDINTERESTRATE)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBUWPOSTEDINTERESTRATE, CHILD_CBUWPOSTEDINTERESTRATE,
                    CHILD_CBUWPOSTEDINTERESTRATE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWPostedInterestRateOptions);

            return child;
        } else if (name.equals(CHILD_STMIPREMIUM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STMIPREMIUM,
                    doUWDealSelectMainModel.FIELD_DFMIPREMIUMAMOUNT,
                    CHILD_STMIPREMIUM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWDISCOUNT)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWDISCOUNT,
                    doUWDealSelectMainModel.FIELD_DFDEALDISCOUNT,
                    CHILD_TXUWDISCOUNT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWTOTALLOAN)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STUWTOTALLOAN,
                    doUWDealSelectMainModel.FIELD_DFTOTALLOANAMOUNT,
                    CHILD_STUWTOTALLOAN_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWPREMIUM)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWPREMIUM,
                    doUWDealSelectMainModel.FIELD_DFDEALPREMIUM,
                    CHILD_TXUWPREMIUM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWAMORTIZATIOPERIODYEARS)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWAMORTIZATIOPERIODYEARS,
                    CHILD_TXUWAMORTIZATIOPERIODYEARS,
                    CHILD_TXUWAMORTIZATIOPERIODYEARS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWAMORTIZATIOPERIODMONTHS)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWAMORTIZATIOPERIODMONTHS,
                    CHILD_TXUWAMORTIZATIOPERIODMONTHS,
                    CHILD_TXUWAMORTIZATIOPERIODMONTHS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWBUYDOWN)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWBUYDOWN,
                    doUWDealSelectMainModel.FIELD_DFBUYDOWNRATE,
                    CHILD_TXUWBUYDOWN_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWNETRATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STUWNETRATE,
                    doUWDealSelectMainModel.FIELD_DFNETRATE,
                    CHILD_STUWNETRATE_RESET_VALUE, null);

            return child;
/*        } else if (name.equals(CHILD_STASTERIK)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STASTERIK,
                    CHILD_STASTERIK,
                    CHILD_STASTERIK_RESET_VALUE, null);

            return child;*/
        } else if (name.equals(CHILD_STUWPIPAYMENT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STUWPIPAYMENT,
                    doUWDealSelectMainModel.FIELD_DFPIPAYMENTAMOUNT,
                    CHILD_STUWPIPAYMENT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWACTUALPAYYEARS)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWACTUALPAYYEARS, CHILD_TXUWACTUALPAYYEARS,
                    CHILD_TXUWACTUALPAYYEARS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWACTUALPAYMONTHS)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWACTUALPAYMONTHS, CHILD_TXUWACTUALPAYMONTHS,
                    CHILD_TXUWACTUALPAYMONTHS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWADDITIONALPIPAY)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWADDITIONALPIPAY,
                    doUWDealSelectMainModel.FIELD_DFADDITIONALPRINCIPAL,
                    CHILD_TXUWADDITIONALPIPAY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWPAYMENTFREQUENCY)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWPAYMENTFREQUENCY,
                    doUWDealSelectMainModel.FIELD_DFPAYMENTFREQUENCYID,
                    CHILD_CBUWPAYMENTFREQUENCY_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWPaymentFrequencyOptions);

            return child;
        } else if (name.equals(CHILD_STUWTOTALESCROW)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STUWTOTALESCROW,
                    doUWDealSelectMainModel.FIELD_DFESCROWPAYMENTAMOUNT,
                    CHILD_STUWTOTALESCROW_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWPREPAYMENTPENALTY)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWPREPAYMENTPENALTY,
                    doUWDealSelectMainModel.FIELD_DFPREPAYMENTOPTIONID,
                    CHILD_CBUWPREPAYMENTPENALTY_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWPrePaymentPenaltyOptions);

            return child;
        } else if (name.equals(CHILD_STUWTOTALPAYMENT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STUWTOTALPAYMENT,
                    doUWDealSelectMainModel.FIELD_DFTOTALPAYMENTAMOUNT,
                    CHILD_STUWTOTALPAYMENT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWPRIVILEGEPAYMENTOPTION)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWPRIVILEGEPAYMENTOPTION,
                    doUWDealSelectMainModel.FIELD_DFPRIVILAGEPAYMENTID,
                    CHILD_CBUWPRIVILEGEPAYMENTOPTION_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWPrivilegePaymentOptionOptions);

            return child;
        } else if (name.equals(CHILD_TXUWADVANCEHOLD)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWADVANCEHOLD,
                    doUWDealSelectMainModel.FIELD_DFADVANCEHOLD,
                    CHILD_TXUWADVANCEHOLD_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWMAXIMUMPRINCIPALBALANCEALLOWED)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STUWMAXIMUMPRINCIPALBALANCEALLOWED,
                    doUWDealSelectMainModel.FIELD_DFMAXIMUMPRICIPALALLOWED,
                    CHILD_STUWMAXIMUMPRINCIPALBALANCEALLOWED_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWMINIMUMINCOMEREQUIRED)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STUWMINIMUMINCOMEREQUIRED,
                    doUWDealSelectMainModel.FIELD_DFMINIMUMINCOMEREQUIRED,
                    CHILD_STUWMINIMUMINCOMEREQUIRED_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWMAXIMUMTDSEXPENSEALLOWED)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STUWMAXIMUMTDSEXPENSEALLOWED,
                    doUWDealSelectMainModel.FIELD_DFMAXIMUMTDSEXPENSEALLOWED,
                    CHILD_STUWMAXIMUMTDSEXPENSEALLOWED_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWDEALPURPOSE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWDEALPURPOSE,
                    doUWDealSelectMainModel.FIELD_DFDEALPURPOSEID,
                    CHILD_CBUWDEALPURPOSE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWDealPurposeOptions);

            return child;
        } else if (name.equals(CHILD_CBUWBRANCH)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWBRANCH,
                    doUWDealSelectMainModel.FIELD_DFBRANCHPROFILEID,
                    CHILD_CBUWBRANCH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWBranchOptions);

            return child;
        } else if (name.equals(CHILD_CBUWDEALTYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWDEALTYPE,
                    doUWDealSelectMainModel.FIELD_DFDEALTYPEID,
                    CHILD_CBUWDEALTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWDealTypeOptions);

            return child;
        }
        // SEAN GECF IV Document type drop down. create the child.
        else if (name.equals(CHILD_CBIVDOCUMENTTYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBIVDOCUMENTTYPE,
                    doUWDealSelectMainModel.FIELD_DFIVDOCUMENTTYPEID,
                    CHILD_CBIVDOCUMENTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbIVDocumentTypeOptions);

            return child;
        } else if (name.equals(CHILD_STHIDEIVDOCUMENTTYPESTART)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHIDEIVDOCUMENTTYPESTART,
                    CHILD_STHIDEIVDOCUMENTTYPESTART,
                    CHILD_STHIDEIVDOCUMENTTYPESTART_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STHIDEIVDOCUMENTTYPEEND)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHIDEIVDOCUMENTTYPEEND,
                    CHILD_STHIDEIVDOCUMENTTYPEEND,
                    CHILD_STHIDEIVDOCUMENTTYPEEND_RESET_VALUE, null);

            return child;
        }
        // END GECF IV document type drop down.
        // SEAN DJ SPEC-Progress Advance Type July 20, 2005: Create the child...
        else if (name.equals(CHILD_CBPROGRESSADVANCETYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBPROGRESSADVANCETYPE,
                    doUWDealSelectMainModel.FIELD_DFPROGRESSADVANCETYPEID,
                    CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbProgressAdvanceTypeOptions);

            return child;
        }else if (name.equals(CHILD_STHIDEPROGRESSADVANCETYPESTART)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHIDEPROGRESSADVANCETYPESTART,
                    CHILD_STHIDEPROGRESSADVANCETYPESTART,
                    CHILD_STHIDEPROGRESSADVANCETYPESTART_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STHIDEPROGRESSADVANCETYPEEND)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHIDEPROGRESSADVANCETYPEEND,
                    CHILD_STHIDEPROGRESSADVANCETYPEEND,
                    CHILD_STHIDEPROGRESSADVANCETYPEEND_RESET_VALUE, null);

            return child;
        }
        // SEAN DJ SPEC-PAT END
        else if (name.equals(CHILD_CBUWCROSSSELL)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWCROSSSELL,
                    doUWDealSelectMainModel.FIELD_DFCROSSELLPROFILEID,
                    CHILD_CBUWCROSSSELL_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWCrossSellOptions);

            return child;
        } else if (name.equals(CHILD_CBUWSPECIALFEATURE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWSPECIALFEATURE,
                    doUWDealSelectMainModel.FIELD_DFSPECIALFEATUREID,
                    CHILD_CBUWSPECIALFEATURE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWSpecialFeatureOptions);

            return child;
        } else if (name.equals(CHILD_TXUWREFERENCEEXISTINGMTGNO)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFERENCEEXISTINGMTGNO,
                    doUWDealSelectMainModel.FIELD_DFEXISTINGMTGNUMBER,
                    CHILD_TXUWREFERENCEEXISTINGMTGNO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWRATELOCKEDIN)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWRATELOCKEDIN,
                    doUWDealSelectMainModel.FIELD_DFRATELOCK,
                    CHILD_CBUWRATELOCKEDIN_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWRateLockedInOptions);

            return child;
        } else if (name.equals(CHILD_CBUWREFERENCETYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWREFERENCETYPE,
                    doUWDealSelectMainModel.FIELD_DFREFERENCEDEALTYPEID,
                    CHILD_CBUWREFERENCETYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWReferenceTypeOptions);

            return child;
        } else if (name.equals(CHILD_CBUWREPAYMENTTYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWREPAYMENTTYPE,
                    doUWDealSelectMainModel.FIELD_DFREPAYMENTTYPEID,
                    CHILD_CBUWREPAYMENTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWRepaymentTypeOptions);

            return child;
        }
        /***************MCM Impl team changes starts - XS_2.43 *******************/
        else if (name.equals(CHILD_STDEFAULTREPAYMENTTYPEID))
        {
            StaticTextField child =
                new StaticTextField(this, getdoUWDealSelectMainModel(),
                        CHILD_STDEFAULTREPAYMENTTYPEID,
                        doUWDealSelectMainModel.FIELD_DFREPAYMENTTYPEID,
                        CHILD_STDEFAULTREPAYMENTTYPEID_RESET_VALUE, null);
            return child;
        
        /***************MCM Impl team changes ends - XS_2.43 *******************/
        } else if (name.equals(CHILD_TXUWREFERENCEDEALNO)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFERENCEDEALNO,
                    doUWDealSelectMainModel.FIELD_DFREFERENCEDEALNUMBER,
                    CHILD_TXUWREFERENCEDEALNO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWSECONDMORTGAGEEXIST)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWSECONDMORTGAGEEXIST,
                    doUWDealSelectMainModel.FIELD_DFSECONDARYFINANCING,
                    CHILD_CBUWSECONDMORTGAGEEXIST_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWSecondMortgageExistOptions);

            return child;
        } else if (name.equals(CHILD_CBUWESTIMATEDCLOSINGDATEMONTH)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBUWESTIMATEDCLOSINGDATEMONTH,
                    CHILD_CBUWESTIMATEDCLOSINGDATEMONTH,
                    CHILD_CBUWESTIMATEDCLOSINGDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWEstimatedClosingDateMonthOptions);

            return child;
        } else if (name.equals(CHILD_TXUWESTIMATEDCLOSINGDATEDAY)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWESTIMATEDCLOSINGDATEDAY,
                    CHILD_TXUWESTIMATEDCLOSINGDATEDAY,
                    CHILD_TXUWESTIMATEDCLOSINGDATEDAY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWESTIMATEDCLOSINGDATEYEAR)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWESTIMATEDCLOSINGDATEYEAR,
                    CHILD_TXUWESTIMATEDCLOSINGDATEYEAR,
                    CHILD_TXUWESTIMATEDCLOSINGDATEYEAR_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STINCLUDEFINANCINGPROGRAMSTART)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STINCLUDEFINANCINGPROGRAMSTART,
                    CHILD_STINCLUDEFINANCINGPROGRAMSTART,
                    CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWFINANCINGPROGRAM)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWFINANCINGPROGRAM,
                    doUWDealSelectMainModel.FIELD_DFFINANCEPROGRAMID,
                    CHILD_CBUWFINANCINGPROGRAM_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWFinancingProgramOptions);

            return child;
        } else if (name.equals(CHILD_STINCLUDEFINANCINGPROGRAMEND)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STINCLUDEFINANCINGPROGRAMEND,
                    CHILD_STINCLUDEFINANCINGPROGRAMEND,
                    CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWFIRSTPAYMETDATEMONTH)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBUWFIRSTPAYMETDATEMONTH,
                    CHILD_CBUWFIRSTPAYMETDATEMONTH,
                    CHILD_CBUWFIRSTPAYMETDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWFirstPaymetDateMonthOptions);

            return child;
        } else if (name.equals(CHILD_TXUWFIRSTPAYMETDATEDAY)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWFIRSTPAYMETDATEDAY, CHILD_TXUWFIRSTPAYMETDATEDAY,
                    CHILD_TXUWFIRSTPAYMETDATEDAY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWFIRSTPAYMETDATEYEAR)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWFIRSTPAYMETDATEYEAR,
                    CHILD_TXUWFIRSTPAYMETDATEYEAR,
                    CHILD_TXUWFIRSTPAYMETDATEYEAR_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWTAXPAYOR)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWTAXPAYOR,
                    doUWDealSelectMainModel.FIELD_DFDEALTAXPAYORID,
                    CHILD_CBUWTAXPAYOR_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWTaxPayorOptions);

            return child;
        } else if (name.equals(CHILD_STUWMATURITYDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STUWMATURITYDATE,
                    doUWDealSelectMainModel.FIELD_DFMATURITYID,
                    CHILD_STUWMATURITYDATE_RESET_VALUE, null);

            return child;
        }
        /*
        else if (name.equals(CHILD_STUWSOURCEFIRM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STUWSOURCEFIRM,
                    doUWSourceDetailsModel.FIELD_DFSOURCEFIRMNAME,
                    CHILD_STUWSOURCEFIRM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWSOURCEADDRESSLINE1)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STUWSOURCEADDRESSLINE1,
                    doUWSourceDetailsModel.FIELD_DFADDRESSLINE1,
                    CHILD_STUWSOURCEADDRESSLINE1_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWSOURCECITY)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STUWSOURCECITY,
                    doUWSourceDetailsModel.FIELD_DFCITY,
                    CHILD_STUWSOURCECITY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWSOURCEPROVINCE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STUWSOURCEPROVINCE,
                    doUWSourceDetailsModel.FIELD_DFPROVINCEABBR,
                    CHILD_STUWSOURCEPROVINCE_RESET_VALUE, null);

            return child;
        }
        // SEAN Ticket #1674 June 30, 2005: don't need the model.
        else if (name.equals(CHILD_STUWSOURCECONTACTPHONE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STUWSOURCECONTACTPHONE,
                    CHILD_STUWSOURCECONTACTPHONE,
                    CHILD_STUWSOURCECONTACTPHONE_RESET_VALUE, null);

            return child;
        }
        // SEAN Ticket #1674 END
        else if (name.equals(CHILD_STUWSOURCEFAX)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STUWSOURCEFAX,
                    doUWSourceDetailsModel.FIELD_DFCONTACTFAXNUMBER,
                    CHILD_STUWSOURCEFAX_RESET_VALUE, null);

            return child;
        }*/
        else if (name.equals(CHILD_REPEATAPPLICANTFINANCIALDETAILS)) {
            pgUWorksheetRepeatApplicantFinancialDetailsTiledView child = new pgUWorksheetRepeatApplicantFinancialDetailsTiledView(
                    this, CHILD_REPEATAPPLICANTFINANCIALDETAILS);

            return child;
        } else if (name.equals(CHILD_BTADDAPPLICANT)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTADDAPPLICANT, CHILD_BTADDAPPLICANT,
                    CHILD_BTADDAPPLICANT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTDUPAPPLICANT)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTDUPAPPLICANT, CHILD_BTDUPAPPLICANT,
                    CHILD_BTDUPAPPLICANT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTCREDITREPORT)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTCREDITREPORT, CHILD_BTCREDITREPORT,
                    CHILD_BTCREDITREPORT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWCOMBINEDTOTALINCOME)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STUWCOMBINEDTOTALINCOME,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDTOTALINCOME,
                    CHILD_STUWCOMBINEDTOTALINCOME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWCOMBINEDTOTALLIABILITIES)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STUWCOMBINEDTOTALLIABILITIES,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDTOTALLIABILITIES,
                    CHILD_STUWCOMBINEDTOTALLIABILITIES_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWTOTALNETWORTH)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STUWTOTALNETWORTH,
                    CHILD_STUWTOTALNETWORTH,
                    CHILD_STUWTOTALNETWORTH_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWCOMBINEDTOTALASSETS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STUWCOMBINEDTOTALASSETS,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDTOTALASSETS,
                    CHILD_STUWCOMBINEDTOTALASSETS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_REPEATPROPERTYINFORMATION)) {
            pgUWorksheetRepeatPropertyInformationTiledView child = new pgUWorksheetRepeatPropertyInformationTiledView(
                    this, CHILD_REPEATPROPERTYINFORMATION);

            return child;
        } else if (name.equals(CHILD_BTADDPROPERTY)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTADDPROPERTY, CHILD_BTADDPROPERTY,
                    CHILD_BTADDPROPERTY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STTOTALPROPERTYEXPENSES)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STTOTALPROPERTYEXPENSES,
                    doUWDealSelectMainModel.FIELD_DFCOMBAINEDTOTALPROPERTYEXPENCE,
                    CHILD_STTOTALPROPERTYEXPENSES_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTAPPRAISALREVIEW)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTAPPRAISALREVIEW, CHILD_BTAPPRAISALREVIEW,
                    CHILD_BTAPPRAISALREVIEW_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_REPEATEDUWDOWNPAYMENT)) {
            pgUWorksheetRepeatedUWDownPaymentTiledView child = new pgUWorksheetRepeatedUWDownPaymentTiledView(
                    this, CHILD_REPEATEDUWDOWNPAYMENT);

            return child;
        } else if (name.equals(CHILD_REPEATEDESCROWDETAILS)) {
            pgUWorksheetRepeatedEscrowDetailsTiledView child = new pgUWorksheetRepeatedEscrowDetailsTiledView(
                    this, CHILD_REPEATEDESCROWDETAILS);

            return child;
        } else if (name.equals(CHILD_STUWTARGETDOWNPAYMENT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STUWTARGETDOWNPAYMENT,
                    CHILD_STUWTARGETDOWNPAYMENT,
                    CHILD_STUWTARGETDOWNPAYMENT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUWDOWNPAYMENTREQUIRED)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STUWDOWNPAYMENTREQUIRED,
                    doUWDealSelectMainModel.FIELD_DFREQUIREDDOWNPAYMENT,
                    CHILD_STUWDOWNPAYMENTREQUIRED_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWTOTALDOWN)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWTOTALDOWN,
                    doUWDealSelectMainModel.FIELD_DFDEALDOWNPAYMENTAMOUNT,
                    CHILD_TXUWTOTALDOWN_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTADDDOWNPAYMENT)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTADDDOWNPAYMENT, CHILD_BTADDDOWNPAYMENT,
                    CHILD_BTADDDOWNPAYMENT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTADDESCROW)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTADDESCROW, CHILD_BTADDESCROW,
                    CHILD_BTADDESCROW_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTSAVESCENARIO)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTSAVESCENARIO, CHILD_BTSAVESCENARIO,
                    CHILD_BTSAVESCENARIO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTVERIFYSCENARIO)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTVERIFYSCENARIO, CHILD_BTVERIFYSCENARIO,
                    CHILD_BTVERIFYSCENARIO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTCONDITIONS)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTCONDITIONS, CHILD_BTCONDITIONS,
                    CHILD_BTCONDITIONS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTRESOLVEDEAL)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTRESOLVEDEAL, CHILD_BTRESOLVEDEAL,
                    CHILD_BTRESOLVEDEAL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTCOLLAPSEDENYDEAL)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTCOLLAPSEDENYDEAL, CHILD_BTCOLLAPSEDENYDEAL,
                    CHILD_BTCOLLAPSEDENYDEAL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTUWCANCEL)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTUWCANCEL, CHILD_BTUWCANCEL,
                    CHILD_BTUWCANCEL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTPREVTASKPAGE)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTPREVTASKPAGE, CHILD_BTPREVTASKPAGE,
                    CHILD_BTPREVTASKPAGE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPREVTASKPAGELABEL)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPREVTASKPAGELABEL,
                    CHILD_STPREVTASKPAGELABEL,
                    CHILD_STPREVTASKPAGELABEL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTNEXTTASKPAGE)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTNEXTTASKPAGE, CHILD_BTNEXTTASKPAGE,
                    CHILD_BTNEXTTASKPAGE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STNEXTTASKPAGELABEL)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STNEXTTASKPAGELABEL,
                    CHILD_STNEXTTASKPAGELABEL,
                    CHILD_STNEXTTASKPAGELABEL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STTASKNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STTASKNAME, CHILD_STTASKNAME,
                    CHILD_STTASKNAME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_SESSIONUSERID)) {
            HiddenField child = new HiddenField(this, getDefaultModel(),
                    CHILD_SESSIONUSERID, CHILD_SESSIONUSERID,
                    CHILD_SESSIONUSERID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMGENERATE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMGENERATE, CHILD_STPMGENERATE,
                    CHILD_STPMGENERATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMHASTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMHASTITLE, CHILD_STPMHASTITLE,
                    CHILD_STPMHASTITLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMHASINFO)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMHASINFO, CHILD_STPMHASINFO,
                    CHILD_STPMHASINFO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMHASTABLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMHASTABLE, CHILD_STPMHASTABLE,
                    CHILD_STPMHASTABLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMHASOK)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMHASOK, CHILD_STPMHASOK,
                    CHILD_STPMHASOK_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMTITLE, CHILD_STPMTITLE,
                    CHILD_STPMTITLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMINFOMSG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMINFOMSG, CHILD_STPMINFOMSG,
                    CHILD_STPMINFOMSG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMONOK)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMONOK, CHILD_STPMONOK,
                    CHILD_STPMONOK_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMMSGS)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMMSGS, CHILD_STPMMSGS,
                    CHILD_STPMMSGS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMMSGTYPES)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STPMMSGTYPES, CHILD_STPMMSGTYPES,
                    CHILD_STPMMSGTYPES_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMGENERATE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMGENERATE, CHILD_STAMGENERATE,
                    CHILD_STAMGENERATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMHASTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMHASTITLE, CHILD_STAMHASTITLE,
                    CHILD_STAMHASTITLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMHASINFO)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMHASINFO, CHILD_STAMHASINFO,
                    CHILD_STAMHASINFO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMHASTABLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMHASTABLE, CHILD_STAMHASTABLE,
                    CHILD_STAMHASTABLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMTITLE, CHILD_STAMTITLE,
                    CHILD_STAMTITLE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMINFOMSG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMINFOMSG, CHILD_STAMINFOMSG,
                    CHILD_STAMINFOMSG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMMSGS)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMMSGS, CHILD_STAMMSGS,
                    CHILD_STAMMSGS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMMSGTYPES)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMMSGTYPES, CHILD_STAMMSGTYPES,
                    CHILD_STAMMSGTYPES_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMDIALOGMSG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMDIALOGMSG,
                    CHILD_STAMDIALOGMSG, CHILD_STAMDIALOGMSG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAMBUTTONSHTML)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAMBUTTONSHTML,
                    CHILD_STAMBUTTONSHTML, CHILD_STAMBUTTONSHTML_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_BTFEEREVIEW)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTFEEREVIEW, CHILD_BTFEEREVIEW,
                    CHILD_BTFEEREVIEW_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTGOSCENARIO)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTGOSCENARIO, CHILD_BTGOSCENARIO,
                    CHILD_BTGOSCENARIO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTRECALCULATE)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTRECALCULATE, CHILD_BTRECALCULATE,
                    CHILD_BTRECALCULATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTCHANGESOURCE)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTCHANGESOURCE, CHILD_BTCHANGESOURCE,
                    CHILD_BTCHANGESOURCE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTBRIDGEREVIEW)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTBRIDGEREVIEW, CHILD_BTBRIDGEREVIEW,
                    CHILD_BTBRIDGEREVIEW_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STTARGETESCROW)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STTARGETESCROW,
                    CHILD_STTARGETESCROW, CHILD_STTARGETESCROW_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_HDLONGPOSTEDDATE)) {
            HiddenField child = new HiddenField(this, getDefaultModel(),
                    CHILD_HDLONGPOSTEDDATE, CHILD_HDLONGPOSTEDDATE,
                    CHILD_HDLONGPOSTEDDATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TBUWRATECODE)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TBUWRATECODE, CHILD_TBUWRATECODE,
                    CHILD_TBUWRATECODE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TBUWPAYMENTTERMDESCRIPTION)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TBUWPAYMENTTERMDESCRIPTION,
                    CHILD_TBUWPAYMENTTERMDESCRIPTION,
                    CHILD_TBUWPAYMENTTERMDESCRIPTION_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STLENDERPRODUCTID)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STLENDERPRODUCTID,
                    CHILD_STLENDERPRODUCTID,
                    CHILD_STLENDERPRODUCTID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STRATETIMESTAMP)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STRATETIMESTAMP,
                    CHILD_STRATETIMESTAMP, CHILD_STRATETIMESTAMP_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_STLENDERPROFILEID)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STLENDERPROFILEID,
                    CHILD_STLENDERPROFILEID,
                    CHILD_STLENDERPROFILEID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TBUWREFERENCESOURCEAPPLNO)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TBUWREFERENCESOURCEAPPLNO,
                    doUWDealSelectMainModel.FIELD_DFSOURCEAPPLICATIONID,
                    CHILD_TBUWREFERENCESOURCEAPPLNO_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TBUWESCROWPAYMENTTOTAL)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TBUWESCROWPAYMENTTOTAL,
                    doUWDealSelectMainModel.FIELD_DFESCROWPAYMENTAMOUNT,
                    CHILD_TBUWESCROWPAYMENTTOTAL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTMIREVIEW)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTMIREVIEW, CHILD_BTMIREVIEW,
                    CHILD_BTMIREVIEW_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_BTUWPARTYSUMMARY)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTUWPARTYSUMMARY, CHILD_BTUWPARTYSUMMARY,
                    CHILD_BTUWPARTYSUMMARY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STNEWSCENARIO1)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STNEWSCENARIO1,
                    CHILD_STNEWSCENARIO1, CHILD_STNEWSCENARIO1_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_STNEWSCENARIO2)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STNEWSCENARIO2,
                    CHILD_STNEWSCENARIO2, CHILD_STNEWSCENARIO2_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_BTOK)) {
            Button child = new Button(this, getDefaultModel(), CHILD_BTOK,
                    CHILD_BTOK, CHILD_BTOK_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWPREAPPESTPURCHASEPRICE)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWPREAPPESTPURCHASEPRICE,
                    doUWDealSelectMainModel.FIELD_DFPAPURCHASEPRICE,
                    CHILD_TXUWPREAPPESTPURCHASEPRICE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWMIINDICATOR)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWMIINDICATOR,
                    doUWDealSelectMainModel.FIELD_DFMIINDICATORID,
                    CHILD_CBUWMIINDICATOR_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWMIIndicatorOptions);

            return child;
        } else if (name.equals(CHILD_CBUWMITYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWMITYPE, doUWDealSelectMainModel.FIELD_DFMITYPEID,
                    CHILD_CBUWMITYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            //child.setOptions(cbUWMITypeOptions);

            return child;
        } else if (name.equals(CHILD_CBUWMIINSURER)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWMIINSURER,
                    doUWDealSelectMainModel.FIELD_DFMINSURERID,
                    CHILD_CBUWMIINSURER_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWMIInsurerOptions);

            return child;
        } else if (name.equals(CHILD_RBUWMIRUINTERVENTION)) {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoUWDealSelectMainModel(), CHILD_RBUWMIRUINTERVENTION,
                    doUWDealSelectMainModel.FIELD_DFMIRUINTERVENTION,
                    CHILD_RBUWMIRUINTERVENTION_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbUWMIRUInterventionOptions);

            return child;
        } else if (name.equals(CHILD_TXUWMIEXISTINGPOLICY)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWMIEXISTINGPOLICY,
                    doUWDealSelectMainModel.FIELD_DFMIEXISTINGPOLICYNUMBER,
                    CHILD_TXUWMIEXISTINGPOLICY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWMIPAYOR)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWMIPAYOR,
                    doUWDealSelectMainModel.FIELD_DFMIPAYORID,
                    CHILD_CBUWMIPAYOR_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWMIPayorOptions);

            return child;
        }

        /**
         * else if (name.equals(CHILD_STUWMIPREQCERTNUM)) { StaticTextField
         * child = new StaticTextField(this, getdoUWDealSelectMainModel(),
         * CHILD_STUWMIPREQCERTNUM,
         * doUWDealSelectMainModel.FIELD_DFPREQUALIFICATIONCERTNUM,
         * CHILD_STUWMIPREQCERTNUM_RESET_VALUE, null); return child; } else if
         * (name.equals(CHILD_STUWMISTATUS)) { StaticTextField child = new
         * StaticTextField(this, getdoUWMIStatusModel(), CHILD_STUWMISTATUS,
         * doUWMIStatusModel.FIELD_DFMISTATUSDESCRIPTION,
         * CHILD_STUWMISTATUS_RESET_VALUE, null); return child; }
         */
        else if (name.equals(CHILD_RBUWMIUPFRONT)) {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoUWDealSelectMainModel(), CHILD_RBUWMIUPFRONT,
                    doUWDealSelectMainModel.FIELD_DFMIUPFRONTID,
                    CHILD_RBUWMIUPFRONT_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbUWMIUpfrontOptions);

            return child;
        } else if (name.equals(CHILD_TXUWMIPREMIUM)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWMIPREMIUM,
                    doUWDealSelectMainModel.FIELD_DFMIPREMIUMAMOUNT,
                    CHILD_TXUWMIPREMIUM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWMICERTIFICATE)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWMICERTIFICATE,
                    doUWDealSelectMainModel.FIELD_DFMIPOLICYNUMBER,
                    CHILD_TXUWMICERTIFICATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS,
                    CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS,
                    CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS,
                    CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS,
                    CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STTOTALLOANAMOUNT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STTOTALLOANAMOUNT,
                    doUWDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
                    CHILD_STTOTALLOANAMOUNT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAPPDATEFORSERVLET)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STAPPDATEFORSERVLET,
                    CHILD_STAPPDATEFORSERVLET,
                    CHILD_STAPPDATEFORSERVLET_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STMIPREMIUMTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STMIPREMIUMTITLE,
                    CHILD_STMIPREMIUMTITLE, CHILD_STMIPREMIUMTITLE_RESET_VALUE,
                    null);

            return child;
        } else if (name.equals(CHILD_STINCLUDEMIPREMIUMSTART)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STINCLUDEMIPREMIUMSTART,
                    CHILD_STINCLUDEMIPREMIUMSTART,
                    CHILD_STINCLUDEMIPREMIUMSTART_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STINCLUDEMIPREMIUMEND)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STINCLUDEMIPREMIUMEND,
                    CHILD_STINCLUDEMIPREMIUMEND,
                    CHILD_STINCLUDEMIPREMIUMEND_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STINCLUDEMIPREMIUMVALUESTART)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STINCLUDEMIPREMIUMVALUESTART,
                    CHILD_STINCLUDEMIPREMIUMVALUESTART,
                    CHILD_STINCLUDEMIPREMIUMVALUESTART_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STINCLUDEMIPREMIUMVALUEEND)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STINCLUDEMIPREMIUMVALUEEND,
                    CHILD_STINCLUDEMIPREMIUMVALUEEND,
                    CHILD_STINCLUDEMIPREMIUMVALUEEND_RESET_VALUE, null);

            return child;
        }
        // SEAN Ticket #1674 June 30, 2005: don't need the model.
        else if (name.equals(CHILD_STUWPHONENOEXTENSION)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STUWPHONENOEXTENSION,
                    CHILD_STUWPHONENOEXTENSION,
                    CHILD_STUWPHONENOEXTENSION_RESET_VALUE, null);

            return child;
        }
        // SEAN Ticket #1674 END
        else if (name.equals(CHILD_STAPPLICATIONDATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(), CHILD_STAPPLICATIONDATE,
                    doUWDealSummarySnapShotModel.FIELD_DFAPPLICATIONDATE,
                    CHILD_STAPPLICATIONDATE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUNDERWRITERLASTNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STUNDERWRITERLASTNAME,
                    doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERLASTNAME,
                    CHILD_STUNDERWRITERLASTNAME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUNDERWRITERFIRSTNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STUNDERWRITERFIRSTNAME,
                    doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERFIRSTNAME,
                    CHILD_STUNDERWRITERFIRSTNAME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STUNDERWRITERMIDDLEINITIAL)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STUNDERWRITERMIDDLEINITIAL,
                    doUWDealSummarySnapShotModel.FIELD_DFUNDERWRITERMIDDLEINITIAL,
                    CHILD_STUNDERWRITERMIDDLEINITIAL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH,
                    CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH,
                    CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWCommitmentExpectedDateMonthOptions);

            return child;
        } else if (name.equals(CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR,
                    CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR,
                    CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY,
                    CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY,
                    CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY_RESET_VALUE, null);

            return child;
        } 

        /***** FFATE start *****/
        else if (name.equals(CHILD_CBUWFINANCINGWAIVERDATEMONTH)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBUWFINANCINGWAIVERDATEMONTH,
                    CHILD_CBUWFINANCINGWAIVERDATEMONTH,
                    CHILD_CBUWFINANCINGWAIVERDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWFinancingWaiverDateMonthOptions);
            return child;
        } else if (name.equals(CHILD_TXUWFINANCINGWAIVERDATEYEAR)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWFINANCINGWAIVERDATEYEAR,
                    CHILD_TXUWFINANCINGWAIVERDATEYEAR,
                    CHILD_TXUWFINANCINGWAIVERDATEYEAR_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWFINANCINGWAIVERDATEDAY)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWFINANCINGWAIVERDATEDAY,
                    CHILD_TXUWFINANCINGWAIVERDATEDAY,
                    CHILD_TXUWFINANCINGWAIVERDATEDAY_RESET_VALUE, null);
            return child;
        } 
        /***** FFATE end *****/
        
        //CR03
        else if (name.equals(CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH,
                    CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH,
                    CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWCommitmentExpirationDateMonthOptions);
            return child;
        } else if (name.equals(CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR,
                    CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR,
                    CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY,
                    CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY,
                    CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY_RESET_VALUE, null);
            return child;
        } 
        //CR03
        else if (name.equals(CHILD_STRATELOCK)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STRATELOCK, CHILD_STRATELOCK,
                    CHILD_STRATELOCK_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STVIEWONLYTAG)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STVIEWONLYTAG,
                    CHILD_STVIEWONLYTAG, CHILD_STVIEWONLYTAG_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSOURCEFIRSTNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STSOURCEFIRSTNAME,
                    doUWSourceDetailsModel.FIELD_DFCONTACTFIRSTNAME,
                    CHILD_STSOURCEFIRSTNAME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSOURCELASTNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STSOURCELASTNAME,
                    doUWSourceDetailsModel.FIELD_DFCONTACTLASTNAME,
                    CHILD_STSOURCELASTNAME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_RBPROGRESSADVANCE)) {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoUWDealSelectMainModel(), CHILD_RBPROGRESSADVANCE,
                    doUWDealSelectMainModel.FIELD_DFPROGRESSADVANCE,
                    CHILD_RBPROGRESSADVANCE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbProgressAdvanceOptions);

            return child;
        } else if (name.equals(CHILD_TXNEXTADVANCEAMOUNT)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXNEXTADVANCEAMOUNT,
                    doUWDealSelectMainModel.FIELD_DFNEXTADVANCEAMOUNT,
                    CHILD_TXNEXTADVANCEAMOUNT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXADVANCETODATEAMT)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXADVANCETODATEAMT,
                    doUWDealSelectMainModel.FIELD_DFADVANCETODATEAMOUNT,
                    CHILD_TXADVANCETODATEAMT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXADVANCENUMBER)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXADVANCENUMBER,
                    doUWDealSelectMainModel.FIELD_DFADVANCENUMBER,
                    CHILD_TXADVANCENUMBER_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBUWREFIORIGPURCHASEDATEMONTH)) {
            ComboBox child = new ComboBox(this, getDefaultModel(),
                    CHILD_CBUWREFIORIGPURCHASEDATEMONTH,
                    CHILD_CBUWREFIORIGPURCHASEDATEMONTH,
                    CHILD_CBUWREFIORIGPURCHASEDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWRefiOrigPurchaseDateMonthOptions);

            return child;
        } else if (name.equals(CHILD_TXUWREFIORIGPURCHASEDATEYEAR)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWREFIORIGPURCHASEDATEYEAR,
                    CHILD_TXUWREFIORIGPURCHASEDATEYEAR,
                    CHILD_TXUWREFIORIGPURCHASEDATEYEAR_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWREFIORIGPURCHASEDATEDAY)) {
            TextField child = new TextField(this, getDefaultModel(),
                    CHILD_TXUWREFIORIGPURCHASEDATEDAY,
                    CHILD_TXUWREFIORIGPURCHASEDATEDAY,
                    CHILD_TXUWREFIORIGPURCHASEDATEDAY_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWREFIPURPOSE)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFIPURPOSE,
                    doUWDealSelectMainModel.FIELD_DFREFIPURPOSE,
                    CHILD_TXUWREFIPURPOSE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWREFIMORTGAGEHOLDER)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFIMORTGAGEHOLDER,
                    doUWDealSelectMainModel.FIELD_DFREFICURMORTGAGEHOLDER,
                    CHILD_TXUWREFIMORTGAGEHOLDER_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_RBBLENDEDAMORTIZATION)) {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoUWDealSelectMainModel(), CHILD_RBBLENDEDAMORTIZATION,
                    doUWDealSelectMainModel.FIELD_DFREFIBLENDEDAMORTIZATION,
                    CHILD_RBBLENDEDAMORTIZATION_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbBlendedAmortizationOptions);

            return child;
        } else if (name.equals(CHILD_TXUWREFIORIGPURCHASEPRICE)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFIORIGPURCHASEPRICE,
                    doUWDealSelectMainModel.FIELD_DFREFIORIGPURCHASEPRICE,
                    CHILD_TXUWREFIORIGPURCHASEPRICE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWREFIIMPROVEMENTVALUE)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFIIMPROVEMENTVALUE,
                    doUWDealSelectMainModel.FIELD_DFREFIIMPROVEMENTVALUE,
                    CHILD_TXUWREFIIMPROVEMENTVALUE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWREFIORIGMTGAMOUNT)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFIORIGMTGAMOUNT,
                    doUWDealSelectMainModel.FIELD_DFREFIORIGMTGAMOUNT,
                    CHILD_TXUWREFIORIGMTGAMOUNT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT,
                    doUWDealSelectMainModel.FIELD_DFEXISTINGLOANAMOUNT,
                    CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWREFIIMPROVEMENTSDESC)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWREFIIMPROVEMENTSDESC,
                    doUWDealSelectMainModel.FIELD_DFREFIIMPROVEMENTSDESC,
                    CHILD_TXUWREFIIMPROVEMENTSDESC_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STVALSDATA)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STVALSDATA, CHILD_STVALSDATA,
                    CHILD_STVALSDATA_RESET_VALUE, null);

            return child;
        }

        //// Hidden button to trigger the ActiveMessage 'fake' submit button
        //// via the new BX ActMessageCommand class
        else if (name.equals(CHILD_BTACTMSG)) {
            Button child = new Button(this, getDefaultModel(), CHILD_BTACTMSG,
                    CHILD_BTACTMSG, CHILD_BTACTMSG_RESET_VALUE,
                    new CommandFieldDescriptor(
                            ActMessageCommand.COMMAND_DESCRIPTOR));

            return child;
        }

        //--Release2.1--//
        //// Link to toggle language. When touched this link should reload the
        //// page in opposite language (french versus english) and set this new  session
        //// language throughout the all modulus.
        else if (name.equals(CHILD_TOGGLELANGUAGEHREF)) {
            HREF child = new HREF(this, getDefaultModel(),
                    CHILD_TOGGLELANGUAGEHREF, CHILD_TOGGLELANGUAGEHREF,
                    CHILD_TOGGLELANGUAGEHREF_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STRATEINVENTORYID)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STRATEINVENTORYID,
                    CHILD_STRATEINVENTORYID,
                    CHILD_STRATEINVENTORYID_RESET_VALUE, null);

            return child;
        }

        //--Release2.1--//
        //// New HiddenField to get PaymentTermId from JavaScript Applet
        // companion functions
        //// and propagate it to the database on submit.
        else if (name.equals(CHILD_HDPAYMENTTERMID)) {
            HiddenField child = new HiddenField(this, getDefaultModel(),
                    CHILD_HDPAYMENTTERMID, CHILD_HDPAYMENTTERMID,
                    CHILD_HDPAYMENTTERMID_RESET_VALUE, null);

            return child;
        }

        //===============================================================
        // New fields to handle MIPolicyNum problem :
        //  the number lost if user cancel MI and re-send again later.
        // -- Modified by Billy 17July2003
        else if (name.equals(CHILD_HDMIPOLICYNOCMHC)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDMIPOLICYNOCMHC,
                    doUWDealSelectMainModel.FIELD_DFMIPOLICYNUMBERCMHC,
                    CHILD_HDMIPOLICYNOCMHC_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDMIPOLICYNOGE)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDMIPOLICYNOGE,
                    doUWDealSelectMainModel.FIELD_DFMIPOLICYNUMBERGE,
                    CHILD_HDMIPOLICYNOGE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDMIPOLICYNOAIGUG))
        {
            HiddenField child = new HiddenField(this, 
                    getdoUWDealSelectMainModel(), CHILD_HDMIPOLICYNOAIGUG,
                    doUWDealSelectMainModel.FIELD_DFMIPOLICYNUMBERAIGUG,
                    CHILD_HDMIPOLICYNOAIGUG_RESET_VALUE, null);

            return child;
        }
		else if (name.equals(CHILD_HDMIPOLICYNOPMI))
		{
			HiddenField child = new HiddenField(this, 
					getdoUWDealSelectMainModel(), CHILD_HDMIPOLICYNOPMI,
                        		doUWDealSelectMainModel.FIELD_DFMIPOLICYNUMBERPMI,
                        		CHILD_HDMIPOLICYNOPMI_RESET_VALUE, null);

      			return child;
		}

        //--BMO_MI_CR--start//
        //// This field should be editable under certain circustances (BMO
        // direct
        //// db update: MI process outside BXP.
        else if (name.equals(CHILD_CBUWMISTATUS)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBUWMISTATUS,
                    doUWDealSelectMainModel.FIELD_DFMISTATUS,
                    CHILD_CBUWMISTATUS_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbUWMIStatusOptions);

            return child;
        } else if (name.equals(CHILD_HDOUTSIDEXPRESS)) {
            HiddenField child = new HiddenField(this, getDefaultModel(),
                    CHILD_HDOUTSIDEXPRESS, CHILD_HDOUTSIDEXPRESS,
                    CHILD_HDOUTSIDEXPRESS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXUWMIPREQCERTNUM)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXUWMIPREQCERTNUM,
                    doUWDealSelectMainModel.FIELD_DFPREQUALIFICATIONCERTNUM,
                    CHILD_TXUWMIPREQCERTNUM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDMISTATUSID)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDMISTATUSID,
                    doUWDealSelectMainModel.FIELD_DFMISTATUS,
                    CHILD_HDMISTATUSID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDMIINDICATORID)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDMIINDICATORID,
                    doUWDealSelectMainModel.FIELD_DFMIINDICATORID,
                    CHILD_HDMIINDICATORID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDMIINSURER)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDMIINSURER,
                    doUWDealSelectMainModel.FIELD_DFMINSURERID,
                    CHILD_HDMIINSURER_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDMIPREMIUM)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDMIPREMIUM,
                    doUWDealSelectMainModel.FIELD_DFMIPREMIUMAMOUNT,
                    CHILD_HDMIPREMIUM_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDMIPREQCERTNUM)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDMIPREQCERTNUM,
                    doUWDealSelectMainModel.FIELD_DFPREQUALIFICATIONCERTNUM,
                    CHILD_HDMIPREQCERTNUM_RESET_VALUE, null);

            return child;
        }

        //--BMO_MI_CR--end//
        //=============================================================================
        //-- FXLink Phase II --//
        //--> New Field for Market Type Indicator
        //--> By Billy 18Nov2003
        else if (name.equals(CHILD_STMARKETTYPELABEL)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STMARKETTYPELABEL,
                    CHILD_STMARKETTYPELABEL,
                    CHILD_STMARKETTYPELABEL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STMARKETTYPE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STMARKETTYPE,
                    doUWDealSelectMainModel.FIELD_DFMCCMARKETTYPE,
                    CHILD_STMARKETTYPE_RESET_VALUE, null);

            return child;
        }

        //--DJ_PT_CR--start//
        else if (name.equals(CHILD_STBRANCHTRANSITLABEL)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STBRANCHTRANSITLABEL,
                    CHILD_STBRANCHTRANSITLABEL,
                    CHILD_STBRANCHTRANSITLABEL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPARTYNAME)) {
            StaticTextField child = new StaticTextField(this,
                    getdoPartySummaryModel(), CHILD_STPARTYNAME,
                    doPartySummaryModel.FIELD_DFPARTYNAME,
                    CHILD_STPARTYNAME_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STBRANCHTRANSITNUM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoPartySummaryModel(), CHILD_STBRANCHTRANSITNUM,
                    doPartySummaryModel.FIELD_DFTRANSITNUMBER,
                    CHILD_STBRANCHTRANSITNUM, null);

            return child;
        } else if (name.equals(CHILD_STADDRESSLINE1)) {
            StaticTextField child = new StaticTextField(this,
                    getdoPartySummaryModel(), CHILD_STADDRESSLINE1,
                    doPartySummaryModel.FIELD_DFADDRESSLINE1,
                    CHILD_STADDRESSLINE1, null);

            return child;
        } else if (name.equals(CHILD_STCITY)) {
            StaticTextField child = new StaticTextField(this,
                    getdoPartySummaryModel(), CHILD_STCITY,
                    doPartySummaryModel.FIELD_DFCITY, CHILD_STCITY, null);

            return child;
        }

        //--DJ_PT_CR--end//
        //--DJ_LDI_CR--start--//
        else if (name.equals(CHILD_BTLDINSURANCEDETAILS)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTLDINSURANCEDETAILS, CHILD_BTLDINSURANCEDETAILS,
                    CHILD_BTLDINSURANCEDETAILS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STINCLUDELIFEDISLABELSSTART)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STINCLUDELIFEDISLABELSSTART,
                    CHILD_STINCLUDELIFEDISLABELSSTART,
                    CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STINCLUDELIFEDISLABELSEND)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STINCLUDELIFEDISLABELSEND,
                    CHILD_STINCLUDELIFEDISLABELSEND,
                    CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STGDSTDSDETAILSLABEL)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STGDSTDSDETAILSLABEL,
                    CHILD_STGDSTDSDETAILSLABEL,
                    CHILD_STGDSTDSDETAILSLABEL_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPMNTINCLUDINGLIFEDISABILITY)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STPMNTINCLUDINGLIFEDISABILITY,
                    doUWDealSelectMainModel.FIELD_DFPMNTPLUSLIFEDISABILITY,
                    CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE, null);

            return child;
        }

        //--DJ_LDI_CR--end--//
        //--FX_LINK--start--//
        else if (name.equals(CHILD_BTSOURCEOFBUSINESSDETAILS)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTSOURCEOFBUSINESSDETAILS,
                    CHILD_BTSOURCEOFBUSINESSDETAILS,
                    CHILD_BTSOURCEOFBUSINESSDETAILS_RESET_VALUE, null);

            return child;
        }

        //--FX_LINK--end--//
        //--DJ_CR010--start//
        else if (name.equals(CHILD_TXCOMMISIONCODE)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXCOMMISIONCODE,
                    doUWDealSelectMainModel.FIELD_DFCOMMISIONCODE,
                    CHILD_TXCOMMISIONCODE_RESET_VALUE, null);

            return child;
        }

        //--DJ_CR010--end//
        //--DJ_CR203.1--start//
        else if (name.equals(CHILD_TXPROPRIETAIREPLUSLOC)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXPROPRIETAIREPLUSLOC,
                    doUWDealSelectMainModel.FIELD_DFPROPRIETAIREPLUSLOC,
                    CHILD_TXPROPRIETAIREPLUSLOC_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_RBMULTIPROJECT)) {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoUWDealSelectMainModel(), CHILD_RBMULTIPROJECT,
                    doUWDealSelectMainModel.FIELD_DFMULTIPROJECT,
                    CHILD_RBMULTIPROJECT_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbMultiProjectOptions);

            return child;
        } else if (name.equals(CHILD_RBPROPRIETAIREPLUS)) {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoUWDealSelectMainModel(), CHILD_RBPROPRIETAIREPLUS,
                    doUWDealSelectMainModel.FIELD_DFPROPRIETAIREPLUS,
                    CHILD_RBPROPRIETAIREPLUS_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbProprietairePlusOptions);

            return child;
        }

        //--DJ_CR203.1--end//
        //--DJ_CR134--start--27May2004--//
        else if (name.equals(CHILD_STHOMEBASEPRODUCTRATEPMNT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHOMEBASEPRODUCTRATEPMNT,
                    CHILD_STHOMEBASEPRODUCTRATEPMNT,
                    CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE, null);

            return child;
        }

        //--DJ_CR134--end--//
        //--DisableProductRateTicket#570--10Aug2004--start--//
        else if (name.equals(CHILD_STDEALRATESTATUSFORSERVLET)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STDEALRATESTATUSFORSERVLET,
                    CHILD_STDEALRATESTATUSFORSERVLET,
                    CHILD_STDEALRATESTATUSFORSERVLET_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STRATEDISDATEFORSERVLET)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STRATEDISDATEFORSERVLET,
                    CHILD_STRATEDISDATEFORSERVLET,
                    CHILD_STRATEDISDATEFORSERVLET, null);

            return child;
        } else if (name.equals(CHILD_STISPAGEEDITABLEFORSERVLET)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STISPAGEEDITABLEFORSERVLET,
                    CHILD_STISPAGEEDITABLEFORSERVLET,
                    CHILD_STISPAGEEDITABLEFORSERVLET, null);

            return child;
        }

        //--DisableProductRateTicket#570--10Aug2004--end--//
        //--Ticket#575--16Sep2004--start--//
        else if (name.equals(CHILD_HDINTERESTTYPEID)) {
            HiddenField child = new HiddenField(this, getDefaultModel(),
                    CHILD_HDINTERESTTYPEID, CHILD_HDINTERESTTYPEID,
                    CHILD_HDINTERESTTYPEID_RESET_VALUE, null);

            return child;
        }
        //--Ticket#575--16Sep2004--end--//
        //-- ========== DJ#725 Begins ========== --//
        //-- By Neil at Nov/29/2004
        else if (name.equals(CHILD_STCONTACTEMAILADDRESS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STCONTACTEMAILADDRESS,
                    doUWSourceDetailsModel.FIELD_DFCONTACTEMAILADDRESS,
                    CHILD_STCONTACTEMAILADDRESS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STPSDESCRIPTION)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STPSDESCRIPTION,
                    doUWSourceDetailsModel.FIELD_DFPSDESCRIPTION,
                    CHILD_STPSDESCRIPTION_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STSYSTEMTYPEDESCRIPTION)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWSourceDetailsModel(), CHILD_STSYSTEMTYPEDESCRIPTION,
                    doUWSourceDetailsModel.FIELD_DFSYSTEMTYPEDESCRIPTION,
                    CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE, null);

            return child;
        }
        //-- ========== DJ#725 Ends ========== --//

        //-- ========== SCR#859 begins ========== --//
        //-- by Neil on Feb 14, 2005
        else if (name.equals(CHILD_STSERVICINGMORTGAGENUMBER)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSummarySnapShotModel(),
                    CHILD_STSERVICINGMORTGAGENUMBER,
                    doUWDealSummarySnapShotModel.FIELD_DFSERVICINGMORTGAGENUMBER,
                    CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STCCAPS)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STCCAPS, CHILD_STCCAPS,
                    CHILD_STCCAPS_RESET_VALUE, null);
            return child;
        }
        //-- ========== SCR#859 ends ========== --//

        //--Release3.1--begins
        //--by Hiro Apr 13, 2006
        else if (name.equals(CHILD_CBPRODUCTTYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBPRODUCTTYPE,
                    doUWDealSelectMainModel.FIELD_DFPRODUCTTYPEID,
                    CHILD_CBPRODUCTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbProductTypeOptions);

            return child;
        } else if (name.equals(CHILD_CBREFIPRODUCTTYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBREFIPRODUCTTYPE,
                    doUWDealSelectMainModel.FIELD_DFREFIPRODUCTTYPEID,
                    CHILD_CBREFIPRODUCTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbRefiProductTypeOptions);

            return child;
        } else if (name.equals(CHILD_RBPROGRESSADVANCEINSPECTIONBY)) {
            RadioButtonGroup child = new RadioButtonGroup(
                    this,
                    getdoUWDealSelectMainModel(),
                    CHILD_RBPROGRESSADVANCEINSPECTIONBY,
                    doUWDealSelectMainModel.FIELD_DFPROGRESSADVANCEINSPECTIONBY,
                    CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbProgressAdvanceInspectionByOptions);

            return child;
        } else if (name.equals(CHILD_RBSELFDIRECTEDRRSP)) {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoUWDealSelectMainModel(), CHILD_RBSELFDIRECTEDRRSP,
                    doUWDealSelectMainModel.FIELD_DFSELFDIRECTEDRRSP,
                    CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbSelfDirectedRRSPOptions);

            return child;
        } else if (name.equals(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER)) {
            TextField child = new TextField(
                    this,
                    getdoUWDealSelectMainModel(),
                    CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER,
                    doUWDealSelectMainModel.FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER,
                    CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBLOCREPAYMENTTYPEID)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBLOCREPAYMENTTYPEID,
                    doUWDealSelectMainModel.FIELD_DFLOCREPAYMENTTYPEID,
                    CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbLOCRepaymentTypeOptions);

            return child;
        } else if (name.equals(CHILD_RBREQUESTSTANDARDSERVICE)) {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_RBREQUESTSTANDARDSERVICE,
                    doUWDealSelectMainModel.FIELD_DFREQUESTSTANDARDSERVICE,
                    CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbRequestStandardServiceOptions);

            return child;
        } else if (name.equals(CHILD_STLOCAMORTIZATIONMONTHS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STLOCAMORTIZATIONMONTHS,
                    doUWDealSelectMainModel.FIELD_DFLOCAMORTIZATIONMONTHS,
                    CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STLOCINTERESTONLYMATURITYDATE)) {
            StaticTextField child = new StaticTextField(
                    this,
                    getdoUWDealSelectMainModel(),
                    CHILD_STLOCINTERESTONLYMATURITYDATE,
                    doUWDealSelectMainModel.FIELD_DFLOCINTERESTONLYMATURITYDATE,
                    CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE, null);

            return child;
        }
//      CR-18
        else if (name.equals(CHILD_HDREQINPROGRESS))
        {
            HiddenField child = new HiddenField(this, CHILD_HDREQINPROGRESS,"");

            return child;
        }
        else if (name.equals(CHILD_HDREQINPROGRESS_FOR_ANOTHER_SCENARIO))
        {
            HiddenField child = new HiddenField(this, CHILD_HDREQINPROGRESS_FOR_ANOTHER_SCENARIO,"");

            return child;
        }
        // --Release3.1--ends

        //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
        else if (name.equals(CHILD_CBAFFILIATIONPROGRAM)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBAFFILIATIONPROGRAM,
                    doUWDealSelectMainModel.FIELD_DFAFFILIATIONPROGRAMID,
                    CHILD_CBAFFILIATIONPROGRAM_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbAffiliationProgramOptions);

            return child;
        }
        //  ***** Change by NBC Impl. Team - Version 1.3 - End *****//

        //***** Change by NBC/PP Implementation Team - GCD - Start *****//

        else if (name.equals(CHILD_TX_CREDIT_DECISION_STATUS)) {
            StaticTextField child =
                new StaticTextField(this, getdoAdjudicationResponseBNCModel(), CHILD_TX_CREDIT_DECISION_STATUS,
                        doAdjudicationResponseBNCModel.FIELD_DFREQUESTSTATUSDESC, CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE, null);
            return child;
        }

        else if (name.equals(CHILD_TX_CREDIT_DECISION)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(),CHILD_TX_CREDIT_DECISION,
                        doAdjudicationResponseBNCModel.FIELD_DFADJUDICATIONDESC,CHILD_TX_CREDIT_DECISION_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(), CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALCREDITBUREAUSCORE,CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_RISK_RATING)) {
            StaticTextField child =
                new StaticTextField(this, getdoAdjudicationResponseBNCModel(),CHILD_TX_GLOBAL_RISK_RATING,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALRISKRATING,CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_INTERNAL_SCORE)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(), CHILD_TX_GLOBAL_INTERNAL_SCORE,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALINTERNALSCORE, CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_MISTATUS)) {
            StaticTextField child =
                new StaticTextField(this, getdoDealMIStatusModel(), CHILD_TX_MISTATUS,
                        doDealMIStatusModel.FIELD_DFMISTATUSDESCRIPTION,CHILD_TX_MISTATUS_RESET_VALUE, null);
            return child;
        }

        //GCD Summary Data Retrieval End

        else if (name.equals(CHILD_BT_CREDIT_DECISION_PG)) {
            Button child =
                new Button (this,getDefaultModel(), CHILD_BT_CREDIT_DECISION_PG,
                        CHILD_BT_CREDIT_DECISION_PG,CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_ST_INCLUDE_GCDSUM_START)) {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_ST_INCLUDE_GCDSUM_START,
                        CHILD_ST_INCLUDE_GCDSUM_START,CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_ST_INCLUDE_GCDSUM_END)) {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_ST_INCLUDE_GCDSUM_END,
                        CHILD_ST_INCLUDE_GCDSUM_END,CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE, null);
            return child;
        }

        //***** Change by NBC/PP Implementation Team - GCD - End *****//


        //CR03      
        else if (name.equals(CHILD_RBAUTOCALCULATECOMMITMENTDATE))
        {
            RadioButtonGroup child = new RadioButtonGroup( this,
                    getdoUWDealSelectMainModel(), CHILD_RBAUTOCALCULATECOMMITMENTDATE,
                    doUWDealSelectMainModel.FIELD_DFAUTOCALCCOMMITEXPIRYDATE,
                    CHILD_RBAUTOCALCULATECOMMITMENTDATE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbAutoCalculateCommitmentDateOptions);
            return child;
        }

        else if (name.equals(CHILD_HDCOMMITMENTEXPIRYDATE)) {
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDCOMMITMENTEXPIRYDATE,
                    doUWDealSelectMainModel.FIELD_DFCOMMITMENTEXPIRATIONDATE,
                    CHILD_HDCOMMITMENTEXPIRYDATE_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_HDMOSPROPERTY)) {
            HiddenField child = new HiddenField(this,
                    getDefaultModel(), CHILD_HDMOSPROPERTY,
                    CHILD_HDMOSPROPERTY,
                    CHILD_HDMOSPROPERTY_RESET_VALUE, null);

            return child;
        }

        //CR03
        //Purchase Plus Improvements
        else if (name.equals(CHILD_TXUWIMPROVEDVALUE)) {
            StaticTextField child = new StaticTextField(this, getDefaultModel(),
                    CHILD_TXUWIMPROVEDVALUE,
                    CHILD_TXUWIMPROVEDVALUE,
                    CHILD_TXUWIMPROVEDVALUE_RESET_VALUE, null);
            return child;
        }

        else if (name.equals(CHILD_STDEALPURPOSETYPEHIDDENSTART)) {
            StaticTextField child = new StaticTextField(this, getDefaultModel(),
                    CHILD_STDEALPURPOSETYPEHIDDENSTART,
                    CHILD_STDEALPURPOSETYPEHIDDENSTART,
                    CHILD_STDEALPURPOSETYPEHIDDENSTART_RESET_VALUE, null);
            return child;
        }

        else if (name.equals(CHILD_STDEALPURPOSETYPEHIDDENEND)) {
            StaticTextField child = new StaticTextField(this, getDefaultModel(),
                    CHILD_STDEALPURPOSETYPEHIDDENEND,
                    CHILD_STDEALPURPOSETYPEHIDDENEND,
                    CHILD_STDEALPURPOSETYPEHIDDENEND_RESET_VALUE, null);
            return child;
        }
        //Purchase Plus Improvements
        /***************MCM Impl team changes starts - XS_2.46 *******************/

        else if (name.equals(CHILD_HDDEALSTATUSID))
        {
            TextField child =
                new TextField(this, getdoUWDealSelectMainModel(),
                		CHILD_HDDEALSTATUSID,
                        doUWDealSelectMainModel.FIELD_DFDEALSTATUSID,
                        CHILD_HDDEALSTATUSID_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_TXREFIADDITIONALINFORMATION))
        {
            TextField child =
                new TextField(this, getdoUWDealSelectMainModel(),
                        CHILD_TXREFIADDITIONALINFORMATION,
                        doUWDealSelectMainModel.FIELD_DFREFIADDITIONALINFORMATION,
                        CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE, null);

            return child;
        }

        /***************MCM Impl team changes starts - XS_2.46 *********************/
        else if (name.equals(CHILD_STQUALIFYINGDETAILSPAGELET))
        {
            pgQualifyingDetailsPageletView child = new pgQualifyingDetailsPageletView (this,CHILD_STQUALIFYINGDETAILSPAGELET,this.handler.cloneSS());
            return child;
        }
        /***************MCM Impl team changes ends - XS_2.46*******************/ 
        
        /***************MCM Impl team changes starts - XS_2.44 *********************/
        else if (name.equals(CHILD_COMPONENTINFOPAGELET))
        {
            //MCM team XS 2.44 modified STARTS
            pgComponentInfoPageletViewBean child = new pgComponentInfoPageletViewBean 
                (this, CHILD_COMPONENTINFOPAGELET, handler);
            // MCM team XS 2.55 modified ENDS
            return child;
        }
        /***************MCM Impl team changes ends - XS_2.4*******************/ 
        //4.4 Submission Agent
        else if (name.equals(CHILD_REPEATEDSOB))
        {
            pgUWRepeatedSOBTiledView child =
                new pgUWRepeatedSOBTiledView(this,
                		CHILD_REPEATEDSOB);

            return child;
        }
        //***** Qualifying Rate *****//
        else if (name.equals(CHILD_CBQUALIFYPRODUCTTYPE)) {
            ComboBox child = new ComboBox(this, getdoUWDealSelectMainModel(),
                    CHILD_CBQUALIFYPRODUCTTYPE,
                    doUWDealSelectMainModel.FIELD_DFOVERRIDEQUALPROD,
                    CHILD_CBQUALIFYPRODUCTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            return child;
        }
        else if (name.equals(CHILD_TXQUALIFYRATE)) {
            TextField child = new TextField(this, getdoUWDealSelectMainModel(),
                    CHILD_TXQUALIFYRATE,
                    doUWDealSelectMainModel.FIELD_DFQUALIFYRATE,
                    CHILD_TXQUALIFYRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CHQUALIFYRATEOVERRIDE)) {
            CheckBox child = new CheckBox(
                    this,
                    getdoUWDealSelectMainModel(),
                    CHILD_CHQUALIFYRATEOVERRIDE,
                    // CHILD_CHCASHBACKOVERRIDEINDOLLARS,
                    doUWDealSelectMainModel.FIELD_DFQUALIFYINGOVERRIDEFLAG,
                    "Y", // Checkbox should be checked when the value in
                    // database is Y
                    "N", // Checkbox should be unchecked when the value in
                    // database is N
                    false, // 
                    null);

            return child;
        }
        else if (name.equals(CHILD_CHQUALIFYRATEOVERRIDERATE)) {
            CheckBox child = new CheckBox(
                    this,
                    getdoUWDealSelectMainModel(),
                    CHILD_CHQUALIFYRATEOVERRIDERATE,
                    doUWDealSelectMainModel.FIELD_DFQUALIFYINGOVERRIDERATEFLAG,
                    "Y", // Checkbox should be checked when the value in
                    // database is Y
                    "N", // Checkbox should be unchecked when the value in
                    // database is N
                    false, // 
                    null);

            return child;
        }
        else if (name.equals(CHILD_BTRECALCULATEGDS)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTRECALCULATEGDS, CHILD_BTRECALCULATEGDS,
                    CHILD_BTRECALCULATEGDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STQUALIFYRATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STQUALIFYRATE,
                    doUWDealSelectMainModel.FIELD_DFQUALIFYRATE,
                    CHILD_STQUALIFYRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STQUALIFYPRODUCTTYPE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoUWDealSelectMainModel(), CHILD_STQUALIFYPRODUCTTYPE,
                    doUWDealSelectMainModel.FIELD_DFOVERRIDEQUALPROD,
                    CHILD_STQUALIFYPRODUCTTYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STQUALIFYRATEHIDDEN)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STQUALIFYRATEHIDDEN,
                    CHILD_STQUALIFYRATEHIDDEN,
                    CHILD_STQUALIFYRATEHIDDEN_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STQUALIFYRATEEDIT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STQUALIFYRATEEDIT,
                    CHILD_STQUALIFYRATEEDIT,
                    CHILD_STQUALIFYRATEEDIT_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STHDQUALIFYRATE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHDQUALIFYRATE,
                    CHILD_STHDQUALIFYRATE,
                    CHILD_STHDQUALIFYRATE_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STHDQUALIFYPRODUCTID)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHDQUALIFYPRODUCTID,
                    CHILD_STHDQUALIFYPRODUCTID,
                    CHILD_STHDQUALIFYPRODUCTID_RESET_VALUE, null);
            return child;
        }
        // ***** Qualifying Rate *****//
        else if (name.equals(CHILD_HDQUALIFYRATEDISABLED)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_HDQUALIFYRATEDISABLED,
                    CHILD_HDQUALIFYRATEDISABLED,
                    CHILD_HDQUALIFYRATEDISABLED_RESET_VALUE, null);
            return child;
        }
        
        //5.0 MI -- start
        else if (name.equals(CHILD_TXMIRESPONSE_FIRSTLIEN)) {
            TextField child = new TextField(this, 
                    //getdoUWDealSelectMainModel(),
                    getDefaultModel(),
            		CHILD_TXMIRESPONSE_FIRSTLIEN,
                    //doUWDealSelectMainModel.FIELD_DFMIRESPONSE_FIRSTLINE,
            		CHILD_TXMIRESPONSE_FIRSTLIEN,
                    CHILD_TXMIRESPONSE_FIRSTLIEN_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_BTEXPAND_MIRESPONSE)) {
            Button child = new Button(this, 
                    getDefaultModel(),
                    CHILD_BTEXPAND_MIRESPONSE, 
                    CHILD_BTEXPAND_MIRESPONSE,
                    CHILD_BTEXPAND_MIRESPONSE_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_HDUWCOMBINEDLENDINGVALUE)) { //QC702
            HiddenField child = new HiddenField(this,
                    getdoUWDealSelectMainModel(), CHILD_HDUWCOMBINEDLENDINGVALUE,
                    doUWDealSelectMainModel.FIELD_DFCOMBINEDLENDINGVALUE,
                    CHILD_HDUWCOMBINEDLENDINGVALUE_RESET_VALUE, null);

            return child;
        }
        //5.0 MI -- end
        
        else {
            throw new IllegalArgumentException("Invalid child name [" + name
                    + "]");
        }
    }

    /**
     * resetChildren()
     * 
     * 
     * @param <br>
     *  
     * @return void :
     * @version 1.2 <br>
     *          Date: 06/26/006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: <br>
     *          added
     *          getTxRateGuaranteePeriod().setValue(CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE);
     *          
     * @version 55 
     * Date: 11/20/2006 
     * Author: NBC/PP Implementation Team 
     * added reset child for
     * CHILD_HDFORCEPROGRESSADVANCE
     * 
     *  @Version 1.3 <br>
     *  Date: 06/09/2008
     *  Author: MCM Impl Team <br>
     *  Change Log:  <br>
     *  XS_2.46 -- 06/06/2008 -- Added necessary changes required for the new fields added in Refinance section<br>
     */
    public void resetChildren() {

        super.resetChildren();

        // ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
        getTxRateGuaranteePeriod().setValue(
                CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE);
        // ***** Change by NBC Impl. Team - Version 1.2 - End*****//


//      ***** Change by NBC Impl. Team - Version 1.5 - Start *****//

        getChCashBackOverrideInDollars().setValue(
                CHILD_CHCASHBACKOVERRIDEINDOLLARS_RESET_VALUE);
        getTxCashBackInPercentage().setValue(
                CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE);
        getTxCashBackInDollars()
        .setValue(CHILD_TXCASHBACKINDOLLARS_RESET_VALUE);


//      ***** Change by NBC Impl. Team - Version 1.5 - End*****//

        // ***** Change by NBC/PP Implementation Team - GCD - Start *****//

        getTxCreditDecisionStatusChild().setValue(CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE);
        getTxCreditDecisionChild().setValue(CHILD_TX_CREDIT_DECISION_RESET_VALUE);
        getTxGlobalCreditBureauScoreChild().setValue(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE);
        getTxGlobalRiskRatingChild().setValue(CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE);
        getTxGlobalInternalScoreChild().setValue(CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE);
        getTxMIStatusChild().setValue(CHILD_TX_MISTATUS_RESET_VALUE);
        getBtCreditDecisionPGChild().setValue(CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE);
        getStIncludeGCDSumStartChild().setValue(CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE);
        getStIncludeGCDSumEndChild().setValue(CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE);

        //***** Change by NBC/PP Implementation Team - GCD - End *****//

        getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
        getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
        getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
        getHref1().setValue(CHILD_HREF1_RESET_VALUE);
        getHref2().setValue(CHILD_HREF2_RESET_VALUE);
        getHref3().setValue(CHILD_HREF3_RESET_VALUE);
        getHref4().setValue(CHILD_HREF4_RESET_VALUE);
        getHref5().setValue(CHILD_HREF5_RESET_VALUE);
        getHref6().setValue(CHILD_HREF6_RESET_VALUE);
        getHref7().setValue(CHILD_HREF7_RESET_VALUE);
        getHref8().setValue(CHILD_HREF8_RESET_VALUE);
        getHref9().setValue(CHILD_HREF9_RESET_VALUE);
        getHref10().setValue(CHILD_HREF10_RESET_VALUE);
        getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
        getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
        getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
        getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
        getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
        getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
        getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
        getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
        getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
        getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
        getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
        getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
        getHdDealId().setValue(CHILD_HDDEALID_RESET_VALUE);
        getHdDealCopyId().setValue(CHILD_HDDEALCOPYID_RESET_VALUE);
        getHdDealInstituionId().setValue(CHILD_HDDEALINSTITUTIONID_RESET_VALUE);
        getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
        getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
        getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
        getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
        getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
        getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
        getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
        getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
        getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
        getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
        getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
        getStScenarioNo().setValue(CHILD_STSCENARIONO_RESET_VALUE);
        getTxUWScenerioDesc().setValue(CHILD_TXUWSCENERIODESC_RESET_VALUE);
        getStRecoemendScenario().setValue(CHILD_STRECOEMENDSCENARIO_RESET_VALUE);
        getStLocked().setValue(CHILD_STLOCKED_RESET_VALUE);
        getCbScenarios().setValue(CHILD_CBSCENARIOS_RESET_VALUE);
        getCbScenarioPickListTop().setValue(
                CHILD_CBSCENARIOPICKLISTTOP_RESET_VALUE);
        getBtProceedToCreatScenarioTop().setValue(
                CHILD_BTPROCEEDTOCREATSCENARIOTOP_RESET_VALUE);
        getCbScenarioPickListBottom().setValue(
                CHILD_CBSCENARIOPICKLISTBOTTOM_RESET_VALUE);
        getBtProceedToCreateScenarioBottom().setValue(
                CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM_RESET_VALUE);
        getStCombinedGDS().setValue(CHILD_STCOMBINEDGDS_RESET_VALUE);
        getStCombined3YrGDS().setValue(CHILD_STCOMBINED3YRGDS_RESET_VALUE);
        getStCombinedBorrowerGDS().setValue(CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE);
        getStCombinedTDS().setValue(CHILD_STCOMBINEDTDS_RESET_VALUE);
        getStCombined3YrTDS().setValue(CHILD_STCOMBINED3YRTDS_RESET_VALUE);
        getStCombinedBorrowerTDS().setValue(CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE);
        getRepeatGDSTDSDetails().resetChildren();
        getStUWPurchasePrice().setValue(CHILD_STUWPURCHASEPRICE_RESET_VALUE);
        getHdUWPurchasePrice().setValue(CHILD_HDUWPURCHASEPRICE_RESET_VALUE);
        getCbUWCharge().setValue(CHILD_CBUWCHARGE_RESET_VALUE);
        getStUWDownPayment().setValue(CHILD_STUWDOWNPAYMENT_RESET_VALUE);
        getCbUWLOB().setValue(CHILD_CBUWLOB_RESET_VALUE);
        getCbUWLender().setValue(CHILD_CBUWLENDER_RESET_VALUE);
        getStUWBridgeLoan().setValue(CHILD_STUWBRIDGELOAN_RESET_VALUE);
        getCbUWProduct().setValue(CHILD_CBUWPRODUCT_RESET_VALUE);
        getTxUWLoanAmount().setValue(CHILD_TXUWLOANAMOUNT_RESET_VALUE);
        getTxUWLTV().setValue(CHILD_TXUWLTV_RESET_VALUE);
        getHdUWOrigLTV().setValue(CHILD_HDUWORIGLTV_RESET_VALUE);
        getBtRecalculateLTV().setValue(CHILD_BTRECALCULATELTV_RESET_VALUE);
        getCbUWPostedInterestRate().setValue(CHILD_CBUWPOSTEDINTERESTRATE_RESET_VALUE);
        getStMIPremium().setValue(CHILD_STMIPREMIUM_RESET_VALUE);
        getTxUWDiscount().setValue(CHILD_TXUWDISCOUNT_RESET_VALUE);
        getStUWTotalLoan().setValue(CHILD_STUWTOTALLOAN_RESET_VALUE);
        getTxUWPremium().setValue(CHILD_TXUWPREMIUM_RESET_VALUE);
        getTxUWAmortizatioPeriodYears().setValue(CHILD_TXUWAMORTIZATIOPERIODYEARS_RESET_VALUE);
        getTxUWAmortizatioPeriodMonths().setValue(CHILD_TXUWAMORTIZATIOPERIODMONTHS_RESET_VALUE);
        getTxUWBuyDown().setValue(CHILD_TXUWBUYDOWN_RESET_VALUE);
        getStUWNetRate().setValue(CHILD_STUWNETRATE_RESET_VALUE);
//        getStAsterik().setValue(CHILD_STASTERIK_RESET_VALUE);
        getStUWPIPayment().setValue(CHILD_STUWPIPAYMENT_RESET_VALUE);
        getTxUWActualPayYears().setValue(CHILD_TXUWACTUALPAYYEARS_RESET_VALUE);
        getTxUWActualPayMonths()
        .setValue(CHILD_TXUWACTUALPAYMONTHS_RESET_VALUE);
        getTxUWAdditionalPIPay()
        .setValue(CHILD_TXUWADDITIONALPIPAY_RESET_VALUE);
        getCbUWPaymentFrequency().setValue(
                CHILD_CBUWPAYMENTFREQUENCY_RESET_VALUE);
        getStUWTotalEscrow().setValue(CHILD_STUWTOTALESCROW_RESET_VALUE);
        getCbUWPrePaymentPenalty().setValue(
                CHILD_CBUWPREPAYMENTPENALTY_RESET_VALUE);
        getStUWTotalPayment().setValue(CHILD_STUWTOTALPAYMENT_RESET_VALUE);
        getCbUWPrivilegePaymentOption().setValue(
                CHILD_CBUWPRIVILEGEPAYMENTOPTION_RESET_VALUE);
        getTxUWAdvanceHold().setValue(CHILD_TXUWADVANCEHOLD_RESET_VALUE);
        getStUWMaximumPrincipalBalanceAllowed().setValue(
                CHILD_STUWMAXIMUMPRINCIPALBALANCEALLOWED_RESET_VALUE);
        getStUWMinimumIncomeRequired().setValue(
                CHILD_STUWMINIMUMINCOMEREQUIRED_RESET_VALUE);
        getStUWMaximumTDSExpenseAllowed().setValue(
                CHILD_STUWMAXIMUMTDSEXPENSEALLOWED_RESET_VALUE);
        getCbUWDealPurpose().setValue(CHILD_CBUWDEALPURPOSE_RESET_VALUE);
        getCbUWBranch().setValue(CHILD_CBUWBRANCH_RESET_VALUE);
        getCbUWDealType().setValue(CHILD_CBUWDEALTYPE_RESET_VALUE);
        // SEAN GECF IV document type drop down. reset value.
        getCbIVDocumentType().setValue(CHILD_CBIVDOCUMENTTYPE_RESET_VALUE);
        getStHideIVDocumentTypeStart().setValue(
                CHILD_STHIDEIVDOCUMENTTYPESTART_RESET_VALUE);
        getStHideIVDocumentTypeEnd().setValue(
                CHILD_STHIDEIVDOCUMENTTYPEEND_RESET_VALUE);
        // END GECF IV document type drop down.
        // SEAN DJ SPEC-Progress Advance Type July 20, 2005: Reset value.
        getCbProgressAdvanceType().setValue(
                CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE);
        getStHideProgressAdvanceTypeStart().setValue(
                CHILD_STHIDEPROGRESSADVANCETYPESTART_RESET_VALUE);
        getStHideProgressAdvanceTypeEnd().setValue(
                CHILD_STHIDEPROGRESSADVANCETYPEEND_RESET_VALUE);
        // SEAN DJ SPEC-PAT END
        getCbUWCrossSell().setValue(CHILD_CBUWCROSSSELL_RESET_VALUE);
        getCbUWSpecialFeature().setValue(CHILD_CBUWSPECIALFEATURE_RESET_VALUE);
        getTxUWReferenceExistingMTGNo().setValue(
                CHILD_TXUWREFERENCEEXISTINGMTGNO_RESET_VALUE);
        getCbUWRateLockedIn().setValue(CHILD_CBUWRATELOCKEDIN_RESET_VALUE);
        getCbUWReferenceType().setValue(CHILD_CBUWREFERENCETYPE_RESET_VALUE);
        getCbUWRepaymentType().setValue(CHILD_CBUWREPAYMENTTYPE_RESET_VALUE);
        /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
        getStDefaultRepaymentTypeId().setValue(CHILD_STDEFAULTREPAYMENTTYPEID_RESET_VALUE);
        /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
        getTxUWReferenceDealNo()
        .setValue(CHILD_TXUWREFERENCEDEALNO_RESET_VALUE);
        getCbUWSecondMortgageExist().setValue(
                CHILD_CBUWSECONDMORTGAGEEXIST_RESET_VALUE);
        getCbUWEstimatedClosingDateMonth().setValue(
                CHILD_CBUWESTIMATEDCLOSINGDATEMONTH_RESET_VALUE);
        getTxUWEstimatedClosingDateDay().setValue(
                CHILD_TXUWESTIMATEDCLOSINGDATEDAY_RESET_VALUE);
        getTxUWEstimatedClosingDateYear().setValue(
                CHILD_TXUWESTIMATEDCLOSINGDATEYEAR_RESET_VALUE);
        getStIncludeFinancingProgramStart().setValue(
                CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE);
        getCbUWFinancingProgram().setValue(
                CHILD_CBUWFINANCINGPROGRAM_RESET_VALUE);
        getStIncludeFinancingProgramEnd().setValue(
                CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE);
        getCbUWFirstPaymetDateMonth().setValue(
                CHILD_CBUWFIRSTPAYMETDATEMONTH_RESET_VALUE);
        getTxUWFirstPaymetDateDay().setValue(
                CHILD_TXUWFIRSTPAYMETDATEDAY_RESET_VALUE);
        getTxUWFirstPaymetDateYear().setValue(
                CHILD_TXUWFIRSTPAYMETDATEYEAR_RESET_VALUE);
        getCbUWTaxPayor().setValue(CHILD_CBUWTAXPAYOR_RESET_VALUE);
        getStUWMaturityDate().setValue(CHILD_STUWMATURITYDATE_RESET_VALUE);
/*        getStUWSourceFirm().setValue(CHILD_STUWSOURCEFIRM_RESET_VALUE);
        getStUWSourceAddressLine1().setValue(
                CHILD_STUWSOURCEADDRESSLINE1_RESET_VALUE);
        getStUWSourceCity().setValue(CHILD_STUWSOURCECITY_RESET_VALUE);
        getStUWSourceProvince().setValue(CHILD_STUWSOURCEPROVINCE_RESET_VALUE);
        getStUWSourceContactPhone().setValue(
                CHILD_STUWSOURCECONTACTPHONE_RESET_VALUE);
        getStUWSourceFax().setValue(CHILD_STUWSOURCEFAX_RESET_VALUE);*/
        getRepeatApplicantFinancialDetails().resetChildren();
        getBtAddApplicant().setValue(CHILD_BTADDAPPLICANT_RESET_VALUE);
        getBtDupApplicant().setValue(CHILD_BTDUPAPPLICANT_RESET_VALUE);
        getBtCreditReport().setValue(CHILD_BTCREDITREPORT_RESET_VALUE);
        getStUWCombinedTotalIncome().setValue(
                CHILD_STUWCOMBINEDTOTALINCOME_RESET_VALUE);
        getStUWCombinedTotalLiabilities().setValue(
                CHILD_STUWCOMBINEDTOTALLIABILITIES_RESET_VALUE);
        getStUWTotalNetWorth().setValue(CHILD_STUWTOTALNETWORTH_RESET_VALUE);
        getStUWCombinedTotalAssets().setValue(
                CHILD_STUWCOMBINEDTOTALASSETS_RESET_VALUE);
        getRepeatPropertyInformation().resetChildren();
        getBtAddProperty().setValue(CHILD_BTADDPROPERTY_RESET_VALUE);
        getStTotalPropertyExpenses().setValue(
                CHILD_STTOTALPROPERTYEXPENSES_RESET_VALUE);
        getBtAppraisalReview().setValue(CHILD_BTAPPRAISALREVIEW_RESET_VALUE);
        getRepeatedUWDownPayment().resetChildren();
        getRepeatedEscrowDetails().resetChildren();
        getStUWTargetDownPayment().setValue(
                CHILD_STUWTARGETDOWNPAYMENT_RESET_VALUE);
        getStUWDownPaymentRequired().setValue(
                CHILD_STUWDOWNPAYMENTREQUIRED_RESET_VALUE);
        getTxUWTotalDown().setValue(CHILD_TXUWTOTALDOWN_RESET_VALUE);
        getBtAddDownPayment().setValue(CHILD_BTADDDOWNPAYMENT_RESET_VALUE);
        getBtAddEscrow().setValue(CHILD_BTADDESCROW_RESET_VALUE);
        getBtSaveScenario().setValue(CHILD_BTSAVESCENARIO_RESET_VALUE);
        getBtVerifyScenario().setValue(CHILD_BTVERIFYSCENARIO_RESET_VALUE);
        getBtConditions().setValue(CHILD_BTCONDITIONS_RESET_VALUE);
        getBtResolveDeal().setValue(CHILD_BTRESOLVEDEAL_RESET_VALUE);
        getBtCollapseDenyDeal().setValue(CHILD_BTCOLLAPSEDENYDEAL_RESET_VALUE);
        getBtUWCancel().setValue(CHILD_BTUWCANCEL_RESET_VALUE);
        getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
        getStPrevTaskPageLabel()
        .setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
        getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
        getStNextTaskPageLabel()
        .setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
        getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
        getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
        getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
        getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
        getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
        getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
        getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
        getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
        getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
        getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
        getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
        getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
        getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
        getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
        getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
        getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
        getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
        getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
        getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
        getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
        getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
        getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
        getBtFeeReview().setValue(CHILD_BTFEEREVIEW_RESET_VALUE);
        getBtGoScenario().setValue(CHILD_BTGOSCENARIO_RESET_VALUE);
        getBtRecalculate().setValue(CHILD_BTRECALCULATE_RESET_VALUE);
        getBtChangeSource().setValue(CHILD_BTCHANGESOURCE_RESET_VALUE);
        getBtBridgeReview().setValue(CHILD_BTBRIDGEREVIEW_RESET_VALUE);
        getStTargetEscrow().setValue(CHILD_STTARGETESCROW_RESET_VALUE);
        getHdLongPostedDate().setValue(CHILD_HDLONGPOSTEDDATE_RESET_VALUE);
        getTbUWRateCode().setValue(CHILD_TBUWRATECODE_RESET_VALUE);
        getTbUWPaymentTermDescription().setValue(
                CHILD_TBUWPAYMENTTERMDESCRIPTION_RESET_VALUE);
        getStLenderProductId().setValue(CHILD_STLENDERPRODUCTID_RESET_VALUE);
        getStRateTimeStamp().setValue(CHILD_STRATETIMESTAMP_RESET_VALUE);
        getStLenderProfileId().setValue(CHILD_STLENDERPROFILEID_RESET_VALUE);
        getTbUWReferenceSourceApplNo().setValue(
                CHILD_TBUWREFERENCESOURCEAPPLNO_RESET_VALUE);
        getTbUWEscrowPaymentTotal().setValue(
                CHILD_TBUWESCROWPAYMENTTOTAL_RESET_VALUE);
        getBtMIReview().setValue(CHILD_BTMIREVIEW_RESET_VALUE);
        getBtUWPartySummary().setValue(CHILD_BTUWPARTYSUMMARY_RESET_VALUE);
        getStNewScenario1().setValue(CHILD_STNEWSCENARIO1_RESET_VALUE);
        getStNewScenario2().setValue(CHILD_STNEWSCENARIO2_RESET_VALUE);
        getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
        getTxUWPreAppEstPurchasePrice().setValue(
                CHILD_TXUWPREAPPESTPURCHASEPRICE_RESET_VALUE);
        getCbUWMIIndicator().setValue(CHILD_CBUWMIINDICATOR_RESET_VALUE);
        getCbUWMIType().setValue(CHILD_CBUWMITYPE_RESET_VALUE);
        getCbUWMIInsurer().setValue(CHILD_CBUWMIINSURER_RESET_VALUE);
        getRbUWMIRUIntervention().setValue(
                CHILD_RBUWMIRUINTERVENTION_RESET_VALUE);
        getTxUWMIExistingPolicy().setValue(
                CHILD_TXUWMIEXISTINGPOLICY_RESET_VALUE);
        getCbUWMIPayor().setValue(CHILD_CBUWMIPAYOR_RESET_VALUE);

        //getStUWMIPreQCertNum().setValue(CHILD_STUWMIPREQCERTNUM_RESET_VALUE);
        //getStUWMIStatus().setValue(CHILD_STUWMISTATUS_RESET_VALUE);
        getRbUWMIUpfront().setValue(CHILD_RBUWMIUPFRONT_RESET_VALUE);
        getTxUWMIPremium().setValue(CHILD_TXUWMIPREMIUM_RESET_VALUE);
        getTxUWMICertificate().setValue(CHILD_TXUWMICERTIFICATE_RESET_VALUE);
        getTxUWEffectiveAmortizationYears().setValue(
                CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE);
        getTxUWEffectiveAmortizationMonths().setValue(
                CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE);
        getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
        getStAppDateForServlet()
        .setValue(CHILD_STAPPDATEFORSERVLET_RESET_VALUE);
        getStMIPremiumTitle().setValue(CHILD_STMIPREMIUMTITLE_RESET_VALUE);
        getStIncludeMIPremiumStart().setValue(
                CHILD_STINCLUDEMIPREMIUMSTART_RESET_VALUE);
        getStIncludeMIPremiumEnd().setValue(
                CHILD_STINCLUDEMIPREMIUMEND_RESET_VALUE);
        getStIncludeMIPremiumValueStart().setValue(
                CHILD_STINCLUDEMIPREMIUMVALUESTART_RESET_VALUE);
        getStIncludeMIPremiumValueEnd().setValue(
                CHILD_STINCLUDEMIPREMIUMVALUEEND_RESET_VALUE);
        getStUWPhoneNoExtension().setValue(
                CHILD_STUWPHONENOEXTENSION_RESET_VALUE);
        getStApplicationDate().setValue(CHILD_STAPPLICATIONDATE_RESET_VALUE);
        getStUnderwriterLastName().setValue(
                CHILD_STUNDERWRITERLASTNAME_RESET_VALUE);
        getStUnderwriterFirstName().setValue(
                CHILD_STUNDERWRITERFIRSTNAME_RESET_VALUE);
        getStUnderwriterMiddleInitial().setValue(
                CHILD_STUNDERWRITERMIDDLEINITIAL_RESET_VALUE);
        getCbUWCommitmentExpectedDateMonth().setValue(
                CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH_RESET_VALUE);
        getTxUWCommitmentExpectedDateYear().setValue(
                CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR_RESET_VALUE);
        getTxUWCommitmentExpectedDateDay().setValue(
                CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY_RESET_VALUE);

        /***** FFATE START *****/
        getCbUWFinancingWaiverDateMonth().setValue(
                CHILD_CBUWFINANCINGWAIVERDATEMONTH_RESET_VALUE);
        getTxUWFinancingWaiverDateYear().setValue(
                CHILD_TXUWFINANCINGWAIVERDATEYEAR_RESET_VALUE);
        getTxUWFinancingWaiverDateDay().setValue(
                CHILD_TXUWFINANCINGWAIVERDATEDAY_RESET_VALUE);
        /***** FFATE START *****/
        
        //CR03
        getCbUWCommitmentExpirationDateMonth().setValue(
                CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH_RESET_VALUE);
        getTxUWCommitmentExpirationDateYear().setValue(
                CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR_RESET_VALUE);
        getTxUWCommitmentExpirationDateDay().setValue(
                CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY_RESET_VALUE);
        //CR03

        getStRateLock().setValue(CHILD_STRATELOCK_RESET_VALUE);
        getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
        getStSourceFirstName().setValue(CHILD_STSOURCEFIRSTNAME_RESET_VALUE);
        getStSourceLastName().setValue(CHILD_STSOURCELASTNAME_RESET_VALUE);
        getRbProgressAdvance().setValue(CHILD_RBPROGRESSADVANCE_RESET_VALUE);
        getTxNextAdvanceAmount()
        .setValue(CHILD_TXNEXTADVANCEAMOUNT_RESET_VALUE);
        getTxAdvanceToDateAmt().setValue(CHILD_TXADVANCETODATEAMT_RESET_VALUE);
        getTxAdvanceNumber().setValue(CHILD_TXADVANCENUMBER_RESET_VALUE);
        getCbUWRefiOrigPurchaseDateMonth().setValue(
                CHILD_CBUWREFIORIGPURCHASEDATEMONTH_RESET_VALUE);
        getTxUWRefiOrigPurchaseDateYear().setValue(
                CHILD_TXUWREFIORIGPURCHASEDATEYEAR_RESET_VALUE);
        getTxUWRefiOrigPurchaseDateDay().setValue(
                CHILD_TXUWREFIORIGPURCHASEDATEDAY_RESET_VALUE);
        getTxUWRefiPurpose().setValue(CHILD_TXUWREFIPURPOSE_RESET_VALUE);
        getTxUWRefiMortgageHolder().setValue(
                CHILD_TXUWREFIMORTGAGEHOLDER_RESET_VALUE);
        getRbBlendedAmortization().setValue(
                CHILD_RBBLENDEDAMORTIZATION_RESET_VALUE);
        getTxUWRefiOrigPurchasePrice().setValue(
                CHILD_TXUWREFIORIGPURCHASEPRICE_RESET_VALUE);
        getTxUWRefiImprovementValue().setValue(
                CHILD_TXUWREFIIMPROVEMENTVALUE_RESET_VALUE);
        getTxUWRefiOrigMtgAmount().setValue(
                CHILD_TXUWREFIORIGMTGAMOUNT_RESET_VALUE);
        getTxUWRefiOutstandingMtgAmount().setValue(
                CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE);
        getTxUWRefiImprovementsDesc().setValue(
                CHILD_TXUWREFIIMPROVEMENTSDESC_RESET_VALUE);
        getStVALSData().setValue(CHILD_STVALSDATA_RESET_VALUE);

        //// new hidden button to activate 'fake' submit button from the
        // ActiveMessage class.
        getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);

        //--Release2.1--//
        //// New link to toggle the language. When touched this link should
        // reload the
        //// page in opposite language (french versus english) and set this new
        // language
        //// session value throughout all modulus.
        getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
        getStRateInventoryId().setValue(CHILD_STRATEINVENTORYID_RESET_VALUE);

        //--Release2.1--//
        //// New HiddenField to get PaymentTermId from JavaScript Applet
        // companion functions
        //// and propagate it to the database on submit.
        getHdPaymentTermId().setValue(CHILD_HDPAYMENTTERMID_RESET_VALUE);

        //===============================================================
        // New fields to handle MIPolicyNum problem :
        //  the number lost if user cancel MI and re-send again later.
        // -- Modified by Billy 16July2003
        getHdMIPolicyNoCMHC().setValue(CHILD_HDMIPOLICYNOCMHC_RESET_VALUE);
        getHdMIPolicyNoGE().setValue(CHILD_HDMIPOLICYNOGE_RESET_VALUE);
        getHdMIPolicyNoAIGUG().setValue(CHILD_HDMIPOLICYNOAIGUG_RESET_VALUE);
		getHdMIPolicyNoPMI().setValue(CHILD_HDMIPOLICYNOPMI_RESET_VALUE);

        //--BMO_MI_CR--start//
        //// This field should be editable under certain circustances (BMO
        // direct
        //// db update: MI process outside BXP.
        getCbUWMIStatus().setValue(CHILD_CBUWMISTATUS_RESET_VALUE);
        getHdOutsideXpress().setValue(CHILD_HDOUTSIDEXPRESS_RESET_VALUE);
        getTxUWMIPreQCertNum().setValue(CHILD_TXUWMIPREQCERTNUM_RESET_VALUE);
        getHdMIStatusId().setValue(CHILD_HDMISTATUSID_RESET_VALUE);
        getHdMIIndicatorId().setValue(CHILD_HDMIINDICATORID_RESET_VALUE);
        getHdMIInsurer().setValue(CHILD_HDMIINSURER_RESET_VALUE);
        getHdMIPremium().setValue(CHILD_HDMIPREMIUM_RESET_VALUE);
        getHdMIPreqCertNum().setValue(CHILD_HDMIPREQCERTNUM_RESET_VALUE);

        //-- FXLink Phase II --//
        //--> New Field for Market Type Indicator
        //--> By Billy 18Nov2003
        getStMarketTypeLabel().setValue(CHILD_STMARKETTYPELABEL_RESET_VALUE);
        getStMarketType().setValue(CHILD_STMARKETTYPE_RESET_VALUE);

        //--DJ_PT_CR--start//
        getStBranchTransitLabel().setValue(
                CHILD_STBRANCHTRANSITLABEL_RESET_VALUE);
        getStCompanyName().setValue(CHILD_STPARTYNAME_RESET_VALUE);
        getStBranchTransitNumber().setValue(
                CHILD_STBRANCHTRANSITNUM_RESET_VALUE);
        getStAddressLine1().setValue(CHILD_STADDRESSLINE1_RESET_VALUE);
        getStCity().setValue(CHILD_STCITY_RESET_VALUE);

        //--DJ_PT_CR--end//
        //--DJ_LDI_CR--start--//
        getBtLDInsuranceDetails().setValue(
                CHILD_BTLDINSURANCEDETAILS_RESET_VALUE);
        getStIncludeLifeDisLabelsStart().setValue(
                CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE);
        getStIncludeLifeDisLabelsEnd().setValue(
                CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE);
        getStGdsTdsDetailsLabel().setValue(
                CHILD_STGDSTDSDETAILSLABEL_RESET_VALUE);
        getStPmntIncludingLifeDisability().setValue(
                CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE);

        //--DJ_LDI_CR--end--//
        //--FX_LINK--start--//
        getBtSOBDetails().setValue(CHILD_BTSOURCEOFBUSINESSDETAILS_RESET_VALUE);

        //--FX_LINK--end--//
        //--DJ_CR010--start//
        getTxCommisionCode().setValue(CHILD_TXCOMMISIONCODE_RESET_VALUE);

        //--DJ_CR010--end//
        //--DJ_CR203.1--start//
        getTxProprietairePlusLOC().setValue(CHILD_TXPROPRIETAIREPLUSLOC_RESET_VALUE);
        getRbMultiProject().setValue(CHILD_RBMULTIPROJECT_RESET_VALUE);
        getRbProprietairePlus().setValue(CHILD_RBPROPRIETAIREPLUS_RESET_VALUE);

        //--DJ_CR203.1--end//
        //--DJ_CR134--start--27May2004--//
        getStHomeBASEProductRatePmnt().setValue(CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE);

        //--DJ_CR134--end--//
        //--DisableProductRateTicket#570--10Aug2004--start--//
        getStDealRateStatusForServlet().setValue(
                CHILD_STDEALRATESTATUSFORSERVLET_RESET_VALUE);
        getStRateDisDateForServlet().setValue(
                CHILD_STRATEDISDATEFORSERVLET_RESET_VALUE);
        getStIsPageEditableForServlet().setValue(
                CHILD_STISPAGEEDITABLEFORSERVLET_RESET_VALUE);

        //--DisableProductRateTicket#570--10Aug2004--end--//
        //--Ticket#575--16Sep2004--start--//
        getHdInterestTypeId().setValue(CHILD_HDINTERESTTYPEID_RESET_VALUE);

        //--Ticket#575--16Sep2004--end--//
        //-- ========== DJ#725 begins ========== --//
        //-- by Neil at Nov/29/2004
        getStContactEmailAddress().setValue(
                CHILD_STCONTACTEMAILADDRESS_RESET_VALUE);
        getStPsDescription().setValue(CHILD_STPSDESCRIPTION_RESET_VALUE);
        getStSystemTypeDescription().setValue(
                CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE);
        //-- ========== DJ#725 ends ========== --//

        //-- ========== BMO Phase II CCAPS Section 4.6 begins ========== --//
        //-- by Neil on Feb 14, 2005
        getStServicingMortgageNumber().setValue(
                CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE);
        getStCCAPS().setValue(CHILD_STCCAPS_RESET_VALUE);
        //-- ========== BMO Phase II CCAPS Section 4.6 ends ========== --//

        //--Release3.1--begins
        //--by Hiro Apr 13, 2006
        getCbProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);
        getCbRefiProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);

        getRbProgressAdvanceInspectionBy().setValue(
                CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE);
        getRbSelfDirectedRRSP().setValue(CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE);
        getTxCMHCProductTrackerIdentifier().setValue(
                CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE);
        getCbLOCRepaymentType()
        .setValue(CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE);
        getRbRequestStandardService().setValue(
                CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE);
        getStLOCAmortizationMonths().setValue(
                CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE);
        getStLOCInterestOnlyMaturityDate().setValue(
                CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE);

        //CR03
        getRbAutoCalculateCommitmentDate()
        .setValue(CHILD_RBAUTOCALCULATECOMMITMENTDATE_RESET_VALUE);
        //CR03

        //Purchase Plus Improvements
        getTxUWImprovedValue().setValue(
                CHILD_TXUWIMPROVEDVALUE_RESET_VALUE);

        getStDealPurposeTypeHiddenStart().setValue(
                CHILD_STDEALPURPOSETYPEHIDDENSTART_RESET_VALUE);

        getStDealPurposeTypeHiddenEnd().setValue(
                CHILD_STDEALPURPOSETYPEHIDDENEND_RESET_VALUE);
        //Purchase Plus Improvements
        
        // Ticket 267
        getHdDealStatusId().setValue(CHILD_HDDEALSTATUSID_RESET_VALUE);

//      ***** Change by NBC Impl. Team - Version 55 - Start *****//
        getHdForceProgressAdvance().setValue(CHILD_HDFORCEPROGRESSADVANCE_RESET_VALUE);

//      ***** Change by NBC Impl. Team - Version 55 - End*****//
//      CR-18
        getHdReqInProgress().setValue(CHILD_HDREQINPROGRESS_RESET_VALUE);
        getHdReqInProgressForAnotherScenario().setValue(CHILD_HDREQINPROGRESS_FOR_ANOTHER_SCENARIO_RESET_VALUE);
        //--Release3.1--ends
        //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
        getCbAffiliationProgram().setValue(CHILD_CBAFFILIATIONPROGRAM_RESET_VALUE);
        //  ***** Change by NBC Impl. Team - Version 1.3 - End *****//



        //CR03
        getHdCommitmentExpiryDate().setValue(CHILD_HDCOMMITMENTEXPIRYDATE_RESET_VALUE);

        getHdMosProperty().setValue(CHILD_HDMOSPROPERTY_RESET_VALUE);
        //CR03

        /***************MCM Impl team changes starts - XS_2.46 *******************/

        getTxRefiAdditionalInformation().setValue(CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE);

        /***************MCM Impl team changes ends - XS_2.46 *********************/

		//4.4 Submission Agent
        getRepeatedSOB().resetChildren();
        
        // ***** Qualifying rate ***** //
        getCbQualifyProductType().setValue(CHILD_CBQUALIFYPRODUCTTYPE_RESET_VALUE);
        getTxQualifyRate().setValue(CHILD_TXQUALIFYRATE_RESET_VALUE);
        getChQualifyRateOverride().setValue(
                CHILD_CHQUALIFYRATEOVERRIDE_RESET_VALUE);
        getChQualifyRateOverrideRate().setValue(
                CHILD_CHQUALIFYRATEOVERRIDERATE_RESET_VALUE);
        getBtRecalculateGDS().setValue(CHILD_BTRECALCULATEGDS_RESET_VALUE);
        getStQualifyRate().setValue(CHILD_STQUALIFYRATE_RESET_VALUE);
        getStQualifyProductType().setValue(CHILD_STQUALIFYPRODUCTTYPE_RESET_VALUE);
        getStQualifyRateHidden().setValue(CHILD_STQUALIFYRATEHIDDEN_RESET_VALUE);
        getStQualifyRateEdit().setValue(CHILD_STQUALIFYRATEEDIT_RESET_VALUE);
        getSthdQualifyRate().setValue(CHILD_STHDQUALIFYRATE_RESET_VALUE);
        getSthdQualifyProductId().setValue(CHILD_STHDQUALIFYPRODUCTID_RESET_VALUE);
        getHdQualifyRateDisabled().setValue(CHILD_HDQUALIFYRATEDISABLED_RESET_VALUE);
        //5.0 MI -- start
        getTxMIResponseFirstLine().setValue(CHILD_TXMIRESPONSE_FIRSTLIEN_RESET_VALUE);
        getBtExpandMIResponse().setValue(CHILD_BTEXPAND_MIRESPONSE_RESET_VALUE);
        getHdUWCombinedLendingValue().setValue(CHILD_HDUWCOMBINEDLENDINGVALUE_RESET_VALUE);
        
        //5.0 MI -- end
    }

    /**
     * registerChildren()
     * 
     *  
     * @param 
     * @return void : the result of registerChildren 
     * @version 1.2 
     *          Date: 06/26/006
     *          Author: NBC/PP Implementation Team 
     *          Change: <br>
     *          added registerChild(CHILD_TXRATEGUARANTEEPERIOD,
     *          TextField.class); 
     *@version 55
     *          Date: 11/20/2006 
     *          Author: NBC/PP Implementation Team
     *          Change: 
     *          added registerChild(CHILD_HDFORCEPROGRESSADVANCE) 
     *@Version 1.3 <br>
     *          Date: 06/09/2008
     *          Author: MCM Impl Team <br>
     *          Change Log:  <br>
     *          XS_2.46 -- 06/06/2008 -- Added necessary changes required for the new fields added in Refinance section<br>
     *@Version 1.4 <br>
     *          Date: July 21, 2008
     *          Author: MCM Impl Team <br>
     *          Change Log:  <br>
     *          XS_2.44 -- Added ComponentInfo paglet<br>
     */
    protected void registerChildren() {

        super.registerChildren();

        // ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
        registerChild(CHILD_TXRATEGUARANTEEPERIOD, TextField.class);
        // ***** Change by NBC Impl. Team - Version 1.2 - End*****//



//      ***** Change by NBC Impl. Team - Version 1.5 - Start *****//

        registerChild(CHILD_CHCASHBACKOVERRIDEINDOLLARS, CheckBox.class);
        registerChild(CHILD_TXCASHBACKINPERCENTAGE, TextField.class);
        registerChild(CHILD_TXCASHBACKINDOLLARS, TextField.class);

        // ***** Change by NBC Impl. Team - Version 1.5 - End*****//


        registerChild(CHILD_TBDEALID, TextField.class);
        registerChild(CHILD_CBPAGENAMES, ComboBox.class);
        registerChild(CHILD_BTPROCEED, Button.class);
        registerChild(CHILD_HREF1, HREF.class);
        registerChild(CHILD_HREF2, HREF.class);
        registerChild(CHILD_HREF3, HREF.class);
        registerChild(CHILD_HREF4, HREF.class);
        registerChild(CHILD_HREF5, HREF.class);
        registerChild(CHILD_HREF6, HREF.class);
        registerChild(CHILD_HREF7, HREF.class);
        registerChild(CHILD_HREF8, HREF.class);
        registerChild(CHILD_HREF9, HREF.class);
        registerChild(CHILD_HREF10, HREF.class);
        registerChild(CHILD_STPAGELABEL, StaticTextField.class);
        registerChild(CHILD_STCOMPANYNAME, StaticTextField.class);
        registerChild(CHILD_STTODAYDATE, StaticTextField.class);
        registerChild(CHILD_STUSERNAMETITLE, StaticTextField.class);
        registerChild(CHILD_BTWORKQUEUELINK, Button.class);
        registerChild(CHILD_CHANGEPASSWORDHREF, HREF.class);
        registerChild(CHILD_BTTOOLHISTORY, Button.class);
        registerChild(CHILD_BTTOONOTES, Button.class);
        registerChild(CHILD_BTTOOLSEARCH, Button.class);
        registerChild(CHILD_BTTOOLLOG, Button.class);
        registerChild(CHILD_STERRORFLAG, StaticTextField.class);
        registerChild(CHILD_DETECTALERTTASKS, HiddenField.class);
        registerChild(CHILD_HDDEALID, HiddenField.class);
        registerChild(CHILD_HDDEALCOPYID, HiddenField.class);
        registerChild(CHILD_HDDEALINSTITUTIONID, HiddenField.class);
        registerChild(CHILD_STDEALID, StaticTextField.class);
        registerChild(CHILD_STBORRFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_STDEALSTATUS, StaticTextField.class);
        registerChild(CHILD_STDEALSTATUSDATE, StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRM, StaticTextField.class);
        registerChild(CHILD_STSOURCE, StaticTextField.class);
        registerChild(CHILD_STDEALTYPE, StaticTextField.class);
        registerChild(CHILD_STDEALPURPOSE, StaticTextField.class);
        registerChild(CHILD_STPMTTERM, StaticTextField.class);
        registerChild(CHILD_STESTCLOSINGDATE, StaticTextField.class);
        registerChild(CHILD_STSPECIALFEATURE, StaticTextField.class);
        registerChild(CHILD_STSCENARIONO, StaticTextField.class);
        registerChild(CHILD_TXUWSCENERIODESC, TextField.class);
        registerChild(CHILD_STRECOEMENDSCENARIO, StaticTextField.class);
        registerChild(CHILD_STLOCKED, StaticTextField.class);
        registerChild(CHILD_CBSCENARIOS, ComboBox.class);
        registerChild(CHILD_CBSCENARIOPICKLISTTOP, ComboBox.class);
        registerChild(CHILD_BTPROCEEDTOCREATSCENARIOTOP, Button.class);
        registerChild(CHILD_CBSCENARIOPICKLISTBOTTOM, ComboBox.class);
        registerChild(CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM, Button.class);
        registerChild(CHILD_STCOMBINEDGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINED3YRGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDBORROWERGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDTDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINED3YRTDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDBORROWERTDS, StaticTextField.class);
        registerChild(CHILD_REPEATGDSTDSDETAILS,
                pgUWorksheetRepeatGDSTDSDetailsTiledView.class);
        registerChild(CHILD_STUWPURCHASEPRICE, StaticTextField.class);
        registerChild(CHILD_HDUWPURCHASEPRICE, HiddenField.class);
        registerChild(CHILD_CBUWCHARGE, ComboBox.class);
        registerChild(CHILD_STUWDOWNPAYMENT, StaticTextField.class);
        registerChild(CHILD_CBUWLOB, ComboBox.class);
        registerChild(CHILD_CBUWLENDER, ComboBox.class);
        registerChild(CHILD_STUWBRIDGELOAN, StaticTextField.class);
        registerChild(CHILD_CBUWPRODUCT, ComboBox.class);
        registerChild(CHILD_TXUWLOANAMOUNT, TextField.class);
        registerChild(CHILD_TXUWLTV, TextField.class);
        registerChild(CHILD_HDUWORIGLTV, HiddenField.class);
        registerChild(CHILD_BTRECALCULATELTV, Button.class);
        registerChild(CHILD_CBUWPOSTEDINTERESTRATE, ComboBox.class);
        registerChild(CHILD_STMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_TXUWDISCOUNT, TextField.class);
        registerChild(CHILD_STUWTOTALLOAN, StaticTextField.class);
        registerChild(CHILD_TXUWPREMIUM, TextField.class);
        registerChild(CHILD_TXUWAMORTIZATIOPERIODYEARS, TextField.class);
        registerChild(CHILD_TXUWAMORTIZATIOPERIODMONTHS, TextField.class);
        registerChild(CHILD_TXUWBUYDOWN, TextField.class);
        registerChild(CHILD_STUWNETRATE, StaticTextField.class);
//        registerChild(CHILD_STASTERIK, StaticTextField.class);
        registerChild(CHILD_STUWPIPAYMENT, StaticTextField.class);
        registerChild(CHILD_TXUWACTUALPAYYEARS, TextField.class);
        registerChild(CHILD_TXUWACTUALPAYMONTHS, TextField.class);
        registerChild(CHILD_TXUWADDITIONALPIPAY, TextField.class);
        registerChild(CHILD_CBUWPAYMENTFREQUENCY, ComboBox.class);
        registerChild(CHILD_STUWTOTALESCROW, StaticTextField.class);
        registerChild(CHILD_CBUWPREPAYMENTPENALTY, ComboBox.class);
        registerChild(CHILD_STUWTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_CBUWPRIVILEGEPAYMENTOPTION, ComboBox.class);
        registerChild(CHILD_TXUWADVANCEHOLD, TextField.class);
        registerChild(CHILD_STUWMAXIMUMPRINCIPALBALANCEALLOWED,
                StaticTextField.class);
        registerChild(CHILD_STUWMINIMUMINCOMEREQUIRED, StaticTextField.class);
        registerChild(CHILD_STUWMAXIMUMTDSEXPENSEALLOWED, StaticTextField.class);
        registerChild(CHILD_CBUWDEALPURPOSE, ComboBox.class);
        registerChild(CHILD_CBUWBRANCH, ComboBox.class);
        registerChild(CHILD_CBUWDEALTYPE, ComboBox.class);
        // SEAN GECF IV document type drop down register child.
        registerChild(CHILD_CBIVDOCUMENTTYPE, ComboBox.class);
        registerChild(CHILD_STHIDEIVDOCUMENTTYPESTART, StaticTextField.class);
        registerChild(CHILD_STHIDEIVDOCUMENTTYPEEND, StaticTextField.class);
        // END GECF IV DOCUMENT TYPE DROP DOWN
        // SEAN DJ SPEC-Progress Advance Type July 20, 2005: Register Child.
        registerChild(CHILD_CBPROGRESSADVANCETYPE, ComboBox.class);
        registerChild(CHILD_STHIDEPROGRESSADVANCETYPESTART,
                StaticTextField.class);
        registerChild(CHILD_STHIDEPROGRESSADVANCETYPEEND, StaticTextField.class);
        // SEAN DJ SPEC-PAT END
        registerChild(CHILD_CBUWCROSSSELL, ComboBox.class);
        registerChild(CHILD_CBUWSPECIALFEATURE, ComboBox.class);
        registerChild(CHILD_TXUWREFERENCEEXISTINGMTGNO, TextField.class);
        registerChild(CHILD_CBUWRATELOCKEDIN, ComboBox.class);
        registerChild(CHILD_CBUWREFERENCETYPE, ComboBox.class);
        registerChild(CHILD_CBUWREPAYMENTTYPE, ComboBox.class);
        /***************MCM Impl team changes starts - XS_2.43 *******************/
        registerChild(CHILD_STDEFAULTREPAYMENTTYPEID, StaticTextField.class);
        /***************MCM Impl team changes ends - XS_2.43 *******************/
        registerChild(CHILD_TXUWREFERENCEDEALNO, TextField.class);
        registerChild(CHILD_CBUWSECONDMORTGAGEEXIST, ComboBox.class);
        registerChild(CHILD_CBUWESTIMATEDCLOSINGDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXUWESTIMATEDCLOSINGDATEDAY, TextField.class);
        registerChild(CHILD_TXUWESTIMATEDCLOSINGDATEYEAR, TextField.class);
        registerChild(CHILD_STINCLUDEFINANCINGPROGRAMSTART, StaticTextField.class);
        registerChild(CHILD_CBUWFINANCINGPROGRAM, ComboBox.class);
        registerChild(CHILD_STINCLUDEFINANCINGPROGRAMEND, StaticTextField.class);
        registerChild(CHILD_CBUWFIRSTPAYMETDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXUWFIRSTPAYMETDATEDAY, TextField.class);
        registerChild(CHILD_TXUWFIRSTPAYMETDATEYEAR, TextField.class);
        registerChild(CHILD_CBUWTAXPAYOR, ComboBox.class);
        registerChild(CHILD_STUWMATURITYDATE, StaticTextField.class);
/*        registerChild(CHILD_STUWSOURCEFIRM, StaticTextField.class);
        registerChild(CHILD_STUWSOURCEADDRESSLINE1, StaticTextField.class);
        registerChild(CHILD_STUWSOURCECITY, StaticTextField.class);
        registerChild(CHILD_STUWSOURCEPROVINCE, StaticTextField.class);
        registerChild(CHILD_STUWSOURCECONTACTPHONE, StaticTextField.class);
        registerChild(CHILD_STUWSOURCEFAX, StaticTextField.class);*/
        registerChild(CHILD_REPEATAPPLICANTFINANCIALDETAILS,
                pgUWorksheetRepeatApplicantFinancialDetailsTiledView.class);
        registerChild(CHILD_BTADDAPPLICANT, Button.class);
        registerChild(CHILD_BTDUPAPPLICANT, Button.class);
        registerChild(CHILD_BTCREDITREPORT, Button.class);
        registerChild(CHILD_STUWCOMBINEDTOTALINCOME, StaticTextField.class);
        registerChild(CHILD_STUWCOMBINEDTOTALLIABILITIES, StaticTextField.class);
        registerChild(CHILD_STUWTOTALNETWORTH, StaticTextField.class);
        registerChild(CHILD_STUWCOMBINEDTOTALASSETS, StaticTextField.class);
        registerChild(CHILD_REPEATPROPERTYINFORMATION,
                pgUWorksheetRepeatPropertyInformationTiledView.class);
        registerChild(CHILD_BTADDPROPERTY, Button.class);
        registerChild(CHILD_STTOTALPROPERTYEXPENSES, StaticTextField.class);
        registerChild(CHILD_BTAPPRAISALREVIEW, Button.class);
        registerChild(CHILD_REPEATEDUWDOWNPAYMENT,
                pgUWorksheetRepeatedUWDownPaymentTiledView.class);
        registerChild(CHILD_REPEATEDESCROWDETAILS,
                pgUWorksheetRepeatedEscrowDetailsTiledView.class);
        registerChild(CHILD_STUWTARGETDOWNPAYMENT, StaticTextField.class);
        registerChild(CHILD_STUWDOWNPAYMENTREQUIRED, StaticTextField.class);
        registerChild(CHILD_TXUWTOTALDOWN, TextField.class);
        registerChild(CHILD_BTADDDOWNPAYMENT, Button.class);
        registerChild(CHILD_BTADDESCROW, Button.class);
        registerChild(CHILD_BTSAVESCENARIO, Button.class);
        registerChild(CHILD_BTVERIFYSCENARIO, Button.class);
        registerChild(CHILD_BTCONDITIONS, Button.class);
        registerChild(CHILD_BTRESOLVEDEAL, Button.class);
        registerChild(CHILD_BTCOLLAPSEDENYDEAL, Button.class);
        registerChild(CHILD_BTUWCANCEL, Button.class);
        registerChild(CHILD_BTPREVTASKPAGE, Button.class);
        registerChild(CHILD_STPREVTASKPAGELABEL, StaticTextField.class);
        registerChild(CHILD_BTNEXTTASKPAGE, Button.class);
        registerChild(CHILD_STNEXTTASKPAGELABEL, StaticTextField.class);
        registerChild(CHILD_STTASKNAME, StaticTextField.class);
        registerChild(CHILD_SESSIONUSERID, HiddenField.class);
        registerChild(CHILD_STPMGENERATE, StaticTextField.class);
        registerChild(CHILD_STPMHASTITLE, StaticTextField.class);
        registerChild(CHILD_STPMHASINFO, StaticTextField.class);
        registerChild(CHILD_STPMHASTABLE, StaticTextField.class);
        registerChild(CHILD_STPMHASOK, StaticTextField.class);
        registerChild(CHILD_STPMTITLE, StaticTextField.class);
        registerChild(CHILD_STPMINFOMSG, StaticTextField.class);
        registerChild(CHILD_STPMONOK, StaticTextField.class);
        registerChild(CHILD_STPMMSGS, StaticTextField.class);
        registerChild(CHILD_STPMMSGTYPES, StaticTextField.class);
        registerChild(CHILD_STAMGENERATE, StaticTextField.class);
        registerChild(CHILD_STAMHASTITLE, StaticTextField.class);
        registerChild(CHILD_STAMHASINFO, StaticTextField.class);
        registerChild(CHILD_STAMHASTABLE, StaticTextField.class);
        registerChild(CHILD_STAMTITLE, StaticTextField.class);
        registerChild(CHILD_STAMINFOMSG, StaticTextField.class);
        registerChild(CHILD_STAMMSGS, StaticTextField.class);
        registerChild(CHILD_STAMMSGTYPES, StaticTextField.class);
        registerChild(CHILD_STAMDIALOGMSG, StaticTextField.class);
        registerChild(CHILD_STAMBUTTONSHTML, StaticTextField.class);
        registerChild(CHILD_BTFEEREVIEW, Button.class);
        registerChild(CHILD_BTGOSCENARIO, Button.class);
        registerChild(CHILD_BTRECALCULATE, Button.class);
        registerChild(CHILD_BTCHANGESOURCE, Button.class);
        registerChild(CHILD_BTBRIDGEREVIEW, Button.class);
        registerChild(CHILD_STTARGETESCROW, StaticTextField.class);
        registerChild(CHILD_HDLONGPOSTEDDATE, HiddenField.class);
        registerChild(CHILD_TBUWRATECODE, TextField.class);
        registerChild(CHILD_TBUWPAYMENTTERMDESCRIPTION, TextField.class);
        registerChild(CHILD_STLENDERPRODUCTID, StaticTextField.class);
        registerChild(CHILD_STRATETIMESTAMP, StaticTextField.class);
        registerChild(CHILD_STLENDERPROFILEID, StaticTextField.class);
        registerChild(CHILD_TBUWREFERENCESOURCEAPPLNO, TextField.class);
        registerChild(CHILD_TBUWESCROWPAYMENTTOTAL, TextField.class);
        registerChild(CHILD_BTMIREVIEW, Button.class);
        registerChild(CHILD_BTUWPARTYSUMMARY, Button.class);
        registerChild(CHILD_STNEWSCENARIO1, StaticTextField.class);
        registerChild(CHILD_STNEWSCENARIO2, StaticTextField.class);
        registerChild(CHILD_BTOK, Button.class);
        registerChild(CHILD_TXUWPREAPPESTPURCHASEPRICE, TextField.class);
        registerChild(CHILD_CBUWMIINDICATOR, ComboBox.class);
        registerChild(CHILD_CBUWMITYPE, ComboBox.class);
        registerChild(CHILD_CBUWMIINSURER, ComboBox.class);
        registerChild(CHILD_RBUWMIRUINTERVENTION, RadioButtonGroup.class);
        registerChild(CHILD_TXUWMIEXISTINGPOLICY, TextField.class);
        registerChild(CHILD_CBUWMIPAYOR, ComboBox.class);

        //registerChild(CHILD_STUWMIPREQCERTNUM,StaticTextField.class);
        //registerChild(CHILD_STUWMISTATUS,StaticTextField.class);
        registerChild(CHILD_RBUWMIUPFRONT, RadioButtonGroup.class);
        registerChild(CHILD_TXUWMIPREMIUM, TextField.class);
        registerChild(CHILD_TXUWMICERTIFICATE, TextField.class);
        registerChild(CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS, TextField.class);
        registerChild(CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS, TextField.class);
        registerChild(CHILD_STTOTALLOANAMOUNT, StaticTextField.class);
        registerChild(CHILD_STAPPDATEFORSERVLET, StaticTextField.class);
        registerChild(CHILD_STMIPREMIUMTITLE, StaticTextField.class);
        registerChild(CHILD_STINCLUDEMIPREMIUMSTART, StaticTextField.class);
        registerChild(CHILD_STINCLUDEMIPREMIUMEND, StaticTextField.class);
        registerChild(CHILD_STINCLUDEMIPREMIUMVALUESTART, StaticTextField.class);
        registerChild(CHILD_STINCLUDEMIPREMIUMVALUEEND, StaticTextField.class);
        registerChild(CHILD_STUWPHONENOEXTENSION, StaticTextField.class);
        registerChild(CHILD_STAPPLICATIONDATE, StaticTextField.class);
        registerChild(CHILD_STUNDERWRITERLASTNAME, StaticTextField.class);
        registerChild(CHILD_STUNDERWRITERFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_STUNDERWRITERMIDDLEINITIAL, StaticTextField.class);
        registerChild(CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR, TextField.class);
        registerChild(CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY, TextField.class);
        //FFATE
        registerChild(CHILD_CBUWFINANCINGWAIVERDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXUWFINANCINGWAIVERDATEYEAR, TextField.class);
        registerChild(CHILD_TXUWFINANCINGWAIVERDATEDAY, TextField.class);
        //FFATE
        //CR03
        registerChild(CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR, TextField.class);
        registerChild(CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY, TextField.class);
        //CR03
        registerChild(CHILD_STRATELOCK, StaticTextField.class);
        registerChild(CHILD_STVIEWONLYTAG, StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_STSOURCELASTNAME, StaticTextField.class);
        registerChild(CHILD_RBPROGRESSADVANCE, RadioButtonGroup.class);
        registerChild(CHILD_TXNEXTADVANCEAMOUNT, TextField.class);
        registerChild(CHILD_TXADVANCETODATEAMT, TextField.class);
        registerChild(CHILD_TXADVANCENUMBER, TextField.class);
        registerChild(CHILD_CBUWREFIORIGPURCHASEDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXUWREFIORIGPURCHASEDATEYEAR, TextField.class);
        registerChild(CHILD_TXUWREFIORIGPURCHASEDATEDAY, TextField.class);
        registerChild(CHILD_TXUWREFIPURPOSE, TextField.class);
        registerChild(CHILD_TXUWREFIMORTGAGEHOLDER, TextField.class);
        registerChild(CHILD_RBBLENDEDAMORTIZATION, RadioButtonGroup.class);
        registerChild(CHILD_TXUWREFIORIGPURCHASEPRICE, TextField.class);
        registerChild(CHILD_TXUWREFIIMPROVEMENTVALUE, TextField.class);
        registerChild(CHILD_TXUWREFIORIGMTGAMOUNT, TextField.class);
        registerChild(CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT, TextField.class);
        registerChild(CHILD_TXUWREFIIMPROVEMENTSDESC, TextField.class);
        registerChild(CHILD_STVALSDATA, StaticTextField.class);

        //// New hidden button implementation.
        registerChild(CHILD_BTACTMSG, Button.class);

        //--Release2.1--//
        //// New link to toggle the language. When touched this link should reload the
        //// page in opposite language (french versus english) and set this new language
        //// session value throughout all modulus.
        registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);
        registerChild(CHILD_STRATEINVENTORYID, StaticTextField.class);

        //--Release2.1--//
        //// New HiddenField to get PaymentTermId from JavaScript Applet companion functions
        //// and propagate it to the database on submit.
        registerChild(CHILD_HDPAYMENTTERMID, StaticTextField.class);

        //===============================================================
        // New fields to handle MIPolicyNum problem :
        //  the number lost if user cancel MI and re-send again later.
        // -- Modified by Billy 14July2003
        registerChild(CHILD_HDMIPOLICYNOCMHC, HiddenField.class);
        registerChild(CHILD_HDMIPOLICYNOGE, HiddenField.class);
        registerChild(CHILD_HDMIPOLICYNOAIGUG, HiddenField.class);
		registerChild(CHILD_HDMIPOLICYNOPMI, HiddenField.class);

        //--BMO_MI_CR--start//
        //// This field should be editable under certain circustances (BMO direct
        //// db update: MI process outside BXP.
        registerChild(CHILD_CBUWMISTATUS, TextField.class);
        registerChild(CHILD_HDOUTSIDEXPRESS, HiddenField.class);
        registerChild(CHILD_TXUWMIPREQCERTNUM, TextField.class);
        registerChild(CHILD_HDMISTATUSID, HiddenField.class);
        registerChild(CHILD_HDMIINDICATORID, HiddenField.class);
        registerChild(CHILD_HDMIINSURER, HiddenField.class);
        registerChild(CHILD_HDMIPREMIUM, HiddenField.class);
        registerChild(CHILD_HDMIPREQCERTNUM, HiddenField.class);

        //-- FXLink Phase II --//
        //--> New Field for Market Type Indicator
        //--> By Billy 18Nov2003
        registerChild(CHILD_STMARKETTYPELABEL, StaticTextField.class);
        registerChild(CHILD_STMARKETTYPE, StaticTextField.class);

        //--DJ_PT_CR--start//
        registerChild(CHILD_STBRANCHTRANSITLABEL, StaticTextField.class);
        registerChild(CHILD_STPARTYNAME, StaticTextField.class);
        registerChild(CHILD_STBRANCHTRANSITNUM, StaticTextField.class);
        registerChild(CHILD_STADDRESSLINE1, StaticTextField.class);
        registerChild(CHILD_STCITY, StaticTextField.class);

        //--DJ_PT_CR--end//
        //--DJ_LDI_CR--start--//
        registerChild(CHILD_BTLDINSURANCEDETAILS, Button.class);
        registerChild(CHILD_STINCLUDELIFEDISLABELSSTART, StaticTextField.class);
        registerChild(CHILD_STINCLUDELIFEDISLABELSEND, StaticTextField.class);
        registerChild(CHILD_STGDSTDSDETAILSLABEL, StaticTextField.class);
        registerChild(CHILD_STPMNTINCLUDINGLIFEDISABILITY, StaticTextField.class);

        //--DJ_LDI_CR--end--//
        //--FX_LINK--start--//
        registerChild(CHILD_BTSOURCEOFBUSINESSDETAILS, Button.class);

        //--FX_LINK--end--//
        //--DJ_CR010--start//
        registerChild(CHILD_TXCOMMISIONCODE, TextField.class);

        //--DJ_CR010--end//
        //--DJ_CR203.1--start//
        registerChild(CHILD_TXPROPRIETAIREPLUSLOC, TextField.class);
        registerChild(CHILD_RBMULTIPROJECT, RadioButtonGroup.class);
        registerChild(CHILD_RBPROPRIETAIREPLUS, RadioButtonGroup.class);

        //--DJ_CR203.1--end//
        //--DJ_CR134--start--27May2004--//
        registerChild(CHILD_STHOMEBASEPRODUCTRATEPMNT, StaticTextField.class);

        //--DJ_CR134--end--//
        //--DisableProductRateTicket#570--10Aug2004--start--//
        registerChild(CHILD_STDEALRATESTATUSFORSERVLET, StaticTextField.class);
        registerChild(CHILD_STRATEDISDATEFORSERVLET, StaticTextField.class);
        registerChild(CHILD_STISPAGEEDITABLEFORSERVLET, StaticTextField.class);

        //--DisableProductRateTicket#570--10Aug2004--end--//
        //--Ticket#575--16Sep2004--start--//
        registerChild(CHILD_HDINTERESTTYPEID, HiddenField.class);

        //--Ticket#575--16Sep2004--end--//
        //-- ========== DJ#725 begins ========== --//
        //-- by Neil at Nov/29/2004
        registerChild(CHILD_STCONTACTEMAILADDRESS, StaticTextField.class);
        registerChild(CHILD_STPSDESCRIPTION, StaticTextField.class);
        registerChild(CHILD_STSYSTEMTYPEDESCRIPTION, StaticTextField.class);
        //-- ========== DJ#725 ends ========== --//

        //-- ========== BMO Phase II CCAPS Section 4.6 begins ========== --//
        //-- by Neil on Feb 14, 2005
        registerChild(CHILD_STSERVICINGMORTGAGENUMBER, StaticTextField.class);
        registerChild(CHILD_STCCAPS, StaticTextField.class);
        //-- ========== BMO Phase II CCAPS Section 4.6 ends ========== --//

        //--Release3.1--begins
        //--by Hiro Apr 13, 2006
        registerChild(CHILD_CBPRODUCTTYPE, ComboBox.class);
        registerChild(CHILD_CBREFIPRODUCTTYPE, ComboBox.class);
        registerChild(CHILD_RBPROGRESSADVANCEINSPECTIONBY,
                RadioButtonGroup.class);
        registerChild(CHILD_RBSELFDIRECTEDRRSP, RadioButtonGroup.class);
        registerChild(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER, TextField.class);
        registerChild(CHILD_CBLOCREPAYMENTTYPEID, ComboBox.class);
        registerChild(CHILD_RBREQUESTSTANDARDSERVICE, RadioButtonGroup.class);
        registerChild(CHILD_STLOCAMORTIZATIONMONTHS, StaticTextField.class);
        registerChild(CHILD_STLOCINTERESTONLYMATURITYDATE,
                StaticTextField.class);

        //CR03
        registerChild(CHILD_RBAUTOCALCULATECOMMITMENTDATE, RadioButtonGroup.class);
        //CR03

//      ***** Change by NBC Impl. Team - Version 55 - Start*****//
        registerChild(CHILD_HDFORCEPROGRESSADVANCE, HiddenField.class);

//      ***** Change by NBC Impl. Team - Version 55 - End*****//

//      CR-18
        registerChild(CHILD_HDREQINPROGRESS, HiddenField.class);
        registerChild(CHILD_HDREQINPROGRESS_FOR_ANOTHER_SCENARIO, HiddenField.class);
        //--Release3.1--ends

        //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
        registerChild(CHILD_CBAFFILIATIONPROGRAM,ComboBox.class);
        //  ***** Change by NBC Impl. Team - Version 1.3 - End *****//

        //***** Change by NBC/PP Implementation Team - GCD - Start *****//

        registerChild(CHILD_TX_CREDIT_DECISION_STATUS, StaticTextField.class);
        registerChild(CHILD_TX_CREDIT_DECISION, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_RISK_RATING, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_INTERNAL_SCORE, StaticTextField.class);
        registerChild(CHILD_TX_MISTATUS, StaticTextField.class);
        registerChild(CHILD_BT_CREDIT_DECISION_PG, Button.class);
        registerChild(CHILD_ST_INCLUDE_GCDSUM_START, StaticTextField.class);
        registerChild(CHILD_ST_INCLUDE_GCDSUM_END, StaticTextField.class);

        //***** Change by NBC/PP Implementation Team - GCD - End *****//

        //CR03
        registerChild(CHILD_HDCOMMITMENTEXPIRYDATE, HiddenField.class);

        registerChild(CHILD_HDMOSPROPERTY, HiddenField.class);

        //CR03

        //Purchase Plus Improvement
        registerChild(CHILD_TXUWIMPROVEDVALUE, StaticTextField.class);
        registerChild(CHILD_STDEALPURPOSETYPEHIDDENSTART, StaticTextField.class);
        registerChild(CHILD_STDEALPURPOSETYPEHIDDENEND, StaticTextField.class);
        registerChild(CHILD_HDDEALSTATUSID, HiddenField.class);
        //Purchase Plus Improvement

        /***************MCM Impl team changes starts - XS_2.46 *******************/

        registerChild(CHILD_TXREFIADDITIONALINFORMATION, TextField.class);

        /***************MCM Impl team changes ends - XS_2.46 *********************/
        /***************MCM Impl team changes starts - XS_2.13*******************/
        registerChild(CHILD_STQUALIFYINGDETAILSPAGELET,pgQualifyingDetailsPageletView.class);
        /***************MCM Impl team changes ends - XS_2.13*******************/
        /***************MCM Impl team changes starts - XS_2.44*******************/
        registerChild(CHILD_COMPONENTINFOPAGELET,pgComponentInfoPageletViewBean.class);
        /***************MCM Impl team changes ends - XS_2.44*******************/

		//4.4 Submission Agent
        registerChild(CHILD_REPEATEDSOB, pgUWRepeatedSOBTiledView.class);
        //Qualifying Rate
        registerChild(CHILD_CBQUALIFYPRODUCTTYPE, ComboBox.class);
        registerChild(CHILD_TXQUALIFYRATE, TextField.class);
        registerChild(CHILD_CHQUALIFYRATEOVERRIDE, CheckBox.class);
        registerChild(CHILD_CHQUALIFYRATEOVERRIDERATE, CheckBox.class);
        registerChild(CHILD_BTRECALCULATEGDS, Button.class);
        registerChild(CHILD_STQUALIFYRATE, StaticTextField.class);
        registerChild(CHILD_STQUALIFYPRODUCTTYPE, StaticTextField.class);
        registerChild(CHILD_STQUALIFYRATEHIDDEN, StaticTextField.class);
        registerChild(CHILD_STQUALIFYRATEEDIT, StaticTextField.class);
        registerChild(CHILD_STHDQUALIFYRATE, StaticTextField.class);
        registerChild(CHILD_STHDQUALIFYPRODUCTID, StaticTextField.class);
        registerChild(CHILD_HDQUALIFYRATEDISABLED, StaticTextField.class);
        //5.0 MI -- start
        registerChild(CHILD_TXMIRESPONSE_FIRSTLIEN, TextField.class);
        registerChild(CHILD_BTEXPAND_MIRESPONSE, Button.class);
        registerChild(CHILD_HDUWCOMBINEDLENDINGVALUE, HiddenField.class);

        //5.0 MI -- end
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Model management methods
    //////////////////////////////////;//////////////////////////////////////////////

    /**
     * 
     *  
     */
    public Model[] getWebActionModels(int executionType) {
        List modelList = new ArrayList();

        switch (executionType) {
        case MODEL_TYPE_RETRIEVE:

            /// BJH:TRIAL
            ///
            // modelList.add(getdoUWDealSelectMainModel());;modelList.add(getdoUWDealSummarySnapShotModel());;modelList.add(getdoUWBridgeModel());;modelList.add(getdoUWSourceDetailsModel());;modelList.add(getdoUWMIStatusModel());;modelList.add(getdoUWSourceInformationModel());;
            modelList.add(getdoUWDealSummarySnapShotModel());

            //-- ========== DJ#725 begins ========== --//
            //-- by Neil : Nov/29/2004
            modelList.add(getdoUWSourceDetailsModel());

            //-- ========== DJ#725 ends ========== --//

            // ***** Change by NBC/PP Implementation Team - GCD - Start

            modelList.add(getdoAdjudicationResponseBNCModel ());
            ;
            modelList.add(getdoDealMIStatusModel ());
            ;

            // ***** Change by NBC/PP Implementation Team - GCD - END


            break;

        case MODEL_TYPE_UPDATE:
            ;

            break;

        case MODEL_TYPE_DELETE:
            ;

            break;

        case MODEL_TYPE_INSERT:
            ;

            break;

        case MODEL_TYPE_EXECUTE:
            ;

            break;
        }

        return (Model[]) modelList.toArray(new Model[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // View flow control methods
    ////////////////////////////////////////////////////////////////////////////////

    /**
     * 
     *  
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        //logger = SysLog.getSysLogger("UWVB");
        handler.pageGetState(this);

        //--Release2.1--start//
        ////Populate all ComboBoxes manually here
        //// 1. Setup Options for cbPageNames
        //cbPageNamesOptions..populate(getRequestContext());
        int langId = handler.getTheSessionState().getLanguageId();
        cbPageNamesOptions.populate(getRequestContext(), langId);

        ////Setup NoneSelected Label for cbPageNames
        CHILD_CBPAGENAMES_NONSELECTED_LABEL = BXResources.getGenericMsg(
                "CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState()
                .getLanguageId());

        ////Check if the cbPageNames is already created
        if (getCbPageNames() != null) {
            getCbPageNames().setLabelForNoneSelected(
                    CHILD_CBPAGENAMES_NONSELECTED_LABEL);
        }

        //// 2-... Repopulate all Comboboxes on the page.
        //// IMPORTANT!! To prevent the population of comboboxes twice
        //// is case of splitted JSP (which is the case for pgUWorksheet.jsp)
        //// the content should be null. It will provide the initial population only.
        cbUWChargeOptions.populate(getRequestContext());
        cbUWLOBOptions.populate(getRequestContext());
        cbUWPaymentFrequencyOptions.populate(getRequestContext());
        cbUWPrePaymentPenaltyOptions.populate(getRequestContext());
        cbUWPrivilegePaymentOptionOptions.populate(getRequestContext());
        cbUWDealPurposeOptions.populate(getRequestContext());
        cbUWBranchOptions.populate(getRequestContext());
        cbUWDealTypeOptions.populate(getRequestContext());
        // SEAN GECF IV DOCUMENT TYPE DROP DOWN populate options.
        cbIVDocumentTypeOptions.populate(getRequestContext());
        // END GECF IV DOCUMENT TYPE DROP
        // SEAN DJ SPEC-Progress Advance Type July 20, 2005: populate the options.
        cbProgressAdvanceTypeOptions.populate(getRequestContext());
        // SEAN DJ SPEC-PAT END
        cbUWCrossSellOptions.populate(getRequestContext());
        cbUWSpecialFeatureOptions.populate(getRequestContext());
        cbUWReferenceTypeOptions.populate(getRequestContext());
        cbUWRepaymentTypeOptions.populate(getRequestContext());
        cbUWFinancingProgramOptions.populate(getRequestContext());
        cbUWTaxPayorOptions.populate(getRequestContext());

        cbUWMIIndicatorOptions.populate(getRequestContext());
        //cbUWMITypeOptions.populate(getRequestContext()); //5.0 MI deleted
        cbUWMIInsurerOptions.populate(getRequestContext());
        cbUWMIPayorOptions.populate(getRequestContext());

        //--Release3.1--begins
        //--by Hiro Apr 13, 2006
        cbProductTypeOptions.populate(getRequestContext());
        cbRefiProductTypeOptions.populate(getRequestContext());
        cbLOCRepaymentTypeOptions.populate(getRequestContext());
        //--Release3.1--ends

        // ***** Change by NBC Impl. Team - Version 1.3 - Start*****//
        cbAffiliationProgramOptions.populate(getRequestContext());
        // ***** Change by NBC Impl. Team - Version 1.3 - End*****//

        //// 3. Setup the Yes/No ComboBox and Radio Options
        String yesStr = BXResources.getGenericMsg("YES_LABEL", handler
                .getTheSessionState().getLanguageId());
        String noStr = BXResources.getGenericMsg("NO_LABEL", handler
                .getTheSessionState().getLanguageId());

        cbUWRateLockedInOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        cbUWSecondMortgageExistOptions.setOptions(
                new String[] { yesStr, noStr }, new String[] { "Y", "N" });
        rbUWMIRUInterventionOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbUWMIUpfrontOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbProgressAdvanceOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbBlendedAmortizationOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });

        //--Release3.1--begins
        //--by Hiro Apr 18, 2006
        String lenderStr = BXResources.getGenericMsg("LENDER_LABEL", handler
                .getTheSessionState().getLanguageId());
        String InsurerStr = BXResources.getGenericMsg("INSURER_LABEL", handler
                .getTheSessionState().getLanguageId());

        rbProgressAdvanceInspectionByOptions.setOptions(new String[] {
                lenderStr, InsurerStr }, new String[] { "L", "I" });
        rbSelfDirectedRRSPOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbRequestStandardServiceOptions.setOptions(
                new String[] { yesStr, noStr }, new String[] { "Y", "N" });
        // --Release3.1--ends

        //CR03
        rbAutoCalculateCommitmentDateOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        //CR03

        // // 4. Translate the textbox message.
        handler.setupBeforePageGeneration();

        // --BMO_MI_CR--start//
        // // MIStatus is combobox to handle MI Processing outside BXP
        cbUWMIStatusOptions.populate(getRequestContext());

        //--BMO_MI_CR--end//
        //--DJ_CR203.1--start//
        rbMultiProjectOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbProprietairePlusOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });

        //--DJ_CR203.1--end//
        handler.pageSaveState();

        super.beginDisplay(event);
    }

    /**
     * 
     *  
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {
        /// UWorksheetHandler handler =(UWorksheetHandler)
        // this.handler.cloneSS();
        /// handler.pageGetState(this);
        /// handler.setupBeforePageGeneration();
        /// handler.pageSaveState();
        // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
        return super.beforeModelExecutes(model, executionContext);

        // The following code block was migrated from the
        // this_onBeforeDataObjectExecuteEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * UWorksheetHandler handler =(UWorksheetHandler)
         * this.handler.cloneSS();
         * 
         * handler.pageGetState(this);
         * 
         * handler.setupBeforePageGeneration();
         * 
         * handler.pageSaveState();
         * 
         * return;
         */
    }

    /**
     * 
     *  
     */
    public void afterModelExecutes(Model model, int executionContext) {
        // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);
    }

    /**
     * 
     *  
     */
    public void afterAllModelsExecute(int executionContext) {
        // This is the analog of NetDynamics
        // this_onAfterAllDataObjectsExecuteEvent
        super.afterAllModelsExecute(executionContext);

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.populatePageDisplayFields();

        handler.pageSaveState();
    }

    /**
     * 
     *  
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception) throws ModelControlException {
        // This is the analog of NetDynamics this_onDataObjectErrorEvent
        super.onModelError(model, executionContext, exception);
    }

    /**
     * 
     *  
     */
    public TextField getTbDealId() {
        return (TextField) getChild(CHILD_TBDEALID);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbPageNames() {
        return (ComboBox) getChild(CHILD_CBPAGENAMES);
    }

    //--DJ_Ticket661--end--//


    //***** Change by NBC/PP Implementation Team - GCD - Start *****//

    public void handleBtCreditDecisionPGRequest(RequestInvocationEvent event) 
    {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtCreditDecisionPG");
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleCreditDecisionPGButton(event);
        handler.postHandlerProtocol();
    }

    //***** Change by NBC/PP Implementation Team - GCD - End *****//


    /**
     * 
     *  
     */
    public void handleBtProceedRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtProceed");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleGoPage();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtProceed() {
        return (Button) getChild(CHILD_BTPROCEED);
    }

    /**
     * 
     *  
     */
    public void handleHref1Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href1");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(0);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref1() {
        return (HREF) getChild(CHILD_HREF1);
    }

    /**
     * 
     *  
     */
    public void handleHref2Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href2");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(1);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref2() {
        return (HREF) getChild(CHILD_HREF2);
    }

    /**
     * 
     *  
     */
    public void handleHref3Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href3");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(2);
    }

    /**
     * 
     *  
     */
    public HREF getHref3() {
        return (HREF) getChild(CHILD_HREF3);
    }

    /**
     * 
     *  
     */
    public void handleHref4Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href4");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(3);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref4() {
        return (HREF) getChild(CHILD_HREF4);
    }

    /**
     * 
     *  
     */
    public void handleHref5Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href5");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(4);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref5() {
        return (HREF) getChild(CHILD_HREF5);
    }

    /**
     * 
     *  
     */
    public void handleHref6Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href6");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(5);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref6() {
        return (HREF) getChild(CHILD_HREF6);
    }

    /**
     * 
     *  
     */
    public void handleHref7Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href7");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(6);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref7() {
        return (HREF) getChild(CHILD_HREF7);
    }

    /**
     * 
     *  
     */
    public void handleHref8Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href8");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(7);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref8() {
        return (HREF) getChild(CHILD_HREF8);
    }

    /**
     * 
     *  
     */
    public void handleHref9Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href9");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(8);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref9() {
        return (HREF) getChild(CHILD_HREF9);
    }

    /**
     * 
     *  
     */
    public void handleHref10Request(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".Href10");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(9);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getHref10() {
        return (HREF) getChild(CHILD_HREF10);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPageLabel() {
        return (StaticTextField) getChild(CHILD_STPAGELABEL);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStCompanyName() {
        return (StaticTextField) getChild(CHILD_STCOMPANYNAME);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStTodayDate() {
        return (StaticTextField) getChild(CHILD_STTODAYDATE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUserNameTitle() {
        return (StaticTextField) getChild(CHILD_STUSERNAMETITLE);
    }

    /**
     * 
     *  
     */
    public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtWorkQueueLink");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayWorkQueue();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtWorkQueueLink() {
        return (Button) getChild(CHILD_BTWORKQUEUELINK);
    }

    /**
     * 
     *  
     */
    public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".ChangePasswordHref");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleChangePassword();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getChangePasswordHref() {
        return (HREF) getChild(CHILD_CHANGEPASSWORDHREF);
    }

    //--Release2.1--start//
    //// Two new methods providing the business logic for the new link to
    // toggle language.
    //// When touched this link should reload the page in opposite language
    // (french versus english)
    //// and set this new session value.

    /**
     * 
     *  
     */
    public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".ToggleLanguageHref");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleToggleLanguage();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public HREF getToggleLanguageHref() {
        return (HREF) getChild(CHILD_TOGGLELANGUAGEHREF);
    }

    //--Release2.1--end//

    /**
     * 
     *  
     */
    public void handleBtToolHistoryRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtToolHistory");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayDealHistory();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtToolHistory() {
        return (Button) getChild(CHILD_BTTOOLHISTORY);
    }

    /**
     * 
     *  
     */
    public void handleBtTooNotesRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtTooNotes");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayDealNotes(true);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtTooNotes() {
        return (Button) getChild(CHILD_BTTOONOTES);
    }

    /**
     * 
     *  
     */
    public void handleBtToolSearchRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtToolSearch");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDealSearch();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtToolSearch() {
        return (Button) getChild(CHILD_BTTOOLSEARCH);
    }

    /**
     * 
     *  
     */
    public void handleBtToolLogRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtToolLog");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleSignOff();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtToolLog() {
        return (Button) getChild(CHILD_BTTOOLLOG);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStErrorFlag() {
        return (StaticTextField) getChild(CHILD_STERRORFLAG);
    }

    /**
     * 
     *  
     */
    public HiddenField getDetectAlertTasks() {
        return (HiddenField) getChild(CHILD_DETECTALERTTASKS);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdDealId() {
        return (HiddenField) getChild(CHILD_HDDEALID);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdDealCopyId() {
        return (HiddenField) getChild(CHILD_HDDEALCOPYID);
    }
    /**
     * 
     *  
     */
    public HiddenField getHdDealInstituionId() {
        return (HiddenField) getChild(CHILD_HDDEALINSTITUTIONID);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStDealId() {
        return (StaticTextField) getChild(CHILD_STDEALID);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStBorrFirstName() {
        return (StaticTextField) getChild(CHILD_STBORRFIRSTNAME);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStDealStatus() {
        return (StaticTextField) getChild(CHILD_STDEALSTATUS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStDealStatusDate() {
        return (StaticTextField) getChild(CHILD_STDEALSTATUSDATE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStSourceFirm() {
        return (StaticTextField) getChild(CHILD_STSOURCEFIRM);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStSource() {
        return (StaticTextField) getChild(CHILD_STSOURCE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStDealType() {
        return (StaticTextField) getChild(CHILD_STDEALTYPE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStDealPurpose() {
        return (StaticTextField) getChild(CHILD_STDEALPURPOSE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmtTerm() {
        return (StaticTextField) getChild(CHILD_STPMTTERM);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStEstClosingDate() {
        return (StaticTextField) getChild(CHILD_STESTCLOSINGDATE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStSpecialFeature() {
        return (StaticTextField) getChild(CHILD_STSPECIALFEATURE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStScenarioNo() {
        return (StaticTextField) getChild(CHILD_STSCENARIONO);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWScenerioDesc() {
        return (TextField) getChild(CHILD_TXUWSCENERIODESC);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStRecoemendScenario() {
        return (StaticTextField) getChild(CHILD_STRECOEMENDSCENARIO);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStLocked() {
        return (StaticTextField) getChild(CHILD_STLOCKED);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbScenarios() {
        return (ComboBox) getChild(CHILD_CBSCENARIOS);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbScenarioPickListTop() {
        return (ComboBox) getChild(CHILD_CBSCENARIOPICKLISTTOP);
    }

    /**
     * 
     *  
     */
    public void handleBtProceedToCreatScenarioTopRequest(
            RequestInvocationEvent event) throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtProceedToCreatScenarioTop");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleCreateNewScenario(0, -1, false, "cbScenarioPickListTop");

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtProceedToCreatScenarioTop() {
        return (Button) getChild(CHILD_BTPROCEEDTOCREATSCENARIOTOP);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbScenarioPickListBottom() {
        return (ComboBox) getChild(CHILD_CBSCENARIOPICKLISTBOTTOM);
    }

    /**
     * 
     *  
     */
    public void handleBtProceedToCreateScenarioBottomRequest(
            RequestInvocationEvent event) throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtProceedToCreateScenarioBottom");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleCreateNewScenario(0, -1, false,
        "cbScenarioPickListBottom");

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtProceedToCreateScenarioBottom() {
        return (Button) getChild(CHILD_BTPROCEEDTOCREATESCENARIOBOTTOM);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStCombinedGDS() {
        return (StaticTextField) getChild(CHILD_STCOMBINEDGDS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStCombined3YrGDS() {
        return (StaticTextField) getChild(CHILD_STCOMBINED3YRGDS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStCombinedBorrowerGDS() {
        return (StaticTextField) getChild(CHILD_STCOMBINEDBORROWERGDS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStCombinedTDS() {
        return (StaticTextField) getChild(CHILD_STCOMBINEDTDS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStCombined3YrTDS() {
        return (StaticTextField) getChild(CHILD_STCOMBINED3YRTDS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStCombinedBorrowerTDS() {
        return (StaticTextField) getChild(CHILD_STCOMBINEDBORROWERTDS);
    }

    /**
     * 
     *  
     */
    public pgUWorksheetRepeatGDSTDSDetailsTiledView getRepeatGDSTDSDetails() {
        return (pgUWorksheetRepeatGDSTDSDetailsTiledView) getChild(CHILD_REPEATGDSTDSDETAILS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWPurchasePrice() {
        return (StaticTextField) getChild(CHILD_STUWPURCHASEPRICE);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdUWPurchasePrice() {
        return (HiddenField) getChild(CHILD_HDUWPURCHASEPRICE);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWCharge() {
        return (ComboBox) getChild(CHILD_CBUWCHARGE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWDownPayment() {
        return (StaticTextField) getChild(CHILD_STUWDOWNPAYMENT);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWLOB() {
        return (ComboBox) getChild(CHILD_CBUWLOB);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWLender() {
        return (ComboBox) getChild(CHILD_CBUWLENDER);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWBridgeLoan() {
        return (StaticTextField) getChild(CHILD_STUWBRIDGELOAN);
    }

    /**
     * 
     *  
     */
    public String endStUWBridgeLoanDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stUWBridgeLoan_onBeforeHtmlOutputEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * return(SKIP);
         */
        return event.getContent();
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWProduct() {
        return (ComboBox) getChild(CHILD_CBUWPRODUCT);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWLoanAmount() {
        return (TextField) getChild(CHILD_TXUWLOANAMOUNT);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWLTV() {
        return (TextField) getChild(CHILD_TXUWLTV);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdUWOrigLTV() {
        return (HiddenField) getChild(CHILD_HDUWORIGLTV);
    }

    /**
     * 
     *  
     */
    public void handleBtRecalculateLTVRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtRecalculateLTV");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        //Ticket #3457. Change false to true.
        handler.handleRecalculate(true);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtRecalculateLTV() {
        return (Button) getChild(CHILD_BTRECALCULATELTV);
    }

    //--BMO_MI_CR--start//
    //// This field should be editable under certain circustances (BMO direct
    //// db update: MI process outside BXP.

    /**
     * 
     *  
     */
    public HiddenField getHdMIPreqCertNum() {
        return (HiddenField) getChild(CHILD_HDMIPREQCERTNUM);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWMIStatus() {
        return (ComboBox) getChild(CHILD_CBUWMISTATUS);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdOutsideXpress() {
        return (HiddenField) getChild(CHILD_HDOUTSIDEXPRESS);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWMIPreQCertNum() {
        return (TextField) getChild(CHILD_TXUWMIPREQCERTNUM);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdMIStatusId() {
        return (HiddenField) getChild(CHILD_HDMISTATUSID);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdMIIndicatorId() {
        return (HiddenField) getChild(CHILD_HDMIINDICATORID);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdMIInsurer() {
        return (HiddenField) getChild(CHILD_HDMIINSURER);
    }

    /**
     * 
     *  
     */
    public HiddenField getHdMIPremium() {
        return (HiddenField) getChild(CHILD_HDMIPREMIUM);
    }

    //--BMO_MI_CR--end//

    /**
     * 
     *  
     */
    public String endBtRecalculateLTVDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btRecalculateLTV_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWPostedInterestRate() {
        return (ComboBox) getChild(CHILD_CBUWPOSTEDINTERESTRATE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStMIPremium() {
        return (StaticTextField) getChild(CHILD_STMIPREMIUM);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWDiscount() {
        return (TextField) getChild(CHILD_TXUWDISCOUNT);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWTotalLoan() {
        return (StaticTextField) getChild(CHILD_STUWTOTALLOAN);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWPremium() {
        return (TextField) getChild(CHILD_TXUWPREMIUM);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWAmortizatioPeriodYears() {
        return (TextField) getChild(CHILD_TXUWAMORTIZATIOPERIODYEARS);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWAmortizatioPeriodMonths() {
        return (TextField) getChild(CHILD_TXUWAMORTIZATIOPERIODMONTHS);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWBuyDown() {
        return (TextField) getChild(CHILD_TXUWBUYDOWN);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWNetRate() {
        return (StaticTextField) getChild(CHILD_STUWNETRATE);
    }

    /**
     * 
     *  
     */
/*    public StaticTextField getStAsterik() {
        return (StaticTextField) getChild(CHILD_STASTERIK);
    }
*/    
    /**
     * 
     *  
     */
    public StaticTextField getStUWPIPayment() {
        return (StaticTextField) getChild(CHILD_STUWPIPAYMENT);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWActualPayYears() {
        return (TextField) getChild(CHILD_TXUWACTUALPAYYEARS);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWActualPayMonths() {
        return (TextField) getChild(CHILD_TXUWACTUALPAYMONTHS);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWAdditionalPIPay() {
        return (TextField) getChild(CHILD_TXUWADDITIONALPIPAY);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWPaymentFrequency() {
        return (ComboBox) getChild(CHILD_CBUWPAYMENTFREQUENCY);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWTotalEscrow() {
        return (StaticTextField) getChild(CHILD_STUWTOTALESCROW);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWPrePaymentPenalty() {
        return (ComboBox) getChild(CHILD_CBUWPREPAYMENTPENALTY);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWTotalPayment() {
        return (StaticTextField) getChild(CHILD_STUWTOTALPAYMENT);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWPrivilegePaymentOption() {
        return (ComboBox) getChild(CHILD_CBUWPRIVILEGEPAYMENTOPTION);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWAdvanceHold() {
        return (TextField) getChild(CHILD_TXUWADVANCEHOLD);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWMaximumPrincipalBalanceAllowed() {
        return (StaticTextField) getChild(CHILD_STUWMAXIMUMPRINCIPALBALANCEALLOWED);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWMinimumIncomeRequired() {
        return (StaticTextField) getChild(CHILD_STUWMINIMUMINCOMEREQUIRED);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWMaximumTDSExpenseAllowed() {
        return (StaticTextField) getChild(CHILD_STUWMAXIMUMTDSEXPENSEALLOWED);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWDealPurpose() {
        return (ComboBox) getChild(CHILD_CBUWDEALPURPOSE);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWBranch() {
        return (ComboBox) getChild(CHILD_CBUWBRANCH);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWDealType() {
        return (ComboBox) getChild(CHILD_CBUWDEALTYPE);
    }

    // SEAN GECF IV DOCUMENT TYPE DROP DOWN: the getter methods..
    public ComboBox getCbIVDocumentType() {
        return (ComboBox) getChild(CHILD_CBIVDOCUMENTTYPE);
    }

    public StaticTextField getStHideIVDocumentTypeStart() {
        return (StaticTextField) getChild(CHILD_STHIDEIVDOCUMENTTYPESTART);
    }

    public StaticTextField getStHideIVDocumentTypeEnd() {
        return (StaticTextField) getChild(CHILD_STHIDEIVDOCUMENTTYPEEND);
    }

    // EDN GECF IV DOCUMENT TYPE DROP DOWN

    // SEAN DJ SPEC-Progress Advance Type July 20, 2005: The getter methods.
    public ComboBox getCbProgressAdvanceType() {
        return (ComboBox) getChild(CHILD_CBPROGRESSADVANCETYPE);
    }
    //The following Code merge may be incorrect
    public StaticTextField getStHideProgressAdvanceTypeStart() {
        return (StaticTextField) getChild(CHILD_STHIDEPROGRESSADVANCETYPESTART);
    }

    public StaticTextField getStHideProgressAdvanceTypeEnd() {
        return (StaticTextField) getChild(CHILD_STHIDEPROGRESSADVANCETYPEEND);
    }
    //end
    // SEAN DJ SPEC-PAT END

    /**
     * 
     *  
     */
    public ComboBox getCbUWCrossSell() {
        return (ComboBox) getChild(CHILD_CBUWCROSSSELL);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWSpecialFeature() {
        return (ComboBox) getChild(CHILD_CBUWSPECIALFEATURE);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWReferenceExistingMTGNo() {
        return (TextField) getChild(CHILD_TXUWREFERENCEEXISTINGMTGNO);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWRateLockedIn() {
        return (ComboBox) getChild(CHILD_CBUWRATELOCKEDIN);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWReferenceType() {
        return (ComboBox) getChild(CHILD_CBUWREFERENCETYPE);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWRepaymentType() {
        return (ComboBox) getChild(CHILD_CBUWREPAYMENTTYPE);
    }

    /***************MCM Impl team changes starts - XS_2.43 *******************/
    public StaticTextField getStDefaultRepaymentTypeId()
    {
        return (StaticTextField) getChild(CHILD_STDEFAULTREPAYMENTTYPEID);
    }
    /***************MCM Impl team changes ends - XS_2.43 *******************/
    
    /**
     * 
     *  
     */
    public TextField getTxUWReferenceDealNo() {
        return (TextField) getChild(CHILD_TXUWREFERENCEDEALNO);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWSecondMortgageExist() {
        return (ComboBox) getChild(CHILD_CBUWSECONDMORTGAGEEXIST);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWEstimatedClosingDateMonth() {
        return (ComboBox) getChild(CHILD_CBUWESTIMATEDCLOSINGDATEMONTH);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWEstimatedClosingDateDay() {
        return (TextField) getChild(CHILD_TXUWESTIMATEDCLOSINGDATEDAY);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWEstimatedClosingDateYear() {
        return (TextField) getChild(CHILD_TXUWESTIMATEDCLOSINGDATEYEAR);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStIncludeFinancingProgramStart() {
        return (StaticTextField) getChild(CHILD_STINCLUDEFINANCINGPROGRAMSTART);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWFinancingProgram() {
        return (ComboBox) getChild(CHILD_CBUWFINANCINGPROGRAM);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStIncludeFinancingProgramEnd() {
        return (StaticTextField) getChild(CHILD_STINCLUDEFINANCINGPROGRAMEND);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWFirstPaymetDateMonth() {
        return (ComboBox) getChild(CHILD_CBUWFIRSTPAYMETDATEMONTH);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWFirstPaymetDateDay() {
        return (TextField) getChild(CHILD_TXUWFIRSTPAYMETDATEDAY);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWFirstPaymetDateYear() {
        return (TextField) getChild(CHILD_TXUWFIRSTPAYMETDATEYEAR);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWTaxPayor() {
        return (ComboBox) getChild(CHILD_CBUWTAXPAYOR);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWMaturityDate() {
        return (StaticTextField) getChild(CHILD_STUWMATURITYDATE);
    }
    
    /**
     * 
     *  
     */
    public HiddenField getHdUWCombinedLendingValue() {
        return (HiddenField) getChild(CHILD_HDUWCOMBINEDLENDINGVALUE);
    }

    /*
    public StaticTextField getStUWSourceFirm() {
        return (StaticTextField) getChild(CHILD_STUWSOURCEFIRM);
    }

    public StaticTextField getStUWSourceAddressLine1() {
        return (StaticTextField) getChild(CHILD_STUWSOURCEADDRESSLINE1);
    }

    public StaticTextField getStUWSourceCity() {
        return (StaticTextField) getChild(CHILD_STUWSOURCECITY);
    }

    public StaticTextField getStUWSourceProvince() {
        return (StaticTextField) getChild(CHILD_STUWSOURCEPROVINCE);
    }

    public StaticTextField getStUWSourceContactPhone() {
        return (StaticTextField) getChild(CHILD_STUWSOURCECONTACTPHONE);
    }

    public StaticTextField getStUWSourceFax() {
        return (StaticTextField) getChild(CHILD_STUWSOURCEFAX);
    }
*/
    /**
     * 
     *  
     */
    public pgUWorksheetRepeatApplicantFinancialDetailsTiledView getRepeatApplicantFinancialDetails() {
        return (pgUWorksheetRepeatApplicantFinancialDetailsTiledView) getChild(CHILD_REPEATAPPLICANTFINANCIALDETAILS);
    }

    /**
     * 
     *  
     */

    /***************MCM Impl team changes starts - XS_2.46 *******************/

    public TextField getTxRefiAdditionalInformation()
    {
        return (TextField) getChild(CHILD_TXREFIADDITIONALINFORMATION);
    }

    /***************MCM Impl team changes ends - XS_2.46 *********************/

    /***************MCM Impl team changes starts - XS_2.44 *******************/
    public pgComponentInfoPageletViewBean getComponentInfoPagelet()
    {
        return (pgComponentInfoPageletViewBean) getChild(CHILD_COMPONENTINFOPAGELET);
    }
    /***************MCM Impl team changes ends - XS_2.44 *******************/

    public void handleBtAddApplicantRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtAddApplicant");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddApplicant(false);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtAddApplicant() {
        return (Button) getChild(CHILD_BTADDAPPLICANT);
    }

    /**
     * 
     *  
     */
    public String endBtAddApplicantDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btAddApplicant_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtDupApplicantRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtDupApplicant");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddApplicant(true);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtDupApplicant() {
        return (Button) getChild(CHILD_BTDUPAPPLICANT);
    }

    /**
     * 
     *  
     */
    public String endBtDupApplicantDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btDupApplicant_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtCreditReportRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtCreditReport");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleCreditReport();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtCreditReport() {
        return (Button) getChild(CHILD_BTCREDITREPORT);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWCombinedTotalIncome() {
        return (StaticTextField) getChild(CHILD_STUWCOMBINEDTOTALINCOME);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWCombinedTotalLiabilities() {
        return (StaticTextField) getChild(CHILD_STUWCOMBINEDTOTALLIABILITIES);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWTotalNetWorth() {
        return (StaticTextField) getChild(CHILD_STUWTOTALNETWORTH);
    }

    /**
     * 
     * Original signature.
     */
    public boolean beginStUWTotalNetWorthDisplay(ChildDisplayEvent event) {
        Object value = getStUWTotalNetWorth().getValue();
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.setNetWorthTotal(this, event);
        handler.pageSaveState();

        return true;
    }

    //// Changed signature to set the NetWorth static text field on fly.

    /**
     * public String beginStUWTotalNetWorthDisplay(ChildDisplayEvent event) {
     * UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
     * handler.pageGetState(this); String rc =
     * handler.setNetWorthTotal((StaticTextField)(this.getStUWTotalNetWorth()));
     * handler.pageSaveState(); return rc; }
     */
    /**
     * 
     *  
     */
    public StaticTextField getStUWCombinedTotalAssets() {
        return (StaticTextField) getChild(CHILD_STUWCOMBINEDTOTALASSETS);
    }

    /**
     * 
     *  
     */
    public pgUWorksheetRepeatPropertyInformationTiledView getRepeatPropertyInformation() {
        return (pgUWorksheetRepeatPropertyInformationTiledView) getChild(CHILD_REPEATPROPERTYINFORMATION);
    }

    /**
     * 
     *  
     */
    public void handleBtAddPropertyRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtAddProperty");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddProperty();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtAddProperty() {
        return (Button) getChild(CHILD_BTADDPROPERTY);
    }

    /**
     * 
     *  
     */
    public String endBtAddPropertyDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btAddProperty_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public StaticTextField getStTotalPropertyExpenses() {
        return (StaticTextField) getChild(CHILD_STTOTALPROPERTYEXPENSES);
    }

    /**
     * 
     *  
     */
    public void handleBtAppraisalReviewRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtAppraisalReview");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAppraisalReview();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtAppraisalReview() {
        return (Button) getChild(CHILD_BTAPPRAISALREVIEW);
    }

    /**
     * 
     *  
     */
    public pgUWorksheetRepeatedUWDownPaymentTiledView getRepeatedUWDownPayment() {
        return (pgUWorksheetRepeatedUWDownPaymentTiledView) getChild(CHILD_REPEATEDUWDOWNPAYMENT);
    }

    /**
     * 
     *  
     */
    public pgUWorksheetRepeatedEscrowDetailsTiledView getRepeatedEscrowDetails() {
        return (pgUWorksheetRepeatedEscrowDetailsTiledView) getChild(CHILD_REPEATEDESCROWDETAILS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWTargetDownPayment() {
        return (StaticTextField) getChild(CHILD_STUWTARGETDOWNPAYMENT);
    }

    /**
     * 
     *  
     */
    public String endStUWTargetDownPaymentDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stUWTargetDownPayment_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateLoadTarget(Mc.TARGET_DE_DOWNPAYMENT);

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWDownPaymentRequired() {
        return (StaticTextField) getChild(CHILD_STUWDOWNPAYMENTREQUIRED);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWTotalDown() {
        return (TextField) getChild(CHILD_TXUWTOTALDOWN);
    }
    
    //-- ========== DJ#725 Starts ========== --//
    //-- By Neil at Nov/29/2004

    /**
     *  
     */
    public StaticTextField getStContactEmailAddress() {
        return (StaticTextField) getChild(CHILD_STCONTACTEMAILADDRESS);
    }

    /**
     * 
     * @return
     */
    public StaticTextField getStPsDescription() {
        return (StaticTextField) getChild(CHILD_STPSDESCRIPTION);
    }

    /**
     * 
     * @return
     */
    public StaticTextField getStSystemTypeDescription() {
        return (StaticTextField) getChild(CHILD_STSYSTEMTYPEDESCRIPTION);
    }

    //-- ========== DJ#725 Ends ========== --//

    /**
     * 
     *  
     */
    public void handleBtAddDownPaymentRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtAddDownPayment");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddDownpayment();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtAddDownPayment() {
        return (Button) getChild(CHILD_BTADDDOWNPAYMENT);
    }

    /**
     * 
     *  
     */
    public String endBtAddDownPaymentDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btAddDownPayment_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtAddEscrowRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtAddEscrow");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddEscrowPayment();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtAddEscrow() {
        return (Button) getChild(CHILD_BTADDESCROW);
    }

    /**
     * 
     *  
     */
    public String endBtAddEscrowDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btAddEscrow_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtSaveScenarioRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtSaveScenario");

        //Call this method to update cash back amount, if cash back percentage has been modified
        updateCashBackAmount();

        //this clears the ETO fields based on deal purpose selected - Ticket 716
        clearETOFields();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleSaveScenario(0, false);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtSaveScenario() {
        return (Button) getChild(CHILD_BTSAVESCENARIO);
    }

    /**
     * 
     *  
     */
    public String endBtSaveScenarioDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btSaveScenario_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayUWReadOnlyButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtVerifyScenarioRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtVerifyScenario");

        //Call this method to update cash back amount, if cash back percentage has been modified
        updateCashBackAmount();

        //this clears the ETO fields based on deal purpose selected - Ticket 716
        clearETOFields();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleSaveScenario(0, true);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtVerifyScenario() {
        return (Button) getChild(CHILD_BTVERIFYSCENARIO);
    }

    /**
     * 
     *  
     */
    public String endBtVerifyScenarioDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btVerifyScenario_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayUWReadOnlyButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtConditionsRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtConditions");

        //Call this method to update cash back amount, if cash back percentage has been modified
        updateCashBackAmount();

        //this clears the ETO fields based on deal purpose selected - Ticket 716
        clearETOFields();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleConditionsReview(true, true);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtConditions() {
        return (Button) getChild(CHILD_BTCONDITIONS);
    }

    /**
     * 
     *  
     */
    public void handleBtResolveDealRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtResolveDeal");

        //Call this method to update cash back amount, if cash back percentage has been modified
        updateCashBackAmount();

        //this clears the ETO fields based on deal purpose selected - Ticket 716
        clearETOFields();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleResolveDeal(0);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtResolveDeal() {
        return (Button) getChild(CHILD_BTRESOLVEDEAL);
    }

    /**
     * 
     *  
     */
    public String endBtResolveDealDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btResolveDeal_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displaySubmitButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtCollapseDenyDealRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtCollapseDenyDeal");

        //Call this method to update cash back amount, if cash back percentage has been modified
        updateCashBackAmount();

        //this clears the ETO fields based on deal purpose selected - Ticket 716
        clearETOFields();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleCollapseDenyDeal();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtCollapseDenyDeal() {
        return (Button) getChild(CHILD_BTCOLLAPSEDENYDEAL);
    }

    /**
     * 
     *  
     */
    public String endBtCollapseDenyDealDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btCollapseDenyDeal_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displaySubmitButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtUWCancelRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtUWCancel");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin
        // --------------
        // handler.handleCancelStandard(); Catherine for GE
        handler.handleReturn();
        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end
        // --------------

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtUWCancel() {
        return (Button) getChild(CHILD_BTUWCANCEL);
    }

    /**
     * 
     *  
     */
    public String endBtUWCancelDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btUWCancel_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayCancelButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtPrevTaskPage");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handlePrevTaskPage();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtPrevTaskPage() {
        return (Button) getChild(CHILD_BTPREVTASKPAGE);
    }

    /**
     * 
     *  
     */
    public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btPrevTaskPage_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generatePrevTaskPage();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPrevTaskPageLabel() {
        return (StaticTextField) getChild(CHILD_STPREVTASKPAGELABEL);
    }

    /**
     * 
     *  
     */
    public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generatePrevTaskPage();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtNextTaskPage");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleNextTaskPage();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtNextTaskPage() {
        return (Button) getChild(CHILD_BTNEXTTASKPAGE);
    }

    /**
     * 
     *  
     */
    public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btNextTaskPage_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateNextTaskPage();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public StaticTextField getStNextTaskPageLabel() {
        return (StaticTextField) getChild(CHILD_STNEXTTASKPAGELABEL);
    }

    /**
     * 
     *  
     */
    public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stNextTaskPageLabel_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateNextTaskPage();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public StaticTextField getStTaskName() {
        return (StaticTextField) getChild(CHILD_STTASKNAME);
    }

    /**
     * 
     *  
     */
    public String endStTaskNameDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stTaskName_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateTaskName();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public HiddenField getSessionUserId() {
        return (HiddenField) getChild(CHILD_SESSIONUSERID);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmGenerate() {
        return (StaticTextField) getChild(CHILD_STPMGENERATE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmHasTitle() {
        return (StaticTextField) getChild(CHILD_STPMHASTITLE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmHasInfo() {
        return (StaticTextField) getChild(CHILD_STPMHASINFO);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmHasTable() {
        return (StaticTextField) getChild(CHILD_STPMHASTABLE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmHasOk() {
        return (StaticTextField) getChild(CHILD_STPMHASOK);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmTitle() {
        return (StaticTextField) getChild(CHILD_STPMTITLE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmInfoMsg() {
        return (StaticTextField) getChild(CHILD_STPMINFOMSG);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmOnOk() {
        return (StaticTextField) getChild(CHILD_STPMONOK);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmMsgs() {
        return (StaticTextField) getChild(CHILD_STPMMSGS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmMsgTypes() {
        return (StaticTextField) getChild(CHILD_STPMMSGTYPES);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmGenerate() {
        return (StaticTextField) getChild(CHILD_STAMGENERATE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmHasTitle() {
        return (StaticTextField) getChild(CHILD_STAMHASTITLE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmHasInfo() {
        return (StaticTextField) getChild(CHILD_STAMHASINFO);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmHasTable() {
        return (StaticTextField) getChild(CHILD_STAMHASTABLE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmTitle() {
        return (StaticTextField) getChild(CHILD_STAMTITLE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmInfoMsg() {
        return (StaticTextField) getChild(CHILD_STAMINFOMSG);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmMsgs() {
        return (StaticTextField) getChild(CHILD_STAMMSGS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmMsgTypes() {
        return (StaticTextField) getChild(CHILD_STAMMSGTYPES);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmDialogMsg() {
        return (StaticTextField) getChild(CHILD_STAMDIALOGMSG);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAmButtonsHtml() {
        return (StaticTextField) getChild(CHILD_STAMBUTTONSHTML);
    }

    /**
     * 
     *  
     */
    public void handleBtFeeReviewRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtFeeReview");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleFeeReview();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtFeeReview() {
        return (Button) getChild(CHILD_BTFEEREVIEW);
    }

    /**
     * 
     *  
     */
    public void handleBtGoScenarioRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtGoScenario");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleGoScenario(0, -1);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtGoScenario() {
        return (Button) getChild(CHILD_BTGOSCENARIO);
    }

    /**
     * 
     *  
     */
    public void handleBtRecalculateRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtRecalculate");

        //Call this method to update cash back amount, if cash back percentage has been modified
        updateCashBackAmount();

        //this clears the ETO fields based on deal purpose selected - Ticket 716
        clearETOFields();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleRecalculate(true);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtRecalculate() {
        return (Button) getChild(CHILD_BTRECALCULATE);
    }

    /**
     * 
     *  
     */
    public String endBtRecalculateDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btRecalculate_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtChangeSourceRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtChangeSource");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleChangeSource();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtChangeSource() {
        return (Button) getChild(CHILD_BTCHANGESOURCE);
    }

    /**
     * 
     *  
     */
    public String endBtChangeSourceDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btChangeSource_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        //-- FXLink Phase II --//
        //--> - Source cannot be changed if deal.ChannelMedia = 'E'
        //--> By Billy 18Nov2003
        boolean rc = handler.displayEditButton()
        && handler.isChangeSourceAllowed();

        //==========================================================
        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtBridgeReviewRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtBridgeReview");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleReviewBridge();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtBridgeReview() {
        return (Button) getChild(CHILD_BTBRIDGEREVIEW);
    }

    /**
     * 
     *  
     */
    public String endBtBridgeReviewDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btBridgeReview_onBeforeHtmlOutputEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * return(SKIP);
         */

        //// This button should never appear in the current version. If we need
        // to get
        //// later on under different circumstanses the logic should be added
        // as usual.
        ////return event.getContent();
        return "";
    }

    /**
     * 
     *  
     */
    public StaticTextField getStTargetEscrow() {
        return (StaticTextField) getChild(CHILD_STTARGETESCROW);
    }

    /**
     * 
     *  
     */
    public String endStTargetEscrowDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stTargetEscrow_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateLoadTarget(Mc.TARGET_DE_ESCROW);

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public HiddenField getHdLongPostedDate() {
        return (HiddenField) getChild(CHILD_HDLONGPOSTEDDATE);
    }

    /**
     * 
     *  
     */
    public TextField getTbUWRateCode() {
        return (TextField) getChild(CHILD_TBUWRATECODE);
    }

    /**
     * 
     *  
     */
    public TextField getTbUWPaymentTermDescription() {
        return (TextField) getChild(CHILD_TBUWPAYMENTTERMDESCRIPTION);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStLenderProductId() {
        return (StaticTextField) getChild(CHILD_STLENDERPRODUCTID);
    }

    /**
     * 
     *  
     */
    public boolean beginStLenderProductIdDisplay(ChildDisplayEvent event) {
        Object value = getStLenderProductId().getValue();

        handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.handleProductDealIdPassToHtml(this, event);

        handler.pageSaveState();

        return true;
    }

    /**
     * 
     *  
     */
    public StaticTextField getStRateTimeStamp() {
        return (StaticTextField) getChild(CHILD_STRATETIMESTAMP);
    }

    /**
     * 
     *  
     */
    public boolean beginStRateTimeStampDisplay(ChildDisplayEvent event) {
        Object value = getStRateTimeStamp().getValue();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.handleRateTimeStampPassToHtml(this, event);

        handler.pageSaveState();

        return true;
    }

    ////--Release2.1--start//
    //// New logic for Lender-->Product-->Rate module based on the
    //// rateInventoryId comparison.

    /**
     * 
     *  
     */
    public StaticTextField getStRateInventoryId() {
        return (StaticTextField) getChild(CHILD_STRATEINVENTORYID);
    }

    public boolean beginStRateInventoryIdDisplay(ChildDisplayEvent event) {
        Object value = getStRateTimeStamp().getValue();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.handleRateInventoryIdPassToHtml(this, event);

        handler.pageSaveState();

        return true;
    }

    /**
     * 
     *  
     */

    // New HiddenField to get the paymentTermId from JavaScript and propagate into
    // the database.
    public HiddenField getHdPaymentTermId() {
        return (HiddenField) getChild(CHILD_HDPAYMENTTERMID);
    }

    public boolean beginHdPaymentTermIdDisplay(ChildDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.handlePaymentTermIdPassToHtml(this, event);

        handler.pageSaveState();

        return true;
    }

    ////--Release2.1--end//

    /**
     * 
     *  
     */
    public StaticTextField getStLenderProfileId() {
        return (StaticTextField) getChild(CHILD_STLENDERPROFILEID);
    }

    /**
     * 
     *  
     */
    public boolean beginStLenderProfileIdDisplay(ChildDisplayEvent event) {
        Object value = getStLenderProfileId().getValue();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.handleLenderProfileIdPassToHtml(this, event);

        handler.pageSaveState();

        return true;

        // The following code block was migrated from the
        // stLenderProfileId_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
         * UWorksheetHandler handler =(UWorksheetHandler)
         * this.handler.cloneSS();
         * 
         * handler.pageGetState(this);
         * 
         * handler.handleLenderProfileIdPassToHtml(this, event,
         * "stLenderProfileId", "doUWDealSelectMain", "dfLenderProfileId");
         * 
         * handler.pageSaveState();
         * 
         * return;
         */
    }

    /**
     * 
     *  
     */
    public TextField getTbUWReferenceSourceApplNo() {
        return (TextField) getChild(CHILD_TBUWREFERENCESOURCEAPPLNO);
    }

    /**
     * 
     *  
     */
    public TextField getTbUWEscrowPaymentTotal() {
        return (TextField) getChild(CHILD_TBUWESCROWPAYMENTTOTAL);
    }

    /**
     * 
     *  
     */
    public void handleBtMIReviewRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtMIReview");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        //late addition to QC716 fix.
        clearETOFields();
        
        handler.preHandlerProtocol(this);

        handler.handleMIReview(0);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtMIReview() {
        return (Button) getChild(CHILD_BTMIREVIEW);
    }

    /**
     * 
     *  
     */
    public String endBtMIReviewDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btMIReview_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayUWReadOnlyButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtUWPartySummaryRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtUWPartySummary");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayPartySummary();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtUWPartySummary() {
        return (Button) getChild(CHILD_BTUWPARTYSUMMARY);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStNewScenario1() {
        return (StaticTextField) getChild(CHILD_STNEWSCENARIO1);
    }

    /**
     * 
     *  
     */
    public boolean beginStNewScenario1Display(ChildDisplayEvent event) {
        Object value = getStNewScenario1().getValue();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.setNewScenario1();

        handler.pageSaveState();

        return true;
    }

    /**
     * 
     *  
     */
    public String endStNewScenario1Display(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stNewScenario1_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayCreateScenarioButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public StaticTextField getStNewScenario2() {
        return (StaticTextField) getChild(CHILD_STNEWSCENARIO2);
    }

    /**
     * 
     *  
     */
    public boolean beginStNewScenario2Display(ChildDisplayEvent event) {
        Object value = getStNewScenario2().getValue();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.setNewScenario2();

        handler.pageSaveState();

        return true;
    }

    /**
     * 
     *  
     */
    public String endStNewScenario2Display(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stNewScenario2_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayCreateScenarioButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public void handleBtOKRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtOK");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleCancelStandard();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public Button getBtOK() {
        return (Button) getChild(CHILD_BTOK);
    }

    /**
     * 
     *  
     */
    public String endBtOKDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btOK_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayOKButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    /**
     * 
     *  
     */
    public TextField getTxUWPreAppEstPurchasePrice() {
        return (TextField) getChild(CHILD_TXUWPREAPPESTPURCHASEPRICE);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWMIIndicator() {
        return (ComboBox) getChild(CHILD_CBUWMIINDICATOR);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWMIType() {
        return (ComboBox) getChild(CHILD_CBUWMITYPE);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWMIInsurer() {
        return (ComboBox) getChild(CHILD_CBUWMIINSURER);
    }

    /**
     * 
     *  
     */
    public RadioButtonGroup getRbUWMIRUIntervention() {
        return (RadioButtonGroup) getChild(CHILD_RBUWMIRUINTERVENTION);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWMIExistingPolicy() {
        return (TextField) getChild(CHILD_TXUWMIEXISTINGPOLICY);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWMIPayor() {
        return (ComboBox) getChild(CHILD_CBUWMIPAYOR);
    }

    /**
     * 
     *  
     */
    /**
     * public StaticTextField getStUWMIPreQCertNum() { return
     * (StaticTextField)getChild(CHILD_STUWMIPREQCERTNUM); }
     */
    /**
     * 
     *  
     */
    /**
     * public StaticTextField getStUWMIStatus() { return
     * (StaticTextField)getChild(CHILD_STUWMISTATUS); }
     */
    /**
     * 
     *  
     */
    public RadioButtonGroup getRbUWMIUpfront() {
        return (RadioButtonGroup) getChild(CHILD_RBUWMIUPFRONT);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWMIPremium() {
        return (TextField) getChild(CHILD_TXUWMIPREMIUM);
    }

    /**
     * 
     *  
     */

    //===============================================================
    // New fields to handle MIPolicyNum problem :
    //  the number lost if user cancel MI and re-send again later.
    // -- Modified by Billy 16July2003
    public TextField getTxUWMICertificate() {
        return (TextField) getChild(CHILD_TXUWMICERTIFICATE);
    }

    public HiddenField getHdMIPolicyNoCMHC() {
        return (HiddenField) getChild(CHILD_HDMIPOLICYNOCMHC);
    }

    public HiddenField getHdMIPolicyNoGE() {
        return (HiddenField) getChild(CHILD_HDMIPOLICYNOGE);
    }

    public HiddenField getHdMIPolicyNoAIGUG()
    {
        return (HiddenField) getChild(CHILD_HDMIPOLICYNOAIGUG);
    }
	public HiddenField getHdMIPolicyNoPMI()
	{
		return (HiddenField) getChild(CHILD_HDMIPOLICYNOPMI);
	}

    //==============================================================

    /**
     * 
     *  
     */
    public TextField getTxUWEffectiveAmortizationYears() {
        return (TextField) getChild(CHILD_TXUWEFFECTIVEAMORTIZATIONYEARS);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWEffectiveAmortizationMonths() {
        return (TextField) getChild(CHILD_TXUWEFFECTIVEAMORTIZATIONMONTHS);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStTotalLoanAmount() {
        return (StaticTextField) getChild(CHILD_STTOTALLOANAMOUNT);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStAppDateForServlet() {
        return (StaticTextField) getChild(CHILD_STAPPDATEFORSERVLET);
    }

    //--DisableProductRateTicket#570--10Aug2004--start--//

    /**
     * 
     *  
     */
    public StaticTextField getStDealRateStatusForServlet() {
        return (StaticTextField) getChild(CHILD_STDEALRATESTATUSFORSERVLET);
    }

    public StaticTextField getStRateDisDateForServlet() {
        return (StaticTextField) getChild(CHILD_STRATEDISDATEFORSERVLET);
    }

    public StaticTextField getStIsPageEditableForServlet() {
        return (StaticTextField) getChild(CHILD_STISPAGEEDITABLEFORSERVLET);
    }

    //--DisableProductRateTicket#570--10Aug2004--end--//
    //--Ticket#575--16Sep2004--start--//
    public HiddenField getHdInterestTypeId() {
        return (HiddenField) getChild(CHILD_HDINTERESTTYPEID);
    }

    //--Ticket#575--16Sep2004--end--//

    /**
     * 
     *  
     */
    public StaticTextField getStMIPremiumTitle() {
        return (StaticTextField) getChild(CHILD_STMIPREMIUMTITLE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStIncludeMIPremiumStart() {
        return (StaticTextField) getChild(CHILD_STINCLUDEMIPREMIUMSTART);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStIncludeMIPremiumEnd() {
        return (StaticTextField) getChild(CHILD_STINCLUDEMIPREMIUMEND);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStIncludeMIPremiumValueStart() {
        return (StaticTextField) getChild(CHILD_STINCLUDEMIPREMIUMVALUESTART);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStIncludeMIPremiumValueEnd() {
        return (StaticTextField) getChild(CHILD_STINCLUDEMIPREMIUMVALUEEND);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUWPhoneNoExtension() {
        return (StaticTextField) getChild(CHILD_STUWPHONENOEXTENSION);
    }

    /**
     * 
     *  
     */
    //  public String endStUWPhoneNoExtensionDisplay(ChildContentDisplayEvent
    // event)
    //  {
    //    // The following code block was migrated from the
    // stUWPhoneNoExtension_onBeforeHtmlOutputEvent method
    //    UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
    //
    //    handler.pageGetState(this);
    //
    //    String ext =
    //      handler.displayPhoneExtension((StaticTextField)
    // (this.getStViewOnlyTag()));
    //
    //    handler.pageSaveState();
    //
    //    return ext;
    //  }
    //
    /**
     * 
     *  
     */
    public StaticTextField getStApplicationDate() {
        return (StaticTextField) getChild(CHILD_STAPPLICATIONDATE);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUnderwriterLastName() {
        return (StaticTextField) getChild(CHILD_STUNDERWRITERLASTNAME);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUnderwriterFirstName() {
        return (StaticTextField) getChild(CHILD_STUNDERWRITERFIRSTNAME);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStUnderwriterMiddleInitial() {
        return (StaticTextField) getChild(CHILD_STUNDERWRITERMIDDLEINITIAL);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWCommitmentExpectedDateMonth() {
        return (ComboBox) getChild(CHILD_CBUWCOMMITMENTEXPECTEDDATEMONTH);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWCommitmentExpectedDateYear() {
        return (TextField) getChild(CHILD_TXUWCOMMITMENTEXPECTEDDATEYEAR);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWCommitmentExpectedDateDay() {
        return (TextField) getChild(CHILD_TXUWCOMMITMENTEXPECTEDDATEDAY);
    }

    /***** FFATE START *****/
    /**
     * 
     *  
     */
    public ComboBox getCbUWFinancingWaiverDateMonth() {
        return (ComboBox) getChild(CHILD_CBUWFINANCINGWAIVERDATEMONTH);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWFinancingWaiverDateYear() {
        return (TextField) getChild(CHILD_TXUWFINANCINGWAIVERDATEYEAR);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWFinancingWaiverDateDay() {
        return (TextField) getChild(CHILD_TXUWFINANCINGWAIVERDATEDAY);
    }
    /***** FFATE END *****/

    //CR03
    /**
     * 
     *  
     */
    public ComboBox getCbUWCommitmentExpirationDateMonth() {
        return (ComboBox) getChild(CHILD_CBUWCOMMITMENTEXPIRATIONDATEMONTH);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWCommitmentExpirationDateYear() {
        return (TextField) getChild(CHILD_TXUWCOMMITMENTEXPIRATIONDATEYEAR);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWCommitmentExpirationDateDay() {
        return (TextField) getChild(CHILD_TXUWCOMMITMENTEXPIRATIONDATEDAY);
    }
    //CR03


    /**
     * 
     *  
     */
    public StaticTextField getStRateLock() {
        return (StaticTextField) getChild(CHILD_STRATELOCK);
    }

    /**
     * 
     *  
     */
    public boolean beginStRateLockDisplay(ChildDisplayEvent event) {
        Object value = getStRateLock().getValue();

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.handleRateLockPassToHtml(this, event);

        handler.pageSaveState();

        return true;
    }

    /**
     * 
     *  
     */
    public StaticTextField getStViewOnlyTag() {
        return (StaticTextField) getChild(CHILD_STVIEWONLYTAG);
    }

    /**
     * 
     *  
     */
    public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stViewOnlyTag_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        String rc = handler.displayViewOnlyTag();

        handler.pageSaveState();

        return rc;
    }

    /**
     * 
     *  
     */
    public StaticTextField getStSourceFirstName() {
        return (StaticTextField) getChild(CHILD_STSOURCEFIRSTNAME);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStSourceLastName() {
        return (StaticTextField) getChild(CHILD_STSOURCELASTNAME);
    }

    /**
     * 
     *  
     */
    public RadioButtonGroup getRbProgressAdvance() {
        return (RadioButtonGroup) getChild(CHILD_RBPROGRESSADVANCE);
    }

    /**
     * 
     *  
     */
    public TextField getTxNextAdvanceAmount() {
        return (TextField) getChild(CHILD_TXNEXTADVANCEAMOUNT);
    }

    /**
     * 
     *  
     */
    public TextField getTxAdvanceToDateAmt() {
        return (TextField) getChild(CHILD_TXADVANCETODATEAMT);
    }

    /**
     * 
     *  
     */
    public TextField getTxAdvanceNumber() {
        return (TextField) getChild(CHILD_TXADVANCENUMBER);
    }

    /**
     * 
     *  
     */
    public ComboBox getCbUWRefiOrigPurchaseDateMonth() {
        return (ComboBox) getChild(CHILD_CBUWREFIORIGPURCHASEDATEMONTH);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiOrigPurchaseDateYear() {
        return (TextField) getChild(CHILD_TXUWREFIORIGPURCHASEDATEYEAR);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiOrigPurchaseDateDay() {
        return (TextField) getChild(CHILD_TXUWREFIORIGPURCHASEDATEDAY);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiPurpose() {
        return (TextField) getChild(CHILD_TXUWREFIPURPOSE);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiMortgageHolder() {
        return (TextField) getChild(CHILD_TXUWREFIMORTGAGEHOLDER);
    }

    /**
     * 
     *  
     */
    public RadioButtonGroup getRbBlendedAmortization() {
        return (RadioButtonGroup) getChild(CHILD_RBBLENDEDAMORTIZATION);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiOrigPurchasePrice() {
        return (TextField) getChild(CHILD_TXUWREFIORIGPURCHASEPRICE);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiImprovementValue() {
        return (TextField) getChild(CHILD_TXUWREFIIMPROVEMENTVALUE);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiOrigMtgAmount() {
        return (TextField) getChild(CHILD_TXUWREFIORIGMTGAMOUNT);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiOutstandingMtgAmount() {
        return (TextField) getChild(CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT);
    }

    /**
     * 
     *  
     */
    public TextField getTxUWRefiImprovementsDesc() {
        return (TextField) getChild(CHILD_TXUWREFIIMPROVEMENTSDESC);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStVALSData() {
        return (StaticTextField) getChild(CHILD_STVALSDATA);
    }

    /**
     * Purchase Plus Improvements
     *  txUWImprovedValue
     */
    public StaticTextField getTxUWImprovedValue() {
        return (StaticTextField) getChild(CHILD_TXUWIMPROVEDVALUE);
    }

    /**
     * Purchase Plus Improvements
     *  stDealPurposeTypeHiddenStart
     */
    public StaticTextField getStDealPurposeTypeHiddenStart() {
        return (StaticTextField) getChild(CHILD_STDEALPURPOSETYPEHIDDENSTART);
    }


    /**
     * Purchase Plus Improvements
     *  stDealPurposeTypeHiddenEnd
     */
    public StaticTextField getStDealPurposeTypeHiddenEnd() {
        return (StaticTextField) getChild(CHILD_STDEALPURPOSETYPEHIDDENEND);
    }


    /**
     * 
     *  
     */
    public doUWDealSelectMainModel getdoUWDealSelectMainModel() {
        if (doUWDealSelectMain == null) {
            doUWDealSelectMain = (doUWDealSelectMainModel) getModel(doUWDealSelectMainModel.class);
        }

        return doUWDealSelectMain;
    }

    /**
     * 
     *  
     */
    public void setdoUWDealSelectMainModel(doUWDealSelectMainModel model) {
        doUWDealSelectMain = model;
    }

    //--DJ_PT_CR--start//

    /**
     * 
     *  
     */
    public doPartySummaryModel getdoPartySummaryModel() {
        if (doPartySummary == null) {
            doPartySummary = (doPartySummaryModel) getModel(doPartySummaryModel.class);
        }

        return doPartySummary;
    }

    /**
     * 
     *  
     */
    public void setdoPartySummaryModel(doPartySummaryModel model) {
        doPartySummary = model;
    }

    //--DJ_PT_CR--end//

    /**
     * 
     *  
     */
    public doUWDealSummarySnapShotModel getdoUWDealSummarySnapShotModel() {
        if (doUWDealSummarySnapShot == null) {
            doUWDealSummarySnapShot = (doUWDealSummarySnapShotModel) getModel(doUWDealSummarySnapShotModel.class);
        }

        return doUWDealSummarySnapShot;
    }

    /**
     * 
     *  
     */
    public void setdoUWDealSummarySnapShotModel(
            doUWDealSummarySnapShotModel model) {
        doUWDealSummarySnapShot = model;
    }

    /**
     * 
     *  
     */
    public doUWBridgeModel getdoUWBridgeModel() {
        if (doUWBridge == null) {
            doUWBridge = (doUWBridgeModel) getModel(doUWBridgeModel.class);
        }

        return doUWBridge;
    }

    /**
     * 
     *  
     */
    public void setdoUWBridgeModel(doUWBridgeModel model) {
        doUWBridge = model;
    }

    /**
     * 
     *  
     */
    public doUWSourceDetailsModel getdoUWSourceDetailsModel() {
        if (doUWSourceDetails == null) {
            doUWSourceDetails = (doUWSourceDetailsModel) getModel(doUWSourceDetailsModel.class);
        }

        return doUWSourceDetails;
    }

    /**
     * 
     *  
     */
    public void setdoUWSourceDetailsModel(doUWSourceDetailsModel model) {
        doUWSourceDetails = model;
    }

    /**
     * 
     *  
     */
    public doUWMIStatusModel getdoUWMIStatusModel() {
        if (doUWMIStatus == null) {
            doUWMIStatus = (doUWMIStatusModel) getModel(doUWMIStatusModel.class);
        }

        return doUWMIStatus;
    }

    /**
     * 
     *  
     */
    public void setdoUWMIStatusModel(doUWMIStatusModel model) {
        doUWMIStatus = model;
    }

    /**
     * 
     *  
     */
    public doUWSourceInformationModel getdoUWSourceInformationModel() {
        if (doUWSourceInformation == null) {
            doUWSourceInformation = (doUWSourceInformationModel) getModel(doUWSourceInformationModel.class);
        }

        return doUWSourceInformation;
    }

    /**
     * 
     *  
     */
    public void setdoUWSourceInformationModel(doUWSourceInformationModel model) {
        doUWSourceInformation = model;
    }

    //// New hidden button for the ActiveMessage 'fake' submit button.
    public Button getBtActMsg() {
        return (Button) getChild(CHILD_BTACTMSG);
    }

    //// Populate previous links new set of methods to populate Href display
    // Strings.
    public String endHref1Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 0);
        handler.pageSaveState();

        return rc;
    }

    public String endHref2Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 1);
        handler.pageSaveState();

        return rc;
    }

    public String endHref3Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 2);
        handler.pageSaveState();

        return rc;
    }

    public String endHref4Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 3);
        handler.pageSaveState();

        return rc;
    }

    public String endHref5Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 4);
        handler.pageSaveState();

        return rc;
    }

    public String endHref6Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 5);
        handler.pageSaveState();

        return rc;
    }

    public String endHref7Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 6);
        handler.pageSaveState();

        return rc;
    }

    public String endHref8Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 7);
        handler.pageSaveState();

        return rc;
    }

    public String endHref9Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 8);
        handler.pageSaveState();

        return rc;
    }

    public String endHref10Display(ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.populatePreviousPagesLinksDisplayString(event
                .getContent(), 9);
        handler.pageSaveState();

        return rc;
    }

    //// This method overrides the getDefaultURL() framework JATO method and it
    // is located
    //// in each ViewBean. This allows not to stay with the JATO ViewBean
    // extension,
    //// otherwise each ViewBean a.k.a page should extend its Handler (to be
    // called by the framework)
    //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
    //// The full method is still in PHC base class. It should care the
    //// the url="/" case (non-initialized defaultURL) by the BX framework
    // methods.
    public String getDisplayURL() {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        ////logger = SysLog.getSysLogger("UWVB");
        String url = getDefaultDisplayURL();

        ////logger.debug("UWVB@getDisplayURL::DefaultURL: " + url);
        int languageId = handler.theSessionState.getLanguageId();

        ////logger.debug("UWVB@getDisplayURL::LanguageFromUSOSession: " +
        // languageId);
        //// Call the language specific URL (business delegation done in the
        // BXResource).
        if ((url != null) && !url.trim().equals("") && !url.trim().equals("/")) {
            url = BXResources.getBXUrl(url, languageId);
        } else {
            url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
        }

        return url;
    }

    //-- FXLink Phase II --//
    //--> New Field for Market Type Indicator
    //--> By Billy 18Nov2003
    public StaticTextField getStMarketTypeLabel() {
        return (StaticTextField) getChild(CHILD_STMARKETTYPELABEL);
    }

    public String endStMarketTypeLabelDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stTargetEscrow_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayMarketType();
        handler.pageSaveState();

        if (rc == true) {
            return handler.getMarketTypeLabel();
        } else {
            return "";
        }
    }

    public StaticTextField getStMarketType() {
        return (StaticTextField) getChild(CHILD_STMARKETTYPE);
    }

    //=========================================
    //--DJ_PT_CR--start//
    public StaticTextField getStBranchTransitLabel() {
        return (StaticTextField) getChild(CHILD_STBRANCHTRANSITLABEL);
    }

    public StaticTextField getStPartyName() {
        return (StaticTextField) getChild(CHILD_STPARTYNAME);
    }

    public StaticTextField getStBranchTransitNumber() {
        return (StaticTextField) getChild(CHILD_STBRANCHTRANSITNUM);
    }

    public StaticTextField getStAddressLine1() {
        return (StaticTextField) getChild(CHILD_STADDRESSLINE1);
    }

    public StaticTextField getStCity() {
        return (StaticTextField) getChild(CHILD_STCITY);
    }

    /**
     * 
     *  
     */
    public String endStBranchTransitNumberDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stTargetEscrow_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationInfo();
        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    public String endStBranchTransitLabelDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stTargetEscrow_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationLabel();
        handler.pageSaveState();

        if (rc == true) {
            return handler.getTransitNumberLabel();
        } else {
            return "";
        }
    }

    public String endStAddressLine1Display(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stTargetEscrow_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationInfo();
        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    public String endStPartyNameDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stTargetEscrow_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationInfo();
        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    public String endStCityDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // stTargetEscrow_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationInfo();
        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }
//  ***** Change by NBC Impl. Team - Version 1.5- Start *****//
    /**
     * getTxCashBackInDollars
     *
     * @param None <br>
     * @return TextField : the result of getTxCashBackInDollars <br>
     */
    public TextField getTxCashBackInDollars() {
        return (TextField) getChild(CHILD_TXCASHBACKINDOLLARS);
    }
    /**
     * getTxCashBackInPercentage
     *
     * @param None <br>
     * @return TextField : the result of getTxCashBackInPercentage <br>
     */
    public TextField getTxCashBackInPercentage() {
        return (TextField) getChild(CHILD_TXCASHBACKINPERCENTAGE);
    }
    /**
     * getChCashBackOverrideInDollars
     *
     * @param None <br>
     * @return CheckBox : the result of getChCashBackOverrideInDollars <br>
     */
    public CheckBox getChCashBackOverrideInDollars() {
        return (CheckBox) getChild(CHILD_CHCASHBACKOVERRIDEINDOLLARS);
    }


//  ***** Change by NBC Impl. Team - Version 1.5 - End*****//

    /**
     * 
     *  
     */
    public StaticTextField getStIncludeLifeDisLabelsStart() {
        return (StaticTextField) getChild(CHILD_STINCLUDELIFEDISLABELSSTART);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStIncludeLifeDisLabelsEnd() {
        return (StaticTextField) getChild(CHILD_STINCLUDELIFEDISLABELSEND);
    }

    /**
     * 
     *  
     */
    public StaticTextField getStGdsTdsDetailsLabel() {
        return (StaticTextField) getChild(CHILD_STGDSTDSDETAILSLABEL);
    }

    //--DJ_PT_CR--end//
    //--DJ_LDI_CR--start--//

    /**
     * 
     *  
     */
    public void handleBtLDInsuranceDetailsRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtMIReview");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleLDInsuranceDetails();

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */
    public StaticTextField getStPmntIncludingLifeDisability() {
        return (StaticTextField) getChild(CHILD_STPMNTINCLUDINGLIFEDISABILITY);
    }

    /**
     * 
     *  
     */
    public Button getBtLDInsuranceDetails() {
        return (Button) getChild(CHILD_BTLDINSURANCEDETAILS);
    }

    /**
     * 
     *  
     */
    public String endBtLDInsuranceDetailsDisplay(ChildContentDisplayEvent event) {
        // The following code block was migrated from the
        // btMIReview_onBeforeHtmlOutputEvent method
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayInsuranceDetailsButton();

        handler.pageSaveState();

        if (rc == true) {
            return event.getContent();
        } else {
            return "";
        }
    }

    //--DJ_LDI_CR--end--//
    //--FX_LINK--start--//

    /**
     * 
     *  
     */
    public Button getBtSOBDetails() {
        return (Button) getChild(CHILD_BTSOURCEOFBUSINESSDETAILS);
    }

    /**
     * 
     *  
     */
/*    public void handleBtSOBDetailsRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtMIReview");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

 //       handler.handleDetailSourceBusiness();

        handler.postHandlerProtocol();
    }*/
    //--FX_LINK--end--//
    //--DJ_CR010--start//

    /**
     * 
     *  
     */
    public TextField getTxCommisionCode() {
        return (TextField) getChild(CHILD_TXCOMMISIONCODE);
    }

    //--DJ_CR010--end//
    //--DJ_CR203.1--start//

    /**
     * 
     *  
     */
    public TextField getTxProprietairePlusLOC() {
        return (TextField) getChild(CHILD_TXPROPRIETAIREPLUSLOC);
    }

    /**
     * 
     *  
     */
    public RadioButtonGroup getRbMultiProject() {
        return (RadioButtonGroup) getChild(CHILD_RBMULTIPROJECT);
    }

    /**
     * 
     *  
     */
    public RadioButtonGroup getRbProprietairePlus() {
        return (RadioButtonGroup) getChild(CHILD_RBPROPRIETAIREPLUS);
    }

    //--DJ_CR203.1--end//
    //--DJ_CR134--start--27May2004--//

    /**
     * 
     *  
     */
    public StaticTextField getStHomeBASEProductRatePmnt() {
        return (StaticTextField) getChild(CHILD_STHOMEBASEPRODUCTRATEPMNT);
    }

    //--DJ_CR134--end--//

    //--Release3.1--begins
    //--by Hiro Apr 20, 2006

    public ComboBox getCbProductType() {
        return (ComboBox) getChild(CHILD_CBPRODUCTTYPE);
    }

    public ComboBox getCbRefiProductType() {
        return (ComboBox) getChild(CHILD_CBREFIPRODUCTTYPE);
    }

    public RadioButtonGroup getRbProgressAdvanceInspectionBy() {
        return (RadioButtonGroup) getChild(CHILD_RBPROGRESSADVANCEINSPECTIONBY);
    }

    public RadioButtonGroup getRbSelfDirectedRRSP() {
        return (RadioButtonGroup) getChild(CHILD_RBSELFDIRECTEDRRSP);
    }

    public TextField getTxCMHCProductTrackerIdentifier() {
        return (TextField) getChild(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER);
    }

    public ComboBox getCbLOCRepaymentType() {
        return (ComboBox) getChild(CHILD_CBLOCREPAYMENTTYPEID);
    }

    public RadioButtonGroup getRbRequestStandardService() {
        return (RadioButtonGroup) getChild(CHILD_RBREQUESTSTANDARDSERVICE);
    }

    public StaticTextField getStLOCAmortizationMonths() {
        return (StaticTextField) getChild(CHILD_STLOCAMORTIZATIONMONTHS);
    }


    //CR03
    public RadioButtonGroup getRbAutoCalculateCommitmentDate() {
        return (RadioButtonGroup) getChild(CHILD_RBAUTOCALCULATECOMMITMENTDATE);
    }
    //CR03

    public String endStLOCAmortizationMonthsDisplay(
            ChildContentDisplayEvent event) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setMonthsToYearsMonths(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    public StaticTextField getStLOCInterestOnlyMaturityDate() {
        return (StaticTextField) getChild(CHILD_STLOCINTERESTONLYMATURITYDATE);
    }

    //--Release3.1--ends

    //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
    /**
     * @return Object of Type ComboBox
     * 
     */
    public ComboBox getCbAffiliationProgram() {
        return (ComboBox) getChild(CHILD_CBAFFILIATIONPROGRAM);
    }

    //  ***** Change by NBC Impl. Team - Version 1.3 - End *****//

    //  ***** Change by NBC/PP Implementation Team - GCD - Start *****//

    /** Returns the <code>txCreditDecisionStatus</code> child View component */
    public StaticTextField getTxCreditDecisionStatusChild() {
        return (StaticTextField)getChild(CHILD_TX_CREDIT_DECISION_STATUS);
    }

    /** Returns the <code>txCreditDecision</code> child View component */
    public StaticTextField getTxCreditDecisionChild() {
        return (StaticTextField)getChild(CHILD_TX_CREDIT_DECISION);
    }

    /** Returns the <code>txGlobalCreditBureauScore</code> child View component */
    public StaticTextField getTxGlobalCreditBureauScoreChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE);
    }

    /** Returns the <code>txGlobalRiskRating</code> child View component */
    public StaticTextField getTxGlobalRiskRatingChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_RISK_RATING);
    }

    /** Returns the <code>txGlobalInternalScore</code> child View component */
    public StaticTextField getTxGlobalInternalScoreChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_INTERNAL_SCORE);
    }

    /** Returns the <code>txMIStatus</code> child View component */
    public StaticTextField getTxMIStatusChild() {
        return (StaticTextField)getChild(CHILD_TX_MISTATUS);
    }

    /** Returns the <code>btCreditDecisionPG</code> child View component */
    public Button getBtCreditDecisionPGChild() {
        return (Button)getChild(CHILD_BT_CREDIT_DECISION_PG);
    }

    /** Returns the <code>stIncludeGCDSumStart</code> child View component */
    public StaticTextField getStIncludeGCDSumStartChild() {
        return (StaticTextField)getChild(CHILD_ST_INCLUDE_GCDSUM_START);
    }

    /** Returns the <code>stIncludeGCDSumEnd</code> child View component */
    public StaticTextField getStIncludeGCDSumEndChild() {
        return (StaticTextField)getChild(CHILD_ST_INCLUDE_GCDSUM_END);
    }

//  ***** Change by NBC/PP Implementation Team - GCD - End *****//

    //CR03
    public HiddenField getHdCommitmentExpiryDate() {
        return (HiddenField) getChild(CHILD_HDCOMMITMENTEXPIRYDATE);
    }

    public HiddenField getHdMosProperty() {
        return (HiddenField) getChild(CHILD_HDMOSPROPERTY);
    }
    
    // Ticket 267
    public HiddenField getHdDealStatusId() {
        return (HiddenField) getChild(CHILD_HDDEALSTATUSID);
    }
    //CR03

    //***** Qualifying Rate *****//
    public ComboBox getCbQualifyProductType() {
        return (ComboBox) getChild(CHILD_CBQUALIFYPRODUCTTYPE);
    }

    public TextField getTxQualifyRate() {
        return (TextField) getChild(CHILD_TXQUALIFYRATE);
    }

    public CheckBox getChQualifyRateOverride() {
        return (CheckBox) getChild(CHILD_CHQUALIFYRATEOVERRIDE);
    }
    
    public CheckBox getChQualifyRateOverrideRate() {
        return (CheckBox) getChild(CHILD_CHQUALIFYRATEOVERRIDERATE);
    }

    public Button getBtRecalculateGDS() {
        return (Button) getChild(CHILD_BTRECALCULATEGDS);
    }
    
    public StaticTextField getStQualifyRate() {
        return (StaticTextField) getChild(CHILD_STQUALIFYRATE);
    }
    
    public StaticTextField getStQualifyProductType() {
        return (StaticTextField) getChild(CHILD_STQUALIFYPRODUCTTYPE);
    }
    
    public StaticTextField getStQualifyRateHidden() {
        return (StaticTextField) getChild(CHILD_STQUALIFYRATEHIDDEN);
    }
    
    public StaticTextField getStQualifyRateEdit() {
        return (StaticTextField) getChild(CHILD_STQUALIFYRATEEDIT);
    }
    
    public StaticTextField getSthdQualifyRate() {
        return (StaticTextField) getChild(CHILD_STHDQUALIFYRATE);
    }
    
    public StaticTextField getSthdQualifyProductId() {
        return (StaticTextField) getChild(CHILD_STHDQUALIFYPRODUCTID);
    }

    public StaticTextField getHdQualifyRateDisabled() {
        return (StaticTextField) getChild(CHILD_HDQUALIFYRATEDISABLED);
    }
    
    //5.0 MI -- start
    public TextField getTxMIResponseFirstLine() {
    	return (TextField) getChild(CHILD_TXMIRESPONSE_FIRSTLIEN);
    }
    public Button getBtExpandMIResponse() {
        return (Button) getChild(CHILD_BTEXPAND_MIRESPONSE);
    }
    //5.0 MI -- end
    
    public void handleBtRecalculateGDSRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _logger.debug("Web event invoked: " + getClass().getName()
                + ".BtRecalculateLTV");

        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleRecalculate(true);

        handler.postHandlerProtocol();
    }

    //  ***** Qualifying Rate *****//
    
    
//  ***** Change by NBC/PP Implementation Team - GCD - Start *****//  
//  GCD Summary data retrieval model begin

    public doAdjudicationResponseBNCModel getdoAdjudicationResponseBNCModel ()
    {
        if(doAdjudicationResponseBNCModel == null)
        {
            doAdjudicationResponseBNCModel = (doAdjudicationResponseBNCModel)getModel(doAdjudicationResponseBNCModel.class);
        }
        return doAdjudicationResponseBNCModel;
    }

    public void setdoGCDSummarySectionModel (doAdjudicationResponseBNCModel model)
    {
        doAdjudicationResponseBNCModel = model;
    }

    public doDealMIStatusModel getdoDealMIStatusModel()
    {
        if(doDealMIStatusModel == null)
        {
            doDealMIStatusModel = (doDealMIStatusModel)getModel(doDealMIStatusModel.class);
        }
        return doDealMIStatusModel;
    }

    public void setdoDealMIStatusModel (doDealMIStatusModel model)
    {
        doDealMIStatusModel = model;
    }

//  ***** Change by NBC/PP Implementation Team - GCD - End *****//

    ////////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Custom Methods - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    public void handleActMessageOK(String[] args) {
        UWorksheetHandler handler = (UWorksheetHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this, true);

        handler.handleActMessageOK(args);

        handler.postHandlerProtocol();
    }

    /**
     * 
     *  
     */

    //--DJ_Ticket661--start--//
    //Modified to sort by PageLabel based on the selected language

    //Not sure if this new extension will affect other 3.2 components.  
    //Used to extend OptionList
    static class CbPageNamesOptionList extends GotoOptionList {
        /**
         * 
         *  
         */
        CbPageNamesOptionList() {
        }

        public void populate(RequestContext rc) {
            Connection c = null;

            try {
                clear();

                SelectQueryModel m = null;

                SelectQueryExecutionContext eContext = new SelectQueryExecutionContext(
                        (Connection) null,
                        DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
                        DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

                if (rc == null) {
                    m = new doPageNameLabelModelImpl();
                    c = SQLConnectionManagerImpl.obtainConnection(m
                            .getDataSourceName());
                    eContext.setConnection(c);
                } else {
                    m = (SelectQueryModel) rc.getModelManager().getModel(
                            doPageNameLabelModel.class);
                }

                m.retrieve(eContext);
                m.beforeFirst();

                // Sort the results from DataObject
                TreeMap sorted = new TreeMap();

                while (m.next()) {
                    Object dfPageLabel = m
                    .getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
                    String label = ((dfPageLabel == null) ? "" : dfPageLabel
                            .toString());
                    Object dfPageId = m
                    .getValue(doPageNameLabelModel.FIELD_DFPAGEID);
                    String value = ((dfPageId == null) ? "" : dfPageId
                            .toString());
                    String[] theVal = new String[2];
                    theVal[0] = label;
                    theVal[1] = value;
                    sorted.put(label, theVal);
                }

                // Set the sorted list to the optionlist
                Iterator theList = sorted.values().iterator();
                String[] theVal = new String[2];

                while (theList.hasNext()) {
                    theVal = (String[]) (theList.next());

                    //_logger.debug("The label = " + theVal[0] + " :: Val
                    // = " + theVal[1]);
                    add(theVal[0], theVal[1]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (c != null) {
                        c.close();
                    }
                } catch (SQLException ex) {
                    // ignore
                }
            }
        }
    }

    //--Release2.1--//

    /**
     * Since the population of comboboxes on the pages should be done from
     * BXResource bundle, either the iMT converted methods could be adjusted
     * with the FETCH_DATA_STATEMENT element if place or it could be done
     * directly from the BXResource. Each approach has its pros and cons. The
     * most important is: for English and French versions could be different
     * default values. It forces to use the second approach.
     * 
     * It this case to escape annoying code clone and follow the object oriented
     * design the following abstact base class should encapsulate the combobox
     * OptionList population. Each ComboBoxOptionList inner class should extend
     * this base class and implement the BXResources table name.
     * 
     *  
     */
    static abstract class BaseComboBoxOptionList extends OptionList {
        /**
         * 
         *  
         */
        BaseComboBoxOptionList() {
        }

        //// In order to escape the double population for the splitted JSP(s):
        // UW and
        //// DE the OptionList should be repopulated and set.
        protected final void populateOptionList(RequestContext rc, int langId,
                String tablename, OptionList name) {
            try {
                String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName
                    (SessionStateModel.class);
                SessionStateModelImpl theSessionState =
                    (SessionStateModelImpl)rc.getModelManager().getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);

                //Get IDs and Labels from BXResources
                Collection<String[]> c = BXResources.getPickListValuesAndDesc
                (theSessionState.getDealInstitutionId(), tablename, langId);
                int tableSize = BXResources.getPickListTableSize
                (theSessionState.getDealInstitutionId(), tablename, langId);
                Iterator l = c.iterator();

                String[] vals = new String[tableSize];
                String[] labels = new String[tableSize];
                String[] theVal = new String[2];

                int i = 0;

                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    vals[i] = theVal[1];
                    labels[i] = theVal[0];
                    i++;
                }

                name.setOptions(vals, labels);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWChargeOptionList extends OptionList
    class CbUWChargeOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWChargeOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "LIENPOSITION",
                    cbUWChargeOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWLOBOptionList extends OptionList
    class CbUWLOBOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWLOBOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "LINEOFBUSINESS",
                    cbUWLOBOptions);
        }
    }

    //// MI Status field is combobox now in order to handel the MI process
    // outside BXP
    class CbUWMIStatusOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWMIStatusOptionList() {
        }

        /**
         * 
         *  
         */

        //// See comment in the BaseComboBoxOptionList base class.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            ////super.populate(rc, languageId, "MISTATUS");
            super.populateOptionList(rc, languageId, "MISTATUS",
                    cbUWMIStatusOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWPaymentFrequencyOptionList extends OptionList
    class CbUWPaymentFrequencyOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWPaymentFrequencyOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PAYMENTFREQUENCY",
                    cbUWPaymentFrequencyOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWPrePaymentPenaltyOptionList extends OptionList
    class CbUWPrePaymentPenaltyOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWPrePaymentPenaltyOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PREPAYMENTOPTIONS",
                    cbUWPrePaymentPenaltyOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWPrivilegePaymentOptionOptionList extends OptionList
    class CbUWPrivilegePaymentOptionOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWPrivilegePaymentOptionOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PRIVILEGEPAYMENT",
                    cbUWPrivilegePaymentOptionOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWDealPurposeOptionList extends OptionList
    class CbUWDealPurposeOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWDealPurposeOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "DEALPURPOSE",
                    cbUWDealPurposeOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWBranchOptionList extends OptionList
    class CbUWBranchOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWBranchOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "BRANCHPROFILE",
                    cbUWBranchOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWDealTypeOptionList extends OptionList
    class CbUWDealTypeOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWDealTypeOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "DEALTYPE",
                    cbUWDealTypeOptions);
        }
    }

    // SEAN GECF IV DOCUMENT TYPE DROP DOWN define the inner option list class
    class CbIVDocumentTypeOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbIVDocumentTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "INCOMEVERIFICATIONTYPE",
                    cbIVDocumentTypeOptions);
        }
    }

    // END GECF IV DOCUMENT TYPE DROP DOWN

    // SEAN DJ SPEC-Progress Advance Type July 20, 2005: define the inner option
    // list class
    class CbProgressAdvanceTypeOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbProgressAdvanceTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PROGRESSADVANCETYPE",
                    cbProgressAdvanceTypeOptions);
        }
    }

    // SEAN DJ SPEC-PAT END

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWCrossSellOptionList extends OptionList
    class CbUWCrossSellOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWCrossSellOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "CROSSSELLPROFILE",
                    cbUWCrossSellOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWSpecialFeatureOptionList extends OptionList
    class CbUWSpecialFeatureOptionList extends BaseComboBoxOptionList {
        CbUWSpecialFeatureOptionList() {
        }

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            //logger = SysLog.getSysLogger("UWVB");
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            //// super.populate(rc, languageId, "SPECIALFEATURE");
            super.populateOptionList(rc, languageId, "SPECIALFEATURE",
                    cbUWSpecialFeatureOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWReferenceTypeOptionList extends OptionList
    class CbUWReferenceTypeOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWReferenceTypeOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "REFERENCEDEALTYPE",
                    cbUWReferenceTypeOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWRepaymentTypeOptionList extends OptionList
    class CbUWRepaymentTypeOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWRepaymentTypeOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "REPAYMENTTYPE",
                    cbUWRepaymentTypeOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWFinancingProgramOptionList extends OptionList
    class CbUWFinancingProgramOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWFinancingProgramOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "FINANCEPROGRAM",
                    cbUWFinancingProgramOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWTaxPayorOptionList extends OptionList
    class CbUWTaxPayorOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWTaxPayorOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        //// See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "TAXPAYOR",
                    cbUWTaxPayorOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWMIIndicatorOptionList extends OptionList
    class CbUWMIIndicatorOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWMIIndicatorOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "MIINDICATOR",
                    cbUWMIIndicatorOptions);
        }
    }

    /**
     * 
     *  
     */

    //5.0 MI -- start
    // Delete 
    // class CbUWMITypeOptionList extends BaseComboBoxOptionList {
    //5.0 MI -- end
    
    
    //--Release2.1--//
    ////static class CbUWMIInsurerOptionList extends OptionList
    class CbUWMIInsurerOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWMIInsurerOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId,
                    "MORTGAGEINSURANCECARRIER", cbUWMIInsurerOptions);
        }
    }

    /**
     * 
     *  
     */

    //--Release2.1--//
    ////static class CbUWMIPayorOptionList extends OptionList
    class CbUWMIPayorOptionList extends BaseComboBoxOptionList {
        /**
         * 
         *  
         */
        CbUWMIPayorOptionList() {
        }

        /**
         * 
         *  
         */

        //--Release2.1--//
        public void populate(RequestContext rc) {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "MORTGAGEINSURANCEPAYOR",
                    cbUWMIPayorOptions);
        }
    }

    //--Release3.1--begins
    //--by Hiro Apr 13, 2006
    ////static class CbUWChargeOptionList extends OptionList
    class CbProductTypeOptionList extends BaseComboBoxOptionList {
        CbProductTypeOptionList() {
        }

        // --Release3.1--//
        // // See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc) {
            // Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PRODUCTTYPE",
                    cbProductTypeOptions);
        }
    }

    class CbRefiProductTypeOptionList extends BaseComboBoxOptionList {
        CbRefiProductTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PRODUCTTYPE",
                    cbRefiProductTypeOptions);
        }
    }

    class CbLOCRepaymentTypeOptionList extends BaseComboBoxOptionList {
        CbLOCRepaymentTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "LOCREPAYMENTTYPE",
                    cbLOCRepaymentTypeOptions);
        }
    }

    // --Release3.1--ends

    //  ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
    class CbAffiliationProgramOptionList extends BaseComboBoxOptionList {

        CbAffiliationProgramOptionList() {
        }

        /**
         * 
         * 
         */

        public void populate(RequestContext rc) {
            // Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "AFFILIATIONPROGRAM",
                    cbAffiliationProgramOptions);
        }
    }
    //  ***** Change by NBC Impl. Team - Version 1.3 - End *****//
    
    // -- ========== SCR#859 begins ========== --//
    //-- by Neil on Feb 14, 2005
    public StaticTextField getStCCAPS() {
        return (StaticTextField) getChild(CHILD_STCCAPS);
    }

    public StaticTextField getStServicingMortgageNumber() {
        return (StaticTextField) getChild(CHILD_STSERVICINGMORTGAGENUMBER);
    }

    //-- ========== SCR#859 ends ========== --//
    // ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
    /**
     * getTxRateGuaranteePeriod
     * 
     * 
     * @param <br>
     * 
     * @return TextField : the result of getTxRateGuaranteePeriod <br>
     * @version 1.2 <br>
     *          Date: 06/26/006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: <br>
     *          <br>
     */

    public TextField getTxRateGuaranteePeriod() {
        return (TextField) getChild(CHILD_TXRATEGUARANTEEPERIOD);
    }
    // ***** Change by NBC Impl. Team - Version 1.2 - End*****//

    // ***** Change by NBC Impl. Team - Version 50 - Start****//
    /**
     * updateCashBackAmount This method calculates the cash back amount, if the cash back override flag is not
     * checked. The cash back amount, thus calculated, will be set into the view bean
     * 
     * This method has been created to ensure that the bidirectional relationship between cash back amount and
     * cash back percent is captured. If cash back percent is changed, then cash back amount in changed. If
     * cash back amount is changed, cash back percent is changed. However, the corresponding calculation class
     * takes cash back as input and cash back percentage as output. Hence, this method, which will set the
     * cash back amount
     */
    private void updateCashBackAmount() {
        BigDecimal totalLoanAmount = new BigDecimal(0);
        String cashBackOverride = "Y";
        BigDecimal cashBackPercent = new BigDecimal(0);
        BigDecimal cashBackAmount = new BigDecimal(0);

        // If the total loan amount is present, get the value and convert to BigDecimal
        if (this.getTxUWLoanAmount().getValue() != null) {
            totalLoanAmount = (BigDecimal) this.getTxUWLoanAmount().getValue();
        }

        // If the cash back override checkbox has a value, get the value and convert to String
        if (this.getChCashBackOverrideInDollars().getValue() != null) {
            cashBackOverride = (String) this.getChCashBackOverrideInDollars().getValue();
        }

        // If the cash back percentage, get the value and convert to BigDecimal
        if (this.getTxCashBackInPercentage().getValue() != null) {
            cashBackPercent = (BigDecimal) this.getTxCashBackInPercentage().getValue();
        }

        // If the cash back amount, get the value and convert to BigDecimal
        if (this.getTxCashBackInDollars().getValue() != null) {
            cashBackAmount = (BigDecimal) this.getTxCashBackInDollars().getValue();
        }

        /*
         * Check if the cashback override flag is set to N - this implies that the cash back override checkbox
         * has not been checked and cash back percent should be used to determine the cash back amount
         */
        if (cashBackOverride.equalsIgnoreCase("N")) {
            //The result is rounded off to two decimals - this is in line with the formatting on screen
            cashBackAmount = cashBackPercent.multiply(totalLoanAmount).divide(new BigDecimal(100), 2);
            this.getTxCashBackInDollars().setValue(cashBackAmount);            
        }
    }
    // ***** Change by NBC Impl. Team - Version 50 - End****//
    
    //4.4 Submission Agent
    public pgUWRepeatedSOBTiledView getRepeatedSOB()
    {
        return (pgUWRepeatedSOBTiledView) getChild(CHILD_REPEATEDSOB);
    }
    
    /*
     * Ticket 716
     * This methods clears all the saved ETO Fields based on
     * selected Deal Purpose in Underwriter Worksheet
     */
    public void clearETOFields()
    {
    	List<String> dealPurposeIds = PicklistData.getValueList("DEALPURPOSE", "DEALPURPOSEID");
    	int index = dealPurposeIds.indexOf(String.valueOf(getCbUWDealPurpose().getValue()));
    	
    	List<String> refFlagList = PicklistData.getValueList("DEALPURPOSE", "REFINANCEFLAG");
    	String refFlag = refFlagList.get(index);

    	// checking the refinance flag
    	if (refFlag.equals("N"))
    	{
    		getCbUWRefiOrigPurchaseDateMonth().setValue(CHILD_CBUWREFIORIGPURCHASEDATEMONTH_RESET_VALUE);
            getTxUWRefiOrigPurchaseDateYear().setValue(CHILD_TXUWREFIORIGPURCHASEDATEYEAR_RESET_VALUE);
            getTxUWRefiOrigPurchaseDateDay().setValue(CHILD_TXUWREFIORIGPURCHASEDATEDAY_RESET_VALUE);
            getTxUWRefiMortgageHolder().setValue(CHILD_TXUWREFIMORTGAGEHOLDER_RESET_VALUE);
            getTxUWRefiOrigPurchasePrice().setValue(CHILD_TXUWREFIORIGPURCHASEPRICE_RESET_VALUE);
            getTxUWRefiImprovementValue().setValue(CHILD_TXUWREFIIMPROVEMENTVALUE_RESET_VALUE);
            getTxUWRefiImprovementsDesc().setValue(CHILD_TXUWREFIIMPROVEMENTSDESC_RESET_VALUE);
            getCbRefiProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);
            getTxRefiAdditionalInformation().setValue(CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE);
            getTxUWRefiPurpose().setValue(CHILD_TXUWREFIPURPOSE_RESET_VALUE);
            getRbBlendedAmortization().setValue(CHILD_RBBLENDEDAMORTIZATION_RESET_VALUE);
            getTxUWRefiOrigMtgAmount().setValue(CHILD_TXUWREFIORIGMTGAMOUNT_RESET_VALUE);
            getTxUWRefiOutstandingMtgAmount().setValue(CHILD_TXUWREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE);
            getTxUWReferenceExistingMTGNo().setValue(CHILD_TXUWREFERENCEEXISTINGMTGNO_RESET_VALUE);
    	}
    }
    
}
