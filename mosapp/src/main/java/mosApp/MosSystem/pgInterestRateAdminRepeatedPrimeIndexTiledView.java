package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;


/**
 *
 *
 *
 */
//--Ticket#781--XD_ARM/VRM--19Apr2005--//
public class pgInterestRateAdminRepeatedPrimeIndexTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgInterestRateAdminRepeatedPrimeIndexTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doPrimeIndexRateModel.class );
		registerChildren();
		initialize();
	}

	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}

	/**
	 *
	 *
	 */
	public void resetChildren()
	{
    getStIndexRateDescription().setValue(CHILD_STINDEXRATEDESCRIPTION_RESET_VALUE);
    getStLastUpdatedDate().setValue(CHILD_STLASTUPDATEDDATE_RESET_VALUE);
    getStPrimeIndexName().setValue(CHILD_STPRIMEINDEXNAME_RESET_VALUE);
    getPrimeIndexProfileId().setValue(CHILD_HDPRIMEINDEXPROFILEID_RESET_VALUE);
    getTxPrimeIndexEffRate().setValue(CHILD_TXPRIMEINDEXEFFRATE_RESET_VALUE);
    getStPrimeIndexRate().setValue(CHILD_STPRIMEINDEXRATE_RESET_VALUE);
    getCbIndexRateEffMonth().setValue(CHILD_CBINDEXRATEEFFMONTH_RESET_VALUE);
    getTxIndexRateEffDay().setValue(CHILD_TXINDEXRATEEFFDAY_RESET_VALUE);
    getTxIndexRateEffYear().setValue(CHILD_TXINDEXRATEEFFYEAR_RESET_VALUE);
    getTbHour().setValue(CHILD_TBHOUR_RESET_VALUE);
    getTbMinute().setValue(CHILD_TBMINUTE_RESET_VALUE);
    getStIncludePrimeIndxSectionStart().setValue(CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE);
    getStIncludePrimeIndxSectionEnd().setValue(CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE);
    getBtUpdatePrimeRates().setValue(CHILD_BTUPDATEPRIMERATES_RESET_VALUE);
	}

	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STINDEXRATEDESCRIPTION,StaticTextField.class);
		registerChild(CHILD_STLASTUPDATEDDATE,StaticTextField.class);
		registerChild(CHILD_STPRIMEINDEXNAME,StaticTextField.class);
		registerChild(CHILD_HDPRIMEINDEXPROFILEID,HiddenField.class);
		registerChild(CHILD_TXPRIMEINDEXEFFRATE,TextField.class);
    registerChild(CHILD_STPRIMEINDEXRATE,StaticTextField.class);
    registerChild(CHILD_CBINDEXRATEEFFMONTH, ComboBox.class);
    registerChild(CHILD_TXINDEXRATEEFFDAY, TextField.class);
    registerChild(CHILD_TXINDEXRATEEFFYEAR, TextField.class);
    registerChild(CHILD_TBHOUR,TextField.class);
    registerChild(CHILD_TBMINUTE,TextField.class);
    registerChild(CHILD_STINCLUDEPRIMEINDXSECTIONSTART,StaticTextField.class);
    registerChild(CHILD_STINCLUDEPRIMEINDXSECTIONEND,StaticTextField.class);
    registerChild(CHILD_BTUPDATEPRIMERATES,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoPrimeIndexRateModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.checkAndSyncDOWithPrimeRateCursor(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
    boolean movedToRow = super.nextTile();
    logger = SysLog.getSysLogger("PRINXTV");

    if (movedToRow)
    {
      int rowNum = this.getTileIndex();
      boolean ret = true;

      RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      logger.debug("LDTV2@RetvalBefore: " + ret + " , RowIndx: " + rowNum);

      //ret = handler.checkDisplayThisRow2("RepeatedPrimeIndex");
     //logger.debug("LDTV2@RetvalAfter: " + ret + " , RowIndx: " + rowNum);

     doPrimeIndexRateModelImpl m =(doPrimeIndexRateModelImpl)this.getdoPrimeIndexRateModel();

     logger.debug("LDTV2@ModelSize: " + m.getSize());

      if (m.getSize() > 0 && ret == true)
      {
        logger.debug("LDTV2@nextTile::Location: " + m.getLocation());
        handler.setPrimeIndexRateDisplayFields(m, rowNum);
      }
      else { ret = false; }
      handler.pageSaveState();

      return ret;
    }

    return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STINDEXRATEDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
        getdoPrimeIndexRateModel(),
				CHILD_STINDEXRATEDESCRIPTION,
        getdoPrimeIndexRateModel().FIELD_DFPRIMEINDEXDESCRIPTION,
				CHILD_STINDEXRATEDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
    else
		if (name.equals(CHILD_STLASTUPDATEDDATE))
		{
			StaticTextField child = new StaticTextField(this,
      getdoPrimeIndexRateModel(),
				CHILD_STLASTUPDATEDDATE,
        getdoPrimeIndexRateModel().FIELD_DFEFFECTIVEDATE,
				CHILD_STLASTUPDATEDDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPRIMEINDEXNAME))
		{
			StaticTextField child = new StaticTextField(this,
       getdoPrimeIndexRateModel(),
				CHILD_STPRIMEINDEXNAME,
        getdoPrimeIndexRateModel().FIELD_DFPRIMEINDEXNAME,
				CHILD_STPRIMEINDEXNAME_RESET_VALUE,
				null);
			return child;
		}
    else
    if(name.equals(CHILD_CBINDEXRATEEFFMONTH))
    {
      ComboBox child = new ComboBox(this,
        //getDefaultModel(),
        getdoPrimeIndexRateModel(),
        CHILD_CBINDEXRATEEFFMONTH,
        CHILD_CBINDEXRATEEFFMONTH,
        CHILD_CBINDEXRATEEFFMONTH_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbIndexRateEffMonthOptions);
      return child;
    }
    else
    if(name.equals(CHILD_TXINDEXRATEEFFDAY))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoPrimeIndexRateModel(),
        CHILD_TXINDEXRATEEFFDAY,
        CHILD_TXINDEXRATEEFFDAY,
        CHILD_TXINDEXRATEEFFDAY_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXINDEXRATEEFFYEAR))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoPrimeIndexRateModel(),
        CHILD_TXINDEXRATEEFFYEAR,
        CHILD_TXINDEXRATEEFFYEAR,
        CHILD_TXINDEXRATEEFFYEAR_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_TBHOUR))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoPrimeIndexRateModel(),
        CHILD_TBHOUR,
        CHILD_TBHOUR,
        CHILD_TBHOUR_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_TBMINUTE))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoPrimeIndexRateModel(),
        CHILD_TBMINUTE,
        CHILD_TBMINUTE,
        CHILD_TBMINUTE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_TXPRIMEINDEXEFFRATE))
    {
      TextField child = new TextField(this,
        //getDefaultModel(),
        getdoPrimeIndexRateModel(),
        CHILD_TXPRIMEINDEXEFFRATE,
        CHILD_TXPRIMEINDEXEFFRATE,
        CHILD_TXPRIMEINDEXEFFRATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if(name.equals(CHILD_STPRIMEINDEXRATE))
    {
      TextField child = new TextField(this,
        getdoPrimeIndexRateModel(),
        CHILD_STPRIMEINDEXRATE,
        getdoPrimeIndexRateModel().FIELD_DFPRIMEINDEXRATE,
        CHILD_STPRIMEINDEXRATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_HDPRIMEINDEXPROFILEID))
    {
      HiddenField child = new HiddenField(this,
        getdoPrimeIndexRateModel(),
        CHILD_HDPRIMEINDEXPROFILEID,
        getdoPrimeIndexRateModel().FIELD_DFPRIMEINDEXRATEPROFILEID,
        CHILD_HDPRIMEINDEXPROFILEID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STINCLUDEPRIMEINDXSECTIONSTART))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART,
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART,
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STINCLUDEPRIMEINDXSECTIONEND))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEPRIMEINDXSECTIONEND,
        CHILD_STINCLUDEPRIMEINDXSECTIONEND,
        CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_BTUPDATEPRIMERATES))
    {
      Button child = new Button(
        this,
        getdoPrimeIndexRateModel(),
        //getDefaultModel(),
        CHILD_BTUPDATEPRIMERATES,
        CHILD_BTUPDATEPRIMERATES,
        CHILD_BTUPDATEPRIMERATES_RESET_VALUE,
        null);
        return child;
    }
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}

	/**
	 *
	 *
	 */
	public void handleBtUpdatePrimeRatesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    // The following code block was migrated from the btUpdate_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());


    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger = SysLog.getSysLogger("PRINXTV");
    //logger.debug("pgUpdatePrimeIndexRateRequest::TileIndexNew: " + tileIndx);

    handler.handleOnPrimeIndexRateUpdate(handler.getRowNdxFromWebEventMethod(event));
    //handler.handleOnPrimeIndexRateUpdate(tileIndx);

		handler.postHandlerProtocol();
	}

  /**
   *
   *
   */
  public Button getBtUpdatePrimeRates()
  {
    return (Button)getChild(CHILD_BTUPDATEPRIMERATES);
  }

  /**
   *
   *
   */
  public String endBtUpdatePrimeRatesDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btUpdate_onBeforeHtmlOutputEvent method
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());

    boolean rc = handler.viewUpdateIndexRateButton();
    logger = SysLog.getSysLogger("PRINXTV1");

    logger.debug("pgUpdatePrimeIndexRateRequest::RCode: " + rc);

    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";
  }

  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_REFRESH);
    // Have to re-execute the Model here.  Otherwise, not working
    executeAutoRetrievingModels();
  }

  /**
   *
   *
   */
  public ComboBox getCbIndexRateEffMonth()
  {
    return(ComboBox)getChild(CHILD_CBINDEXRATEEFFMONTH);
  }

  /**
   *
   *
   */
  public TextField getTxIndexRateEffDay()
  {
    return(TextField)getChild(CHILD_TXINDEXRATEEFFDAY);
  }

  /**
   *
   *
   */
  public TextField getTxIndexRateEffYear()
  {
    return(TextField)getChild(CHILD_TXINDEXRATEEFFYEAR);
  }


  /**
   *
   *
   */
  public TextField getTbHour()
  {
    return (TextField)getChild(CHILD_TBHOUR);
  }


  /**
   *
   *
   */
  public TextField getTbMinute()
  {
    return (TextField)getChild(CHILD_TBMINUTE);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludePrimeIndxSectionStart()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDEPRIMEINDXSECTIONSTART);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludePrimeIndxSectionEnd()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDEPRIMEINDXSECTIONEND);
  }


	/**
	 *
	 *
	 */
	public StaticTextField getStIndexRateDescription()
	{
		return (StaticTextField)getChild(CHILD_STINDEXRATEDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLastUpdatedDate()
	{
		return (StaticTextField)getChild(CHILD_STLASTUPDATEDDATE);
	}
	/**
	 *
	 *
	 */
	public StaticTextField getStPrimeIndexName()
	{
		return (StaticTextField)getChild(CHILD_STPRIMEINDEXNAME);
	}


  /**
   *
   *
   */
  public HiddenField getPrimeIndexProfileId()
  {
    return(HiddenField)getChild(CHILD_HDPRIMEINDEXPROFILEID);
  }

  /**
   *
   *
   */
  public TextField getTxPrimeIndexEffRate()
  {
    return(TextField)getChild(CHILD_TXPRIMEINDEXEFFRATE);
  }

  /**
   *
   *
   */
  public TextField getStPrimeIndexRate()
  {
    return(TextField)getChild(CHILD_STPRIMEINDEXRATE);
  }

	/**
	 *
	 *
	 */
	public doPrimeIndexRateModel getdoPrimeIndexRateModel()
	{
		if (doPrimeIndexRate == null)
			doPrimeIndexRate = (doPrimeIndexRateModel) getModel(doPrimeIndexRateModel.class);
		return doPrimeIndexRate;
	}


	/**
	 *
	 *
	 */
	public void setdoPrimeIndexRateModel(doPrimeIndexRateModel model)
	{
			doPrimeIndexRate = model;
	}


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;

	public static final String CHILD_BTUPDATEPRIMERATES="btUpdatePrimeRates";
	public static final String CHILD_BTUPDATEPRIMERATES_RESET_VALUE="";

	public static final String CHILD_STINDEXRATEDESCRIPTION="stIndexRateDescription";
	public static final String CHILD_STINDEXRATEDESCRIPTION_RESET_VALUE="";

	public static final String CHILD_STLASTUPDATEDDATE="stLastUpdateDate";
	public static final String CHILD_STLASTUPDATEDDATE_RESET_VALUE="";

  public static final String CHILD_STPRIMEINDEXNAME="stPrimeIndexName";
  public static final String CHILD_STPRIMEINDEXNAME_RESET_VALUE="";

  public static final String CHILD_HDPRIMEINDEXPROFILEID="hdPrimeIndexProfileId";
  public static final String CHILD_HDPRIMEINDEXPROFILEID_RESET_VALUE="";

  public static final String CHILD_TXPRIMEINDEXEFFRATE="txPrimeIndexEffRate";
  public static final String CHILD_TXPRIMEINDEXEFFRATE_RESET_VALUE="";

  public static final String CHILD_STPRIMEINDEXRATE="stPrimeIndexRate";
  public static final String CHILD_STPRIMEINDEXRATE_RESET_VALUE="";

  public static final String CHILD_CBINDEXRATEEFFMONTH="cbIndexRateEffMonth";
  public static final String CHILD_CBINDEXRATEEFFMONTH_RESET_VALUE="";
  private OptionList cbIndexRateEffMonthOptions = new OptionList(new String[] {} , new String[] {});

  public static final String CHILD_TXINDEXRATEEFFYEAR="txIndexRateEffYear";
  public static final String CHILD_TXINDEXRATEEFFYEAR_RESET_VALUE="";

  public static final String CHILD_TXINDEXRATEEFFDAY="txIndexRateEffDay";
  public static final String CHILD_TXINDEXRATEEFFDAY_RESET_VALUE="";

  public static final String CHILD_TBHOUR="txIndexRateEffHour";
  public static final String CHILD_TBHOUR_RESET_VALUE="";

  public static final String CHILD_TBMINUTE="txIndexRateEffMinute";
  public static final String CHILD_TBMINUTE_RESET_VALUE="";

  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONSTART="stIncludePrimeIndxSectionStart";
  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE="";

  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONEND="stIncludePrimeIndxSectionEnd";
  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE="";

  public static final String CHILD_STPENDINGPRIMERATE="stPendingPrimeRate";
  public static final String CHILD_STPENDINGPRIMERATE_RESET_VALUE="";

  public static final String CHILD_STPRIMERATEEFFDATE="stPendingPrimeRate";
  public static final String CHILD_STPRIMERATEEFFDATE_RESET_VALUE="";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doPrimeIndexRateModel doPrimeIndexRate=null;
  private RateAdminHandler handler=new RateAdminHandler();
  public SysLogger logger;

}

