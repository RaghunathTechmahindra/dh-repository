package mosApp.MosSystem;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;

import com.basis100.deal.util.StringUtil;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.JspChildDisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ListBox;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 *
 */
public class pgMWorkQueueViewBean extends ExpressViewBeanBase
{
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////
  public static final String PAGE_NAME = "pgMWorkQueue";

  //// It is a variable now (see explanation in the getDisplayURL() method above.
  ////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgMWorkQueue.jsp";
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_REPEATED1 = "Repeated1";
  public static final String CHILD_REPEATED2 = "Repeated2";
  public static final String CHILD_ROWSDISPLAYED = "rowsDisplayed";
  public static final String CHILD_ROWSDISPLAYED_RESET_VALUE = "";
  public static final String CHILD_STUSERNAMETITLE = "stUserNameTitle";
  public static final String CHILD_STUSERNAMETITLE_RESET_VALUE = "";
  public static final String CHILD_STTODAYDATE = "stTodayDate";
  public static final String CHILD_STTODAYDATE_RESET_VALUE = "";
  public static final String CHILD_STCOMPANYNAME = "stCompanyName";
  public static final String CHILD_STCOMPANYNAME_RESET_VALUE = "";
  public static final String CHILD_STPAGELABEL = "stPageLabel";
  public static final String CHILD_STPAGELABEL_RESET_VALUE = "";
  public static final String CHILD_CBPAGENAMES = "cbPageNames";
  public static final String CHILD_CBPAGENAMES_RESET_VALUE = "";
  public static final String CHILD_BTPROCEED = "btProceed";
  public static final String CHILD_BTPROCEED_RESET_VALUE = "Proceed";
  public static final String CHILD_STESTCLOSINGDATE = "stEstClosingDate";
  public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE = "";
  public static final String CHILD_STSPECIALFEATURE = "stSpecialFeature";
  public static final String CHILD_STSPECIALFEATURE_RESET_VALUE = "";
  public static final String CHILD_STPMTTERM = "stPmtTerm";
  public static final String CHILD_STPMTTERM_RESET_VALUE = "";
  public static final String CHILD_STPURCHASEPRICE = "stPurchasePrice";
  public static final String CHILD_STPURCHASEPRICE_RESET_VALUE = "";
  public static final String CHILD_STDEALPURPOSE = "stDealPurpose";
  public static final String CHILD_STDEALPURPOSE_RESET_VALUE = "";
  public static final String CHILD_STDEALTYPE = "stDealType";
  public static final String CHILD_STDEALTYPE_RESET_VALUE = "";
  public static final String CHILD_STLOB = "stLOB";
  public static final String CHILD_STLOB_RESET_VALUE = "";
  public static final String CHILD_STSOURCE = "stSource";
  public static final String CHILD_STSOURCE_RESET_VALUE = "";
  public static final String CHILD_STSOURCEFIRM = "stSourceFirm";
  public static final String CHILD_STSOURCEFIRM_RESET_VALUE = "";
  public static final String CHILD_STDEALSTATUSDATE = "stDealStatusDate";
  public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE = "";
  public static final String CHILD_STDEALSTATUS = "stDealStatus";
  public static final String CHILD_STDEALSTATUS_RESET_VALUE = "";
  public static final String CHILD_STDEALID = "stDealId";
  public static final String CHILD_STDEALID_RESET_VALUE = "";
  public static final String CHILD_HREF1 = "Href1";
  public static final String CHILD_HREF1_RESET_VALUE = "";
  public static final String CHILD_HREF2 = "Href2";
  public static final String CHILD_HREF2_RESET_VALUE = "";
  public static final String CHILD_HREF3 = "Href3";
  public static final String CHILD_HREF3_RESET_VALUE = "";
  public static final String CHILD_HREF4 = "Href4";
  public static final String CHILD_HREF4_RESET_VALUE = "";
  public static final String CHILD_HREF5 = "Href5";
  public static final String CHILD_HREF5_RESET_VALUE = "";
  public static final String CHILD_HREF6 = "Href6";
  public static final String CHILD_HREF6_RESET_VALUE = "";
  public static final String CHILD_HREF7 = "Href7";
  public static final String CHILD_HREF7_RESET_VALUE = "";
  public static final String CHILD_HREF8 = "Href8";
  public static final String CHILD_HREF8_RESET_VALUE = "";
  public static final String CHILD_HREF9 = "Href9";
  public static final String CHILD_HREF9_RESET_VALUE = "";
  public static final String CHILD_HREF10 = "Href10";
  public static final String CHILD_HREF10_RESET_VALUE = "";
  public static final String CHILD_TBDEALID = "tbDealId";
  public static final String CHILD_TBDEALID_RESET_VALUE = "";
  public static final String CHILD_BTBACKWARDBUTTON = "btBackwardButton";
  public static final String CHILD_BTBACKWARDBUTTON_RESET_VALUE = "Previous";
  public static final String CHILD_BTFILTERSUBMITBUTTON = "btFilterSubmitButton";
  public static final String CHILD_BTFILTERSUBMITBUTTON_RESET_VALUE = "Filter Pop-up Submit";
  public static final String CHILD_CBUSERFILTER = "cbUserFilter";
  public static final String CHILD_CBUSERFILTER_RESET_VALUE = "";
  public static final String CHILD_STUSERFILTER = "stUserFilter";
  public static final String CHILD_STUSERFILTER_RESET_VALUE = "";
  public static final String CHILD_TBDEALIDFILTER = "tbDealIdFilter";
  public static final String CHILD_TBDEALIDFILTER_RESET_VALUE = "";
  public static final String CHILD_BTSORTBUTTON = "btSortButton";
  public static final String CHILD_BTSORTBUTTON_RESET_VALUE = "Sort Pop-up Submit";
  public static final String CHILD_CBSORTOPTIONS = "cbSortOptions";
  public static final String CHILD_CBSORTOPTIONS_RESET_VALUE = "0";

  //// access has been changed to manually manipulate with the OptionList via the handler
  ////private static OptionList cbSortOptionsOptions=new OptionList(new String[]{},new String[]{});
  protected static OptionList cbSortOptionsOptions =
    new OptionList(new String[] {  }, new String[] {  });
  public static final String CHILD_STSORTOPTION = "stSortOption";
  public static final String CHILD_STSORTOPTION_RESET_VALUE = "";
  public static final String CHILD_BTFILTERSORTRESET = "btFilterSortReset";
  public static final String CHILD_BTFILTERSORTRESET_RESET_VALUE = " ";

  //// access has been changed to manually manipulate with the OptionList via the handler
  ////private static OptionList cbStatusFilterOptions=new OptionList(new String[]{},new String[]{});
  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screwed up the population.
  protected static OptionList cbStatusFilterOptions =
    new OptionList(new String[] {  }, new String[] {  });
  public static final String CHILD_CBSTATUSFILTER = "cbStatusFilter";
  public static final String CHILD_CBSTATUSFILTER_RESET_VALUE = "";
  public static final String CHILD_STSTATUSFILTER = "stStatusFilter";
  public static final String CHILD_STSTATUSFILTER_RESET_VALUE = "";
  public static final String CHILD_CHANGEPASSWORDHREF = "changePasswordHref";
  public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE = "";
  public static final String CHILD_BTTOOLHISTORY = "btToolHistory";
  public static final String CHILD_BTTOOLHISTORY_RESET_VALUE = " ";
  public static final String CHILD_BTTOONOTES = "btTooNotes";
  public static final String CHILD_BTTOONOTES_RESET_VALUE = " ";
  public static final String CHILD_BTTOOLSEARCH = "btToolSearch";
  public static final String CHILD_BTTOOLSEARCH_RESET_VALUE = " ";
  public static final String CHILD_BTTOOLLOG = "btToolLog";
  public static final String CHILD_BTTOOLLOG_RESET_VALUE = " ";
  public static final String CHILD_BTWORKQUEUELINK = "btWorkQueueLink";
  public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE = " ";
  public static final String CHILD_STERRORFLAG = "stErrorFlag";
  public static final String CHILD_STERRORFLAG_RESET_VALUE = "N";
  public static final String CHILD_BTFORWARDBUTTON = "btForwardButton";
  public static final String CHILD_BTFORWARDBUTTON_RESET_VALUE = "Next";
  public static final String CHILD_BTSUBMIT = "btSubmit";
  public static final String CHILD_BTSUBMIT_RESET_VALUE = " ";
  public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";
  public static final String CHILD_DETECTALERTTASKS_RESET_VALUE = "";
  public static final String CHILD_STTOTALLOANAMOUNT = "stTotalLoanAmount";
  public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE = "";
  public static final String CHILD_STBORRFIRSTNAME = "stBorrFirstName";
  public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE = "";
  public static final String CHILD_SESSIONUSERID = "sessionUserId";
  public static final String CHILD_SESSIONUSERID_RESET_VALUE = "";
  public static final String CHILD_STPMGENERATE = "stPmGenerate";
  public static final String CHILD_STPMGENERATE_RESET_VALUE = "";
  public static final String CHILD_STPMHASTITLE = "stPmHasTitle";
  public static final String CHILD_STPMHASTITLE_RESET_VALUE = "";
  public static final String CHILD_STPMHASINFO = "stPmHasInfo";
  public static final String CHILD_STPMHASINFO_RESET_VALUE = "";
  public static final String CHILD_STPMHASTABLE = "stPmHasTable";
  public static final String CHILD_STPMHASTABLE_RESET_VALUE = "";
  public static final String CHILD_STPMHASOK = "stPmHasOk";
  public static final String CHILD_STPMHASOK_RESET_VALUE = "";
  public static final String CHILD_STPMTITLE = "stPmTitle";
  public static final String CHILD_STPMTITLE_RESET_VALUE = "";
  public static final String CHILD_STPMINFOMSG = "stPmInfoMsg";
  public static final String CHILD_STPMINFOMSG_RESET_VALUE = "";
  public static final String CHILD_STPMONOK = "stPmOnOk";
  public static final String CHILD_STPMONOK_RESET_VALUE = "";
  public static final String CHILD_STPMMSGS = "stPmMsgs";
  public static final String CHILD_STPMMSGS_RESET_VALUE = "";
  public static final String CHILD_STPMMSGTYPES = "stPmMsgTypes";
  public static final String CHILD_STPMMSGTYPES_RESET_VALUE = "";
  public static final String CHILD_STAMGENERATE = "stAmGenerate";
  public static final String CHILD_STAMGENERATE_RESET_VALUE = "";
  public static final String CHILD_STAMHASTITLE = "stAmHasTitle";
  public static final String CHILD_STAMHASTITLE_RESET_VALUE = "";
  public static final String CHILD_STAMHASINFO = "stAmHasInfo";
  public static final String CHILD_STAMHASINFO_RESET_VALUE = "";
  public static final String CHILD_STAMHASTABLE = "stAmHasTable";
  public static final String CHILD_STAMHASTABLE_RESET_VALUE = "";
  public static final String CHILD_STAMTITLE = "stAmTitle";
  public static final String CHILD_STAMTITLE_RESET_VALUE = "";
  public static final String CHILD_STAMINFOMSG = "stAmInfoMsg";
  public static final String CHILD_STAMINFOMSG_RESET_VALUE = "";
  public static final String CHILD_STAMMSGS = "stAmMsgs";
  public static final String CHILD_STAMMSGS_RESET_VALUE = "";
  public static final String CHILD_STAMMSGTYPES = "stAmMsgTypes";
  public static final String CHILD_STAMMSGTYPES_RESET_VALUE = "";
  public static final String CHILD_STAMDIALOGMSG = "stAmDialogMsg";
  public static final String CHILD_STAMDIALOGMSG_RESET_VALUE = "";
  public static final String CHILD_STAMBUTTONSHTML = "stAmButtonsHtml";
  public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE = "";

  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG = "btActMsg";
  public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";

  // Added CheckBox to Hide or Display Hidden Tasks
  // By Billy 09Oct2002
  public static final String CHILD_CHISDISPLAYHIDDEN = "chIsDisplayHidden";
  public static final String CHILD_CHISDISPLAYHIDDEN_RESET_VALUE = "F";

  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session value throughout all modulus.
  public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";
  public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

  //--> Added hidden fields for Scrolling JavaScript Function
  //--> By Billy 21July2003
  public static final String CHILD_HDSELECTEDROWID = "hdSelectedRowID";
  public static final String CHILD_HDSELECTEDROWID_RESET_VALUE = "";
  public static final String CHILD_HDCURRENTFIRSTROWID = "hdCurrentFirstRowID";
  public static final String CHILD_HDCURRENTFIRSTROWID_RESET_VALUE = "";

  //--TD_MWQ_CR--start//
  public static final String CHILD_REPEATED3 = "Repeated3";
  public static final String CHILD_CBTASKNAMEFILTER = "cbTaskNameFilter";
  public static final String CHILD_CBTASKNAMEFILTER_RESET_VALUE = "";
  public static final String CHILD_CBDEALSTATUSFILTER = "cbDealStatusFilter";
  public static final String CHILD_CBDEALSTATUSFILTER_RESET_VALUE = "";

  //-- ========== SCR#750   ends ========== --//
  public static final String CHILD_LBMWQBRANCHES = "lbMWQBranches";
  public static final String CHILD_LBMWQBRANCHES_RESET_VALUE = "";
  public static final String CHILD_LBMWQGROUPS = "lbMWQGroups";
  public static final String CHILD_LBMWQGROUPS_RESET_VALUE = "";
  public static final String CHILD_STTASKNAME = "stTaskName";
  public static final String CHILD_STTASKNAME_RESET_VALUE = "";
  public static final String CHILD_STTASKNAMEFULL = "stTaskNameFull";
  public static final String CHILD_STTASKNAMEFULL_RESET_VALUE = "";
  public static final String CHILD_STLOCATIONFULL = "stLocationFull";
  public static final String CHILD_STLOCATIONFULL_RESET_VALUE = "";

  public static final String CHILD_CBINSTITUTION = "cbInstitution";
  public static final String CHILD_CBINSTITUTION_RESET_VALUE = "";
  //--TD_MWQ_CR--end//

  // new hidden field for ML to identify the user is multiple access or not  
  // to control to display filter/sort criteria
  public static final String CHILD_MULTIACCESS = "multiAccess";
  public static final String CHILD_MULTIACCESS_RESET_VALUE = "";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screwed up the population.
  ////private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions = new CbPageNamesOptionList();
  private CbInstitutionOptions cbInstitutionOptions = new CbInstitutionOptions();
  ////Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screwed up the population.
  ////private static CbUserFilterOptionList cbUserFilterOptions=new CbUserFilterOptionList();
  protected CbUserFilterOptionList cbUserFilterOptions = new CbUserFilterOptionList();

  ////Added the Variable for NonSelected Label
  private String CBUSERFILTER_ALL_LABEL = "";

  ////protected CbStatusFilterOptionList cbStatusFilterOptions=new CbStatusFilterOptionList();
  ////Added the Variable for NonSelected Label
  private String CBSTATUSFILTER_NONE_SELECTED_LABEL = "";
  protected CbTaskNameFilterOptionList cbTaskNameFilterOptions = new CbTaskNameFilterOptionList();
  private String CBTASKNAMEFILTER_ALL_SELECTED_LABEL = "";

  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/21/2004
  protected CbDealStatusFilterOptionList cbDealStatusFilterOptions =
    new CbDealStatusFilterOptionList();
  private String CBDEALSTATUSFILTER_ALL_SELECTED_LABEL = "";
  private LbMWQBranchesOptionList lbMWQBranchesOptions = new LbMWQBranchesOptionList();
  private String LBMWQBRANCHES_ALL_SELECTED_LABEL = "";
  private LbMWQGroupsOptionList lbMWQGroupsOptions = new LbMWQGroupsOptionList();
  private String LBMWQGROUPS_ALL_SELECTED_LABEL = "";

  public static final String CHILD_CBTASKEXPIRYDATESTARTFILTER = "cbTaskExpiryDateStartFilter";
  public static final String CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE = "";
  public static final String CHILD_CBTASKEXPIRYDATEENDFILTER = "cbTaskExpiryDateEndFilter";
  public static final String CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE = "";
	
  
  // FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- start
  public static final String CHILD_TXWINDOWSCROLLPOSITION = "txWindowScrollPosition";
  public static final String CHILD_TXWINDOWSCROLLPOSITION_RESET_VALUE = "";
  // FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- end
	
  
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
  private doDealSummarySnapShotModel doDealSummarySnapShot = null;

  private String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgMWorkQueue.jsp";

  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  // MigrationToDo : Migrate custom member
  private MasterWorkQueueHandler handler = new MasterWorkQueueHandler();
  public SysLogger logger;

  /**
   *
   *
   */
  public pgMWorkQueueViewBean()
  {
    super(PAGE_NAME);
    setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

    registerChildren();

    initialize();
  }

  /**
   *
   *
   */
  protected void initialize()
  {
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  protected View createChild(String name)
  {
      View superReturn = super.createChild(name);
      if (superReturn != null) {
          return superReturn;
      } 
      else if (name.equals(CHILD_REPEATED1))
    {
      pgMWorkQueueRepeated1TiledView child =
        new pgMWorkQueueRepeated1TiledView(this, CHILD_REPEATED1);

      return child;
    }
    else if (name.equals(CHILD_REPEATED2))
    {
      pgMWorkQueueRepeated2TiledView child =
        new pgMWorkQueueRepeated2TiledView(this, CHILD_REPEATED2);

      return child;
    }

    //--TD_MWQ_CR--start//
    else if (name.equals(CHILD_REPEATED3))
    {
      pgMWorkQueueRepeated3TiledView child =
        new pgMWorkQueueRepeated3TiledView(this, CHILD_REPEATED3);

      return child;
    }
    else if (name.equals(CHILD_CBTASKNAMEFILTER))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBTASKNAMEFILTER, CHILD_CBTASKNAMEFILTER,
          CHILD_CBTASKNAMEFILTER_RESET_VALUE, null);

      ////child.setLabelForNoneSelected("CBTASKNAMEFILTER_ALL_SELECTED_LABEL");
      child.setLabelForNoneSelected(" ");
      child.setOptions(cbTaskNameFilterOptions);

      return child;

      //-- ========== SCR#750 begins ========== --//
      //-- by Neil on Dec/21/2004
    }
    else if (name.equals(CHILD_CBDEALSTATUSFILTER))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBDEALSTATUSFILTER, CHILD_CBDEALSTATUSFILTER,
          CHILD_CBDEALSTATUSFILTER_RESET_VALUE, null);
      child.setLabelForNoneSelected(" ");
      child.setOptions(cbDealStatusFilterOptions);

      return child;

      //-- ========== SCR#750 ends ========== --//
    }
    else if (name.equals(CHILD_LBMWQBRANCHES))
    {
      ListBox child =
        new ListBox(this, getDefaultModel(), CHILD_LBMWQBRANCHES, CHILD_LBMWQBRANCHES,
          CHILD_LBMWQBRANCHES_RESET_VALUE, null);
      child.setOptions(lbMWQBranchesOptions);
      child.setMultiSelect(true);

      return child;
    }
    else if (name.equals(CHILD_LBMWQGROUPS))
    {
      ListBox child =
        new ListBox(this, getDefaultModel(), CHILD_LBMWQGROUPS, CHILD_LBMWQGROUPS,
          CHILD_LBMWQGROUPS_RESET_VALUE, null);
      child.setOptions(lbMWQGroupsOptions);
      child.setMultiSelect(true);

      return child;
    }
    else if (name.equals(CHILD_STTASKNAME))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STTASKNAME, CHILD_STTASKNAME,
          CHILD_STTASKNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKNAMEFULL))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STTASKNAMEFULL, CHILD_STTASKNAMEFULL,
          CHILD_STTASKNAMEFULL_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STLOCATIONFULL))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STLOCATIONFULL, CHILD_STLOCATIONFULL,
          CHILD_STLOCATIONFULL_RESET_VALUE, null);

      return child;
    }

    //--TD_MWQ_CR--end//
    else if (name.equals(CHILD_ROWSDISPLAYED))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_ROWSDISPLAYED, CHILD_ROWSDISPLAYED,
          CHILD_ROWSDISPLAYED_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STUSERNAMETITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STUSERNAMETITLE, CHILD_STUSERNAMETITLE,
          CHILD_STUSERNAMETITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTODAYDATE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STTODAYDATE, CHILD_STTODAYDATE,
          CHILD_STTODAYDATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STCOMPANYNAME))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STCOMPANYNAME, CHILD_STCOMPANYNAME,
          CHILD_STCOMPANYNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPAGELABEL))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPAGELABEL, CHILD_STPAGELABEL,
          CHILD_STPAGELABEL_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CBPAGENAMES))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBPAGENAMES, CHILD_CBPAGENAMES,
          CHILD_CBPAGENAMES_RESET_VALUE, null);

      //--Release2.1--//
      ////Modified to set NonSelected Label as a CHILD_CBPAGENAMES_NONSELECTED_LABEL
      ////child.setLabelForNoneSelected("Choose a Page");
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
      child.setOptions(cbPageNamesOptions);

      return child;
    }
    else if (name.equals(CHILD_BTPROCEED))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTPROCEED, CHILD_BTPROCEED,
          CHILD_BTPROCEED_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STESTCLOSINGDATE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STESTCLOSINGDATE,
          doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE, CHILD_STESTCLOSINGDATE_RESET_VALUE,
          null);

      return child;
    }
    else if (name.equals(CHILD_STSPECIALFEATURE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STSPECIALFEATURE,
          doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE, CHILD_STSPECIALFEATURE_RESET_VALUE,
          null);

      return child;
    }
    else if (name.equals(CHILD_STPMTTERM))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STPMTTERM,
          doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR, CHILD_STPMTTERM_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPURCHASEPRICE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STPURCHASEPRICE,
          doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE, CHILD_STPURCHASEPRICE_RESET_VALUE,
          null);

      return child;
    }
    else if (name.equals(CHILD_STDEALPURPOSE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STDEALPURPOSE,
          doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR, CHILD_STDEALPURPOSE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STDEALTYPE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STDEALTYPE,
          doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR, CHILD_STDEALTYPE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STLOB))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STLOB,
          doDealSummarySnapShotModel.FIELD_DFLOBDESCR, CHILD_STLOB_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STSOURCE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STSOURCE,
          doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME, CHILD_STSOURCE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STSOURCEFIRM))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STSOURCEFIRM,
          doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME, CHILD_STSOURCEFIRM_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STDEALSTATUSDATE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STDEALSTATUSDATE,
          doDealSummarySnapShotModel.FIELD_DFSTATUSDATE, CHILD_STDEALSTATUSDATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STDEALSTATUS))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STDEALSTATUS,
          doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR, CHILD_STDEALSTATUS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STDEALID))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STDEALID,
          doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID, CHILD_STDEALID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF1))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF1, CHILD_HREF1, CHILD_HREF1_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF2))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF2, CHILD_HREF2, CHILD_HREF2_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF3))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF3, CHILD_HREF3, CHILD_HREF3_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF4))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF4, CHILD_HREF4, CHILD_HREF4_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF5))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF5, CHILD_HREF5, CHILD_HREF5_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF6))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF6, CHILD_HREF6, CHILD_HREF6_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF7))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF7, CHILD_HREF7, CHILD_HREF7_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF8))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF8, CHILD_HREF8, CHILD_HREF8_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF9))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF9, CHILD_HREF9, CHILD_HREF9_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HREF10))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_HREF10, CHILD_HREF10, CHILD_HREF10_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBDEALID))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBDEALID, CHILD_TBDEALID,
          CHILD_TBDEALID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTBACKWARDBUTTON))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTBACKWARDBUTTON, CHILD_BTBACKWARDBUTTON,
          CHILD_BTBACKWARDBUTTON_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTFILTERSUBMITBUTTON))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTFILTERSUBMITBUTTON, CHILD_BTFILTERSUBMITBUTTON,
          CHILD_BTFILTERSUBMITBUTTON_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CBUSERFILTER))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBUSERFILTER, CHILD_CBUSERFILTER,
          CHILD_CBUSERFILTER_RESET_VALUE, null);

      //--Release2.1--//
      ////Modified to set a NonSelected Label as the CBUSERFILTER_ALL_LABEL.
      ////child.setLabelForNoneSelected("All");
      ////child.setLabelForNoneSelected(CBUSERFILTER_ALL_LABEL);
      child.setLabelForNoneSelected(" ");
      child.setOptions(cbUserFilterOptions);

      return child;
    }
    else if (name.equals(CHILD_STUSERFILTER))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STUSERFILTER, CHILD_STUSERFILTER,
          CHILD_STUSERFILTER_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TBDEALIDFILTER))
    {
      TextField child =
        new TextField(this, getDefaultModel(), CHILD_TBDEALIDFILTER, CHILD_TBDEALIDFILTER,
          CHILD_TBDEALIDFILTER_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTSORTBUTTON))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTSORTBUTTON, CHILD_BTSORTBUTTON,
          CHILD_BTSORTBUTTON_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CBSORTOPTIONS))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBSORTOPTIONS, CHILD_CBSORTOPTIONS,
          CHILD_CBSORTOPTIONS_RESET_VALUE, null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbSortOptionsOptions);

      return child;
    }
    else if (name.equals(CHILD_STSORTOPTION))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STSORTOPTION, CHILD_STSORTOPTION,
          CHILD_STSORTOPTION_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTFILTERSORTRESET))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTFILTERSORTRESET, CHILD_BTFILTERSORTRESET,
          CHILD_BTFILTERSORTRESET_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CBSTATUSFILTER))
    {
      ComboBox child =
        new ComboBox(this, getDefaultModel(), CHILD_CBSTATUSFILTER, CHILD_CBSTATUSFILTER,
          CHILD_CBSTATUSFILTER_RESET_VALUE, null);

      //--Release2.1--//
      ////Modified to set a NonSelected Label as the CBSTATUSFILTER_NONE_SELECTED_LABEL.
      ////child.setLabelForNoneSelected("None Selected");
      ////child.setLabelForNoneSelected("CBSTATUSFILTER_NONE_SELECTED_LABEL");
      child.setOptions(cbStatusFilterOptions);

      return child;
    }
    else if (name.equals(CHILD_STSTATUSFILTER))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STSTATUSFILTER, CHILD_STSTATUSFILTER,
          CHILD_STSTATUSFILTER_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_CHANGEPASSWORDHREF))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_CHANGEPASSWORDHREF, CHILD_CHANGEPASSWORDHREF,
          CHILD_CHANGEPASSWORDHREF_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTTOOLHISTORY))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTTOOLHISTORY, CHILD_BTTOOLHISTORY,
          CHILD_BTTOOLHISTORY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTTOONOTES))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTTOONOTES, CHILD_BTTOONOTES,
          CHILD_BTTOONOTES_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTTOOLSEARCH))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTTOOLSEARCH, CHILD_BTTOOLSEARCH,
          CHILD_BTTOOLSEARCH_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTTOOLLOG))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTTOOLLOG, CHILD_BTTOOLLOG,
          CHILD_BTTOOLLOG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTWORKQUEUELINK))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTWORKQUEUELINK, CHILD_BTWORKQUEUELINK,
          CHILD_BTWORKQUEUELINK_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STERRORFLAG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STERRORFLAG, CHILD_STERRORFLAG,
          CHILD_STERRORFLAG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTFORWARDBUTTON))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTFORWARDBUTTON, CHILD_BTFORWARDBUTTON,
          CHILD_BTFORWARDBUTTON_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BTSUBMIT))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTSUBMIT, CHILD_BTSUBMIT,
          CHILD_BTSUBMIT_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_DETECTALERTTASKS))
    {
      HiddenField child =
        new HiddenField(this, getDefaultModel(), CHILD_DETECTALERTTASKS, CHILD_DETECTALERTTASKS,
          CHILD_DETECTALERTTASKS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTOTALLOANAMOUNT))
    {
      StaticTextField child =
        new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STTOTALLOANAMOUNT,
          doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT, CHILD_STTOTALLOANAMOUNT_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STBORRFIRSTNAME))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STBORRFIRSTNAME, CHILD_STBORRFIRSTNAME,
          CHILD_STBORRFIRSTNAME_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_SESSIONUSERID))
    {
      HiddenField child =
        new HiddenField(this, getDefaultModel(), CHILD_SESSIONUSERID, CHILD_SESSIONUSERID,
          CHILD_SESSIONUSERID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMGENERATE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMGENERATE, CHILD_STPMGENERATE,
          CHILD_STPMGENERATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMHASTITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMHASTITLE, CHILD_STPMHASTITLE,
          CHILD_STPMHASTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMHASINFO))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMHASINFO, CHILD_STPMHASINFO,
          CHILD_STPMHASINFO_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMHASTABLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMHASTABLE, CHILD_STPMHASTABLE,
          CHILD_STPMHASTABLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMHASOK))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMHASOK, CHILD_STPMHASOK,
          CHILD_STPMHASOK_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMTITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMTITLE, CHILD_STPMTITLE,
          CHILD_STPMTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMINFOMSG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMINFOMSG, CHILD_STPMINFOMSG,
          CHILD_STPMINFOMSG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMONOK))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMONOK, CHILD_STPMONOK,
          CHILD_STPMONOK_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMMSGS))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMMSGS, CHILD_STPMMSGS,
          CHILD_STPMMSGS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STPMMSGTYPES))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STPMMSGTYPES, CHILD_STPMMSGTYPES,
          CHILD_STPMMSGTYPES_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMGENERATE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMGENERATE, CHILD_STAMGENERATE,
          CHILD_STAMGENERATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMHASTITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMHASTITLE, CHILD_STAMHASTITLE,
          CHILD_STAMHASTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMHASINFO))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMHASINFO, CHILD_STAMHASINFO,
          CHILD_STAMHASINFO_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMHASTABLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMHASTABLE, CHILD_STAMHASTABLE,
          CHILD_STAMHASTABLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMTITLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMTITLE, CHILD_STAMTITLE,
          CHILD_STAMTITLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMINFOMSG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMINFOMSG, CHILD_STAMINFOMSG,
          CHILD_STAMINFOMSG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMMSGS))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMMSGS, CHILD_STAMMSGS,
          CHILD_STAMMSGS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMMSGTYPES))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMMSGTYPES, CHILD_STAMMSGTYPES,
          CHILD_STAMMSGTYPES_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMDIALOGMSG))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMDIALOGMSG, CHILD_STAMDIALOGMSG,
          CHILD_STAMDIALOGMSG_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STAMBUTTONSHTML))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STAMBUTTONSHTML, CHILD_STAMBUTTONSHTML,
          CHILD_STAMBUTTONSHTML_RESET_VALUE, null);

      return child;
    }

    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else if (name.equals(CHILD_BTACTMSG))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_BTACTMSG, CHILD_BTACTMSG,
          CHILD_BTACTMSG_RESET_VALUE,
          new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));

      return child;
    }
    else
    // Added CheckBox to Hide or Display Hidden Tasks
    // By Billy 09Oct2002
    if (name.equals(CHILD_CHISDISPLAYHIDDEN))
    {
      CheckBox child =
        new CheckBox(this, getDefaultModel(), CHILD_CHISDISPLAYHIDDEN, CHILD_CHISDISPLAYHIDDEN,
          "T", "F", false, null);

      return child;
    }

    //--Release2.1--//
    //// Link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language throughout the all modulus.
    else if (name.equals(CHILD_TOGGLELANGUAGEHREF))
    {
      HREF child =
        new HREF(this, getDefaultModel(), CHILD_TOGGLELANGUAGEHREF, CHILD_TOGGLELANGUAGEHREF,
          CHILD_TOGGLELANGUAGEHREF_RESET_VALUE, null);

      return child;
    }

    //--> Added hidden fields for Scrolling JavaScript Function
    //--> By Billy 21July2003
    else if (name.equals(CHILD_HDSELECTEDROWID))
    {
      HiddenField child =
        new HiddenField(this, getDefaultModel(), CHILD_HDSELECTEDROWID, CHILD_HDSELECTEDROWID,
          CHILD_HDSELECTEDROWID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDCURRENTFIRSTROWID))
    {
      HiddenField child =
        new HiddenField(this, getDefaultModel(), CHILD_HDCURRENTFIRSTROWID,
          CHILD_HDCURRENTFIRSTROWID, CHILD_HDCURRENTFIRSTROWID_RESET_VALUE, null);

      return child;
    }
    else if(name.equals(CHILD_CBINSTITUTION))
    {
        ComboBox child =
            new ComboBox(this, getDefaultModel(), CHILD_CBINSTITUTION, CHILD_CBINSTITUTION,
                    CHILD_CBINSTITUTION_RESET_VALUE, null);
            child.setOptions(cbInstitutionOptions);
            return child;
    }
    else if(name.equals(CHILD_MULTIACCESS))
    {
        HiddenField child =
            new HiddenField(this, getDefaultModel(), CHILD_MULTIACCESS, 
                            CHILD_MULTIACCESS, CHILD_MULTIACCESS_RESET_VALUE, null);
            return child;
    } else if (CHILD_CBTASKEXPIRYDATESTARTFILTER.equals(name)) {
		TextField child = new TextField(this, getDefaultModel(), CHILD_CBTASKEXPIRYDATESTARTFILTER, CHILD_CBTASKEXPIRYDATESTARTFILTER, CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE, null);
		return child;
	} else if (CHILD_CBTASKEXPIRYDATEENDFILTER.equals(name)) {
		TextField child = new TextField(this, getDefaultModel(), CHILD_CBTASKEXPIRYDATEENDFILTER, CHILD_CBTASKEXPIRYDATEENDFILTER, CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE, null);
		return child;
	}
      
    // FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- start
	else if (name.equals(CHILD_TXWINDOWSCROLLPOSITION)) {
		TextField child = new TextField(this, getDefaultModel(),
				CHILD_TXWINDOWSCROLLPOSITION, CHILD_TXWINDOWSCROLLPOSITION,
				CHILD_TXWINDOWSCROLLPOSITION_RESET_VALUE, null);
		return child;
	}
    // FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- end

    //=========================================================
    else
    {
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }
  }

  /**
   *
   *
   */
  public void resetChildren()
  {
      super.resetChildren();
    getRepeated1().resetChildren();
    getRepeated2().resetChildren();
    getRowsDisplayed().setValue(CHILD_ROWSDISPLAYED_RESET_VALUE);
    getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
    getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
    getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
    getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
    getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
    getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
    getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
    getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
    getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
    getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
    getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
    getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
    getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
    getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
    getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
    getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
    getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
    getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
    getHref1().setValue(CHILD_HREF1_RESET_VALUE);
    getHref2().setValue(CHILD_HREF2_RESET_VALUE);
    getHref3().setValue(CHILD_HREF3_RESET_VALUE);
    getHref4().setValue(CHILD_HREF4_RESET_VALUE);
    getHref5().setValue(CHILD_HREF5_RESET_VALUE);
    getHref6().setValue(CHILD_HREF6_RESET_VALUE);
    getHref7().setValue(CHILD_HREF7_RESET_VALUE);
    getHref8().setValue(CHILD_HREF8_RESET_VALUE);
    getHref9().setValue(CHILD_HREF9_RESET_VALUE);
    getHref10().setValue(CHILD_HREF10_RESET_VALUE);
    getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
    getBtBackwardButton().setValue(CHILD_BTBACKWARDBUTTON_RESET_VALUE);
    getBtFilterSubmitButton().setValue(CHILD_BTFILTERSUBMITBUTTON_RESET_VALUE);
    getCbUserFilter().setValue(CHILD_CBUSERFILTER_RESET_VALUE);
    getStUserFilter().setValue(CHILD_STUSERFILTER_RESET_VALUE);
    getTbDealIdFilter().setValue(CHILD_TBDEALIDFILTER_RESET_VALUE);
    getBtSortButton().setValue(CHILD_BTSORTBUTTON_RESET_VALUE);
    getCbSortOptions().setValue(CHILD_CBSORTOPTIONS_RESET_VALUE);
    getStSortOption().setValue(CHILD_STSORTOPTION_RESET_VALUE);
    getBtFilterSortReset().setValue(CHILD_BTFILTERSORTRESET_RESET_VALUE);
    getCbStatusFilter().setValue(CHILD_CBSTATUSFILTER_RESET_VALUE);
    getStStatusFilter().setValue(CHILD_STSTATUSFILTER_RESET_VALUE);
    getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
    getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
    getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
    getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
    getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
    getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
    getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
    getBtForwardButton().setValue(CHILD_BTFORWARDBUTTON_RESET_VALUE);
    getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
    getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
    getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
    getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
    getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
    getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
    getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
    getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
    getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
    getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
    getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
    getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
    getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
    getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
    getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
    getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
    getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
    getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
    getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
    getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
    getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
    getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
    getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
    getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
    getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);

    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);

    // Added CheckBox to Hide or Display Hidden Tasks
    // By Billy 09Oct2002
    getChIsDisplayHidden().setValue(CHILD_CHISDISPLAYHIDDEN_RESET_VALUE);

    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value throughout all modulus.
    getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);

    //--> Added hidden fields for Scrolling JavaScript Function
    //--> By Billy 21July2003
    getHdSelectedRowID().setValue(CHILD_HDSELECTEDROWID_RESET_VALUE);
    getHdCurrentFirstRowID().setValue(CHILD_HDCURRENTFIRSTROWID_RESET_VALUE);

    //--TD_MWQ_CR--start//
    getRepeated3().resetChildren();
    getCbTaskNameFilter().setValue(CHILD_CBTASKNAMEFILTER_RESET_VALUE);

    //-- ========== SCR#750 begins ========== --//
    //-- by Neil on Dec/21/2004
    getCbDealStatusFilter().setValue(CHILD_CBDEALSTATUSFILTER_RESET_VALUE);

    //-- ========== SCR#750   ends ========== --//
    getLbMWQBranches().setValue(CHILD_LBMWQBRANCHES_RESET_VALUE);
    getLbMWQGroups().setValue(CHILD_LBMWQGROUPS_RESET_VALUE);
    getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
    getStTaskNameFull().setValue(CHILD_STTASKNAMEFULL_RESET_VALUE);
    getStTaskNameFull().setValue(CHILD_STLOCATIONFULL_RESET_VALUE);
    getCbInstitution().setValue(CHILD_CBINSTITUTION_RESET_VALUE);
    getMultiAccess().setValue(CHILD_MULTIACCESS_RESET_VALUE);
    
	((TextField) getChild(CHILD_CBTASKEXPIRYDATESTARTFILTER)).setValue(CHILD_CBTASKEXPIRYDATESTARTFILTER_RESET_VALUE);
	((TextField) getChild(CHILD_CBTASKEXPIRYDATEENDFILTER)).setValue(CHILD_CBTASKEXPIRYDATEENDFILTER_RESET_VALUE);
	 
	//FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position
	getTxWindowScrollPosition().setValue(CHILD_TXWINDOWSCROLLPOSITION_RESET_VALUE); 
  }

  /**
   *
   *
   */
  protected void registerChildren()
  {
      super.registerChildren();
    registerChild(CHILD_REPEATED1, pgMWorkQueueRepeated1TiledView.class);
    registerChild(CHILD_REPEATED2, pgMWorkQueueRepeated2TiledView.class);
    registerChild(CHILD_ROWSDISPLAYED, StaticTextField.class);
    registerChild(CHILD_STUSERNAMETITLE, StaticTextField.class);
    registerChild(CHILD_STTODAYDATE, StaticTextField.class);
    registerChild(CHILD_STCOMPANYNAME, StaticTextField.class);
    registerChild(CHILD_STPAGELABEL, StaticTextField.class);
    registerChild(CHILD_CBPAGENAMES, ComboBox.class);
    registerChild(CHILD_BTPROCEED, Button.class);
    registerChild(CHILD_STESTCLOSINGDATE, StaticTextField.class);
    registerChild(CHILD_STSPECIALFEATURE, StaticTextField.class);
    registerChild(CHILD_STPMTTERM, StaticTextField.class);
    registerChild(CHILD_STPURCHASEPRICE, StaticTextField.class);
    registerChild(CHILD_STDEALPURPOSE, StaticTextField.class);
    registerChild(CHILD_STDEALTYPE, StaticTextField.class);
    registerChild(CHILD_STLOB, StaticTextField.class);
    registerChild(CHILD_STSOURCE, StaticTextField.class);
    registerChild(CHILD_STSOURCEFIRM, StaticTextField.class);
    registerChild(CHILD_STDEALSTATUSDATE, StaticTextField.class);
    registerChild(CHILD_STDEALSTATUS, StaticTextField.class);
    registerChild(CHILD_STDEALID, StaticTextField.class);
    registerChild(CHILD_HREF1, HREF.class);
    registerChild(CHILD_HREF2, HREF.class);
    registerChild(CHILD_HREF3, HREF.class);
    registerChild(CHILD_HREF4, HREF.class);
    registerChild(CHILD_HREF5, HREF.class);
    registerChild(CHILD_HREF6, HREF.class);
    registerChild(CHILD_HREF7, HREF.class);
    registerChild(CHILD_HREF8, HREF.class);
    registerChild(CHILD_HREF9, HREF.class);
    registerChild(CHILD_HREF10, HREF.class);
    registerChild(CHILD_TBDEALID, TextField.class);
    registerChild(CHILD_BTBACKWARDBUTTON, Button.class);
    registerChild(CHILD_BTFILTERSUBMITBUTTON, Button.class);
    registerChild(CHILD_CBUSERFILTER, ComboBox.class);
    registerChild(CHILD_STUSERFILTER, StaticTextField.class);
    registerChild(CHILD_TBDEALIDFILTER, TextField.class);
    registerChild(CHILD_BTSORTBUTTON, Button.class);
    registerChild(CHILD_CBSORTOPTIONS, ComboBox.class);
    registerChild(CHILD_STSORTOPTION, StaticTextField.class);
    registerChild(CHILD_BTFILTERSORTRESET, Button.class);
    registerChild(CHILD_CBSTATUSFILTER, ComboBox.class);
    registerChild(CHILD_STSTATUSFILTER, StaticTextField.class);
    registerChild(CHILD_CHANGEPASSWORDHREF, HREF.class);
    registerChild(CHILD_BTTOOLHISTORY, Button.class);
    registerChild(CHILD_BTTOONOTES, Button.class);
    registerChild(CHILD_BTTOOLSEARCH, Button.class);
    registerChild(CHILD_BTTOOLLOG, Button.class);
    registerChild(CHILD_BTWORKQUEUELINK, Button.class);
    registerChild(CHILD_STERRORFLAG, StaticTextField.class);
    registerChild(CHILD_BTFORWARDBUTTON, Button.class);
    registerChild(CHILD_BTSUBMIT, Button.class);
    registerChild(CHILD_DETECTALERTTASKS, HiddenField.class);
    registerChild(CHILD_STTOTALLOANAMOUNT, StaticTextField.class);
    registerChild(CHILD_STBORRFIRSTNAME, StaticTextField.class);
    registerChild(CHILD_SESSIONUSERID, HiddenField.class);
    registerChild(CHILD_STPMGENERATE, StaticTextField.class);
    registerChild(CHILD_STPMHASTITLE, StaticTextField.class);
    registerChild(CHILD_STPMHASINFO, StaticTextField.class);
    registerChild(CHILD_STPMHASTABLE, StaticTextField.class);
    registerChild(CHILD_STPMHASOK, StaticTextField.class);
    registerChild(CHILD_STPMTITLE, StaticTextField.class);
    registerChild(CHILD_STPMINFOMSG, StaticTextField.class);
    registerChild(CHILD_STPMONOK, StaticTextField.class);
    registerChild(CHILD_STPMMSGS, StaticTextField.class);
    registerChild(CHILD_STPMMSGTYPES, StaticTextField.class);
    registerChild(CHILD_STAMGENERATE, StaticTextField.class);
    registerChild(CHILD_STAMHASTITLE, StaticTextField.class);
    registerChild(CHILD_STAMHASINFO, StaticTextField.class);
    registerChild(CHILD_STAMHASTABLE, StaticTextField.class);
    registerChild(CHILD_STAMTITLE, StaticTextField.class);
    registerChild(CHILD_STAMINFOMSG, StaticTextField.class);
    registerChild(CHILD_STAMMSGS, StaticTextField.class);
    registerChild(CHILD_STAMMSGTYPES, StaticTextField.class);
    registerChild(CHILD_STAMDIALOGMSG, StaticTextField.class);
    registerChild(CHILD_STAMBUTTONSHTML, StaticTextField.class);

    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG, Button.class);

    // Added CheckBox to Hide or Display Hidden Tasks
    // By Billy 09Oct2002
    registerChild(CHILD_CHISDISPLAYHIDDEN, CheckBox.class);

    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value throughout all modulus.
    registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);

    //--> Added hidden fields for Scrolling JavaScript Function
    //--> By Billy 21July2003
    registerChild(CHILD_HDSELECTEDROWID, HiddenField.class);
    registerChild(CHILD_HDCURRENTFIRSTROWID, HiddenField.class);

    //--TD_MWQ_CR--start//
    registerChild(CHILD_REPEATED3, pgMWorkQueueRepeated3TiledView.class);
    registerChild(CHILD_CBTASKNAMEFILTER, ComboBox.class);

    //-- ========== SCR#750 begins ========== --//
    //-- by Neil on Dec/21/2004
    registerChild(CHILD_CBDEALSTATUSFILTER, ComboBox.class);

    //-- ========== SCR#750   ends ========== --//
    registerChild(CHILD_LBMWQBRANCHES, ListBox.class);
    registerChild(CHILD_LBMWQGROUPS, ListBox.class);
    registerChild(CHILD_STTASKNAME, StaticTextField.class);
    registerChild(CHILD_STTASKNAMEFULL, StaticTextField.class);
    registerChild(CHILD_STLOCATIONFULL, StaticTextField.class);
    registerChild(CHILD_CBINSTITUTION, ComboBox.class);
    registerChild(CHILD_MULTIACCESS, StaticTextField.class);
    
    registerChild(CHILD_CBTASKEXPIRYDATESTARTFILTER, TextField.class);
	registerChild(CHILD_CBTASKEXPIRYDATEENDFILTER, TextField.class);
	
	// FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position
	registerChild(CHILD_TXWINDOWSCROLLPOSITION, TextField.class);
	
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList = new ArrayList();

    switch (executionType)
    {
    case MODEL_TYPE_RETRIEVE:
      modelList.add(getdoDealSummarySnapShotModel());
      ;

      break;

    case MODEL_TYPE_UPDATE:
      ;

      break;

    case MODEL_TYPE_DELETE:
      ;

      break;

    case MODEL_TYPE_INSERT:
      ;

      break;

    case MODEL_TYPE_EXECUTE:
      ;

      break;
    }

    return (Model[]) modelList.toArray(new Model[0]);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //--Release2.1--start//
    ////Populate all ComboBoxes manually here
    //// 1. Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);
    cbInstitutionOptions.populate(getRequestContext());
    ////Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
      BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL",
        handler.getTheSessionState().getLanguageId());

    ////Check if the cbPageNames is already created
    if (getCbPageNames() != null)
    {
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
    }

    //// 2. Setup Options for cbUserFilter
    cbUserFilterOptions.populate(getRequestContext());

    ////Setup NoneSelected Label for cbPageNames
    CBUSERFILTER_ALL_LABEL =
      BXResources.getGenericMsg("CBUSERFILTER_ALL_LABEL",
        handler.getTheSessionState().getLanguageId());

    ////Check if the cbPageNames is already created
    if (getCbUserFilter() != null)
    {
      getCbUserFilter().setLabelForNoneSelected(CBUSERFILTER_ALL_LABEL);
    }

    //// 3. Setup Options for cbStatusFilter. The combobox content should not
    //// be populated from the RequestContext (it is done in the handler).
    ////cbStatusFilterOptions.populate(getRequestContext());
    ////Setup NoneSelected Label for cbPageNames
    ////CBSTATUSFILTER_NONE_SELECTED_LABEL =
    ////    BXResources.getGenericMsg("CBSTATUSFILTER_NONE_SELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    ////Check if the cbPageNames is already created
    ////if(getCbStatusFilter() != null)
    ////  getCbStatusFilter().setLabelForNoneSelected(CBSTATUSFILTER_NONE_SELECTED_LABEL);
    //--Release2.1--end//
    //--TD_MWQ_CR--start//
    //// 1. New Task Name Filter option.
    cbTaskNameFilterOptions.populate(getRequestContext());
    CBTASKNAMEFILTER_ALL_SELECTED_LABEL =
      BXResources.getGenericMsg("MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (getCbTaskNameFilter() != null)
    {
      getCbTaskNameFilter().setLabelForNoneSelected(CBTASKNAMEFILTER_ALL_SELECTED_LABEL);
    }

    //-- ========== SCR#750 begins ========== --//
    //-- by Neil on Dec/21/2004
    cbDealStatusFilterOptions.populate(getRequestContext());
    CBDEALSTATUSFILTER_ALL_SELECTED_LABEL =
      BXResources.getGenericMsg("MWQ_STATUS_DEAL_CBOPTIONS_ALL_LABEL",
        handler.getTheSessionState().getLanguageId());

    if (getCbDealStatusFilter() != null)
    {
      getCbDealStatusFilter().setLabelForNoneSelected(CBDEALSTATUSFILTER_ALL_SELECTED_LABEL);
    }

    //-- ========== SCR#750   ends ========== --//
    //// 2. New Branch and Group ListBox Filter Options.
    lbMWQBranchesOptions.populate(getRequestContext());
    lbMWQGroupsOptions.populate(getRequestContext());

    //// Discuss with users this option.
    /*
       LBMWQBRANCHES_ALL_SELECTED_LABEL  =
            BXResources.getGenericMsg("CBUSERFILTER_ALL_LABEL", handler.getTheSessionState().getLanguageId());
       if(getLbMWQBranches() != null)
         getLbMWQBranches().setLabelForNoneSelected(LBMWQBRANCHES_ALL_SELECTED_LABEL);

       LBMWQGROUPS_ALL_SELECTED_LABEL  =
            BXResources.getGenericMsg("CBUSERFILTER_ALL_LABEL", handler.getTheSessionState().getLanguageId());
       if(getLbMWQGroups() != null)
         getLbMWQGroups().setLabelForNoneSelected(LBMWQGROUPS_ALL_SELECTED_LABEL);
     */
    //--TD_MWQ_CR--end//
    //// 3. Select the indexes for combo and list boxes.
    //// TaskNameFilter.
    logger = SysLog.getSysLogger("MWQVB");

    //FXP26606
    String taskNameFilter = handler.theSessionState.getCurrentPage().getMWQTaskNameFilter();
    this.getCbTaskNameFilter().setValue(taskNameFilter, true);

    //-- ========== SCR#750 begins ========== --//
    //-- by Neil on Dec/21/2004
    //-- DealStatusFilter
    // TODO
    //-- ========== SCR#750   ends ========== --//
    //// Branches.
    if ((handler.theSessionState.getCurrentPage().getPageStateTable() != null)
          && (handler.theSessionState.getCurrentPage().getPageStateTable().get("branches") != null))
    {
      //Vector ndxs =
        //(Vector) handler.theSessionState.getCurrentPage().getPageStateTable().get("branches");
      
      Map<Integer, Integer> branchMap = (Map<Integer,Integer>)handler.theSessionState
          .getCurrentPage().getPageStateTable().get("branches");
      
      logger.debug("pgMWQB@beginDisplay::BranchesSize: " + branchMap.size());
      
      Iterator institutionIds = branchMap.keySet().iterator();
      int size = branchMap.size();

      Object[] andxs = new Object[branchMap.size()];

      for (int i = 0; i < size; i++) {
          Integer branchIdObj = (Integer) institutionIds.next();
          int branchId = branchIdObj.intValue();
          int instId = ((Integer) branchMap.get(branchId))
                  .intValue();
          andxs[i] = new String(branchId  + ", " + instId );
      }

      this.getLbMWQBranches().setValues(andxs, true);
    }

    //// Groups.
    if ((handler.theSessionState.getCurrentPage().getPageStateTable() != null)
          && (handler.theSessionState.getCurrentPage().getPageStateTable().get("groups") != null))
    {
      Vector ndxs =
        (Vector) handler.theSessionState.getCurrentPage().getPageStateTable().get("groups");
      logger.debug("pgMWQB@beginDisplay::GroupsSize: " + ndxs.size());

      Object[] andxs = new Object[ndxs.size()];

      for (int i = 0; i < ndxs.size(); i++)
      {
        logger.debug("pgMWQB@beginDisplay::GroupIndex: " + (ndxs.elementAt(i)).toString());
        andxs[i] = ndxs.elementAt(i).toString();
      }

      this.getLbMWQGroups().setValues(andxs, true);
    }

    handler.pageSaveState();
    super.beginDisplay(event);
  }

  public void handleBtInstSelectedRequest(RequestInvocationEvent event)
	throws ServletException, Exception 
	{
	  MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
	  handler.preHandlerProtocol(this);
	  // the page is submiited to regresh the quick links
	  handler.postHandlerProtocol();
	}
	  
  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.setupBeforePageGeneration();

    handler.pageSaveState();

    // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
    return super.beforeModelExecutes(model, executionContext);

    // The following code block was migrated from the this_onBeforeDataObjectExecuteEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.pageGetState(this);


       //CSpHtml.sendMessage("DO Exec Event at deal summary " + event.getDataObject());
       handler.setupBeforePageGeneration();

       handler.pageSaveState();

       return;
     */
  }

  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
    // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    handler.populatePageDisplayFields();

    handler.pageSaveState();

    // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);

    // The following code block was migrated from the this_onBeforeRowDisplayEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.populatePageDisplayFields();

       handler.pageSaveState();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {
    // This is the analog of NetDynamics this_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);
  }

  /**
   *
   *
   */
  public pgMWorkQueueRepeated1TiledView getRepeated1()
  {
    return (pgMWorkQueueRepeated1TiledView) getChild(CHILD_REPEATED1);
  }

  /**
   *
   *
   */
  public pgMWorkQueueRepeated2TiledView getRepeated2()
  {
    return (pgMWorkQueueRepeated2TiledView) getChild(CHILD_REPEATED2);
  }

  //--TD_MWQ_CR--start//

  /**
   *
   *
   */
  public StaticTextField getStTaskName()
  {
    return (StaticTextField) getChild(CHILD_STTASKNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskNameFull()
  {
    return (StaticTextField) getChild(CHILD_STTASKNAMEFULL);
  }

  /**
   *
   *
   */
  public StaticTextField getStLocationFull()
  {
    return (StaticTextField) getChild(CHILD_STLOCATIONFULL);
  }

  /**
   *
   *
   */
  public pgMWorkQueueRepeated3TiledView getRepeated3()
  {
    return (pgMWorkQueueRepeated3TiledView) getChild(CHILD_REPEATED3);
  }

  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/21/2004

  /**
   *
   */
  public ComboBox getCbDealStatusFilter()
  {
    return (ComboBox) getChild(CHILD_CBDEALSTATUSFILTER);
  }

  //-- ========== SCR#750   ends ========== --//

  /**
   *
   *
   */
  public ComboBox getCbTaskNameFilter()
  {
    return (ComboBox) getChild(CHILD_CBTASKNAMEFILTER);
  }

  /**
   *
   *
   */
  public ListBox getLbMWQBranches()
  {
    return (ListBox) getChild(CHILD_LBMWQBRANCHES);
  }

  /**
   *
   *
   */
  public ListBox getLbMWQGroups()
  {
    return (ListBox) getChild(CHILD_LBMWQGROUPS);
  }

  //--TD_MWQ_CR--end//

  /**
   *
   *
   */
  public StaticTextField getRowsDisplayed()
  {
    return (StaticTextField) getChild(CHILD_ROWSDISPLAYED);
  }

  /**
   *
   *
   */
  public StaticTextField getStUserNameTitle()
  {
    return (StaticTextField) getChild(CHILD_STUSERNAMETITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStTodayDate()
  {
    return (StaticTextField) getChild(CHILD_STTODAYDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStCompanyName()
  {
    return (StaticTextField) getChild(CHILD_STCOMPANYNAME);
  }

  /**
   *
   *
   */
  public StaticTextField getStPageLabel()
  {
    return (StaticTextField) getChild(CHILD_STPAGELABEL);
  }

  /**
   *
   *
   */
  public ComboBox getCbPageNames()
  {
    return (ComboBox) getChild(CHILD_CBPAGENAMES);
  }

  //-- ========== SCR#750   ends ========== --//

  
  public ComboBox getCbInstitution() {
      return (ComboBox) getChild(CHILD_CBINSTITUTION);
  }
    
  /**
   *
   *
   */
  public void handleBtProceedRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleGoPage();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btProceed_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleGoPage();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtProceed()
  {
    return (Button) getChild(CHILD_BTPROCEED);
  }

  /**
   *
   *
   */
  public StaticTextField getStEstClosingDate()
  {
    return (StaticTextField) getChild(CHILD_STESTCLOSINGDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStSpecialFeature()
  {
    return (StaticTextField) getChild(CHILD_STSPECIALFEATURE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmtTerm()
  {
    return (StaticTextField) getChild(CHILD_STPMTTERM);
  }

  /**
   *
   *
   */
  public StaticTextField getStPurchasePrice()
  {
    return (StaticTextField) getChild(CHILD_STPURCHASEPRICE);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealPurpose()
  {
    return (StaticTextField) getChild(CHILD_STDEALPURPOSE);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealType()
  {
    return (StaticTextField) getChild(CHILD_STDEALTYPE);
  }

  /**
   *
   *
   */
  public StaticTextField getStLOB()
  {
    return (StaticTextField) getChild(CHILD_STLOB);
  }

  /**
   *
   *
   */
  public StaticTextField getStSource()
  {
    return (StaticTextField) getChild(CHILD_STSOURCE);
  }

  /**
   *
   *
   */
  public StaticTextField getStSourceFirm()
  {
    return (StaticTextField) getChild(CHILD_STSOURCEFIRM);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealStatusDate()
  {
    return (StaticTextField) getChild(CHILD_STDEALSTATUSDATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealStatus()
  {
    return (StaticTextField) getChild(CHILD_STDEALSTATUS);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealId()
  {
    return (StaticTextField) getChild(CHILD_STDEALID);
  }

  public StaticTextField getMultiAccess() {
      return (StaticTextField) getChild(CHILD_MULTIACCESS);
  }
  

  /**
   *
   *
   */
  public void handleHref1Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(0);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href1_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(0);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref1()
  {
    return (HREF) getChild(CHILD_HREF1);
  }

  /**
   *
   *
   */
  public void handleHref2Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(1);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href2_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(1);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref2()
  {
    return (HREF) getChild(CHILD_HREF2);
  }

  /**
   *
   *
   */
  public void handleHref3Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(2);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href3_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(2);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref3()
  {
    return (HREF) getChild(CHILD_HREF3);
  }

  /**
   *
   *
   */
  public void handleHref4Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(3);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href4_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(3);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref4()
  {
    return (HREF) getChild(CHILD_HREF4);
  }

  /**
   *
   *
   */
  public void handleHref5Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(4);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href5_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(4);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref5()
  {
    return (HREF) getChild(CHILD_HREF5);
  }

  /**
   *
   *
   */
  public void handleHref6Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(5);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href6_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(5);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref6()
  {
    return (HREF) getChild(CHILD_HREF6);
  }

  /**
   *
   *
   */
  public void handleHref7Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(6);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href7_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(6);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref7()
  {
    return (HREF) getChild(CHILD_HREF7);
  }

  /**
   *
   *
   */
  public void handleHref8Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(7);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href8_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(7);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref8()
  {
    return (HREF) getChild(CHILD_HREF8);
  }

  /**
   *
   *
   */
  public void handleHref9Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(8);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href9_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(8);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref9()
  {
    return (HREF) getChild(CHILD_HREF9);
  }

  /**
   *
   *
   */
  public void handleHref10Request(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleOpenDialogLink(9);

    handler.postHandlerProtocol();

    // The following code block was migrated from the Href10_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(9);

       return handler.postHandlerProtocol();
     */
  }

  /**
   *
   *
   */
  public HREF getHref10()
  {
    return (HREF) getChild(CHILD_HREF10);
  }

  /**
   *
   *
   */
  public TextField getTbDealId()
  {
    return (TextField) getChild(CHILD_TBDEALID);
  }

  /**
   *
   *
   */
  public void handleBtBackwardButtonRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    getPgMWorkQueueRepeated1TiledView().handleWebAction(WebActions.ACTION_PREV);

    handler.handleBackwardButton();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btBackwardButton_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleBackwardButton();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtBackwardButton()
  {
    return (Button) getChild(CHILD_BTBACKWARDBUTTON);
  }

  /**
   *
   *
   */
  public String endBtBackwardButtonDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btBackwardButton_onBeforeHtmlOutputEvent method

    /**
     * MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();
     * handler.pageGetState(this);     boolean rc = handler.generateBackwardButtonStd();
     * handler.pageSaveState();     if(rc == true) return event.getContent(); else return "";
     */

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.generateBackwardButtonStd();

       handler.pageSaveState();

       return rc;
     */
    return event.getContent();
  }

  /**
   *
   *
   */
  public void handleBtFilterSubmitButtonRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleFilterButton();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btFilterSubmitButton_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleFilterButton();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtFilterSubmitButton()
  {
    return (Button) getChild(CHILD_BTFILTERSUBMITBUTTON);
  }

  /**
   *
   *
   */
  public ComboBox getCbUserFilter()
  {
    return (ComboBox) getChild(CHILD_CBUSERFILTER);
  }

  /**
   *
   *
   */
  public StaticTextField getStUserFilter()
  {
    return (StaticTextField) getChild(CHILD_STUSERFILTER);
  }

  /**
   *
   *
   */
  public TextField getTbDealIdFilter()
  {
    return (TextField) getChild(CHILD_TBDEALIDFILTER);
  }

  /**
   *
   *
   */
  public void handleBtSortButtonRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleSortButton();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btSortButton_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleSortButton();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtSortButton()
  {
    return (Button) getChild(CHILD_BTSORTBUTTON);
  }

  /**
   *
   *
   */
  public ComboBox getCbSortOptions()
  {
    return (ComboBox) getChild(CHILD_CBSORTOPTIONS);
  }

  /**
   *
   *
   */
  public StaticTextField getStSortOption()
  {
    return (StaticTextField) getChild(CHILD_STSORTOPTION);
  }

  /**
   *
   *
   */
  public void handleBtFilterSortResetRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleResetButton();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btFilterSortReset_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleResetButton();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtFilterSortReset()
  {
    return (Button) getChild(CHILD_BTFILTERSORTRESET);
  }

  /**
   *
   *
   */
  public ComboBox getCbStatusFilter()
  {
    return (ComboBox) getChild(CHILD_CBSTATUSFILTER);
  }

  /**
   *
   *
   */
  public StaticTextField getStStatusFilter()
  {
    return (StaticTextField) getChild(CHILD_STSTATUSFILTER);
  }

  /**
   *
   *
   */
  public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleChangePassword();

    handler.postHandlerProtocol();

    // The following code block was migrated from the changePasswordHref_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleChangePassword();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public HREF getChangePasswordHref()
  {
    return (HREF) getChild(CHILD_CHANGEPASSWORDHREF);
  }

  //--Release2.1-- start//
  //// Four new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.

  /**
   *
   *
   */
  public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleToggleLanguage();

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public HREF getToggleLanguageHref()
  {
    return (HREF) getChild(CHILD_TOGGLELANGUAGEHREF);
  }

  /**
   * Get LanguageId from the SessionStateModel
   *
   * @return DOCUMENT ME!
   */
  private String getSessionLanguageId()
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    int langId = handler.getTheSessionState().getLanguageId();
    handler.pageSaveState();

    return new Integer(langId).toString();
  }

  //--Release2.1--end//

  /**
   *
   *
   */
  public void handleBtToolHistoryRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayDealHistory();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btToolHistory_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDisplayDealHistory();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtToolHistory()
  {
    return (Button) getChild(CHILD_BTTOOLHISTORY);
  }

  /**
   *
   *
   */
  public void handleBtTooNotesRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayDealNotes();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btTooNotes_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDisplayDealNotes();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtTooNotes()
  {
    return (Button) getChild(CHILD_BTTOONOTES);
  }

  /**
   *
   *
   */
  public void handleBtToolSearchRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDealSearch();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btToolSearch_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDealSearch();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtToolSearch()
  {
    return (Button) getChild(CHILD_BTTOOLSEARCH);
  }

  /**
   *
   *
   */
  public void handleBtToolLogRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleSignOff();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btToolLog_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleSignOff();

       handler.postHandlerProtocol();

       return;
     */
  }

  /**
   *
   *
   */
  public Button getBtToolLog()
  {
    return (Button) getChild(CHILD_BTTOOLLOG);
  }

  /**
   *
   *
   */
  public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleDisplayWorkQueue();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btWorkQueueLink_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDisplayWorkQueue();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtWorkQueueLink()
  {
    return (Button) getChild(CHILD_BTWORKQUEUELINK);
  }

  /**
   *
   *
   */
  public StaticTextField getStErrorFlag()
  {
    return (StaticTextField) getChild(CHILD_STERRORFLAG);
  }

  ///////////
  //// VERY IMPORTANT BACK/FORWARD JATO MVC IMPLEMENTATION BASED OF THE
  //// (FIRST/NEST/PREVIOUS/LAST NETDYNAMICS PATTERN.
  ///////////

  /**
   *
   *
   */
  public boolean beginBtForwardButtonDisplay(ChildDisplayEvent event)  ////BTFORWARDBUTTON
    throws IOException
  {
    boolean result =
      ((doMasterWorkQueueModelImpl) getPgMWorkQueueRepeated1TiledView().getdoMasterWorkQueueModel())
      .hasMoreResults();

    return result;
  }

  public pgMWorkQueueRepeated1TiledView getPgMWorkQueueRepeated1TiledView()
  {
    return (pgMWorkQueueRepeated1TiledView) getChild(CHILD_REPEATED1);
  }

  public boolean beginBtBackwardButtonDisplay(ChildDisplayEvent event)  ////BTBACKWARDBUTTON
    throws IOException
  {
    boolean result =
      ((doMasterWorkQueueModelImpl) getPgMWorkQueueRepeated1TiledView().getdoMasterWorkQueueModel())
      .hasPreviousResults();

    return result;
  }

  private void renderDisabledButton(ChildDisplayEvent event, String name, String value)
    throws IOException
  {
    // This demonstates how to use the supplied display event to output
    // directly to the JSP output stream
    ((JspChildDisplayEvent) event).getPageContext().getOut().println("<input type=\"button\" name=\""
      + getChild(name).getQualifiedName() + "\" value=\"" + value + "\" disabled>");
  }

  /**
   *
   *
   */
  public void handleBtForwardButtonRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    getPgMWorkQueueRepeated1TiledView().handleWebAction(WebActions.ACTION_NEXT);

    handler.handleForwardButton();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btForwardButton_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleForwardButton();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtForwardButton()
  {
    return (Button) getChild(CHILD_BTFORWARDBUTTON);
  }

  /**
   *
   *
   */
  public String endBtForwardButtonDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btForwardButton_onBeforeHtmlOutputEvent method
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.pageGetState(this);

    boolean rc = handler.generateForwardButtonStd();

    handler.pageSaveState();

    if (rc == true)
    {
      return event.getContent();
    }
    else
    {
      return "";
    }

    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.generateForwardButtonStd();

       handler.pageSaveState();

       return rc;
     */
  }

  /**
   *
   *
   */
  public void handleBtSubmitRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleSubmit();

    handler.postHandlerProtocol();

    // The following code block was migrated from the btSubmit_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleSubmit();

       handler.postHandlerProtocol();

       return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getBtSubmit()
  {
    return (Button) getChild(CHILD_BTSUBMIT);
  }

  /**
   *
   *
   */
  public HiddenField getDetectAlertTasks()
  {
    return (HiddenField) getChild(CHILD_DETECTALERTTASKS);
  }

  /**
   *
   *
   */
  public StaticTextField getStTotalLoanAmount()
  {
    return (StaticTextField) getChild(CHILD_STTOTALLOANAMOUNT);
  }

  /**
   *
   *
   */
  public StaticTextField getStBorrFirstName()
  {
    return (StaticTextField) getChild(CHILD_STBORRFIRSTNAME);
  }

  /**
   *
   *
   */
  public HiddenField getSessionUserId()
  {
    return (HiddenField) getChild(CHILD_SESSIONUSERID);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmGenerate()
  {
    return (StaticTextField) getChild(CHILD_STPMGENERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasTitle()
  {
    return (StaticTextField) getChild(CHILD_STPMHASTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasInfo()
  {
    return (StaticTextField) getChild(CHILD_STPMHASINFO);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasTable()
  {
    return (StaticTextField) getChild(CHILD_STPMHASTABLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmHasOk()
  {
    return (StaticTextField) getChild(CHILD_STPMHASOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmTitle()
  {
    return (StaticTextField) getChild(CHILD_STPMTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmInfoMsg()
  {
    return (StaticTextField) getChild(CHILD_STPMINFOMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmOnOk()
  {
    return (StaticTextField) getChild(CHILD_STPMONOK);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmMsgs()
  {
    return (StaticTextField) getChild(CHILD_STPMMSGS);
  }

  /**
   *
   *
   */
  public StaticTextField getStPmMsgTypes()
  {
    return (StaticTextField) getChild(CHILD_STPMMSGTYPES);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmGenerate()
  {
    return (StaticTextField) getChild(CHILD_STAMGENERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasTitle()
  {
    return (StaticTextField) getChild(CHILD_STAMHASTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasInfo()
  {
    return (StaticTextField) getChild(CHILD_STAMHASINFO);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmHasTable()
  {
    return (StaticTextField) getChild(CHILD_STAMHASTABLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmTitle()
  {
    return (StaticTextField) getChild(CHILD_STAMTITLE);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmInfoMsg()
  {
    return (StaticTextField) getChild(CHILD_STAMINFOMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmMsgs()
  {
    return (StaticTextField) getChild(CHILD_STAMMSGS);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmMsgTypes()
  {
    return (StaticTextField) getChild(CHILD_STAMMSGTYPES);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmDialogMsg()
  {
    return (StaticTextField) getChild(CHILD_STAMDIALOGMSG);
  }

  /**
   *
   *
   */
  public StaticTextField getStAmButtonsHtml()
  {
    return (StaticTextField) getChild(CHILD_STAMBUTTONSHTML);
  }

  /**
   *
   *
   */
  public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
  {
    if (doDealSummarySnapShot == null)
    {
      doDealSummarySnapShot =
        (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
    }

    return doDealSummarySnapShot;
  }

  /**
   *
   *
   */
  public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
  {
    doDealSummarySnapShot = model;
  }

  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
  {
    return (Button) getChild(CHILD_BTACTMSG);
  }

  //// Populate previous links new set of methods to populate Href display Strings.
  public String endHref1Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
    handler.pageSaveState();

    return rc;
  }

  public String endHref2Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
    handler.pageSaveState();

    return rc;
  }

  public String endHref3Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
    handler.pageSaveState();

    return rc;
  }

  public String endHref4Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
    handler.pageSaveState();

    return rc;
  }

  public String endHref5Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
    handler.pageSaveState();

    return rc;
  }

  public String endHref6Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
    handler.pageSaveState();

    return rc;
  }

  public String endHref7Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
    handler.pageSaveState();

    return rc;
  }

  public String endHref8Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
    handler.pageSaveState();

    return rc;
  }

  public String endHref9Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
    handler.pageSaveState();

    return rc;
  }

  public String endHref10Display(ChildContentDisplayEvent event)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
    handler.pageSaveState();

    return rc;
  }

  // Added CheckBox to Hide or Display Hidden Tasks
  // By Billy 09Oct2002
  public CheckBox getChIsDisplayHidden()
  {
    return (CheckBox) getChild(CHILD_CHISDISPLAYHIDDEN);
  }

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this);

    //logger = SysLog.getSysLogger("MWQVB");
    String url = getDefaultDisplayURL();

    //logger.debug("MWQVB@getDisplayURL::DefaultURL: " + url);
    int languageId = handler.theSessionState.getLanguageId();

    //logger.debug("MWQV@getDisplayURL::LanguageFromUSOSession: " + languageId);
    //// Call the language specific URL (business delegation done in the BXResource).
    if ((url != null) && !url.trim().equals("") && !url.trim().equals("/"))
    {
      url = BXResources.getBXUrl(url, languageId);

      //logger.debug("MWQV@getDisplayURL::GoodMode: " + url);
    }
    else
    {
      url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);

      //logger.debug("MWQV@getDisplayURL::BadMode: " + url);
    }

    return url;
  }

  //--> Added hidden fields for Scrolling JavaScript Function
  //--> By Billy 21July2003
  public HiddenField getHdSelectedRowID()
  {
    return (HiddenField) getChild(CHILD_HDSELECTEDROWID);
  }

  public HiddenField getHdCurrentFirstRowID()
  {
    return (HiddenField) getChild(CHILD_HDCURRENTFIRSTROWID);
  }

  //FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- start
  public TextField getTxWindowScrollPosition() {
	  return (TextField) getChild(CHILD_TXWINDOWSCROLLPOSITION);
  }
  //FXP30170, 4.4, Oct 08, 2010, saving/reapplying screen scroll position -- end
	
  //=========================================================
  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  //]]SPIDER_EVENT<btSubmit_onWebEvent>

  /* MigrationToDo : Migrate custom method
     public int handleActMessageOK(String[] args)
     {
       MasterWorkQueueHandler handler =(MasterWorkQueueHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleActMessageOK(args);

       handler.postHandlerProtocol();

       return;
     }
   */
  public void handleActMessageOK(String[] args)
  {
    MasterWorkQueueHandler handler = (MasterWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

    handler.handleActMessageOK(args);

    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  static class LbMWQBranchesOptionList extends OptionList
  {
    /**
     *
     *
     */
    LbMWQBranchesOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;
      String defaultInstanceStateName =
          rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
          (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
            defaultInstanceStateName, true);

      int languageId = theSessionState.getLanguageId();
      try
      {
        clear();

        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUserAdminGetBranchesModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUserAdminGetBranchesModel.class);
        }

        //m.retrieve(eContext);
        m.clearUserWhereCriteria();
        m.executeSelect(null);
        m.beforeFirst();

        while (m.next())
        {
          Object dfBranchProfileId =
            m.getValue(doUserAdminGetBranchesModel.FIELD_DFBRANCHPROFILEID);
          Object dfBranchName = m.getValue(doUserAdminGetBranchesModel.FIELD_DFBRANCHNAME);
          
          int instId = ((BigDecimal)m.getValue(doUserAdminGetBranchesModel.FIELD_DFINSTITUTIONPROFILEID)).intValue();
          String instName = BXResources.getInstitutionName(instId); 
          
          String label = ((dfBranchName == null) ? "" : dfBranchName.toString() + ", " + instName);
          String value = ((dfBranchProfileId == null) ? "" : dfBranchProfileId.toString() + ", " + instId);
          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  /////////////////////

  /**
   *
   *
   */
  class LbMWQGroupsOptionList extends BaseComboBoxOptionList
  {
    /**
     *
     *
     */
    LbMWQGroupsOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      //Get the Language ID from the SessionStateModel
      String defaultInstanceStateName =
        rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
      SessionStateModelImpl theSessionState =
        (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
          defaultInstanceStateName, true);

      int languageId = theSessionState.getLanguageId();

      super.populateOptionList(rc, languageId, "GROUPPROFILE", lbMWQGroupsOptions);
    }
  }

  /**
   *
   *
   */

  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
  {
    /**
     *
     *
     */
    CbPageNamesOptionList()
    {
    }

    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();

        while (m.next())
        {
          Object dfPageLabel = m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = ((dfPageLabel == null) ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = ((dfPageId == null) ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];

        while (theList.hasNext())
        {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }

  //--DJ_Ticket661--end--//
  //--TD_MWQ_CR--start//

  /**
   * Since the population of comboboxes on the pages should be done from BXResource bundle, either
   * the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT element if place or
   * it could be done directly from the BXResource. Each approach has its pros and cons. The most
   * important is: for English and French versions could be different default values. It forces to
   * use the second approach. It this case to escape annoying code clone and follow the object
   * oriented design the following abstact base class should encapsulate the combobox OptionList
   * population. Each ComboBoxOptionList inner class should extend this base class and implement
   * the BXResources table name.
   */
  abstract class BaseComboBoxOptionList extends OptionList
  {
    /**
     *
     *
     */
    BaseComboBoxOptionList()
    {
    }

    //// In order to escape the double population for the splitted JSP(s): UW and
    //// DE the OptionList should be repopulated and set.
    protected final void populateOptionList(RequestContext rc, int langId, String tablename,
      OptionList name)
    {
        
      try
      {
       String defaultInstanceStateName = RequestManager
           .getRequestContext().getModelManager()
           .getDefaultModelInstanceName(SessionStateModel.class);

       SessionStateModelImpl theSessionState = (SessionStateModelImpl) RequestManager
           .getRequestContext().getModelManager().getModel(
                   SessionStateModel.class,
                   defaultInstanceStateName, true, true);
          
        //Get IDs and Labels from BXResources
        int institutionProfileId = theSessionState.getDealInstitutionId();
        if (institutionProfileId < 0)
        {
            institutionProfileId = theSessionState.getUserInstitutionId();
        }
        Collection c = BXResources.getPickListValuesAndDesc(institutionProfileId,tablename, langId);
        int tableSize = BXResources.getPickListTableSize(institutionProfileId,tablename, langId);
        Iterator l = c.iterator();

        String[] vals = new String[tableSize];
        String[] labels = new String[tableSize];
        String[] theVal = new String[2];

        int i = 0;

        while (l.hasNext())
        {
          theVal = (String[]) (l.next());
          vals[i] = theVal[1];
          labels[i] = theVal[0];
          i++;
        }

        name.setOptions(vals, labels);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }

    
    /*
     * mothod populate taskName optionList
     */
    protected final void populateTaskNameOptionList(RequestContext rc, int langId, String tablename,
            OptionList name)
    {
        try
        {
            String defaultInstanceStateName = RequestManager
            .getRequestContext().getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);

            SessionStateModelImpl theSessionState = (SessionStateModelImpl) RequestManager
            .getRequestContext().getModelManager().getModel(
                    SessionStateModel.class,
                    defaultInstanceStateName, true, true);

            Map<String, Object> taskMap = new HashMap<String, Object>();
            for(Integer institutionProfileId: theSessionState.institutionList()){

                Collection c = BXResources.getPickListValuesAndDesc(
                        institutionProfileId,tablename, langId);
                int tableSize = BXResources.getPickListTableSize(
                        institutionProfileId,tablename, langId);
                Iterator l = c.iterator();

                String[] vals = new String[tableSize];
                String[] labels = new String[tableSize];
                String[] theVal = new String[2];

                int i = 0;            

                while (l.hasNext())
                {
                    theVal = (String[]) (l.next());
                    labels[i] = theVal[1];
                    vals[i] = theVal[0];
                    List<Map> taskIdList = new ArrayList<Map>();
                    HashMap<Integer, Integer> taskIdMap = new HashMap<Integer, Integer>();

                    if(taskMap.containsKey(labels[i])) {
                        ArrayList<Map> previousList = (ArrayList<Map>)taskMap.get(labels[i]);
                        Iterator iter = previousList.iterator();

                        while(iter.hasNext()) {
                            Map previousMap = (Map)iter.next();
                            taskIdList.add(previousMap);
                        }
                        taskIdMap.put(institutionProfileId, Integer.valueOf(vals[i]));
                        taskIdList.add(taskIdMap);
                    }
                    else {
                        taskIdList = new ArrayList<Map>();                    
                        taskIdMap.put(institutionProfileId, Integer.valueOf(vals[i]));
                        taskIdList.add(taskIdMap);
                    }

                    taskMap.put(labels[i], taskIdList);
                    i++;
                }
            }
            TreeMap<String, Object> sortedMap = new TreeMap<String, Object>();
            sortedMap.putAll(taskMap);
            Iterator taskName = sortedMap.keySet().iterator();

            while(taskName.hasNext())
            {
                String taskNameKey = taskName.next().toString();
                String taskNameLabel = sortedMap.get(taskNameKey).toString();

                name.add(new Option(taskNameKey, taskNameLabel));
            }

        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage());
            logger.error(StringUtil.stack2string(ex));
        }
    }
  }

  /**
   *
   *
   */
  class CbTaskNameFilterOptionList extends BaseComboBoxOptionList
  {
    /**
     *
     *
     */
    CbTaskNameFilterOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      //Get the Language ID from the SessionStateModel
      String defaultInstanceStateName =
        rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
      SessionStateModelImpl theSessionState =
        (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
          defaultInstanceStateName, true);

      int languageId = theSessionState.getLanguageId();

      super.populateTaskNameOptionList(rc, languageId, "TASK741", cbTaskNameFilterOptions);
    }
  }

  //--TD_MWQ_CR--end//
  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/21/2004

  /**
   *
   */
  class CbDealStatusFilterOptionList extends BaseComboBoxOptionList
  {
    /**
     *
     */
    CbDealStatusFilterOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      //Get the Language ID from the SessionStateModel
      String defaultInstanceStateName =
        rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
      SessionStateModelImpl theSessionState =
        (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
          defaultInstanceStateName, true);

      int languageId = theSessionState.getLanguageId();

      // retrieve from property file: STATUS.properties
      super.populateOptionList(rc, languageId, "STATUS", cbDealStatusFilterOptions);
    }
  }

  /**
   *
   *
   */
  static class CbUserFilterOptionList extends OptionList
  {
    /**
     *
     *
     */
    CbUserFilterOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      Connection c = null;

      try
      {
        clear();

        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext =
          new SelectQueryExecutionContext((Connection) null,
            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null)
        {
          m = new doUnderwriterModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else
        {
          m = (SelectQueryModel) rc.getModelManager().getModel(doUnderwriterModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        while (m.next())
        {
          Object dfUserProfileId = m.getValue(doUnderwriterModel.FIELD_DFUSERPROFILEID);
          Object dfUsername = m.getValue(doUnderwriterModel.FIELD_DFUSERNAME);

          String label = ((dfUsername == null) ? "" : dfUsername.toString());

          String value = ((dfUserProfileId == null) ? "" : dfUserProfileId.toString());

          add(label, value);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (c != null)
          {
            c.close();
          }
        }
        catch (SQLException ex)
        {
          // ignore
        }
      }
    }
  }
  
  
  /*
     * Combo Box for Institution
     */
    class CbInstitutionOptions extends
            pgIWorkQueueViewBean.CbInstitutionOptions {

        /**
         * constructor.
         */
        CbInstitutionOptions() {

        }
    }

}
