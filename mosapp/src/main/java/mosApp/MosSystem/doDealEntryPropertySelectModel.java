package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doDealEntryPropertySelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyId(java.math.BigDecimal value);

	
    /**
     * 
     * 
     */
    public java.math.BigDecimal getDfCopyId();

    
    /**
     * 
     * 
     */
    public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryPropertyFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryPropertyFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetName();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetType();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetDirection();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetDirection(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfProvince();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvince(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPurchasePrice();

	
	/**
	 * 
	 * 
	 */
	public void setDfPurchasePrice(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLTV();

	
	/**
	 * 
	 * 
	 */
	public void setDfLTV(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFPROPERTYID="dfPropertyId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFPRIMARYPROPERTYFLAG="dfPrimaryPropertyFlag";
	public static final String FIELD_DFSTREETNUMBER="dfStreetNumber";
	public static final String FIELD_DFSTREETNAME="dfStreetName";
	public static final String FIELD_DFSTREETTYPE="dfStreetType";
	public static final String FIELD_DFSTREETDIRECTION="dfStreetDirection";
	public static final String FIELD_DFPROVINCE="dfProvince";
	public static final String FIELD_DFPURCHASEPRICE="dfPurchasePrice";
	public static final String FIELD_DFLTV="dfLTV";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

