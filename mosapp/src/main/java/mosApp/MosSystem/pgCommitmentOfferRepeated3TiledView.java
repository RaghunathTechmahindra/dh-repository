package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.filogix.util.Xc;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 *
 */
public class pgCommitmentOfferRepeated3TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated3TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(200);
		setPrimaryModelClass( doCusConditionModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getTbCusCondition().setValue(CHILD_TBCUSCONDITION_RESET_VALUE);
		getBtCusDeleteCondition().setValue(CHILD_BTCUSDELETECONDITION_RESET_VALUE);
		getTbDefCondition().setValue(CHILD_TBDEFCONDITION_RESET_VALUE);
		getImDefCondBtnLess().setValue(CHILD_IMDEFCONDBTNLESS_RESET_VALUE);
		getImDefCondBtnMore().setValue(CHILD_IMDEFCONDBTNMORE_RESET_VALUE);
		getImViewDefCondBtn().setValue(CHILD_IMVIEWDEFCONDBTN_RESET_VALUE);
		getChUseDefaultCondition().setValue(CHILD_CHUSEDEFAULTCONDITION_RESET_VALUE);
		getHdDisplayDefaultCondition().setValue(CHILD_HDDISPLAYDEFAULTCONDITION_RESET_VALUE);
		getTbCusDocumentLabel().setValue(CHILD_TBCUSDOCUMENTLABEL_RESET_VALUE);
		getCbCusAction().setValue(CHILD_CBCUSACTION_RESET_VALUE);
		getCbCusResponsibility().setValue(CHILD_CBCUSRESPONSIBILITY_RESET_VALUE);
		getHdCusDocTrackId().setValue(CHILD_HDCUSDOCTRACKID_RESET_VALUE);
	}

	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_TBCUSCONDITION,TextField.class);
		registerChild(CHILD_BTCUSDELETECONDITION,Button.class);
		registerChild(CHILD_TBDEFCONDITION,TextField.class);
		registerChild(CHILD_IMDEFCONDBTNLESS, ImageField.class);
		registerChild(CHILD_IMDEFCONDBTNMORE, ImageField.class);
		registerChild(CHILD_IMVIEWDEFCONDBTN, ImageField.class);
		registerChild(CHILD_CHUSEDEFAULTCONDITION, CheckBox.class);
		registerChild(CHILD_HDDISPLAYDEFAULTCONDITION, HiddenField.class); 
		registerChild(CHILD_TBCUSDOCUMENTLABEL,TextField.class);
		registerChild(CHILD_CBCUSACTION,ComboBox.class);
		registerChild(CHILD_CBCUSRESPONSIBILITY,ComboBox.class);
		registerChild(CHILD_HDCUSDOCTRACKID,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoCusConditionModel());
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

	    //--Release2.1--//
	    //Populate all ComboBoxes manually here
	    //--> By Billy 26Nov2002
	    cbCusActionOptions.populate(getRequestContext());
	    cbCusResponsibilityOptions.populate(getRequestContext());
	    
	    if(!displayDefaultConditions()) {
	    	this.getTbDefCondition().setVisible(false);
	    	this.getChUseDefaultCondition().setVisible(false);
	    	this.getImDefCondBtnLess().setVisible(false);
	    	this.getImDefCondBtnMore().setVisible(false);
	    	this.getImViewDefCondBtn().setVisible(false);
	    } 
	    
		super.beginDisplay(event);		
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
			
			handler.pageGetState(this.getParentViewBean());
			boolean retval = handler.checkDisplayThisRow("Repeated3");
			handler.setButtonsDisplayFields(getTileIndex());
			
//			if("Y".equalsIgnoreCase((String)displayDefault.getValue())) {
//				ImageField vbtn = (ImageField)getChild(CHILD_IMVIEWDEFCONDBTN, getTileIndex());
//				vbtn.setExtraHtml("style=\"display:none;\"");
//			}

			HiddenField displayDefault = (HiddenField)getChild(CHILD_HDDISPLAYDEFAULTCONDITION, getTileIndex());

			if (hasDefCondMatch()){
			    if(expandDefaultCondition()) {
        		        displayDefault.setValue("Y");
        		    } else {
        		        displayDefault.setValue("N");
                            }
			} else {
                            displayDefault.setValue("N");
			}
			
			handler.pageSaveState();

			return retval;
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_TBCUSCONDITION))
		{
			TextField child = new TextField(this,
				getdoCusConditionModel(),
				CHILD_TBCUSCONDITION,
				doCusConditionModel.FIELD_DFCUSCONDITION,
				CHILD_TBCUSCONDITION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCUSDELETECONDITION))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCUSDELETECONDITION,
				CHILD_BTCUSDELETECONDITION,
				CHILD_BTCUSDELETECONDITION_RESET_VALUE,
				null);
				return child;
		}
		else
		if (name.equals(CHILD_IMDEFCONDBTNLESS))
		{
			ImageField child = new ImageField(this,
				getDefaultModel(),
				CHILD_IMDEFCONDBTNLESS,
				CHILD_IMDEFCONDBTNLESS,
				CHILD_IMDEFCONDBTNLESS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_IMDEFCONDBTNMORE))
		{
			ImageField child = new ImageField(this,
				getDefaultModel(),
				CHILD_IMDEFCONDBTNMORE,
				CHILD_IMDEFCONDBTNMORE,
				CHILD_IMDEFCONDBTNMORE_RESET_VALUE,
				null);
			return child;
		}
		else
			if (name.equals(CHILD_IMVIEWDEFCONDBTN))
			{
				ImageField child = new ImageField(this,
					getdoCusConditionModel(),
					CHILD_IMVIEWDEFCONDBTN,
					CHILD_IMVIEWDEFCONDBTN,
					CHILD_IMVIEWDEFCONDBTN_RESET_VALUE,
					null);
				return child;
			}
		else
		if (name.equals(CHILD_TBDEFCONDITION))
		{
			TextField child = new TextField(this,
				getdoCusConditionModel(),
				CHILD_TBDEFCONDITION,
				doCusConditionModel.FIELD_DFDEFCONDITION,
				CHILD_TBDEFCONDITION_RESET_VALUE,
				null);
			return child;
		}
		else 
		if (name.equals(CHILD_CHUSEDEFAULTCONDITION)) {
            CheckBox child = new CheckBox(
                    this,
                    getdoCusConditionModel(),
                    CHILD_CHUSEDEFAULTCONDITION,
                    doCusConditionModel.FIELD_DFUSEDEFAULT,
                    "Y",
                    "N",
                    false,
                    null);
            return child;
        }
		else
		if (name.equals(CHILD_TBCUSDOCUMENTLABEL))
		{
			TextField child = new TextField(this,
				getdoCusConditionModel(),
				CHILD_TBCUSDOCUMENTLABEL,
				doCusConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_TBCUSDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBCUSACTION))
		{
			ComboBox child = new ComboBox( this,
				getdoCusConditionModel(),
				CHILD_CBCUSACTION,
				doCusConditionModel.FIELD_DFACTION,
				CHILD_CBCUSACTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbCusActionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBCUSRESPONSIBILITY))
		{
			ComboBox child = new ComboBox( this,
				getdoCusConditionModel(),
				CHILD_CBCUSRESPONSIBILITY,
				doCusConditionModel.FIELD_DFRESPONSIBILITY,
				CHILD_CBCUSRESPONSIBILITY_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbCusResponsibilityOptions);
			return child;
		}
		else
		if (name.equals(CHILD_HDCUSDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoCusConditionModel(),
				CHILD_HDCUSDOCTRACKID,
				doCusConditionModel.FIELD_DFCUSDOCTRACKID,
				CHILD_HDCUSDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDISPLAYDEFAULTCONDITION))
		{
			HiddenField child = new HiddenField(this,
				getdoCusConditionModel(),
				CHILD_HDDISPLAYDEFAULTCONDITION,
				doCusConditionModel.FIELD_DFDISPLAYDEFAULT,
				CHILD_HDDISPLAYDEFAULTCONDITION,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public TextField getTbCusCondition()
	{
		return (TextField)getChild(CHILD_TBCUSCONDITION);
	}


	/**
	 *
	 *
	 */
	public void handleBtCusDeleteConditionRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCusDeleteCondition_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleDelete(handler.getRowNdxFromWebEventMethod(event), "btCusDeleteCondition");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtCusDeleteCondition()
	{
		return (Button)getChild(CHILD_BTCUSDELETECONDITION);
	}


	/**
	 *
	 *
	 */
	public String endBtCusDeleteConditionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btCusDeleteCondition_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}
	
	/**
	 *
	 *
	 */
	public TextField getTbDefCondition()
	{
		return (TextField)getChild(CHILD_TBDEFCONDITION);
	}
	
	/**
	 * Controls whether or not to display default condition box in the current tile.
	 *
	 */
	public String endTbDefConditionDisplay(ChildContentDisplayEvent event)
	{
		//Check if there is a matching default condition.
	    if(hasDefCondMatch() ) {		
	    	return event.getContent();
	    } else {
	    	return "";
	    }
	}
	
	public ImageField getImDefCondBtnLess()
	{
		return (ImageField)getChild(CHILD_IMDEFCONDBTNLESS);
	}
	
	public ImageField getImDefCondBtnMore()
	{
		return (ImageField)getChild(CHILD_IMDEFCONDBTNMORE);
	}
	
	public ImageField getImViewDefCondBtn()
	{
		return (ImageField)getChild(CHILD_IMVIEWDEFCONDBTN);
	}
	
	/**
	 * Controls whether or not to display ImViewDefCondBtn in the current tile.
	 *
	 */
	public String endImViewDefCondBtnDisplay(ChildContentDisplayEvent event)
	{
		//Check if there is a matching default condition.
		if(hasDefCondMatch()) {
	    	return event.getContent();
	    } else {
	    	return "";
	    }
	}
	
    /**
     * getChUseDefaultCondition
     *
     * @param None <br>
     * @return CheckBox : the result of getChUseDefaultCondition <br>
     */
    public CheckBox getChUseDefaultCondition() {
        return (CheckBox) getChild(CHILD_CHUSEDEFAULTCONDITION);
    }
    
	public String endChUseDefaultConditionDisplay(ChildContentDisplayEvent event)
	{
		//Check if there is a matching default condition.
	    if(hasDefCondMatch()) {
	    	return event.getContent();
	    } else {
	    	return "";
	    }
	}

	/**
	 *
	 *
	 */
	public TextField getTbCusDocumentLabel()
	{
		return (TextField)getChild(CHILD_TBCUSDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbCusAction()
	{
		return (ComboBox)getChild(CHILD_CBCUSACTION);
	}

	/**
	 *
	 *
	 */
	static class CbCusActionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbCusActionOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 26Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "DOCUMENTSTATUS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbCusResponsibility()
	{
		return (ComboBox)getChild(CHILD_CBCUSRESPONSIBILITY);
	}

	/**
	 *
	 *
	 */
	static class CbCusResponsibilityOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbCusResponsibilityOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 26Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "CONDITIONRESPONSIBILITYROLE", languageId);

        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public HiddenField getHdCusDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDCUSDOCTRACKID);
	}
	


	public HiddenField getHdDisplayDefaultCondition() {
		return (HiddenField)getChild(CHILD_HDDISPLAYDEFAULTCONDITION);
	}


	/**
	 *
	 *
	 */
	public doCusConditionModel getdoCusConditionModel()
	{
		if (doCusCondition == null)
			doCusCondition = (doCusConditionModel) getModel(doCusConditionModel.class);
		return doCusCondition;
	}
	
	/**
	 *
	 *
	 */
	public void setdoCusConditionModel(doCusConditionModel model)
	{
			doCusCondition = model;
	}
	


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBCUSCONDITION="tbCusCondition";
	public static final String CHILD_TBCUSCONDITION_RESET_VALUE="";
	public static final String CHILD_BTCUSDELETECONDITION="btCusDeleteCondition";
	public static final String CHILD_BTCUSDELETECONDITION_RESET_VALUE="Delete Condition";
	public static final String CHILD_TBDEFCONDITION="tbDefCondition";
	public static final String CHILD_TBDEFCONDITION_RESET_VALUE="";
	public static final String CHILD_IMDEFCONDBTNLESS="imDefCondBtnLess";
	public static final String CHILD_IMDEFCONDBTNLESS_RESET_VALUE="";
	public static final String CHILD_IMDEFCONDBTNMORE="imDefCondBtnMore";
	public static final String CHILD_IMDEFCONDBTNMORE_RESET_VALUE="";
	public static final String CHILD_IMVIEWDEFCONDBTN="imViewDefCondBtn";
	public static final String CHILD_IMVIEWDEFCONDBTN_RESET_VALUE="";
	public static final String CHILD_CHUSEDEFAULTCONDITION="chUseDefaultCondition";
	public static final String CHILD_CHUSEDEFAULTCONDITION_RESET_VALUE="N";
	public static final String CHILD_HDDISPLAYDEFAULTCONDITION="hdDisplayDefaultCondition";
	public static final String CHILD_HDDISPLAYDEFAULTCONDITION_RESET_VALUE="N";
	public static final String CHILD_TBCUSDOCUMENTLABEL="tbCusDocumentLabel";
	public static final String CHILD_TBCUSDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_CBCUSACTION="cbCusAction";
	public static final String CHILD_CBCUSACTION_RESET_VALUE="";
  //--Release2.1--//
	//private static CbCusActionOptionList cbCusActionOptions=new CbCusActionOptionList();
  private CbCusActionOptionList cbCusActionOptions=new CbCusActionOptionList();
	public static final String CHILD_CBCUSRESPONSIBILITY="cbCusResponsibility";
	public static final String CHILD_CBCUSRESPONSIBILITY_RESET_VALUE="";
  //--Release2.1--//
	//private static CbCusResponsibilityOptionList cbCusResponsibilityOptions=new CbCusResponsibilityOptionList();
	private CbCusResponsibilityOptionList cbCusResponsibilityOptions=new CbCusResponsibilityOptionList();
	public static final String CHILD_HDCUSDOCTRACKID="hdCusDocTrackId";
	public static final String CHILD_HDCUSDOCTRACKID_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doCusConditionModel doCusCondition=null;
  private ConditionsReviewHandler handler=new ConditionsReviewHandler();
  
/**
 * @return true if there is a default condition matching the current custom condition.
 */
	private boolean hasDefCondMatch() {
		return this.doCusCondition.getDfConditionId().intValue() != 0;
	}
	
	/**
	 * Determine if we should display default conditions or not based on the folowing criteria:
	 * 1. duplicate.default.conditions in sysproperty table should be 'Y'
	 * 2. user came from Underwriter Work Sheet.
	 * When both conditions are true, method returns true. If either or both is/are false, we return false.
	 * @return
	 */
	private boolean displayDefaultConditions() {
		
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		SessionStateModelImpl theSessionState = handler.getTheSessionState();
	    String prop = 
	    	PropertiesCache.getInstance().getProperty(theSessionState.
	    			getDealInstitutionId(), Xc.DUPLICATE_DEFAULT_CONDITIONS, "Y");

	    boolean from_UWWS = handler.isFromUWWS();
	    return ("Y".equalsIgnoreCase(prop)) &&  from_UWWS;
	}

	/**
	 * Determine if expand default conditions and don't display "view Default condition" button
	 * 1. duplicate.default.conditions.expand in sysproperty table is 'Y'
	 */

	private boolean expandDefaultCondition() {
		
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		SessionStateModelImpl theSessionState = handler.getTheSessionState();
		
		String displayProp = 
	    	PropertiesCache.getInstance().getProperty(theSessionState.
	    			getDealInstitutionId(),Xc.DUPLICATE_DEFAULT_CONDITIONS, "Y");

		String expandProp = 
	    	PropertiesCache.getInstance().getProperty(theSessionState.
	    			getDealInstitutionId(),Xc.DUPLICATE_DEFAULT_CONDITIONS_EXPANDED, "Y");
		
		//Only when both duplicate.default.conditions and duplicate.default.conditions.expanded are Y,
		//do we enable the expanded view.

	    return "Y".equalsIgnoreCase(displayProp) && "Y".equalsIgnoreCase(expandProp);
	}


}

