package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import java.text.*;

/**
 *
 *
 *
 */
public class doUserExistsModelImpl extends QueryModelBase
	implements doUserExistsModel
{
	/**
	 *
	 *
	 */
	public doUserExistsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUSERPROFILEID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUSERPROFILEID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUSERLOGIN()
	{
		return (String)getValue(FIELD_DFUSERLOGIN);
	}


	/**
	 *
	 *
	 */
	public void setDfUSERLOGIN(String value)
	{
		setValue(FIELD_DFUSERLOGIN,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUSERTYPEID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUSERTYPEID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGROUPPROFILEID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGROUPPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfGROUPPROFILEID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGROUPPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfHigherApproverUerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFHIGHERAPPROVERUERID);
	}


	/**
	 *
	 *
	 */
	public void setDfHigherApproverUerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFHIGHERAPPROVERUERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfJOINTAPPROVERUSERID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFJOINTAPPROVERUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfJOINTAPPROVERUSERID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFJOINTAPPROVERUSERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCONTACTID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONTACTID);
	}


	/**
	 *
	 *
	 */
	public void setDfCONTACTID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONTACTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPROFILESTATUSID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROFILESTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfPROFILESTATUSID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROFILESTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfSTANDARDACCESS()
	{
		return (String)getValue(FIELD_DFSTANDARDACCESS);
	}


	/**
	 *
	 *
	 */
	public void setDfSTANDARDACCESS(String value)
	{
		setValue(FIELD_DFSTANDARDACCESS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBILINGUAL()
	{
		return (String)getValue(FIELD_DFBILINGUAL);
	}


	/**
	 *
	 *
	 */
	public void setDfBILINGUAL(String value)
	{
		setValue(FIELD_DFBILINGUAL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUTDESCRIPTION()
	{
		return (String)getValue(FIELD_DFUTDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfUTDESCRIPTION(String value)
	{
		setValue(FIELD_DFUTDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCONTACTFIRSTNAME()
	{
		return (String)getValue(FIELD_DFCONTACTFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfCONTACTFIRSTNAME(String value)
	{
		setValue(FIELD_DFCONTACTFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCONTACTMIDDLEINITIAL()
	{
		return (String)getValue(FIELD_DFCONTACTMIDDLEINITIAL);
	}


	/**
	 *
	 *
	 */
	public void setDfCONTACTMIDDLEINITIAL(String value)
	{
		setValue(FIELD_DFCONTACTMIDDLEINITIAL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCONTACTLASTNAME()
	{
		return (String)getValue(FIELD_DFCONTACTLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfCONTACTLASTNAME(String value)
	{
		setValue(FIELD_DFCONTACTLASTNAME,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, USERPROFILE.USERLOGIN, USERPROFILE.USERTYPEID, USERPROFILE.GROUPPROFILEID, USERPROFILE.HIGHERAPPROVERUSERID, USERPROFILE.JOINTAPPROVERUSERID, USERPROFILE.CONTACTID, USERPROFILE.PROFILESTATUSID, USERPROFILE.STANDARDACCESS, USERPROFILE.BILINGUAL, USERTYPE.UTDESCRIPTION, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME FROM USERPROFILE, USERTYPE, CONTACT  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="USERPROFILE, USERTYPE, CONTACT";
	public static final String STATIC_WHERE_CRITERIA=" (USERPROFILE.USERTYPEID  =  USERTYPE.USERTYPEID) AND (USERPROFILE.CONTACTID  =  CONTACT.CONTACTID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID="USERPROFILE.USERPROFILEID";
	public static final String COLUMN_DFUSERPROFILEID="USERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFUSERLOGIN="USERPROFILE.USERLOGIN";
	public static final String COLUMN_DFUSERLOGIN="USERLOGIN";
	public static final String QUALIFIED_COLUMN_DFUSERTYPEID="USERPROFILE.USERTYPEID";
	public static final String COLUMN_DFUSERTYPEID="USERTYPEID";
	public static final String QUALIFIED_COLUMN_DFGROUPPROFILEID="USERPROFILE.GROUPPROFILEID";
	public static final String COLUMN_DFGROUPPROFILEID="GROUPPROFILEID";
	public static final String QUALIFIED_COLUMN_DFHIGHERAPPROVERUERID="USERPROFILE.HIGHERAPPROVERUSERID";
	public static final String COLUMN_DFHIGHERAPPROVERUERID="HIGHERAPPROVERUSERID";
	public static final String QUALIFIED_COLUMN_DFJOINTAPPROVERUSERID="USERPROFILE.JOINTAPPROVERUSERID";
	public static final String COLUMN_DFJOINTAPPROVERUSERID="JOINTAPPROVERUSERID";
	public static final String QUALIFIED_COLUMN_DFCONTACTID="USERPROFILE.CONTACTID";
	public static final String COLUMN_DFCONTACTID="CONTACTID";
	public static final String QUALIFIED_COLUMN_DFPROFILESTATUSID="USERPROFILE.PROFILESTATUSID";
	public static final String COLUMN_DFPROFILESTATUSID="PROFILESTATUSID";
	public static final String QUALIFIED_COLUMN_DFSTANDARDACCESS="USERPROFILE.STANDARDACCESS";
	public static final String COLUMN_DFSTANDARDACCESS="STANDARDACCESS";
	public static final String QUALIFIED_COLUMN_DFBILINGUAL="USERPROFILE.BILINGUAL";
	public static final String COLUMN_DFBILINGUAL="BILINGUAL";
	public static final String QUALIFIED_COLUMN_DFUTDESCRIPTION="USERTYPE.UTDESCRIPTION";
	public static final String COLUMN_DFUTDESCRIPTION="UTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFCONTACTFIRSTNAME="CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_DFCONTACTFIRSTNAME="CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFCONTACTMIDDLEINITIAL="CONTACT.CONTACTMIDDLEINITIAL";
	public static final String COLUMN_DFCONTACTMIDDLEINITIAL="CONTACTMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFCONTACTLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFCONTACTLASTNAME="CONTACTLASTNAME";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERLOGIN,
				COLUMN_DFUSERLOGIN,
				QUALIFIED_COLUMN_DFUSERLOGIN,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERTYPEID,
				COLUMN_DFUSERTYPEID,
				QUALIFIED_COLUMN_DFUSERTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGROUPPROFILEID,
				COLUMN_DFGROUPPROFILEID,
				QUALIFIED_COLUMN_DFGROUPPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHIGHERAPPROVERUERID,
				COLUMN_DFHIGHERAPPROVERUERID,
				QUALIFIED_COLUMN_DFHIGHERAPPROVERUERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOINTAPPROVERUSERID,
				COLUMN_DFJOINTAPPROVERUSERID,
				QUALIFIED_COLUMN_DFJOINTAPPROVERUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTID,
				COLUMN_DFCONTACTID,
				QUALIFIED_COLUMN_DFCONTACTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROFILESTATUSID,
				COLUMN_DFPROFILESTATUSID,
				QUALIFIED_COLUMN_DFPROFILESTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTANDARDACCESS,
				COLUMN_DFSTANDARDACCESS,
				QUALIFIED_COLUMN_DFSTANDARDACCESS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBILINGUAL,
				COLUMN_DFBILINGUAL,
				QUALIFIED_COLUMN_DFBILINGUAL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUTDESCRIPTION,
				COLUMN_DFUTDESCRIPTION,
				QUALIFIED_COLUMN_DFUTDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTFIRSTNAME,
				COLUMN_DFCONTACTFIRSTNAME,
				QUALIFIED_COLUMN_DFCONTACTFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTMIDDLEINITIAL,
				COLUMN_DFCONTACTMIDDLEINITIAL,
				QUALIFIED_COLUMN_DFCONTACTMIDDLEINITIAL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTLASTNAME,
				COLUMN_DFCONTACTLASTNAME,
				QUALIFIED_COLUMN_DFCONTACTLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

