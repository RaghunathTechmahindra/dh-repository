package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doApplicantOtherIncomesSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncomeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomePeriodId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomePeriodId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeAmt();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeAmt(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludeInGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludeInGDS(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludeInTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludeInTDS(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFINCOMEID="dfIncomeId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFINCOMETYPEID="dfIncomeTypeId";
	public static final String FIELD_DFINCOMEDESC="dfIncomeDesc";
	public static final String FIELD_DFINCOMEPERIODID="dfIncomePeriodId";
	public static final String FIELD_DFINCOMEAMT="dfIncomeAmt";
	public static final String FIELD_DFINCLUDEINGDS="dfIncludeInGDS";
	public static final String FIELD_DFPERCENTINCLUDEDINGDS="dfPercentIncludedInGDS";
	public static final String FIELD_DFINCLUDEINTDS="dfIncludeInTDS";
	public static final String FIELD_DFPERCENTINCLUDEDINTDS="dfPercentIncludedInTDS";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

