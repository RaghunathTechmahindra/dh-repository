package mosApp.MosSystem;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ContainerView;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;

/**
 * This class is added for FXP27053 -- Deal Notes Lazy Load 
 */
public class pgDealNotes2ViewBean extends ExpressViewBeanBase {

	public static final String PAGE_NAME = "pgDealNotes2";
	public static final String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgDealNotes2.jsp";
	public static final String CHILD_STDEALID = "stDealId";
	public static final String CHILD_STDEALID_RESET_VALUE = "";
	public static final String CHILD_DEALNOTETILE = "dealNoteTile";

	int dealId;
	boolean dealPage;
	int languageId;

	public pgDealNotes2ViewBean() {
	    super(PAGE_NAME);
	    setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
	    registerChild(CHILD_STDEALID, StaticTextField.class);
	    registerChild(CHILD_DEALNOTETILE, pgDealNotes2TiledView.class);

	    DealNotesHandler handler = new DealNotesHandler();
	    try{
	        handler.pageGetState(this);
	        dealId = handler.getTheSessionState().getCurrentPage().getPageDealId();
	        dealPage = handler.getTheSessionState().getCurrentPage().isDealPage();
	        languageId = handler.getTheSessionState().getLanguageId();
	    }finally{
	        handler.unsetSessionResourceKit();
	    }

	}

	protected View createChild(String name) {
		if (CHILD_STDEALID.equals(name)) {
			StaticTextField child = new StaticTextField(this, getDefaultModel(), CHILD_STDEALID, CHILD_STDEALID, CHILD_STDEALID_RESET_VALUE, null);
			return child;
		} else if (CHILD_DEALNOTETILE.equals(name)) {
			pgDealNotes2TiledView child = new pgDealNotes2TiledView(this, CHILD_DEALNOTETILE);
			return child;
		}
		return super.createChild(name);
	}

	public void resetChildren() {
		((DisplayField) this.getChild(CHILD_STDEALID)).setValue(CHILD_STDEALID_RESET_VALUE);
		((ContainerView) this.getChild(CHILD_DEALNOTETILE)).resetChildren();
		super.resetChildren();
	}

	public void beginDisplay(DisplayEvent event) throws ModelControlException {
		((DisplayField) this.getChild(CHILD_STDEALID)).setValue(dealId);
		super.beginDisplay(event);
	}
	
	public String getDisplayURL() {
        return BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
	}
}
