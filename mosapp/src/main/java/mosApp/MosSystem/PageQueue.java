package mosApp.MosSystem;

import java.util.Vector;
import java.io.*;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;

/**
 *
 *
 */
public class PageQueue extends Object implements Serializable
{
	private Vector queue=null;
	private int queueSize=0;
	private int queueSizeMax=0;
	private static int[] ADD_EXCLUSIONS_LIST={ Mc.PGNM_SIGN_ON_ID, Mc.PGNM_SIGNOFF_ID, Mc.PGNM_INDIV_WORK_QUEUE_ID, Mc.PGNM_DEAL_ENTRY_ID, Mc.PGNM_MASTER_WORK_QUEUE_ID, Mc.PGNM_DEAL_SEARCH_ID, Mc.PGNM_CHANGE_PASSWORD_ID, Mc.PGNM_PARTY_ADD};
	private static int ADD_EXCLUSIONS_LIST_SIZE=ADD_EXCLUSIONS_LIST.length;

  public SysLogger logger;


	/**
	 *
	 *
	 */
	public PageQueue(int aSize)
	{
		queueSizeMax = aSize;

		queue = new Vector(queueSizeMax);

	}


	/**
	 *
	 *
	 */
	public void add(PageEntry pg)
	{

		// check for exclusion
		int pgId = pg.getPageId();

		if (pg.isSubPage() == true)
		{
			return;
		}

		if (pgId == 0)
		{
			return;
		}

		for(int i = 0;
		i < ADD_EXCLUSIONS_LIST_SIZE;
		++ i)
		{
			if (pgId == ADD_EXCLUSIONS_LIST [ i ]) return;
		}

		queue.insertElementAt(pg, 0);


		// check for overflow
		if (queueSize == queueSizeMax) queue.removeElementAt(queueSize);
		else queueSize ++;

	}


	/**
	 *  SessionState class is converted into the SessionStateModel, but
	 *  particularly this method is moved to the SessionStateModel.
	 *  Current signature is left for compability only. Should be changed later on.
	 *
	 */
  public void add(SessionStateModelImpl theSessionState)
	{
		////PageEntry pg = theSession.getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

		if (pg != null)
		{
			add(pg);
		}

	}


	/**
	 *
	 *
	 */
	public int getQueueSize()
	{
		return queueSize;

	}


	/**
	 *
	 *
	 */
	public PageEntry get(int queueNdx)
	{

		//retrun first element if the index is outside the range
		if (queueNdx <= 0 || queueNdx > queueSize - 1)
		{
			return(PageEntry) queue.elementAt(0);
		}

		return(PageEntry) queue.elementAt(queueNdx);

	}


	/**
	 *
	 *
	 */
	public void remove(int queueNdx)
	{

		//remove first element if the index is outside the range
		if (queueNdx <= 0 || queueNdx > queueSize - 1)
		{
			queue.removeElementAt(0);
		}
		else
		{
			queue.removeElementAt(queueNdx);
		}

		queueSize --;

	}


	/**
	 * delete pageEntry from the PageEntryQueue, if it already has the identical PageEntry 
	 * this method identify a pageEntry by 1.pageId, 2.DealId, 3.institutionId (since 3.3) 
	 */
	public void removeIfQueued(SessionStateModel theSessionState) {

		PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

		for (int i = 0; i < queueSize; i++) {
			PageEntry aPGinQueue = (PageEntry) queue.elementAt(i);

			if (aPGinQueue.getPageId() != pg.getPageId())
				continue;
			if (aPGinQueue.getPageDealId() != pg.getPageDealId())
				continue;
			if (aPGinQueue.getDealInstitutionId() != pg.getDealInstitutionId())
				continue;

			queue.removeElementAt(i);
			queueSize--;
			break;
		}

	}

}

