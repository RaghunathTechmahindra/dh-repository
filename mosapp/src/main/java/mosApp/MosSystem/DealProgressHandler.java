package mosApp.MosSystem;

import java.text.*;
import java.util.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.deal.security.*;
import com.basis100.resources.*;
import com.basis100.picklist.*;
import com.basis100.deal.util.*;

import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 */
public class DealProgressHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public DealProgressHandler cloneSS()
	{
		return(DealProgressHandler) super.cloneSafeShallow();

	}


	////////////////////////////////////////////////////////////////////////

	public void setupBeforePageGeneration()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);


		// cursor object for work queue tasks list (named "DEFAULT" for this page)
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		if (tds == null)
		{
			// determine number of task (shown) per display page
			String voName = "Repeated1";
			int tasksPerPage = ((TiledView)(getCurrNDPage().getChild(voName))).getMaxDisplayTiles();

			// set up and save task display state object
			tds = new PageCursorInfo("DEFAULT", voName, tasksPerPage, 0);

			// last (0) - don't know total number yet!
			tds.setRefresh(true);
			tds.setFilterOption(0, false);
			pst.put(tds.getName(), tds);
		}

		Vector assignedTo = null;


		// ensure data objects have correct criteria ...
		////CSpDataObject wqDO = getModel(doDealProgressTasksModel.class);
    doDealProgressTasksModelImpl wqDO = (doDealProgressTasksModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doDealProgressTasksModel.class));

		assignedTo = new Vector();

		pst.put("ASSIGNED_TO", assignedTo);

		setDealProgressCriteria(wqDO, tds, pg);

		logger.debug("DP :: setupBeforePageGeneration 3");


		// execute work queue DO and store application ids (for snap shot population)
		// and work queue record id (for task selection support)
    //--> For debug !!
		//showCriteria(wqDO);
		////spider.database.CSpDBResultTable resultTab = null;

		try
		{
			wqDO.executeSelect(null);
			tds.setTotalRows(wqDO.getSize());
			////resultTab = wqDO.getLastResults().getResultTable();
			logger.debug("DP :: setupBeforePageGeneration 4");

		int lim = tds.getTotalRows();

      // store state info

      while (wqDO.next())
      {
        int profileId = com.iplanet.jato.util.TypeConverter.asInt(wqDO.getValue(doDealProgressTasksModel.FIELD_DFUSERPROFILEID));
        //int ndx = profileId.indexOf(".");
        //if (ndx > 0) profileId = profileId.substring(0, ndx);
        assignedTo.addElement(new Integer(profileId));
        // changes to user names below
      }
    }
		catch(Exception ex)
		{
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Problem encountered getting work queue tasks");
			logger.error(ex);
			return;
		}

		logger.debug("DP :: setupBeforePageGeneration 5");

		// determine if we will force display of first page
		if (tds.isTotalRowsChanged() || tds.isCriteriaChanged())
		{
			// number of rows has changed - unconditional display of first page
			TiledView vo =(TiledView)(getCurrNDPage().getChild(tds.getVoName()));
			tds.setCurrPageNdx(0);
      tds.setTotalRowsChanged(false);
			tds.setCriteriaChanged(false);
			try
      {
			  vo.resetTileIndex();
      }
      catch (ModelControlException mce)
      {
        logger.debug("DP :: @setupBeforePageGeneration::Exception: " + mce);
      }

		}

		setupAssignedUserList(assignedTo);

		logger.debug("DP :: setupBeforePageGeneration 6");

		setupDealSummarySnapShotDO(pg);

		logger.debug("DP :: setupBeforePageGeneration 7");

	}


	////////////////////////////////////////////////////////////////////////
	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		populatePageShellDisplayFields();

		// Quick Link Menu display
		displayQuickLinkMenu();

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();

		revisePrevNextButtonGeneration(pg, tds);


		// generate tasks displayed display field
		populateRowsDisplayed(getCurrNDPage(), tds);

	}


	////////////////////////////////////////////////////////////////////////

	public void populatedRepeatedFields(int ndx)
	{
		logger.debug("DP :: populatedRepeatedFields");

		PageEntry pg = getTheSessionState().getCurrentPage();

		logger.debug("DP :: populatedDPRepeatedFields");

		Hashtable pst = pg.getPageStateTable();

		logger.debug("DP :: populatedDPRepeatedFields 2");

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		logger.debug("DP :: populatedDPRepeatedFields 3");

		int absNdx = getCursorAbsoluteNdx(tds, ndx);

		String dispStr = "";

		try
		{
			if (tds.getTotalRows() > 0)
			{
				Vector assignedTo =(Vector) pst.get("ASSIGNED_TO");
				dispStr =(String) assignedTo.elementAt(absNdx);
			}
		}
		catch(Exception e)
		{
			dispStr = "Cannot Determine!";
		}

		getCurrNDPage().setDisplayFieldValue("Repeated1/stAssignedTo", new String(dispStr));

		logger.debug("DP :: populatedDPRepeatedFields 4");

		return;

	}


	////////////////////////////////////////////////////////////////////////

	public void showCriteria(QueryModelBase doC)
	{
		System.out.println("\nData Object Criteria: do = " + doC.getSelectSQL());
	}


	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////

	public void handleForwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
		handleForwardBackwardCommon(pg, tds, - 1, true);

		return;
 	}


	////////////////////////////////////////////////////////////////////////

	public void handleBackwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");
		handleForwardBackwardCommon(pg, tds, - 1, false);

		return;
	}


	////////////////////////////////////////////////////////////////////////

	private void setDealProgressCriteria(QueryModelBase wqDO, PageCursorInfo tds, PageEntry pg)
	{
		// deal criteria
		String dealCriteria = "(ASSIGNEDTASKSWORKQUEUE.dealID = " + pg.getPageDealId() + ")";
		// visibility criteria (visible or tickle)
		String visibleCriteria = "(ASSIGNEDTASKSWORKQUEUE.visibleFlag = 1 or ASSIGNEDTASKSWORKQUEUE.visibleFlag = 2)";
		String basicCriteria = dealCriteria + " AND " + visibleCriteria;

    // order by ...
		String orderBy = "ASSIGNEDTASKSWORKQUEUE.taskStatusId ASC, ASSIGNEDTASKSWORKQUEUE.endTimeStamp DESC";
		orderBy = setSchemaName(orderBy);

		// work queue
		wqDO.clearUserWhereCriteria();
    wqDO.setStaticWhereCriteriaString(doDealProgressTasksModelImpl.STATIC_WHERE_CRITERIA
                  + " AND " + basicCriteria     // The basicOne
                  + " order by " + orderBy);   // order by ...

		// save criteria and count of records
		tds.setCriteria(basicCriteria + orderBy);
	}


	////////////////////////////////////////////////////////////////////////
	// on input list contains user profile ids - on output these are changed to contact
	// strings (e.g. Lastname, FirstName)

	private void setupAssignedUserList(Vector assignedTo)
	{
		Hashtable assignedUserContactInfoList = new Hashtable();

		int len = assignedTo.size();

		for(int i = 0; i < len;	++ i)
		{
			Integer profileId =(Integer) (assignedTo.elementAt(i));

			// check for information already obtained
			String nameStr =(String) assignedUserContactInfoList.get(profileId);
			if (nameStr == null)
			{
				// not encountered yet - obtain via Contact entity
				nameStr = "Unknown";
				try
				{
					Contact contact = new Contact(getSessionResourceKit());
          contact.findByUserProfileId(profileId.intValue());
					nameStr = contact.getContactFullName();
				}
				catch(Exception e)
				{
					;
				}
				assignedUserContactInfoList.put(profileId , nameStr);
			}
			assignedTo.setElementAt(nameStr, i);
		}

	}


	////////////////////////////////////////////////////////////////////////

    private void populateRowsDisplayed(ViewBean NDPage, PageCursorInfo tds)
    {
            String rowsRpt = "";

            //--Release2.1--//
            //Modified to display Message based on LanguageId
            //--> By Billy 15Nov2002
    if (tds.getTotalRows() > 0)
    {
              //rowsRpt = "Displaying deal progress records " + tds.getFirstDisplayRowNumber() + "-" + tds.getLastDisplayRowNumber() + " of " + tds.getTotalRows();
              rowsRpt = BXResources.getSysMsg("DP_ROWSDISPLAYED",
                                              theSessionState.getLanguageId());
              try {
                rowsRpt = com.basis100.deal.util.Format.printf(rowsRpt,
                    new Parameters(tds.getFirstDisplayRowNumber())
                    .add(tds.getLastDisplayRowNumber())
                    .add(tds.getTotalRows()));
              }
              catch (Exception e) {
                logger.warning(
                    "String Format error @ DealProgressHandler.populateRowsDisplayed !!");
              }
              //rowsRpt = BXStringTokenizer.replace(rowsRpt, "%d1", tds.getFirstDisplayRowNumber()+"");
              //rowsRpt = BXStringTokenizer.replace(rowsRpt, "%d2", tds.getLastDisplayRowNumber()+"");
              //rowsRpt = BXStringTokenizer.replace(rowsRpt, "%d3", tds.getTotalRows()+"");
            }
		NDPage.setDisplayFieldValue("rowsDisplayed", new String(rowsRpt));
    }

    //--CervusPhaseII--start--//
    public void handleCustomActMessageOk(String[] args)
    {
      logger.trace("--T--> DPH@handleCustomActMessageOk: " +((args != null && args [ 0 ] != null) ? args [ 0 ] : "No args"));

      PageEntry pg = theSessionState.getCurrentPage();
      SessionResourceKit srk = getSessionResourceKit();
      ViewBean thePage = getCurrNDPage();

      if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_YES"))
      {
        logger.trace("DPH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
        provideOverrideDealLockActivity(pg, srk, thePage);
        return;
      }

      if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_NO"))
      {
        logger.trace("DPH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

        theSessionState.setActMessage(null);
        theSessionState.overrideDealLock(true);
        handleCancelStandard(false);

        return;
      }
    }
    //--CervusPhaseII--end--//
}

