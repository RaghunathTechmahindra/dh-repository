package mosApp.MosSystem;
/*
 * @(#)doAppraisalProviderModel.java    2006-1-19
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */
import java.math.BigDecimal;
import java.sql.Timestamp;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * doAppraisalProviderModel - is the definition of model for Appraisal
 *
 */
public interface doAppraisalProviderModel extends QueryModel, SelectQueryModel {

    /**
     * fields bound name definition.
     */
    
    public final static String FIELD_DFAPPRAISALDEALID = "dfAppraisalDealId";
    public final static String FIELD_DFAPPRAISALCOPYID = "dfAppraisalCopyId";

    
    public final static String FIELD_DFAPPRAISALPROVIDERID =
        "dfAppraisalProviderId";
    
    public final static String FIELD_DFAPPRAISALPRODUCTID =
        "dfAppraisalProductId";

    
    public final static String FIELD_DFAPPRAISALSPECIALINST =
    	"dfAppraisalSpecialInst";
    public final static String FIELD_DFAPPRAISALREQUESTREFNUM =
        "dfAppraisalRequestRefNum";
    public final static String FIELD_DFAPPRAISALREQUESTSTATUS =
        "dfAppraisalRequestStatus";
    public final static String FIELD_DFAPPRAISALREQUESTSTATUSMSG =
        "dfAppraisalRequestStatusMsg";
    public final static String FIELD_DFAPPRAISALREQUESTSTATUSDATE =
        "dfAppraisalRequestStatusDate";

    public final static String FIELD_DFPROPERTYID = "dfPropertyId";

	public final static String FIELD_DFAOCONTACTFIRSTNAME =
		"dfAOContactFirstName";
    
    public final static String FIELD_DFAOCONTACTLASTNAME = 
		"dfAOContactLastName";
	
	public final static String FIELD_DFAOEMAIL =
		"dfAOEmail";
	
	public final static String FIELD_DFAOWORKPHONEAREACODE =
		"dfAOWorkPhoneAreaCode";
		
	public final static String FIELD_DFAOWORKPHONEFIRSTTHREEDIGITS =
		"dfAOWorkPhoneFirstThreeDigits";

	public final static String FIELD_DFAOWORKPHONELASTFOURDIGITS =
		"dfAOWorkPhoneLastFourDigits";
	
	public final static String FIELD_DFAOWORKPHONENUMEXTENSION =
		"dfAOWorkPhoneNumExtension";
	
	public final static String FIELD_DFAOCELLPHONEAREACODE =
		"dfAOCellPhoneAreaCode";
		
	public final static String FIELD_DFAOCELLPHONEFIRSTTHREEDIGITS =
		"dfAOCellPhoneFirstThreeDigits";
	
	public final static String FIELD_DFAOCELLPHONELASTFOURDIGITS =
		"dfAOCellPhoneLastFourDigits";
	
	public final static String FIELD_DFAOHOMEPHONEAREACODE =
		"dfAOHomePhoneAreaCode";
	
	public final static String FIELD_DFAOHOMEPHONEFIRSTTHREEDIGITS =
		"dfAOHomePhoneFirstThreeDigits";
	
	public final static String FIELD_DFAOHOMEPHONELASTFOURDIGITS =
		"dfAOHomePhoneLastFourDigits";
    
	public final static String FIELD_DFAOPROPERTYOWNERNAME =
		"dfAOPropertyOwnername";
	
    public final static String FIELD_DFAPPRAISALBORROWERID =
        "dfAppraisalBorrowerId";
    
    public final static String FIELD_DFAPPRAISALREQUESTID =
        "dfAppraisalRequestId";
    
    public final static String FIELD_DFAPPRAISALCONTACTID =
        "dfAppraisalContactId";
       
    public final static String FIELD_DFAPPRAISALSTATUSID =
        "dfAppraisalStatusId";
    /**
     * getter and setter definition.
     */
	
    public String getDfAOPropertyOwnerName();
    public void setDfAOPropertyOwnerName(String name);
	

    public BigDecimal getDfAppraisalProviderId();
    public void setDfAppraisalProviderId(BigDecimal id);
    
    public BigDecimal getDfAppraisalProductId();
    public void setDfAppraisalProductId(BigDecimal id);

    public String getDfAppraisalRequestRefNum();
    public void setDfAppraisalRequestRefNum(String num);
    
    public String getDfAppraisalSpecialInst();
    public void setDfAppraisalSpecialInst(String inst);

    public String getDfAppraisalRequestStatus();
    public void setDfAppraisalRequestStatus(String status);

    public String getDfAppraisalRequestStatusMsg();
    public void setDfAppraisalRequestStatusMsg(String msg);

    public Timestamp getDfAppraisalRequestStatusDate();
    public void setDfAppraisalRequestStatusDate(Timestamp date);

    public BigDecimal getDfAppraisalDealId();
    public void setDfAppraisalDealId(BigDecimal id);

    public BigDecimal getDfAppraisalCopyId();
    public void setDfAppraisalCopyId(BigDecimal id);
    
    
    public BigDecimal getDfAppraisalBorrowerId();
    public void setDfAppraisalBorrowerId(BigDecimal id);
    
    public BigDecimal getDfAppraisalRequestId();
    public void setDfAppraisalRequestId(BigDecimal id);
    
    public BigDecimal getDfAppraisalContactId();
    public void setDfAppraisalContactId(BigDecimal id);
    
    public BigDecimal getDfAppraisalStatusId();
    public void setDfAppraisalStatusId(BigDecimal id);
    
	public String getDfAOContactFirstName();
	public void setDfAOContactFristName(String dfAOContactFirstName);
	
	public String getDfAOContactLastName();
	public void setDfAOContactLastName(String dfAOContactLastName);

	public String getDfAOEmail();
	public void setDfAOEmail(String dfAOEmail);

	public String getDfAOWorkPhoneAreaCode();
	public void setDfAOWorkPhoneAreaCode(String tbWorkPhoneAreaCode);
		
	public String getDfAOWorkPhoneFirstThreeDigits();
	public void setDfAOWorkPhoneFirstThreeDigits(String tbWorkPhoneFirstThreeDigits);

	public String getDfAOWorkPhoneLastFourDigits();
	public void setDfAOWorkPhoneLastFourDigits(String tbWorkPhoneLastFourDigits);
	
	public String getDfAOWorkPhoneNumExtension();
	public void setDfAOWorkPhoneNumExtension(String tbWorkPhoneNumExtension);
	
	public String getDfAOCellPhoneAreaCode();
	public void setDfAOCellPhoneAreaCode(String tbCellPhoneAreaCode);
		
	public String getDfAOCellPhoneFirstThreeDigits();
	public void setDfAOCellPhoneFirstThreeDigits(String tbCellPhoneFirstThreeDigits);

	public String getDfAOCellPhoneLastFourDigits();
	public void setDfAOCellPhoneLastFourDigits(String tbCellPhoneLastFourDigits);

	public String getDfAOHomePhoneAreaCode();
	public void setDfAOHomePhoneAreaCode(String dfAOHomePhoneAreaCode);
	
	public String getDfAOHomePhoneFirstThreeDigits();
	public void setDfAOHomePhoneFirstThreeDigits(String dfAOHomePhoneFirstThreeDigits);

	public String getDfAOHomePhoneLastFourDigits();
	public void setDfAOHomePhoneLastFourDigits(String dfAOHomePhoneLastFourDigits);

}
