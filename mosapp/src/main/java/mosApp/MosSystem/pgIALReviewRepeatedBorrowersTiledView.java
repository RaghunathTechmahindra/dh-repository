package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgIALReviewRepeatedBorrowersTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedBorrowersTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doIALBorrowerModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getRepeatedAddAsset().resetChildren();
		getRepeatedLiability().resetChildren();
		getRepeatedIncome().resetChildren();
		getRepeatedCredit().resetChildren();
		getBtAddAsset().setValue(CHILD_BTADDASSET_RESET_VALUE);
		getBtAddLiability().setValue(CHILD_BTADDLIABILITY_RESET_VALUE);
		getBtAddIncome().setValue(CHILD_BTADDINCOME_RESET_VALUE);
		getBtAddCredit().setValue(CHILD_BTADDCREDIT_RESET_VALUE);
		getStBorrowerName().setValue(CHILD_STBORROWERNAME_RESET_VALUE);
		getStPrimaryFlag().setValue(CHILD_STPRIMARYFLAG_RESET_VALUE);
		getStBorrowerType().setValue(CHILD_STBORROWERTYPE_RESET_VALUE);
		getStBorrowerTDS().setValue(CHILD_STBORROWERTDS_RESET_VALUE);
		getStBorrowerGDS().setValue(CHILD_STBORROWERGDS_RESET_VALUE);
		getStNetworth().setValue(CHILD_STNETWORTH_RESET_VALUE);
		getHdBorrowerid().setValue(CHILD_HDBORROWERID_RESET_VALUE);
		getRepeatedEmployer().resetChildren();
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_REPEATEDADDASSET,pgIALReviewRepeatedAddAssetTiledView.class);
		registerChild(CHILD_REPEATEDLIABILITY,pgIALReviewRepeatedLiabilityTiledView.class);
		registerChild(CHILD_REPEATEDINCOME,pgIALReviewRepeatedIncomeTiledView.class);
		registerChild(CHILD_REPEATEDCREDIT,pgIALReviewRepeatedCreditTiledView.class);
		registerChild(CHILD_BTADDASSET,Button.class);
		registerChild(CHILD_BTADDLIABILITY,Button.class);
		registerChild(CHILD_BTADDINCOME,Button.class);
		registerChild(CHILD_BTADDCREDIT,Button.class);
		registerChild(CHILD_STBORROWERNAME,StaticTextField.class);
		registerChild(CHILD_STPRIMARYFLAG,StaticTextField.class);
		registerChild(CHILD_STBORROWERTYPE,StaticTextField.class);
		registerChild(CHILD_STBORROWERTDS,TextField.class);
		registerChild(CHILD_STBORROWERGDS,TextField.class);
		registerChild(CHILD_STNETWORTH,TextField.class);
		registerChild(CHILD_HDBORROWERID,HiddenField.class);
		registerChild(CHILD_REPEATEDEMPLOYER,pgIALReviewRepeatedEmployerTiledView.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoIALBorrowerModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;


		// The following code block was migrated from the RepeatedBorrowers_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		doIALBorrower dobrorsmr =(doIALBorrower) getModel(doIALBorrowerModel.class);

		if (dobrorsmr != null)
		{
			int rowindx = event.getRowIndex();
			if (rowindx == 0)
			{
				//CSpHtml.sendMessage("@RepeatedBorrowers_onBeforeRowDisplayEvent");
				sumofrowsForIncome = - 1;
				sumofrowsForAsset = - 1;
				sumofrowsForCredit = - 1;
				sumofrowsForLiliability = - 1;
			}
			CSpDBResultTable resultbl = dobrorsmr.getLastResults().getResultTable();
			String borrowerid = new String(resultbl.getValue(rowindx, 0).toString());

			//CSpHtml.sendMessage("Borrower Id "+ borrowerid.toString());
			//CSpHtml.sendMessage("Event aource "+event.getSource());
			String name = handler.makeBorrowerName(resultbl.getValue(rowindx, 4).toString().toString(), resultbl.getValue(rowindx, 2).toString().toString(), resultbl.getValue(rowindx, 3).toString().toString());
			CSpRepeated rep =(CSpRepeated) getCommonRepeated("RepeatedBorrowers");
			rep.setDisplayFieldValue("stBorrowerName", new String(name));
			doBorrowerEntryAsset doborasset =(doBorrowerEntryAsset) getModel(doBorrowerEntryAssetModel.class);
			doborasset.clearUserWhereCriteria();
			doborasset.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
			doborasset.execute();
			doBorrowerEntryLiability doborliab =(doBorrowerEntryLiability) getModel(doBorrowerEntryLiabilityModel.class);
			doborliab.clearUserWhereCriteria();
			doborliab.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
			doborliab.execute();
			doBorrowerEntryIncome dobrorinc =(doBorrowerEntryIncome) getModel(doBorrowerEntryIncomeModel.class);
			dobrorinc.clearUserWhereCriteria();
			dobrorinc.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
			dobrorinc.execute();
			doBorrowerEntryCredit dobrorcredit =(doBorrowerEntryCredit) getModel(doBorrowerEntryCreditModel.class);
			dobrorcredit.clearUserWhereCriteria();
			dobrorcredit.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
			dobrorcredit.execute();
			doIALEmploymentHistory dobroremp =(doIALEmploymentHistory) getModel(doIALEmploymentHistoryModel.class);
			dobroremp.clearUserWhereCriteria();
			dobroremp.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
			dobroremp.execute();
		}

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_REPEATEDADDASSET))
		{
			pgIALReviewRepeatedAddAssetTiledView child = new pgIALReviewRepeatedAddAssetTiledView(this,
				CHILD_REPEATEDADDASSET);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATEDLIABILITY))
		{
			pgIALReviewRepeatedLiabilityTiledView child = new pgIALReviewRepeatedLiabilityTiledView(this,
				CHILD_REPEATEDLIABILITY);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATEDINCOME))
		{
			pgIALReviewRepeatedIncomeTiledView child = new pgIALReviewRepeatedIncomeTiledView(this,
				CHILD_REPEATEDINCOME);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATEDCREDIT))
		{
			pgIALReviewRepeatedCreditTiledView child = new pgIALReviewRepeatedCreditTiledView(this,
				CHILD_REPEATEDCREDIT);
			return child;
		}
		else
		if (name.equals(CHILD_BTADDASSET))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTADDASSET,
				CHILD_BTADDASSET,
				CHILD_BTADDASSET_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTADDLIABILITY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTADDLIABILITY,
				CHILD_BTADDLIABILITY,
				CHILD_BTADDLIABILITY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTADDINCOME))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTADDINCOME,
				CHILD_BTADDINCOME,
				CHILD_BTADDINCOME_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTADDCREDIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTADDCREDIT,
				CHILD_BTADDCREDIT,
				CHILD_BTADDCREDIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STBORROWERNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBORROWERNAME,
				CHILD_STBORROWERNAME,
				CHILD_STBORROWERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPRIMARYFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getdoIALBorrowerModel(),
				CHILD_STPRIMARYFLAG,
				doIALBorrowerModel.FIELD_DFBORROWERPRIMARYFLAG,
				CHILD_STPRIMARYFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORROWERTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoIALBorrowerModel(),
				CHILD_STBORROWERTYPE,
				doIALBorrowerModel.FIELD_DFBORROWERTYPE,
				CHILD_STBORROWERTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORROWERTDS))
		{
			TextField child = new TextField(this,
				getdoIALBorrowerModel(),
				CHILD_STBORROWERTDS,
				doIALBorrowerModel.FIELD_DFBORROWERTDS,
				CHILD_STBORROWERTDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBORROWERGDS))
		{
			TextField child = new TextField(this,
				getdoIALBorrowerModel(),
				CHILD_STBORROWERGDS,
				doIALBorrowerModel.FIELD_DFBORROWERGDS,
				CHILD_STBORROWERGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STNETWORTH))
		{
			TextField child = new TextField(this,
				getdoIALBorrowerModel(),
				CHILD_STNETWORTH,
				doIALBorrowerModel.FIELD_DFBORROWERNETWORTH,
				CHILD_STNETWORTH_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDBORROWERID))
		{
			HiddenField child = new HiddenField(this,
				getdoIALBorrowerModel(),
				CHILD_HDBORROWERID,
				doIALBorrowerModel.FIELD_DFBORROWERID,
				CHILD_HDBORROWERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATEDEMPLOYER))
		{
			pgIALReviewRepeatedEmployerTiledView child = new pgIALReviewRepeatedEmployerTiledView(this,
				CHILD_REPEATEDEMPLOYER);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedAddAssetTiledView getRepeatedAddAsset()
	{
		return (pgIALReviewRepeatedAddAssetTiledView)getChild(CHILD_REPEATEDADDASSET);
	}


	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedLiabilityTiledView getRepeatedLiability()
	{
		return (pgIALReviewRepeatedLiabilityTiledView)getChild(CHILD_REPEATEDLIABILITY);
	}


	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedIncomeTiledView getRepeatedIncome()
	{
		return (pgIALReviewRepeatedIncomeTiledView)getChild(CHILD_REPEATEDINCOME);
	}


	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedCreditTiledView getRepeatedCredit()
	{
		return (pgIALReviewRepeatedCreditTiledView)getChild(CHILD_REPEATEDCREDIT);
	}


	/**
	 *
	 *
	 */
	public void handleBtAddAssetRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btAddAsset_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		IALReviewHandler handler =(IALReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleAddAsset(handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtAddAsset()
	{
		return (Button)getChild(CHILD_BTADDASSET);
	}


	/**
	 *
	 *
	 */
	public void handleBtAddLiabilityRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btAddLiability_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		IALReviewHandler handler =(IALReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleAddLiability(handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtAddLiability()
	{
		return (Button)getChild(CHILD_BTADDLIABILITY);
	}


	/**
	 *
	 *
	 */
	public void handleBtAddIncomeRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btAddIncome_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		IALReviewHandler handler =(IALReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleAddIncome(handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtAddIncome()
	{
		return (Button)getChild(CHILD_BTADDINCOME);
	}


	/**
	 *
	 *
	 */
	public void handleBtAddCreditRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btAddCredit_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		IALReviewHandler handler =(IALReviewHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleAddCredit(handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		return PROCEED;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtAddCredit()
	{
		return (Button)getChild(CHILD_BTADDCREDIT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerName()
	{
		return (StaticTextField)getChild(CHILD_STBORROWERNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrimaryFlag()
	{
		return (StaticTextField)getChild(CHILD_STPRIMARYFLAG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBorrowerType()
	{
		return (StaticTextField)getChild(CHILD_STBORROWERTYPE);
	}


	/**
	 *
	 *
	 */
	public TextField getStBorrowerTDS()
	{
		return (TextField)getChild(CHILD_STBORROWERTDS);
	}


	/**
	 *
	 *
	 */
	public TextField getStBorrowerGDS()
	{
		return (TextField)getChild(CHILD_STBORROWERGDS);
	}


	/**
	 *
	 *
	 */
	public TextField getStNetworth()
	{
		return (TextField)getChild(CHILD_STNETWORTH);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdBorrowerid()
	{
		return (HiddenField)getChild(CHILD_HDBORROWERID);
	}


	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedEmployerTiledView getRepeatedEmployer()
	{
		return (pgIALReviewRepeatedEmployerTiledView)getChild(CHILD_REPEATEDEMPLOYER);
	}


	/**
	 *
	 *
	 */
	public doIALBorrowerModel getdoIALBorrowerModel()
	{
		if (doIALBorrower == null)
			doIALBorrower = (doIALBorrowerModel) getModel(doIALBorrowerModel.class);
		return doIALBorrower;
	}


	/**
	 *
	 *
	 */
	public void setdoIALBorrowerModel(doIALBorrowerModel model)
	{
			doIALBorrower = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_REPEATEDADDASSET="RepeatedAddAsset";
	public static final String CHILD_REPEATEDLIABILITY="RepeatedLiability";
	public static final String CHILD_REPEATEDINCOME="RepeatedIncome";
	public static final String CHILD_REPEATEDCREDIT="RepeatedCredit";
	public static final String CHILD_BTADDASSET="btAddAsset";
	public static final String CHILD_BTADDASSET_RESET_VALUE=" ";
	public static final String CHILD_BTADDLIABILITY="btAddLiability";
	public static final String CHILD_BTADDLIABILITY_RESET_VALUE="Add Additional Liability";
	public static final String CHILD_BTADDINCOME="btAddIncome";
	public static final String CHILD_BTADDINCOME_RESET_VALUE="Add Additional Income";
	public static final String CHILD_BTADDCREDIT="btAddCredit";
	public static final String CHILD_BTADDCREDIT_RESET_VALUE="Add Additional Credit";
	public static final String CHILD_STBORROWERNAME="stBorrowerName";
	public static final String CHILD_STBORROWERNAME_RESET_VALUE="";
	public static final String CHILD_STPRIMARYFLAG="stPrimaryFlag";
	public static final String CHILD_STPRIMARYFLAG_RESET_VALUE="";
	public static final String CHILD_STBORROWERTYPE="stBorrowerType";
	public static final String CHILD_STBORROWERTYPE_RESET_VALUE="";
	public static final String CHILD_STBORROWERTDS="stBorrowerTDS";
	public static final String CHILD_STBORROWERTDS_RESET_VALUE="";
	public static final String CHILD_STBORROWERGDS="stBorrowerGDS";
	public static final String CHILD_STBORROWERGDS_RESET_VALUE="";
	public static final String CHILD_STNETWORTH="stNetworth";
	public static final String CHILD_STNETWORTH_RESET_VALUE="";
	public static final String CHILD_HDBORROWERID="hdBorrowerid";
	public static final String CHILD_HDBORROWERID_RESET_VALUE="";
	public static final String CHILD_REPEATEDEMPLOYER="RepeatedEmployer";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doIALBorrowerModel doIALBorrower=null;

}

