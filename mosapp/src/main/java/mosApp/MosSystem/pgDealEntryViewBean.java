package mosApp.MosSystem;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.log.SysLogger;

import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;

import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;
import com.iplanet.jato.view.html.CheckBox;

import mosApp.SQLConnectionManagerImpl;

import java.io.IOException;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;


/**
 * @author @
 * @version 1.6 <br>
 * Date: 07/28/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change: added 2 constants
 * - CHILD_CBAFFILIATIONPROGRAM,CHILD_CBAFFILIATIONPROGRAM_RESET_VALUE
 * - modified registerChildren(),resetChildren() ,createChild(String)
 * - Add inner class CbUWAffiliationProgramOptionList
 */
/**
 * @version 1.7 <br>
 * Date: 06/09/2008 <br>
 * Author: <To be clarified> <br>
 * Change: added 2 fields in Refinance section
 * - CHILD_TXREFIADDITIONALINFORMATION,CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE,
 * CHILD_TXREFEXISTINGMTGNUMBER,CHILD_TXREFEXISTINGMTGNUMBER_RESET_VALUE
 * - modified registerChildren(),resetChildren() ,createChild(String)
 *
 *
 *	@Version 1.4 <br>
 *	Date: 06/09/2008
 *	Author: MCM Impl Team <br>
 *	Change Log:  <br>
 *	XS_2.7 -- added 2 fields in Refinance section<br>
 *		- CHILD_TXREFIADDITIONALINFORMATION,CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE,
 * 		  CHILD_TXREFEXISTINGMTGNUMBER,CHILD_TXREFEXISTINGMTGNUMBER_RESET_VALUE
 *		- modified registerChildren(),resetChildren() ,createChild(String)	
 *
 * @version 1.8 06-JUN-2008 XS_2.13 Included pgQualifyingDetailsPageletView
 * - Added CHILD_STQUALIFYINGDETAILSPAGELET
 * - modified registerChildren(),createChild(String)
 *
 * @version 1.9 22-JULY-2008 XS_2.44 added this.handler as a constractor parameter 
 *              for pgComponentInfoPageletView parameter QualifyingDetailsPageletView
 *              
 * @version 1.10 Aug 08, 2008 XS_2.1, XS_2.2_, XS_2.4 added field : stDefaultRepaymentTypeId 
 */

////Original (iMT) inheritance.
public class pgDealEntryViewBean extends ExpressViewBeanBase implements Mc, Sc

////Highly discussable implementation with the XXXXHandler extension of the
////ViewBeanBase JATO class. The sequence diagrams must be verified. On the other hand,
////This implementation allows to use the basic JATO framework elements such as
////RequestContext() class. Theoretically, it could allow to override the JATO/iMT
////auto-migration. Temporarily these fragments are commented out now for the compability
////with the stand alone MWHC<--PHC<--XXXHandlerName Utility hierarchy.
////getModel(ClassName.class), etc., could be also rewritten in the Utility hierarchy
////using this inheritance.
////public class pgDealEntryViewBean extends DealEntryHandler
////implements ViewBean
{
    ////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////////
    public static final String PAGE_NAME = "pgDealEntry";

    //// It is a variable now (see explanation in the getDisplayURL() method above.
    ////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDealEntry.jsp";
    public static Map FIELD_DESCRIPTORS;
    public static final String CHILD_STPAGELABEL = "stPageLabel";
    public static final String CHILD_STPAGELABEL_RESET_VALUE = "";
    public static final String CHILD_STCOMPANYNAME = "stCompanyName";
    public static final String CHILD_STCOMPANYNAME_RESET_VALUE = "";
    public static final String CHILD_STTODAYDATE = "stTodayDate";
    public static final String CHILD_STTODAYDATE_RESET_VALUE = "";
    public static final String CHILD_STUSERNAMETITLE = "stUserNameTitle";
    public static final String CHILD_STUSERNAMETITLE_RESET_VALUE = "";
    public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";
    public static final String CHILD_DETECTALERTTASKS_RESET_VALUE = "";
    public static final String CHILD_SESSIONUSERID = "sessionUserId";
    public static final String CHILD_SESSIONUSERID_RESET_VALUE = "";
    public static final String CHILD_TBDEALID = "tbDealId";
    public static final String CHILD_TBDEALID_RESET_VALUE = "";
    public static final String CHILD_CBPAGENAMES = "cbPageNames";
    public static final String CHILD_CBPAGENAMES_RESET_VALUE = "";

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    private CbPageNamesOptionList cbPageNamesOptions =
        new CbPageNamesOptionList();
    public static final String CHILD_BTPROCEED = "btProceed";
    public static final String CHILD_BTPROCEED_RESET_VALUE = "Proceed";
    public static final String CHILD_HREF1 = "Href1";
    public static final String CHILD_HREF1_RESET_VALUE = "";
    public static final String CHILD_HREF2 = "Href2";
    public static final String CHILD_HREF2_RESET_VALUE = "";
    public static final String CHILD_HREF3 = "Href3";
    public static final String CHILD_HREF3_RESET_VALUE = "";
    public static final String CHILD_HREF4 = "Href4";
    public static final String CHILD_HREF4_RESET_VALUE = "";
    public static final String CHILD_HREF5 = "Href5";
    public static final String CHILD_HREF5_RESET_VALUE = "";
    public static final String CHILD_HREF6 = "Href6";
    public static final String CHILD_HREF6_RESET_VALUE = "";
    public static final String CHILD_HREF7 = "Href7";
    public static final String CHILD_HREF7_RESET_VALUE = "";
    public static final String CHILD_HREF8 = "Href8";
    public static final String CHILD_HREF8_RESET_VALUE = "";
    public static final String CHILD_HREF9 = "Href9";
    public static final String CHILD_HREF9_RESET_VALUE = "";
    public static final String CHILD_HREF10 = "Href10";
    public static final String CHILD_HREF10_RESET_VALUE = "";
    public static final String CHILD_CHANGEPASSWORDHREF = "changePasswordHref";
    public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE = "";
    public static final String CHILD_BTTOOLHISTORY = "btToolHistory";
    public static final String CHILD_BTTOOLHISTORY_RESET_VALUE = " ";
    public static final String CHILD_BTTOONOTES = "btTooNotes";
    public static final String CHILD_BTTOONOTES_RESET_VALUE = " ";
    public static final String CHILD_BTTOOLSEARCH = "btToolSearch";
    public static final String CHILD_BTTOOLSEARCH_RESET_VALUE = " ";
    public static final String CHILD_BTTOOLLOG = "btToolLog";
    public static final String CHILD_BTTOOLLOG_RESET_VALUE = " ";
    public static final String CHILD_BTWORKQUEUELINK = "btWorkQueueLink";
    public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE = " ";
    public static final String CHILD_BTPREVTASKPAGE = "btPrevTaskPage";
    public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE = " ";
    public static final String CHILD_BTNEXTTASKPAGE = "btNextTaskPage";
    public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE = " ";
    public static final String CHILD_STERRORFLAG = "stErrorFlag";
    public static final String CHILD_STERRORFLAG_RESET_VALUE = "N";
    public static final String CHILD_STPREVTASKPAGELABEL = "stPrevTaskPageLabel";
    public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE = "";
    public static final String CHILD_STNEXTTASKPAGELABEL = "stNextTaskPageLabel";
    public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE = "";
    public static final String CHILD_STTASKNAME = "stTaskName";
    public static final String CHILD_STTASKNAME_RESET_VALUE = "";
    public static final String CHILD_HDDEALID = "hdDealId";
    public static final String CHILD_HDDEALID_RESET_VALUE = "";
    public static final String CHILD_HDDEALCOPYID = "hdDealCopyId";
    public static final String CHILD_HDDEALCOPYID_RESET_VALUE = "";
    public static final String CHILD_HDDEALINSTITUTIONID = "hdDealInstitutionId";
    public static final String CHILD_HDDEALINSTITUTIONID_RESET_VALUE = "";
    public static final String CHILD_STDEALNO = "stDealNo";
    public static final String CHILD_STDEALNO_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDGDS = "stCombinedGDS";
    public static final String CHILD_STCOMBINEDGDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINED3YRGDS = "stCombined3YrGDS";
    public static final String CHILD_STCOMBINED3YRGDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDBORROWERGDS =
        "stCombinedBorrowerGDS";
    public static final String CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDTDS = "stCombinedTDS";
    public static final String CHILD_STCOMBINEDTDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINED3YRTDS = "stCombined3YrTDS";
    public static final String CHILD_STCOMBINED3YRTDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDBORROWERTDS =
        "stCombinedBorrowerTDS";
    public static final String CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE = "";
    public static final String CHILD_REPEATGDSTDSDETAILS = "RepeatGDSTDSDetails";
    public static final String CHILD_REPEATAPPLICANT = "RepeatApplicant";
    public static final String CHILD_BTADDAPPLICANT = "btAddApplicant";
    public static final String CHILD_BTADDAPPLICANT_RESET_VALUE = " ";
    public static final String CHILD_BTDUPAPPLICANT = "btDupApplicant";
    public static final String CHILD_BTDUPAPPLICANT_RESET_VALUE = " ";
/*    public static final String CHILD_BTCHANGESOURCE = "btChangeSource";
    public static final String CHILD_BTCHANGESOURCE_RESET_VALUE = " ";
    public static final String CHILD_STSOURCEFIRMSOURCEPART =
        "stSourceFirmSourcePart";
    public static final String CHILD_STSOURCEFIRMSOURCEPART_RESET_VALUE = "";
    public static final String CHILD_STSOURCEFIRSTNAME = "stSourceFirstName";
    public static final String CHILD_STSOURCEFIRSTNAME_RESET_VALUE = "";
    public static final String CHILD_STSOURCELASTNAME = "stSourceLastName";
    public static final String CHILD_STSOURCELASTNAME_RESET_VALUE = "";
    public static final String CHILD_STSOURCEADDRESS = "stSourceAddress";
    public static final String CHILD_STSOURCEADDRESS_RESET_VALUE = "";
    public static final String CHILD_STSOURCEPHONENO = "stSourcePhoneNo";
    public static final String CHILD_STSOURCEPHONENO_RESET_VALUE = "";
    public static final String CHILD_STSOURCEFAXNO = "stSourceFaxNo";
    public static final String CHILD_STSOURCEFAXNO_RESET_VALUE = ""; */
  //4.4 Submission Agent
    public static final String CHILD_REPEATEDSOB = "RepeatedSOB";
    public static final String CHILD_REPEATEDSOB_RESET_VALUE = "";

    public static final String CHILD_TXREFERENCESOURCEAPP =
        "txReferenceSourceApp";
    public static final String CHILD_TXREFERENCESOURCEAPP_RESET_VALUE = "";
    public static final String CHILD_REPEATPROPERTYINFORMATION =
        "RepeatPropertyInformation";
    public static final String CHILD_BTADDPROPERTY = "btAddProperty";
    public static final String CHILD_BTADDPROPERTY_RESET_VALUE = " ";
    public static final String CHILD_CBDEALPURPOSE = "cbDealPurpose";
    public static final String CHILD_CBDEALPURPOSE_RESET_VALUE = "";
    public static final String CHILD_CBDEALTYPE = "cbDealType";
    public static final String CHILD_CBDEALTYPE_RESET_VALUE = "";
    public static final String CHILD_CBLOB = "cbLOB";
    public static final String CHILD_CBLOB_RESET_VALUE = "";
    public static final String CHILD_CBLENDER = "cbLender";
    public static final String CHILD_CBLENDER_RESET_VALUE = "";
    private OptionList cbLenderOptions =
        new OptionList(new String[] { " " }, new String[] { " " });
    public static final String CHILD_CBPRODUCT = "cbProduct";
    public static final String CHILD_CBPRODUCT_RESET_VALUE = "";
    private OptionList cbProductOptions =
        new OptionList(new String[] { " " }, new String[] { " " });
    public static final String CHILD_CBRATELOCKED = "cbRateLocked";
    public static final String CHILD_CBRATELOCKED_RESET_VALUE = "N";
    public static final String CHILD_CBCHARGETERM = "cbChargeTerm";
    public static final String CHILD_CBCHARGETERM_RESET_VALUE = "";
    public static final String CHILD_CBPOSTEDINTERESTRATE =
        "cbPostedInterestRate";
    public static final String CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE = "";
    private OptionList cbPostedInterestRateOptions =
        new OptionList(new String[] { " " }, new String[] { " " });
    public static final String CHILD_TXDISCOUNT = "txDiscount";
    public static final String CHILD_TXDISCOUNT_RESET_VALUE = "";
    public static final String CHILD_TXPREMIUM = "txPremium";
    public static final String CHILD_TXPREMIUM_RESET_VALUE = "";
    public static final String CHILD_TXBUYDOWNRATE = "txBuydownRate";
    public static final String CHILD_TXBUYDOWNRATE_RESET_VALUE = "";
    public static final String CHILD_STNETRATE = "stNetRate";
    public static final String CHILD_STNETRATE_RESET_VALUE = "";
    public static final String CHILD_STTOTALESCROWPAYMENT =
        "stTotalEscrowPayment";
    public static final String CHILD_STTOTALESCROWPAYMENT_RESET_VALUE = "";
    public static final String CHILD_TXREQUESTEDLOANAMOUNT =
        "txRequestedLoanAmount";
    public static final String CHILD_TXREQUESTEDLOANAMOUNT_RESET_VALUE = "";
    public static final String CHILD_CBPAYEMNTFREQUENCYTERM =
        "cbPayemntFrequencyTerm";
    public static final String CHILD_CBPAYEMNTFREQUENCYTERM_RESET_VALUE = "";
    public static final String CHILD_TXAMORTIZATIONYEARS = "txAmortizationYears";
    public static final String CHILD_TXAMORTIZATIONYEARS_RESET_VALUE = "";
    public static final String CHILD_TXAMORTIZATIONMONTHS =
        "txAmortizationMonths";
    public static final String CHILD_TXAMORTIZATIONMONTHS_RESET_VALUE = "";
    public static final String CHILD_STEFFECTIVEAMORTIZATIONYEARS =
        "stEffectiveAmortizationYears";
    public static final String CHILD_STEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE =
        "";
    public static final String CHILD_STEFFECTIVEAMORTIZATIONMONTHS =
        "stEffectiveAmortizationMonths";
    public static final String CHILD_STEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE =
        "";
    public static final String CHILD_TXADDITIONALPRINCIPALPAYMENT =
        "txAdditionalPrincipalPayment";
    public static final String CHILD_TXADDITIONALPRINCIPALPAYMENT_RESET_VALUE =
        "";
    public static final String CHILD_TXPAYTERMYEARS = "txPayTermYears";
    public static final String CHILD_TXPAYTERMYEARS_RESET_VALUE = "25";
    public static final String CHILD_TXPAYTERMMONTHS = "txPayTermMonths";
    public static final String CHILD_TXPAYTERMMONTHS_RESET_VALUE = "";
    public static final String CHILD_STPIPAYMENT = "stPIPayment";
    public static final String CHILD_STPIPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STTOTALPAYMENT = "stTotalPayment";
    public static final String CHILD_STTOTALPAYMENT_RESET_VALUE = "";
    public static final String CHILD_CBREPAYMENTTYPE = "cbRepaymentType";
    public static final String CHILD_CBREPAYMENTTYPE_RESET_VALUE = "";
    /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
    public static final String CHILD_STDEFAULTREPAYMENTTYPEID = "stDefaultRepaymentTypeId";
    public static final String CHILD_STDEFAULTREPAYMENTTYPEID_RESET_VALUE = "";
    /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
    public static final String CHILD_CBESTCLOSINGDATEMONTHS =
        "cbEstClosingDateMonths";
    public static final String CHILD_CBESTCLOSINGDATEMONTHS_RESET_VALUE = "";
    private OptionList cbEstClosingDateMonthsOptions =
        new OptionList(new String[] {  }, new String[] {  });
    public static final String CHILD_TXESTCLOSINGDATEDAY = "txEstClosingDateDay";
    public static final String CHILD_TXESTCLOSINGDATEDAY_RESET_VALUE = "";
    public static final String CHILD_TXESTCLOSINGDATEYEAR =
        "txEstClosingDateYear";
    public static final String CHILD_TXESTCLOSINGDATEYEAR_RESET_VALUE = "";
    public static final String CHILD_CBFIRSTPAYDATEMONTH = "cbFirstPayDateMonth";
    public static final String CHILD_CBFIRSTPAYDATEMONTH_RESET_VALUE = "";
    private OptionList cbFirstPayDateMonthOptions =
        new OptionList(new String[] {  }, new String[] {  });
    public static final String CHILD_TXFIRSTPAYDATEDAY = "txFirstPayDateDay";
    public static final String CHILD_TXFIRSTPAYDATEDAY_RESET_VALUE = "";
    public static final String CHILD_TXFIRSTPAYDATEYEAR = "txFirstPayDateYear";
    public static final String CHILD_TXFIRSTPAYDATEYEAR_RESET_VALUE = "";
    public static final String CHILD_STMATURITYDATE = "stMaturityDate";
    public static final String CHILD_STMATURITYDATE_RESET_VALUE = "";
    public static final String CHILD_TXPREAPPESTPURCHASEPRICE =
        "txPreAppEstPurchasePrice";
    public static final String CHILD_TXPREAPPESTPURCHASEPRICE_RESET_VALUE = "";
    public static final String CHILD_CBSECONDMORTGAGEEXIST =
        "cbSecondMortgageExist";
    public static final String CHILD_CBSECONDMORTGAGEEXIST_RESET_VALUE = "N";
    public static final String CHILD_CBPREPAYMENTPENANLTY =
        "cbPrePaymentPenanlty";
    public static final String CHILD_CBPREPAYMENTPENANLTY_RESET_VALUE = "";
    public static final String CHILD_CBPRIVILAGEPAYMENTOPTION =
        "cbPrivilagePaymentOption";
    public static final String CHILD_CBPRIVILAGEPAYMENTOPTION_RESET_VALUE = "";
    public static final String CHILD_STINCLUDEFINANCINGPROGRAMSTART =
        "stIncludeFinancingProgramStart";
    public static final String CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE =
        "";
    public static final String CHILD_CBFINANCINGPROGRAM = "cbFinancingProgram";
    public static final String CHILD_CBFINANCINGPROGRAM_RESET_VALUE = "";
    public static final String CHILD_STINCLUDEFINANCINGPROGRAMEND =
        "stIncludeFinancingProgramEnd";
    public static final String CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE =
        "";
    public static final String CHILD_CBSPECIALFEATURE = "cbSpecialFeature";
    public static final String CHILD_CBSPECIALFEATURE_RESET_VALUE = "";
    public static final String CHILD_CBCOMMEXPECTEDDATEMONTH =
        "cbCommExpectedDateMonth";
    public static final String CHILD_CBCOMMEXPECTEDDATEMONTH_RESET_VALUE = "";
    private OptionList cbCommExpectedDateMonthOptions =
        new OptionList(new String[] {  }, new String[] {  });
    public static final String CHILD_TXCOMMEXPECTEDDATEDAY =
        "txCommExpectedDateDay";
    public static final String CHILD_TXCOMMEXPECTEDDATEDAY_RESET_VALUE = "";
    public static final String CHILD_TXCOMMEXPECTEDDATEYEAR =
        "txCommExpectedDateYear";
    public static final String CHILD_TXCOMMEXPECTEDDATEYEAR_RESET_VALUE = "";
    public static final String CHILD_STCOMMITMENTPERIOD = "stCommitmentPeriod";
    public static final String CHILD_STCOMMITMENTPERIOD_RESET_VALUE = "";
    public static final String CHILD_STCOMMEXPDATE = "stCommExpDate";
    public static final String CHILD_STCOMMEXPDATE_RESET_VALUE = "";
    public static final String CHILD_STCOMMACCEPTANCEDATE =
        "stCommAcceptanceDate";
    public static final String CHILD_STCOMMACCEPTANCEDATE_RESET_VALUE = "";
    public static final String CHILD_STBRIDGELOANAMOUNT = "stBridgeLoanAmount";
    public static final String CHILD_STBRIDGELOANAMOUNT_RESET_VALUE = "";
    public static final String CHILD_TXADVANCEHOLD = "txAdvanceHold";
    public static final String CHILD_TXADVANCEHOLD_RESET_VALUE = "";
    public static final String CHILD_BTREVIEWBRIDGE = "btReviewBridge";
    public static final String CHILD_BTREVIEWBRIDGE_RESET_VALUE = " ";
    public static final String CHILD_REPEATEDDOWNPAYMENT = "RepeatedDownPayment";
    public static final String CHILD_BTADDDOWNPAYMENT = "btAddDownPayment";
    public static final String CHILD_BTADDDOWNPAYMENT_RESET_VALUE = " ";
    public static final String CHILD_STDOWNPAYMENTREQUIRED =
        "stDownPaymentRequired";
    public static final String CHILD_STDOWNPAYMENTREQUIRED_RESET_VALUE = "";
    public static final String CHILD_REPEATEDESCROWDETAILS =
        "RepeatedEscrowDetails";
    public static final String CHILD_BTADDESCROW = "btAddEscrow";
    public static final String CHILD_BTADDESCROW_RESET_VALUE = " ";
    public static final String CHILD_CBBRANCH = "cbBranch";
    public static final String CHILD_CBBRANCH_RESET_VALUE = "";
    public static final String CHILD_TXREFEXISITINGMTG = "txRefExisitingMTG";
    public static final String CHILD_TXREFEXISITINGMTG_RESET_VALUE = "";
    public static final String CHILD_CBCROSSSELL = "cbCrossSell";
    public static final String CHILD_CBCROSSSELL_RESET_VALUE = "";
    public static final String CHILD_CBTAXPAYOR = "cbTaxPayor";
    public static final String CHILD_CBTAXPAYOR_RESET_VALUE = "";
    public static final String CHILD_BTPARTYSUMMARY = "btPartySummary";
    public static final String CHILD_BTPARTYSUMMARY_RESET_VALUE = " ";
    public static final String CHILD_STREFERENCETYPE = "stReferenceType";
    public static final String CHILD_STREFERENCETYPE_RESET_VALUE = "";
    public static final String CHILD_STREFERENCEDEALNO = "stReferenceDealNo";
    public static final String CHILD_STREFERENCEDEALNO_RESET_VALUE = "";
    public static final String CHILD_CBMIINDICATOR = "cbMIIndicator";
    public static final String CHILD_CBMIINDICATOR_RESET_VALUE = "";
    public static final String CHILD_CBMITYPE = "cbMIType";
    public static final String CHILD_CBMITYPE_RESET_VALUE = "";
    public static final String CHILD_CBMIINSURER = "cbMIInsurer";
    public static final String CHILD_CBMIINSURER_RESET_VALUE = "";
    public static final String CHILD_CBMIPAYOR = "cbMIPayor";
    public static final String CHILD_CBMIPAYOR_RESET_VALUE = "";
    public static final String CHILD_TXMIPREMIUM = "txMIPremium";
    public static final String CHILD_TXMIPREMIUM_RESET_VALUE = "";
    public static final String CHILD_RBMIUPFRONT = "rbMIUpfront";
    public static final String CHILD_RBMIUPFRONT_RESET_VALUE = "N";
    public static final String CHILD_RBMIRUINTERVENTION = "rbMIRUIntervention";
    public static final String CHILD_RBMIRUINTERVENTION_RESET_VALUE = "N";
    public static final String CHILD_STMIPREQCERTNUM = "stMIPreQCertNum";
    public static final String CHILD_STMIPREQCERTNUM_RESET_VALUE = "";
    public static final String CHILD_BTDEALSUBMIT = "btDealSubmit";
    public static final String CHILD_BTDEALSUBMIT_RESET_VALUE = " ";
    public static final String CHILD_BTDEALCANCEL = "btDealCancel";
    public static final String CHILD_BTDEALCANCEL_RESET_VALUE = " ";
    public static final String CHILD_STPMGENERATE = "stPmGenerate";
    public static final String CHILD_STPMGENERATE_RESET_VALUE = "";
    public static final String CHILD_STPMHASTITLE = "stPmHasTitle";
    public static final String CHILD_STPMHASTITLE_RESET_VALUE = "";
    public static final String CHILD_STPMHASINFO = "stPmHasInfo";
    public static final String CHILD_STPMHASINFO_RESET_VALUE = "";
    public static final String CHILD_STPMHASTABLE = "stPmHasTable";
    public static final String CHILD_STPMHASTABLE_RESET_VALUE = "";
    public static final String CHILD_STPMHASOK = "stPmHasOk";
    public static final String CHILD_STPMHASOK_RESET_VALUE = "";
    public static final String CHILD_STPMTITLE = "stPmTitle";
    public static final String CHILD_STPMTITLE_RESET_VALUE = "";
    public static final String CHILD_STPMINFOMSG = "stPmInfoMsg";
    public static final String CHILD_STPMINFOMSG_RESET_VALUE = "";
    public static final String CHILD_STPMONOK = "stPmOnOk";
    public static final String CHILD_STPMONOK_RESET_VALUE = "";
    public static final String CHILD_STPMMSGS = "stPmMsgs";
    public static final String CHILD_STPMMSGS_RESET_VALUE = "";
    public static final String CHILD_STPMMSGTYPES = "stPmMsgTypes";
    public static final String CHILD_STPMMSGTYPES_RESET_VALUE = "";
    public static final String CHILD_STAMGENERATE = "stAmGenerate";
    public static final String CHILD_STAMGENERATE_RESET_VALUE = "";
    public static final String CHILD_STAMHASTITLE = "stAmHasTitle";
    public static final String CHILD_STAMHASTITLE_RESET_VALUE = "";
    public static final String CHILD_STAMHASINFO = "stAmHasInfo";
    public static final String CHILD_STAMHASINFO_RESET_VALUE = "";
    public static final String CHILD_STAMHASTABLE = "stAmHasTable";
    public static final String CHILD_STAMHASTABLE_RESET_VALUE = "";
    public static final String CHILD_STAMTITLE = "stAmTitle";
    public static final String CHILD_STAMTITLE_RESET_VALUE = "";
    public static final String CHILD_STAMINFOMSG = "stAmInfoMsg";
    public static final String CHILD_STAMINFOMSG_RESET_VALUE = "";
    public static final String CHILD_STAMMSGS = "stAmMsgs";
    public static final String CHILD_STAMMSGS_RESET_VALUE = "";
    public static final String CHILD_STAMMSGTYPES = "stAmMsgTypes";
    public static final String CHILD_STAMMSGTYPES_RESET_VALUE = "";
    public static final String CHILD_STAMDIALOGMSG = "stAmDialogMsg";
    public static final String CHILD_STAMDIALOGMSG_RESET_VALUE = "";
    public static final String CHILD_STAMBUTTONSHTML = "stAmButtonsHtml";
    public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE = "";
    public static final String CHILD_STTARGETDOWNPAYMENT = "stTargetDownPayment";
    public static final String CHILD_STTARGETDOWNPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STTARGETESCROW = "stTargetEscrow";
    public static final String CHILD_STTARGETESCROW_RESET_VALUE = "";
    public static final String CHILD_TXTOTALDOWN = "txTotalDown";
    public static final String CHILD_TXTOTALDOWN_RESET_VALUE = "";
    public static final String CHILD_TXTOTALESCROW = "txTotalEscrow";
    public static final String CHILD_TXTOTALESCROW_RESET_VALUE = "";
    public static final String CHILD_TBPAYMENTTERMDESCRIPTION =
        "tbPaymentTermDescription";
    public static final String CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE = "";
    public static final String CHILD_TBRATECODE = "tbRateCode";
    public static final String CHILD_TBRATECODE_RESET_VALUE = "";
    public static final String CHILD_STRATETIMESTAMP = "stRateTimeStamp";
    public static final String CHILD_STRATETIMESTAMP_RESET_VALUE = "";
    public static final String CHILD_HDLONGPOSTEDDATE = "hdLongPostedDate";
    public static final String CHILD_HDLONGPOSTEDDATE_RESET_VALUE = "";
    public static final String CHILD_STLENDERPRODUCTID = "stLenderProductId";
    public static final String CHILD_STLENDERPRODUCTID_RESET_VALUE = "";
    public static final String CHILD_STLENDERPROFILEID = "stLenderProfileId";
    public static final String CHILD_STLENDERPROFILEID_RESET_VALUE = "";
    public static final String CHILD_STINCLUDEDSSSTART = "stIncludeDSSstart";
    public static final String CHILD_STINCLUDEDSSSTART_RESET_VALUE = "";
    public static final String CHILD_STINCLUDEDSSEND = "stIncludeDSSend";
    public static final String CHILD_STINCLUDEDSSEND_RESET_VALUE = "";
    public static final String CHILD_STDEALID = "stDealId";
    public static final String CHILD_STDEALID_RESET_VALUE = "";
    public static final String CHILD_STBORRFIRSTNAME = "stBorrFirstName";
    public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE = "";
    public static final String CHILD_STDEALSTATUS = "stDealStatus";
    public static final String CHILD_STDEALSTATUS_RESET_VALUE = "";
    public static final String CHILD_STDEALSTATUSDATE = "stDealStatusDate";
    public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE = "";
    public static final String CHILD_STLOB = "stLOB";
    public static final String CHILD_STLOB_RESET_VALUE = "";
    public static final String CHILD_STDEALTYPE = "stDealType";
    public static final String CHILD_STDEALTYPE_RESET_VALUE = "";
    public static final String CHILD_STDEALPURPOSE = "stDealPurpose";
    public static final String CHILD_STDEALPURPOSE_RESET_VALUE = "";
    public static final String CHILD_STTOTALLOANAMOUNT = "stTotalLoanAmount";
    public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STPMTTERM = "stPmtTerm";
    public static final String CHILD_STPMTTERM_RESET_VALUE = "";
    public static final String CHILD_STESTCLOSINGDATE = "stEstClosingDate";
    public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE = "";
    public static final String CHILD_STSOURCE = "stSource";
    public static final String CHILD_STSOURCE_RESET_VALUE = "";
    public static final String CHILD_STSOURCEFIRM = "stSourceFirm";
    public static final String CHILD_STSOURCEFIRM_RESET_VALUE = "";
    public static final String CHILD_STPURCHASEPRICE = "stPurchasePrice";
    public static final String CHILD_STPURCHASEPRICE_RESET_VALUE = "";
    public static final String CHILD_STSPECIALFEATURE = "stSpecialFeature";
    public static final String CHILD_STSPECIALFEATURE_RESET_VALUE = "";
    public static final String CHILD_BTOK = "btOK";
    public static final String CHILD_BTOK_RESET_VALUE = "OK";
    public static final String CHILD_STPAGETITLE = "stPageTitle";
    public static final String CHILD_STPAGETITLE_RESET_VALUE = "";
    public static final String CHILD_BTRESSURECT = "btRessurect";
    public static final String CHILD_BTRESSURECT_RESET_VALUE = "Resurrect";
    public static final String CHILD_BTPREAPPROVALFIRM = "btPreapprovalFirm";
    public static final String CHILD_BTPREAPPROVALFIRM_RESET_VALUE =
        "Preapproval Firm";
    public static final String CHILD_STAPPDATEFORSERVLET = "stAppDateForServlet";
    public static final String CHILD_STAPPDATEFORSERVLET_RESET_VALUE = "";
    public static final String CHILD_STMISTATUS = "stMIStatus";
    public static final String CHILD_STMISTATUS_RESET_VALUE = "";
    public static final String CHILD_TXMIEXISTINGPOLICY = "txMIExistingPolicy";
    public static final String CHILD_TXMIEXISTINGPOLICY_RESET_VALUE = "";
    public static final String CHILD_TXMICERTIFICATE = "txMICertificate";
    public static final String CHILD_TXMICERTIFICATE_RESET_VALUE = "";
    public static final String CHILD_STINCLUDECOMMITMENTSTART =
        "stIncludeCommitmentStart";
    public static final String CHILD_STINCLUDECOMMITMENTSTART_RESET_VALUE = "";
    public static final String CHILD_STINCLUDECOMMITMENTEND =
        "stIncludeCommitmentEnd";
    public static final String CHILD_STINCLUDECOMMITMENTEND_RESET_VALUE = "";
    public static final String CHILD_STSOURCEPHONENOEXTENSION =
        "stSourcePhoneNoExtension";
    public static final String CHILD_STSOURCEPHONENOEXTENSION_RESET_VALUE = "";
    public static final String CHILD_STRATELOCK = "stRateLock";
    public static final String CHILD_STRATELOCK_RESET_VALUE = "";
    public static final String CHILD_STDEALENTRY = "stDealEntry";
    public static final String CHILD_STDEALENTRY_RESET_VALUE = "";
    public static final String CHILD_STDEFAULTPRODUCTID = "stDefaultProductId";
    public static final String CHILD_STDEFAULTPRODUCTID_RESET_VALUE = "";
    public static final String CHILD_STVIEWONLYTAG = "stViewOnlyTag";
    public static final String CHILD_STVIEWONLYTAG_RESET_VALUE = "";
    public static final String CHILD_RBPROGRESSADVANCE = "rbProgressAdvance";
    public static final String CHILD_RBPROGRESSADVANCE_RESET_VALUE = "N";
    public static final String CHILD_TXNEXTADVANCEAMOUNT = "txNextAdvanceAmount";
    public static final String CHILD_TXNEXTADVANCEAMOUNT_RESET_VALUE = "";
    public static final String CHILD_TXADVANCETODATEAMT = "txAdvanceToDateAmt";
    public static final String CHILD_TXADVANCETODATEAMT_RESET_VALUE = "";
    public static final String CHILD_TXADVANCENUMBER = "txAdvanceNumber";
    public static final String CHILD_TXADVANCENUMBER_RESET_VALUE = "";
    public static final String CHILD_CBREFIORIGPURCHASEDATEMONTH =
        "cbRefiOrigPurchaseDateMonth";
    public static final String CHILD_CBREFIORIGPURCHASEDATEMONTH_RESET_VALUE = "";
    private OptionList cbRefiOrigPurchaseDateMonthOptions =
        new OptionList(new String[] {  }, new String[] {  });
    public static final String CHILD_TXREFIORIGPURCHASEDATEYEAR =
        "txRefiOrigPurchaseDateYear";
    public static final String CHILD_TXREFIORIGPURCHASEDATEYEAR_RESET_VALUE = "";
    public static final String CHILD_TXREFIORIGPURCHASEDATEDAY =
        "txRefiOrigPurchaseDateDay";
    public static final String CHILD_TXREFIORIGPURCHASEDATEDAY_RESET_VALUE = "";
    public static final String CHILD_TXREFIPURPOSE = "txRefiPurpose";
    public static final String CHILD_TXREFIPURPOSE_RESET_VALUE = "";
    public static final String CHILD_TXREFIMORTGAGEHOLDER =
        "txRefiMortgageHolder";
    public static final String CHILD_TXREFIMORTGAGEHOLDER_RESET_VALUE = "";
    public static final String CHILD_RBBLENDEDAMORTIZATION =
        "rbBlendedAmortization";
    public static final String CHILD_RBBLENDEDAMORTIZATION_RESET_VALUE = "N";
    public static final String CHILD_TXREFIORIGPURCHASEPRICE =
        "txRefiOrigPurchasePrice";
    public static final String CHILD_TXREFIORIGPURCHASEPRICE_RESET_VALUE = "";
    public static final String CHILD_TXREFIIMPROVEMENTVALUE =
        "txRefiImprovementValue";
    public static final String CHILD_TXREFIIMPROVEMENTVALUE_RESET_VALUE = "";
    public static final String CHILD_TXREFIORIGMTGAMOUNT = "txRefiOrigMtgAmount";
    public static final String CHILD_TXREFIORIGMTGAMOUNT_RESET_VALUE = "";
    public static final String CHILD_TXREFIOUTSTANDINGMTGAMOUNT =
        "txRefiOutstandingMtgAmount";
    public static final String CHILD_TXREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE = "";
    public static final String CHILD_TXREFIIMPROVEMENTSDESC =
        "txRefiImprovementsDesc";
    public static final String CHILD_TXREFIIMPROVEMENTSDESC_RESET_VALUE = "";
    public static final String CHILD_STVALSDATA = "stVALSData";
    public static final String CHILD_STVALSDATA_RESET_VALUE = "";
    public static final String CHILD_BTRECALCULATE = "btRecalculate";
    public static final String CHILD_BTRECALCULATE_RESET_VALUE = " ";

    //// New hidden button implementing the 'fake' one from the ActiveMessage class.
    public static final String CHILD_BTACTMSG = "btActMsg";
    public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";

    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session id throughout all modulus.
    public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";
    public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

    ////--Release2.1--//
    public static final String CHILD_STRATEINVENTORYID = "stRateInventoryId";
    public static final String CHILD_STRATEINVENTORYID_RESET_VALUE = "";
    public static final String CHILD_PAYMENTTERMDESC = "stPaymentTermDesc";
    public static final String CHILD_PAYMENTTERMDESC_RESET_VALUE = "";

    ////--Release2.1--//
    //// New HiddenField to get the paymentTermId  from JavaScript and propagate into
    //// the database.
    public static final String CHILD_HDPAYMENTTERMID = "hdPaymentTermId";
    public static final String CHILD_HDPAYMENTTERMID_RESET_VALUE = "";

    //===============================================================
    // New fields to handle MIPolicyNum problem :
    //  the number lost if user cancel MI and re-send again later.
    // -- Modified by Billy 14July2003
    public static final String CHILD_HDMIPOLICYNOCMHC = "hdMIPolicyNoCMHC";
    public static final String CHILD_HDMIPOLICYNOCMHC_RESET_VALUE = "";
    public static final String CHILD_HDMIPOLICYNOGE = "hdMIPolicyNoGE";
    public static final String CHILD_HDMIPOLICYNOGE_RESET_VALUE = "";

    //===============================================================
    //-- FXLink Phase II --//
    //--> New Field for Market Type Indicator
    //--> By Billy 18Nov2003
    public static final String CHILD_STMARKETTYPELABEL = "stMarketTypeLabel";
    public static final String CHILD_STMARKETTYPELABEL_RESET_VALUE = "";
    public static final String CHILD_STMARKETTYPE = "stMarketType";
    public static final String CHILD_STMARKETTYPE_RESET_VALUE = "";

    //=======================================
    //--DJ_PT_CR--start//
    public static final String CHILD_STBRANCHTRANSITLABEL =
        "stBranchTransitLabel";
    public static final String CHILD_STBRANCHTRANSITLABEL_RESET_VALUE = "";
    public static final String CHILD_STPARTYNAME = "stPartyName";
    public static final String CHILD_STPARTYNAME_RESET_VALUE = "";
    public static final String CHILD_STBRANCHTRANSITNUM = "stBranchTransitNumber";
    public static final String CHILD_STBRANCHTRANSITNUM_RESET_VALUE = "";
    public static final String CHILD_STADDRESSLINE1 = "stAddressLine1";
    public static final String CHILD_STADDRESSLINE1_RESET_VALUE = "";
    public static final String CHILD_STCITY = "stCity";
    public static final String CHILD_STCITY_RESET_VALUE = "";

    //--DJ_PT_CR--end//
    //--DJ_LDI_CR--start--//
    public static final String CHILD_BTLDINSURANCEDETAILS =
        "btLDInsuranceDetails";
    public static final String CHILD_BTLDINSURANCEDETAILS_RESET_VALUE = " ";
    public static final String CHILD_STINCLUDELIFEDISLABELSSTART =
        "stIncludeLifeDisLabelsStart";
    public static final String CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE = "";
    public static final String CHILD_STINCLUDELIFEDISLABELSEND =
        "stIncludeLifeDisLabelsEnd";
    public static final String CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE = "";
    public static final String CHILD_STGDSTDSDETAILSLABEL =
        "stGdsTdsDetailsLabel";
    public static final String CHILD_STGDSTDSDETAILSLABEL_RESET_VALUE = "";
    public static final String CHILD_STPMNTINCLUDINGLIFEDISABILITY =
        "stPmntInclLifeDis";
    public static final String CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE =
        "";

    //--DJ_LDI_CR--end--//
    //--FX_LINK--start--//
    public static final String CHILD_BTSOURCEOFBUSINESSDETAILS = "btSOBDetails";
    public static final String CHILD_BTSOURCEOFBUSINESSDETAILS_RESET_VALUE = " ";

    //--FX_LINK--end--//
    //--DJ_CR010--start//
    public static final String CHILD_TXCOMMISIONCODE = "txCommisionCode";
    public static final String CHILD_TXCOMMISIONCODE_RESET_VALUE = "";

    //--DJ_CR010--end//
    //--DJ_CR203.1--start//
    public static final String CHILD_TXPROPRIETAIREPLUSLOC =
        "txProprietairePlusLOC";
    public static final String CHILD_TXPROPRIETAIREPLUSLOC_RESET_VALUE = "";
    protected OptionList rbMultiProjectOptions =
        new OptionList(new String[] {  }, new String[] {  });
    public static final String CHILD_RBMULTIPROJECT = "rbMultiProject";
    public static final String CHILD_RBMULTIPROJECT_RESET_VALUE = "";
    protected OptionList rbProprietairePlusOptions =
        new OptionList(new String[] {  }, new String[] {  });
    public static final String CHILD_RBPROPRIETAIREPLUS = "rbProprietairePlus";
    public static final String CHILD_RBPROPRIETAIREPLUS_RESET_VALUE = "N";

    //--DJ_CR203.1--end//
    //--DJ_CR134--start--27May2004--//
    public static final String CHILD_STHOMEBASEPRODUCTRATEPMNT =
        "stHomeBASERateProductPmnt";
    public static final String CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE = "";

    //--DJ_CR134--end--//
    //--DisableProductRateTicket#570--10Aug2004--start--//
    public static final String CHILD_STDEALRATESTATUSFORSERVLET =
        "stDealRateStatusForServlet";
    public static final String CHILD_STDEALRATESTATUSFORSERVLET_RESET_VALUE = "";
    public static final String CHILD_STRATEDISDATEFORSERVLET =
        "stRateDisDateForServlet";
    public static final String CHILD_STRATEDISDATEFORSERVLET_RESET_VALUE = "";
    public static final String CHILD_STISPAGEEDITABLEFORSERVLET =
        "stIsPageEditableForServlet";
    public static final String CHILD_STISPAGEEDITABLEFORSERVLET_RESET_VALUE = "";

    //--DisableProductRateTicket#570--10Aug2004--end--//
    //-- ========== DJ#725 Begins ========== --//
    //-- By Neil at Nov/30/2004
    public static final String CHILD_STCONTACTEMAILADDRESS =
        "stContactEmailAddress";
    public static final String CHILD_STCONTACTEMAILADDRESS_RESET_VALUE = "";
    public static final String CHILD_STPSDESCRIPTION = "stPsDescription";
    public static final String CHILD_STPSDESCRIPTION_RESET_VALUE = "";
    public static final String CHILD_STSYSTEMTYPEDESCRIPTION =
        "stSystemTypeDescription";
    public static final String CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE = "";
    //-- ========== DJ#725 Ends ========== --//

    //-- ========== SCR#859 begins ========== --//
    //-- by Neil on Feb 15, 2005
    public static final String CHILD_STSERVICINGMORTGAGENUMBER = "stServicingMortgageNumber";
    public static final String CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE = "";
    public static final String CHILD_STCCAPS = "stCCAPS";
    public static final String CHILD_STCCAPS_RESET_VALUE = "";
    //-- ========== SCR#859 ends ========== --//

    // SEAN GECF IV Document Type drop down  Define constants.
    public static final String CHILD_CBIVDOCUMENTTYPE = "cbIVDocumentType";
    public static final String CHILD_CBIVDOCUMENTTYPE_RESET_VALUE = "";
    public static final String CHILD_STHIDEIVDOCUMENTTYPESTART =
        "stHideIVDocumentTypeStart";
    public static final String CHILD_STHIDEIVDOCUMENTTYPESTART_RESET_VALUE = "";
    public static final String CHILD_STHIDEIVDOCUMENTTYPEEND =
        "stHideIVDocumentTypeEnd";
    public static final String CHILD_STHIDEIVDOCUMENTTYPEEND_RESET_VALUE = "";
    // END GECF IV Document Type drop down

    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: added a drop down combo
    // box, it will be hidden for non-DJ client.
    public static final String CHILD_CBPROGRESSADVANCETYPE = "cbProgressAdvanceType";
    public static final String CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE = "";
    public static final String CHILD_STHIDEPROGRESSADVANCETYPESTART =
        "stHideProgressAdvanceTypeStart";
    public static final String CHILD_STHIDEPROGRESSADVANCETYPESTART_RESET_VALUE = "";
    public static final String CHILD_STHIDEPROGRESSADVANCETYPEEND =
        "stHideProgressAdvanceTypeEnd";
    public static final String CHILD_STHIDEPROGRESSADVANCETYPEEND_RESET_VALUE = "";
    // SEAN DJ SPEC-PAT

    //***** Change by NBC/PP Implementation Team - Version 1.3 - Start *****//
    public static final String CHILD_TXRATEGUARANTEEPERIOD = "txRateGuaranteePeriod";
    public static final String CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE = "";
    public static final String CHILD_HDCHANGEDESTIMATEDCLOSINGDATE = "hdChangedEstimatedClosingDate";
    public static final String CHILD_HDCHANGEDESTIMATEDCLOSINGDATE_RESET_VALUE = "";
    //***** Change by NBC/PP Implementation Team - Version 1.3 - End *****//

    // ***** Change by NBC Impl. Team - Version 1.6 - Start*****//
    public static final String CHILD_CBAFFILIATIONPROGRAM = "cbAffiliationProgram";

    public static final String CHILD_CBAFFILIATIONPROGRAM_RESET_VALUE = "";
    // ***** Change by NBC Impl. Team - Version 1.6 - End*****//

    ////private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
    ////Added the Variable for NonSelected Label
    private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";


    //--Release3.1--begins
    //--by Hiro Apr 13, 2006
    public static final String CHILD_CBPRODUCTTYPE = "cbProductType";
    public static final String CHILD_CBPRODUCTTYPE_RESET_VALUE = " ";

    public static final String CHILD_CBREFIPRODUCTTYPE = "cbRefiProductType";
    public static final String CHILD_CBREFIPRODUCTTYPE_RESET_VALUE = " ";

    public static final String CHILD_RBPROGRESSADVANCEINSPECTIONBY = "rbProgressAdvanceInspectionBy";
    public static final String CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE = "";
    private OptionList rbProgressAdvanceInspectionByOptions = new OptionList();

    public static final String CHILD_RBSELFDIRECTEDRRSP = "rbSelfDirectedRRSP";
    public static final String CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE = "";
    private OptionList rbSelfDirectedRRSPOptions = new OptionList();

    public static final String CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER = "txCMHCProductTrackerIdentifier";
    public static final String CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE = "";

    public static final String CHILD_CBLOCREPAYMENTTYPEID = "cbLOCRepaymentTypeId";
    public static final String CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE = " ";


    public static final String CHILD_RBREQUESTSTANDARDSERVICE = "rbRequestStandardService";
    public static final String CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE = "";
    private OptionList rbRequestStandardServiceOptions = new OptionList();

    public static final String CHILD_STLOCAMORTIZATIONMONTHS = "stLOCAmortizationMonths";
    public static final String CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE = "";

    public static final String CHILD_STLOCINTERESTONLYMATURITYDATE = "stLOCInterestOnlyMaturityDate";
    public static final String CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE = "";
    //--Release3.1--ends
    //***** Change by NBC/PP Implementation Team - GCD - Start *****//	

    public static final String CHILD_TX_CREDIT_DECISION_STATUS = "txCreditDecisionStatus";
    public static final String CHILD_TX_CREDIT_DECISION = "txCreditDecision";
    public static final String CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE = "txGlobalCreditBureauScore";
    public static final String CHILD_TX_GLOBAL_RISK_RATING = "txGlobalRiskRating";
    public static final String CHILD_TX_GLOBAL_INTERNAL_SCORE = "txGlobalInternalScore";

    public static final String CHILD_TX_MISTATUS = "txMIStatus";
    public static final String CHILD_BT_CREDIT_DECISION_PG = "btCreditDecisionPG";
    public static final String CHILD_HD_DEAL_ID = "hdDealID";

    public static final String CHILD_ST_INCLUDE_GCDSUM_START = "stIncludeGCDSumStart";
    public static final String CHILD_ST_INCLUDE_GCDSUM_END = "stIncludeGCDSumEnd";

    public static final String CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE = "";
    public static final String CHILD_TX_CREDIT_DECISION_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE = "";

    public static final String CHILD_TX_MISTATUS_RESET_VALUE = "";
    public static final String CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE = "";
    public static final String CHILD_HD_DEAL_ID_RESET_VALUE = "";

    public static final String CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE = "";
    public static final String CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE = "";

    private doAdjudicationResponseBNCModel doAdjudicationResponseBNCModel= null;

    //***** Change by NBC/PP Implementation Team - GCD - End *****//

    //***** Change by NBC/PP Implementation Team - SCR #1679 - Start *****//
    public static final String CHILD_HDFORCEPROGRESSADVANCE = "hdForceProgressAdvance";
    public static final String CHILD_HDFORCEPROGRESSADVANCE_RESET_VALUE = "";
//  ***** Change by NBC/PP Implementation Team - SCR #1679 - End *****//

    //FFATE
    public static final String CHILD_CBFINANCINGWAIVERDATEMONTH =
        "cbFinancingWaiverDateMonth";
    public static final String CHILD_CBFINANCINGWAIVERDATEMONTH_RESET_VALUE = "";
    private OptionList cbFinancingWaiverDateMonthOptions =
        new OptionList(new String[] {  }, new String[] {  });

    public static final String CHILD_TXFINANCINGWAIVERDATEDAY =
        "txFinancingWaiverDateDay";
    public static final String CHILD_TXFINANCINGWAIVERDATEDAY_RESET_VALUE = "";
    public static final String CHILD_TXFINANCINGWAIVERDATEYEAR =
        "txFinancingWaiverDateYear";
    public static final String CHILD_TXFINANCINGWAIVERDATEYEAR_RESET_VALUE  = "";
    // FFATE
    
    //CR03
    public static final String CHILD_RBAUTOCALCOMMITDATE = "rbAutoCalCommitDate";
    public static final String CHILD_RBAUTOCALCOMMITDATE_RESET_VALUE = "Y";

    public static final String CHILD_CBCOMMEXPIRATIONDATEMONTH =
        "cbCommExpirationDateMonth";
    public static final String CHILD_CBCOMMEXPIRATIONDATEMONTH_RESET_VALUE = "";
    private OptionList cbCommExpirationDateMonthOptions =
        new OptionList(new String[] {  }, new String[] {  });

    public static final String CHILD_TXCOMMEXPIRATIONDATEDAY =
        "txCommExpirationDateDay";
    public static final String CHILD_TXCOMMEXPIRATIONDATEDAY_RESET_VALUE = "";
    public static final String CHILD_TXCOMMEXPIRATIONDATEYEAR =
        "txCommExpirationDateYear";
    public static final String CHILD_TXCOMMEXPIRATIONDATEYEAR_RESET_VALUE  = "";
    public static final String CHILD_HDCOMMITMENTEXPIRYDATE = "hdCommitmentExpiryDate";
    public static final String CHILD_HDCOMMITMENTEXPIRYDATE_RESET_VALUE = "";
    public static final String CHILD_HDMOSPROPERTY = "hdMosProperty";
    public static final String CHILD_HDMOSPROPERTY_RESET_VALUE = "";
    //CR03

    //PPI start
    public static final String CHILD_TXDEALPURPOSETYPEHIDDENSTART ="stDealPurposeTypeHiddenStart";
    public static final String CHILD_TXDEALPURPOSETYPEHIDDENSTART_RESET_VALUE = "";

    public static final String CHILD_TXIMPROVEDVALUE ="txImprovedValue";
    public static final String CHILD_TXIMPROVEDVALUE_RESET_VALUE = "";

    public static final String CHILD_TXDEALPURPOSETYPEHIDDENEND ="stDealPurposeTypeHiddenEnd";
    public static final String CHILD_TXDEALPURPOSETYPEHIDDENEND_RESET_VALUE = "";
    //PPI end

    /***************MCM Impl team changes starts - XS_2.7 *******************/

    public static final String CHILD_TXREFIADDITIONALINFORMATION ="txRefiAdditionalInformation";
    public static final String CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE = "";

    public static final String CHILD_TXREFEXISTINGMTGNUMBER ="txRefExistingMTGNumber";
    public static final String CHILD_TXREFEXISTINGMTGNUMBER_RESET_VALUE = "";


    /***************MCM Impl team changes ends - XS_2.7 *********************/

    /******************* MCM Impl team changes  - XS 2.9 *******************/
    public static final String CHILD_STCOMPONENTINFOPAGELET = "componentInfoPagelet";
    
    //***** Qualifying Rate *****/
    public static final String CHILD_CBQUALIFYPRODUCTTYPE = "cbQualifyProductType";
    public static final String CHILD_CBQUALIFYPRODUCTTYPE_RESET_VALUE = " ";
    
    public static final String CHILD_TXQUALIFYRATE = "txQualifyRate";
    public static final String CHILD_TXQUALIFYRATE_RESET_VALUE = "";
    
    public static final String CHILD_CHQUALIFYRATEOVERRIDE = "chQualifyRateOverride";
    public static final String CHILD_CHQUALIFYRATEOVERRIDE_RESET_VALUE = "N";
    
    public static final String CHILD_CHQUALIFYRATEOVERRIDERATE = "chQualifyRateOverrideRate";
    public static final String CHILD_CHQUALIFYRATEOVERRIDERATE_RESET_VALUE = "N";

    public static final String CHILD_BTRECALCULATEGDS = "btRecalculateGDS";
    public static final String CHILD_BTRECALCULATEGDS_RESET_VALUE = " ";
    
    public static final String CHILD_STQUALIFYRATE = "stQualifyRate";
    public static final String CHILD_STQUALIFYRATE_RESET_VALUE = "";
    
    public static final String CHILD_STQUALIFYPRODUCTTYPE = "stQualifyProductType";
    public static final String CHILD_STQUALIFYPRODUCTTYPE_RESET_VALUE = "";
    
    public static final String CHILD_STQUALIFYRATEHIDDEN = "stQualifyRateHidden";
    public static final String CHILD_STQUALIFYRATEHIDDEN_RESET_VALUE = "none";
    
    public static final String CHILD_STQUALIFYRATEEDIT = "stQualifyRateEdit";
    public static final String CHILD_STQUALIFYRATEEDIT_RESET_VALUE = "none";
    
    public static final String CHILD_STHDQUALIFYRATE = "sthdQualifyRate";
    public static final String CHILD_STHDQUALIFYRATE_RESET_VALUE = "";
    
    public static final String CHILD_STHDQUALIFYPRODUCTID = "sthdQualifyProductId";
    public static final String CHILD_STHDQUALIFYPRODUCTID_RESET_VALUE = "";
    
    public static final String CHILD_HDQUALIFYRATEDISABLED = "hdQualifyRateDisabled";
    public static final String CHILD_HDQUALIFYRATEDISABLED_RESET_VALUE = "";
    //***** Qualifying Rate *****/
    
    // Modified for Ticket 267 by Neha
    public static final String CHILD_HDDEALSTATUSID = "hdDealStatusId";
    public static final String CHILD_HDDEALSTATUSID_RESET_VALUE="";
    
    public void handleBtRecalculateGDSRequest(RequestInvocationEvent event)
      throws ServletException, IOException {

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleRecalculate(true);
        handler.postHandlerProtocol();
    }
    //  ***** Qualifying Rate *****/
    
    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbDealPurposeOptionList cbDealPurposeOptions=new CbDealPurposeOptionList();
    private CbDealPurposeOptionList cbDealPurposeOptions =
        new CbDealPurposeOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbDealTypeOptionList cbDealTypeOptions=new CbDealTypeOptionList();
    private CbDealTypeOptionList cbDealTypeOptions = new CbDealTypeOptionList();

    // SEAN GECF Document Type Drop Down define the option list.
    private CbIVDocumentTypeOptionList cbIVDocumentTypeOptions =
        new CbIVDocumentTypeOptionList();
    // END GECF Document Type Drop Down

    // SEAN DJ SPEC-Progress Advance Type July 22, 2005: Define the ComboBox
    // option list.
    private CbProgressAdvanceTypeOptionList cbProgressAdvanceTypeOptions =
        new CbProgressAdvanceTypeOptionList();
    // SEAN DJ SPEC-PAT

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbLOBOptionList cbLOBOptions=new CbLOBOptionList();
    private CbLOBOptionList cbLOBOptions = new CbLOBOptionList();

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList cbRateLockedOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList cbRateLockedOptions = new OptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbChargeTermOptionList cbChargeTermOptions=new CbChargeTermOptionList();
    private CbChargeTermOptionList cbChargeTermOptions =
        new CbChargeTermOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbPayemntFrequencyTermOptionList cbPayemntFrequencyTermOptions=new CbPayemntFrequencyTermOptionList();
    private CbPayemntFrequencyTermOptionList cbPayemntFrequencyTermOptions =
        new CbPayemntFrequencyTermOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbRepaymentTypeOptionList cbRepaymentTypeOptions=new CbRepaymentTypeOptionList();
    private CbRepaymentTypeOptionList cbRepaymentTypeOptions =
        new CbRepaymentTypeOptionList();

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList cbSecondMortgageExistOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList cbSecondMortgageExistOptions = new OptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbPrePaymentPenanltyOptionList cbPrePaymentPenanltyOptions=new CbPrePaymentPenanltyOptionList();
    private CbPrePaymentPenanltyOptionList cbPrePaymentPenanltyOptions =
        new CbPrePaymentPenanltyOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbPrivilagePaymentOptionOptionList cbPrivilagePaymentOptionOptions=new CbPrivilagePaymentOptionOptionList();
    private CbPrivilagePaymentOptionOptionList cbPrivilagePaymentOptionOptions =
        new CbPrivilagePaymentOptionOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbFinancingProgramOptionList cbFinancingProgramOptions=new CbFinancingProgramOptionList();
    private CbFinancingProgramOptionList cbFinancingProgramOptions =
        new CbFinancingProgramOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbSpecialFeatureOptionList cbSpecialFeatureOptions=new CbSpecialFeatureOptionList();
    private CbSpecialFeatureOptionList cbSpecialFeatureOptions =
        new CbSpecialFeatureOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbBranchOptionList cbBranchOptions=new CbBranchOptionList();
    private CbBranchOptionList cbBranchOptions = new CbBranchOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbCrossSellOptionList cbCrossSellOptions=new CbCrossSellOptionList();
    private CbCrossSellOptionList cbCrossSellOptions =
        new CbCrossSellOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbTaxPayorOptionList cbTaxPayorOptions=new CbTaxPayorOptionList();
    private CbTaxPayorOptionList cbTaxPayorOptions = new CbTaxPayorOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////protected static CbMIIndicatorOptionList cbMIIndicatorOptions=new CbMIIndicatorOptionList();
    protected CbMIIndicatorOptionList cbMIIndicatorOptions =
        new CbMIIndicatorOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbMIInsurerOptionList cbMIInsurerOptions=new CbMIInsurerOptionList();
    private CbMIInsurerOptionList cbMIInsurerOptions =
        new CbMIInsurerOptionList();

    //--Release2.1--//
    ////The Option list should not be Static, otherwise this will screw up the population.
    ////private static CbMIPayorOptionList cbMIPayorOptions=new CbMIPayorOptionList();
    private CbMIPayorOptionList cbMIPayorOptions = new CbMIPayorOptionList();

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList rbMIUpfrontOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList rbMIUpfrontOptions = new OptionList();

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList rbMIRUInterventionOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList rbMIRUInterventionOptions = new OptionList();

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList rbProgressAdvanceOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList rbProgressAdvanceOptions = new OptionList();

    //--Release2.1--//
    //// This OptionList population is done now in the BeginDisplay via the BXResources
    ////private static OptionList rbBlendedAmortizationOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
    private OptionList rbBlendedAmortizationOptions = new OptionList();

    //--Release3.1--begins
    //--by Hiro Apr 13, 2006
    private CbProductTypeOptionList cbProductTypeOptions = new CbProductTypeOptionList();
    private CbRefiProductTypeOptionList cbRefiProductTypeOptions = new CbRefiProductTypeOptionList();
    private CbLOCRepaymentTypeOptionList cbLOCRepaymentTypeOptions = new CbLOCRepaymentTypeOptionList();
    //--Release3.1--ends

    //  ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
    private CbAffiliationProgramOptionList cbAffiliationProgramOptions = new CbAffiliationProgramOptionList();
    //  ***** Change by NBC Impl. Team - Version 1.6 - End *****//

    ////////////////////////////////////////////////////////////////////////////
    // Instance variables
    ////////////////////////////////////////////////////////////////////////////
    private doDealEntryMainSelectModel doDealEntryMainSelect = null;
    private doDealEntrySourceInfoModel doDealEntrySourceInfo = null;
    private doDealEntryBridgeModel doDealEntryBridge = null;
    private doDealGetReferenceInfoModel doDealGetReferenceInfo = null;
    private doDealSummarySnapShotModel doDealSummarySnapShot = null;
    private doDealMIStatusModel doDealMIStatus = null;
    protected String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgDealEntry.jsp";

    //--DJ_PT_CR--start//
    private doPartySummaryModel doPartySummary = null;

    //--DJ_PT_CR--end//
    ////////////////////////////////////////////////////////////////////////////
    // Custom Members - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    // MigrationToDo : Migrate custom member
    protected DealEntryHandler handler = new DealEntryHandler();
    public SysLogger logger;

    //***** Change by NBC Impl. Team - Version 1.5 - Start *****//

    public static final String CHILD_TXCASHBACKINDOLLARS = "txCashBackInDollars";
    public static final String CHILD_TXCASHBACKINDOLLARS_RESET_VALUE = "";
    public static final String CHILD_TXCASHBACKINPERCENTAGE = "txCashBackInPercentage";
    public static final String CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE = "";
    public static final String CHILD_CHCASHBACKOVERRIDEINDOLLARS = "chCashBackOverrideInDollars";
    public static final String CHILD_CHCASHBACKOVERRIDEINDOLLARS_RESET_VALUE = "N";

    // ***** Change by NBC Impl. Team - Version 1.5 - End*****//

    //CR03
    private OptionList rbAutoCalCommitDateOptions = new OptionList();
    //CR03

    /***************MCM Impl team changes starts - XS_2.13*******************/
    public static final String CHILD_STQUALIFYINGDETAILSPAGELET = "qualifyingDetailsPagelet";
    /***************MCM Impl team changes ends - XS_2.13*******************/
    /**
     *
     *
     */
    public pgDealEntryViewBean()
    {
        super(PAGE_NAME);
        setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

        registerChildren();

        initialize();
    }

    /**
     *
     *
     */
    protected void initialize()
    {
        // The following code block was migrated from the this_onAfterInitEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       //disable all of the log messages from showing on the screen
       Log.setSendErrorsToHtml(false);

       ViewBean page =(ViewBean) event.getSource();

       page.setCacheDuration(0);

       return;
         */
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Child manipulation methods
    ////////////////////////////////////////////////////////////////////////////////

    /**
     * createChild
     *
     * @version 1.3 <br>
     * Date: 06/29/2006<br> 
     * Author NBC/PP Implementation Team<br>
     * Change: <br>
     * 	Added new fields txRateGuaranteePeriod & hdChangedEstimatedClosingDate<br>
     * <br>
     * Date: 11/21/2006<br> 
     * Author NBC/PP Implementation Team<br>
     * Change: <br>
     * 	Added new field hdForceProgressAdvance for SCR #1679<br>
     *
     *	@Version 1.4 <br>
     *	Date: 06/09/2008
     *	Author: MCM Impl Team <br>
     *	Change Log:  <br>
     *	XS_2.7 -- 06/06/2008 -- Added necessary changes required for the new fields added in Refinance section<br>
     *
     *  @Version 1.7 <br>
     *  Date: 06/09/2008
     *  Author: MCM Impl Team <br>
     *  Change Log:  <br>
     *  XS_2.7 -- 06/06/2008 -- Added necessary changes required for the new fields added in Refinance section<br>
     *  
     *  @Version 1.9 <br>
     *  Date: July 22, 2008
     *  Author: MCM Impl Team <br>
     *  Change Log:  <br>
     *  XS_244 -- Added handler to pgComponentInfoPageletView constractor<br>
     *  
     *  
     */
    protected View createChild(String name)
    {
        View superReturn = super.createChild(name);  
        if (superReturn != null) {  
            return superReturn;  
        }
        else if (name.equals(CHILD_STPAGELABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPAGELABEL,
                        CHILD_STPAGELABEL, CHILD_STPAGELABEL_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STCOMPANYNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STCOMPANYNAME,
                        CHILD_STCOMPANYNAME,
                        CHILD_STCOMPANYNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTODAYDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STTODAYDATE,
                        CHILD_STTODAYDATE, CHILD_STTODAYDATE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STUSERNAMETITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STUSERNAMETITLE,
                        CHILD_STUSERNAMETITLE,
                        CHILD_STUSERNAMETITLE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_DETECTALERTTASKS))
        {
            HiddenField child =
                new HiddenField(this, getDefaultModel(), CHILD_DETECTALERTTASKS,
                        CHILD_DETECTALERTTASKS,
                        CHILD_DETECTALERTTASKS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_SESSIONUSERID))
        {
            HiddenField child =
                new HiddenField(this, getDefaultModel(), CHILD_SESSIONUSERID,
                        CHILD_SESSIONUSERID, CHILD_SESSIONUSERID_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_TBDEALID))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TBDEALID, CHILD_TBDEALID,
                        CHILD_TBDEALID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBPAGENAMES))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBPAGENAMES,
                        CHILD_CBPAGENAMES, CHILD_CBPAGENAMES_RESET_VALUE, null);

            //--Release2.1--//
            ////Modified to set NonSelected Label as a CHILD_CBPAGENAMES_NONSELECTED_LABEL
            ////child.setLabelForNoneSelected("Choose a Page");
            child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
            child.setOptions(cbPageNamesOptions);

            return child;
        }
        else if (name.equals(CHILD_BTPROCEED))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPROCEED, CHILD_BTPROCEED,
                        CHILD_BTPROCEED_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF1))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF1, CHILD_HREF1,
                        CHILD_HREF1_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF2))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF2, CHILD_HREF2,
                        CHILD_HREF2_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF3))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF3, CHILD_HREF3,
                        CHILD_HREF3_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF4))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF4, CHILD_HREF4,
                        CHILD_HREF4_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF5))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF5, CHILD_HREF5,
                        CHILD_HREF5_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF6))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF6, CHILD_HREF6,
                        CHILD_HREF6_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF7))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF7, CHILD_HREF7,
                        CHILD_HREF7_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF8))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF8, CHILD_HREF8,
                        CHILD_HREF8_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF9))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF9, CHILD_HREF9,
                        CHILD_HREF9_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF10))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF10, CHILD_HREF10,
                        CHILD_HREF10_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CHANGEPASSWORDHREF))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_CHANGEPASSWORDHREF,
                        CHILD_CHANGEPASSWORDHREF,
                        CHILD_CHANGEPASSWORDHREF_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTTOOLHISTORY))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTTOOLHISTORY,
                        CHILD_BTTOOLHISTORY, CHILD_BTTOOLHISTORY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTTOONOTES))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTTOONOTES, CHILD_BTTOONOTES,
                        CHILD_BTTOONOTES_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTTOOLSEARCH))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTTOOLSEARCH,
                        CHILD_BTTOOLSEARCH, CHILD_BTTOOLSEARCH_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTTOOLLOG))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTTOOLLOG, CHILD_BTTOOLLOG,
                        CHILD_BTTOOLLOG_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTWORKQUEUELINK))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTWORKQUEUELINK,
                        CHILD_BTWORKQUEUELINK, CHILD_BTWORKQUEUELINK_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_BTPREVTASKPAGE))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPREVTASKPAGE,
                        CHILD_BTPREVTASKPAGE, CHILD_BTPREVTASKPAGE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTNEXTTASKPAGE))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTNEXTTASKPAGE,
                        CHILD_BTNEXTTASKPAGE, CHILD_BTNEXTTASKPAGE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STERRORFLAG))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STERRORFLAG,
                        CHILD_STERRORFLAG, CHILD_STERRORFLAG_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPREVTASKPAGELABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPREVTASKPAGELABEL,
                        CHILD_STPREVTASKPAGELABEL,
                        CHILD_STPREVTASKPAGELABEL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STNEXTTASKPAGELABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STNEXTTASKPAGELABEL,
                        CHILD_STNEXTTASKPAGELABEL,
                        CHILD_STNEXTTASKPAGELABEL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTASKNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STTASKNAME,
                        CHILD_STTASKNAME, CHILD_STTASKNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDDEALID))
        {
            HiddenField child =
                new HiddenField(this, getdoDealEntryMainSelectModel(), CHILD_HDDEALID,
                        doDealEntryMainSelectModel.FIELD_DFDEALID,
                        CHILD_HDDEALID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDDEALCOPYID))
        {
            HiddenField child =
                new HiddenField(this, getdoDealEntryMainSelectModel(),
                        CHILD_HDDEALCOPYID,
                        doDealEntryMainSelectModel.FIELD_DFCOPYID,
                        CHILD_HDDEALCOPYID_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_HDDEALINSTITUTIONID)) {
            HiddenField child =
                new HiddenField(this, getdoDealEntryMainSelectModel(), 
                        CHILD_HDDEALINSTITUTIONID,
                        doDealEntryMainSelectModel.FIELD_DFINSTITUTIONID,
                        CHILD_HDDEALINSTITUTIONID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALNO))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STDEALNO,
                        doDealEntryMainSelectModel.FIELD_DFDEALID,
                        CHILD_STDEALNO_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDGDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMBINEDGDS,
                        doDealEntryMainSelectModel.FIELD_DFCOMBINEDGDS,
                        CHILD_STCOMBINEDGDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINED3YRGDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMBINED3YRGDS,
                        doDealEntryMainSelectModel.FIELD_DFCOMBINEDGDS3YEAR,
                        CHILD_STCOMBINED3YRGDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDBORROWERGDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMBINEDBORROWERGDS,
                        doDealEntryMainSelectModel.FIELD_DFCOMBINEDGDSBORROWER,
                        CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDTDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMBINEDTDS,
                        doDealEntryMainSelectModel.FIELD_DFCOMBINEDTDS,
                        CHILD_STCOMBINEDTDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINED3YRTDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMBINED3YRTDS,
                        doDealEntryMainSelectModel.FIELD_DFCOMBINEDTDS3YEAR,
                        CHILD_STCOMBINED3YRTDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDBORROWERTDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMBINEDBORROWERTDS,
                        doDealEntryMainSelectModel.FIELD_DFCOMBINEDTDSBOROWER,
                        CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_REPEATGDSTDSDETAILS))
        {
            pgDealEntryRepeatGDSTDSDetailsTiledView child =
                new pgDealEntryRepeatGDSTDSDetailsTiledView(this,
                        CHILD_REPEATGDSTDSDETAILS);

            return child;
        }
        else if (name.equals(CHILD_REPEATAPPLICANT))
        {
            pgDealEntryRepeatApplicantTiledView child =
                new pgDealEntryRepeatApplicantTiledView(this, CHILD_REPEATAPPLICANT);

            return child;
        }
        else if (name.equals(CHILD_BTADDAPPLICANT))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTADDAPPLICANT,
                        CHILD_BTADDAPPLICANT, CHILD_BTADDAPPLICANT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTDUPAPPLICANT))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTDUPAPPLICANT,
                        CHILD_BTDUPAPPLICANT, CHILD_BTDUPAPPLICANT_RESET_VALUE, null);

            return child;
        }
/*        else if (name.equals(CHILD_BTCHANGESOURCE))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTCHANGESOURCE,
                        CHILD_BTCHANGESOURCE, CHILD_BTCHANGESOURCE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCEFIRMSOURCEPART))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STSOURCEFIRMSOURCEPART,
                        doDealEntrySourceInfoModel.FIELD_DFSOURCEFIRMNAME,
                        CHILD_STSOURCEFIRMSOURCEPART_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCEFIRSTNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STSOURCEFIRSTNAME,
                        doDealEntrySourceInfoModel.FIELD_DFSOURCEFIRSTNAME,
                        CHILD_STSOURCEFIRSTNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCELASTNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STSOURCELASTNAME,
                        doDealEntrySourceInfoModel.FIELD_DFSOURCELASTNAME,
                        CHILD_STSOURCELASTNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCEADDRESS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STSOURCEADDRESS,
                        doDealEntrySourceInfoModel.FIELD_DFFULLADDRESS,
                        CHILD_STSOURCEADDRESS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCEPHONENO))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STSOURCEPHONENO,
                        doDealEntrySourceInfoModel.FIELD_DFCONTACTPHONENUMBER,
                        CHILD_STSOURCEPHONENO_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCEFAXNO))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STSOURCEFAXNO,
                        doDealEntrySourceInfoModel.FIELD_DFCONTACTFAXNUMBER,
                        CHILD_STSOURCEFAXNO_RESET_VALUE, null);

            return child;
        }*/
        else if (name.equals(CHILD_TXREFERENCESOURCEAPP))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXREFERENCESOURCEAPP,
                        doDealEntryMainSelectModel.FIELD_DFREFSOURCEAPPID,
                        CHILD_TXREFERENCESOURCEAPP_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_REPEATPROPERTYINFORMATION))
        {
            pgDealEntryRepeatPropertyInformationTiledView child =
                new pgDealEntryRepeatPropertyInformationTiledView(this,
                        CHILD_REPEATPROPERTYINFORMATION);

            return child;
        }
        else if (name.equals(CHILD_BTADDPROPERTY))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTADDPROPERTY,
                        CHILD_BTADDPROPERTY, CHILD_BTADDPROPERTY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBDEALPURPOSE))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBDEALPURPOSE,

                        //--Release2.1--//
                        doDealEntryMainSelectModel.FIELD_DFDEALPURPOSEID,

                        ////doDealEntryMainSelectModel.FIELD_DFDEALPURPOSELABEL,
                        CHILD_CBDEALPURPOSE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbDealPurposeOptions);

            return child;
        }
        else if (name.equals(CHILD_CBDEALTYPE))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBDEALTYPE,
                        doDealEntryMainSelectModel.FIELD_DFDEALTYPEID,
                        CHILD_CBDEALTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbDealTypeOptions);

            return child;
        }
        // SEAN GECF Document type drop down. create the child.
        else if (name.equals(CHILD_CBIVDOCUMENTTYPE))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBIVDOCUMENTTYPE,
                        doDealEntryMainSelectModel.FIELD_DFIVDOCUMENTTYPEID,
                        CHILD_CBIVDOCUMENTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbIVDocumentTypeOptions);

            return child;
        }
        else if (name.equals(CHILD_STHIDEIVDOCUMENTTYPESTART))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STHIDEIVDOCUMENTTYPESTART,
                        CHILD_STHIDEIVDOCUMENTTYPESTART,
                        CHILD_STHIDEIVDOCUMENTTYPESTART_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STHIDEIVDOCUMENTTYPEEND))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STHIDEIVDOCUMENTTYPEEND,
                        CHILD_STHIDEIVDOCUMENTTYPEEND,
                        CHILD_STHIDEIVDOCUMENTTYPEEND_RESET_VALUE,
                        null);

            return child;
        }
        // END GECF document type drop down.
        // SEAN DJ SPEC-Progress Advance Type July 22, 2005: Create the child...
        else if (name.equals(CHILD_CBPROGRESSADVANCETYPE))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBPROGRESSADVANCETYPE,
                        doDealEntryMainSelectModel.FIELD_DFPROGRESSADVANCETYPEID,
                        CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbProgressAdvanceTypeOptions);

            return child;
        }
        // SEAN DJ SPEC-PAT END
        else if (name.equals(CHILD_CBLOB))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBLOB,
                        doDealEntryMainSelectModel.FIELD_DFLOBID,
                        CHILD_CBLOB_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbLOBOptions);

            return child;
        }
        else if (name.equals(CHILD_CBLENDER))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBLENDER, CHILD_CBLENDER,
                        CHILD_CBLENDER_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbLenderOptions);

            return child;
        }
        else if (name.equals(CHILD_CBPRODUCT))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBPRODUCT, CHILD_CBPRODUCT,
                        CHILD_CBPRODUCT_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbProductOptions);

            return child;
        }
        else if (name.equals(CHILD_CBRATELOCKED))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBRATELOCKED,
                        doDealEntryMainSelectModel.FIELD_DFRATELOCKEDIN,
                        CHILD_CBRATELOCKED_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbRateLockedOptions);

            return child;
        }
        else if (name.equals(CHILD_CBCHARGETERM))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBCHARGETERM,
                        doDealEntryMainSelectModel.FIELD_DFCHARGEID,
                        CHILD_CBCHARGETERM_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbChargeTermOptions);

            return child;
        }
        else if (name.equals(CHILD_CBPOSTEDINTERESTRATE))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBPOSTEDINTERESTRATE,
                        CHILD_CBPOSTEDINTERESTRATE,
                        CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbPostedInterestRateOptions);

            return child;
        }
        else if (name.equals(CHILD_TXDISCOUNT))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(), CHILD_TXDISCOUNT,
                        doDealEntryMainSelectModel.FIELD_DFDISCOUNT,
                        CHILD_TXDISCOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXPREMIUM))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(), CHILD_TXPREMIUM,
                        doDealEntryMainSelectModel.FIELD_DFPREMIUM,
                        CHILD_TXPREMIUM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXBUYDOWNRATE))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXBUYDOWNRATE,
                        doDealEntryMainSelectModel.FIELD_DFBUYDOWNRATE,
                        CHILD_TXBUYDOWNRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STNETRATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STNETRATE,
                        doDealEntryMainSelectModel.FIELD_DFNETRATE,
                        CHILD_STNETRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALESCROWPAYMENT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STTOTALESCROWPAYMENT,
                        doDealEntryMainSelectModel.FIELD_DFTOTALESCROWPAYMENT,
                        CHILD_STTOTALESCROWPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXREQUESTEDLOANAMOUNT))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXREQUESTEDLOANAMOUNT,
                        doDealEntryMainSelectModel.FIELD_DFREQUESTEDLOANAMOUNT,
                        CHILD_TXREQUESTEDLOANAMOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBPAYEMNTFREQUENCYTERM))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBPAYEMNTFREQUENCYTERM,
                        doDealEntryMainSelectModel.FIELD_DFPAYMENTFREQUENCYID,
                        CHILD_CBPAYEMNTFREQUENCYTERM_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbPayemntFrequencyTermOptions);

            return child;
        }
        else if (name.equals(CHILD_TXAMORTIZATIONYEARS))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXAMORTIZATIONYEARS,
                        CHILD_TXAMORTIZATIONYEARS,
                        CHILD_TXAMORTIZATIONYEARS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXAMORTIZATIONMONTHS))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXAMORTIZATIONMONTHS,
                        CHILD_TXAMORTIZATIONMONTHS,
                        CHILD_TXAMORTIZATIONMONTHS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STEFFECTIVEAMORTIZATIONYEARS))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STEFFECTIVEAMORTIZATIONYEARS,
                        CHILD_STEFFECTIVEAMORTIZATIONYEARS,
                        CHILD_STEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STEFFECTIVEAMORTIZATIONMONTHS))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STEFFECTIVEAMORTIZATIONMONTHS,
                        CHILD_STEFFECTIVEAMORTIZATIONMONTHS,
                        CHILD_STEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_TXADDITIONALPRINCIPALPAYMENT))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXADDITIONALPRINCIPALPAYMENT,
                        doDealEntryMainSelectModel.FIELD_DFADDITIONALPRINCIPALPAYMENT,
                        CHILD_TXADDITIONALPRINCIPALPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXPAYTERMYEARS))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXPAYTERMYEARS,
                        CHILD_TXPAYTERMYEARS, CHILD_TXPAYTERMYEARS_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_TXPAYTERMMONTHS))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXPAYTERMMONTHS,
                        CHILD_TXPAYTERMMONTHS, CHILD_TXPAYTERMMONTHS_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPIPAYMENT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STPIPAYMENT,
                        doDealEntryMainSelectModel.FIELD_DFPANDIPAYMENT,
                        CHILD_STPIPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALPAYMENT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STTOTALPAYMENT,
                        doDealEntryMainSelectModel.FIELD_DFTOTALPAYMENTAMOUNT,
                        CHILD_STTOTALPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBREPAYMENTTYPE))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBREPAYMENTTYPE,
                        doDealEntryMainSelectModel.FIELD_DFREPAYMENTID,
                        CHILD_CBREPAYMENTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbRepaymentTypeOptions);

            return child;
        }
        /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
        else if (name.equals(CHILD_STDEFAULTREPAYMENTTYPEID))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STDEFAULTREPAYMENTTYPEID,
                        doDealEntryMainSelectModel.FIELD_DFREPAYMENTID,
                        CHILD_STDEFAULTREPAYMENTTYPEID_RESET_VALUE, null);
            return child;
        }
        /***************MCM Impl team changes ends - XS_2.1, XS_2.2, XS_2.4 *******************/
        else if (name.equals(CHILD_CBESTCLOSINGDATEMONTHS))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBESTCLOSINGDATEMONTHS,
                        CHILD_CBESTCLOSINGDATEMONTHS,
                        CHILD_CBESTCLOSINGDATEMONTHS_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbEstClosingDateMonthsOptions);

            return child;
        }
        else if (name.equals(CHILD_TXESTCLOSINGDATEDAY))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXESTCLOSINGDATEDAY,
                        CHILD_TXESTCLOSINGDATEDAY,
                        CHILD_TXESTCLOSINGDATEDAY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXESTCLOSINGDATEYEAR))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXESTCLOSINGDATEYEAR,
                        CHILD_TXESTCLOSINGDATEYEAR,
                        CHILD_TXESTCLOSINGDATEYEAR_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBFIRSTPAYDATEMONTH))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBFIRSTPAYDATEMONTH,
                        CHILD_CBFIRSTPAYDATEMONTH,
                        CHILD_CBFIRSTPAYDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbFirstPayDateMonthOptions);

            return child;
        }
        else if (name.equals(CHILD_TXFIRSTPAYDATEDAY))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXFIRSTPAYDATEDAY,
                        CHILD_TXFIRSTPAYDATEDAY,
                        CHILD_TXFIRSTPAYDATEDAY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXFIRSTPAYDATEYEAR))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXFIRSTPAYDATEYEAR,
                        CHILD_TXFIRSTPAYDATEYEAR,
                        CHILD_TXFIRSTPAYDATEYEAR_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMATURITYDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STMATURITYDATE,
                        doDealEntryMainSelectModel.FIELD_DFMATURITYDATE,
                        CHILD_STMATURITYDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXPREAPPESTPURCHASEPRICE))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXPREAPPESTPURCHASEPRICE,
                        doDealEntryMainSelectModel.FIELD_DFPAPURCHASE,
                        CHILD_TXPREAPPESTPURCHASEPRICE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBSECONDMORTGAGEEXIST))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBSECONDMORTGAGEEXIST,
                        doDealEntryMainSelectModel.FIELD_DFSECONDARYFINANCING,
                        CHILD_CBSECONDMORTGAGEEXIST_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbSecondMortgageExistOptions);

            return child;
        }
        else if (name.equals(CHILD_CBPREPAYMENTPENANLTY))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBPREPAYMENTPENANLTY,
                        doDealEntryMainSelectModel.FIELD_DFPREPAYMENTOPTIONSID,
                        CHILD_CBPREPAYMENTPENANLTY_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbPrePaymentPenanltyOptions);

            return child;
        }
        else if (name.equals(CHILD_CBPRIVILAGEPAYMENTOPTION))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBPRIVILAGEPAYMENTOPTION,
                        doDealEntryMainSelectModel.FIELD_DFPREVILAGEPAYMENTID,
                        CHILD_CBPRIVILAGEPAYMENTOPTION_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbPrivilagePaymentOptionOptions);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDEFINANCINGPROGRAMSTART))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDEFINANCINGPROGRAMSTART,
                        CHILD_STINCLUDEFINANCINGPROGRAMSTART,
                        CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_CBFINANCINGPROGRAM))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBFINANCINGPROGRAM,
                        doDealEntryMainSelectModel.FIELD_DFFINANCINGPROGRAMID,
                        CHILD_CBFINANCINGPROGRAM_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbFinancingProgramOptions);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDEFINANCINGPROGRAMEND))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDEFINANCINGPROGRAMEND,
                        CHILD_STINCLUDEFINANCINGPROGRAMEND,
                        CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBSPECIALFEATURE))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBSPECIALFEATURE,
                        doDealEntryMainSelectModel.FIELD_DFSPEDCIALFEATUREID,
                        CHILD_CBSPECIALFEATURE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbSpecialFeatureOptions);

            return child;
        }
        else if (name.equals(CHILD_CBCOMMEXPECTEDDATEMONTH))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBCOMMEXPECTEDDATEMONTH,
                        CHILD_CBCOMMEXPECTEDDATEMONTH,
                        CHILD_CBCOMMEXPECTEDDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbCommExpectedDateMonthOptions);

            return child;
        }
        else if (name.equals(CHILD_TXCOMMEXPECTEDDATEDAY))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXCOMMEXPECTEDDATEDAY,
                        CHILD_TXCOMMEXPECTEDDATEDAY,
                        CHILD_TXCOMMEXPECTEDDATEDAY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXCOMMEXPECTEDDATEYEAR))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXCOMMEXPECTEDDATEYEAR,
                        CHILD_TXCOMMEXPECTEDDATEYEAR,
                        CHILD_TXCOMMEXPECTEDDATEYEAR_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMMITMENTPERIOD))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMMITMENTPERIOD,
                        doDealEntryMainSelectModel.FIELD_DFCOMMITMENTPERIOD,
                        CHILD_STCOMMITMENTPERIOD_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMMEXPDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMMEXPDATE,
                        doDealEntryMainSelectModel.FIELD_DFCOMMITMENTEXPIRATIONDATE,
                        CHILD_STCOMMEXPDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMMACCEPTANCEDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STCOMMACCEPTANCEDATE,
                        doDealEntryMainSelectModel.FIELD_DFCOMMITMENTACCEPTDATE,
                        CHILD_STCOMMACCEPTANCEDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGELOANAMOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryBridgeModel(),
                        CHILD_STBRIDGELOANAMOUNT,
                        doDealEntryBridgeModel.FIELD_DFNETLOANAMT,
                        CHILD_STBRIDGELOANAMOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXADVANCEHOLD))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXADVANCEHOLD,
                        doDealEntryMainSelectModel.FIELD_DFADVANCEHOLD,
                        CHILD_TXADVANCEHOLD_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTREVIEWBRIDGE))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTREVIEWBRIDGE,
                        CHILD_BTREVIEWBRIDGE, CHILD_BTREVIEWBRIDGE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDDOWNPAYMENT))
        {
            pgDealEntryRepeatedDownPaymentTiledView child =
                new pgDealEntryRepeatedDownPaymentTiledView(this,
                        CHILD_REPEATEDDOWNPAYMENT);

            return child;
        }
        else if (name.equals(CHILD_BTADDDOWNPAYMENT))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTADDDOWNPAYMENT,
                        CHILD_BTADDDOWNPAYMENT, CHILD_BTADDDOWNPAYMENT_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STDOWNPAYMENTREQUIRED))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STDOWNPAYMENTREQUIRED,
                        doDealEntryMainSelectModel.FIELD_DFREQUIREDDOWNPAYMENT,
                        CHILD_STDOWNPAYMENTREQUIRED_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDESCROWDETAILS))
        {
            pgDealEntryRepeatedEscrowDetailsTiledView child =
                new pgDealEntryRepeatedEscrowDetailsTiledView(this,
                        CHILD_REPEATEDESCROWDETAILS);

            return child;
        }
        else if (name.equals(CHILD_BTADDESCROW))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTADDESCROW,
                        CHILD_BTADDESCROW, CHILD_BTADDESCROW_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBBRANCH))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBBRANCH,
                        doDealEntryMainSelectModel.FIELD_DFBRANCHID,
                        CHILD_CBBRANCH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbBranchOptions);

            return child;
        }
        else if (name.equals(CHILD_TXREFEXISITINGMTG))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXREFEXISITINGMTG,
                        doDealEntryMainSelectModel.FIELD_DFREFERENCEEXISITINGMTG,
                        CHILD_TXREFEXISITINGMTG_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBCROSSSELL))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBCROSSSELL,
                        doDealEntryMainSelectModel.FIELD_DFCROSSSELLID,
                        CHILD_CBCROSSSELL_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbCrossSellOptions);

            return child;
        }
        else if (name.equals(CHILD_CBTAXPAYOR))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBTAXPAYOR,
                        doDealEntryMainSelectModel.FIELD_DFTAXPAYORID,
                        CHILD_CBTAXPAYOR_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbTaxPayorOptions);

            return child;
        }
        else if (name.equals(CHILD_BTPARTYSUMMARY))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPARTYSUMMARY,
                        CHILD_BTPARTYSUMMARY, CHILD_BTPARTYSUMMARY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFERENCETYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealGetReferenceInfoModel(),
                        CHILD_STREFERENCETYPE,
                        doDealGetReferenceInfoModel.FIELD_DFREFDEALTYPEDESCRIPTION,
                        CHILD_STREFERENCETYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFERENCEDEALNO))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealGetReferenceInfoModel(),
                        CHILD_STREFERENCEDEALNO,
                        doDealGetReferenceInfoModel.FIELD_DFREFERENCEDEALNUMBER,
                        CHILD_STREFERENCEDEALNO_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBMIINDICATOR))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(),
                        CHILD_CBMIINDICATOR,
                        doDealEntryMainSelectModel.FIELD_DFMIINDICATOR,
                        CHILD_CBMIINDICATOR_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbMIIndicatorOptions);

            return child;
        }
        else if (name.equals(CHILD_CBMITYPE))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBMITYPE,
                        doDealEntryMainSelectModel.FIELD_DFMITYPEID,
                        CHILD_CBMITYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");

            return child;
        }
        else if (name.equals(CHILD_CBMIINSURER))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBMIINSURER,
                        doDealEntryMainSelectModel.FIELD_DFMIINSURERID,
                        CHILD_CBMIINSURER_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbMIInsurerOptions);

            return child;
        }
        else if (name.equals(CHILD_CBMIPAYOR))
        {
            ComboBox child =
                new ComboBox(this, getdoDealEntryMainSelectModel(), CHILD_CBMIPAYOR,
                        doDealEntryMainSelectModel.FIELD_DFMIPAYORID,
                        CHILD_CBMIPAYOR_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbMIPayorOptions);

            return child;
        }
        else if (name.equals(CHILD_TXMIPREMIUM))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(), CHILD_TXMIPREMIUM,
                        doDealEntryMainSelectModel.FIELD_DFMIPREMIUM,
                        CHILD_TXMIPREMIUM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_RBMIUPFRONT))
        {
            RadioButtonGroup child =
                new RadioButtonGroup(this, getdoDealEntryMainSelectModel(),
                        CHILD_RBMIUPFRONT,
                        doDealEntryMainSelectModel.FIELD_DFMIUPFRONTID,
                        CHILD_RBMIUPFRONT_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbMIUpfrontOptions);

            return child;
        }
        else if (name.equals(CHILD_RBMIRUINTERVENTION))
        {
            RadioButtonGroup child =
                new RadioButtonGroup(this, getdoDealEntryMainSelectModel(),
                        CHILD_RBMIRUINTERVENTION,
                        doDealEntryMainSelectModel.FIELD_DFMIRUINTERVENTION,
                        CHILD_RBMIRUINTERVENTION_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbMIRUInterventionOptions);

            return child;
        }
        else if (name.equals(CHILD_STMIPREQCERTNUM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STMIPREQCERTNUM,
                        doDealEntryMainSelectModel.FIELD_DFPREQUALIFICATIONMICERTNUM,
                        CHILD_STMIPREQCERTNUM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTDEALSUBMIT))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTDEALSUBMIT,
                        CHILD_BTDEALSUBMIT, CHILD_BTDEALSUBMIT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTDEALCANCEL))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTDEALCANCEL,
                        CHILD_BTDEALCANCEL, CHILD_BTDEALCANCEL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMGENERATE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMGENERATE,
                        CHILD_STPMGENERATE, CHILD_STPMGENERATE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMHASTITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMHASTITLE,
                        CHILD_STPMHASTITLE, CHILD_STPMHASTITLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMHASINFO))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMHASINFO,
                        CHILD_STPMHASINFO, CHILD_STPMHASINFO_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMHASTABLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMHASTABLE,
                        CHILD_STPMHASTABLE, CHILD_STPMHASTABLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMHASOK))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMHASOK,
                        CHILD_STPMHASOK, CHILD_STPMHASOK_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMTITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMTITLE,
                        CHILD_STPMTITLE, CHILD_STPMTITLE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMINFOMSG))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMINFOMSG,
                        CHILD_STPMINFOMSG, CHILD_STPMINFOMSG_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMONOK))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMONOK,
                        CHILD_STPMONOK, CHILD_STPMONOK_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMMSGS))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMMSGS,
                        CHILD_STPMMSGS, CHILD_STPMMSGS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMMSGTYPES))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMMSGTYPES,
                        CHILD_STPMMSGTYPES, CHILD_STPMMSGTYPES_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMGENERATE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMGENERATE,
                        CHILD_STAMGENERATE, CHILD_STAMGENERATE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMHASTITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMHASTITLE,
                        CHILD_STAMHASTITLE, CHILD_STAMHASTITLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMHASINFO))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMHASINFO,
                        CHILD_STAMHASINFO, CHILD_STAMHASINFO_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMHASTABLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMHASTABLE,
                        CHILD_STAMHASTABLE, CHILD_STAMHASTABLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMTITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMTITLE,
                        CHILD_STAMTITLE, CHILD_STAMTITLE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STAMINFOMSG))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMINFOMSG,
                        CHILD_STAMINFOMSG, CHILD_STAMINFOMSG_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMMSGS))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMMSGS,
                        CHILD_STAMMSGS, CHILD_STAMMSGS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STAMMSGTYPES))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMMSGTYPES,
                        CHILD_STAMMSGTYPES, CHILD_STAMMSGTYPES_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMDIALOGMSG))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMDIALOGMSG,
                        CHILD_STAMDIALOGMSG,
                        CHILD_STAMDIALOGMSG_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STAMBUTTONSHTML))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMBUTTONSHTML,
                        CHILD_STAMBUTTONSHTML,
                        CHILD_STAMBUTTONSHTML_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTARGETDOWNPAYMENT))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STTARGETDOWNPAYMENT,
                        CHILD_STTARGETDOWNPAYMENT,
                        CHILD_STTARGETDOWNPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTARGETESCROW))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STTARGETESCROW,
                        CHILD_STTARGETESCROW,
                        CHILD_STTARGETESCROW_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXTOTALDOWN))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(), CHILD_TXTOTALDOWN,
                        doDealEntryMainSelectModel.FIELD_DFDOWNPAYMENTAMOUNT,
                        CHILD_TXTOTALDOWN_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXTOTALESCROW))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXTOTALESCROW,
                        doDealEntryMainSelectModel.FIELD_DFTOTALESCROWPAYMENT,
                        CHILD_TXTOTALESCROW_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TBPAYMENTTERMDESCRIPTION))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TBPAYMENTTERMDESCRIPTION,
                        CHILD_TBPAYMENTTERMDESCRIPTION,
                        CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TBRATECODE))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TBRATECODE,
                        CHILD_TBRATECODE, CHILD_TBRATECODE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STRATETIMESTAMP))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STRATETIMESTAMP,
                        CHILD_STRATETIMESTAMP,
                        CHILD_STRATETIMESTAMP_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDLONGPOSTEDDATE))
        {
            HiddenField child =
                new HiddenField(this, getDefaultModel(), CHILD_HDLONGPOSTEDDATE,
                        CHILD_HDLONGPOSTEDDATE,
                        CHILD_HDLONGPOSTEDDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STLENDERPRODUCTID))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STLENDERPRODUCTID,
                        CHILD_STLENDERPRODUCTID,
                        CHILD_STLENDERPRODUCTID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STLENDERPROFILEID))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STLENDERPROFILEID,
                        CHILD_STLENDERPROFILEID,
                        CHILD_STLENDERPROFILEID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDEDSSSTART))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STINCLUDEDSSSTART,
                        CHILD_STINCLUDEDSSSTART,
                        CHILD_STINCLUDEDSSSTART_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDEDSSEND))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STINCLUDEDSSEND,
                        CHILD_STINCLUDEDSSEND,
                        CHILD_STINCLUDEDSSEND_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALID))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALID,
                        doDealSummarySnapShotModel.FIELD_DFDEALID,
                        CHILD_STDEALID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBORRFIRSTNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STBORRFIRSTNAME,
                        CHILD_STBORRFIRSTNAME,
                        CHILD_STBORRFIRSTNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALSTATUS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALSTATUS,
                        doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
                        CHILD_STDEALSTATUS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALSTATUSDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALSTATUSDATE,
                        doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
                        CHILD_STDEALSTATUSDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STLOB))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STLOB,
                        doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
                        CHILD_STLOB_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALTYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALTYPE,
                        doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
                        CHILD_STDEALTYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALPURPOSE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALPURPOSE,
                        doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
                        CHILD_STDEALPURPOSE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALLOANAMOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STTOTALLOANAMOUNT,
                        doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
                        CHILD_STTOTALLOANAMOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMTTERM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STPMTTERM,
                        doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
                        CHILD_STPMTTERM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STESTCLOSINGDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STESTCLOSINGDATE,
                        doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
                        CHILD_STESTCLOSINGDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STSOURCE,
                        doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
                        CHILD_STSOURCE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCEFIRM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STSOURCEFIRM,
                        doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
                        CHILD_STSOURCEFIRM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPURCHASEPRICE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STPURCHASEPRICE,
                        doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
                        CHILD_STPURCHASEPRICE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSPECIALFEATURE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STSPECIALFEATURE,
                        doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
                        CHILD_STSPECIALFEATURE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTOK))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTOK, CHILD_BTOK,
                        CHILD_BTOK_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPAGETITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPAGETITLE,
                        CHILD_STPAGETITLE, CHILD_STPAGETITLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_BTRESSURECT))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTRESSURECT,
                        CHILD_BTRESSURECT, CHILD_BTRESSURECT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTPREAPPROVALFIRM))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPREAPPROVALFIRM,
                        CHILD_BTPREAPPROVALFIRM,
                        CHILD_BTPREAPPROVALFIRM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STAPPDATEFORSERVLET))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAPPDATEFORSERVLET,
                        CHILD_STAPPDATEFORSERVLET,
                        CHILD_STAPPDATEFORSERVLET_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMISTATUS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealMIStatusModel(), CHILD_STMISTATUS,
                        doDealMIStatusModel.FIELD_DFMISTATUSDESCRIPTION,
                        CHILD_STMISTATUS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXMIEXISTINGPOLICY))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXMIEXISTINGPOLICY,
                        doDealEntryMainSelectModel.FIELD_DFEXISTINGPOLICYNUMBER,
                        CHILD_TXMIEXISTINGPOLICY_RESET_VALUE, null);

            return child;
        }
        else
            // Changed to TextBox in order to display the Policy dynamically based on the MI-Insurer
            // -- by Billy 16July2003
            if (name.equals(CHILD_TXMICERTIFICATE))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXMICERTIFICATE,
                            doDealEntryMainSelectModel.FIELD_DFMICERTIFICATENO,
                            CHILD_TXMICERTIFICATE_RESET_VALUE, null);

                return child;
            }

        //======================================================================================
            else if (name.equals(CHILD_STINCLUDECOMMITMENTSTART))
            {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(),
                            CHILD_STINCLUDECOMMITMENTSTART,
                            CHILD_STINCLUDECOMMITMENTSTART,
                            CHILD_STINCLUDECOMMITMENTSTART_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_STINCLUDECOMMITMENTEND))
            {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(),
                            CHILD_STINCLUDECOMMITMENTEND,
                            CHILD_STINCLUDECOMMITMENTEND,
                            CHILD_STINCLUDECOMMITMENTEND_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_STSOURCEPHONENOEXTENSION))
            {
                StaticTextField child =
                    new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                            CHILD_STSOURCEPHONENOEXTENSION,
                            doDealEntrySourceInfoModel.FIELD_DFCONTACTPHONEEXTENSION,
                            CHILD_STSOURCEPHONENOEXTENSION_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_STRATELOCK))
            {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(), CHILD_STRATELOCK,
                            CHILD_STRATELOCK, CHILD_STRATELOCK_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_STDEALENTRY))
            {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(), CHILD_STDEALENTRY,
                            CHILD_STDEALENTRY, CHILD_STDEALENTRY_RESET_VALUE,
                            null);

                return child;
            }
            else if (name.equals(CHILD_STDEFAULTPRODUCTID))
            {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(), CHILD_STDEFAULTPRODUCTID,
                            CHILD_STDEFAULTPRODUCTID,
                            CHILD_STDEFAULTPRODUCTID_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_STVIEWONLYTAG))
            {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(), CHILD_STVIEWONLYTAG,
                            CHILD_STVIEWONLYTAG,
                            CHILD_STVIEWONLYTAG_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_RBPROGRESSADVANCE))
            {
                RadioButtonGroup child =
                    new RadioButtonGroup(this, getdoDealEntryMainSelectModel(),
                            CHILD_RBPROGRESSADVANCE,
                            doDealEntryMainSelectModel.FIELD_DFPROGRESSADVANCE,
                            CHILD_RBPROGRESSADVANCE_RESET_VALUE, null);
                child.setLabelForNoneSelected("");
                child.setOptions(rbProgressAdvanceOptions);

                return child;
            }
            else if (name.equals(CHILD_TXNEXTADVANCEAMOUNT))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXNEXTADVANCEAMOUNT,
                            doDealEntryMainSelectModel.FIELD_DFNEXTADVANCEAMOUNT,
                            CHILD_TXNEXTADVANCEAMOUNT_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXADVANCETODATEAMT))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXADVANCETODATEAMT,
                            doDealEntryMainSelectModel.FIELD_DFADVANCETODATEAMOUNT,
                            CHILD_TXADVANCETODATEAMT_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXADVANCENUMBER))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXADVANCENUMBER,
                            doDealEntryMainSelectModel.FIELD_DFADVANCENUMBER,
                            CHILD_TXADVANCENUMBER_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_CBREFIORIGPURCHASEDATEMONTH))
            {
                ComboBox child =
                    new ComboBox(this, getDefaultModel(),
                            CHILD_CBREFIORIGPURCHASEDATEMONTH,
                            CHILD_CBREFIORIGPURCHASEDATEMONTH,
                            CHILD_CBREFIORIGPURCHASEDATEMONTH_RESET_VALUE, null);
                child.setLabelForNoneSelected("");
                child.setOptions(cbRefiOrigPurchaseDateMonthOptions);

                return child;
            }
            else if (name.equals(CHILD_TXREFIORIGPURCHASEDATEYEAR))
            {
                TextField child =
                    new TextField(this, getDefaultModel(),
                            CHILD_TXREFIORIGPURCHASEDATEYEAR,
                            CHILD_TXREFIORIGPURCHASEDATEYEAR,
                            CHILD_TXREFIORIGPURCHASEDATEYEAR_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXREFIORIGPURCHASEDATEDAY))
            {
                TextField child =
                    new TextField(this, getDefaultModel(), CHILD_TXREFIORIGPURCHASEDATEDAY,
                            CHILD_TXREFIORIGPURCHASEDATEDAY,
                            CHILD_TXREFIORIGPURCHASEDATEDAY_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXREFIPURPOSE))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXREFIPURPOSE,
                            doDealEntryMainSelectModel.FIELD_DFREFIPURPOSE,
                            CHILD_TXREFIPURPOSE_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXREFIMORTGAGEHOLDER))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXREFIMORTGAGEHOLDER,
                            doDealEntryMainSelectModel.FIELD_DFREFICURMORTGAGEHOLDER,
                            CHILD_TXREFIMORTGAGEHOLDER_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_RBBLENDEDAMORTIZATION))
            {
                RadioButtonGroup child =
                    new RadioButtonGroup(this, getdoDealEntryMainSelectModel(),
                            CHILD_RBBLENDEDAMORTIZATION,
                            doDealEntryMainSelectModel.FIELD_DFREFIBLENDEDAMORTIZATION,
                            CHILD_RBBLENDEDAMORTIZATION_RESET_VALUE, null);
                child.setLabelForNoneSelected("");
                child.setOptions(rbBlendedAmortizationOptions);

                return child;
            }
            else if (name.equals(CHILD_TXREFIORIGPURCHASEPRICE))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXREFIORIGPURCHASEPRICE,
                            doDealEntryMainSelectModel.FIELD_DFREFIORIGPURCHASEPRICE,
                            CHILD_TXREFIORIGPURCHASEPRICE_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXREFIIMPROVEMENTVALUE))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXREFIIMPROVEMENTVALUE,
                            doDealEntryMainSelectModel.FIELD_DFREFIIMPORVEMENTAMOUNT,
                            CHILD_TXREFIIMPROVEMENTVALUE_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXREFIORIGMTGAMOUNT))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXREFIORIGMTGAMOUNT,
                            doDealEntryMainSelectModel.FIELD_DFREFIORIGMTGAMOUNT,
                            CHILD_TXREFIORIGMTGAMOUNT_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXREFIOUTSTANDINGMTGAMOUNT))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXREFIOUTSTANDINGMTGAMOUNT,
                            doDealEntryMainSelectModel.FIELD_DFEXISTINGLOANAMOUNT,
                            CHILD_TXREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_TXREFIIMPROVEMENTSDESC))
            {
                TextField child =
                    new TextField(this, getdoDealEntryMainSelectModel(),
                            CHILD_TXREFIIMPROVEMENTSDESC,
                            doDealEntryMainSelectModel.FIELD_DFREFIIMPROVEMENTSDESC,
                            CHILD_TXREFIIMPROVEMENTSDESC_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_STVALSDATA))
            {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(), CHILD_STVALSDATA,
                            CHILD_STVALSDATA, CHILD_STVALSDATA_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_BTRECALCULATE))
            {
                Button child =
                    new Button(this, getDefaultModel(), CHILD_BTRECALCULATE,
                            CHILD_BTRECALCULATE, CHILD_BTRECALCULATE_RESET_VALUE, null);

                return child;
            }

        //// Hidden button to trigger the ActiveMessage 'fake' submit button
        //// via the new BX ActMessageCommand class
            else if (name.equals(CHILD_BTACTMSG))
            {
                Button child =
                    new Button(this, getDefaultModel(), CHILD_BTACTMSG, CHILD_BTACTMSG,
                            CHILD_BTACTMSG_RESET_VALUE,
                            new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));

                return child;
            }

        //--Release2.1--//
        //// New link to toggle language. When touched this link should reload the
        //// page in opposite language (french versus english) and set this new session
        //// language id throughout the all modulus.
        else if (name.equals(CHILD_TOGGLELANGUAGEHREF))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_TOGGLELANGUAGEHREF,
                        CHILD_TOGGLELANGUAGEHREF,
                        CHILD_TOGGLELANGUAGEHREF_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_PAYMENTTERMDESC))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_PAYMENTTERMDESC,
                        doDealEntryMainSelectModel.FIELD_DFPAYMENTTERMDESC,
                        CHILD_PAYMENTTERMDESC_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STRATEINVENTORYID))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STRATEINVENTORYID,
                        CHILD_STRATEINVENTORYID,
                        CHILD_STRATEINVENTORYID_RESET_VALUE, null);

            return child;
        }

        //--Release2.1--//
        //// New HiddenField to get PaymentTermId from JavaScript Applet companion functions
        //// and propagate it to the database on submit.
        else if (name.equals(CHILD_HDPAYMENTTERMID))
        {
            HiddenField child =
                new HiddenField(this, getDefaultModel(), CHILD_HDPAYMENTTERMID,
                        CHILD_HDPAYMENTTERMID,
                        CHILD_HDPAYMENTTERMID_RESET_VALUE, null);

            return child;
        }

        //===============================================================
        // New fields to handle MIPolicyNum problem :
        //  the number lost if user cancel MI and re-send again later.
        // -- Modified by Billy 16July2003
        else if (name.equals(CHILD_HDMIPOLICYNOCMHC))
        {
            HiddenField child =
                new HiddenField(this, getdoDealEntryMainSelectModel(),
                        CHILD_HDMIPOLICYNOCMHC,
                        doDealEntryMainSelectModel.FIELD_DFMIPOLICYNUMBERCMHC,
                        CHILD_HDMIPOLICYNOCMHC_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDMIPOLICYNOGE))
        {
            HiddenField child =
                new HiddenField(this, getdoDealEntryMainSelectModel(),
                        CHILD_HDMIPOLICYNOGE,
                        doDealEntryMainSelectModel.FIELD_DFMIPOLICYNUMBERGE,
                        CHILD_HDMIPOLICYNOGE_RESET_VALUE, null);

            return child;
        }

        //================================================================
        //-- FXLink Phase II --//
        //--> New Field for Market Type Indicator
        //--> By Billy 18Nov2003
        else if (name.equals(CHILD_STMARKETTYPELABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STMARKETTYPELABEL,
                        CHILD_STMARKETTYPELABEL,
                        CHILD_STMARKETTYPELABEL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMARKETTYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STMARKETTYPE,
                        doDealEntryMainSelectModel.FIELD_DFMCCMARKETTYPE,
                        CHILD_STMARKETTYPE_RESET_VALUE, null);

            return child;
        }

        //--DJ_PT_CR--start//
        else if (name.equals(CHILD_STBRANCHTRANSITLABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STBRANCHTRANSITLABEL,
                        CHILD_STBRANCHTRANSITLABEL,
                        CHILD_STBRANCHTRANSITLABEL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPARTYNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getdoPartySummaryModel(), CHILD_STPARTYNAME,
                        doPartySummaryModel.FIELD_DFPARTYNAME,
                        CHILD_STPARTYNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRANCHTRANSITNUM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoPartySummaryModel(),
                        CHILD_STBRANCHTRANSITNUM,
                        doPartySummaryModel.FIELD_DFTRANSITNUMBER,
                        CHILD_STBRANCHTRANSITNUM, null);

            return child;
        }
        else if (name.equals(CHILD_STADDRESSLINE1))
        {
            StaticTextField child =
                new StaticTextField(this, getdoPartySummaryModel(),
                        CHILD_STADDRESSLINE1,
                        doPartySummaryModel.FIELD_DFADDRESSLINE1,
                        CHILD_STADDRESSLINE1, null);

            return child;
        }
        else if (name.equals(CHILD_STCITY))
        {
            StaticTextField child =
                new StaticTextField(this, getdoPartySummaryModel(), CHILD_STCITY,
                        doPartySummaryModel.FIELD_DFCITY, CHILD_STCITY, null);

            return child;
        }

        //--DJ_PT_CR--end//
        //--DJ_LDI_CR--start--//
        else if (name.equals(CHILD_BTLDINSURANCEDETAILS))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTLDINSURANCEDETAILS,
                        CHILD_BTLDINSURANCEDETAILS,
                        CHILD_BTLDINSURANCEDETAILS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDELIFEDISLABELSSTART))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDELIFEDISLABELSSTART,
                        CHILD_STINCLUDELIFEDISLABELSSTART,
                        CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDELIFEDISLABELSEND))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDELIFEDISLABELSEND,
                        CHILD_STINCLUDELIFEDISLABELSEND,
                        CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STGDSTDSDETAILSLABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STGDSTDSDETAILSLABEL,
                        CHILD_STGDSTDSDETAILSLABEL,
                        CHILD_STGDSTDSDETAILSLABEL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMNTINCLUDINGLIFEDISABILITY))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_STPMNTINCLUDINGLIFEDISABILITY,
                        doDealEntryMainSelectModel.FIELD_DFPMNTPLUSLIFEDISABILITY,
                        CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE,
                        null);

            return child;
        }

        //--DJ_LDI_CR--end--//
        //--FX_LINK--start--//
        else if (name.equals(CHILD_BTSOURCEOFBUSINESSDETAILS))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTSOURCEOFBUSINESSDETAILS,
                        CHILD_BTSOURCEOFBUSINESSDETAILS,
                        CHILD_BTSOURCEOFBUSINESSDETAILS_RESET_VALUE, null);

            return child;
        }

        //--FX_LINK--end--//
        //--DJ_CR010--start//
        else if (name.equals(CHILD_TXCOMMISIONCODE))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXCOMMISIONCODE,
                        doDealEntryMainSelectModel.FIELD_DFCOMMISIONCODE,
                        CHILD_TXCOMMISIONCODE_RESET_VALUE, null);

            return child;
        }

        //--DJ_CR010--end//
        //--DJ_CR203.1--start//
        else if (name.equals(CHILD_TXPROPRIETAIREPLUSLOC))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXPROPRIETAIREPLUSLOC,
                        doDealEntryMainSelectModel.FIELD_DFPROPRIETAIREPLUSLOC,
                        CHILD_TXPROPRIETAIREPLUSLOC_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_RBMULTIPROJECT))
        {
            RadioButtonGroup child =
                new RadioButtonGroup(this, getdoDealEntryMainSelectModel(),
                        CHILD_RBMULTIPROJECT,
                        doDealEntryMainSelectModel.FIELD_DFMULTIPROJECT,
                        CHILD_RBMULTIPROJECT_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbMultiProjectOptions);

            return child;
        }
        else if (name.equals(CHILD_RBPROPRIETAIREPLUS))
        {
            RadioButtonGroup child =
                new RadioButtonGroup(this, getdoDealEntryMainSelectModel(),
                        CHILD_RBPROPRIETAIREPLUS,
                        doDealEntryMainSelectModel.FIELD_DFPROPRIETAIREPLUS,
                        CHILD_RBPROPRIETAIREPLUS_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbProprietairePlusOptions);

            return child;
        }

        //--DJ_CR203.1--end//
        //--DJ_CR134--start--27May2004--//
        else if (name.equals(CHILD_STHOMEBASEPRODUCTRATEPMNT))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STHOMEBASEPRODUCTRATEPMNT,
                        CHILD_STHOMEBASEPRODUCTRATEPMNT,
                        CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE, null);

            return child;
        }

        //--DJ_CR134--end--//
        //--DisableProductRateTicket#570--10Aug2004--start--//
        else if (name.equals(CHILD_STDEALRATESTATUSFORSERVLET))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STDEALRATESTATUSFORSERVLET,
                        CHILD_STDEALRATESTATUSFORSERVLET,
                        CHILD_STDEALRATESTATUSFORSERVLET_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STRATEDISDATEFORSERVLET))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STRATEDISDATEFORSERVLET,
                        CHILD_STRATEDISDATEFORSERVLET,
                        CHILD_STRATEDISDATEFORSERVLET, null);

            return child;
        }
        else if (name.equals(CHILD_STISPAGEEDITABLEFORSERVLET))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STISPAGEEDITABLEFORSERVLET,
                        CHILD_STISPAGEEDITABLEFORSERVLET,
                        CHILD_STISPAGEEDITABLEFORSERVLET, null);

            return child;
        }
        //-- ========== DJ#725 Begins ========== --//
        //-- By Neil at Nov/30/2004
        else if (name.equals(CHILD_STCONTACTEMAILADDRESS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STCONTACTEMAILADDRESS,
                        doDealEntrySourceInfoModel.FIELD_DFCONTACTEMAILADDRESS,
                        CHILD_STCONTACTEMAILADDRESS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPSDESCRIPTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STPSDESCRIPTION,
                        doDealEntrySourceInfoModel.FIELD_DFPSDESCRIPTION,
                        CHILD_STPSDESCRIPTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSYSTEMTYPEDESCRIPTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealEntrySourceInfoModel(),
                        CHILD_STSYSTEMTYPEDESCRIPTION,
                        doDealEntrySourceInfoModel.FIELD_DFSYSTEMTYPEDESCRIPTION,
                        CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE, null);

            return child;
        }
        //-- ========== DJ#725 Ends ========== --//

        //-- ========== SCR#859 begins ========== --//
        //-- by Neil on Feb 15, 2005
        else if (name.equals(CHILD_STSERVICINGMORTGAGENUMBER)) {
            StaticTextField child = new StaticTextField(this, getdoDealSummarySnapShotModel(),
                    CHILD_STSERVICINGMORTGAGENUMBER,
                    doDealSummarySnapShotModel.FIELD_DFSERVICINGMORTGAGENUMBER,
                    CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE,
                    null);
            return child;
        } else if (name.equals(CHILD_STCCAPS)) {
            StaticTextField child = new StaticTextField(this, getDefaultModel(), CHILD_STCCAPS, CHILD_STCCAPS, CHILD_STCCAPS_RESET_VALUE, null);
            return child;
        }
        //-- ========== SCR#859 ends ========== --//

        //***** Change by NBC/PP Implementation Team - Version 1.3 - Start *****//
        else if (name.equals(CHILD_TXRATEGUARANTEEPERIOD))
        {
            TextField child = new TextField (this, getdoDealEntryMainSelectModel(), 
                    CHILD_TXRATEGUARANTEEPERIOD,
                    doDealEntryMainSelectModel.FIELD_DFRATEGUARANTEEPERIOD,
                    CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE, null);
            return child;

        }		
        else if (name.equals(CHILD_HDCHANGEDESTIMATEDCLOSINGDATE))
        {
            HiddenField child = new HiddenField (this, getDefaultModel(), 
                    CHILD_HDCHANGEDESTIMATEDCLOSINGDATE,
                    CHILD_HDCHANGEDESTIMATEDCLOSINGDATE,
                    CHILD_HDCHANGEDESTIMATEDCLOSINGDATE_RESET_VALUE, null);
            return child;

        }	    
        //***** Change by NBC/PP Implementation Team - Version 1.3 - End *****//

        //--Release3.1--begins
        //--by Hiro Apr 13, 2006
        else if (name.equals(CHILD_CBPRODUCTTYPE))
        {
            ComboBox child = new ComboBox(this, getdoDealEntryMainSelectModel(),
                    CHILD_CBPRODUCTTYPE, doDealEntryMainSelectModel.FIELD_DFPRODUCTTYPEID,
                    CHILD_CBPRODUCTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbProductTypeOptions);

            return child;
        }
        else if (name.equals(CHILD_CBREFIPRODUCTTYPE))
        {
            ComboBox child = new ComboBox(this, getdoDealEntryMainSelectModel(),
                    CHILD_CBREFIPRODUCTTYPE, doDealEntryMainSelectModel.FIELD_DFREFIPRODUCTTYPEID,
                    CHILD_CBREFIPRODUCTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbRefiProductTypeOptions);

            return child;
        }
        else if (name.equals(CHILD_RBPROGRESSADVANCEINSPECTIONBY))
        {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoDealEntryMainSelectModel(), CHILD_RBPROGRESSADVANCEINSPECTIONBY,
                    doDealEntryMainSelectModel.FIELD_DFPROGRESSADVANCEINSPECTIONBY,
                    CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbProgressAdvanceInspectionByOptions);

            return child;
        } else if (name.equals(CHILD_RBSELFDIRECTEDRRSP))
        {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoDealEntryMainSelectModel(), CHILD_RBSELFDIRECTEDRRSP,
                    doDealEntryMainSelectModel.FIELD_DFSELFDIRECTEDRRSP,
                    CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbSelfDirectedRRSPOptions);

            return child;
        } else if (name.equals(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER))
        {
            TextField child = new TextField(this, getdoDealEntryMainSelectModel(),
                    CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER,
                    doDealEntryMainSelectModel.FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER,
                    CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CBLOCREPAYMENTTYPEID))
        {
            ComboBox child = new ComboBox(this, getdoDealEntryMainSelectModel(),
                    CHILD_CBLOCREPAYMENTTYPEID,
                    doDealEntryMainSelectModel.FIELD_DFLOCREPAYMENTTYPEID,
                    CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbLOCRepaymentTypeOptions);

            return child;
        }
        else if (name.equals(CHILD_RBREQUESTSTANDARDSERVICE))
        {
            RadioButtonGroup child = new RadioButtonGroup(this,
                    getdoDealEntryMainSelectModel(), CHILD_RBREQUESTSTANDARDSERVICE,
                    doDealEntryMainSelectModel.FIELD_DFREQUESTSTANDARDSERVICE,
                    CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbRequestStandardServiceOptions);

            return child;
        } else if (name.equals(CHILD_STLOCAMORTIZATIONMONTHS))
        {
            StaticTextField child = new StaticTextField(this, 
                    getdoDealEntryMainSelectModel(), CHILD_STLOCAMORTIZATIONMONTHS,
                    doDealEntryMainSelectModel.FIELD_DFLOCAMORTIZATIONMONTHS,
                    CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STLOCINTERESTONLYMATURITYDATE))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoDealEntryMainSelectModel(),
                    CHILD_STLOCINTERESTONLYMATURITYDATE,
                    doDealEntryMainSelectModel.FIELD_DFLOCINTERESTONLYMATURITYDATE,
                    CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE, null);

            return child;
        }
        // --Release3.1--ends

        //--DisableProductRateTicket#570--10Aug2004--end--//

//      ***** Change by NBC Impl. Team - Version 1.5 - Start *****//
        else if (name.equals(CHILD_TXCASHBACKINDOLLARS)) {

            TextField child = new TextField(this,
                    getdoDealSummarySnapShotModel(), CHILD_TXCASHBACKINDOLLARS,
                    doDealSummarySnapShotModel.FIELD_DFCASHBACKINDOLLARS,
                    CHILD_TXCASHBACKINDOLLARS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_TXCASHBACKINPERCENTAGE)) {

            TextField child = new TextField(this,
                    getdoDealSummarySnapShotModel(),
                    CHILD_TXCASHBACKINPERCENTAGE,
                    doDealSummarySnapShotModel.FIELD_DFCASHBACKINPERCENTAGE,
                    CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_CHCASHBACKOVERRIDEINDOLLARS)) {
            CheckBox child = new CheckBox(
                    this,
                    getdoDealSummarySnapShotModel(),
                    CHILD_CHCASHBACKOVERRIDEINDOLLARS,
                    doDealSummarySnapShotModel.FIELD_DFCASHBACKOVERRIDEINDOLLARS,
                    "Y", // Checkbox should be checked when the value in
                    // database is Y
                    "N", // Checkbox should be unchecked when the value in
                    // database is N
                    false, // 
                    null);

            return child;

        }

//      ***** Change by NBC Impl. Team - Version 1.5 - End*****//

        //  ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
        else if (name.equals(CHILD_CBAFFILIATIONPROGRAM)) {
            ComboBox child = new ComboBox(this, getdoDealEntryMainSelectModel(),
                    CHILD_CBAFFILIATIONPROGRAM,
                    doDealEntryMainSelectModel.FIELD_DFAFFILIATIONPROGRAMID,
                    CHILD_CBAFFILIATIONPROGRAM_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbAffiliationProgramOptions);

            return child;
        }
        //  ***** Change by NBC Impl. Team - Version 1.6 - End *****//  



        //***** Change by NBC/PP Implementation Team - GCD - Start *****//

        else if (name.equals(CHILD_TX_CREDIT_DECISION_STATUS)) {
            StaticTextField child =
                new StaticTextField(this, getdoAdjudicationResponseBNCModel(), CHILD_TX_CREDIT_DECISION_STATUS,
                        doAdjudicationResponseBNCModel.FIELD_DFREQUESTSTATUSDESC, CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE, null);
            return child;
        }

        else if (name.equals(CHILD_TX_CREDIT_DECISION)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(),CHILD_TX_CREDIT_DECISION,
                        doAdjudicationResponseBNCModel.FIELD_DFADJUDICATIONDESC,CHILD_TX_CREDIT_DECISION_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(), CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALCREDITBUREAUSCORE,CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_RISK_RATING)) {
            StaticTextField child =
                new StaticTextField(this, getdoAdjudicationResponseBNCModel(),CHILD_TX_GLOBAL_RISK_RATING,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALRISKRATING,CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_INTERNAL_SCORE)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(), CHILD_TX_GLOBAL_INTERNAL_SCORE,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALINTERNALSCORE, CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_MISTATUS)) {
            StaticTextField child =
                new StaticTextField(this, getdoDealMIStatusModel(), CHILD_TX_MISTATUS,
                        doDealMIStatusModel.FIELD_DFMISTATUSDESCRIPTION,CHILD_TX_MISTATUS_RESET_VALUE, null);
            return child;
        }

        //GCD Summary Data Retrieval End

        else if (name.equals(CHILD_BT_CREDIT_DECISION_PG)) {
            Button child =
                new Button (this,getDefaultModel(), CHILD_BT_CREDIT_DECISION_PG,
                        CHILD_BT_CREDIT_DECISION_PG,CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_ST_INCLUDE_GCDSUM_START)) {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_ST_INCLUDE_GCDSUM_START,
                        CHILD_ST_INCLUDE_GCDSUM_START,CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_ST_INCLUDE_GCDSUM_END)) {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_ST_INCLUDE_GCDSUM_END,
                        CHILD_ST_INCLUDE_GCDSUM_END,CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE, null);
            return child;
        }

        //***** Change by NBC/PP Implementation Team - GCD - End *****//

        //  ***** Change by NBC/PP Implementation Team - SCR #1679 - Start *****//
        else if (name.equals(CHILD_HDFORCEPROGRESSADVANCE)) {
            HiddenField child =
                new HiddenField(this, getDefaultModel(), CHILD_HDFORCEPROGRESSADVANCE,
                        CHILD_HDFORCEPROGRESSADVANCE,CHILD_HDFORCEPROGRESSADVANCE_RESET_VALUE, null);
            return child;
        }    
        //  ***** Change by NBC/PP Implementation Team - SCR #1679 - End *****//
        // FFATE
        else if (name.equals(CHILD_CBFINANCINGWAIVERDATEMONTH))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBFINANCINGWAIVERDATEMONTH,
                        CHILD_CBFINANCINGWAIVERDATEMONTH,
                        CHILD_CBFINANCINGWAIVERDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbFinancingWaiverDateMonthOptions);

            return child;
        }
        else if (name.equals(CHILD_TXFINANCINGWAIVERDATEDAY))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXFINANCINGWAIVERDATEDAY,
                        CHILD_TXFINANCINGWAIVERDATEDAY,
                        CHILD_TXFINANCINGWAIVERDATEDAY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXFINANCINGWAIVERDATEYEAR))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXFINANCINGWAIVERDATEYEAR,
                        CHILD_TXFINANCINGWAIVERDATEYEAR,
                        CHILD_TXFINANCINGWAIVERDATEYEAR_RESET_VALUE, null);

            return child;
        }
        // FFATE
        
        //CR03
        else if (name.equals(CHILD_RBAUTOCALCOMMITDATE))
        {
            RadioButtonGroup child =
                new RadioButtonGroup(this, getdoDealEntryMainSelectModel(),
                        CHILD_RBAUTOCALCOMMITDATE,
                        doDealEntryMainSelectModel.FIELD_DFAUTOCALCOMMITDATE,
                        CHILD_RBAUTOCALCOMMITDATE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(rbAutoCalCommitDateOptions);

            return child;
        }


        else if (name.equals(CHILD_CBCOMMEXPIRATIONDATEMONTH))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBCOMMEXPIRATIONDATEMONTH,
                        CHILD_CBCOMMEXPIRATIONDATEMONTH,
                        CHILD_CBCOMMEXPIRATIONDATEMONTH_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbCommExpirationDateMonthOptions);

            return child;
        }
        else if (name.equals(CHILD_TXCOMMEXPIRATIONDATEDAY))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXCOMMEXPIRATIONDATEDAY,
                        CHILD_TXCOMMEXPIRATIONDATEDAY,
                        CHILD_TXCOMMEXPIRATIONDATEDAY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXCOMMEXPIRATIONDATEYEAR))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TXCOMMEXPIRATIONDATEYEAR,
                        CHILD_TXCOMMEXPIRATIONDATEYEAR,
                        CHILD_TXCOMMEXPIRATIONDATEYEAR_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDCOMMITMENTEXPIRYDATE)) {
            HiddenField child = new HiddenField(this,
                    getdoDealEntryMainSelectModel(), CHILD_HDCOMMITMENTEXPIRYDATE,
                    doDealEntryMainSelectModel.FIELD_DFCOMMITMENTEXPIRATIONDATE,
                    CHILD_HDCOMMITMENTEXPIRYDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDMOSPROPERTY)) {
            HiddenField child = new HiddenField(this,
                    getDefaultModel(), CHILD_HDMOSPROPERTY,
                    CHILD_HDMOSPROPERTY,
                    CHILD_HDMOSPROPERTY_RESET_VALUE, null);

            return child;
        }
        //CR03

        //PPI start
        else if (name.equals(CHILD_TXDEALPURPOSETYPEHIDDENSTART))
        {
            TextField child =
                new TextField(this, getDefaultModel(), 
                        CHILD_TXDEALPURPOSETYPEHIDDENSTART,
                        CHILD_TXDEALPURPOSETYPEHIDDENSTART,
                        CHILD_TXDEALPURPOSETYPEHIDDENSTART_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_TXIMPROVEDVALUE))
        {
            TextField child =
                new TextField(this, getDefaultModel(), 
                        CHILD_TXIMPROVEDVALUE,
                        CHILD_TXIMPROVEDVALUE,
                        CHILD_TXIMPROVEDVALUE_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_TXDEALPURPOSETYPEHIDDENEND))
        {
            TextField child =
                new TextField(this, getDefaultModel(), 
                        CHILD_TXDEALPURPOSETYPEHIDDENEND,
                        CHILD_TXDEALPURPOSETYPEHIDDENEND,
                        CHILD_TXDEALPURPOSETYPEHIDDENEND_RESET_VALUE, null);

            return child;
        }
        //PPI end

        /***************MCM Impl team changes starts - XS_2.7 *******************/

        else if (name.equals(CHILD_TXREFEXISTINGMTGNUMBER))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXREFEXISTINGMTGNUMBER,
                        doDealEntryMainSelectModel.FIELD_DFREFERENCEEXISITINGMTG,
                        CHILD_TXREFEXISTINGMTGNUMBER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXREFIADDITIONALINFORMATION))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                        CHILD_TXREFIADDITIONALINFORMATION,
                        doDealEntryMainSelectModel.FIELD_DFREFIADDITIONALINFORMATION,
                        CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE, null);

            return child;
        }

        /***************MCM Impl team changes ends - XS_2.7 *********************/



        /******************* MCM Impl team changes starts - XS 2.9 *******************/
        else if (name.equals(CHILD_STCOMPONENTINFOPAGELET))
        {
            // MCM TEAM XS 2.44 modified STARTS
            pgComponentInfoPageletViewBean child 
                = new pgComponentInfoPageletViewBean (this, CHILD_STCOMPONENTINFOPAGELET, handler);
            // MCM TEAM XS 2.44 modified ENDS
            return child;
        }
        /******************* MCM Impl team changes end - XS 2.9 *******************/

        /***************MCM Impl team changes starts - XS_2.13*******************/
        else 
            if (name.equals(CHILD_STQUALIFYINGDETAILSPAGELET))
            {
                pgQualifyingDetailsPageletView child = new pgQualifyingDetailsPageletView (this,CHILD_STQUALIFYINGDETAILSPAGELET,this.handler.cloneSS());
                return child;
            }
        /***************MCM Impl team changes ends - XS_2.13*******************/   
        //4.4 Submission Agent
        else if (name.equals(CHILD_REPEATEDSOB))
        {
            pgDealEntryRepeatedSOBTiledView child =
                new pgDealEntryRepeatedSOBTiledView(this,
                		CHILD_REPEATEDSOB);

            return child;
        }
        //      ***** Qualifying Rate *****   //
        else if (name.equals(CHILD_CBQUALIFYPRODUCTTYPE)) {
            ComboBox child = new ComboBox(this, getdoDealEntryMainSelectModel(),
                    CHILD_CBQUALIFYPRODUCTTYPE,
                    doDealEntryMainSelectModel.FIELD_DFOVERRIDEQUALPROD,
                    CHILD_CBQUALIFYPRODUCTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            return child;
        }
        else if (name.equals(CHILD_TXQUALIFYRATE)) {
            TextField child = new TextField(this, getdoDealEntryMainSelectModel(),
                    CHILD_TXQUALIFYRATE,
                    doDealEntryMainSelectModel.FIELD_DFQUALIFYRATE,
                    CHILD_TXQUALIFYRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CHQUALIFYRATEOVERRIDE)) {
            CheckBox child = new CheckBox(
                    this,
                    getdoDealEntryMainSelectModel(),
                    CHILD_CHQUALIFYRATEOVERRIDE,
                    // CHILD_CHCASHBACKOVERRIDEINDOLLARS,
                    doDealEntryMainSelectModel.FIELD_DFQUALIFYINGOVERRIDEFLAG,
                    "Y", // Checkbox should be checked when the value in
                    // database is Y
                    "N", // Checkbox should be unchecked when the value in
                    // database is N
                    false, // 
                    null);

            return child;
        }
        else if (name.equals(CHILD_CHQUALIFYRATEOVERRIDERATE)) {
            CheckBox child = new CheckBox(
                    this,
                    getdoDealEntryMainSelectModel(),
                    CHILD_CHQUALIFYRATEOVERRIDERATE,
                    doDealEntryMainSelectModel.FIELD_DFQUALIFYINGOVERRIDERATEFLAG,
                    "Y", // Checkbox should be checked when the value in
                    // database is Y
                    "N", // Checkbox should be unchecked when the value in
                    // database is N
                    false, // 
                    null);

            return child;
        }
        else if (name.equals(CHILD_BTRECALCULATEGDS)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTRECALCULATEGDS, CHILD_BTRECALCULATEGDS,
                    CHILD_BTRECALCULATEGDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STQUALIFYRATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealEntryMainSelectModel(), CHILD_STQUALIFYRATE,
                    doDealEntryMainSelectModel.FIELD_DFQUALIFYRATE,
                    CHILD_STQUALIFYRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STQUALIFYPRODUCTTYPE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoDealEntryMainSelectModel(), CHILD_STQUALIFYPRODUCTTYPE,
                    doDealEntryMainSelectModel.FIELD_DFOVERRIDEQUALPROD,
                    CHILD_STQUALIFYPRODUCTTYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STQUALIFYRATEHIDDEN)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STQUALIFYRATEHIDDEN,
                    CHILD_STQUALIFYRATEHIDDEN,
                    CHILD_STQUALIFYRATEHIDDEN_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STQUALIFYRATEEDIT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STQUALIFYRATEEDIT,
                    CHILD_STQUALIFYRATEEDIT,
                    CHILD_STQUALIFYRATEEDIT_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STHDQUALIFYRATE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHDQUALIFYRATE,
                    CHILD_STHDQUALIFYRATE,
                    CHILD_STHDQUALIFYRATE_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_STHDQUALIFYPRODUCTID)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STHDQUALIFYPRODUCTID,
                    CHILD_STHDQUALIFYPRODUCTID,
                    CHILD_STHDQUALIFYPRODUCTID_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_HDQUALIFYRATEDISABLED)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_HDQUALIFYRATEDISABLED,
                    CHILD_HDQUALIFYRATEDISABLED,
                    CHILD_HDQUALIFYRATEDISABLED_RESET_VALUE, null);
            return child;
        }
        // ***** Qualifying Rate *****//
        else if (name.equals(CHILD_HDDEALSTATUSID))
        {
            TextField child =
                new TextField(this, getdoDealEntryMainSelectModel(),
                		CHILD_HDDEALSTATUSID,
                		doDealEntryMainSelectModel.FIELD_DFDEALSTATUSID,
                        CHILD_HDDEALSTATUSID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTRECALCULATEGDS)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTRECALCULATEGDS, CHILD_BTRECALCULATEGDS,
                    CHILD_BTRECALCULATEGDS_RESET_VALUE, null);

            return child;
        }
        else
        {
            throw new IllegalArgumentException("Invalid child name [" + name + "]");
        }
    }

    /**
     * resetChildren
     *
     * @version 1.3 <br>
     * Date: 06/29/2006<br>
     * Author: NBC/PP Implementation Team <br>
     * Change: <br>
     * 	Added new fields txRateGuaranteePeriod & hdChangedEstimatedClosingDate<br>
     * Date: 11/21/2006<br>
     * Author: NBC/PP Implementation Team <br>
     * Change: <br>
     * 	Added new field hdForceProgressAdvance for SCR #1679<br>
     *
     *
     *  @Version 1.4 <br>
     *  Date: 06/09/2008
     *  Author: MCM Impl Team <br>
     *  Change Log:  <br>
     *  XS_2.7 -- 06/06/2008 -- Added necessary changes required for the new fields added in Refinance section<br>
     */
    public void resetChildren()
    {
        super.resetChildren();
        getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
        getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
        getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
        getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
        getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
        getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
        getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
        getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
        getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
        getHref1().setValue(CHILD_HREF1_RESET_VALUE);
        getHref2().setValue(CHILD_HREF2_RESET_VALUE);
        getHref3().setValue(CHILD_HREF3_RESET_VALUE);
        getHref4().setValue(CHILD_HREF4_RESET_VALUE);
        getHref5().setValue(CHILD_HREF5_RESET_VALUE);
        getHref6().setValue(CHILD_HREF6_RESET_VALUE);
        getHref7().setValue(CHILD_HREF7_RESET_VALUE);
        getHref8().setValue(CHILD_HREF8_RESET_VALUE);
        getHref9().setValue(CHILD_HREF9_RESET_VALUE);
        getHref10().setValue(CHILD_HREF10_RESET_VALUE);
        getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
        getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
        getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
        getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
        getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
        getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
        getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
        getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
        getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
        getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
        getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
        getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
        getHdDealId().setValue(CHILD_HDDEALID_RESET_VALUE);
        getHdDealCopyId().setValue(CHILD_HDDEALCOPYID_RESET_VALUE);
        getStDealNo().setValue(CHILD_STDEALNO_RESET_VALUE);
        getStCombinedGDS().setValue(CHILD_STCOMBINEDGDS_RESET_VALUE);
        getStCombined3YrGDS().setValue(CHILD_STCOMBINED3YRGDS_RESET_VALUE);
        getStCombinedBorrowerGDS().setValue(CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE);
        getStCombinedTDS().setValue(CHILD_STCOMBINEDTDS_RESET_VALUE);
        getStCombined3YrTDS().setValue(CHILD_STCOMBINED3YRTDS_RESET_VALUE);
        getStCombinedBorrowerTDS().setValue(CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE);
        getRepeatGDSTDSDetails().resetChildren();
        getRepeatApplicant().resetChildren();
        getBtAddApplicant().setValue(CHILD_BTADDAPPLICANT_RESET_VALUE);
        getBtDupApplicant().setValue(CHILD_BTDUPAPPLICANT_RESET_VALUE);
/*        getBtChangeSource().setValue(CHILD_BTCHANGESOURCE_RESET_VALUE);
        getStSourceFirmSourcePart().setValue(CHILD_STSOURCEFIRMSOURCEPART_RESET_VALUE);
        getStSourceFirstName().setValue(CHILD_STSOURCEFIRSTNAME_RESET_VALUE);
        getStSourceLastName().setValue(CHILD_STSOURCELASTNAME_RESET_VALUE);
        getStSourceAddress().setValue(CHILD_STSOURCEADDRESS_RESET_VALUE);
        getStSourcePhoneNo().setValue(CHILD_STSOURCEPHONENO_RESET_VALUE);
        getStSourceFaxNo().setValue(CHILD_STSOURCEFAXNO_RESET_VALUE); */
		//4.4 Submission Agent
        getRepeatedSOB().resetChildren();

        getTxReferenceSourceApp().setValue(CHILD_TXREFERENCESOURCEAPP_RESET_VALUE);
        getRepeatPropertyInformation().resetChildren();
        getBtAddProperty().setValue(CHILD_BTADDPROPERTY_RESET_VALUE);
        getCbDealPurpose().setValue(CHILD_CBDEALPURPOSE_RESET_VALUE);
        getCbDealType().setValue(CHILD_CBDEALTYPE_RESET_VALUE);
        // SEAN GECF document type drop down. reset value.
        getCbIVDocumentType().setValue(CHILD_CBIVDOCUMENTTYPE_RESET_VALUE);
        getStHideIVDocumentTypeStart().setValue(CHILD_STHIDEIVDOCUMENTTYPESTART_RESET_VALUE);
        getStHideIVDocumentTypeEnd().setValue(CHILD_STHIDEIVDOCUMENTTYPEEND_RESET_VALUE);
        // END GECF document type drop down.
        // SEAN DJ SPEC-Progress Advance Type July 22, 2005: Reset value.
        getCbProgressAdvanceType().setValue(CHILD_CBPROGRESSADVANCETYPE_RESET_VALUE);
        // SEAN DJ SPEC-PAT END
        getCbLOB().setValue(CHILD_CBLOB_RESET_VALUE);
        getCbLender().setValue(CHILD_CBLENDER_RESET_VALUE);
        getCbProduct().setValue(CHILD_CBPRODUCT_RESET_VALUE);
        getCbRateLocked().setValue(CHILD_CBRATELOCKED_RESET_VALUE);
        getCbChargeTerm().setValue(CHILD_CBCHARGETERM_RESET_VALUE);
        getCbPostedInterestRate().setValue(CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE);
        getTxDiscount().setValue(CHILD_TXDISCOUNT_RESET_VALUE);
        getTxPremium().setValue(CHILD_TXPREMIUM_RESET_VALUE);
        getTxBuydownRate().setValue(CHILD_TXBUYDOWNRATE_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getStTotalEscrowPayment().setValue(CHILD_STTOTALESCROWPAYMENT_RESET_VALUE);
        getTxRequestedLoanAmount().setValue(CHILD_TXREQUESTEDLOANAMOUNT_RESET_VALUE);
        getCbPayemntFrequencyTerm().setValue(CHILD_CBPAYEMNTFREQUENCYTERM_RESET_VALUE);
        getTxAmortizationYears().setValue(CHILD_TXAMORTIZATIONYEARS_RESET_VALUE);
        getTxAmortizationMonths().setValue(CHILD_TXAMORTIZATIONMONTHS_RESET_VALUE);
        getStEffectiveAmortizationYears().setValue(CHILD_STEFFECTIVEAMORTIZATIONYEARS_RESET_VALUE);
        getStEffectiveAmortizationMonths().setValue(CHILD_STEFFECTIVEAMORTIZATIONMONTHS_RESET_VALUE);
        getTxAdditionalPrincipalPayment().setValue(CHILD_TXADDITIONALPRINCIPALPAYMENT_RESET_VALUE);
        getTxPayTermYears().setValue(CHILD_TXPAYTERMYEARS_RESET_VALUE);
        getTxPayTermMonths().setValue(CHILD_TXPAYTERMMONTHS_RESET_VALUE);
        getStPIPayment().setValue(CHILD_STPIPAYMENT_RESET_VALUE);
        getStTotalPayment().setValue(CHILD_STTOTALPAYMENT_RESET_VALUE);
        getCbRepaymentType().setValue(CHILD_CBREPAYMENTTYPE_RESET_VALUE);
        /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
        getStDefaultRepaymentTypeId().setValue(CHILD_STDEFAULTREPAYMENTTYPEID_RESET_VALUE);
        /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
        getCbEstClosingDateMonths().setValue(CHILD_CBESTCLOSINGDATEMONTHS_RESET_VALUE);
        getTxEstClosingDateDay().setValue(CHILD_TXESTCLOSINGDATEDAY_RESET_VALUE);
        getTxEstClosingDateYear().setValue(CHILD_TXESTCLOSINGDATEYEAR_RESET_VALUE);
        getCbFirstPayDateMonth().setValue(CHILD_CBFIRSTPAYDATEMONTH_RESET_VALUE);
        getTxFirstPayDateDay().setValue(CHILD_TXFIRSTPAYDATEDAY_RESET_VALUE);
        getTxFirstPayDateYear().setValue(CHILD_TXFIRSTPAYDATEYEAR_RESET_VALUE);
        getStMaturityDate().setValue(CHILD_STMATURITYDATE_RESET_VALUE);
        getTxPreAppEstPurchasePrice().setValue(CHILD_TXPREAPPESTPURCHASEPRICE_RESET_VALUE);
        getCbSecondMortgageExist().setValue(CHILD_CBSECONDMORTGAGEEXIST_RESET_VALUE);
        getCbPrePaymentPenanlty().setValue(CHILD_CBPREPAYMENTPENANLTY_RESET_VALUE);
        getCbPrivilagePaymentOption().setValue(CHILD_CBPRIVILAGEPAYMENTOPTION_RESET_VALUE);
        getStIncludeFinancingProgramStart().setValue(CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE);
        getCbFinancingProgram().setValue(CHILD_CBFINANCINGPROGRAM_RESET_VALUE);
        getStIncludeFinancingProgramEnd().setValue(CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE);
        getCbSpecialFeature().setValue(CHILD_CBSPECIALFEATURE_RESET_VALUE);
        getCbCommExpectedDateMonth().setValue(CHILD_CBCOMMEXPECTEDDATEMONTH_RESET_VALUE);
        getTxCommExpectedDateDay().setValue(CHILD_TXCOMMEXPECTEDDATEDAY_RESET_VALUE);
        getTxCommExpectedDateYear().setValue(CHILD_TXCOMMEXPECTEDDATEYEAR_RESET_VALUE);
        getStCommitmentPeriod().setValue(CHILD_STCOMMITMENTPERIOD_RESET_VALUE);
        getStCommExpDate().setValue(CHILD_STCOMMEXPDATE_RESET_VALUE);
        getStCommAcceptanceDate().setValue(CHILD_STCOMMACCEPTANCEDATE_RESET_VALUE);
        getStBridgeLoanAmount().setValue(CHILD_STBRIDGELOANAMOUNT_RESET_VALUE);
        getTxAdvanceHold().setValue(CHILD_TXADVANCEHOLD_RESET_VALUE);
        getBtReviewBridge().setValue(CHILD_BTREVIEWBRIDGE_RESET_VALUE);
        getRepeatedDownPayment().resetChildren();
        getBtAddDownPayment().setValue(CHILD_BTADDDOWNPAYMENT_RESET_VALUE);
        getStDownPaymentRequired().setValue(CHILD_STDOWNPAYMENTREQUIRED_RESET_VALUE);
        getRepeatedEscrowDetails().resetChildren();
        getBtAddEscrow().setValue(CHILD_BTADDESCROW_RESET_VALUE);
        getCbBranch().setValue(CHILD_CBBRANCH_RESET_VALUE);
        getTxRefExisitingMTG().setValue(CHILD_TXREFEXISITINGMTG_RESET_VALUE);
        getCbCrossSell().setValue(CHILD_CBCROSSSELL_RESET_VALUE);
        getCbTaxPayor().setValue(CHILD_CBTAXPAYOR_RESET_VALUE);
        getBtPartySummary().setValue(CHILD_BTPARTYSUMMARY_RESET_VALUE);
        getStReferenceType().setValue(CHILD_STREFERENCETYPE_RESET_VALUE);
        getStReferenceDealNo().setValue(CHILD_STREFERENCEDEALNO_RESET_VALUE);
        getCbMIIndicator().setValue(CHILD_CBMIINDICATOR_RESET_VALUE);
        getCbMIType().setValue(CHILD_CBMITYPE_RESET_VALUE);
        getCbMIInsurer().setValue(CHILD_CBMIINSURER_RESET_VALUE);
        getCbMIPayor().setValue(CHILD_CBMIPAYOR_RESET_VALUE);
        getTxMIPremium().setValue(CHILD_TXMIPREMIUM_RESET_VALUE);
        getRbMIUpfront().setValue(CHILD_RBMIUPFRONT_RESET_VALUE);
        getRbMIRUIntervention().setValue(CHILD_RBMIRUINTERVENTION_RESET_VALUE);
        getStMIPreQCertNum().setValue(CHILD_STMIPREQCERTNUM_RESET_VALUE);
        getBtDealSubmit().setValue(CHILD_BTDEALSUBMIT_RESET_VALUE);
        getBtDealCancel().setValue(CHILD_BTDEALCANCEL_RESET_VALUE);
        getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
        getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
        getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
        getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
        getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
        getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
        getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
        getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
        getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
        getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
        getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
        getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
        getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
        getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
        getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
        getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
        getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
        getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
        getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
        getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
        getStTargetDownPayment().setValue(CHILD_STTARGETDOWNPAYMENT_RESET_VALUE);
        getStTargetEscrow().setValue(CHILD_STTARGETESCROW_RESET_VALUE);
        getTxTotalDown().setValue(CHILD_TXTOTALDOWN_RESET_VALUE);
        getTxTotalEscrow().setValue(CHILD_TXTOTALESCROW_RESET_VALUE);
        getTbPaymentTermDescription().setValue(CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE);
        getTbRateCode().setValue(CHILD_TBRATECODE_RESET_VALUE);
        getStRateTimeStamp().setValue(CHILD_STRATETIMESTAMP_RESET_VALUE);
        getHdLongPostedDate().setValue(CHILD_HDLONGPOSTEDDATE_RESET_VALUE);
        getStLenderProductId().setValue(CHILD_STLENDERPRODUCTID_RESET_VALUE);
        getStLenderProfileId().setValue(CHILD_STLENDERPROFILEID_RESET_VALUE);
        getStIncludeDSSstart().setValue(CHILD_STINCLUDEDSSSTART_RESET_VALUE);
        getStIncludeDSSend().setValue(CHILD_STINCLUDEDSSEND_RESET_VALUE);
        getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
        getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
        getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
        getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
        getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
        getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
        getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
        getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
        getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
        getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
        getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
        getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
        getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
        getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
        getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
        getStPageTitle().setValue(CHILD_STPAGETITLE_RESET_VALUE);
        getBtRessurect().setValue(CHILD_BTRESSURECT_RESET_VALUE);
        getBtPreapprovalFirm().setValue(CHILD_BTPREAPPROVALFIRM_RESET_VALUE);
        getStAppDateForServlet().setValue(CHILD_STAPPDATEFORSERVLET_RESET_VALUE);
        getStMIStatus().setValue(CHILD_STMISTATUS_RESET_VALUE);
        getTxMIExistingPolicy().setValue(CHILD_TXMIEXISTINGPOLICY_RESET_VALUE);
        getTxMICertificate().setValue(CHILD_TXMICERTIFICATE_RESET_VALUE);
        getStIncludeCommitmentStart().setValue(CHILD_STINCLUDECOMMITMENTSTART_RESET_VALUE);
        getStIncludeCommitmentEnd().setValue(CHILD_STINCLUDECOMMITMENTEND_RESET_VALUE);
        getStSourcePhoneNoExtension().setValue(CHILD_STSOURCEPHONENOEXTENSION_RESET_VALUE);
        getStRateLock().setValue(CHILD_STRATELOCK_RESET_VALUE);
        getStDealEntry().setValue(CHILD_STDEALENTRY_RESET_VALUE);
        getStDefaultProductId().setValue(CHILD_STDEFAULTPRODUCTID_RESET_VALUE);
        getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
        getRbProgressAdvance().setValue(CHILD_RBPROGRESSADVANCE_RESET_VALUE);
        getTxNextAdvanceAmount().setValue(CHILD_TXNEXTADVANCEAMOUNT_RESET_VALUE);
        getTxAdvanceToDateAmt().setValue(CHILD_TXADVANCETODATEAMT_RESET_VALUE);
        getTxAdvanceNumber().setValue(CHILD_TXADVANCENUMBER_RESET_VALUE);
        getCbRefiOrigPurchaseDateMonth().setValue(CHILD_CBREFIORIGPURCHASEDATEMONTH_RESET_VALUE);
        getTxRefiOrigPurchaseDateYear().setValue(CHILD_TXREFIORIGPURCHASEDATEYEAR_RESET_VALUE);
        getTxRefiOrigPurchaseDateDay().setValue(CHILD_TXREFIORIGPURCHASEDATEDAY_RESET_VALUE);
        getTxRefiPurpose().setValue(CHILD_TXREFIPURPOSE_RESET_VALUE);
        getTxRefiMortgageHolder().setValue(CHILD_TXREFIMORTGAGEHOLDER_RESET_VALUE);
        getRbBlendedAmortization().setValue(CHILD_RBBLENDEDAMORTIZATION_RESET_VALUE);
        getTxRefiOrigPurchasePrice().setValue(CHILD_TXREFIORIGPURCHASEPRICE_RESET_VALUE);
        getTxRefiImprovementValue().setValue(CHILD_TXREFIIMPROVEMENTVALUE_RESET_VALUE);
        getTxRefiOrigMtgAmount().setValue(CHILD_TXREFIORIGMTGAMOUNT_RESET_VALUE);
        getTxRefiOutstandingMtgAmount().setValue(CHILD_TXREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE);
        getTxRefiImprovementsDesc().setValue(CHILD_TXREFIIMPROVEMENTSDESC_RESET_VALUE);
        getStVALSData().setValue(CHILD_STVALSDATA_RESET_VALUE);
        getBtRecalculate().setValue(CHILD_BTRECALCULATE_RESET_VALUE);

        //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
        getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);

        //--Release2.1--//
        //// New link to toggle the language. When touched this link should reload the
        //// page in opposite language (french versus english) and set this new language
        //// session value throughout all modulus.
        getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
        getStPaymentTermDesc().setValue(CHILD_PAYMENTTERMDESC_RESET_VALUE);
        getStRateInventoryId().setValue(CHILD_STRATEINVENTORYID_RESET_VALUE);

        //--Release2.1--//
        //// New HiddenField to get PaymentTermId from JavaScript Applet companion functions
        //// and propagate it to the database on submit.
        getHdPaymentTermId().setValue(CHILD_HDPAYMENTTERMID_RESET_VALUE);

        //===============================================================
        // New fields to handle MIPolicyNum problem :
        //  the number lost if user cancel MI and re-send again later.
        // -- Modified by Billy 16July2003
        getHdMIPolicyNoCMHC().setValue(CHILD_HDMIPOLICYNOCMHC_RESET_VALUE);
        getHdMIPolicyNoGE().setValue(CHILD_HDMIPOLICYNOGE_RESET_VALUE);

        //===============================================================
        //-- FXLink Phase II --//
        //--> New Field for Market Type Indicator
        //--> By Billy 18Nov2003
        getStMarketTypeLabel().setValue(CHILD_STMARKETTYPELABEL_RESET_VALUE);
        getStMarketType().setValue(CHILD_STMARKETTYPE_RESET_VALUE);

        //--DJ_PT_CR--start//
        getStBranchTransitLabel().setValue(CHILD_STBRANCHTRANSITLABEL_RESET_VALUE);
        getStCompanyName().setValue(CHILD_STPARTYNAME_RESET_VALUE);
        getStBranchTransitNumber().setValue(CHILD_STBRANCHTRANSITNUM_RESET_VALUE);
        getStAddressLine1().setValue(CHILD_STADDRESSLINE1_RESET_VALUE);
        getStCity().setValue(CHILD_STCITY_RESET_VALUE);

        //--DJ_PT_CR--end//
        //--DJ_LDI_CR--start--//
        getBtLDInsuranceDetails().setValue(CHILD_BTLDINSURANCEDETAILS_RESET_VALUE);
        getStIncludeLifeDisLabelsStart().setValue(CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE);
        getStIncludeLifeDisLabelsEnd().setValue(CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE);
        getStGdsTdsDetailsLabel().setValue(CHILD_STGDSTDSDETAILSLABEL_RESET_VALUE);
        getStPmntIncludingLifeDisability().setValue(CHILD_STPMNTINCLUDINGLIFEDISABILITY_RESET_VALUE);

        //--DJ_LDI_CR--end--//
        //--FX_LINK--start--//
        getBtSOBDetails().setValue(CHILD_BTSOURCEOFBUSINESSDETAILS_RESET_VALUE);

        //--FX_LINK--end--//
        //--DJ_CR010--start//
        getTxCommisionCode().setValue(CHILD_TXCOMMISIONCODE_RESET_VALUE);

        //--DJ_CR010--end//
        //--DJ_CR203.1--start//
        getTxProprietairePlusLOC().setValue(CHILD_TXPROPRIETAIREPLUSLOC_RESET_VALUE);
        getRbMultiProject().setValue(CHILD_RBMULTIPROJECT_RESET_VALUE);
        getRbProprietairePlus().setValue(CHILD_RBPROPRIETAIREPLUS_RESET_VALUE);

        //--DJ_CR203.1--end//
        //--DJ_CR134--start--27May2004--//
        getStHomeBASEProductRatePmnt().setValue(CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE);

        //--DJ_CR134--end--//
        //--DisableProductRateTicket#570--10Aug2004--start--//
        getStDealRateStatusForServlet().setValue(CHILD_STDEALRATESTATUSFORSERVLET_RESET_VALUE);
        getStRateDisDateForServlet().setValue(CHILD_STRATEDISDATEFORSERVLET_RESET_VALUE);
        getStIsPageEditableForServlet().setValue(CHILD_STISPAGEEDITABLEFORSERVLET_RESET_VALUE);

        //--DisableProductRateTicket#570--10Aug2004--end--//
        //-- ========== DJ#725 begins ========== --//
        //-- by Neil at Nov/30/2004
        getStContactEmailAddress().setValue(CHILD_STCONTACTEMAILADDRESS_RESET_VALUE);
        getStPsDescription().setValue(CHILD_STPSDESCRIPTION_RESET_VALUE);
        getStSystemTypeDescription().setValue(CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE);
        //-- ========== DJ#725 ends ========== --//

        //-- ========== SCR#859 begins ========== --//
        //-- by Neil on Feb 15, 2005
        getStServicingMortgageNumber().setValue(CHILD_STSERVICINGMORTGAGENUMBER_RESET_VALUE);
        getStCCAPS().setValue(CHILD_STCCAPS_RESET_VALUE);
        //-- ========== SCR#859 ends ========== --//

        //--Release3.1--begins
        //--by Hiro Apr 13, 2006
        getCbProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);
        getCbRefiProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);

        getRbProgressAdvanceInspectionBy().setValue(CHILD_RBPROGRESSADVANCEINSPECTIONBY_RESET_VALUE);
        getRbSelfDirectedRRSP().setValue(CHILD_RBSELFDIRECTEDRRSP_RESET_VALUE);
        getTxCMHCProductTrackerIdentifier().setValue(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE);
        getCbLOCRepaymentType().setValue(CHILD_CBLOCREPAYMENTTYPEID_RESET_VALUE);
        getRbRequestStandardService().setValue(CHILD_RBREQUESTSTANDARDSERVICE_RESET_VALUE);
        getStLOCAmortizationMonths().setValue(CHILD_STLOCAMORTIZATIONMONTHS_RESET_VALUE);
        getStLOCInterestOnlyMaturityDate().setValue(CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE);

        //--Release3.1--ends



        // ***** Change by NBC/PP Implementation Team - Version 1.3 - START *****//
        getTxRateGuaranteePeriod().setValue(CHILD_TXRATEGUARANTEEPERIOD_RESET_VALUE);
        getHdChangedEstimatedClosingDate().setValue(CHILD_HDCHANGEDESTIMATEDCLOSINGDATE_RESET_VALUE);
        // ***** Change by NBC/PP Implementation Team - Version 1.3 - END *****//

        // ***** Change by NBC Impl. Team - Version 1.5 - Start *****//
        getChCashBackOverrideInDollars().setValue(
                CHILD_CHCASHBACKOVERRIDEINDOLLARS_RESET_VALUE);
        getTxCashBackInPercentage().setValue(
                CHILD_TXCASHBACKINPERCENTAGE_RESET_VALUE);
        getTxCashBackInDollars()
        .setValue(CHILD_TXCASHBACKINDOLLARS_RESET_VALUE);
        // ***** Change by NBC Impl. Team - Version 1.5 - End*****//

        // ***** Change by NBC Impl. Team - Version 1.4 - Start *****//
        getCbAffiliationProgram().setValue(CHILD_CBAFFILIATIONPROGRAM_RESET_VALUE);
        // ***** Change by NBC Impl. Team - Version 1.4 - End *****//

//      ***** Change by NBC/PP Implementation Team - GCD - Start *****//

        getTxCreditDecisionStatusChild().setValue(CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE);
        getTxCreditDecisionChild().setValue(CHILD_TX_CREDIT_DECISION_RESET_VALUE);
        getTxGlobalCreditBureauScoreChild().setValue(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE);
        getTxGlobalRiskRatingChild().setValue(CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE);
        getTxGlobalInternalScoreChild().setValue(CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE);
        getTxMIStatusChild().setValue(CHILD_TX_MISTATUS_RESET_VALUE);
        getBtCreditDecisionPGChild().setValue(CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE);
        getStIncludeGCDSumStartChild().setValue(CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE);
        getStIncludeGCDSumEndChild().setValue(CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE);

//      ***** Change by NBC/PP Implementation Team - GCD - End *****//
//      ***** Change by NBC/PP Implementation Team - SCR #1679 - Start *****//
        getHdForceProgressAdvanceChild().setValue(CHILD_HDFORCEPROGRESSADVANCE_RESET_VALUE);
//      ***** Change by NBC/PP Implementation Team - SCR #1679 - End *****//
        
        // FFATE
        getCbFinancingWaiverDateMonth().setValue(CHILD_CBFINANCINGWAIVERDATEMONTH_RESET_VALUE);
        getTxFinancingWaiverDateDay().setValue(CHILD_TXFINANCINGWAIVERDATEDAY_RESET_VALUE);
        getTxFinancingWaiverDateYear().setValue(CHILD_TXFINANCINGWAIVERDATEYEAR_RESET_VALUE);
        // FFATE

//      CR03
        getRbAutoCalCommitDate().setValue(CHILD_RBAUTOCALCOMMITDATE_RESET_VALUE);
        getCbCommExpirationDateMonth().setValue(CHILD_CBCOMMEXPIRATIONDATEMONTH_RESET_VALUE);
        getTxCommExpirationDateDay().setValue(CHILD_TXCOMMEXPIRATIONDATEDAY_RESET_VALUE);
        getTxCommExpirationDateYear().setValue(CHILD_TXCOMMEXPIRATIONDATEYEAR_RESET_VALUE);
        getHdCommitmentExpiryDate().setValue(CHILD_HDCOMMITMENTEXPIRYDATE_RESET_VALUE);
        getHdMosProperty().setValue(CHILD_HDMOSPROPERTY_RESET_VALUE);
//      CR03

        //PPI start
        getTxDealPurposeTypeHiddenStart().setValue(CHILD_TXDEALPURPOSETYPEHIDDENSTART_RESET_VALUE);
        getTxImprovedValue().setValue(CHILD_TXIMPROVEDVALUE_RESET_VALUE);
        getTxDealPurposeTypeHiddenEnd().setValue(CHILD_TXDEALPURPOSETYPEHIDDENEND_RESET_VALUE);
        //PPI end

        /***************MCM Impl team changes starts - XS_2.7 *******************/

        getTxRefiAdditionalInformation().setValue(CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE);
        getTxRefExistingMTGNumber().setValue(CHILD_TXREFEXISTINGMTGNUMBER_RESET_VALUE);

        /***************MCM Impl team changes ends - XS_2.7 *********************/
        
        // ***** Qualifying rate ***** //
        getCbQualifyProductType().setValue(CHILD_CBQUALIFYPRODUCTTYPE_RESET_VALUE);
        getTxQualifyRate().setValue(CHILD_TXQUALIFYRATE_RESET_VALUE);
        getChQualifyRateOverride().setValue(
                CHILD_CHQUALIFYRATEOVERRIDE_RESET_VALUE);
        getChQualifyRateOverrideRate().setValue(
                CHILD_CHQUALIFYRATEOVERRIDERATE_RESET_VALUE);
        getBtRecalculateGDS().setValue(CHILD_BTRECALCULATEGDS_RESET_VALUE);
        getStQualifyRate().setValue(CHILD_STQUALIFYRATE_RESET_VALUE);
        getStQualifyProductType().setValue(CHILD_STQUALIFYPRODUCTTYPE_RESET_VALUE);
        getStQualifyRateHidden().setValue(CHILD_STQUALIFYRATEHIDDEN_RESET_VALUE);
        getStQualifyRateEdit().setValue(CHILD_STQUALIFYRATEEDIT_RESET_VALUE);
        getSthdQualifyRate().setValue(CHILD_STHDQUALIFYRATE_RESET_VALUE);
        getSthdQualifyProductId().setValue(CHILD_STHDQUALIFYPRODUCTID_RESET_VALUE);
        getHdQualifyRateDisabled().setValue(CHILD_HDQUALIFYRATEDISABLED_RESET_VALUE);
        // Ticket 267
        getHdDealStatusId().setValue(CHILD_HDDEALSTATUSID_RESET_VALUE);
    }

    /**
     * registerChildren
     * 
     * @version 1.3 <br>
     * Date: 06/29/2006<br>
     * Author: NBC/PP Implementation Team <br>
     * Change: <br>
     * 	Added new fields txRateGuaranteePeriod & hdChangedEstimatedClosingDate<br>
     * Date: 11/21/2006<br>
     * Author: NBC/PP Implementation Team <br>
     * Change: <br>
     *
     * 	Added new field hdForceProgressAdvance for SCR #1679<br>
     *
     *
     *	@Version 1.4 <br>
     *	Date: 06/09/2008
     *	Author: MCM Impl Team <br>
     *	Change Log:  <br>
     *	XS_2.7 -- 06/06/2008 -- Added necessary changes required for the new fields added in Refinance section<br>
     */
    protected void registerChildren()
    {
        super.registerChildren();
        registerChild(CHILD_STPAGELABEL, StaticTextField.class);
        registerChild(CHILD_STCOMPANYNAME, StaticTextField.class);
        registerChild(CHILD_STTODAYDATE, StaticTextField.class);
        registerChild(CHILD_STUSERNAMETITLE, StaticTextField.class);
        registerChild(CHILD_DETECTALERTTASKS, HiddenField.class);
        registerChild(CHILD_SESSIONUSERID, HiddenField.class);
        registerChild(CHILD_TBDEALID, TextField.class);
        registerChild(CHILD_CBPAGENAMES, ComboBox.class);
        registerChild(CHILD_BTPROCEED, Button.class);
        registerChild(CHILD_HREF1, HREF.class);
        registerChild(CHILD_HREF2, HREF.class);
        registerChild(CHILD_HREF3, HREF.class);
        registerChild(CHILD_HREF4, HREF.class);
        registerChild(CHILD_HREF5, HREF.class);
        registerChild(CHILD_HREF6, HREF.class);
        registerChild(CHILD_HREF7, HREF.class);
        registerChild(CHILD_HREF8, HREF.class);
        registerChild(CHILD_HREF9, HREF.class);
        registerChild(CHILD_HREF10, HREF.class);
        registerChild(CHILD_CHANGEPASSWORDHREF, HREF.class);
        registerChild(CHILD_BTTOOLHISTORY, Button.class);
        registerChild(CHILD_BTTOONOTES, Button.class);
        registerChild(CHILD_BTTOOLSEARCH, Button.class);
        registerChild(CHILD_BTTOOLLOG, Button.class);
        registerChild(CHILD_BTWORKQUEUELINK, Button.class);
        registerChild(CHILD_BTPREVTASKPAGE, Button.class);
        registerChild(CHILD_BTNEXTTASKPAGE, Button.class);
        registerChild(CHILD_STERRORFLAG, StaticTextField.class);
        registerChild(CHILD_STPREVTASKPAGELABEL, StaticTextField.class);
        registerChild(CHILD_STNEXTTASKPAGELABEL, StaticTextField.class);
        registerChild(CHILD_STTASKNAME, StaticTextField.class);
        registerChild(CHILD_HDDEALID, HiddenField.class);
        registerChild(CHILD_HDDEALCOPYID, HiddenField.class);
        registerChild(CHILD_STDEALNO, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINED3YRGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDBORROWERGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDTDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINED3YRTDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDBORROWERTDS, StaticTextField.class);
        registerChild(CHILD_REPEATGDSTDSDETAILS,
                pgDealEntryRepeatGDSTDSDetailsTiledView.class);
        registerChild(CHILD_REPEATAPPLICANT,
                pgDealEntryRepeatApplicantTiledView.class);
        registerChild(CHILD_BTADDAPPLICANT, Button.class);
        registerChild(CHILD_BTDUPAPPLICANT, Button.class);
/*        registerChild(CHILD_BTCHANGESOURCE, Button.class);
        registerChild(CHILD_STSOURCEFIRMSOURCEPART, StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_STSOURCELASTNAME, StaticTextField.class);
        registerChild(CHILD_STSOURCEADDRESS, StaticTextField.class);
        registerChild(CHILD_STSOURCEPHONENO, StaticTextField.class);
        registerChild(CHILD_STSOURCEFAXNO, StaticTextField.class);*/
		//4.4 Submission Agent
        registerChild(CHILD_REPEATEDSOB, pgDealEntryRepeatedSOBTiledView.class);
        
        registerChild(CHILD_TXREFERENCESOURCEAPP, TextField.class);
        registerChild(CHILD_REPEATPROPERTYINFORMATION,
                pgDealEntryRepeatPropertyInformationTiledView.class);
        registerChild(CHILD_BTADDPROPERTY, Button.class);
        registerChild(CHILD_CBDEALPURPOSE, ComboBox.class);
        registerChild(CHILD_CBDEALTYPE, ComboBox.class);
        // SEAN GECF document type drop down register child.
        registerChild(CHILD_CBIVDOCUMENTTYPE, ComboBox.class);
        registerChild(CHILD_STHIDEIVDOCUMENTTYPESTART, StaticTextField.class);
        registerChild(CHILD_STHIDEIVDOCUMENTTYPEEND, StaticTextField.class);
        // END GECF DOCUMENT TYPE DROP DOWN
        // SEAN DJ SPEC-Progress Advance Type July 20, 2005: Register Child.
        registerChild(CHILD_CBPROGRESSADVANCETYPE, ComboBox.class);
        // SEAN DJ SPEC-PAT END
        registerChild(CHILD_CBLOB, ComboBox.class);
        registerChild(CHILD_CBLENDER, ComboBox.class);
        registerChild(CHILD_CBPRODUCT, ComboBox.class);
        registerChild(CHILD_CBRATELOCKED, ComboBox.class);
        registerChild(CHILD_CBCHARGETERM, ComboBox.class);
        registerChild(CHILD_CBPOSTEDINTERESTRATE, ComboBox.class);
        registerChild(CHILD_TXDISCOUNT, TextField.class);
        registerChild(CHILD_TXPREMIUM, TextField.class);
        registerChild(CHILD_TXBUYDOWNRATE, TextField.class);
        registerChild(CHILD_STNETRATE, StaticTextField.class);
        registerChild(CHILD_STTOTALESCROWPAYMENT, StaticTextField.class);
        registerChild(CHILD_TXREQUESTEDLOANAMOUNT, TextField.class);
        registerChild(CHILD_CBPAYEMNTFREQUENCYTERM, ComboBox.class);
        registerChild(CHILD_TXAMORTIZATIONYEARS, TextField.class);
        registerChild(CHILD_TXAMORTIZATIONMONTHS, TextField.class);
        registerChild(CHILD_STEFFECTIVEAMORTIZATIONYEARS, StaticTextField.class);
        registerChild(CHILD_STEFFECTIVEAMORTIZATIONMONTHS, StaticTextField.class);
        registerChild(CHILD_TXADDITIONALPRINCIPALPAYMENT, TextField.class);
        registerChild(CHILD_TXPAYTERMYEARS, TextField.class);
        registerChild(CHILD_TXPAYTERMMONTHS, TextField.class);
        registerChild(CHILD_STPIPAYMENT, StaticTextField.class);
        registerChild(CHILD_STTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_CBREPAYMENTTYPE, ComboBox.class);
        /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
        registerChild(CHILD_STDEFAULTREPAYMENTTYPEID, StaticTextField.class);
        /***************MCM Impl team changes ends - XS_2.1, XS_2.2, XS_2.4 *******************/
        registerChild(CHILD_CBESTCLOSINGDATEMONTHS, ComboBox.class);
        registerChild(CHILD_TXESTCLOSINGDATEDAY, TextField.class);
        registerChild(CHILD_TXESTCLOSINGDATEYEAR, TextField.class);
        registerChild(CHILD_CBFIRSTPAYDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXFIRSTPAYDATEDAY, TextField.class);
        registerChild(CHILD_TXFIRSTPAYDATEYEAR, TextField.class);
        registerChild(CHILD_STMATURITYDATE, StaticTextField.class);
        registerChild(CHILD_TXPREAPPESTPURCHASEPRICE, TextField.class);
        registerChild(CHILD_CBSECONDMORTGAGEEXIST, ComboBox.class);
        registerChild(CHILD_CBPREPAYMENTPENANLTY, ComboBox.class);
        registerChild(CHILD_CBPRIVILAGEPAYMENTOPTION, ComboBox.class);
        registerChild(CHILD_STINCLUDEFINANCINGPROGRAMSTART, StaticTextField.class);
        registerChild(CHILD_CBFINANCINGPROGRAM, ComboBox.class);
        registerChild(CHILD_STINCLUDEFINANCINGPROGRAMEND, StaticTextField.class);
        registerChild(CHILD_CBSPECIALFEATURE, ComboBox.class);
        registerChild(CHILD_CBCOMMEXPECTEDDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXCOMMEXPECTEDDATEDAY, TextField.class);
        registerChild(CHILD_TXCOMMEXPECTEDDATEYEAR, TextField.class);
        registerChild(CHILD_STCOMMITMENTPERIOD, StaticTextField.class);
        registerChild(CHILD_STCOMMEXPDATE, StaticTextField.class);
        registerChild(CHILD_STCOMMACCEPTANCEDATE, StaticTextField.class);
        registerChild(CHILD_STBRIDGELOANAMOUNT, StaticTextField.class);
        registerChild(CHILD_TXADVANCEHOLD, TextField.class);
        registerChild(CHILD_BTREVIEWBRIDGE, Button.class);
        registerChild(CHILD_REPEATEDDOWNPAYMENT,
                pgDealEntryRepeatedDownPaymentTiledView.class);
        registerChild(CHILD_BTADDDOWNPAYMENT, Button.class);
        registerChild(CHILD_STDOWNPAYMENTREQUIRED, StaticTextField.class);
        registerChild(CHILD_REPEATEDESCROWDETAILS,
                pgDealEntryRepeatedEscrowDetailsTiledView.class);
        registerChild(CHILD_BTADDESCROW, Button.class);
        registerChild(CHILD_CBBRANCH, ComboBox.class);
        registerChild(CHILD_TXREFEXISITINGMTG, TextField.class);
        registerChild(CHILD_CBCROSSSELL, ComboBox.class);
        registerChild(CHILD_CBTAXPAYOR, ComboBox.class);
        registerChild(CHILD_BTPARTYSUMMARY, Button.class);
        registerChild(CHILD_STREFERENCETYPE, StaticTextField.class);
        registerChild(CHILD_STREFERENCEDEALNO, StaticTextField.class);
        registerChild(CHILD_CBMIINDICATOR, ComboBox.class);
        registerChild(CHILD_CBMITYPE, ComboBox.class);
        registerChild(CHILD_CBMIINSURER, ComboBox.class);
        registerChild(CHILD_CBMIPAYOR, ComboBox.class);
        registerChild(CHILD_TXMIPREMIUM, TextField.class);
        registerChild(CHILD_RBMIUPFRONT, RadioButtonGroup.class);
        registerChild(CHILD_RBMIRUINTERVENTION, RadioButtonGroup.class);
        registerChild(CHILD_STMIPREQCERTNUM, StaticTextField.class);
        registerChild(CHILD_BTDEALSUBMIT, Button.class);
        registerChild(CHILD_BTDEALCANCEL, Button.class);
        registerChild(CHILD_STPMGENERATE, StaticTextField.class);
        registerChild(CHILD_STPMHASTITLE, StaticTextField.class);
        registerChild(CHILD_STPMHASINFO, StaticTextField.class);
        registerChild(CHILD_STPMHASTABLE, StaticTextField.class);
        registerChild(CHILD_STPMHASOK, StaticTextField.class);
        registerChild(CHILD_STPMTITLE, StaticTextField.class);
        registerChild(CHILD_STPMINFOMSG, StaticTextField.class);
        registerChild(CHILD_STPMONOK, StaticTextField.class);
        registerChild(CHILD_STPMMSGS, StaticTextField.class);
        registerChild(CHILD_STPMMSGTYPES, StaticTextField.class);
        registerChild(CHILD_STAMGENERATE, StaticTextField.class);
        registerChild(CHILD_STAMHASTITLE, StaticTextField.class);
        registerChild(CHILD_STAMHASINFO, StaticTextField.class);
        registerChild(CHILD_STAMHASTABLE, StaticTextField.class);
        registerChild(CHILD_STAMTITLE, StaticTextField.class);
        registerChild(CHILD_STAMINFOMSG, StaticTextField.class);
        registerChild(CHILD_STAMMSGS, StaticTextField.class);
        registerChild(CHILD_STAMMSGTYPES, StaticTextField.class);
        registerChild(CHILD_STAMDIALOGMSG, StaticTextField.class);
        registerChild(CHILD_STAMBUTTONSHTML, StaticTextField.class);
        registerChild(CHILD_STTARGETDOWNPAYMENT, StaticTextField.class);
        registerChild(CHILD_STTARGETESCROW, StaticTextField.class);
        registerChild(CHILD_TXTOTALDOWN, TextField.class);
        registerChild(CHILD_TXTOTALESCROW, TextField.class);
        registerChild(CHILD_TBPAYMENTTERMDESCRIPTION, TextField.class);
        registerChild(CHILD_TBRATECODE, TextField.class);
        registerChild(CHILD_STRATETIMESTAMP, StaticTextField.class);
        registerChild(CHILD_HDLONGPOSTEDDATE, HiddenField.class);
        registerChild(CHILD_STLENDERPRODUCTID, StaticTextField.class);
        registerChild(CHILD_STLENDERPROFILEID, StaticTextField.class);
        registerChild(CHILD_STINCLUDEDSSSTART, StaticTextField.class);
        registerChild(CHILD_STINCLUDEDSSEND, StaticTextField.class);
        registerChild(CHILD_STDEALID, StaticTextField.class);
        registerChild(CHILD_STBORRFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_STDEALSTATUS, StaticTextField.class);
        registerChild(CHILD_STDEALSTATUSDATE, StaticTextField.class);
        registerChild(CHILD_STLOB, StaticTextField.class);
        registerChild(CHILD_STDEALTYPE, StaticTextField.class);
        registerChild(CHILD_STDEALPURPOSE, StaticTextField.class);
        registerChild(CHILD_STTOTALLOANAMOUNT, StaticTextField.class);
        registerChild(CHILD_STPMTTERM, StaticTextField.class);
        registerChild(CHILD_STESTCLOSINGDATE, StaticTextField.class);
        registerChild(CHILD_STSOURCE, StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRM, StaticTextField.class);
        registerChild(CHILD_STPURCHASEPRICE, StaticTextField.class);
        registerChild(CHILD_STSPECIALFEATURE, StaticTextField.class);
        registerChild(CHILD_BTOK, Button.class);
        registerChild(CHILD_STPAGETITLE, StaticTextField.class);
        registerChild(CHILD_BTRESSURECT, Button.class);
        registerChild(CHILD_BTPREAPPROVALFIRM, Button.class);
        registerChild(CHILD_STAPPDATEFORSERVLET, StaticTextField.class);
        registerChild(CHILD_STMISTATUS, StaticTextField.class);
        registerChild(CHILD_TXMIEXISTINGPOLICY, TextField.class);
        registerChild(CHILD_TXMICERTIFICATE, TextField.class);
        registerChild(CHILD_STINCLUDECOMMITMENTSTART, StaticTextField.class);
        registerChild(CHILD_STINCLUDECOMMITMENTEND, StaticTextField.class);
        registerChild(CHILD_STSOURCEPHONENOEXTENSION, StaticTextField.class);
        registerChild(CHILD_STRATELOCK, StaticTextField.class);
        registerChild(CHILD_STDEALENTRY, StaticTextField.class);
        registerChild(CHILD_STDEFAULTPRODUCTID, StaticTextField.class);
        registerChild(CHILD_STVIEWONLYTAG, StaticTextField.class);
        registerChild(CHILD_RBPROGRESSADVANCE, RadioButtonGroup.class);
        registerChild(CHILD_TXNEXTADVANCEAMOUNT, TextField.class);
        registerChild(CHILD_TXADVANCETODATEAMT, TextField.class);
        registerChild(CHILD_TXADVANCENUMBER, TextField.class);
        registerChild(CHILD_CBREFIORIGPURCHASEDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXREFIORIGPURCHASEDATEYEAR, TextField.class);
        registerChild(CHILD_TXREFIORIGPURCHASEDATEDAY, TextField.class);
        registerChild(CHILD_TXREFIPURPOSE, TextField.class);
        registerChild(CHILD_TXREFIMORTGAGEHOLDER, TextField.class);
        registerChild(CHILD_RBBLENDEDAMORTIZATION, RadioButtonGroup.class);
        registerChild(CHILD_TXREFIORIGPURCHASEPRICE, TextField.class);
        registerChild(CHILD_TXREFIIMPROVEMENTVALUE, TextField.class);
        registerChild(CHILD_TXREFIORIGMTGAMOUNT, TextField.class);
        registerChild(CHILD_TXREFIOUTSTANDINGMTGAMOUNT, TextField.class);
        registerChild(CHILD_TXREFIIMPROVEMENTSDESC, TextField.class);
        registerChild(CHILD_STVALSDATA, StaticTextField.class);
        registerChild(CHILD_BTRECALCULATE, Button.class);

        //// New hidden button implementation.
        registerChild(CHILD_BTACTMSG, Button.class);

        //--Release2.1--//
        //// New link to toggle the language. When touched this link should reload the
        //// page in opposite language (french versus english) and set this new language
        //// session value id throughout all modulus.
        registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);
        registerChild(CHILD_PAYMENTTERMDESC, StaticTextField.class);
        registerChild(CHILD_STRATEINVENTORYID, StaticTextField.class);

        //--Release2.1--//
        //// New HiddenField to get PaymentTermId from JavaScript Applet companion functions
        //// and propagate it to the database on submit.
        registerChild(CHILD_HDPAYMENTTERMID, StaticTextField.class);

        //===============================================================
        // New fields to handle MIPolicyNum problem :
        //  the number lost if user cancel MI and re-send again later.
        // -- Modified by Billy 14July2003
        registerChild(CHILD_HDMIPOLICYNOCMHC, HiddenField.class);
        registerChild(CHILD_HDMIPOLICYNOGE, HiddenField.class);

        //===============================================================
        //-- FXLink Phase II --//
        //--> New Field for Market Type Indicator
        //--> By Billy 18Nov2003
        registerChild(CHILD_STMARKETTYPELABEL, StaticTextField.class);
        registerChild(CHILD_STMARKETTYPE, StaticTextField.class);

        //--DJ_PT_CR--start//
        registerChild(CHILD_STBRANCHTRANSITLABEL, StaticTextField.class);
        registerChild(CHILD_STPARTYNAME, StaticTextField.class);
        registerChild(CHILD_STBRANCHTRANSITNUM, StaticTextField.class);
        registerChild(CHILD_STADDRESSLINE1, StaticTextField.class);
        registerChild(CHILD_STCITY, StaticTextField.class);

        //--DJ_PT_CR--end//
        //--DJ_LDI_CR--start--//
        registerChild(CHILD_BTLDINSURANCEDETAILS, Button.class);
        registerChild(CHILD_STINCLUDELIFEDISLABELSSTART, StaticTextField.class);
        registerChild(CHILD_STINCLUDELIFEDISLABELSEND, StaticTextField.class);
        registerChild(CHILD_STGDSTDSDETAILSLABEL, StaticTextField.class);
        registerChild(CHILD_STPMNTINCLUDINGLIFEDISABILITY, StaticTextField.class);

        //--DJ_LDI_CR--end--//
        //--FX_LINK--start--//
        registerChild(CHILD_BTSOURCEOFBUSINESSDETAILS, Button.class);

        //--FX_LINK--end--//
        //--DJ_CR010--start//
        registerChild(CHILD_TXCOMMISIONCODE, TextField.class);

        //--DJ_CR010--end//
        //--DJ_CR203.1--start//
        registerChild(CHILD_TXCOMMISIONCODE, TextField.class);
        registerChild(CHILD_RBMULTIPROJECT, RadioButtonGroup.class);
        registerChild(CHILD_RBPROPRIETAIREPLUS, RadioButtonGroup.class);

        //--DJ_CR203.1--end//
        //--DJ_CR134--start--27May2004--//
        registerChild(CHILD_STHOMEBASEPRODUCTRATEPMNT, StaticTextField.class);

        //--DJ_CR134--end--//
        //--DisableProductRateTicket#570--10Aug2004--start--//
        registerChild(CHILD_STDEALRATESTATUSFORSERVLET, StaticTextField.class);
        registerChild(CHILD_STRATEDISDATEFORSERVLET, StaticTextField.class);
        registerChild(CHILD_STISPAGEEDITABLEFORSERVLET, StaticTextField.class);
        //--DisableProductRateTicket#570--10Aug2004--end--//
        //-- ========== DJ#725 begins ========== --//
        //-- by Neil at Nov/30/2004
        registerChild(CHILD_STCONTACTEMAILADDRESS, StaticTextField.class);
        registerChild(CHILD_STPSDESCRIPTION, StaticTextField.class);
        registerChild(CHILD_STSYSTEMTYPEDESCRIPTION, StaticTextField.class);
        //-- ========== DJ#725 ends ========== --//

        //-- ========== SCR#859 begins ========== --//
        //-- by Neil on Feb 15, 2005
        registerChild(CHILD_STSERVICINGMORTGAGENUMBER, StaticTextField.class);
        registerChild(CHILD_STCCAPS, StaticTextField.class);
        //-- ========== SCR#859 ends ========== --//

        //  ***** Change by NBC/PP Implementation Team - Version 1.3 - START *****//
        registerChild(CHILD_TXRATEGUARANTEEPERIOD, TextField.class);
        registerChild(CHILD_HDCHANGEDESTIMATEDCLOSINGDATE, HiddenField.class);
        //  ***** Change by NBC/PP Implementation Team - Version 1.3 - END *****//

        //--Release3.1--begins
        //--by Hiro Apr 13, 2006
        registerChild(CHILD_CBPRODUCTTYPE, ComboBox.class);
        registerChild(CHILD_CBREFIPRODUCTTYPE, ComboBox.class);
        registerChild(CHILD_RBPROGRESSADVANCEINSPECTIONBY, RadioButtonGroup.class);
        registerChild(CHILD_RBSELFDIRECTEDRRSP, RadioButtonGroup.class);
        registerChild(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER, TextField.class);
        registerChild(CHILD_CBLOCREPAYMENTTYPEID, ComboBox.class);
        registerChild(CHILD_RBREQUESTSTANDARDSERVICE, RadioButtonGroup.class);
        registerChild(CHILD_STLOCAMORTIZATIONMONTHS, StaticTextField.class);
        registerChild(CHILD_STLOCINTERESTONLYMATURITYDATE, StaticTextField.class);
        //--Release3.1--ends

        // ***** Change by NBC Impl. Team - Version 1.5 - Start *****//
        registerChild(CHILD_CHCASHBACKOVERRIDEINDOLLARS, CheckBox.class);
        registerChild(CHILD_TXCASHBACKINPERCENTAGE, TextField.class);
        registerChild(CHILD_TXCASHBACKINDOLLARS, TextField.class);
        // ***** Change by NBC Impl. Team - Version 1.5 - End*****//

        // ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
        registerChild(CHILD_CBAFFILIATIONPROGRAM,ComboBox.class);
        // ***** Change by NBC Impl. Team - Version 1.6 - End *****//

        //  ***** Change by NBC/PP Implementation Team - GCD - Start *****//

        registerChild(CHILD_TX_CREDIT_DECISION_STATUS, StaticTextField.class);
        registerChild(CHILD_TX_CREDIT_DECISION, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_RISK_RATING, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_INTERNAL_SCORE, StaticTextField.class);
        registerChild(CHILD_TX_MISTATUS, StaticTextField.class);
        registerChild(CHILD_BT_CREDIT_DECISION_PG, Button.class);
        registerChild(CHILD_ST_INCLUDE_GCDSUM_START, StaticTextField.class);
        registerChild(CHILD_ST_INCLUDE_GCDSUM_END, StaticTextField.class);

        //  ***** Change by NBC/PP Implementation Team - GCD - End *****//
//      ***** Change by NBC/PP Implementation Team - SCR #1679 - Start *****//
        registerChild(CHILD_HDFORCEPROGRESSADVANCE, HiddenField.class);
//      ***** Change by NBC/PP Implementation Team - SCR #1679 - End *****//
        
        // FFATE
        registerChild(CHILD_CBFINANCINGWAIVERDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXFINANCINGWAIVERDATEDAY, TextField.class);
        registerChild(CHILD_TXFINANCINGWAIVERDATEYEAR, TextField.class);
        // FFATE

//      CR03
        registerChild(CHILD_RBAUTOCALCOMMITDATE, RadioButtonGroup.class);
        registerChild(CHILD_CBCOMMEXPIRATIONDATEMONTH, ComboBox.class);
        registerChild(CHILD_TXCOMMEXPIRATIONDATEDAY, TextField.class);
        registerChild(CHILD_TXCOMMEXPIRATIONDATEYEAR, TextField.class);
        registerChild(CHILD_HDCOMMITMENTEXPIRYDATE, HiddenField.class);
        registerChild(CHILD_HDMOSPROPERTY, HiddenField.class);

//      CR03

        //PPI start
        registerChild(CHILD_TXDEALPURPOSETYPEHIDDENSTART, TextField.class);
        registerChild(CHILD_TXIMPROVEDVALUE, TextField.class);
        registerChild(CHILD_TXDEALPURPOSETYPEHIDDENEND, TextField.class);
        //PPI end

        /***************MCM Impl team changes starts - XS_2.7 *******************/

        registerChild(CHILD_TXREFIADDITIONALINFORMATION, TextField.class);
        registerChild(CHILD_TXREFEXISTINGMTGNUMBER, TextField.class);

        /***************MCM Impl team changes ends - XS_2.7 *********************/



        /******************* MCM Impl team changes  - XS 2.9 *******************/
        registerChild(CHILD_STCOMPONENTINFOPAGELET, pgComponentInfoPageletViewBean.class);
        /***************MCM Impl team changes starts - XS_2.13*******************/
        registerChild(CHILD_STQUALIFYINGDETAILSPAGELET,pgQualifyingDetailsPageletView.class);
        /***************MCM Impl team changes starts - XS_2.13*******************/
        
        //Qualifying Rate
        registerChild(CHILD_CBQUALIFYPRODUCTTYPE, ComboBox.class);
        registerChild(CHILD_TXQUALIFYRATE, TextField.class);
        registerChild(CHILD_CHQUALIFYRATEOVERRIDE, CheckBox.class);
        registerChild(CHILD_CHQUALIFYRATEOVERRIDERATE, CheckBox.class);
        registerChild(CHILD_BTRECALCULATEGDS, Button.class);
        registerChild(CHILD_STQUALIFYRATE, StaticTextField.class);
        registerChild(CHILD_STQUALIFYPRODUCTTYPE, StaticTextField.class);
        registerChild(CHILD_STQUALIFYRATEHIDDEN, StaticTextField.class);
        registerChild(CHILD_STQUALIFYRATEEDIT, StaticTextField.class);
        registerChild(CHILD_STHDQUALIFYRATE, StaticTextField.class);
        registerChild(CHILD_STHDQUALIFYPRODUCTID, StaticTextField.class);
        registerChild(CHILD_HDQUALIFYRATEDISABLED, StaticTextField.class);
        registerChild(CHILD_HDDEALSTATUSID, HiddenField.class);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Model management methods
    ////////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    public Model[] getWebActionModels(int executionType)
    {
        List modelList = new ArrayList();

        switch (executionType)
        {
        case MODEL_TYPE_RETRIEVE:

            //--> For optimization we should not auto execute the DOs
            //--> - They already execute @setupBeforePageGeneration method
            //--> By Billy 17July2003
            //modelList.add(getdoDealEntryMainSelectModel());;
            //modelList.add(getdoDealEntrySourceInfoModel());;
            //modelList.add(getdoDealEntryBridgeModel());;
            //modelList.add(getdoDealGetReferenceInfoModel());;
            modelList.add(getdoDealSummarySnapShotModel());
            ;

            //-- ========== DJ#725 begins ========== --//
            //-- by Neil : Nov/30/2004
            //modelList.add(getdoDealEntrySourceInfoModel()); // Oct 28, 2010. FXP30439

            //-- ========== DJ#725 ends ========== --//
            //modelList.add(getdoDealMIStatusModel());;
            // ***** Change by NBC/PP Implementation Team - GCD - Start

            modelList.add(getdoAdjudicationResponseBNCModel());

            modelList.add(getdoDealMIStatusModel());
            ;
            // ***** Change by NBC/PP Implementation Team - GCD - END      


            break;

        case MODEL_TYPE_UPDATE:
            ;

            break;

        case MODEL_TYPE_DELETE:
            ;

            break;

        case MODEL_TYPE_INSERT:
            ;

            break;

        case MODEL_TYPE_EXECUTE:
            ;

            break;
        }

        return (Model[]) modelList.toArray(new Model[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // View flow control methods
    ////////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        //--Release2.1--start//
        ////Populate all ComboBoxes manually here
        //// 1. Setup Options for cbPageNames
        //cbPageNamesOptions..populate(getRequestContext());
        int langId = handler.getTheSessionState().getLanguageId();
        cbPageNamesOptions.populate(getRequestContext(), langId);

        ////Setup NoneSelected Label for cbPageNames
        CHILD_CBPAGENAMES_NONSELECTED_LABEL =
            BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL",
                    handler.getTheSessionState().getLanguageId());

        ////Check if the cbPageNames is already created
        if (getCbPageNames() != null)
        {
            getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
        }

        //// 2. Populate Options for cbDealPurpose
        cbDealPurposeOptions.populate(getRequestContext());

        //// 3. Populate Options for cbDealType
        cbDealTypeOptions.populate(getRequestContext());

        //// 4. Populate Options for cbLOBOption
        cbLOBOptions.populate(getRequestContext());

        //// 5. Populate Options for cbCharge
        cbChargeTermOptions.populate(getRequestContext());

        //// 6-... Repopulate all Comboboxes on the page.
        cbPayemntFrequencyTermOptions.populate(getRequestContext());
        // SEAN GECF DOCUMENT TYPE DROP DOWN populate options.
        cbIVDocumentTypeOptions.populate(getRequestContext());
        // END GECF DOCUMENT TYPE DROP
        // SEAN DJ SPEC-Progress Advance Type July 22, 2005: populate the options.
        cbProgressAdvanceTypeOptions.populate(getRequestContext());
        // SEAN DJ SPEC-PAT END
        cbRepaymentTypeOptions.populate(getRequestContext());
        cbPrePaymentPenanltyOptions.populate(getRequestContext());
        cbPrivilagePaymentOptionOptions.populate(getRequestContext());
        cbFinancingProgramOptions.populate(getRequestContext());
        cbSpecialFeatureOptions.populate(getRequestContext());
        cbMIIndicatorOptions.populate(getRequestContext());
        cbBranchOptions.populate(getRequestContext());
        cbCrossSellOptions.populate(getRequestContext());
        cbTaxPayorOptions.populate(getRequestContext());
        cbMIIndicatorOptions.populate(getRequestContext());
        cbMIInsurerOptions.populate(getRequestContext());
        cbMIPayorOptions.populate(getRequestContext());

        //--Release3.1--begins
        //--by Hiro Apr 13, 2006
        cbProductTypeOptions.populate(getRequestContext());
        cbRefiProductTypeOptions.populate(getRequestContext());
        cbLOCRepaymentTypeOptions.populate(getRequestContext());
        //--Release3.1--ends

        // ***** Change by NBC Impl. Team - Version 1.4 - Start*****//
        cbAffiliationProgramOptions.populate(getRequestContext());
        // ***** Change by NBC Impl. Team - Version 1.4 - End*****//

        // 7. Setup the Yes/No/Co-App ComboBox and Radio Options
        String yesStr =
            BXResources.getGenericMsg("YES_LABEL",
                    handler.getTheSessionState().getLanguageId());
        String noStr =
            BXResources.getGenericMsg("NO_LABEL",
                    handler.getTheSessionState().getLanguageId());
        String coAppStr =
            BXResources.getGenericMsg("COAPP_LABEL",
                    handler.getTheSessionState().getLanguageId());

        cbRateLockedOptions.setOptions(new String[] { noStr, yesStr },
                new String[] { "N", "Y" });
        cbSecondMortgageExistOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbMIUpfrontOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbMIRUInterventionOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbProgressAdvanceOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbBlendedAmortizationOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });

        //--DJ_CR203.1--start//
        rbMultiProjectOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        rbProprietairePlusOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });

        //--Release3.1--begins
        //--by Hiro Apr 18, 2006
        String lenderStr =
            BXResources.getGenericMsg("LENDER_LABEL",
                    handler.getTheSessionState().getLanguageId());
        String InsurerStr =
            BXResources.getGenericMsg("INSURER_LABEL",
                    handler.getTheSessionState().getLanguageId());
        rbProgressAdvanceInspectionByOptions.setOptions(new String[]
                                                                   { lenderStr, InsurerStr }, new String[]
                                                                                                         { "L", "I" });
        rbSelfDirectedRRSPOptions.setOptions(new String[]
                                                        { yesStr, noStr }, new String[]
                                                                                      { "Y", "N" });
        rbRequestStandardServiceOptions.setOptions(new String[]
                                                              { yesStr, noStr }, new String[]
                                                                                            { "Y", "N" });
        // --Release3.1--ends

        //--DJ_CR203.1--end//


        //CR03
        rbAutoCalCommitDateOptions.setOptions(new String[] { yesStr, noStr },
                new String[] { "Y", "N" });
        //CR03
        handler.pageSaveState();
        super.beginDisplay(event);
    }

    /**
     *
     *
     */
    public boolean beforeModelExecutes(Model model, int executionContext)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.setupBeforePageGeneration();

        handler.pageSaveState();

        // This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
        return super.beforeModelExecutes(model, executionContext);
    }

    /**
     *
     *
     */
    public void afterModelExecutes(Model model, int executionContext)
    {
        // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);
    }

    /**
     *
     *
     */
    public void afterAllModelsExecute(int executionContext)
    {
        // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
        super.afterAllModelsExecute(executionContext);

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.populatePageDisplayFields();

        handler.pageSaveState();

        // The following code block was migrated from the this_onBeforeRowDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       int rc = PROCEED;

       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.populatePageDisplayFields();

       handler.pageSaveState();

       return(rc);
         */
    }

    /**
     *
     *
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception)
    throws ModelControlException
    {
        // This is the analog of NetDynamics this_onDataObjectErrorEvent
        super.onModelError(model, executionContext, exception);
    }
    
    //-- ========== DJ#725 Starts ========== --//
    //-- By Neil at Nov/30/2004

    /**
     *
     */
    public StaticTextField getStContactEmailAddress()
    {
        return (StaticTextField) getChild(CHILD_STCONTACTEMAILADDRESS);
    }

    /**
     *
     * @return
     */
    public StaticTextField getStPsDescription()
    {
        return (StaticTextField) getChild(CHILD_STPSDESCRIPTION);
    }

    /**
     *
     * @return
     */
    public StaticTextField getStSystemTypeDescription()
    {
        return (StaticTextField) getChild(CHILD_STSYSTEMTYPEDESCRIPTION);
    }

    //-- ========== DJ#725 Ends ========== --//

    /**
     *
     *
     */
    public StaticTextField getStPageLabel()
    {
        return (StaticTextField) getChild(CHILD_STPAGELABEL);
    }

    /**
     *
     *
     */
    public StaticTextField getStCompanyName()
    {
        return (StaticTextField) getChild(CHILD_STCOMPANYNAME);
    }

    /**
     *
     *
     */
    public StaticTextField getStTodayDate()
    {
        return (StaticTextField) getChild(CHILD_STTODAYDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStUserNameTitle()
    {
        return (StaticTextField) getChild(CHILD_STUSERNAMETITLE);
    }

    /**
     *
     *
     */
    public HiddenField getDetectAlertTasks()
    {
        return (HiddenField) getChild(CHILD_DETECTALERTTASKS);
    }

    /**
     *
     *
     */
    public HiddenField getSessionUserId()
    {
        return (HiddenField) getChild(CHILD_SESSIONUSERID);
    }

    /**
     *
     *
     */
    public TextField getTbDealId()
    {
        return (TextField) getChild(CHILD_TBDEALID);
    }

    /**
     *
     *
     */
    public ComboBox getCbPageNames()
    {
        return (ComboBox) getChild(CHILD_CBPAGENAMES);
    }

    //--DJ_Ticket661--end--//

    //***** Change by NBC/PP Implementation Team - GCD - Start *****//

    public void handleBtCreditDecisionPGRequest(RequestInvocationEvent event) 
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleCreditDecisionPGButton(event);
        handler.postHandlerProtocol();

    }

    //***** Change by NBC/PP Implementation Team - GCD - End *****//


    /**
     *
     *
     */
    public void handleBtProceedRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {

        updateCashBackAmount();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleGoPage();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btProceed_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleGoPage();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtProceed()
    {
        return (Button) getChild(CHILD_BTPROCEED);
    }

    /**
     *
     *
     */
    public void handleHref1Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(0);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href1_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(0);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref1()
    {
        return (HREF) getChild(CHILD_HREF1);
    }

    /**
     *
     *
     */
    public void handleHref2Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(1);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href2_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(1);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref2()
    {
        return (HREF) getChild(CHILD_HREF2);
    }

    /**
     *
     *
     */
    public void handleHref3Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(2);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href3_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(2);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref3()
    {
        return (HREF) getChild(CHILD_HREF3);
    }

    /**
     *
     *
     */
    public void handleHref4Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(3);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href4_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(3);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref4()
    {
        return (HREF) getChild(CHILD_HREF4);
    }

    /**
     *
     *
     */
    public void handleHref5Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(4);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href5_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(4);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref5()
    {
        return (HREF) getChild(CHILD_HREF5);
    }

    /**
     *
     *
     */
    public void handleHref6Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(5);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href6_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(5);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref6()
    {
        return (HREF) getChild(CHILD_HREF6);
    }

    /**
     *
     *
     */
    public void handleHref7Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(6);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href7_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(6);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref7()
    {
        return (HREF) getChild(CHILD_HREF7);
    }

    /**
     *
     *
     */
    public void handleHref8Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(7);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href8_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(7);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref8()
    {
        return (HREF) getChild(CHILD_HREF8);
    }

    /**
     *
     *
     */
    public void handleHref9Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(8);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href9_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(8);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref9()
    {
        return (HREF) getChild(CHILD_HREF9);
    }

    /**
     *
     *
     */
    public void handleHref10Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleOpenDialogLink(9);

        handler.postHandlerProtocol();

        // The following code block was migrated from the Href10_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleOpenDialogLink(9);

       return handler.postHandlerProtocol();
         */
    }

    /**
     *
     *
     */
    public HREF getHref10()
    {
        return (HREF) getChild(CHILD_HREF10);
    }

    /**
     *
     *
     */
    public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleChangePassword();

        handler.postHandlerProtocol();

        // The following code block was migrated from the changePasswordHref_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleChangePassword();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public HREF getChangePasswordHref()
    {
        return (HREF) getChild(CHILD_CHANGEPASSWORDHREF);
    }

    //--Release2.1--start//
    //// Two new methods providing the business logic for the new link to toggle language.
    //// When touched this link should reload the page in opposite language (french versus english)
    //// and set this new session value.

    /**
     *
     *
     */
    public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this, true);

        handler.handleToggleLanguage();

        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getToggleLanguageHref()
    {
        return (HREF) getChild(CHILD_TOGGLELANGUAGEHREF);
    }

    /**
     *
     *
     */
    public StaticTextField getStPaymentTermDesc()
    {
        return (StaticTextField) getChild(CHILD_PAYMENTTERMDESC);
    }

    /**
     * Get LanguageId from the SessionStateModel
     *
     */
    private String getSessionLanguageId()
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        int langId = handler.getTheSessionState().getLanguageId();
        handler.pageSaveState();

        return new Integer(langId).toString();
    }

    //--Release2.1--end//

    /**
     *
     *
     */
    public void handleBtToolHistoryRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayDealHistory();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btToolHistory_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDisplayDealHistory();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtToolHistory()
    {
        return (Button) getChild(CHILD_BTTOOLHISTORY);
    }

    /**
     *
     *
     */
    public void handleBtTooNotesRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayDealNotes(true);

        handler.postHandlerProtocol();

        // The following code block was migrated from the btTooNotes_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDisplayDealNotes(true);

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtTooNotes()
    {
        return (Button) getChild(CHILD_BTTOONOTES);
    }

    /**
     *
     *
     */
    public void handleBtToolSearchRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDealSearch();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btToolSearch_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDealSearch();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtToolSearch()
    {
        return (Button) getChild(CHILD_BTTOOLSEARCH);
    }

    /**
     *
     *
     */
    public void handleBtToolLogRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleSignOff();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btToolLog_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleSignOff();

       handler.postHandlerProtocol();

       return;
         */
    }

    /**
     *
     *
     */
    public Button getBtToolLog()
    {
        return (Button) getChild(CHILD_BTTOOLLOG);
    }

    /**
     *
     *
     */
    public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayWorkQueue();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btWorkQueueLink_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDisplayWorkQueue();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtWorkQueueLink()
    {
        return (Button) getChild(CHILD_BTWORKQUEUELINK);
    }

    /**
     *
     *
     */
    public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handlePrevTaskPage();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btPrevTaskPage_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handlePrevTaskPage();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtPrevTaskPage()
    {
        return (Button) getChild(CHILD_BTPREVTASKPAGE);
    }

    /**
     *
     *
     */
    public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generatePrevTaskPage();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.generatePrevTaskPage();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleNextTaskPage();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btNextTaskPage_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleNextTaskPage();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtNextTaskPage()
    {
        return (Button) getChild(CHILD_BTNEXTTASKPAGE);
    }

    /**
     *
     *
     */
    public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateNextTaskPage();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.generateNextTaskPage();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStErrorFlag()
    {
        return (StaticTextField) getChild(CHILD_STERRORFLAG);
    }

    /**
     *
     *
     */
    public StaticTextField getStPrevTaskPageLabel()
    {
        return (StaticTextField) getChild(CHILD_STPREVTASKPAGELABEL);
    }

    /**
     *
     *
     */
    public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generatePrevTaskPage();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.generatePrevTaskPage();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStNextTaskPageLabel()
    {
        return (StaticTextField) getChild(CHILD_STNEXTTASKPAGELABEL);
    }

    /**
     *
     *
     */
    public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateNextTaskPage();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.generateNextTaskPage();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStTaskName()
    {
        return (StaticTextField) getChild(CHILD_STTASKNAME);
    }

    /**
     *
     *
     */
    public String endStTaskNameDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateTaskName();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.generateTaskName();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public HiddenField getHdDealId()
    {
        return (HiddenField) getChild(CHILD_HDDEALID);
    }

    /**
     *
     *
     */
    public HiddenField getHdDealCopyId()
    {
        return (HiddenField) getChild(CHILD_HDDEALCOPYID);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealNo()
    {
        return (StaticTextField) getChild(CHILD_STDEALNO);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedGDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDGDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombined3YrGDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINED3YRGDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedBorrowerGDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDBORROWERGDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedTDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDTDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombined3YrTDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINED3YRTDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedBorrowerTDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDBORROWERTDS);
    }

    /**
     *
     *
     */
    public pgDealEntryRepeatGDSTDSDetailsTiledView getRepeatGDSTDSDetails()
    {
        return (pgDealEntryRepeatGDSTDSDetailsTiledView) getChild(CHILD_REPEATGDSTDSDETAILS);
    }

    /**
     *
     *
     */
    public pgDealEntryRepeatApplicantTiledView getRepeatApplicant()
    {
        return (pgDealEntryRepeatApplicantTiledView) getChild(CHILD_REPEATAPPLICANT);
    }

    /**
     *
     *
     */
    public void handleBtAddApplicantRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        //logger = SysLog.getSysLogger("PGDE");
        Button addApplicant = getBtAddApplicant();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        //logger.debug("pgDealEntry@handleBtAddApplicantRequest::Before PreHandler");
        handler.preHandlerProtocol(this);

        //logger.debug("pgDealEntry@handleBtAddApplicantRequest::Before handleAddApplicant()");
        handler.handleAddApplicant(false);

        //logger.debug("pgDealEntry@handleBtAddApplicantRequest::After handleAddApplicant()");
        handler.postHandlerProtocol();

        //logger.debug("pgDealEntry@handleBtAddApplicantRequest::After postHandlerProtocol()");
        // The following code block was migrated from the btAddApplicant_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleAddApplicant(false);

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtAddApplicant()
    {
        return (Button) getChild(CHILD_BTADDAPPLICANT);
    }

    /**
     *
     *
     */
    public String endBtAddApplicantDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btAddApplicant_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayEditButton();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public void handleBtDupApplicantRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddApplicant(true);

        handler.postHandlerProtocol();

        // The following code block was migrated from the btDupApplicant_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleAddApplicant(true);

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtDupApplicant()
    {
        return (Button) getChild(CHILD_BTDUPAPPLICANT);
    }

    /**
     *
     *
     */
    public String endBtDupApplicantDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btDupApplicant_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayEditButton();

       handler.pageSaveState();

       return rc;
         */
    }


    /**
     *
     *
     */
/*    public Button getBtChangeSource()
    {
        return (Button) getChild(CHILD_BTCHANGESOURCE);
    }*/

    /**
     *
     *
     */
/*    public String endBtChangeSourceDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btChangeSource_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        //-- FXLink Phase II --//
        //-->   - Source cannot be changed if deal.ChannelMedia = 'E'
        //--> By Billy 18Nov2003
        boolean rc = handler.displayEditButton() && handler.isChangeSourceAllowed();

        //==========================================================
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }
*/
    /**
     *
     *
     * /
    public StaticTextField getStSourceFirmSourcePart()
    {
        return (StaticTextField) getChild(CHILD_STSOURCEFIRMSOURCEPART);
    }

    / **
     *
     *
     * /
    public StaticTextField getStSourceFirstName()
    {
        return (StaticTextField) getChild(CHILD_STSOURCEFIRSTNAME);
    }

    / **
     *
     *
     * /
    public StaticTextField getStSourceLastName()
    {
        return (StaticTextField) getChild(CHILD_STSOURCELASTNAME);
    }

    / **
     *
     *
     * /
    public StaticTextField getStSourceAddress()
    {
        return (StaticTextField) getChild(CHILD_STSOURCEADDRESS);
    }

    / **
     *
     *
     * /
    public StaticTextField getStSourcePhoneNo()
    {
        return (StaticTextField) getChild(CHILD_STSOURCEPHONENO);
    }

    / **
     *
     *
     * /
    public StaticTextField getStSourceFaxNo()
    {
        return (StaticTextField) getChild(CHILD_STSOURCEFAXNO);
    }*/

    /**
     *
     *
     */
    public TextField getTxReferenceSourceApp()
    {
        return (TextField) getChild(CHILD_TXREFERENCESOURCEAPP);
    }

    /**
     *
     *
     */
    public pgDealEntryRepeatPropertyInformationTiledView getRepeatPropertyInformation()
    {
        return (pgDealEntryRepeatPropertyInformationTiledView) getChild(CHILD_REPEATPROPERTYINFORMATION);
    }

    /**
     *
     *
     */
    public void handleBtAddPropertyRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddProperty();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btAddProperty_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleAddProperty();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtAddProperty()
    {
        return (Button) getChild(CHILD_BTADDPROPERTY);
    }

    /**
     *
     *
     */
    public String endBtAddPropertyDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btAddProperty_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayEditButton();

       handler.pageSaveState();

       return rc;
         */
    }

    // SEAN DJ SPEC-Progress Advance Type July 20, 2005: The getter methods.
    public ComboBox getCbProgressAdvanceType()
    {
        return (ComboBox) getChild(CHILD_CBPROGRESSADVANCETYPE);
    }
    // SEAN DJ SPEC-PAT END

    // SEAN GECF DOCUMENT TYPE DROP DOWN: the getter methods..
    public ComboBox getCbIVDocumentType()
    {
        return (ComboBox) getChild(CHILD_CBIVDOCUMENTTYPE);
    }

    public StaticTextField getStHideIVDocumentTypeStart()
    {
        return (StaticTextField) getChild(CHILD_STHIDEIVDOCUMENTTYPESTART);
    }

    public StaticTextField getStHideIVDocumentTypeEnd()
    {
        return (StaticTextField) getChild(CHILD_STHIDEIVDOCUMENTTYPEEND);
    }
    // EDN GECF DOCUMENT TYPE DROP DOWN

    /**
     *
     *
     */
    public ComboBox getCbDealPurpose()
    {
        return (ComboBox) getChild(CHILD_CBDEALPURPOSE);
    }

    /**
     *
     *
     */
    public ComboBox getCbDealType()
    {
        return (ComboBox) getChild(CHILD_CBDEALTYPE);
    }

    /**
     *
     *
     */
    public ComboBox getCbLOB()
    {
        return (ComboBox) getChild(CHILD_CBLOB);
    }

    /**
     *
     *
     */
    public ComboBox getCbLender()
    {
        return (ComboBox) getChild(CHILD_CBLENDER);
    }

    /**
     *
     *
     */
    public ComboBox getCbProduct()
    {
        return (ComboBox) getChild(CHILD_CBPRODUCT);
    }

    /**
     *
     *
     */
    public ComboBox getCbRateLocked()
    {
        return (ComboBox) getChild(CHILD_CBRATELOCKED);
    }

    /**
     *
     *
     */
    public ComboBox getCbChargeTerm()
    {
        return (ComboBox) getChild(CHILD_CBCHARGETERM);
    }

    /**
     *
     *
     */
    public ComboBox getCbPostedInterestRate()
    {
        return (ComboBox) getChild(CHILD_CBPOSTEDINTERESTRATE);
    }

    /**
     *
     *
     */
    public TextField getTxDiscount()
    {
        return (TextField) getChild(CHILD_TXDISCOUNT);
    }

    /**
     *
     *
     */
    public TextField getTxPremium()
    {
        return (TextField) getChild(CHILD_TXPREMIUM);
    }

    /**
     *
     *
     */
    public TextField getTxBuydownRate()
    {
        return (TextField) getChild(CHILD_TXBUYDOWNRATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStNetRate()
    {
        return (StaticTextField) getChild(CHILD_STNETRATE);
    }

    /**
     *
     *
     */
    public boolean beginStNetRateDisplay(ChildDisplayEvent event)
    {
        Object value = getStNetRate().getValue();

        return true;

        // The following code block was migrated from the stNetRate_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.handleStaticFieldSetup(this, event, "stNetRate", "doDealEntryMainSelect", "dfNetRate");

       handler.pageSaveState();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalEscrowPayment()
    {
        return (StaticTextField) getChild(CHILD_STTOTALESCROWPAYMENT);
    }

    /**
     *
     *
     */
    public TextField getTxRequestedLoanAmount()
    {
        return (TextField) getChild(CHILD_TXREQUESTEDLOANAMOUNT);
    }

    /**
     *
     *
     */
    public ComboBox getCbPayemntFrequencyTerm()
    {
        return (ComboBox) getChild(CHILD_CBPAYEMNTFREQUENCYTERM);
    }

    /**
     *
     *
     */
    public TextField getTxAmortizationYears()
    {
        return (TextField) getChild(CHILD_TXAMORTIZATIONYEARS);
    }

    /**
     *
     *
     */
    public TextField getTxAmortizationMonths()
    {
        return (TextField) getChild(CHILD_TXAMORTIZATIONMONTHS);
    }

    /**
     *
     *
     */
    public StaticTextField getStEffectiveAmortizationYears()
    {
        return (StaticTextField) getChild(CHILD_STEFFECTIVEAMORTIZATIONYEARS);
    }

    /**
     *
     *
     */
    public StaticTextField getStEffectiveAmortizationMonths()
    {
        return (StaticTextField) getChild(CHILD_STEFFECTIVEAMORTIZATIONMONTHS);
    }

    /**
     *
     *
     */
    public TextField getTxAdditionalPrincipalPayment()
    {
        return (TextField) getChild(CHILD_TXADDITIONALPRINCIPALPAYMENT);
    }

    /**
     *
     *
     */
    public TextField getTxPayTermYears()
    {
        return (TextField) getChild(CHILD_TXPAYTERMYEARS);
    }

    /**
     *
     *
     */
    public TextField getTxPayTermMonths()
    {
        return (TextField) getChild(CHILD_TXPAYTERMMONTHS);
    }

    /**
     *
     *
     */
    public StaticTextField getStPIPayment()
    {
        return (StaticTextField) getChild(CHILD_STPIPAYMENT);
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalPayment()
    {
        return (StaticTextField) getChild(CHILD_STTOTALPAYMENT);
    }

    /**
     *
     *
     */
    public ComboBox getCbRepaymentType()
    {
        return (ComboBox) getChild(CHILD_CBREPAYMENTTYPE);
    }
    
    /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4 *******************/
    public StaticTextField getStDefaultRepaymentTypeId()
    {
        return (StaticTextField) getChild(CHILD_STDEFAULTREPAYMENTTYPEID);
    }
    /***************MCM Impl team changes ends - XS_2.1, XS_2.2, XS_2.4 *******************/
    /**
     *
     *
     */
    public ComboBox getCbEstClosingDateMonths()
    {
        return (ComboBox) getChild(CHILD_CBESTCLOSINGDATEMONTHS);
    }

    /**
     *
     *
     */
    public TextField getTxEstClosingDateDay()
    {
        return (TextField) getChild(CHILD_TXESTCLOSINGDATEDAY);
    }

    /**
     *
     *
     */
    public TextField getTxEstClosingDateYear()
    {
        return (TextField) getChild(CHILD_TXESTCLOSINGDATEYEAR);
    }

    /**
     *
     *
     */
    public ComboBox getCbFirstPayDateMonth()
    {
        return (ComboBox) getChild(CHILD_CBFIRSTPAYDATEMONTH);
    }

    /**
     *
     *
     */
    public TextField getTxFirstPayDateDay()
    {
        return (TextField) getChild(CHILD_TXFIRSTPAYDATEDAY);
    }

    /**
     *
     *
     */
    public TextField getTxFirstPayDateYear()
    {
        return (TextField) getChild(CHILD_TXFIRSTPAYDATEYEAR);
    }

    /**
     *
     *
     */
    public StaticTextField getStMaturityDate()
    {
        return (StaticTextField) getChild(CHILD_STMATURITYDATE);
    }

    /**
     *
     *
     */
    public TextField getTxPreAppEstPurchasePrice()
    {
        return (TextField) getChild(CHILD_TXPREAPPESTPURCHASEPRICE);
    }

    /**
     *
     *
     */
    public ComboBox getCbSecondMortgageExist()
    {
        return (ComboBox) getChild(CHILD_CBSECONDMORTGAGEEXIST);
    }

    /**
     *
     *
     */
    public ComboBox getCbPrePaymentPenanlty()
    {
        return (ComboBox) getChild(CHILD_CBPREPAYMENTPENANLTY);
    }

    /**
     *
     *
     */
    public ComboBox getCbPrivilagePaymentOption()
    {
        return (ComboBox) getChild(CHILD_CBPRIVILAGEPAYMENTOPTION);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeFinancingProgramStart()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDEFINANCINGPROGRAMSTART);
    }

    /**
     *
     *
     */
    public ComboBox getCbFinancingProgram()
    {
        return (ComboBox) getChild(CHILD_CBFINANCINGPROGRAM);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeFinancingProgramEnd()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDEFINANCINGPROGRAMEND);
    }

    /**
     *
     *
     */
    public ComboBox getCbSpecialFeature()
    {
        return (ComboBox) getChild(CHILD_CBSPECIALFEATURE);
    }

    /**
     *
     *
     */
    public ComboBox getCbCommExpectedDateMonth()
    {
        return (ComboBox) getChild(CHILD_CBCOMMEXPECTEDDATEMONTH);
    }

    /**
     *
     *
     */
    public TextField getTxCommExpectedDateDay()
    {
        return (TextField) getChild(CHILD_TXCOMMEXPECTEDDATEDAY);
    }

    /**
     *
     *
     */
    public TextField getTxCommExpectedDateYear()
    {
        return (TextField) getChild(CHILD_TXCOMMEXPECTEDDATEYEAR);
    }
    
    //FFATE
    /**
    *
    *
    */
   public ComboBox getCbFinancingWaiverDateMonth()
   {
       return (ComboBox) getChild(CHILD_CBFINANCINGWAIVERDATEMONTH);
   }

   /**
    *
    *
    */
   public TextField getTxFinancingWaiverDateDay()
   {
       return (TextField) getChild(CHILD_TXFINANCINGWAIVERDATEDAY);
   }

   /**
    *
    *
    */
   public TextField getTxFinancingWaiverDateYear()
   {
       return (TextField) getChild(CHILD_TXFINANCINGWAIVERDATEYEAR);
   }
   //FFATE



    //CR03
    /**
     *
     *
     */
    public ComboBox getCbCommExpirationDateMonth()
    {
        return (ComboBox) getChild(CHILD_CBCOMMEXPIRATIONDATEMONTH);
    }

    /**
     *
     *
     */
    public TextField getTxCommExpirationDateDay()
    {
        return (TextField) getChild(CHILD_TXCOMMEXPIRATIONDATEDAY);
    }

    /**
     *
     *
     */
    public TextField getTxCommExpirationDateYear()
    {
        return (TextField) getChild(CHILD_TXCOMMEXPIRATIONDATEYEAR);
    }
    //CR03

    /**
     *
     *
     */
    public StaticTextField getStCommitmentPeriod()
    {
        return (StaticTextField) getChild(CHILD_STCOMMITMENTPERIOD);
    }

    /**
     *
     *
     */
    public StaticTextField getStCommExpDate()
    {
        return (StaticTextField) getChild(CHILD_STCOMMEXPDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStCommAcceptanceDate()
    {
        return (StaticTextField) getChild(CHILD_STCOMMACCEPTANCEDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeLoanAmount()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGELOANAMOUNT);
    }

    /**
     *
     *
     */
    public TextField getTxAdvanceHold()
    {
        return (TextField) getChild(CHILD_TXADVANCEHOLD);
    }

//  PPI start
    /**
     *
     *
     */
    public TextField getTxDealPurposeTypeHiddenStart()
    {
        return (TextField) getChild(CHILD_TXDEALPURPOSETYPEHIDDENSTART);
    }

    /**
     *
     *
     */
    public TextField getTxImprovedValue()
    {
        return (TextField) getChild(CHILD_TXIMPROVEDVALUE);
    }

    /**
     *
     *
     */
    public TextField getTxDealPurposeTypeHiddenEnd()
    {
        return (TextField) getChild(CHILD_TXDEALPURPOSETYPEHIDDENEND);
    }
//  PPI end


    /**
     *
     *
     */
    public void handleBtReviewBridgeRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the btReviewBridge_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleReviewBridge();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtReviewBridge()
    {
        return (Button) getChild(CHILD_BTREVIEWBRIDGE);
    }

    /**
     *
     *
     */
    public String endBtReviewBridgeDisplay(ChildContentDisplayEvent event)
    {
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       return(SKIP);
         */

        //// This button should never appear in the current version. If we need to get
        //// later on under different circumstanses the logic should be added as usual.
        ////return event.getContent();
        return "";
    }

    /**
     *
     *
     */
    public pgDealEntryRepeatedDownPaymentTiledView getRepeatedDownPayment()
    {
        return (pgDealEntryRepeatedDownPaymentTiledView) getChild(CHILD_REPEATEDDOWNPAYMENT);
    }

    /**
     *
     *
     */
    public void handleBtAddDownPaymentRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddDownpayment();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btAddDownPayment_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleAddDownpayment();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtAddDownPayment()
    {
        return (Button) getChild(CHILD_BTADDDOWNPAYMENT);
    }

    /**
     *
     *
     */
    public String endBtAddDownPaymentDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btAddDownPayment_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayEditButton();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStDownPaymentRequired()
    {
        return (StaticTextField) getChild(CHILD_STDOWNPAYMENTREQUIRED);
    }

    /**
     *
     *
     */
    public pgDealEntryRepeatedEscrowDetailsTiledView getRepeatedEscrowDetails()
    {
        return (pgDealEntryRepeatedEscrowDetailsTiledView) getChild(CHILD_REPEATEDESCROWDETAILS);
    }

    /**
     *
     *
     */
    public void handleBtAddEscrowRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleAddEscrowPayment();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btAddEscrow_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleAddEscrowPayment();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtAddEscrow()
    {
        return (Button) getChild(CHILD_BTADDESCROW);
    }

    /**
     *
     *
     */
    public String endBtAddEscrowDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btAddEscrow_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayEditButton();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public ComboBox getCbBranch()
    {
        return (ComboBox) getChild(CHILD_CBBRANCH);
    }

    /**
     *
     *
     */
    public boolean beginCbBranchDisplay(ChildDisplayEvent event)
    {
        Object value = getCbBranch().getValue();

        return true;

        // The following code block was migrated from the cbBranch_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.handleSetBranch((DisplayField) event.getSource());

       handler.pageSaveState();

       return;
         */
    }

    /**
     *
     *
     */
    public TextField getTxRefExisitingMTG()
    {
        return (TextField) getChild(CHILD_TXREFEXISITINGMTG);
    }

    /**
     *
     *
     */
    public ComboBox getCbCrossSell()
    {
        return (ComboBox) getChild(CHILD_CBCROSSSELL);
    }

    /**
     *
     *
     */
    public ComboBox getCbTaxPayor()
    {
        return (ComboBox) getChild(CHILD_CBTAXPAYOR);
    }

    /**
     *
     *
     */
    public void handleBtPartySummaryRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDisplayPartySummary();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btPartySummary_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDisplayPartySummary();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtPartySummary()
    {
        return (Button) getChild(CHILD_BTPARTYSUMMARY);
    }

    /**
     *
     *
     */
    public StaticTextField getStReferenceType()
    {
        return (StaticTextField) getChild(CHILD_STREFERENCETYPE);
    }

    /**
     *
     *
     */
    public StaticTextField getStReferenceDealNo()
    {
        return (StaticTextField) getChild(CHILD_STREFERENCEDEALNO);
    }

    /**
     *
     *
     */
    public ComboBox getCbMIIndicator()
    {
        return (ComboBox) getChild(CHILD_CBMIINDICATOR);
    }

    /**
     *
     *
     */
    public ComboBox getCbMIType()
    {
        return (ComboBox) getChild(CHILD_CBMITYPE);
    }

    /**
     *
     *
     */
    public ComboBox getCbMIInsurer()
    {
        return (ComboBox) getChild(CHILD_CBMIINSURER);
    }

    /**
     *
     *
     */
    public ComboBox getCbMIPayor()
    {
        return (ComboBox) getChild(CHILD_CBMIPAYOR);
    }

    /**
     *
     *
     */
    public TextField getTxMIPremium()
    {
        return (TextField) getChild(CHILD_TXMIPREMIUM);
    }

    /**
     *
     *
     */
    public RadioButtonGroup getRbMIUpfront()
    {
        return (RadioButtonGroup) getChild(CHILD_RBMIUPFRONT);
    }

    /**
     *
     *
     */
    public RadioButtonGroup getRbMIRUIntervention()
    {
        return (RadioButtonGroup) getChild(CHILD_RBMIRUINTERVENTION);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIPreQCertNum()
    {
        return (StaticTextField) getChild(CHILD_STMIPREQCERTNUM);
    }


    /**
     *CR03
     *
     */
    public RadioButtonGroup getRbAutoCalCommitDate()
    {
        return (RadioButtonGroup) getChild(CHILD_RBAUTOCALCOMMITDATE);
    }

    /**
     *
     *
     */
    public void handleBtDealSubmitRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        //logger = SysLog.getSysLogger("PGDE");
        Button dealSubmit = getBtDealSubmit();

        //this clears the ETO fields based on deal purpose selected - Ticket 716
        clearETOFields();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        //logger.debug("pgDealEntry@handleBtDealSubmitRequest::Before PreHandler");
        handler.preHandlerProtocol(this);

        //logger.debug("pgDealEntry@handleBtDealSubmitRequest::Before handleAddApplicant()");
        handler.handleDESubmit();

        //logger.debug("pgDealEntry@handleBtDealSubmitRequest::After handleDESubmit()");
        handler.postHandlerProtocol();

        //logger.debug("pgDealEntry@handleBtDealSubmitRequest::After postHandlerProtocol()");
        // The following code block was migrated from the btDealSubmit_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleDESubmit();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtDealSubmit()
    {
        return (Button) getChild(CHILD_BTDEALSUBMIT);
    }

    /**
     *
     *
     */
    public String endBtDealSubmitDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btDealSubmit_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displaySubmitButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displaySubmitButton();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public void handleBtDealCancelRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        //logger = SysLog.getSysLogger("PGDE");
        //// Pre-finished only, but enough to test this functionality.
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        //logger.debug("pgDealEntry@handleBtDealCancelRequest::Before PreHandler");
        handler.preHandlerProtocol(this);

        //logger.debug("pgDealEntry@handleBtDealSubmitRequest::Before handleAddApplicant()");
        handler.handleCancelStandard();

        //logger.debug("pgDealEntry@handleBtDealSubmitRequest::After handleDESubmit()");
        handler.postHandlerProtocol();

        //logger.debug("pgDealEntry@handleBtDealSubmitRequest::After postHandlerProtocol()");
        // The following code block was migrated from the btDealCancel_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleCancelStandard();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtDealCancel()
    {
        return (Button) getChild(CHILD_BTDEALCANCEL);
    }

    /**
     *
     *
     */
    public String endBtDealCancelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btDealCancel_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayCancelButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayCancelButton();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStPmGenerate()
    {
        return (StaticTextField) getChild(CHILD_STPMGENERATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmHasTitle()
    {
        return (StaticTextField) getChild(CHILD_STPMHASTITLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmHasInfo()
    {
        return (StaticTextField) getChild(CHILD_STPMHASINFO);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmHasTable()
    {
        return (StaticTextField) getChild(CHILD_STPMHASTABLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmHasOk()
    {
        return (StaticTextField) getChild(CHILD_STPMHASOK);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmTitle()
    {
        return (StaticTextField) getChild(CHILD_STPMTITLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmInfoMsg()
    {
        return (StaticTextField) getChild(CHILD_STPMINFOMSG);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmOnOk()
    {
        return (StaticTextField) getChild(CHILD_STPMONOK);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmMsgs()
    {
        return (StaticTextField) getChild(CHILD_STPMMSGS);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmMsgTypes()
    {
        return (StaticTextField) getChild(CHILD_STPMMSGTYPES);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmGenerate()
    {
        return (StaticTextField) getChild(CHILD_STAMGENERATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmHasTitle()
    {
        return (StaticTextField) getChild(CHILD_STAMHASTITLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmHasInfo()
    {
        return (StaticTextField) getChild(CHILD_STAMHASINFO);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmHasTable()
    {
        return (StaticTextField) getChild(CHILD_STAMHASTABLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmTitle()
    {
        return (StaticTextField) getChild(CHILD_STAMTITLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmInfoMsg()
    {
        return (StaticTextField) getChild(CHILD_STAMINFOMSG);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmMsgs()
    {
        return (StaticTextField) getChild(CHILD_STAMMSGS);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmMsgTypes()
    {
        return (StaticTextField) getChild(CHILD_STAMMSGTYPES);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmDialogMsg()
    {
        return (StaticTextField) getChild(CHILD_STAMDIALOGMSG);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmButtonsHtml()
    {
        return (StaticTextField) getChild(CHILD_STAMBUTTONSHTML);
    }

    /**
     *
     *
     */
    public StaticTextField getStTargetDownPayment()
    {
        return (StaticTextField) getChild(CHILD_STTARGETDOWNPAYMENT);
    }

    /**
     *
     *
     */
    public String endStTargetDownPaymentDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTargetDownPayment_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateLoadTarget(Mc.TARGET_DE_DOWNPAYMENT);

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int retval = handler.generateLoadTarget(Mc.TARGET_DE_DOWNPAYMENT);

       handler.pageSaveState();

       return retval;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStTargetEscrow()
    {
        return (StaticTextField) getChild(CHILD_STTARGETESCROW);
    }

    /**
     *
     *
     */
    public String endStTargetEscrowDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTargetEscrow_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.generateLoadTarget(Mc.TARGET_DE_ESCROW);

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int retval = handler.generateLoadTarget(Mc.TARGET_DE_ESCROW);

       handler.pageSaveState();

       return retval;
         */
    }

    /**
     *
     *
     */
    public TextField getTxTotalDown()
    {
        return (TextField) getChild(CHILD_TXTOTALDOWN);
    }

    /**
     *
     *
     */
    public TextField getTxTotalEscrow()
    {
        return (TextField) getChild(CHILD_TXTOTALESCROW);
    }

    /**
     *
     *
     */
    public TextField getTbPaymentTermDescription()
    {
        return (TextField) getChild(CHILD_TBPAYMENTTERMDESCRIPTION);
    }

    /**
     *
     *
     */
    public TextField getTbRateCode()
    {
        return (TextField) getChild(CHILD_TBRATECODE);
    }

    /**
     *
     *
     */
    public StaticTextField getStRateTimeStamp()
    {
        return (StaticTextField) getChild(CHILD_STRATETIMESTAMP);
    }

    /**
     *
     *
     */
    public boolean beginStRateTimeStampDisplay(ChildDisplayEvent event)
    {
        Object value = getStRateTimeStamp().getValue();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        ////handler.handleRateTimeStampPassToHtml(this, event, "stRateTimeStamp", "doDealEntryMainSelect", "dfRateDate");
        handler.handleRateTimeStampPassToHtml(this, event);

        handler.pageSaveState();

        return true;

        // The following code block was migrated from the stRateTimeStamp_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.handleRateTimeStampPassToHtml(this, event, "stRateTimeStamp", "doDealEntryMainSelect", "dfRateDate");

       handler.pageSaveState();

       return;
         */
    }

    /**
     *
     *
     */

    ////--Release2.1--start//
    //// New logic for Lender-->Product-->Rate module based on the
    //// rateInventoryId comparison.

    /**
     *
     *
     */
    public StaticTextField getStRateInventoryId()
    {
        return (StaticTextField) getChild(CHILD_STRATEINVENTORYID);
    }

    public boolean beginStRateInventoryIdDisplay(ChildDisplayEvent event)
    {
        Object value = getStRateTimeStamp().getValue();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.handleRateInventoryIdPassToHtml(this, event);

        handler.pageSaveState();

        return true;
    }

    /**
     *
     *
     */

    //// New HiddenField to get the paymentTermId  from JavaScript and propagate into
    //// the database.
    public HiddenField getHdPaymentTermId()
    {
        return (HiddenField) getChild(CHILD_HDPAYMENTTERMID);
    }

    //CR03
    public HiddenField getHdCommitmentExpiryDate()
    {
        return (HiddenField) getChild(CHILD_HDCOMMITMENTEXPIRYDATE);
    }


    public HiddenField getHdMosProperty()
    {
        return (HiddenField) getChild(CHILD_HDMOSPROPERTY);
    }
    //CR03

    public boolean beginHdPaymentTermIdDisplay(ChildDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        handler.handlePaymentTermIdPassToHtml(this, event);

        handler.pageSaveState();

        return true;
    }

    ////--Release2.1--end//
    ////--Release2.1--end//

    /**
     *
     *
     */
    public HiddenField getHdLongPostedDate()
    {
        return (HiddenField) getChild(CHILD_HDLONGPOSTEDDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStLenderProductId()
    {
        return (StaticTextField) getChild(CHILD_STLENDERPRODUCTID);
    }

    /**
     *
     *
     */
    public boolean beginStLenderProductIdDisplay(ChildDisplayEvent event)
    {
        Object value = getStLenderProductId().getValue();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        ////handler.handleProductDealIdPassToHtml(this, event, "stLenderProductId", "doDealEntryMainSelect", "dfProductId");
        handler.handleProductDealIdPassToHtml(this, event);

        handler.pageSaveState();

        return true;

        // The following code block was migrated from the stLenderProductId_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.handleProductDealIdPassToHtml(this, event, "stLenderProductId", "doDealEntryMainSelect", "dfProductId");

       handler.pageSaveState();

       return;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStLenderProfileId()
    {
        return (StaticTextField) getChild(CHILD_STLENDERPROFILEID);
    }

    /**
     *
     *
     */
    public boolean beginStLenderProfileIdDisplay(ChildDisplayEvent event)
    {
        //---> BUG !!!
        //SysLog.getSysLogger("PGDE");
        Object value = getStLenderProfileId().getValue();

        ////logger.debug("pgDEVB@beginStLenderProfileIdDisplay::Value: " + value.toString());
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        //logger.debug("pgDEVB@beginStLenderProfileIdDisplay::before StaticPass call");
        ////handler.handleStaticPassToHtml(this, event, "stLenderProfileId", "doDealEntryMainSelect", "dfLenderProfile");
        handler.handleStaticPassToHtml(this, event);

        //logger.debug("pgDEVB@beginStLenderProfileIdDisplay::after StaticPass call");
        handler.pageSaveState();

        return true;

        // The following code block was migrated from the stLenderProfileId_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /**
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();
       handler.pageGetState(this);
       handler.handleStaticPassToHtml(this, event, "stLenderProfileId", "doDealEntryMainSelect", "dfLenderProfile");
       handler.pageSaveState();
       return;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeDSSstart()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDEDSSSTART);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeDSSend()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDEDSSEND);
    }

    //--DisableProductRateTicket#570--10Aug2004--start--//

    /**
     *
     *
     */
    public StaticTextField getStDealRateStatusForServlet()
    {
        return (StaticTextField) getChild(CHILD_STDEALRATESTATUSFORSERVLET);
    }

    public StaticTextField getStRateDisDateForServlet()
    {
        return (StaticTextField) getChild(CHILD_STRATEDISDATEFORSERVLET);
    }

    public StaticTextField getStIsPageEditableForServlet()
    {
        return (StaticTextField) getChild(CHILD_STISPAGEEDITABLEFORSERVLET);
    }

    //--DisableProductRateTicket#570--10Aug2004--end--//

    /**
     *
     *
     */
    public StaticTextField getStDealId()
    {
        return (StaticTextField) getChild(CHILD_STDEALID);
    }

    /**
     *
     *
     */
    public StaticTextField getStBorrFirstName()
    {
        return (StaticTextField) getChild(CHILD_STBORRFIRSTNAME);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealStatus()
    {
        return (StaticTextField) getChild(CHILD_STDEALSTATUS);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealStatusDate()
    {
        return (StaticTextField) getChild(CHILD_STDEALSTATUSDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStLOB()
    {
        return (StaticTextField) getChild(CHILD_STLOB);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealType()
    {
        return (StaticTextField) getChild(CHILD_STDEALTYPE);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealPurpose()
    {
        return (StaticTextField) getChild(CHILD_STDEALPURPOSE);
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalLoanAmount()
    {
        return (StaticTextField) getChild(CHILD_STTOTALLOANAMOUNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmtTerm()
    {
        return (StaticTextField) getChild(CHILD_STPMTTERM);
    }

    /**
     *
     *
     */
    public StaticTextField getStEstClosingDate()
    {
        return (StaticTextField) getChild(CHILD_STESTCLOSINGDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStSource()
    {
        return (StaticTextField) getChild(CHILD_STSOURCE);
    }

    /**
     *
     *
     */
    public StaticTextField getStSourceFirm()
    {
        return (StaticTextField) getChild(CHILD_STSOURCEFIRM);
    }

    /**
     *
     *
     */
    public StaticTextField getStPurchasePrice()
    {
        return (StaticTextField) getChild(CHILD_STPURCHASEPRICE);
    }

    /**
     *
     *
     */
    public StaticTextField getStSpecialFeature()
    {
        return (StaticTextField) getChild(CHILD_STSPECIALFEATURE);
    }

    /**
     *
     *
     */
    public void handleBtOKRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleCancelStandard();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btOK_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleCancelStandard();

       handler.postHandlerProtocol();

       return PROCEED;
         */
    }

    /**
     *
     *
     */
    public Button getBtOK()
    {
        return (Button) getChild(CHILD_BTOK);
    }

    /**
     *
     *
     */
    public String endBtOKDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btOK_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayOKButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayOKButton();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStPageTitle()
    {
        return (StaticTextField) getChild(CHILD_STPAGETITLE);
    }

    /**
     *
     *
     */
    public void handleBtRessurectRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleResurrect();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btRessurect_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleResurrect();

       handler.postHandlerProtocol();

       return;
         */
    }

    /**
     *
     *
     */
    public Button getBtRessurect()
    {
        return (Button) getChild(CHILD_BTRESSURECT);
    }

    /**
     *
     *
     */
    public String endBtRessurectDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btRessurect_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayRessurectButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayRessurectButton();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public void handleBtPreapprovalFirmRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handlePreapprovalFirm();

        handler.postHandlerProtocol();

        // The following code block was migrated from the btPreapprovalFirm_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handlePreapprovalFirm();

       handler.postHandlerProtocol();

       return;
         */
    }

    /**
     *
     *
     */
    public Button getBtPreapprovalFirm()
    {
        return (Button) getChild(CHILD_BTPREAPPROVALFIRM);
    }

    /**
     *
     *
     */
    public String endBtPreapprovalFirmDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btPreapprovalFirm_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayPreapprovalButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayPreapprovalButton();

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStAppDateForServlet()
    {
        return (StaticTextField) getChild(CHILD_STAPPDATEFORSERVLET);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIStatus()
    {
        return (StaticTextField) getChild(CHILD_STMISTATUS);
    }

    /**
     *
     *
     */
    public TextField getTxMIExistingPolicy()
    {
        return (TextField) getChild(CHILD_TXMIEXISTINGPOLICY);
    }

    /**
     *
     *
     */

    //===============================================================
    // New fields to handle MIPolicyNum problem :
    //  the number lost if user cancel MI and re-send again later.
    // -- Modified by Billy 16July2003
    public TextField getTxMICertificate()
    {
        return (TextField) getChild(CHILD_TXMICERTIFICATE);
    }

    public HiddenField getHdMIPolicyNoCMHC()
    {
        return (HiddenField) getChild(CHILD_HDMIPOLICYNOCMHC);
    }

    public HiddenField getHdMIPolicyNoGE()
    {
        return (HiddenField) getChild(CHILD_HDMIPOLICYNOGE);
    }

    //==============================================================

    /**
     *
     *
     */
    public StaticTextField getStIncludeCommitmentStart()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDECOMMITMENTSTART);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeCommitmentEnd()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDECOMMITMENTEND);
    }

    /**
     *
     *
     */
    public StaticTextField getStSourcePhoneNoExtension()
    {
        return (StaticTextField) getChild(CHILD_STSOURCEPHONENOEXTENSION);
    }

    /**
     *
     *
     */
    public String endStSourcePhoneNoExtensionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stSourcePhoneNoExtension_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        String ext =
            handler.displayPhoneExtension((StaticTextField) (this.getStViewOnlyTag()));

        handler.pageSaveState();

        return ext;

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayPhoneExtension((DisplayField) event.getSource());

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStRateLock()
    {
        return (StaticTextField) getChild(CHILD_STRATELOCK);
    }

    /**
     *
     *
     */
    public boolean beginStRateLockDisplay(ChildDisplayEvent event)
    {
        Object value = getStRateLock().getValue();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        ////handler.handleRateLockPassToHtml(this, event, "stRateLock", "doDealRateLock", "dfRateLock");
        handler.handleRateLockPassToHtml(this, event);

        handler.pageSaveState();

        return true;

        // The following code block was migrated from the stRateLock_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.handleRateLockPassToHtml(this, event, "stRateLock", "doDealRateLock", "dfRateLock");

       handler.pageSaveState();

       return;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStDealEntry()
    {
        return (StaticTextField) getChild(CHILD_STDEALENTRY);
    }

    /**
     *
     *
     */
    public boolean beginStDealEntryDisplay(ChildDisplayEvent event)
    {
        Object value = getStDealEntry().getValue();

        //logger = SysLog.getSysLogger("pgDE");
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        //logger.debug("pgDE@beginStDealEntryDisplay::Before pass to html");
        handler.handleIsPageEntryPassToHtml(this, event);

        //logger.debug("pgDE@beginStDealEntryDisplay::After pass to html");
        handler.pageSaveState();

        return true;

        // The following code block was migrated from the stDealEntry_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.handleIsPageEntryPassToHtml(this, event, "stDealEntry");

       handler.pageSaveState();

       return;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStDefaultProductId()
    {
        return (StaticTextField) getChild(CHILD_STDEFAULTPRODUCTID);
    }

    /**
     *
     *
     */
    public boolean beginStDefaultProductIdDisplay(ChildDisplayEvent event)
    {
        Object value = getStDefaultProductId().getValue();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        ////handler.handleDefaultProductPassToHtml(this, event, "stDefaultProductId", "doDealGetDefaultProduct", "dfMtgProdId");
        handler.handleDefaultProductPassToHtml(this, event);

        handler.pageSaveState();

        return true;

        // The following code block was migrated from the stDefaultProductId_onBeforeDisplayEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       handler.handleDefaultProductPassToHtml(this, event, "stDefaultProductId", "doDealGetDefaultProduct", "dfMtgProdId");

       handler.pageSaveState();

       return;
         */
    }

    /**
     *
     *
     */
    public StaticTextField getStViewOnlyTag()
    {
        return (StaticTextField) getChild(CHILD_STVIEWONLYTAG);
    }

    /**
     *
     *
     */
    public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        String rc = handler.displayViewOnlyTag();

        handler.pageSaveState();

        return rc;

        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.pageGetState(this);

       int rc = handler.displayViewOnlyTag((DisplayField) event.getSource());

       handler.pageSaveState();

       return rc;
         */
    }

    /**
     *
     *
     */
    public RadioButtonGroup getRbProgressAdvance()
    {
        return (RadioButtonGroup) getChild(CHILD_RBPROGRESSADVANCE);
    }

    /**
     *
     *
     */
    public TextField getTxNextAdvanceAmount()
    {
        return (TextField) getChild(CHILD_TXNEXTADVANCEAMOUNT);
    }

    /**
     *
     *
     */
    public TextField getTxAdvanceToDateAmt()
    {
        return (TextField) getChild(CHILD_TXADVANCETODATEAMT);
    }

    /**
     *
     *
     */
    public TextField getTxAdvanceNumber()
    {
        return (TextField) getChild(CHILD_TXADVANCENUMBER);
    }

    /**
     *
     *
     */
    public ComboBox getCbRefiOrigPurchaseDateMonth()
    {
        return (ComboBox) getChild(CHILD_CBREFIORIGPURCHASEDATEMONTH);
    }

    /**
     *
     *
     */
    public TextField getTxRefiOrigPurchaseDateYear()
    {
        return (TextField) getChild(CHILD_TXREFIORIGPURCHASEDATEYEAR);
    }

    /**
     *
     *
     */
    public TextField getTxRefiOrigPurchaseDateDay()
    {
        return (TextField) getChild(CHILD_TXREFIORIGPURCHASEDATEDAY);
    }

    /**
     *
     *
     */
    public TextField getTxRefiPurpose()
    {
        return (TextField) getChild(CHILD_TXREFIPURPOSE);
    }

    /**
     *
     *
     */
    public TextField getTxRefiMortgageHolder()
    {
        return (TextField) getChild(CHILD_TXREFIMORTGAGEHOLDER);
    }

    /**
     *
     *
     */
    public RadioButtonGroup getRbBlendedAmortization()
    {
        return (RadioButtonGroup) getChild(CHILD_RBBLENDEDAMORTIZATION);
    }

    /**
     *
     *
     */
    public TextField getTxRefiOrigPurchasePrice()
    {
        return (TextField) getChild(CHILD_TXREFIORIGPURCHASEPRICE);
    }

    /**
     *
     *
     */
    public TextField getTxRefiImprovementValue()
    {
        return (TextField) getChild(CHILD_TXREFIIMPROVEMENTVALUE);
    }

    /**
     *
     *
     */
    public TextField getTxRefiOrigMtgAmount()
    {
        return (TextField) getChild(CHILD_TXREFIORIGMTGAMOUNT);
    }

    /**
     *
     *
     */
    public TextField getTxRefiOutstandingMtgAmount()
    {
        return (TextField) getChild(CHILD_TXREFIOUTSTANDINGMTGAMOUNT);
    }

    /**
     *
     *
     */
    public TextField getTxRefiImprovementsDesc()
    {
        return (TextField) getChild(CHILD_TXREFIIMPROVEMENTSDESC);
    }

    /**
     *
     *
     */
    public StaticTextField getStVALSData()
    {
        return (StaticTextField) getChild(CHILD_STVALSDATA);
    }

    /**
     *
     *
     */
    public void handleBtRecalculateRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        updateCashBackAmount();

        //this clears the ETO fields based on deal purpose selected - Ticket 716
        clearETOFields();

        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleRecalculate(true);

        handler.postHandlerProtocol();

        // The following code block was migrated from the btRecalculate_onWebEvent method
        // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

        /*
       DealEntryHandler handler =(DealEntryHandler) this.handler.cloneSS();

       handler.preHandlerProtocol(this);

       handler.handleRecalculate(true);

       handler.postHandlerProtocol();

       return;
         */
    }

    /**
     *
     *
     */
    public Button getBtRecalculate()
    {
        return (Button) getChild(CHILD_BTRECALCULATE);
    }

    /**
     *
     *
     */
    public String endBtRecalculateDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btRecalculate_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayEditButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    /**
     *
     *
     */
    public doDealEntryMainSelectModel getdoDealEntryMainSelectModel()
    {
        if (doDealEntryMainSelect == null)
        {
            doDealEntryMainSelect =
                (doDealEntryMainSelectModel) getModel(doDealEntryMainSelectModel.class);
        }

        return doDealEntryMainSelect;
    }

    /**
     *
     *
     */
    public void setdoDealEntryMainSelectModel(doDealEntryMainSelectModel model)
    {
        doDealEntryMainSelect = model;
    }

    /**
     *
     *
     */
    public doDealEntrySourceInfoModel getdoDealEntrySourceInfoModel()
    {
    	// Oct 28, 2010. FXP30439, Midori told me not to delete this garbage.
    	if(true) throw new ExpressRuntimeException("don't call this method this is obsolute!");
    	
        if (doDealEntrySourceInfo == null)
        {
            doDealEntrySourceInfo =
                (doDealEntrySourceInfoModel) getModel(doDealEntrySourceInfoModel.class);
        }

        return doDealEntrySourceInfo;
    }

    //--DJ_PT_CR--start//

    /**
     *
     *
     */
    public doPartySummaryModel getdoPartySummaryModel()
    {
        if (doPartySummary == null)
        {
            doPartySummary =
                (doPartySummaryModel) getModel(doPartySummaryModel.class);
        }

        return doPartySummary;
    }

    /**
     *
     *
     */
    public void setdoPartySummaryModel(doPartySummaryModel model)
    {
        doPartySummary = model;
    }

    //--DJ_PT_CR--end//
    //--DJ_LDI_CR--start--//

    /**
     *
     *
     */
    public void handleBtLDInsuranceDetailsRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleLDInsuranceDetails();

        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtLDInsuranceDetails()
    {
        return (Button) getChild(CHILD_BTLDINSURANCEDETAILS);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmntIncludingLifeDisability()
    {
        return (StaticTextField) getChild(CHILD_STPMNTINCLUDINGLIFEDISABILITY);
    }

    /**
     *
     *
     */
    public String endBtLDInsuranceDetailsDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the btRessurect_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.pageGetState(this);

        boolean rc = handler.displayInsuranceDetailsButton();

        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeLifeDisLabelsStart()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDELIFEDISLABELSSTART);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeLifeDisLabelsEnd()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDELIFEDISLABELSEND);
    }

    /**
     *
     *
     */
    public StaticTextField getStGdsTdsDetailsLabel()
    {
        return (StaticTextField) getChild(CHILD_STGDSTDSDETAILSLABEL);
    }

    //--DJ_LDI_CR--end--//
    //--DJ_CR010--start//

    /**
     *
     *
     */
    public TextField getTxCommisionCode()
    {
        return (TextField) getChild(CHILD_TXCOMMISIONCODE);
    }

    //--DJ_CR010--end//
    //--DJ_CR203.1--start//

    /**
     *
     *
     */
    public TextField getTxProprietairePlusLOC()
    {
        return (TextField) getChild(CHILD_TXPROPRIETAIREPLUSLOC);
    }

    /**
     *
     *
     */
    public RadioButtonGroup getRbMultiProject()
    {
        return (RadioButtonGroup) getChild(CHILD_RBMULTIPROJECT);
    }

    /**
     *
     *
     */
    public RadioButtonGroup getRbProprietairePlus()
    {
        return (RadioButtonGroup) getChild(CHILD_RBPROPRIETAIREPLUS);
    }

    //--DJ_CR203.1--end//

    /**
     *
     *
     */
    public void setdoDealEntrySourceInfoModel(doDealEntrySourceInfoModel model)
    {
        doDealEntrySourceInfo = model;
    }

    /**
     *
     *
     */
    public doDealEntryBridgeModel getdoDealEntryBridgeModel()
    {
        if (doDealEntryBridge == null)
        {
            doDealEntryBridge =
                (doDealEntryBridgeModel) getModel(doDealEntryBridgeModel.class);
        }

        return doDealEntryBridge;
    }

    /**
     *
     *
     */
    public void setdoDealEntryBridgeModel(doDealEntryBridgeModel model)
    {
        doDealEntryBridge = model;
    }

    /**
     *
     *
     */
    public doDealGetReferenceInfoModel getdoDealGetReferenceInfoModel()
    {
        if (doDealGetReferenceInfo == null)
        {
            doDealGetReferenceInfo =
                (doDealGetReferenceInfoModel) getModel(doDealGetReferenceInfoModel.class);
        }

        return doDealGetReferenceInfo;
    }

    /**
     *
     *
     */
    public void setdoDealGetReferenceInfoModel(doDealGetReferenceInfoModel model)
    {
        doDealGetReferenceInfo = model;
    }

    /**
     *
     *
     */
    public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
    {
        if (doDealSummarySnapShot == null)
        {
            doDealSummarySnapShot =
                (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
        }

        return doDealSummarySnapShot;
    }

    /**
     *
     *
     */
    public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
    {
        doDealSummarySnapShot = model;
    }

    /**
     *
     *
     */
    public doDealMIStatusModel getdoDealMIStatusModel()
    {
        if (doDealMIStatus == null)
        {
            doDealMIStatus =
                (doDealMIStatusModel) getModel(doDealMIStatusModel.class);
        }

        return doDealMIStatus;
    }

    /**
     *
     *
     */
    public void setdoDealMIStatusModel(doDealMIStatusModel model)
    {
        doDealMIStatus = model;
    }

    /**
     *
     *
     */

    //// New hidden button for the ActiveMessage 'fake' submit button.
    public Button getBtActMsg()
    {
        return (Button) getChild(CHILD_BTACTMSG);
    }

    //// Populate previous links set of methods.
    public String endHref1Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
        handler.pageSaveState();

        return rc;
    }

    public String endHref2Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
        handler.pageSaveState();

        return rc;
    }

    public String endHref3Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
        handler.pageSaveState();

        return rc;
    }

    public String endHref4Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
        handler.pageSaveState();

        return rc;
    }

    public String endHref5Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
        handler.pageSaveState();

        return rc;
    }

    public String endHref6Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
        handler.pageSaveState();

        return rc;
    }

    public String endHref7Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
        handler.pageSaveState();

        return rc;
    }

    public String endHref8Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
        handler.pageSaveState();

        return rc;
    }

    public String endHref9Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
        handler.pageSaveState();

        return rc;
    }

    public String endHref10Display(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
        handler.pageSaveState();

        return rc;
    }

    //=====================================================
    //// This method overrides the getDefaultURL() framework JATO method and it is located
    //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
    //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
    //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
    //// The full method is still in PHC base class. It should care the
    //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
    public String getDisplayURL()
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String url = getDefaultDisplayURL();

        int languageId = handler.theSessionState.getLanguageId();

        //// Call the language specific URL (business delegation done in the BXResource).
        if ((url != null) && !url.trim().equals("") && !url.trim().equals("/"))
        {
            url = BXResources.getBXUrl(url, languageId);
        }
        else
        {
            url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
        }

        return url;
    }

    //-- FXLink Phase II --//
    //--> New Field for Market Type Indicator
    //--> By Billy 18Nov2003
    public StaticTextField getStMarketTypeLabel()
    {
        return (StaticTextField) getChild(CHILD_STMARKETTYPELABEL);
    }

    public String endStMarketTypeLabelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTargetEscrow_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayMarketType();
        handler.pageSaveState();

        if (rc == true)
        {
            return handler.getMarketTypeLabel();
        }
        else
        {
            return "";
        }
    }

    public StaticTextField getStMarketType()
    {
        return (StaticTextField) getChild(CHILD_STMARKETTYPE);
    }

    //=========================================
    //--DJ_PT_CR--start//
    public StaticTextField getStBranchTransitLabel()
    {
        return (StaticTextField) getChild(CHILD_STBRANCHTRANSITLABEL);
    }

    public StaticTextField getStPartyName()
    {
        return (StaticTextField) getChild(CHILD_STPARTYNAME);
    }

    public StaticTextField getStBranchTransitNumber()
    {
        return (StaticTextField) getChild(CHILD_STBRANCHTRANSITNUM);
    }

    public StaticTextField getStAddressLine1()
    {
        return (StaticTextField) getChild(CHILD_STADDRESSLINE1);
    }

    public StaticTextField getStCity()
    {
        return (StaticTextField) getChild(CHILD_STCITY);
    }

    /**
     *
     *
     */
    public String endStBranchTransitNumberDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTargetEscrow_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationInfo();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    public String endStBranchTransitLabelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTargetEscrow_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationLabel();
        handler.pageSaveState();

        if (rc == true)
        {
            return handler.getTransitNumberLabel();
        }
        else
        {
            return "";
        }
    }

    public String endStAddressLine1Display(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTargetEscrow_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationInfo();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    public String endStPartyNameDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTargetEscrow_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationInfo();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    public String endStCityDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the stTargetEscrow_onBeforeHtmlOutputEvent method
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isDisplayBranchOriginationInfo();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    //--DJ_PT_CR--end//
    //--FX_LINK--start--//

    /**
     *
     *
     */
    public Button getBtSOBDetails()
    {
        return (Button) getChild(CHILD_BTSOURCEOFBUSINESSDETAILS);
    }

    /**
     *
     *
     */
    public void handleBtSOBDetailsRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this);

        handler.handleDetailSourceBusiness();

        handler.postHandlerProtocol();
    }

    //--FX_LINK--end--//
    //--DJ_CR134--start--27May2004--//

    /**
     *
     *
     */
    public StaticTextField getStHomeBASEProductRatePmnt()
    {
        return (StaticTextField) getChild(CHILD_STHOMEBASEPRODUCTRATEPMNT);
    }
    //--DJ_CR134--end--//

    //--Release3.1--begins
    //--by Hiro Apr 20, 2006

    public ComboBox getCbProductType()
    {
        return (ComboBox) getChild(CHILD_CBPRODUCTTYPE);
    }

    public ComboBox getCbRefiProductType()
    {
        return (ComboBox) getChild(CHILD_CBREFIPRODUCTTYPE);
    }

    public RadioButtonGroup getRbProgressAdvanceInspectionBy()
    {
        return (RadioButtonGroup) getChild(CHILD_RBPROGRESSADVANCEINSPECTIONBY);
    }

    public RadioButtonGroup getRbSelfDirectedRRSP()
    {
        return (RadioButtonGroup) getChild(CHILD_RBSELFDIRECTEDRRSP);
    }

    public TextField getTxCMHCProductTrackerIdentifier()
    {
        return (TextField) getChild(CHILD_TXCMHCPRODUCTTRACKERIDENTIFIER);
    }

    public ComboBox getCbLOCRepaymentType()
    {
        return (ComboBox) getChild(CHILD_CBLOCREPAYMENTTYPEID);
    }

    public RadioButtonGroup getRbRequestStandardService()
    {
        return (RadioButtonGroup) getChild(CHILD_RBREQUESTSTANDARDSERVICE);
    }

    public StaticTextField getStLOCAmortizationMonths()
    {
        return (StaticTextField) getChild(CHILD_STLOCAMORTIZATIONMONTHS);
    }

    public String endStLOCAmortizationMonthsDisplay(ChildContentDisplayEvent event)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setMonthsToYearsMonths(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    public StaticTextField getStLOCInterestOnlyMaturityDate()
    {
        return (StaticTextField) getChild(CHILD_STLOCINTERESTONLYMATURITYDATE);
    }
    //--Release3.1--ends

    ////////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Custom Methods - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    public void handleActMessageOK(String[] args)
    {
        DealEntryHandler handler = (DealEntryHandler) this.handler.cloneSS();

        handler.preHandlerProtocol(this, true);

        handler.handleActMessageOK(args);

        handler.postHandlerProtocol();
    }

    //  ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
    /**
     * @return Object of Type ComboBox
     * 
     */
    public ComboBox getCbAffiliationProgram() {
        return (ComboBox) getChild(CHILD_CBAFFILIATIONPROGRAM);
    }

    //  ***** Change by NBC Impl. Team - Version 1.6 - End *****//
    /**
     *
     *
     */

    //--DJ_Ticket661--start--//
    //Modified to sort by PageLabel based on the selected language
    static class CbPageNamesOptionList extends GotoOptionList
    {
        /**
         *
         *
         */
        CbPageNamesOptionList()
        {
        }

        public void populate(RequestContext rc)
        {
            Connection c = null;

            try
            {
                clear();

                SelectQueryModel m = null;

                SelectQueryExecutionContext eContext =
                    new SelectQueryExecutionContext((Connection) null,
                            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
                            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

                if (rc == null)
                {
                    m = new doPageNameLabelModelImpl();
                    c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
                    eContext.setConnection(c);
                }
                else
                {
                    m = (SelectQueryModel) rc.getModelManager().getModel(doPageNameLabelModel.class);
                }

                m.retrieve(eContext);
                m.beforeFirst();

                // Sort the results from DataObject
                TreeMap sorted = new TreeMap();

                while (m.next())
                {
                    Object dfPageLabel =
                        m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
                    String label = ((dfPageLabel == null) ? "" : dfPageLabel.toString());
                    Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
                    String value = ((dfPageId == null) ? "" : dfPageId.toString());
                    String[] theVal = new String[2];
                    theVal[0] = label;
                    theVal[1] = value;
                    sorted.put(label, theVal);
                }

                // Set the sorted list to the optionlist
                Iterator theList = sorted.values().iterator();
                String[] theVal = new String[2];

                while (theList.hasNext())
                {
                    theVal = (String[]) (theList.next());
                    add(theVal[0], theVal[1]);
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            finally
            {
                try
                {
                    if (c != null)
                    {
                        c.close();
                    }
                }
                catch (SQLException ex)
                {
                    // ignore
                }
            }
        }
    }

    //--Release2.1--/
    ////class CbDealPurposeOptionList extends OptionList
    class CbDealPurposeOptionList extends BaseComboBoxOptionList
    {
        CbDealPurposeOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element if place or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
         //--Release2.1--/
         ////Obtain the languageId from the SessionState
         String defaultInstanceStateName =
                         getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
         SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                 SessionStateModel.class,
                                                 defaultInstanceStateName,
                                                 true);
         int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object DEALPURPOSEID = results.getObject("DEALPURPOSEID");
             Object DEALPURPOSE_DPDESCRIPTION = results.getObject("DPDESCRIPTION");
             String value = (DEALPURPOSEID == null?"":DEALPURPOSEID.toString());
           //--Release2.1--/
           //// Override label with its multilingual context from the BXResources.
           String multilingualLabel = BXResources.getPickListDescription("DEALPURPOSE",
                                                                         new Integer(value).intValue(),
                                                                         languageId);
             String label = (DEALPURPOSE_DPDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--/
       //// Should not be static.
         ////private static String FETCH_DATA_STATEMENT="SELECT DEALPURPOSE.DPDESCRIPTION, DEALPURPOSE.DEALPURPOSEID FROM DEALPURPOSE";
         private String FETCH_DATA_STATEMENT="SELECT DEALPURPOSE.DPDESCRIPTION, DEALPURPOSE.DEALPURPOSEID FROM DEALPURPOSE";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "DEALPURPOSE",
                    cbDealPurposeOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbDealTypeOptionList extends OptionList
    class CbDealTypeOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbDealTypeOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
        int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object DEALTYPEID = results.getObject("DEALTYPEID");
             Object DEALTYPE_DTDESCRIPTION = results.getObject("DTDESCRIPTION");
             String value = (DEALTYPEID == null?"":DEALTYPEID.toString());
          //--Release2.1--/
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("DEALTYPE",
                                                                        new Integer(value).intValue(),
                                                                        languageId);
             String label = (DEALTYPE_DTDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--/
       //// Should not be static.
          ////private static String FETCH_DATA_STATEMENT="SELECT DEALTYPE.DTDESCRIPTION, DEALTYPE.DEALTYPEID FROM DEALTYPE";
          private String FETCH_DATA_STATEMENT="SELECT DEALTYPE.DTDESCRIPTION, DEALTYPE.DEALTYPEID FROM DEALTYPE";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "DEALTYPE", cbDealTypeOptions);
        }
    }

    // SEAN GECF DOCUMENT TYPE DROP DOWN define the inner option list class
    class CbIVDocumentTypeOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbIVDocumentTypeOptionList()
        {
        }

        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "INCOMEVERIFICATIONTYPE", cbIVDocumentTypeOptions);
        }
    }
    // END GECF DOCUMENT TYPE DROP DOWN

    // SEAN DJ SPEC-Progress Advance Type July 20, 2005: define the inner option list class
    class CbProgressAdvanceTypeOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbProgressAdvanceTypeOptionList()
        {
        }

        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PROGRESSADVANCETYPE", cbProgressAdvanceTypeOptions);
        }
    }
    // SEAN DJ SPEC-PAT END

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbLOBOptionList extends OptionList
    class CbLOBOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbLOBOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
        int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object LINEOFBUSINESSID = results.getObject("LINEOFBUSINESSID");
             Object LINEOFBUSINESS_LOBDESCRIPTION = results.getObject("LOBDESCRIPTION");
             String value = (LINEOFBUSINESSID == null?"":LINEOFBUSINESSID.toString());
          //--Release2.1--/
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("LINEOFBUSINESS",
                                                                        new Integer(value).intValue(),
                                                                        languageId);
             String label = (LINEOFBUSINESS_LOBDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--//
       //// Should not be static.
          ////private static String FETCH_DATA_STATEMENT="SELECT LINEOFBUSINESS.LOBDESCRIPTION, LINEOFBUSINESS.LINEOFBUSINESSID FROM LINEOFBUSINESS";
          private String FETCH_DATA_STATEMENT="SELECT LINEOFBUSINESS.LOBDESCRIPTION, LINEOFBUSINESS.LINEOFBUSINESSID FROM LINEOFBUSINESS";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "LINEOFBUSINESS", cbLOBOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbChargeTermOptionList extends OptionList
    class CbChargeTermOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbChargeTermOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
        int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object LIENPOSITION_LPDESCRIPTION = results.getObject("LPDESCRIPTION");
             Object LIENPOSITIONID = results.getObject("LIENPOSITIONID");
             String value = (LIENPOSITIONID == null?"":LIENPOSITIONID.toString());
          //--Release2.1--//
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("LIENPOSITION",
                                                                        new Integer(value).intValue(),
                                                                        languageId);
             String label = (LIENPOSITION_LPDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--//
       //// Should not be static.
          ////private static String FETCH_DATA_STATEMENT="SELECT LIENPOSITION.LPDESCRIPTION, LIENPOSITION.LIENPOSITIONID FROM LIENPOSITION";
          private String FETCH_DATA_STATEMENT="SELECT LIENPOSITION.LPDESCRIPTION, LIENPOSITION.LIENPOSITIONID FROM LIENPOSITION";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "LIENPOSITION",
                    cbChargeTermOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbPayemntFrequencyTermOptionList extends OptionList
    class CbPayemntFrequencyTermOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbPayemntFrequencyTermOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
        int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object PAYMENTFREQUENCY_PFDESCRIPTION = results.getObject("PFDESCRIPTION");
             Object PAYMENTFREQUENCYID = results.getObject("PAYMENTFREQUENCYID");
             String value = (PAYMENTFREQUENCYID == null?"":PAYMENTFREQUENCYID.toString());
          //--Release2.1--//
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("PAYMENTFREQUENCY",
                                                                        new Integer(value).intValue(),
                                                                        languageId);
             String label = (PAYMENTFREQUENCY_PFDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--//
       ////Should not be static.
          ////private static String FETCH_DATA_STATEMENT="SELECT PAYMENTFREQUENCY.PFDESCRIPTION, PAYMENTFREQUENCY.PAYMENTFREQUENCYID FROM PAYMENTFREQUENCY";
          private String FETCH_DATA_STATEMENT="SELECT PAYMENTFREQUENCY.PFDESCRIPTION, PAYMENTFREQUENCY.PAYMENTFREQUENCYID FROM PAYMENTFREQUENCY";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PAYMENTFREQUENCY",
                    cbPayemntFrequencyTermOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbRepaymentTypeOptionList extends OptionList
    class CbRepaymentTypeOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbRepaymentTypeOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
        int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object REPAYMENTTYPE_REPAYMENTTYPEDESCRIPTION = results.getObject("REPAYMENTTYPEDESCRIPTION");
             Object REPAYMENTTYPEID = results.getObject("REPAYMENTTYPEID");
             String value = (REPAYMENTTYPEID == null?"":REPAYMENTTYPEID.toString());
          //--Release2.1--//
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("REPAYMENTTYPE",
                                                                        new Integer(value).intValue(),
                                                                        languageId);
             String label = (REPAYMENTTYPE_REPAYMENTTYPEDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--//
       ////Should not be static.
          ////private static String FETCH_DATA_STATEMENT="SELECT REPAYMENTTYPE.REPAYMENTTYPEDESCRIPTION, REPAYMENTTYPE.REPAYMENTTYPEID FROM REPAYMENTTYPE";
          private String FETCH_DATA_STATEMENT="SELECT REPAYMENTTYPE.REPAYMENTTYPEDESCRIPTION, REPAYMENTTYPE.REPAYMENTTYPEID FROM REPAYMENTTYPE";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "REPAYMENTTYPE",
                    cbRepaymentTypeOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbPrePaymentPenanltyOptionList extends OptionList
    class CbPrePaymentPenanltyOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbPrePaymentPenanltyOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */

        /*
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }

        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);

        int languageId = theSessionState.getLanguageId();

           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

           while(results.next()) {
             Object PREPAYMENTOPTIONSID = results.getObject("PREPAYMENTOPTIONSID");
             Object PREPAYMENTOPTIONS_PODESCRIPTION = results.getObject("PODESCRIPTION");

             String value = (PREPAYMENTOPTIONSID == null?"":PREPAYMENTOPTIONSID.toString());

          //--Release2.1--//
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("PREPAYMENTOPTIONS",
                                                                        new Integer(value).intValue(),
                                                                        languageId);

             String label = (PREPAYMENTOPTIONS_PODESCRIPTION == null?"":multilingualLabel);

             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--//
       ////Should not be static.
          ////private static String FETCH_DATA_STATEMENT="SELECT PREPAYMENTOPTIONS.PODESCRIPTION, PREPAYMENTOPTIONS.PREPAYMENTOPTIONSID FROM PREPAYMENTOPTIONS";
          private String FETCH_DATA_STATEMENT="SELECT PREPAYMENTOPTIONS.PODESCRIPTION, PREPAYMENTOPTIONS.PREPAYMENTOPTIONSID FROM PREPAYMENTOPTIONS";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PREPAYMENTOPTIONS",
                    cbPrePaymentPenanltyOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbPrivilagePaymentOptionOptionList extends OptionList
    class CbPrivilagePaymentOptionOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbPrivilagePaymentOptionOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
        int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object PRIVILEGEPAYMENT_PPDESCRIPTION = results.getObject("PPDESCRIPTION");
             Object PRIVILEGEPAYMENTID = results.getObject("PRIVILEGEPAYMENTID");
             String value = (PRIVILEGEPAYMENTID == null?"":PRIVILEGEPAYMENTID.toString());
          //--Release2.1--//
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("PRIVILEGEPAYMENT",
                                                                        new Integer(value).intValue(),
                                                                        languageId);
             String label = (PRIVILEGEPAYMENT_PPDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--//
       ////Should not be static.
          ////private static String FETCH_DATA_STATEMENT="SELECT PRIVILEGEPAYMENT.PPDESCRIPTION, PRIVILEGEPAYMENT.PRIVILEGEPAYMENTID FROM PRIVILEGEPAYMENT";
          private String FETCH_DATA_STATEMENT="SELECT PRIVILEGEPAYMENT.PPDESCRIPTION, PRIVILEGEPAYMENT.PRIVILEGEPAYMENTID FROM PRIVILEGEPAYMENT";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PRIVILEGEPAYMENT",
                    cbPrivilagePaymentOptionOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbFinancingProgramOptionList extends OptionList
    class CbFinancingProgramOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbFinancingProgramOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
        int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object FINANCEPROGRAM_FPDESCRIPTION = results.getObject("FPDESCRIPTION");
             Object FINANCEPROGRAMID = results.getObject("FINANCEPROGRAMID");
             String value = (FINANCEPROGRAMID == null?"":FINANCEPROGRAMID.toString());
          //--Release2.1--//
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("FINANCEPROGRAM",
                                                                        new Integer(value).intValue(),
                                                                        languageId);
             String label = (FINANCEPROGRAM_FPDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--//
       ////Should not be static.
          ////private static String FETCH_DATA_STATEMENT="SELECT FINANCEPROGRAM.FPDESCRIPTION, FINANCEPROGRAM.FINANCEPROGRAMID FROM FINANCEPROGRAM";
          private String FETCH_DATA_STATEMENT="SELECT FINANCEPROGRAM.FPDESCRIPTION, FINANCEPROGRAM.FINANCEPROGRAMID FROM FINANCEPROGRAM";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "FINANCEPROGRAM",
                    cbFinancingProgramOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbSpecialFeatureOptionList extends OptionList
    class CbSpecialFeatureOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbSpecialFeatureOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy
         *  the latest approach is picked up. Nevertheless, the first method (as an example)
         *  should stay here (at least for now).
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
        //--Release2.1--/
        ////Obtain the languageId from the SessionState
        String defaultInstanceStateName =
                        getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                                SessionStateModel.class,
                                                defaultInstanceStateName,
                                                true);
        int languageId = theSessionState.getLanguageId();
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object SPECIALFEATUREID = results.getObject("SPECIALFEATUREID");
             Object SPECIALFEATURE_SFDESCRIPTION = results.getObject("SFDESCRIPTION");
             String value = (SPECIALFEATUREID == null?"":SPECIALFEATUREID.toString());
          //--Release2.1--//
          //// Override label with its multilingual context from the BXResources.
          String multilingualLabel = BXResources.getPickListDescription("SPECIALFEATURE",
                                                                        new Integer(value).intValue(),
                                                                        languageId);
             String label = (SPECIALFEATURE_SFDESCRIPTION == null?"":multilingualLabel);
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       //--Release2.1--/
       //// Should not be static to override from the BXResources
          ////private static String FETCH_DATA_STATEMENT="SELECT SPECIALFEATURE.SFDESCRIPTION, SPECIALFEATURE.SPECIALFEATUREID FROM SPECIALFEATURE";
          private String FETCH_DATA_STATEMENT="SELECT SPECIALFEATURE.SFDESCRIPTION, SPECIALFEATURE.SPECIALFEATUREID FROM SPECIALFEATURE";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "SPECIALFEATURE",
                    cbSpecialFeatureOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbBranchOptionList extends OptionList
    class CbBranchOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbBranchOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy the latest approach is picked up.
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object BRANCHPROFILE_BRANCHNAME = results.getObject("BRANCHNAME");
             Object BRANCHPROFILEID = results.getObject("BRANCHPROFILEID");
             String label = (BRANCHPROFILE_BRANCHNAME == null?"":BRANCHPROFILE_BRANCHNAME.toString());
             String value = (BRANCHPROFILEID == null?"":BRANCHPROFILEID.toString());
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       private static String FETCH_DATA_STATEMENT="SELECT BRANCHPROFILE.BRANCHNAME, BRANCHPROFILE.BRANCHPROFILEID FROM BRANCHPROFILE";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "BRANCHPROFILE", cbBranchOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbCrossSellOptionList extends OptionList
    class CbCrossSellOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbCrossSellOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy the latest approach is picked up.
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object CROSSSELLPROFILE_CSDESCRIPTION = results.getObject("CSDESCRIPTION");
             Object CROSSSELLPROFILEID = results.getObject("CROSSSELLPROFILEID");
             String label = (CROSSSELLPROFILE_CSDESCRIPTION == null?"":CROSSSELLPROFILE_CSDESCRIPTION.toString());
             String value = (CROSSSELLPROFILEID == null?"":CROSSSELLPROFILEID.toString());
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       private static String FETCH_DATA_STATEMENT="SELECT CROSSSELLPROFILE.CSDESCRIPTION, CROSSSELLPROFILE.CROSSSELLPROFILEID FROM CROSSSELLPROFILE";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "CROSSSELLPROFILE",
                    cbCrossSellOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--/
    //// Should not be static to override from the BXResources
    ////static class CbTaxPayorOptionList extends OptionList
    class CbTaxPayorOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbTaxPayorOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy the latest approach is picked up.
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object TAXPAYORID = results.getObject("TAXPAYORID");
             Object TAXPAYOR_TPDESCRIPTION = results.getObject("TPDESCRIPTION");
             String label = (TAXPAYOR_TPDESCRIPTION == null?"":TAXPAYOR_TPDESCRIPTION.toString());
             String value = (TAXPAYORID == null?"":TAXPAYORID.toString());
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       private static String FETCH_DATA_STATEMENT="SELECT TAXPAYOR.TPDESCRIPTION, TAXPAYOR.TAXPAYORID FROM TAXPAYOR";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "TAXPAYOR", cbTaxPayorOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--//
    ////static class CbMIIndicatorOptionList extends OptionList
    class CbMIIndicatorOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbMIIndicatorOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy the latest approach is picked up.
         *
         */
        /***
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object MIINDICATOR_MIIDESCRIPTION = results.getObject("MIIDESCRIPTION");
             Object MIINDICATORID = results.getObject("MIINDICATORID");
             String label = (MIINDICATOR_MIIDESCRIPTION == null?"":MIINDICATOR_MIIDESCRIPTION.toString());
             String value = (MIINDICATORID == null?"":MIINDICATORID.toString());
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       private static String FETCH_DATA_STATEMENT="SELECT MIINDICATOR.MIIDESCRIPTION, MIINDICATOR.MIINDICATORID FROM MIINDICATOR";
         ***/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "MIINDICATOR",
                    cbMIIndicatorOptions);
        }
    }

    //--Release2.1--//

    /**
     *  Since the population of comboboxes on the pages should be done from BXResource
     *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
     *  element if place or it could be done directly from the BXResource. Each approach
     *  has its pros and cons. The most important is: for English and French versions
     *  could be different default values. It forces to use the second approach.
     *
     *  It this case to escape annoying code clone and follow the object oriented
     *  design the following abstact base class should encapsulate the combobox
     *  OptionList population. Each ComboBoxOptionList inner class should extend
     *  this base class and implement the BXResources table name.
     *
     *
     */
    abstract class BaseComboBoxOptionList extends OptionList
    {
        /**
         *
         *
         */
        BaseComboBoxOptionList()
        {
        }


        //// In order to escape the double population for the splitted JSP(s): UW and
        //// DE the OptionList should be repopulated and set.
        protected final void populateOptionList(RequestContext rc, int langId,
                String tablename, OptionList name)
        {
            DealEntryHandler handler = (DealEntryHandler) pgDealEntryViewBean.this.handler.cloneSS();
            handler.pageGetState(pgDealEntryViewBean.this);
            try
            {
                //Get IDs and Labels from BXResources
                Collection c 
                = BXResources
                .getPickListValuesAndDesc(handler.getTheSessionState().getDealInstitutionId(), 
                        tablename, langId);
                int tableSize 
                = BXResources
                .getPickListTableSize(handler.getTheSessionState().getDealInstitutionId(),
                        tablename, langId);
                Iterator l = c.iterator();

                String[] vals = new String[tableSize];
                String[] labels = new String[tableSize];
                String[] theVal = new String[2];

                int i = 0;

                while (l.hasNext())
                {
                    theVal = (String[]) (l.next());
                    vals[i] = theVal[1];
                    labels[i] = theVal[0];
                    i++;
                }

                name.setOptions(vals, labels);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    //5.0 MI -- deleted
    //class CbMITypeOptionList extends BaseComboBoxOptionList


    //--Release2.1--//
    //// Should not be static.
    ////static class CbMIInsurerOptionList extends OptionList
    class CbMIInsurerOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbMIInsurerOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy the latest approach is picked up.
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object MORTGAGEINSURERID = results.getObject("MORTGAGEINSURERID");
             Object MORTGAGEINSURER_MORTGAGEINSURERNAME = results.getObject("MORTGAGEINSURERNAME");
             String label = (MORTGAGEINSURER_MORTGAGEINSURERNAME == null?"":MORTGAGEINSURER_MORTGAGEINSURERNAME.toString());
             String value = (MORTGAGEINSURERID == null?"":MORTGAGEINSURERID.toString());
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       private static String FETCH_DATA_STATEMENT="SELECT MORTGAGEINSURER.MORTGAGEINSURERNAME, MORTGAGEINSURER.MORTGAGEINSURERID FROM MORTGAGEINSURER";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "MORTGAGEINSURANCECARRIER",
                    cbMIInsurerOptions);
        }
    }

    /**
     *
     *
     */

    //--Release2.1--//
    //// Should not be static.
    ////static class CbMIPayorOptionList extends OptionList
    class CbMIPayorOptionList extends BaseComboBoxOptionList
    {
        /**
         *
         *
         */
        CbMIPayorOptionList()
        {
        }

        //// IMPORTANT!!! //--Release2.1--//

        /**
         *  Since the population of comboboxes on the pages should be done from BXResource
         *  bundle, either the iMT converted methods could be adjusted with the FETCH_DATA_STATEMENT
         *  element or it could be done directly from the BXResource. Each approach
         *  has its pros and cons. The most important is: for English and French versions
         *  could be different default values. It forces to use the second approach.
         *
         *  It this case to escape annoying code clone and follow the object oriented
         *  design the following abstact base class should encapsulate the combobox
         *  OptionList population. Each ComboBoxOptionList inner class should extend
         *  this base class and implement the BXResources table name.
         *
         *  Based on discussion with Billy the latest approach is picked up.
         *
         */
        /**
       public void populate(RequestContext rc)
       {
         Connection c = null;
         try {
           clear();
           if(rc == null) {
             c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
           }
           else {
             c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
           }
           Statement query = c.createStatement();
           ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);
           while(results.next()) {
             Object MORTGAGEINSURANCEPAYORID = results.getObject("MORTGAGEINSURANCEPAYORID");
             Object MORTGAGEINSURANCEPAYOR_MIPDESCRIPTION = results.getObject("MIPDESCRIPTION");
             String label = (MORTGAGEINSURANCEPAYOR_MIPDESCRIPTION == null?"":MORTGAGEINSURANCEPAYOR_MIPDESCRIPTION.toString());
             String value = (MORTGAGEINSURANCEPAYORID == null?"":MORTGAGEINSURANCEPAYORID.toString());
             add(label, value);
           }
         }
         catch (Exception ex) {
           ex.printStackTrace();
         }
         finally {
           try {
             if(c != null)
               c.close();
           }
           catch (SQLException  ex) {
             // ignore
           }
         }
       }
       private static String FETCH_DATA_STATEMENT="SELECT MORTGAGEINSURANCEPAYOR.MIPDESCRIPTION, MORTGAGEINSURANCEPAYOR.MORTGAGEINSURANCEPAYORID FROM MORTGAGEINSURANCEPAYOR";
         **/

        //--Release2.1--//
        public void populate(RequestContext rc)
        {
            //Get the Language ID from the SessionStateModel
            String defaultInstanceStateName =
                rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState =
                (SessionStateModelImpl) rc.getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName,
                        true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "MORTGAGEINSURANCEPAYOR",
                    cbMIPayorOptions);
        }
    }


    //--Release3.1--begins
    //--by Hiro Apr 13, 2006
    ////static class CbUWChargeOptionList extends OptionList
    class CbProductTypeOptionList
    extends BaseComboBoxOptionList
    {
        CbProductTypeOptionList()
        {
        }

        // --Release3.1--//
        // // See detailed comment in DealEntryViewBean.
        public void populate(RequestContext rc)
        {
            // Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PRODUCTTYPE",
                    cbProductTypeOptions);
        }
    }

    class CbRefiProductTypeOptionList
    extends BaseComboBoxOptionList
    {
        CbRefiProductTypeOptionList()
        {
        }

        public void populate(RequestContext rc)
        {
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "PRODUCTTYPE",
                    cbRefiProductTypeOptions);
        }
    }



    class CbLOCRepaymentTypeOptionList
    extends BaseComboBoxOptionList
    {
        CbLOCRepaymentTypeOptionList()
        {
        }

        public void populate(RequestContext rc)
        {
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "LOCREPAYMENTTYPE",
                    cbLOCRepaymentTypeOptions);
        }
    }

    // --Release3.1--ends

    //  ***** Change by NBC Impl. Team - Version 1.6 - Start *****//
    class CbAffiliationProgramOptionList extends BaseComboBoxOptionList {

        CbAffiliationProgramOptionList() {
        }

        /**
         * 
         */

        public void populate(RequestContext rc) {
            // Get the Language ID from the SessionStateModel
            String defaultInstanceStateName = rc.getModelManager()
            .getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
            .getModelManager().getModel(SessionStateModel.class,
                    defaultInstanceStateName, true);

            int languageId = theSessionState.getLanguageId();

            super.populateOptionList(rc, languageId, "AFFILIATIONPROGRAM",
                    cbAffiliationProgramOptions);
        }
    }
    //  ***** Change by NBC Impl. Team - Version 1.6 - End *****//

    //-- ========== SCR#859 begins ========== --//
    //-- by Neil on Feb 15, 2005
    public StaticTextField getStServicingMortgageNumber() {
        return (StaticTextField) getChild(CHILD_STSERVICINGMORTGAGENUMBER);
    }

    public StaticTextField getStCCAPS() {
        return (StaticTextField) getChild(CHILD_STCCAPS);
    }

    //-- ========== SCR#859 ends ========== --//

//  ***** Change by NBC/PP Implementation Team - GCD - Start *****//

    /** Returns the <code>txCreditDecisionStatus</code> child View component */
    public StaticTextField getTxCreditDecisionStatusChild() {
        return (StaticTextField)getChild(CHILD_TX_CREDIT_DECISION_STATUS);
    }

    /** Returns the <code>txCreditDecision</code> child View component */
    public StaticTextField getTxCreditDecisionChild() {
        return (StaticTextField)getChild(CHILD_TX_CREDIT_DECISION);
    }

    /** Returns the <code>txGlobalCreditBureauScore</code> child View component */
    public StaticTextField getTxGlobalCreditBureauScoreChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE);
    }

    /** Returns the <code>txGlobalRiskRating</code> child View component */
    public StaticTextField getTxGlobalRiskRatingChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_RISK_RATING);
    }

    /** Returns the <code>txGlobalInternalScore</code> child View component */
    public StaticTextField getTxGlobalInternalScoreChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_INTERNAL_SCORE);
    }

    /** Returns the <code>txMIStatus</code> child View component */
    public StaticTextField getTxMIStatusChild() {
        return (StaticTextField)getChild(CHILD_TX_MISTATUS);
    }

    /** Returns the <code>btCreditDecisionPG</code> child View component */
    public Button getBtCreditDecisionPGChild() {
        return (Button)getChild(CHILD_BT_CREDIT_DECISION_PG);
    }

    /** Returns the <code>stIncludeGCDSumStart</code> child View component */
    public StaticTextField getStIncludeGCDSumStartChild() {
        return (StaticTextField)getChild(CHILD_ST_INCLUDE_GCDSUM_START);
    }

    /** Returns the <code>stIncludeGCDSumEnd</code> child View component */
    public StaticTextField getStIncludeGCDSumEndChild() {
        return (StaticTextField)getChild(CHILD_ST_INCLUDE_GCDSUM_END);
    }

//  ***** Change by NBC/PP Implementation Team - GCD - End *****//
//  ***** Change by NBC/PP Implementation Team - SCR #1679 - Start *****//
    /**
     * Returns the <code>hdForceProgressAdvance</code> child View component
     */
    public HiddenField getHdForceProgressAdvanceChild() {
        return (HiddenField) getChild(CHILD_HDFORCEPROGRESSADVANCE);
    }
//  ***** Change by NBC/PP Implementation Team - SCR #1679 - End *****//
//  ***** Change by NBC/PP Implementation Team - GCD - Start *****//	
//  GCD Summary data retrieval Begin

    /***************MCM Impl team changes starts - XS_2.7 *******************/

    public TextField getTxRefiAdditionalInformation()
    {
        return (TextField) getChild(CHILD_TXREFIADDITIONALINFORMATION);
    }

    public TextField getTxRefExistingMTGNumber()
    {
        return (TextField) getChild(CHILD_TXREFEXISTINGMTGNUMBER);
    }

    /***************MCM Impl team changes ends - XS_2.7 *********************/


    public doAdjudicationResponseBNCModel getdoAdjudicationResponseBNCModel ()
    {
        if(doAdjudicationResponseBNCModel == null)
        {
            doAdjudicationResponseBNCModel = (doAdjudicationResponseBNCModel)getModel(doAdjudicationResponseBNCModel.class);
        }
        return doAdjudicationResponseBNCModel;
    }

    public void setdoGCDSummarySectionModel (doAdjudicationResponseBNCModel model)
    {
        doAdjudicationResponseBNCModel = model;
    }

//  ***** Change by NBC/PP Implementation Team - GCD - End *****//

    /**
     * getTxRateGuaranteePeriod<br>
     * 	This method returns the textfield for CHILD_TXRATEGUARANTEEPERIOD
     * 
     * @return TextField <br>
     * @param none<br>
     * @version 1.0 (Initial Version - 02 June 2006)
     * @author:NBC/PP Implementation Team<br>
     * 
     */
    public TextField getTxRateGuaranteePeriod ()
    { 
        return (TextField) getChild(CHILD_TXRATEGUARANTEEPERIOD);
    }

    /**
     * getHdChangedEstimatedClosingDate<br>
     * 	This method returns the textfield for HILD_HDCHANGEDESTIMATEDCLOSINGDATE
     * 
     * @return HiddenField <br>
     * @param none<br>
     * @version 1.0 (Initial Version - 02 June 2006)
     * @author:NBC/PP Implementation Team<br>
     * 
     */
    public HiddenField getHdChangedEstimatedClosingDate ()
    { 
        return (HiddenField) getChild(CHILD_HDCHANGEDESTIMATEDCLOSINGDATE);
    }

//  ***** Change by NBC Impl. Team - Version 1.5- Start *****//
    /**
     * getTxCashBackInDollars
     *
     * @param None <br>
     * @return TextField : the result of getTxCashBackInDollars <br>
     */
    public TextField getTxCashBackInDollars() {
        return (TextField) getChild(CHILD_TXCASHBACKINDOLLARS);
    }
    /**
     * getTxCashBackInPercentage
     *
     * @param None <br>
     * @return TextField : the result of getTxCashBackInPercentage <br>
     */
    public TextField getTxCashBackInPercentage() {
        return (TextField) getChild(CHILD_TXCASHBACKINPERCENTAGE);
    }
    /**
     * getChCashBackOverrideInDollars
     *
     * @param None <br>
     * @return CheckBox : the result of getChCashBackOverrideInDollars <br>
     */
    public CheckBox getChCashBackOverrideInDollars() {
        return (CheckBox) getChild(CHILD_CHCASHBACKOVERRIDEINDOLLARS);
    }


//  ***** Change by NBC Impl. Team - Version 1.5 - End*****//

    // ***** Change by NBC Impl. Team - Version 50 - Start****//
    /**
     * updateCashBackAmount This method calculates the cash back amount, if the cash back override flag is not
     * checked. The cash back amount, thus calculated, will be set into the view bean
     * 
     * This method has been created to ensure that the bidirectional relationship between cash back amount and
     * cash back percent is captured. If cash back percent is changed, then cash back amount in changed. If
     * cash back amount is changed, cash back percent is changed. However, the corresponding calculation class
     * takes cash back as input and cash back percentage as output. Hence, this method, which will set the
     * cash back amount
     */
    private void updateCashBackAmount() {
        BigDecimal totalLoanAmount = new BigDecimal(0);
        String cashBackOverride = "Y";
        BigDecimal cashBackPercent = new BigDecimal(0);
        BigDecimal cashBackAmount = new BigDecimal(0);

        // If the total loan amount is present, get the value and convert to BigDecimal
        if (this.getTxRequestedLoanAmount().getValue() != null) {
            totalLoanAmount = (BigDecimal) this.getTxRequestedLoanAmount().getValue();
        }

        // If the cash back override checkbox has a value, get the value and convert to String
        if (this.getChCashBackOverrideInDollars().getValue() != null) {
            cashBackOverride = (String) this.getChCashBackOverrideInDollars().getValue();
        }

        // If the cash back percentage, get the value and convert to BigDecimal
        if (this.getTxCashBackInPercentage().getValue() != null) {
            cashBackPercent = (BigDecimal) this.getTxCashBackInPercentage().getValue();
        }

        // If the cash back amount, get the value and convert to BigDecimal
        if (this.getTxCashBackInDollars().getValue() != null) {
            cashBackAmount = (BigDecimal) this.getTxCashBackInDollars().getValue();
        }

        /*
         * Check if the cashback override flag is set to N - this implies that the cash back override checkbox
         * has not been checked and cash back percent should be used to determine the cash back amount
         */
        if (cashBackOverride.equalsIgnoreCase("N")) {
            //The result is rounded off to two decimals - this is in line with the formatting on screen
            cashBackAmount = cashBackPercent.multiply(totalLoanAmount).divide(new BigDecimal(100), 2);
            this.getTxCashBackInDollars().setValue(cashBackAmount);            
        }
    }
    // ***** Change by NBC Impl. Team - Version 50 - End****//
    
    /*
     * Ticket 716
     * This methods clears all the saved ETO Fields based on
     * selected Deal Purpose in Deal Modification screen
     */
    public void clearETOFields()
    {
    	List<String> dealPurposeIds = PicklistData.getValueList("DEALPURPOSE", "DEALPURPOSEID");
    	int index = dealPurposeIds.indexOf(String.valueOf(getCbDealPurpose().getValue()));
    	
    	List<String> refFlagList = PicklistData.getValueList("DEALPURPOSE", "REFINANCEFLAG");
    	String refFlag = refFlagList.get(index);

    	// checking the refinance flag
    	if (refFlag.equals("N"))
    	{
    		getCbRefiOrigPurchaseDateMonth().setValue(CHILD_CBREFIORIGPURCHASEDATEMONTH_RESET_VALUE);
            getTxRefiOrigPurchaseDateYear().setValue(CHILD_TXREFIORIGPURCHASEDATEYEAR_RESET_VALUE);
            getTxRefiOrigPurchaseDateDay().setValue(CHILD_TXREFIORIGPURCHASEDATEDAY_RESET_VALUE);
            getTxRefiMortgageHolder().setValue(CHILD_TXREFIMORTGAGEHOLDER_RESET_VALUE);
            getTxRefiOrigPurchasePrice().setValue(CHILD_TXREFIORIGPURCHASEPRICE_RESET_VALUE);
            getTxRefiImprovementValue().setValue(CHILD_TXREFIIMPROVEMENTVALUE_RESET_VALUE);
            getTxRefiImprovementsDesc().setValue(CHILD_TXREFIIMPROVEMENTSDESC_RESET_VALUE);
            getCbRefiProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);
            getTxRefiAdditionalInformation().setValue(CHILD_TXREFIADDITIONALINFORMATION_RESET_VALUE);
            getTxRefiPurpose().setValue(CHILD_TXREFIPURPOSE_RESET_VALUE);
            getRbBlendedAmortization().setValue(CHILD_RBBLENDEDAMORTIZATION_RESET_VALUE);
            getTxRefiOrigMtgAmount().setValue(CHILD_TXREFIORIGMTGAMOUNT_RESET_VALUE);
            getTxRefiOutstandingMtgAmount().setValue(CHILD_TXREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE);
            getTxRefExistingMTGNumber().setValue(CHILD_TXREFEXISTINGMTGNUMBER_RESET_VALUE);
    	}
    }
    
	//4.4 Submission Agent
    public pgDealEntryRepeatedSOBTiledView getRepeatedSOB()
    {
        return (pgDealEntryRepeatedSOBTiledView) getChild(CHILD_REPEATEDSOB);
    }
    
    //***** Qualifying Rate *****//
    public ComboBox getCbQualifyProductType() {
        return (ComboBox) getChild(CHILD_CBQUALIFYPRODUCTTYPE);
    }

    public TextField getTxQualifyRate() {
        return (TextField) getChild(CHILD_TXQUALIFYRATE);
    }

    public CheckBox getChQualifyRateOverride() {
        return (CheckBox) getChild(CHILD_CHQUALIFYRATEOVERRIDE);
    }
    
    public CheckBox getChQualifyRateOverrideRate() {
        return (CheckBox) getChild(CHILD_CHQUALIFYRATEOVERRIDERATE);
    }

    public Button getBtRecalculateGDS() {
        return (Button) getChild(CHILD_BTRECALCULATEGDS);
    }
    
    public StaticTextField getStQualifyRate() {
        return (StaticTextField) getChild(CHILD_STQUALIFYRATE);
    }
    
    public StaticTextField getStQualifyProductType() {
        return (StaticTextField) getChild(CHILD_STQUALIFYPRODUCTTYPE);
    }
    
    public StaticTextField getStQualifyRateHidden() {
        return (StaticTextField) getChild(CHILD_STQUALIFYRATEHIDDEN);
    }
    
    public StaticTextField getStQualifyRateEdit() {
        return (StaticTextField) getChild(CHILD_STQUALIFYRATEEDIT);
    }
    
    public StaticTextField getSthdQualifyRate() {
        return (StaticTextField) getChild(CHILD_STHDQUALIFYRATE);
    }
    
    public StaticTextField getSthdQualifyProductId() {
        return (StaticTextField) getChild(CHILD_STHDQUALIFYPRODUCTID);
    }
    
    public StaticTextField getHdQualifyRateDisabled() {
        return (StaticTextField) getChild(CHILD_HDQUALIFYRATEDISABLED);
    }
    
    // Ticket 267
    public HiddenField getHdDealStatusId() {
        return (HiddenField) getChild(CHILD_HDDEALSTATUSID);
    }
}
