package mosApp.MosSystem;



import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.deal.security.ViewBeanFactory;

import MosSystem.Mc;

/**
 * <p>Title: AppraisalReviewFactory.java </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: filogix</p>
 *
 * @author Vlad Ivanov
 * @version 1.0
 */

public class AppraisalReviewFactory extends ViewBeanFactory {
    
    
    // TODO: move to Mc constant interface
    private static final String PATH = "mosApp.MosSystem";
    private static final String DJ = "DJ";
    private static final String pgAppraiserReviewDJ = "pgAppraiserReviewDJ";
    private static final String pgAppraiserReview = "pgAppraiserReview";
    

    private Deal deal;
    public SysLogger logger;

    public AppraisalReviewFactory() {
      path = PATH;
    }

    public AppraisalReviewFactory(Deal deal) {
      path = PATH;
      this.deal = deal;
    }

    /**
     * A factory method to determine which pgAdjudicationReviewXXViewBean to use.
     * @return ViewBean
     */
    public String getViewBeanByClassName()
    {
      // if deal not set, just return null;
      if( deal == null )
          return null;

      try
      {
        String lenderShortName = this.deal.getLenderProfile().getLPShortName();
        //logger.debug("ARF@getViewBeanClassName::LenderShortName: " + lenderShortName);

        //TODO: Create method to call short name convertion to the abbreviation
        if (lenderShortName.toUpperCase().startsWith(DJ)) {
          return pgAppraiserReviewDJ;
        } else {
          return pgAppraiserReview;
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }

      // no Adjudication Request Page identified for an instance
      // this gets returned but access rights should NEVER be defined for this page so the "Access is denied" dialog is displayed
      // therefore, do not define values for this view bean in USERTYPEPAGEACCESS table or DEALSTATUSPAGEACCESS table

      return pgAppraiserReview;
    }

    /**
     * returns the view bean's id.
     */
    public int getViewBeanId() {

        if (deal != null) {
            try {
                String lenderShortName = this.deal.getLenderProfile().getLPShortName();

                if (lenderShortName.toUpperCase().startsWith(DJ)) {
                    return Mc.PGNM_APPRAISER_REVIEW_DJ;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return Mc.PGNM_APPRAISER_REVIEW;
    }
}
