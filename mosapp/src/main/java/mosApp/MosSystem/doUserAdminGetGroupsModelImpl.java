package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doUserAdminGetGroupsModelImpl extends QueryModelBase
	implements doUserAdminGetGroupsModel
{
	/**
	 *
	 *
	 */
	public doUserAdminGetGroupsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}

	/**
	 *
	 *
	 */
	public void initialize()
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}

	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
		//--Release2.1--//
		//To override all the Description fields by populating from BXResource
		//--> By Billy 06Dec2002
		//Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
		int languageId = theSessionState.getLanguageId();
		String tmpStr = "";
		while(this.next())
		{
			//Convert Group name.
			//--> Modified to get the Group name from Table if it is not found in ResourceBundle
			//--> In order to support creating New Group on UserAdmin screen
			//--> By Billy 14Aug2003
      tmpStr = BXResources.getPickListDescription(this.getDfInstitutionId().intValue(),
          "GROUPPROFILE", this.getDfGroupProfileId().intValue(), languageId);
			//Key Not found if Return String starts with '?' ==> then keep the original value
			if(!(tmpStr.startsWith("?") && tmpStr.endsWith("?")))
			{
				this.setDfGroupName(tmpStr);
			}
			//==================================================================================
		}
		//Reset Location
		this.beforeFirst();
	}

	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGroupProfileId()
	{
		return(java.math.BigDecimal)getValue(FIELD_DFGROUPPROFILEID);
	}

	/**
	 *
	 *
	 */
	public void setDfGroupProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGROUPPROFILEID, value);
	}

	/**
	 *
	 *
	 */
	public String getDfGroupName()
	{
		return(String)getValue(FIELD_DFGROUPNAME);
	}

	/**
	 *
	 *
	 */
	public void setDfGroupName(String value)
	{
		setValue(FIELD_DFGROUPNAME, value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBranchProfileId()
	{
		return(java.math.BigDecimal)getValue(FIELD_DFBRANCHPROFILEID);
	}

	/**
	 *
	 *
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRANCHPROFILEID, value);
	}
	  public java.math.BigDecimal getDfInstitutionId() {
	      return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
	    }

	    public void setDfInstitutionId(java.math.BigDecimal value) {
	      setValue(FIELD_DFINSTITUTIONID, value);
	    }
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME = "jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE =
    "SELECT ALL GROUPPROFILE.GROUPPROFILEID, GROUPPROFILE.GROUPNAME, "+
    "GROUPPROFILE.BRANCHPROFILEID,GROUPPROFILE.INSTITUTIONPROFILEID "+
"FROM GROUPPROFILE  " + 
"__WHERE__  "		+ 
"ORDER BY GROUPPROFILE.GROUPNAME ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME = "GROUPPROFILE";
	public static final String STATIC_WHERE_CRITERIA = "";
	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFGROUPPROFILEID = "GROUPPROFILE.GROUPPROFILEID";
	public static final String COLUMN_DFGROUPPROFILEID = "GROUPPROFILEID";
	public static final String QUALIFIED_COLUMN_DFGROUPNAME = "GROUPPROFILE.GROUPNAME";
	public static final String COLUMN_DFGROUPNAME = "GROUPNAME";
	public static final String QUALIFIED_COLUMN_DFBRANCHPROFILEID = "GROUPPROFILE.BRANCHPROFILEID";
	public static final String COLUMN_DFBRANCHPROFILEID = "BRANCHPROFILEID";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "GROUPPROFILE.INSTITUTIONPROFILEID";
  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGROUPPROFILEID,
				COLUMN_DFGROUPPROFILEID,
				QUALIFIED_COLUMN_DFGROUPPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
			""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGROUPNAME,
				COLUMN_DFGROUPNAME,
				QUALIFIED_COLUMN_DFGROUPNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
			""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRANCHPROFILEID,
				COLUMN_DFBRANCHPROFILEID,
				QUALIFIED_COLUMN_DFBRANCHPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
			""));

	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	}

}
