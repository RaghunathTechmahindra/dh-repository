package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * <p>
 * Title: doApplicantIdentificationTypeModelImpl
 * </p>
 * 
 * <p>
 * Description: Model interface for Applicant Entry screen
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.0
 * 	
 */
public class doApplicantIdentificationTypeModelImpl extends QueryModelBase
		implements  doApplicantIdentificationTypeModel {

	/**
	 *
	 *
	 */
	public doApplicantIdentificationTypeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	
	/* (non-Javadoc)
	 * @see com.iplanet.jato.model.sql.QueryModelBase#beforeExecute(com.iplanet.jato.model.ModelExecutionContext, int, java.lang.String)
	 */
	protected String beforeExecute(ModelExecutionContext context, 
			int queryType, String sql)	throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}

	/* (non-Javadoc)
	 * @see com.iplanet.jato.model.sql.QueryModelBase#afterExecute(com.iplanet.jato.model.ModelExecutionContext, int)
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/* (non-Javadoc)
	 * @see com.iplanet.jato.model.sql.QueryModelBase#onDatabaseError(com.iplanet.jato.model.ModelExecutionContext, int, java.sql.SQLException)
	 */
	protected void onDatabaseError(ModelExecutionContext context, 
			int queryType, SQLException exception)
	{
	}
	
	
	/**
	 * <p>returns borrower Id</p>
	 * @return - returns borrower id
	 */
	public java.math.BigDecimal getDfBorrowerId(){
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}

	
	/**
	 * <p>sets borrower id</p>
	 * @param - sets borrower id
	 */
	public void setDfBorrowerId(java.math.BigDecimal value){
		setValue(FIELD_DFBORROWERID,value);
	}

	
	/**
	 * <p>returns Identification Id</p>
	 * @return - returns Identificationt id
	 */
	public java.math.BigDecimal getDfIdentificationId(){
		return (java.math.BigDecimal)getValue(FIELD_DFIDENTIFICATIONID);
	}

	
	/**
	 * <p>sets Identification id</p>
	 * @param - sets Identification id
	 */
	public void setDfIdentificationId(java.math.BigDecimal value){
		setValue(FIELD_DFIDENTIFICATIONID,value);
	}
	
	/**
	 * <p>returns copy Id</p>
	 * @return - returns copy id
	 */
	public java.math.BigDecimal getDfCopyId(){
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}

	
	/**
	 * <p>sets copy id</p>
	 * @param - sets copy id
	 */
	public void setDfCopyId(java.math.BigDecimal value){
		setValue(FIELD_DFCOPYID,value);
	}

	/**
	 * <p>returns Identification Type </p>
	 * @return - returns Identification Type
	 */
	public java.math.BigDecimal getDfIdentificationTypeId(){
		return (java.math.BigDecimal)getValue(FIELD_DFIDENTIFICATIONTYPEID);
	}

	
	/**
	 * <p>sets  Identification Type</p>
	 * @param - sets  Identification Type
	 */
	public void setDfIdentificationTypeId(java.math.BigDecimal value){
		setValue(FIELD_DFIDENTIFICATIONTYPEID,value);
	}

	
	/**
	 * <p>returns  Identification Number</p>
	 * @return - returns Identification Number
	 */
	public String getDfIdentificationNumber(){
		return (String)getValue(FIELD_DFIDENTIFICATIONNUMBER);
	}

	
	/**
	 * <p>sets Identification Number</p>
	 * @param - sets Identification Number
	 */	
	public void setDfIdentificationNumber(String value){
		setValue(FIELD_DFIDENTIFICATIONNUMBER,value);
	}

	
	/**
	 * <p>returns Identification Country Id</p>
	 * @return - returns Identification Country id
	 */
	public String getDfIdentificationCountry(){
		return (String)getValue(FIELD_DFIDENTIFICATIONCOUNTRY);
	}

	
	/**
	 * <p>sets Identification Country id</p>
	 * @param - sets Identification Country id
	 */
	public void setDfIdentificationCountry(String value){
		setValue(FIELD_DFIDENTIFICATIONCOUNTRY,value);
	}

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

	public static final String SELECT_SQL_TEMPLATE = 
			"SELECT DISTINCT BORROWERIDENTIFICATION.IDENTIFICATIONID, BORROWERIDENTIFICATION.BORROWERID, " +
			"BORROWERIDENTIFICATION.COPYID, BORROWERIDENTIFICATION.IDENTIFICATIONNUMBER, " +
            "BORROWERIDENTIFICATION.IDENTIFICATIONTYPEID, BORROWERIDENTIFICATION.IDENTIFICATIONCOUNTRY " +                                              
            "FROM BORROWERIDENTIFICATION  __WHERE__  ";	
	
	public static final String MODIFYING_QUERY_TABLE_NAME = 
			"BORROWERIDENTIFICATION";
	public static final String STATIC_WHERE_CRITERIA = "";
	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
	
	public static final String QUALIFIED_COLUMN_DFBORROWERID = 
			"BORROWERIDENTIFICATION.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	
	public static final String QUALIFIED_COLUMN_DFCOPYID = 
			"BORROWERIDENTIFICATION.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	
	public static final String QUALIFIED_COLUMN_DFIDENTIFICATIONID =
			"BORROWERIDENTIFICATION.IDENTIFICATIONID";
	public static final String COLUMN_DFIDENTIFICATIONID="IDENTIFICATIONID";
	
	public static final String QUALIFIED_COLUMN_DFIDENTIFICATIONNUMBER = 
			"BORROWERIDENTIFICATION.IDENTIFICATIONNUMBER";
	public static final String COLUMN_DFIDENTIFICATIONNUMBER=
			"IDENTIFICATIONNUMBER";
	
	public static final String QUALIFIED_COLUMN_DFIDENTIFICATIONTYPEID=
			"BORROWERIDENTIFICATION.IDENTIFICATIONTYPEID";
	public static final String COLUMN_DFIDENTIFICATIONTYPEID=
			"IDENTIFICATIONTYPEID";

	public static final String QUALIFIED_COLUMN_DFIDENTIFICATIONCOUNTRY=
			"BORROWERIDENTIFICATION.IDENTIFICATIONCOUNTRY";
	public static final String COLUMN_DFIDENTIFICATIONCOUNTRY=
			"IDENTIFICATIONCOUNTRY";
	
	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFIDENTIFICATIONID,
				COLUMN_DFIDENTIFICATIONID,
				QUALIFIED_COLUMN_DFIDENTIFICATIONID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFIDENTIFICATIONTYPEID,
					COLUMN_DFIDENTIFICATIONTYPEID,
					QUALIFIED_COLUMN_DFIDENTIFICATIONTYPEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFIDENTIFICATIONNUMBER,
					COLUMN_DFIDENTIFICATIONNUMBER,
					QUALIFIED_COLUMN_DFIDENTIFICATIONNUMBER,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFIDENTIFICATIONCOUNTRY,
					COLUMN_DFIDENTIFICATIONCOUNTRY,
					QUALIFIED_COLUMN_DFIDENTIFICATIONCOUNTRY,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
	}
}
