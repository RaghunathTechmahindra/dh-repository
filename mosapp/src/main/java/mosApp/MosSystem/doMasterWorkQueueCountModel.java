package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

//--> Performance enhancement : elimination of the 2nd call of the DataObject
//--> the DealId, ApplicationId, CopyId and WFTaskId will be stored and retrieved in
//--> the hidden values on each row.
//--> By Billy 16Jan2004

/**
 *
 *
 *
 */
public interface doMasterWorkQueueCountModel extends QueryModel, SelectQueryModel
{
  /**
   *
   *
   */
  public java.math.BigDecimal getDfWQNumOfRows();

  /**
   *
   *
   */
  public void setDfWQNumOfRows(java.math.BigDecimal value);

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  public static final String FIELD_DFWQNUMOFROWS="dfWQNumOfRows";

  //=================================================================================

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

}

