package mosApp.MosSystem;

import java.text.*;
import java.util.*;

import MosSystem.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;


/**
 *
 *
 */
public class PortfolioStatsHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public PortfolioStatsHandler cloneSS()
	{
		return(PortfolioStatsHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();

		populatePreviousPagesLinks();

		getCurrNDPage().setDisplayFieldValue("stDateMask", new String(Sc.STANDARD_DATE_INPUT_FORMAT));

    //// Data validation API implementation.
    try
    {
      String portfolioStatsValidPath = Sc.DVALID_PORTFOLIO_STATS_PROPS_FILE_NAME;

      logger.debug("PSH@portfolioStatsValidPath: " + portfolioStatsValidPath);

      JSValidationRules portfolioStatsValidObj = new JSValidationRules(portfolioStatsValidPath, logger, getCurrNDPage());
      portfolioStatsValidObj.attachJSValidationRules();
    }
    catch( Exception e )
    {
        logger.error("Exception PSH@PopulatePageDisplayFields: Problem encountered in JS data validation API." + e.getMessage());
    }
	}


	/////////////////////////////////////////////////////////////////////
  /**
	public void setJSValidation()
	{

		// Validate # of Dependants
		DisplayField df = null;

		String theExtraHtml = null;

		ViewBean thePage = getCurrNDPage();


		// Set the compare date to today
		//  Validate Month
		df = thePage.getDisplayField("cbMonthFrom");

		theExtraHtml = "onBlur=\"isFieldValidDate('tbYearFrom', 'cbMonthFrom', 'tbDayFrom');\"";

		df.setExtraHtmlText(theExtraHtml);


		//  Validate Day
		df = thePage.getDisplayField("tbDayFrom");

		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('tbYearFrom', 'cbMonthFrom', 'tbDayFrom');\"";

		df.setExtraHtmlText(theExtraHtml);


		// Validate Year
		df = thePage.getDisplayField("tbYearFrom");

		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('tbYearFrom', 'cbMonthFrom', 'tbDayFrom');\"";

		df.setExtraHtmlText(theExtraHtml);

		df = thePage.getDisplayField("cbMonthTo");

		theExtraHtml = "onBlur=\"isFieldValidDate('tbYearTo', 'cbMonthTo', 'tbDayTo');\"";

		df.setExtraHtmlText(theExtraHtml);


		//  Validate Day
		df = thePage.getDisplayField("tbDayTo");

		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDate('tbYearTo', 'cbMonthTo', 'tbDayTo');\"";

		df.setExtraHtmlText(theExtraHtml);


		// Validate Year
		df = thePage.getDisplayField("tbYearTo");

		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDate('tbYearTo', 'cbMonthTo', 'tbDayTo');\"";

		df.setExtraHtmlText(theExtraHtml);

	}
  **/

	/////////////////////////////////////////////////////////////////////////
	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);

	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		handleCancelStandard();

	}

}

