package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class pgDealSummaryEmploymentTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryEmploymentTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewEmploymentModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStIDEmployerName().setValue(CHILD_STIDEMPLOYERNAME_RESET_VALUE);
		getStIDEmploymentStatus().setValue(CHILD_STIDEMPLOYMENTSTATUS_RESET_VALUE);
		getStIDEmploymentType().setValue(CHILD_STIDEMPLOYMENTTYPE_RESET_VALUE);
		getStIDEmplAddr1().setValue(CHILD_STIDEMPLADDR1_RESET_VALUE);
		getStIDEmplAddr2().setValue(CHILD_STIDEMPLADDR2_RESET_VALUE);
		getStIDEmplCity().setValue(CHILD_STIDEMPLCITY_RESET_VALUE);
		getStIDEmplProv().setValue(CHILD_STIDEMPLPROV_RESET_VALUE);
		getStIDEmplPostCode1().setValue(CHILD_STIDEMPLPOSTCODE1_RESET_VALUE);
		getStIDEmplPostCode2().setValue(CHILD_STIDEMPLPOSTCODE2_RESET_VALUE);
		getStIDWorkPhone().setValue(CHILD_STIDWORKPHONE_RESET_VALUE);
		getStIDFaxNum().setValue(CHILD_STIDFAXNUM_RESET_VALUE);
		getStIDEmployeeEmailAddress().setValue(CHILD_STIDEMPLOYEEEMAILADDRESS_RESET_VALUE);
		getStIDIndustrySector().setValue(CHILD_STIDINDUSTRYSECTOR_RESET_VALUE);
		getStIDOccupation().setValue(CHILD_STIDOCCUPATION_RESET_VALUE);
		getStIDJobTitle().setValue(CHILD_STIDJOBTITLE_RESET_VALUE);
		getStIDTimeAtJob().setValue(CHILD_STIDTIMEATJOB_RESET_VALUE);
		getStIDMIncomeType().setValue(CHILD_STIDMINCOMETYPE_RESET_VALUE);
		getStIDMIncomeDesc().setValue(CHILD_STIDMINCOMEDESC_RESET_VALUE);
		getStIDMIncomePeriod().setValue(CHILD_STIDMINCOMEPERIOD_RESET_VALUE);
		getStIDMIncomeAmount().setValue(CHILD_STIDMINCOMEAMOUNT_RESET_VALUE);
		getStIDMIncomePercentIncludeGDS().setValue(CHILD_STIDMINCOMEPERCENTINCLUDEGDS_RESET_VALUE);
		getStIDMIncomePercentIncludeTDS().setValue(CHILD_STIDMINCOMEPERCENTINCLUDETDS_RESET_VALUE);
		getStIDMWorkPhoneExt().setValue(CHILD_STIDMWORKPHONEEXT_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STIDEMPLOYERNAME,StaticTextField.class);
		registerChild(CHILD_STIDEMPLOYMENTSTATUS,StaticTextField.class);
		registerChild(CHILD_STIDEMPLOYMENTTYPE,StaticTextField.class);
		registerChild(CHILD_STIDEMPLADDR1,StaticTextField.class);
		registerChild(CHILD_STIDEMPLADDR2,StaticTextField.class);
		registerChild(CHILD_STIDEMPLCITY,StaticTextField.class);
		registerChild(CHILD_STIDEMPLPROV,StaticTextField.class);
		registerChild(CHILD_STIDEMPLPOSTCODE1,StaticTextField.class);
		registerChild(CHILD_STIDEMPLPOSTCODE2,StaticTextField.class);
		registerChild(CHILD_STIDWORKPHONE,StaticTextField.class);
		registerChild(CHILD_STIDFAXNUM,StaticTextField.class);
		registerChild(CHILD_STIDEMPLOYEEEMAILADDRESS,StaticTextField.class);
		registerChild(CHILD_STIDINDUSTRYSECTOR,StaticTextField.class);
		registerChild(CHILD_STIDOCCUPATION,StaticTextField.class);
		registerChild(CHILD_STIDJOBTITLE,StaticTextField.class);
		registerChild(CHILD_STIDTIMEATJOB,StaticTextField.class);
		registerChild(CHILD_STIDMINCOMETYPE,StaticTextField.class);
		registerChild(CHILD_STIDMINCOMEDESC,StaticTextField.class);
		registerChild(CHILD_STIDMINCOMEPERIOD,StaticTextField.class);
		registerChild(CHILD_STIDMINCOMEAMOUNT,StaticTextField.class);
		registerChild(CHILD_STIDMINCOMEPERCENTINCLUDEGDS,StaticTextField.class);
		registerChild(CHILD_STIDMINCOMEPERCENTINCLUDETDS,StaticTextField.class);
		registerChild(CHILD_STIDMWORKPHONEEXT,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewEmploymentModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the Employment_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedEmployment(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the Employment_onBeforeRowDisplayEvent method
      DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      handler.setEmployment(this.getTileIndex());
      handler.populateBorrowerIncomes(this.getTileIndex(), this);
      handler.pageSaveState();
 		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STIDEMPLOYERNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLOYERNAME,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERNAME,
				CHILD_STIDEMPLOYERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLOYMENTSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLOYMENTSTATUS,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYMENTSTATUS,
				CHILD_STIDEMPLOYMENTSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLOYMENTTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLOYMENTTYPE,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYMENTTYPE,
				CHILD_STIDEMPLOYMENTTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLADDR1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLADDR1,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERADDRESSLINE1,
				CHILD_STIDEMPLADDR1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLADDR2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLADDR2,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERADDRESSLINE2,
				CHILD_STIDEMPLADDR2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLCITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLCITY,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERCITY,
				CHILD_STIDEMPLCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLPROV))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLPROV,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERPROVINCE,
				CHILD_STIDEMPLPROV_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLPOSTCODE1))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLPOSTCODE1,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERPOSTALFSA,
				CHILD_STIDEMPLPOSTCODE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLPOSTCODE2))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLPOSTCODE2,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERPOSTALLDU,
				CHILD_STIDEMPLPOSTCODE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDWORKPHONE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDWORKPHONE,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERPHONENO,
				CHILD_STIDWORKPHONE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDFAXNUM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDFAXNUM,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYERFAXNO,
				CHILD_STIDFAXNUM_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDEMPLOYEEEMAILADDRESS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDEMPLOYEEEMAILADDRESS,
				doDetailViewEmploymentModel.FIELD_DFEMPLOYEREMAILADDRESS,
				CHILD_STIDEMPLOYEEEMAILADDRESS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDINDUSTRYSECTOR))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDINDUSTRYSECTOR,
				doDetailViewEmploymentModel.FIELD_DFINDUSTRYSECTOR,
				CHILD_STIDINDUSTRYSECTOR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDOCCUPATION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDOCCUPATION,
				doDetailViewEmploymentModel.FIELD_DFOCCUPATION,
				CHILD_STIDOCCUPATION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDJOBTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDJOBTITLE,
				doDetailViewEmploymentModel.FIELD_DFJOBTITLE1,
				CHILD_STIDJOBTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDTIMEATJOB))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDTIMEATJOB,
				doDetailViewEmploymentModel.FIELD_DFTIMEATJOB,
				CHILD_STIDTIMEATJOB_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDMINCOMETYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STIDMINCOMETYPE,
				CHILD_STIDMINCOMETYPE,
				CHILD_STIDMINCOMETYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDMINCOMEDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STIDMINCOMEDESC,
				CHILD_STIDMINCOMEDESC,
				CHILD_STIDMINCOMEDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDMINCOMEPERIOD))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STIDMINCOMEPERIOD,
				CHILD_STIDMINCOMEPERIOD,
				CHILD_STIDMINCOMEPERIOD_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDMINCOMEAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STIDMINCOMEAMOUNT,
				CHILD_STIDMINCOMEAMOUNT,
				CHILD_STIDMINCOMEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDMINCOMEPERCENTINCLUDEGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STIDMINCOMEPERCENTINCLUDEGDS,
				CHILD_STIDMINCOMEPERCENTINCLUDEGDS,
				CHILD_STIDMINCOMEPERCENTINCLUDEGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDMINCOMEPERCENTINCLUDETDS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STIDMINCOMEPERCENTINCLUDETDS,
				CHILD_STIDMINCOMEPERCENTINCLUDETDS,
				CHILD_STIDMINCOMEPERCENTINCLUDETDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDMWORKPHONEEXT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmploymentModel(),
				CHILD_STIDMWORKPHONEEXT,
				doDetailViewEmploymentModel.FIELD_DFEMPWORKPHONEEXT,
				CHILD_STIDMWORKPHONEEXT_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmployerName()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLOYERNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmploymentStatus()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLOYMENTSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmploymentType()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLOYMENTTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmplAddr1()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLADDR1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmplAddr2()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLADDR2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmplCity()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLCITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmplProv()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLPROV);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmplPostCode1()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLPOSTCODE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmplPostCode2()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLPOSTCODE2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDWorkPhone()
	{
		return (StaticTextField)getChild(CHILD_STIDWORKPHONE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDFaxNum()
	{
		return (StaticTextField)getChild(CHILD_STIDFAXNUM);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDEmployeeEmailAddress()
	{
		return (StaticTextField)getChild(CHILD_STIDEMPLOYEEEMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDIndustrySector()
	{
		return (StaticTextField)getChild(CHILD_STIDINDUSTRYSECTOR);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDOccupation()
	{
		return (StaticTextField)getChild(CHILD_STIDOCCUPATION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDJobTitle()
	{
		return (StaticTextField)getChild(CHILD_STIDJOBTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDTimeAtJob()
	{
		return (StaticTextField)getChild(CHILD_STIDTIMEATJOB);
	}


	/**
	 *
	 *
	 */
	public String endStIDTimeAtJobDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stIDTimeAtJob_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setMonthsToYearsMonths(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDMIncomeType()
	{
		return (StaticTextField)getChild(CHILD_STIDMINCOMETYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDMIncomeDesc()
	{
		return (StaticTextField)getChild(CHILD_STIDMINCOMEDESC);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDMIncomePeriod()
	{
		return (StaticTextField)getChild(CHILD_STIDMINCOMEPERIOD);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDMIncomeAmount()
	{
		return (StaticTextField)getChild(CHILD_STIDMINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDMIncomePercentIncludeGDS()
	{
		return (StaticTextField)getChild(CHILD_STIDMINCOMEPERCENTINCLUDEGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDMIncomePercentIncludeTDS()
	{
		return (StaticTextField)getChild(CHILD_STIDMINCOMEPERCENTINCLUDETDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDMWorkPhoneExt()
	{
		return (StaticTextField)getChild(CHILD_STIDMWORKPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public String endStIDMWorkPhoneExtDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stIDMWorkPhoneExt_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setXPhoneExtInsideEmpRepeated(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public doDetailViewEmploymentModel getdoDetailViewEmploymentModel()
	{
		if (doDetailViewEmployment == null)
			doDetailViewEmployment = (doDetailViewEmploymentModel) getModel(doDetailViewEmploymentModel.class);
		return doDetailViewEmployment;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewEmploymentModel(doDetailViewEmploymentModel model)
	{
			doDetailViewEmployment = model;
	}

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STIDEMPLOYERNAME="stIDEmployerName";
	public static final String CHILD_STIDEMPLOYERNAME_RESET_VALUE="";
	public static final String CHILD_STIDEMPLOYMENTSTATUS="stIDEmploymentStatus";
	public static final String CHILD_STIDEMPLOYMENTSTATUS_RESET_VALUE="";
	public static final String CHILD_STIDEMPLOYMENTTYPE="stIDEmploymentType";
	public static final String CHILD_STIDEMPLOYMENTTYPE_RESET_VALUE="";
	public static final String CHILD_STIDEMPLADDR1="stIDEmplAddr1";
	public static final String CHILD_STIDEMPLADDR1_RESET_VALUE="";
	public static final String CHILD_STIDEMPLADDR2="stIDEmplAddr2";
	public static final String CHILD_STIDEMPLADDR2_RESET_VALUE="";
	public static final String CHILD_STIDEMPLCITY="stIDEmplCity";
	public static final String CHILD_STIDEMPLCITY_RESET_VALUE="";
	public static final String CHILD_STIDEMPLPROV="stIDEmplProv";
	public static final String CHILD_STIDEMPLPROV_RESET_VALUE="";
	public static final String CHILD_STIDEMPLPOSTCODE1="stIDEmplPostCode1";
	public static final String CHILD_STIDEMPLPOSTCODE1_RESET_VALUE="";
	public static final String CHILD_STIDEMPLPOSTCODE2="stIDEmplPostCode2";
	public static final String CHILD_STIDEMPLPOSTCODE2_RESET_VALUE="";
	public static final String CHILD_STIDWORKPHONE="stIDWorkPhone";
	public static final String CHILD_STIDWORKPHONE_RESET_VALUE="";
	public static final String CHILD_STIDFAXNUM="stIDFaxNum";
	public static final String CHILD_STIDFAXNUM_RESET_VALUE="";
	public static final String CHILD_STIDEMPLOYEEEMAILADDRESS="stIDEmployeeEmailAddress";
	public static final String CHILD_STIDEMPLOYEEEMAILADDRESS_RESET_VALUE="";
	public static final String CHILD_STIDINDUSTRYSECTOR="stIDIndustrySector";
	public static final String CHILD_STIDINDUSTRYSECTOR_RESET_VALUE="";
	public static final String CHILD_STIDOCCUPATION="stIDOccupation";
	public static final String CHILD_STIDOCCUPATION_RESET_VALUE="";
	public static final String CHILD_STIDJOBTITLE="stIDJobTitle";
	public static final String CHILD_STIDJOBTITLE_RESET_VALUE="";
	public static final String CHILD_STIDTIMEATJOB="stIDTimeAtJob";
	public static final String CHILD_STIDTIMEATJOB_RESET_VALUE="";
	public static final String CHILD_STIDMINCOMETYPE="stIDMIncomeType";
	public static final String CHILD_STIDMINCOMETYPE_RESET_VALUE="";
	public static final String CHILD_STIDMINCOMEDESC="stIDMIncomeDesc";
	public static final String CHILD_STIDMINCOMEDESC_RESET_VALUE="";
	public static final String CHILD_STIDMINCOMEPERIOD="stIDMIncomePeriod";
	public static final String CHILD_STIDMINCOMEPERIOD_RESET_VALUE="";
	public static final String CHILD_STIDMINCOMEAMOUNT="stIDMIncomeAmount";
	public static final String CHILD_STIDMINCOMEAMOUNT_RESET_VALUE="";
	public static final String CHILD_STIDMINCOMEPERCENTINCLUDEGDS="stIDMIncomePercentIncludeGDS";
	public static final String CHILD_STIDMINCOMEPERCENTINCLUDEGDS_RESET_VALUE="";
	public static final String CHILD_STIDMINCOMEPERCENTINCLUDETDS="stIDMIncomePercentIncludeTDS";
	public static final String CHILD_STIDMINCOMEPERCENTINCLUDETDS_RESET_VALUE="";
	public static final String CHILD_STIDMWORKPHONEEXT="stIDMWorkPhoneExt";
	public static final String CHILD_STIDMWORKPHONEEXT_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewEmploymentModel doDetailViewEmployment=null;
  private DealSummaryHandler handler=new DealSummaryHandler();
}

