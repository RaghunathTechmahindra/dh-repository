package mosApp.MosSystem;

import java.sql.SQLException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doUWPropertyInfosModelImpl extends QueryModelBase
	implements doUWPropertyInfosModel
{
	/**
	 *
	 *
	 */
	public doUWPropertyInfosModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{
    ////logger = SysLog.getSysLogger("DOUWPIM");
    ////logger.debug("DOIWQMI@SQLBeforeExecute::SQL: " + sql);

    // TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    ////To override all the Description fields by populating from BXResource.
    ////Get the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
                                          (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                           SessionStateModel.class,
                                           defaultInstanceStateName,
                                           true);

    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    while(this.next())
    {
      //// 1. Convert Street Type Description.
      this.setDfPropertyStreetType(BXResources.getPickListDescription(institutionId, "STREETTYPE", this.getDfPropertyStreetType(), languageId));

      //// 2. Convert Street Direction Description.
      this.setDfPropertyStreetDirection(BXResources.getPickListDescription(institutionId, "STREETDIRECTION", this.getDfPropertyStreetDirection(), languageId));

      //// 3. Convert Property Description Description.
      this.setDfPropertyTypeDesc(BXResources.getPickListDescription(institutionId, "PROPERTYTYPE", this.getDfPropertyTypeDesc(), languageId));

      //// 4. Convert Occupancy Type Description.
      this.setDfPropertyOccupancyTypeDesc(BXResources.getPickListDescription(institutionId, "OCCUPANCYTYPE", this.getDfPropertyOccupancyTypeDesc(), languageId));

      //// 5. Convert Occupancy Type Description.
      this.setDfPropertyProvince(BXResources.getPickListDescription(institutionId, "PROVINCE", this.getDfPropertyProvince(), languageId));

    }

    ////Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryPropertyFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYPROPERTYFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryPropertyFlag(String value)
	{
		setValue(FIELD_DFPRIMARYPROPERTYFLAG,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyStreetNumber()
	{
		return (String)getValue(FIELD_DFPROPERTYSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetNumber(String value)
	{
		setValue(FIELD_DFPROPERTYSTREETNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyStreetName()
	{
		return (String)getValue(FIELD_DFPROPERTYSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetName(String value)
	{
		setValue(FIELD_DFPROPERTYSTREETNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyStreetType()
	{
		return (String)getValue(FIELD_DFPROPERTYSTREETTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetType(String value)
	{
		setValue(FIELD_DFPROPERTYSTREETTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyStreetDirection()
	{
		return (String)getValue(FIELD_DFPROPERTYSTREETDIRECTION);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetDirection(String value)
	{
		setValue(FIELD_DFPROPERTYSTREETDIRECTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyUnitNumber()
	{
		return (String)getValue(FIELD_DFPROPERTYUNITNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyUnitNumber(String value)
	{
		setValue(FIELD_DFPROPERTYUNITNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyCity()
	{
		return (String)getValue(FIELD_DFPROPERTYCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyCity(String value)
	{
		setValue(FIELD_DFPROPERTYCITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyProvince()
	{
		return (String)getValue(FIELD_DFPROPERTYPROVINCE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyProvince(String value)
	{
		setValue(FIELD_DFPROPERTYPROVINCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyPostalFSA()
	{
		return (String)getValue(FIELD_DFPROPERTYPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyPostalFSA(String value)
	{
		setValue(FIELD_DFPROPERTYPOSTALFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyPostalLDU()
	{
		return (String)getValue(FIELD_DFPROPERTYPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyPostalLDU(String value)
	{
		setValue(FIELD_DFPROPERTYPOSTALLDU,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyTotalEstimatedValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYTOTALESTIMATEDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyTotalEstimatedValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYTOTALESTIMATEDVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyActualAppraisalValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYACTUALAPPRAISALVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyActualAppraisalValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYACTUALAPPRAISALVALUE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyTypeDesc()
	{
		return (String)getValue(FIELD_DFPROPERTYTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyTypeDesc(String value)
	{
		setValue(FIELD_DFPROPERTYTYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyOccupancyTypeDesc()
	{
		return (String)getValue(FIELD_DFPROPERTYOCCUPANCYTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyOccupancyTypeDesc(String value)
	{
		setValue(FIELD_DFPROPERTYOCCUPANCYTYPEDESC,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Adjust Field names definitions to string format.

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL PROPERTY.DEALID, PROPERTY.PROPERTYID, PROPERTY.COPYID, PROPERTY.PRIMARYPROPERTYFLAG, PROPERTY.PROPERTYSTREETNUMBER, PROPERTY.PROPERTYSTREETNAME, STREETTYPE.STDESCRIPTION, STREETDIRECTION.SDDESCRIPTION, PROPERTY.UNITNUMBER, PROPERTY.PROPERTYCITY, PROVINCE.PROVINCENAME, PROPERTY.PROPERTYPOSTALFSA, PROPERTY.PROPERTYPOSTALLDU, PROPERTY.PURCHASEPRICE, PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.ACTUALAPPRAISALVALUE, PROPERTYTYPE.PTDESCRIPTION, OCCUPANCYTYPE.OTDESCRIPTION FROM PROPERTY, PROVINCE, PROPERTYTYPE, STREETTYPE, STREETDIRECTION, OCCUPANCYTYPE  __WHERE__  ";

	public static final String SELECT_SQL_TEMPLATE="SELECT distinct PROPERTY.DEALID, PROPERTY.PROPERTYID, " +
  "PROPERTY.COPYID, PROPERTY.PRIMARYPROPERTYFLAG, PROPERTY.PROPERTYSTREETNUMBER, " +
  "PROPERTY.PROPERTYSTREETNAME, to_char(PROPERTY.STREETTYPEID) STREETTYPEID_STR, " +
  "to_char(PROPERTY.STREETDIRECTIONID) STREETDIRECTIONID_STR, PROPERTY.UNITNUMBER, " +
  "PROPERTY.PROPERTYCITY, to_char(PROPERTY.PROVINCEID) PROVINCEID_STR, PROPERTY.PROPERTYPOSTALFSA, " +
  "PROPERTY.PROPERTYPOSTALLDU, PROPERTY.PURCHASEPRICE, " +
  "PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.ACTUALAPPRAISALVALUE, " +
  "to_char(PROPERTY.PROPERTYTYPEID) PROPERTYTYPEID_STR, to_char(PROPERTY.OCCUPANCYTYPEID) OCCUPANCYTYPEID_STR " +
  "FROM PROPERTY, PROVINCE, PROPERTYTYPE, STREETTYPE, STREETDIRECTION, OCCUPANCYTYPE " +
  "__WHERE__  ";

	////public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTY, PROVINCE, PROPERTYTYPE, STREETTYPE, STREETDIRECTION, OCCUPANCYTYPE";
  //--Release2.1--//
	public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTY";

	////public static final String STATIC_WHERE_CRITERIA=" (PROPERTY.PROPERTYTYPEID  =  PROPERTYTYPE.PROPERTYTYPEID) AND (PROPERTY.PROVINCEID  =  PROVINCE.PROVINCEID) AND (PROPERTY.STREETDIRECTIONID  =  STREETDIRECTION.STREETDIRECTIONID) AND (PROPERTY.STREETTYPEID  =  STREETTYPE.STREETTYPEID) AND (PROPERTY.OCCUPANCYTYPEID  =  OCCUPANCYTYPE.OCCUPANCYTYPEID)";
	public static final String STATIC_WHERE_CRITERIA=" (PROPERTY.PROPERTYTYPEID  =  PROPERTYTYPE.PROPERTYTYPEID) " +
                                                      "AND (PROPERTY.PROVINCEID  =  PROVINCE.PROVINCEID) " +
                                                      "AND (PROPERTY.STREETDIRECTIONID  =  STREETDIRECTION.STREETDIRECTIONID) " +
                                                      "AND (PROPERTY.STREETTYPEID  =  STREETTYPE.STREETTYPEID) " +
                                                      "AND (PROPERTY.OCCUPANCYTYPEID  =  OCCUPANCYTYPE.OCCUPANCYTYPEID)";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="PROPERTY.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTY.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYPROPERTYFLAG="PROPERTY.PRIMARYPROPERTYFLAG";
	public static final String COLUMN_DFPRIMARYPROPERTYFLAG="PRIMARYPROPERTYFLAG";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETNUMBER="PROPERTY.PROPERTYSTREETNUMBER";
	public static final String COLUMN_DFPROPERTYSTREETNUMBER="PROPERTYSTREETNUMBER";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETNAME="PROPERTY.PROPERTYSTREETNAME";
	public static final String COLUMN_DFPROPERTYSTREETNAME="PROPERTYSTREETNAME";

  //--Release2.1--//
  //// 3. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETTYPE="STREETTYPE.STDESCRIPTION";
	////public static final String COLUMN_DFPROPERTYSTREETTYPE="STDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETTYPE="PROPERTY.STREETTYPEID_STR";
	public static final String COLUMN_DFPROPERTYSTREETTYPE="STREETTYPEID_STR";

  //--Release2.1--//
  //// 3. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETDIRECTION="STREETDIRECTION.SDDESCRIPTION";
	////public static final String COLUMN_DFPROPERTYSTREETDIRECTION="SDDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETDIRECTION="PROPERTY.STREETDIRECTIONID_STR";
	public static final String COLUMN_DFPROPERTYSTREETDIRECTION="STREETDIRECTIONID_STR";

	public static final String QUALIFIED_COLUMN_DFPROPERTYUNITNUMBER="PROPERTY.UNITNUMBER";
	public static final String COLUMN_DFPROPERTYUNITNUMBER="UNITNUMBER";
	public static final String QUALIFIED_COLUMN_DFPROPERTYCITY="PROPERTY.PROPERTYCITY";
	public static final String COLUMN_DFPROPERTYCITY="PROPERTYCITY";

  //--Release2.1--//
  //// 3. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFPROPERTYPROVINCE="PROVINCE.PROVINCENAME";
	////public static final String COLUMN_DFPROPERTYPROVINCE="PROVINCENAME";
	public static final String QUALIFIED_COLUMN_DFPROPERTYPROVINCE="PROPERTY.PROVINCEID_STR";
	public static final String COLUMN_DFPROPERTYPROVINCE="PROVINCEID_STR";

	public static final String QUALIFIED_COLUMN_DFPROPERTYPOSTALFSA="PROPERTY.PROPERTYPOSTALFSA";
	public static final String COLUMN_DFPROPERTYPOSTALFSA="PROPERTYPOSTALFSA";
	public static final String QUALIFIED_COLUMN_DFPROPERTYPOSTALLDU="PROPERTY.PROPERTYPOSTALLDU";
	public static final String COLUMN_DFPROPERTYPOSTALLDU="PROPERTYPOSTALLDU";
	public static final String QUALIFIED_COLUMN_DFPROPERTYPURCHASEPRICE="PROPERTY.PURCHASEPRICE";
	public static final String COLUMN_DFPROPERTYPURCHASEPRICE="PURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFPROPERTYTOTALESTIMATEDVALUE="PROPERTY.ESTIMATEDAPPRAISALVALUE";
	public static final String COLUMN_DFPROPERTYTOTALESTIMATEDVALUE="ESTIMATEDAPPRAISALVALUE";
	public static final String QUALIFIED_COLUMN_DFPROPERTYACTUALAPPRAISALVALUE="PROPERTY.ACTUALAPPRAISALVALUE";
	public static final String COLUMN_DFPROPERTYACTUALAPPRAISALVALUE="ACTUALAPPRAISALVALUE";

  //--Release2.1--//
  //// 3. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFPROPERTYTYPEDESC="PROPERTYTYPE.PTDESCRIPTION";
	////public static final String COLUMN_DFPROPERTYTYPEDESC="PTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPROPERTYTYPEDESC="PROPERTY.PROPERTYTYPEID_STR";
	public static final String COLUMN_DFPROPERTYTYPEDESC="PROPERTYTYPEID_STR";

	////public static final String QUALIFIED_COLUMN_DFPROPERTYOCCUPANCYTYPEDESC="OCCUPANCYTYPE.OTDESCRIPTION";
	////public static final String COLUMN_DFPROPERTYOCCUPANCYTYPEDESC="OTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPROPERTYOCCUPANCYTYPEDESC="PROPERTY.OCCUPANCYTYPEID_STR";
	public static final String COLUMN_DFPROPERTYOCCUPANCYTYPEDESC="OCCUPANCYTYPEID_STR";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYPROPERTYFLAG,
				COLUMN_DFPRIMARYPROPERTYFLAG,
				QUALIFIED_COLUMN_DFPRIMARYPROPERTYFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETNUMBER,
				COLUMN_DFPROPERTYSTREETNUMBER,
				QUALIFIED_COLUMN_DFPROPERTYSTREETNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETNAME,
				COLUMN_DFPROPERTYSTREETNAME,
				QUALIFIED_COLUMN_DFPROPERTYSTREETNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETTYPE,
				COLUMN_DFPROPERTYSTREETTYPE,
				QUALIFIED_COLUMN_DFPROPERTYSTREETTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETDIRECTION,
				COLUMN_DFPROPERTYSTREETDIRECTION,
				QUALIFIED_COLUMN_DFPROPERTYSTREETDIRECTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYUNITNUMBER,
				COLUMN_DFPROPERTYUNITNUMBER,
				QUALIFIED_COLUMN_DFPROPERTYUNITNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYCITY,
				COLUMN_DFPROPERTYCITY,
				QUALIFIED_COLUMN_DFPROPERTYCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYPROVINCE,
				COLUMN_DFPROPERTYPROVINCE,
				QUALIFIED_COLUMN_DFPROPERTYPROVINCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYPOSTALFSA,
				COLUMN_DFPROPERTYPOSTALFSA,
				QUALIFIED_COLUMN_DFPROPERTYPOSTALFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYPOSTALLDU,
				COLUMN_DFPROPERTYPOSTALLDU,
				QUALIFIED_COLUMN_DFPROPERTYPOSTALLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYPURCHASEPRICE,
				COLUMN_DFPROPERTYPURCHASEPRICE,
				QUALIFIED_COLUMN_DFPROPERTYPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYTOTALESTIMATEDVALUE,
				COLUMN_DFPROPERTYTOTALESTIMATEDVALUE,
				QUALIFIED_COLUMN_DFPROPERTYTOTALESTIMATEDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYACTUALAPPRAISALVALUE,
				COLUMN_DFPROPERTYACTUALAPPRAISALVALUE,
				QUALIFIED_COLUMN_DFPROPERTYACTUALAPPRAISALVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYTYPEDESC,
				COLUMN_DFPROPERTYTYPEDESC,
				QUALIFIED_COLUMN_DFPROPERTYTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYOCCUPANCYTYPEDESC,
				COLUMN_DFPROPERTYOCCUPANCYTYPEDESC,
				QUALIFIED_COLUMN_DFPROPERTYOCCUPANCYTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

