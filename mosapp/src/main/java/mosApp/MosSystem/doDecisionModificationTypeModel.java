package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDecisionModificationTypeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDecisionModificationType();

	
	/**
	 * 
	 * 
	 */
	public void setDfDecisionModificationType(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDecisionModificationDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfDecisionModificationDescription(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDECISIONMODIFICATIONTYPE="dfDecisionModificationType";
	public static final String FIELD_DFDECISIONMODIFICATIONDESCRIPTION="dfDecisionModificationDescription";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

