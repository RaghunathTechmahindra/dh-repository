package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doClosingDetailsModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfEstimatedClosingDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfEstimatedClosingDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfInterestAdjustmentDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfInterestAdjustmentDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIADNumberOfDays();

	
	/**
	 * 
	 * 
	 */
	public void setDfIADNumberOfDays(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfFirstPaymentDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfFirstPaymentDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfFirstPaymentDateMonthly();

	
	/**
	 * 
	 * 
	 */
	public void setDfFirstPaymentDateMonthly(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfMaturityDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfMaturityDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPerDiemInterest();

	
	/**
	 * 
	 * 
	 */
	public void setDfPerDiemInterest(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNetInterestRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfNetInterestRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfTotalLoanBridgeAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfTotalLoanBridgeAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIADInterest();

	
	/**
	 * 
	 * 
	 */
	public void setDfIADInterest(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfInterestCompoundDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfInterestCompoundDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAdvanceHoldBack();

	
	/**
	 * 
	 * 
	 */
	public void setDfAdvanceHoldBack(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPaymantFreqId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPaymantFreqId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFESTIMATEDCLOSINGDATE="dfEstimatedClosingDate";
	public static final String FIELD_DFINTERESTADJUSTMENTDATE="dfInterestAdjustmentDate";
	public static final String FIELD_DFIADNUMBEROFDAYS="dfIADNumberOfDays";
	public static final String FIELD_DFFIRSTPAYMENTDATE="dfFirstPaymentDate";
	public static final String FIELD_DFFIRSTPAYMENTDATEMONTHLY="dfFirstPaymentDateMonthly";
	public static final String FIELD_DFMATURITYDATE="dfMaturityDate";
	public static final String FIELD_DFPERDIEMINTEREST="dfPerDiemInterest";
	public static final String FIELD_DFNETINTERESTRATE="dfNetInterestRate";
	public static final String FIELD_DFTOTALLOANBRIDGEAMOUNT="dfTotalLoanBridgeAmount";
	public static final String FIELD_DFIADINTEREST="dfIADInterest";
	public static final String FIELD_DFINTERESTCOMPOUNDDESC="dfInterestCompoundDesc";
	public static final String FIELD_DFADVANCEHOLDBACK="dfAdvanceHoldBack";
	public static final String FIELD_DFPAYMANTFREQID="dfPaymantFreqId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

