package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import config.DocTrackingDetailsConfigManager;
import com.filogix.express.web.jato.ExpressViewBeanBase; 
/**
 *
 *
 *
 */
public class pgDocTrackingDetailsViewBean extends ExpressViewBeanBase

{
	/**
	 *
	 *
	 */
	public pgDocTrackingDetailsViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		View superReturn = super.createChild(name);  
		if (superReturn != null) {  
		return superReturn;  
		} else if (name.equals(CHILD_TBDEALID)) 
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);
      //--Release2.1--//
      ////Modified to set NonSelected Label as a CHILD_CBPAGENAMES_NONSELECTED_LABEL
			////child.setLabelForNoneSelected("Choose a Page");
			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDEALID))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDEALID,
				CHILD_STDEALID,
				CHILD_STDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPRIMARYBORROWER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPRIMARYBORROWER,
				CHILD_STPRIMARYBORROWER,
				CHILD_STPRIMARYBORROWER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STROLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STROLE,
				CHILD_STROLE,
				CHILD_STROLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDOCUMENTLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDOCUMENTLABEL,
				CHILD_STDOCUMENTLABEL,
				CHILD_STDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMMENTOPEN))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMMENTOPEN,
				CHILD_STCOMMENTOPEN,
				CHILD_STCOMMENTOPEN_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCONTACTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCONTACTNAME,
				CHILD_STCONTACTNAME,
				CHILD_STCONTACTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPHONE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPHONE,
				CHILD_STPHONE,
				CHILD_STPHONE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFAX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STFAX,
				CHILD_STFAX,
				CHILD_STFAX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEMAIL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STEMAIL,
				CHILD_STEMAIL,
				CHILD_STEMAIL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADDRLINE1))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STADDRLINE1,
				CHILD_STADDRLINE1,
				CHILD_STADDRLINE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBUSINESSIDPREFIX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBUSINESSIDPREFIX,
				CHILD_STBUSINESSIDPREFIX,
				CHILD_STBUSINESSIDPREFIX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBUSINESSID))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBUSINESSID,
				CHILD_STBUSINESSID,
				CHILD_STBUSINESSID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBUSINESSIDSUFFIX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBUSINESSIDSUFFIX,
				CHILD_STBUSINESSIDSUFFIX,
				CHILD_STBUSINESSIDSUFFIX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYTYPEPREFIX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPARTYTYPEPREFIX,
				CHILD_STPARTYTYPEPREFIX,
				CHILD_STPARTYTYPEPREFIX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPARTYTYPE,
				CHILD_STPARTYTYPE,
				CHILD_STPARTYTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYTYPESUFFIX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPARTYTYPESUFFIX,
				CHILD_STPARTYTYPESUFFIX,
				CHILD_STPARTYTYPESUFFIX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCONTACTCOMPANYNAMEPREFIX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCONTACTCOMPANYNAMEPREFIX,
				CHILD_STCONTACTCOMPANYNAMEPREFIX,
				CHILD_STCONTACTCOMPANYNAMEPREFIX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCONTACTCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCONTACTCOMPANYNAME,
				CHILD_STCONTACTCOMPANYNAME,
				CHILD_STCONTACTCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCONTACTCOMPANYNAMESUFFIX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCONTACTCOMPANYNAMESUFFIX,
				CHILD_STCONTACTCOMPANYNAMESUFFIX,
				CHILD_STCONTACTCOMPANYNAMESUFFIX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADDRLINE2))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STADDRLINE2,
				CHILD_STADDRLINE2,
				CHILD_STADDRLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STREQUESTDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STREQUESTDATE,
				CHILD_STREQUESTDATE,
				CHILD_STREQUESTDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDOCSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDOCSTATUSDATE,
				CHILD_STDOCSTATUSDATE,
				CHILD_STDOCSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDOCSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDOCSTATUS,
				CHILD_STDOCSTATUS,
				CHILD_STDOCSTATUS_RESET_VALUE,
				null);
			return child;
		}
    //--Release2.1--TD_DTS_CR--start//
    //// Condition Text Field should be editable under certain circumstances.
		else
		if (name.equals(CHILD_TXENGDOCTEXT))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TXENGDOCTEXT,
				CHILD_TXENGDOCTEXT,
				CHILD_TXENGDOCTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TXFRENCHDOCTEXT))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TXFRENCHDOCTEXT,
				CHILD_TXFRENCHDOCTEXT,
				CHILD_TXFRENCHDOCTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
    //// DocumentTrackingId should be passed to the DocTrackDetails screen in
    //// order to update the English and French document's text fields.
		else
		if (name.equals(CHILD_HDDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_HDDOCTRACKID,
				CHILD_HDDOCTRACKID,
				CHILD_HDDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
    //--Release2.1--TD_DTS_CR--end//
		else
		if (name.equals(CHILD_STCOMMENTCLOSE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMMENTCLOSE,
				CHILD_STCOMMENTCLOSE,
				CHILD_STCOMMENTCLOSE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDEMPTYLINKMAIN))
		{
			HiddenField child = new HiddenField(this,
				getdoDealNotesInfoModel(),
				CHILD_HDEMPTYLINKMAIN,
				doDealNotesInfoModel.FIELD_DFUSERNAME,
				CHILD_HDEMPTYLINKMAIN_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATED1))
		{
			pgDocTrackingDetailsRepeated1TiledView child = new pgDocTrackingDetailsRepeated1TiledView(this,
				CHILD_REPEATED1);
			return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STVIEWONLYTAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG_RESET_VALUE,
				null);
			return child;
		}
    //// Hidden button to trigger the ActiveMessage 'fake' submit button
    //// via the new BX ActMessageCommand class
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		super.resetChildren(); 
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
		getStPrimaryBorrower().setValue(CHILD_STPRIMARYBORROWER_RESET_VALUE);
		getStRole().setValue(CHILD_STROLE_RESET_VALUE);
		getStDocumentLabel().setValue(CHILD_STDOCUMENTLABEL_RESET_VALUE);
		getStCommentOpen().setValue(CHILD_STCOMMENTOPEN_RESET_VALUE);
		getStContactName().setValue(CHILD_STCONTACTNAME_RESET_VALUE);
		getStPhone().setValue(CHILD_STPHONE_RESET_VALUE);
		getStFax().setValue(CHILD_STFAX_RESET_VALUE);
		getStEmail().setValue(CHILD_STEMAIL_RESET_VALUE);
		getStAddrLine1().setValue(CHILD_STADDRLINE1_RESET_VALUE);
		getStBusinessIdPrefix().setValue(CHILD_STBUSINESSIDPREFIX_RESET_VALUE);
		getStBusinessId().setValue(CHILD_STBUSINESSID_RESET_VALUE);
		getStBusinessIdSuffix().setValue(CHILD_STBUSINESSIDSUFFIX_RESET_VALUE);
		getStPartyTypePrefix().setValue(CHILD_STPARTYTYPEPREFIX_RESET_VALUE);
		getStPartyType().setValue(CHILD_STPARTYTYPE_RESET_VALUE);
		getStPartyTypeSuffix().setValue(CHILD_STPARTYTYPESUFFIX_RESET_VALUE);
		getStContactCompanyNamePrefix().setValue(CHILD_STCONTACTCOMPANYNAMEPREFIX_RESET_VALUE);
		getStContactCompanyName().setValue(CHILD_STCONTACTCOMPANYNAME_RESET_VALUE);
		getStContactCompanyNameSuffix().setValue(CHILD_STCONTACTCOMPANYNAMESUFFIX_RESET_VALUE);
		getStAddrLine2().setValue(CHILD_STADDRLINE2_RESET_VALUE);
		getStRequestDate().setValue(CHILD_STREQUESTDATE_RESET_VALUE);
		getStDocStatusDate().setValue(CHILD_STDOCSTATUSDATE_RESET_VALUE);
		getStDocStatus().setValue(CHILD_STDOCSTATUS_RESET_VALUE);
    //--Release2.1--TD_DTS_CR//
    //// Condition Text Field should be editable under certain circumstances.
		getTxEngDocText().setValue(CHILD_TXENGDOCTEXT_RESET_VALUE);
		getTxFrenchDocText().setValue(CHILD_TXFRENCHDOCTEXT_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
    //// DocumentTrackingId should be passed to the DocTrackDetails screen in
    //// order to update the English and French document's text fields.
    getHdDocTrackId().setValue(CHILD_HDDOCTRACKID_RESET_VALUE);
    //--Release2.1--TD_DTS_CR--end//
		getStCommentClose().setValue(CHILD_STCOMMENTCLOSE_RESET_VALUE);
		getHdEmptyLinkMain().setValue(CHILD_HDEMPTYLINKMAIN_RESET_VALUE);
		getRepeated1().resetChildren();
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
    //// new hidden button to activate 'fake' submit button from the ActiveMessage class.
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren(); 
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_STDEALID,StaticTextField.class);
		registerChild(CHILD_STPRIMARYBORROWER,StaticTextField.class);
		registerChild(CHILD_STROLE,StaticTextField.class);
		registerChild(CHILD_STDOCUMENTLABEL,StaticTextField.class);
		registerChild(CHILD_STCOMMENTOPEN,StaticTextField.class);
		registerChild(CHILD_STCONTACTNAME,StaticTextField.class);
		registerChild(CHILD_STPHONE,StaticTextField.class);
		registerChild(CHILD_STFAX,StaticTextField.class);
		registerChild(CHILD_STEMAIL,StaticTextField.class);
		registerChild(CHILD_STADDRLINE1,StaticTextField.class);
		registerChild(CHILD_STBUSINESSIDPREFIX,StaticTextField.class);
		registerChild(CHILD_STBUSINESSID,StaticTextField.class);
		registerChild(CHILD_STBUSINESSIDSUFFIX,StaticTextField.class);
		registerChild(CHILD_STPARTYTYPEPREFIX,StaticTextField.class);
		registerChild(CHILD_STPARTYTYPE,StaticTextField.class);
		registerChild(CHILD_STPARTYTYPESUFFIX,StaticTextField.class);
		registerChild(CHILD_STCONTACTCOMPANYNAMEPREFIX,StaticTextField.class);
		registerChild(CHILD_STCONTACTCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STCONTACTCOMPANYNAMESUFFIX,StaticTextField.class);
		registerChild(CHILD_STADDRLINE2,StaticTextField.class);
		registerChild(CHILD_STREQUESTDATE,StaticTextField.class);
		registerChild(CHILD_STDOCSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STDOCSTATUS,StaticTextField.class);
    //--Release2.1--TD_DTS_CR//
    //// Condition Text Field should be editable under certain circumstances.
		registerChild(CHILD_TXENGDOCTEXT,TextField.class);
		registerChild(CHILD_TXFRENCHDOCTEXT,TextField.class);
		registerChild(CHILD_BTCANCEL,Button.class);
    //// DocumentTrackingId should be passed to the DocTrackDetails screen in
    //// order to update the English and French document's text fields.
		registerChild(CHILD_HDDOCTRACKID,HiddenField.class);
    //--Release2.1--TD_DTS_CR--end//

		registerChild(CHILD_STCOMMENTCLOSE,StaticTextField.class);
		registerChild(CHILD_HDEMPTYLINKMAIN,HiddenField.class);
		registerChild(CHILD_REPEATED1,pgDocTrackingDetailsRepeated1TiledView.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
    //// New hidden button implementation.
    registerChild(CHILD_BTACTMSG,Button.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealNotesInfoModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
		handler.pageGetState(this);
    //--Release2.1--start//
    ////Populate all ComboBoxes manually here
    //// 1. Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    ////Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
        BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    ////Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    //--Release2.1--end//

    handler.pageSaveState();
    super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatePageDisplayFields();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleGoPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(0);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(1);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(2);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(3);

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(4);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(5);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(6);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(7);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(8);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleOpenDialogLink(9);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}


	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSubmit();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayWorkQueue();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleChangePassword();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolHistory_onWebEvent method
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealHistory();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDisplayDealNotes();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleDealSearch();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleSignOff();

		handler.postHandlerProtocol();

	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.

	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//

	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handlePrevTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(getParentViewBean());

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(getParentViewBean());

		boolean rc = handler.generatePrevTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleNextTaskPage();

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(getParentViewBean());

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(getParentViewBean());

		boolean rc = handler.generateNextTaskPage();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(getParentViewBean());

		boolean rc = handler.generateTaskName();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDealId()
	{
		return (StaticTextField)getChild(CHILD_STDEALID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrimaryBorrower()
	{
		return (StaticTextField)getChild(CHILD_STPRIMARYBORROWER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStRole()
	{
		return (StaticTextField)getChild(CHILD_STROLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDocumentLabel()
	{
		return (StaticTextField)getChild(CHILD_STDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCommentOpen()
	{
		return (StaticTextField)getChild(CHILD_STCOMMENTOPEN);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStContactName()
	{
		return (StaticTextField)getChild(CHILD_STCONTACTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPhone()
	{
		return (StaticTextField)getChild(CHILD_STPHONE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFax()
	{
		return (StaticTextField)getChild(CHILD_STFAX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEmail()
	{
		return (StaticTextField)getChild(CHILD_STEMAIL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAddrLine1()
	{
		return (StaticTextField)getChild(CHILD_STADDRLINE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBusinessIdPrefix()
	{
		return (StaticTextField)getChild(CHILD_STBUSINESSIDPREFIX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBusinessId()
	{
		return (StaticTextField)getChild(CHILD_STBUSINESSID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBusinessIdSuffix()
	{
		return (StaticTextField)getChild(CHILD_STBUSINESSIDSUFFIX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyTypePrefix()
	{
		return (StaticTextField)getChild(CHILD_STPARTYTYPEPREFIX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyType()
	{
		return (StaticTextField)getChild(CHILD_STPARTYTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyTypeSuffix()
	{
		return (StaticTextField)getChild(CHILD_STPARTYTYPESUFFIX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStContactCompanyNamePrefix()
	{
		return (StaticTextField)getChild(CHILD_STCONTACTCOMPANYNAMEPREFIX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStContactCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCONTACTCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStContactCompanyNameSuffix()
	{
		return (StaticTextField)getChild(CHILD_STCONTACTCOMPANYNAMESUFFIX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAddrLine2()
	{
		return (StaticTextField)getChild(CHILD_STADDRLINE2);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStRequestDate()
	{
		return (StaticTextField)getChild(CHILD_STREQUESTDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDocStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STDOCSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDocStatus()
	{
		return (StaticTextField)getChild(CHILD_STDOCSTATUS);
	}


	/**
	 *
	 *
	 */
  //--Release2.1--TD_DTS_CR--start//
  //// Condition Text Field should be editable under certain circumstances.
	public TextField getTxEngDocText()
	{
		return (TextField)getChild(CHILD_TXENGDOCTEXT);
	}

	/**
	 *
	 *
	 */
	public TextField getTxFrenchDocText()
	{
		return (TextField)getChild(CHILD_TXFRENCHDOCTEXT);
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDDOCTRACKID);
	}

	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCancel_onWebEvent method
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		// ALERT - no warn: handler.handleCancelStandard(false);
		handler.handleCancelStandard();
		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}

	/**
	 *
	 *
	 */
	public String endBtCancelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btCancel_onBeforeHtmlOutputEvent method
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayCancelButton();
		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";
	}

  //--Release2.1--TD_DTS_CR--end//

	/**
	 *
	 *
	 */
	public StaticTextField getStCommentClose()
	{
		return (StaticTextField)getChild(CHILD_STCOMMENTCLOSE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEmptyLinkMain()
	{
		return (HiddenField)getChild(CHILD_HDEMPTYLINKMAIN);
	}


	/**
	 *
	 *
	 */
	public pgDocTrackingDetailsRepeated1TiledView getRepeated1()
	{
		return (pgDocTrackingDetailsRepeated1TiledView)getChild(CHILD_REPEATED1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStViewOnlyTag()
	{
		return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
	}


	/**
	 *
	 *
	 */
	public String endStViewOnlyTagDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the stViewOnlyTag_onBeforeHtmlOutputEvent method
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		String rc = handler.displayViewOnlyTag();

		handler.pageSaveState();

    return rc;

	}


	/**
	 *
	 *
	 */
	public doDealNotesInfoModel getdoDealNotesInfoModel()
	{
		if (doDealNotesInfo == null)
			doDealNotesInfo = (doDealNotesInfoModel) getModel(doDealNotesInfoModel.class);
		return doDealNotesInfo;
	}


	/**
	 *
	 *
	 */
	public void setdoDealNotesInfoModel(doDealNotesInfoModel model)
	{
			doDealNotesInfo = model;
	}


  //// New hidden button for the ActiveMessage 'fake' submit button.
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}

  //// Populate previous links new set of methods to populate Href display Strings.
  public String endHref1Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

  //--Release2.1--TD_TDS_CR--start//
  //// End Display events for checking if filed is Hidden
  //// Method to check if the field is Hidden and display the Standard
  //// Hidden Tag if hidden.
  private String commonEndDisplayHandle(ChildContentDisplayEvent event)
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
		handler.pageGetState(this);
    boolean isHidden = handler.isFieldHidden(event.getChildName());
		handler.pageSaveState();

    if(isHidden == true)
      return DocTrackingDetailsConfigManager.HIDDENTAG;
    else
    	return event.getContent();
	}

	/**
	 *
	 *
	 */
  //// Added special handle to check if field is hidden
	public String endTxFrenchDocText(ChildContentDisplayEvent event)
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();
		handler.pageGetState(this);
    String finalHtml;
    boolean isHidden = handler.isFieldHidden(event.getChildName());

    logger = SysLog.getSysLogger("pgDTD");
    logger.debug("pgDTD@endTxFrenchDoc::isHidden? " + isHidden);
    if(isHidden == false)
		  finalHtml = handler.removeJScriptFunction(event.getContent());
    else
      finalHtml = DocTrackingDetailsConfigManager.HIDDENTAG;
		handler.pageSaveState();

		return finalHtml;
	}

  public String endTxEngDocText(ChildContentDisplayEvent event)
	{
    return commonEndDisplayHandle(event);
	}
  //--Release2.1--TD_TDS_CR--end//


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

  public void handleActMessageOK(String[] args)
	{
		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

		handler.handleActMessageOK(args);

    handler.postHandlerProtocol();
	}



	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgDocTrackingDetails";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
	///public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDocTrackingDetails.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";

  //--Release2.1--//
  ////The Option list should not be Static, otherwise this will screwed up the population.
	////private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  ////Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_STDEALID="stDealId";
	public static final String CHILD_STDEALID_RESET_VALUE="";
	public static final String CHILD_STPRIMARYBORROWER="stPrimaryBorrower";
	public static final String CHILD_STPRIMARYBORROWER_RESET_VALUE="";
	public static final String CHILD_STROLE="stRole";
	public static final String CHILD_STROLE_RESET_VALUE="";
	public static final String CHILD_STDOCUMENTLABEL="stDocumentLabel";
	public static final String CHILD_STDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_STCOMMENTOPEN="stCommentOpen";
	public static final String CHILD_STCOMMENTOPEN_RESET_VALUE="";
	public static final String CHILD_STCONTACTNAME="stContactName";
	public static final String CHILD_STCONTACTNAME_RESET_VALUE="";
	public static final String CHILD_STPHONE="stPhone";
	public static final String CHILD_STPHONE_RESET_VALUE="";
	public static final String CHILD_STFAX="stFax";
	public static final String CHILD_STFAX_RESET_VALUE="";
	public static final String CHILD_STEMAIL="stEmail";
	public static final String CHILD_STEMAIL_RESET_VALUE="";
	public static final String CHILD_STADDRLINE1="stAddrLine1";
	public static final String CHILD_STADDRLINE1_RESET_VALUE="";
	public static final String CHILD_STBUSINESSIDPREFIX="stBusinessIdPrefix";
	public static final String CHILD_STBUSINESSIDPREFIX_RESET_VALUE="";
	public static final String CHILD_STBUSINESSID="stBusinessId";
	public static final String CHILD_STBUSINESSID_RESET_VALUE="";
	public static final String CHILD_STBUSINESSIDSUFFIX="stBusinessIdSuffix";
	public static final String CHILD_STBUSINESSIDSUFFIX_RESET_VALUE="";
	public static final String CHILD_STPARTYTYPEPREFIX="stPartyTypePrefix";
	public static final String CHILD_STPARTYTYPEPREFIX_RESET_VALUE="";
	public static final String CHILD_STPARTYTYPE="stPartyType";
	public static final String CHILD_STPARTYTYPE_RESET_VALUE="";
	public static final String CHILD_STPARTYTYPESUFFIX="stPartyTypeSuffix";
	public static final String CHILD_STPARTYTYPESUFFIX_RESET_VALUE="";
	public static final String CHILD_STCONTACTCOMPANYNAMEPREFIX="stContactCompanyNamePrefix";
	public static final String CHILD_STCONTACTCOMPANYNAMEPREFIX_RESET_VALUE="";
	public static final String CHILD_STCONTACTCOMPANYNAME="stContactCompanyName";
	public static final String CHILD_STCONTACTCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STCONTACTCOMPANYNAMESUFFIX="stContactCompanyNameSuffix";
	public static final String CHILD_STCONTACTCOMPANYNAMESUFFIX_RESET_VALUE="";
	public static final String CHILD_STADDRLINE2="stAddrLine2";
	public static final String CHILD_STADDRLINE2_RESET_VALUE="";
	public static final String CHILD_STREQUESTDATE="stRequestDate";
	public static final String CHILD_STREQUESTDATE_RESET_VALUE="";
	public static final String CHILD_STDOCSTATUSDATE="stDocStatusDate";
	public static final String CHILD_STDOCSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STDOCSTATUS="stDocStatus";
	public static final String CHILD_STDOCSTATUS_RESET_VALUE="";

  //--Release2.1--TD_DTS_CR//
  //// Condition Text Field should be editable under certain circumstances.
	public static final String CHILD_TXENGDOCTEXT="txEngDocText";
	public static final String CHILD_TXENGDOCTEXT_RESET_VALUE="";
	public static final String CHILD_TXFRENCHDOCTEXT="txFrenchDocText";
	public static final String CHILD_TXFRENCHDOCTEXT_RESET_VALUE="";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
	public static final String CHILD_HDDOCTRACKID="hdDocTrackId";
	public static final String CHILD_HDDOCTRACKID_RESET_VALUE="";
  //--Release2.1--TD_DTS_CR--end//

	public static final String CHILD_STCOMMENTCLOSE="stCommentClose";
	public static final String CHILD_STCOMMENTCLOSE_RESET_VALUE="";
	public static final String CHILD_HDEMPTYLINKMAIN="hdEmptyLinkMain";
	public static final String CHILD_HDEMPTYLINKMAIN_RESET_VALUE="";
	public static final String CHILD_REPEATED1="Repeated1";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
	public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";
  //// New hidden button implementing the 'fake' one from the ActiveMessage class.
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";

  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealNotesInfoModel doDealNotesInfo=null;
	protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDocTrackingDetails.jsp";


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	private DocTrackDetailHandler handler=new DocTrackDetailHandler();

}

