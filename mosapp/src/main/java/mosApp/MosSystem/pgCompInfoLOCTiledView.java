package mosApp.MosSystem;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.HiddenField;

/**
 * <p>Title: pgCompInfoLOCTiledView</p>
 * <p>Description: TiledViewBean for ComponentInfo Section </p>
 * 
 *
 * @author MCM Team
 * @version 1.0 Jul 17, 2008 Initial Version � XS 2.8
 * @version 1.1 July 29, 2008 modified for XS 2.60: add ComponentId and CopyId as hidden field
 */
public class pgCompInfoLOCTiledView extends pgDealSummaryComponentLocTiledView {

    public pgCompInfoLOCTiledView (View parent, String name) {
        super(parent, name);
    }

    /**
     * <p>createChild</p>
     * <p>Description: createChild method for Jato Framework</p>
     * 
     * @param name:String - child name
     * @return View 
     * 
     * @version 1.1 add ComponentId and CopyId as hidden field 
     */
    protected View createChild(String name) {
        
        try {
            return super.createChild(name);
        } catch (IllegalArgumentException iae) {
            if (name.equals(CHILD_CHALLOCATEMIPREM)){
                CheckBox child 
                = new CheckBox(this,
                        getDoComponentLocModel(),
                        CHILD_CHALLOCATEMIPREM,
                        doComponentLocModel.FIELD_DFMIALLOCAGTEFLAG,
                        "Y", "N", false,  null);
            return child;
            } else if (name.equals(CHILD_CHALLOCATETAXESCROW)) {
                CheckBox child 
                    = new CheckBox(this,
                        getDoComponentLocModel(),
                        CHILD_CHALLOCATETAXESCROW,
                        doComponentLocModel.FIELD_DFPROPERTYTAXALLOCATEFLAG,
                        "Y", "N", false,  null);
                return child;
            // MCM team for XS 2.60 STARTS
            } else if (name.equals(CHILD_HDCOMPONENTID)) {
                HiddenField child = new HiddenField(this,
                        getDoComponentLocModel(),
                        CHILD_HDCOMPONENTID,
                        doComponentLocModel.FIELD_DFCOMPONENTID,
                        CHILD_HDCOMPONENTID_RESET_VALUE,
                        null);
                return child;
            } else if (name.equals(CHILD_HDCOPYID)) {
                HiddenField child = new HiddenField(this,
                        getDoComponentLocModel(),
                        CHILD_HDCOPYID,
                        doComponentLocModel.FIELD_DFCOPYID,
                        CHILD_HDCOPYID_RESET_VALUE,
                        null);
                return child;
                // MCM team for XS 2.60 ENDS
            } else {
                throw new IllegalArgumentException("Invalid child name [" + name + "]");
            }
        }
    }
        
    /**
     * <p>
     * registerChildren
     * </p>
     * <p>
     * Description: registerChildren method for Jato Framework
     * </p>
     * 
     * @param
     * @return
     * 
     * @version 1.1 add ComponentId and CopyId as hidden field     
     */
    protected void registerChildren() {
        
        super.registerChildren();
        registerChild(CHILD_CHALLOCATEMIPREM, CheckBox.class);
        registerChild(CHILD_CHALLOCATETAXESCROW, CheckBox.class);
        // MCM XS 2.60 STARTS
        registerChild(CHILD_HDCOMPONENTID, HiddenField.class);
        registerChild(CHILD_HDCOPYID, HiddenField.class);
        // MCM XS 2.60 ENDS
    }

    
    /**
     * <p>resetChildren</p>
     * <p>Description: resetChildren method for Jato Framework</p>
     * 
     * @param 
     * @return
     * 
     * @version 1.1 add ComponentId and CopyId as hidden field     
     */
    public void resetChildren() {

        super.resetChildren();
        getChAllocateMIPremium().setValue(CHILD_CHALLOCATEMIPREM_RESET_VALUE);
        getChAllocatePropTas().setValue(CHILD_CHALLOCATETAXESCROW_RESET_VALUE);
        // MCM XS 2.60 STARTS
        getHdComponentId().setValue(CHILD_HDCOMPONENTID_RESET_VALUE);
        getHdCopyId().setValue(CHILD_HDCOPYID_RESET_VALUE);
        // MCM XS 2.60 ENDS
    }
    
    /**
     * <p>nextTile</p>
     * <p>Description: display next mortgage section tiled view </p>
     */
    public boolean nextTile() throws ModelControlException {

        boolean movedToRow = super.nextTile(true);
        if (movedToRow) {
            ComponentInfoHandler handler = (ComponentInfoHandler) this.handler
            .cloneSS();
            handler.pageGetState(this.getParentViewBean());
            handler.setLOCCompDisplayFields(getTileIndex());
            handler.pageSaveState();
        }
        return movedToRow;
    }

    public CheckBox getChAllocateMIPremium() {
        return (CheckBox) getChild(CHILD_CHALLOCATEMIPREM);
    }

    public CheckBox getChAllocatePropTas() {
        return (CheckBox) getChild(CHILD_CHALLOCATETAXESCROW);
    }
    
    // MCM TEAM XS 2.60 STARTS
    public HiddenField getHdComponentId() {
        return (HiddenField) getChild(CHILD_HDCOMPONENTID);
    }

    public HiddenField getHdCopyId() {
        return (HiddenField) getChild(CHILD_HDCOPYID);
    }
    // MCM TEAM XS 2.60 ENDS

    /**
     * <p>beginDisplay</p>
     * <p>Description: beginDisplay method for Jato Framework</p>
     * 
     * @param event: DisplayEvent
     * @return
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {

        super.beginDisplay(event);
    }

    
    public static final String CHILD_CHALLOCATEMIPREM = "chAllocateMIPremium";
    public static final String CHILD_CHALLOCATEMIPREM_RESET_VALUE = "";
    public static final String CHILD_CHALLOCATETAXESCROW = "chAllocateTaxEscrow";
    public static final String CHILD_CHALLOCATETAXESCROW_RESET_VALUE = "";

    // MCM XS 2.60 STARTS
    public static final String CHILD_HDCOMPONENTID="hdComponentId";
    public static final String CHILD_HDCOMPONENTID_RESET_VALUE="";
    public static final String CHILD_HDCOPYID="hdCopyId";
    public static final String CHILD_HDCOPYID_RESET_VALUE="";
    // MCM XS 2.60 ENDS

    /**
     * Handler class used by this class to display information.
     */
    private ComponentInfoHandler handler = new ComponentInfoHandler();


}
