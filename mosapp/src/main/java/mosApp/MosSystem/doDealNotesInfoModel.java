package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doDealNotesInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDealNotesDate();


	/**
	 *
	 *
	 */
	public void setDfDealNotesDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfDealNotesCategory();


	/**
	 *
	 *
	 */
	public void setDfDealNotesCategory(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesID();


	/**
	 *
	 *
	 */
	public void setDfDealNotesID(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
  //--DJ_CR134--start--27May2004--//
  public java.math.BigDecimal getDfDealNotesCategoryID();


	/**
	 *
	 *
	 */
	public void setDfDealNotesCategoryID(java.math.BigDecimal value);
  //--DJ_CR134--end--//

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealID();


	/**
	 *
	 *
	 */
	public void setDfDealID(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesUserID();


	/**
	 *
	 *
	 */
	public void setDfDealNotesUserID(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfDealNotesText();


	/**
	 *
	 *
	 */
	public void setDfDealNotesText(String value);


	/**
	 *
	 *
	 */
	public String getDfUserName();


	/**
	 *
	 *
	 */
	public void setDfUserName(String value);

    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALNOTESDATE="dfDealNotesDate";
	public static final String FIELD_DFDEALNOTESCATEGORY="dfDealNotesCategory";
	public static final String FIELD_DFDEALNOTESID="dfDealNotesID";
  //--DJ_CR134--start--27May2004--//
	public static final String FIELD_DFDEALNOTESCATEGORYID="dfDealNotesCategoryID";
  //--DJ_CR134--end--//
	public static final String FIELD_DFDEALID="dfDealID";
	public static final String FIELD_DFDEALNOTESUSERID="dfDealNotesUserID";
	public static final String FIELD_DFDEALNOTESTEXT="dfDealNotesText";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String FIELD_DFDEALNOTESTEXTVARCHAR="dfDealNotesTextVarchar";
	public static final String FIELD_DFDEALNOTESTEXTLENGTH="dfDealNotesTextLength";
	//================================================================
	public static final String FIELD_DFUSERNAME="dfUserName";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	public static final String FIELD_DFLANGUAGEPREFERENCEID="dfLanguagePreferenceID";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

