package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *  WARNING - BJH: Nov 22, 02 : Dropped usage - no longer needed
 *
 */
public interface doCountCreditBureauLiabModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityId();


	/**
	 *
	 *
	 */
	public void setDfLiabilityId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId();


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityAmount();


	/**
	 *
	 *
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment();


	/**
	 *
	 *
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentOutGDS();


	/**
	 *
	 *
	 */
	public void setDfPercentOutGDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFLIABILITYID="dfLiabilityId";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFLIABILITYAMOUNT="dfLiabilityAmount";
	public static final String FIELD_DFLIABILITYMONTHLYPAYMENT="dfLiabilityMonthlyPayment";
	public static final String FIELD_DFPERCENTOUTGDS="dfPercentOutGDS";
	public static final String FIELD_DFCOPYID="dfCopyId";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

