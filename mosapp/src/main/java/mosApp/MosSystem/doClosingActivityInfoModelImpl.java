package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class doClosingActivityInfoModelImpl extends QueryModelBase
	implements doClosingActivityInfoModel
{
	/**
	 *
	 *
	 */
	public doClosingActivityInfoModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	    //--Release2.1--//
	    //logger = SysLog.getSysLogger("DOCAM");
	    //logger.debug("DOCAM@afterExecute::SQLTemplate: " + this.getSelectSQL());
	
	    //To override all the Description fields by populating from BXResource
	    //--> By John 20Dec2002
	    //Get the Language ID from the SessionStateModel
	    String defaultInstanceStateName =
				getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	    SessionStateModelImpl theSessionState =
	        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
	                        SessionStateModel.class,
	                        defaultInstanceStateName,
	                        true);
	    int languageId = theSessionState.getLanguageId();
		int institutionId = theSessionState.getDealInstitutionId();
	    
	    String abv = getDfProvinceAbbreviation();
	    if (abv==null) {
	    	setDfProvinceAbbreviation("");
	    }
	    else {
	    	setDfProvinceAbbreviation(BXResources.getPickListDescription(
	    			institutionId, "PROVINCESHORTNAME", getDfProvinceAbbreviation(), languageId));
	    }
	    
	    String phone = getDfPhoneNumber();
	    if (phone==null) {
	    	setDfPhoneNumber("");
	    }
	    else {
	    	setDfPhoneNumber("("+phone.substring(0,3)+")"+ phone.substring(3,6)+"-" + phone.substring(6));
	    }
	    
	    String fax = getDfFaxNumber();
	    if (fax==null) {
	    	setDfFaxNumber("");
	    }
	    else {
	    	setDfFaxNumber("(" + fax.substring(0,3) + ")" + fax.substring(3,6) + "-" + fax.substring(6));
	    }

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProvinceAbbreviation()
	{
		return (String)getValue(FIELD_DFPROVINCEABBREVIATION);
	}


	/**
	 *
	 *
	 */
	public void setDfProvinceAbbreviation(String value)
	{
		setValue(FIELD_DFPROVINCEABBREVIATION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalFSALDU()
	{
		return (String)getValue(FIELD_DFPOSTALFSALDU);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalFSALDU(String value)
	{
		setValue(FIELD_DFPOSTALFSALDU,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine1()
	{
		return (String)getValue(FIELD_DFADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine1(String value)
	{
		setValue(FIELD_DFADDRESSLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine2()
	{
		return (String)getValue(FIELD_DFADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine2(String value)
	{
		setValue(FIELD_DFADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPhoneNumber()
	{
		return (String)getValue(FIELD_DFPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfPhoneNumber(String value)
	{
		setValue(FIELD_DFPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFaxNumber()
	{
		return (String)getValue(FIELD_DFFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfFaxNumber(String value)
	{
		setValue(FIELD_DFFAXNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmailAddress()
	{
		return (String)getValue(FIELD_DFEMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfEmailAddress(String value)
	{
		setValue(FIELD_DFEMAILADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfContactId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONTACTID);
	}


	/**
	 *
	 *
	 */
	public void setDfContactId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONTACTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAddrId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFADDRID);
	}


	/**
	 *
	 *
	 */
	public void setDfAddrId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFADDRID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPhoneNumberExtension()
	{
		return (String)getValue(FIELD_DFPHONENUMBEREXTENSION);
	}


	/**
	 *
	 *
	 */
	public void setDfPhoneNumberExtension(String value)
	{
		setValue(FIELD_DFPHONENUMBEREXTENSION,value);
	}


	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doClosingActivityInfoModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL ADDR.CITY, PROVINCE.PROVINCEABBREVIATION, ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU POSTCODE, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTEMAILADDRESS, CONTACT.CONTACTID, ADDR.ADDRID, CONTACT.CONTACTPHONENUMBEREXTENSION FROM CONTACT, ADDR, PROVINCE  __WHERE__  ";

  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL ADDR.CITY, PROVINCE.PROVINCEABBREVIATION, ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU SYNTHETICPOSTCODE, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTEMAILADDRESS, CONTACT.CONTACTID, ADDR.ADDRID, CONTACT.CONTACTPHONENUMBEREXTENSION FROM CONTACT, ADDR, PROVINCE  __WHERE__  ";
	//public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, ADDR, PROVINCE";
	//public static final String STATIC_WHERE_CRITERIA=" (CONTACT.COPYID  =  ADDR.COPYID) AND (CONTACT.ADDRID  =  ADDR.ADDRID) AND (ADDR.PROVINCEID  =  PROVINCE.PROVINCEID)";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL ADDR.CITY, to_char(ADDR.PROVINCEID) PROVINCEID_STR, " +
    "ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU SYNTHETICPOSTCODE, ADDR.ADDRESSLINE1, " +
    "ADDR.ADDRESSLINE2, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, " +
    "CONTACT.CONTACTEMAILADDRESS, CONTACT.CONTACTID, ADDR.ADDRID, " +
    "CONTACT.CONTACTPHONENUMBEREXTENSION FROM CONTACT, ADDR __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, ADDR";
	public static final String STATIC_WHERE_CRITERIA=
	    " (CONTACT.INSTITUTIONPROFILEID  =  ADDR.INSTITUTIONPROFILEID) AND " +    
  " (CONTACT.COPYID  =  ADDR.COPYID) AND " +
  "(CONTACT.ADDRID  =  ADDR.ADDRID)";
  //--------------//

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFCITY="ADDR.CITY";
	public static final String COLUMN_DFCITY="CITY";

  //--Release2.1--//
  //public static final String QUALIFIED_COLUMN_DFPROVINCEABBREVIATION="PROVINCE.PROVINCEABBREVIATION";
	//public static final String COLUMN_DFPROVINCEABBREVIATION="PROVINCEABBREVIATION";
  public static final String QUALIFIED_COLUMN_DFPROVINCEABBREVIATION="PROVINCE.PROVINCEID_STR";
	public static final String COLUMN_DFPROVINCEABBREVIATION="PROVINCEID_STR";
	//--------------//

  ////public static final String QUALIFIED_COLUMN_DFPOSTALFSALDU=".";
	////public static final String COLUMN_DFPOSTALFSALDU="";
	public static final String QUALIFIED_COLUMN_DFPOSTALFSALDU="ADDR.SYNTHETICPOSTCODE";
	public static final String COLUMN_DFPOSTALFSALDU="SYNTHETICPOSTCODE";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFADDRESSLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFADDRESSLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFPHONENUMBER="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFPHONENUMBER="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFFAXNUMBER="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFFAXNUMBER="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFEMAILADDRESS="CONTACT.CONTACTEMAILADDRESS";
	public static final String COLUMN_DFEMAILADDRESS="CONTACTEMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFCONTACTID="CONTACT.CONTACTID";
	public static final String COLUMN_DFCONTACTID="CONTACTID";
	public static final String QUALIFIED_COLUMN_DFADDRID="ADDR.ADDRID";
	public static final String COLUMN_DFADDRID="ADDRID";
	public static final String QUALIFIED_COLUMN_DFPHONENUMBEREXTENSION="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFPHONENUMBEREXTENSION="CONTACTPHONENUMBEREXTENSION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCEABBREVIATION,
				COLUMN_DFPROVINCEABBREVIATION,
				QUALIFIED_COLUMN_DFPROVINCEABBREVIATION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//// Improper converted by iMT tool FieldDescriptor for the ComputedColumn
    ////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFPOSTALFSALDU,
		////		COLUMN_DFPOSTALFSALDU,
		////		QUALIFIED_COLUMN_DFPOSTALFSALDU,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALFSALDU,
				COLUMN_DFPOSTALFSALDU,
				QUALIFIED_COLUMN_DFPOSTALFSALDU,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"ADDR.POSTALFSA || ' ' || ADDR.POSTALLDU"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE1,
				COLUMN_DFADDRESSLINE1,
				QUALIFIED_COLUMN_DFADDRESSLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE2,
				COLUMN_DFADDRESSLINE2,
				QUALIFIED_COLUMN_DFADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPHONENUMBER,
				COLUMN_DFPHONENUMBER,
				QUALIFIED_COLUMN_DFPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFAXNUMBER,
				COLUMN_DFFAXNUMBER,
				QUALIFIED_COLUMN_DFFAXNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMAILADDRESS,
				COLUMN_DFEMAILADDRESS,
				QUALIFIED_COLUMN_DFEMAILADDRESS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTID,
				COLUMN_DFCONTACTID,
				QUALIFIED_COLUMN_DFCONTACTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRID,
				COLUMN_DFADDRID,
				QUALIFIED_COLUMN_DFADDRID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPHONENUMBEREXTENSION,
				COLUMN_DFPHONENUMBEREXTENSION,
				QUALIFIED_COLUMN_DFPHONENUMBEREXTENSION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

