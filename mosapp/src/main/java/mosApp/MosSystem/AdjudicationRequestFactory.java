package mosApp.MosSystem;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.deal.security.ViewBeanFactory;

/**
 * <p>Title: AdjudicationRequestFactory.java </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: filogix</p>
 *
 * @author Vlad Ivanov
 * @version 1.0
 */
public class AdjudicationRequestFactory extends ViewBeanFactory {

  // TODO: move to Mc constant interface
  private static final String PATH = "mosApp.MosSystem";
  private static final String NBC = "NBC";
  private static final String pgAdjudicationRequestNBC = "pgAdjudicationRequestNBC";
  private static final String DJ = "DJ";
  private static final String pgAdjudicationRequestDJ = "pgAdjudicationRequestDJ";
  private static final String pgAdjudicationRequestBase = "pgAdjudicationRequest";

  private Deal deal;
  public SysLogger logger;

  public AdjudicationRequestFactory() {
    path = PATH;
  }

  public AdjudicationRequestFactory(Deal deal) {
    path = PATH;
    this.deal = deal;
  }

  /**
   * A factory method to determine which pgAdjudicationReviewXXViewBean to use.
   * @return ViewBean
   */
  public String getViewBeanByClassName()
  {
    // if deal not set, just return null;
    if( deal == null )
      return null;

    try
    {
      String lenderShortName = this.deal.getLenderProfile().getLPShortName();
      //logger.debug("ARF@getViewBeanClassName::LenderShortName: " + lenderShortName);

      //TODO: Create method to call short name convertion to the abbreviation
      if (NBC.equals(lenderShortName)) {
        return pgAdjudicationRequestNBC;
      } else if (DJ.equals(lenderShortName)) {
        return pgAdjudicationRequestDJ;
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    // no Adjudication Request Page identified for an instance
    // this gets returned but access rights should NEVER be defined for this page so the "Access is denied" dialog is displayed
    // therefore, do not define values for this view bean in USERTYPEPAGEACCESS table or DEALSTATUSPAGEACCESS table

    return pgAdjudicationRequestBase;
  }

}
