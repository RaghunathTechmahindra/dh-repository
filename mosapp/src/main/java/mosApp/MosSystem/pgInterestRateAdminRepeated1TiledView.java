package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.WebActions;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;

/**
 *
 *
 *
 */
public class pgInterestRateAdminRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgInterestRateAdminRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doRowGeneratorModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getBtUpdate().setValue(CHILD_BTUPDATE_RESET_VALUE);
		getBtHistory().setValue(CHILD_BTHISTORY_RESET_VALUE);
		getStCode().setValue(CHILD_STCODE_RESET_VALUE);
		getStStatus().setValue(CHILD_STSTATUS_RESET_VALUE);
		getStDescription().setValue(CHILD_STDESCRIPTION_RESET_VALUE);
		getStEffectiveDate().setValue(CHILD_STEFFECTIVEDATE_RESET_VALUE);
		getStMaxDiscountRate().setValue(CHILD_STMAXDISCOUNTRATE_RESET_VALUE);
		getStBestRate().setValue(CHILD_STBESTRATE_RESET_VALUE);
		getStPostedRate().setValue(CHILD_STPOSTEDRATE_RESET_VALUE);
		getHdRowNdx().setValue(CHILD_HDROWNDX_RESET_VALUE);

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    getStTeaserRate().setValue(CHILD_STTEASERRATE_RESET_VALUE);
    getStBaseIndex().setValue(CHILD_STBASEINDEX_RESET_VALUE);
    getStBaseRate().setValue(CHILD_STBASERATE_RESET_VALUE);
    getStBaseAdjustment().setValue(CHILD_STBASEADJUSTMENT_RESET_VALUE);
    getStTeaserDiscount().setValue(CHILD_STTEASERDISCOUNT_RESET_VALUE);
    getStTeaserTerms().setValue(CHILD_STTEASERTERMS_RESET_VALUE);
    getStIncludePrimeIndxSectionStart().setValue(CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE);
    getStIncludePrimeIndxSectionEnd().setValue(CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE);
    getStPrimeRateEffDate().setValue(CHILD_STPRIMERATEEFFDATE_RESET_VALUE);
    getStPrimeRateStatus().setValue(CHILD_STPRIMERATESTATUS_RESET_VALUE);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_BTUPDATE,Button.class);
		registerChild(CHILD_BTHISTORY,Button.class);
		registerChild(CHILD_STCODE,StaticTextField.class);
		registerChild(CHILD_STSTATUS,StaticTextField.class);
		registerChild(CHILD_STDESCRIPTION,StaticTextField.class);
		registerChild(CHILD_STEFFECTIVEDATE,StaticTextField.class);
		registerChild(CHILD_STMAXDISCOUNTRATE,StaticTextField.class);
		registerChild(CHILD_STBESTRATE,StaticTextField.class);
		registerChild(CHILD_STPOSTEDRATE,StaticTextField.class);
		registerChild(CHILD_HDROWNDX,HiddenField.class);

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    registerChild(CHILD_STTEASERRATE,StaticTextField.class);
    registerChild(CHILD_STBASEINDEX,StaticTextField.class);
    registerChild(CHILD_STBASERATE,StaticTextField.class);
    registerChild(CHILD_STBASEADJUSTMENT,StaticTextField.class);
    registerChild(CHILD_STTEASERDISCOUNT,StaticTextField.class);
    registerChild(CHILD_STTEASERTERMS,StaticTextField.class);
    registerChild(CHILD_STINCLUDEPRIMEINDXSECTIONSTART,StaticTextField.class);
    registerChild(CHILD_STINCLUDEPRIMEINDXSECTIONEND,StaticTextField.class);
    registerChild(CHILD_STPRIMERATEEFFDATE,StaticTextField.class);
    registerChild(CHILD_STPRIMERATESTATUS,StaticTextField.class);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
	}

	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoRowGeneratorModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.checkAndSyncDOWithCursorInfo(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			boolean rt = handler.populateRepeatedFields(this.getTileIndex());
			handler.pageSaveState();
      return(rt);
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_BTUPDATE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTUPDATE,
				CHILD_BTUPDATE,
				CHILD_BTUPDATE_RESET_VALUE,
				null);
				return child;

      }
		else
		if (name.equals(CHILD_BTHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTHISTORY,
				CHILD_BTHISTORY,
				CHILD_BTHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STCODE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCODE,
				CHILD_STCODE,
				CHILD_STCODE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSTATUS,
				CHILD_STSTATUS,
				CHILD_STSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDESCRIPTION,
				CHILD_STDESCRIPTION,
				CHILD_STDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STEFFECTIVEDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STEFFECTIVEDATE,
				CHILD_STEFFECTIVEDATE,
				CHILD_STEFFECTIVEDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STMAXDISCOUNTRATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STMAXDISCOUNTRATE,
				CHILD_STMAXDISCOUNTRATE,
				CHILD_STMAXDISCOUNTRATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBESTRATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBESTRATE,
				CHILD_STBESTRATE,
				CHILD_STBESTRATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPOSTEDRATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPOSTEDRATE,
				CHILD_STPOSTEDRATE,
				CHILD_STPOSTEDRATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDROWNDX))
		{
			HiddenField child = new HiddenField(this,
				getdoRowGeneratorModel(),
				CHILD_HDROWNDX,
				doRowGeneratorModel.FIELD_DFROWNDX,
				CHILD_HDROWNDX_RESET_VALUE,
				null);
			return child;
		}
    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    else
    if (name.equals(CHILD_STTEASERRATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTEASERRATE,
        CHILD_STTEASERRATE,
        CHILD_STTEASERRATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STBASEINDEX))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STBASEINDEX,
        CHILD_STBASEINDEX,
        CHILD_STBASEINDEX_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STBASERATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STBASERATE,
        CHILD_STBASERATE,
        CHILD_STBASERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STBASEADJUSTMENT))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STBASEADJUSTMENT,
        CHILD_STBASEADJUSTMENT,
        CHILD_STBASEADJUSTMENT_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STTEASERDISCOUNT))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTEASERDISCOUNT,
        CHILD_STTEASERDISCOUNT,
        CHILD_STTEASERDISCOUNT_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STTEASERTERMS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STTEASERTERMS,
        CHILD_STTEASERTERMS,
        CHILD_STTEASERTERMS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STINCLUDEPRIMEINDXSECTIONSTART))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART,
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART,
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STINCLUDEPRIMEINDXSECTIONEND))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEPRIMEINDXSECTIONEND,
        CHILD_STINCLUDEPRIMEINDXSECTIONEND,
        CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPRIMERATEEFFDATE))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPRIMERATEEFFDATE,
        CHILD_STPRIMERATEEFFDATE,
        CHILD_STPRIMERATEEFFDATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STPRIMERATESTATUS))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STPRIMERATESTATUS,
        CHILD_STPRIMERATESTATUS,
        CHILD_STPRIMERATESTATUS_RESET_VALUE,
        null);
      return child;
    }
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void handleBtUpdateRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btUpdate_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.navigateToRateAdminSubpage(handler.getRowNdxFromWebEventMethod(event),"btUpdate");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtUpdate()
	{
		return (Button)getChild(CHILD_BTUPDATE);
	}

	/**
	 *
	 *
	 */
	public String endBtUpdateDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btUpdate_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.generateUpdateButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}

	/**
	 *
	 *
	 */
	public void handleBtHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btHistory_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.navigateToRateAdminSubpage(handler.getRowNdxFromWebEventMethod(event),"btHistory");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtHistory()
	{
		return (Button)getChild(CHILD_BTHISTORY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCode()
	{
		return (StaticTextField)getChild(CHILD_STCODE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStStatus()
	{
		return (StaticTextField)getChild(CHILD_STSTATUS);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStDescription()
	{
		return (StaticTextField)getChild(CHILD_STDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEffectiveDate()
	{
		return (StaticTextField)getChild(CHILD_STEFFECTIVEDATE);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStMaxDiscountRate()
	{
		return (StaticTextField)getChild(CHILD_STMAXDISCOUNTRATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBestRate()
	{
		return (StaticTextField)getChild(CHILD_STBESTRATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPostedRate()
	{
		return (StaticTextField)getChild(CHILD_STPOSTEDRATE);
	}


  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  /**
   *
   *
   */
  public StaticTextField getStTeaserRate()
  {
    return (StaticTextField)getChild(CHILD_STTEASERRATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStBaseIndex()
  {
    return (StaticTextField)getChild(CHILD_STBASEINDEX);
  }

  /**
   *
   *
   */
  public StaticTextField getStBaseRate()
  {
    return (StaticTextField)getChild(CHILD_STBASERATE);
  }

  /**
   *
   *
   */
  public StaticTextField getStBaseAdjustment()
  {
    return (StaticTextField)getChild(CHILD_STBASEADJUSTMENT);
  }

  /**
   *
   *
   */
  public StaticTextField getStTeaserDiscount()
  {
    return (StaticTextField)getChild(CHILD_STTEASERDISCOUNT);
  }

  /**
   *
   *
   */
  public StaticTextField getStTeaserTerms()
  {
    return (StaticTextField)getChild(CHILD_STTEASERTERMS);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludePrimeIndxSectionStart()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDEPRIMEINDXSECTIONSTART);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludePrimeIndxSectionEnd()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDEPRIMEINDXSECTIONEND);
  }

  /**
   *
   *
   */
  public StaticTextField getStPrimeRateEffDate()
  {
    return (StaticTextField)getChild(CHILD_STPRIMERATEEFFDATE);
  }
  /**
   *
   *
   */
  public StaticTextField getStPrimeRateStatus()
  {
    return (StaticTextField)getChild(CHILD_STPRIMERATESTATUS);
  }
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	/**
	 *
	 *
	 */
	public HiddenField getHdRowNdx()
	{
		return (HiddenField)getChild(CHILD_HDROWNDX);
	}


	/**
	 *
	 *
	 */
	public doRowGeneratorModel getdoRowGeneratorModel()
	{
		if (doRowGenerator == null)
			doRowGenerator = (doRowGeneratorModel) getModel(doRowGeneratorModel.class);
		return doRowGenerator;
	}


	/**
	 *
	 *
	 */
	public void setdoRowGeneratorModel(doRowGeneratorModel model)
	{
			doRowGenerator = model;
	}


  //--> New method to manually set the current Display Offset
  //--> Current in JATO there is no method to mamually set the display offset, that's why
  //--> we have to make our own method.
  //--> Note : This method may need to be implemented in all TiledViews with Forward, Backward buttons.
  //--> By Billy 22July2002
  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_REFRESH);
  }
  //=====================================================================================

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_BTUPDATE="btUpdate";
	public static final String CHILD_BTUPDATE_RESET_VALUE=" ";
	public static final String CHILD_BTHISTORY="btHistory";
	public static final String CHILD_BTHISTORY_RESET_VALUE=" ";
	public static final String CHILD_STCODE="stCode";
	public static final String CHILD_STCODE_RESET_VALUE="";
	public static final String CHILD_STSTATUS="stStatus";
	public static final String CHILD_STSTATUS_RESET_VALUE="";
	public static final String CHILD_STDESCRIPTION="stDescription";
	public static final String CHILD_STDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_STEFFECTIVEDATE="stEffectiveDate";
	public static final String CHILD_STEFFECTIVEDATE_RESET_VALUE="";
	public static final String CHILD_STMAXDISCOUNTRATE="stMaxDiscountRate";
	public static final String CHILD_STMAXDISCOUNTRATE_RESET_VALUE="";
	public static final String CHILD_STBESTRATE="stBestRate";
	public static final String CHILD_STBESTRATE_RESET_VALUE="";
	public static final String CHILD_STPOSTEDRATE="stPostedRate";
	public static final String CHILD_STPOSTEDRATE_RESET_VALUE="";
	public static final String CHILD_HDROWNDX="hdRowNdx";
	public static final String CHILD_HDROWNDX_RESET_VALUE="";

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  public static final String CHILD_STTEASERRATE="stTeaserRate";
  public static final String CHILD_STTEASERRATE_RESET_VALUE="";

  public static final String CHILD_STBASEINDEX="stBaseIndex";
  public static final String CHILD_STBASEINDEX_RESET_VALUE="";

  public static final String CHILD_STBASERATE="stBaseRate";
  public static final String CHILD_STBASERATE_RESET_VALUE="";

  public static final String CHILD_STBASEADJUSTMENT="stBaseAdjustment";
  public static final String CHILD_STBASEADJUSTMENT_RESET_VALUE="";

  public static final String CHILD_STTEASERDISCOUNT="stTeaserDiscount";
  public static final String CHILD_STTEASERDISCOUNT_RESET_VALUE="";

  public static final String CHILD_STTEASERTERMS="stTeaserTerms";
  public static final String CHILD_STTEASERTERMS_RESET_VALUE="";

  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONSTART="stIncludePrimeIndxSectionStart";
  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE="";

  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONEND="stIncludePrimeIndxSectionEnd";
  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE="";

  public static final String CHILD_STPRIMERATEEFFDATE="stPrimeRateEffDate";
  public static final String CHILD_STPRIMERATEEFFDATE_RESET_VALUE="";

  public static final String CHILD_STPRIMERATESTATUS="stPrimeRateStatus";
  public static final String CHILD_STPRIMERATESTATUS_RESET_VALUE="";

  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doRowGeneratorModel doRowGenerator=null;
  private RateAdminHandler handler=new RateAdminHandler();

}

