package mosApp.MosSystem;

import java.util.Map;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.TextField;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import java.io.IOException;
import javax.servlet.ServletException;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.Model;
/**
 * <p>
 * Title: pgDisclosureABViewBean
 * <p>
 * Description: View bean for Disclosure AB PAGE
 * <p>
 * Company: Filogix Inc. Date: 11/28/2006
 * @author NBC Implementation Team
 * @version 1.0
 */

public class pgDisclosureABViewBean extends pgDisclosureViewBean implements
	ViewBean
{

  /**
   * pgDisclosureABViewBean()- constructor Initializes the PAGE_NAME, and
   * inturn calls the registerChildren() and also set the default URL <br>
   * Date: 11/28/2006
   */
  public pgDisclosureABViewBean()
  {
	super(PAGE_NAME);
	setDefaultDisplayURL(DISPLAY_URL);
	setDefaultDisplayUrl(DISPLAY_URL);
	registerChildren();
  }

  // protected void initialize() {
  // }

  /**
   * createChild() This method covers creating form elements specific to AB
   * The method calls createChild() of the parent<code>pgDisclosureViewBean</code>
   * to load the base elements common to the page
   * @param -
   *            name The name of the child form element to create
   * @return - View The childname is validated with the parent if the
   *         childFromParent is not null then childFromParent is returned else
   *         name from other values <br>
   * @throws IllegalArgumentException
   *             <br>
   *             Date: 11/28/2006
   */
  protected View createChild(String name)// throws IllegalArgumentException
  {
	View childFromParent = super.createChild(name);

	if (childFromParent != null)
	{
	  return childFromParent;
	}
	else
	  if (name.equals(CHILD_TXBONUSDISCOUNT))
	  {
		TextField child = new TextField(this, getDefaultModel(),
			CHILD_TXBONUSDISCOUNT, CHILD_TXBONUSDISCOUNT,
			CHILD_TXBONUSDISCOUNT_RESET_VALUE, null);
		return child;
	  }
	  else
		if (name.equals(CHILD_TXTERMMORT))
		{
		  TextField child = new TextField(this, getDefaultModel(),
			  CHILD_TXTERMMORT, CHILD_TXTERMMORT, CHILD_TXTERMMORT_RESET_VALUE,
			  null);
		  return child;
		}
		else
		  if (name.equals(CHILD_TXANNPERCRATE))
		  {
			TextField child = new TextField(this, getDefaultModel(),
				CHILD_TXANNPERCRATE, CHILD_TXANNPERCRATE,
				CHILD_TXANNPERCRATE_RESET_VALUE, null);
			return child;
		  }
		  else
			if (name.equals(CHILD_TXTERMSCONDS_AB))
			{
			  TextField child = new TextField(this, getDefaultModel(),
				  CHILD_TXTERMSCONDS_AB, CHILD_TXTERMSCONDS_AB,
				  CHILD_TXTERMSCONDS_RESET_VALUE_AB, null);
			  return child;
			}

			else
			  throw new IllegalArgumentException("Invalid child name [" + name
				  + "]");
  }

  /**
   * resetChildren() This method covers reseting form elements specific to AB
   * The method calls resetChildren() of the parent<code>pgDisclosureViewBean</code>
   * to reset the base elements common to the page<br>
   * Date: 11/28/2006
   */

  public void resetChildren()
  {
	super.resetChildren();
	getTxBonusDiscount().setValue(CHILD_TXBONUSDISCOUNT_RESET_VALUE);
	getTxMortgageTerm().setValue(CHILD_TXTERMMORT_RESET_VALUE);
	getTxAnnualPercent().setValue(CHILD_TXANNPERCRATE_RESET_VALUE);
	getTxTermsCondsAB().setValue(CHILD_TXTERMSCONDS_RESET_VALUE_AB);

  }

  /**
   * registerChildren() This method covers regestering form elements specific
   * to AB The method calls registerChildren()of the parent<code>pgDisclosureViewBean</code>
   * to register the base elements common to the page<br>
   * Date: 11/28/2006
   */
  protected void registerChildren()
  {
	super.registerChildren();
	registerChild(CHILD_TXBONUSDISCOUNT, TextField.class);
	registerChild(CHILD_TXTERMMORT, TextField.class);
	registerChild(CHILD_TXANNPERCRATE, TextField.class);
	registerChild(CHILD_TXTERMSCONDS_AB, TextField.class);
  }

  /**
   * beginDisplay() This method renders the jsp
   * @param -
   *            DisplayEvent<br>
   *            Date: 11/28/2006
   */

  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {

	// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
	// Ensure the primary model is non-null; if null, it would cause havoc
	// with the various methods dependent on the primary model
	if (getdoDisclosureSelectModel() == null)
	  throw new ModelControlException("Primary model is null");

	DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
	handler.pageGetState(this.getParentViewBean());

	handler.overridePageLabel(62);

	super.beginDisplay(event);
  }

  /**
   * getTxBonusDiscount <br>
   * Returns an reference of the "BonusDiscount" child element <br>
   * Creation Date: 11/28/2006
   * @return reference of the "BonusDiscount" text field
   */
  public TextField getTxBonusDiscount()
  {
	return (TextField) getChild(CHILD_TXBONUSDISCOUNT);
  }

  /**
   * getTxBonusDiscount <br>
   * Returns an reference of the "Term Mortgage" child element <br>
   * Creation Date: 11/28/2006
   * @return reference of the "Term Mortgage" text field
   */
  public TextField getTxMortgageTerm()
  {
	return (TextField) getChild(CHILD_TXTERMMORT);
  }

  /**
   * getTxBonusDiscount <br>
   * Returns an reference of the "Annual Percentgage Rate" child element <br>
   * Creation Date: 11/28/2006
   * @return reference of the "Annual Percentgage Rate" text field
   */
  public TextField getTxAnnualPercent()
  {
	return (TextField) getChild(CHILD_TXANNPERCRATE);
  }

  /**
   * getTxBonusDiscount <br>
   * Returns an reference of the "Terms conditions" child element <br>
   * Creation Date: 11/28/2006
   * @return reference of the "Terms conditions" text field
   */
  public TextField getTxTermsCondsAB()
  {
	return (TextField) getChild(CHILD_TXTERMSCONDS_AB);
  }

  
  /**
   * beforeModelExecutes <br>
   * This method executes any application logic required before rendering the jsp <br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that is about to be auto-executed
   * @Param executionContext
   *            The context under which this model is being executed
   * @return True if the execution of the model should proceed, false to skip
   *         execution of the model
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.pageGetState(this);
	handler.setupBeforePageGeneration();
	handler.pageSaveState();
	return super.beforeModelExecutes(model, executionContext);
  }

  /**
   * afterModelExecutes <br>
   * This method executes any application logic required after rendering the
   * jsp <br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that is about to be auto-executed
   * @Param executionContext
   *            The context under which this model is being executed
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
	super.afterModelExecutes(model, executionContext);
  }

  /**
   * afterModelExecutes <br>
   * This method executes after all models have finished execution <br>
   * Creation Date: 11/28/2006
   * @Param executionContext
   *            The context under which this model is being executed
   */
  public void afterAllModelsExecute(int executionContext)
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.pageGetState(this);
	handler.populatePageDisplayFields();
	handler.pageSaveState();
	super.afterAllModelsExecute(executionContext);
  }

  /**
   * onModelError <br>
   * Invoked as notification that a given model threw a <code>
   * ModelControlException</code>
   * during auto-execution.<br>
   * Creation Date: 11/28/2006
   * @Param model
   *            The model that which caused an execution error
   * @Param executionContext
   *            The context under which this model is being executed
   * @param exception
   *            The exception thrown during auto-execution
   */
  public void onModelError(Model model, int executionContext,
	  ModelControlException exception) throws ModelControlException
  {
	super.onModelError(model, executionContext, exception);
  }

  /**
   * handleBtProceedRequest <br>
   * This method handles the go button click <br>
   * Creation Date: 11/28/2006
   * @Param event
   *            The RequestInvocationEvent.
   */
  public void handleBtProceedRequest(RequestInvocationEvent event)
	  throws ServletException, IOException
  {
	DisclosureHandler handler = this.handler.cloneSS();
	handler.preHandlerProtocol(this);
	handler.handleGoPage();
	handler.postHandlerProtocol();
  }

  /**
   * getDisplayURL <br>
   * This method overrides the getDefaultURL() framework JATO method and it is
   * located in each ViewBean. This allows not to stay with the JATO ViewBean
   * extension, otherwise each ViewBean a.k.a page should extend its Handler
   * (to be called by the framework) and it is not good. DEFAULT_DISPLAY_URL
   * String is a variable now. The full method is still in PHC base class. It
   * should care the the url="/" case (non-initialized defaultURL) by the BX
   * framework methods. <br>
   * Creation Date: 11/28/2006
   * @return Url for the jsp which is displayed
   */
  public String getDisplayURL()
  {
	DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
	handler.pageGetState(this);
	String url = getDefaultDisplayURL();
	int languageId = handler.theSessionState.getLanguageId();
	if (url != null && !url.trim().equals("") && !url.trim().equals("/"))
	{
	  url = BXResources.getBXUrl(url, languageId);
	}
	else
	{
	  url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
	}
	return url;
  }

  
  
  // //////////////////////////////////////////////////////////////////////////////
  // Class variables
  // //////////////////////////////////////////////////////////////////////////////
  /**
   * Constant defining the page name
   * @value "pgDisclosureAB"
   */
  public static final String PAGE_NAME = "pgDisclosureAB";

  public static Map FIELD_DESCRIPTORS;

  /**
   * Constant defining the bonus discount text field
   * @value "101Q090V"
   */
  public static final String CHILD_TXBONUSDISCOUNT = "101Q090V";

  /**
   * Constant defining the reset value for bonus discount text field
   * @value ""
   */
  public static final String CHILD_TXBONUSDISCOUNT_RESET_VALUE = "";

  /**
   * Constant defining the bonus Term Mortgage text field
   * @value "101Q080U"
   */
  public static final String CHILD_TXTERMMORT = "101Q080U";

  /**
   * Constant defining the reset value for term mortgage text field
   * @value ""
   */
  public static final String CHILD_TXTERMMORT_RESET_VALUE = "";

  /**
   * Constant defining the actual percentage rate text field
   * @value "101Q10AC"
   */
  public static final String CHILD_TXANNPERCRATE = "101Q10AC";

  /**
   * Constant defining the reset value for actual percentage rate text field
   * @value ""
   */
  public static final String CHILD_TXANNPERCRATE_RESET_VALUE = "";

  /**
   * Constant defining the terms conditions text field
   * @value "101Q10AD"
   */
  public static final String CHILD_TXTERMSCONDS_AB = "101Q10AD";

  /**
   * Constant defining the reset value for terms conditions text field
   * @value ""
   */
  public static final String CHILD_TXTERMSCONDS_RESET_VALUE_AB = "";

  /**
   * variable defining display_url
   * @value "/mosApp/MosSystem/pgDisclosureAB.jsp"
   */
  private String DISPLAY_URL = "/mosApp/MosSystem/pgDisclosureAB.jsp";
}
