package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import MosSystem.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 * <p>Title: doAdjudicationResponseBNCDecisionMessagesModelImpl</p>
 * 
 * <p>Description: Implementation class for doAdjudicationResponseBNCDecisionMessagesModel</p>
 * 
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public class doAdjudicationResponseBNCDecisionMessagesModelImpl extends QueryModelBase
	implements doAdjudicationResponseBNCDecisionMessagesModel
{
	/**
	 *
	 *
	 */
	public doAdjudicationResponseBNCDecisionMessagesModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

//   logger = SysLog.getSysLogger("AdjudicationResponseBNCDecisionMessagesModel");
//   logger.debug("AdjudicationResponseBNCDecisionMessagesModel@afterExecute::Size: " + sql);

		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
//	    logger = SysLog.getSysLogger("AdjudicationResponseBNCDecisionMessagesModel");
//	    logger.debug("AdjudicationResponseBNCDecisionMessagesModel@afterExecute::Size: " + this.getSize());
//	    logger.debug("AdjudicationResponseBNCDecisionMessagesModel@afterExecute::SQLTemplate: " + this.getSelectSQL());
	
	    //--Release2.1--//
	    //To override all the Description fields by populating from BXResource
	    //Get the Language ID from the SessionStateModel
	    String defaultInstanceStateName =
				getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	    SessionStateModelImpl theSessionState =
	        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
	                        SessionStateModel.class,
	                        defaultInstanceStateName,
	                        true);
	    int languageId = theSessionState.getLanguageId();
		int institutionId = theSessionState.getDealInstitutionId();

	    String fullDecisionMessage = "";

	    while(this.next())
	    {

	//	     For every result returned, retrieve the messages, except for the 
	//	     recommended code.  Retrieve this only on the first round
	    	if (fullDecisionMessage.equals("")){
	          	this.setDfRecommendCodeDesc(BXResources.getPickListDescription
			    (institutionId, "RECOMMENDCODE", this.getDfRecommendCodeDesc(), languageId));
		
			    fullDecisionMessage=fullDecisionMessage.concat(this.getDfRecommendCodeDesc());
			    fullDecisionMessage=fullDecisionMessage.concat("\n");
			    
		
			}

	    	fullDecisionMessage=fullDecisionMessage.concat(this.getDfCodeAndReason());
		    fullDecisionMessage=fullDecisionMessage.concat("\n");
	    }

	      this.setDfDecisionMessages(fullDecisionMessage);

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponseId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFRESPONSEID);
	}

	/**
	 *
	 *
	 */
	public void setDfResponseId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFRESPONSEID,value);
	}
	
	/**
	 * 
	 *
	 */
	public String getDfDecisionMessages()
	{
		return (String)getValue(FIELD_DFDECISIONMESSAGES);
	}

	/**
	 *
	 *
	 */
	public void setDfDecisionMessages(String value)
	{
		setValue(FIELD_DFDECISIONMESSAGES,value);
	}
	
	/**
	 * 
	 *
	 */
	public java.math.BigDecimal getDfRecommendedCodeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFRECOMMENDEDCODEID);
	}

	/**
	 *
	 *
	 */
	public void setDfRecommendedCodeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFRECOMMENDEDCODEID,value);
	}


	/**
	 * 
	 *
	 */
	public String getDfRecommendCodeDesc()
	{
		return (String)getValue(FIELD_DFRECOMMENDCODEDESC);
	}

	/**
	 *
	 *
	 */
	public void setDfRecommendCodeDesc(String value)
	{
		setValue(FIELD_DFRECOMMENDCODEDESC,value);
	}

	/**
	 * 
	 *
	 */
	public String getDfCodeAndReason()
	{
		return (String)getValue(FIELD_DFCODEANDREASON);
	}

	/**
	 *
	 *
	 */
	public void setDfCodeAndReason(String value)
	{
		setValue(FIELD_DFCODEANDREASON,value);
	}





	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  public static final String SELECT_SQL_TEMPLATE= 
  "SELECT DISTINCT RESPONSE.RESPONSEID, to_char(BNCADJUDICATIONRESPONSE.RECOMMENDEDCODEID) DECISIONMESSAGES, " +
  "to_char(BNCADJUDICATIONRESPONSE.RECOMMENDEDCODEID) RECOMMENDEDCODEID_STR,  BNCADJUDICATIONRESPONSE.RECOMMENDEDCODEID, " +
  "BNCMANREVIEWREASON.MANREVIEWREASONCODE || ' ' || BNCMANREVIEWREASON.MANREVIEWREASONDESC CODEANDREASON " +
  "FROM RESPONSE, BNCADJUDICATIONRESPONSE, BNCMANREVIEWREASON " + 
  "__WHERE__ " +
  "AND BNCADJUDICATIONRESPONSE.RESPONSEID = BNCMANREVIEWREASON.RESPONSEID " + 
  " UNION ALL " +
  "SELECT DISTINCT RESPONSE.RESPONSEID, to_char(BNCADJUDICATIONRESPONSE.RECOMMENDEDCODEID) DECISIONMESSAGES, " +
  "to_char(BNCADJUDICATIONRESPONSE.RECOMMENDEDCODEID) RECOMMENDEDCODEID_STR,  BNCADJUDICATIONRESPONSE.RECOMMENDEDCODEID, " + 
  "BNCAUTODECLINEREASON.AUTODECLINEREASONCODE || ' ' || BNCAUTODECLINEREASON.AUTODECLINEREASONDESC CODEANDREASON " + 
  "FROM RESPONSE, BNCADJUDICATIONRESPONSE, BNCAUTODECLINEREASON " + 
  "__WHERE__ " +
  "AND BNCADJUDICATIONRESPONSE.RESPONSEID = BNCAUTODECLINEREASON.RESPONSEID ";

  public static final String MODIFYING_QUERY_TABLE_NAME=
    "RESPONSE, DEAL, BNCADJUDICATIONRESPONSE, BNCMANREVIEWREASON, BNCAUTODECLINEREASON";

  public static final String STATIC_WHERE_CRITERIA= "RESPONSE.RESPONSEID = BNCADJUDICATIONRESPONSE.RESPONSEID ";
  
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();

	public static final String QUALIFIED_COLUMN_DFRESPONSEID="RESPONSE.RESPONSEID";
	public static final String COLUMN_DFRESPONSEID="RESPONSEID";
	
	public static final String QUALIFIED_COLUMN_DFDECISIONMESSAGES = "BNCADJUDICATIONRESPONSE.DECISIONMESSAGES";
	public static final String COLUMN_DFDECISIONMESSAGES = "DECISIONMESSAGES";
	
	public static final String QUALIFIED_COLUMN_DFRECOMMENDEDCODEID = "BNCADJUDICATIONRESPONSE.RECOMMENDEDCODEID";
	public static final String COLUMN_DFRECOMMENDEDCODEID = "RECOMMENDEDCODEID";

	public static final String QUALIFIED_COLUMN_DFRECOMMENDCODEDESC = "BNCADJUDICATIONRESPONSE.RECOMMENDEDCODEID_STR";
	public static final String COLUMN_DFRECOMMENDCODEDESC = "RECOMMENDEDCODEID_STR";

	public static final String QUALIFIED_COLUMN_DFCODEANDREASON = "BNCADJUDICATIONRESPONSE.CODEANDREASON";
	public static final String COLUMN_DFCODEANDREASON = "CODEANDREASON";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFRESPONSEID,
					COLUMN_DFRESPONSEID,
					QUALIFIED_COLUMN_DFRESPONSEID,
					java.math.BigDecimal.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));

		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFDECISIONMESSAGES,
				COLUMN_DFDECISIONMESSAGES,
				QUALIFIED_COLUMN_DFDECISIONMESSAGES,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFRECOMMENDEDCODEID,
				COLUMN_DFRECOMMENDEDCODEID,
				QUALIFIED_COLUMN_DFRECOMMENDEDCODEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFRECOMMENDCODEDESC,
				COLUMN_DFRECOMMENDCODEDESC,
				QUALIFIED_COLUMN_DFRECOMMENDCODEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
			
			FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
				FIELD_DFCODEANDREASON,
				COLUMN_DFCODEANDREASON,
				QUALIFIED_COLUMN_DFCODEANDREASON,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));


	}

}

