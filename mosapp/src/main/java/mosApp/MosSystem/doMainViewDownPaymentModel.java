package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doMainViewDownPaymentModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDownPaymentAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfDownPaymentAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDownPaymentSource();

	
	/**
	 * 
	 * 
	 */
	public void setDfDownPaymentSource(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDownPaymentDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfDownPaymentDesc(String value);
	
	  public java.math.BigDecimal getDfInstitutionId();
	  public void setDfInstitutionId(java.math.BigDecimal id);
	  
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFDOWNPAYMENTAMOUNT="dfDownPaymentAmount";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFDOWNPAYMENTSOURCE="dfDownPaymentSource";
	public static final String FIELD_DFDOWNPAYMENTDESC="dfDownPaymentDesc";
	
	  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

