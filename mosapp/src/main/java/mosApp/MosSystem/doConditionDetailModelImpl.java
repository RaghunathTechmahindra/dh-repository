package mosApp.MosSystem;

/**
 * 11/Oct/2007 DVG #DG638 LEN217180: Malcolm Whitton Condition Text exceeds 4000 characters 
 */

import java.sql.Clob;
import java.sql.SQLException;

import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doConditionDetailModelImpl extends QueryModelBase
	implements doConditionDetailModel
{
	/**
	 *
	 *
	 */
	public doConditionDetailModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 25Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    while(this.next())
    {
      //Convert LANGUAGEPREFERENCE Desc.
      this.setDfLanguage(BXResources.getPickListDescription(
          institutionId, "LANGUAGEPREFERENCE", this.getDfLanguage(), languageId));
  		//#DG638 reset clob as string		
  		String condText;
		//================================================================
  		//CLOB Performance Enhancement June 2010
		Object docTextVarchar = getValue(FIELD_DFDOCUMENTTEXTLITE);
		if( docTextVarchar != null) {
			condText = (String)docTextVarchar;
		} else {
	  		try {
	  			Clob condTextObj = (Clob)getValue(FIELD_DFDOCUMENTTEXT);
	  			condText = TypeConverter.stringFromClob(condTextObj);
	  		}
	  		catch (Exception e) {
	  			SysLog.error(getClass(), StringUtil.stack2string(e), "ConDetIM");
	  			condText = null;
	  		}
		}
		//================================================================
  		setDfDocumentText(condText);
  		//#DG638 end		
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDocTrackId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void setDfDocTrackId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOCTRACKID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentText()
	{
		return (String)getValue(FIELD_DFDOCUMENTTEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentText(String value)
	{
		setValue(FIELD_DFDOCUMENTTEXT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel()
	{
		return (String)getValue(FIELD_DFDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value)
	{
		setValue(FIELD_DFDOCUMENTLABEL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLanguage()
	{
		return (String)getValue(FIELD_DFLANGUAGE);
	}


	/**
	 *
	 *
	 */
	public void setDfLanguage(String value)
	{
		setValue(FIELD_DFLANGUAGE,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 25Nov2002
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKINGVERBIAGE.COPYID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, LANGUAGEPREFERENCE.LPDESCRIPTION FROM DOCUMENTTRACKINGVERBIAGE, LANGUAGEPREFERENCE  __WHERE__  ORDER BY DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID  ASC";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, "+
	"DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE, "+
    "DOCUMENTTRACKINGVERBIAGE.COPYID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, "+
    "to_char(DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID) LANGUAGEPREFERENCEID_STR "+
    "FROM DOCUMENTTRACKINGVERBIAGE  "+
    "__WHERE__  "+
    "ORDER BY DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID  ASC";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="DOCUMENTTRACKINGVERBIAGE, LANGUAGEPREFERENCE";
  public static final String MODIFYING_QUERY_TABLE_NAME="DOCUMENTTRACKINGVERBIAGE";
  //--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
	//public static final String STATIC_WHERE_CRITERIA=" (LANGUAGEPREFERENCE.LANGUAGEPREFERENCEID  =  DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID) ";
	public static final String STATIC_WHERE_CRITERIA=
    "";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDOCTRACKID="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID";
	public static final String COLUMN_DFDOCTRACKID="DOCUMENTTRACKINGID";
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXT="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT";
	public static final String COLUMN_DFDOCUMENTTEXT="DOCUMENTTEXT";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXTLITE="DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXTLITE";
	public static final String COLUMN_DFDOCUMENTTEXTLITE="DOCUMENTTEXTLITE";
	//================================================================
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOCUMENTTRACKINGVERBIAGE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDOCUMENTLABEL="DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL";
	public static final String COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFLANGUAGE="LANGUAGEPREFERENCE.LPDESCRIPTION";
	//public static final String COLUMN_DFLANGUAGE="LPDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFLANGUAGE="DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID_STR";
	public static final String COLUMN_DFLANGUAGE="LANGUAGEPREFERENCEID_STR";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCTRACKID,
				COLUMN_DFDOCTRACKID,
				QUALIFIED_COLUMN_DFDOCTRACKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTTEXT,
				COLUMN_DFDOCUMENTTEXT,
				QUALIFIED_COLUMN_DFDOCUMENTTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		//================================================================
		//CLOB Performance Enhancement June 2010
		FIELD_SCHEMA.addFieldDescriptor(
				new QueryFieldDescriptor(
					FIELD_DFDOCUMENTTEXTLITE,
					COLUMN_DFDOCUMENTTEXTLITE,
					QUALIFIED_COLUMN_DFDOCUMENTTEXTLITE,
					String.class,
					false,
					false,
					QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
					"",
					QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
					""));
		//================================================================

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTLABEL,
				COLUMN_DFDOCUMENTLABEL,
				QUALIFIED_COLUMN_DFDOCUMENTLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGE,
				COLUMN_DFLANGUAGE,
				QUALIFIED_COLUMN_DFLANGUAGE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

