package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doDealGetDefaultProductModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfMtgProdId();

	
	/**
	 * 
	 * 
	 */
	public void setDfMtgProdId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDefaultProduct();

	
	/**
	 * 
	 * 
	 */
	public void setDfDefaultProduct(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLenderProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLenderProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfDefaultLender();

	
	/**
	 * 
	 * 
	 */
	public void setDfDefaultLender(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFMTGPRODID="dfMtgProdId";
	public static final String FIELD_DFDEFAULTPRODUCT="dfDefaultProduct";
	public static final String FIELD_DFLENDERPROFILEID="dfLenderProfileId";
	public static final String FIELD_DFDEFAULTLENDER="dfDefaultLender";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

