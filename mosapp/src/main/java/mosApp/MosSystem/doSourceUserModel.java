package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doSourceUserModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUserProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUsername();

	
	/**
	 * 
	 * 
	 */
	public void setDfUsername(String value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFUSERPROFILEID="dfUserProfileId";
	public static final String FIELD_DFUSERNAME="dfUsername";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

