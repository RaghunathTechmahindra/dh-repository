package mosApp.MosSystem;

import java.util.Calendar;
import java.util.Vector;



import com.basis100.deal.security.ACM;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.security.JumpState;
import com.basis100.deal.security.PassiveMessage;
import com.filogix.express.state.ExpressState;
import com.iplanet.jato.model.Model;

/**
 *
 *
 *
 */
public interface SessionStateModel extends Model
{
    /**
     * returns a the express state object.
     *
     * @since 3.3
     */
    public ExpressState getExpressState();

    /**
     * returns the user institution profile id,
     *
     * @since 3.3
     */
    public int getUserInstitutionId();

    /**
     * returns deal institution profile id.
     *
     * @since 3.3
     */
    public int getDealInstitutionId();


  //--Release2.1--//
    public java.util.Date getSessionDate();
    public void setSessionDate(java.util.Date sessionDateString);
  public String getSessionDateStr();
  //================================

    /**
     *
     *
     */
    public boolean getIsAlert();

    /**
     *
     *
     */
    public boolean getIsFatal();

    /**
     *
     *
     */
    public void setIsAlert(boolean isAlrt);

    /**
     *
     *
     */
    public void setIsFatal(boolean isFatl);

    /**
     *
     *
     */
    public boolean isInUse();

    /**
     *
     *
     */
    public void setInUse(boolean b);

    /**
     *
     *
     */
    public boolean isActMessageSet();

    /**
     *
     *
     */
    public boolean isPasMessageSet();


    /**
     *  This method is eventually implemented in the <<ViewBean>>::RequestHandlingViewBase::
     *  ViewBeanBase. JATO's MVC implementation takes care of the view/data integrity.
     *
     *  Access to an appropriate PageEntry entity is provided by the
     *  RequestManager.getRequestContext().getViewBeanManager().getViewBean(pgXXXXX.class) method.
     *
     *  The classical call from the BasisXpress project:
     *  PageEntry pg = getTheSession().getCurrentPage() or its variations:
     *  SessionState theSession = getTheSession();
     *  PageEntry pg = theSession.getCurrentPage(); etc.
     *  should be catched and transformed in app2JATO.xml extra translation tool using the
     *  call:
     *
     *  RequestManager().getRequestContext().getModelManager().getModel(YYYModelImpl.class).
     *
     *  SavedSessionObject/ProjectPage data integrity implemented in the BasisXpress project
     *  via SavedObjects class. The constructor of it takes care of it. The drawback of this
     *  implementation is the impossibility to provide multipshread access to the project. It
     *  leads to the performance/exensibility problems of the BasisXpress project running on ND
     *  platform (impossibility for several CP workers to run in a Separate Process).
     *
     *  The 'session part' of the SavedObjects class will be implemented in the set of SessionStateModel,
     *  SessionStateModelImpl, SessionServiceModel, SessionServiceModelImpl interfaces and
     *  classes. These classes should extend SessionModel class of JATO and the interfaces
     *  must implement JATO Model interface. Thus, this new BasisXpress SessionModel approach
     *  will work as an extension to a proxy to HttpSession via a JATO Model API. This proxy
     *  to the HttpSession provides the page/data(implemented in model) integrity via MVC.
     *
     *  PageEntry::PageStateHandler::MosWebHandlerCommon::PageHandlerCommon::DealHandlerCommon classes
     *  must be reorginazed appropriately. The view(PageEntry)/session parts will be included into
     *  the ViewBean/Session...Model classes. This transformation will be described in the
     *  appropriate places. See:
     *
     *  The rest will be reorganized to reflect the very powerfull design of JATO namely the interface
     *  implementation. There are classical indicators in these classes and and extra reasons
     *  to do this. Both groups are listed as follows:
     *
     *  1. Number of repetiations (there a lot of them).
     *  2. Proxy methods existence.
     *  3. Multiple clients implementation ahead.
     *  4. Future extension of the project.
     *  5. Possible usage in other BasisXpress applications.
     *
     */
    public PageEntry getCurrentPage();

    public DealEditControl getDec();

    public ACM getAcm();

    public ActiveMessage getActMessage();

    public PassiveMessage getPasMessage();

    public JumpState getJmpState();

    /**
     *
     *
     */
    public String getDetectAlertTasks();

    /**
     *
     *
     */
    public boolean isEndTheSession();

    /**
     *
     *
     */
    public String getInstitutionName();

    /**
     *
     *
     */
    public boolean getPageIsWorkQueue();

    /**
     * the user profile id.
     */
    public int getSessionUserId();

    /**
     *
     *
     */
    public String getSessionUserName();

    /**
     *
     *
     */
    public String getSessionUserRole();

    /**
     *
     *
     */
    public int getSessionUserType();

    /**
     *
     *
     */
    public int getSessionUserTimeZone();

    /**
     *
     *
     */
    public Calendar getLastPasswordChange();

    /**
     *
     *
     */
    public int getAllowedAuthenticationAttempts();

    /**
     *
     *
     */
    public int getAuthenticationAttempts();

//    /**
//     *
//     *
//     */
//    public String getLanguage();

    /**
     *
     *
     */
    public void setBrandName(String brndName);

//    /**
//     *
//     *
//     */
//    public void setLanguage(String currLanguage);

  //--Release2.1--//
  public int getLanguageId();
  public void setLanguageId(int currentLanguageId);


    /**
     * This must be changed/integrated with the
     * PageEntry::PageStateHandler::MosWebHandlerCommon::PageHandlerCommon::DealHandlerCommon classes
     * transformation.
     *
     * Currently here for the compilation only.
     *
     * See also:
     *
     */
    public void setCurrentPage(PageEntry aPageEntry);

    /**
     *
     *
     */
    public void setDec(DealEditControl dec);

    public void setAcm(ACM acm);

    public void setActMessage(ActiveMessage am);

    public void setPasMessage(PassiveMessage pm);

    public void setJmpState(JumpState js);

    /**
     *
     *
     */
    public void setDetectAlertTasks(String in);

    /**
     *  No need to have this method. HttpSession iniation/end is provided by the
     *  container itself.
     *
     */
    ////public void setEndTheSession(boolean anEndTheSession)
    ////{
    ////    endTheSession = anEndTheSession;
    ////
    ////////}

    /**
     *
     *
     */
    public void setInstitutionName(String instName);

    /**
     *
     *
     */
    public void setPageIsWorkQueue(boolean pgIsWorkQueue);

//    /**
//     * this should be the user profile id and user institution profile id.
//     *
//     * @since 3.3
//     */
//    public void setSessionUser(UserProfile userProfile);

    /**
     *
     *
     */
    public void setSessionUserName(String sesUserName);

    /**
     *
     *
     */
    public void setSessionUserRole(String sesUserRole);

    /**
     *
     *
     */
    public void setSessionUserType(int sesUserType);

    /**
     *
     *
     */
    public void setSessionUserTimeZone(int tzId);

    /**
     *
     *
     */
    public void setLastPasswordChange(Calendar d);

    /**
     *
     *
     */
    public void setAllowedAuthenticationAttempts(int count);

    /**
     *
     *
     */
    public void setAuthenticationAttempts(int count);

  //--Release2.1--TD_DTS_CR--start//
    /**
     *
     *
     */
  public void setDocTrackingIds(Vector ids);

  /**
     *
     *
     */
  public Vector getDocTrackingIds();

  /**
     *
     *
     */
  public void setCkProduceCondUpdateDoc(String value);

  /**
     *
     *
     */
  public String getCkProduceCondUpdateDoc();
  //--Release2.1--TD_DTS_CR--end//

  //--DJ_LDI_CR--start--//
    /**
     *
     *
     */
    public String getInputToRTPCalc();

    /**
     *
     *
     */
    public String getInputToInfocalCalc();

    /**
     *
     *
     */
    public String getOutputFromInfocalCalc();

    /**
     *
     *
     */
    public void setInputToRTPCalc(String val);


    /**
     *
     *
     */
    public void setInputToInfocalCalc(String val);

    /**
     *
     *
     */
    public void setOutputFromInfocalCalc(String val);

  /**
   *
   *
   */
  public boolean getIsDisplayLifeDisabilitySubmit();

  /**
   *
   *
   */
  public void setIsDisplayLifeDisabilitySubmit(boolean value);
  //--DJ_LDI_CR--end--//

   //--CervusPhaseII--start--//
   public void overrideDealLock(boolean value);

   public boolean isOverrideDealLock();
   //--CervusPhaseII--end--//

   //--STRESSTEST--start--//  
   public String getPreviousPage();  

   public void setPreviousPage(String name);  
 
   public String getInvokeButtonName();  

   public void setInvokeButtonName(String name);  
   //--STRESSTEST--end--//  

//Closing button display end


    ////////////////////////////////////////////////////////////////////////////////
    // Field names
    ////////////////////////////////////////////////////////////////////////////////

   /***** 4.3 GR MI Response Alert *****/
   public String getMiResponseExpected();

   /**
    * @param miResponseExpected the miResponseExpected to set
    */
   public void setMiResponseExpected(String miResponseExpected);
   /***** 4.3 GR MI Response Alert *****/
   
}
