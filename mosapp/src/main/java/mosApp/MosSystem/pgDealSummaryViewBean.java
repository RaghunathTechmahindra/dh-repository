package mosApp.MosSystem;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;

import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.util.HtmlUtil;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.DisplayFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

import mosApp.SQLConnectionManagerImpl;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;


/**
 * <p>
 * Title: pgDealSummaryViewBean
 * </p>
 * 
 * <p>
 * Description: view bean for deal summary page
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 *
 *
 * @author
 * @version 1.1 Date: 7/28/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Added tile view bean registration Added 3 new fields in deal
 *          information section of deal summary page
 *          Also added the tile entry for identification section
 *          
 * * @version 1.2 06-JUN-2008 XS_16.9 Included pgQualifyingDetailsPageletView
 * - Added CHILD_STQUALIFYINGDETAILSPAGELET
 * - modified registerChildren(),createChild(String)
 *
 * 	@Version 1.3 <br>
 *	Date: 06/14/2008
 *	Author: MCM Impl Team <br>
 *	Change Log:  <br>
 *	XS_16.8 -- added 2 fields in Refinance section<br>
 *		- CHILD_STREFIADDITIONALINFORMATION,CHILD_STREFIADDITIONALINFORMATION_RESET_VALUE,
 * 		  CHILD_STREFEXISTINGMTGNUMBER,CHILD_STREFEXISTINGMTGNUMBER_RESET_VALUE
 *		- modified registerChildren(),resetChildren() ,createChild(String)	
 * @version 1.4 10/06/2008 XS_16.7  <br>
 *          Author: MCM Impl Team<br>
 *          Change: <br>
 *          Added new tiled view beans registration for component information and added TotalAmount,CashBackAmount and UnAlloacatedAmount fields
 *          Modified registerChildren(),resetChildren() ,createChild(String)  
 */
public class pgDealSummaryViewBean extends ExpressViewBeanBase
{
    ////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // Class variables
    ////////////////////////////////////////////////////////////////////////////////
    public static final String PAGE_NAME = "pgDealSummary";
    public SysLogger logger;

    public SessionStateModelImpl theSessionState;

    //// It is a variable now (see explanation in the getDisplayURL() method
    // above.
    ////public static final String
    // DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgDealSummary.jsp";
    public static Map FIELD_DESCRIPTORS;
    public static final String CHILD_STPAGELABEL = "stPageLabel";
    public static final String CHILD_STPAGELABEL_RESET_VALUE = "";
    public static final String CHILD_STCOMPANYNAME = "stCompanyName";
    public static final String CHILD_STCOMPANYNAME_RESET_VALUE = "";
    public static final String CHILD_STTODAYDATE = "stTodayDate";
    public static final String CHILD_STTODAYDATE_RESET_VALUE = "";
    public static final String CHILD_STUSERNAMETITLE = "stUserNameTitle";
    public static final String CHILD_STUSERNAMETITLE_RESET_VALUE = "";
    public static final String CHILD_DETECTALERTTASKS = "detectAlertTasks";
    public static final String CHILD_DETECTALERTTASKS_RESET_VALUE = "";
    public static final String CHILD_SESSIONUSERID = "sessionUserId";
    public static final String CHILD_SESSIONUSERID_RESET_VALUE = "";
    public static final String CHILD_TBDEALID = "tbDealId";
    public static final String CHILD_TBDEALID_RESET_VALUE = "";
    public static final String CHILD_CBPAGENAMES = "cbPageNames";
    public static final String CHILD_CBPAGENAMES_RESET_VALUE = "";

    //==================================================================
    public static final String CHILD_BTPROCEED = "btProceed";
    public static final String CHILD_BTPROCEED_RESET_VALUE = "Proceed";
    public static final String CHILD_HREF1 = "Href1";
    public static final String CHILD_HREF1_RESET_VALUE = "";
    public static final String CHILD_HREF2 = "Href2";
    public static final String CHILD_HREF2_RESET_VALUE = "";
    public static final String CHILD_HREF3 = "Href3";
    public static final String CHILD_HREF3_RESET_VALUE = "";
    public static final String CHILD_HREF4 = "Href4";
    public static final String CHILD_HREF4_RESET_VALUE = "";
    public static final String CHILD_HREF5 = "Href5";
    public static final String CHILD_HREF5_RESET_VALUE = "";
    public static final String CHILD_HREF6 = "Href6";
    public static final String CHILD_HREF6_RESET_VALUE = "";
    public static final String CHILD_HREF7 = "Href7";
    public static final String CHILD_HREF7_RESET_VALUE = "";
    public static final String CHILD_HREF8 = "Href8";
    public static final String CHILD_HREF8_RESET_VALUE = "";
    public static final String CHILD_HREF9 = "Href9";
    public static final String CHILD_HREF9_RESET_VALUE = "";
    public static final String CHILD_HREF10 = "Href10";
    public static final String CHILD_HREF10_RESET_VALUE = "";
    public static final String CHILD_CHANGEPASSWORDHREF = "changePasswordHref";
    public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE = "";
    public static final String CHILD_BTWORKQUEUELINK = "btWorkQueueLink";
    public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE = " ";
    public static final String CHILD_BTTOOLHISTORY = "btToolHistory";
    public static final String CHILD_BTTOOLHISTORY_RESET_VALUE = " ";
    public static final String CHILD_BTTOONOTES = "btTooNotes";
    public static final String CHILD_BTTOONOTES_RESET_VALUE = " ";
    public static final String CHILD_BTTOOLSEARCH = "btToolSearch";
    public static final String CHILD_BTTOOLSEARCH_RESET_VALUE = " ";
    public static final String CHILD_BTTOOLLOG = "btToolLog";
    public static final String CHILD_BTTOOLLOG_RESET_VALUE = " ";
    public static final String CHILD_BTPREVTASKPAGE = "btPrevTaskPage";
    public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE = " ";
    public static final String CHILD_BTNEXTTASKPAGE = "btNextTaskPage";
    public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE = " ";
    public static final String CHILD_BTPRINTDEALSUMMARY = "btPrintDealSummary";
    public static final String CHILD_BTPRINTDEALSUMMARY_RESET_VALUE = " ";
    public static final String CHILD_BTPRINTDEALSUMMARYLITE =
        "btPrintDealSummaryLite";
    public static final String CHILD_BTPRINTDEALSUMMARYLITE_RESET_VALUE = " ";
    public static final String CHILD_STPREVTASKPAGELABEL = "stPrevTaskPageLabel";
    public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE = "";
    public static final String CHILD_BTPARTYSUMMARY = "btPartySummary";
    public static final String CHILD_BTPARTYSUMMARY_RESET_VALUE = " ";
    public static final String CHILD_BTDECLINELETTER = "btDeclineLetter";
    public static final String CHILD_BTDECLINELETTER_RESET_VALUE = " ";
    public static final String CHILD_REPEATEDDOWNPAYMENTS =
        "RepeatedDownPayments";
    public static final String CHILD_REPEATEDESCROWPAYMENTS =
        "RepeatedEscrowPayments";
    public static final String CHILD_REPEATEDBORROWERDETAILS =
        "RepeatedBorrowerDetails";
    public static final String CHILD_REPEATEDGDS = "RepeatedGDS";
    public static final String CHILD_REPEATEDTDS = "RepeatedTDS";
    public static final String CHILD_REPEATEDINCOMEDETAILS =
        "RepeatedIncomeDetails";
    public static final String CHILD_REPEATEDLIABDETAILS = "RepeatedLiabDetails";
    public static final String CHILD_REPEATEDASSETDETAILS =
        "RepeatedAssetDetails";
    public static final String CHILD_REPEATEDPROPERTYDETAILS =
        "RepeatedPropertyDetails";
    public static final String CHILD_STLASTNAME = "stLastName";
    public static final String CHILD_STLASTNAME_RESET_VALUE = "";
    public static final String CHILD_STSUFFIX = "stSuffix";
    public static final String CHILD_STSUFFIX_RESET_VALUE = "";
    public static final String CHILD_STMIDDLEINITIAL = "stMiddleInitial";
    public static final String CHILD_STMIDDLEINITIAL_RESET_VALUE = "";
    public static final String CHILD_STFIRSTNAME = "stFirstName";
    public static final String CHILD_STFIRSTNAME_RESET_VALUE = "";
    public static final String CHILD_STLANGUAGEPREFERENCE =
        "stLanguagePreference";
    public static final String CHILD_STLANGUAGEPREFERENCE_RESET_VALUE = "";
    public static final String CHILD_STEXISTINGCLIENT = "stExistingClient";
    public static final String CHILD_STEXISTINGCLIENT_RESET_VALUE = "";
    public static final String CHILD_STREFERENCECLIENTNUMBER =
        "stReferenceClientNumber";
    public static final String CHILD_STREFERENCECLIENTNUMBER_RESET_VALUE = "";
    public static final String CHILD_STUNDERWRITER = "stUnderwriter";
    public static final String CHILD_STUNDERWRITER_RESET_VALUE = "";
    public static final String CHILD_STADMINISTRATOR = "stAdministrator";
    public static final String CHILD_STADMINISTRATOR_RESET_VALUE = "";
    public static final String CHILD_STFUNDER = "stFunder";
    public static final String CHILD_STFUNDER_RESET_VALUE = "";
    public static final String CHILD_STHOMEPHONE = "stHomePhone";
    public static final String CHILD_STHOMEPHONE_RESET_VALUE = "";
    public static final String CHILD_STWORKPHONE = "stWorkPhone";
    public static final String CHILD_STWORKPHONE_RESET_VALUE = "";
    public static final String CHILD_STNOOFBORROWERS = "stNoOfBorrowers";
    public static final String CHILD_STNOOFBORROWERS_RESET_VALUE = "";
    public static final String CHILD_STNOOFGUARANTORS = "stNoOfGuarantors";
    public static final String CHILD_STNOOFGUARANTORS_RESET_VALUE = "";
    public static final String CHILD_STBRANCH = "stBranch";
    public static final String CHILD_STBRANCH_RESET_VALUE = "";
    public static final String CHILD_STDITOTALLOANAMOUNT = "stDITotalLoanAmount";
    public static final String CHILD_STDITOTALLOANAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STPAYMENTTERM = "stPaymentTerm";
    public static final String CHILD_STPAYMENTTERM_RESET_VALUE = "";
    public static final String CHILD_STPAYMENTTERMDESC = "stPaymentTermDesc";
    public static final String CHILD_STPAYMENTTERMDESC_RESET_VALUE = "";
    public static final String CHILD_STEFFECTIVEAMORTIZATION =
        "stEffectiveAmortization";
    public static final String CHILD_STEFFECTIVEAMORTIZATION_RESET_VALUE = "";
    public static final String CHILD_STPAYMENTFREQUENCY = "stPaymentFrequency";
    public static final String CHILD_STPAYMENTFREQUENCY_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDLTV = "stCombinedLTV";
    public static final String CHILD_STCOMBINEDLTV_RESET_VALUE = "";
    public static final String CHILD_STRATECODE = "stRateCode";
    public static final String CHILD_STRATECODE_RESET_VALUE = "";
    public static final String CHILD_STRATEDATE = "stRateDate";
    public static final String CHILD_STRATEDATE_RESET_VALUE = "";
    public static final String CHILD_STFINANCINGDATE = "stFinancingDate";
    public static final String CHILD_STFINANCINGDATE_RESET_VALUE = "";
    public static final String CHILD_STPOSTEDRATE = "stPostedRate";
    public static final String CHILD_STPOSTEDRATE_RESET_VALUE = "";
    public static final String CHILD_STNETRATE = "stNetRate";
    public static final String CHILD_STNETRATE_RESET_VALUE = "";
    public static final String CHILD_STRATELOCKEDIN = "stRateLockedIn";
    public static final String CHILD_STRATELOCKEDIN_RESET_VALUE = "";
    public static final String CHILD_STDISCOUNT = "stDiscount";
    public static final String CHILD_STDISCOUNT_RESET_VALUE = "";
    public static final String CHILD_STPREMIUM = "stPremium";
    public static final String CHILD_STPREMIUM_RESET_VALUE = "";
    public static final String CHILD_STBUYDOWNRATE = "stBuyDownRate";
    public static final String CHILD_STBUYDOWNRATE_RESET_VALUE = "";
    public static final String CHILD_STDISPECIALFEATURE = "stDISpecialFeature";
    public static final String CHILD_STDISPECIALFEATURE_RESET_VALUE = "";
    public static final String CHILD_STPREAPPPRICEPURCHASE =
        "stPreAppPricePurchase";
    public static final String CHILD_STPREAPPPRICEPURCHASE_RESET_VALUE = "";
    public static final String CHILD_STPIPAYMENT = "stPIPayment";
    public static final String CHILD_STPIPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STADDITIONALPRINCIPALPAYEMT =
        "stAdditionalPrincipalPayemt";
    public static final String CHILD_STADDITIONALPRINCIPALPAYEMT_RESET_VALUE = "";
    public static final String CHILD_STTOTALESCROWPAYMENT =
        "stTotalEscrowPayment";
    public static final String CHILD_STTOTALESCROWPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STTOTALPAYMENT = "stTotalPayment";
    public static final String CHILD_STTOTALPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STFIRSTPAYMENTDATE = "stFirstPaymentDate";
    public static final String CHILD_STFIRSTPAYMENTDATE_RESET_VALUE = "";
    public static final String CHILD_STMATURITYDATE = "stMaturityDate";
    public static final String CHILD_STMATURITYDATE_RESET_VALUE = "";
    public static final String CHILD_STSECONDMORTGAGE = "stSecondMortgage";
    public static final String CHILD_STSECONDMORTGAGE_RESET_VALUE = "";
    public static final String CHILD_STEQUITYAVAILABLE = "stEquityAvailable";
    public static final String CHILD_STEQUITYAVAILABLE_RESET_VALUE = "";
    public static final String CHILD_STDIDEALPURPOSE = "stDIDealPurpose";
    public static final String CHILD_STDIDEALPURPOSE_RESET_VALUE = "";
    public static final String CHILD_STREQUIREDDOWNPAYMENT =
        "stRequiredDownPayment";
    public static final String CHILD_STREQUIREDDOWNPAYMENT_RESET_VALUE = "";
    public static final String CHILD_STDILOB = "stDILOB";
    public static final String CHILD_STDILOB_RESET_VALUE = "";
    public static final String CHILD_STDIDEALTYPE = "stDIDealType";
    public static final String CHILD_STDIDEALTYPE_RESET_VALUE = "";
    public static final String CHILD_STPRODUCT = "stProduct";
    public static final String CHILD_STPRODUCT_RESET_VALUE = "";
    public static final String CHILD_STPREPAYMENTOPTION = "stPrePaymentOption";
    public static final String CHILD_STPREPAYMENTOPTION_RESET_VALUE = "";
    public static final String CHILD_STREFERENCETYPE = "stReferenceType";
    public static final String CHILD_STREFERENCETYPE_RESET_VALUE = "";
    public static final String CHILD_STREFERENCENO = "stReferenceNo";
    public static final String CHILD_STREFERENCENO_RESET_VALUE = "";
    public static final String CHILD_STESTIMATEDCLOSINGDATE =
        "stEstimatedClosingDate";
    public static final String CHILD_STESTIMATEDCLOSINGDATE_RESET_VALUE = "";
    public static final String CHILD_STACTUALCLOSINGDATE = "stActualClosingDate";
    public static final String CHILD_STACTUALCLOSINGDATE_RESET_VALUE = "";
    public static final String CHILD_STPRIVILEGEPAYMENTOPTION =
        "stPrivilegePaymentOption";
    public static final String CHILD_STPRIVILEGEPAYMENTOPTION_RESET_VALUE = "";
    public static final String CHILD_STINCLUDEFINANCINGPROGRAMSTART =
        "stIncludeFinancingProgramStart";
    public static final String CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE =
        "";
    public static final String CHILD_STFINANCINGPROGRAM = "stFinancingProgram";
    public static final String CHILD_STFINANCINGPROGRAM_RESET_VALUE = "";
    public static final String CHILD_STINCLUDEFINANCINGPROGRAMEND =
        "stIncludeFinancingProgramEnd";
    public static final String CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE =
        "";
/*    public static final String CHILD_STDISOURCE = "stDISource";
    public static final String CHILD_STDISOURCE_RESET_VALUE = "";
    public static final String CHILD_STDISOURCEFIRM = "stDISourceFirm";
    public static final String CHILD_STDISOURCEFIRM_RESET_VALUE = "";
    public static final String CHILD_STDISOURCEADDRESS = "stDISourceAddress";
    public static final String CHILD_STDISOURCEADDRESS_RESET_VALUE = "";
    public static final String CHILD_STDISOURCECONTACTPHONE =
        "stDISourceContactPhone";
    public static final String CHILD_STDISOURCECONTACTPHONE_RESET_VALUE = "";
    public static final String CHILD_STDISOURCECONTACTPHONEEXT =
        "stDISourceContactPhoneExt";
    public static final String CHILD_STDISOURCECONTACTPHONEEXT_RESET_VALUE = "";
    public static final String CHILD_STDISOURCECONTACTFAX =
        "stDISourceContactFax";
    public static final String CHILD_STDISOURCECONTACTFAX_RESET_VALUE = "";*/
	//4.4 Submission Agent
    public static final String CHILD_REPEATEDSOB = "RepeatedSOB";
    public static final String CHILD_REPEATEDSOB_RESET_VALUE = "";

    public static final String CHILD_STREFERENCESOURCEAPP =
        "stReferenceSourceApp";
    public static final String CHILD_STREFERENCESOURCEAPP_RESET_VALUE = "";
    public static final String CHILD_STCROSSSELL = "stCrossSell";
    public static final String CHILD_STCROSSSELL_RESET_VALUE = "";
    public static final String CHILD_STTOTALPURCHASEPRICE =
        "stTotalPurchasePrice";
    public static final String CHILD_STTOTALPURCHASEPRICE_RESET_VALUE = "";
    public static final String CHILD_STTOTALESTIMATEDVALUE =
        "stTotalEstimatedValue";
    public static final String CHILD_STTOTALESTIMATEDVALUE_RESET_VALUE = "";
    public static final String CHILD_STTOTALACTUALAPPRAISEDVALUE =
        "stTotalActualAppraisedValue";
    public static final String CHILD_STTOTALACTUALAPPRAISEDVALUE_RESET_VALUE = "";
    public static final String CHILD_STLENDER = "stLender";
    public static final String CHILD_STLENDER_RESET_VALUE = "";
    public static final String CHILD_STINVESTOR = "stInvestor";
    public static final String CHILD_STINVESTOR_RESET_VALUE = "";
    public static final String CHILD_STBRIDGEPURPOSE = "stBridgePurpose";
    public static final String CHILD_STBRIDGEPURPOSE_RESET_VALUE = "";
    public static final String CHILD_STBRIDGELENDER = "stBridgeLender";
    public static final String CHILD_STBRIDGELENDER_RESET_VALUE = "";
    public static final String CHILD_STBRIDGEPRODUCT = "stBridgeProduct";
    public static final String CHILD_STBRIDGEPRODUCT_RESET_VALUE = "";
    public static final String CHILD_STBRIDGEPAYMENTTERMDESC =
        "stBridgePaymentTermDesc";
    public static final String CHILD_STBRIDGEPAYMENTTERMDESC_RESET_VALUE = "";
    public static final String CHILD_STBRIDGERATECODE = "stBridgeRateCode";
    public static final String CHILD_STBRIDGERATECODE_RESET_VALUE = "";
    public static final String CHILD_STBRIDGEPOSTEDINTERESTRATE =
        "stBridgePostedInterestRate";
    public static final String CHILD_STBRIDGEPOSTEDINTERESTRATE_RESET_VALUE = "";
    public static final String CHILD_STBRIDGERATEDATE = "stBridgeRateDate";
    public static final String CHILD_STBRIDGERATEDATE_RESET_VALUE = "";
    public static final String CHILD_STBRIDGEDISCOUNT = "stBridgeDiscount";
    public static final String CHILD_STBRIDGEDISCOUNT_RESET_VALUE = "";
    public static final String CHILD_STBRIDGEPREMIUM = "stBridgePremium";
    public static final String CHILD_STBRIDGEPREMIUM_RESET_VALUE = "";
    public static final String CHILD_STBRIDGENETRATE = "stBridgeNetRate";
    public static final String CHILD_STBRIDGENETRATE_RESET_VALUE = "";
    public static final String CHILD_STBRIDGELOANAMOUNT = "stBridgeLoanAmount";
    public static final String CHILD_STBRIDGELOANAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STBRIDGEACTUALPAYMENTTERM =
        "stBridgeActualPaymentTerm";
    public static final String CHILD_STBRIDGEACTUALPAYMENTTERM_RESET_VALUE = "";
    public static final String CHILD_STBRIDGEMATURITYDATE =
        "stBridgeMaturityDate";
    public static final String CHILD_STBRIDGEMATURITYDATE_RESET_VALUE = "";
    public static final String CHILD_STBEGINHIDEREFISECTION =
        "stBeginHideRefiSection";
    public static final String CHILD_STBEGINHIDEREFISECTION_RESET_VALUE = "";
    public static final String CHILD_STREFIPURCHASEDATE = "stRefiPurchaseDate";
    public static final String CHILD_STREFIPURCHASEDATE_RESET_VALUE = "";
    public static final String CHILD_STREFIMORTGAGEHOLDER =
        "stRefiMortgageHolder";
    public static final String CHILD_STREFIMORTGAGEHOLDER_RESET_VALUE = "";
    public static final String CHILD_STREFIORIGPURCHASEPRICE =
        "stRefiOrigPurchasePrice";
    public static final String CHILD_STREFIORIGPURCHASEPRICE_RESET_VALUE = "";
    public static final String CHILD_STREFIIMPROVEMENTVALUE =
        "stRefiImprovementValue";
    public static final String CHILD_STREFIIMPROVEMENTVALUE_RESET_VALUE = "";
    public static final String CHILD_STREFIBLENDEDAMORIZATION =
        "stRefiBlendedAmorization";
    public static final String CHILD_STREFIBLENDEDAMORIZATION_RESET_VALUE = "";
    public static final String CHILD_STREFIPURPOSE = "stRefiPurpose";
    public static final String CHILD_STREFIPURPOSE_RESET_VALUE = "";
    public static final String CHILD_STREFIORIGMTGAMOUNT = "stRefiOrigMtgAmount";
    public static final String CHILD_STREFIORIGMTGAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STREFIOUTSTANDINGMTGAMOUNT =
        "stRefiOutstandingMtgAmount";
    public static final String CHILD_STREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STREFIIMPROVEMENTDESC =
        "stRefiImprovementDesc";
    public static final String CHILD_STREFIIMPROVEMENTDESC_RESET_VALUE = "";
    public static final String CHILD_STENDHIDEREFISECTION =
        "stEndHideRefiSection";

    public static final String CHILD_STENDHIDEREFISECTION_RESET_VALUE = "";
    public static final String CHILD_STPROGRESSADVANCE = "stProgressAdvance";
    public static final String CHILD_STPROGRESSADVANCE_RESET_VALUE = "";
    public static final String CHILD_STADVANCETODATEAMT = "stAdvanceToDateAmt";
    public static final String CHILD_STADVANCETODATEAMT_RESET_VALUE = "";
    public static final String CHILD_STNEXTADVANCEAMT = "stNextAdvanceAmt";
    public static final String CHILD_STNEXTADVANCEAMT_RESET_VALUE = "";
    public static final String CHILD_STADVANCENUMBER = "stAdvanceNumber";
    public static final String CHILD_STADVANCENUMBER_RESET_VALUE = "";
    public static final String CHILD_STCREDITSCORE = "stCreditScore";
    public static final String CHILD_STCREDITSCORE_RESET_VALUE = "0";
    public static final String CHILD_STCOMBINEDGDS = "stCombinedGDS";
    public static final String CHILD_STCOMBINEDGDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINED3YRGDS = "stCombined3YrGds";
    public static final String CHILD_STCOMBINED3YRGDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDTDS = "stCombinedTDS";
    public static final String CHILD_STCOMBINEDTDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINED3YRTDS = "stCombined3YrTds";
    public static final String CHILD_STCOMBINED3YRTDS_RESET_VALUE = "";
    //***** Qualifying Rate *****/
    public static final String CHILD_STQUALIFYPRODUCT = "stQualifyProduct";
    public static final String CHILD_STQUALIFYPRODUCT_RESET_VALUE = "";
    public static final String CHILD_STQUALIFYRATE = "stQualifyRate";
    public static final String CHILD_STQUALIFYRATE_RESET_VALUE = "";
    public static final String CHILD_HDQUALIFYRATEOVERRIDE = "hdQualifyRateOverride";
    public static final String CHILD_HDQUALIFYRATEOVERRIDE_RESET_VALUE = "N";    
    public static final String CHILD_HDQUALIFYRATEOVERRIDERATE = "hdQualifyRateOverrideRate";
    public static final String CHILD_HDQUALIFYRATEOVERRIDERATE_RESET_VALUE = "N";
    //***** Qualifying Rate *****/
    
    public static final String CHILD_STCOMBINEDTOTALINCOME =
        "stcombinedTotalIncome";
    public static final String CHILD_STCOMBINEDTOTALINCOME_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDTOTALLIABILITIES =
        "stcombinedTotalLiabilities";
    public static final String CHILD_STCOMBINEDTOTALLIABILITIES_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDTOTALASSETS =
        "stcombinedTotalAssets";
    public static final String CHILD_STCOMBINEDTOTALASSETS_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYSTREETNO = "stPropertyStreetno";
    public static final String CHILD_STPROPERTYSTREETNO_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYSTREETNAME =
        "stPropertyStreetname";
    public static final String CHILD_STPROPERTYSTREETNAME_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYSTREETTYPE =
        "stPropertyStreetType";
    public static final String CHILD_STPROPERTYSTREETTYPE_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYSTREETDIRECTION =
        "stPropertyStreetDirection";
    public static final String CHILD_STPROPERTYSTREETDIRECTION_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYUNITNUMBER =
        "stPropertyUnitNumber";
    public static final String CHILD_STPROPERTYUNITNUMBER_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYADDRESS2 = "stPropertyAddress2";
    public static final String CHILD_STPROPERTYADDRESS2_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYCITY = "stPropertyCity";
    public static final String CHILD_STPROPERTYCITY_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYPROVINCE = "stPropertyProvince";
    public static final String CHILD_STPROPERTYPROVINCE_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYPOSTALCODE =
        "stPropertyPostalCode";
    public static final String CHILD_STPROPERTYPOSTALCODE_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYPOSTALCODE1 =
        "stPropertyPostalCode1";
    public static final String CHILD_STPROPERTYPOSTALCODE1_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYUSAGE = "stPropertyUsage";
    public static final String CHILD_STPROPERTYUSAGE_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYOCCUPANCY = "stPropertyOccupancy";
    public static final String CHILD_STPROPERTYOCCUPANCY_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYTYPE = "stPropertyType";
    public static final String CHILD_STPROPERTYTYPE_RESET_VALUE = "";
    public static final String CHILD_STNOOFPROPERTIES = "stNoOfProperties";
    public static final String CHILD_STNOOFPROPERTIES_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYPURCHASEPRICE =
        "stPropertyPurchasePrice";
    public static final String CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE = "";
    public static final String CHILD_STPROPERTYESTIMATEDAPPRAISALVALUE =
        "stPropertyEstimatedAppraisalValue";
    public static final String CHILD_STPROPERTYESTIMATEDAPPRAISALVALUE_RESET_VALUE =
        "";
    public static final String CHILD_STPROPERTYACTUALAPRAISEDVALUE =
        "stPropertyActualApraisedValue";
    public static final String CHILD_STPROPERTYACTUALAPRAISEDVALUE_RESET_VALUE =
        "";
    public static final String CHILD_STPROPERTYLANDVALUE = "stPropertyLandValue";
    public static final String CHILD_STPROPERTYLANDVALUE_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDTOTALPROPERTYEXP =
        "stCombinedTotalPropertyExp";
    public static final String CHILD_STCOMBINEDTOTALPROPERTYEXP_RESET_VALUE = "";
    public static final String CHILD_STMIINDICTORDESC = "stMIIndictorDesc";
    public static final String CHILD_STMIINDICTORDESC_RESET_VALUE = "";
    public static final String CHILD_STMIREQUESTSTATUS = "stMIRequestStatus";
    public static final String CHILD_STMIREQUESTSTATUS_RESET_VALUE = "";
    public static final String CHILD_STMIEXISTINGPOLICYNUM =
        "stMIExistingPolicyNum";
    public static final String CHILD_STMIEXISTINGPOLICYNUM_RESET_VALUE = "";
    public static final String CHILD_STMIPOLICENUMBER = "stMIPoliceNumber";
    public static final String CHILD_STMIPOLICENUMBER_RESET_VALUE = "";
    public static final String CHILD_STMIPREQUALIFICATIONMICERTNUM =
        "stMIPrequalificationMICertNum";
    public static final String CHILD_STMIPREQUALIFICATIONMICERTNUM_RESET_VALUE =
        "";
    public static final String CHILD_STMITYPE = "stMIType";
    public static final String CHILD_STMITYPE_RESET_VALUE = "";
    public static final String CHILD_STMIINSURER = "stMIInsurer";
    public static final String CHILD_STMIINSURER_RESET_VALUE = "";
    // SEAN DJ SPEC-Progress Advance Type July 25, 2005: added a display field
    // it will be hidden for non-DJ client.
    public static final String CHILD_STPROGRESSADVANCETYPE = "stProgressAdvanceType";
    public static final String CHILD_STPROGRESSADVANCETYPE_RESET_VALUE = "";
    // SEAN DJ SPEC-PAT END
    public static final String CHILD_STMIPAYORDESC = "stMIPayorDesc";
    public static final String CHILD_STMIPAYORDESC_RESET_VALUE = "";
    public static final String CHILD_STMIUPFRONT = "stMIUpfront";
    public static final String CHILD_STMIUPFRONT_RESET_VALUE = "";
    public static final String CHILD_STMIPREMIUM = "stMIPremium";
    public static final String CHILD_STMIPREMIUM_RESET_VALUE = "";
    public static final String CHILD_STMIRUINTERVENTION = "stMIRUIntervention";
    public static final String CHILD_STMIRUINTERVENTION_RESET_VALUE = "";
    public static final String CHILD_STERRORFLAG = "stErrorFlag";
    public static final String CHILD_STERRORFLAG_RESET_VALUE = "N";
    public static final String CHILD_STDEALID = "stDealId";
    public static final String CHILD_STDEALID_RESET_VALUE = "";
    public static final String CHILD_STBORRFIRSTNAME = "stBorrFirstName";
    public static final String CHILD_STBORRFIRSTNAME_RESET_VALUE = "";
    public static final String CHILD_STDEALSTATUS = "stDealStatus";
    public static final String CHILD_STDEALSTATUS_RESET_VALUE = "";
    public static final String CHILD_STDEALSTATUSDATE = "stDealStatusDate";
    public static final String CHILD_STDEALSTATUSDATE_RESET_VALUE = "";
    public static final String CHILD_STSOURCEFIRM = "stSourceFirm";
    public static final String CHILD_STSOURCEFIRM_RESET_VALUE = "";
    public static final String CHILD_STSOURCE = "stSource";
    public static final String CHILD_STSOURCE_RESET_VALUE = "";
    public static final String CHILD_STESTCLOSINGDATE = "stEstClosingDate";
    public static final String CHILD_STESTCLOSINGDATE_RESET_VALUE = "";
    public static final String CHILD_STLOB = "stLOB";
    public static final String CHILD_STLOB_RESET_VALUE = "";
    public static final String CHILD_STSPECIALFEATURE = "stSpecialFeature";
    public static final String CHILD_STSPECIALFEATURE_RESET_VALUE = "";
    public static final String CHILD_STDEALTYPE = "stDealType";
    public static final String CHILD_STDEALTYPE_RESET_VALUE = "";
    public static final String CHILD_STDEALPURPOSE = "stDealPurpose";
    public static final String CHILD_STDEALPURPOSE_RESET_VALUE = "";
    public static final String CHILD_STPURCHASEPRICE = "stPurchasePrice";
    public static final String CHILD_STPURCHASEPRICE_RESET_VALUE = "";
    public static final String CHILD_STPMTTERM = "stPmtTerm";
    public static final String CHILD_STPMTTERM_RESET_VALUE = "";
    public static final String CHILD_STTOTALLOANAMOUNT = "stTotalLoanAmount";
    public static final String CHILD_STTOTALLOANAMOUNT_RESET_VALUE = "";
    public static final String CHILD_STNEXTTASKPAGELABEL = "stNextTaskPageLabel";
    public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE = "";
    public static final String CHILD_STTASKNAME = "stTaskName";
    public static final String CHILD_STTASKNAME_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDBORROWERGDS =
        "stCombinedBorrowerGDS";
    public static final String CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE = "";
    public static final String CHILD_STCOMBINEDBORROWERTDS =
        "stCombinedBorrowerTDS";
    public static final String CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE = "";
    public static final String CHILD_STPMGENERATE = "stPmGenerate";
    public static final String CHILD_STPMGENERATE_RESET_VALUE = "";
    public static final String CHILD_STPMHASTITLE = "stPmHasTitle";
    public static final String CHILD_STPMHASTITLE_RESET_VALUE = "";
    public static final String CHILD_STPMHASINFO = "stPmHasInfo";
    public static final String CHILD_STPMHASINFO_RESET_VALUE = "";
    public static final String CHILD_STPMHASTABLE = "stPmHasTable";
    public static final String CHILD_STPMHASTABLE_RESET_VALUE = "";
    public static final String CHILD_STPMHASOK = "stPmHasOk";
    public static final String CHILD_STPMHASOK_RESET_VALUE = "";
    public static final String CHILD_STPMTITLE = "stPmTitle";
    public static final String CHILD_STPMTITLE_RESET_VALUE = "";
    public static final String CHILD_STPMINFOMSG = "stPmInfoMsg";
    public static final String CHILD_STPMINFOMSG_RESET_VALUE = "";
    public static final String CHILD_STPMONOK = "stPmOnOk";
    public static final String CHILD_STPMONOK_RESET_VALUE = "";
    public static final String CHILD_STPMMSGS = "stPmMsgs";
    public static final String CHILD_STPMMSGS_RESET_VALUE = "";
    public static final String CHILD_STPMMSGTYPES = "stPmMsgTypes";
    public static final String CHILD_STPMMSGTYPES_RESET_VALUE = "";
    public static final String CHILD_STAMGENERATE = "stAmGenerate";
    public static final String CHILD_STAMGENERATE_RESET_VALUE = "";
    public static final String CHILD_STAMHASTITLE = "stAmHasTitle";
    public static final String CHILD_STAMHASTITLE_RESET_VALUE = "";
    public static final String CHILD_STAMHASINFO = "stAmHasInfo";
    public static final String CHILD_STAMHASINFO_RESET_VALUE = "";
    public static final String CHILD_STAMHASTABLE = "stAmHasTable";
    public static final String CHILD_STAMHASTABLE_RESET_VALUE = "";
    public static final String CHILD_STAMTITLE = "stAmTitle";
    public static final String CHILD_STAMTITLE_RESET_VALUE = "";
    public static final String CHILD_STAMINFOMSG = "stAmInfoMsg";
    public static final String CHILD_STAMINFOMSG_RESET_VALUE = "";
    public static final String CHILD_STAMMSGS = "stAmMsgs";
    public static final String CHILD_STAMMSGS_RESET_VALUE = "";
    public static final String CHILD_STAMMSGTYPES = "stAmMsgTypes";
    public static final String CHILD_STAMMSGTYPES_RESET_VALUE = "";
    public static final String CHILD_STAMDIALOGMSG = "stAmDialogMsg";
    public static final String CHILD_STAMDIALOGMSG_RESET_VALUE = "";
    public static final String CHILD_STAMBUTTONSHTML = "stAmButtonsHtml";
    public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE = "";
    public static final String CHILD_STBEGINHIDEBRIDGESECTION =
        "stBeginHideBridgeSection";
    public static final String CHILD_STBEGINHIDEBRIDGESECTION_RESET_VALUE = "";
    public static final String CHILD_STENDHIDEBRIDGESECTION =
        "stEndHideBridgeSection";
    public static final String CHILD_STENDHIDEBRIDGESECTION_RESET_VALUE = "";
    public static final String CHILD_STENDHIDEESCROWSECTION =
        "stEndHideEscrowSection";
    public static final String CHILD_STENDHIDEESCROWSECTION_RESET_VALUE = "";
    public static final String CHILD_STBEGINHIDEESCROWSECTION =
        "stBeginHideEscrowSection";
    public static final String CHILD_STBEGINHIDEESCROWSECTION_RESET_VALUE = "";
    public static final String CHILD_HDEXECUTEESCOW = "hdExecuteEscow";
    public static final String CHILD_HDEXECUTEESCOW_RESET_VALUE = "";
    public static final String CHILD_REPEATEDPROPERTYADDRESSANDEXPENSES =
        "RepeatedPropertyAddressAndExpenses";
    public static final String CHILD_STBORROWERWORKPHONEEXT =
        "stBorrowerWorkPhoneExt";
    public static final String CHILD_STBORROWERWORKPHONEEXT_RESET_VALUE = "??";
    public static final String CHILD_BTOKMAINPAGEBUTTON = "btOkMainPageButton";
    public static final String CHILD_BTOKMAINPAGEBUTTON_RESET_VALUE = "Custom";

    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    public static final String CHILD_BTACTMSG = "btActMsg";
    public static final String CHILD_BTACTMSG_RESET_VALUE = "ActMsg";

    //// New link to toggle the language. When touched this link should reload
    // the
    //// page in opposite language (french versus english) and set this new
    // language
    //// session id throughout all modulus.
    public static final String CHILD_TOGGLELANGUAGEHREF = "toggleLanguageHref";
    public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE = "";

    //--> New requirement : added new button for reverse funding
    //--> By Billy 11June2003
    public static final String CHILD_BTREVERSEFUNDING = "btReverseFunding";
    public static final String CHILD_BTREVERSEFUNDING_RESET_VALUE = " ";

    //--DJ_LDI_CR--start--//
    public static final String CHILD_REPEATEDLIFEDISABILITYINSURANCE =
        "RepeatedLifeDisabilityInsurance";
    public static final String CHILD_STINCLUDELIFEDISSECTIONSTART =
        "stIncludeLifeDisSectionStart";
    public static final String CHILD_STINCLUDELIFEDISSECTIONSTART_RESET_VALUE =
        "";
    public static final String CHILD_STINCLUDELIFEDISSECTIONEND =
        "stIncludeLifeDisSectionEnd";
    public static final String CHILD_STINCLUDELIFEDISSECTIONEND_RESET_VALUE = "";

    //--DJ_LDI_CR--end--//
    //--DJ_CR134--start--27May2004--//
    public static final String CHILD_STHOMEBASEPRODUCTRATEPMNT =
        "stHomeBASERateProductPmnt";
    public static final String CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE = "";
    public static final String CHILD_STINCLUDELIFEDISLABELSSTART =
        "stIncludeLifeDisLabelsStart";
    public static final String CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE = "";
    public static final String CHILD_STINCLUDELIFEDISLABELSEND =
        "stIncludeLifeDisLabelsEnd";
    public static final String CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE = "";

    //--DJ_CR134--end--//
    //-- ========== DJ#725 Begins ========== --//
    //-- By Neil at Nov/29/2004
    public static final String CHILD_STCONTACTEMAILADDRESS =
        "stContactEmailAddress";
    public static final String CHILD_STCONTACTEMAILADDRESS_RESET_VALUE = "";
    public static final String CHILD_STPSDESCRIPTION = "stPsDescription";
    public static final String CHILD_STPSDESCRIPTION_RESET_VALUE = "";
    public static final String CHILD_STSYSTEMTYPEDESCRIPTION =
        "stSystemTypeDescription";
    public static final String CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE = "";

    //-- ========== DJ#725 Ends ========== --//
    // #1676, Catherine, 2-Sep-05 ---- start ----------------------------
    public static final String CHILD_STHIDEREVERSEBTNSTART =
        "stHideReverseBtnStart";
    public static final String CHILD_STHIDEREVERSEBTNSTART_RESET_VALUE = "";
    public static final String CHILD_STHIDEREVERSEBTNEND =
        "stHideReverseBtnEnd";
    public static final String CHILD_STHIDEREVERSEBTNEND_RESET_VALUE = "";
    // #1676, Catherine, 2-Sep-05 ---- end ----------------------------


    // SEAN Ticket #1681 June 27, 2005: Hide the print deal summary button for
    // DJ.
    public static final String CHILD_STBEGINHIDEPRINTDEALSUMMARY =
        "stBeginHidePrintDealSummary";
    public static final String CHILD_STBEGINHIDEPRINTDEALSUMMARY_RESET_VALUE = "";
    public static final String CHILD_STENDHIDEPRINTDEALSUMMARY =
        "stEndHidePrintDealSummary";
    public static final String CHILD_STENDHIDEPRINTDEALSUMMARY_RESET_VALUE = "";
    // SEAN Ticket #1681 EDN

    //--Release3.1--begins
    //--by Hiro Mar 31, 2006

    //***** Change by NBC/PP Implementation Team - GCD - Start *****//

    public static final String CHILD_TX_CREDIT_DECISION_STATUS = "txCreditDecisionStatus";
    public static final String CHILD_TX_CREDIT_DECISION = "txCreditDecision";
    public static final String CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE = "txGlobalCreditBureauScore";
    public static final String CHILD_TX_GLOBAL_RISK_RATING = "txGlobalRiskRating";
    public static final String CHILD_TX_GLOBAL_INTERNAL_SCORE = "txGlobalInternalScore";
    public static final String CHILD_TX_MISTATUS = "txMIStatus";
    public static final String CHILD_BT_CREDIT_DECISION_PG = "btCreditDecisionPG";
    public static final String CHILD_HD_DEAL_ID = "hdDealID";

    public static final String CHILD_ST_INCLUDE_GCDSUM_START = "stIncludeGCDSumStart";
    public static final String CHILD_ST_INCLUDE_GCDSUM_END = "stIncludeGCDSumEnd";

    public static final String CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE = "";
    public static final String CHILD_TX_CREDIT_DECISION_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE = "";
    public static final String CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE = "";
    public static final String CHILD_TX_MISTATUS_RESET_VALUE = "";
    public static final String CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE = "";
    public static final String CHILD_HD_DEAL_ID_RESET_VALUE = "";

    public static final String CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE = "";
    public static final String CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE = "";

    private doAdjudicationResponseBNCModel doAdjudicationResponseBNCModel= null;

    //***** Change by NBC/PP Implementation Team - GCD - End *****//

    public static final String CHILD_STPRODUCTTYPE = "stProductType";

    public static final String CHILD_STPRODUCTTYPE_RESET_VALUE = "";

    public static final String CHILD_STCHARGE = "stCharge";

    public static final String CHILD_STCHARGE_RESET_VALUE = "";

    public static final String CHILD_STREFIPRODUCTTYPE = "stRefiProductType";

    public static final String CHILD_STREFIPRODUCTTYPE_RESET_VALUE = "";

    public static final String CHILD_STPROGRESSADVANCEINSPECTIONBY = "stProgressAdvanceInspectionBy";

    public static final String CHILD_STPROGRESSADVANCEINSPECTIONBY_RESET_VALUE = "";

    public static final String CHILD_STSELFDIRECTEDRRSP = "stSelfDirectedRRSP";

    public static final String CHILD_STSELFDIRECTEDRRSP_RESET_VALUE = "";

    public static final String CHILD_STCMHCPRODUCTTRACKERIDENTIFIER = "stCMHCProductTrackerIdentifier";

    public static final String CHILD_STCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE = "";

    public static final String CHILD_STLOCREPAYMENT = "stLOCRepayment";

    public static final String CHILD_STLOCREPAYMENT_RESET_VALUE = "";

    public static final String CHILD_STREQUESTSTANDARDSERVICE = "stRequestStandardService";

    public static final String CHILD_STREQUESTSTANDARDSERVICE_RESET_VALUE = "";

    public static final String CHILD_STLOCAMORTIZATION = "stLOCAmortization";

    public static final String CHILD_STLOCAMORTIZATION_RESET_VALUE = "";

    public static final String CHILD_STLOCINTERESTONLYMATURITYDATE = "stLOCInterestOnlyMaturityDate";

    public static final String CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE = "";

    //--Release3.1--ends
    //PPI start
    public static final String CHILD_STIMPROVEDVALUE = "stImprovedValue";
    public static final String CHILD_STIMPROVEDVALUE_RESET_VALUE = "";

    public static final String CHILD_TXDEALPURPOSETYPEHIDDENSTART ="stDealPurposeTypeHiddenStart";
    public static final String CHILD_TXDEALPURPOSETYPEHIDDENSTART_RESET_VALUE = "";

    public static final String CHILD_TXDEALPURPOSETYPEHIDDENEND ="stDealPurposeTypeHiddenEnd";
    public static final String CHILD_TXDEALPURPOSETYPEHIDDENEND_RESET_VALUE = "";
    //PPI end

    //--Release2.1--//
    //The Option list should not be Static, otherwise this will screw up
    //--> By Billy 15Nov2002
    //private static CbPageNamesOptionList cbPageNamesOptions=new
    // CbPageNamesOptionList();
    private CbPageNamesOptionList cbPageNamesOptions =
        new CbPageNamesOptionList();

    //Added the Variable for NonSelected Label
    private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

    ////////////////////////////////////////////////////////////////////////////
    // Instance variables
    ////////////////////////////////////////////////////////////////////////////
    private doMainViewBorrowerModel doMainViewBorrower = null;
    private doMainViewDealModel doMainViewDeal = null;
    private doMainViewBridgeModel doMainViewBridge = null;
    private doMainViewPrimaryPropertyModel doMainViewPrimaryProperty = null;
    private doMainViewMortgageInsurerModel doMainViewMortgageInsurer = null;
    private doDealSummarySnapShotModel doDealSummarySnapShot = null;
    private doMainViewEscrowModel doMainViewEscrow = null;

    //--DJ_LDI_CR--start--//
    private doUWGDSTDSApplicantDetailsModel doUWGDSTDSApplicantDetails = null;

    //--DJ_LDI_CR--end--//
    protected String DEFAULT_DISPLAY_URL = "/mosApp/MosSystem/pgDealSummary.jsp";

//  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//


    public static final String CHILD_STCASHBACKINDOLLARS = "stCashBackInDollars";

    public static final String CHILD_STCASHBACKINDOLLARS_RESET_VALUE = "";

    public static final String CHILD_STCASHBACKINPERCENTAGE = "stCashBackInPercentage";

    public static final String CHILD_STCASHBACKINPERCENTAGE_RESET_VALUE = "";

    public static final String CHILD_STAFFILIATIONPROGRAM = "stAffiliationProgram";

    public static final String CHILD_STAFFILIATIONPROGRAM_RESET_VALUE = "";

    /***************MCM Impl team changes starts - XS_16.9*******************/
    public static final String CHILD_STQUALIFYINGDETAILSPAGELET = "qualifyingDetailsPagelet";
    /***************MCM Impl team changes ends - XS_16.9*******************/

    /***************MCM Impl team changes starts - XS_16.8 *******************/

    public static final String CHILD_STREFIADDITIONALINFORMATION ="stRefiAdditionalInformation";
    public static final String CHILD_STREFIADDITIONALINFORMATION_RESET_VALUE = "";

    public static final String CHILD_STREFEXISTINGMTGNUMBER ="stRefExistingMTGNumber";
    public static final String CHILD_STREFEXISTINGMTGNUMBER_RESET_VALUE = "";


    /***************MCM Impl team changes ends - XS_16.8 *********************/

    /***************MCM Impl team changes starts - XS_16.7 ****************************************/
		 public static final String CHILD_REPEATEDMORTGAGECOMPONENTS ="RepeatedMortgageComponents";
    public static final String CHILD_REPEATEDLOCCOMPONENTS ="RepeatedLocComponents";
    public static final String CHILD_REPEATEDLOANCOMPONENTS ="RepeatedLoanComponents";
    public static final String CHILD_REPEATEDCREDITCARDCOMPONENTS ="RepeatedCreditCardComponents";
    public static final String CHILD_REPEATEDOVERDRAFTCOMPONENTS ="RepeatedOverDraftComponents";

    public static final String CHILD_STTOTALAMOUNT = "stTotalAmount";
    public static final String CHILD_STTOTALAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STTOTALCASHBACKAMOUNT = "stTotalCashBackAmount";
    public static final String CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE = "";

    public static final String CHILD_STUNALLOCATEDAMOUNT = "stUnAllocatedAmount";
    public static final String CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE = "";

    public static final String CHILD_BTCOMPONENTDETAILS = "btComponentDetails";
    public static final String CHILD_BTCOMPONENTDETAILS_RESET_VALUE = "";

    public static final String CHILD_STINCLUDE_COMPONENTDETAIL_START = "stIncludeComponentDetailsButtonStart";
    public static final String CHILD_STINCLUDE_COMPONENTDETAIL_END = "stIncludeComponentDetailsButtonEnd";

    public static final String CHILD_STINCLUDE_COMPONENTDETAIL_START_RESET_VALUE = "";
    public static final String CHILD_STINCLUDE_COMPONENTDETAIL_END_RESET_VALUE = "";

		 public static final String CHILD_STINCLUDECOMPONENTINFOSECTION_START_RESET_VALUE = "";
		 public static final String CHILD_STINCLUDECOMPONENTINFOSECTION_START =
		    "stIncludeComponentInfoSectionStart";

		 public static final String CHILD_STINCLUDECOMPONENTINFOSECTION_END_RESET_VALUE = "";
		 public static final String CHILD_STINCLUDECOMPONENTINFOSECTION_END =
		    "stIncludeComponentInfoSectionEnd";

    public static final String CHILD_STQUALIFYRATEEDIT = "stQualifyRateEdit";
    public static final String CHILD_STQUALIFYRATEEDIT_RESET_VALUE = "none";

    private doComponentSummaryModel doComponentSummaryModel = null;
	/***************MCM Impl team changes ends - XS_16.7 ****************************************/
    // ***** Change by NBC Impl. Team - Version 1.1 - End*****//
    ////////////////////////////////////////////////////////////////////////////
    // Custom Members - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    // MigrationToDo : Migrate custom member
    private DealSummaryHandler handler = new DealSummaryHandler();

    /**
     *
     *
     */
    public pgDealSummaryViewBean() {
        super(PAGE_NAME);
        setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
        registerChildren();
        initialize();
    }

    /**
     *
     *
     */
    protected void initialize() {
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Child manipulation methods
    ////////////////////////////////////////////////////////////////////////////////

    /**
     * createChild
     * 
     * @param String
     *            <br>
     * 
     * @return View : the result of createChild <br>
     * @version 1.1 Date: 7/28/006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: <br>
     *          Added 3 new fields cash back amount, cash back percentage, one
     *          tile for borrower identification
     *
     *  @Version 1.2 <br>
     *	Date: 13/06/2008
     *	Author: MCM Impl Team <br>
     *	Change Log:  <br>
     *	XS_16.8 -- Added necessary changes required for the new fields added in Refinance section<br>
	   *  @version 1.4 10/06/2008 XS_16.7  <br>
	   *          Author: MCM Impl Team  <br>
     *          Change: <br>
	   *          Added necessary changes required for the new fields added in Component Information section
     */
    protected View createChild(String name)
    {
        View superReturn = super.createChild(name);  
        if (superReturn != null) {  
            return superReturn;  
        }
        else if (name.equals(CHILD_STPAGELABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPAGELABEL,
                        CHILD_STPAGELABEL, CHILD_STPAGELABEL_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STCOMPANYNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STCOMPANYNAME,
                        CHILD_STCOMPANYNAME,
                        CHILD_STCOMPANYNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTODAYDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STTODAYDATE,
                        CHILD_STTODAYDATE, CHILD_STTODAYDATE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STUSERNAMETITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STUSERNAMETITLE,
                        CHILD_STUSERNAMETITLE,
                        CHILD_STUSERNAMETITLE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_DETECTALERTTASKS))
        {
            HiddenField child =
                new HiddenField(this, getDefaultModel(), CHILD_DETECTALERTTASKS,
                        CHILD_DETECTALERTTASKS,
                        CHILD_DETECTALERTTASKS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_SESSIONUSERID))
        {
            HiddenField child =
                new HiddenField(this, getDefaultModel(), CHILD_SESSIONUSERID,
                        CHILD_SESSIONUSERID, CHILD_SESSIONUSERID_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_TBDEALID))
        {
            TextField child =
                new TextField(this, getDefaultModel(), CHILD_TBDEALID, CHILD_TBDEALID,
                        CHILD_TBDEALID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CBPAGENAMES))
        {
            ComboBox child =
                new ComboBox(this, getDefaultModel(), CHILD_CBPAGENAMES,
                        CHILD_CBPAGENAMES, CHILD_CBPAGENAMES_RESET_VALUE, null);

            //--Release2.1--//
            //Modified to set NonSelected Label from
            // CHILD_CBPAGENAMES_NONSELECTED_LABEL
            //--> By Billy 05Nov2002
            //child.setLabelForNoneSelected("Choose a Page");
            child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

            //==========================================================================
            child.setOptions(cbPageNamesOptions);

            return child;
        }
        else if (name.equals(CHILD_BTPROCEED))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPROCEED, CHILD_BTPROCEED,
                        CHILD_BTPROCEED_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF1))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF1, CHILD_HREF1,
                        CHILD_HREF1_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF2))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF2, CHILD_HREF2,
                        CHILD_HREF2_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF3))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF3, CHILD_HREF3,
                        CHILD_HREF3_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF4))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF4, CHILD_HREF4,
                        CHILD_HREF4_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF5))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF5, CHILD_HREF5,
                        CHILD_HREF5_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF6))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF6, CHILD_HREF6,
                        CHILD_HREF6_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF7))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF7, CHILD_HREF7,
                        CHILD_HREF7_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF8))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF8, CHILD_HREF8,
                        CHILD_HREF8_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF9))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF9, CHILD_HREF9,
                        CHILD_HREF9_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HREF10))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_HREF10, CHILD_HREF10,
                        CHILD_HREF10_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_CHANGEPASSWORDHREF))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_CHANGEPASSWORDHREF,
                        CHILD_CHANGEPASSWORDHREF,
                        CHILD_CHANGEPASSWORDHREF_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTWORKQUEUELINK))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTWORKQUEUELINK,
                        CHILD_BTWORKQUEUELINK, CHILD_BTWORKQUEUELINK_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_BTTOOLHISTORY))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTTOOLHISTORY,
                        CHILD_BTTOOLHISTORY, CHILD_BTTOOLHISTORY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTTOONOTES))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTTOONOTES, CHILD_BTTOONOTES,
                        CHILD_BTTOONOTES_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTTOOLSEARCH))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTTOOLSEARCH,
                        CHILD_BTTOOLSEARCH, CHILD_BTTOOLSEARCH_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTTOOLLOG))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTTOOLLOG, CHILD_BTTOOLLOG,
                        CHILD_BTTOOLLOG_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTPREVTASKPAGE))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPREVTASKPAGE,
                        CHILD_BTPREVTASKPAGE, CHILD_BTPREVTASKPAGE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTNEXTTASKPAGE))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTNEXTTASKPAGE,
                        CHILD_BTNEXTTASKPAGE, CHILD_BTNEXTTASKPAGE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTPRINTDEALSUMMARY))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPRINTDEALSUMMARY,
                        CHILD_BTPRINTDEALSUMMARY,
                        CHILD_BTPRINTDEALSUMMARY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTPRINTDEALSUMMARYLITE))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPRINTDEALSUMMARYLITE,
                        CHILD_BTPRINTDEALSUMMARYLITE,
                        CHILD_BTPRINTDEALSUMMARYLITE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPREVTASKPAGELABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPREVTASKPAGELABEL,
                        CHILD_STPREVTASKPAGELABEL,
                        CHILD_STPREVTASKPAGELABEL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTPARTYSUMMARY))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTPARTYSUMMARY,
                        CHILD_BTPARTYSUMMARY, CHILD_BTPARTYSUMMARY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTDECLINELETTER))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTDECLINELETTER,
                        CHILD_BTDECLINELETTER, CHILD_BTDECLINELETTER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDDOWNPAYMENTS))
        {
            pgDealSummaryRepeatedDownPaymentsTiledView child =
                new pgDealSummaryRepeatedDownPaymentsTiledView(this,
                        CHILD_REPEATEDDOWNPAYMENTS);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDESCROWPAYMENTS))
        {
            pgDealSummaryRepeatedEscrowPaymentsTiledView child =
                new pgDealSummaryRepeatedEscrowPaymentsTiledView(this,
                        CHILD_REPEATEDESCROWPAYMENTS);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDBORROWERDETAILS))
        {
            pgDealSummaryRepeatedBorrowerDetailsTiledView child =
                new pgDealSummaryRepeatedBorrowerDetailsTiledView(this,
                        CHILD_REPEATEDBORROWERDETAILS);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDGDS))
        {
            pgDealSummaryRepeatedGDSTiledView child =
                new pgDealSummaryRepeatedGDSTiledView(this, CHILD_REPEATEDGDS);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDTDS))
        {
            pgDealSummaryRepeatedTDSTiledView child =
                new pgDealSummaryRepeatedTDSTiledView(this, CHILD_REPEATEDTDS);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDINCOMEDETAILS))
        {
            pgDealSummaryRepeatedIncomeDetailsTiledView child =
                new pgDealSummaryRepeatedIncomeDetailsTiledView(this,
                        CHILD_REPEATEDINCOMEDETAILS);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDLIABDETAILS))
        {
            pgDealSummaryRepeatedLiabDetailsTiledView child =
                new pgDealSummaryRepeatedLiabDetailsTiledView(this,
                        CHILD_REPEATEDLIABDETAILS);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDASSETDETAILS))
        {
            pgDealSummaryRepeatedAssetDetailsTiledView child =
                new pgDealSummaryRepeatedAssetDetailsTiledView(this,
                        CHILD_REPEATEDASSETDETAILS);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDPROPERTYDETAILS))
        {
            pgDealSummaryRepeatedPropertyDetailsTiledView child =
                new pgDealSummaryRepeatedPropertyDetailsTiledView(this,
                        CHILD_REPEATEDPROPERTYDETAILS);

            return child;
        }

        //--DJ_LDI_CR--start--//
        else if (name.equals(CHILD_REPEATEDLIFEDISABILITYINSURANCE))
        {
            pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView child =
                new pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView(this,
                        CHILD_REPEATEDLIFEDISABILITYINSURANCE);
            return child;
        }
        else if (name.equals(CHILD_STINCLUDELIFEDISSECTIONSTART))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDELIFEDISSECTIONSTART,
                        CHILD_STINCLUDELIFEDISSECTIONSTART,
                        CHILD_STINCLUDELIFEDISSECTIONSTART_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDELIFEDISSECTIONEND))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDELIFEDISSECTIONEND,
                        CHILD_STINCLUDELIFEDISSECTIONEND,
                        CHILD_STINCLUDELIFEDISSECTIONEND_RESET_VALUE, null);

            return child;
        }

        //--DJ_LDI_CR--end--//
        else if (name.equals(CHILD_STLASTNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STLASTNAME,
                        doMainViewBorrowerModel.FIELD_DFLASTNAME,
                        CHILD_STLASTNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSUFFIX))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STSUFFIX,
                        doMainViewBorrowerModel.FIELD_DFSUFFIX,
                        CHILD_STSUFFIX_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIDDLEINITIAL))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STMIDDLEINITIAL,
                        doMainViewBorrowerModel.FIELD_DFMIDDLEINITIAL,
                        CHILD_STMIDDLEINITIAL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STFIRSTNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STFIRSTNAME,
                        doMainViewBorrowerModel.FIELD_DFFIRSTNAME,
                        CHILD_STFIRSTNAME_RESET_VALUE, null);

            return child;
        }
        // ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
        else if (name.equals(CHILD_STCASHBACKINDOLLARS)) {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewDealModel(),
                    // CHILD_STCASHBACKINDOLLARS,
                    CHILD_STCASHBACKINDOLLARS,
                    doMainViewDealModel.FIELD_DFCASHBACKINDOLLARS,
                    CHILD_STCASHBACKINDOLLARS_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCASHBACKINPERCENTAGE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewDealModel(), CHILD_STCASHBACKINPERCENTAGE,
                    // CHILD_STCASHBACKINPERCENTAGE ,
                    doMainViewDealModel.FIELD_DFCASHBACKINPERCENTAGE,
                    CHILD_STCASHBACKINPERCENTAGE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STAFFILIATIONPROGRAM)) {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewDealModel(), CHILD_STAFFILIATIONPROGRAM,
                    doMainViewDealModel.FIELD_DFAFFILIATIONPROGRAM,
                    CHILD_STAFFILIATIONPROGRAM_RESET_VALUE, null);

            return child;
        } 

        // ***** Change by NBC Impl. Team - Version 1.1 - End*****//
        else if (name.equals(CHILD_STLANGUAGEPREFERENCE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STLANGUAGEPREFERENCE,
                        doMainViewBorrowerModel.FIELD_DFLANGUAGEPREFERENCE,
                        CHILD_STLANGUAGEPREFERENCE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STEXISTINGCLIENT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STEXISTINGCLIENT,
                        doMainViewBorrowerModel.FIELD_DFEXISITINGCLEINT,
                        CHILD_STEXISTINGCLIENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFERENCECLIENTNUMBER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STREFERENCECLIENTNUMBER,
                        doMainViewBorrowerModel.FIELD_DFREFCLEINTNO,
                        CHILD_STREFERENCECLIENTNUMBER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STUNDERWRITER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STUNDERWRITER,
                        doMainViewDealModel.FIELD_DFUNDERWRITERUSERID,
                        CHILD_STUNDERWRITER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STADMINISTRATOR))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STADMINISTRATOR,
                        doMainViewDealModel.FIELD_DFADMINISTRATORID,
                        CHILD_STADMINISTRATOR_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STFUNDER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STFUNDER,
                        doMainViewDealModel.FIELD_DFFUNDERID,
                        CHILD_STFUNDER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STHOMEPHONE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STHOMEPHONE,
                        doMainViewBorrowerModel.FIELD_DFHOMEPHONENUMBER,
                        CHILD_STHOMEPHONE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STWORKPHONE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STWORKPHONE,
                        doMainViewBorrowerModel.FIELD_DFWORKPHONENUMBER,
                        CHILD_STWORKPHONE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STNOOFBORROWERS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STNOOFBORROWERS,
                        doMainViewDealModel.FIELD_DFNOOFBORROWERS,
                        CHILD_STNOOFBORROWERS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STNOOFGUARANTORS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STNOOFGUARANTORS,
                        doMainViewDealModel.FIELD_DFNOGURANTORS,
                        CHILD_STNOOFGUARANTORS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRANCH))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STBRANCH,
                        CHILD_STBRANCH, CHILD_STBRANCH_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDITOTALLOANAMOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STDITOTALLOANAMOUNT,
                        doMainViewDealModel.FIELD_DFTOTALAMOUNT,
                        CHILD_STDITOTALLOANAMOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPAYMENTTERM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STPAYMENTTERM,
                        doMainViewDealModel.FIELD_DFACTUALPAYMENTTERM,
                        CHILD_STPAYMENTTERM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPAYMENTTERMDESC))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STPAYMENTTERMDESC,
                        doMainViewDealModel.FIELD_DFPAYMENTTERMTYPEDESC,
                        CHILD_STPAYMENTTERMDESC_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STEFFECTIVEAMORTIZATION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STEFFECTIVEAMORTIZATION,
                        doMainViewDealModel.FIELD_DFEFFECTIVEAMORTIZATION,
                        CHILD_STEFFECTIVEAMORTIZATION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPAYMENTFREQUENCY))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STPAYMENTFREQUENCY,
                        doMainViewDealModel.FIELD_DFPAYMENTFREQUENCY,
                        CHILD_STPAYMENTFREQUENCY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDLTV))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDLTV,
                        doMainViewDealModel.FIELD_DFCOMBINEDLTV,
                        CHILD_STCOMBINEDLTV_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STRATECODE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STRATECODE,
                        CHILD_STRATECODE, CHILD_STRATECODE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STRATEDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STRATEDATE,
                        doMainViewDealModel.FIELD_DFRATEDATE,
                        CHILD_STRATEDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STFINANCINGDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STFINANCINGDATE,
                        doMainViewDealModel.FIELD_DFFINANCINGDATE,
                        CHILD_STFINANCINGDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPOSTEDRATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STPOSTEDRATE,
                        doMainViewDealModel.FIELD_DFPOSTEDRATE,
                        CHILD_STPOSTEDRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STNETRATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STNETRATE,
                        doMainViewDealModel.FIELD_DFNETRATE,
                        CHILD_STNETRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STRATELOCKEDIN))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STRATELOCKEDIN,
                        doMainViewDealModel.FIELD_DFRATELOCK,
                        CHILD_STRATELOCKEDIN_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDISCOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STDISCOUNT,
                        doMainViewDealModel.FIELD_DFDISCOUNT,
                        CHILD_STDISCOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPREMIUM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STPREMIUM,
                        doMainViewDealModel.FIELD_DFPREMIUM,
                        CHILD_STPREMIUM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBUYDOWNRATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STBUYDOWNRATE,
                        doMainViewDealModel.FIELD_DFBUYDOWNRATE,
                        CHILD_STBUYDOWNRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDISPECIALFEATURE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STDISPECIALFEATURE,
                        doMainViewDealModel.FIELD_DFSPECIALFEATURE,
                        CHILD_STDISPECIALFEATURE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPREAPPPRICEPURCHASE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STPREAPPPRICEPURCHASE,
                        doMainViewDealModel.FIELD_DFPREAPPROVALPURCHASEPRICE,
                        CHILD_STPREAPPPRICEPURCHASE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPIPAYMENT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STPIPAYMENT,
                        doMainViewDealModel.FIELD_DFPIPAYMENT,
                        CHILD_STPIPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STADDITIONALPRINCIPALPAYEMT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STADDITIONALPRINCIPALPAYEMT,
                        doMainViewDealModel.FIELD_DFADDITIONALPRICIPALAMOUNT,
                        CHILD_STADDITIONALPRINCIPALPAYEMT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALESCROWPAYMENT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STTOTALESCROWPAYMENT,
                        doMainViewDealModel.FIELD_DFTOTALESCROWPAYMENT,
                        CHILD_STTOTALESCROWPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALPAYMENT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STTOTALPAYMENT,
                        doMainViewDealModel.FIELD_DFTOTALPAYMENT,
                        CHILD_STTOTALPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STFIRSTPAYMENTDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STFIRSTPAYMENTDATE,
                        doMainViewDealModel.FIELD_DFFIRSTPAYMENTDATE,
                        CHILD_STFIRSTPAYMENTDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMATURITYDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STMATURITYDATE,
                        doMainViewDealModel.FIELD_DFMATURITYDATE,
                        CHILD_STMATURITYDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSECONDMORTGAGE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STSECONDMORTGAGE,
                        doMainViewDealModel.FIELD_SFSECONDMORGAGE,
                        CHILD_STSECONDMORTGAGE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STEQUITYAVAILABLE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STEQUITYAVAILABLE,
                        doMainViewDealModel.FIELD_DFEQUITYAVAILABLE,
                        CHILD_STEQUITYAVAILABLE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDIDEALPURPOSE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STDIDEALPURPOSE,
                        doMainViewDealModel.FIELD_DFDEALPURPOSE,
                        CHILD_STDIDEALPURPOSE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREQUIREDDOWNPAYMENT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREQUIREDDOWNPAYMENT,
                        doMainViewDealModel.FIELD_DFREQUIREDDOWNPAYMENT,
                        CHILD_STREQUIREDDOWNPAYMENT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDILOB))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STDILOB,
                        doMainViewDealModel.FIELD_DFLOB,
                        CHILD_STDILOB_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDIDEALTYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STDIDEALTYPE,
                        doMainViewDealModel.FIELD_DFDEALTYPE,
                        CHILD_STDIDEALTYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPRODUCT))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPRODUCT,
                        CHILD_STPRODUCT, CHILD_STPRODUCT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPREPAYMENTOPTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STPREPAYMENTOPTION,
                        doMainViewDealModel.FIELD_DFPREPAYMENTOPTION,
                        CHILD_STPREPAYMENTOPTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFERENCETYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STREFERENCETYPE,
                        CHILD_STREFERENCETYPE,
                        CHILD_STREFERENCETYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFERENCENO))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFERENCENO,
                        doMainViewDealModel.FIELD_DFREFERENCENO,
                        CHILD_STREFERENCENO_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STESTIMATEDCLOSINGDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STESTIMATEDCLOSINGDATE,
                        doMainViewDealModel.FIELD_DFESTCLOSINGDATE,
                        CHILD_STESTIMATEDCLOSINGDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STACTUALCLOSINGDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STACTUALCLOSINGDATE,
                        doMainViewDealModel.FIELD_DFACTUALCLOSINGDATE,
                        CHILD_STACTUALCLOSINGDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPRIVILEGEPAYMENTOPTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STPRIVILEGEPAYMENTOPTION,
                        doMainViewDealModel.FIELD_DFPRIVILEGEPAYMENTOPTIONS,
                        CHILD_STPRIVILEGEPAYMENTOPTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDEFINANCINGPROGRAMSTART))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDEFINANCINGPROGRAMSTART,
                        CHILD_STINCLUDEFINANCINGPROGRAMSTART,
                        CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STFINANCINGPROGRAM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STFINANCINGPROGRAM,
                        doMainViewDealModel.FIELD_DFFINANCINGPROGRAM,
                        CHILD_STFINANCINGPROGRAM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDEFINANCINGPROGRAMEND))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDEFINANCINGPROGRAMEND,
                        CHILD_STINCLUDEFINANCINGPROGRAMEND,
                        CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE, null);

            return child;
        }
/*        else if (name.equals(CHILD_STDISOURCE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STDISOURCE,
                        doMainViewDealModel.FIELD_DFSOURCEFULLNAME,
                        CHILD_STDISOURCE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDISOURCEFIRM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STDISOURCEFIRM,
                        doMainViewDealModel.FIELD_DFSOURCEFIRMNAME,
                        CHILD_STDISOURCEFIRM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDISOURCEADDRESS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STDISOURCEADDRESS,
                        doMainViewDealModel.FIELD_DFSOURCEFULLADDRESS,
                        CHILD_STDISOURCEADDRESS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDISOURCECONTACTPHONE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STDISOURCECONTACTPHONE,
                        doMainViewDealModel.FIELD_DFSOURCECONTACTPHONENUM,
                        CHILD_STDISOURCECONTACTPHONE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDISOURCECONTACTPHONEEXT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STDISOURCECONTACTPHONEEXT,
                        doMainViewDealModel.FIELD_DFSOURCECONTACTPHONEEXT,
                        CHILD_STDISOURCECONTACTPHONEEXT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDISOURCECONTACTFAX))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STDISOURCECONTACTFAX,
                        doMainViewDealModel.FIELD_DFSOURCECONTACTFAXNUM,
                        CHILD_STDISOURCECONTACTFAX_RESET_VALUE, null);

            return child;
        }*/
        //4.4 Submission Agent
        else if (name.equals(CHILD_REPEATEDSOB))
        {
            pgDealSummaryRepeatedSOBTiledView child =
                new pgDealSummaryRepeatedSOBTiledView(this,
                		CHILD_REPEATEDSOB);

            return child;
        }
        else if (name.equals(CHILD_STREFERENCESOURCEAPP))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFERENCESOURCEAPP,
                        doMainViewDealModel.FIELD_DFREFSOURCEAPPNO,
                        CHILD_STREFERENCESOURCEAPP_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCROSSSELL))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STCROSSSELL,
                        doMainViewDealModel.FIELD_DFCROSSSELL,
                        CHILD_STCROSSSELL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALPURCHASEPRICE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STTOTALPURCHASEPRICE,
                        doMainViewDealModel.FIELD_DFTOTALPURCHASEPRICE,
                        CHILD_STTOTALPURCHASEPRICE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALESTIMATEDVALUE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STTOTALESTIMATEDVALUE,
                        doMainViewDealModel.FIELD_DFTOTALESTIMATEDVALUE,
                        CHILD_STTOTALESTIMATEDVALUE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALACTUALAPPRAISEDVALUE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STTOTALACTUALAPPRAISEDVALUE,
                        doMainViewDealModel.FIELD_DFACTUALAPPRAISEDVALUE,
                        CHILD_STTOTALACTUALAPPRAISEDVALUE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STLENDER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STLENDER,
                        doMainViewDealModel.FIELD_DFLENDER,
                        CHILD_STLENDER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINVESTOR))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(), CHILD_STINVESTOR,
                        doMainViewDealModel.FIELD_DFINVESTOR,
                        CHILD_STINVESTOR_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGEPURPOSE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGEPURPOSE,
                        doMainViewBridgeModel.FIELD_DFBRIDGEPURPOSE,
                        CHILD_STBRIDGEPURPOSE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGELENDER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGELENDER,
                        doMainViewBridgeModel.FIELD_DFBRIDGEPURPOSE,
                        CHILD_STBRIDGELENDER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGEPRODUCT))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STBRIDGEPRODUCT,
                        CHILD_STBRIDGEPRODUCT,
                        CHILD_STBRIDGEPRODUCT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGEPAYMENTTERMDESC))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGEPAYMENTTERMDESC,
                        doMainViewBridgeModel.FIELD_DFPAYMENTTERM,
                        CHILD_STBRIDGEPAYMENTTERMDESC_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGERATECODE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STBRIDGERATECODE,
                        CHILD_STBRIDGERATECODE,
                        CHILD_STBRIDGERATECODE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGEPOSTEDINTERESTRATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGEPOSTEDINTERESTRATE,
                        doMainViewBridgeModel.FIELD_DFPOSTEDINTERESTRATE,
                        CHILD_STBRIDGEPOSTEDINTERESTRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGERATEDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGERATEDATE,
                        doMainViewBridgeModel.FIELD_DFRATEDATE,
                        CHILD_STBRIDGERATEDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGEDISCOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGEDISCOUNT,
                        doMainViewBridgeModel.FIELD_DFDISCOUNT,
                        CHILD_STBRIDGEDISCOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGEPREMIUM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGEPREMIUM,
                        doMainViewBridgeModel.FIELD_DFBRIDGEPREMIUM,
                        CHILD_STBRIDGEPREMIUM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGENETRATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGENETRATE,
                        doMainViewBridgeModel.FIELD_DFNETRATE,
                        CHILD_STBRIDGENETRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGELOANAMOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGELOANAMOUNT,
                        doMainViewBridgeModel.FIELD_DFBRIDGELOANAMOUNT,
                        CHILD_STBRIDGELOANAMOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGEACTUALPAYMENTTERM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGEACTUALPAYMENTTERM,
                        doMainViewBridgeModel.FIELD_DFACTUALPAYMENTTERM,
                        CHILD_STBRIDGEACTUALPAYMENTTERM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBRIDGEMATURITYDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBridgeModel(),
                        CHILD_STBRIDGEMATURITYDATE,
                        doMainViewBridgeModel.FIELD_DFMATURITYDATE,
                        CHILD_STBRIDGEMATURITYDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBEGINHIDEREFISECTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STBEGINHIDEREFISECTION,
                        doMainViewDealModel.FIELD_DFISREFINANCE,
                        CHILD_STBEGINHIDEREFISECTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIPURCHASEDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIPURCHASEDATE,
                        doMainViewDealModel.FIELD_DFREFIORIGPURCHASEDATE,
                        CHILD_STREFIPURCHASEDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIMORTGAGEHOLDER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIMORTGAGEHOLDER,
                        doMainViewDealModel.FIELD_DFREFICURMORTGAGEHOLDER,
                        CHILD_STREFIMORTGAGEHOLDER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIORIGPURCHASEPRICE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIORIGPURCHASEPRICE,
                        doMainViewDealModel.FIELD_DFREFIORIGPURCHASEPRICE,
                        CHILD_STREFIORIGPURCHASEPRICE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIIMPROVEMENTVALUE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIIMPROVEMENTVALUE,
                        doMainViewDealModel.FIELD_DFREFIIMPROVEMENTAMOUNT,
                        CHILD_STREFIIMPROVEMENTVALUE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIBLENDEDAMORIZATION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIBLENDEDAMORIZATION,
                        doMainViewDealModel.FIELD_DFREFIBLENDEDAMORIZATION,
                        CHILD_STREFIBLENDEDAMORIZATION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIPURPOSE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIPURPOSE,
                        doMainViewDealModel.FIELD_DFREFIPURPOSE,
                        CHILD_STREFIPURPOSE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIORIGMTGAMOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIORIGMTGAMOUNT,
                        doMainViewDealModel.FIELD_DFREFIORIGMTGAMOUNT,
                        CHILD_STREFIORIGMTGAMOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIOUTSTANDINGMTGAMOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIOUTSTANDINGMTGAMOUNT,
                        doMainViewDealModel.FIELD_DFREFIOUTSTANDINGMTGAMT,
                        CHILD_STREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STREFIIMPROVEMENTDESC))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STREFIIMPROVEMENTDESC,
                        doMainViewDealModel.FIELD_DFREFIIMPROVEMENTDESC,
                        CHILD_STREFIIMPROVEMENTDESC_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STENDHIDEREFISECTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STENDHIDEREFISECTION,
                        doMainViewDealModel.FIELD_DFISREFINANCE,
                        CHILD_STENDHIDEREFISECTION_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STPROGRESSADVANCE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STPROGRESSADVANCE,
                        doMainViewDealModel.FIELD_DFPROGRESSADVANCE,
                        CHILD_STPROGRESSADVANCE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STADVANCETODATEAMT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STADVANCETODATEAMT,
                        doMainViewDealModel.FIELD_DFADVANCETODATEAMOUNT,
                        CHILD_STADVANCETODATEAMT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STNEXTADVANCEAMT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STNEXTADVANCEAMT,
                        doMainViewDealModel.FIELD_DFNEXTADVANCEAMOUNT,
                        CHILD_STNEXTADVANCEAMT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STADVANCENUMBER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STADVANCENUMBER,
                        doMainViewDealModel.FIELD_DFADVANCENUMBER,
                        CHILD_STADVANCENUMBER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCREDITSCORE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STCREDITSCORE,
                        CHILD_STCREDITSCORE,
                        CHILD_STCREDITSCORE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDGDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDGDS,
                        doMainViewDealModel.FIELD_DFCOMBINEDGDS,
                        CHILD_STCOMBINEDGDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINED3YRGDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINED3YRGDS,
                        doMainViewDealModel.FIELD_DFCOMBINED3YRGDS,
                        CHILD_STCOMBINED3YRGDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDTDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDTDS,
                        doMainViewDealModel.FIELD_DFCOMBINEDTDS,
                        CHILD_STCOMBINEDTDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINED3YRTDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINED3YRTDS,
                        doMainViewDealModel.FIELD_DFCOMBINED3YRTDS,
                        CHILD_STCOMBINED3YRTDS_RESET_VALUE, null);

            return child;
        }
        // ***** Qualify Rate ***** //
        else if (name.equals(CHILD_STQUALIFYPRODUCT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STQUALIFYPRODUCT,
                        doMainViewDealModel.FIELD_DFOVERRIDEQUALPROD,
                        CHILD_STQUALIFYPRODUCT_RESET_VALUE, null);

            return child;
        }        
        else if (name.equals(CHILD_STQUALIFYRATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STQUALIFYRATE,
                        doMainViewDealModel.FIELD_DFQUALIFYRATE,
                        CHILD_STQUALIFYRATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDQUALIFYRATEOVERRIDE))
        {
        	HiddenField child =
                new HiddenField(this, getdoMainViewDealModel(),
                		CHILD_HDQUALIFYRATEOVERRIDE,
                        doMainViewDealModel.FIELD_DFQUALIFYINGOVERRIDEFLAG,
                        CHILD_HDQUALIFYRATEOVERRIDE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDQUALIFYRATEOVERRIDERATE))
        {
        	HiddenField child =
                new HiddenField(this, getdoMainViewDealModel(),
                		CHILD_HDQUALIFYRATEOVERRIDERATE,
                        doMainViewDealModel.FIELD_DFQUALIFYINGOVERRIDERATEFLAG,
                        CHILD_HDQUALIFYRATEOVERRIDERATE, null);

            return child;
        }
        // ***** Qualify Rate ***** //   
        else if (name.equals(CHILD_STCOMBINEDTOTALINCOME))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDTOTALINCOME,
                        doMainViewDealModel.FIELD_DFCOMBINEDTOTALINCOME,
                        CHILD_STCOMBINEDTOTALINCOME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDTOTALLIABILITIES))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDTOTALLIABILITIES,
                        doMainViewDealModel.FIELD_DFCOMBINEDTOTALLIABILITIES,
                        CHILD_STCOMBINEDTOTALLIABILITIES_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDTOTALASSETS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDTOTALASSETS,
                        doMainViewDealModel.FIELD_DFCOMBINEDTOTALASSETS,
                        CHILD_STCOMBINEDTOTALASSETS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYSTREETNO))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYSTREETNO,
                        doMainViewPrimaryPropertyModel.FIELD_DFSTREETNUMBER,
                        CHILD_STPROPERTYSTREETNO_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYSTREETNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYSTREETNAME,
                        doMainViewPrimaryPropertyModel.FIELD_DFSTREETNAME,
                        CHILD_STPROPERTYSTREETNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYSTREETTYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYSTREETTYPE,
                        doMainViewPrimaryPropertyModel.FIELD_DFSTREETTYPEDESC,
                        CHILD_STPROPERTYSTREETTYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYSTREETDIRECTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYSTREETDIRECTION,
                        doMainViewPrimaryPropertyModel.FIELD_DFSTREETDIRECTIONDESC,
                        CHILD_STPROPERTYSTREETDIRECTION_RESET_VALUE, null);

            return child;
        }else if (name.equals(CHILD_STPROPERTYUNITNUMBER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                		CHILD_STPROPERTYUNITNUMBER,
                        doMainViewPrimaryPropertyModel.FIELD_DFUNITNUMBER,
                        CHILD_STPROPERTYUNITNUMBER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYADDRESS2))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYADDRESS2,
                        doMainViewPrimaryPropertyModel.FIELD_DFADDRESSLINE2,
                        CHILD_STPROPERTYADDRESS2_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYCITY))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYCITY,
                        doMainViewPrimaryPropertyModel.FIELD_DFCITY,
                        CHILD_STPROPERTYCITY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYPROVINCE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYPROVINCE,
                        doMainViewPrimaryPropertyModel.FIELD_DFPROVINCE,
                        CHILD_STPROPERTYPROVINCE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYPOSTALCODE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYPOSTALCODE,
                        doMainViewPrimaryPropertyModel.FIELD_DFPOSTALCODE1,
                        CHILD_STPROPERTYPOSTALCODE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYPOSTALCODE1))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYPOSTALCODE1,
                        doMainViewPrimaryPropertyModel.FIELD_DFPOSTALCODE2,
                        CHILD_STPROPERTYPOSTALCODE1_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYUSAGE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYUSAGE,
                        doMainViewPrimaryPropertyModel.FIELD_DFPORPERTYUSAGE,
                        CHILD_STPROPERTYUSAGE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYOCCUPANCY))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYOCCUPANCY,
                        doMainViewPrimaryPropertyModel.FIELD_DFOCCUPANCY,
                        CHILD_STPROPERTYOCCUPANCY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYTYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYTYPE,
                        doMainViewPrimaryPropertyModel.FIELD_PROPERTYTYPE,
                        CHILD_STPROPERTYTYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STNOOFPROPERTIES))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STNOOFPROPERTIES,
                        doMainViewDealModel.FIELD_DFNOOFPROPERTIES,
                        CHILD_STNOOFPROPERTIES_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYPURCHASEPRICE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYPURCHASEPRICE,
                        doMainViewPrimaryPropertyModel.FIELD_DFPURCHASEPRICE,
                        CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYESTIMATEDAPPRAISALVALUE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYESTIMATEDAPPRAISALVALUE,
                        doMainViewPrimaryPropertyModel.FIELD_DFESTIMATEDAPPRAISALVALUE,
                        CHILD_STPROPERTYESTIMATEDAPPRAISALVALUE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYACTUALAPRAISEDVALUE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYACTUALAPRAISEDVALUE,
                        doMainViewPrimaryPropertyModel.FIELD_DFACTUALAPPRAISALVALUE,
                        CHILD_STPROPERTYACTUALAPRAISEDVALUE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPROPERTYLANDVALUE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewPrimaryPropertyModel(),
                        CHILD_STPROPERTYLANDVALUE,
                        doMainViewPrimaryPropertyModel.FIELD_DFLANDVALUE,
                        CHILD_STPROPERTYLANDVALUE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDTOTALPROPERTYEXP))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDTOTALPROPERTYEXP,
                        doMainViewDealModel.FIELD_DFCOMBINEDTOTALPROPERTYEXPENSES,
                        CHILD_STCOMBINEDTOTALPROPERTYEXP_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIINDICTORDESC))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIINDICTORDESC,
                        doMainViewMortgageInsurerModel.FIELD_DFMIINDICATORDESC,
                        CHILD_STMIINDICTORDESC_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIREQUESTSTATUS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIREQUESTSTATUS,
                        doMainViewMortgageInsurerModel.FIELD_DFMIREQUESTSTATUS,
                        CHILD_STMIREQUESTSTATUS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIEXISTINGPOLICYNUM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIEXISTINGPOLICYNUM,
                        doMainViewMortgageInsurerModel.FIELD_DFMIEXISTINGPOLICYNUMBER,
                        CHILD_STMIEXISTINGPOLICYNUM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIPOLICENUMBER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIPOLICENUMBER,
                        doMainViewMortgageInsurerModel.FIELD_DFMICERTIFICATENO,
                        CHILD_STMIPOLICENUMBER_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIPREQUALIFICATIONMICERTNUM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIPREQUALIFICATIONMICERTNUM,
                        doMainViewMortgageInsurerModel.FIELD_DFPREQUALIFICATIONMICERTNUM,
                        CHILD_STMIPREQUALIFICATIONMICERTNUM_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STMITYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMITYPE,
                        doMainViewMortgageInsurerModel.FIELD_DFMITYPEDESC,
                        CHILD_STMITYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIINSURER))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIINSURER,
                        doMainViewMortgageInsurerModel.FIELD_DFMIINSURER,
                        CHILD_STMIINSURER_RESET_VALUE, null);

            return child;
        }
        // SEAN DJ SPEC-Progress Advance Type July 25, 2005: Create the child...
        else if (name.equals(CHILD_STPROGRESSADVANCETYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STPROGRESSADVANCETYPE,
                        doMainViewMortgageInsurerModel.FIELD_DFPROGRESSADVANCETYPE,
                        CHILD_STPROGRESSADVANCETYPE_RESET_VALUE, null);

            return child;
        }
        // SEAN DJ SPEC-PAT END
        else if (name.equals(CHILD_STMIPAYORDESC))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIPAYORDESC,
                        doMainViewMortgageInsurerModel.FIELD_DFMIPAYORDESC,
                        CHILD_STMIPAYORDESC_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIUPFRONT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIUPFRONT,
                        doMainViewMortgageInsurerModel.FIELD_DFMIUPFRONT,
                        CHILD_STMIUPFRONT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIPREMIUM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIPREMIUM,
                        doMainViewMortgageInsurerModel.FIELD_DFMIPREMIUMAMOUNT,
                        CHILD_STMIPREMIUM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STMIRUINTERVENTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewMortgageInsurerModel(),
                        CHILD_STMIRUINTERVENTION,
                        doMainViewMortgageInsurerModel.FIELD_DFMIRUINTERVENTION,
                        CHILD_STMIRUINTERVENTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STERRORFLAG))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STERRORFLAG,
                        CHILD_STERRORFLAG, CHILD_STERRORFLAG_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STDEALID))
        {
            StaticTextField child =
                new StaticTextField(this,
                        ////SYNCADD. There is no dataobject binding for this
                        //// field anymore.
                        getdoDealSummarySnapShotModel(), CHILD_STDEALID,
                        doDealSummarySnapShotModel.FIELD_DFAPPLICATIONID,

                        ////getDefaultModel(),
                        ////CHILD_STDEALSTATUS,
                        ////CHILD_STDEALSTATUS,
                        CHILD_STDEALID_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBORRFIRSTNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STBORRFIRSTNAME,
                        CHILD_STBORRFIRSTNAME,
                        CHILD_STBORRFIRSTNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALSTATUS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALSTATUS,
                        doDealSummarySnapShotModel.FIELD_DFSTATUSDESCR,
                        CHILD_STDEALSTATUS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALSTATUSDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALSTATUSDATE,
                        doDealSummarySnapShotModel.FIELD_DFSTATUSDATE,
                        CHILD_STDEALSTATUSDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCEFIRM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STSOURCEFIRM,
                        doDealSummarySnapShotModel.FIELD_DFSFSHORTNAME,
                        CHILD_STSOURCEFIRM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSOURCE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STSOURCE,
                        doDealSummarySnapShotModel.FIELD_DFSOBPSHORTNAME,
                        CHILD_STSOURCE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STESTCLOSINGDATE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STESTCLOSINGDATE,
                        doDealSummarySnapShotModel.FIELD_DFESTCLOSINGDATE,
                        CHILD_STESTCLOSINGDATE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STLOB))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(), CHILD_STLOB,
                        doDealSummarySnapShotModel.FIELD_DFLOBDESCR,
                        CHILD_STLOB_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSPECIALFEATURE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STSPECIALFEATURE,
                        doDealSummarySnapShotModel.FIELD_DFSPECIALFEATURE,
                        CHILD_STSPECIALFEATURE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALTYPE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALTYPE,
                        doDealSummarySnapShotModel.FIELD_DFDEALTYPEDESCR,
                        CHILD_STDEALTYPE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STDEALPURPOSE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STDEALPURPOSE,
                        doDealSummarySnapShotModel.FIELD_DFLOANPURPOSEDESCR,
                        CHILD_STDEALPURPOSE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPURCHASEPRICE))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STPURCHASEPRICE,
                        doDealSummarySnapShotModel.FIELD_DFTOTALPURCHASEPRICE,
                        CHILD_STPURCHASEPRICE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMTTERM))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STPMTTERM,
                        doDealSummarySnapShotModel.FIELD_DFPAYMENTTERMDESCR,
                        CHILD_STPMTTERM_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTOTALLOANAMOUNT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoDealSummarySnapShotModel(),
                        CHILD_STTOTALLOANAMOUNT,
                        doDealSummarySnapShotModel.FIELD_DFTOTALLOANAMT,
                        CHILD_STTOTALLOANAMOUNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STNEXTTASKPAGELABEL))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STNEXTTASKPAGELABEL,
                        CHILD_STNEXTTASKPAGELABEL,
                        CHILD_STNEXTTASKPAGELABEL_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STTASKNAME))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STTASKNAME,
                        CHILD_STTASKNAME, CHILD_STTASKNAME_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDBORROWERGDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDBORROWERGDS,
                        doMainViewDealModel.FIELD_DFCOMBINEDGDS,
                        CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STCOMBINEDBORROWERTDS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCOMBINEDBORROWERTDS,
                        doMainViewDealModel.FIELD_DFCOMBINEDTDS,
                        CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMGENERATE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMGENERATE,
                        CHILD_STPMGENERATE, CHILD_STPMGENERATE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMHASTITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMHASTITLE,
                        CHILD_STPMHASTITLE, CHILD_STPMHASTITLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMHASINFO))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMHASINFO,
                        CHILD_STPMHASINFO, CHILD_STPMHASINFO_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMHASTABLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMHASTABLE,
                        CHILD_STPMHASTABLE, CHILD_STPMHASTABLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMHASOK))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMHASOK,
                        CHILD_STPMHASOK, CHILD_STPMHASOK_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMTITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMTITLE,
                        CHILD_STPMTITLE, CHILD_STPMTITLE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMINFOMSG))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMINFOMSG,
                        CHILD_STPMINFOMSG, CHILD_STPMINFOMSG_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STPMONOK))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMONOK,
                        CHILD_STPMONOK, CHILD_STPMONOK_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMMSGS))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMMSGS,
                        CHILD_STPMMSGS, CHILD_STPMMSGS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPMMSGTYPES))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STPMMSGTYPES,
                        CHILD_STPMMSGTYPES, CHILD_STPMMSGTYPES_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMGENERATE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMGENERATE,
                        CHILD_STAMGENERATE, CHILD_STAMGENERATE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMHASTITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMHASTITLE,
                        CHILD_STAMHASTITLE, CHILD_STAMHASTITLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMHASINFO))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMHASINFO,
                        CHILD_STAMHASINFO, CHILD_STAMHASINFO_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMHASTABLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMHASTABLE,
                        CHILD_STAMHASTABLE, CHILD_STAMHASTABLE_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMTITLE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMTITLE,
                        CHILD_STAMTITLE, CHILD_STAMTITLE_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STAMINFOMSG))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMINFOMSG,
                        CHILD_STAMINFOMSG, CHILD_STAMINFOMSG_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMMSGS))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMMSGS,
                        CHILD_STAMMSGS, CHILD_STAMMSGS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STAMMSGTYPES))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMMSGTYPES,
                        CHILD_STAMMSGTYPES, CHILD_STAMMSGTYPES_RESET_VALUE,
                        null);

            return child;
        }
        else if (name.equals(CHILD_STAMDIALOGMSG))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMDIALOGMSG,
                        CHILD_STAMDIALOGMSG,
                        CHILD_STAMDIALOGMSG_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STAMBUTTONSHTML))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STAMBUTTONSHTML,
                        CHILD_STAMBUTTONSHTML,
                        CHILD_STAMBUTTONSHTML_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBEGINHIDEBRIDGESECTION))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STBEGINHIDEBRIDGESECTION,
                        CHILD_STBEGINHIDEBRIDGESECTION,
                        CHILD_STBEGINHIDEBRIDGESECTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STENDHIDEBRIDGESECTION))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STENDHIDEBRIDGESECTION,
                        CHILD_STENDHIDEBRIDGESECTION,
                        CHILD_STENDHIDEBRIDGESECTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STENDHIDEESCROWSECTION))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STENDHIDEESCROWSECTION,
                        CHILD_STENDHIDEESCROWSECTION,
                        CHILD_STENDHIDEESCROWSECTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STBEGINHIDEESCROWSECTION))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STBEGINHIDEESCROWSECTION,
                        CHILD_STBEGINHIDEESCROWSECTION,
                        CHILD_STBEGINHIDEESCROWSECTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_HDEXECUTEESCOW))
        {
            HiddenField child =
                new HiddenField(this, getdoMainViewEscrowModel(), CHILD_HDEXECUTEESCOW,
                        doMainViewEscrowModel.FIELD_DFDEALID,
                        CHILD_HDEXECUTEESCOW_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_REPEATEDPROPERTYADDRESSANDEXPENSES))
        {
            pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView child =
                new pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView(this,
                        CHILD_REPEATEDPROPERTYADDRESSANDEXPENSES);

            return child;
        }
        else if (name.equals(CHILD_STBORROWERWORKPHONEEXT))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewBorrowerModel(),
                        CHILD_STBORROWERWORKPHONEEXT,
                        doMainViewBorrowerModel.FIELD_DFWORKPHONEEXT,
                        CHILD_STBORROWERWORKPHONEEXT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_BTOKMAINPAGEBUTTON))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTOKMAINPAGEBUTTON,
                        CHILD_BTOKMAINPAGEBUTTON,
                        CHILD_BTOKMAINPAGEBUTTON_RESET_VALUE, null);

            return child;
        }

        //--> Hidden ActMessageButton (FINDTHISLATER)
        //--> by BILLY 15July2002
        else if (name.equals(CHILD_BTACTMSG))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTACTMSG, CHILD_BTACTMSG,
                        CHILD_BTACTMSG_RESET_VALUE,
                        new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));

            return child;
        }

        //// New link to toggle language. When touched this link should reload
        // the
        //// page in opposite language (french versus english) and set this new
        // session
        //// language id throughout the all modulus.
        else if (name.equals(CHILD_TOGGLELANGUAGEHREF))
        {
            HREF child =
                new HREF(this, getDefaultModel(), CHILD_TOGGLELANGUAGEHREF,
                        CHILD_TOGGLELANGUAGEHREF,
                        CHILD_TOGGLELANGUAGEHREF_RESET_VALUE, null);

            return child;
        }

        //--> New requirement : added new button for reverse funding
        //--> By Billy 11June2003
        else if (name.equals(CHILD_BTREVERSEFUNDING))
        {
            Button child =
                new Button(this, getDefaultModel(), CHILD_BTREVERSEFUNDING,
                        CHILD_BTREVERSEFUNDING, CHILD_BTREVERSEFUNDING_RESET_VALUE,
                        null);

            return child;
        }

        //--DJ_CR134--start--27May2004--//
        else if (name.equals(CHILD_STHOMEBASEPRODUCTRATEPMNT))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STHOMEBASEPRODUCTRATEPMNT,
                        CHILD_STHOMEBASEPRODUCTRATEPMNT,
                        CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDELIFEDISLABELSSTART))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDELIFEDISLABELSSTART,
                        CHILD_STINCLUDELIFEDISLABELSSTART,
                        CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STINCLUDELIFEDISLABELSEND))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STINCLUDELIFEDISLABELSEND,
                        CHILD_STINCLUDELIFEDISLABELSEND,
                        CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE, null);

            return child;
        }

        //--DJ_CR134--end--//
        //-- ========== DJ#725 Begins ========== --//
        //-- By Neil at Nov/29/2004
        else if (name.equals(CHILD_STCONTACTEMAILADDRESS))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STCONTACTEMAILADDRESS,
                        doMainViewDealModel.FIELD_DFCONTACTEMAILADDRESS,
                        CHILD_STCONTACTEMAILADDRESS_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STPSDESCRIPTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STPSDESCRIPTION,
                        doMainViewDealModel.FIELD_DFPSDESCRIPTION,
                        CHILD_STPSDESCRIPTION_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STSYSTEMTYPEDESCRIPTION))
        {
            StaticTextField child =
                new StaticTextField(this, getdoMainViewDealModel(),
                        CHILD_STSYSTEMTYPEDESCRIPTION,
                        doMainViewDealModel.FIELD_DFSYSTEMTYPEDESCRIPTION,
                        CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE, null);

            return child;
        }

        //-- ========== DJ#725 Ends ========== --//

        // #1676, Catherine, 2-Sep-05 ---- start ----------------------------
        else if (name.equals(CHILD_STHIDEREVERSEBTNSTART))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STHIDEREVERSEBTNSTART,
                        CHILD_STHIDEREVERSEBTNSTART,
                        CHILD_STHIDEREVERSEBTNSTART_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STHIDEREVERSEBTNEND))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_STHIDEREVERSEBTNEND,
                        CHILD_STHIDEREVERSEBTNEND,
                        CHILD_STHIDEREVERSEBTNEND_RESET_VALUE, null);

            return child;
        }  
        // #1676, Catherine, 2-Sep-05 ---- end ----------------------------
        // SEAN Ticket #1681 June 27, 2005: Hide print deal summary bt for DJ.
        else if (name.equals(CHILD_STBEGINHIDEPRINTDEALSUMMARY))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STBEGINHIDEPRINTDEALSUMMARY,
                        CHILD_STBEGINHIDEPRINTDEALSUMMARY,
                        CHILD_STBEGINHIDEPRINTDEALSUMMARY_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_STENDHIDEPRINTDEALSUMMARY))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(),
                        CHILD_STENDHIDEPRINTDEALSUMMARY,
                        CHILD_STENDHIDEPRINTDEALSUMMARY,
                        CHILD_STENDHIDEPRINTDEALSUMMARY_RESET_VALUE, null);

            return child;
        }
        // SEAN Ticket #1681 END

        //--Release3.1--begins
        //--by Hiro Mar 31, 2006
        else if (name.equals(CHILD_STPRODUCTTYPE))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewDealModel(), CHILD_STPRODUCTTYPE,
                    doMainViewDealModel.FIELD_DFPRODUCTTYPEID,
                    CHILD_STPRODUCTTYPE_RESET_VALUE, null);

            return child;
        } else if (name.equals(CHILD_STCHARGE))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewDealModel(), CHILD_STCHARGE,
                    doMainViewDealModel.FIELD_DFLIENPOSITIONID,
                    CHILD_STCHARGE_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STREFIPRODUCTTYPE))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewDealModel(), CHILD_STREFIPRODUCTTYPE,
                    doMainViewDealModel.FIELD_DFREFIPRODUCTTYPEID,
                    CHILD_STREFIPRODUCTTYPE_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STPROGRESSADVANCEINSPECTIONBY))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewMortgageInsurerModel(),
                    CHILD_STPROGRESSADVANCEINSPECTIONBY,
                    doMainViewMortgageInsurerModel.FIELD_DFPROGRESSADVANCEINSPECTIONBY,
                    CHILD_STPROGRESSADVANCEINSPECTIONBY_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STSELFDIRECTEDRRSP))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewMortgageInsurerModel(), CHILD_STSELFDIRECTEDRRSP,
                    doMainViewMortgageInsurerModel.FIELD_DFSELFDIRECTEDRRSP,
                    CHILD_STSELFDIRECTEDRRSP_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STCMHCPRODUCTTRACKERIDENTIFIER))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewMortgageInsurerModel(),
                    CHILD_STCMHCPRODUCTTRACKERIDENTIFIER,
                    doMainViewMortgageInsurerModel.FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER,
                    CHILD_STCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STLOCREPAYMENT))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewMortgageInsurerModel(), CHILD_STLOCREPAYMENT,
                    doMainViewMortgageInsurerModel.FIELD_DFLOCREPAYMENT,
                    CHILD_STLOCREPAYMENT_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STREQUESTSTANDARDSERVICE))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewMortgageInsurerModel(), CHILD_STREQUESTSTANDARDSERVICE,
                    doMainViewMortgageInsurerModel.FIELD_DFREQUESTSTANDARDSERVICE,
                    CHILD_STREQUESTSTANDARDSERVICE_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STLOCAMORTIZATION))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewMortgageInsurerModel(), CHILD_STLOCAMORTIZATION,
                    doMainViewMortgageInsurerModel.FIELD_DFLOCAMORTIZATION,
                    CHILD_STLOCAMORTIZATION_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_STLOCINTERESTONLYMATURITYDATE))
        {
            StaticTextField child = new StaticTextField(this,
                    getdoMainViewMortgageInsurerModel(),
                    CHILD_STLOCINTERESTONLYMATURITYDATE,
                    doMainViewMortgageInsurerModel.FIELD_DFLOCINTERESTONLYMATURITYDATE,
                    CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE, null);

            return child;
        }
        //--Release3.1--ends
        //PPI start
        else if (name.equals(CHILD_STIMPROVEDVALUE))
        {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), 
                        CHILD_STIMPROVEDVALUE,
                        CHILD_STIMPROVEDVALUE,
                        CHILD_STIMPROVEDVALUE_RESET_VALUE, null);

            return child;
        }

        else if (name.equals(CHILD_TXDEALPURPOSETYPEHIDDENSTART))
        {
            TextField child =
                new TextField(this, getDefaultModel(), 
                        CHILD_TXDEALPURPOSETYPEHIDDENSTART,
                        CHILD_TXDEALPURPOSETYPEHIDDENSTART,
                        CHILD_TXDEALPURPOSETYPEHIDDENSTART_RESET_VALUE, null);

            return child;
        }
        else if (name.equals(CHILD_TXDEALPURPOSETYPEHIDDENEND))
        {
            TextField child =
                new TextField(this, getDefaultModel(), 
                        CHILD_TXDEALPURPOSETYPEHIDDENEND,
                        CHILD_TXDEALPURPOSETYPEHIDDENEND,
                        CHILD_TXDEALPURPOSETYPEHIDDENEND_RESET_VALUE, null);

            return child;
        }
        //PPI end

        //  ***** Change by NBC/PP Implementation Team - GCD - Start *****//

        else if (name.equals(CHILD_TX_CREDIT_DECISION_STATUS)) {
            StaticTextField child =
                new StaticTextField(this, getdoAdjudicationResponseBNCModel(), CHILD_TX_CREDIT_DECISION_STATUS,
                        doAdjudicationResponseBNCModel.FIELD_DFREQUESTSTATUSDESC, CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE, null);
            return child;
        }

        else if (name.equals(CHILD_TX_CREDIT_DECISION)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(),CHILD_TX_CREDIT_DECISION,
                        doAdjudicationResponseBNCModel.FIELD_DFADJUDICATIONDESC,CHILD_TX_CREDIT_DECISION_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(), CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALCREDITBUREAUSCORE,CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_RISK_RATING)) {
            StaticTextField child =
                new StaticTextField(this, getdoAdjudicationResponseBNCModel(),CHILD_TX_GLOBAL_RISK_RATING,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALRISKRATING,CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TX_GLOBAL_INTERNAL_SCORE)) {
            StaticTextField child =
                new StaticTextField(this,getdoAdjudicationResponseBNCModel(), CHILD_TX_GLOBAL_INTERNAL_SCORE,
                        doAdjudicationResponseBNCModel.FIELD_DFGLOBALINTERNALSCORE, CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE, null);
            return child;
        }

        else if (name.equals(CHILD_BT_CREDIT_DECISION_PG)) {
            Button child =
                new Button (this,getDefaultModel(), CHILD_BT_CREDIT_DECISION_PG,
                        CHILD_BT_CREDIT_DECISION_PG,CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_ST_INCLUDE_GCDSUM_START)) {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_ST_INCLUDE_GCDSUM_START,
                        CHILD_ST_INCLUDE_GCDSUM_START,CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_ST_INCLUDE_GCDSUM_END)) {
            StaticTextField child =
                new StaticTextField(this, getDefaultModel(), CHILD_ST_INCLUDE_GCDSUM_END,
                        CHILD_ST_INCLUDE_GCDSUM_END,CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE, null);
            return child;
        }

        //***** Change by NBC/PP Implementation Team - GCD - End *****//

        /***************MCM Impl team changes starts - XS_16.9*******************/
        else 
            if (name.equals(CHILD_STQUALIFYINGDETAILSPAGELET))
            {
                pgQualifyingDetailsPageletView child = new pgQualifyingDetailsPageletView (this,CHILD_STQUALIFYINGDETAILSPAGELET,this.handler.cloneSS());
                return child;
            }
        /***************MCM Impl team changes ends - XS_16.9*******************/

        /***************MCM Impl team changes starts - XS_16.8 *******************/

            else if (name.equals(CHILD_STREFEXISTINGMTGNUMBER))
            {
                TextField child =
                    new TextField(this, getdoMainViewDealModel(),
                            CHILD_STREFEXISTINGMTGNUMBER,
                            doMainViewDealModel.FIELD_DFREFIEXISTINGMTGNUMBER,
                            CHILD_STREFEXISTINGMTGNUMBER_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_STREFIADDITIONALINFORMATION))
            {
                TextField child =
                    new TextField(this, getdoMainViewDealModel(),
                            CHILD_STREFIADDITIONALINFORMATION,
                            doMainViewDealModel.FIELD_DFREFIADDITIONALINFORMATION,
                            CHILD_STREFIADDITIONALINFORMATION_RESET_VALUE, null);

                return child;
            }

        /***************MCM Impl team changes ends - XS_16.8 *********************/

        /************************MCM Impl team changes starts - XS_16.7 ***************************************/
            else if (name.equals(CHILD_STTOTALAMOUNT)) {
                StaticTextField child = new StaticTextField(this,
                        getDoComponentSummaryModel(), CHILD_STTOTALAMOUNT,
                        doComponentSummaryModel.FIELD_DFTOTALAMOUNT,
                        CHILD_STTOTALAMOUNT_RESET_VALUE, null);

                return child;
            } 
            else if (name.equals(CHILD_STTOTALCASHBACKAMOUNT)) {
                StaticTextField child = new StaticTextField(this,
                        getDoComponentSummaryModel(), CHILD_STTOTALCASHBACKAMOUNT,
                        doComponentSummaryModel.FIELD_DFTOTALCASHBACKAMOUNT,
                        CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE, null);

                return child;
            } 
            else if (name.equals(CHILD_STUNALLOCATEDAMOUNT)) {
                StaticTextField child = new StaticTextField(this,
                        getDoComponentSummaryModel(), CHILD_STUNALLOCATEDAMOUNT,
                        doComponentSummaryModel.FIELD_DFUNALLOCATEDAMOUNT,
                        CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE, null);

                return child;
            } 
            else if (name.equals(CHILD_BTCOMPONENTDETAILS))
            {
                Button child =
                    new Button(this, getDefaultModel(), CHILD_BTCOMPONENTDETAILS,
                            CHILD_BTCOMPONENTDETAILS,
                            CHILD_BTCOMPONENTDETAILS_RESET_VALUE, null);

                return child;
            }
            else if (name.equals(CHILD_REPEATEDMORTGAGECOMPONENTS))
            {
                pgDealSummaryComponentMortgageTiledView child =
                    new pgDealSummaryComponentMortgageTiledView(this,
                            CHILD_REPEATEDMORTGAGECOMPONENTS);

                return child;
            }
            else if (name.equals(CHILD_REPEATEDLOCCOMPONENTS))
            {
                pgDealSummaryComponentLocTiledView child =
                    new pgDealSummaryComponentLocTiledView(this,
                            CHILD_REPEATEDLOCCOMPONENTS);

                return child;
            }
            else if (name.equals(CHILD_REPEATEDLOANCOMPONENTS))
            {
                pgDealSummaryComponentLoanTiledView child =
                    new pgDealSummaryComponentLoanTiledView(this,
                            CHILD_REPEATEDLOANCOMPONENTS);

                return child;
            }
            else if (name.equals(CHILD_REPEATEDCREDITCARDCOMPONENTS))
            {
                pgDealSummaryComponentCreditCardTiledView child =
                    new pgDealSummaryComponentCreditCardTiledView(this,
                            CHILD_REPEATEDCREDITCARDCOMPONENTS);

                return child;
            }
            else if (name.equals(CHILD_REPEATEDOVERDRAFTCOMPONENTS))
            {
                pgDealSummaryComponentOverDraftTiledView child =
                    new pgDealSummaryComponentOverDraftTiledView(this,
                            CHILD_REPEATEDOVERDRAFTCOMPONENTS);

                return child;
            }
            else if (name.equals(CHILD_STINCLUDE_COMPONENTDETAIL_START)) {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(), CHILD_STINCLUDE_COMPONENTDETAIL_START,
                            CHILD_STINCLUDE_COMPONENTDETAIL_START,CHILD_STINCLUDE_COMPONENTDETAIL_START_RESET_VALUE, null);
                return child;
            }
            else if (name.equals(CHILD_STINCLUDE_COMPONENTDETAIL_END)) {
                StaticTextField child =
                    new StaticTextField(this, getDefaultModel(), CHILD_STINCLUDE_COMPONENTDETAIL_END,
                            CHILD_STINCLUDE_COMPONENTDETAIL_END,CHILD_STINCLUDE_COMPONENTDETAIL_END_RESET_VALUE, null);
                return child;
            }
       else if (name.equals(CHILD_STINCLUDECOMPONENTINFOSECTION_START))
            {
                StaticTextField child =
                    new StaticTextField(this, getdoMainViewDealModel(),
                   CHILD_STINCLUDECOMPONENTINFOSECTION_START,
                    CHILD_STINCLUDECOMPONENTINFOSECTION_START,
                    CHILD_STINCLUDECOMPONENTINFOSECTION_START_RESET_VALUE, null);

                return child;
            }
       else if (name.equals(CHILD_STINCLUDECOMPONENTINFOSECTION_END))
            {
                StaticTextField child =
                    new StaticTextField(this, getdoMainViewDealModel(),
              CHILD_STINCLUDECOMPONENTINFOSECTION_END,
              CHILD_STINCLUDECOMPONENTINFOSECTION_END,
              CHILD_STINCLUDECOMPONENTINFOSECTION_END_RESET_VALUE, null);

                return child;
            }
      /************************MCM Impl team changes ends - XS_16.7 ***************************************/
        else if (name.equals(CHILD_STQUALIFYRATEEDIT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STQUALIFYRATEEDIT,
                    CHILD_STQUALIFYRATEEDIT,
                    CHILD_STQUALIFYRATEEDIT_RESET_VALUE, null);
            return child;
        }
            else
            {
                throw new IllegalArgumentException("Invalid child name [" + name + "]");
            }
    }

    /**
     * resetChildren
     * 
     * @param None
     * 
     * @return void : the result of resetChildren
     * @version 1.1 Date: 7/28/006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: <br>
     *          Added 3 fields cash back amount, cash back %, affiliation
     *          program
     *          
     *  @Version 1.2 <br>
     *	Date: 13/06/2008
     *	Author: MCM Impl Team <br>
     *	Change Log:  <br>
     *	XS_16.8 -- Added necessary changes required for the new fields added in Refinance section<br>
   *  @version 1.4 10/06/2008 XS_16.7  <br>
	 *           Author: MCM Impl Team <br>
     *          Change: <br>
	 *           Added necessary changes required for the new fields added in Component Information Section<br>
     *        
     */
    public void resetChildren()
    {
        super.resetChildren();
        getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
        getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
        getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
        getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
        getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
        getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
        getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
        getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
        getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
        getHref1().setValue(CHILD_HREF1_RESET_VALUE);
        getHref2().setValue(CHILD_HREF2_RESET_VALUE);
        getHref3().setValue(CHILD_HREF3_RESET_VALUE);
        getHref4().setValue(CHILD_HREF4_RESET_VALUE);
        getHref5().setValue(CHILD_HREF5_RESET_VALUE);
        getHref6().setValue(CHILD_HREF6_RESET_VALUE);
        getHref7().setValue(CHILD_HREF7_RESET_VALUE);
        getHref8().setValue(CHILD_HREF8_RESET_VALUE);
        getHref9().setValue(CHILD_HREF9_RESET_VALUE);
        getHref10().setValue(CHILD_HREF10_RESET_VALUE);
        getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
        getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
        getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
        getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
        getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
        getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
        getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
        getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
        getBtPrintDealSummary().setValue(CHILD_BTPRINTDEALSUMMARY_RESET_VALUE);

        getBtPrintDealSummaryLite().setValue(CHILD_BTPRINTDEALSUMMARYLITE_RESET_VALUE);
        getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
        getBtPartySummary().setValue(CHILD_BTPARTYSUMMARY_RESET_VALUE);
        getBtDeclineLetter().setValue(CHILD_BTDECLINELETTER_RESET_VALUE);
        getRepeatedDownPayments().resetChildren();
        getRepeatedEscrowPayments().resetChildren();
        getRepeatedBorrowerDetails().resetChildren();
        getRepeatedGDS().resetChildren();
        getRepeatedTDS().resetChildren();
        getRepeatedIncomeDetails().resetChildren();
        getRepeatedLiabDetails().resetChildren();
        getRepeatedAssetDetails().resetChildren();
        getRepeatedPropertyDetails().resetChildren();
        getStLastName().setValue(CHILD_STLASTNAME_RESET_VALUE);
        getStSuffix().setValue(CHILD_STSUFFIX_RESET_VALUE);
        getStMiddleInitial().setValue(CHILD_STMIDDLEINITIAL_RESET_VALUE);
        getStFirstName().setValue(CHILD_STFIRSTNAME_RESET_VALUE);
        getStLanguagePreference().setValue(CHILD_STLANGUAGEPREFERENCE_RESET_VALUE);
        getStExistingClient().setValue(CHILD_STEXISTINGCLIENT_RESET_VALUE);
        getStReferenceClientNumber().setValue(CHILD_STREFERENCECLIENTNUMBER_RESET_VALUE);
        getStUnderwriter().setValue(CHILD_STUNDERWRITER_RESET_VALUE);
        getStAdministrator().setValue(CHILD_STADMINISTRATOR_RESET_VALUE);
        getStFunder().setValue(CHILD_STFUNDER_RESET_VALUE);
        getStHomePhone().setValue(CHILD_STHOMEPHONE_RESET_VALUE);
        getStWorkPhone().setValue(CHILD_STWORKPHONE_RESET_VALUE);
        getStNoOfBorrowers().setValue(CHILD_STNOOFBORROWERS_RESET_VALUE);
        getStNoOfGuarantors().setValue(CHILD_STNOOFGUARANTORS_RESET_VALUE);
        getStBranch().setValue(CHILD_STBRANCH_RESET_VALUE);
        getStDITotalLoanAmount().setValue(CHILD_STDITOTALLOANAMOUNT_RESET_VALUE);
        getStPaymentTerm().setValue(CHILD_STPAYMENTTERM_RESET_VALUE);
        getStPaymentTermDesc().setValue(CHILD_STPAYMENTTERMDESC_RESET_VALUE);
        getStEffectiveAmortization().setValue(CHILD_STEFFECTIVEAMORTIZATION_RESET_VALUE);
        getStPaymentFrequency().setValue(CHILD_STPAYMENTFREQUENCY_RESET_VALUE);
        getStCombinedLTV().setValue(CHILD_STCOMBINEDLTV_RESET_VALUE);
        getStRateCode().setValue(CHILD_STRATECODE_RESET_VALUE);
        getStRateDate().setValue(CHILD_STRATEDATE_RESET_VALUE);
        getStFinancingDate().setValue(CHILD_STFINANCINGDATE_RESET_VALUE);
        getStPostedRate().setValue(CHILD_STPOSTEDRATE_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getStRateLockedIn().setValue(CHILD_STRATELOCKEDIN_RESET_VALUE);
        getStDiscount().setValue(CHILD_STDISCOUNT_RESET_VALUE);
        getStPremium().setValue(CHILD_STPREMIUM_RESET_VALUE);
        getStBuyDownRate().setValue(CHILD_STBUYDOWNRATE_RESET_VALUE);
        getStDISpecialFeature().setValue(CHILD_STDISPECIALFEATURE_RESET_VALUE);
        getStPreAppPricePurchase().setValue(CHILD_STPREAPPPRICEPURCHASE_RESET_VALUE);
        getStPIPayment().setValue(CHILD_STPIPAYMENT_RESET_VALUE);
        getStAdditionalPrincipalPayemt().setValue(CHILD_STADDITIONALPRINCIPALPAYEMT_RESET_VALUE);
        getStTotalEscrowPayment().setValue(CHILD_STTOTALESCROWPAYMENT_RESET_VALUE);
        getStTotalPayment().setValue(CHILD_STTOTALPAYMENT_RESET_VALUE);
        getStFirstPaymentDate().setValue(CHILD_STFIRSTPAYMENTDATE_RESET_VALUE);
        getStMaturityDate().setValue(CHILD_STMATURITYDATE_RESET_VALUE);
        getStSecondMortgage().setValue(CHILD_STSECONDMORTGAGE_RESET_VALUE);
        getStEquityAvailable().setValue(CHILD_STEQUITYAVAILABLE_RESET_VALUE);
        getStDIDealPurpose().setValue(CHILD_STDIDEALPURPOSE_RESET_VALUE);
        getStRequiredDownPayment().setValue(CHILD_STREQUIREDDOWNPAYMENT_RESET_VALUE);
        getStDILOB().setValue(CHILD_STDILOB_RESET_VALUE);
        getStDIDealType().setValue(CHILD_STDIDEALTYPE_RESET_VALUE);
        getStProduct().setValue(CHILD_STPRODUCT_RESET_VALUE);
        getStPrePaymentOption().setValue(CHILD_STPREPAYMENTOPTION_RESET_VALUE);
        getStReferenceType().setValue(CHILD_STREFERENCETYPE_RESET_VALUE);
        getStReferenceNo().setValue(CHILD_STREFERENCENO_RESET_VALUE);
        getStEstimatedClosingDate().setValue(CHILD_STESTIMATEDCLOSINGDATE_RESET_VALUE);
        getStActualClosingDate().setValue(CHILD_STACTUALCLOSINGDATE_RESET_VALUE);
        getStPrivilegePaymentOption().setValue(CHILD_STPRIVILEGEPAYMENTOPTION_RESET_VALUE);
        getStIncludeFinancingProgramStart().setValue(CHILD_STINCLUDEFINANCINGPROGRAMSTART_RESET_VALUE);
        getStFinancingProgram().setValue(CHILD_STFINANCINGPROGRAM_RESET_VALUE);
        getStIncludeFinancingProgramEnd().setValue(CHILD_STINCLUDEFINANCINGPROGRAMEND_RESET_VALUE);
/*        getStDISource().setValue(CHILD_STDISOURCE_RESET_VALUE);
        getStDISourceFirm().setValue(CHILD_STDISOURCEFIRM_RESET_VALUE);
        getStDISourceAddress().setValue(CHILD_STDISOURCEADDRESS_RESET_VALUE);
        getStDISourceContactPhone().setValue(CHILD_STDISOURCECONTACTPHONE_RESET_VALUE);
        getStDISourceContactPhoneExt().setValue(CHILD_STDISOURCECONTACTPHONEEXT_RESET_VALUE);
        getStDISourceContactFax().setValue(CHILD_STDISOURCECONTACTFAX_RESET_VALUE);*/
		//4.4 Submission Agent
        getRepeatedSOB().resetChildren();

        getStReferenceSourceApp().setValue(CHILD_STREFERENCESOURCEAPP_RESET_VALUE);
        getStCrossSell().setValue(CHILD_STCROSSSELL_RESET_VALUE);
        getStTotalPurchasePrice().setValue(CHILD_STTOTALPURCHASEPRICE_RESET_VALUE);
        getStTotalEstimatedValue().setValue(CHILD_STTOTALESTIMATEDVALUE_RESET_VALUE);
        getStTotalActualAppraisedValue().setValue(CHILD_STTOTALACTUALAPPRAISEDVALUE_RESET_VALUE);
        getStLender().setValue(CHILD_STLENDER_RESET_VALUE);
        getStInvestor().setValue(CHILD_STINVESTOR_RESET_VALUE);
        getStBridgePurpose().setValue(CHILD_STBRIDGEPURPOSE_RESET_VALUE);
        getStBridgeLender().setValue(CHILD_STBRIDGELENDER_RESET_VALUE);
        getStBridgeProduct().setValue(CHILD_STBRIDGEPRODUCT_RESET_VALUE);
        getStBridgePaymentTermDesc().setValue(CHILD_STBRIDGEPAYMENTTERMDESC_RESET_VALUE);
        getStBridgeRateCode().setValue(CHILD_STBRIDGERATECODE_RESET_VALUE);
        getStBridgePostedInterestRate().setValue(CHILD_STBRIDGEPOSTEDINTERESTRATE_RESET_VALUE);
        getStBridgeRateDate().setValue(CHILD_STBRIDGERATEDATE_RESET_VALUE);
        getStBridgeDiscount().setValue(CHILD_STBRIDGEDISCOUNT_RESET_VALUE);
        getStBridgePremium().setValue(CHILD_STBRIDGEPREMIUM_RESET_VALUE);
        getStBridgeNetRate().setValue(CHILD_STBRIDGENETRATE_RESET_VALUE);
        getStBridgeLoanAmount().setValue(CHILD_STBRIDGELOANAMOUNT_RESET_VALUE);
        getStBridgeActualPaymentTerm().setValue(CHILD_STBRIDGEACTUALPAYMENTTERM_RESET_VALUE);
        getStBridgeMaturityDate().setValue(CHILD_STBRIDGEMATURITYDATE_RESET_VALUE);
        getStBeginHideRefiSection().setValue(CHILD_STBEGINHIDEREFISECTION_RESET_VALUE);
        getStRefiPurchaseDate().setValue(CHILD_STREFIPURCHASEDATE_RESET_VALUE);
        getStRefiMortgageHolder().setValue(CHILD_STREFIMORTGAGEHOLDER_RESET_VALUE);
        getStRefiOrigPurchasePrice().setValue(CHILD_STREFIORIGPURCHASEPRICE_RESET_VALUE);
        getStRefiImprovementValue().setValue(CHILD_STREFIIMPROVEMENTVALUE_RESET_VALUE);
        getStRefiBlendedAmorization().setValue(CHILD_STREFIBLENDEDAMORIZATION_RESET_VALUE);
        getStRefiPurpose().setValue(CHILD_STREFIPURPOSE_RESET_VALUE);
        getStRefiOrigMtgAmount().setValue(CHILD_STREFIORIGMTGAMOUNT_RESET_VALUE);
        getStRefiOutstandingMtgAmount().setValue(CHILD_STREFIOUTSTANDINGMTGAMOUNT_RESET_VALUE);
        getStRefiImprovementDesc().setValue(CHILD_STREFIIMPROVEMENTDESC_RESET_VALUE);
        getStEndHideRefiSection().setValue(CHILD_STENDHIDEREFISECTION_RESET_VALUE);

        getStProgressAdvance().setValue(CHILD_STPROGRESSADVANCE_RESET_VALUE);
        getStAdvanceToDateAmt().setValue(CHILD_STADVANCETODATEAMT_RESET_VALUE);
        getStNextAdvanceAmt().setValue(CHILD_STNEXTADVANCEAMT_RESET_VALUE);
        getStAdvanceNumber().setValue(CHILD_STADVANCENUMBER_RESET_VALUE);
        getStCreditScore().setValue(CHILD_STCREDITSCORE_RESET_VALUE);
        getStCombinedGDS().setValue(CHILD_STCOMBINEDGDS_RESET_VALUE);
        getStCombined3YrGds().setValue(CHILD_STCOMBINED3YRGDS_RESET_VALUE);
        getStCombinedTDS().setValue(CHILD_STCOMBINEDTDS_RESET_VALUE);
        getStCombined3YrTds().setValue(CHILD_STCOMBINED3YRTDS_RESET_VALUE);
        //***** Qualify Rate *****//
        getStQualifyRate().setValue(CHILD_STQUALIFYRATE_RESET_VALUE);
        getStQualifyProduct().setValue(CHILD_STQUALIFYPRODUCT_RESET_VALUE);
        getHdQualifyRateOverride().setValue(CHILD_HDQUALIFYRATEOVERRIDE_RESET_VALUE);
        getHdQualifyRateOverrideRate().setValue(CHILD_HDQUALIFYRATEOVERRIDERATE_RESET_VALUE);
        //***** Qualify Rate *****//      
        getStcombinedTotalIncome().setValue(CHILD_STCOMBINEDTOTALINCOME_RESET_VALUE);
        getStcombinedTotalLiabilities().setValue(CHILD_STCOMBINEDTOTALLIABILITIES_RESET_VALUE);
        getStcombinedTotalAssets().setValue(CHILD_STCOMBINEDTOTALASSETS_RESET_VALUE);
        getStPropertyStreetno().setValue(CHILD_STPROPERTYSTREETNO_RESET_VALUE);
        getStPropertyStreetname().setValue(CHILD_STPROPERTYSTREETNAME_RESET_VALUE);
        getStPropertyStreetType().setValue(CHILD_STPROPERTYSTREETTYPE_RESET_VALUE);
        getStPropertyStreetDirection().setValue(CHILD_STPROPERTYSTREETDIRECTION_RESET_VALUE);
        getStPropertyUnitNumber().setValue(CHILD_STPROPERTYUNITNUMBER_RESET_VALUE);
        getStPropertyAddress2().setValue(CHILD_STPROPERTYADDRESS2_RESET_VALUE);
        getStPropertyCity().setValue(CHILD_STPROPERTYCITY_RESET_VALUE);
        getStPropertyProvince().setValue(CHILD_STPROPERTYPROVINCE_RESET_VALUE);
        getStPropertyPostalCode().setValue(CHILD_STPROPERTYPOSTALCODE_RESET_VALUE);
        getStPropertyPostalCode1().setValue(CHILD_STPROPERTYPOSTALCODE1_RESET_VALUE);
        getStPropertyUsage().setValue(CHILD_STPROPERTYUSAGE_RESET_VALUE);
        getStPropertyOccupancy().setValue(CHILD_STPROPERTYOCCUPANCY_RESET_VALUE);
        getStPropertyType().setValue(CHILD_STPROPERTYTYPE_RESET_VALUE);
        getStNoOfProperties().setValue(CHILD_STNOOFPROPERTIES_RESET_VALUE);
        getStPropertyPurchasePrice().setValue(CHILD_STPROPERTYPURCHASEPRICE_RESET_VALUE);
        getStPropertyEstimatedAppraisalValue().setValue(CHILD_STPROPERTYESTIMATEDAPPRAISALVALUE_RESET_VALUE);
        getStPropertyActualApraisedValue().setValue(CHILD_STPROPERTYACTUALAPRAISEDVALUE_RESET_VALUE);
        getStPropertyLandValue().setValue(CHILD_STPROPERTYLANDVALUE_RESET_VALUE);
        getStCombinedTotalPropertyExp().setValue(CHILD_STCOMBINEDTOTALPROPERTYEXP_RESET_VALUE);
        getStMIIndictorDesc().setValue(CHILD_STMIINDICTORDESC_RESET_VALUE);
        getStMIRequestStatus().setValue(CHILD_STMIREQUESTSTATUS_RESET_VALUE);
        getStMIExistingPolicyNum().setValue(CHILD_STMIEXISTINGPOLICYNUM_RESET_VALUE);
        getStMIPoliceNumber().setValue(CHILD_STMIPOLICENUMBER_RESET_VALUE);
        getStMIPrequalificationMICertNum().setValue(CHILD_STMIPREQUALIFICATIONMICERTNUM_RESET_VALUE);
        getStMIType().setValue(CHILD_STMITYPE_RESET_VALUE);
        getStMIInsurer().setValue(CHILD_STMIINSURER_RESET_VALUE);
        // SEAN DJ SPEC-Progress Advance Type July 25, 2005: Reset value.
        getStProgressAdvanceType().setValue(CHILD_STPROGRESSADVANCETYPE_RESET_VALUE);
        // SEAN DJ SPEC-PAT END
        getStMIPayorDesc().setValue(CHILD_STMIPAYORDESC_RESET_VALUE);
        getStMIUpfront().setValue(CHILD_STMIUPFRONT_RESET_VALUE);
        getStMIPremium().setValue(CHILD_STMIPREMIUM_RESET_VALUE);
        getStMIRUIntervention().setValue(CHILD_STMIRUINTERVENTION_RESET_VALUE);
        getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
        getStDealId().setValue(CHILD_STDEALID_RESET_VALUE);
        getStBorrFirstName().setValue(CHILD_STBORRFIRSTNAME_RESET_VALUE);
        getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
        getStDealStatusDate().setValue(CHILD_STDEALSTATUSDATE_RESET_VALUE);
        getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
        getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
        getStEstClosingDate().setValue(CHILD_STESTCLOSINGDATE_RESET_VALUE);
        getStLOB().setValue(CHILD_STLOB_RESET_VALUE);
        getStSpecialFeature().setValue(CHILD_STSPECIALFEATURE_RESET_VALUE);
        getStDealType().setValue(CHILD_STDEALTYPE_RESET_VALUE);
        getStDealPurpose().setValue(CHILD_STDEALPURPOSE_RESET_VALUE);
        getStPurchasePrice().setValue(CHILD_STPURCHASEPRICE_RESET_VALUE);
        getStPmtTerm().setValue(CHILD_STPMTTERM_RESET_VALUE);
        getStTotalLoanAmount().setValue(CHILD_STTOTALLOANAMOUNT_RESET_VALUE);
        getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
        getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
        getStCombinedBorrowerGDS().setValue(CHILD_STCOMBINEDBORROWERGDS_RESET_VALUE);
        getStCombinedBorrowerTDS().setValue(CHILD_STCOMBINEDBORROWERTDS_RESET_VALUE);
        getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
        getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
        getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
        getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
        getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
        getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
        getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
        getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
        getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
        getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
        getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
        getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
        getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
        getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
        getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
        getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
        getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
        getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
        getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
        getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
        getStBeginHideBridgeSection().setValue(CHILD_STBEGINHIDEBRIDGESECTION_RESET_VALUE);
        getStEndHideBridgeSection().setValue(CHILD_STENDHIDEBRIDGESECTION_RESET_VALUE);
        getStEndHideEscrowSection().setValue(CHILD_STENDHIDEESCROWSECTION_RESET_VALUE);
        getStBeginHideEscrowSection().setValue(CHILD_STBEGINHIDEESCROWSECTION_RESET_VALUE);
        getHdExecuteEscow().setValue(CHILD_HDEXECUTEESCOW_RESET_VALUE);
        getRepeatedPropertyAddressAndExpenses().resetChildren();
        getStBorrowerWorkPhoneExt().setValue(CHILD_STBORROWERWORKPHONEEXT_RESET_VALUE);
        getBtOkMainPageButton().setValue(CHILD_BTOKMAINPAGEBUTTON_RESET_VALUE);

        //--> Hidden ActMessageButton (FINDTHISLATER)
        //--> Test by BILLY 15July2002
        getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);

        //// New link to toggle the language. When touched this link should
        // reload the
        //// page in opposite language (french versus english) and set this new
        // language
        //// session value id throughout all modulus.
        getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);

        //--> New requirement : added new button for reverse funding
        //--> By Billy 11June2003
        getBtReverseFunding().setValue(CHILD_BTREVERSEFUNDING_RESET_VALUE);

        //--DJ_LDI_CR--start--//
        getRepeatedLifeDisabilityInsurance().resetChildren();
        getStIncludeLifeDisSectionStart().setValue(CHILD_STINCLUDELIFEDISSECTIONSTART_RESET_VALUE);
        getStIncludeLifeDisSectionEnd().setValue(CHILD_STINCLUDELIFEDISSECTIONEND_RESET_VALUE);

        //--DJ_LDI_CR--end--//
        //--DJ_CR134--start--27May2004--//
        getStHomeBASEProductRatePmnt().setValue(CHILD_STHOMEBASEPRODUCTRATEPMNT_RESET_VALUE);
        getStIncludeLifeDisLabelsStart().setValue(CHILD_STINCLUDELIFEDISLABELSSTART_RESET_VALUE);
        getStIncludeLifeDisLabelsEnd().setValue(CHILD_STINCLUDELIFEDISLABELSEND_RESET_VALUE);

        //--DJ_CR134--end--//
        //-- ========== DJ#725 begins ========== --//
        //-- by Neil at Nov/29/2004
        getStContactEmailAddress().setValue(CHILD_STCONTACTEMAILADDRESS_RESET_VALUE);
        getStPsDescription().setValue(CHILD_STPSDESCRIPTION_RESET_VALUE);
        getStSystemTypeDescription().setValue(CHILD_STSYSTEMTYPEDESCRIPTION_RESET_VALUE);

        //-- ========== DJ#725 ends ========== --//

        // SEAN Ticket #1681 June 27, 2005: Hide print deal summary bt for DJ.
        getStBeginHidePrintDealSummary().setValue(CHILD_STBEGINHIDEPRINTDEALSUMMARY_RESET_VALUE);
        getStEndHidePrintDealSummary().setValue(CHILD_STENDHIDEPRINTDEALSUMMARY_RESET_VALUE);
        // SEAN Ticket #1681 END.

//      ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
        getStCashBackInDollars()
        .setValue(CHILD_STCASHBACKINDOLLARS_RESET_VALUE);
        getStCashBackInPercentage().setValue(
                CHILD_STCASHBACKINPERCENTAGE_RESET_VALUE);

        getStAffiliationProgram().setValue(
                CHILD_STAFFILIATIONPROGRAM_RESET_VALUE);

        // ***** Change by NBC Impl. Team - Version 1.1 - End*****//
        //--Release3.1--begins
        //--by Hiro Apr 10, 2006

        getStProductType().setValue(CHILD_STPRODUCTTYPE_RESET_VALUE);
        getStCharge().setValue(CHILD_STCHARGE_RESET_VALUE);
        getStRefiProductType().setValue(CHILD_STREFIPRODUCTTYPE_RESET_VALUE);

        getStProgressAdvanceInspectionBy().setValue(
                CHILD_STPROGRESSADVANCEINSPECTIONBY_RESET_VALUE);
        getStSelfDirectedRRSP().setValue(CHILD_STSELFDIRECTEDRRSP_RESET_VALUE);
        getStCMHCProductTrackerIdentifier().setValue(
                CHILD_STCMHCPRODUCTTRACKERIDENTIFIER_RESET_VALUE);
        getStLOCRepayment().setValue(CHILD_STLOCREPAYMENT_RESET_VALUE);
        getStRequestStandardService().setValue(
                CHILD_STREQUESTSTANDARDSERVICE_RESET_VALUE);
        getStLOCAmortization().setValue(CHILD_STLOCAMORTIZATION_RESET_VALUE);
        getStLOCInterestOnlyMaturityDate().setValue(
                CHILD_STLOCINTERESTONLYMATURITYDATE_RESET_VALUE);

        //--Release3.1--ends
        //***** Change by NBC/PP Implementation Team - GCD - Start *****//

        getTxCreditDecisionStatusChild().setValue(CHILD_TX_CREDIT_DECISION_STATUS_RESET_VALUE);
        getTxCreditDecisionChild().setValue(CHILD_TX_CREDIT_DECISION_RESET_VALUE);
        getTxGlobalCreditBureauScoreChild().setValue(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE_RESET_VALUE);
        getTxGlobalRiskRatingChild().setValue(CHILD_TX_GLOBAL_RISK_RATING_RESET_VALUE);
        getTxGlobalInternalScoreChild().setValue(CHILD_TX_GLOBAL_INTERNAL_SCORE_RESET_VALUE);
        getTxMIStatusChild().setValue(CHILD_TX_MISTATUS_RESET_VALUE);
        getBtCreditDecisionPGChild().setValue(CHILD_BT_CREDIT_DECISION_PG_RESET_VALUE);
        getStIncludeGCDSumStartChild().setValue(CHILD_ST_INCLUDE_GCDSUM_START_RESET_VALUE);
        getStIncludeGCDSumEndChild().setValue(CHILD_ST_INCLUDE_GCDSUM_END_RESET_VALUE);

        //***** Change by NBC/PP Implementation Team - GCD - End *****//
        //PPI start
        getStImprovedValue().setValue(CHILD_STIMPROVEDVALUE_RESET_VALUE);
        getTxDealPurposeTypeHiddenStart().setValue(CHILD_TXDEALPURPOSETYPEHIDDENSTART_RESET_VALUE);
        getTxDealPurposeTypeHiddenEnd().setValue(CHILD_TXDEALPURPOSETYPEHIDDENEND_RESET_VALUE);
        //PPI end

        /***************MCM Impl team changes starts - XS_16.8 *******************/

        getStRefiAdditionalInformation().setValue(CHILD_STREFIADDITIONALINFORMATION_RESET_VALUE);
        getStRefExistingMTGNumber().setValue(CHILD_STREFEXISTINGMTGNUMBER_RESET_VALUE);

        /***************MCM Impl team changes ends - XS_16.8 *********************/
        /***************MCM Impl team changes starts - XS_16.7  *******************/
        getRepeatedComponentMortgage().resetChildren();
        getRepeatedComponentLoc().resetChildren();
        getRepeatedComponentLoan().resetChildren();
        getRepeatedComponentCreditCard().resetChildren();
        getRepeatedComponentOverDraft().resetChildren();
    getStIncludeComponentDetailsButtonEnd().setValue(CHILD_STINCLUDE_COMPONENTDETAIL_START_RESET_VALUE);
	  getStIncludeComponentDetailsButtonStart().setValue(CHILD_STINCLUDE_COMPONENTDETAIL_END_RESET_VALUE);
    getStIncludeComponentInfoSectionEnd().setValue(CHILD_STINCLUDECOMPONENTINFOSECTION_END_RESET_VALUE);
    getStIncludeComponentInfoSectionStart().setValue(CHILD_STINCLUDECOMPONENTINFOSECTION_START_RESET_VALUE);
        getStTotalAmount().setValue(CHILD_STTOTALAMOUNT_RESET_VALUE);
        getStTotalCashBackAmount().setValue(CHILD_STTOTALCASHBACKAMOUNT_RESET_VALUE);
	  getStUnAllocatedAmount().setValue(CHILD_STUNALLOCATEDAMOUNT_RESET_VALUE);
        getBtComponentDeatails().setValue(CHILD_BTCOMPONENTDETAILS_RESET_VALUE);
        /***************MCM Impl team changes ends - XS_16.7  *******************/	
        getStQualifyRateEdit().setValue(CHILD_STQUALIFYRATEEDIT_RESET_VALUE);
    }

    /**
     * registerChildren
     * 
     * @param None
     *            <br>
     * 
     * @return void : the result of registerChildren <br>
     * @version 1.1 Date: 7/28/006 <br>
     *          Author: NBC/PP Implementation Team <br>
     *          Change: <br>
     *          Added 3 new fields cash back amount, cash back percentage, and
     *          affilication program
     *          
     *  @Version 1.2 <br>
     *	Date: 13/06/2008
     *	Author: MCM Impl Team <br>
     *	Change Log:  <br>
     *	XS_16.8 -- Added necessary changes required for the new fields added in Refinance section<br>
	 *  @version 1.4 10/06/2008 XS_16.7  <br>
	 *          Author:MCM Impl Team <br>
     *          Change: <br>
	 *          Added necessary changes required for the new fields added in Component Information Section<br>
     *          
     */

    protected void registerChildren()
    {
        super.registerChildren();
        registerChild(CHILD_STPAGELABEL, StaticTextField.class);
        registerChild(CHILD_STCOMPANYNAME, StaticTextField.class);
        registerChild(CHILD_STTODAYDATE, StaticTextField.class);
        registerChild(CHILD_STUSERNAMETITLE, StaticTextField.class);
        registerChild(CHILD_DETECTALERTTASKS, HiddenField.class);
        registerChild(CHILD_SESSIONUSERID, HiddenField.class);
        registerChild(CHILD_TBDEALID, TextField.class);
        registerChild(CHILD_CBPAGENAMES, ComboBox.class);
        registerChild(CHILD_BTPROCEED, Button.class);
        registerChild(CHILD_HREF1, HREF.class);
        registerChild(CHILD_HREF2, HREF.class);
        registerChild(CHILD_HREF3, HREF.class);
        registerChild(CHILD_HREF4, HREF.class);
        registerChild(CHILD_HREF5, HREF.class);
        registerChild(CHILD_HREF6, HREF.class);
        registerChild(CHILD_HREF7, HREF.class);
        registerChild(CHILD_HREF8, HREF.class);
        registerChild(CHILD_HREF9, HREF.class);
        registerChild(CHILD_HREF10, HREF.class);
        registerChild(CHILD_CHANGEPASSWORDHREF, HREF.class);
        registerChild(CHILD_BTWORKQUEUELINK, Button.class);
        registerChild(CHILD_BTTOOLHISTORY, Button.class);
        registerChild(CHILD_BTTOONOTES, Button.class);
        registerChild(CHILD_BTTOOLSEARCH, Button.class);
        registerChild(CHILD_BTTOOLLOG, Button.class);
        registerChild(CHILD_BTPREVTASKPAGE, Button.class);
        registerChild(CHILD_BTNEXTTASKPAGE, Button.class);
        registerChild(CHILD_BTPRINTDEALSUMMARY, Button.class);
       

        registerChild(CHILD_BTPRINTDEALSUMMARYLITE, Button.class);
        registerChild(CHILD_STPREVTASKPAGELABEL, StaticTextField.class);
        registerChild(CHILD_BTPARTYSUMMARY, Button.class);
        registerChild(CHILD_BTDECLINELETTER, Button.class);
        registerChild(CHILD_REPEATEDDOWNPAYMENTS,
                pgDealSummaryRepeatedDownPaymentsTiledView.class);
        registerChild(CHILD_REPEATEDESCROWPAYMENTS,
                pgDealSummaryRepeatedEscrowPaymentsTiledView.class);
        registerChild(CHILD_REPEATEDBORROWERDETAILS,
                pgDealSummaryRepeatedBorrowerDetailsTiledView.class);
        registerChild(CHILD_REPEATEDGDS, pgDealSummaryRepeatedGDSTiledView.class);
        registerChild(CHILD_REPEATEDTDS, pgDealSummaryRepeatedTDSTiledView.class);
        registerChild(CHILD_REPEATEDINCOMEDETAILS,
                pgDealSummaryRepeatedIncomeDetailsTiledView.class);
        registerChild(CHILD_REPEATEDLIABDETAILS,
                pgDealSummaryRepeatedLiabDetailsTiledView.class);
        registerChild(CHILD_REPEATEDASSETDETAILS,
                pgDealSummaryRepeatedAssetDetailsTiledView.class);
        registerChild(CHILD_REPEATEDPROPERTYDETAILS,
                pgDealSummaryRepeatedPropertyDetailsTiledView.class);
        registerChild(CHILD_STLASTNAME, StaticTextField.class);
        registerChild(CHILD_STSUFFIX, StaticTextField.class);
        registerChild(CHILD_STMIDDLEINITIAL, StaticTextField.class);
        registerChild(CHILD_STFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_STLANGUAGEPREFERENCE, StaticTextField.class);
        registerChild(CHILD_STEXISTINGCLIENT, StaticTextField.class);
        registerChild(CHILD_STREFERENCECLIENTNUMBER, StaticTextField.class);
        registerChild(CHILD_STUNDERWRITER, StaticTextField.class);
        registerChild(CHILD_STADMINISTRATOR, StaticTextField.class);
        registerChild(CHILD_STFUNDER, StaticTextField.class);
        registerChild(CHILD_STHOMEPHONE, StaticTextField.class);
        registerChild(CHILD_STWORKPHONE, StaticTextField.class);
        registerChild(CHILD_STNOOFBORROWERS, StaticTextField.class);
        registerChild(CHILD_STNOOFGUARANTORS, StaticTextField.class);
        registerChild(CHILD_STBRANCH, StaticTextField.class);
        registerChild(CHILD_STDITOTALLOANAMOUNT, StaticTextField.class);
        registerChild(CHILD_STPAYMENTTERM, StaticTextField.class);
        registerChild(CHILD_STPAYMENTTERMDESC, StaticTextField.class);
        registerChild(CHILD_STEFFECTIVEAMORTIZATION, StaticTextField.class);
        registerChild(CHILD_STPAYMENTFREQUENCY, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDLTV, StaticTextField.class);
        registerChild(CHILD_STRATECODE, StaticTextField.class);
        registerChild(CHILD_STRATEDATE, StaticTextField.class);
        registerChild(CHILD_STFINANCINGDATE, StaticTextField.class);
        registerChild(CHILD_STPOSTEDRATE, StaticTextField.class);
        registerChild(CHILD_STNETRATE, StaticTextField.class);
        registerChild(CHILD_STRATELOCKEDIN, StaticTextField.class);
        registerChild(CHILD_STDISCOUNT, StaticTextField.class);
        registerChild(CHILD_STPREMIUM, StaticTextField.class);
        registerChild(CHILD_STBUYDOWNRATE, StaticTextField.class);
        registerChild(CHILD_STDISPECIALFEATURE, StaticTextField.class);
        registerChild(CHILD_STPREAPPPRICEPURCHASE, StaticTextField.class);
        registerChild(CHILD_STPIPAYMENT, StaticTextField.class);
        registerChild(CHILD_STADDITIONALPRINCIPALPAYEMT, StaticTextField.class);
        registerChild(CHILD_STTOTALESCROWPAYMENT, StaticTextField.class);
        registerChild(CHILD_STTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_STFIRSTPAYMENTDATE, StaticTextField.class);
        registerChild(CHILD_STMATURITYDATE, StaticTextField.class);
        registerChild(CHILD_STSECONDMORTGAGE, StaticTextField.class);
        registerChild(CHILD_STEQUITYAVAILABLE, StaticTextField.class);
        registerChild(CHILD_STDIDEALPURPOSE, StaticTextField.class);
        // SEAN DJ SPEC-Progress Advance Type July 25, 2005: Register Child.
        registerChild(CHILD_STPROGRESSADVANCETYPE, StaticTextField.class);
        // SEAN DJ SPEC-PAT END
        registerChild(CHILD_STREQUIREDDOWNPAYMENT, StaticTextField.class);
        registerChild(CHILD_STDILOB, StaticTextField.class);
        registerChild(CHILD_STDIDEALTYPE, StaticTextField.class);
        registerChild(CHILD_STPRODUCT, StaticTextField.class);
        registerChild(CHILD_STPREPAYMENTOPTION, StaticTextField.class);
        registerChild(CHILD_STREFERENCETYPE, StaticTextField.class);
        registerChild(CHILD_STREFERENCENO, StaticTextField.class);
        registerChild(CHILD_STESTIMATEDCLOSINGDATE, StaticTextField.class);
        registerChild(CHILD_STACTUALCLOSINGDATE, StaticTextField.class);
        registerChild(CHILD_STPRIVILEGEPAYMENTOPTION, StaticTextField.class);
        registerChild(CHILD_STINCLUDEFINANCINGPROGRAMSTART, StaticTextField.class);
        registerChild(CHILD_STFINANCINGPROGRAM, StaticTextField.class);
        registerChild(CHILD_STINCLUDEFINANCINGPROGRAMEND, StaticTextField.class);
/*        registerChild(CHILD_STDISOURCE, StaticTextField.class);
        registerChild(CHILD_STDISOURCEFIRM, StaticTextField.class);
        registerChild(CHILD_STDISOURCEADDRESS, StaticTextField.class);
        registerChild(CHILD_STDISOURCECONTACTPHONE, StaticTextField.class);
        registerChild(CHILD_STDISOURCECONTACTPHONEEXT, StaticTextField.class);
        registerChild(CHILD_STDISOURCECONTACTFAX, StaticTextField.class);
        registerChild(CHILD_STREFERENCESOURCEAPP, StaticTextField.class);*/
		//4.4 Submission Agent
        registerChild(CHILD_REPEATEDSOB,
        		pgDealSummaryRepeatedSOBTiledView.class);
        registerChild(CHILD_STCROSSSELL, StaticTextField.class);
        registerChild(CHILD_STTOTALPURCHASEPRICE, StaticTextField.class);
        registerChild(CHILD_STTOTALESTIMATEDVALUE, StaticTextField.class);
        registerChild(CHILD_STTOTALACTUALAPPRAISEDVALUE, StaticTextField.class);
        registerChild(CHILD_STLENDER, StaticTextField.class);
        registerChild(CHILD_STINVESTOR, StaticTextField.class);
        registerChild(CHILD_STBRIDGEPURPOSE, StaticTextField.class);
        registerChild(CHILD_STBRIDGELENDER, StaticTextField.class);
        registerChild(CHILD_STBRIDGEPRODUCT, StaticTextField.class);
        registerChild(CHILD_STBRIDGEPAYMENTTERMDESC, StaticTextField.class);
        registerChild(CHILD_STBRIDGERATECODE, StaticTextField.class);
        registerChild(CHILD_STBRIDGEPOSTEDINTERESTRATE, StaticTextField.class);
        registerChild(CHILD_STBRIDGERATEDATE, StaticTextField.class);
        registerChild(CHILD_STBRIDGEDISCOUNT, StaticTextField.class);
        registerChild(CHILD_STBRIDGEPREMIUM, StaticTextField.class);
        registerChild(CHILD_STBRIDGENETRATE, StaticTextField.class);
        registerChild(CHILD_STBRIDGELOANAMOUNT, StaticTextField.class);
        registerChild(CHILD_STBRIDGEACTUALPAYMENTTERM, StaticTextField.class);
        registerChild(CHILD_STBRIDGEMATURITYDATE, StaticTextField.class);
        registerChild(CHILD_STBEGINHIDEREFISECTION, StaticTextField.class);
        registerChild(CHILD_STREFIPURCHASEDATE, StaticTextField.class);
        registerChild(CHILD_STREFIMORTGAGEHOLDER, StaticTextField.class);
        registerChild(CHILD_STREFIORIGPURCHASEPRICE, StaticTextField.class);
        registerChild(CHILD_STREFIIMPROVEMENTVALUE, StaticTextField.class);
        registerChild(CHILD_STREFIBLENDEDAMORIZATION, StaticTextField.class);
        registerChild(CHILD_STREFIPURPOSE, StaticTextField.class);
        registerChild(CHILD_STREFIORIGMTGAMOUNT, StaticTextField.class);
        registerChild(CHILD_STREFIOUTSTANDINGMTGAMOUNT, StaticTextField.class);
        registerChild(CHILD_STREFIIMPROVEMENTDESC, StaticTextField.class);
        registerChild(CHILD_STENDHIDEREFISECTION, StaticTextField.class);

        registerChild(CHILD_STPROGRESSADVANCE, StaticTextField.class);
        registerChild(CHILD_STADVANCETODATEAMT, StaticTextField.class);
        registerChild(CHILD_STNEXTADVANCEAMT, StaticTextField.class);
        registerChild(CHILD_STADVANCENUMBER, StaticTextField.class);
        registerChild(CHILD_STCREDITSCORE, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINED3YRGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDTDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINED3YRTDS, StaticTextField.class);
        //***** Qualify Rate *****//
        registerChild(CHILD_STQUALIFYRATE, StaticTextField.class);
        registerChild(CHILD_STQUALIFYPRODUCT, StaticTextField.class);
        registerChild(CHILD_HDQUALIFYRATEOVERRIDE, StaticTextField.class);
        registerChild(CHILD_HDQUALIFYRATEOVERRIDERATE, StaticTextField.class);
        //***** Qualify Rate *****//  
        registerChild(CHILD_STCOMBINEDTOTALINCOME, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDTOTALLIABILITIES, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDTOTALASSETS, StaticTextField.class);
        registerChild(CHILD_STPROPERTYSTREETNO, StaticTextField.class);
        registerChild(CHILD_STPROPERTYSTREETNAME, StaticTextField.class);
        registerChild(CHILD_STPROPERTYSTREETTYPE, StaticTextField.class);
        registerChild(CHILD_STPROPERTYSTREETDIRECTION, StaticTextField.class);
        registerChild(CHILD_STPROPERTYUNITNUMBER, StaticTextField.class);
        registerChild(CHILD_STPROPERTYADDRESS2, StaticTextField.class);
        registerChild(CHILD_STPROPERTYCITY, StaticTextField.class);
        registerChild(CHILD_STPROPERTYPROVINCE, StaticTextField.class);
        registerChild(CHILD_STPROPERTYPOSTALCODE, StaticTextField.class);
        registerChild(CHILD_STPROPERTYPOSTALCODE1, StaticTextField.class);
        registerChild(CHILD_STPROPERTYUSAGE, StaticTextField.class);
        registerChild(CHILD_STPROPERTYOCCUPANCY, StaticTextField.class);
        registerChild(CHILD_STPROPERTYTYPE, StaticTextField.class);
        registerChild(CHILD_STNOOFPROPERTIES, StaticTextField.class);
        registerChild(CHILD_STPROPERTYPURCHASEPRICE, StaticTextField.class);
        registerChild(CHILD_STPROPERTYESTIMATEDAPPRAISALVALUE, StaticTextField.class);
        registerChild(CHILD_STPROPERTYACTUALAPRAISEDVALUE, StaticTextField.class);
        registerChild(CHILD_STPROPERTYLANDVALUE, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDTOTALPROPERTYEXP, StaticTextField.class);
        registerChild(CHILD_STMIINDICTORDESC, StaticTextField.class);
        registerChild(CHILD_STMIREQUESTSTATUS, StaticTextField.class);
        registerChild(CHILD_STMIEXISTINGPOLICYNUM, StaticTextField.class);
        registerChild(CHILD_STMIPOLICENUMBER, StaticTextField.class);
        registerChild(CHILD_STMIPREQUALIFICATIONMICERTNUM, StaticTextField.class);
        registerChild(CHILD_STMITYPE, StaticTextField.class);
        registerChild(CHILD_STMIINSURER, StaticTextField.class);
        registerChild(CHILD_STMIPAYORDESC, StaticTextField.class);
        registerChild(CHILD_STMIUPFRONT, StaticTextField.class);
        registerChild(CHILD_STMIPREMIUM, StaticTextField.class);
        registerChild(CHILD_STMIRUINTERVENTION, StaticTextField.class);
        registerChild(CHILD_STERRORFLAG, StaticTextField.class);
        registerChild(CHILD_STDEALID, StaticTextField.class);
        registerChild(CHILD_STBORRFIRSTNAME, StaticTextField.class);
        registerChild(CHILD_STDEALSTATUS, StaticTextField.class);
        registerChild(CHILD_STDEALSTATUSDATE, StaticTextField.class);
        registerChild(CHILD_STSOURCEFIRM, StaticTextField.class);
        registerChild(CHILD_STSOURCE, StaticTextField.class);
        registerChild(CHILD_STESTCLOSINGDATE, StaticTextField.class);
        registerChild(CHILD_STLOB, StaticTextField.class);
        registerChild(CHILD_STSPECIALFEATURE, StaticTextField.class);
        registerChild(CHILD_STDEALTYPE, StaticTextField.class);
        registerChild(CHILD_STDEALPURPOSE, StaticTextField.class);
        registerChild(CHILD_STPURCHASEPRICE, StaticTextField.class);
        registerChild(CHILD_STPMTTERM, StaticTextField.class);
        registerChild(CHILD_STTOTALLOANAMOUNT, StaticTextField.class);
        registerChild(CHILD_STNEXTTASKPAGELABEL, StaticTextField.class);
        registerChild(CHILD_STTASKNAME, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDBORROWERGDS, StaticTextField.class);
        registerChild(CHILD_STCOMBINEDBORROWERTDS, StaticTextField.class);
        registerChild(CHILD_STPMGENERATE, StaticTextField.class);
        registerChild(CHILD_STPMHASTITLE, StaticTextField.class);
        registerChild(CHILD_STPMHASINFO, StaticTextField.class);
        registerChild(CHILD_STPMHASTABLE, StaticTextField.class);
        registerChild(CHILD_STPMHASOK, StaticTextField.class);
        registerChild(CHILD_STPMTITLE, StaticTextField.class);
        registerChild(CHILD_STPMINFOMSG, StaticTextField.class);
        registerChild(CHILD_STPMONOK, StaticTextField.class);
        registerChild(CHILD_STPMMSGS, StaticTextField.class);
        registerChild(CHILD_STPMMSGTYPES, StaticTextField.class);
        registerChild(CHILD_STAMGENERATE, StaticTextField.class);
        registerChild(CHILD_STAMHASTITLE, StaticTextField.class);
        registerChild(CHILD_STAMHASINFO, StaticTextField.class);
        registerChild(CHILD_STAMHASTABLE, StaticTextField.class);
        registerChild(CHILD_STAMTITLE, StaticTextField.class);
        registerChild(CHILD_STAMINFOMSG, StaticTextField.class);
        registerChild(CHILD_STAMMSGS, StaticTextField.class);
        registerChild(CHILD_STAMMSGTYPES, StaticTextField.class);
        registerChild(CHILD_STAMDIALOGMSG, StaticTextField.class);
        registerChild(CHILD_STAMBUTTONSHTML, StaticTextField.class);
        registerChild(CHILD_STBEGINHIDEBRIDGESECTION, StaticTextField.class);
        registerChild(CHILD_STENDHIDEBRIDGESECTION, StaticTextField.class);
        registerChild(CHILD_STENDHIDEESCROWSECTION, StaticTextField.class);
        registerChild(CHILD_STBEGINHIDEESCROWSECTION, StaticTextField.class);
        registerChild(CHILD_HDEXECUTEESCOW, HiddenField.class);
        registerChild(CHILD_REPEATEDPROPERTYADDRESSANDEXPENSES,
                pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView.class);
        registerChild(CHILD_STBORROWERWORKPHONEEXT, StaticTextField.class);
        registerChild(CHILD_BTOKMAINPAGEBUTTON, Button.class);

        //--> Hidden ActMessageButton (FINDTHISLATER)
        //--> Test by BILLY 15July2002
        registerChild(CHILD_BTACTMSG, Button.class);

        //// New link to toggle the language. When touched this link should
        // reload the
        //// page in opposite language (french versus english) and set this new
        // language
        //// session value id throughout all modulus.
        registerChild(CHILD_TOGGLELANGUAGEHREF, HREF.class);

        //--> New requirement : added new button for reverse funding
        //--> By Billy 11June2003
        registerChild(CHILD_BTREVERSEFUNDING, Button.class);

        //--DJ_LDI_CR--start--//
        registerChild(CHILD_REPEATEDLIFEDISABILITYINSURANCE,
                pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView.class);
        registerChild(CHILD_STINCLUDELIFEDISSECTIONSTART, StaticTextField.class);
        registerChild(CHILD_STINCLUDELIFEDISSECTIONEND, StaticTextField.class);

        //--DJ_LDI_CR--end--//
        //--DJ_CR134--start--27May2004--//
        registerChild(CHILD_STHOMEBASEPRODUCTRATEPMNT, StaticTextField.class);
        registerChild(CHILD_STINCLUDELIFEDISLABELSSTART, StaticTextField.class);
        registerChild(CHILD_STINCLUDELIFEDISLABELSEND, StaticTextField.class);

        //--DJ_CR134--end--//
        //-- ========== DJ#725 begins ========== --//
        //-- by Neil at Nov/29/2004
        registerChild(CHILD_STCONTACTEMAILADDRESS, StaticTextField.class);
        registerChild(CHILD_STPSDESCRIPTION, StaticTextField.class);
        registerChild(CHILD_STSYSTEMTYPEDESCRIPTION, StaticTextField.class);

        //-- ========== DJ#725 ends ========== --//

//      ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
        registerChild(CHILD_STCASHBACKINDOLLARS, StaticTextField.class);
        registerChild(CHILD_STCASHBACKINPERCENTAGE, StaticTextField.class);
        registerChild(CHILD_STAFFILIATIONPROGRAM, StaticTextField.class);

        // ***** Change by NBC Impl. Team - Version 1.1 - End*****//

        //--Release3.1--begins
        //--by Hiro Mar 31, 2006

        registerChild(CHILD_STPRODUCTTYPE, StaticTextField.class);
        registerChild(CHILD_STCHARGE, StaticTextField.class);
        registerChild(CHILD_STREFIPRODUCTTYPE, StaticTextField.class);

        registerChild(CHILD_STPROGRESSADVANCEINSPECTIONBY, StaticTextField.class);
        registerChild(CHILD_STSELFDIRECTEDRRSP, StaticTextField.class);
        registerChild(CHILD_STCMHCPRODUCTTRACKERIDENTIFIER, StaticTextField.class);
        registerChild(CHILD_STLOCREPAYMENT, StaticTextField.class);
        registerChild(CHILD_STREQUESTSTANDARDSERVICE, StaticTextField.class);
        registerChild(CHILD_STLOCAMORTIZATION, StaticTextField.class);
        registerChild(CHILD_STLOCINTERESTONLYMATURITYDATE, StaticTextField.class);
        //--Release3.1--ends
        //  ***** Change by NBC/PP Implementation Team - GCD - Start *****//

        registerChild(CHILD_TX_CREDIT_DECISION_STATUS, StaticTextField.class);
        registerChild(CHILD_TX_CREDIT_DECISION, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_RISK_RATING, StaticTextField.class);
        registerChild(CHILD_TX_GLOBAL_INTERNAL_SCORE, StaticTextField.class);
        registerChild(CHILD_TX_MISTATUS, StaticTextField.class);
        registerChild(CHILD_BT_CREDIT_DECISION_PG, Button.class);
        registerChild(CHILD_ST_INCLUDE_GCDSUM_START, StaticTextField.class);
        registerChild(CHILD_ST_INCLUDE_GCDSUM_END, StaticTextField.class);

        //***** Change by NBC/PP Implementation Team - GCD - End *****//
        //PPI start
        registerChild(CHILD_STIMPROVEDVALUE, StaticTextField.class);
        registerChild(CHILD_TXDEALPURPOSETYPEHIDDENSTART, TextField.class);
        registerChild(CHILD_TXDEALPURPOSETYPEHIDDENEND, TextField.class);
        //PPI end

        /***************MCM Impl team changes starts - XS_16.8 *******************/

        registerChild(CHILD_STREFIADDITIONALINFORMATION, StaticTextField.class);
        registerChild(CHILD_STREFEXISTINGMTGNUMBER, StaticTextField.class);

        /***************MCM Impl team changes ends - XS_16.8 *********************/

        /***************MCM Impl team changes starts - XS_16.7***********************************/
    registerChild(CHILD_STINCLUDECOMPONENTINFOSECTION_START, StaticTextField.class);
    registerChild(CHILD_STINCLUDECOMPONENTINFOSECTION_END, StaticTextField.class);
        registerChild(CHILD_STTOTALAMOUNT, StaticTextField.class);
        registerChild(CHILD_STTOTALCASHBACKAMOUNT, StaticTextField.class);
        registerChild(CHILD_STUNALLOCATEDAMOUNT,StaticTextField.class);
        registerChild(CHILD_REPEATEDMORTGAGECOMPONENTS, pgDealSummaryComponentMortgageTiledView.class);
        registerChild(CHILD_REPEATEDLOCCOMPONENTS, pgDealSummaryComponentLocTiledView.class);
        registerChild(CHILD_REPEATEDLOANCOMPONENTS, pgDealSummaryComponentLoanTiledView.class);
        registerChild(CHILD_REPEATEDCREDITCARDCOMPONENTS, pgDealSummaryComponentCreditCardTiledView.class);
        registerChild(CHILD_REPEATEDOVERDRAFTCOMPONENTS, pgDealSummaryComponentOverDraftTiledView.class);
    registerChild(CHILD_BTCOMPONENTDETAILS, Button.class);
        /************************MCM Impl team changes ends - XS_16.7***********************************/
        registerChild(CHILD_STQUALIFYRATEEDIT, StaticTextField.class);
    }
    
    public StaticTextField getStQualifyRateEdit() {
        return (StaticTextField) getChild(CHILD_STQUALIFYRATEEDIT);
    }

    //***** Change by NBC/PP Implementation Team - GCD - Start *****//
    /** Returns the <code>txCreditDecisionStatus</code> child View component */
    public StaticTextField getTxCreditDecisionStatusChild() {
        return (StaticTextField)getChild(CHILD_TX_CREDIT_DECISION_STATUS);
    }

    /** Returns the <code>txCreditDecision</code> child View component */
    public StaticTextField getTxCreditDecisionChild() {
        return (StaticTextField)getChild(CHILD_TX_CREDIT_DECISION);
    }

    /** Returns the <code>txGlobalCreditBureauScore</code> child View component */
    public StaticTextField getTxGlobalCreditBureauScoreChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_CREDIT_BUREAU_SCORE);
    }

    /** Returns the <code>txGlobalRiskRating</code> child View component */
    public StaticTextField getTxGlobalRiskRatingChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_RISK_RATING);
    }

    /** Returns the <code>txGlobalInternalScore</code> child View component */
    public StaticTextField getTxGlobalInternalScoreChild() {
        return (StaticTextField)getChild(CHILD_TX_GLOBAL_INTERNAL_SCORE);
    }

    /** Returns the <code>txMIStatus</code> child View component */
    public StaticTextField getTxMIStatusChild() {
        return (StaticTextField)getChild(CHILD_TX_MISTATUS);
    }

    /** Returns the <code>btCreditDecisionPG</code> child View component */
    public Button getBtCreditDecisionPGChild() {
        return (Button)getChild(CHILD_BT_CREDIT_DECISION_PG);
    }

    /** Returns the <code>stIncludeGCDSumStart</code> child View component */
    public StaticTextField getStIncludeGCDSumStartChild() {
        return (StaticTextField)getChild(CHILD_ST_INCLUDE_GCDSUM_START);
    }

    /** Returns the <code>stIncludeGCDSumEnd</code> child View component */
    public StaticTextField getStIncludeGCDSumEndChild() {
        return (StaticTextField)getChild(CHILD_ST_INCLUDE_GCDSUM_END);
    }

//  ***** Change by NBC/PP Implementation Team - GCD - End *****//


//  ***** Change by NBC/PP Implementation Team - GCD - Start *****//	
//  GCD Summary data retrieval Models Begin

    public doAdjudicationResponseBNCModel getdoAdjudicationResponseBNCModel ()
    {
        if(doAdjudicationResponseBNCModel == null)
        {
            doAdjudicationResponseBNCModel = (doAdjudicationResponseBNCModel)getModel(doAdjudicationResponseBNCModel.class);
        }
        return doAdjudicationResponseBNCModel;
    }

    public void setdoGCDSummarySectionModel (doAdjudicationResponseBNCModel model)
    {
        doAdjudicationResponseBNCModel = model;
    }

//  GCD Summary Data Retrieval Models End
//  ***** Change by NBC/PP Implementation Team - GCD - End *****//



    ////////////////////////////////////////////////////////////////////////////////
    // Model management methods
    ////////////////////////////////////////////////////////////////////////////////

    /**
   *@version 1.4 10/06/2008 XS_16.7  <br>
   *          Author:MCM Impl Team <br>
   *          Change: <br>
   *          Added new model doComponentSummaryModel <br>
     *
     */
    public Model[] getWebActionModels(int executionType)
    {
        List modelList = new ArrayList();

        switch (executionType)
        {
        case MODEL_TYPE_RETRIEVE:
            modelList.add(getdoMainViewBorrowerModel());
            ;
            modelList.add(getdoMainViewDealModel());
            ;
            modelList.add(getdoMainViewBridgeModel());
            ;
            modelList.add(getdoMainViewPrimaryPropertyModel());
            ;
            modelList.add(getdoMainViewMortgageInsurerModel());
            ;
            modelList.add(getdoDealSummarySnapShotModel());
            ;
            modelList.add(getdoMainViewEscrowModel());
            ;
            // ***** Change by NBC/PP Implementation Team - GCD - Start

            modelList.add(getdoAdjudicationResponseBNCModel());

            // ***** Change by NBC/PP Implementation Team - GCD - END
/**********************MCM Impl Team XS_16.7 **************************/
            modelList.add(getDoComponentSummaryModel());
/**********************MCM Impl Team XS_16.7 **************************/
            break;

        case MODEL_TYPE_UPDATE:
            ;

            break;

        case MODEL_TYPE_DELETE:
            ;

            break;

        case MODEL_TYPE_INSERT:
            ;

            break;

        case MODEL_TYPE_EXECUTE:
            ;

            break;
        }

        return (Model[]) modelList.toArray(new Model[0]);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // View flow control methods
    ////////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        //--Release2.1--//
        //Populate all ComboBoxes manually here
        //--> By Billy 05Nov2002
        //Setup Options for cbPageNames
        //cbPageNamesOptions..populate(getRequestContext());
        int langId = handler.getTheSessionState().getLanguageId();
        cbPageNamesOptions.populate(getRequestContext(), langId);

        //Setup NoneSelected Label for cbPageNames
        CHILD_CBPAGENAMES_NONSELECTED_LABEL =
            BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL",
                    handler.getTheSessionState().getLanguageId());

        //Check if the cbPageNames is already created
        if (getCbPageNames() != null)
        {
            getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
        }

        //=====================================
        handler.pageSaveState();
        super.beginDisplay(event);
    }

    /**
     *
     *
     */
    public boolean beforeModelExecutes(Model model, int executionContext)
    {
        // The following code block was migrated from the
        // this_onBeforeDataObjectExecuteEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.setupBeforePageGeneration();
        handler.pageSaveState();

        return super.beforeModelExecutes(model, executionContext);
    }

    /**
     *
     *
     */
    public void afterModelExecutes(Model model, int executionContext)
    {
        // This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);
    }

    /**
     *
     *
     */
    public void afterAllModelsExecute(int executionContext)
    {
        // This is the analog of NetDynamics
        // this_onAfterAllDataObjectsExecuteEvent
        super.afterAllModelsExecute(executionContext);

        // The following code block was migrated from the
        // this_onBeforeRowDisplayEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.populatePageDisplayFields();
        handler.pageSaveState();
    }

    /**
     *
     *
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception)
    throws ModelControlException
    {
        // This is the analog of NetDynamics this_onDataObjectErrorEvent
        super.onModelError(model, executionContext, exception);
    }

    //-- ========== DJ#725 Starts ========== --//
    //-- By Neil at Nov/29/2004

    /**
     *
     */
    public StaticTextField getStContactEmailAddress()
    {
        return (StaticTextField) getChild(CHILD_STCONTACTEMAILADDRESS);
    }

    /**
     *
     * @return
     */
    public StaticTextField getStPsDescription()
    {
        return (StaticTextField) getChild(CHILD_STPSDESCRIPTION);
    }

    /**
     *
     * @return
     */
    public StaticTextField getStSystemTypeDescription()
    {
        return (StaticTextField) getChild(CHILD_STSYSTEMTYPEDESCRIPTION);
    }

    //-- ========== DJ#725 Ends ========== --//

    /**
     *
     *
     */
    public StaticTextField getStPageLabel()
    {
        return (StaticTextField) getChild(CHILD_STPAGELABEL);
    }

    /**
     *
     *
     */
    public StaticTextField getStCompanyName()
    {
        return (StaticTextField) getChild(CHILD_STCOMPANYNAME);
    }

    /**
     *
     *
     */
    public StaticTextField getStTodayDate()
    {
        return (StaticTextField) getChild(CHILD_STTODAYDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStUserNameTitle()
    {
        return (StaticTextField) getChild(CHILD_STUSERNAMETITLE);
    }

    /**
     *
     *
     */
    public HiddenField getDetectAlertTasks()
    {
        return (HiddenField) getChild(CHILD_DETECTALERTTASKS);
    }

    /**
     *
     *
     */
    public HiddenField getSessionUserId()
    {
        return (HiddenField) getChild(CHILD_SESSIONUSERID);
    }

    /**
     *
     *
     */
    public TextField getTbDealId()
    {
        return (TextField) getChild(CHILD_TBDEALID);
    }

    /**
     *
     *
     */
    public ComboBox getCbPageNames()
    {
        return (ComboBox) getChild(CHILD_CBPAGENAMES);
    }

    //--DJ_Ticket661--end--//

    /**
     *
     *
     */
    public void handleBtProceedRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the btProceed_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleGoPage();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtProceed()
    {
        return (Button) getChild(CHILD_BTPROCEED);
    }

    /**
     *
     *
     */
    public void handleHref1Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href1_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(0);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref1()
    {
        return (HREF) getChild(CHILD_HREF1);
    }

    /**
     *
     *
     */
    public void handleHref2Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href2_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(1);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref2()
    {
        return (HREF) getChild(CHILD_HREF2);
    }

    /**
     *
     *
     */
    public void handleHref3Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href3_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(2);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref3()
    {
        return (HREF) getChild(CHILD_HREF3);
    }

    /**
     *
     *
     */
    public void handleHref4Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href4_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(3);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref4()
    {
        return (HREF) getChild(CHILD_HREF4);
    }

    /**
     *
     *
     */
    public void handleHref5Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href5_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(4);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref5()
    {
        return (HREF) getChild(CHILD_HREF5);
    }

    /**
     *
     *
     */
    public void handleHref6Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href6_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(5);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref6()
    {
        return (HREF) getChild(CHILD_HREF6);
    }

    /**
     *
     *
     */
    public void handleHref7Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href7_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(6);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref7()
    {
        return (HREF) getChild(CHILD_HREF7);
    }

    /**
     *
     *
     */
    public void handleHref8Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href8_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(7);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref8()
    {
        return (HREF) getChild(CHILD_HREF8);
    }

    /**
     *
     *
     */
    public void handleHref9Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href9_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(8);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref9()
    {
        return (HREF) getChild(CHILD_HREF9);
    }

    /**
     *
     *
     */
    public void handleHref10Request(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the Href10_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleOpenDialogLink(9);
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getHref10()
    {
        return (HREF) getChild(CHILD_HREF10);
    }

    /**
     *
     *
     */
    public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // changePasswordHref_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleChangePassword();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getChangePasswordHref()
    {
        return (HREF) getChild(CHILD_CHANGEPASSWORDHREF);
    }

    //// Two new methods providing the business logic for the new link to
    // toggle language.
    //// When touched this link should reload the page in opposite language
    // (french versus english)
    //// and set this new session value.

    /**
     *
     *
     */
    public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this, true);
        handler.handleToggleLanguage();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public HREF getToggleLanguageHref()
    {
        return (HREF) getChild(CHILD_TOGGLELANGUAGEHREF);
    }

    /**
     *
     *
     */
    public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btWorkQueueLink_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleDisplayWorkQueue();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtWorkQueueLink()
    {
        return (Button) getChild(CHILD_BTWORKQUEUELINK);
    }

    /**
     *
     *
     */
    public void handleBtToolHistoryRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btToolHistory_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleDisplayDealHistory();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtToolHistory()
    {
        return (Button) getChild(CHILD_BTTOOLHISTORY);
    }

    /**
     *
     *
     */
    public void handleBtTooNotesRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the btTooNotes_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleDisplayDealNotes();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtTooNotes()
    {
        return (Button) getChild(CHILD_BTTOONOTES);
    }

    /**
     *
     *
     */
    public void handleBtToolSearchRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btToolSearch_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleDealSearch();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtToolSearch()
    {
        return (Button) getChild(CHILD_BTTOOLSEARCH);
    }

    /**
     *
     *
     */
    public void handleBtToolLogRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the btToolLog_onWebEvent
        // method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleSignOff();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtToolLog()
    {
        return (Button) getChild(CHILD_BTTOOLLOG);
    }

    /**
     *
     *
     */
    public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btPrevTaskPage_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handlePrevTaskPage();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtPrevTaskPage()
    {
        return (Button) getChild(CHILD_BTPREVTASKPAGE);
    }

    /**
     *
     *
     */
    public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // btPrevTaskPage_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.generatePrevTaskPage();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    /**
     *
     *
     */
    public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btNextTaskPage_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleNextTaskPage();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtNextTaskPage()
    {
        return (Button) getChild(CHILD_BTNEXTTASKPAGE);
    }

    /**
     *
     *
     */
    public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // btNextTaskPage_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.generateNextTaskPage();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }


    /**
     * CR02a
     */
    public String endBtDeclineLetterDisplay(ChildContentDisplayEvent event)
    {

        logger = SysLog.getSysLogger("DealSummary");

        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean declineDisplay = false;

        logger.debug("--------Anu-----@testing ="+handler.getTheSessionState().getDealInstitutionId());

        declineDisplay = (PropertiesCache.getInstance().
                getProperty(handler.getTheSessionState().getDealInstitutionId(),"mosApp.MosSystem.dealresolution.declineletter", "N")).equals("Y");

        if (declineDisplay == true)
        { 
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    /**
     *
     *
     */
    public void handleBtPrintDealSummaryRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btPrintDealSummary_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handlePrintDealSummary();
        handler.postHandlerProtocol();
    }
 /*************************MCM Impl team Starts XS_16.7*******************************/
  /**
   *
   *
   */
public Button getBtComponentDeatails()
    {
        return (Button) getChild(CHILD_BTCOMPONENTDETAILS);
    }
    /**
	 * This method navigates the user to the Components Details screen on clicking the Components Details Button.
     * 
     * @param RequestInvocationEvent event
     * 
	 *  @version 1.0 10-06-2008 XS_16.7 Initial version
     */
    public void handleBtComponentDetailsRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {

        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleGoToComponentDetailsPage();
        handler.postHandlerProtocol();
    }
/*************************MCM Impl team Starts XS_16.7*******************************/

    /**
     *
     *
     */
    public Button getBtPrintDealSummary()
    {
        return (Button) getChild(CHILD_BTPRINTDEALSUMMARY);
    }

  

    /**
     *
     *
     */
    public void handleBtPrintDealSummaryLiteRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btPrintDealSummaryLite_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handlePrintDealSummaryLite();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtPrintDealSummaryLite()
    {
        return (Button) getChild(CHILD_BTPRINTDEALSUMMARYLITE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPrevTaskPageLabel()
    {
        return (StaticTextField) getChild(CHILD_STPREVTASKPAGELABEL);
    }

    /**
     *
     *
     */
    public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.generatePrevTaskPage();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    /**
     *
     *
     */
    public void handleBtPartySummaryRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btPartySummary_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
//      handler.handleGenerateDeclineLetter();
        handler.handleGoToPartySummaryPage();
        handler.postHandlerProtocol();
    }

    /**
     * handler method for decline letter / rate bonus letter button
     * @param event RequestInvocationEvent
     * @throws ServletException
     * @throws IOException
     */
    public void handleBtDeclineLetterRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleGenerateDeclineLetter();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtPartySummary()
    {
        return (Button) getChild(CHILD_BTPARTYSUMMARY);
    }

    /**
     * Returns the instance of the Button object representing the decline letter button
     * @return Button
     */
    public Button getBtDeclineLetter()
    {
        return (Button) getChild(CHILD_BTDECLINELETTER);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedDownPaymentsTiledView getRepeatedDownPayments()
    {
        return (pgDealSummaryRepeatedDownPaymentsTiledView) getChild(CHILD_REPEATEDDOWNPAYMENTS);
    }

    //--DJ_LDI_CR--start--//

    /**
     *
     *
     */
    public pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView getRepeatedLifeDisabilityInsurance()
    {
        return (pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView) getChild(CHILD_REPEATEDLIFEDISABILITYINSURANCE);
    }
    /***************MCM Impl team changes starts - XS_16.7*******************/
    public pgDealSummaryComponentMortgageTiledView getRepeatedComponentMortgage() {
        return (pgDealSummaryComponentMortgageTiledView) getChild(CHILD_REPEATEDMORTGAGECOMPONENTS);
    }

    public pgDealSummaryComponentLocTiledView getRepeatedComponentLoc() {
        return (pgDealSummaryComponentLocTiledView) getChild(CHILD_REPEATEDLOCCOMPONENTS);
    }

    public pgDealSummaryComponentLoanTiledView getRepeatedComponentLoan() {
        return (pgDealSummaryComponentLoanTiledView) getChild(CHILD_REPEATEDLOANCOMPONENTS);
    }

    public pgDealSummaryComponentCreditCardTiledView getRepeatedComponentCreditCard() {
        return (pgDealSummaryComponentCreditCardTiledView) getChild(CHILD_REPEATEDCREDITCARDCOMPONENTS);
    }

    public pgDealSummaryComponentOverDraftTiledView getRepeatedComponentOverDraft() {
        return (pgDealSummaryComponentOverDraftTiledView) getChild(CHILD_REPEATEDOVERDRAFTCOMPONENTS);
    }

    public StaticTextField getStIncludeComponentDetailsButtonStart() {
        return (StaticTextField) getChild(CHILD_STINCLUDE_COMPONENTDETAIL_START);
    }

    public StaticTextField getStIncludeComponentDetailsButtonEnd() {
        return (StaticTextField) getChild(CHILD_STINCLUDE_COMPONENTDETAIL_END);
    }

    public doComponentSummaryModel getDoComponentSummaryModel() {
        if (doComponentSummaryModel == null) {
            doComponentSummaryModel = (doComponentSummaryModel) getModel(doComponentSummaryModel.class);
        }

        return doComponentSummaryModel;
    }

    public StaticTextField getStTotalAmount() {
        return (StaticTextField) getChild(CHILD_STTOTALAMOUNT);
    }

    public StaticTextField getStTotalCashBackAmount() {
        return (StaticTextField) getChild(CHILD_STTOTALCASHBACKAMOUNT);
    }

    public StaticTextField getStUnAllocatedAmount() {
        return (StaticTextField) getChild(CHILD_STUNALLOCATEDAMOUNT);
    }

	public StaticTextField getStIncludeComponentInfoSectionEnd() {
		return (StaticTextField) getChild(CHILD_STINCLUDECOMPONENTINFOSECTION_END);
    }

	public StaticTextField getStIncludeComponentInfoSectionStart() {
		return (StaticTextField) getChild(CHILD_STINCLUDECOMPONENTINFOSECTION_START);
    }
    /** *************MCM Impl team changes ends - XS_16.7****************** */

    /**
     *
     *
     */
    public StaticTextField getStIncludeLifeDisSectionStart()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDELIFEDISSECTIONSTART);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeLifeDisSectionEnd()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDELIFEDISSECTIONEND);
    }

    //--DJ_LDI_CR--end--//

    /**
     *
     *
     */
    public pgDealSummaryRepeatedEscrowPaymentsTiledView getRepeatedEscrowPayments()
    {
        return (pgDealSummaryRepeatedEscrowPaymentsTiledView) getChild(CHILD_REPEATEDESCROWPAYMENTS);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedBorrowerDetailsTiledView getRepeatedBorrowerDetails()
    {
        return (pgDealSummaryRepeatedBorrowerDetailsTiledView) getChild(CHILD_REPEATEDBORROWERDETAILS);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedGDSTiledView getRepeatedGDS()
    {
        return (pgDealSummaryRepeatedGDSTiledView) getChild(CHILD_REPEATEDGDS);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedTDSTiledView getRepeatedTDS()
    {
        return (pgDealSummaryRepeatedTDSTiledView) getChild(CHILD_REPEATEDTDS);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedIncomeDetailsTiledView getRepeatedIncomeDetails()
    {
        return (pgDealSummaryRepeatedIncomeDetailsTiledView) getChild(CHILD_REPEATEDINCOMEDETAILS);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedLiabDetailsTiledView getRepeatedLiabDetails()
    {
        return (pgDealSummaryRepeatedLiabDetailsTiledView) getChild(CHILD_REPEATEDLIABDETAILS);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedAssetDetailsTiledView getRepeatedAssetDetails()
    {
        return (pgDealSummaryRepeatedAssetDetailsTiledView) getChild(CHILD_REPEATEDASSETDETAILS);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedPropertyDetailsTiledView getRepeatedPropertyDetails()
    {
        return (pgDealSummaryRepeatedPropertyDetailsTiledView) getChild(CHILD_REPEATEDPROPERTYDETAILS);
    }

    /**
     *
     *
     */
    public StaticTextField getStLastName()
    {
        return (StaticTextField) getChild(CHILD_STLASTNAME);
    }

    /**
    *
    *
    */
    public StaticTextField getStSuffix()
    {
        return (StaticTextField) getChild(CHILD_STSUFFIX);
    }
    /**
     *
     *
     */
    public StaticTextField getStMiddleInitial()
    {
        return (StaticTextField) getChild(CHILD_STMIDDLEINITIAL);
    }

    /**
     *
     *
     */
    public StaticTextField getStFirstName()
    {
        return (StaticTextField) getChild(CHILD_STFIRSTNAME);
    }

    /**
     *
     *
     */
    public StaticTextField getStLanguagePreference()
    {
        return (StaticTextField) getChild(CHILD_STLANGUAGEPREFERENCE);
    }

    /**
     *
     *
     */
    public StaticTextField getStExistingClient()
    {
        return (StaticTextField) getChild(CHILD_STEXISTINGCLIENT);
    }

    /**
     *
     *
     */
    public String endStExistingClientDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stExistingClient_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStReferenceClientNumber()
    {
        return (StaticTextField) getChild(CHILD_STREFERENCECLIENTNUMBER);
    }

    /**
     *
     *
     */
    public StaticTextField getStUnderwriter()
    {
        return (StaticTextField) getChild(CHILD_STUNDERWRITER);
    }

    /**
     *
     *
     */
    public String endStUnderwriterDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stUnderwriter_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setUnderWriterName(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStAdministrator()
    {
        return (StaticTextField) getChild(CHILD_STADMINISTRATOR);
    }

    /**
     *
     *
     */
    public String endStAdministratorDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stAdministrator_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setUnderWriterName(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStFunder()
    {
        return (StaticTextField) getChild(CHILD_STFUNDER);
    }

    /**
     *
     *
     */
    public String endStFunderDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stFunder_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setUnderWriterName(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStHomePhone()
    {
        return (StaticTextField) getChild(CHILD_STHOMEPHONE);
    }

    /**
     *
     *
     */
    public StaticTextField getStWorkPhone()
    {
        return (StaticTextField) getChild(CHILD_STWORKPHONE);
    }

    /**
     *
     *
     */
    public StaticTextField getStNoOfBorrowers()
    {
        return (StaticTextField) getChild(CHILD_STNOOFBORROWERS);
    }

    /**
     *
     *
     */
    public StaticTextField getStNoOfGuarantors()
    {
        return (StaticTextField) getChild(CHILD_STNOOFGUARANTORS);
    }

    /**
     *
     *
     */
    public StaticTextField getStBranch()
    {
        return (StaticTextField) getChild(CHILD_STBRANCH);
    }

    /**
     *
     *
     */
    public boolean beginStBranchDisplay(ChildDisplayEvent event)
    {
        // The following code block was migrated from the
        // stBranch_onBeforeDisplayEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.setBranchName(event.getChildName());
        handler.pageSaveState();

        return true;
    }

    /**
     *
     *
     */
    public StaticTextField getStDITotalLoanAmount()
    {
        return (StaticTextField) getChild(CHILD_STDITOTALLOANAMOUNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStPaymentTerm()
    {
        return (StaticTextField) getChild(CHILD_STPAYMENTTERM);
    }

    /**
     *
     *
     */
    public String endStPaymentTermDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stPaymentTerm_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setMonthsToYearsMonths(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStPaymentTermDesc()
    {
        return (StaticTextField) getChild(CHILD_STPAYMENTTERMDESC);
    }

    /**
     *
     *
     */
    public StaticTextField getStEffectiveAmortization()
    {
        return (StaticTextField) getChild(CHILD_STEFFECTIVEAMORTIZATION);
    }

    /**
     *
     *
     */
    public String endStEffectiveAmortizationDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stEffectiveAmortization_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setMonthsToYearsMonths(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStPaymentFrequency()
    {
        return (StaticTextField) getChild(CHILD_STPAYMENTFREQUENCY);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedLTV()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDLTV);
    }

    /**
     *
     *
     */
    public String endStCombinedLTVDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stCombinedLTV_onBeforeDisplayEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setRateToPercentage(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStRateCode()
    {
        return (StaticTextField) getChild(CHILD_STRATECODE);
    }

    /**
     *
     *
     */
    public boolean beginStRateCodeDisplay(ChildDisplayEvent event)
    {
        // The following code block was migrated from the
        // stRateCode_onBeforeDisplayEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rt = handler.setRateCode(event.getChildName());
        handler.pageSaveState();

        return (rt);
    }

    /**
     *
     *
     */
    public StaticTextField getStRateDate()
    {
        return (StaticTextField) getChild(CHILD_STRATEDATE);
    }
    
    /**
    *
    *
    */
    public StaticTextField getStFinancingDate()
    {
        return (StaticTextField) getChild(CHILD_STFINANCINGDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPostedRate()
    {
        return (StaticTextField) getChild(CHILD_STPOSTEDRATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStNetRate()
    {
        return (StaticTextField) getChild(CHILD_STNETRATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStRateLockedIn()
    {
        return (StaticTextField) getChild(CHILD_STRATELOCKEDIN);
    }

    /**
     *
     *
     */
    public String endStRateLockedInDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stRateLockedIn_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStDiscount()
    {
        return (StaticTextField) getChild(CHILD_STDISCOUNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStPremium()
    {
        return (StaticTextField) getChild(CHILD_STPREMIUM);
    }

    /**
     *
     *
     */
    public StaticTextField getStBuyDownRate()
    {
        return (StaticTextField) getChild(CHILD_STBUYDOWNRATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStDISpecialFeature()
    {
        return (StaticTextField) getChild(CHILD_STDISPECIALFEATURE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPreAppPricePurchase()
    {
        return (StaticTextField) getChild(CHILD_STPREAPPPRICEPURCHASE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPIPayment()
    {
        return (StaticTextField) getChild(CHILD_STPIPAYMENT);
    }

    /**
     *
     *
     */
    public StaticTextField getStAdditionalPrincipalPayemt()
    {
        return (StaticTextField) getChild(CHILD_STADDITIONALPRINCIPALPAYEMT);
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalEscrowPayment()
    {
        return (StaticTextField) getChild(CHILD_STTOTALESCROWPAYMENT);
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalPayment()
    {
        return (StaticTextField) getChild(CHILD_STTOTALPAYMENT);
    }

    /**
     *
     *
     */
    public StaticTextField getStFirstPaymentDate()
    {
        return (StaticTextField) getChild(CHILD_STFIRSTPAYMENTDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStMaturityDate()
    {
        return (StaticTextField) getChild(CHILD_STMATURITYDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStSecondMortgage()
    {
        return (StaticTextField) getChild(CHILD_STSECONDMORTGAGE);
    }

    /**
     *
     *
     */
    public String endStSecondMortgageDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stSecondMortgage_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStEquityAvailable()
    {
        return (StaticTextField) getChild(CHILD_STEQUITYAVAILABLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStDIDealPurpose()
    {
        return (StaticTextField) getChild(CHILD_STDIDEALPURPOSE);
    }

    /**
     *
     *
     */
    public StaticTextField getStRequiredDownPayment()
    {
        return (StaticTextField) getChild(CHILD_STREQUIREDDOWNPAYMENT);
    }

    /**
     *
     *
     */
    public StaticTextField getStDILOB()
    {
        return (StaticTextField) getChild(CHILD_STDILOB);
    }

    /**
     *
     *
     */
    public StaticTextField getStDIDealType()
    {
        return (StaticTextField) getChild(CHILD_STDIDEALTYPE);
    }

    /**
     *
     *
     */
    public StaticTextField getStProduct()
    {
        return (StaticTextField) getChild(CHILD_STPRODUCT);
    }

    /**
     *
     *
     */
    public boolean beginStProductDisplay(ChildDisplayEvent event)
    {
        // The following code block was migrated from the
        // stProduct_onBeforeDisplayEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rt = handler.setProductName(event.getChildName());
        handler.pageSaveState();

        return (rt);
    }

    /**
     *
     *
     */
    public StaticTextField getStPrePaymentOption()
    {
        return (StaticTextField) getChild(CHILD_STPREPAYMENTOPTION);
    }

    /**
     *
     *
     */
    public StaticTextField getStReferenceType()
    {
        return (StaticTextField) getChild(CHILD_STREFERENCETYPE);
    }

    /**
     *
     *
     */
    public String endStReferenceTypeDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stReferenceType_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rt = handler.setReferenceType();
        handler.pageSaveState();

        return (rt);
    }

    /**
     *
     *
     */
    public StaticTextField getStReferenceNo()
    {
        return (StaticTextField) getChild(CHILD_STREFERENCENO);
    }

    /**
     *
     *
     */
    public StaticTextField getStEstimatedClosingDate()
    {
        return (StaticTextField) getChild(CHILD_STESTIMATEDCLOSINGDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStActualClosingDate()
    {
        return (StaticTextField) getChild(CHILD_STACTUALCLOSINGDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPrivilegePaymentOption()
    {
        return (StaticTextField) getChild(CHILD_STPRIVILEGEPAYMENTOPTION);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeFinancingProgramStart()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDEFINANCINGPROGRAMSTART);
    }

    /**
     *
     *
     */
    public StaticTextField getStFinancingProgram()
    {
        return (StaticTextField) getChild(CHILD_STFINANCINGPROGRAM);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeFinancingProgramEnd()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDEFINANCINGPROGRAMEND);
    }
/*
    public StaticTextField getStDISource()
    {
        return (StaticTextField) getChild(CHILD_STDISOURCE);
    }

    public StaticTextField getStDISourceFirm()
    {
        return (StaticTextField) getChild(CHILD_STDISOURCEFIRM);
    }

    public StaticTextField getStDISourceAddress()
    {
        return (StaticTextField) getChild(CHILD_STDISOURCEADDRESS);
    }

    public StaticTextField getStDISourceContactPhone()
    {
        return (StaticTextField) getChild(CHILD_STDISOURCECONTACTPHONE);
    }

    public StaticTextField getStDISourceContactPhoneExt()
    {
        return (StaticTextField) getChild(CHILD_STDISOURCECONTACTPHONEEXT);
    }*/
	//Submission Agent
    public pgDealSummaryRepeatedSOBTiledView getRepeatedSOB()
    {
        return (pgDealSummaryRepeatedSOBTiledView) getChild(CHILD_REPEATEDSOB);
    }

    /**
     *
     *
     */
    public String endStDISourceContactPhoneExtDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stDISourceContactPhoneExt_onBeforeDisplayEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setXPhoneExtCommon(event.getContent());
        handler.pageSaveState();

        return rc;
    }

/*    public StaticTextField getStDISourceContactFax()
    {
        return (StaticTextField) getChild(CHILD_STDISOURCECONTACTFAX);
    }
*/
    /**
     *
     *
     */
    public StaticTextField getStReferenceSourceApp()
    {
        return (StaticTextField) getChild(CHILD_STREFERENCESOURCEAPP);
    }

    /**
     *
     *
     */
    public StaticTextField getStCrossSell()
    {
        return (StaticTextField) getChild(CHILD_STCROSSSELL);
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalPurchasePrice()
    {
        return (StaticTextField) getChild(CHILD_STTOTALPURCHASEPRICE);
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalEstimatedValue()
    {
        return (StaticTextField) getChild(CHILD_STTOTALESTIMATEDVALUE);
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalActualAppraisedValue()
    {
        return (StaticTextField) getChild(CHILD_STTOTALACTUALAPPRAISEDVALUE);
    }

    /**
     *
     *
     */
    public StaticTextField getStLender()
    {
        return (StaticTextField) getChild(CHILD_STLENDER);
    }

    /**
     *
     *
     */
    public StaticTextField getStInvestor()
    {
        return (StaticTextField) getChild(CHILD_STINVESTOR);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgePurpose()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGEPURPOSE);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeLender()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGELENDER);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeProduct()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGEPRODUCT);
    }


//  PPI start
    /**
     *
     *
     */
    public StaticTextField getStImprovedValue()
    {
        return (StaticTextField) getChild(CHILD_STIMPROVEDVALUE);
    }

    /**
     *
     *
     */
    public TextField getTxDealPurposeTypeHiddenStart()
    {
        return (TextField) getChild(CHILD_TXDEALPURPOSETYPEHIDDENSTART);
    }

    /**
     *
     *
     */
    public TextField getTxDealPurposeTypeHiddenEnd()
    {
        return (TextField) getChild(CHILD_TXDEALPURPOSETYPEHIDDENEND);
    }

//  PPI end

    /**
     *
     *
     */
    public boolean beginStBridgeProductDisplay(ChildDisplayEvent event)
    {
        // The following code block was migrated from the
        // stBridgeProduct_onBeforeDisplayEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.setBridgeProductName(event.getChildName());
        handler.pageSaveState();

        return true;
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgePaymentTermDesc()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGEPAYMENTTERMDESC);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeRateCode()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGERATECODE);
    }

    /**
     *
     *
     */
    public boolean beginStBridgeRateCodeDisplay(ChildDisplayEvent event)
    {
        // The following code block was migrated from the
        // stBridgeRateCode_onBeforeDisplayEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);
        handler.setBridgeRateCode(event.getChildName());
        handler.pageSaveState();

        return true;
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgePostedInterestRate()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGEPOSTEDINTERESTRATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeRateDate()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGERATEDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeDiscount()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGEDISCOUNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgePremium()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGEPREMIUM);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeNetRate()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGENETRATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeLoanAmount()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGELOANAMOUNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeActualPaymentTerm()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGEACTUALPAYMENTTERM);
    }

    /**
     *
     *
     */
    public StaticTextField getStBridgeMaturityDate()
    {
        return (StaticTextField) getChild(CHILD_STBRIDGEMATURITYDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStBeginHideRefiSection()
    {
        return (StaticTextField) getChild(CHILD_STBEGINHIDEREFISECTION);
    }

    /**
     *
     *
     */
    public String endStBeginHideRefiSectionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stBeginHideRefiSection_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.generateOrHideBeginRefiSect(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiPurchaseDate()
    {
        return (StaticTextField) getChild(CHILD_STREFIPURCHASEDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiMortgageHolder()
    {
        return (StaticTextField) getChild(CHILD_STREFIMORTGAGEHOLDER);
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiOrigPurchasePrice()
    {
        return (StaticTextField) getChild(CHILD_STREFIORIGPURCHASEPRICE);
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiImprovementValue()
    {
        return (StaticTextField) getChild(CHILD_STREFIIMPROVEMENTVALUE);
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiBlendedAmorization()
    {
        return (StaticTextField) getChild(CHILD_STREFIBLENDEDAMORIZATION);
    }

    /**
     *
     *
     */
    public String endStRefiBlendedAmorizationDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stRefiBlendedAmorization_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiPurpose()
    {
        return (StaticTextField) getChild(CHILD_STREFIPURPOSE);
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiOrigMtgAmount()
    {
        return (StaticTextField) getChild(CHILD_STREFIORIGMTGAMOUNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiOutstandingMtgAmount()
    {
        return (StaticTextField) getChild(CHILD_STREFIOUTSTANDINGMTGAMOUNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStRefiImprovementDesc()
    {
        return (StaticTextField) getChild(CHILD_STREFIIMPROVEMENTDESC);
    }

    /**
     *
     *
     */
    public StaticTextField getStEndHideRefiSection()
    {
        return (StaticTextField) getChild(CHILD_STENDHIDEREFISECTION);
    }

    /**
     *
     *
     */
    public String endStEndHideRefiSectionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stEndHideRefiSection_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.generateOrHideEndRefiSect(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStProgressAdvance()
    {
        return (StaticTextField) getChild(CHILD_STPROGRESSADVANCE);
    }

    /**
     *
     *
     */
    public String endStProgressAdvanceDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stProgressAdvance_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStAdvanceToDateAmt()
    {
        return (StaticTextField) getChild(CHILD_STADVANCETODATEAMT);
    }

    /**
     *
     *
     */
    public StaticTextField getStNextAdvanceAmt()
    {
        return (StaticTextField) getChild(CHILD_STNEXTADVANCEAMT);
    }

    /**
     *
     *
     */
    public StaticTextField getStAdvanceNumber()
    {
        return (StaticTextField) getChild(CHILD_STADVANCENUMBER);
    }

    /**
     *
     *
     */
    public StaticTextField getStCreditScore()
    {
        return (StaticTextField) getChild(CHILD_STCREDITSCORE);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedGDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDGDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombined3YrGds()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINED3YRGDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedTDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDTDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombined3YrTds()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINED3YRTDS);
    }

    //***** Qualify Rate *****//
    /**
     *
     *
     */
    public StaticTextField getStQualifyRate()
    {
        return (StaticTextField) getChild(CHILD_STQUALIFYRATE);
    }
    /**
     *
     *
    */
    public StaticTextField getStQualifyProduct()
    {
        return (StaticTextField) getChild(CHILD_STQUALIFYPRODUCT);
    }
    
    public HiddenField getHdQualifyRateOverride()
    {
        return (HiddenField) getChild(CHILD_HDQUALIFYRATEOVERRIDE);
    }
    /**
     *
     *
    */
    public HiddenField getHdQualifyRateOverrideRate()
    {
        return (HiddenField) getChild(CHILD_HDQUALIFYRATEOVERRIDERATE);
    }
        
    //***** Qualify Rate *****//
    
    /**
     *
     *
     */
    public StaticTextField getStcombinedTotalIncome()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDTOTALINCOME);
    }

    /**
     *
     *
     */
    public StaticTextField getStcombinedTotalLiabilities()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDTOTALLIABILITIES);
    }

    /**
     *
     *
     */
    public StaticTextField getStcombinedTotalAssets()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDTOTALASSETS);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyStreetno()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYSTREETNO);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyStreetname()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYSTREETNAME);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyStreetType()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYSTREETTYPE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyStreetDirection()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYSTREETDIRECTION);
    }
   
    /**
    *
    *
    */
   public StaticTextField getStPropertyUnitNumber()
   {
       return (StaticTextField) getChild(CHILD_STPROPERTYUNITNUMBER);
   }
    /**
     *
     *
     */
    public StaticTextField getStPropertyAddress2()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYADDRESS2);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyCity()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYCITY);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyProvince()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYPROVINCE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyPostalCode()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYPOSTALCODE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyPostalCode1()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYPOSTALCODE1);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyUsage()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYUSAGE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyOccupancy()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYOCCUPANCY);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyType()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYTYPE);
    }

    /**
     *
     *
     */
    public StaticTextField getStNoOfProperties()
    {
        return (StaticTextField) getChild(CHILD_STNOOFPROPERTIES);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyPurchasePrice()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYPURCHASEPRICE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyEstimatedAppraisalValue()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYESTIMATEDAPPRAISALVALUE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyActualApraisedValue()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYACTUALAPRAISEDVALUE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPropertyLandValue()
    {
        return (StaticTextField) getChild(CHILD_STPROPERTYLANDVALUE);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedTotalPropertyExp()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDTOTALPROPERTYEXP);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIIndictorDesc()
    {
        return (StaticTextField) getChild(CHILD_STMIINDICTORDESC);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIRequestStatus()
    {
        return (StaticTextField) getChild(CHILD_STMIREQUESTSTATUS);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIExistingPolicyNum()
    {
        return (StaticTextField) getChild(CHILD_STMIEXISTINGPOLICYNUM);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIPoliceNumber()
    {
        return (StaticTextField) getChild(CHILD_STMIPOLICENUMBER);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIPrequalificationMICertNum()
    {
        return (StaticTextField) getChild(CHILD_STMIPREQUALIFICATIONMICERTNUM);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIType()
    {
        return (StaticTextField) getChild(CHILD_STMITYPE);
    }

    // SEAN DJ SPEC-Progress Advance Type July 25, 2005: The getter methods.
    public StaticTextField getStProgressAdvanceType()
    {
        return (StaticTextField) getChild(CHILD_STPROGRESSADVANCETYPE);
    }
    // SEAN DJ SPEC-PAT END

    /**
     *
     *
     */
    public StaticTextField getStMIInsurer()
    {
        return (StaticTextField) getChild(CHILD_STMIINSURER);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIPayorDesc()
    {
        return (StaticTextField) getChild(CHILD_STMIPAYORDESC);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIUpfront()
    {
        return (StaticTextField) getChild(CHILD_STMIUPFRONT);
    }

    /**
     *
     *
     */
    public String endStMIUpfrontDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stMIUpfront_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStMIPremium()
    {
        return (StaticTextField) getChild(CHILD_STMIPREMIUM);
    }

    /**
     *
     *
     */
    public StaticTextField getStMIRUIntervention()
    {
        return (StaticTextField) getChild(CHILD_STMIRUINTERVENTION);
    }

    /**
     *
     *
     */
    public String endStMIRUInterventionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stMIRUIntervention_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStErrorFlag()
    {
        return (StaticTextField) getChild(CHILD_STERRORFLAG);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealId()
    {
        return (StaticTextField) getChild(CHILD_STDEALID);
    }

    /**
     *
     *
     */
    public StaticTextField getStBorrFirstName()
    {
        return (StaticTextField) getChild(CHILD_STBORRFIRSTNAME);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealStatus()
    {
        return (StaticTextField) getChild(CHILD_STDEALSTATUS);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealStatusDate()
    {
        return (StaticTextField) getChild(CHILD_STDEALSTATUSDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStSourceFirm()
    {
        return (StaticTextField) getChild(CHILD_STSOURCEFIRM);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStCashBackInDollars() {
        return (StaticTextField) getChild(CHILD_STCASHBACKINDOLLARS);
    }

    /**
     * 
     * 
     */
    public StaticTextField getStCashBackInPercentage() {
        return (StaticTextField) getChild(CHILD_STCASHBACKINPERCENTAGE);
    }

    public StaticTextField getStAffiliationProgram() {
        return (StaticTextField) getChild(CHILD_STAFFILIATIONPROGRAM);
    }

    /**
     *
     *
     */
    public StaticTextField getStSource()
    {
        return (StaticTextField) getChild(CHILD_STSOURCE);
    }

    /**
     *
     *
     */
    public StaticTextField getStEstClosingDate()
    {
        return (StaticTextField) getChild(CHILD_STESTCLOSINGDATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStLOB()
    {
        return (StaticTextField) getChild(CHILD_STLOB);
    }

    /**
     *
     *
     */
    public StaticTextField getStSpecialFeature()
    {
        return (StaticTextField) getChild(CHILD_STSPECIALFEATURE);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealType()
    {
        return (StaticTextField) getChild(CHILD_STDEALTYPE);
    }

    /**
     *
     *
     */
    public StaticTextField getStDealPurpose()
    {
        return (StaticTextField) getChild(CHILD_STDEALPURPOSE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPurchasePrice()
    {
        return (StaticTextField) getChild(CHILD_STPURCHASEPRICE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmtTerm()
    {
        return (StaticTextField) getChild(CHILD_STPMTTERM);
    }

    /**
     *
     *
     */
    public StaticTextField getStTotalLoanAmount()
    {
        return (StaticTextField) getChild(CHILD_STTOTALLOANAMOUNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStNextTaskPageLabel()
    {
        return (StaticTextField) getChild(CHILD_STNEXTTASKPAGELABEL);
    }

    /**
     *
     *
     */
    public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stNextTaskPageLabel_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.generateNextTaskPage();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    /**
     *
     *
     */
    public StaticTextField getStTaskName()
    {
        return (StaticTextField) getChild(CHILD_STTASKNAME);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedBorrowerGDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDBORROWERGDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStCombinedBorrowerTDS()
    {
        return (StaticTextField) getChild(CHILD_STCOMBINEDBORROWERTDS);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmGenerate()
    {
        return (StaticTextField) getChild(CHILD_STPMGENERATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmHasTitle()
    {
        return (StaticTextField) getChild(CHILD_STPMHASTITLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmHasInfo()
    {
        return (StaticTextField) getChild(CHILD_STPMHASINFO);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmHasTable()
    {
        return (StaticTextField) getChild(CHILD_STPMHASTABLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmHasOk()
    {
        return (StaticTextField) getChild(CHILD_STPMHASOK);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmTitle()
    {
        return (StaticTextField) getChild(CHILD_STPMTITLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmInfoMsg()
    {
        return (StaticTextField) getChild(CHILD_STPMINFOMSG);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmOnOk()
    {
        return (StaticTextField) getChild(CHILD_STPMONOK);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmMsgs()
    {
        return (StaticTextField) getChild(CHILD_STPMMSGS);
    }

    /**
     *
     *
     */
    public StaticTextField getStPmMsgTypes()
    {
        return (StaticTextField) getChild(CHILD_STPMMSGTYPES);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmGenerate()
    {
        return (StaticTextField) getChild(CHILD_STAMGENERATE);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmHasTitle()
    {
        return (StaticTextField) getChild(CHILD_STAMHASTITLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmHasInfo()
    {
        return (StaticTextField) getChild(CHILD_STAMHASINFO);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmHasTable()
    {
        return (StaticTextField) getChild(CHILD_STAMHASTABLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmTitle()
    {
        return (StaticTextField) getChild(CHILD_STAMTITLE);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmInfoMsg()
    {
        return (StaticTextField) getChild(CHILD_STAMINFOMSG);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmMsgs()
    {
        return (StaticTextField) getChild(CHILD_STAMMSGS);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmMsgTypes()
    {
        return (StaticTextField) getChild(CHILD_STAMMSGTYPES);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmDialogMsg()
    {
        return (StaticTextField) getChild(CHILD_STAMDIALOGMSG);
    }

    /**
     *
     *
     */
    public StaticTextField getStAmButtonsHtml()
    {
        return (StaticTextField) getChild(CHILD_STAMBUTTONSHTML);
    }

    /**
     *
     *
     */
    public StaticTextField getStBeginHideBridgeSection()
    {
        return (StaticTextField) getChild(CHILD_STBEGINHIDEBRIDGESECTION);
    }

    //--Release3.1--begins
    //--by Hiro Mar 31, 2006
    public StaticTextField getStProductType()
    {
        return (StaticTextField) getChild(CHILD_STPRODUCTTYPE);
    }

    public StaticTextField getStCharge()
    {
        return (StaticTextField) getChild(CHILD_STCHARGE);
    }

    public StaticTextField getStRefiProductType()
    {
        return (StaticTextField) getChild(CHILD_STREFIPRODUCTTYPE);
    }

    public StaticTextField getStProgressAdvanceInspectionBy()
    {
        return (StaticTextField) getChild(CHILD_STPROGRESSADVANCEINSPECTIONBY);
    }

    public String endStProgressAdvanceInspectionByDisplay(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());
        String rc = handler.setLenderOrInsurer(event.getContent());
        handler.pageSaveState();

        return rc;
    }	

    public StaticTextField getStSelfDirectedRRSP()
    {
        return (StaticTextField) getChild(CHILD_STSELFDIRECTEDRRSP);
    }

    public String endStSelfDirectedRRSPDisplay(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    public StaticTextField getStCMHCProductTrackerIdentifier()
    {
        return (StaticTextField) getChild(CHILD_STCMHCPRODUCTTRACKERIDENTIFIER);
    }

    public StaticTextField getStLOCRepayment()
    {
        return (StaticTextField) getChild(CHILD_STLOCREPAYMENT);
    }

    public StaticTextField getStRequestStandardService()
    {
        return (StaticTextField) getChild(CHILD_STREQUESTSTANDARDSERVICE);
    }
    /***************MCM Impl team changes starts - XS_16.8 *******************/

    public StaticTextField getStRefiAdditionalInformation()
    {
        return (StaticTextField) getChild(CHILD_STREFIADDITIONALINFORMATION);
    }

    public StaticTextField getStRefExistingMTGNumber()
    {
        return (StaticTextField) getChild(CHILD_STREFEXISTINGMTGNUMBER);
    }

    /***************MCM Impl team changes ends - XS_16.8 *********************/
    public String endStRequestStandardServiceDisplay(
            ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setYesOrNo(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    public StaticTextField getStLOCAmortization()
    {
        return (StaticTextField) getChild(CHILD_STLOCAMORTIZATION);
    }

    public String endStLOCAmortizationDisplay(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setMonthsToYearsMonths(event.getContent());
        handler.pageSaveState();

        return rc;
    }
    public StaticTextField getStLOCInterestOnlyMaturityDate()
    {
        return (StaticTextField) getChild(CHILD_STLOCINTERESTONLYMATURITYDATE);
    }
    // --Release3.1--ends

    /**
     *
     *
     */
    public String endStBeginHideBridgeSectionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stBeginHideBridgeSection_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.generateOrHideBeginBridgeSect();
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStEndHideBridgeSection()
    {
        return (StaticTextField) getChild(CHILD_STENDHIDEBRIDGESECTION);
    }

    /**
     *
     *
     */
    public String endStEndHideBridgeSectionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stEndHideBridgeSection_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.generateOrHideEndBridgeSect();
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStEndHideEscrowSection()
    {
        return (StaticTextField) getChild(CHILD_STENDHIDEESCROWSECTION);
    }

    /**
     *
     *
     */
    public String endStEndHideEscrowSectionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stEndHideEscrowSection_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.generateOrHideEndEscrowSect();
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStBeginHideEscrowSection()
    {
        return (StaticTextField) getChild(CHILD_STBEGINHIDEESCROWSECTION);
    }

    /**
     *
     *
     */
    public String endStBeginHideEscrowSectionDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stBeginHideEscrowSection_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.generateOrHideBeginEscrowSect();
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public HiddenField getHdExecuteEscow()
    {
        return (HiddenField) getChild(CHILD_HDEXECUTEESCOW);
    }

    /**
     *
     *
     */
    public pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView getRepeatedPropertyAddressAndExpenses()
    {
        return (pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView) getChild(CHILD_REPEATEDPROPERTYADDRESSANDEXPENSES);
    }

    /**
     *
     *
     */
    public StaticTextField getStBorrowerWorkPhoneExt()
    {
        return (StaticTextField) getChild(CHILD_STBORROWERWORKPHONEEXT);
    }

    /**
     *
     *
     */
    public String endStBorrowerWorkPhoneExtDisplay(ChildContentDisplayEvent event)
    {
        // The following code block was migrated from the
        // stBorrowerWorkPhoneExt_onBeforeHtmlOutputEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.setXExtension(event.getContent());
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public void handleBtOkMainPageButtonRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        // The following code block was migrated from the
        // btOkMainPageButton_onWebEvent method
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleSubmitStandard();
        handler.postHandlerProtocol();
    }

    /**
     *
     *
     */
    public Button getBtOkMainPageButton()
    {
        return (Button) getChild(CHILD_BTOKMAINPAGEBUTTON);
    }

    /**
     *
     *
     */
    public doMainViewBorrowerModel getdoMainViewBorrowerModel()
    {
        if (doMainViewBorrower == null)
        {
            doMainViewBorrower =
                (doMainViewBorrowerModel) getModel(doMainViewBorrowerModel.class);
        }

        return doMainViewBorrower;
    }

    /**
     *
     *
     */
    public void setdoMainViewBorrowerModel(doMainViewBorrowerModel model)
    {
        doMainViewBorrower = model;
    }

    /**
     *
     *
     */
    public doMainViewDealModel getdoMainViewDealModel()
    {
        if (doMainViewDeal == null)
        {
            doMainViewDeal =
                (doMainViewDealModel) getModel(doMainViewDealModel.class);
        }

        return doMainViewDeal;
    }

    /**
     *
     *
     */
    public void setdoMainViewDealModel(doMainViewDealModel model)
    {
        doMainViewDeal = model;
    }

    /**
     *
     *
     */
    public doMainViewBridgeModel getdoMainViewBridgeModel()
    {
        if (doMainViewBridge == null)
        {
            doMainViewBridge =
                (doMainViewBridgeModel) getModel(doMainViewBridgeModel.class);
        }

        return doMainViewBridge;
    }

    /**
     *
     *
     */
    public void setdoMainViewBridgeModel(doMainViewBridgeModel model)
    {
        doMainViewBridge = model;
    }

    /**
     *
     *
     */
    public doMainViewPrimaryPropertyModel getdoMainViewPrimaryPropertyModel()
    {
        if (doMainViewPrimaryProperty == null)
        {
            doMainViewPrimaryProperty =
                (doMainViewPrimaryPropertyModel) getModel(doMainViewPrimaryPropertyModel.class);
        }

        return doMainViewPrimaryProperty;
    }

    /**
     *
     *
     */
    public void setdoMainViewPrimaryPropertyModel(doMainViewPrimaryPropertyModel model)
    {
        doMainViewPrimaryProperty = model;
    }

    /**
     *
     *
     */
    public doMainViewMortgageInsurerModel getdoMainViewMortgageInsurerModel()
    {
        if (doMainViewMortgageInsurer == null)
        {
            doMainViewMortgageInsurer =
                (doMainViewMortgageInsurerModel) getModel(doMainViewMortgageInsurerModel.class);
        }

        return doMainViewMortgageInsurer;
    }

    /**
     *
     *
     */
    public void setdoMainViewMortgageInsurerModel(doMainViewMortgageInsurerModel model)
    {
        doMainViewMortgageInsurer = model;
    }

    /**
     *
     *
     */
    public doDealSummarySnapShotModel getdoDealSummarySnapShotModel()
    {
        if (doDealSummarySnapShot == null)
        {
            doDealSummarySnapShot =
                (doDealSummarySnapShotModel) getModel(doDealSummarySnapShotModel.class);
        }

        return doDealSummarySnapShot;
    }

    /**
     *
     *
     */
    public void setdoDealSummarySnapShotModel(doDealSummarySnapShotModel model)
    {
        doDealSummarySnapShot = model;
    }

    /**
     *
     *
     */
    public doMainViewEscrowModel getdoMainViewEscrowModel()
    {
        if (doMainViewEscrow == null)
        {
            doMainViewEscrow =
                (doMainViewEscrowModel) getModel(doMainViewEscrowModel.class);
        }

        return doMainViewEscrow;
    }

    /**
     *
     *
     */
    public void setdoMainViewEscrowModel(doMainViewEscrowModel model)
    {
        doMainViewEscrow = model;
    }

    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    public Button getBtActMsg()
    {
        return (Button) getChild(CHILD_BTACTMSG);
    }

    //===========================================
    //--> Addition methods to propulate Href display String
    //--> Test by BILLY 07Aug2002
    public String endHref1Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
        handler.pageSaveState();

        return rc;
    }

    public String endHref2Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
        handler.pageSaveState();

        return rc;
    }

    public String endHref3Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
        handler.pageSaveState();

        return rc;
    }

    public String endHref4Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
        handler.pageSaveState();

        return rc;
    }

    public String endHref5Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
        handler.pageSaveState();

        return rc;
    }

    public String endHref6Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
        handler.pageSaveState();

        return rc;
    }

    public String endHref7Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
        handler.pageSaveState();

        return rc;
    }

    public String endHref8Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
        handler.pageSaveState();

        return rc;
    }

    public String endHref9Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
        handler.pageSaveState();

        return rc;
    }

    public String endHref10Display(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc =
            handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
        handler.pageSaveState();

        return rc;
    }

    //=====================================================
    //--> New requirement : added new button for reverse funding
    //--> By Billy 11June2003
    public void handleBtReverseFundingRequest(RequestInvocationEvent event)
    throws ServletException, IOException
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleReverseFunding();
        handler.postHandlerProtocol();
    }

    public Button getBtReverseFunding()
    {
        return (Button) getChild(CHILD_BTREVERSEFUNDING);
    }

    public String endBtReverseFundingDisplay(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        boolean rc = handler.isReverseFunding();
        handler.pageSaveState();

        if (rc == true)
        {
            return event.getContent();
        }
        else
        {
            return "";
        }
    }

    //=========================================================
    //// This method overrides the getDefaultURL() framework JATO method and it
    // is located
    //// in each ViewBean. This allows not to stay with the JATO ViewBean
    // extension,
    //// otherwise each ViewBean a.k.a page should extend its Handler (to be
    // called by the framework)
    //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
    //// The full method is still in PHC base class. It should care the
    //// the url="/" case (non-initialized defaultURL) by the BX framework
    // methods.
    public String getDisplayURL()
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String url = getDefaultDisplayURL();
        int languageId = handler.theSessionState.getLanguageId();

        //// Call the language specific URL (business delegation done in the
        // BXResource).
        if ((url != null) && !url.trim().equals("") && !url.trim().equals("/"))
        {
            url = BXResources.getBXUrl(url, languageId);
        }
        else
        {
            url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
        }

        return url;
    }

    //--DJ_CR134--start--27May2004--//

    /**
     *
     *
     */
    public StaticTextField getStHomeBASEProductRatePmnt()
    {
        return (StaticTextField) getChild(CHILD_STHOMEBASEPRODUCTRATEPMNT);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeLifeDisLabelsStart()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDELIFEDISLABELSSTART);
    }

    /**
     *
     *
     */
    public StaticTextField getStIncludeLifeDisLabelsEnd()
    {
        return (StaticTextField) getChild(CHILD_STINCLUDELIFEDISLABELSEND);
    }

    //--DJ_CR134--end--//
    ////////////////////////////////////////////////////////////////////////////
    // Obsolete Netdynamics Events - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Custom Methods - Require Manual Migration
    ////////////////////////////////////////////////////////////////////////////
    public void handleActMessageOK(String[] args)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.preHandlerProtocol(this);
        handler.handleActMessageOK(args);
        handler.postHandlerProtocol();
    }


    // SEAN Ticket #1681 June 27, 2005: the getter and setter methods.
    public StaticTextField getStBeginHidePrintDealSummary()
    {
        return (StaticTextField) getChild(CHILD_STBEGINHIDEPRINTDEALSUMMARY);
    }

    /**
     *
     *
     */
    public String endStBeginHidePrintDealSummaryDisplay(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.generateOrHideBeginPrintDealSummary();
        handler.pageSaveState();

        return rc;
    }

    /**
     *
     *
     */
    public StaticTextField getStEndHidePrintDealSummary()
    {
        return (StaticTextField) getChild(CHILD_STENDHIDEPRINTDEALSUMMARY);
    }

    /**
     *
     *
     */
    public String endStEndHidePrintDealSummaryDisplay(ChildContentDisplayEvent event)
    {
        DealSummaryHandler handler = (DealSummaryHandler) this.handler.cloneSS();
        handler.pageGetState(this);

        String rc = handler.generateOrHideEndPrintDealSummary();
        handler.pageSaveState();

        return rc;
    }
    // SEAN Ticket #1681 END
    /**
     *
     *
     */

    //--DJ_Ticket661--start--//
    //Modified to sort by PageLabel based on the selected language
    static class CbPageNamesOptionList extends GotoOptionList
    {
        /**
         *
         *
         */
        CbPageNamesOptionList()
        {
        }

        public void populate(RequestContext rc)
        {
            Connection c = null;

            try
            {
                clear();

                SelectQueryModel m = null;
                SelectQueryExecutionContext eContext =
                    new SelectQueryExecutionContext((Connection) null,
                            DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
                            DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

                if (rc == null)
                {
                    m = new doPageNameLabelModelImpl();
                    c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
                    eContext.setConnection(c);
                }
                else
                {
                    m = (SelectQueryModel) rc.getModelManager().getModel(doPageNameLabelModel.class);
                }

                m.retrieve(eContext);
                m.beforeFirst();

                // Sort the results from DataObject
                TreeMap sorted = new TreeMap();

                while (m.next())
                {
                    Object dfPageLabel =
                        m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
                    String label = ((dfPageLabel == null) ? "" : dfPageLabel.toString());
                    Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
                    String value = ((dfPageId == null) ? "" : dfPageId.toString());
                    String[] theVal = new String[2];
                    theVal[0] = label;
                    theVal[1] = value;
                    sorted.put(label, theVal);
                }

                // Set the sorted list to the optionlist
                Iterator theList = sorted.values().iterator();
                String[] theVal = new String[2];

                while (theList.hasNext())
                {
                    theVal = (String[]) (theList.next());
                    add(theVal[0], theVal[1]);
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            finally
            {
                try
                {
                    if (c != null)
                    {
                        c.close();
                    }
                }
                catch (SQLException ex)
                {
                    // ignore
                }
            }
        }
    }
}
