package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealEntryApplicantSelectModelImpl extends QueryModelBase
	implements doDealEntryApplicantSelectModel
{
	/**
	 *
	 *
	 */
	public doDealEntryApplicantSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
      //--Release2.1--//
    ////To override all the Description fields by populating from BXResource.
    ////Get the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
                                          (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                           SessionStateModel.class,
                                           defaultInstanceStateName,
                                           true);

    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    while(this.next())
    {
      //// Convert ApplicantStatus Desc.
      this.setDfApplicantType(BXResources.getPickListDescription(institutionId, "BORROWERTYPE", this.getDfApplicantType(), languageId));
      //// Convert LanguagePreference Desc.
      this.setDfLanguagePreferenceDescr(BXResources.getPickListDescription(institutionId, "LANGUAGEPREFERENCE", this.getDfLanguagePreferenceDescr(), languageId));
      this.setDfSuffix(BXResources.getPickListDescription(institutionId, "SUFFIX", this.getDfSuffix(), languageId));    
    }

    //Reset Location
    this.beforeFirst();
	}

	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerID()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerID(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryBorrowerFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYBORROWERFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value)
	{
		setValue(FIELD_DFPRIMARYBORROWERFLAG,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFirstName()
	{
		return (String)getValue(FIELD_DFFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstName(String value)
	{
		setValue(FIELD_DFFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMiddleName()
	{
		return (String)getValue(FIELD_DFMIDDLENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfMiddleName(String value)
	{
		setValue(FIELD_DFMIDDLENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLastName()
	{
		return (String)getValue(FIELD_DFLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfLastName(String value)
	{
		setValue(FIELD_DFLASTNAME,value);
	}

    /**
     *
     *
     */
    public String getDfSuffix()
    {
        return (String)getValue(FIELD_DFSUFFIX);
    }


    /**
     *
     *
     */
    public void setDfSuffix(String value)
    {
        setValue(FIELD_DFSUFFIX,value);
    }

	/**
	 *
	 *
	 */
	public String getDfHomePhoneNumber()
	{
		return (String)getValue(FIELD_DFHOMEPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfHomePhoneNumber(String value)
	{
		setValue(FIELD_DFHOMEPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfWorkPhoneNumber()
	{
		return (String)getValue(FIELD_DFWORKPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfWorkPhoneNumber(String value)
	{
		setValue(FIELD_DFWORKPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfWorkPhoneExt()
	{
		return (String)getValue(FIELD_DFWORKPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfWorkPhoneExt(String value)
	{
		setValue(FIELD_DFWORKPHONEEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfApplicantType()
	{
		return (String)getValue(FIELD_DFAPPLICANTTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfApplicantType(String value)
	{
		setValue(FIELD_DFAPPLICANTTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLanguagePreferenceDescr()
	{
		return (String)getValue(FIELD_DFLANGUAGEPREFERENCEDESCR);
	}


	/**
	 *
	 *
	 */
	public void setDfLanguagePreferenceDescr(String value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCEDESCR,value);
	}


	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
   			super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
              	        logger.debug("Testing MetaData Object for new synthetic fields for" +
                 	            " doDealEntryApplicantSelectModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Adjust Field names definitions to string format.

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL BORROWER.DEALID, BORROWER.BORROWERID, BORROWER.COPYID, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, BORROWER.BORROWERHOMEPHONENUMBER, BORROWER.BORROWERWORKPHONENUMBER, BORROWER.BORROWERWORKPHONEEXTENSION, BORROWERTYPE.BTDESCRIPTION, LANGUAGEPREFERENCE.LPDESCRIPTION FROM BORROWER, LANGUAGEPREFERENCE, BORROWERTYPE  __WHERE__  ORDER BY BORROWER.BORROWERID  ASC";

	public static final String SELECT_SQL_TEMPLATE="SELECT distinct BORROWER.DEALID, BORROWER.BORROWERID, " +
  "BORROWER.COPYID, BORROWER.PRIMARYBORROWERFLAG, BORROWER.BORROWERFIRSTNAME, " +
  "BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, to_char(BORROWER.SUFFIXID) BORROWERSUFFIXID_STR, " +
  "BORROWER.BORROWERHOMEPHONENUMBER, BORROWER.BORROWERWORKPHONENUMBER, " +
  "BORROWER.BORROWERWORKPHONEEXTENSION, to_char(BORROWER.BORROWERTYPEID) BORROWERTYPEID_STR, " +
  "to_char(BORROWER.LANGUAGEPREFERENCEID) LANGUAGEPREFERENCE_STR " +
  "FROM BORROWER, LANGUAGEPREFERENCE, BORROWERTYPE  " +
  "__WHERE__  ORDER BY BORROWER.BORROWERID  ASC";

	public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER, LANGUAGEPREFERENCE, BORROWERTYPE";

	public static final String STATIC_WHERE_CRITERIA=" (BORROWER.LANGUAGEPREFERENCEID  =  LANGUAGEPREFERENCE.LANGUAGEPREFERENCEID) " +
                                                    "AND (BORROWER.BORROWERTYPEID  =  BORROWERTYPE.BORROWERTYPEID) ";

	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BORROWER.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG="BORROWER.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFPRIMARYBORROWERFLAG="PRIMARYBORROWERFLAG";
	public static final String QUALIFIED_COLUMN_DFFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
	public static final String COLUMN_DFFIRSTNAME="BORROWERFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFMIDDLENAME="BORROWER.BORROWERMIDDLEINITIAL";
	public static final String COLUMN_DFMIDDLENAME="BORROWERMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFLASTNAME="BORROWER.BORROWERLASTNAME";
	public static final String COLUMN_DFLASTNAME="BORROWERLASTNAME";
    public static final String QUALIFIED_COLUMN_DFSUFFIX="BORROWER.BORROWERSUFFIXID_STR";
    public static final String COLUMN_DFSUFFIX="BORROWERSUFFIXID_STR";
	public static final String QUALIFIED_COLUMN_DFHOMEPHONENUMBER="BORROWER.BORROWERHOMEPHONENUMBER";
	public static final String COLUMN_DFHOMEPHONENUMBER="BORROWERHOMEPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFWORKPHONENUMBER="BORROWER.BORROWERWORKPHONENUMBER";
	public static final String COLUMN_DFWORKPHONENUMBER="BORROWERWORKPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFWORKPHONEEXT="BORROWER.BORROWERWORKPHONEEXTENSION";
	public static final String COLUMN_DFWORKPHONEEXT="BORROWERWORKPHONEEXTENSION";

  //--Release2.1--//
  //// 3. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFAPPLICANTTYPE="BORROWERTYPE.BTDESCRIPTION";
	////public static final String COLUMN_DFAPPLICANTTYPE="BTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFAPPLICANTTYPE="BORROWER.BORROWERTYPEID_STR";
	public static final String COLUMN_DFAPPLICANTTYPE="BORROWERTYPEID_STR";

	////public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEDESCR="LANGUAGEPREFERENCE.LPDESCRIPTION";
	////public static final String COLUMN_DFLANGUAGEPREFERENCEDESCR="LPDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEDESCR="BORROWER.LANGUAGEPREFERENCE_STR";
	public static final String COLUMN_DFLANGUAGEPREFERENCEDESCR="LANGUAGEPREFERENCE_STR";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYBORROWERFLAG,
				COLUMN_DFPRIMARYBORROWERFLAG,
				QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTNAME,
				COLUMN_DFFIRSTNAME,
				QUALIFIED_COLUMN_DFFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIDDLENAME,
				COLUMN_DFMIDDLENAME,
				QUALIFIED_COLUMN_DFMIDDLENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLASTNAME,
				COLUMN_DFLASTNAME,
				QUALIFIED_COLUMN_DFLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFSUFFIX,
                COLUMN_DFSUFFIX,
                QUALIFIED_COLUMN_DFSUFFIX,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
        
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHOMEPHONENUMBER,
				COLUMN_DFHOMEPHONENUMBER,
				QUALIFIED_COLUMN_DFHOMEPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFWORKPHONENUMBER,
				COLUMN_DFWORKPHONENUMBER,
				QUALIFIED_COLUMN_DFWORKPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFWORKPHONEEXT,
				COLUMN_DFWORKPHONEEXT,
				QUALIFIED_COLUMN_DFWORKPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFAPPLICANTTYPE,
				COLUMN_DFAPPLICANTTYPE,
				QUALIFIED_COLUMN_DFAPPLICANTTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCEDESCR,
				COLUMN_DFLANGUAGEPREFERENCEDESCR,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEDESCR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

