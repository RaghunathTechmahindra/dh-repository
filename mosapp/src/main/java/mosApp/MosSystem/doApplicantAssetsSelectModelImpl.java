package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doApplicantAssetsSelectModelImpl extends QueryModelBase
	implements doApplicantAssetsSelectModel
{
	/**
	 *
	 *
	 */
	public doApplicantAssetsSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssetId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSETID);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSETID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssetTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSETTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSETTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAssetDesc()
	{
		return (String)getValue(FIELD_DFASSETDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetDesc(String value)
	{
		setValue(FIELD_DFASSETDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAssetValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFASSETVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfAssetValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFASSETVALUE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludeInNetworth()
	{
		return (String)getValue(FIELD_DFINCLUDEINNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludeInNetworth(String value)
	{
		setValue(FIELD_DFINCLUDEINNETWORTH,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInNetworth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInNetworth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINNETWORTH,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE
        ="SELECT distinct ASSET.BORROWERID, ASSET.ASSETID, ASSET.COPYID, " +
                "ASSET.ASSETTYPEID, ASSET.ASSETDESCRIPTION, ASSET.ASSETVALUE, " +
                "ASSET.INCLUDEINNETWORTH, ASSET.PERCENTINNETWORTH FROM ASSET " +
                "  __WHERE__  ORDER BY ASSET.ASSETID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="ASSET";
	public static final String STATIC_WHERE_CRITERIA ="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="ASSET.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFASSETID="ASSET.ASSETID";
	public static final String COLUMN_DFASSETID="ASSETID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="ASSET.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFASSETTYPEID="ASSET.ASSETTYPEID";
	public static final String COLUMN_DFASSETTYPEID="ASSETTYPEID";
	public static final String QUALIFIED_COLUMN_DFASSETDESC="ASSET.ASSETDESCRIPTION";
	public static final String COLUMN_DFASSETDESC="ASSETDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFASSETVALUE="ASSET.ASSETVALUE";
	public static final String COLUMN_DFASSETVALUE="ASSETVALUE";
	public static final String QUALIFIED_COLUMN_DFINCLUDEINNETWORTH="ASSET.INCLUDEINNETWORTH";
	public static final String COLUMN_DFINCLUDEINNETWORTH="INCLUDEINNETWORTH";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINNETWORTH="ASSET.PERCENTINNETWORTH";
	public static final String COLUMN_DFPERCENTINCLUDEDINNETWORTH="PERCENTINNETWORTH";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETID,
				COLUMN_DFASSETID,
				QUALIFIED_COLUMN_DFASSETID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETTYPEID,
				COLUMN_DFASSETTYPEID,
				QUALIFIED_COLUMN_DFASSETTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETDESC,
				COLUMN_DFASSETDESC,
				QUALIFIED_COLUMN_DFASSETDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFASSETVALUE,
				COLUMN_DFASSETVALUE,
				QUALIFIED_COLUMN_DFASSETVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDEINNETWORTH,
				COLUMN_DFINCLUDEINNETWORTH,
				QUALIFIED_COLUMN_DFINCLUDEINNETWORTH,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINNETWORTH,
				COLUMN_DFPERCENTINCLUDEDINNETWORTH,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINNETWORTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

