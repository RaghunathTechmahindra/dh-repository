package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doBorrowerEntryLiabilityUpdateModel extends QueryModel, UpdateQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabilityIncludingGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityIncludingGDS(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabilityIncludingTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityIncludingTDS(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFLIABILITYID="dfLiabilityId";
	public static final String FIELD_DFLIABILITYTYPEID="dfLiabilityTypeId";
	public static final String FIELD_DFLIABILITYAMOUNT="dfLiabilityAmount";
	public static final String FIELD_DFLIABILITYMONTHLYPAYMENT="dfLiabilityMonthlyPayment";
	public static final String FIELD_DFLIABILITYINCLUDINGGDS="dfLiabilityIncludingGDS";
	public static final String FIELD_DFLIABILITYINCLUDINGTDS="dfLiabilityIncludingTDS";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

