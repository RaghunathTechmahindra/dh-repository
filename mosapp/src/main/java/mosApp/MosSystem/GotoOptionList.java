package mosApp.MosSystem;

import java.sql.Connection;
import java.sql.SQLException;

import mosApp.SQLConnectionManagerImpl;

import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.html.OptionList;

/**
 * 
 * @author BZhang
 * Date May 31, 2006
 */
public class GotoOptionList extends OptionList{
	
	/**
	 * Populates GOTO dropdown list.
	 * @param rc
	 * @param langId
	 */
	public void populate(RequestContext rc, int langId) {
    	clear();
    	String[][] pageLabelValue = getPageOption(rc, langId);
    	
    	for (int i = 0; i < pageLabelValue.length; i++)
          	add(pageLabelValue[i][0], pageLabelValue[i][1]);
      }
	
	/**
	 * Get page's label&value pair for GOTO menu dropdown list.
	 * @param rc
	 * @param langId
	 * @return
	 */
	String[][] getPageOption(RequestContext rc, int langId) {
        Connection c = null;
        String[][] pageLabelValue = null;
        try {
          SelectQueryModel m = null;

          SelectQueryExecutionContext eContext = 
        	  new SelectQueryExecutionContext((Connection)null,
        			  DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
        			  DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

          if (rc == null) {
            m = new doPageNameLabelModelImpl();
            c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
            eContext.setConnection(c);
          }
          else {
            m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
          }

          m.retrieve(eContext);
          m.beforeFirst();
          
          pageLabelValue = new String[m.getSize()][2];	
          int _i = 0;
          
          while (m.next()) {
            Object dfPageLabel = m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
            String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
            Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
            String value = (dfPageId == null ? "" : dfPageId.toString());
            
            pageLabelValue[_i][0] = label;
            pageLabelValue[_i][1] = value;
            _i++;
          }
          
          pageLabelValue = JatoInputUtils.sortOption(pageLabelValue, langId);

        }
        catch (Exception ex) {
        	ex.printStackTrace();
        } 
        finally {
        	try {
        		if (c != null)
        			c.close();
        	} catch (SQLException ex) {
        	}
        }
        return pageLabelValue;
      }
}
