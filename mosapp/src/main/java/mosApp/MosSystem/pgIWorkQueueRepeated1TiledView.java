package mosApp.MosSystem;

import com.basis100.log.*;
import com.basis100.resources.PropertiesCache;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import mosApp.*;

import java.io.*;

import java.lang.reflect.*;

import java.sql.*;

import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;


/**
 *
 */
public class pgIWorkQueueRepeated1TiledView extends RequestHandlingTiledViewBase
  implements TiledView, RequestHandler
{
  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Child accessors
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Child rendering methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Repeated event methods
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////
  public static Map FIELD_DESCRIPTORS;
  public static final String CHILD_STSOURCE = "stSource";
  public static final String CHILD_STSOURCE_RESET_VALUE = "";
  public static final String CHILD_STSOURCEFIRM = "stSourceFirm";
  public static final String CHILD_STSOURCEFIRM_RESET_VALUE = "";
  public static final String CHILD_STBORROWER = "stBorrower";
  public static final String CHILD_STBORROWER_RESET_VALUE = "";
  public static final String CHILD_HDDEALID = "hdDealId";
  public static final String CHILD_HDDEALID_RESET_VALUE = "";
  public static final String CHILD_STDEALNUMBER = "stDealNumber";
  public static final String CHILD_STDEALNUMBER_RESET_VALUE = "";
  public static final String CHILD_HDWQID = "hdWqID";
  public static final String CHILD_HDWQID_RESET_VALUE = "";
  public static final String CHILD_HDTASKID = "hdTaskId";
  public static final String CHILD_HDTASKID_RESET_VALUE = "";
  public static final String CHILD_STTASKDESCRIPTION = "stTaskDescription";
  public static final String CHILD_STTASKDESCRIPTION_RESET_VALUE = "";
  public static final String CHILD_STTASKSTATUS = "stTaskStatus";
  public static final String CHILD_STTASKSTATUS_RESET_VALUE = "";
  public static final String CHILD_STTASKPRIORITY = "stTaskPriority";
  public static final String CHILD_STTASKPRIORITY_RESET_VALUE = "";
  public static final String CHILD_STTASKDUEDATE = "stTaskDueDate";
  public static final String CHILD_STTASKDUEDATE_RESET_VALUE = "";
  public static final String CHILD_STTIMESTATUS = "stTimeStatus";
  public static final String CHILD_STTIMESTATUS_RESET_VALUE = "";
  public static final String CHILD_STTASKWARNING = "stTaskWarning";
  public static final String CHILD_STTASKWARNING_RESET_VALUE = "";
  public static final String CHILD_SNAPSHOTBUTTON = "snapShotButton";
  public static final String CHILD_SNAPSHOTBUTTON_RESET_VALUE = "Snapshot";
  public static final String CHILD_BGCOLOR = "bgColor";
  public static final String CHILD_BGCOLOR_RESET_VALUE = "";
  public static final String CHILD_TASKBUTTON = "taskButton";
  public static final String CHILD_TASKBUTTON_RESET_VALUE = "Snapshot";

  //-- ========== SCR#750 begins ============================================ --//
  //-- by Neil on Dec/15/2004
  public static final String CHILD_STDEALSTATUS = "stDealStatus";
  public static final String CHILD_STDEALSTATUS_RESET_VALUE = "";
  public static final String CHILD_STHOLDREASON = "stHoldReason";
  public static final String CHILD_STHOLDREASON_RESET_VALUE = "";

  //-- these field holds the style format for TaskStatus/HoldReason textbox.
  //-- TaskStatus and HoldReason use the same textbox according to SCR#750.
  public static final String CHILD_TASKSTATUSORHOLDREASONSTYLE = "taskStatusOrHoldReasonStyle";
  public static final String CHILD_TASKSTATUSORHOLDREASONSTYLE_RESET_VALUE = "";

  public static final String CHILD_STINSTITUTIONID = "stInstitutionId";
  public static final String CHILD_STINSTITUTIONID_RESET_VALUE ="";
  //-- ========== SCR#750 ends ============================================== --//

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
  private iWorkQueueHandler handler = new iWorkQueueHandler();
  private doIWorkQueueModel doIWorkQueue = null;
  public SysLogger logger;

  public static final int MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE = 200;
  public static final int MIN_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE = 10;

  /**
   *
   *
   */
  public pgIWorkQueueRepeated1TiledView(View parent, String name)
  {
    super(parent, name);
    iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    int institutionId = handler.getTheSessionState().getCurrentPage().dealPage ? handler.getTheSessionState().getDealInstitutionId():handler.getTheSessionState().getUserInstitutionId();    
    int numOfRows = TypeConverter.asInt(PropertiesCache.getInstance().getProperty(institutionId, "com.filogix.individualworkqueue.numofrows", String.valueOf(MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE)), MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE);
	if (numOfRows > MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE) {
		numOfRows = MAX_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE;
	} else if (numOfRows < MIN_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE) {
		numOfRows = MIN_NUMBER_OF_INDIVIDUALWORKQUEUE_PER_PAGE;
	}
    setMaxDisplayTiles(numOfRows);
    setPrimaryModelClass(doIWorkQueueModel.class);

    registerChildren();

    initialize();
  }

  /**
   *
   *
   */
  protected void initialize()
  {
  }

  /**
   *
   *
   */
  public void resetChildren()
  {
    getStSource().setValue(CHILD_STSOURCE_RESET_VALUE);
    getStSourceFirm().setValue(CHILD_STSOURCEFIRM_RESET_VALUE);
    getStBorrower().setValue(CHILD_STBORROWER_RESET_VALUE);
    getHdDealId().setValue(CHILD_HDDEALID_RESET_VALUE);
    getStDealNumber().setValue(CHILD_STDEALNUMBER_RESET_VALUE);
    getHdWqID().setValue(CHILD_HDWQID_RESET_VALUE);
    getHdTaskId().setValue(CHILD_HDTASKID_RESET_VALUE);
    getStTaskDescription().setValue(CHILD_STTASKDESCRIPTION_RESET_VALUE);
    getStTaskStatus().setValue(CHILD_STTASKSTATUS_RESET_VALUE);
    getStTaskPriority().setValue(CHILD_STTASKPRIORITY_RESET_VALUE);
    getStTaskDueDate().setValue(CHILD_STTASKDUEDATE_RESET_VALUE);
    getStTimeStatus().setValue(CHILD_STTIMESTATUS_RESET_VALUE);
    getStTaskWarning().setValue(CHILD_STTASKWARNING_RESET_VALUE);
    getSnapShotButton().setValue(CHILD_SNAPSHOTBUTTON_RESET_VALUE);
    getBgColor().setValue(CHILD_BGCOLOR_RESET_VALUE);
    getTaskButton().setValue(CHILD_TASKBUTTON_RESET_VALUE);

    //-- ========== SCR#750 begins ============================================ --//
    //-- by Neil on Dec/15/2004
    getStDealStatus().setValue(CHILD_STDEALSTATUS_RESET_VALUE);
    getStHoldReason().setValue(CHILD_STHOLDREASON_RESET_VALUE);
    getTaskStatusOrHoldReasonStyle().setValue(CHILD_TASKSTATUSORHOLDREASONSTYLE_RESET_VALUE);

    //-- ========== SCR#750 ends ============================================== --//
    getStInstitutionId().setValue(CHILD_STINSTITUTIONID_RESET_VALUE);
  }

  /**
   *
   *
   */
  protected void registerChildren()
  {
    registerChild(CHILD_STSOURCE, StaticTextField.class);
    registerChild(CHILD_STSOURCEFIRM, StaticTextField.class);
    registerChild(CHILD_STBORROWER, StaticTextField.class);
    registerChild(CHILD_HDDEALID, HiddenField.class);
    registerChild(CHILD_STDEALNUMBER, StaticTextField.class);
    registerChild(CHILD_HDWQID, HiddenField.class);
    registerChild(CHILD_HDTASKID, HiddenField.class);
    registerChild(CHILD_STTASKDESCRIPTION, StaticTextField.class);
    registerChild(CHILD_STTASKSTATUS, StaticTextField.class);
    registerChild(CHILD_STTASKPRIORITY, StaticTextField.class);
    registerChild(CHILD_STTASKDUEDATE, StaticTextField.class);
    registerChild(CHILD_STTIMESTATUS, StaticTextField.class);
    registerChild(CHILD_STTASKWARNING, StaticTextField.class);
    registerChild(CHILD_SNAPSHOTBUTTON, Button.class);
    registerChild(CHILD_BGCOLOR, StaticTextField.class);
    registerChild(CHILD_TASKBUTTON, Button.class);

    //-- ========== SCR#750 begins ============================================ --//
    //-- by Neil on Dec/15/2004
    registerChild(CHILD_STDEALSTATUS, StaticTextField.class);
    registerChild(CHILD_STHOLDREASON, StaticTextField.class);
    registerChild(CHILD_TASKSTATUSORHOLDREASONSTYLE, StaticTextField.class);

    //-- ========== SCR#750 ends ============================================== --//
    registerChild(CHILD_STINSTITUTIONID, StaticTextField.class);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Model management methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  public Model[] getWebActionModels(int executionType)
  {
    List modelList = new ArrayList();

    switch (executionType)
    {
    case MODEL_TYPE_RETRIEVE:
      modelList.add(getdoIWorkQueueModel());
      ;

      break;

    case MODEL_TYPE_UPDATE:

      break;

    case MODEL_TYPE_DELETE:
      ;

      break;

    case MODEL_TYPE_INSERT:
      ;

      break;

    case MODEL_TYPE_EXECUTE:

      break;
    }

    return (Model[]) modelList.toArray(new Model[0]);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // View flow control methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  public void beginDisplay(DisplayEvent event) throws ModelControlException
  {
    if (getPrimaryModel() == null)
    {
      throw new ModelControlException("Primary model is null");
    }

    iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    handler.checkAndSyncDOWithCursorInfo(this);
    handler.pageSaveState();

    super.beginDisplay(event);
    resetTileIndex();
  }

  /**
   *
   *
   */
  public boolean nextTile() throws ModelControlException
  {
    //logger = SysLog.getSysLogger("IWQR1TW");
    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
    boolean movedToRow = super.nextTile();

    if (movedToRow)
    {
      int rowNum = this.getTileIndex();

      //logger.debug("IWQR1TW@nextTile::rowNum: " + rowNum);
      iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

      //logger.debug("IWQR1TW@nextTile::got Handler");
      handler.pageGetState(this.getParentViewBean());

      //-- ========== SCR#750 begins ============================================ --//
      //-- by Neil on Dec/17/2004
      //			doIWorkQueueModelImpl dmIWorkQueue =
      //				(doIWorkQueueModelImpl) getdoIWorkQueueModel();
      //			String taskStatus =
      //				dmIWorkQueue
      //					.getValue(
      //						doIWorkQueueModelImpl.FIELD_DFTASKSTATUS_TSDESCRIPTION)
      //					.toString();
      try
      {
        handler.setTaskStatusWithHoldReason();
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }

      //-- ========== SCR#750   ends ============================================ --//
      //logger.debug("IWQR1TW@nextTile::before populateRepeatedFields");
      //logger.debug("IWQR1TW@nextTile::CheckHandler: " + handler);
      handler.populatedRepeatedFields(1, rowNum);
      handler.pageSaveState();
    }

    return movedToRow;
  }

  /**
   *
   *
   */
  public boolean beforeModelExecutes(Model model, int executionContext)
  {
    //logger = SysLog.getSysLogger("IWQH");
    // This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
    ////logger.debug("IWQH@:beforeModelExecutes "+getClass().getName()
    ////    + " :: the Model Name = " + model.getName() + " :: ExecutionContext = " + executionContext);
    return super.beforeModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterModelExecutes(Model model, int executionContext)
  {
    // This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
    super.afterModelExecutes(model, executionContext);
  }

  /**
   *
   *
   */
  public void afterAllModelsExecute(int executionContext)
  {
    // This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
    super.afterAllModelsExecute(executionContext);
  }

  /**
   *
   *
   */
  public void onModelError(Model model, int executionContext, ModelControlException exception)
    throws ModelControlException
  {
    // This is the analog of NetDynamics repeated_onDataObjectErrorEvent
    super.onModelError(model, executionContext, exception);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Child manipulation methods
  ////////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   */
  protected View createChild(String name)
  {
    if (name.equals(CHILD_STSOURCE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STSOURCE,
          doIWorkQueueModel.FIELD_DFCONTACTLASTNAME, CHILD_STSOURCE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STSOURCEFIRM))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STSOURCEFIRM,
          doIWorkQueueModel.FIELD_DFSFSHORTNAME, CHILD_STSOURCEFIRM_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STBORROWER))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STBORROWER, CHILD_STBORROWER,
          CHILD_STBORROWER_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDDEALID))
    {
      HiddenField child =
        new HiddenField(this, getdoIWorkQueueModel(), CHILD_HDDEALID,
          doIWorkQueueModel.FIELD_DFDEALID, CHILD_HDDEALID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STDEALNUMBER))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STDEALNUMBER,
          doIWorkQueueModel.FIELD_DFDEALID, CHILD_STDEALNUMBER_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDWQID))
    {
      HiddenField child =
        new HiddenField(this, getdoIWorkQueueModel(), CHILD_HDWQID,
          doIWorkQueueModel.FIELD_DFASSIGNEDTASKSWORKQUEUEID, CHILD_HDWQID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_HDTASKID))
    {
      HiddenField child =
        new HiddenField(this, getdoIWorkQueueModel(), CHILD_HDTASKID,
          doIWorkQueueModel.FIELD_DFTASKID, CHILD_HDTASKID_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKDESCRIPTION))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STTASKDESCRIPTION,
          doIWorkQueueModel.FIELD_DFTASK_TASKLABEL, CHILD_STTASKDESCRIPTION_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKSTATUS))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STTASKSTATUS,
          doIWorkQueueModel.FIELD_DFTASKSTATUS_TSDESCRIPTION, CHILD_STTASKSTATUS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKPRIORITY))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STTASKPRIORITY,
          doIWorkQueueModel.FIELD_DFPRIORITY_PDESCRIPTION, CHILD_STTASKPRIORITY_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKDUEDATE))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STTASKDUEDATE,
          doIWorkQueueModel.FIELD_DFDUETIMESTAMP, CHILD_STTASKDUEDATE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTIMESTATUS))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STTIMESTATUS,
          doIWorkQueueModel.FIELD_DFTMDESCRIPTION, CHILD_STTIMESTATUS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STTASKWARNING))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_STTASKWARNING, CHILD_STTASKWARNING,
          CHILD_STTASKWARNING_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_SNAPSHOTBUTTON))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_SNAPSHOTBUTTON, CHILD_SNAPSHOTBUTTON,
          CHILD_SNAPSHOTBUTTON_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_BGCOLOR))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_BGCOLOR, CHILD_BGCOLOR,
          CHILD_BGCOLOR_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_TASKBUTTON))
    {
      Button child =
        new Button(this, getDefaultModel(), CHILD_TASKBUTTON, CHILD_TASKBUTTON,
          CHILD_TASKBUTTON_RESET_VALUE, null);

      return child;

      //-- ========== SCR#750 begins ============================================ --//
      //-- by Neil on Dec/15/2004
    }
    else if (name.equals(CHILD_TASKSTATUSORHOLDREASONSTYLE))
    {
      StaticTextField child =
        new StaticTextField(this, getDefaultModel(), CHILD_TASKSTATUSORHOLDREASONSTYLE,
          CHILD_TASKSTATUSORHOLDREASONSTYLE, CHILD_TASKSTATUSORHOLDREASONSTYLE_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STDEALSTATUS))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STDEALSTATUS,
          doIWorkQueueModel.FIELD_DFDEALSTATUS, CHILD_STDEALSTATUS_RESET_VALUE, null);

      return child;
    }
    else if (name.equals(CHILD_STHOLDREASON))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STHOLDREASON,
          doIWorkQueueModel.FIELD_DFHOLDREASONDESCRIPTION, CHILD_STHOLDREASON_RESET_VALUE, null);

      return child;

      //-- ========== SCR#750 ends ============================================== --//
    }
    else if (name.equals(CHILD_STINSTITUTIONID))
    {
      StaticTextField child =
        new StaticTextField(this, getdoIWorkQueueModel(), CHILD_STINSTITUTIONID,
            doIWorkQueueModel.FIELD_DFINSTITUTIONNAME, CHILD_STINSTITUTIONID_RESET_VALUE, null);
      return child;
    }
    else
    {
      throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }
  }

  /**
   *
   *
   */
  public StaticTextField getStSource()
  {
    return (StaticTextField) getChild(CHILD_STSOURCE);
  }

  /**
   *
   *
   */
  public StaticTextField getStSourceFirm()
  {
    return (StaticTextField) getChild(CHILD_STSOURCEFIRM);
  }

  /**
   *
   *
   */
  public StaticTextField getStBorrower()
  {
    return (StaticTextField) getChild(CHILD_STBORROWER);
  }

  /**
   *
   *
   */
  public HiddenField getHdDealId()
  {
    return (HiddenField) getChild(CHILD_HDDEALID);
  }

  /**
   *
   *
   */
  public StaticTextField getStDealNumber()
  {
    return (StaticTextField) getChild(CHILD_STDEALNUMBER);
  }

  /**
   *
   *
   */
  public HiddenField getHdWqID()
  {
    return (HiddenField) getChild(CHILD_HDWQID);
  }

  /**
   *
   *
   */
  public HiddenField getHdTaskId()
  {
    return (HiddenField) getChild(CHILD_HDTASKID);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskDescription()
  {
    return (StaticTextField) getChild(CHILD_STTASKDESCRIPTION);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskStatus()
  {
    return (StaticTextField) getChild(CHILD_STTASKSTATUS);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskPriority()
  {
    return (StaticTextField) getChild(CHILD_STTASKPRIORITY);
  }

  /**
   *
   *
   */
  public StaticTextField getStTaskDueDate()
  {
    return (StaticTextField) getChild(CHILD_STTASKDUEDATE);
  }

  /**
   *
   *
   */
  public boolean beginStTaskDueDateDisplay(ChildDisplayEvent event)
  {
    //Object value = getStTaskDueDate().getValue();
    iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();
    handler.pageGetState(this.getParentViewBean());
    handler.convertToUserTimeZone(this.getParentViewBean(), "Repeated1/stTaskDueDate");
    handler.pageSaveState();

    return true;
  }

  /**
   *
   *
   */
  public StaticTextField getStTimeStatus()
  {
    return (StaticTextField) getChild(CHILD_STTIMESTATUS);
  }

  //-- ========== SCR#750 begins ============================================ --//
  //-- by Neil on Dec/15/2004

  /**
   * DOCUMENT ME!
   *
   * @return StaticTextField
   */
  public StaticTextField getStDealStatus()
  {
    return (StaticTextField) getChild(CHILD_STDEALSTATUS);
  }

  public StaticTextField getStHoldReason()
  {
    return (StaticTextField) getChild(CHILD_STHOLDREASON);
  }

  public StaticTextField getTaskStatusOrHoldReasonStyle()
  {
    return (StaticTextField) getChild(CHILD_TASKSTATUSORHOLDREASONSTYLE);
  }

  //-- ========== SCR#750 ends ============================================== --//

  /**
   *
   *
   */
  public StaticTextField getStTaskWarning()
  {
    return (StaticTextField) getChild(CHILD_STTASKWARNING);
  }

  /**
   *
   *
   */
  public void handleSnapShotButtonRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this.getParentViewBean());

    //--> Must call handleWebAction with refresh option in order to refresh screen without
    //--> resetting the current display row index.
    //--> by Billy 22July2002
    this.handleWebAction(WebActions.ACTION_REFRESH);

    //====================================================================================
    handler.handleSnapShotButton(((TiledViewRequestInvocationEvent) (event)).getTileNumber());

    handler.postHandlerProtocol();

    // The following code block was migrated from the snapShotButton_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();

         handler.preHandlerProtocol(this);

       handler.handleSnapShotButton(handler.getRowNdxFromWebEventMethod(event));

         handler.postHandlerProtocol();

         return PROCEED;
     */
  }

  /**
   *
   *
   */
  public Button getSnapShotButton()
  {
    return (Button) getChild(CHILD_SNAPSHOTBUTTON);
  }

  /**
   *
   *
   */
  public StaticTextField getBgColor()
  {
    return (StaticTextField) getChild(CHILD_BGCOLOR);
  }

  /**
   *
   *
   */
  public void handleTaskButtonRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    iWorkQueueHandler handler = (iWorkQueueHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this.getParentViewBean());

    handler.handleSelectTask(((TiledViewRequestInvocationEvent) (event)).getTileNumber());

    handler.postHandlerProtocol();

    // The following code block was migrated from the taskButton_onWebEvent method
    // MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED

    /*
       iWorkQueueHandler handler =(iWorkQueueHandler) this.handler.cloneSS();

         handler.preHandlerProtocol(this);

       handler.handleSelectTask(handler.getRowNdxFromWebEventMethod(event));

         handler.postHandlerProtocol();

         return PROCEED;
     */
  }

  //--> New method to manually set the current Display Offset
  //--> Current in JATO there is no method to mamually set the display offset, that's why
  //--> we have to make our own method.
  //--> Note : This method may need to be implemented in all TiledViews with Forward, Backward buttons.
  //--> By Billy 22July2002
  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_REFRESH);
  }

  //=====================================================================================

  /**
   *
   *
   */
  public Button getTaskButton()
  {
    return (Button) getChild(CHILD_TASKBUTTON);
  }

  /**
   *
   *
   */
  public doIWorkQueueModel getdoIWorkQueueModel()
  {
    if (doIWorkQueue == null)
    {
      doIWorkQueue = (doIWorkQueueModel) getModel(doIWorkQueueModel.class);
    }

    return doIWorkQueue;
  }

  /**
   *
   *
   */
  public void setdoIWorkQueueModel(doIWorkQueueModel model)
  {
    doIWorkQueue = model;
  }
  
  /**
  *
  *
  */
 public StaticTextField getStInstitutionId()
 {
   return (StaticTextField) getChild(CHILD_STINSTITUTIONID);
 }

}
