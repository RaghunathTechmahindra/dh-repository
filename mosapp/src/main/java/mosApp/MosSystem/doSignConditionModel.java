package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doSignConditionModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public String getDfDocumentLabel();


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value);


	/**
	 *
	 *
	 */
	public String getDfDocumentText();


	/**
	 *
	 *
	 */
	public void setDfDocumentText(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction();


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDefDocTrackId();


	/**
	 *
	 *
	 */
	public void setDfDefDocTrackId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLanguagePrederenceId();


	/**
	 *
	 *
	 */
	public void setDfLanguagePrederenceId(java.math.BigDecimal value);


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDOCUMENTLABEL="dfDocumentLabel";
	public static final String FIELD_DFDOCUMENTTEXT="dfDocumentText";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFACTION="dfAction";
	public static final String FIELD_DFDEFDOCTRACKID="dfDefDocTrackId";
	//--Release2.1--//
  //--> Added LanguagePreferenceId DataField
	public static final String FIELD_DFLANGUAGEPREFERENCEID="dfLanguagePrederenceId";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

