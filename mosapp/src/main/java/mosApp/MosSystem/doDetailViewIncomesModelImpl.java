package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewIncomesModelImpl extends QueryModelBase
	implements doDetailViewIncomesModel
{
	/**
	 *
	 *
	 */
	public doDetailViewIncomesModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 18Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert INCOMEPERIOD Desc.
      this.setDfIncomePeriod(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "INCOMEPERIOD", this.getDfIncomePeriod(), languageId));
      //Convert INCOMETYPE Desc.
      this.setDfIncomeType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "INCOMETYPE", this.getDfIncomeType(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncomeType()
	{
		return (String)getValue(FIELD_DFINCOMETYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeType(String value)
	{
		setValue(FIELD_DFINCOMETYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncomeDesc()
	{
		return (String)getValue(FIELD_DFINCOMEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeDesc(String value)
	{
		setValue(FIELD_DFINCOMEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncomePeriod()
	{
		return (String)getValue(FIELD_DFINCOMEPERIOD);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomePeriod(String value)
	{
		setValue(FIELD_DFINCOMEPERIOD,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeAmt()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEAMT);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeAmt(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEAMT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINTDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmploymentRelated()
	{
		return (String)getValue(FIELD_DFEMPLOYMENTRELATED);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentRelated(String value)
	{
		setValue(FIELD_DFEMPLOYMENTRELATED,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 18Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT INCOME.BORROWERID, INCOMETYPE.ITDESCRIPTION, INCOME.INCOMEDESCRIPTION, INCOMEPERIOD.IPDESCRIPTION, INCOME.INCOMEAMOUNT, INCOME.INCPERCENTINGDS, INCOME.INCPERCENTINTDS, INCOMETYPE.EMPLOYMENTRELATED, INCOME.INCOMEID, INCOME.COPYID FROM INCOME, INCOMEPERIOD, INCOMETYPE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT INCOME.BORROWERID, to_char(INCOME.INCOMETYPEID) INCOMETYPEID_STR, "+
    "INCOME.INCOMEDESCRIPTION, to_char(INCOME.INCOMEPERIODID) INCOMEPERIODID_STR, "+
    "INCOME.INCOMEAMOUNT, INCOME.INCPERCENTINGDS, INCOME.INCPERCENTINTDS, "+
    "INCOMETYPE.EMPLOYMENTRELATED, INCOME.INCOMEID, INCOME.COPYID, "+
    "INCOME.INSTITUTIONPROFILEID "+
    " FROM INCOME, INCOMETYPE  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="INCOME, INCOMEPERIOD, INCOMETYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME="INCOME, INCOMETYPE";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (((INCOME.INCOMEPERIODID  =  INCOMEPERIOD.INCOMEPERIODID) AND (INCOME.INCOMETYPEID  =  INCOMETYPE.INCOMETYPEID)) AND INCOMETYPE.EMPLOYMENTRELATED = 'Y')";
	public static final String STATIC_WHERE_CRITERIA=
    " (INCOME.INCOMETYPEID  =  INCOMETYPE.INCOMETYPEID AND "+
    "INCOMETYPE.EMPLOYMENTRELATED = 'Y')";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="INCOME.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";

	public static final String QUALIFIED_COLUMN_DFINCOMEDESC="INCOME.INCOMEDESCRIPTION";
	public static final String COLUMN_DFINCOMEDESC="INCOMEDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFINCOMEAMT="INCOME.INCOMEAMOUNT";
	public static final String COLUMN_DFINCOMEAMT="INCOMEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS="INCOME.INCPERCENTINGDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINGDS="INCPERCENTINGDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS="INCOME.INCPERCENTINTDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINTDS="INCPERCENTINTDS";
	public static final String QUALIFIED_COLUMN_DFEMPLOYMENTRELATED="INCOMETYPE.EMPLOYMENTRELATED";
	public static final String COLUMN_DFEMPLOYMENTRELATED="EMPLOYMENTRELATED";
	public static final String QUALIFIED_COLUMN_DFINCOMEID="INCOME.INCOMEID";
	public static final String COLUMN_DFINCOMEID="INCOMEID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="INCOME.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	public static final String QUALIFIED_COLUMN_DFINCOMEPERIOD="INCOME.INCOMEPERIODID_STR";
	public static final String COLUMN_DFINCOMEPERIOD="INCOMEPERIODID_STR";
  public static final String QUALIFIED_COLUMN_DFINCOMETYPE="INCOME.INCOMETYPEID_STR";
	public static final String COLUMN_DFINCOMETYPE="INCOMETYPEID_STR";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "DEAL.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMETYPE,
				COLUMN_DFINCOMETYPE,
				QUALIFIED_COLUMN_DFINCOMETYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEDESC,
				COLUMN_DFINCOMEDESC,
				QUALIFIED_COLUMN_DFINCOMEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEPERIOD,
				COLUMN_DFINCOMEPERIOD,
				QUALIFIED_COLUMN_DFINCOMEPERIOD,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEAMT,
				COLUMN_DFINCOMEAMT,
				QUALIFIED_COLUMN_DFINCOMEAMT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINGDS,
				COLUMN_DFPERCENTINCLUDEDINGDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINTDS,
				COLUMN_DFPERCENTINCLUDEDINTDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTRELATED,
				COLUMN_DFEMPLOYMENTRELATED,
				QUALIFIED_COLUMN_DFEMPLOYMENTRELATED,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEID,
				COLUMN_DFINCOMEID,
				QUALIFIED_COLUMN_DFINCOMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                  FIELD_DFINSTITUTIONID,
                  COLUMN_DFINSTITUTIONID,
                  QUALIFIED_COLUMN_DFINSTITUTIONID,
                  java.math.BigDecimal.class,
                  false,
                  false,
                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                  "",
                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                  ""));
	}

}

