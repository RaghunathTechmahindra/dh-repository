package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryOtherIncomeTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryOtherIncomeTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewEmpOtherIncomeModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStIDIncomeType().setValue(CHILD_STIDINCOMETYPE_RESET_VALUE);
		getStIDOtherIncomeDesc().setValue(CHILD_STIDOTHERINCOMEDESC_RESET_VALUE);
		getStIDIncomePeriod().setValue(CHILD_STIDINCOMEPERIOD_RESET_VALUE);
		getStIDIncomeAmount().setValue(CHILD_STIDINCOMEAMOUNT_RESET_VALUE);
		getStIDOtherIncomeGDS().setValue(CHILD_STIDOTHERINCOMEGDS_RESET_VALUE);
		getStIDOtherIncomeTDS().setValue(CHILD_STIDOTHERINCOMETDS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STIDINCOMETYPE,StaticTextField.class);
		registerChild(CHILD_STIDOTHERINCOMEDESC,StaticTextField.class);
		registerChild(CHILD_STIDINCOMEPERIOD,StaticTextField.class);
		registerChild(CHILD_STIDINCOMEAMOUNT,StaticTextField.class);
		registerChild(CHILD_STIDOTHERINCOMEGDS,StaticTextField.class);
		registerChild(CHILD_STIDOTHERINCOMETDS,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewEmpOtherIncomeModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the OtherIncome_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedOtherIncome(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STIDINCOMETYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmpOtherIncomeModel(),
				CHILD_STIDINCOMETYPE,
				doDetailViewEmpOtherIncomeModel.FIELD_DFINCOMETYPEDESC,
				CHILD_STIDINCOMETYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDOTHERINCOMEDESC))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmpOtherIncomeModel(),
				CHILD_STIDOTHERINCOMEDESC,
				doDetailViewEmpOtherIncomeModel.FIELD_DFINCOMEDESC,
				CHILD_STIDOTHERINCOMEDESC_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDINCOMEPERIOD))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmpOtherIncomeModel(),
				CHILD_STIDINCOMEPERIOD,
				doDetailViewEmpOtherIncomeModel.FIELD_DFINCOMEPERIODDESC,
				CHILD_STIDINCOMEPERIOD_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDINCOMEAMOUNT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmpOtherIncomeModel(),
				CHILD_STIDINCOMEAMOUNT,
				doDetailViewEmpOtherIncomeModel.FIELD_DFINCOMEAMOUNT,
				CHILD_STIDINCOMEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDOTHERINCOMEGDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmpOtherIncomeModel(),
				CHILD_STIDOTHERINCOMEGDS,
				doDetailViewEmpOtherIncomeModel.FIELD_DFINCOMEPERCENTAGEINGDS,
				CHILD_STIDOTHERINCOMEGDS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STIDOTHERINCOMETDS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewEmpOtherIncomeModel(),
				CHILD_STIDOTHERINCOMETDS,
				doDetailViewEmpOtherIncomeModel.FIELD_DFINCOMEPERCENTAGEINTDS,
				CHILD_STIDOTHERINCOMETDS_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDIncomeType()
	{
		return (StaticTextField)getChild(CHILD_STIDINCOMETYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDOtherIncomeDesc()
	{
		return (StaticTextField)getChild(CHILD_STIDOTHERINCOMEDESC);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDIncomePeriod()
	{
		return (StaticTextField)getChild(CHILD_STIDINCOMEPERIOD);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDIncomeAmount()
	{
		return (StaticTextField)getChild(CHILD_STIDINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDOtherIncomeGDS()
	{
		return (StaticTextField)getChild(CHILD_STIDOTHERINCOMEGDS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIDOtherIncomeTDS()
	{
		return (StaticTextField)getChild(CHILD_STIDOTHERINCOMETDS);
	}


	/**
	 *
	 *
	 */
	public doDetailViewEmpOtherIncomeModel getdoDetailViewEmpOtherIncomeModel()
	{
		if (doDetailViewEmpOtherIncome == null)
			doDetailViewEmpOtherIncome = (doDetailViewEmpOtherIncomeModel) getModel(doDetailViewEmpOtherIncomeModel.class);
		return doDetailViewEmpOtherIncome;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewEmpOtherIncomeModel(doDetailViewEmpOtherIncomeModel model)
	{
			doDetailViewEmpOtherIncome = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STIDINCOMETYPE="stIDIncomeType";
	public static final String CHILD_STIDINCOMETYPE_RESET_VALUE="";
	public static final String CHILD_STIDOTHERINCOMEDESC="stIDOtherIncomeDesc";
	public static final String CHILD_STIDOTHERINCOMEDESC_RESET_VALUE="";
	public static final String CHILD_STIDINCOMEPERIOD="stIDIncomePeriod";
	public static final String CHILD_STIDINCOMEPERIOD_RESET_VALUE="";
	public static final String CHILD_STIDINCOMEAMOUNT="stIDIncomeAmount";
	public static final String CHILD_STIDINCOMEAMOUNT_RESET_VALUE="";
	public static final String CHILD_STIDOTHERINCOMEGDS="stIDOtherIncomeGDS";
	public static final String CHILD_STIDOTHERINCOMEGDS_RESET_VALUE="";
	public static final String CHILD_STIDOTHERINCOMETDS="stIDOtherIncomeTDS";
	public static final String CHILD_STIDOTHERINCOMETDS_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewEmpOtherIncomeModel doDetailViewEmpOtherIncome=null;
  private DealSummaryHandler handler=new DealSummaryHandler();
}

