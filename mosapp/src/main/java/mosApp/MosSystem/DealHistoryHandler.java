package mosApp.MosSystem;

import java.text.*;
import java.util.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.deal.security.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

import mosApp.*;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;
import com.iplanet.jato.view.event.*;

/**
 *
 *
 */
public class DealHistoryHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public DealHistoryHandler cloneSS()
	{
		return(DealHistoryHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("Repeated1");

		try
		{
			populatePageShellDisplayFields();
			// Quick Link Menu display
			displayQuickLinkMenu();
			populateTaskNavigator(pg);
			populatePageDealSummarySnapShot();
			populatePreviousPagesLinks();
			revisePrevNextButtonGeneration(pg, tds);
			getCurrNDPage().setDisplayFieldValue("stBeginLine", new Integer(tds.getFirstDisplayRowNumber()));
			getCurrNDPage().setDisplayFieldValue("stEndLine", new Integer(tds.getLastDisplayRowNumber()));
			getCurrNDPage().setDisplayFieldValue("stTotalLines", new Integer(tds.getTotalRows()));
		}
		catch(Exception e)
		{
			logger.error("Exception @DealHistoryHandler.populatePageDisplayFields()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	/**
	 *
	 *
	 */
	public void setupBeforePageGeneration()
	{
		try
		{
			PageEntry pg = getTheSessionState().getCurrentPage();
			if (pg.getSetupBeforeGenerationCalled() == true) return;
			pg.setSetupBeforeGenerationCalled(true);
			setupDealSummarySnapShotDO(pg);
			QueryModelBase theDO =(QueryModelBase)
          (RequestManager.getRequestContext().getModelManager().getModel(doDealHistoryModel.class));
			theDO.clearUserWhereCriteria();
			theDO.addUserWhereCriterion("dfDealId", "=", new String("" + pg.getPageDealId()));
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated1");
			Hashtable pst = pg.getPageStateTable();
			theDO.executeSelect(null);
			int numRows = theDO.getSize();

			//			logger.trace("!!!!!!!!!NUM_ROWS=" + numRows);
			if (tds == null)
			{
				String voName = "Repeated1";
				int perPage =((TiledView)(getCurrNDPage().getChild(voName))).getMaxDisplayTiles();
				tds = new PageCursorInfo(voName, voName, perPage, numRows);
				tds.setRefresh(true);
				pst.put(tds.getName(), tds);
			}
			else
			{
				tds.setTotalRows(numRows);
			}
		}
		catch(Exception e)
		{
			logger.error("Exception @DealHistoryHandler.setupBeforePageGeneration()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	/**
	 *
	 *
	 */
  //--> This method is not necessary
  //--> Commented out by Billy 09Sept2002
  /*
	public boolean viewRepeated()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds =(PageCursorInfo) pst.get("Repeated1");
			if (tds.getTotalRows() > 0)
			{
				return true;
			}
		}
		catch(Exception e)
		{
			logger.error("Exception @DealHistoryHandler.viewRepeated()");
			logger.error(e);
			setStandardFailMessage();
		}
		return false;
	}
  */

	/**
	 *
	 *
	 */
	public void handleForwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			PageCursorInfo tds =(PageCursorInfo) pst.get("Repeated1");
			handleForwardBackwardCommon(pg, tds, - 1, true);
		}
		catch(Exception e)
		{
			logger.error("Exception @DealHistoryHandler.handleForwardButton()");
			logger.error(e);
			setStandardFailMessage();
		}
	}


	/**
	 *
	 *
	 */
	public void handleBackwardButton()
	{
		PageEntry pg = getTheSessionState().getCurrentPage();
		Hashtable pst = pg.getPageStateTable();

		try
		{
			PageCursorInfo tds =(PageCursorInfo) pst.get("Repeated1");
			handleForwardBackwardCommon(pg, tds, - 1, false);
		}
		catch(Exception e)
		{
			logger.error("Exception @DealHistoryHandler.handleBackwardButton()");
			logger.error(e);
			setStandardFailMessage();
		}
	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		handleCancelStandard();
	}


	/**
	 *
	 *
	 */
	public void navigateToTransactionHistory()
	{
		PageEntry subPage = null;

		PageEntry pg = getTheSessionState().getCurrentPage();

		try
		{
			subPage = setupSubPagePageEntry(pg, Mc.PGNM_TRANSACTION_HISTORY, true);
			getSavedPages().setNextPage(subPage);
			navigateToNextPage(true);
			return;
		}
		catch(Exception e)
		{
			logger.error("Exception @DealHistoryHandler.navigateToTransactionHistory()");
			logger.error(e);
			setStandardFailMessage();
		}
	}
}

