package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewPropertyExpensesModelImpl extends QueryModelBase
	implements doDetailViewPropertyExpensesModel
{
	/**
	 *
	 *
	 */
	public doDetailViewPropertyExpensesModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert PROPERTYEXPENSETYPE Desc.
      this.setDfPropertyExpenseType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROPERTYEXPENSETYPE", this.getDfPropertyExpenseType(), languageId));
      //Convert PROPERTYEXPENSEPERIOD Desc.
      this.setDfPropertyExpensePeriodDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROPERTYEXPENSEPERIOD", this.getDfPropertyExpensePeriodDesc(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpenseId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyExpenseType()
	{
		return (String)getValue(FIELD_DFPROPERTYEXPENSETYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseType(String value)
	{
		setValue(FIELD_DFPROPERTYEXPENSETYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyExpenseDesc()
	{
		return (String)getValue(FIELD_DFPROPERTYEXPENSEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseDesc(String value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyExpensePeriodDesc()
	{
		return (String)getValue(FIELD_DFPROPERTYEXPENSEPERIODDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpensePeriodDesc(String value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEPERIODDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpenseAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyExpenseIncludeInGDS()
	{
		return (String)getValue(FIELD_DFPROPERTYEXPENSEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseIncludeInGDS(String value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEINCLUDEINGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpensePercentageIncludedGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpensePercentageIncludedGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPropertyExpenseIncludeTDS()
	{
		return (String)getValue(FIELD_DFPROPERTYEXPENSEINCLUDETDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpenseIncludeTDS(String value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEINCLUDETDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyExpensePercentageIncludedTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyExpensePercentageIncludedTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}

	  public java.math.BigDecimal getDfInstitutionId() {
		    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
		  }

		  public void setDfInstitutionId(java.math.BigDecimal value) {
		    setValue(FIELD_DFINSTITUTIONID, value);
		  }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 18Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT PROPERTYEXPENSE.PROPERTYEXPENSEID, PROPERTYEXPENSE.PROPERTYID, PROPERTYEXPENSETYPE.PETDESCRIPTION, PROPERTYEXPENSE.PROPERTYEXPENSEDESCRIPTION, PROPERTYEXPENSEPERIOD.PEDESCRIPTION, PROPERTYEXPENSE.PROPERTYEXPENSEAMOUNT, PROPERTYEXPENSE.PEINCLUDEINGDS, PROPERTYEXPENSE.PEPERCENTINGDS, PROPERTYEXPENSE.PEINCLUDEINTDS, PROPERTYEXPENSE.PEPERCENTINTDS, PROPERTYEXPENSE.COPYID FROM PROPERTYEXPENSE, PROPERTYEXPENSEPERIOD, PROPERTYEXPENSETYPE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT PROPERTYEXPENSE.PROPERTYEXPENSEID, PROPERTYEXPENSE.PROPERTYID, "+
    "to_char(PROPERTYEXPENSE.PROPERTYEXPENSETYPEID) PROPERTYEXPENSETYPEID_STR, PROPERTYEXPENSE.PROPERTYEXPENSEDESCRIPTION, "+
    "to_char(PROPERTYEXPENSE.PROPERTYEXPENSEPERIODID) PROPERTYEXPENSEPERIODID_STR, PROPERTYEXPENSE.PROPERTYEXPENSEAMOUNT, "+
    "PROPERTYEXPENSE.PEINCLUDEINGDS, PROPERTYEXPENSE.PEPERCENTINGDS, "+
    "PROPERTYEXPENSE.PEINCLUDEINTDS, PROPERTYEXPENSE.PEPERCENTINTDS, "+
    "PROPERTYEXPENSE.COPYID, "+
    "PROPERTYEXPENSE.INSTITUTIONPROFILEID "+
    " FROM PROPERTYEXPENSE  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTYEXPENSE, PROPERTYEXPENSEPERIOD, PROPERTYEXPENSETYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "PROPERTYEXPENSE";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (PROPERTYEXPENSE.PROPERTYEXPENSETYPEID  =  PROPERTYEXPENSETYPE.PROPERTYEXPENSETYPEID) AND (PROPERTYEXPENSE.PROPERTYEXPENSEPERIODID  =  PROPERTYEXPENSEPERIOD.PROPERTYEXPENSEPERIODID)";
	public static final String STATIC_WHERE_CRITERIA=
    "";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEID="PROPERTYEXPENSE.PROPERTYEXPENSEID";
	public static final String COLUMN_DFPROPERTYEXPENSEID="PROPERTYEXPENSEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTYEXPENSE.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEDESC="PROPERTYEXPENSE.PROPERTYEXPENSEDESCRIPTION";
	public static final String COLUMN_DFPROPERTYEXPENSEDESC="PROPERTYEXPENSEDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEAMOUNT="PROPERTYEXPENSE.PROPERTYEXPENSEAMOUNT";
	public static final String COLUMN_DFPROPERTYEXPENSEAMOUNT="PROPERTYEXPENSEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEINCLUDEINGDS="PROPERTYEXPENSE.PEINCLUDEINGDS";
	public static final String COLUMN_DFPROPERTYEXPENSEINCLUDEINGDS="PEINCLUDEINGDS";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS="PROPERTYEXPENSE.PEPERCENTINGDS";
	public static final String COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS="PEPERCENTINGDS";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEINCLUDETDS="PROPERTYEXPENSE.PEINCLUDEINTDS";
	public static final String COLUMN_DFPROPERTYEXPENSEINCLUDETDS="PEINCLUDEINTDS";
	public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS="PROPERTYEXPENSE.PEPERCENTINTDS";
	public static final String COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS="PEPERCENTINTDS";
	public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTYEXPENSE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSETYPE="PROPERTYEXPENSETYPE.PETDESCRIPTION";
	//public static final String COLUMN_DFPROPERTYEXPENSETYPE="PETDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSETYPE="PROPERTYEXPENSE.PROPERTYEXPENSETYPEID_STR";
	public static final String COLUMN_DFPROPERTYEXPENSETYPE="PROPERTYEXPENSETYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERIODDESC="PROPERTYEXPENSEPERIOD.PEDESCRIPTION";
	//public static final String COLUMN_DFPROPERTYEXPENSEPERIODDESC="PEDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERIODDESC="PROPERTYEXPENSE.PROPERTYEXPENSEPERIODID_STR";
	public static final String COLUMN_DFPROPERTYEXPENSEPERIODDESC="PROPERTYEXPENSEPERIODID_STR";
	  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
		    "PROPERTYEXPENSE.INSTITUTIONPROFILEID";
		  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEID,
				COLUMN_DFPROPERTYEXPENSEID,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSETYPE,
				COLUMN_DFPROPERTYEXPENSETYPE,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSETYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEDESC,
				COLUMN_DFPROPERTYEXPENSEDESC,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEPERIODDESC,
				COLUMN_DFPROPERTYEXPENSEPERIODDESC,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERIODDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEAMOUNT,
				COLUMN_DFPROPERTYEXPENSEAMOUNT,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEINCLUDEINGDS,
				COLUMN_DFPROPERTYEXPENSEINCLUDEINGDS,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEINCLUDEINGDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,
				COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEINCLUDETDS,
				COLUMN_DFPROPERTYEXPENSEINCLUDETDS,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEINCLUDETDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,
				COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,
				QUALIFIED_COLUMN_DFPROPERTYEXPENSEPERCENTAGEINCLUDEDTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	}

}

