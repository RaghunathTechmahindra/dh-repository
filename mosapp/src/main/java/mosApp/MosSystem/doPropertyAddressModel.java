package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doPropertyAddressModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyStreetTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetTypeDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetTypeDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPropertyStreetDirectionId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetDirectionId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStreetDirectionDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfStreetDirectionDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyStreetNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPropertyStreetName();

	
	/**
	 * 
	 * 
	 */
	public void setDfPropertyStreetName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPrimaryPropertyFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfPrimaryPropertyFlag(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPROPERTYDEALID="dfPropertyDealId";
	public static final String FIELD_DFPROPERTYID="dfPropertyId";
	public static final String FIELD_DFPROPERTYCOPYID="dfPropertyCopyId";
	public static final String FIELD_DFPROPERTYSTREETTYPEID="dfPropertyStreetTypeId";
	public static final String FIELD_DFSTREETTYPEDESCRIPTION="dfStreetTypeDescription";
	public static final String FIELD_DFPROPERTYSTREETDIRECTIONID="dfPropertyStreetDirectionId";
	public static final String FIELD_DFSTREETDIRECTIONDESCRIPTION="dfStreetDirectionDescription";
	public static final String FIELD_DFPROPERTYADDRESSLINE2="dfPropertyAddressLine2";
	public static final String FIELD_DFPROPERTYSTREETNUMBER="dfPropertyStreetNumber";
	public static final String FIELD_DFPROPERTYSTREETNAME="dfPropertyStreetName";
	public static final String FIELD_DFPRIMARYPROPERTYFLAG="dfPrimaryPropertyFlag";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

