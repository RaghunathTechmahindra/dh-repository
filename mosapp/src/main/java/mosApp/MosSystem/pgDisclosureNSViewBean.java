package mosApp.MosSystem;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;
import java.io.IOException;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.view.event.RequestInvocationEvent;

/**
 * <p>Title: pgDisclosureNSViewBean
 *
 * Description: View bean for Disclosure NS PAGE
 * Company: Filogix Inc.
 * Date: 11/28/2006 
 * @author NBC/PP Implementation Team
 * @version 1.0 
 *
 */
public class pgDisclosureNSViewBean extends pgDisclosureViewBean {
	/**
	 * pgDisclosureNSViewBean() This constructor sets the url of the jsp and also
	 * makes a call to the registerChildren() method 
	 * Date: 11/28/2006
	 * 
	 * @param name
	 *            This is the name of the child form element to create	 
	 *
	 **/
	public pgDisclosureNSViewBean() {
		super(PAGE_NAME);
		setDefaultDisplayURL(DISPLAY_URL);
		setDefaultDisplayUrl(DISPLAY_URL);
		registerChildren();

	}

	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * createChild() This method covers creating form elements specific to NS.
	 * The method calls createChild() of the parent base view bean to load the
	 * base elements common to the page<br>
	 * Date: 11/28/2006
	 * 
	 * @param name
	 *            This is the name of the child form element to instantiate.
	 * @return Name of child view if there is a child associated with the container 
	 * 
	 */
	protected View createChild(String name) {
		View childFromParent = super.createChild(name);

		if (childFromParent != null) {
			return childFromParent;
		}

		else if (name.equals(CHILD_CBDATETOAPPEARMONTH_NS)) {
			ComboBox child = new ComboBox(this, getDefaultModel(),
					CHILD_CBDATETOAPPEARMONTH_NS, CHILD_CBDATETOAPPEARMONTH_NS,
					CHILD_CBDATETOAPPEARMONTH_RESET_VALUE_NS, null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMonthOptions);
			return child;
		} else if (name.equals(CHILD_TXDATETOAPPEARDAY_NS)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXDATETOAPPEARDAY_NS, CHILD_TXDATETOAPPEARDAY_NS,
					CHILD_TXDATETOAPPEARDAY_RESET_VALUE_NS, null);
			return child;
		} else if (name.equals(CHILD_TXDATETOAPPEARYEAR_NS)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXDATETOAPPEARYEAR_NS, CHILD_TXDATETOAPPEARYEAR_NS,
					CHILD_TXDATETOAPPEARYEAR_RESET_VALUE_NS, null);
			return child;
		} else if (name.equals(CHILD_TXCALCINTEREST)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXCALCINTEREST, CHILD_TXCALCINTEREST,
					CHILD_TXCALCINTEREST_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_CKCONSTMORTYES)) {
			CheckBox child = new CheckBox(this, getDefaultModel(),
					CHILD_CKCONSTMORTYES, CHILD_CKCONSTMORTYES, "Y", "N",
					false, null);
			return child;
		} else if (name.equals(CHILD_CKCONSTMORTNO)) {
			CheckBox child = new CheckBox(this, getDefaultModel(),
					CHILD_CKCONSTMORTNO, CHILD_CKCONSTMORTNO, "Y", "N", false,
					null);
			return child;
		} else if (name.equals(CHILD_TXRATEVAR)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXRATEVAR, CHILD_TXRATEVAR,
					CHILD_TXRATEVAR_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_TXREPBEFMAT)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXREPBEFMAT, CHILD_TXREPBEFMAT,
					CHILD_TXREPBEFMAT_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_TXLOANNOTREPD)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXLOANNOTREPD, CHILD_TXLOANNOTREPD,
					CHILD_TXLOANNOTREPD_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_CKTAXESPAIDYES)) {
			CheckBox child = new CheckBox(this, getDefaultModel(),
					CHILD_CKTAXESPAIDYES, CHILD_CKTAXESPAIDYES, "Y", "N",
					false, null);
			return child;
		} else if (name.equals(CHILD_CKTAXESPAIDNO)) {
			CheckBox child = new CheckBox(this, getDefaultModel(),
					CHILD_CKTAXESPAIDNO, CHILD_CKTAXESPAIDNO, "Y", "N", false,
					null);
			return child;
		} else if (name.equals(CHILD_TXINTPAIDMANNER)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXINTPAIDMANNER, CHILD_TXINTPAIDMANNER,
					CHILD_TXINTPAIDMANNER_RESET_VALUE, null);
			return child;
		}

		else
			{
			throw new IllegalArgumentException("Invalid child name [" + name
			
					+ "]");
			}
	}

	/**
	 * resetChildren()
	 * This method covers reseting form elements specific to NS
	 * The method calls resetChildren() from the parent base view bean
	 * to reset the base elements common to the page
	 * Date: 11/28/2006
	 *
	 */

	public void resetChildren() {
		super.resetChildren();
		
		getCbDateToAppearMonthNS().setValue(
				CHILD_CBDATETOAPPEARMONTH_RESET_VALUE_NS);
		getTxDateToAppearDayNS().setValue(
				CHILD_TXDATETOAPPEARDAY_RESET_VALUE_NS);
		getTxDateToAppearYearNS().setValue(
				CHILD_TXDATETOAPPEARYEAR_RESET_VALUE_NS);
		getTxCalcInt().setValue(CHILD_TXCALCINTEREST_RESET_VALUE);
		getTxRateVar().setValue(CHILD_TXRATEVAR_RESET_VALUE);
		getTxRepayBefMat().setValue(CHILD_TXREPBEFMAT_RESET_VALUE);
		getTxLoanNotRepd().setValue(CHILD_TXLOANNOTREPD_RESET_VALUE);
		getTxIntPaidManner().setValue(CHILD_TXINTPAIDMANNER_RESET_VALUE);
	}

	/**
	 * registerChildren()
	 * This method covers regestering form elements specific to NS
	 * The method calls registerChildren() from the parent base view bean
	 * to register the base elements common to the page 
	 * Date: 11/28/2006
	 */

	protected void registerChildren() {
		super.registerChildren();
		registerChild(CHILD_CBDATETOAPPEARMONTH_NS, ComboBox.class);
		registerChild(CHILD_TXDATETOAPPEARDAY_NS, TextField.class);
		registerChild(CHILD_TXDATETOAPPEARYEAR_NS, TextField.class);
		registerChild(CHILD_TXCALCINTEREST, TextField.class);
		registerChild(CHILD_CKCONSTMORTYES, CheckBox.class);
		registerChild(CHILD_CKCONSTMORTNO, CheckBox.class);
		registerChild(CHILD_TXRATEVAR, TextField.class);
		registerChild(CHILD_TXREPBEFMAT, TextField.class);
		registerChild(CHILD_TXLOANNOTREPD, TextField.class);
		registerChild(CHILD_CKTAXESPAIDYES, CheckBox.class);
		registerChild(CHILD_CKTAXESPAIDNO, CheckBox.class);
		registerChild(CHILD_TXINTPAIDMANNER, TextField.class);
	}

	/**
	 * beginDisplay()
	 * This method renders the jsp
	 * Date: 11/28/2006
	 * @param event DisplayEvent
	 */
	public void beginDisplay(DisplayEvent event) throws ModelControlException {
		if (getdoDisclosureSelectModel() == null)
			throw new ModelControlException("Primary model is null");

		DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		// populate the cbYesNoOptions
		String yesStr = BXResources.getGenericMsg("YES_LABEL", handler
				.getTheSessionState().getLanguageId());
		String noStr = BXResources.getGenericMsg("NO_LABEL", handler
				.getTheSessionState().getLanguageId());

		cbYesNoOptions.setOptions(new String[] { noStr, yesStr }, new String[] {
				"N", "Y" });

		// populate the months
		cbMonthOptions.populate(getRequestContext());

		handler.overridePageLabel(62);

		super.beginDisplay(event);
	}

	
	/**
	 * getCbDateToAppearMonthNS() - getter for CHILD_CBDATETOAPPEARMONTH_NS
	 * 
	 * Date: 11/28/2006
	 * @return ComboBox form field
	 */
	public ComboBox getCbDateToAppearMonthNS() {
		return (ComboBox) getChild(CHILD_CBDATETOAPPEARMONTH_NS);
	}
	/**
	 * getTxDateToAppearDayNS() - getter for CHILD_TXDATETOAPPEARDAY_NS
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	public TextField getTxDateToAppearDayNS() {
		return (TextField) getChild(CHILD_TXDATETOAPPEARDAY_NS);
	}
	/**
	 * getTxDateToAppearYearNS() - getter for CHILD_TXDATETOAPPEARYEAR_NS
	 * 
	 * Date: 11/28/2006
	 * @return TextField child field
	 */
	public TextField getTxDateToAppearYearNS() {
		return (TextField) getChild(CHILD_TXDATETOAPPEARYEAR_NS);
	}
	/**
	 * getTxCalcInt() - getter for CHILD_TXCALCINTEREST
	 * 
	 * Date: 11/28/2006
	 * @return TextField child field
	 */
	public TextField getTxCalcInt() {
		return (TextField) getChild(CHILD_TXCALCINTEREST);
	}
	/**
	 * getCkConstMortYes() - getter for CHILD_CKCONSTMORTYES
	 * 
	 * Date: 11/28/2006
	 * @return CheckBox child field
	 */
	public CheckBox getCkConstMortYes() {
		return (CheckBox) getChild(CHILD_CKCONSTMORTYES);
	}
	/**
	 * getCkConstMortNo() - getter for CHILD_CKCONSTMORTNO
	 * 
	 * Date: 11/28/2006
	 * @return CheckBox child field
	 */
	public CheckBox getCkConstMortNo() {
		return (CheckBox) getChild(CHILD_CKCONSTMORTNO);
	}
	/**
	 * getTxRateVar() - getter for CHILD_TXRATEVAR
	 * 
	 * Date: 11/28/2006
	 * @return TextField child field
	 */
	public TextField getTxRateVar() {
		return (TextField) getChild(CHILD_TXRATEVAR);
	}
	/**
	 * getTxRepayBefMat() - getter for CHILD_TXREPBEFMAT
	 * 
	 * Date: 11/28/2006
	 * @return TextField child field
	 */
	public TextField getTxRepayBefMat() {
		return (TextField) getChild(CHILD_TXREPBEFMAT);
	}
	/**
	 * getTxLoanNotRepd() - getter for CHILD_TXLOANNOTREPD
	 * 
	 * Date: 11/28/2006
	 * @return TextField child field
	 */
	public TextField getTxLoanNotRepd() {
		return (TextField) getChild(CHILD_TXLOANNOTREPD);
	}
	/**
	 * getCkTaxesPaidYes() - getter for CHILD_CKTAXESPAIDYES
	 * 
	 * Date: 11/28/2006
	 * @return CheckBox child field
	 */
	public CheckBox getCkTaxesPaidYes() {
		return (CheckBox) getChild(CHILD_CKTAXESPAIDYES);
	}
	/**
	 * getCkTaxesPaidNo() - getter for CHILD_CKTAXESPAIDNO
	 * 
	 * Date: 11/28/2006
	 * @return CheckBox form field
	 */
	public CheckBox getCkTaxesPaidNo() {
		return (CheckBox) getChild(CHILD_CKTAXESPAIDNO);
	}
	/**
	 * getTxIntPaidManner() - getter for CHILD_TXINTPAIDMANNER
	 * 
	 * Date: 11/28/2006
	 * @return TextField child field
	 */
	public TextField getTxIntPaidManner() {
		return (TextField) getChild(CHILD_TXINTPAIDMANNER);
	}

	
	/**
	 *
	 * <p>Title: CbMonthOptionList</p>
	 *
	 * <p>Description: Inner class that represents the options in the month CBs on this page</p>
	 *
	 * <p>Copyright: </p>
	 *
	 * <p>Company: </p>
	 *
	 * @author not attributable
	 * @version 1.0
	 */
	static class CbMonthOptionList extends OptionList {
		CbMonthOptionList() {

		}

		/**
		 * populate() 
		 * populate the option list
		 * @Param -  RequestContext
		 * @return 
		 * Date: 11/28/2006
		 */
		public void populate(RequestContext rc) {
			try {
				//Get Language from SessionState
				//Get the Language ID from the SessionStateModel
				String defaultInstanceStateName = rc.getModelManager()
						.getDefaultModelInstanceName(SessionStateModel.class);
				SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
						.getModelManager().getModel(SessionStateModel.class,
								defaultInstanceStateName, true);

				int languageId = theSessionState.getLanguageId();

				//Get IDs and Labels from BXResources
				Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"MONTHS",
						languageId);
				Iterator l = c.iterator();
				String[] theVal = new String[2];

				// remove all values first
				clear();

				while (l.hasNext()) {
					theVal = (String[]) (l.next());
					add(theVal[1], theVal[0]);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	  /**
	   * beforeModelExecutes <br>
	   * This method executes any application logic required before rendering the jsp <br>
	   * Creation Date: 11/28/2006
	   * @Param model
	   *            The model that is about to be auto-executed
	   * @Param executionContext
	   *            The context under which this model is being executed
	   * @return True if the execution of the model should proceed, false to skip
	   *         execution of the model
	   */
	  public boolean beforeModelExecutes(Model model, int executionContext)
	  {
		DisclosureHandler handler = this.handler.cloneSS();
		handler.pageGetState(this);
		handler.setupBeforePageGeneration();
		handler.pageSaveState();
		return super.beforeModelExecutes(model, executionContext);
	  }

	  /**
	   * afterModelExecutes <br>
	   * This method executes any application logic required after rendering the
	   * jsp <br>
	   * Creation Date: 11/28/2006
	   * @Param model
	   *            The model that is about to be auto-executed
	   * @Param executionContext
	   *            The context under which this model is being executed
	   */
	  public void afterModelExecutes(Model model, int executionContext)
	  {
		super.afterModelExecutes(model, executionContext);
	  }

	  /**
	   * afterModelExecutes <br>
	   * This method executes after all models have finished execution <br>
	   * Creation Date: 11/28/2006
	   * @Param executionContext
	   *            The context under which this model is being executed
	   */
	  public void afterAllModelsExecute(int executionContext)
	  {
		DisclosureHandler handler = this.handler.cloneSS();
		handler.pageGetState(this);
		handler.populatePageDisplayFields();
		handler.pageSaveState();
		super.afterAllModelsExecute(executionContext);
	  }

	  /**
	   * onModelError <br>
	   * Invoked as notification that a given model threw a <code>
	   * ModelControlException</code>
	   * during auto-execution.<br>
	   * Creation Date: 11/28/2006
	   * @Param model
	   *            The model that which caused an execution error
	   * @Param executionContext
	   *            The context under which this model is being executed
	   * @param exception
	   *            The exception thrown during auto-execution
	   */
	  public void onModelError(Model model, int executionContext,
		  ModelControlException exception) throws ModelControlException
	  {
		super.onModelError(model, executionContext, exception);
	  }

	  /**
	   * handleBtProceedRequest <br>
	   * This method handles the go button click <br>
	   * Creation Date: 11/28/2006
	   * @Param event
	   *            The RequestInvocationEvent.
	   */
	  public void handleBtProceedRequest(RequestInvocationEvent event)
		  throws ServletException, IOException
	  {
		DisclosureHandler handler = this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleGoPage();
		handler.postHandlerProtocol();
	  }

	  /**
	   * getDisplayURL <br>
	   * This method overrides the getDefaultURL() framework JATO method and it is
	   * located in each ViewBean. This allows not to stay with the JATO ViewBean
	   * extension, otherwise each ViewBean a.k.a page should extend its Handler
	   * (to be called by the framework) and it is not good. DEFAULT_DISPLAY_URL
	   * String is a variable now. The full method is still in PHC base class. It
	   * should care the the url="/" case (non-initialized defaultURL) by the BX
	   * framework methods. <br>
	   * Creation Date: 11/28/2006
	   * @return Url for the jsp which is displayed
	   */
	  public String getDisplayURL()
	  {
		DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		String url = getDefaultDisplayURL();
		int languageId = handler.theSessionState.getLanguageId();
		if (url != null && !url.trim().equals("") && !url.trim().equals("/"))
		{
		  url = BXResources.getBXUrl(url, languageId);
		}
		else
		{
		  url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
		}
		return url;
	  }


	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *the value has the page tag on the top of the jsp page
	 *@value "pgDisclosureNS"
	 */
	public static final String PAGE_NAME = "pgDisclosureNS";
	/**
	 *holds the options yes no
	 */
	// populated in beginDisplay()
	private OptionList cbYesNoOptions = new OptionList();

	/**
	 *cbMonthOptions - holds the months options
	 */
	private OptionList cbMonthOptions = new CbMonthOptionList();
	/**
	 * Variable to hold the value of the month of disclosure
	 * @value "104Q010A_M"
	 */
	public static final String CHILD_CBDATETOAPPEARMONTH_NS = "104Q010A_M";
	/**
	 * Resets the value of the month of disclosure
	 * @value ""
	 */
	public static final String CHILD_CBDATETOAPPEARMONTH_RESET_VALUE_NS = "";
	/**
	 * Variable to hold the value of the day of disclosure
	 * @value "104Q010A_D"
	 */
	public static final String CHILD_TXDATETOAPPEARDAY_NS = "104Q010A_D";
	/**
	 * Resets the value of the day of disclosure
	 * @value ""
	 */
	public static final String CHILD_TXDATETOAPPEARDAY_RESET_VALUE_NS = "";
	/**
	 * Variable to hold the value of the year of disclosure
	 * @value "104Q010A_Y"
	 */
	public static final String CHILD_TXDATETOAPPEARYEAR_NS = "104Q010A_Y";
	/**
	 * Resets the value of the year of disclosure
	 * @value ""
	 */
	public static final String CHILD_TXDATETOAPPEARYEAR_RESET_VALUE_NS = "";
	/**
	 * Variable to hold the value of the 'calculation of interest is as follows:' field
	 * @value "104Q090W"
	 */
	public static final String CHILD_TXCALCINTEREST = "104Q090W";
	/**
	 * Variable to hold the value of the method of calculation of interest is as follows:
	 * @value ""
	 */
	public static final String CHILD_TXCALCINTEREST_RESET_VALUE = "";
	/**
	 * Variable to hold the value of the 'Section 14. In the case of a construction mortgage, interest will' field
	 * @value "104Q14AE_Y"
	 */
	public static final String CHILD_CKCONSTMORTYES = "104Q14AE_Y";
	/** Variable to hold the value of the 'Section 14. In the case of a construction mortgage, interest will' field
	 * @value ""
	 */
	public static final String CHILD_CKCONSTMORTNO = "104Q14AE_N";
	/**
	 * Variable to hold the value of the 'Section 15. Where the annual percentage rate for the term of the mortgage is subject to variations,
	 * it shall vary in the following manner:' field
	 * @value "104Q15AF"
	 */
	public static final String CHILD_TXRATEVAR = "104Q15AF";
	/**
	 * Variable to hold the reset value of the 'Section 15. Where the annual percentage rate for the term of the mortgage is subject to variations,
	 * it shall vary in the following manner:' field
	 * @value ""
	 */
	public static final String CHILD_TXRATEVAR_RESET_VALUE = "";
	/** Variable to hold the value of the 'Section  16' field
	 * @value "104Q16AG"
	 */
	public static final String CHILD_TXREPBEFMAT = "104Q16AG";
	/** Variable to hold the reset value of the 'Section  16' field
	 * @value ""
	 */
	public static final String CHILD_TXREPBEFMAT_RESET_VALUE = "";
	/** Variable to hold the  value of the 'Section Section 17' field
	 * @value "104Q17AH"
	 */
	public static final String CHILD_TXLOANNOTREPD = "104Q17AH";
	/** Variable to hold the  reset value of the 'Section  17' field
	 * @value ""
	 */
	public static final String CHILD_TXLOANNOTREPD_RESET_VALUE = "";
	/** Variable to hold the  value of the 'Section  18' field
	 * @value "104Q17AI_Y"
	 */
	public static final String CHILD_CKTAXESPAIDYES = "104Q17AI_Y";
	/** Variable to hold the  value of the 'Section  18' field
	 * @value "104Q17AI_N"
	 */
	public static final String CHILD_CKTAXESPAIDNO = "104Q17AI_N";
	/** Variable to hold the  value of the 'If 'Yes' interest will be paid in the following manner:' field
	 * @value "104Q18AJ"
	 */
	public static final String CHILD_TXINTPAIDMANNER = "104Q18AJ";
	/** Variable to hold the reset value of the 'If 'Yes' interest will be paid in the following manner:' field
	 * @value ""
	 */
	public static final String CHILD_TXINTPAIDMANNER_RESET_VALUE = "";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	private String DISPLAY_URL = "/mosApp/MosSystem/pgDisclosureNS.jsp";
}
