package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>
 * Title: pgApplicantEntryRepeatedIdentificationTiledView
 * </p>
 * 
 * <p>
 * Description: view bean class for Repeated Identification Tile on Applicant 
 * Entry page
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.0
 * 
 */
public class pgApplicantEntryRepeatedIdentificationTiledView extends
		RequestHandlingTiledViewBase {

	/**
	 * 
	 */
	public pgApplicantEntryRepeatedIdentificationTiledView() {
		super();
	}

	/**
	 * @param parent
	 * @param name
	 */
	public pgApplicantEntryRepeatedIdentificationTiledView(View parent,
			String name) {
		super(parent, name);
		setMaxDisplayTiles(100);
		setPrimaryModelClass(doApplicantIdentificationTypeModel.class);
		registerChildren();
		initialize();
	}

	/**
	 * 
	 * 
	 */
	protected void initialize() {
	}

	/**
	 * <p> reset all children </p>
	 */
	public void resetChildren() {
		getHdIdentificationId().setValue(CHILD_HDIDENTIFICATIONID_RESET_VALUE);
		getHdIdentificationId().setValue(
				CHILD_HDIDENTIFICATIONCOPYID_RESET_VALUE);
		getCbIdentificationType().setValue(
				CHILD_CBIDENTIFICATIONTYPE_RESET_VALUE);
		getTxIdentificationNumber().setValue(
				CHILD_TXIDENTIFICATIONNUMBER_RESET_VALUE);
		getTxIdentificationSourceCountry().setValue(
				CHILD_TXIDENTIFICATIONSOURCECOUNTRY_RESET_VALUE);

	}

	/**
	 * <p> register all children </p>
	 */
	protected void registerChildren() {
		registerChild(CHILD_HDIDENTIFICATIONID, HiddenField.class);
		registerChild(CHILD_HDIDENTIFICATIONCOPYID, HiddenField.class);
		registerChild(CHILD_CBIDENTIFICATIONTYPE, ComboBox.class);
		registerChild(CHILD_TXIDENTIFICATIONNUMBER, TextField.class);
		registerChild(CHILD_TXIDENTIFICATIONSOURCECOUNTRY, TextField.class);
		registerChild(CHILD_BTDELETEAPPLICANTIDENTIFICATION, Button.class);
		registerChild(CHILD_STTARGETIDENTIFICATION, StaticTextField.class);
	}

	// //////////////////////////////////////////////////////////////////////////////
	// Model management methods
	// //////////////////////////////////////////////////////////////////////////////

	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.WebActionHandler#getWebActionModels(int)
	 */
	public Model[] getWebActionModels(int executionType) {
		List modelList = new ArrayList();
		switch (executionType) {
		case MODEL_TYPE_RETRIEVE:
			;
			break;

		case MODEL_TYPE_UPDATE:
			;
			break;

		case MODEL_TYPE_DELETE:
			;
			break;

		case MODEL_TYPE_INSERT:
			;
			break;

		case MODEL_TYPE_EXECUTE:
			;
			break;
		}
		return (Model[]) modelList.toArray(new Model[0]);
	}

	// //////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	// //////////////////////////////////////////////////////////////////////////////

	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.ContainerView#beginDisplay(com.iplanet.jato.view.event.DisplayEvent)
	 */
	public void beginDisplay(DisplayEvent event) throws ModelControlException {

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null)
			throw new ModelControlException("Primary model is null");

		ApplicantEntryHandler handler = (ApplicantEntryHandler) this.handler
				.cloneSS();

		handler.pageGetState(this.getParentViewBean());
		// Set up all ComboBox options
		cbIdentificationTypeOptions.populate(getRequestContext());
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();

	}


	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.TiledView#nextTile()
	 */
	public boolean nextTile() throws ModelControlException {
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow) {
			// Put migrated RepeatedEmployment_onBeforeRowDisplayEvent code here
			int rowNum = this.getTileIndex();
			boolean ret = true;

			ApplicantEntryHandler handler = (ApplicantEntryHandler) this.handler
					.cloneSS();
			handler.pageGetState(this.getParentViewBean());

			// Get Data Object
			doApplicantIdentificationTypeModelImpl theObj = 
					(doApplicantIdentificationTypeModelImpl) this
					.getdoApplicantIdentificationTypeSelectModel();

			if (theObj.getSize() > 0) {
				handler.setApplicantIdentificationDisplayFields(theObj, rowNum);
			} else
				ret = false;

			handler.pageSaveState();

			return ret;
		}

		return movedToRow;
	}

	/**
	 * <p>handles delete identification requests</p>
	 * @param event
	 * @throws ServletException
	 * @throws IOException
	 */
	public void handleBtDeleteApplicantIdentificationRequest(
			RequestInvocationEvent event) throws ServletException, IOException {
	
		ApplicantEntryHandler handler = (ApplicantEntryHandler) this.handler
				.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());
		
		// 14 maps to TARGET_APP_IDENTIFICATION from Mc
		// this is also mapped to switch case 14 in DeleteEntityProperties
		
		handler.handleDelete(false, 14, handler 
				.getRowNdxFromWebEventMethod(event),
				"RepeatedIdentification/hdIdentificationId",
				"identificationId",
				"RepeatedIdentification/hdIdentificationCopyId",
				"BorrowerIdentification");

		handler.postHandlerProtocol();
	}

	/**
	 * <p>returns Button corresponding to deletion of Identification</p>
	 * @return - returns Button
	 */
	public Button getBtDeleteApplicantIdentification() {
		return (Button) getChild(CHILD_BTDELETEAPPLICANTIDENTIFICATION);
	}


	/**
	 * 
	 * @param event - ChildContentDisplayEvent
	 * @return  
	 */
	public String endBtDeleteApplicantIdentificationDisplay(
			ChildContentDisplayEvent event) {
		// The following code block was migrated from the
		// btDeleteEmployment_onBeforeHtmlOutputEvent method
		ApplicantEntryHandler handler = (ApplicantEntryHandler) this.handler
				.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if (rc == true)
			return event.getContent();
		else
			return "";
	}

	/**
	 * <p>returns doApplicantIdentificationTypeModel</p>
	 * @return - returns doApplicantIdentificationTypeModel
	 */
	public doApplicantIdentificationTypeModel 
			getdoApplicantIdentificationTypeSelectModel() {
		
		if (doApplicantIdentificationType == null)
			doApplicantIdentificationType = 
					(doApplicantIdentificationTypeModel) 
					getModel(doApplicantIdentificationTypeModel.class);

		return doApplicantIdentificationType;
	}


	/**
	 * <p>Sets doApplicantIdentificationTypeSelectModel</p>
	 * @param model - doApplicantIdentificationTypeSelectModel
	 */
	public void setdoApplicantIdentificationTypeSelectModel(
			doApplicantIdentificationTypeModel model) {
		doApplicantIdentificationType = model;
	}

	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.RequestHandlingTiledViewBase#beforeModelExecutes(com.iplanet.jato.model.Model, int)
	 */
	public boolean beforeModelExecutes(Model model, int executionContext) {
		return super.beforeModelExecutes(model, executionContext);
	}

	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.RequestHandlingTiledViewBase#afterModelExecutes(com.iplanet.jato.model.Model, int)
	 */
	public void afterModelExecutes(Model model, int executionContext) {

		// This is the analog of NetDynamics
		// repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}

	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.RequestHandlingTiledViewBase#afterAllModelsExecute(int)
	 */
	public void afterAllModelsExecute(int executionContext) {

		// This is the analog of NetDynamics
		// repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.RequestHandlingTiledViewBase#onModelError(com.iplanet.jato.model.Model, int, com.iplanet.jato.model.ModelControlException)
	 */
	public void onModelError(Model model, int executionContext,
			ModelControlException exception) throws ModelControlException {

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}

	public static Map FIELD_DESCRIPTORS;

	public static final String CHILD_HDIDENTIFICATIONID = "hdIdentificationId";

	public static final String CHILD_HDIDENTIFICATIONID_RESET_VALUE = "";

	public static final String CHILD_HDIDENTIFICATIONCOPYID = 
			"hdIdentificationCopyId";

	public static final String CHILD_HDIDENTIFICATIONCOPYID_RESET_VALUE = "";

	public static final String CHILD_TXIDENTIFICATIONNUMBER = 
			"txIdentificationNumber";

	public static final String CHILD_TXIDENTIFICATIONNUMBER_RESET_VALUE = "";

	public static final String CHILD_CBIDENTIFICATIONTYPE = 
			"cbIdentificationType";

	public static final String CHILD_CBIDENTIFICATIONTYPE_RESET_VALUE = "";

	public static final String CHILD_TXIDENTIFICATIONSOURCECOUNTRY = 
			"txIdentificationSourceCountry";

	public static final String CHILD_TXIDENTIFICATIONSOURCECOUNTRY_RESET_VALUE =
			"";

	public static final String CHILD_BTDELETEAPPLICANTIDENTIFICATION = 
			"btDeleteApplicantIdentification";

	public static final String CHILD_BTDELETEAPPLICANTIDENTIFICATION_RESET_VALUE
			= "";

	public static final String CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION = 
			"btAddAdditionalApplicantIdentification";

	public static final String 
			CHILD_BTADDADDITIONALAPPLICANTIDENTIFICATION_RESET_VALUE = "";

	public static final String CHILD_STTARGETIDENTIFICATION = 
			"stTargetIdentification";

	public static final String CHILD_STTARGETIDENTIFICATION_RESET_VALUE = "";

	private CbIdentificationTypeOptionList cbIdentificationTypeOptions = 
			new CbIdentificationTypeOptionList();

	// //////////////////////////////////////////////////////////////////////////
	// Instance variables
	// //////////////////////////////////////////////////////////////////////////

	private doApplicantIdentificationTypeModel doApplicantIdentificationType = 
			null;

	private ApplicantEntryHandler handler = new ApplicantEntryHandler();

	// //////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	// //////////////////////////////////////////////////////////////////////////////

	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.ContainerViewBase#createChild(java.lang.String)
	 */
	protected View createChild(String name) {
		if (name.equals(CHILD_HDIDENTIFICATIONID)) {
			HiddenField child = new HiddenField(
					this,
					getdoApplicantIdentificationTypeSelectModel(),
					CHILD_HDIDENTIFICATIONID,
					doApplicantIdentificationTypeModel.FIELD_DFIDENTIFICATIONID,
					CHILD_HDIDENTIFICATIONID_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_HDIDENTIFICATIONCOPYID)) {
			HiddenField child = new HiddenField(this,
					getdoApplicantIdentificationTypeSelectModel(),
					CHILD_HDIDENTIFICATIONCOPYID,
					doApplicantIdentificationTypeModel.FIELD_DFCOPYID,
					CHILD_HDIDENTIFICATIONCOPYID_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_TXIDENTIFICATIONNUMBER)) {
			TextField child = new TextField(
					this,
					getdoApplicantIdentificationTypeSelectModel(),
					CHILD_TXIDENTIFICATIONNUMBER,
					doApplicantIdentificationTypeModel.FIELD_DFIDENTIFICATIONNUMBER,
					CHILD_TXIDENTIFICATIONNUMBER_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_CBIDENTIFICATIONTYPE)) {
			ComboBox child = new ComboBox(
					this,
					getdoApplicantIdentificationTypeSelectModel(),
					CHILD_CBIDENTIFICATIONTYPE,
					doApplicantIdentificationTypeModel.FIELD_DFIDENTIFICATIONTYPEID,
					CHILD_CBIDENTIFICATIONTYPE_RESET_VALUE, null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIdentificationTypeOptions);
			return child;
		} else if (name.equals(CHILD_TXIDENTIFICATIONSOURCECOUNTRY)) {
			TextField child = new TextField(
					this,
					// getDefaultModel(),
					getdoApplicantIdentificationTypeSelectModel(),
					CHILD_TXIDENTIFICATIONSOURCECOUNTRY,
					doApplicantIdentificationTypeModel.FIELD_DFIDENTIFICATIONCOUNTRY,
					CHILD_TXIDENTIFICATIONSOURCECOUNTRY_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_BTDELETEAPPLICANTIDENTIFICATION)) {
			Button child = new Button(this, getDefaultModel(),
					CHILD_BTDELETEAPPLICANTIDENTIFICATION,
					CHILD_BTDELETEAPPLICANTIDENTIFICATION,
					CHILD_BTDELETEAPPLICANTIDENTIFICATION_RESET_VALUE, null);
			return child;

		} else if (name.equals(CHILD_STTARGETIDENTIFICATION)) {
			StaticTextField child = new StaticTextField(this,
					getDefaultModel(), CHILD_STTARGETIDENTIFICATION,
					CHILD_STTARGETIDENTIFICATION,
					CHILD_STTARGETIDENTIFICATION_RESET_VALUE, null);
			return child;
		} else
			throw new IllegalArgumentException("Invalid child name [" + name
					+ "]");
	}


	/**
	 * <p>returns StaticTextField corresponding to Target Identification<p>
	 * @return - returns StaticTextField
	 */
	public StaticTextField getStTargetIdentification() {
		return (StaticTextField) getChild(CHILD_STTARGETIDENTIFICATION);
	}

	/**
	 * <p>returns HiddenField corresponding to Identification Id<p>
	 * @return - returns HiddenField
	 */
	public HiddenField getHdIdentificationId() {
		return (HiddenField) getChild(CHILD_HDIDENTIFICATIONID);
	}
	
	/**
	 * <p>returns HiddenField corresponding to Identification Copy Id<p>
	 * @return - returns HiddenField
	 */
	public HiddenField getHdIdentificationCopyId() {
		return (HiddenField) getChild(CHILD_HDIDENTIFICATIONCOPYID);
	}
	
	/**
	 * <p>returns TextField corresponding to Identification Number<p>
	 * @return - returns TextField
	 */
	public TextField getTxIdentificationNumber() {
		return (TextField) getChild(CHILD_TXIDENTIFICATIONNUMBER);
	}
	
	/**
	 * <p>returns TextField corresponding to Identification Source Country<p>
	 * @return - returns TextField
	 */
	public TextField getTxIdentificationSourceCountry() {
		return (TextField) getChild(CHILD_TXIDENTIFICATIONSOURCECOUNTRY);
	}
	
	/**
	 * <p>returns ComboBox corresponding to Identification Type<p>
	 * @return - returns ComboBox
	 */
	public ComboBox getCbIdentificationType() {
		return (ComboBox) getChild(CHILD_CBIDENTIFICATIONTYPE);
	}
	

	static class CbIdentificationTypeOptionList extends OptionList {
		CbIdentificationTypeOptionList() {
		}

		public void populate(RequestContext requestContext) {
			try {
				// Get Language from SessionState
				// Get the Language ID from the SessionStateModel
				String defaultInstanceStateName = requestContext.getModelManager()
						.getDefaultModelInstanceName(SessionStateModel.class);

				SessionStateModelImpl theSessionState = 
						(SessionStateModelImpl) requestContext.getModelManager().getModel(
						SessionStateModel.class, defaultInstanceStateName, true);

				int languageId = theSessionState.getLanguageId();

				// Get IDs and Labels from BXResources
				Collection collection 
                    = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
						"IDENTIFICATIONTYPE", languageId);

				Iterator iterator = collection.iterator();
				String[] theVal = new String[2];
				while (iterator.hasNext()) {
					theVal = (String[]) (iterator.next());
					add(theVal[1], theVal[0]);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
