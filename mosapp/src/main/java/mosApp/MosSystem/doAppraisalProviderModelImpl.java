package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;
import com.filogix.externallinks.framework.ServiceConst;

public class doAppraisalProviderModelImpl extends QueryModelBase implements
        doAppraisalProviderModel {

	
    	
    // The logger
    private final static Log _log =
        LogFactory.getLog(doOutsourcedClosingServiceModelImpl.class);

    // static definitions:
    public static final String DATA_SOURCE_NAME = "jdbc/orcl";
    // the select SQL template.
    public static final String SELECT_SQL_TEMPLATE =
        "SELECT DISTINCT " +
        "REQUEST.REQUESTID, " +
        "SERVICEREQUEST.SERVICEPROVIDERREFNO, " +
        "SERVICEREQUEST.SPECIALINSTRUCTIONS, " +
        "SERVICEREQUEST.REQUESTID, " +
        "SERVICEREQUEST.PROPERTYID, " +        
        "SERVICEPRODUCT.SERVICEPROVIDERID, " +
        "to_char(REQUEST.REQUESTSTATUSID) STATUSID_STR, " +
        "REQUEST.REQUESTSTATUSID, " +
        "REQUEST.SERVICEPRODUCTID, " +
        "REQUEST.STATUSMESSAGE, " +
        "REQUEST.STATUSDATE, " +
        "REQUEST.REQUESTDATE, " +
        "REQUEST.DEALID, " +
        "REQUEST.COPYID, " +
        "SERVICEREQUESTCONTACT.NAMEFIRST, " +
        "SERVICEREQUESTCONTACT.NAMELAST, " +
        "SERVICEREQUESTCONTACT.EMAILADDRESS, " +
        "SERVICEREQUESTCONTACT.BORROWERID, " +
        "SERVICEREQUESTCONTACT.SERVICEREQUESTCONTACTID, " +
        "SERVICEREQUESTCONTACT.WORKAREACODE, " +
        "SERVICEREQUESTCONTACT.WORKPHONENUMBER, " +
        "SUBSTR(SERVICEREQUESTCONTACT.WORKPHONENUMBER,0,3) WORKPHONEFIRSTTHREEDIGITS, " +
        "SUBSTR(SERVICEREQUESTCONTACT.WORKPHONENUMBER,4) WORKPHONELASTFOURDIGITS, " +
        "SERVICEREQUESTCONTACT.WORKEXTENSION, " +
        "SERVICEREQUESTCONTACT.CELLAREACODE, " +
        "SERVICEREQUESTCONTACT.CELLPHONENUMBER, " +
        "SUBSTR(SERVICEREQUESTCONTACT.CELLPHONENUMBER,0,3) CELLPHONEFIRSTTHREEDIGITS, " +
        "SUBSTR(SERVICEREQUESTCONTACT.CELLPHONENUMBER,4) CELLPHONELASTFOURDIGITS, " +
        "SERVICEREQUESTCONTACT.HOMEAREACODE, " +
        "SERVICEREQUESTCONTACT.HOMEPHONENUMBER, "+
        "SUBSTR(SERVICEREQUESTCONTACT.HOMEPHONENUMBER,0,3) HOMEPHONEFIRSTTHREEDIGITS, " +
        "SUBSTR(SERVICEREQUESTCONTACT.HOMEPHONENUMBER,4) HOMEPHONELASTFOURDIGITS, " +
        "SERVICEREQUESTCONTACT.PROPERTYOWNERNAME " +
        "FROM " +
        "SERVICEREQUEST, SERVICEREQUESTCONTACT, REQUEST, SERVICEPRODUCT " +
        "__WHERE__ ORDER BY REQUEST.REQUESTDATE DESC";
    
    // the query table name.
    public static final String MODIFYING_QUERY_TABLE_NAME =
        "SERVICEREQUEST, REQUEST, SERVICEREQUESTCONTACT, SERVICEPRODUCT";
    // the static query criteria.
    public static final String STATIC_WHERE_CRITERIA =  
        "(SERVICEREQUEST.INSTITUTIONPROFILEID = REQUEST.INSTITUTIONPROFILEID) AND " +
        "(SERVICEREQUEST.INSTITUTIONPROFILEID = SERVICEREQUESTCONTACT.INSTITUTIONPROFILEID) AND " +
        "(SERVICEREQUEST.INSTITUTIONPROFILEID = SERVICEPRODUCT.INSTITUTIONPROFILEID) AND " +
        "(SERVICEREQUEST.REQUESTID = REQUEST.REQUESTID) AND " +
        "(REQUEST.REQUESTID = SERVICEREQUESTCONTACT.REQUESTID) AND " +
        "(REQUEST.SERVICEPRODUCTID = SERVICEPRODUCT.SERVICEPRODUCTID) AND " + 
        "(SERVICEPRODUCT.SERVICETYPEID = 6) AND " + 
        "(SERVICEPRODUCT.SERVICESUBTYPEID IN (2,10)) ";//Appraisal READ change passing both subtypes i.e Appraisal and READ Appraisal,
           
      
   
    
    //PROPERTY ID
    // the query field schema.
    public static final QueryFieldSchema FIELD_SCHEMA =
        new QueryFieldSchema();

    // column definition for each field.
    public static final String QUALIFIED_COLUMN_DFAPPRAISALDEALID =
        "REQUEST.DEALID";
    public static final String COLUMN_DFAPPRAISALDEALID =
        "DEALID";
   

    public static final String QUALIFIED_COLUMN_DFAPPRAISALCOPYID =
        "REQUEST.COPYID";
    public static final String COLUMN_DFAPPRAISALCOPYID =
        "COPYID";

    public static final String QUALIFIED_COLUMN_DFAPPRAISALBORROWERID =
        "SERVICEREQUESTCONTACT.BORROWERID";
    public static final String COLUMN_DFAPPRAISALBORROWERID =
        "BORROWERID";

    public static final String QUALIFIED_COLUMN_DFAPPRAISALCONTACTID =
        "SERVICEREQUESTCONTACT.SERVICEREQUESTCONTACTID";
    public static final String COLUMN_DFAPPRAISALCONTACTID =
        "SERVICEREQUESTCONTACTID";
    
    public static final String QUALIFIED_COLUMN_DFAPPRAISALREQUESTID =
        "REQUEST.REQUESTID";
    public static final String COLUMN_DFAPPRAISALREQUESTID =
        "REQUESTID";
       
    public static final String QUALIFIED_COLUMN_DFAPPRAISALSTATUSID =
        "REQUEST.REQUESTSTATUSID";
    public static final String COLUMN_DFAPPRAISALSTATUSID =
        "REQUESTSTATUSID";
    
    public static final String QUALIFIED_COLUMN_DFAPPRAISALPROVIDERID =
        "SERVICEPRODUCT.SERVICEPROVIDERID";
    public static final String COLUMN_DFAPPRAISALPROVIDERID =
        "SERVICEPROVIDERID";

    public static final String QUALIFIED_COLUMN_DFAPPRAISALPRODUCTID =
        "REQUEST.SERVICEPRODUCTID";
    public static final String COLUMN_DFAPPRAISALPRODUCTID =
        "SERVICEPRODUCTID";
    
    public static final String QUALIFIED_COLUMN_DFAPPRAISALREQUESTREFNUM =
        "SERVICEREQUEST.SERVICEPROVIDERREFNO";
    public static final String COLUMN_DFAPPRAISALREQUESTREFNUM =
        "SERVICEPROVIDERREFNO";    
    
    public static final String QUALIFIED_COLUMN_DFAPPRAISALREQUESTSTATUS =
        "REQUEST.STATUSID_STR";
    public static final String COLUMN_DFAPPRAISALREQUESTSTATUS =
        "STATUSID_STR";
 
    public static final String QUALIFIED_COLUMN_DFAPPRAISALREQUESTSTATUSMSG =
        "REQUEST.STATUSMESSAGE";
    public static final String COLUMN_DFAPPRAISALREQUESTSTATUSMSG =
        "STATUSMESSAGE";
    //stOCSRequestStatusMsg

    public static final String QUALIFIED_COLUMN_DFAPPRAISALREQUESTSTATUSDATE =
        "REQUEST.STATUSDATE";
    public static final String COLUMN_DFAPPRAISALREQUESTSTATUSDATE =
        "STATUSDATE";
    
    public static final String QUALIFIED_COLUMN_DFPROPERTYID =
        "SERVICEREQUEST.PROPERTYID";
    public static final String COLUMN_DFPROPERTYID =
        "PROPERTYID";  
    
    public static final String QUALIFIED_COLUMN_DFAPPRAISALSPECIALINST = 
        "SERVICEREQUEST.SPECIALINSTRUCTIONS";
    public static final String COLUMN_DFAPPRAISALSPECIALINST = 
        "SPECIALINSTRUCTIONS";

    public final static String QUALIFIED_COLUMN_DFAOCONTACTFIRSTNAME = "SERVICEREQUESTCONTACT.NAMEFIRST";
    public final static String COLUMN_DFAOCONTACTFIRSTNAME = "NAMEFIRST";

    public final static String QUALIFIED_COLUMN_DFAOCONTACTLASTNAME = "SERVICEREQUESTCONTACT.NAMELAST";
    public final static String COLUMN_DFAOCONTACTLASTNAME = "NAMELAST";
    
    public final static String QUALIFIED_COLUMN_DFAOEMAIL = "SERVICEREQUESTCONTACT.EMAILADDRESS";
    public final static String COLUMN_DFAOEMAIL = "EMAILADDRESS";

    public final static String QUALIFIED_COLUMN_DFAOWORKPHONEAREACODE = "SERVICEREQUESTCONTACT.WORKAREACODE";
    public final static String COLUMN_DFAOWORKPHONEAREACODE = "WORKAREACODE";
    
    public final static String QUALIFIED_COLUMN_DFAOWORKPHONEFIRSTTHREEDIGITS = 
        "SERVICEREQUESTCONTACT.WORKPHONEFIRSTTHREEDIGITS";
    public final static String COLUMN_DFAOWORKPHONEFIRSTTHREEDIGITS = "WORKPHONEFIRSTTHREEDIGITS";

    public final static String QUALIFIED_COLUMN_DFAOWORKPHONELASTFOURDIGITS = 
        "SERVICEREQUESTCONTACT.WORKPHONELASTFOURDIGITS";
    public final static String COLUMN_DFAOWORKPHONELASTFOURDIGITS = "WORKPHONELASTFOURDIGITS";
    
    public final static String QUALIFIED_COLUMN_DFAOWORKPHONEEXTENSION = 
        "SERVICEREQUESTCONTACT.WORKEXTENSION";
    public final static String COLUMN_DFAOWORKPHONEEXTENSION = "WORKEXTENSION";

    public final static String QUALIFIED_COLUMN_DFAOCELLPHONEAREACODE = 
        "SERVICEREQUESTCONTACT.CELLAREACODE";
    public final static String COLUMN_DFAOCELLPHONEAREACODE = "CELLAREACODE";
    
    public final static String QUALIFIED_COLUMN_DFAOCELLPHONEFIRSTTHREEDIGITS = 
        "SERVICEREQUESTCONTACT.CELLPHONEFIRSTTHREEDIGITS";
    public final static String COLUMN_DFAOCELLPHONEFIRSTTHREEDIGITS = "CELLPHONEFIRSTTHREEDIGITS";

    public final static String QUALIFIED_COLUMN_DFAOCELLPHONELASTFOURDIGITS = 
        "SERVICEREQUESTCONTACT.CELLPHONELASTFOURDIGITS";
    public final static String COLUMN_DFAOCELLPHONELASTFOURDIGITS = "CELLPHONELASTFOURDIGITS";

    public final static String QUALIFIED_COLUMN_DFAOHOMEPHONEAREACODE = 
        "SERVICEREQUESTCONTACT.HOMEAREACODE";
    public final static String COLUMN_DFAOHOMEPHONEAREACODE = "HOMEAREACODE";
    
    public final static String QUALIFIED_COLUMN_DFAOHOMEPHONEFIRSTTHREEDIGITS = 
        "SERVICEREQUESTCONTACT.HOMEPHONEFIRSTTHREEDIGITS";
    public final static String COLUMN_DFAOHOMEPHONEFIRSTTHREEDIGITS  = "HOMEPHONEFIRSTTHREEDIGITS";
    
    public final static String QUALIFIED_COLUMN_DFAOHOMEPHONELASTFOURDIGITS = 
        "SERVICEREQUESTCONTACT.HOMEPHONELASTFOURDIGITS";
    public final static String COLUMN_DFAOHOMEPHONELASTFOURDIGITS  = "HOMEPHONELASTFOURDIGITS";

    
    public final static String QUALIFIED_COLUMN_DFAOPROPERTYOWNERNAME = "SERVICEREQUESTCONTACT.PROPERTYOWNERNAME";
    public final static String COLUMN_DFAOPROPERTYOWNERNAME = "PROPERTYOWNERNAME";
    
    
    
    
    
    // build the relationship between the column definition and the bound field
    // name, which are defined in the interface.
    static {
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFAPPRAISALPROVIDERID,
                    COLUMN_DFAPPRAISALPROVIDERID,
                    QUALIFIED_COLUMN_DFAPPRAISALPROVIDERID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );

        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFAPPRAISALPRODUCTID,
                    COLUMN_DFAPPRAISALPRODUCTID,
                    QUALIFIED_COLUMN_DFAPPRAISALPRODUCTID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );

        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFAPPRAISALDEALID,
                    COLUMN_DFAPPRAISALDEALID,
                    QUALIFIED_COLUMN_DFAPPRAISALDEALID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFAPPRAISALCOPYID,
                    COLUMN_DFAPPRAISALCOPYID,
                    QUALIFIED_COLUMN_DFAPPRAISALCOPYID,
                    BigDecimal.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAPPRAISALBORROWERID,
                        COLUMN_DFAPPRAISALBORROWERID,
                        QUALIFIED_COLUMN_DFAPPRAISALBORROWERID,
                        BigDecimal.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
            );
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAPPRAISALCONTACTID,
                        COLUMN_DFAPPRAISALCONTACTID,
                        QUALIFIED_COLUMN_DFAPPRAISALCONTACTID,
                        BigDecimal.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
            );
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAPPRAISALREQUESTID,
                        COLUMN_DFAPPRAISALREQUESTID,
                        QUALIFIED_COLUMN_DFAPPRAISALREQUESTID,
                        BigDecimal.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
            );
            
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAPPRAISALSTATUSID,
                        COLUMN_DFAPPRAISALSTATUSID,
                        QUALIFIED_COLUMN_DFAPPRAISALSTATUSID,
                        BigDecimal.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFAPPRAISALREQUESTREFNUM,
                    COLUMN_DFAPPRAISALREQUESTREFNUM,
                    QUALIFIED_COLUMN_DFAPPRAISALREQUESTREFNUM,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAPPRAISALSPECIALINST,
                        COLUMN_DFAPPRAISALSPECIALINST,
                        QUALIFIED_COLUMN_DFAPPRAISALSPECIALINST,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFAPPRAISALREQUESTSTATUS,
                    COLUMN_DFAPPRAISALREQUESTSTATUS,
                    QUALIFIED_COLUMN_DFAPPRAISALREQUESTSTATUS,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFAPPRAISALREQUESTSTATUSMSG,
                    COLUMN_DFAPPRAISALREQUESTSTATUSMSG,
                    QUALIFIED_COLUMN_DFAPPRAISALREQUESTSTATUSMSG,
                    String.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                    FIELD_DFAPPRAISALREQUESTSTATUSDATE,
                    COLUMN_DFAPPRAISALREQUESTSTATUSDATE,
                    QUALIFIED_COLUMN_DFAPPRAISALREQUESTSTATUSDATE,
                    Timestamp.class,
                    false,
                    false,
                    QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                    "",
                    QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                    ""
                )
            );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFPROPERTYID,
                        COLUMN_DFPROPERTYID,
                        QUALIFIED_COLUMN_DFPROPERTYID,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOCONTACTFIRSTNAME,
                        COLUMN_DFAOCONTACTFIRSTNAME,
                        QUALIFIED_COLUMN_DFAOCONTACTFIRSTNAME,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOCONTACTLASTNAME,
                        COLUMN_DFAOCONTACTLASTNAME,
                        QUALIFIED_COLUMN_DFAOCONTACTLASTNAME,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );

            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOEMAIL,
                        COLUMN_DFAOEMAIL,
                        QUALIFIED_COLUMN_DFAOEMAIL,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOWORKPHONEAREACODE,
                        COLUMN_DFAOWORKPHONEAREACODE,
                        QUALIFIED_COLUMN_DFAOWORKPHONEAREACODE,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOWORKPHONEFIRSTTHREEDIGITS,
                        COLUMN_DFAOWORKPHONEFIRSTTHREEDIGITS,
                        QUALIFIED_COLUMN_DFAOWORKPHONEFIRSTTHREEDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );

            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOWORKPHONELASTFOURDIGITS,
                        COLUMN_DFAOWORKPHONELASTFOURDIGITS,
                        QUALIFIED_COLUMN_DFAOWORKPHONELASTFOURDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOWORKPHONENUMEXTENSION,
                        COLUMN_DFAOWORKPHONEEXTENSION,
                        QUALIFIED_COLUMN_DFAOWORKPHONEEXTENSION,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );


            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOHOMEPHONEAREACODE,
                        COLUMN_DFAOHOMEPHONEAREACODE,
                        QUALIFIED_COLUMN_DFAOHOMEPHONEAREACODE,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOHOMEPHONEFIRSTTHREEDIGITS,
                        COLUMN_DFAOHOMEPHONEFIRSTTHREEDIGITS,
                        QUALIFIED_COLUMN_DFAOHOMEPHONEFIRSTTHREEDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOHOMEPHONELASTFOURDIGITS,
                        COLUMN_DFAOHOMEPHONELASTFOURDIGITS,
                        QUALIFIED_COLUMN_DFAOHOMEPHONELASTFOURDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOCELLPHONEAREACODE,
                        COLUMN_DFAOCELLPHONEAREACODE,
                        QUALIFIED_COLUMN_DFAOCELLPHONEAREACODE,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOCELLPHONEFIRSTTHREEDIGITS,
                        COLUMN_DFAOCELLPHONEFIRSTTHREEDIGITS,
                        QUALIFIED_COLUMN_DFAOCELLPHONEFIRSTTHREEDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOCELLPHONELASTFOURDIGITS,
                        COLUMN_DFAOCELLPHONELASTFOURDIGITS,
                        QUALIFIED_COLUMN_DFAOCELLPHONELASTFOURDIGITS,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
            FIELD_SCHEMA.addFieldDescriptor(
                    new QueryFieldDescriptor(
                        FIELD_DFAOPROPERTYOWNERNAME,
                        COLUMN_DFAOPROPERTYOWNERNAME,
                        QUALIFIED_COLUMN_DFAOPROPERTYOWNERNAME,
                        String.class,
                        false,
                        false,
                        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                        "",
                        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                        ""
                    )
                );
               
            
            
    }
    
    
        

    /**
     * Constructor function
     */
    public doAppraisalProviderModelImpl() {

        super();
        setDataSourceName(DATA_SOURCE_NAME);
        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);        
        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
        setFieldSchema(FIELD_SCHEMA);
        initialize();
    }

    /**
     * initializing.  do nothing for now.
     */
    public void initialize() {}

    /**
     * before execute the SQL.
     */
    protected String beforeExecute(ModelExecutionContext context,
                                   int queryType, String sql)
        throws ModelControlException {
    	      

        if (_log.isDebugEnabled())
            _log.debug("SQL before Execute: " + sql);
        return sql;
    }
    
    

    /**
     * after execute, we need change some language sensitive value for the
     * result.
     */
    protected void afterExecute(ModelExecutionContext context,
                                int queryType)
        throws ModelControlException {

        // try to get language id from the session state model.
        String defaultInstanceStateName =
            getRequestContext().getModelManager().
            getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl sessionState = (SessionStateModelImpl)
            getRequestContext().getModelManager().
            getModel(SessionStateModel.class,
                     defaultInstanceStateName, true);
        int languageId = sessionState.getLanguageId();
		int institutionId = sessionState.getDealInstitutionId();

        // get the status from the BXResources and set to the field.
        _log.debug("Trying to get the status from BXResources");

//        BigDecimal statusId = getDfAppraisalStatusId();
//        
//        BigDecimal minusOne = new java.math.BigDecimal(String.valueOf(-1));
//        
//        BigDecimal requestId = getDfAppraisalRequestId();
//        
//        if (requestId==null) {
//            setDfAppraisalRequestId(minusOne);
//            setDfAppraisalStatusId(minusOne);
//        }
//        
//        BigDecimal contactId = getDfAppraisalContactId();
//         
//        if (contactId==null)
//            setDfAppraisalContactId(minusOne);
//        
//        BigDecimal borrowerId =  getDfAppraisalBorrowerId();
//
//        if(borrowerId==null)
//            setDfAppraisalBorrowerId(minusOne);
//
//
//        if(statusId==null) {
//            setDfAppraisalRequestStatus("");
//        }
        if(getSize()>0) {
            setDfAppraisalRequestStatus(
                    BXResources.
                        getPickListDescription(institutionId, "REQUESTSTATUS",
                                    getDfAppraisalRequestStatus(),
                                        languageId));
        }
    }

    
    
    
    /**
     * getter and setter implementation.
     */

    public String getDfAOPropertyOwnerName() {
        return (String) getValue(FIELD_DFAOPROPERTYOWNERNAME);
    }
    
    public void setDfAOPropertyOwnerName(String name) {
         setValue(FIELD_DFAOPROPERTYOWNERNAME, name);
    }
    
    public BigDecimal getDfAppraisalDealId() {
        return (BigDecimal) getValue(FIELD_DFAPPRAISALDEALID);
    }
    public void setDfAppraisalDealId(BigDecimal id) {
        setValue(FIELD_DFAPPRAISALDEALID, id);
    }

    public BigDecimal getDfAppraisalCopyId() {
        return (BigDecimal) getValue(FIELD_DFAPPRAISALCOPYID);
    }
    public void setDfAppraisalCopyId(BigDecimal id) {
        setValue(FIELD_DFAPPRAISALCOPYID, id);
    }

    public BigDecimal getDfAppraisalContactId() {
        return (BigDecimal) getValue(FIELD_DFAPPRAISALCONTACTID);
    }
    public void setDfAppraisalContactId(BigDecimal id) {
        setValue(FIELD_DFAPPRAISALCONTACTID, id);
    }
    
    
    public BigDecimal getDfAppraisalBorrowerId() {
        return (BigDecimal) getValue(FIELD_DFAPPRAISALBORROWERID);
    }
    public void setDfAppraisalBorrowerId(BigDecimal id) {
        setValue(FIELD_DFAPPRAISALBORROWERID, id);
    }
    
    public BigDecimal getDfAppraisalRequestId() {
        return (BigDecimal) getValue(FIELD_DFAPPRAISALREQUESTID);
    }
    public void setDfAppraisalRequestId(BigDecimal id) {
        setValue(FIELD_DFAPPRAISALREQUESTID, id);
    }

    
    public BigDecimal getDfAppraisalStatusId() {
        return (BigDecimal) getValue(FIELD_DFAPPRAISALSTATUSID);
    }
    public void setDfAppraisalStatusId(BigDecimal id) {
        setValue(FIELD_DFAPPRAISALSTATUSID, id);
    }
    
    public BigDecimal getDfAppraisalProviderId() {
        return (BigDecimal) getValue(FIELD_DFAPPRAISALPROVIDERID);
    }
    
    public void setDfAppraisalProviderId(BigDecimal id) {
        setValue(FIELD_DFAPPRAISALPROVIDERID, id);
    }

    public BigDecimal getDfAppraisalProductId() {
        return (BigDecimal) getValue(FIELD_DFAPPRAISALPRODUCTID);
    }
    public void setDfAppraisalProductId(BigDecimal id) {
        setValue(FIELD_DFAPPRAISALPRODUCTID, id);
    }
    
    
    public String getDfAppraisalSpecialInst() {
        return (String) getValue(FIELD_DFAPPRAISALSPECIALINST);
    }
    
    public void setDfAppraisalSpecialInst(String inst) {
        setValue(FIELD_DFAPPRAISALSPECIALINST, inst);

    }

    public String getDfAppraisalRequestRefNum() {
        return (String) getValue(FIELD_DFAPPRAISALREQUESTREFNUM);
    }
    public void setDfAppraisalRequestRefNum(String num) {
        setValue(FIELD_DFAPPRAISALREQUESTREFNUM, num);
    }
     
    public String getDfAppraisalRequestStatus() {
        return (String) getValue(FIELD_DFAPPRAISALREQUESTSTATUS);
    }
    
    public void setDfAppraisalRequestStatus(String status) {
        setValue(FIELD_DFAPPRAISALREQUESTSTATUS, status);
    }
    
    
    public String getDfAppraisalRequestStatusMsg() {
        return (String) getValue(FIELD_DFAPPRAISALREQUESTSTATUSMSG);
    }
    public void setDfAppraisalRequestStatusMsg(String msg) {
        setValue(FIELD_DFAPPRAISALREQUESTSTATUSMSG, msg);
    }

    public Timestamp getDfAppraisalRequestStatusDate() {
        return (Timestamp) getValue(FIELD_DFAPPRAISALREQUESTSTATUSDATE);
    }
    
    public void setDfAppraisalRequestStatusDate(Timestamp date) {
        setValue(FIELD_DFAPPRAISALREQUESTSTATUSDATE, date);
    }

    public String getDfAOContactFirstName() {
        return (String) getValue(FIELD_DFAOCONTACTFIRSTNAME);
    }
    
    public void setDfAOContactFristName(String name) {
        setValue(FIELD_DFAOCONTACTFIRSTNAME, name);
    }
    
    public String getDfAOContactLastName() {
        return (String) getValue(FIELD_DFAOCONTACTLASTNAME);
    }
    
    public void setDfAOContactLastName(String name) {
        setValue(FIELD_DFAOCONTACTLASTNAME, name);
    }
    
    public String getDfAOEmail() {
        return (String) getValue(FIELD_DFAOEMAIL);
    }

    public void setDfAOEmail(String email) {
        setValue(FIELD_DFAOEMAIL, email);
    }

    public String getDfAOWorkPhoneAreaCode() {
        return (String) getValue(FIELD_DFAOWORKPHONEAREACODE);
    }
    
    public void setDfAOWorkPhoneAreaCode(String areaCode) {
        setValue(FIELD_DFAOWORKPHONEAREACODE, areaCode);
    }
        
    public String getDfAOWorkPhoneFirstThreeDigits() {
        return (String) getValue(FIELD_DFAOWORKPHONEFIRSTTHREEDIGITS);
    }
    
    public void setDfAOWorkPhoneFirstThreeDigits(String phoneNumber) {
        setValue(FIELD_DFAOWORKPHONEFIRSTTHREEDIGITS, phoneNumber);
    }

    public String getDfAOWorkPhoneLastFourDigits() {
        return (String) getValue(FIELD_DFAOWORKPHONELASTFOURDIGITS);
    }
    
    public void setDfAOWorkPhoneLastFourDigits(String phoneNumber) {
        setValue(FIELD_DFAOWORKPHONELASTFOURDIGITS, phoneNumber);
    }


    public String getDfAOWorkPhoneNumExtension() {
        return (String) getValue(FIELD_DFAOWORKPHONENUMEXTENSION);
    }
    
    public void setDfAOWorkPhoneNumExtension(String extension) {
        setValue(FIELD_DFAOWORKPHONENUMEXTENSION, extension);
    }

    public String getDfAOCellPhoneAreaCode() {
        return (String) getValue(FIELD_DFAOCELLPHONEAREACODE);
    }
    
    public void setDfAOCellPhoneAreaCode(String areaCode) {
        setValue(FIELD_DFAOCELLPHONEAREACODE, areaCode);
    }
        
    public String getDfAOCellPhoneFirstThreeDigits() {
        return (String) getValue(FIELD_DFAOCELLPHONEFIRSTTHREEDIGITS);
    }
    
    public void setDfAOCellPhoneFirstThreeDigits(String phoneNumber) {
        setValue(FIELD_DFAOCELLPHONEFIRSTTHREEDIGITS, phoneNumber);
    }

    public String getDfAOCellPhoneLastFourDigits() {
        return (String) getValue(FIELD_DFAOCELLPHONELASTFOURDIGITS);
    }
    
    public void setDfAOCellPhoneLastFourDigits(String phoneNumber) {
        setValue(FIELD_DFAOCELLPHONELASTFOURDIGITS, phoneNumber);
    }

    
    public String getDfAOHomePhoneAreaCode() {
        return (String) getValue(FIELD_DFAOHOMEPHONEAREACODE);
    }
    
    public void setDfAOHomePhoneAreaCode(String areaCode) {
        setValue(FIELD_DFAOHOMEPHONEAREACODE, areaCode);
    }
        
    public String getDfAOHomePhoneFirstThreeDigits() {
        return (String) getValue(FIELD_DFAOHOMEPHONEFIRSTTHREEDIGITS);
    }
    
    public void setDfAOHomePhoneFirstThreeDigits(String phoneNumber) {
        setValue(FIELD_DFAOHOMEPHONEFIRSTTHREEDIGITS, phoneNumber);
    }

    public String getDfAOHomePhoneLastFourDigits() {
        return (String) getValue(FIELD_DFAOHOMEPHONELASTFOURDIGITS);
    }
    
    public void setDfAOHomePhoneLastFourDigits(String phoneNumber) {
        setValue(FIELD_DFAOHOMEPHONELASTFOURDIGITS, phoneNumber);
    }
    
}
