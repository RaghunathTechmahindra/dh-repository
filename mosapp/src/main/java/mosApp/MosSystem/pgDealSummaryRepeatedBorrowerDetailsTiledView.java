package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;

import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 * <p>
 * Title: pgDealSummaryRepeatedBorrowerDetailsTiledView
 * </p>
 *
 * <p>
 * Description: tile view bean for deal summary 
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 *
 * @version 1.1 Date: 07/27/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: Added 5 fields for CIF
 *
 */
public class pgDealSummaryRepeatedBorrowerDetailsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedBorrowerDetailsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewBorrowersModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStAPDtlsApplicantSalutation().setValue(CHILD_STAPDTLSAPPLICANTSALUTATION_RESET_VALUE);
		getStAPDtlsFirstName().setValue(CHILD_STAPDTLSFIRSTNAME_RESET_VALUE);
		getStAPDtlsMiddleInitial().setValue(CHILD_STAPDTLSMIDDLEINITIAL_RESET_VALUE);
		getStAPDtlsLastName().setValue(CHILD_STAPDTLSLASTNAME_RESET_VALUE);
		getStAPDtlsSuffix().setValue(CHILD_STAPDTLSSUFFIX_RESET_VALUE);
		getStAPDtlsApplicantType().setValue(CHILD_STAPDTLSAPPLICANTTYPE_RESET_VALUE);
		getStAPDtlsPrimaryBorrower().setValue(CHILD_STAPDTLSPRIMARYBORROWER_RESET_VALUE);
		getStAPDtlsDateOfBirth().setValue(CHILD_STAPDTLSDATEOFBIRTH_RESET_VALUE);
		getStAPDtlsMaritalStatus().setValue(CHILD_STAPDTLSMARITALSTATUS_RESET_VALUE);
		getStAPDtlsSINNo().setValue(CHILD_STAPDTLSSINNO_RESET_VALUE);
		getStAPDtlsCitizenship().setValue(CHILD_STAPDTLSCITIZENSHIP_RESET_VALUE);
		getStAPDtlsNoOfDependants().setValue(CHILD_STAPDTLSNOOFDEPENDANTS_RESET_VALUE);
		getStAPDtlsExistingClient().setValue(CHILD_STAPDTLSEXISTINGCLIENT_RESET_VALUE);
		getStAPDtlsReferenceCllientNo().setValue(CHILD_STAPDTLSREFERENCECLLIENTNO_RESET_VALUE);
		getStAPDtlsNoOfTimeBankrupt().setValue(CHILD_STAPDTLSNOOFTIMEBANKRUPT_RESET_VALUE);
		getStAPDtlsBankruptcyStatus().setValue(CHILD_STAPDTLSBANKRUPTCYSTATUS_RESET_VALUE);
		getStAPDtlsStaffOfLender().setValue(CHILD_STAPDTLSSTAFFOFLENDER_RESET_VALUE);
		getStAPDtlsHomePhone().setValue(CHILD_STAPDTLSHOMEPHONE_RESET_VALUE);
		getStAPDtlsWorkPhone().setValue(CHILD_STAPDTLSWORKPHONE_RESET_VALUE);
		getStAPDtlsCellPhone().setValue(CHILD_STAPDTLSCELLPHONE_RESET_VALUE);
		getStAPDtlsFaxNumber().setValue(CHILD_STAPDTLSFAXNUMBER_RESET_VALUE);
		getStAPDtlsEmailAddress().setValue(CHILD_STAPDTLSEMAILADDRESS_RESET_VALUE);
		getStAPDtlsWorkPhoneExt().setValue(CHILD_STAPDTLSWORKPHONEEXT_RESET_VALUE);
		getStAPDtlsLanguagePreference().setValue(CHILD_STAPDTLSLANGUAGEPREFERENCE_RESET_VALUE);
		getRepeatedApplicantAddress().resetChildren();

//		***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		getStGender().setValue(CHILD_STGENDER_RESET_VALUE);
		getStSolicitation().setValue(CHILD_STSOLICITATION_RESET_VALUE);
		getStSmoker().setValue(CHILD_STSMOKER_RESET_VALUE);
		getStEmployeeNumber().setValue(CHILD_STEMPLOYEENUMBER_RESET_VALUE);
		getStPreferredMethodOfContact().setValue(
				CHILD_STPREFERREDMETHODOFCONTACT_RESET_VALUE);


//		***** Change by NBC Impl. Team - Version 1.1 - End*****//

	}


	/**
	 * registerChildren
	 *
	 * @param String <br>
	 *   
	 *
	 * @return None : the result of registerChildren <br>
	 * @version 1.1 
	 * Date: 7/27/006 <br>
	 * Author: NBC/PP Implementation Team <br>
	 * Change:  <br>
	 *	Added 5 new fields for CIF fields<br>
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STAPDTLSAPPLICANTSALUTATION,StaticTextField.class);
		registerChild(CHILD_STAPDTLSFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STAPDTLSMIDDLEINITIAL,StaticTextField.class);
		registerChild(CHILD_STAPDTLSLASTNAME,StaticTextField.class);
		registerChild(CHILD_STAPDTLSSUFFIX,StaticTextField.class);
		registerChild(CHILD_STAPDTLSAPPLICANTTYPE,StaticTextField.class);
		registerChild(CHILD_STAPDTLSPRIMARYBORROWER,StaticTextField.class);
		registerChild(CHILD_STAPDTLSDATEOFBIRTH,StaticTextField.class);
		registerChild(CHILD_STAPDTLSMARITALSTATUS,StaticTextField.class);
		registerChild(CHILD_STAPDTLSSINNO,StaticTextField.class);
		registerChild(CHILD_STAPDTLSCITIZENSHIP,StaticTextField.class);
		registerChild(CHILD_STAPDTLSNOOFDEPENDANTS,StaticTextField.class);
		registerChild(CHILD_STAPDTLSEXISTINGCLIENT,StaticTextField.class);
		registerChild(CHILD_STAPDTLSREFERENCECLLIENTNO,StaticTextField.class);
		registerChild(CHILD_STAPDTLSNOOFTIMEBANKRUPT,StaticTextField.class);
		registerChild(CHILD_STAPDTLSBANKRUPTCYSTATUS,StaticTextField.class);
		registerChild(CHILD_STAPDTLSSTAFFOFLENDER,StaticTextField.class);
		registerChild(CHILD_STAPDTLSHOMEPHONE,StaticTextField.class);
		registerChild(CHILD_STAPDTLSWORKPHONE,StaticTextField.class);
		registerChild(CHILD_STAPDTLSCELLPHONE,StaticTextField.class);
		registerChild(CHILD_STAPDTLSFAXNUMBER,StaticTextField.class);
		registerChild(CHILD_STAPDTLSEMAILADDRESS,StaticTextField.class);
		registerChild(CHILD_STAPDTLSWORKPHONEEXT,StaticTextField.class);
		registerChild(CHILD_STAPDTLSLANGUAGEPREFERENCE,StaticTextField.class);
		registerChild(CHILD_REPEATEDAPPLICANTADDRESS,pgDealSummaryRepeatedApplicantAddressTiledView.class);

		registerChild(CHILD_REPEATED1,
				pgIdentificationTypeRepeated1TiledView.class);
		
		
//		***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		registerChild(CHILD_STGENDER, StaticTextField.class);
		registerChild(CHILD_STSOLICITATION, StaticTextField.class);
		registerChild(CHILD_STSMOKER, StaticTextField.class);
		registerChild(CHILD_STEMPLOYEENUMBER, StaticTextField.class);
		registerChild(CHILD_STPREFERREDMETHODOFCONTACT, StaticTextField.class);
//		***** Change by NBC Impl. Team - Version 1.1 - End*****//
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewBorrowersModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedBorrowerDetails_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedBorrowerDetails(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// The following code block was migrated from the RepeatedBorrowerDetails_onBeforeRowDisplayEvent method
		  DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());
      handler.setBorrowerAddresses(this.getTileIndex());
      handler.setBorrowerIndentifications(this.getTileIndex());
      handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * createChild
	 *
	 * @param String <br>
	 *   
	 *
	 * @return View : the result of createChild <br>
	 * @version 1.1 
	 * Date: 7/27/006 <br>
	 * Author: NBC/PP Implementation Team <br>
	 * Change:  <br>
	 *	Added 5 new fields for CIF fields<br>
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STAPDTLSAPPLICANTSALUTATION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSAPPLICANTSALUTATION,
				doDetailViewBorrowersModel.FIELD_DFSALUTATION,
				CHILD_STAPDTLSAPPLICANTSALUTATION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSFIRSTNAME,
				doDetailViewBorrowersModel.FIELD_DFFIRSTNAME,
				CHILD_STAPDTLSFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSMIDDLEINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSMIDDLEINITIAL,
				doDetailViewBorrowersModel.FIELD_DFMIDDLENAME,
				CHILD_STAPDTLSMIDDLEINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSLASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSLASTNAME,
				doDetailViewBorrowersModel.FIELD_DFLASTNAME,
				CHILD_STAPDTLSLASTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSSUFFIX))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSSUFFIX,
				doDetailViewBorrowersModel.FIELD_DFSUFFIX,
				CHILD_STAPDTLSSUFFIX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSAPPLICANTTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSAPPLICANTTYPE,
				doDetailViewBorrowersModel.FIELD_DFAPPLICANTTYPE,
				CHILD_STAPDTLSAPPLICANTTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSPRIMARYBORROWER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSPRIMARYBORROWER,
				doDetailViewBorrowersModel.FIELD_DFPRIMARYBORROWER,
				CHILD_STAPDTLSPRIMARYBORROWER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSDATEOFBIRTH))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSDATEOFBIRTH,
				doDetailViewBorrowersModel.FIELD_DFDATEOFBIRTH,
				CHILD_STAPDTLSDATEOFBIRTH_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSMARITALSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSMARITALSTATUS,
				doDetailViewBorrowersModel.FIELD_DFMARITALSTATUS,
				CHILD_STAPDTLSMARITALSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSSINNO))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSSINNO,
				doDetailViewBorrowersModel.FIELD_DFSINNO,
				CHILD_STAPDTLSSINNO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSCITIZENSHIP))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSCITIZENSHIP,
				doDetailViewBorrowersModel.FIELD_DFCITIZENSHIPSTATUS,
				CHILD_STAPDTLSCITIZENSHIP_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSNOOFDEPENDANTS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSNOOFDEPENDANTS,
				doDetailViewBorrowersModel.FIELD_DFNUMBEROFDEPENDANTS,
				CHILD_STAPDTLSNOOFDEPENDANTS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSEXISTINGCLIENT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSEXISTINGCLIENT,
				doDetailViewBorrowersModel.FIELD_DFEXISTINGCLEINT,
				CHILD_STAPDTLSEXISTINGCLIENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSREFERENCECLLIENTNO))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSREFERENCECLLIENTNO,
				doDetailViewBorrowersModel.FIELD_DFREFERENCECLIENTNO,
				CHILD_STAPDTLSREFERENCECLLIENTNO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSNOOFTIMEBANKRUPT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSNOOFTIMEBANKRUPT,
				doDetailViewBorrowersModel.FIELD_DFNOOFTIMESBANKRUPT,
				CHILD_STAPDTLSNOOFTIMEBANKRUPT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSBANKRUPTCYSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSBANKRUPTCYSTATUS,
				doDetailViewBorrowersModel.FIELD_DFBANKRUPTCYSTATUS,
				CHILD_STAPDTLSBANKRUPTCYSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSSTAFFOFLENDER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSSTAFFOFLENDER,
				doDetailViewBorrowersModel.FIELD_DFSTAFFOFLENDER,
				CHILD_STAPDTLSSTAFFOFLENDER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSHOMEPHONE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSHOMEPHONE,
				doDetailViewBorrowersModel.FIELD_DFHOMEPHONENO,
				CHILD_STAPDTLSHOMEPHONE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSWORKPHONE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSWORKPHONE,
				doDetailViewBorrowersModel.FIELD_DFWORKPHONENO,
				CHILD_STAPDTLSWORKPHONE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSCELLPHONE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSCELLPHONE,
				doDetailViewBorrowersModel.FIELD_DFCELLPHONENO,
				CHILD_STAPDTLSCELLPHONE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSFAXNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSFAXNUMBER,
				doDetailViewBorrowersModel.FIELD_DFFAXNUMBER,
				CHILD_STAPDTLSFAXNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSEMAILADDRESS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSEMAILADDRESS,
				doDetailViewBorrowersModel.FIELD_DFEMAILADDRESS,
				CHILD_STAPDTLSEMAILADDRESS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSWORKPHONEEXT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSWORKPHONEEXT,
				doDetailViewBorrowersModel.FIELD_DFBORROWERWORKPHONEEXT,
				CHILD_STAPDTLSWORKPHONEEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPDTLSLANGUAGEPREFERENCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STAPDTLSLANGUAGEPREFERENCE,
				doDetailViewBorrowersModel.FIELD_DFLANGUAGEPREFERENCE,
				CHILD_STAPDTLSLANGUAGEPREFERENCE_RESET_VALUE,
				null);
			return child;
		}
		else if (name.equals(CHILD_REPEATEDAPPLICANTADDRESS))
		{
			pgDealSummaryRepeatedApplicantAddressTiledView child = new pgDealSummaryRepeatedApplicantAddressTiledView(this,
				CHILD_REPEATEDAPPLICANTADDRESS);
			return child;
		}
//		***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		else if (name.equals(CHILD_STGENDER)) {
			StaticTextField child = new StaticTextField(this,
					getdoDetailViewBorrowersModel(), CHILD_STGENDER,
					doDetailViewBorrowersModel.FIELD_DFGENDER,
					CHILD_STGENDER_RESET_VALUE, null);

			return child;
		} else if (name.equals(CHILD_STSOLICITATION)) {
			StaticTextField child = new StaticTextField(this,
					getdoDetailViewBorrowersModel(), CHILD_STSOLICITATION,
					doDetailViewBorrowersModel.FIELD_DFSOLICITATION,
					CHILD_STSOLICITATION_RESET_VALUE, null);

			return child;
		} else if (name.equals(CHILD_STSMOKER)) {
			StaticTextField child = new StaticTextField(this,
					getdoDetailViewBorrowersModel(), CHILD_STSMOKER,
					doDetailViewBorrowersModel.FIELD_DFSMOKER,
					CHILD_STSMOKER_RESET_VALUE, null);

			return child;
		} else if (name.equals(CHILD_STEMPLOYEENUMBER)) {
			StaticTextField child = new StaticTextField(this,
					getdoDetailViewBorrowersModel(), CHILD_STEMPLOYEENUMBER,
					doDetailViewBorrowersModel.FIELD_DFEMPLOYEENUMBER,
					CHILD_STEMPLOYEENUMBER_RESET_VALUE, null);

			return child;
		}
		else if (name.equals(CHILD_REPEATED1)) {
			pgIdentificationTypeRepeated1TiledView child = new pgIdentificationTypeRepeated1TiledView(
					this, CHILD_REPEATED1);
			return child;
		}
		else if (name.equals(CHILD_STPREFERREDMETHODOFCONTACT)) {
			StaticTextField child = new StaticTextField(
					this,
					getdoDetailViewBorrowersModel(),
					CHILD_STPREFERREDMETHODOFCONTACT,
					doDetailViewBorrowersModel.FIELD_DFPREFERREDMETHODOFCONTACT,
					CHILD_STPREFERREDMETHODOFCONTACT_RESET_VALUE, null);

			return child;
		} 

//		***** Change by NBC Impl. Team - Version 1.1 - End*****//
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsApplicantSalutation()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSAPPLICANTSALUTATION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsFirstName()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsMiddleInitial()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSMIDDLEINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsLastName()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSLASTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsSuffix()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSSUFFIX);
	}
	
	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsApplicantType()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSAPPLICANTTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsPrimaryBorrower()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSPRIMARYBORROWER);
	}


	/**
	 *
	 *
	 */
	public String endStAPDtlsPrimaryBorrowerDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stAPDtlsPrimaryBorrower_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsDateOfBirth()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSDATEOFBIRTH);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsMaritalStatus()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSMARITALSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsSINNo()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSSINNO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsCitizenship()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSCITIZENSHIP);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsNoOfDependants()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSNOOFDEPENDANTS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsExistingClient()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSEXISTINGCLIENT);
	}


	/**
	 *
	 *
	 */
	public String endStAPDtlsExistingClientDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stAPDtlsExistingClient_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsReferenceCllientNo()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSREFERENCECLLIENTNO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsNoOfTimeBankrupt()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSNOOFTIMEBANKRUPT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsBankruptcyStatus()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSBANKRUPTCYSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsStaffOfLender()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSSTAFFOFLENDER);
	}


	/**
	 *
	 *
	 */
	public String endStAPDtlsStaffOfLenderDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stAPDtlsStaffOfLender_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsHomePhone()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSHOMEPHONE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsWorkPhone()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSWORKPHONE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsCellPhone()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSCELLPHONE);
	}
	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsFaxNumber()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsEmailAddress()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSEMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsWorkPhoneExt()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSWORKPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public String endStAPDtlsWorkPhoneExtDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stAPDtlsWorkPhoneExt_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setXPhoneExtInsideRepeated(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAPDtlsLanguagePreference()
	{
		return (StaticTextField)getChild(CHILD_STAPDTLSLANGUAGEPREFERENCE);
	}


	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedApplicantAddressTiledView getRepeatedApplicantAddress()
	{
		return (pgDealSummaryRepeatedApplicantAddressTiledView)getChild(CHILD_REPEATEDAPPLICANTADDRESS);
	}


	/**
	 *
	 *
	 */
	public doDetailViewBorrowersModel getdoDetailViewBorrowersModel()
	{
		if (doDetailViewBorrowers == null)
			doDetailViewBorrowers = (doDetailViewBorrowersModel) getModel(doDetailViewBorrowersModel.class);
		return doDetailViewBorrowers;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewBorrowersModel(doDetailViewBorrowersModel model)
	{
			doDetailViewBorrowers = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STAPDTLSAPPLICANTSALUTATION="stAPDtlsApplicantSalutation";
	public static final String CHILD_STAPDTLSAPPLICANTSALUTATION_RESET_VALUE="";
	public static final String CHILD_STAPDTLSFIRSTNAME="stAPDtlsFirstName";
	public static final String CHILD_STAPDTLSFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STAPDTLSMIDDLEINITIAL="stAPDtlsMiddleInitial";
	public static final String CHILD_STAPDTLSMIDDLEINITIAL_RESET_VALUE="";
	public static final String CHILD_STAPDTLSLASTNAME="stAPDtlsLastName";
	public static final String CHILD_STAPDTLSLASTNAME_RESET_VALUE="";
	public static final String CHILD_STAPDTLSSUFFIX="stAPDtlsSuffix";
	public static final String CHILD_STAPDTLSSUFFIX_RESET_VALUE="";
	public static final String CHILD_STAPDTLSAPPLICANTTYPE="stAPDtlsApplicantType";
	public static final String CHILD_STAPDTLSAPPLICANTTYPE_RESET_VALUE="";
	public static final String CHILD_STAPDTLSPRIMARYBORROWER="stAPDtlsPrimaryBorrower";
	public static final String CHILD_STAPDTLSPRIMARYBORROWER_RESET_VALUE="";
	public static final String CHILD_STAPDTLSDATEOFBIRTH="stAPDtlsDateOfBirth";
	public static final String CHILD_STAPDTLSDATEOFBIRTH_RESET_VALUE="";
	public static final String CHILD_STAPDTLSMARITALSTATUS="stAPDtlsMaritalStatus";
	public static final String CHILD_STAPDTLSMARITALSTATUS_RESET_VALUE="";
	public static final String CHILD_STAPDTLSSINNO="stAPDtlsSINNo";
	public static final String CHILD_STAPDTLSSINNO_RESET_VALUE="";
	public static final String CHILD_STAPDTLSCITIZENSHIP="stAPDtlsCitizenship";
	public static final String CHILD_STAPDTLSCITIZENSHIP_RESET_VALUE="";
	public static final String CHILD_STAPDTLSNOOFDEPENDANTS="stAPDtlsNoOfDependants";
	public static final String CHILD_STAPDTLSNOOFDEPENDANTS_RESET_VALUE="";
	public static final String CHILD_STAPDTLSEXISTINGCLIENT="stAPDtlsExistingClient";
	public static final String CHILD_STAPDTLSEXISTINGCLIENT_RESET_VALUE="";
	public static final String CHILD_STAPDTLSREFERENCECLLIENTNO="stAPDtlsReferenceCllientNo";
	public static final String CHILD_STAPDTLSREFERENCECLLIENTNO_RESET_VALUE="";
	public static final String CHILD_STAPDTLSNOOFTIMEBANKRUPT="stAPDtlsNoOfTimeBankrupt";
	public static final String CHILD_STAPDTLSNOOFTIMEBANKRUPT_RESET_VALUE="";
	public static final String CHILD_STAPDTLSBANKRUPTCYSTATUS="stAPDtlsBankruptcyStatus";
	public static final String CHILD_STAPDTLSBANKRUPTCYSTATUS_RESET_VALUE="";
	public static final String CHILD_STAPDTLSSTAFFOFLENDER="stAPDtlsStaffOfLender";
	public static final String CHILD_STAPDTLSSTAFFOFLENDER_RESET_VALUE="";
	public static final String CHILD_STAPDTLSHOMEPHONE="stAPDtlsHomePhone";
	public static final String CHILD_STAPDTLSHOMEPHONE_RESET_VALUE="";
	public static final String CHILD_STAPDTLSWORKPHONE="stAPDtlsWorkPhone";
	public static final String CHILD_STAPDTLSWORKPHONE_RESET_VALUE="";
	public static final String CHILD_STAPDTLSCELLPHONE="stAPDtlsCellPhone";
	public static final String CHILD_STAPDTLSCELLPHONE_RESET_VALUE="";
	public static final String CHILD_STAPDTLSFAXNUMBER="stAPDtlsFaxNumber";
	public static final String CHILD_STAPDTLSFAXNUMBER_RESET_VALUE="";
	public static final String CHILD_STAPDTLSEMAILADDRESS="stAPDtlsEmailAddress";
	public static final String CHILD_STAPDTLSEMAILADDRESS_RESET_VALUE="";
	public static final String CHILD_STAPDTLSWORKPHONEEXT="stAPDtlsWorkPhoneExt";
	public static final String CHILD_STAPDTLSWORKPHONEEXT_RESET_VALUE="";
	public static final String CHILD_STAPDTLSLANGUAGEPREFERENCE="stAPDtlsLanguagePreference";
	public static final String CHILD_STAPDTLSLANGUAGEPREFERENCE_RESET_VALUE="";
	public static final String CHILD_REPEATEDAPPLICANTADDRESS="RepeatedApplicantAddress";

   //	***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final String CHILD_STGENDER = "stGender";

	public static final String CHILD_STGENDER_RESET_VALUE = "";

	public static final String CHILD_STSOLICITATION = "stSolicitation";

	public static final String CHILD_STSOLICITATION_RESET_VALUE = "";

	public static final String CHILD_STSMOKER = "stSmoker";

	public static final String CHILD_STSMOKER_RESET_VALUE = "";

	public static final String CHILD_STEMPLOYEENUMBER = "stEmployeeNumber";

	public static final String CHILD_STEMPLOYEENUMBER_RESET_VALUE = "";

	public static final String CHILD_STPREFERREDMETHODOFCONTACT = "stPreferredMethodOfContact";

	public static final String CHILD_STPREFERREDMETHODOFCONTACT_RESET_VALUE = "";
	public static final String CHILD_REPEATED1 = "Repeated1";


//	***** Change by NBC Impl. Team - Version 1.1 - End*****//

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewBorrowersModel doDetailViewBorrowers=null;
  private DealSummaryHandler handler=new DealSummaryHandler();

// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	/**
	 * getStGender
	 *
	 * @param None <br>
	 *
	 * @return StaticTextField : the result of getStGender <br>
	 */
	
	public StaticTextField getStGender() {
		return (StaticTextField) getChild(CHILD_STGENDER);
	}
	/**
	 * getStSolicitation
	 *
	 * @param None <br>
	 *
	 * @return StaticTextField : the result of getStSolicitation <br>
	 */
	
	public StaticTextField getStSolicitation() {
		return (StaticTextField) getChild(CHILD_STSOLICITATION);
	}
	/**
	 * getStEmployeeNumber
	 *
	 * @param None <br>
	 *
	 * @return StaticTextField : the result of getStEmployeeNumber <br>
	 */
	
	public StaticTextField getStEmployeeNumber() {
		return (StaticTextField) getChild(CHILD_STEMPLOYEENUMBER);
	}
	/**
	 * getStSmoker
	 *
	 * @param None <br>
	 *
	 * @return StaticTextField : the result of getStSmoker <br>
	 */
	
	public StaticTextField getStSmoker() {
		return (StaticTextField) getChild(CHILD_STSMOKER);
	}
	/**
	 * getStPreferredMethodOfContact
	 *
	 * @param None <br>
	 *
	 * @return StaticTextField : the result of getStPreferredMethodOfContact <br>
	 */
	public StaticTextField getStPreferredMethodOfContact() {
		return (StaticTextField) getChild(CHILD_STPREFERREDMETHODOFCONTACT);
	}
	// ***** Change by NBC Impl. Team - Version 1.1 - End*****//
}
