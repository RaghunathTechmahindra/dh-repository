package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doBorrowerEntryIncomeUpdateModel extends QueryModel, UpdateQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomePeriodId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomePeriodId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludingGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludingGDS(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncludingTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncludingTDS(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeAmount(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFINCOMEID="dfIncomeId";
	public static final String FIELD_DFINCOMETYPEID="dfIncomeTypeId";
	public static final String FIELD_DFINCOMEPERIODID="dfIncomePeriodId";
	public static final String FIELD_DFINCLUDINGGDS="dfIncludingGDS";
	public static final String FIELD_DFINCLUDINGTDS="dfIncludingTDS";
	public static final String FIELD_DFINCOMEAMOUNT="dfIncomeAmount";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

