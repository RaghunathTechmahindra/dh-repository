package mosApp.MosSystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ViewBean;

import MosSystem.Sc;



/**
 *
 *
 */
public class SentinelHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	protected static final String goldCopy="1";


	/**
	 *
	 *
	 */
	public SentinelHandler cloneSS()
	{
		return(SentinelHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();

		populateTaskNavigator(pg);

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();

	}

	/**
	 *
	 *
	 */
	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);

		try
		{
			setupDealSummarySnapShotDO(pg);
			ViewBean currNDPage = getCurrNDPage();
			Integer taskWqId = new Integer(pg.getPageCriteriaId1());


			//AssignedTaskWorkQueueId
			doAssignedTaskWorkqueueModel taskDo =(doAssignedTaskWorkqueueModel)
                                                  RequestManager.getRequestContext().getModelManager().getModel(doAssignedTaskWorkqueueModel.class);
			taskDo.clearUserWhereCriteria();
			taskDo.addUserWhereCriterion("dfAssignedTaskWorkqueueId", "=", taskWqId);

			////Object underwriterId = null;
			////taskDo.execute();
			////underwriterId = taskDo.getLastResults().getResultTable().getValue(0, 7);

      String sUnderwriterId = "";
      try
      {
       ResultSet rs = taskDo.executeSelect(null);

        ////((QueryModelBase)theModel).setResultSet(rs);
        if (rs != null && taskDo.getSize() > 0)
        {
          Object oUnderwriterId = taskDo.getValue(taskDo.FIELD_DFUNDERWRITERID);
          if (oUnderwriterId == null)
          {
            sUnderwriterId = "";
          }
          else if((oUnderwriterId != null))
          {
            sUnderwriterId = oUnderwriterId.toString();
            logger.debug("SH@setupBeforePageGeneration::FirmNameFromModel: " + oUnderwriterId.toString());
          }
        }
      }
      catch (ModelControlException mce)
      {
        logger.debug("SH@setupBeforePageGeneration::MCEException: " + mce);
      }
      catch (SQLException sqle)
      {
        logger.debug("SH@setupBeforePageGeneration::SQLException: " + sqle);
      }

      Integer underwriterId = new Integer(sUnderwriterId);
      logger.debug("SH@setupBeforePageGeneration::UnderwriterId: " + underwriterId);

			//dfUnderwriter
			// set underwriter page field
			doDealUnderwriterModel underwriterDo =(doDealUnderwriterModel)
                                              RequestManager.getRequestContext().getModelManager().getModel(doDealUnderwriterModel.class);
			underwriterDo.clearUserWhereCriteria();
			underwriterDo.addUserWhereCriterion("dfUnderwriterId", "=", underwriterId);

			// get deal administrator
			//self-join
      String administrator = "";

      //// Wrong iMT auto-convertion.
			////String sqlStr = "SELECT C.CONTACTLASTNAME || ' ' || SUBSTR(C.CONTACTFIRSTNAME,0,1 )" + " FROM USERPROFILE U, USERPROFILE A,CONTACT C WHERE" + " U.PARTNERUSERID=A.USERPROFILEID AND A.CONTACTID=C.CONTACTID AND" + " C.COPYID=" + goldCopy + " AND U.USERPROFILEID=" +TypeConverter.asInt(underwriterId);
			try
			{
				////Obsolete ND fragment should be replaced with the JATO idiom. In this
        //// it is better just to use the BX framework Executor class for iteration.
        ////CSpDBResult result = CSpDataObject.executeImmediate("orcl", sqlStr);
				////Object administrator = result.getResultTable().getValue(0, 0);

        String theSQL = "SELECT C.CONTACTLASTNAME || ' ' || SUBSTR(C.CONTACTFIRSTNAME,0,1 )" +
                        " FROM USERPROFILE U, USERPROFILE A, CONTACT C WHERE" +
                        " U.PARTNERUSERID=A.USERPROFILEID AND A.CONTACTID=C.CONTACTID AND" +
                        " C.COPYID=" + goldCopy + " AND U.USERPROFILEID=" +
                        underwriterId;

        JdbcExecutor jExec = srk.getJdbcExecutor();
        int key = jExec.execute(theSQL);

        while (jExec.next(key))
        {
          administrator = jExec.getString(key, 1);
        }
        jExec.closeData(key);
      }
      catch(Exception e)
      {
        setStandardFailMessage();
        logger.error("Exception @SentinelHandler.setupBeforePageGeneration().UnderwriterId determination: " +
                      "Problem encountered in getting usertypeid.");

        logger.error(e.getMessage());
      }

      logger.debug("SH@setupBeforePageGeneration::Administator: " + administrator);
      getCurrNDPage().setDisplayFieldValue("stDealAdmin", administrator);
    }
		catch(Exception ex)
		{
			logger.error("Exception @SentinelHandler.setupBeforePageGeneration()");
			logger.error(ex);
			setStandardFailMessage();
		}

	}


	/**
	 *
	 *
	 */
	public void handleSubmit()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			// complete the task - make it go away
			srk.beginTransaction();
			AssignedTask aTask = new AssignedTask(srk);
			aTask.findByPrimaryKey(new AssignedTaskBeanPK(pg.getPageCriteriaId1()));
			aTask.setTaskStatusId(Sc.TASK_COMPLETE);
			aTask.ejbStore();
			srk.commitTransaction();
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			logger.error("Exception @SentinelHandler.handleSubmit()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
  }
}

