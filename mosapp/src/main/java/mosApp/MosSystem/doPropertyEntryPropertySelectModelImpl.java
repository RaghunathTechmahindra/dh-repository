package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doPropertyEntryPropertySelectModelImpl extends QueryModelBase
	implements doPropertyEntryPropertySelectModel
{
	/**
	 *
	 *
	 */
	public doPropertyEntryPropertySelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetNumber()
	{
		return (String)getValue(FIELD_DFSTREETNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetNumber(String value)
	{
		setValue(FIELD_DFSTREETNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfStreetName()
	{
		return (String)getValue(FIELD_DFSTREETNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetName(String value)
	{
		setValue(FIELD_DFSTREETNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfStreetTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSTREETTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfStreetTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSTREETTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyStreetDirectionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYSTREETDIRECTIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyStreetDirectionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYSTREETDIRECTIONID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUnitNumber()
	{
		return (String)getValue(FIELD_DFUNITNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfUnitNumber(String value)
	{
		setValue(FIELD_DFUNITNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine2()
	{
		return (String)getValue(FIELD_DFADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine2(String value)
	{
		setValue(FIELD_DFADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyProvinceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYPROVINCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyProvinceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYPROVINCEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalFSA()
	{
		return (String)getValue(FIELD_DFPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalFSA(String value)
	{
		setValue(FIELD_DFPOSTALFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalLDU()
	{
		return (String)getValue(FIELD_DFPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalLDU(String value)
	{
		setValue(FIELD_DFPOSTALLDU,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLegalLine1()
	{
		return (String)getValue(FIELD_DFLEGALLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfLegalLine1(String value)
	{
		setValue(FIELD_DFLEGALLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLegalLine2()
	{
		return (String)getValue(FIELD_DFLEGALLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfLegalLine2(String value)
	{
		setValue(FIELD_DFLEGALLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLegalLine3()
	{
		return (String)getValue(FIELD_DFLEGALLINE3);
	}


	/**
	 *
	 *
	 */
	public void setDfLegalLine3(String value)
	{
		setValue(FIELD_DFLEGALLINE3,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyDwellingTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYDWELLINGTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyDwellingTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYDWELLINGTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDwellilngStyleId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDWELLILNGSTYLEID);
	}


	/**
	 *
	 *
	 */
	public void setDfDwellilngStyleId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDWELLILNGSTYLEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfStructureAge()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSTRUCTUREAGE);
	}


	/**
	 *
	 *
	 */
	public void setDfStructureAge(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSTRUCTUREAGE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNoOfUnits()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNOOFUNITS);
	}


	/**
	 *
	 *
	 */
	public void setDfNoOfUnits(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNOOFUNITS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNumberOfBedRooms()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNUMBEROFBEDROOMS);
	}


	/**
	 *
	 *
	 */
	public void setDfNumberOfBedRooms(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNUMBEROFBEDROOMS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLivingSpace()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIVINGSPACE);
	}


	/**
	 *
	 *
	 */
	public void setDfLivingSpace(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIVINGSPACE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyLivingSpaceMeasureId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYLIVINGSPACEMEASUREID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyLivingSpaceMeasureId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYLIVINGSPACEMEASUREID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLotSize()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLOTSIZE);
	}


	/**
	 *
	 *
	 */
	public void setDfLotSize(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLOTSIZE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLotsizeDepth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLOTSIZEDEPTH);
	}


	/**
	 *
	 *
	 */
	public void setDfLotsizeDepth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLOTSIZEDEPTH,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLotsizeFrontPage()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLOTSIZEFRONTPAGE);
	}


	/**
	 *
	 *
	 */
	public void setDfLotsizeFrontPage(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLOTSIZEFRONTPAGE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLotSizeUnitmeasuredId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLOTSIZEUNITMEASUREDID);
	}


	/**
	 *
	 *
	 */
	public void setDfLotSizeUnitmeasuredId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLOTSIZEUNITMEASUREDID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyHeatTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYHEATTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyHeatTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYHEATTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUFFIInsulation()
	{
		return (String)getValue(FIELD_DFUFFIINSULATION);
	}


	/**
	 *
	 *
	 */
	public void setDfUFFIInsulation(String value)
	{
		setValue(FIELD_DFUFFIINSULATION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyWaterTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYWATERTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyWaterTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYWATERTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertySwegeTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYSWEGETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertySwegeTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYSWEGETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyUsageId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYUSAGEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyUsageId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYUSAGEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyOccupancyTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYOCCUPANCYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyOccupancyTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYOCCUPANCYTYPEID,value);
	}

//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequestAppraisalId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREQUESTAPPRAISALID);
	}


	/**
	 *
	 *
	 */
	public void setDfRequestAppraisalId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREQUESTAPPRAISALID,value);
	}
//	***** Change by NBC Impl. Team - Version 1.2 - End *****//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPropertyLocationId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYLOCATIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfPropertyLocationId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROPERTYLOCATIONID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfZoning()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFZONING);
	}


	/**
	 *
	 *
	 */
	public void setDfZoning(java.math.BigDecimal value)
	{
		setValue(FIELD_DFZONING,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLandValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfLandValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANDVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEquityAvailable()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEQUITYAVAILABLE);
	}


	/**
	 *
	 *
	 */
	public void setDfEquityAvailable(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEQUITYAVAILABLE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEstimateValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFESTIMATEVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfEstimateValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFESTIMATEVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLTV()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLTV);
	}


	/**
	 *
	 *
	 */
	public void setDfLTV(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLTV,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPropertyExp()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPROPERTYEXP);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPropertyExp(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPROPERTYEXP,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMLSListingFlag()
	{
		return (String)getValue(FIELD_DFMLSLISTINGFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfMLSListingFlag(String value)
	{
		setValue(FIELD_DFMLSLISTINGFLAG,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGarageSizeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGARAGESIZEID);
	}


	/**
	 *
	 *
	 */
	public void setDfGarageSizeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGARAGESIZEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGarageTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGARAGETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfGarageTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGARAGETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBuilderName()
	{
		return (String)getValue(FIELD_DFBUILDERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBuilderName(String value)
	{
		setValue(FIELD_DFBUILDERNAME,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNewConstructionId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNEWCONSTRUCTIONID);
	}


	/**
	 *
	 *
	 */
	public void setDfNewConstructionId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNEWCONSTRUCTIONID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE
        ="SELECT distinct PROPERTY.DEALID, PROPERTY.PROPERTYID, PROPERTY.COPYID, " +
                "PROPERTY.PROPERTYSTREETNUMBER, PROPERTY.PROPERTYSTREETNAME, " +
                "PROPERTY.STREETTYPEID, PROPERTY.STREETDIRECTIONID, " +
                "PROPERTY.UNITNUMBER, PROPERTY.PROPERTYADDRESSLINE2, " +
                "PROPERTY.PROPERTYCITY, PROPERTY.PROVINCEID, PROPERTY.PROPERTYPOSTALFSA, " +
                "PROPERTY.PROPERTYPOSTALLDU, PROPERTY.LEGALLINE1, PROPERTY.LEGALLINE2, " +
                "PROPERTY.LEGALLINE3, PROPERTY.DWELLINGTYPEID, PROPERTY.DWELLINGSTYLEID, " +
                "PROPERTY.STRUCTUREAGE, PROPERTY.NUMBEROFUNITS, PROPERTY.NUMBEROFBEDROOMS, " +
                "PROPERTY.LIVINGSPACE, PROPERTY.LIVINGSPACEUNITOFMEASUREID, " +
                "PROPERTY.LOTSIZE, PROPERTY.LOTSIZEDEPTH, PROPERTY.LOTSIZEFRONTAGE, " +
                "PROPERTY.LOTSIZEUNITOFMEASUREID, PROPERTY.HEATTYPEID, " +
                "PROPERTY.INSULATEDWITHUFFI, PROPERTY.WATERTYPEID, " +
                "PROPERTY.SEWAGETYPEID, PROPERTY.PROPERTYUSAGEID, " +
                "PROPERTY.OCCUPANCYTYPEID, PROPERTY.PROPERTYTYPEID, " +
                "PROPERTY.PROPERTYLOCATIONID, PROPERTY.ZONING, PROPERTY.PURCHASEPRICE, " +
                "PROPERTY.LANDVALUE, PROPERTY.EQUITYAVAILABLE, " +
                "PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.LOANTOVALUE, " +
                "PROPERTY.TOTALPROPERTYEXPENSES, PROPERTY.MLSLISTINGFLAG, " +
                "PROPERTY.GARAGESIZEID, PROPERTY.GARAGETYPEID, PROPERTY.BUILDERNAME, " +
                "PROPERTY.NEWCONSTRUCTIONID, PROPERTY.REQUESTAPPRAISALID, PROPERTY.TENURETYPEID, " +
                "PROPERTY.MIENERGYEFFICIENCY, PROPERTY.ONRESERVETRUSTAGREEMENTNUMBER, " +
                "PROPERTY.SUBDIVISIONDISCOUNT, DEAL.MORTGAGEINSURERID FROM PROPERTY, " +
                "DEAL  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTY";
	public static final String STATIC_WHERE_CRITERIA=" (DEAL.DEALID=PROPERTY.DEALID " +
			"AND DEAL.COPYID=PROPERTY.COPYID " +
			"AND DEAL.INSTITUTIONPROFILEID = PROPERTY.INSTITUTIONPROFILEID ) ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="PROPERTY.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTY.PROPERTYID";
	public static final String COLUMN_DFPROPERTYID="PROPERTYID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFSTREETNUMBER="PROPERTY.PROPERTYSTREETNUMBER";
	public static final String COLUMN_DFSTREETNUMBER="PROPERTYSTREETNUMBER";
	public static final String QUALIFIED_COLUMN_DFSTREETNAME="PROPERTY.PROPERTYSTREETNAME";
	public static final String COLUMN_DFSTREETNAME="PROPERTYSTREETNAME";
	public static final String QUALIFIED_COLUMN_DFSTREETTYPEID="PROPERTY.STREETTYPEID";
	public static final String COLUMN_DFSTREETTYPEID="STREETTYPEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSTREETDIRECTIONID="PROPERTY.STREETDIRECTIONID";
	public static final String COLUMN_DFPROPERTYSTREETDIRECTIONID="STREETDIRECTIONID";
	public static final String QUALIFIED_COLUMN_DFUNITNUMBER="PROPERTY.UNITNUMBER";
	public static final String COLUMN_DFUNITNUMBER="UNITNUMBER";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE2="PROPERTY.PROPERTYADDRESSLINE2";
	public static final String COLUMN_DFADDRESSLINE2="PROPERTYADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFCITY="PROPERTY.PROPERTYCITY";
	public static final String COLUMN_DFCITY="PROPERTYCITY";
	public static final String QUALIFIED_COLUMN_DFPROPERTYPROVINCEID="PROPERTY.PROVINCEID";
	public static final String COLUMN_DFPROPERTYPROVINCEID="PROVINCEID";
	public static final String QUALIFIED_COLUMN_DFPOSTALFSA="PROPERTY.PROPERTYPOSTALFSA";
	public static final String COLUMN_DFPOSTALFSA="PROPERTYPOSTALFSA";
	public static final String QUALIFIED_COLUMN_DFPOSTALLDU="PROPERTY.PROPERTYPOSTALLDU";
	public static final String COLUMN_DFPOSTALLDU="PROPERTYPOSTALLDU";
	public static final String QUALIFIED_COLUMN_DFLEGALLINE1="PROPERTY.LEGALLINE1";
	public static final String COLUMN_DFLEGALLINE1="LEGALLINE1";
	public static final String QUALIFIED_COLUMN_DFLEGALLINE2="PROPERTY.LEGALLINE2";
	public static final String COLUMN_DFLEGALLINE2="LEGALLINE2";
	public static final String QUALIFIED_COLUMN_DFLEGALLINE3="PROPERTY.LEGALLINE3";
	public static final String COLUMN_DFLEGALLINE3="LEGALLINE3";
	public static final String QUALIFIED_COLUMN_DFPROPERTYDWELLINGTYPEID="PROPERTY.DWELLINGTYPEID";
	public static final String COLUMN_DFPROPERTYDWELLINGTYPEID="DWELLINGTYPEID";
	public static final String QUALIFIED_COLUMN_DFDWELLILNGSTYLEID="PROPERTY.DWELLINGSTYLEID";
	public static final String COLUMN_DFDWELLILNGSTYLEID="DWELLINGSTYLEID";
	public static final String QUALIFIED_COLUMN_DFSTRUCTUREAGE="PROPERTY.STRUCTUREAGE";
	public static final String COLUMN_DFSTRUCTUREAGE="STRUCTUREAGE";
	public static final String QUALIFIED_COLUMN_DFNOOFUNITS="PROPERTY.NUMBEROFUNITS";
	public static final String COLUMN_DFNOOFUNITS="NUMBEROFUNITS";
	public static final String QUALIFIED_COLUMN_DFNUMBEROFBEDROOMS="PROPERTY.NUMBEROFBEDROOMS";
	public static final String COLUMN_DFNUMBEROFBEDROOMS="NUMBEROFBEDROOMS";
	public static final String QUALIFIED_COLUMN_DFLIVINGSPACE="PROPERTY.LIVINGSPACE";
	public static final String COLUMN_DFLIVINGSPACE="LIVINGSPACE";
	public static final String QUALIFIED_COLUMN_DFPROPERTYLIVINGSPACEMEASUREID="PROPERTY.LIVINGSPACEUNITOFMEASUREID";
	public static final String COLUMN_DFPROPERTYLIVINGSPACEMEASUREID="LIVINGSPACEUNITOFMEASUREID";
	public static final String QUALIFIED_COLUMN_DFLOTSIZE="PROPERTY.LOTSIZE";
	public static final String COLUMN_DFLOTSIZE="LOTSIZE";
	public static final String QUALIFIED_COLUMN_DFLOTSIZEDEPTH="PROPERTY.LOTSIZEDEPTH";
	public static final String COLUMN_DFLOTSIZEDEPTH="LOTSIZEDEPTH";
	public static final String QUALIFIED_COLUMN_DFLOTSIZEFRONTPAGE="PROPERTY.LOTSIZEFRONTAGE";
	public static final String COLUMN_DFLOTSIZEFRONTPAGE="LOTSIZEFRONTAGE";
	public static final String QUALIFIED_COLUMN_DFLOTSIZEUNITMEASUREDID="PROPERTY.LOTSIZEUNITOFMEASUREID";
	public static final String COLUMN_DFLOTSIZEUNITMEASUREDID="LOTSIZEUNITOFMEASUREID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYHEATTYPEID="PROPERTY.HEATTYPEID";
	public static final String COLUMN_DFPROPERTYHEATTYPEID="HEATTYPEID";
	public static final String QUALIFIED_COLUMN_DFUFFIINSULATION="PROPERTY.INSULATEDWITHUFFI";
	public static final String COLUMN_DFUFFIINSULATION="INSULATEDWITHUFFI";
	public static final String QUALIFIED_COLUMN_DFPROPERTYWATERTYPEID="PROPERTY.WATERTYPEID";
	public static final String COLUMN_DFPROPERTYWATERTYPEID="WATERTYPEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYSWEGETYPEID="PROPERTY.SEWAGETYPEID";
	public static final String COLUMN_DFPROPERTYSWEGETYPEID="SEWAGETYPEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYUSAGEID="PROPERTY.PROPERTYUSAGEID";
	public static final String COLUMN_DFPROPERTYUSAGEID="PROPERTYUSAGEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYOCCUPANCYTYPEID="PROPERTY.OCCUPANCYTYPEID";
	public static final String COLUMN_DFPROPERTYOCCUPANCYTYPEID="OCCUPANCYTYPEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYTYPEID="PROPERTY.PROPERTYTYPEID";
	public static final String COLUMN_DFPROPERTYTYPEID="PROPERTYTYPEID";
	public static final String QUALIFIED_COLUMN_DFPROPERTYLOCATIONID="PROPERTY.PROPERTYLOCATIONID";
	public static final String COLUMN_DFPROPERTYLOCATIONID="PROPERTYLOCATIONID";
	public static final String QUALIFIED_COLUMN_DFZONING="PROPERTY.ZONING";
	public static final String COLUMN_DFZONING="ZONING";
	public static final String QUALIFIED_COLUMN_DFPURCHASEPRICE="PROPERTY.PURCHASEPRICE";
	public static final String COLUMN_DFPURCHASEPRICE="PURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFLANDVALUE="PROPERTY.LANDVALUE";
	public static final String COLUMN_DFLANDVALUE="LANDVALUE";
	public static final String QUALIFIED_COLUMN_DFEQUITYAVAILABLE="PROPERTY.EQUITYAVAILABLE";
	public static final String COLUMN_DFEQUITYAVAILABLE="EQUITYAVAILABLE";
	public static final String QUALIFIED_COLUMN_DFESTIMATEVALUE="PROPERTY.ESTIMATEDAPPRAISALVALUE";
	public static final String COLUMN_DFESTIMATEVALUE="ESTIMATEDAPPRAISALVALUE";
	public static final String QUALIFIED_COLUMN_DFLTV="PROPERTY.LOANTOVALUE";
	public static final String COLUMN_DFLTV="LOANTOVALUE";
	public static final String QUALIFIED_COLUMN_DFTOTALPROPERTYEXP="PROPERTY.TOTALPROPERTYEXPENSES";
	public static final String COLUMN_DFTOTALPROPERTYEXP="TOTALPROPERTYEXPENSES";
	public static final String QUALIFIED_COLUMN_DFMLSLISTINGFLAG="PROPERTY.MLSLISTINGFLAG";
	public static final String COLUMN_DFMLSLISTINGFLAG="MLSLISTINGFLAG";
	public static final String QUALIFIED_COLUMN_DFGARAGESIZEID="PROPERTY.GARAGESIZEID";
	public static final String COLUMN_DFGARAGESIZEID="GARAGESIZEID";
	public static final String QUALIFIED_COLUMN_DFGARAGETYPEID="PROPERTY.GARAGETYPEID";
	public static final String COLUMN_DFGARAGETYPEID="GARAGETYPEID";
	public static final String QUALIFIED_COLUMN_DFBUILDERNAME="PROPERTY.BUILDERNAME";
	public static final String COLUMN_DFBUILDERNAME="BUILDERNAME";
//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	public static final String QUALIFIED_COLUMN_DFREQUESTAPPRAISALID="PROPERTY.REQUESTAPPRAISALID";
	public static final String COLUMN_DFREQUESTAPPRAISALID="REQUESTAPPRAISALID";
//	***** Change by NBC Impl. Team - Version 1.2 - End *****//
	public static final String QUALIFIED_COLUMN_DFNEWCONSTRUCTIONID="PROPERTY.NEWCONSTRUCTIONID";
	public static final String COLUMN_DFNEWCONSTRUCTIONID="NEWCONSTRUCTIONID";
	
	//MI additiona
	public static final String QUALIFIED_COLUMN_DFSUBDIVISIONDISCOUNT="PROPERTY.SUBDIVISIONDISCOUNT";
	public static final String COLUMN_DFSUBDIVISIONDISCOUNT="SUBDIVISIONDISCOUNT";
	public static final String QUALIFIED_COLUMN_DFTENURETYPEID="PROPERTY.TENURETYPEID";
	public static final String COLUMN_DFTENURETYPEID="TENURETYPEID";
	public static final String QUALIFIED_COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER = "PROPERTY.ONRESERVETRUSTAGREEMENTNUMBER";
    public static final String COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER = "ONRESERVETRUSTAGREEMENTNUMBER";
    public static final String QUALIFIED_COLUMN_DFMIENERGYEFFICIENCY = "PROPERTY.MIENERGYEFFICIENCY";
    public static final String COLUMN_DFMIENERGYEFFICIENCY = "MIENERGYEFFICIENCY";
    public static final String QUALIFIED_COLUMN_DFMORTGAGEINSURERID = "DEAL.MORTGAGEINSURERID";
    public static final String COLUMN_DFMORTGAGEINSURERID = "MORTGAGEINSURERID";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYID,
				COLUMN_DFPROPERTYID,
				QUALIFIED_COLUMN_DFPROPERTYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETNUMBER,
				COLUMN_DFSTREETNUMBER,
				QUALIFIED_COLUMN_DFSTREETNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETNAME,
				COLUMN_DFSTREETNAME,
				QUALIFIED_COLUMN_DFSTREETNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTREETTYPEID,
				COLUMN_DFSTREETTYPEID,
				QUALIFIED_COLUMN_DFSTREETTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSTREETDIRECTIONID,
				COLUMN_DFPROPERTYSTREETDIRECTIONID,
				QUALIFIED_COLUMN_DFPROPERTYSTREETDIRECTIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUNITNUMBER,
				COLUMN_DFUNITNUMBER,
				QUALIFIED_COLUMN_DFUNITNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE2,
				COLUMN_DFADDRESSLINE2,
				QUALIFIED_COLUMN_DFADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYPROVINCEID,
				COLUMN_DFPROPERTYPROVINCEID,
				QUALIFIED_COLUMN_DFPROPERTYPROVINCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALFSA,
				COLUMN_DFPOSTALFSA,
				QUALIFIED_COLUMN_DFPOSTALFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALLDU,
				COLUMN_DFPOSTALLDU,
				QUALIFIED_COLUMN_DFPOSTALLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLEGALLINE1,
				COLUMN_DFLEGALLINE1,
				QUALIFIED_COLUMN_DFLEGALLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLEGALLINE2,
				COLUMN_DFLEGALLINE2,
				QUALIFIED_COLUMN_DFLEGALLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLEGALLINE3,
				COLUMN_DFLEGALLINE3,
				QUALIFIED_COLUMN_DFLEGALLINE3,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYDWELLINGTYPEID,
				COLUMN_DFPROPERTYDWELLINGTYPEID,
				QUALIFIED_COLUMN_DFPROPERTYDWELLINGTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDWELLILNGSTYLEID,
				COLUMN_DFDWELLILNGSTYLEID,
				QUALIFIED_COLUMN_DFDWELLILNGSTYLEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTRUCTUREAGE,
				COLUMN_DFSTRUCTUREAGE,
				QUALIFIED_COLUMN_DFSTRUCTUREAGE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNOOFUNITS,
				COLUMN_DFNOOFUNITS,
				QUALIFIED_COLUMN_DFNOOFUNITS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNUMBEROFBEDROOMS,
				COLUMN_DFNUMBEROFBEDROOMS,
				QUALIFIED_COLUMN_DFNUMBEROFBEDROOMS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIVINGSPACE,
				COLUMN_DFLIVINGSPACE,
				QUALIFIED_COLUMN_DFLIVINGSPACE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYLIVINGSPACEMEASUREID,
				COLUMN_DFPROPERTYLIVINGSPACEMEASUREID,
				QUALIFIED_COLUMN_DFPROPERTYLIVINGSPACEMEASUREID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOTSIZE,
				COLUMN_DFLOTSIZE,
				QUALIFIED_COLUMN_DFLOTSIZE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOTSIZEDEPTH,
				COLUMN_DFLOTSIZEDEPTH,
				QUALIFIED_COLUMN_DFLOTSIZEDEPTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOTSIZEFRONTPAGE,
				COLUMN_DFLOTSIZEFRONTPAGE,
				QUALIFIED_COLUMN_DFLOTSIZEFRONTPAGE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLOTSIZEUNITMEASUREDID,
				COLUMN_DFLOTSIZEUNITMEASUREDID,
				QUALIFIED_COLUMN_DFLOTSIZEUNITMEASUREDID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYHEATTYPEID,
				COLUMN_DFPROPERTYHEATTYPEID,
				QUALIFIED_COLUMN_DFPROPERTYHEATTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUFFIINSULATION,
				COLUMN_DFUFFIINSULATION,
				QUALIFIED_COLUMN_DFUFFIINSULATION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYWATERTYPEID,
				COLUMN_DFPROPERTYWATERTYPEID,
				QUALIFIED_COLUMN_DFPROPERTYWATERTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYSWEGETYPEID,
				COLUMN_DFPROPERTYSWEGETYPEID,
				QUALIFIED_COLUMN_DFPROPERTYSWEGETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYUSAGEID,
				COLUMN_DFPROPERTYUSAGEID,
				QUALIFIED_COLUMN_DFPROPERTYUSAGEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYOCCUPANCYTYPEID,
				COLUMN_DFPROPERTYOCCUPANCYTYPEID,
				QUALIFIED_COLUMN_DFPROPERTYOCCUPANCYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYTYPEID,
				COLUMN_DFPROPERTYTYPEID,
				QUALIFIED_COLUMN_DFPROPERTYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

//		***** Change by NBC Impl. Team - Version 1.2 - Start *****//

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREQUESTAPPRAISALID,
				COLUMN_DFREQUESTAPPRAISALID,
				QUALIFIED_COLUMN_DFREQUESTAPPRAISALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
//		***** Change by NBC Impl. Team - Version 1.2 - End *****//	
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROPERTYLOCATIONID,
				COLUMN_DFPROPERTYLOCATIONID,
				QUALIFIED_COLUMN_DFPROPERTYLOCATIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFZONING,
				COLUMN_DFZONING,
				QUALIFIED_COLUMN_DFZONING,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPURCHASEPRICE,
				COLUMN_DFPURCHASEPRICE,
				QUALIFIED_COLUMN_DFPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANDVALUE,
				COLUMN_DFLANDVALUE,
				QUALIFIED_COLUMN_DFLANDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEQUITYAVAILABLE,
				COLUMN_DFEQUITYAVAILABLE,
				QUALIFIED_COLUMN_DFEQUITYAVAILABLE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESTIMATEVALUE,
				COLUMN_DFESTIMATEVALUE,
				QUALIFIED_COLUMN_DFESTIMATEVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLTV,
				COLUMN_DFLTV,
				QUALIFIED_COLUMN_DFLTV,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPROPERTYEXP,
				COLUMN_DFTOTALPROPERTYEXP,
				QUALIFIED_COLUMN_DFTOTALPROPERTYEXP,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMLSLISTINGFLAG,
				COLUMN_DFMLSLISTINGFLAG,
				QUALIFIED_COLUMN_DFMLSLISTINGFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGARAGESIZEID,
				COLUMN_DFGARAGESIZEID,
				QUALIFIED_COLUMN_DFGARAGESIZEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGARAGETYPEID,
				COLUMN_DFGARAGETYPEID,
				QUALIFIED_COLUMN_DFGARAGETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBUILDERNAME,
				COLUMN_DFBUILDERNAME,
				QUALIFIED_COLUMN_DFBUILDERNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNEWCONSTRUCTIONID,
				COLUMN_DFNEWCONSTRUCTIONID,
				QUALIFIED_COLUMN_DFNEWCONSTRUCTIONID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		//MI additions
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
					FIELD_DFSUBDIVISIONDISCOUNT,
				COLUMN_DFSUBDIVISIONDISCOUNT,
				QUALIFIED_COLUMN_DFSUBDIVISIONDISCOUNT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
					FIELD_DFTENURETYPEID,
					COLUMN_DFTENURETYPEID,
				QUALIFIED_COLUMN_DFTENURETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		
		FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFONRESERVETRUSTAGREEMENTNUMBER,
                COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER,
                QUALIFIED_COLUMN_DFONRESERVETRUSTAGREEMENTNUMBER,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFMIENERGYEFFICIENCY,
                COLUMN_DFMIENERGYEFFICIENCY,
                QUALIFIED_COLUMN_DFMIENERGYEFFICIENCY,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	                FIELD_DFMORTGAGEINSURERID,
	                COLUMN_DFMORTGAGEINSURERID,
	                QUALIFIED_COLUMN_DFMORTGAGEINSURERID,
	                String.class,
	                false,
	                false,
	                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                "",
	                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                ""));
	}

	//Begin MI additions
	public String getDfSubdivisionDiscount() {
		return (String)getValue(FIELD_DFSUBDIVISIONDISCOUNT);
	}


	public void setDfSubdivisionDiscount(String value) {
		setValue(FIELD_DFSUBDIVISIONDISCOUNT,value);
	}


	public BigDecimal getDfTenureTypeId() {
		return (java.math.BigDecimal)getValue(FIELD_DFTENURETYPEID);
	}


	public void setDfTenureTypeId(BigDecimal value) {
		setValue(FIELD_DFTENURETYPEID,value);
	}
	
	public String getDfOnReserveTrustAgreementNumber()
    {
        return (String)getValue(FIELD_DFONRESERVETRUSTAGREEMENTNUMBER);
    } 

    public void setDfOnReserveTrustAgreementNumber(String value)
    {
        setValue(FIELD_DFONRESERVETRUSTAGREEMENTNUMBER, value);
    }
    
    public String getDfMIEnergyEfficiency()
    {
        return (String)getValue(FIELD_DFMIENERGYEFFICIENCY);
    } 

    public void setDfMIEnergyEfficiency(String value)
    {
        setValue(FIELD_DFMIENERGYEFFICIENCY, value);
    }
    
    public String getDfMortgageInsurerId()
    {
        return (String)getValue(FIELD_DFMORTGAGEINSURERID);
    } 

    public void setDfMortgageInsurerId(String value)
    {
        setValue(FIELD_DFMORTGAGEINSURERID, value);
    }
}

