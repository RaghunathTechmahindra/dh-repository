package mosApp.MosSystem;
/**
 * 29/Feb/2008 DVG #DG702 MLM x BDN merge adjustments
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.calc.impl.CommitmentExpiryDate;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.CreditBureauReport;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.QualifyDetail;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ACM;
import com.basis100.deal.security.AccessResult;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DLM;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.TypeConverter;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.mtgrates.PricingLogic2;
import com.basis100.mtgrates.RateInfo;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.util.service.QualifyingRateHelper;
import com.filogix.util.Xc;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.TiledViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HtmlDisplayField;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.RadioButtonGroup;
import com.filogix.express.web.util.service.QualifyingRateHelper;

/**
 * <p>DealEntryHandler</p>
 *
 * @version 1.1 MCM team (XS 2.60) July 29, 2008: added saveCompInfoCheckBox in SaveEntity method
 *              MCM team (XS 2.60) July 31, 2008: added JS to control check box enable/disable
 *              by miUpfront and taxPayor
 * @version 1.2 MCM team Aug 21, 2008 : artf763316 Aug 21, 2008 : replacing XS_2.60.
 */
  // Special usage of Deal Entry/Deal Mod PageEntry members:
  //
  // 1) pageCondition2
  //
  //    Set to indicate page generation condition.
  //
  //    Values - false: external page generation. Page is being displayed for the first time
  //                    after navigation from external page. This flag is used to determine if
  //                    page startup processing (e.g. business rule execution) should be
  //                    performed before the page is displayed.
  //
  //             true:  inner page generation. Page is being displayed for inner-worksheet
  //                    purposes.
  //
  // 2) pageCondition5
  //
  //    Used to indicate if update if MI values is allowed. Values true/false
  //
  //    For clarity the method isMIUpdateAllowed() returns the value of the condition.
  //
  // 3) pageCondition7
  //
  //    Set to indicate is Rate locked or not. Values true/false. This condition is used
  //    both on the Deal Modidfication and UWorksheet pages.
  //
  // 4) pageCondition8
  //
  //    Used to indicate the origiantion of the deal. Values true:electronic/false:manual.
  //    This condition is used both on the Deal Modification and UWorksheet pages.
  //
  // 5) pageCondition4
  //
  //    Post-decision status category dialog has been presented and answered. Values true/false.
  //
  // 6) pageCondition6
  //
  //    Indicator to check MI rules when submit. Values true/false.
  //    Used in MI Review screen for Post-Decision handling.
  //

public class DealEntryHandler extends DealHandlerCommon implements Cloneable, Xc {

    private static String DFDEALID = "dfDealId";
    private static String DFCOPYID = "dfCopyId";
    private static String DFINSTITUTIONID = "dfInstitutionId";
    String homeBaseDealNote = "";	//#DG702

  public DealEntryHandler()
  {
    super();
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
  }

  public DealEntryHandler(String name)
  {
    this(null,name); // No parent
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
  }

  protected DealEntryHandler(View parent, String name)
  {
    super(parent,name);
    theSessionState = super.theSessionState;
    savedPages = super.savedPages;
  }


  // key for storing fields edit buffer in page state table
  public DealEntryHandler cloneSS()
  {
    return(DealEntryHandler) super.cloneSafeShallow();

  }


  /**
   *
   *
   */
  public boolean isMIUpdateAllowed(PageEntry pg)
  {
    return pg.getPageCondition5();

  }


  /**
   *
   *
   */
  public boolean isRateLocked(PageEntry pg)
  {
    return pg.getPageCondition7();

  }


  /**
   *
   *
   */
  public boolean isDealElectronic(PageEntry pg)
  {
    return pg.getPageCondition8();

  }

  //This method is used to populate the fields in the header
  //with the appropriate information

  public void populatePageDisplayFields()
  {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
		//#DG702 Hashtable pst = pg.getPageStateTable();
    HashSet skipElements = new HashSet();

    populatePageShellDisplayFields();
    
    // Quick Link Menu display
    displayQuickLinkMenu();
 
    populateTaskNavigator(pg);

    populatePreviousPagesLinks();

    ViewBean thePage = getCurrNDPage();

		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STTARGETDOWNPAYMENT, new String(TARGET_TAG));
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STTARGETESCROW, new String(TARGET_TAG));
    // check for deal entry page - via deal entry flag in page entry
    if (pg.isDealEntry() == true)
    {
      logger.trace("@---T---->DealEntryHandler@DealEntryPage: no Commitment Section");
			thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STPAGETITLE, new String("Deal Entry"));

      // Deal Summary Snapshop has been displaying for both Deal Entry
      // and Deal Modification pages now.	This commented out line should stay.
      ///        	displayDSSConditional(pg, getCurrNDPage());
      displayCommitmentSectionConditional(pg, getCurrNDPage());
    }
    else if (pg.isDealEntry() == false)
    {
      logger.trace("@---T---->DealEntryHandler@DealModificationPage: Commitment Section appears");
			thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STPAGETITLE, new String("Deal Modification"));
      populatePageDealSummarySnapShot();
    }

		customizeTextBox(pgDealEntryViewBean.CHILD_TXMIPREMIUM);

    //--Release2.1--start//
    //// Even non-changable combobox labels should be translated via BXResources.
    int langId = theSessionState.getLanguageId();

    // ALERT - code should be moved to the analog of setDealMainDisplayFields() eventually.
    //// BXTODO: It is better logically separate this very small API to the distint method
    //// to emphasize the business logic. This fragent should be moved on the PHC level because
    //// IWorksheet handler uses almost identical logic. This reorganization will be done along
    //// the IWorksheet Handler adjustment.

    // restrict each combobox lists for the MI section by one current deal value.
    doDealMIIndicatorModel insuranseIndicator =
                      (doDealMIIndicatorModel)(RequestManager.getRequestContext().getModelManager().getModel(doDealMIIndicatorModel.class));
    int indindx = getMIIndicator(insuranseIndicator);

    doDealMITypeModel type =
                      (doDealMITypeModel)(RequestManager.getRequestContext().getModelManager().getModel(doDealMITypeModel.class));
    int typeindx = getMITypeIndex(type);

    doDealMIInsurerModel insurer =
                (doDealMIInsurerModel)(RequestManager.getRequestContext().getModelManager().getModel(doDealMIInsurerModel.class));

    int insurerindx = getMInsurerIndex(insurer);
    //logger.debug("DEH@isMIUpdateAllowed? " + isMIUpdateAllowed(pg));

    // restrict MI choiceboxes to current values (if necessary)
		if (isMIUpdateAllowed(pg) == false) {
			reduceAccessToComboBox(thePage, pgDealEntryViewBean.CHILD_CBMIINDICATOR, indindx, langId, "MIINDICATOR");
			reduceAccessToComboBox(thePage, pgDealEntryViewBean.CHILD_CBMITYPE, typeindx, langId, "MORTGAGEINSURANCETYPE");
			reduceAccessToComboBox(thePage, pgDealEntryViewBean.CHILD_CBMIINSURER, insurerindx, langId, "MORTGAGEINSURANCECARRIER");

    }
    else if (isMIUpdateAllowed(pg) == true)
    {
      //// do nothing, JATO takes care.
    }

    //2. Customization of the rates block in the Finacial Details section.
		if (isRateLocked(pg) == true) {
			customizeTextBox(pgDealEntryViewBean.CHILD_TXDISCOUNT);
			customizeTextBox(pgDealEntryViewBean.CHILD_TXPREMIUM);
			customizeTextBox(pgDealEntryViewBean.CHILD_TXBUYDOWNRATE);
			skipElements.add(new String(pgDealEntryViewBean.CHILD_TXDISCOUNT));
			skipElements.add(new String(pgDealEntryViewBean.CHILD_TXPREMIUM));
			skipElements.add(new String(pgDealEntryViewBean.CHILD_TXBUYDOWNRATE));
    }
		else if (isRateLocked(pg) == false) {
			customDefaultTextBox(pgDealEntryViewBean.CHILD_TXDISCOUNT);
			customDefaultTextBox(pgDealEntryViewBean.CHILD_TXPREMIUM);
			customDefaultTextBox(pgDealEntryViewBean.CHILD_TXBUYDOWNRATE);

			try {
				String rateValidDir = PropertiesCache.getInstance().getProperty(-1, COM_BASIS100_DVALIDPROPERTIES_LOCATION, "");

        String rateValidPath = Sc.DVALID_DEAL_RATES_PROP_FILE_NAME;

        //logger.debug("DEH@RatesValidationPath: " + rateValidPath);

        JSValidationRules rateValidObj = new JSValidationRules(rateValidPath, logger, thePage);
        rateValidObj.attachJSValidationRules();
      }
      catch( Exception e )
      {
        logger.error("Exception DEH@PopulatePageDisplayFields: Problem encountered in JS data validation API." + e.getMessage());      }
      }

    //3. Customization of the Reference Source Application field. In case of
    // ingestion (electronic deal) this field is not editable.
    if (isDealElectronic(pg) == true)
    {
logger.debug("DEH@PopulatePageDisplayFields:isDealElectronic? " + isDealElectronic(pg));

			customizeTextBox(pgDealEntryViewBean.CHILD_TXREFERENCESOURCEAPP);
      //--Ticket#1006--start--//
      // Add electronic media channel disabled field for skipping to db
      // propagation.
			skipElements.add(new String(pgDealEntryViewBean.CHILD_TXREFERENCESOURCEAPP));
      //--Ticket#1006--end--//
    }
		else if (isDealElectronic(pg) == false) {
			customDefaultTextBox(pgDealEntryViewBean.CHILD_TXREFERENCESOURCEAPP);
    }

    //4. Customization for the dynamic Lender-->Product-->Rate module
		if (isRateLocked(pg) == true) {
			restrictedDynamicLPRCombos(pgDealEntryViewBean.CHILD_CBLENDER, pgDealEntryViewBean.CHILD_CBPRODUCT,
					pgDealEntryViewBean.CHILD_CBPOSTEDINTERESTRATE);
			skipElements.add(pgDealEntryViewBean.CHILD_TBRATECODE);
    }
    else if (isRateLocked(pg) == false)
    {
      String formName = "document.forms[0].pgDealEntry_";

      customizeDynamicLPRCombos("cbLender", "cbProduct", "cbPostedInterestRate", formName);
    }

		customizeTextBox(pgDealEntryViewBean.CHILD_TBRATECODE);

		customizeTextBoxWithAccess(pgDealEntryViewBean.CHILD_TBPAYMENTTERMDESCRIPTION);

    pg.setPageExcludePageVars(skipElements);

    //// 4. Data validation API.
		try {
			String dealEntryMainValidDir = PropertiesCache.getInstance().getProperty(-1, COM_BASIS100_DVALIDPROPERTIES_LOCATION, "");

      String dealEntryMainValidPath = Sc.DVALID_DEAL_MAIN_PROPS_FILE_NAME;

      //logger.debug("DEH@dealEntryMainValid: " + dealEntryMainValidPath);

      JSValidationRules dealEntryValidObj = new JSValidationRules(dealEntryMainValidPath, logger, thePage);
      dealEntryValidObj.attachJSValidationRules();
    }
    catch( Exception e )
    {
        logger.error("Exception DEH@PopulatePageDisplayFields: Problem encountered in JS data validation API." + e.getMessage());
    }

    ////5. Custome population of the Option object (combobox).
    populateJSVALSData(thePage);

    //6. Customization for the calculated fields.
		customizeTextBox(pgDealEntryViewBean.CHILD_TXTOTALDOWN);
		customizeTextBox(pgDealEntryViewBean.CHILD_TXTOTALESCROW);

    //--DJ_CR_203.1--start--//
    if(this.isDJClient() == true)
    {
      populateDefaultValues(thePage);
    }
    //--DJ_CR_203.1--end--//

    //7. Populate the Deal.ApplicantionDate for the Servlet
    DateFormat theFormat = new SimpleDateFormat("MM/dd/yyyy");

    String theAppDate;
    String disRateDate = "";
    java.util.Date disabledRateDate;
    int dealRateStatusId = 0; //default is enable.


    //-- ========== SCR#859 begins ========== --//
    //-- by Neil on Feb 15, 2005
    setCCAPSServicingMortgageNumber();
    //-- ========== SCR#859 ends ========== --//

    try
    {
      Deal theDeal = new Deal(getSessionResourceKit(), null, pg.getPageDealId(), pg.getPageDealCID());
      if (theDeal.getApplicationDate() != null)
          theAppDate = theFormat.format(theDeal.getApplicationDate());
      else
          theAppDate = theFormat.format(new java.util.Date());

          //--DisableProductRateTicket#570--10Aug2004--start--//
        int dealPricingProfileId = theDeal.getPricingProfileId();
			logger.debug("DEH@PopulatePageDisplayFields:DealPricingProfileId: "+ dealPricingProfileId);
			logger.debug("DEH@PopulatePageDisplayFields:ApplicationDate: "+ theDeal.getApplicationDate());

        dealRateStatusId = getDealRateStatusId(dealPricingProfileId);

        if (dealRateStatusId == Mc.PRICING_STATUS_DISABLED) {
          disabledRateDate = getDealRateDisDate(dealPricingProfileId);
          DateFormat disDateFormat = new SimpleDateFormat("MM/dd/yyyy");

          disRateDate = disDateFormat.format(disabledRateDate);
        }
        else {
          disabledRateDate = null;
          disRateDate = Mc.NULL_STRING;
        }

        logger.debug("DEH@PopulatePageDisplayFields:DisabledRateDate: " + disabledRateDate);
        logger.debug("DEH@PopulatePageDisplayFields:DealRateStatusId: " + dealPricingProfileId);
        logger.debug("DEH@PopulatePageDisplayFields:DisabledRateDate_Formatted: " + disRateDate);
    }
		catch (Exception e) {
			logger.debug("Exception DEH@PopulatePageDisplayFields:ApplicationDate:"+ e); //#DG702
      // Something goes wrong for the deal -- Just set to Current date by default
      theAppDate = theFormat.format(new java.util.Date());
    }

    String isEditable = getPageEditableFlag(pg);

		getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STAPPDATEFORSERVLET, new String(theAppDate));
		getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STDEALRATESTATUSFORSERVLET, (new Integer(dealRateStatusId)).toString());
		getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STRATEDISDATEFORSERVLET,new String(disRateDate));
		getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STISPAGEEDITABLEFORSERVLET,new String(isEditable));
    //--DisableProductRateTicket#570--10Aug2004--end--//

    //logger.debug("DEH@populatePageDisplayFields::AppDateForServlet: " + new String(theAppDate));

    // Check and set Display or not the FinancingProgram field
    setDisplayFinancingProgram(pg, thePage);

    // SEAN GECF IV Document Type Dorp down: chech the hide setting.
    // IV stands for Income Verification
    setHideDocumentType(pg, thePage);
    // END GECF IV Document Type Dorp down

    //--DJ_LDI_CR--start//
    //// 1. Customize the GDS/TDS Label with regards to displaying of Life&Disability section.
    //// 2. Hide of display Life&Disability Insurance related info on the screen.
    String gdstdsLabel = "";

		if (PropertiesCache.getInstance().getProperty(-1, COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N")
				.equals("N")) {
			gdstdsLabel = BXResources.getGenericMsg(GDS_TDS_NO_LIFE_DISABILITY_LABEL, langId);

			startSupressContent(pgDealEntryViewBean.CHILD_STINCLUDELIFEDISLABELSSTART, thePage);
			endSupressContent(pgDealEntryViewBean.CHILD_STINCLUDELIFEDISLABELSEND, thePage);

      startSupressContent("RepeatGDSTDSDetails/stIncludeLifeDisContentStart", thePage);
      endSupressContent("RepeatGDSTDSDetails/stIncludeLifeDisContentEnd", thePage);
    }
		else if (PropertiesCache.getInstance().getProperty(-1, COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE, "N")
				.equals("Y")) {
			gdstdsLabel = BXResources.getGenericMsg(GDS_TDS_WITH_LIFE_DISABILITY_LABEL, langId);
    }

		getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STGDSTDSDETAILSLABEL, gdstdsLabel);
    //--DJ_LDI_CR--end//

    //--DJ_CR134--start--27May2004--//
    //--Ticket#692--start--02Nov2004//
    // Open this option for every client, not property driven now.
    //if(this.isDJClient() == true && pg.isDealEntry() == false)
		if (pg.isDealEntry() == false) {
			/* #DG702 rewritten
      String homeBASEinfo = "";
			 if (getHomeBASEProductRatePmnt() != null && !getHomeBASEProductRatePmnt().equals(""))      {
        homeBASEinfo = getHomeBASEProductRatePmnt();
        getCurrNDPage().setDisplayFieldValue("stHomeBASERateProductPmnt", convertToHtmString(homeBASEinfo));
//logger.debug("DEH@populatePageDisplayFields::HomeBASEProductRatePmntConverted: " + convertToHtmString(homeBASEinfo));
      }
			 else      {
        getCurrNDPage().setDisplayFieldValue("stHomeBASERateProductPmnt",
			 BXResources.getGenericMsg("NOT_AVAILABLE_LABEL",  langId));
			 }*/
			String homeBASEinfo = homeBaseDealNote.equals("") ?
					BXResources.getGenericMsg(NOT_AVAILABLE_LABEL, langId)
					: convertToHtmString(homeBaseDealNote);
			//logger.debug("DEH@populatePageDisplayFields::HomeBASEProductRatePmntConverted: " + convertToHtmString(homeBASEinfo));
			getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STHOMEBASEPRODUCTRATEPMNT, homeBASEinfo);
   }
   //else if(this.isDJClient() == true && pg.isDealEntry() == true)
		else if (pg.isDealEntry() == true) {
			getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STHOMEBASEPRODUCTRATEPMNT,
					BXResources.getGenericMsg(NOT_AVAILABLE_LABEL, langId));
   }
   //--Ticket#692--end--02Nov2004//
   //--DJ_CR134--end--//

   // -------------- Catherine Rutgaizer, 25-May-05, #1387 begin --------------
   displayTotalAmount(thePage);
   // -------------- Catherine Rutgaizer, 25-May-05, #1387 end --------------

   // ***** Change by NBC/PP Implementation Team - GCD - Start *****//

		if (pg.getPageId() == Mc.PGNM_DEAL_ENTRY_ID) {
			startSupressContent(pgDealEntryViewBean.CHILD_ST_INCLUDE_GCDSUM_START, thePage);
			endSupressContent(pgDealEntryViewBean.CHILD_ST_INCLUDE_GCDSUM_END, thePage);
   }

   if (!isShowGCDSumarySection()) {
			startSupressContent(pgDealEntryViewBean.CHILD_ST_INCLUDE_GCDSUM_START, thePage);
			endSupressContent(pgDealEntryViewBean.CHILD_ST_INCLUDE_GCDSUM_END, thePage);
   }

   // ***** Change by NBC/PP Implementation Team - GCD - End *****//

   // ***** Change by NBC/PP Implementation Team - #1679 - Start *****//
   setHdForceProgressAdvance(thePage);
   // ***** Change by NBC/PP Implementation Team - #1679 - End *****//

   //Qualify Rate
   setHiddenQualifyRateSection(thePage);
   setQualifyRateJSValidation(thePage);

	   try 
	   {
	       Deal deal = new Deal(srk, null).findByPrimaryKey(new DealPK(pg.getPageDealId(), pg.getPageDealCID()));
	   	
	   	//fill the combobox
	       populateQualifyingProducts("cbQualifyProductType", deal);
           
           int qualProd = deal.getOverrideQualProd();
           if (qualProd > 0) {
                   getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_CBQUALIFYPRODUCTTYPE, qualProd);
           }
           
           Map defaultQualify = QualifyingRateHelper.getDefaultQualify(
                    getSessionResourceKit(), deal);
           for (Iterator i = defaultQualify.keySet().iterator(); i.hasNext();) {
                Integer qualifyProduct = (Integer) i.next();
                Integer qualifyRate = (Integer) defaultQualify.get(qualifyProduct);
                getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STHDQUALIFYPRODUCTID, qualifyProduct);
                getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STHDQUALIFYRATE, qualifyRate);
                
                if (pg.isDealEntry() && deal.getQualifyingOverrideFlag()==null ) {
                    getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_CBQUALIFYPRODUCTTYPE, qualifyProduct);
                }
                String prop = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                        "com.filogix.qualoverridechk", "Y");
                if (prop.equals("N")) {
                    HtmlDisplayField qualProdName =(HtmlDisplayField) getCurrNDPage().getDisplayField("stQualifyProductType");
                    String prodName = BXResources.
                    getPickListDescription(deal.getInstitutionProfileId(), "MTGPROD", qualifyProduct+"",
                            theSessionState.getLanguageId());
                    qualProdName.setValue(prodName);
                    getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STQUALIFYRATE, qualifyRate);
                }
                break;
            }
            
	   }
	   catch (Exception e)
	   { 
			 logger.error("Exception @UWorksheetHandler.setupBeforePageGeneration:QualifyRate:" + e);
			 getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_TXQUALIFYRATE, pgDealEntryViewBean.CHILD_TXQUALIFYRATE_RESET_VALUE);
	   }
	}

  // -------------- Catherine Rutgaizer, 25-May-05, #1387 begin --------------
  /**
   * @param thePage
   */
  private void displayTotalAmount(ViewBean thePage) {
    logger.debug("Katya: DEH@displayTotalAmount() begin");

		if (PropertiesCache.getInstance().getProperty(-1, COM_FILOGIX_DEALMOD_HIDE_TOTALLOANAMOUNT, "N").equals("Y")) {
      UserProfile lUserProfile = null;

      // find who the user is
      try {
        lUserProfile = new UserProfile(srk);
        lUserProfile = lUserProfile.findByPrimaryKey(new UserProfileBeanPK(srk.getExpressState().getUserProfileId(),
                                                                           theSessionState.getDealInstitutionId()));

        if (lUserProfile != null) {
          int typeId = lUserProfile.getUserTypeId();
					logger.debug("DEH@displayTotalAmount() userTypeId = " + typeId);

          if (   (typeId == Mc.USER_TYPE_JR_ADMIN)
              || (typeId == Mc.USER_TYPE_ADMIN)
							|| (typeId == Mc.USER_TYPE_SR_ADMIN)) {
						customizeTextBox(pgDealEntryViewBean.CHILD_TXREQUESTEDLOANAMOUNT);
          }
        }
      } catch (Exception e) {
        setStandardFailMessage();
        logger.error("DEH@displayTotalAmount(): error when trying to hide ReqLoanAmount:" + e.getMessage());
      }
    }
  }
  // -------------- Catherine Rutgaizer, 25-May-05, #1387 end --------------

  /**
   *
   *
   */
  ////public int displayRessurectButton()
  public boolean displayRessurectButton()
  {
    ////PageEntry pg = theSession.getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    SessionResourceKit srk = getSessionResourceKit();

    if (pg.isDealEntry() == true)
       ////return CSpPage.SKIP;
       return false;

    try
    {
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
      int statusId = deal.getStatusId();
      int referenceDealTypeId = deal.getReferenceDealTypeId();

      // referencetypeid == 2 ('Resurrect to Deal')
      if (referenceDealTypeId != 2 &&(statusId == Mc.DEAL_COLLAPSED || statusId == Mc.DEAL_DENIED))
            ////return CSpPage.PROCEED;
            return true;
     }
    catch(Exception e)
    {
      //setStandardFailMessage();
      logger.error("Exception DE@displayRessurectButton: Problem encountered in Hidding Ressurect Button." + e.getMessage());

                        ////return CSpPage.SKIP;
                        return false;
    }

    ////return CSpPage.SKIP;
    return false;
  }

  /**
   *
   *
   */
////	public int displayPreapprovalButton()
  public boolean displayPreapprovalButton()
  {
    ////PageEntry pg = theSession.getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    SessionResourceKit srk = getSessionResourceKit();

    if (pg.isDealEntry() == true)
        ////return CSpPage.SKIP;
        return false;

    try
    {
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
      int statusId = deal.getStatusId();
      String preapproved = deal.getPreApproval();
      if (statusId == Mc.DEAL_PRE_APPROVAL_OFFERED && preapproved.equalsIgnoreCase("Y"))
      ////return CSpPage.PROCEED;

      return true;

    }
    catch(Exception e)
    {
      setStandardFailMessage();
			logger.error("Exception DE@displayRessurectButton: Problem encountered in Hidding Preapproval Button:"+ e); //#DG702
      ////return CSpPage.SKIP;
      return false;
    }

    ////return CSpPage.SKIP;
    return false;
  }


  /**
   *
   *
   */
  public void handleResurrect()
  {
    ////PageEntry pg = theSession.getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    SessionResourceKit srk = getSessionResourceKit();

    try
    {
      srk.beginTransaction();

      // no audit for creation of the new (resurrected) deal
      srk.setInterimAuditOn(false);

      // Retrieve current deal info (master and deal tree)
      MasterDeal currMDeal = new MasterDeal(srk, null, pg.getPageDealId());
      Deal currDeal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

      // Create a new Deal and copy old deal to New Deal.
      MasterDeal newMDeal = new MasterDeal(srk, null);
      MasterDealPK mdPK = newMDeal.createPrimaryKey(srk.getExpressState().getDealInstitutionId());
      Deal newDeal = newMDeal.create(mdPK);
      
      //FXP29887:  Resurrected FCX deals show get set up in Master deal as CMX - This Impacts loan decisions returned to Expert - Begin 
      newMDeal.setFXLinkPOSChannel(currMDeal.getFXLinkPOSChannel());
      newMDeal.setFXLinkSchemaId(currMDeal.getFXLinkSchemaId());
      newMDeal.setFXLinkUWChannel(currMDeal.getFXLinkUWChannel());
     //FXP29887:  Resurrected FCX deals show get set up in Master deal as CMX - This Impacts loan decisions returned to Expert - End
      
      int newDealDefaultTreeCID = newDeal.getCopyId();

      // copy tree to new deal
      int newCID = newMDeal.copyFrom(currMDeal, pg.getPageDealCID());

      // clean out default tree of new deal
      newMDeal.deleteCopy(newDealDefaultTreeCID);

      // set new deal for re-underwrite
      newDeal = DBA.getDeal(srk, newMDeal.getDealId(), newCID);

      newDeal.setApplicationId("" + newDeal.getDealId());
      newDeal.setReferenceDealTypeId(Mc.DEAL_REF_TYPE_RESURRECTED_FROM);
      newDeal.setReferenceDealNumber("" + currDeal.getDealId());
      newDeal.setCopyType("G");
      newDeal.setScenarioLocked("N");
      newDeal.setScenarioRecommended("Y");
      newDeal.setDecisionModificationTypeId(Sc.DM_NO_MOD_REQUESTED);
      newDeal.setCheckNotesFlag("N");
      newDeal.setResolutionConditionId(Mc.DEAL_RES_CONDITION_NO_CONDITION);

      // Ticket# 1649 -- new request to set instructionProduced to "N" -- By BILLY 25May2001
      newDeal.setInstructionProduced("N");

      // New requirement from Product -- Ticket# 56 (BW PVCS) -- By Billy 10July2001
      newDeal.setActualClosingDate(null);
      newDeal.setCommisionAmount(0);
      newDeal.setDenialReasonId(0);
      newDeal.setInvestorApproved("");
      newDeal.setInvestorConfirmationNumber("");
      newDeal.setMIComments("");
      newDeal.setMICommunicationFlag("");
      newDeal.setMILoanNumber("");
      newDeal.setMIPolicyNumber("");
      //--> Also needed to clear the preserved MI Policy Numbers
      //--> By Billy 26Aug2003
      newDeal.setMIPolicyNumCMHC("");
      newDeal.setMIPolicyNumGE("");
      newDeal.setMIPolicyNumAIGUG("");
      newDeal.setMIPolicyNumPMI("");
      //========================================================
      newDeal.setMIStatusId(Mc.MI_STATUS_BLANK);
      newDeal.setMortgageInsuranceResponse("");
      newDeal.setOnHoldDate(null);
      newDeal.setOpenMIResponseFlag("");
      if (currDeal.getStatusId() == Mc.DEAL_DENIED) newDeal.setPreviouslyDeclined("Y");
      else newDeal.setPreviouslyDeclined("");
      newDeal.setRateLock("N");
      newDeal.setScenarioApproved("N");
      newDeal.setScenarioDescription("");
      newDeal.setScenarioNumber(0);
      newDeal.setSolicitorSpecialInstructions("");
      // ============== End of Changes ======================

      //--> Ticket # 130 ==> to preserve the Mortgage Servicing Number
      //--> By Billy 24Nov2003
      newDeal.setServicingMortgageNumber(currDeal.getServicingMortgageNumber());
      //===========================================================

      // SEAN Ticket #1381 June 23, 2005: Copy all credit bureau reports and deal notes
      // to the resurrected deal.  Meanwhile I comment out the changed for BMO to CCAPS.
      // Because we will copy all deal notes for the new deal.
      copyAllCreditBureauReports(currDeal, newDeal);
      // note for new deal
			//#DG702 rewritten
			//copyAllDealNotes(currDeal, newDeal);
      DealNotes note = new DealNotes(srk);
      note.copyDealNotes(currDeal, newDeal, -1);
      newDeal.setServicingMortgageNumber(currDeal.getServicingMortgageNumber());
      // SEAN Ticket #1381 END

      // FXP24828 STARTS - March 23, 2009
      Collection<QualifyDetail> qds = currDeal.getQualifyDetail();
      Iterator <QualifyDetail> qdi = qds.iterator();
      int sizeqd = qds.size();
      for (int i=0; i<sizeqd; i++){
          QualifyDetail qd = (QualifyDetail)qdi.next();
          QualifyDetail newQD = new QualifyDetail(srk);
          newQD = newQD.create(newDeal.getDealId(), qd.getInterestCompoundingId(), qd.getRepaymentTypeId());
          newQD.setAmortizationTerm(qd.getAmortizationTerm());
          newQD.setCalcMonitor(qd.getCalcMonitor());
          newQD.setInstitutionProfileId(qd.getInstitutionProfileId());
          newQD.setPAndIPaymentAmountQualify(qd.getPAndIPaymentAmountQualify());
          newQD.setPropagateChanges(qd.getPropagateChanges());
          newQD.setQualifyGds(qd.getQualifyGds());
          newQD.setQualifyRate(qd.getQualifyRate());
          newQD.setQualifyTds(qd.getQualifyTds());
          newQD.ejbStore();          
      }
      // FXP24828 ENDS
                                    
      newMDeal.setGoldCopyId(newDeal.getCopyId());
      CCM ccm = new CCM(getSessionResourceKit());
      ccm.cancelCommitment(newDeal, 'C');

      ////SYNCADD
      //Clear all Default Conditions
      // Modified by Billy 15Aug2002
      ConditionHandler ch = new ConditionHandler(srk);

      //// BXTODO: Temp commented to compile
      ch.updateForCommitmentCancelled(newDeal, false);

      // New requirement to re-assign new deal to the User who resurrect the deal.
      //    -- By BILLY 11Dec2001
      (new TaskReassignmentHandler(this)).reassignDealForResurrect(newDeal, srk.getExpressState().getUserProfileId(), srk);

      newMDeal.ejbStore();
      newDeal.ejbStore();

      // connection to previous deal auditable
      srk.restoreAuditOn();

      // reference current deal to new
      currDeal.setReferenceDealTypeId(Mc.DEAL_REF_TYPE_RESURRECTED_TO);
      currDeal.setReferenceDealNumber("" + newDeal.getDealId());

      // log to the history log files.
      String strFrom = "Resurrected from deal #";
      String strTo = "Resurrected to deal #";
      updateHistoryLog(pg.getPageDealId(), currDeal.getStatusId(), strTo + newDeal.getDealId(), new java.util.Date());
      updateHistoryLog(newDeal.getDealId(), newDeal.getStatusId(), strFrom + currDeal.getDealId(), new java.util.Date());

      currDeal.ejbStore();

      // trigger workflow (initial task generation for new deal)
      PageEntry newDealPg = setupMainPagePageEntry(Mc.PGNM_DEAL_MODIFICATION,
                                                   newDeal.getDealId(),
                                                   newDeal.getCopyId(),
                                                   newDeal.getInstitutionProfileId());
      workflowTrigger(1, srk, newDealPg, null);
      srk.commitTransaction();
      getSavedPages().setNextPage(newDealPg);
      navigateToNextPage(true);
      setActiveMessageToAlert(BXResources.getSysMsg("DEAL_MOD_NEW_DEAL_CREATED", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM, "EXIT");
    }
    catch(Exception e)
    {
      // rollback the transaction
      srk.cleanTransaction();
      logger.error("Problem encountered during HandleResurrect.");
      logger.error(e);
      setStandardFailMessage();
      return;
    }

    return;

  }


  /**
   *
   *
   */
  public void handlePreapprovalFirm()
  {
    ////PageEntry pg = theSession.getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    SessionResourceKit srk = getSessionResourceKit();


    // 1. Preapproval Firm Function consists of two different transactions. First one
    // sets different deal's flags for historical reasons and the second one runs the
    // commitment cancel module.
    try
    {
      logger.debug("--T--->DealEntryHandler@execute the PreapprovalFirm, first transaction");
      srk.beginTransaction();
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
      deal.setDatePreAppFirm(new java.util.Date());

      //Add log to deal history -- Billy 16May2001
      // Create History Record if Status Changed
      int oldStatus = deal.getStatusId();
      deal.setStatusId(Mc.DEAL_PRE_APPROVAL_FIRM);
      if (oldStatus != Mc.DEAL_PRE_APPROVAL_FIRM)
      {
        DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
        String msg = hLog.statusChange(deal.getStatusId(), "Pre-approval Firm");
        hLog.log(deal.getDealId(), deal.getCopyId(), msg, Mc.DEAL_TX_TYPE_EVENT,
                 theSessionState.getExpressState().getUserProfileId(), deal.getStatusId());
      }
      deal.setSpecialFeatureId(0);

      deal.ejbStore();
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      // rollback the transaction
      srk.cleanTransaction();
      logger.error("Problem encountered during the first transaction of the PreApprovalFirm.");
      logger.error(e);
      setStandardFailMessage();
      return;
    }


    //2. Commintment cancel transaction.
    try
    {
      logger.debug("--T--->DealEntryHandler@execute the PreapprovalFirm, second transaction");
      srk.beginTransaction();
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
      CCM ccm = new CCM(getSessionResourceKit());
      ccm.cancelCommitment(deal);

      deal.ejbStore();
      srk.commitTransaction();
    }
    catch(Exception e)
    {
      // rollback the transaction
      srk.cleanTransaction();
      logger.error("Problem encountered during the second transaction of the PreApprovalFirm.");
      logger.error(e);
      setStandardFailMessage();
      return;
    }

    /***** add for Deal Status Update *****/
    try {
        workflowTrigger(1, srk, pg, null);
    } catch (Exception e) {
        logger.error("PreApprovalFirm trigger workflow@ DealEntryHandler");
        logger.error(e);
    }
    /***** add for Deal Status Update *****/
    
    return;

  }


  /**
   *
   *
   */
  private void updateHistoryLog(int dealId, int statusId, String str, java.util.Date date)
    throws Exception
  {

    // create history record for transaction
    DealHistory hist = new DealHistory(getSessionResourceKit());

    hist.create(dealId, theSessionState.getSessionUserId(), Mc.DEAL_TX_TYPE_EVENT, statusId, str, date);

  }


  /////////////////////////////////////////////////////////////////////
  //
  // Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
  //

  public void setupBeforePageGeneration()
  {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    if (pg.getSetupBeforeGenerationCalled() == true) return;

    //--> Have to determine if user have edit right before populate DataObjects
    //--> Otherwise will screw up the page display !!
    //--> Bug Fix by Billy 01May2003
    boolean promptDecMod =(PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                                                                     "com.basis100.deal.mod.prompt.for.decision.mod", "N")).equals("Y");
    boolean isViewOnly = false;
    Deal deal = null;
    if(promptDecMod == true)
    {
      // check post-decision status category	(note - this rules out Deal Entry)
      try
      {
        deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
        DealStatusManager dsm = DealStatusManager.getInstance(srk);
        if (dsm.getStatusCategory(deal.getStatusId(), srk) != Mc.DEAL_STATUS_CATEGORY_POST_DECISION)
          promptDecMod = false;
      }
      catch(Exception e)
      {
        // should never happen ...
				logger.debug("DEH@setupBeforePageGeneration::promptDecMod:" + e); //#DG702
        promptDecMod = false;
      }

logger.debug("DEH@setupBeforePageGeneration::promptDecMod: " + promptDecMod);
    }
		final int langId = theSessionState.getLanguageId();
    //--> Determinate if user have Edit Right to Decision Mod screen
		if (promptDecMod == true) {
      //--> Added to check if user has access right to Decision Mod screen
      try{
         ACM acm = theSessionState.getAcm();
         AccessResult arUser = acm.pageAccess(PGNM_DECISION_MODIFICATION,
                 theSessionState.getLanguageId(), theSessionState.getDealInstitutionId());

         if (arUser.getAccessTypeID() != Sc.PAGE_ACCESS_EDIT)
         {
            //--> Bug Fix :: by Billy 30April2003
            // ensure view mode, drop tx copy, remove deal lock
            srk.beginTransaction();

            // free transactional deal copies (if any)
            DealEditControl dec = theSessionState.getDec();
            if (dec != null)
            {
              dec.dropToTxLevel(0, srk, pg);
              pg.setPageDealCID(dec.getCID());
              theSessionState.setDec(null);
            }
            //Remove Deal Lock
            DLM dlm = DLM.getInstance();
            dlm.unlock(pg.getPageDealId(), pg.getPageMosUserId(), srk, true);
            srk.commitTransaction();

            // set page view only (not editable)
            pg.setEditable(false);
            isViewOnly = true;
            //=========================================================
          }
        }
       catch(Exception e){
        logger.warning("Exception @DealEntryHandler.setupBeforePageGeneration ==> "+
          "Problem to when checking if user has access right to Decision Mod screen :: " + e.toString());
       }
    }
    //===========================================================================

    pg.setSetupBeforeGenerationCalled(true);
    setupDealSummarySnapShotDO(pg);

    // -- //
    SysLogger logger = getSessionResourceKit().getSysLogger();

    int dealId = pg.getPageDealId();

    if (dealId <= 0)
    {
      setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
      logger.error("Problem encountered displaying deal Entry - invalid dealId (" + dealId + ")");
      return;
    }

    //--BMO_MI_CR--start//
    //// When MI is not handled outside Xpress close access to MI Status and
    //// MI Certificate fields (close for all clients except BMO).
    boolean isOutsideBXP = getCretarioIsMIStatusOutsideBXP();
		pg.setPageCondition9(isOutsideBXP);
    //--BMO_MI_CR--end//

    Integer cspDealId = new Integer(dealId);

    int copyid = pg.getPageDealCID();

    Integer cspDealCPId = new Integer(copyid);

    //logger.debug("DEH@setupBeforePageGeneration::DealId: " + dealId + ", CopyId: " + copyid);

    //--DJ_CR134--start--27May2004--//
    // For DJ client execute this model to get HomeBASE product, rate, payment info (if exist)
    // dealnotes table (dealnotescategoryid = 10).
    //--Ticket#692--start--02Nov2004//
    // Open this option for every client, not property driven now.
    //if(this.isDJClient() == true && pg.isDealEntry() == false)
		if (pg.isDealEntry() == false) {
			/* #DG702 replaced - clean up code
      Integer notesCategoryId = new Integer(Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION_PRODUCT);
			 executeCretarioForDealNotesHomeBASEInfo(cspDealId, notesCategoryId);*/
			try {
				DealNotes dn = new DealNotes(srk);
				dn.setSilentMode(true);
				homeBaseDealNote = dn.findLastNoteTextByDealCategory(dealId, langId, DEAL_NOTES_CATEGORY_DEAL_INGESTION_PRODUCT);
				homeBaseDealNote = homeBaseDealNote.trim();
			}
			catch (Exception e) {
				homeBaseDealNote = "";
				logger.error("Exception @DealEntryHandler.setupBeforePageGeneration:homeBaseDealNote:"+ e); //#DG702
			}
    }
    //--DJ_CR134--end--//

	//4.4 Submission Agent
	doUWSourceDetailsModelImpl douwsourcedetails = (doUWSourceDetailsModelImpl) 
		(RequestManager.getRequestContext().getModelManager().getModel(doUWSourceDetailsModel.class));
	douwsourcedetails.clearUserWhereCriteria();
	douwsourcedetails.addUserWhereCriterion("dfDealId", "=", cspDealId);
	
    // call to set criteria for data objects
    executeCretarioForMainDealEntry(logger, cspDealId, cspDealCPId);


    // Added GDS/TDS section -- By BILLY 12Nov2001
    executeCretarioForApplicantGDSTDS(cspDealId, cspDealCPId);


    // =======================================================
    executeCretariaForAppllicant(cspDealId, cspDealCPId);
    //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForApplicant");

    executeCretariaForProperty(cspDealId, cspDealCPId);
    //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForProperty");

    executeCretariaForDownPayment(cspDealId, cspDealCPId);
    //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForDownPayment");

   	//***** Change by NBC/PP Implementation Team - GCD - Start *****//
	//GCD Summary data retrieval Begin

	executeCretarioForAdjudicationResponseBNCModel(cspDealId, cspDealCPId);

	//GCD Summary Data Retrieval End
	//***** Change by NBC/PP Implementation Team - GCD - End *****//


    executeCretarioForEscrowPayment(cspDealId, cspDealCPId);
    //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForEscrowPayment");

    executeCretarioForBridgeLoan(cspDealId, cspDealCPId);
    //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForBridgeLoan");

  //  executeCretarioSourceInfo(cspDealId, cspDealCPId);
    //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForSourceInfo");

    executeCretarioReferenceInfo(cspDealId, cspDealCPId);
    //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForReferenceInfo");

    // to disable MI Fields for Denied and collapsed deals - Ticket 267
    boolean miFieldsDisabled = false;
    try {
        Deal _deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
        miFieldsDisabled = disableMIFieldsForDeniedDeal(_deal.getStatusId());
    } catch (Exception e1) {
        logger.error(e1.toString());
		logger.error(e1);
    }
    
    boolean updateMIAllowed = !miFieldsDisabled && executeCretarioForMIStatus(cspDealId, cspDealCPId);

    pg.setPageCondition5(updateMIAllowed);
    //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForMIStatus");

    //--DJ_PT_CR--start//
    executeCretarioForPartyInfo(cspDealId);
    //--DJ_PT_CR--end//

    if (pg.isDealEntry() == false)
    {
      boolean rateLocked = executeCretarioForRateLock(cspDealId, cspDealCPId);
      //logger.debug("DEH@setupBeforePageGeneration::After executeCretarionForRateLock");

      pg.setPageCondition7(rateLocked);

      boolean isElectronic = executeCretarioForDealOrigination(cspDealId, cspDealCPId);
      logger.debug("DEH@setupBeforePageGeneration::IsDealElectronic? " + isElectronic);
      pg.setPageCondition8(isElectronic);
    }

    SessionResourceKit srk = getSessionResourceKit();
    preparePageGeneration(srk, pg, pg.getPageDealId(), pg.getPageDealCID());

    // check for post-decision modification/unlock query ...
    // check already processed
    //if (pg.getPageCondition4() == true) return;

    //==> Commented out by Billy 27May2003
    //==> For bug fix when user swapping Language when Decision Mod Dialog displaying
    //else
    // Display the post-decision Dialogue only once when 1st entering the page.
    //pg.setPageCondition4(true);
    //================================================================================
    //==> Commented out by Billy 27May2003
    //==> For bug fix when user swapping Language when Decision Mod Dialog displaying
    //else
    // Display the post-decision Dialogue only once when 1st entering the page.
    //================================================================================

    //--CervusPhaseII--17Feb2005--start--//
    //// IMPORTANT!! After the lock overriding facility (Release 3) the Post Decision Dialogue
    //// should be reimplemented. In order to reach this the full state machine based on:
    //// 1. toggle language on the Deal Mod page
    //// 2. toggle language on the ActiveMessage layer
    //// 3. submit on the Deal Mod page
    //// 4. submit on the ActiveMessage layer
    //// 5. cancel on the Deal Mod page
    //// 6. cancel on the ActiveMessage layer
    //// should be implemented.

    //// IMPORTANT!!! The code below will be encapsulated into separate classes!!
    //// Stay temporarily to fix the BMO build urgently.

    //// Must be used any page criteriaId except 1 or 2 (already in use on Deal Entry/Deal Mod!!).
    // 1. Set the initial state of the state machine.
		if (pg.getPageCondition4() == false) {
			pg.setPageCriteriaId3(langId);
			pg.setPageCriteriaId4(langId);
    }
    // 2. Initialize the variables for the initial state.
    int pageLangId = pg.getPageCriteriaId3(); // lang of originally a generated page
    int actMsgLangId = pg.getPageCriteriaId4(); // lang of Active Layer Message screen.

		int tempSessionLangId = langId;

    //temp
    /**
    logger.debug("DEH@setupBeforePageGeneration::PageCondition4: " + pg.getPageCondition4());
    logger.debug("@DEH@setupBeforePageGeneration::SessionStateLanguageId: " + theSessionState.getLanguageId());
    logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageId: " + pageLangId);
    logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageTempId: " + actMsgLangId);
    logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageTempId5: " + tempSessionLangId);
    **/

    // check for post-decision modification/unlock query ...
    // check already processed
		if (pg.getPageCondition4() == true
				&& (pageLangId == langId)
				&& (actMsgLangId == langId)) {
      /**
      logger.debug("DEH@setupBeforePageGeneration::PageCondition4_TrueEqual: " + pg.getPageCondition4());
      logger.debug("@DEH@setupBeforePageGeneration::SessionStateLanguageId_TrueEqual: " + theSessionState.getLanguageId());
      logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageId3_TrueEqual: " + pageLangId);
      logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageTempId4_TrueEqual: " + actMsgLangId);
      logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageTempId5_TrueEqual: " + tempSessionLangId);
      logger.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
      **/
			if (langId == 0 && pageLangId == 0
					&& actMsgLangId == 0) // PageCriteriaId3 && PageCriteriaId4
      {
        logger.debug("@DEH@setupBeforePageGeneration::Initial State: 0, 0, 0 ==> Preferred User Language: English");

        if (pg.getPageCriteriaId5() == theSessionState.getLanguageId()) //sanity check
        {
          logger.debug("@DEH@setupBeforePageGeneration::Submit Action First Time on ActMessage, English: State 0, 0, 0 ==> State 0, 0, 1");
          pg.setPageCriteriaId3(pageLangId);
          pg.setPageCriteriaId4(1);

          return;
        }
      }
			else if (langId == 1 && pageLangId == 1
					&& actMsgLangId == 1) // PageCriteriaId3 && PageCriteriaId4
      {
      logger.debug("@DEH@setupBeforePageGeneration::Initial State: 1, 1, 1 ==> Preferred User Language: French");

				if (pg.getPageCriteriaId5() == langId) //sanity check
       {
         logger.debug("@DEH@setupBeforePageGeneration::Submit Action First Time on ActMessage: State 1, 1, 1 ==> State 1, 0, 1");
         pg.setPageCriteriaId3(0);
					pg.setPageCriteriaId4(langId);

         return;
       }
      }
    }
		else if (pg.getPageCondition4() == true
				&& ((pageLangId != langId) || (actMsgLangId != langId))) {
      //temp
      /**
      logger.debug("DEH@setupBeforePageGeneration::PageCondition4_TrueNotEqual_Before: " + pg.getPageCondition4());
      logger.debug("@DEH@setupBeforePageGeneration::SessionStateLanguageId_TrueNotEqual_Before: " + theSessionState.getLanguageId());
      logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageId3_TrueNotEqual_Before: " + pageLangId);
      logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageTempId4_TrueNotEqual_Before: " + actMsgLangId);
      logger.debug("@DEH@setupBeforePageGeneration::PSTLanguageTempId5_TrueNotEqual_Before: " + tempSessionLangId);
      logger.debug("================================================================");
      **/
       // 3. Preferred user language: English activity.
			if (langId == 0 && pageLangId == 1
					&& actMsgLangId == 0) {
        logger.debug("@DEH@setupBeforePageGeneration::State: 0, 1, 0");
				if (pg.getPageCriteriaId5() == langId) {
          logger.debug("@DEH@setupBeforePageGeneration::Submit Action: State: 0, 1, 0 ==> State 0, 0, 1");
          pg.setPageCriteriaId3(actMsgLangId);
          pg.setPageCriteriaId4(pageLangId);
          return;
        }
				else if (pg.getPageCriteriaId5() != langId) {
          logger.debug("@DEH@setupBeforePageGeneration::Toggle Lang on ActMessage Layer: State: 0, 1, 0 ==> State 1, 0, 1");

					pg.setPageCriteriaId3(langId);
					pg.setPageCriteriaId4(langId);
        }
       }
			else if (langId == 0 && pageLangId == 1
					&& actMsgLangId == 1) {
         logger.debug("@DEH@setupBeforePageGeneration::State: 0, 1, 1");
				if (pg.getPageCriteriaId5() == langId) {
           logger.debug("@DEH@setupBeforePageGeneration::Submit Action: State: 0, 1, 1 ==> State 1, 0, 1");
					pg.setPageCriteriaId3(langId);
           pg.setPageCriteriaId4(actMsgLangId);
           return;
         }
				else if (pg.getPageCriteriaId5() != langId) {
           logger.debug("@DEH@setupBeforePageGeneration::Toggle Lang on GeneratedPage: State: 0, 1, 1 ==> State 1, 0, 0");

					pg.setPageCriteriaId3(langId);
					pg.setPageCriteriaId4(langId);
         }
       }

			else if (langId == 0 && pageLangId == 0
					&& actMsgLangId == 1) {
       logger.debug("@DEH@setupBeforePageGeneration:: Orig: State: 0, 0, 1");

				if (pg.getPageCriteriaId5() == langId) {
           logger.debug("@DEH@setupBeforePageGeneration::Submit Action: State: 0, 0, 1 ==> State 0, 1, 0");
           pg.setPageCriteriaId3(actMsgLangId);
           pg.setPageCriteriaId4(pageLangId);
           return;
         }
				else if (pg.getPageCriteriaId5() != langId) {
           logger.debug("@DEH@setupBeforePageGeneration::Toggle Lang on ActMessage Layer: State: 0, 0, 1 ==> State 0, 1, 1");

           pg.setPageCriteriaId3(pg.getPageCriteriaId5());
           pg.setPageCriteriaId4(pg.getPageCriteriaId5());
         }
       }

       // 4. Preferred user language: French activity.
			else if (langId == 1 && pageLangId == 0
					&& actMsgLangId == 1) {
        logger.debug("@DEH@setupBeforePageGeneration::State: 1, 0, 1");
				if (pg.getPageCriteriaId5() == langId) {
          logger.debug("@DEH@setupBeforePageGeneration::Submit Action: State 1, 0, 1 ==> State 1, 0, 0");
          pg.setPageCriteriaId3(pageLangId);
          pg.setPageCriteriaId4(pageLangId);
          return;
        }
				else if (pg.getPageCriteriaId5() != langId) {
          logger.debug("@DEH@setupBeforePageGeneration::Toggle Lang on Generated Page: 1, 0, 1 ==> State 0, 1, 0");

					pg.setPageCriteriaId3(langId);
          pg.setPageCriteriaId4(pageLangId);
        }
       }

			else if (langId == 1 && pageLangId == 0
					&& actMsgLangId == 0) {
         logger.debug("@DEH@setupBeforePageGeneration::State: 1, 0, 0");
				if (pg.getPageCriteriaId5() == langId) {
           logger.debug("@DEH@setupBeforePageGeneration::Submit Action: State 1, 0, 0 ==> State 1, 0, 1");
           pg.setPageCriteriaId3(pageLangId);
					pg.setPageCriteriaId4(langId);
           return;
         }
				else if (pg.getPageCriteriaId5() != langId) {
           logger.debug("@DEH@setupBeforePageGeneration::Toggle Lang on ActMessage Layer: State 1, 0, 0 ==> State 0, 1, 1");

					pg.setPageCriteriaId3(langId);
					pg.setPageCriteriaId4(langId);
         }
       }

			else if (langId == 1 && pageLangId == 1
					&& actMsgLangId == 0) {
         logger.debug("@DEH@setupBeforePageGeneration::State: 1, 1, 0");
				if (pg.getPageCriteriaId5() == langId) {
           logger.debug("@DEH@setupBeforePageGeneration::Submit Action: State 1, 1, 0 ==> State 1, 0, 1");
           pg.setPageCriteriaId3(pageLangId);
					pg.setPageCriteriaId4(langId);
           return;
         }
				else if (pg.getPageCriteriaId5() != langId) {
           logger.debug("@DEH@setupBeforePageGeneration::Toggle Lang on ActMessage Layer: State 1, 1, 0 ==> State 0, 1, 1");

					pg.setPageCriteriaId3(langId);
					pg.setPageCriteriaId4(langId);
         }
       }

			pg.setPageCriteriaId4(langId);
    }

    // Refresh the page language state.
    pg.setPageCriteriaId5(tempSessionLangId);
    pg.setPageCondition4(true);
    //--CervusPhaseII--17Feb2005--end--//

    // check editable - n/a if not editable
    if (pg.isEditable() == false) return;

    // check if need to prompt for Decision Mod.
    if (promptDecMod != true || isViewOnly == true) return;

    // presumable here because of usage of decision mod screen - must assume all ok
    // check dec. mod already set (e.g. via Decision Mod. screen - hance no need to prompt)
    int dmtId = deal.getDecisionModificationTypeId();

logger.debug("@DEH@setupBeforePageGeneration::DecisionModificationTypeId: " + deal.getDecisionModificationTypeId());

    if (dmtId == Mc.DM_NON_CRITICAL_NO_DOC_PREP || dmtId == Mc.DM_NON_CRITICAL_DOC_PREP) return;

    // present dialog as active message
    ActiveMessage am = null;

    //--Release2.1--//
    //Modified to pass LanguageId for Multilingual handling
		am = ActiveMsgFactory.getActiveMessage(ActiveMsgFactory.ISCUSTOMDIALOG, langId);
		am.setDialogMsg(BXResources.getSysMsg(UNLOCK_POST_DECISION_DEAL_MOD_QUERY, langId));
		am.setTitle(BXResources.getSysMsg(UNLOCK_POST_DECISION_DEAL_MOD_QUERY_TITLE, langId)
				+ deal.getDealId());

    ////SYNCADD
    //--Sync--//
    //--> This was missed in the previous sync.  Fixed by Billy 28Feb2003
    //Removed the POST_DECISION_UNLOCK_WITH_DOC option -- by BILLY 12June2002
    Vector responses = new Vector();
    responses.add("POST_DECISION_VIEW_ONLY");
    responses.add("POST_DECISION_UNLOCK_NO_DOC");
    //responses.add("POST_DECISION_UNLOCK_WITH_DOC");

    Vector images = new Vector();
		images.add(BXResources.getGenericMsg(POST_DECISION_VIEW_ONLY_IMAGE, langId));
		images.add(BXResources.getGenericMsg(POST_DECISION_UNLOCK_NO_DOC_IMAGE, langId));
    //images.add("unlockdealco.gif");
    //=======================================================================

    am.setButtons(responses, images);
    theSessionState.setActMessage(am);
    
/*    HtmlDisplayField qualProdName =(HtmlDisplayField) thePage.getDisplayField("stQualifyProductType");
    
    String prodName = BXResources.
    getPickListDescription(institutionId, "MTGPROD", qualProdId,
            theSessionState.getLanguageId());
    qualProdName.setValue(prodName); */
    
    logger.debug("DEH@setupBeforePageGeneration::I finished setupBeforePageGeneration");

  }
  
  /**
   * used to determine that MI fileds should be disabled for denied and collapsed deals - Ticket 267
   * @param dealStatusId
   * @return
   */
  private boolean disableMIFieldsForDeniedDeal(Integer dealStatusId)
  {

  	if (dealStatusId == Mc.DEAL_DENIED || dealStatusId == Mc.DEAL_COLLAPSED )
  	{
  		return true;
  	}
  	return false;
	}



  //
  // Perform any special activities required immediately before page generation.
  //
  private void preparePageGeneration(SessionResourceKit srk, PageEntry pg, int dealId, int copyId)
  {

    //  data-validation messages, produced only during first-time display (see
    //  pageCondition2 documentation above) - and only for deal mod (never deal entry)
    if (pg.getPageCondition2() == false && pg.getPageId() == Mc.PGNM_DEAL_MODIFICATION)
    {
      try
      {
      	Deal deal = DBA.getDeal(srk, dealId, copyId);  
      	//4.3GR, Update Total Loan Amount
      	if(displayAMLForUpdatedMIPremium(deal)) return;
        
      	pg.setPageCondition2(true);

        // run data validation business rules
        PassiveMessage pm = validateDealEntry(deal, pg, srk);

        if (pm != null && pm.getNumMessages() > 0 )
        {
          pm.setGenerate(true);
          ////getTheSession().setPasMessage(pm);
          theSessionState.setPasMessage(pm);
        }
      }
      catch(Exception e)
      {
        logger.error("Exception @DealEntryHandler.preparePageGeneration: - ignored.");
        logger.error(e);
      }
    }

  }

  //// BXTODO: finish adjustment of this method. Looks like nobody calls this method. VERIFY!!
  //// eliminate during the BX framework final cleaning up.
  /**
  private void validateRepeatableResult(String fullRepeatableName, String repeatableName, String resultName, String eventType, String extraJSFunction)
  {
    try
    {
      logger.trace("@DealEntryHandler.calculateRepeatableResult");
      CSpTextBox txInput =(CSpTextBox) getCurrNDPage().getDisplayField(fullRepeatableName);
      CSpTextBox txResult =(CSpTextBox) getCurrNDPage().getDisplayField(resultName);
      String onChangeResult = new String(eventType + " =\"" + extraJSFunction + "; " + "addForm('" + repeatableName + "', '" + resultName + "'); \"");
      txInput.setExtraHtmlText(onChangeResult);
    }
    catch(Exception e)
    {
      setStandardFailMessage();
      logger.error("Exception DealEntryHandler@calculateRepeatableResult: Problem encountered in downpayment calculations." + e.getMessage());
    }

  }
  **/

  /**
   *
   *
   * This method should be split into two different ones.
   */
  /**
  public void handleStaticFieldSetup(ViewBean pg, CSpDisplayEvent event, String staticFieldName, String doName, String dataFieldName)
  {
    SessionState theSession = getTheSession();

    ////PageEntry currPage = getTheSession().getCurrentPage();

    //0. reset the static field
    pg.setDisplayFieldValue(staticFieldName, new CSpNull());


    //1. get the data object whose data will be used to initiate
    //	 the static field value
    CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) CSpider.getDataObject(doName);


    //4. execute the data object to retrieve the info
    spider.database.CSpDBResultTable resultTab = null;

    try
    {
      resultTab = theDO.getLastResults().getResultTable();
    }
    catch(Exception e)
    {
      logger.error("Exception @DealEntryHandler().handleStaticFieldSetup(): executing data object");
      logger.error(e);
      return;
    }


    //5. retrieve the actual value from the executed data object
    String value = null;

    try
    {
      int doFldNdx = theDO.getDataFieldPosition(dataFieldName);
      value = resultTab.getValue(0, doFldNdx).toString();
    }
    catch(Exception e)
    {
      logger.error("Exception @EditFieldsBuffer.readFields: reading from data object");
      logger.error(e);
    }

    if (value != null)
    {
      ((StaticTextField) event.getSource()).setValue(new String(value));
      logger.debug("---D---->DEH.handleStaticFieldSetup@setting the selected value");
    }
    else
    {
      ((StaticTextField) event.getSource()).setValue(new CSpNull());
      logger.debug("---D---->DEH.handleStaticFieldSetup@setting the null value");
    }

    return;

  }
  **/


  /**
   * actions taken after calling a handler specific method. Loads the specified
   * static field name on the specified ViewBean.
   *
   * BXTODO: This method is partly reorganized in accordancy with the JATO architecture
   * The final adjustment and testing against both Migration and local databases must
   * be done along with the IWorksheet Handler adjustment.
   * IMPORTANT!. Currently the family of so-called 'passedStaticNameToHtml' methods
   * is located on each level: PHC, DEH, IWQH. After IWorksheet adjustment this family
   * should be reorganized into the API and moved to the PHC level.
   *
   */
  /**
  ////public void handleRateTimeStampPassToHtml(ViewBean pg, CSpDisplayEvent event, String staticFieldName, String doName, String dataFieldName)
  public void handleRateTimeStampPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {
    ////SessionState theSession = getTheSession();

    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    logger.debug("DEH@handleRateTimeStampPassToHtml::currPg: " + currPg);
    logger.debug("DEH@handleRateTimeStampPassToHtml::ViewBean: " + pg);

    doDealEntryMainSelectModel theModel = (doDealEntryMainSelectModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doDealEntryMainSelectModel.class);

    //1. retreive the deal id value to work with the data object
    int dealId = currPg.getPageDealId();
    logger.debug("DEH@handleRateTimeStampPassToHtml::DealId: " + dealId);

    Integer cspDealId = new Integer(dealId);

    int copyid = currPg.getPageDealCID();
    logger.debug("DEH@handleRateTimeStampPassToHtml::CopyId: " + copyid);

    Integer cspDealCPId = new Integer(copyid);

    ////2. init the dynamic criteria using the deal id retrieved and
    ///// retrieve the actual value from the executed data object
    ////java.util.Date rateDate = null;
    String sRateDate = "";

    GregorianCalendar grRateDate = null;

    theModel.clearUserWhereCriteria();

    theModel.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    theModel.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

//////////////

//          Object oSBPFirmDate = theDO.getValue(theDO.FIELD_DFSBPFIRMDATE);
//          if (oSBPFirmDate == null)
//          {
//            SBPFirmDate = "";
//          }
//          else if((oSBPFirmDate != null))
//          {
//            SBPFirmDate = oSBPFirmDate.toString();
//            logger.debug("SBEH@setupEditBuffer::SBPFirmDateFromModel: " + oSBPFirmDate.toString());
//          }


/////////////

    try
    {
      ResultSet rs = theModel.executeSelect(null);

      if(rs == null || theModel.getSize() < 0)
      {
        sRateDate =  "";
      }

      else if(rs != null && theModel.getSize() > 0)
      {
        logger.debug("DEH@handleRateTimeStampPassToHtml::Size of the result set: " + theModel.getSize());
        logger.debug( "____START____" );
          while (theModel.next()) {
                Object oRateDate = theModel.getValue(theModel.FIELD_DFRATEDATE);
                if(oRateDate == null)
                {
                    //// should not happen, take care after the testing.
                    logger.debug("DEH@handleRateTimeStampPassToHtml::Null mode");
                    sRateDate = "NULL";
                }
                else
                {
                  //// change to the date format.
                  sRateDate = oRateDate.toString();
                  logger.debug("DEH@handleRateTimeStampPassToHtml::RateDate: " + sRateDate);
                  logger.debug("DEH@handleRateTimeStampPassToHtml::RateDateJATOTypeConvSQLTimestamp: " +
                                com.iplanet.jato.util.TypeConverter.asType(com.iplanet.jato.util.TypeConverter.TYPE_SQL_TIMESTAMP, oRateDate));

                  logger.debug("DEH@handleRateTimeStampPassToHtml::RateDateJATOTypeConvString: " +
                                com.iplanet.jato.util.TypeConverter.asType(com.iplanet.jato.util.TypeConverter.TYPE_STRING, oRateDate));

                  ////rateDate = new java.util.Date(oRateDate.toString());

                  ////Calendar tmpRateDate = rateDate.getCalendar();

                  //// SQL TimeStamp format: 2001-10-11 10:48:00.0
                  //// The mask must be exactly the same with this lower/capital
                  //// letters combination.
                  SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd hh:mm:ss.SSS" );

                  java.util.Date tmpRateDate = formatter.parse( sRateDate, new ParsePosition(0));
                  logger.debug( "DEH@handleRateTimeStampPassToHtml::Parsed time: " + ((tmpRateDate == null) ? "NULL" : tmpRateDate.toString()));
                  grRateDate = new GregorianCalendar();
                  grRateDate.setTime(tmpRateDate);
                  logger.debug( "DEH@handleRateTimeStampPassToHtml::Final Result: " + grRateDate.toString() );

                  ////grRateDate.setTime(rateDate);

                  //// Obsolete ND fragment incoming from the CSpDateTime format
                  //// definition.
                  ////grRateDate.set(Calendar.YEAR, rateDate.getYear());
                  ////grRateDate.set(Calendar.MONTH, rateDate.getMonth());
                  ////grRateDate.set(Calendar.DAY_OF_MONTH, rateDate.getDay());
                  ////grRateDate.set(Calendar.HOUR_OF_DAY, rateDate.getHours());
                  ////grRateDate.set(Calendar.MINUTE, rateDate.getMinutes());
                  ////grRateDate.set(Calendar.SECOND, rateDate.getSeconds());
                  ////grRateDate.set(Calendar.MILLISECOND, rateDate.get(Calendar.MILLISECOND));
                  ////grRateDate.setTimeZone(rateDate.getTimezoneOffset());


                  //// VERY TEMP hardcoded to get the result for the default ProductId.
                  ////grRateDate = new GregorianCalendar();
                  ////java.util.Date tmpRateDate = new java.util.Date();

                  ////grRateDate.set(Calendar.YEAR, (new Integer("2001")).intValue());
                  ////grRateDate.set(Calendar.MONTH, (new Integer("10")).intValue());
                  ////grRateDate.set(Calendar.DAY_OF_MONTH, (new Integer("11")).intValue());
                  ////grRateDate.set(Calendar.HOUR_OF_DAY, (new Integer("10")).intValue());
                  ////grRateDate.set(Calendar.MINUTE, (new Integer("48")).intValue());
                  ////grRateDate.set(Calendar.SECOND, (new Integer("00")).intValue());
                  ////grRateDate.set(Calendar.MILLISECOND, (new Integer("0")).intValue());
                  //// This should be done generically as it is done in the DealUIParser class.
                  ////grRateDate.setTimeZone(gr.getTimezoneOffset());

                  ////logger.debug("DEH@handleRateTimeStampPassToHtml::GRCalRateDate: " + grRateDate);
                }
              }
              logger.debug( "__END__" );
          }
    }
    catch (ModelControlException mce)
    {
      logger.debug("handleRateTimeStampPassToHtml::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.debug("DEH@handleStaticPassToHtml::SQLException: " + sqle);
    }

    //// Obsolete ND's fragment.
    ////DisplayField field =(DisplayField) event.getSource();
    String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

    logger.debug("DEH@handleRateTimeStampPassToHtml::fieldName: " + fieldName);
    logger.debug("DEH@handleRateTimeStampPassToHtml::fld: " + fld);

    if (grRateDate != null)
    {
      fld.setValue(new String(convertDateToServletFormat(grRateDate)));
    }
    else
    {
      // null is not passed
      // field.setValue(new CSpNull());
      // temp pass for now some fake indicator of null: empty string
      ////field.setValue(new String(""));
      fld.setValue(new String(""));
    }
  }
**/
  public void handleRateTimeStampPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {
    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    doDealEntryMainSelectModel theModel = (doDealEntryMainSelectModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doDealEntryMainSelectModel.class);

    GregorianCalendar grRateDate = null;

    Object oRateDate = theModel.getValue(theModel.FIELD_DFRATEDATE);

    if(oRateDate != null)
    {
      //logger.debug("DEH@handleRateTimeStampPassToHtml::RateDate: " + oRateDate.toString());
      //logger.debug("DEH@handleRateTimeStampPassToHtml::RateDateJATOTypeConvSQLTimestamp: " +
      //            com.iplanet.jato.util.TypeConverter.asType(com.iplanet.jato.util.TypeConverter.TYPE_SQL_TIMESTAMP, oRateDate));

      //logger.debug("DEH@handleRateTimeStampPassToHtml::RateDateJATOTypeConvString: " +
      //              com.iplanet.jato.util.TypeConverter.asType(com.iplanet.jato.util.TypeConverter.TYPE_STRING, oRateDate));

//        SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd hh:mm:ss.SSS" );
        SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd" );

      java.util.Date tmpRateDate = formatter.parse( oRateDate.toString(), new ParsePosition(0));

      //logger.debug( "DEH@handleRateTimeStampPassToHtml::Parsed time: " + ((tmpRateDate == null) ? "NULL" : tmpRateDate.toString()));
      grRateDate = new GregorianCalendar();
      grRateDate.setTime(tmpRateDate);
      //logger.debug( "DEH@handleRateTimeStampPassToHtml::Final Result: " + grRateDate.toString() );
    }
    else
    {
      logger.debug( "DEH@handleRateTimeStampPassToHtml:: Rate Date Object mode");
    }

    String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

    if (grRateDate != null)
    {
      fld.setValue(new String(convertDateToServletFormat(grRateDate)));
    }
    else
    {
      fld.setValue(new String(""));
    }

  }


   /**
   * actions taken after calling a handler specific method. Loads the specified
   * static field name on the specified ViewBean.
   *
   * This method is reorganized in accordancy with the JATO architecture and finally
   * should be reorganized into the API.
   *
   */
  ////public void handleProductDealIdPassToHtml(ViewBean pg, CSpDisplayEvent event, String staticFieldName, String doName, String dataFieldName)
  //--> Shouldn't execute the DataObject again for optimization
  //--> Modified by Billy 17July2003
  public void handleProductDealIdPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {
    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    //logger.debug("DEH@handleProductDealIdPassToHtml::currPg: " + currPg);
    //logger.debug("DEH@handleProductDealIdPassToHtml::ViewBean: " + pg);

    doDealEntryMainSelectModel theModel = (doDealEntryMainSelectModel)
        RequestManager.getRequestContext().getModelManager().getModel(doDealEntryMainSelectModel.class);

    int lenderProductId = 0;

    try
    {
      if(theModel.getSize() > 0)
      {
        //logger.debug("DEH@handleProductDealIdPassToHtml::Size of the result set: " + theModel.getSize());
				Object fieldValue = theModel.getDfProductId();
        //logger.debug("DEH@handleProductDealIdPassToHtml::ProductId: " + new Integer(fieldValue.toString()).intValue());
        lenderProductId = new Integer(fieldValue.toString()).intValue();
      }
    }
    catch(Exception e)
    {
      logger.error("DEH@handleProductDealIdPassToHtml::Exception: " + e);
      lenderProductId = 0;
    }

    String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

    //logger.debug("DEH@handleProductDealIdPassToHtml::fieldName: " + fieldName);
    //logger.debug("DEH@handleProductDealIdPassToHtml::fld: " + fld);

    fld.setValue((new Integer(lenderProductId)).toString());
  }

   /**
   *
   * This method is reorganized in accordancy with the JATO architecture
   *
   */
  ////public void handleDefaultProductPassToHtml(ViewBean pg, CSpDisplayEvent event, String staticFieldName, String doName, String dataFieldName)
  public void handleDefaultProductPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {

    ////SessionState theSession = getTheSession();

    ////PageEntry currPage = getTheSession().getCurrentPage();

    PageEntry currPg = (PageEntry) theSessionState.getCurrentPage();

    //logger.debug("DEH@handleDefaultProductPassToHtml::currPg: " + currPg);
    //logger.debug("DEH@handleDefaultProductPassToHtml::ViewBean: " + pg);

    doDealGetDefaultProductModel theModel = (doDealGetDefaultProductModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doDealGetDefaultProductModel.class);

    String defaultYes = new String("Y");

    int defaultProductId = 0;

    ////3. init the dynamic criteria using the deal id retrieved and
    ////   retrieve the actual value from the executed data object
    theModel.clearUserWhereCriteria();
		theModel.addUserWhereCriterion(doDealGetDefaultProductModel.FIELD_DFDEFAULTPRODUCT, "=", defaultYes);
		theModel.addUserWhereCriterion(doDealGetDefaultProductModel.FIELD_DFDEFAULTLENDER, "=", defaultYes);
		try {
      ResultSet rs = theModel.executeSelect(null);

      if(rs == null || theModel.getSize() < 0)
      {
        defaultProductId = 0;
      }

      else if(rs != null && theModel.getSize() > 0)
      {
        //logger.debug("DEH@handleDefaultProductPassToHtml::Size of the result set: " + theModel.getSize());

          while (theModel.next()) {
					Object fieldValue = theModel.getDfMtgProdId();
                //logger.debug("DEH@handleDefaultProductPassToHtml::DefaultProductId: " + new Integer(fieldValue.toString()).intValue());
                defaultProductId = new Integer(fieldValue.toString()).intValue();
              }
          }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@handleDefaultProductPassToHtml::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@handleDefaultProductPassToHtml::SQLException: " + sqle);
    }

    String fieldName = (String) event.getChildName();
    //logger.debug("DEH@handleDefaultProductPassToHtml::fieldName: " + fieldName);

    DisplayField fld = (DisplayField) pg.getChild(fieldName);
    //logger.debug("DEH@handleDefaultProductPassToHtml::fld: " + fld);

    fld.setValue((new Integer(defaultProductId)).toString());
  }

  /**
   * This method is reorganized in accordancy with the JATO architecture.
   *
   *
   */
  ////public void handleIsPageEntryPassToHtml(ViewBean pg, CSpDisplayEvent event, String staticFieldName)
  public void handleIsPageEntryPassToHtml(ViewBean pg, ChildDisplayEvent event)
  {
    ////SessionState theSession = getTheSession();

    ////PageEntry currPage = getTheSession().getCurrentPage();

    ////Obsolete ND fragment, no need now.
    //0. reset the static field.
    ////pg.setDisplayFieldValue(staticFieldName, new CSpNull());

    PageEntry currPage = theSessionState.getCurrentPage();

    //logger.debug("DEH@handleIsPageEntryPassToHtml:: start");

    //1. retrieve the actual value from the PageEntry
    String dealEntry = "";

    //logger.debug("DEH@handleIsPageEntryPassToHtml::DealPageMode: " + currPage.isDealEntry());

    if (currPage.isDealEntry() == true)
    {
      dealEntry = "Y";
    }
    else if (currPage.isDealEntry() == false)
    {
      dealEntry = "N";
    }

    //// Obsolete ND fragment is rewritten below.
    ////DisplayField field =(DisplayField) event.getSource();
    ////field.setValue(new String(dealEntry));

    String fieldName = (String) event.getChildName();
    DisplayField fld = (DisplayField) pg.getChild(fieldName);

    //logger.debug("---D---->DEH@handleIsPageEntryPassToHtml::fieldName: " + fieldName);
    //logger.debug("---D---->DEHhandleIsPageEntryPassToHtml@::fld: " + fld);

    fld.setValue(new String(dealEntry));

    //logger.debug("---D---->DEHhandleIsPageEntryPassToHtml@ setting dealEntry value to " + dealEntry);

  }

  /**
   *
   * This method is adjusted to the JATO framework.
   */

  protected void reduceAccessToComboBox(ViewBean page, String cbName, String cbValue, int rowindx)
  {
    try
    {
      logger.trace("@---T---->DealEntryHandler ---- restrict the list of the combobox for MI part: " + cbName);
      ////ComboBox combobox =(ComboBox) getCurrNDPage().getDisplayField(cbName);

      ComboBox combobox =(ComboBox) page.getDisplayField(cbName);
      OptionList optList = (OptionList) combobox.getOptions();
      //logger.debug("DEH@reduceAccessToComboBox::OptionTest: " + optList);

      //// Obsolete ND methods.
      ////combobox.removeAllChildren(true);
      ////combobox.setAutomaticallyReadData(false);
      ////combobox.addSelectable(cbValue, rowindx);
      ////combobox.select(new String(cbValue));
      ////combobox.select(rowindx);
      ////Object idValue =((CSpListItem) combobox.getSelected()).getValue();
      ////int id = com.iplanet.jato.util.TypeConverter.asInt(idValue);

      String [] labels = new String[1];
      String [] values = new String[1];

      //logger.debug("DEH@reduceAccessToComboBox::Label[0]: " + rowindx);
      //logger.debug("DEH@reduceAccessToComboBox::Value[0]: " + cbValue);

      values[0] = (new Integer(rowindx)).toString();
      labels[0] = cbValue;

      optList.setOptions(labels, values);

      // 1. Customize colors, borders, etc.,
      String changeColorStyle = new String("style=\"background-color:d1ebff\"" + " style=\"font-weight: bold\"");
      ////combobox.setExtraHtmlText(changeColorStyle);

      combobox.setExtraHtml(changeColorStyle);

      //// Obsolete ND method.
      ////getCurrNDPage().clearBeforeDisplay();
    }
    catch(Exception e)
    {
      setStandardFailMessage();
			logger.error("Exception DealEntryHandler@reduceAccessToComboBox: Problem encountered in combobox customization:"+ e); //#DG702
    }

  }

  /**
   *
   * BXTODO: Obsolete method. JATO takes care on it. Test more and eliminate
   * during the final cleaning of the BX framework classes.
   */
  protected void fullAccessToComboBox(String cbName)
  {
    try
    {
      logger.trace("@---T---->DealEntryHandler ---- full access to the combobox for MI part: " + cbName);
      ComboBox combobox =(ComboBox) getCurrNDPage().getDisplayField(cbName);

      ////Obsolete ND method.
      ////combobox.doAutoFill();
      String defaultColorStyle = new String("style=\"background-color:ffffff\"");
      ////combobox.setExtraHtmlText(defaultColorStyle);

      combobox.setExtraHtml(defaultColorStyle);
      ////Obsolete ND method.
      ////getCurrNDPage().clearBeforeDisplay();
    }
    catch(Exception e)
    {
      setStandardFailMessage();
			logger.error("Exception DealEntryHandler@fullAccessToComboBox: Problem encountered in combobox customization:"+ e); //#DG702
    }

  }

  /**
   *
   *
   */

  ////public String getMIInfo(String doName, com.iplanet.jato.model.sql.QueryModelBase theDO, String dfName, int clmnidx)
  public String getMIDescription(doDealMIIndicatorModel theDO)
  {
    ////SessionState theSession = getTheSession();

    ////PageEntry pg = getTheSession().getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    String indicatorDescr = "";

    int dealId = pg.getPageDealId();

    Integer cspDealId = new Integer(dealId);

    ////int copyid = theSession.getCurrentPage().getPageDealCID();
    int copyid = pg.getPageDealCID();

    Integer cspDealCPId = new Integer(copyid);

    //logger.debug("DEH@getMIDescription::pageDealId: " + dealId + ", dealId: " + cspDealId + ", copyId: " + copyid);

    ////theDO =(com.iplanet.jato.model.sql.QueryModelBase) CSpider.getDataObject(doName);

    theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(doDealMIIndicatorModel.FIELD_DFDEALID, "=", cspDealId);
		theDO.addUserWhereCriterion(doDealMIIndicatorModel.FIELD_DFCOPYID, "=", cspDealCPId);

    theDO.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    theDO.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    try
    {
      ResultSet rs = theDO.executeSelect(null);

      if(rs == null || theDO.getSize() < 0)
      {
        indicatorDescr = "0";
      }

      else if(rs != null && theDO.getSize() > 0)
      {
        //logger.debug("DEH@getMIDescription::Size of the result set: " + theDO.getSize());

          while (theDO.next()) {
					Object oIndicatorDescr = theDO.getDfMIIndicatorDescription();
					if (oIndicatorDescr != null) {
                  indicatorDescr = oIndicatorDescr.toString();
                  //logger.debug("DEH@getMIDescription::oIndicatorDescr: " + oIndicatorDescr.toString());
                }
                else
                {
                  indicatorDescr = "";
                }
              }
          }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@getMIDescription::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@getMIDescription::SQLException: " + sqle);
    }

    return indicatorDescr;

  }
    ////Obsolete ND fragment.
    ////if (theDO.succeeded())
    ////{
    ////	int noofrows = theDO.getLastResults().getNumRows();
    ////	if (noofrows <= 0)
    ////	{
    ////		value = "0";
    ////	}
    ////	if (noofrows > 0)
    ////	{
    ////		CSpDBResultTable instRslt = theDO.getLastResults().getResultTable();
    ////		value = instRslt.getValue(0, clmnidx).toString();
    ////	}
    ////}


  /**
   *
   *
   */
  ////public int getMIIndex(String doName, com.iplanet.jato.model.sql.QueryModelBase theDO, String dfName, int clmnidx)
  public int getMIIndicator(doDealMIIndicatorModel theDO)
  {
    ////SessionState theSession = getTheSession();

    ////PageEntry pg = getTheSession().getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    int indx = 0;

    int dealId = pg.getPageDealId();
    Integer cspDealId = new Integer(dealId);
    ////int copyid = theSession.getCurrentPage().getPageDealCID();
    int copyid = pg.getPageDealCID();
    Integer cspDealCPId = new Integer(copyid);
    //logger.debug("DEH@getMIIndicator::pageDealId: " + dealId + ", dealId: " + cspDealId + ", copyId: " + copyid);
    ////theDO =(com.iplanet.jato.model.sql.QueryModelBase) CSpider.getDataObject(doName);
    theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(doDealMIIndicatorModel.FIELD_DFDEALID, "=", cspDealId);
		theDO.addUserWhereCriterion(doDealMIIndicatorModel.FIELD_DFCOPYID, "=", cspDealCPId);
		try {
      ResultSet rs = theDO.executeSelect(null);

			if (rs == null || theDO.getSize() < 0) {
        indx = 0;
      }

			else if (rs != null && theDO.getSize() > 0) {
        //logger.debug("DEH@getMIIndicator::Size of the result set: " + theDO.getSize());

          while (theDO.next()) {
					Object mIIndicator = theDO.getDfMIIndicatorId();
					if (mIIndicator != null) {
                  //logger.debug("DEH@getMIIndicator::miIndicator: " + new Integer(mIIndicator.toString()).intValue());
                  indx = new Integer(mIIndicator.toString()).intValue();
                }
                else
                {
                  indx = 0;
                }
              }
          }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@getMIIndicator::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@getMIIndicator::SQLException: " + sqle);
    }

    //// Original ND version.
    ////theDO.execute();

    ////if (theDO.succeeded())
    ////{
    ////	int noofrows = theDO.getLastResults().getNumRows();
    ////	if (noofrows <= 0)
    ////	{
    ////		indx = 0;
    ////	}
    ////	if (noofrows > 0)
    ////	{
    ////		CSpDBResultTable instRslt = theDO.getLastResults().getResultTable();
    ////		indx = instRslt.getValue(0,com.iplanet.jato.util.TypeConverter.asInt(clmnidx));
    ////	}
    ////}

    return indx;

  }

  /**
   *
   *
   */
	public String getMITypeDescription(doDealMITypeModel theDO) {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    String typeDescr = "";
    int dealId = pg.getPageDealId();
    Integer cspDealId = new Integer(dealId);
    int copyid = pg.getPageDealCID();
    Integer cspDealCPId = new Integer(copyid);
    //logger.debug("DEH@getMITypeDescription::pageDealId: " + dealId + ", dealId: " + cspDealId + ", copyId: " + copyid);
    theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(doDealMITypeModel.FIELD_DFDEALID, "=", cspDealId);
		theDO.addUserWhereCriterion(doDealMITypeModel.FIELD_DFCOPYID, "=", cspDealCPId);
		try {
      ResultSet rs = theDO.executeSelect(null);

      if(rs == null || theDO.getSize() < 0)
      {
        typeDescr = "";
      }

      else if(rs != null && theDO.getSize() > 0)
      {
        //logger.debug("DEH@getMITypeDescription::Size of the result set: " + theDO.getSize());

          while (theDO.next()) {
					Object oTypeDescr = theDO.getDfMIMortgageTypeDescription();
                if (oTypeDescr == null) {
                  typeDescr = "";
                } else if (oTypeDescr != null) {
                //logger.debug("DEH@getMITypeDescription::TypeDescr: " + oTypeDescr.toString());
                typeDescr = oTypeDescr.toString();
                }
              }
          }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@getMITypeDescription::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@getMITypeDescription::SQLException: " + sqle);
    }

    return typeDescr;

  }

  /**
   *
   *
   */
	public String getMInsurerDescription(doDealMIInsurerModel theDO) {
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    String insurerDescr = "";
    int dealId = pg.getPageDealId();
    Integer cspDealId = new Integer(dealId);
    int copyid = pg.getPageDealCID();
    Integer cspDealCPId = new Integer(copyid);
    //logger.debug("DEH@getMInsurerDescription::pageDealId: " + dealId + ", dealId: " + cspDealId + ", copyId: " + copyid);
    theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(doDealMITypeModel.FIELD_DFDEALID, "=", cspDealId);
		theDO.addUserWhereCriterion(doDealMITypeModel.FIELD_DFCOPYID, "=", cspDealCPId);

    theDO.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    theDO.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    try
    {
      ResultSet rs = theDO.executeSelect(null);

      if(rs == null || theDO.getSize() < 0)
      {
        insurerDescr = "0";
      }

      else if(rs != null && theDO.getSize() > 0)
      {
        //logger.debug("DEH@getMInsurerDescription::Size of the result set: " + theDO.getSize());

          while (theDO.next()) {
					Object oInsurerDescr = theDO.getDfMIInsurerUserName();
					if (oInsurerDescr != null) {
                  //logger.debug("DEH@getMITypeDescription::TypeDescr: " + oInsurerDescr.toString());
                  insurerDescr = oInsurerDescr.toString();
                }
                else
                  insurerDescr = "";
              }
          }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@getMInsurerDescription::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@getMInsurerDescription::SQLException: " + sqle);
    }

    return insurerDescr;

  }

  /**
   *
   *
   */
  public int getMITypeIndex(doDealMITypeModel theDO)
  {

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    int indx = 0;

    int dealId = pg.getPageDealId();

    Integer cspDealId = new Integer(dealId);

    int copyid = pg.getPageDealCID();

    Integer cspDealCPId = new Integer(copyid);

    //logger.debug("DEH@getMITypeIndex::pageDealId: " + dealId + ", dealId: " + cspDealId + ", copyId: " + copyid);

    theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(doDealMITypeModel.FIELD_DFDEALID, "=", cspDealId);
		theDO.addUserWhereCriterion(doDealMITypeModel.FIELD_DFCOPYID, "=", cspDealCPId);
		try {
      ResultSet rs = theDO.executeSelect(null);

      if(rs == null || theDO.getSize() < 0)
      {
        indx = 0;
      }

      else if(rs != null && theDO.getSize() > 0)
      {
        //logger.debug("DEH@getMITypeIndex::Size of the result set: " + theDO.getSize());

          while (theDO.next()) {
					Object mIType = theDO.getDfMITypeId();
					if (mIType != null) {
                  //logger.debug("DEH@getMITypeIndex::TypeIndex: " + new Integer(mIType.toString()).intValue());
                  indx = new Integer(mIType.toString()).intValue();
                }
                else
                {
                  indx = 0;
                }
              }
          }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@getMITypeIndex::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@getMITypeIndex::SQLException: " + sqle);
    }

    return indx;
  }

  /**
   *
   *
   */
  public int getMInsurerIndex(doDealMIInsurerModel theDO)
  {

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    int indx = 0;

    int dealId = pg.getPageDealId();

    Integer cspDealId = new Integer(dealId);

    int copyid = pg.getPageDealCID();

    Integer cspDealCPId = new Integer(copyid);

    //logger.debug("DEH@getMIInsurerIndex::pageDealId: " + dealId + ", dealId: " + cspDealId + ", copyId: " + copyid);

    theDO.clearUserWhereCriteria();
		theDO.addUserWhereCriterion(doDealMIInsurerModel.FIELD_DFDEALID, "=", cspDealId);
		theDO.addUserWhereCriterion(doDealMIInsurerModel.FIELD_DFCOPYID, "=", cspDealCPId);

    theDO.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    theDO.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    try
    {
      ResultSet rs = theDO.executeSelect(null);

      if(rs == null || theDO.getSize() < 0)
      {
        indx = 0;
      }

      else if(rs != null && theDO.getSize() > 0)
      {
        //logger.debug("DEH@getMIInsurerIndex::Size of the result set: " + theDO.getSize());

          while (theDO.next()) {
					Object mInsurer = theDO.getDfMIInsurerId();
					if (mInsurer != null) {
                  //logger.debug("DEH@getMIInsurerIndex::InsurerIndex: " + new Integer(mInsurer.toString()).intValue());
                  indx = new Integer(mInsurer.toString()).intValue();
                }
                else
                {
                  indx = 0;
                }
              }
          }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@getMIInsurerIndex::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@getMIInsurerIndex::SQLException: " + sqle);
    }

    return indx;
  }

	/** #DG702 not used
   *
   *
   */
  /*
	private void displayDSSConditional(PageEntry pg, ViewBean thePage) {
    String pageTitle = "Deal Entry";
		//#DG702 int dealId = pg.getPageDealId();
		//#DG702 Integer cspDealId = new Integer(dealId);
    // suppress
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STPAGETITLE, new String(pageTitle));
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STINCLUDEDSSSTART, new String(supresTagStart));
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STINCLUDEDSSEND, new String(supresTagEnd));
    return;
	} */

  /**
   *
   *
   */
  private void displayCommitmentSectionConditional(PageEntry pg, ViewBean thePage)
  {
    //String suppressStart = "<!--";

    //String suppressEnd = "-->";

    int dealId = pg.getPageDealId();

		//#DG702 Integer cspDealId = new Integer(dealId);
    // suppress
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STINCLUDECOMMITMENTSTART, new String(supresTagStart));
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STINCLUDECOMMITMENTEND, new String(supresTagEnd));

    return;

  }


  /**
   *
   * BXTODO: Finish this!!!!
   */
  protected void fullAccessToComboBox(ViewBean page, String cbName, String cbValue, int rowindx)
  {
    try
    {
      logger.trace("@---T---->DealEntryHandler ---- full access to the combobox for MI part: " + cbName);
      ComboBox combobox =(ComboBox) getCurrNDPage().getDisplayField(cbName);

      ////Obsolete ND methods.
      ////combobox.doAutoFill();
      ////combobox.select(new String(cbValue));
      ////combobox.select(rowindx);

      //// Non-converted element by the auto-phase.
      ////Object idValue =((CSpListItem) combobox.getSelected()).getValue();

      //// temp commented out.
      ////Object idValue = combobox.getDisplayFieldValue(cbName);
      ////int id = com.iplanet.jato.util.TypeConverter.asInt(idValue);

      String defaultColorStyle = new String("style=\"background-color:ffffff\"");
      ////combobox.setExtraHtmlText(defaultColorStyle);
      combobox.setValue(defaultColorStyle);
    }
    catch(Exception e)
    {
      setStandardFailMessage();
			logger.error("Exception DealEntryHandler@fullAccessToComboBox: Problem encountered in combobox customization:"+ e); //#DG702
    }

  }

  /**
   *
   *
   */

  private void executeCretarioForMainDealEntry(SysLogger logger, Integer cspDealId, Integer cspDealCPId)
  {
    doDealEntryMainSelectModel dodealentrymain =
                          (doDealEntryMainSelectModel) RequestManager.getRequestContext().getModelManager().getModel(doDealEntryMainSelectModel.class);

    dodealentrymain.clearUserWhereCriteria();
		dodealentrymain.addUserWhereCriterion(doDealEntryMainSelectModel.FIELD_DFDEALID, "=", cspDealId);
		dodealentrymain.addUserWhereCriterion(doDealEntryMainSelectModel.FIELD_DFCOPYID, "=", cspDealCPId);
    executeModel(dodealentrymain, "UWH@@executeCretarioForUWMainDeal", true);

    setDealMainDisplayFields(dodealentrymain, 0);

    doDealNotesInfoModel theDO =
                      (doDealNotesInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doDealNotesInfoModel.class);


    theDO.clearUserWhereCriteria();
	theDO.addUserWhereCriterion(doDealNotesInfoModel.FIELD_DFDEALID, "=", cspDealId);

	//fxp23512 - add language filter to dealnotes model instance
	final String langStr = "("+ theSessionState.getLanguageId()+ ", "+ LANGUAGE_UNKNOW + ")";
    theDO.addUserWhereCriterion("dfLanguagePreferenceID", "in", langStr);
  }

  /**
   *
   *
   */
  private boolean executeCretarioForRateLock(Integer dealId, Integer cspDealCPId)
  {
    doDealRateLockModel doIsRateLocked =
                      (doDealRateLockModel) RequestManager.getRequestContext().getModelManager().getModel(doDealRateLockModel.class);

    doIsRateLocked.clearUserWhereCriteria();
		doIsRateLocked.addUserWhereCriterion("dfDealId", "=", dealId);
		doIsRateLocked.addUserWhereCriterion("dfCopyId", "=", cspDealCPId);
		try {
      ResultSet rs = doIsRateLocked.executeSelect(null);

      if(rs == null || doIsRateLocked.getSize() < 0)
      {
        return false;
      }

      else if(rs != null && doIsRateLocked.getSize() > 0)
      {
        //logger.debug("DEH@executeCretarioForRateLock::Size of the result set: " + doIsRateLocked.getSize());

          while (doIsRateLocked.next()) {
					Object isRateLocked = doIsRateLocked.getDfRateLock();
                //--> Bug fix by Billy 15Jan2004
                if (isRateLocked != null && isRateLocked.toString().equals("Y"))
                //==============================================================
                {
                    return true;
                }
                 else if (isRateLocked == null || (isRateLocked.toString()).equals("N")) {
                    return false;
                }
          }
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretarioForRateLock::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretarioForRateLock::SQLException: " + sqle);
    }

    return false;

  }



  /**
   *
   *
   */
  private void executeCretarioSourceInfo(Integer cspDealId, Integer cspDealCPId)
  {
    doDealEntrySourceInfoModel dodealsourceinfo =
                      (doDealEntrySourceInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doDealEntrySourceInfoModel.class);

    dodealsourceinfo.clearUserWhereCriteria();

    dodealsourceinfo.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    dodealsourceinfo.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    ////dodealsourceinfo.execute();
    try
    {
      ResultSet rs = dodealsourceinfo.executeSelect(null);

      if(rs == null || dodealsourceinfo.getSize() < 0)
      {
        //logger.debug("DEH@executeCretarioSourceInfo::No row returned!!");
      }

      else if(rs != null && dodealsourceinfo.getSize() > 0)
      {
        //logger.debug("DEH@executeCretarioSourceInfo::Size of the result set: " + dodealsourceinfo.getSize());
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretarioSourceInfo::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretarioSourceInfo::SQLException: " + sqle);
    }

  }

  /**
   *
   *
   */
  private void executeCretarioReferenceInfo(Integer cspDealId, Integer cspDealCPId)
  {
    doDealGetReferenceInfoModel dodealrefinfo =
                      (doDealGetReferenceInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doDealGetReferenceInfoModel.class);

    dodealrefinfo.clearUserWhereCriteria();

    dodealrefinfo.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    dodealrefinfo.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    ////dodealrefinfo.execute();
    try
    {
      ResultSet rs = dodealrefinfo.executeSelect(null);

      if(rs == null || dodealrefinfo.getSize() < 0)
      {
        //logger.debug("DEH@executeCretarioReferenceInfo::No row returned!!");
      }

      else if(rs != null && dodealrefinfo.getSize() > 0)
      {
        //logger.debug("DEH@executeCretarioReferenceInfo::Size of the result set: " + dodealrefinfo.getSize());
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretarioReferenceInfo::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretarioReferenceInfo::SQLException: " + sqle);
    }
  }

  //--DJ_PT_CR--start//
	private void executeCretarioForPartyInfo(Integer dealId)
	{
		///logger.debug("--D--> @executeCretarioForMainDeal: dealId = " +TypeConverter.asInt(dealId));

    doPartySummaryModel partyInfoModel = (doPartySummaryModel)
                                RequestManager.getRequestContext().getModelManager().getModel(doPartySummaryModel.class);

    partyInfoModel.clearUserWhereCriteria();
		partyInfoModel.addUserWhereCriterion(DFDEALID, "=", dealId);

    //// Display Party Info only for Origination Branch type (it is only one party type could be assigned to a deal.
    partyInfoModel.addUserWhereCriterion("dfPartyTypeId", "=", new Integer(Mc.PARTY_TYPE_ORIGINATION_BRANCH));

    this.executeModel(partyInfoModel, "DEH@executeCretarioForUWPartyInfo", false);
  }
  //--DJ_PT_CR--end//

  //--DJ_CR134--start--27May2004--//
	/**
	 *
	 *
	 */
  // New model introduced in order to escape overriding of its execution for different purposes.
  // Also Products wants to enhance the broker information further with other notesTypes.
	/* #DG702 replaced - clean up code
	 private void executeCretarioForDealNotesHomeBASEInfo(Integer dealId, Integer notesCategoryId)	 {
		doDealNotesBrokerInfoModel theDealNotes =
                          (doDealNotesBrokerInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doDealNotesBrokerInfoModel.class);
		theDealNotes.clearUserWhereCriteria();
		theDealNotes.addUserWhereCriterion("dfDealID", "=", dealId);
		theDealNotes.addUserWhereCriterion("dfDealNotesCategoryID", "=", notesCategoryId);

    this.executeModel(theDealNotes, "DEH@executeCretarioForDealNotes" , true);
	}
	 protected String getHomeBASEProductRatePmnt()	{
    doDealNotesBrokerInfoModel theDO = (doDealNotesBrokerInfoModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doDealNotesBrokerInfoModel.class);
		String notestext = "";
<<<<<<< .working

    Object mNotesText = theDO.getValue(theDO.FIELD_DFDEALNOTESTEXT);
    if(mNotesText != null) {

        // BEGIN FIX FOR FXP20810: ML 3.3 - Oracle.SQL text
        // Mohan V Premkumar
        // 03/19/2007

        try {
            notestext = DealEntity.readClob((Clob)mNotesText);
            logger.debug("DEH@getHomeBASEProductRatePmnt:: " + notestext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // END FIX FOR FXP20810: ML 3.3 - Oracle.SQL text
=======
		 Object mNotesText = theDO.getValue(theDO.FIELD_DFDEALNOTESTEXT);
		 if(mNotesText != null)    {
		 notestext = mNotesText.toString();
>>>>>>> .merge-right.r1681
    }
<<<<<<< .working
=======
		 logger.debug("DEH@getHomeBASEProductRatePmnt:: " + notestext);
>>>>>>> .merge-right.r1681
    return notestext;
	 }*/
  //--DJ_CR134--end--//

  //--DJ_CR_203.1--start--//
	private void populateDefaultValues(ViewBean thePage)
	{

    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";
		HtmlDisplayFieldBase df;

		// Trigger to set MI Premium display if MIType changed
		df = (HtmlDisplayFieldBase) thePage.getDisplayField("rbProprietairePlus");
		theExtraHtml = "onclick=\"SetLineOfCreditDisplay();\"";

    df.setExtraHtml(theExtraHtml);

  }
  //--DJ_CR_203.1--end--//

  /**
   * <p>setDealMainDisplayFields</p>
   * This method is totally redesigned (see detailed comment in the PHC class)
   *
   * @version 1.1 added JS to control check box enable/disable by miUpfront and taxPayor
   */
  private void setDealMainDisplayFields(doDealEntryMainSelectModel dobject, int rowNum)
  {
    //logger.debug("DEH@setDealMainDisplayFields::TestFieldName: " + test);

    ////setTermsDisplayField(dobject, "txAmortizationYears", "txAmortizationMonths", "dfAmortization");
		setTermsDisplayField(dobject, pgDealEntryViewBean.CHILD_TXAMORTIZATIONYEARS,
				pgDealEntryViewBean.CHILD_TXAMORTIZATIONMONTHS, doDealEntryMainSelectModel.FIELD_DFAMORTIZATION, rowNum);

		setTermsDisplayField(dobject, pgDealEntryViewBean.CHILD_STEFFECTIVEAMORTIZATIONYEARS,
				pgDealEntryViewBean.CHILD_STEFFECTIVEAMORTIZATIONMONTHS, doDealEntryMainSelectModel.FIELD_DFEFFECTIVEAMORTIZATION, rowNum);

		setTermsDisplayField(dobject, pgDealEntryViewBean.CHILD_TXPAYTERMYEARS,
				pgDealEntryViewBean.CHILD_TXPAYTERMMONTHS,doDealEntryMainSelectModel.FIELD_DFACTUALPAYMENTTERM, rowNum);

		fillupMonths(pgDealEntryViewBean.CHILD_CBESTCLOSINGDATEMONTHS);

		fillupMonths(pgDealEntryViewBean.CHILD_CBFIRSTPAYDATEMONTH);

		fillupMonths(pgDealEntryViewBean.CHILD_CBCOMMEXPECTEDDATEMONTH);

    // Added Refinance Section -- BILLY 07Feb2002
		fillupMonths(pgDealEntryViewBean.CHILD_CBREFIORIGPURCHASEDATEMONTH);

		setDateDisplayField(dobject, pgDealEntryViewBean.CHILD_CBESTCLOSINGDATEMONTHS,
				pgDealEntryViewBean.CHILD_TXESTCLOSINGDATEDAY, pgDealEntryViewBean.CHILD_TXESTCLOSINGDATEYEAR,
				doDealEntryMainSelectModel.FIELD_DFESTIMATEDCLOSINGDATE, rowNum);

		setDateDisplayField(dobject, pgDealEntryViewBean.CHILD_CBFIRSTPAYDATEMONTH,
				pgDealEntryViewBean.CHILD_TXFIRSTPAYDATEDAY,
				pgDealEntryViewBean.CHILD_TXFIRSTPAYDATEYEAR, doDealEntryMainSelectModel.FIELD_DFFIRSTPAYMENTDATE, rowNum);

		setDateDisplayField(dobject, pgDealEntryViewBean.CHILD_CBCOMMEXPECTEDDATEMONTH,
				pgDealEntryViewBean.CHILD_TXCOMMEXPECTEDDATEDAY, pgDealEntryViewBean.CHILD_TXCOMMEXPECTEDDATEYEAR,
				doDealEntryMainSelectModel.FIELD_DFRETURNDATE, rowNum);

    // Added Refinance Section -- BILLY 07Feb2002
		setDateDisplayField(dobject, pgDealEntryViewBean.CHILD_CBREFIORIGPURCHASEDATEMONTH,
				pgDealEntryViewBean.CHILD_TXREFIORIGPURCHASEDATEDAY, pgDealEntryViewBean.CHILD_TXREFIORIGPURCHASEDATEYEAR,
				doDealEntryMainSelectModel.FIELD_DFREFIORIGPURCHASEDATE, rowNum);

    // FFATE
        fillupMonths(pgDealEntryViewBean.CHILD_CBFINANCINGWAIVERDATEMONTH);
        setDateDisplayField(dobject, pgDealEntryViewBean.CHILD_CBFINANCINGWAIVERDATEMONTH,
                pgDealEntryViewBean.CHILD_TXFINANCINGWAIVERDATEDAY, pgDealEntryViewBean.CHILD_TXFINANCINGWAIVERDATEYEAR,
                doDealEntryMainSelectModel.FIELD_DFFINANCINGWAIVERDATE, rowNum);
        
    //CR03

		fillupMonths(pgDealEntryViewBean.CHILD_CBCOMMEXPIRATIONDATEMONTH);
		setDateDisplayField(dobject, pgDealEntryViewBean.CHILD_CBCOMMEXPIRATIONDATEMONTH,
				pgDealEntryViewBean.CHILD_TXCOMMEXPIRATIONDATEDAY, pgDealEntryViewBean.CHILD_TXCOMMEXPIRATIONDATEYEAR,
				doDealEntryMainSelectModel.FIELD_DFCOMMITMENTEXPIRATIONDATE, rowNum);

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    if(PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                                                 "com.basis100.calc.commitmentexpirydate.usecommitmentterm", "N").equals("Y")){
        getCurrNDPage().setDisplayFieldValue("hdMosProperty", "true");
    }

    try {
        CalcMonitor dcm = CalcMonitor.getMonitor(srk);
        Deal dealCalc;
        dealCalc = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), dcm);
        CommitmentExpiryDate commitExpiryDate = new CommitmentExpiryDate();
        commitExpiryDate.setResourceKit(srk);
        Date commitDate = commitExpiryDate.getCalcCommitmentExpiryDate(dealCalc);
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
       
        String dateString = "";
        if(commitDate != null)
        	dateString = formatter.format(commitDate);

			getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_HDCOMMITMENTEXPIRYDATE, dateString);
		}
		catch (Exception e) {
			logger.error(e); //#DG702
        e.printStackTrace();
    }
    //CR03

    //Purchase Plus Improvements start
    try {
	    Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

	    double improvedValue = deal.getTotalPurchasePrice() + deal.getRefiImprovementAmount();
			getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_TXIMPROVEDVALUE, new Double(improvedValue));

	    switch (deal.getDealPurposeId()) {
	        case Mc.DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS:
					getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_TXDEALPURPOSETYPEHIDDENSTART, "");
					getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_TXDEALPURPOSETYPEHIDDENEND, "");
	            break;
	        default:
					getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_TXDEALPURPOSETYPEHIDDENSTART,supresTagStart);
					getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_TXDEALPURPOSETYPEHIDDENEND, supresTagEnd);
	            break;
	    }
    }
		catch (Exception e) {
			logger.error("Exception @Deal Entry Handler setDealMainDisplayFields.:"+ e);
    }
    //Purchase Plus Improvements end
  }

  /**
   *
   *
   */
  private void executeCretariaForAppllicant(Integer cspDealId, Integer cspDealCPId)
  {
    doDealEntryApplicantSelectModel dodealentryapplicant =
                        (doDealEntryApplicantSelectModel) RequestManager.getRequestContext().getModelManager().getModel(doDealEntryApplicantSelectModel.class);

    dodealentryapplicant.clearUserWhereCriteria();

    dodealentryapplicant.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    dodealentryapplicant.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    ////dodealentryapplicant.execute();
    try
    {
      ResultSet rs = dodealentryapplicant.executeSelect(null);

      if(rs == null || dodealentryapplicant.getSize() < 0)
      {
        //logger.debug("DEH@executeCretariaForAppllicant::No row returned!!");
      }

      else if(rs != null && dodealentryapplicant.getSize() > 0)
      {
        //logger.debug("DEH@executeCretariaForAppllicant::Size of the result set: " + dodealentryapplicant.getSize());
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretariaForAppllicant::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretariaForAppllicant::SQLException: " + sqle);
    }

  }


  /**
   *
   *
   */
  private void executeCretariaForProperty(Integer cspDealId, Integer cspDealCPId)
  {
    doDealEntryPropertySelectModel dodealentryproperty =
                        (doDealEntryPropertySelectModel) RequestManager.getRequestContext().getModelManager().getModel(doDealEntryPropertySelectModel.class);

    dodealentryproperty.clearUserWhereCriteria();

    dodealentryproperty.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    dodealentryproperty.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    ////dodealentryproperty.execute();
    try
    {
      ResultSet rs = dodealentryproperty.executeSelect(null);

      if(rs == null || dodealentryproperty.getSize() < 0)
      {
        //logger.debug("DEH@executeCretariaForProperty::No row returned!!");
      }

      else if(rs != null && dodealentryproperty.getSize() > 0)
      {
        //logger.debug("DEH@executeCretariaForProperty::Size of the result set: " + dodealentryproperty.getSize());
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretariaForProperty::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretariaForProperty::SQLException: " + sqle);
    }

  }

	//***** Change by NBC/PP Implementation Team - GCD - Start *****//
    //GCD Summary data retrieval Begin
  /**
	 *
	 *
	 */
	private void executeCretarioForAdjudicationResponseBNCModel(Integer dealId, Integer cspDealCPId)
	{
		doAdjudicationResponseBNCModel doARBNCDo =
                          (doAdjudicationResponseBNCModel) RequestManager.getRequestContext().getModelManager().getModel(doAdjudicationResponseBNCModel.class);

		doARBNCDo.clearUserWhereCriteria();
		doARBNCDo.addUserWhereCriterion(DFDEALID, "=", dealId);
		doARBNCDo.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    this.executeModel(doARBNCDo, "UWH@executeCretarioFordoAdjudicationResponseBNCModel", false);
	}

	//GCD Summary Data Retrieval End
	//***** Change by NBC/PP Implementation Team - GCD - End *****//


  /**
   *
   *
   */
  private void executeCretariaForDownPayment(Integer cspDealId, Integer cspDealCPId)
  {
        doDealEntryDownPaymentModel dodealentrydownpay =
                  (doDealEntryDownPaymentModel) RequestManager.getRequestContext().getModelManager().getModel(doDealEntryDownPaymentModel.class);
        dodealentrydownpay.clearUserWhereCriteria();
        dodealentrydownpay.addUserWhereCriterion(DFDEALID, "=", cspDealId);
        dodealentrydownpay.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);
        ////dodealentrydownpay.execute();

        try
        {
          ResultSet rs = dodealentrydownpay.executeSelect(null);

          if(rs == null || dodealentrydownpay.getSize() < 0)
          {
            //logger.debug("DEH@executeCretariaForDownPayment::No row returned!!");
          }

          else if(rs != null && dodealentrydownpay.getSize() > 0)
          {
            //logger.debug("DEH@executeCretariaForDownPayment::Size of the result set: " + dodealentrydownpay.getSize());
          }
        }
        catch (ModelControlException mce)
        {
          logger.warning("DEH@executeCretariaForDownPayment::MCEException: " + mce);
        }

        catch (SQLException sqle)
        {
          logger.warning("DEH@executeCretariaForDownPayment::SQLException: " + sqle);
        }
  }


  /**
   *
   *
   */
  private void executeCretarioForBridgeLoan(Integer cspDealId, Integer cspDealCPId)
  {
      doDealEntryBridgeModel dodealentrybridge =
                      (doDealEntryBridgeModel) RequestManager.getRequestContext().getModelManager().getModel(doDealEntryBridgeModel.class);
      dodealentrybridge.clearUserWhereCriteria();
      dodealentrybridge.addUserWhereCriterion(DFDEALID, "=", cspDealId);
      dodealentrybridge.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);
      ////dodealentrybridge.execute();
      try
      {
        ResultSet rs = dodealentrybridge.executeSelect(null);

        if(rs == null || dodealentrybridge.getSize() < 0)
        {
          //logger.debug("DEH@executeCretarioForBridgeLoan::No row returned!!");
        }

        else if(rs != null && dodealentrybridge.getSize() > 0)
        {
          //logger.debug("DEH@executeCretarioForBridgeLoan::Size of the result set: " + dodealentrybridge.getSize());
        }
      }
      catch (ModelControlException mce)
      {
        logger.warning("DEH@executeCretarioForBridgeLoan::MCEException: " + mce);
      }

      catch (SQLException sqle)
      {
        logger.warning("DEH@executeCretarioForBridgeLoan::SQLException: " + sqle);
      }

  }

  //
  // Result:
  //
  //	true  - MI values (e.g. MI Indicator, MI Insurer, etc) may be changed by user
  //  false - MI values cannot be changed,
  //
  private boolean executeCretarioForMIStatus(Integer dealId, Integer cspDealCPId)
  {
    doDealMIStatusModel doMIStatusObj =
                        (doDealMIStatusModel) RequestManager.getRequestContext().getModelManager().getModel(doDealMIStatusModel.class);

    doMIStatusObj.clearUserWhereCriteria();

    doMIStatusObj.addUserWhereCriterion(DFDEALID, "=", dealId);

    doMIStatusObj.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);
    try
    {
      ResultSet rs = doMIStatusObj.executeSelect(null);

      if(rs == null || doMIStatusObj.getSize() < 0)
      {
        return false;
      }

      else if(rs != null && doMIStatusObj.getSize() > 0)
      {
        //logger.debug("DEH@executeCretarioForMIStatus::Size of the result set: " + doMIStatusObj.getSize());

          while (doMIStatusObj.next()) {
                Object isMIStatusUpdateAllowed = doMIStatusObj.getValue(doDealMIStatusModel.FIELD_DFMISTATUSUPDATEALLOWED);
                if ((isMIStatusUpdateAllowed.toString()).equals("Y")) {
                    return true;
                }
                else if(isMIStatusUpdateAllowed == null || (isMIStatusUpdateAllowed.toString()).equals("N"))
                {
                    return false;
                }
          }
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretarioForMIStatus::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretarioForMIStatus::SQLException: " + sqle);
    }

    return false;
  }

  /**
   *
   *
   */
  private boolean executeCretarioForDealOrigination(Integer dealId, Integer cspDealCPId)
  {
    doDealOriginationModel doOrigin =
                          (doDealOriginationModel) RequestManager.getRequestContext().getModelManager().getModel(doDealOriginationModel.class);

    doOrigin.clearUserWhereCriteria();
    doOrigin.addUserWhereCriterion(DFDEALID, "=", dealId);
    doOrigin.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    try
    {
      ResultSet rs = doOrigin.executeSelect(null);

      if(rs == null || doOrigin.getSize() < 0)
      {
        return false;
      }

      else if(rs != null && doOrigin.getSize() > 0)
      {
          while (doOrigin.next()) {
                Object channelMedia = doOrigin.getValue(doDealOriginationModel.FIELD_DFCHANNELMEDIA);
                //--Ticket#XXXX--09Feb2005--start--//
                if ((channelMedia.toString()).equals("E")) {
                    return true;
                }
                else if (channelMedia == null || !(channelMedia.toString()).equals("E")) {
                    return false;
                }
                //--Ticket#XXXX--09Feb2005--end--//
          }
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretarioForDealOrigination::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretarioForDealOrigination::SQLException: " + sqle);
    }
    return false;
  }

    //// Original ND version.
    ////if (doOrigin.succeeded() == false) return false;
    ////int noofrows = doOrigin.getLastResults().getNumRows();
    ////if (noofrows <= 0) return false;
    ////CSpDBResultTable instRslt = doOrigin.getLastResults().getResultTable();
    ////if (instRslt.getValue(0, 2).toString().equals("E")) return true;


  /**
   * Looks like there is no need in this method now.
   * Test/think more.
   *
   */
  /***
  public int getRowDisplaySetting(String dobject)
  {
    try
    {
      ////PageEntry pg = theSession.getCurrentPage();

      PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

      Integer cspDealId = new Integer(pg.getPageDealId());
      Integer cspDealCPId = new Integer(pg.getPageDealCID());
      com.iplanet.jato.model.sql.QueryModelBase chkDoObject =(com.iplanet.jato.model.sql.QueryModelBase) CSpider.getDataObject(dobject);
      chkDoObject.clearUserWhereCriteria();
      chkDoObject.addUserWhereCriterion(DFDEALID, "=", cspDealId);
      chkDoObject.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);
      chkDoObject.execute();
      int noofrows = chkDoObject.getLastResults().getNumRows();

      if (noofrows > 0)
      {
        ////return CSpPage.PROCEED;

      }

      ////return CSpPage.SKIP;

    }
    catch(Exception e)
    {
      logger.error("Exception @getRowDisplayString ");
      logger.error(e);
      ////return CSpPage.SKIP;
                        throw new CompleteRequestException();
    }

  }
  **/

  /**
   *
   *
   */
  private void executeCretarioForEscrowPayment(Integer cspDealId, Integer cspDealCPId)
  {
    doDealEntryEscrowModel dodealentryescrowpay =
                          (doDealEntryEscrowModel) RequestManager.getRequestContext().getModelManager().getModel(doDealEntryEscrowModel.class);

    dodealentryescrowpay.clearUserWhereCriteria();

    dodealentryescrowpay.addUserWhereCriterion(DFDEALID, "=", cspDealId);

    dodealentryescrowpay.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    ////dodealentryescrowpay.execute();
    try
    {
      ResultSet rs = dodealentryescrowpay.executeSelect(null);

      if(rs == null || dodealentryescrowpay.getSize() < 0)
      {
        //logger.debug("DEH@executeCretarioForEscrowPayment::No row returned!!");
      }

      else if(rs != null && dodealentryescrowpay.getSize() > 0)
      {
        //logger.debug("DEH@executeCretarioForEscrowPayment::Size of the result set: " + dodealentryescrowpay.getSize());
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretarioForEscrowPayment::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretarioForEscrowPayment::SQLException: " + sqle);
    }
  }


  /**
   *
   *
   */
  public String[] getDealKeys()
  {
    String [ ] dealkeys =
    {
      "hdDealId", "hdDealCopyId"
    }
    ;

    return dealkeys;

  }


  /**
   * <p>SaveEntity</p>
   * <p>save data from page</p>
   *
   * @pram webPage Viewbean
   *
   * @version 1.1 (july 29, 2008) MCM Team: added saveCompInfoCheckBox before calc
   */
  public void SaveEntity(ViewBean webPage)
    throws Exception
  {
    logger.trace("--T--> @DealEntryHandler.SaveEntity");

    CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

    String [ ] dealkeys = getDealKeys();
    
    //Qualify Rate
    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    HashSet skipElements = pg.getPageExcludePageVars();
    String qualifyFlag = (String) webPage
            .getDisplayFieldValue(pgDealEntryViewBean.CHILD_CHQUALIFYRATEOVERRIDE);
    
    String qualifyRateFlag =  (String) webPage
        .getDisplayFieldValue(pgDealEntryViewBean.CHILD_CHQUALIFYRATEOVERRIDERATE);
    
/*    if (qualifyFlag != null && "N".equalsIgnoreCase(qualifyFlag))
    	skipElements.add(new String(
                pgDealEntryViewBean.CHILD_CBQUALIFYPRODUCTTYPE));
*/
    Object qualifyRateValue =  webPage
            .getDisplayFieldValue(pgDealEntryViewBean.CHILD_TXQUALIFYRATE);
    
   	if ((qualifyRateFlag != null && "N".equalsIgnoreCase(qualifyRateFlag)) || qualifyRateValue == null)
   		skipElements.add(new String(
   				pgDealEntryViewBean.CHILD_TXQUALIFYRATE));

    if (qualifyFlag != null && "N".equalsIgnoreCase(qualifyFlag))
    {
		try 
        {
                Deal _deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
               
               Map defaultQualify = QualifyingRateHelper.getDefaultQualify(getSessionResourceKit(), _deal);
		    for (Iterator i = defaultQualify.keySet().iterator(); i.hasNext(); ) 
		    {
                    Integer qualifyProduct = (Integer) i.next();
                    Integer qualifyRate = (Integer) defaultQualify.get(qualifyProduct);
                    getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STHDQUALIFYPRODUCTID, qualifyProduct);
                    //getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_CBPRODUCTTYPE, _deal.getMtgProdId()+"");
                    skipElements.remove(new String(
                            pgDealEntryViewBean.CHILD_CBQUALIFYPRODUCTTYPE));
                    break;
                }
                
		} 
		catch (Exception e1) 
		{
                logger.error(e1.getMessage());
                logger.error(e1);
            }
       }
    
    //FXP33510  v5.0_XpS - Dual Qualifying Rate - Qualifying Product was changed with different LTV - begin
    if (qualifyRateValue != null && "Y".equalsIgnoreCase(qualifyRateFlag))
    {
    	skipElements.remove(new String(
                pgUWorksheetViewBean.CHILD_CBQUALIFYPRODUCTTYPE));
    	skipElements.remove(new String(
                pgUWorksheetViewBean.CHILD_TXQUALIFYRATE));
    }
    //FXP33510  v5.0_XpS - Dual Qualifying Rate - Qualifying Product was changed with different LTV - end
    logger.debug("UWH@SkipElementSize: " + skipElements.size());
    pg.setPageExcludePageVars(skipElements);
    try
    {
      handleUpdateDeal(DEAL_PROPS_FILE_NAME, dealkeys, dcm);
      setBranchId(dcm);

      //Leo
    }
    catch(Exception ex)
    {
      logger.error("Exception occurred @updateDeal of DealEntryHandler " + ex.toString());
      throw new Exception("Exception @SaveEntity");
    }

    try
    {
      String [ ] downpaykeys =
      {
        ////"RepeatedDownPayment.hdDownPaymentSourceId", "RepeatedDownPayment.hdDownPaymentCopyId"
        //// Important JATO container idiom to read the nested child. Should be forward slash
        //// except the ND's dot.
        "RepeatedDownPayment/hdDownPaymentSourceId", "RepeatedDownPayment/hdDownPaymentCopyId"
      };

      handleUpdateDownPay(DOWNPAY_PROP_FILE_NAME, downpaykeys, dcm);
    }
    catch(Exception ex)
    {
      logger.error("Exception occurred @updateDownPay of DealEntryHandler " + ex.toString());
      throw new Exception("Exception @SaveEntity");
    }

    try
    {
      String [ ] escrowkeys =
      {
        ////"RepeatedEscrowDetails.hdEscrowPaymentId", "RepeatedEscrowDetails.hdEscrowCopyId"
        //// Important JATO container idiom to read the nested child. Should be forward slash
        //// except the ND's dot.
        "RepeatedEscrowDetails/hdEscrowPaymentId", "RepeatedEscrowDetails/hdEscrowCopyId"
      };

      handleUpdateEscrowPay(ESCROWPAY_PROP_FILE_NAME, escrowkeys, dcm);
    }
    catch(Exception ex)
    {
      logger.error("Exception occurred @updateEscrow of DealEntryHandler " + ex.toString());
      throw new Exception("Exception @SaveEntity");
    }

    // MCM XS 2.60
    saveCompInfoCheckBox(pgDealEntryViewBean.CHILD_STCOMPONENTINFOPAGELET, dcm);

    try
    {
      doCalculation(dcm);
    }
		catch (Exception e) {
			logger.error("Exception occurred @doCalculation:" + e); //#DG702
			throw new Exception("Exception @SaveEntity", e);
    }

    saveApplicantPrimaryFlag();

    savePropertyPrimaryFlag();

  }


  /**
   *
   *
   */
  private void setBranchId(CalcMonitor dcm)
    throws Exception
  {
    ////PageEntry pe = getTheSession().getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    SessionResourceKit srk = getSessionResourceKit();

    ////Deal deal = DBA.getDeal(srk, pe.getPageDealId(), pe.getPageDealCID(), dcm);
    Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), dcm);

		Object branchId = getCurrNDPage().getDisplayFieldValue(pgDealEntryViewBean.CHILD_CBBRANCH);
		if (com.iplanet.jato.util.TypeConverter.asInt(branchId) > 0) {
        deal.setBranchProfileId(com.iplanet.jato.util.TypeConverter.asInt(branchId));
        deal.ejbStore();
    }
  }


  /**
   *
   * This method must is adjusted in accordancy with the JATO TiledView
   * access/manipulation methods.
   */

  private void saveApplicantPrimaryFlag()
  {
	  try
	  {
		  int rowcnt = getTotalNoOfRowsForThisPageVar("RepeatApplicant/hdApplicantId");
		  int applicantid = 0;
		  int copyid = 0;
		  String primaryflag = "N";
		  if (rowcnt < 0) return;
		  for(int i = 0;
		  i < rowcnt;
		  i ++)
		  {

			  TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(pgDealEntryViewBean.CHILD_REPEATAPPLICANT);

			  tiledView.setTileIndex(i);
			  applicantid = parseStringToInt(""+ (tiledView.getDisplayFieldValue(pgDealEntryRepeatApplicantTiledView.CHILD_HDAPPLICANTID)).toString());
			  copyid = parseStringToInt(""+ (tiledView.getDisplayFieldValue(pgDealEntryRepeatApplicantTiledView.CHILD_HDAPPLICANTCOPYID)).toString());
			  primaryflag = ""+ (tiledView.getDisplayFieldValue(pgDealEntryRepeatApplicantTiledView.CHILD_CBPRIMARYAPPLICANT)).toString();

			  //4.4 SimpleEntityCache - stopped using direct update sql
			  Borrower borrower = new Borrower(srk, null,applicantid, copyid);
			  borrower.setPrimaryBorrowerFlag(primaryflag);
			  borrower.ejbStore();

		  }
	  }
	  catch(Exception e)
	  {
		  logger.error("Exception @DealEntryHandler.saveApplicantPrimaryFlag - ignored");
		  logger.error(e);
	  }

  }

  /**
   * This method must is adjusted in accordancy with the JATO TiledView
   * access/manipulation methods.
   *
   */

  private void savePropertyPrimaryFlag()
  {
	  try
	  {
		  int rowcnt = getTotalNoOfRowsForThisPageVar("RepeatPropertyInformation/hdPropertyId");
		  int propertyid = 0;
		  int copyid = 0;
		  String primaryflag = "N";
		  if (rowcnt < 0) return;
		  for(int i = 0;
		  i < rowcnt;
		  i ++)
		  {

			  TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild("RepeatPropertyInformation");

			  tiledView.setTileIndex(i);
			  propertyid = parseStringToInt(""+ (tiledView.getDisplayFieldValue(pgDealEntryRepeatPropertyInformationTiledView.CHILD_HDPROPERTYID)).toString());
			  copyid = parseStringToInt(""+ (tiledView.getDisplayFieldValue(pgDealEntryRepeatPropertyInformationTiledView.CHILD_HDPROPERTYCOPYID)).toString());
			  primaryflag = ""+ (tiledView.getDisplayFieldValue(pgDealEntryRepeatPropertyInformationTiledView.CHILD_CBPRIMARYPROPERTY)).toString();

			  //4.4 SimpleEntityCache - stopped using direct update sql
			  Property property = new Property(srk, null, propertyid, copyid);
			  property.setPrimaryPropertyFlag(primaryflag);
			  property.ejbStore();
		  }
	  }
	  catch(Exception e)
	  {
		  logger.error("Exception @DealEntryHandler.savePropertyPrimaryFlag - ignored");
		  logger.error(e);
	  }

  }


  /**
   *
   *
   */
  public void handleSetBranch(DisplayField branchField)
  {
    int branchprofileid = 0;

    SessionResourceKit srk = getSessionResourceKit();

    ////PageEntry pg = getTheSession().getCurrentPage();

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    if (pg.isDealEntry() == false) return;

    try
    {
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), null);
      if (deal.getBranchProfileId() > 0)
      {
        branchprofileid = deal.getBranchProfileId();
      }
      else
      {
        branchprofileid = getUserBranchProfileId(srk, pg);
      }
      branchField.setValue(new Integer(branchprofileid));
    }
		catch (Exception e) {
			logger.error("@DealEntryHandler.handleSetBranch Exception at getting branchId:"+ e); //#DG702
    }

  }


  /**
   *
   *
   */
  public int getUserBranchProfileId(SessionResourceKit srk, PageEntry pg)
  {
    int branchid = - 1;

    int userid = pg.getPageMosUserId();

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      String sql = " SELECT BRANCHPROFILE.BRANCHPROFILEID" + " FROM BRANCHPROFILE, GROUPPROFILE,USERPROFILE" + " WHERE BRANCHPROFILE.BRANCHPROFILEID  =" + " GROUPPROFILE.BRANCHPROFILEID AND GROUPPROFILE.GROUPPROFILEID =" + " USERPROFILE.GROUPPROFILEID AND USERPROFILE.USERPROFILEID = " + userid;
      int key = jExec.execute(sql);
      while(jExec.next(key))
      {
        branchid = jExec.getInt(key, 1);
        break;
      }
      jExec.closeData(key);
    }
		catch (Exception e) {
			logger.error("@DealEntryHandler.getUserBranchProfileId Exception at getting branchId:" + e);		//#DG702
    }

    return branchid;

  }


	/**
	 * handleCreditDecisionPGButton
	 *  Setup and send user to the Credit Decision screen
	 *
	 *  @version <br>
	 *  Date: 10/17/2006 <br>
	 *  Author: GCD Implmentation Team<br>
	 *  Change:<br>
	 *  	Added calls to adopt transactional copy before loading Credit Decision screen
	 */
  	//***** Change by NBC/PP Implementation Team - GCD - Start *****//

	public void handleCreditDecisionPGButton(RequestInvocationEvent event) {
	  PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

	  SessionResourceKit srk = getSessionResourceKit();

	  SysLogger logger = srk.getSysLogger();

	  try
	  {
		srk.beginTransaction();

		// adopt transactional copy unconditionally - ensures we enter Credit Decision screen with saved
		// (non-discardable) scenario data
		standardAdoptTxCopy(pg, true);
		srk.commitTransaction();

		PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_CREDIT_DECISION, true);
		getSavedPages().setNextPage(pgEntry);
		navigateToNextPage(true);
	  }
     catch(Exception e)
     {
         srk.cleanTransaction();
         logger.error("Exception @handleCreditDecisionPGButton().");
         logger.error(e);
         setStandardFailMessage();
     }

	}

	//	***** Change by NBC/PP Implementation Team - GCD - End *****//


  /**
   *
   * 'Fake' method, nobody has used it.
   * BXTODO: delete it along with code clean up.
   */
  public void handleSetDefaultAmortization(DisplayField field)
  {
    Object val = field.getValue();

    //// chect this auto-conversion
    ////if (val != null) {
    ////ifTypeConverter.asInt((val) == 0)
    if(com.iplanet.jato.util.TypeConverter.asInt(val) != 0)
    {
          field.setValue(new Integer(25));
    }

    return;

  }


  /**
   *
   *
   */
  //--Release2.1--//
  //// This method (with changed signature has been moved to PHC class. No need
  //// in this one anymore.
  /**
  public String displayViewOnlyTag(DisplayField field)
  {

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

    if (pg.isEditable() == false)
    {
      ////field.setHtmlText("<IMG NAME=\"stViewOnlyTag\" SRC=\"/eNet_images/ViewOnly.gif\" ALIGN=TOP>");
      return "<IMG NAME=\"stViewOnlyTag\" SRC=\"/images/ViewOnly.gif\" ALIGN=TOP>";
    }
    else if (pg.isEditable() == false)
    {
      return "";
    }

    return "";
  }
  **/

  //Method to set display or not display the FinancingProgram filed based on the property definition

  private void setDisplayFinancingProgram(PageEntry pg, ViewBean thePage)
  {
    if ((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                                                   "com.basis100.deal.uw.displayfinancingprogram", "Y")).equals("Y"))
    {
      // Unset mask to display the field
			thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STINCLUDECOMMITMENTSTART, new String(""));
			thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STINCLUDECOMMITMENTEND, new String(""));
      return;
    }


    // Set Mask to make the filed not dispalyed
    //String suppressStart = "<!--";

    //String suppressEnd = "-->";


    // suppress
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STINCLUDEFINANCINGPROGRAMSTART, new String(supresTagStart));
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STINCLUDEFINANCINGPROGRAMEND, new String( supresTagEnd));
    return;
  }

	// SEAN GECF IV Document type dorp down: dide control.
	private void setHideDocumentType(PageEntry pg, ViewBean thePage) {

		String start = supresTagStart;
		String end = supresTagEnd;

		// get flag from configuration file, default is Y.
		String flag = PropertiesCache.getInstance().
			getProperty(theSessionState.getDealInstitutionId(),
                        "com.basis100.fxp.clientid.gecf", "N");
		if (flag.equalsIgnoreCase("Y")) {
			start = "";
			end = "";
		}

		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STHIDEIVDOCUMENTTYPESTART, start);
		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STHIDEIVDOCUMENTTYPEEND, end);
	}
	// END GECF IV Document type dorp down

  // Added GDS/TDS section -- By BILLY 12Nov2001

  private void executeCretarioForApplicantGDSTDS(Integer dealId, Integer cspDealCPId)
  {
    doDealEntryGDSTDSAppDetailsModel doAppGDSTDS =
                            (doDealEntryGDSTDSAppDetailsModel) RequestManager.getRequestContext().getModelManager().getModel(doDealEntryGDSTDSAppDetailsModel.class);

    doAppGDSTDS.clearUserWhereCriteria();

    doAppGDSTDS.addUserWhereCriterion(DFDEALID, "=", dealId);

    doAppGDSTDS.addUserWhereCriterion(DFCOPYID, "=", cspDealCPId);

    ////doAppGDSTDS.execute();
    try
    {
      ResultSet rs = doAppGDSTDS.executeSelect(null);

      if(rs == null || doAppGDSTDS.getSize() < 0)
      {
        //logger.debug("DEH@executeCretarioForApplicantGDSTDS::No row returned!!");
      }

      else if(rs != null && doAppGDSTDS.getSize() > 0)
      {
        //logger.debug("DEH@executeCretarioForApplicantGDSTDS::Size of the result set: " + doAppGDSTDS.getSize());
      }
    }
    catch (ModelControlException mce)
    {
      logger.warning("DEH@executeCretarioForApplicantGDSTDS::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.warning("DEH@executeCretarioForApplicantGDSTDS::SQLException: " + sqle);
    }
  }


  // New method to populate JS variables for Refinance Section display/hide
  //  -- By BILLY 15Feb2002

  private void populateJSVALSData(ViewBean thePage)
  {
    String valsData = "";

    //logger.debug("DEH@populateJSVALSData::ViewBeanIn: " + thePage);

    // The VAL String to be populated on the HTML
    // Set DealPurpose array
    valsData += "VALScbDealPurpose = new Array(" + PicklistData.getColumnValuesCDV("DEALPURPOSE", "DEALPURPOSEID") + ");" + "\n";
    //logger.debug("DEH@populateJSVALSData::VALcbDealPurpose: " + valsData);

    // Set EmployPercentageIncludeInGDS array
    valsData += "VALSRefinanceFlags = new Array(" + PicklistData.getColumnValuesCDS("DEALPURPOSE", "REFINANCEFLAG") + ");" + "\n";

    //logger.debug("DEH@populateJSVALSData::VALSRefinanceFlags: " + valsData);

		thePage.setDisplayFieldValue(pgDealEntryViewBean.CHILD_STVALSDATA, new String(valsData));
  }

  //--BMO_MI_CR--start//
	/**
	 *
	 *
	 */
  //// MI process logic is eliminated for BMO. They can update database either from
  //// UW or MortgageIns screens directly.
	public boolean isMIProcessOutsideBXP(PageEntry pg)
	{
		return pg.getPageCondition9();
	}

  protected boolean getCretarioIsMIStatusOutsideBXP()
  {
    boolean isOutsideBXP = false; // default.
    
    // no lender uses this property anymore. 
    // isOutsideBXP is always false.
    // this logic should be removed completly
    // 
    // Dec 14, 2011
    
    
//    // regular execution path
//    if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
//                                                  "com.basis100.miinsurance.miprocessinghandledoutsidexpress", "N").equals("N"))
//    {
//       isOutsideBXP = false;
//    }
//    // BMO execution path
//    else if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
//                                                       "com.basis100.miinsurance.miprocessinghandledoutsidexpress", "N").equals("Y"))
//    {
//       isOutsideBXP = true;
//    }
//    logger.debug("DEH@setCretarioIsMIStatusOutsideBXP::isOutsideXpress? " + isOutsideBXP);

    return isOutsideBXP;
  }
  //--BMO_MI_CR--end//

  //--FX_LINK--start--//
  /**
	 *
	 *
	 */
  //// Product doesn't allow now to re-assign the SOB, but the possiblity to edit
  //// the pre-assigned SOB should exist.
  public static final String SOB_ID_PASSED_PARM="sourceOBIdPassed";

  public void handleDetailSourceBusiness()
  {
          try
          {
                  // Goto review SOB page
                  PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
                  PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SOB_REVIEW, true);
                  Hashtable subPst = pgEntry.getPageStateTable();
                  Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());
                  int sobProfileId = deal.getSourceOfBusinessProfileId();

logger.debug("DEH@handleDetailSourceBusiness::SOBProfileId: " + sobProfileId);
                  subPst.put(SOB_ID_PASSED_PARM, new String("" + sobProfileId));
                  getSavedPages().setNextPage(pgEntry);
                  navigateToNextPage(true);
          }
          catch(Exception e)
          {
                  srk.cleanTransaction();
                  setStandardFailMessage();
                  logger.error("Error @UWorksheetHandler.handleDetailSourceBusiness: ");
                  logger.error(e);
          }
  }

  //4.4 Submission Agent
  public void handleDetailSourceBusiness(String fieldName, int rowIndex)
  {
          try
          {
              // Goto review SOB page
              PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
              PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_SOB_REVIEW, true);
              Hashtable subPst = pgEntry.getPageStateTable();

              int sobProfileId = com.iplanet.jato.util.TypeConverter.asInt(getRepeatedField(fieldName, rowIndex));
              
              logger.debug("DEH@handleDetailSourceBusiness::SOBProfileId: " + sobProfileId);
              subPst.put(SOB_ID_PASSED_PARM, new String("" + sobProfileId));
              getSavedPages().setNextPage(pgEntry);
              navigateToNextPage(true);
          }
          catch(Exception e)
          {
              srk.cleanTransaction();
              setStandardFailMessage();
              logger.error("Error @UWorksheetHandler.handleDetailSourceBusiness: ");
              logger.error(e);
          }
  }
  //--FX_LINK--end--//

	/** #DG702 not used - delete after MLM project is in production
	 * @param deal
	 * @param newDeal
	 * @throws Exception
	 * Catherine, BMO to CCAPS  01Feb05
	 * copy dealNotesCategoryId = DEAL_NOTES_CATEGORY_INGESTION_LENDER
	 * /
	 private void copyDealNotesCategoryLender(Deal deal, Deal newDeal) throws Exception
	 {
	 if (PropertiesCache.getInstance().getProperty("com.filogix.deal.copy.lender.notes", "N").equalsIgnoreCase("Y"))
	 {
	 Collection notes  = deal.getDealNotes();
	 Iterator nIt = notes.iterator();
	 while (nIt.hasNext()){
	 DealNotes oneNote = (DealNotes)nIt.next();
	 if (oneNote.getDealNotesCategoryId() == Mc.DEAL_NOTES_CATEGORY_INGESTION_LENDER){
	 int appId = TypeConverter.intTypeFrom(deal.getApplicationId());
	 DealNotes newNotes  = new DealNotes(srk);
	 newNotes.create(new DealPK(newDeal.getDealId(), newDeal.getCopyId()), appId,
	 Mc.DEAL_NOTES_CATEGORY_INGESTION_LENDER, oneNote.getDealNotesText());
	 logger.debug("DEAL_NOTES_CATEGORY_INGESTION_LENDER added to a new deal");
	 }
	 }
	 }
	 // copy servicingMortgageNumber
	 newDeal.setServicingMortgageNumber(deal.getServicingMortgageNumber());
	 logger.debug("SERVICINGMORTGAGENUMBER copied over");
	 }*/

  // SEAN Ticket #1381 June 23, 2005: copy the credit bureau reports and deal notes
  // for the resurrected deals.
  /**
   * copy all of the credit bureau reports from the current deal to the new deal.
   *
   * @param deal the current deal.
   * @param newDeal the resurrected deal.
   */
  protected void copyAllCreditBureauReports(Deal deal, Deal newDeal)
	  throws Exception {

	  // get all credit reports for the current deal.
	  Collection reports = deal.getCreditBureauReports();
	  logger.debug("Credit Bureau Reports Amount: " + reports.size());
	  for (Iterator i = reports.iterator(); i.hasNext(); ) {

		  // processing each report.
		  CreditBureauReport report = (CreditBureauReport) i.next();
		  // create new report for the new deal.
		  CreditBureauReport newReport = new CreditBureauReport(srk);
		  newReport.create(new DealPK(newDeal.getDealId(),
                     newDeal.getCopyId()));
		  // copy the report description. it is a oracle clob
		  newReport.setCreditReport(report.getCreditReport());
		  newReport.ejbStore();
		  logger.debug("New Credit Bureau Report: " + newReport.toString());
	  }
  }

  /**
   * copy all of the deal notes from the current deal to the new deal.
   */
  protected void copyAllDealNotes(Deal deal, Deal newDeal) throws Exception {

	  // get all deal notes from current deal.
	  Collection notes	= deal.getDealNotes();
	  logger.debug("Deal notes amount: " + notes.size());
	  for (Iterator i = notes.iterator(); i.hasNext(); ) {

		  DealNotes aNote = (DealNotes) i.next();
		  int appId = TypeConverter.intTypeFrom(deal.getApplicationId());
		  int categoryId = aNote.getDealNotesCategoryId();
		  // create a new deal notes for the new deal.
		  DealNotes newNote = new DealNotes(srk);
		  // submit the new deal notes.
		  newNote.create(new DealPK(newDeal.getDealId(),
             newDeal.getCopyId()),
						 appId, categoryId, aNote.getDealNotesText(),
						 aNote.getDealNotesDate().toString(),
						 aNote.getDealNotesUserId());
		  logger.debug("New Note created: " + newNote.toString());
	  }

	  newDeal.setServicingMortgageNumber(deal.getServicingMortgageNumber());
  }
  // SEAN Ticket #1381 END

  //-- ========== SCR#859 begins ========== --//
  //-- by Neil on Feb 15, 2005
  protected void setCCAPSServicingMortgageNumber() {
        doDealSummarySnapShotModel myModel = (doDealSummarySnapShotModel)(RequestManager.getRequestContext().getModelManager().getModel(doDealSummarySnapShotModel.class));
        String servicingMortgageNumber = null;

        try {
                if (myModel.getSize() > 0) {
                        Object myObj = myModel.getValue(doDealSummarySnapShotModelImpl.FIELD_DFSERVICINGMORTGAGENUMBER);
                        if (myObj == null) {
                                servicingMortgageNumber = "";
                        } else {
                                servicingMortgageNumber = myObj.toString();
                        }
                } else {
                        logger.error(
                                "DEH@setCCAPSServicingMortgageNumber(): Problem when getting ServicingMortgageNumber info !");
                }

        } catch (ModelControlException mce) {
                String strError = "DEH@setCCAPSServicingMortgageNumber(): " + mce.getMessage();
                logger.error(mce);
                logger.error(strError);
        }

        logger.debug("DEH@setCCAPSServicingMortgageNumber(): servicingMortgageNumber = " + servicingMortgageNumber);

        boolean isBMO = false;
        if (PropertiesCache.getInstance().getInstanceProperty(
//        		if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),
                                                      "com.basis100.fxp.clientid.bmo", "N").equals("Y")) {
                isBMO = true;
        }

        if (isBMO) {
			getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STCCAPS, "CCAPS: ");
			getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STSERVICINGMORTGAGENUMBER, servicingMortgageNumber);
		}
		else {
			getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STCCAPS, "");
			getCurrNDPage().setDisplayFieldValue(pgDealEntryViewBean.CHILD_STSERVICINGMORTGAGENUMBER, "");
        }
  }
  //-- ========== SCR#859 ends ========== --//

	/**
	 * isForceProgessAdvance This method reads the property from the
	 * mossys.properties and returns true if the property value is Y else
	 * returns false
	 *
	 * @param SessionResourceKit
	 * @return boolean - returns true when the  property com.basis100.system.forceprogressadvancetype is set to Y
	 *  Date: 11/21/2006
	 *  Author: NBC/PP Implementation Team
	 *
	 */
	private boolean isForceProgessAdvance(SessionResourceKit srk) {
		boolean returnVal = false;

		try {
			if (PropertiesCache.getInstance().getProperty(
			        theSessionState.getDealInstitutionId(),
					"com.basis100.system.forceprogressadvancetype", "N")
					.equalsIgnoreCase("Y")) {
				returnVal = true;
			}
		} catch (Exception e) {
			srk.getSysLogger().error(
					"@isForceProgessAdvance cannot read mossys properties:" + e); //#DG702
		}

		return returnVal;
	}

	 /**
	 * setHdForceProgressAdvance <br>
	 *  This method sets the hidden field value to 'Y'
	 * based on the property set in the mossys.properties
	 * (if property com.basis100.system.forceprogressadvancetype is set to Y this method sets the hidden feld value to 'Y')
	 *
	 * @param ViewBean
	 *          Date: 11/20/2006
	 *          Author: NBC/PP Implementation Team
	 *
	 */
	protected void setHdForceProgressAdvance(ViewBean thePage) {

		boolean isShowProgressAdvance = isForceProgessAdvance(srk);

		if (isShowProgressAdvance) {
			thePage.getDisplayField("hdForceProgressAdvance").setValue("Y");
		}
	}

	//4.4 Submission Agent
	public boolean setupNumOfRepeatedSOB(TiledView repeated1)
	{
		try
		{
			doUWSourceDetailsModelImpl listOfSOBs =(doUWSourceDetailsModelImpl)
	          (RequestManager.getRequestContext().getModelManager().getModel(doUWSourceDetailsModelImpl.class));

			if(listOfSOBs.getSize() == 0) return false;
			repeated1.setMaxDisplayTiles(listOfSOBs.getSize());
		}
		catch(Exception e)
		{
			setActiveMessageToAlert(BXResources.getSysMsg(TASK_RELATED_UNEXPECTED_FAILURE, theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
			logger.error("Exception @DealEntryHandler.setupNumOfRepeatedSOB");
			logger.error(e);
		}
		return true;
	}
    
    /**
     * Qualify Rate 
     * @param thePage
     */
    private void setHiddenQualifyRateSection(ViewBean thePage) {

        String prop = PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),
                "com.filogix.qualoverridechk", "Y");
        if (prop.equals("N")) {
            thePage.setDisplayFieldValue("stQualifyRateHidden", "visible");
            thePage.setDisplayFieldValue("stQualifyRateEdit", "none");

        } else {
            // Set Mask to make the filed not dispalyed
            thePage.setDisplayFieldValue("stQualifyRateHidden", "none");
            thePage.setDisplayFieldValue("stQualifyRateEdit", "visible");
        }
    }
    
    /**
     * Qualify Rate 
     * @param thePage
     */
    private void setQualifyRateJSValidation(ViewBean thePage) {
        
        boolean propRateIsN = PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),
                "com.filogix.qualrateoverride", "Y").equalsIgnoreCase("N");

        if (propRateIsN)
            thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_HDQUALIFYRATEDISABLED, new String("Y"));
        else
        	thePage.setDisplayFieldValue(pgUWorksheetViewBean.CHILD_HDQUALIFYRATEDISABLED, new String("N"));

        boolean propCheckBox = PropertiesCache.getInstance().getProperty(
                theSessionState.getDealInstitutionId(),
                "com.filogix.qualoverridechk", "Y").equalsIgnoreCase("N");
        
        doDealEntryMainSelectModel theModel = (doDealEntryMainSelectModel) RequestManager
                .getRequestContext().getModelManager().getModel(
                        doDealEntryMainSelectModel.class);

        Object flag = theModel.getDfQualifyingOverrideFlag();
        
        HtmlDisplayFieldBase chQualifyRateOverrideDf = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField("chQualifyRateOverride");

        String extraId = "id='chQualifyRateOverride' ";
        String jsChange = " onclick ='qualifyProdRate();'"; //qualifyChkBoxClicked()
        String disable = " disabled='true'";

        chQualifyRateOverrideDf.setExtraHtml(extraId + jsChange);
        
        //new FSD
        HtmlDisplayFieldBase chQualifyRateOverrideRateDf = (HtmlDisplayFieldBase) getCurrNDPage()
                .getDisplayField("chQualifyRateOverrideRate");

        extraId = "id='chQualifyRateOverrideRate' ";
        jsChange = " onclick ='qualifyProdRate();'"; //qualifyRateChkBoxClicked();
        if (propRateIsN) //rate changes are disabled
        	chQualifyRateOverrideRateDf.setExtraHtml(extraId + jsChange + disable);        
        else
        	chQualifyRateOverrideRateDf.setExtraHtml(extraId + jsChange);        
        //end new FSD
        
        HtmlDisplayFieldBase qualifyRateDf = (HtmlDisplayFieldBase) getCurrNDPage()
                .getDisplayField("txQualifyRate");

        extraId = "id='txQualifyRate'";
        String n = "'N'";
        jsChange = " onBlur=\"if(isFieldDecimal(3)) isFieldInDecRange(0, 100); showRatePencil("+ n +");\"";

        String extraHtml = extraId + jsChange;

        if ( flag==null && (!propCheckBox && !propRateIsN)) {
            extraHtml = extraHtml + disable;
        } else if ( propCheckBox || propRateIsN || (flag!=null && "N".equalsIgnoreCase(flag.toString()))) {
            extraHtml = extraHtml + disable;
        }
        qualifyRateDf.setExtraHtml(extraHtml);

        HtmlDisplayFieldBase cbProductType = (HtmlDisplayFieldBase) getCurrNDPage()
                .getDisplayField("cbQualifyProductType");

        extraId = "id='cbQualifyProductType' onChange ='qualifyProductChanged();'";
        extraHtml = extraId;

        if ( flag==null && (!propCheckBox && !propRateIsN)) {
            extraHtml = extraHtml + disable;
        } else if ( propCheckBox || propRateIsN || (flag!=null && "N".equalsIgnoreCase(flag.toString()))) {
            extraHtml = extraHtml + disable;
        }
        cbProductType.setExtraHtml(extraHtml);
    }
    
    /**
     * populate product options.
     */
    protected void populateQualifyingProducts(String fieldName, Deal deal) 
    {

        // let's prepare the options.
    	OptionList theProductOptions = new OptionList();
        Map products = QualifyingRateHelper.getValidQualifyingProducts(getSessionResourceKit(),
                             theSessionState.getLanguageId(),
                             deal);
        
        for (Iterator i = products.keySet().iterator(); i.hasNext(); ) {
            Integer id = (Integer) i.next();
            String label = (String) products.get(id);
            theProductOptions.add(new Option(label, id.toString()));
        }
        // populate the product options.
        ComboBox cbProduct =
            (ComboBox) getCurrNDPage().getChild(fieldName);
        cbProduct.setOptions(theProductOptions);
    }
    
}


