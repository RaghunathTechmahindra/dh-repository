package mosApp.MosSystem;

/**
 * 14/Mar/2007 DVG #DG586 FXP16091  Hide UserType Options from Sys Admins 
 */

import java.sql.SQLException;

import MosSystem.Mc;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doUserAdminProfileInfoModelImpl extends QueryModelBase
	implements doUserAdminProfileInfoModel
{
	/**
	 *
	 *
	 */
	public doUserAdminProfileInfoModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUserProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUserLogin()
	{
		return (String)getValue(FIELD_DFUSERLOGIN);
	}


	/**
	 *
	 *
	 */
	public void setDfUserLogin(String value)
	{
		setValue(FIELD_DFUSERLOGIN,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUserTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfGroupProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFGROUPPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfGroupProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFGROUPPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfJointApproverUserId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFJOINTAPPROVERUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfJointApproverUserId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFJOINTAPPROVERUSERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfContactId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCONTACTID);
	}


	/**
	 *
	 *
	 */
	public void setDfContactId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCONTACTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProfileStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPROFILESTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfProfileStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPROFILESTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBilingual()
	{
		return (String)getValue(FIELD_DFBILINGUAL);
	}


	/**
	 *
	 *
	 */
	public void setDfBilingual(String value)
	{
		setValue(FIELD_DFBILINGUAL,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPartnerUserId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPARTNERUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfPartnerUserId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPARTNERUSERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfStandInUserId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSTANDINUSERID);
	}


	/**
	 *
	 *
	 */
	public void setDfStandInUserId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSTANDINUSERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfManagerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMANAGERID);
	}


	/**
	 *
	 *
	 */
	public void setDfManagerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMANAGERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfHigherApproverId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFHIGHERAPPROVERID);
	}


	/**
	 *
	 *
	 */
	public void setDfHigherApproverId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFHIGHERAPPROVERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUPBusinessId()
	{
		return (String)getValue(FIELD_DFUPBUSINESSID);
	}


	/**
	 *
	 *
	 */
	public void setDfUPBusinessId(String value)
	{
		setValue(FIELD_DFUPBUSINESSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFirstName()
	{
		return (String)getValue(FIELD_DFFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfFirstName(String value)
	{
		setValue(FIELD_DFFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfInitial()
	{
		return (String)getValue(FIELD_DFINITIAL);
	}


	/**
	 *
	 *
	 */
	public void setDfInitial(String value)
	{
		setValue(FIELD_DFINITIAL,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLastName()
	{
		return (String)getValue(FIELD_DFLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfLastName(String value)
	{
		setValue(FIELD_DFLASTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPhoneNumber()
	{
		return (String)getValue(FIELD_DFPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfPhoneNumber(String value)
	{
		setValue(FIELD_DFPHONENUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfFaxNumber()
	{
		return (String)getValue(FIELD_DFFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfFaxNumber(String value)
	{
		setValue(FIELD_DFFAXNUMBER,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL USERPROFILE.USERPROFILEID, USERPROFILE.USERLOGIN, "+
    "USERPROFILE.USERTYPEID, USERPROFILE.GROUPPROFILEID, "+
    "USERPROFILE.JOINTAPPROVERUSERID, USERPROFILE.CONTACTID, "+
    "USERPROFILE.PROFILESTATUSID, USERPROFILE.BILINGUAL, "+
    "USERPROFILE.PARTNERUSERID, USERPROFILE.STANDINUSERID, "+
    "USERPROFILE.MANAGERID, USERPROFILE.HIGHERAPPROVERUSERID, "+
    "USERPROFILE.UPBUSINESSID, CONTACT.CONTACTFIRSTNAME, "+
    "CONTACT.CONTACTMIDDLEINITIAL, CONTACT.CONTACTLASTNAME, "+
    "CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER "+
    "FROM CONTACT, USERPROFILE  "+
    "__WHERE__  "+
    "ORDER BY CONTACT.CONTACTFIRSTNAME  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, USERPROFILE";
	public static final String STATIC_WHERE_CRITERIA=
    " (((CONTACT.CONTACTID  =  USERPROFILE.CONTACTID) AND " +
    " (CONTACT.INSTITUTIONPROFILEID = USERPROFILE.INSTITUTIONPROFILEID)) AND "
    //  #DG586 hide special users
    //"USERPROFILE.USERPROFILEID <> 0) "
    +" USERPROFILE.USERTYPEID >= "+Mc.USER_TYPE_PASSIVE 
    +" and USERPROFILE.USERTYPEID < "+Mc.USER_TYPE_SPECIAL +')';
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID="USERPROFILE.USERPROFILEID";
	public static final String COLUMN_DFUSERPROFILEID="USERPROFILEID";
	public static final String QUALIFIED_COLUMN_DFUSERLOGIN="USERPROFILE.USERLOGIN";
	public static final String COLUMN_DFUSERLOGIN="USERLOGIN";
	public static final String QUALIFIED_COLUMN_DFUSERTYPEID="USERPROFILE.USERTYPEID";
	public static final String COLUMN_DFUSERTYPEID="USERTYPEID";
	public static final String QUALIFIED_COLUMN_DFGROUPPROFILEID="USERPROFILE.GROUPPROFILEID";
	public static final String COLUMN_DFGROUPPROFILEID="GROUPPROFILEID";
	public static final String QUALIFIED_COLUMN_DFJOINTAPPROVERUSERID="USERPROFILE.JOINTAPPROVERUSERID";
	public static final String COLUMN_DFJOINTAPPROVERUSERID="JOINTAPPROVERUSERID";
	public static final String QUALIFIED_COLUMN_DFCONTACTID="USERPROFILE.CONTACTID";
	public static final String COLUMN_DFCONTACTID="CONTACTID";
	public static final String QUALIFIED_COLUMN_DFPROFILESTATUSID="USERPROFILE.PROFILESTATUSID";
	public static final String COLUMN_DFPROFILESTATUSID="PROFILESTATUSID";
	public static final String QUALIFIED_COLUMN_DFBILINGUAL="USERPROFILE.BILINGUAL";
	public static final String COLUMN_DFBILINGUAL="BILINGUAL";
	public static final String QUALIFIED_COLUMN_DFPARTNERUSERID="USERPROFILE.PARTNERUSERID";
	public static final String COLUMN_DFPARTNERUSERID="PARTNERUSERID";
	public static final String QUALIFIED_COLUMN_DFSTANDINUSERID="USERPROFILE.STANDINUSERID";
	public static final String COLUMN_DFSTANDINUSERID="STANDINUSERID";
	public static final String QUALIFIED_COLUMN_DFMANAGERID="USERPROFILE.MANAGERID";
	public static final String COLUMN_DFMANAGERID="MANAGERID";
	public static final String QUALIFIED_COLUMN_DFHIGHERAPPROVERID="USERPROFILE.HIGHERAPPROVERUSERID";
	public static final String COLUMN_DFHIGHERAPPROVERID="HIGHERAPPROVERUSERID";
	public static final String QUALIFIED_COLUMN_DFUPBUSINESSID="USERPROFILE.UPBUSINESSID";
	public static final String COLUMN_DFUPBUSINESSID="UPBUSINESSID";
	public static final String QUALIFIED_COLUMN_DFFIRSTNAME="CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_DFFIRSTNAME="CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFINITIAL="CONTACT.CONTACTMIDDLEINITIAL";
	public static final String COLUMN_DFINITIAL="CONTACTMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFLASTNAME="CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_DFLASTNAME="CONTACTLASTNAME";
	public static final String QUALIFIED_COLUMN_DFPHONENUMBER="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFPHONENUMBER="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFFAXNUMBER="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFFAXNUMBER="CONTACTFAXNUMBER";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERLOGIN,
				COLUMN_DFUSERLOGIN,
				QUALIFIED_COLUMN_DFUSERLOGIN,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERTYPEID,
				COLUMN_DFUSERTYPEID,
				QUALIFIED_COLUMN_DFUSERTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFGROUPPROFILEID,
				COLUMN_DFGROUPPROFILEID,
				QUALIFIED_COLUMN_DFGROUPPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOINTAPPROVERUSERID,
				COLUMN_DFJOINTAPPROVERUSERID,
				QUALIFIED_COLUMN_DFJOINTAPPROVERUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTACTID,
				COLUMN_DFCONTACTID,
				QUALIFIED_COLUMN_DFCONTACTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROFILESTATUSID,
				COLUMN_DFPROFILESTATUSID,
				QUALIFIED_COLUMN_DFPROFILESTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBILINGUAL,
				COLUMN_DFBILINGUAL,
				QUALIFIED_COLUMN_DFBILINGUAL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPARTNERUSERID,
				COLUMN_DFPARTNERUSERID,
				QUALIFIED_COLUMN_DFPARTNERUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTANDINUSERID,
				COLUMN_DFSTANDINUSERID,
				QUALIFIED_COLUMN_DFSTANDINUSERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMANAGERID,
				COLUMN_DFMANAGERID,
				QUALIFIED_COLUMN_DFMANAGERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFHIGHERAPPROVERID,
				COLUMN_DFHIGHERAPPROVERID,
				QUALIFIED_COLUMN_DFHIGHERAPPROVERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUPBUSINESSID,
				COLUMN_DFUPBUSINESSID,
				QUALIFIED_COLUMN_DFUPBUSINESSID,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIRSTNAME,
				COLUMN_DFFIRSTNAME,
				QUALIFIED_COLUMN_DFFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINITIAL,
				COLUMN_DFINITIAL,
				QUALIFIED_COLUMN_DFINITIAL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLASTNAME,
				COLUMN_DFLASTNAME,
				QUALIFIED_COLUMN_DFLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPHONENUMBER,
				COLUMN_DFPHONENUMBER,
				QUALIFIED_COLUMN_DFPHONENUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFAXNUMBER,
				COLUMN_DFFAXNUMBER,
				QUALIFIED_COLUMN_DFFAXNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

