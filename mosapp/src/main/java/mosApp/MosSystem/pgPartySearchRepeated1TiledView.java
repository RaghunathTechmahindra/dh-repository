package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;

/**
 *
 *
 *
 */
public class pgPartySearchRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgPartySearchRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doPartySearchModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.ContainerView#resetChildren()
	 */
	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.ContainerView#resetChildren()
	 */
	/* (non-Javadoc)
	 * @see com.iplanet.jato.view.ContainerView#resetChildren()
	 */
	public void resetChildren()
	{
		getStPartyType().setValue(CHILD_STPARTYTYPE_RESET_VALUE);
		getStPartyStatus().setValue(CHILD_STPARTYSTATUS_RESET_VALUE);
		getStPhoneNumber().setValue(CHILD_STPHONENUMBER_RESET_VALUE);
		getStFaxNumber().setValue(CHILD_STFAXNUMBER_RESET_VALUE);
		getStPartyCompanyName().setValue(CHILD_STPARTYCOMPANYNAME_RESET_VALUE);
		getStCity().setValue(CHILD_STCITY_RESET_VALUE);
		getStProvince().setValue(CHILD_STPROVINCE_RESET_VALUE);
		getBtSelectParty().setValue(CHILD_BTSELECTPARTY_RESET_VALUE);
		getStPartyShortName().setValue(CHILD_STPARTYSHORTNAME_RESET_VALUE);
		getHdPartyId().setValue(CHILD_HDPARTYID_RESET_VALUE);
		getHdPartyTypeId().setValue(CHILD_HDPARTYTYPEID_RESET_VALUE);
		getHdPartyStatusId().setValue(CHILD_HDPARTYSTATUSID_RESET_VALUE);
		getBtReview().setValue(CHILD_BTREVIEW_RESET_VALUE);
    //--DJ_PT_CR--start//
    //// The new field in thesearch (Client or Branch Transit#)
		getStClientOrBranchTransitNum().setValue(CHILD_STCLIENTORBRANCHTRANSITNUM_RESET_VALUE);
//***** NBC/PP Implementation Team - Version 1.1 - START *****//
		getStLocation().setValue(CHILD_STLOCATION_RESET_VALUE);
//***** NBC/PP Implementation Team - Version 1.1 - END *****//		
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STPARTYTYPE,StaticTextField.class);
		registerChild(CHILD_STPARTYSTATUS,StaticTextField.class);
		registerChild(CHILD_STPHONENUMBER,StaticTextField.class);
		registerChild(CHILD_STFAXNUMBER,StaticTextField.class);
		registerChild(CHILD_STPARTYCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STCITY,StaticTextField.class);
		registerChild(CHILD_STPROVINCE,StaticTextField.class);
		registerChild(CHILD_BTSELECTPARTY,Button.class);
		registerChild(CHILD_STPARTYSHORTNAME,StaticTextField.class);
		registerChild(CHILD_HDPARTYID,HiddenField.class);
		registerChild(CHILD_HDPARTYTYPEID,HiddenField.class);
		registerChild(CHILD_HDPARTYSTATUSID,HiddenField.class);
		registerChild(CHILD_BTREVIEW,Button.class);
    //--DJ_PT_CR--start//
    //// The new field in thesearch (Client or Branch Transit#)
		registerChild(CHILD_STCLIENTORBRANCHTRANSITNUM,StaticTextField.class);
//		***** NBC/PP Implementation Team - Version 1.1 - START *****//
		registerChild(CHILD_STLOCATION,StaticTextField.class);
//		***** NBC/PP Implementation Team - Version 1.1 - END *****//			
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoPartySearchModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.checkAndSyncDOWithCursorInfo(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
      //// Get Row Index first.
      int rowNum = this.getTileIndex();
      boolean ret = true;

		  PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());

      //logger = SysLog.getSysLogger("pgPSRRTV");

      //logger.debug("pgPSRRTV@nextTile::rowNum: " + rowNum);

      doPartySearchModelImpl theObj =(doPartySearchModelImpl) getdoPartySearchModel();
      //logger.debug("pgPSRRTV@nextTile::Size: " + theObj.getSize());

      if (theObj.getNumRows() > 0)
      {
        //logger.debug("pgPSRRTV@nextTile::Location: " + theObj.getLocation());
        handler.setPartySearchResultDisplayFields(theObj, rowNum);
      }
      else
        ret = false;

      handler.pageSaveState();

      return ret;

		}

		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * createChild<br>
	 *    Creates view children
	 *
	 * @param name String<br> 
	 * 	 Name of the child to be created
	 * 
	 * @returns View : the child as a view object
	 * @version?? <br>
	 * Date: 05/30/2006
	 * Author: NBC/PP Implementation Team<br>
	 * Change: <br>
	 * 	- Added static text field location 
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STPARTYTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySearchModel(),
				CHILD_STPARTYTYPE,
				doPartySearchModel.FIELD_DFPARTYTYPEDESCRIPTION,
				CHILD_STPARTYTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYSTATUS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySearchModel(),
				CHILD_STPARTYSTATUS,
				doPartySearchModel.FIELD_DFPARTYPROFILESTATUSDESC,
				CHILD_STPARTYSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPHONENUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPHONENUMBER,
				CHILD_STPHONENUMBER,
				CHILD_STPHONENUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFAXNUMBER))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STFAXNUMBER,
				CHILD_STFAXNUMBER,
				CHILD_STFAXNUMBER_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPARTYCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySearchModel(),
				CHILD_STPARTYCOMPANYNAME,
				doPartySearchModel.FIELD_DFCOMPANY,
				CHILD_STPARTYCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySearchModel(),
				CHILD_STCITY,
				doPartySearchModel.FIELD_DFCITY,
				CHILD_STCITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROVINCE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySearchModel(),
				CHILD_STPROVINCE,
				doPartySearchModel.FIELD_DFPROVINCEABBREVIATION,
				CHILD_STPROVINCE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSELECTPARTY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSELECTPARTY,
				CHILD_BTSELECTPARTY,
				CHILD_BTSELECTPARTY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPARTYSHORTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySearchModel(),
				CHILD_STPARTYSHORTNAME,
				doPartySearchModel.FIELD_DFPARTYNAME,
				CHILD_STPARTYSHORTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPARTYID))
		{
			HiddenField child = new HiddenField(this,
				getdoPartySearchModel(),
				CHILD_HDPARTYID,
				doPartySearchModel.FIELD_DFPARTYID,
				CHILD_HDPARTYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPARTYTYPEID))
		{
			HiddenField child = new HiddenField(this,
				getdoPartySearchModel(),
				CHILD_HDPARTYTYPEID,
				doPartySearchModel.FIELD_DFPARTYTYPE,
				CHILD_HDPARTYTYPEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDPARTYSTATUSID))
		{
			HiddenField child = new HiddenField(this,
				getdoPartySearchModel(),
				CHILD_HDPARTYSTATUSID,
				doPartySearchModel.FIELD_DFPARTYSTATUS,
				CHILD_HDPARTYSTATUSID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTREVIEW))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTREVIEW,
				CHILD_BTREVIEW,
				CHILD_BTREVIEW_RESET_VALUE,
				null);
				return child;

		}
    //--DJ_PT_CR--start//
    //// The new field in thesearch (Client or Branch Transit#)
		else
		if (name.equals(CHILD_STCLIENTORBRANCHTRANSITNUM))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySearchModel(),
				CHILD_STCLIENTORBRANCHTRANSITNUM,
				doPartySearchModel.FIELD_DFCLIENTNUMBER,
				CHILD_STCLIENTORBRANCHTRANSITNUM_RESET_VALUE,
				null);
			return child;
		}
    //--DJ_PT_CR--end//
//		***** NBC/PP Implementation Team - Version 1.1 - START *****//
		else
		if (name.equals(CHILD_STLOCATION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoPartySearchModel(),
				CHILD_STLOCATION,
				doPartySearchModel.FIELD_DFLOCATION,
				CHILD_STLOCATION_RESET_VALUE,
				null);
			return child;
		}
//		***** NBC/PP Implementation Team - Version 1.1 - END *****//			
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyType()
	{
		return (StaticTextField)getChild(CHILD_STPARTYTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyStatus()
	{
		return (StaticTextField)getChild(CHILD_STPARTYSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPhoneNumber()
	{
		return (StaticTextField)getChild(CHILD_STPHONENUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFaxNumber()
	{
		return (StaticTextField)getChild(CHILD_STFAXNUMBER);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STPARTYCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCity()
	{
		return (StaticTextField)getChild(CHILD_STCITY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStProvince()
	{
		return (StaticTextField)getChild(CHILD_STPROVINCE);
	}

  //--DJ_PT_CR--start//
  //// The new field in thesearch (Client or Branch Transit#)
	/**
	 *
	 *
	 */
	public StaticTextField getStClientOrBranchTransitNum()
	{
		return (StaticTextField)getChild(CHILD_STCLIENTORBRANCHTRANSITNUM);
	}
  //--DJ_PT_CR--end//
	/**
	 * getStLocation<br>
	 * 	Returns StaticTextField child of this view
	 * 
	 * @param none
	 * 
	 * @return StaticTextField : Location static text field<br>
	 * @version 1.0 (Initial Version - 30 May 2006)
	 * @author NBC/PP Implementation Team
	 * 
	 * 
	 */
	public StaticTextField getStLocation()
	{
		return (StaticTextField)getChild(CHILD_STLOCATION);
	}
	/**
	 *
	 *
	 */
	public void handleBtSelectPartyRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

    //logger = SysLog.getSysLogger("pgPSRRTV");

		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();
        handler.pageGetState(this.getParentViewBean());

		handler.preHandlerProtocol(this.getParentViewBean());

		////handler.handleSelectParty(handler.getRowNdxFromWebEventMethod(event));
	handler.handleSelectParty(((TiledViewRequestInvocationEvent)(event)).getTileNumber());

    //logger.debug("pgPSRRTV@handleBtSelectPartyRequest::Index: " + ((TiledViewRequestInvocationEvent)(event)).getTileNumber());

		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSelectParty()
	{
		return (Button)getChild(CHILD_BTSELECTPARTY);
	}


	/**
	 *
	 *
	 */
	public String endBtSelectPartyDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btSelectParty_onBeforeHtmlOutputEvent method
		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.pageGetState(getParentViewBean());

		boolean rc = handler.handleViewPartySelectButton();

		handler.pageSaveState();

    if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPartyShortName()
	{
		return (StaticTextField)getChild(CHILD_STPARTYSHORTNAME);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPartyId()
	{
		return (HiddenField)getChild(CHILD_HDPARTYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPartyTypeId()
	{
		return (HiddenField)getChild(CHILD_HDPARTYTYPEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdPartyStatusId()
	{
		return (HiddenField)getChild(CHILD_HDPARTYSTATUSID);
	}


	/**
	 *
	 *
	 */
	public void handleBtReviewRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{

		PartySearchHandler handler =(PartySearchHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleReview(handler.getRowNdxFromWebEventMethod(event));
    ////handler.handleReview(((TiledViewRequestInvocationEvent)(event)).getTileNumber());

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtReview()
	{
		return (Button)getChild(CHILD_BTREVIEW);
	}


	/**
	 *
	 *
	 */
	public doPartySearchModel getdoPartySearchModel()
	{
		if (doPartySearch == null)
			doPartySearch = (doPartySearchModel) getModel(doPartySearchModel.class);
		return doPartySearch;
	}


	/**
	 *
	 *
	 */
	public void setdoPartySearchModel(doPartySearchModel model)
	{
			doPartySearch = model;
	}

  public void setCurrentDisplayOffset(int offset) throws Exception
  {
    setWebActionModelOffset(offset);
    handleWebAction(WebActions.ACTION_REFRESH);
    // Have to re-execute the Model here.  Otherwise, not working
    executeAutoRetrievingModels();
  }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STPARTYTYPE="stPartyType";
	public static final String CHILD_STPARTYTYPE_RESET_VALUE="";
	public static final String CHILD_STPARTYSTATUS="stPartyStatus";
	public static final String CHILD_STPARTYSTATUS_RESET_VALUE="";
	public static final String CHILD_STPHONENUMBER="stPhoneNumber";
	public static final String CHILD_STPHONENUMBER_RESET_VALUE="";
	public static final String CHILD_STFAXNUMBER="stFaxNumber";
	public static final String CHILD_STFAXNUMBER_RESET_VALUE="";
	public static final String CHILD_STPARTYCOMPANYNAME="stPartyCompanyName";
	public static final String CHILD_STPARTYCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STCITY="stCity";
	public static final String CHILD_STCITY_RESET_VALUE="";
	public static final String CHILD_STPROVINCE="stProvince";
	public static final String CHILD_STPROVINCE_RESET_VALUE="";
	public static final String CHILD_BTSELECTPARTY="btSelectParty";
	public static final String CHILD_BTSELECTPARTY_RESET_VALUE="Select";
	public static final String CHILD_STPARTYSHORTNAME="stPartyShortName";
	public static final String CHILD_STPARTYSHORTNAME_RESET_VALUE="";
	public static final String CHILD_HDPARTYID="hdPartyId";
	public static final String CHILD_HDPARTYID_RESET_VALUE="";
	public static final String CHILD_HDPARTYTYPEID="hdPartyTypeId";
	public static final String CHILD_HDPARTYTYPEID_RESET_VALUE="";
	public static final String CHILD_HDPARTYSTATUSID="hdPartyStatusId";
	public static final String CHILD_HDPARTYSTATUSID_RESET_VALUE="";
	public static final String CHILD_BTREVIEW="btReview";
	public static final String CHILD_BTREVIEW_RESET_VALUE="Custom";

  //--DJ_PT_CR--start//
  //// The new field in thesearch (Client or Branch Transit#)
	public static final String CHILD_STCLIENTORBRANCHTRANSITNUM="stClientOrBranchTransitNum";
	public static final String CHILD_STCLIENTORBRANCHTRANSITNUM_RESET_VALUE="";
        //--DJ_PT_CR--end//
//	***** NBC/PP Implementation Team - Version 1.1 - START *****//
	public static final String CHILD_STLOCATION="stLocation";
	public static final String CHILD_STLOCATION_RESET_VALUE="";
//	***** NBC/PP Implementation Team - Version 1.1 - END *****//	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doPartySearchModel doPartySearch=null;
	private PartySearchHandler handler=new PartySearchHandler();
  public SysLogger logger;

}

