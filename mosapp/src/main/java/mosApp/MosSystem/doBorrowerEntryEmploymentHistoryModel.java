package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doBorrowerEntryEmploymentHistoryModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmploymentHistoryId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentHistoryId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmploymentHistoryOccupationId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentHistoryOccupationId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmploymentHistoryName();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentHistoryName(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmploymentHistoryMonthsOfService();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentHistoryMonthsOfService(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmploymentHistoryContactId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentHistoryContactId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfContactId();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfContactPhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactPhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfContactFaxNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactFaxNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfContactEmailAddress();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactEmailAddress(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfContactAddrId();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactAddrId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAddrId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddrId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine2();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine2(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAddressLine1();

	
	/**
	 * 
	 * 
	 */
	public void setDfAddressLine1(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfCity();

	
	/**
	 * 
	 * 
	 */
	public void setDfCity(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalFSA();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalFSA(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPostalLDU();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostalLDU(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfProvinceId();

	
	/**
	 * 
	 * 
	 */
	public void setDfProvinceId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFEMPLOYMENTHISTORYID="dfEmploymentHistoryId";
	public static final String FIELD_DFEMPLOYMENTHISTORYOCCUPATIONID="dfEmploymentHistoryOccupationId";
	public static final String FIELD_DFEMPLOYMENTHISTORYNAME="dfEmploymentHistoryName";
	public static final String FIELD_DFEMPLOYMENTHISTORYMONTHSOFSERVICE="dfEmploymentHistoryMonthsOfService";
	public static final String FIELD_DFEMPLOYMENTHISTORYCONTACTID="dfEmploymentHistoryContactId";
	public static final String FIELD_DFCONTACTID="dfContactId";
	public static final String FIELD_DFCONTACTPHONENUMBER="dfContactPhoneNumber";
	public static final String FIELD_DFCONTACTFAXNUMBER="dfContactFaxNumber";
	public static final String FIELD_DFCONTACTEMAILADDRESS="dfContactEmailAddress";
	public static final String FIELD_DFCONTACTADDRID="dfContactAddrId";
	public static final String FIELD_DFADDRID="dfAddrId";
	public static final String FIELD_DFADDRESSLINE2="dfAddressLine2";
	public static final String FIELD_DFADDRESSLINE1="dfAddressLine1";
	public static final String FIELD_DFCITY="dfCity";
	public static final String FIELD_DFPOSTALFSA="dfPostalFSA";
	public static final String FIELD_DFPOSTALLDU="dfPostalLDU";
	public static final String FIELD_DFPROVINCEID="dfProvinceId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

