package mosApp.MosSystem;

import java.util.Collection;
import java.util.Iterator;

import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.ViewBeanBase;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.docprep.Dc;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.deal.security.ViewBeanFactory;

/**
 * <p>Title: DisclosureFactory.java </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 * @version 4
 * Date: 11/28/2006
 * Author: NBC/PP Implementation Team
 * Change:
 *  Modified getViewBeanByClassName to add the framework for implementing other provinces like NS, PEI, NL and AB
 *  - Modified the if loop in getViewBeanByClassName to return names of view bean depending on province
 */
public class DisclosureFactory extends ViewBeanFactory {

  private doDisclosureSelectModel doDisclosure = null;
  private DisclosureHandler handler=new DisclosureHandler("pgDisclosure");
  private static final String PATH = "mosApp.MosSystem";
  private Deal deal;
  private int provinceCode = -1;
  
  public DisclosureFactory() {
    path = PATH;
  }

  public DisclosureFactory(Deal deal) {
    path = PATH;
    this.deal = deal;
  }

  /**
     * A factory method to determine which pgDisclosureXXViewBean to use.
     * 
     * @return String - name of the view
     * @version 4
     * Date: 11/28/2006
     * Author: NBC/PP Implementation Team
     * Change:
     *  Modified getViewBeanByClassName to add the framework for implementing other provinces like NS, PEI, NL and AB
     */
    public String getViewBeanByClassName() {
        // if deal not set, just return null;
        if (deal == null)
            return null;

        try {
            Collection c = deal.getProperties();
            Property prop = null;
            if (c != null) {
                Iterator it = c.iterator();

                // loop until a primary (subject) property is found.
                while (it.hasNext()) {
                    prop = (Property) it.next();

                    if (prop.isPrimaryProperty()) {
                        break;
                    }
                }

                //***** Change by NBC Impl. Team - Version 1.2 - Start *****//
                if (prop != null) {
                    int provId = prop.getProvinceId();
                    if (Dc.PROVINCE_ONTARIO == provId) {
                        return "pgDisclosureON";
                    }
                    if (Dc.PROVINCE_NEWFOUNDLAND == provId) {
                        return "pgDisclosureNL";
                    }
                    if (Dc.PROVINCE_NOVA_SCOTIA == provId) {
                        return "pgDisclosureNS";
                    }
                    if (Dc.PROVINCE_ALBERTA == provId) {
                        return "pgDisclosureAB";
                    }
                    if (Dc.PROVINCE_PRINCE_EDWARD_ISLAND == provId) {
                        return "pgDisclosurePEI";
                    }
                }                
            }
        } catch (Exception ex) {
            SysLog.error("Exception while trying to retrieve view bean name is disclosure Factory due to " + ex.getMessage());
        }
        //***** Change by NBC Impl. Team - Version 1.2 - End *****//

        // no subject property defined
        // this gets returned but access rights should NEVER be defined for this page so the "Access is
        // denied" dialog is displayed
        // therefore, do not define values for this view bean in USERTYPEPAGEACCESS table or
        // DEALSTATUSPAGEACCESS table
        return "pgDisclosure";
    }
    
}
