package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDealMIInsurerModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
    /**
     * 
     * 
     */
    public java.math.BigDecimal getDfCopyId();

    
    /**
     * 
     * 
     */
    public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfMIInsurerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIInsurerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIInsurerUserName();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIInsurerUserName(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
    public static final String FIELD_DFCOPYID="dfCopyId";
    public static final String FIELD_DFMIINSURERID="dfMIInsurerId";
	public static final String FIELD_DFMIINSURERUSERNAME="dfMIInsurerUserName";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

