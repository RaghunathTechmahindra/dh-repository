package mosApp.MosSystem;

import java.math.BigDecimal;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * Title: doComponentCreditCardModel
 * <p>
 * Description: This is the interface class for doComponentCreditCardModelImpl
 * used to retrieve data from componentcreditcard table.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 * @version 1.1 25-June-2008 XS_16.16 Added new Additional Information Field.
 * @version 1.2 July 8, 2008 XS_2.36(dynamic drop down list) Added MtgProdId,new Additional Information Field.
 */
public interface doComponentCreditCardModel extends QueryModel,
SelectQueryModel {

    public static final String FIELD_DFCOMPONENTID = "dfComponentId";

    public static final String FIELD_DFCOPYID = "dfCopyId";

    public static final String FIELD_DFDEALID = "dfDealId";

    public static final String FIELD_DFISNTITUTIONID = "dfInstitutionId";

    public static final String FIELD_DFCREDITCARDAMOUNT = "dfCreditCardAmount";

    public static final String FIELD_DFPOSTEDRATE = "dfPostedRate";

    public static final String FIELD_DFCOMPONENTTYPE = "dfComponentType";

    public static final String FIELD_DFPRODUCTNAME = "dfProductName";

    public static final String FIELD_DFADDITIONALINFORMATION = "dfAdditionalInformation";

    /**************************MCM Impl Team XS_2.36 changes starts *****************/
    public static final String FIELD_DFMTGPRODID = "dfMtgProdId";
    public static final String FIELD_DFCCINTERESTRATE = "dfccInterestRate";
    /**************************MCM Impl Team XS_2.36 changes ebds *****************/

    /*****************MCM Impl Team XS_2.36 (dynamic drop down lists) changes start*************************/
    public static final String FIELD_DFPRICINGRATEINVENTORYID = "dfPricingrateinventoryid";
    /*****************MCM Impl Team XS_2.36 (dynamic drop down lists) changes ends*************************/

    /**
     * This method declaration returns the value of field ComponentId.
     * @return BigDecimal componentId from the model.
     */
    public java.math.BigDecimal getDfComponentId();

    /**
     * This method declaration sets the value of field ComponentId
     * @param value -
     *            new value of the field
     */
    public void setDfComponentId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field copyId.
     * @return BigDecimal copyId from the model.
     */
    public java.math.BigDecimal getDfCopyId();

    /**
     * This method declaration sets the value of field copyId
     * @param value -
     *            new value of the field
     */
    public void setDfCopyId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field InstitutionId.
     * @return BigDecimal InstitutionId from the model.
     */
    public java.math.BigDecimal getDfInstitutionId();

    /**
     * This method declaration sets the value of field InstitutionId
     * @param value -
     *            new InstitutionId.
     */
    public void setDfInstitutionId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field creditcardAmount.
     * @return BigDecimal creditcardAmount from the model
     */
    public java.math.BigDecimal getDfCreditCardAmount();

    /**
     * This method declaration sets the creditcardAmount.
     * @param value
     *            the new creditcardAmount
     */
    public void setDfCreditCardAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field ComponentType.
     * @return String componentType from the model
     */
    public String getDfComponentType();

    /**
     * This method declaration sets the dfComponentType.
     * @param value
     *            the new dfComponentType
     */
    public void setDfComponentType(String value);

    /**
     * This method declaration returns the value of field postedrate.
     * @return the postedrate
     */
    public java.math.BigDecimal getDfPostedRate();

    /**
     * This method declaration sets the postedrate.
     * @param value
     *            the new postedrate
     */
    public void setDfPostedRate(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the DealId.
     * @return the DealId
     */
    public java.math.BigDecimal getDfDealId();

    /**
     * This method declaration sets the the dealId.
     * @param value
     *            the new dealId
     */
    public void setDfDealId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ProductName.
     * @return the ProductName
     */
    public String getDfProductName();

    /**
     * This method declaration sets the the dfProductName.
     * @param value
     *            the new dfProductName
     */
    public void setDfProductName(String value);

    /*********************MCM Impl Team XS_16.16 changes starts******************/
    /**
     * This method declaration returns the value of field the DfAdditionalInformation.
     * @return the ProductName
     */
    public String getDfAdditionalInformation();

    /**
     * This method declaration sets the the DfAdditionalInformation.
     * @param value new DfAdditionalInformation
     */
    public void setDfAdditionalInformation(String value);
    /*********************MCM Impl Team XS_16.16 changes ends******************/

    /*********************MCM Impl Team XS_2.36 changes starts*********************/    
    /**
     * This method declaration returns the value of field the MtgProdId.
     * @return dfMtgProdId
     */
    public java.math.BigDecimal getDfMtgProdId();

    /**
     * This method declaration sets the MtgProdId.
     * @param value: the new dfMtgProdId
     */
    public void setDfMtgProdId(java.math.BigDecimal value);
    /*********************MCM Impl Team XS_2.36 changes ends*********************/

    /*********************MCM Impl Team XS_2.36 (dynamic drop down lists) changes starts******************/
    /**
     * This method declaration returns the value of field the dfPricingrateinventoryid.
     * 
     * @return the dfPricingrateinventoryid
     */
    public BigDecimal getDfPricingrateinventoryid();

    /**
     * This method declaration sets the the dfPricingrateinventoryid.
     * 
     * @param value  the new dfPricingrateinventoryid
     */
    public void setDfPricingrateinventoryid(BigDecimal value);
    /*********************MCM Impl Team XS_2.36 (dynamic drop down lists) changes ends******************/


}
