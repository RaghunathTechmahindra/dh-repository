package mosApp.MosSystem;


import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Vector;
import MosSystem.Sc;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.CreditBureauReport;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.DatasetModel;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.util.TypeConverter;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildDisplayEvent;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;


//============================================================================================
//Developer Note:
//
//  1) Use PageCondition3 to switch display different Cancel Button
//      -- false (default, first entry) ==> display 'Cancel' button
//      -- true (refresh screen by add/delete sub records, e.g. Liability, asset ...
//            or recalculate button pressed ==> display 'Cancel Current Changes Only button.
//      -- By BILLY 03May2002
//============================================================================================
/**
 * <p>
 * Title: ApplicantEntryHandler
 * </p>
 * 
 * <p>
 * Description: Handler for Applicant Entry screen
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.1
 * Change: Modifications to handle Identification Type <br>
 * 		
 *	Added methods for <br>
 *	- Added following methods <br>
 *		filterApplicantEntryIdentificationDataSet(
 *			Integer borrowerid, Integer cspAppCPId)
 *
 *		setApplicantIdentificationDisplayFields(
 *			QueryModelBase dobject, int rowNum)
 *
 *	- Modified following methods <br>
 *		populatePageDisplayFields()
 *		setupBeforePageGeneration()
 *		SaveEntity(ViewBean webPage) 
 *		
 */
public class ApplicantEntryHandler extends DealHandlerCommon  implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public ApplicantEntryHandler cloneSS()
	{
		return(ApplicantEntryHandler) super.cloneSafeShallow();

	}


	//This method is used to populate the fields in the header
	//with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();
		populatePageShellDisplayFields();

		// Quick Link Menu display
	    displayQuickLinkMenu();
	    
		populateTaskNavigator(pg);

		populatePreviousPagesLinks();
		
		ViewBean thePage = getCurrNDPage();

	    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		
		thePage.setDisplayFieldValue(
				"stTargetIdentification", new String(TARGET_TAG));
		
	    //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
		
		thePage.setDisplayFieldValue("stTargetAddress", new String(TARGET_TAG));

		thePage.setDisplayFieldValue("stTargetEmployment", new String(TARGET_TAG));

		thePage.setDisplayFieldValue("stTargetIncome", new String(TARGET_TAG));

		thePage.setDisplayFieldValue("stTargetCredit", new String(TARGET_TAG));

		thePage.setDisplayFieldValue("stTargetAsset", new String(TARGET_TAG));

		thePage.setDisplayFieldValue("stTargetLiab", new String(TARGET_TAG));

		thePage.setDisplayFieldValue("stTargetCreditBureauLiab", new String(TARGET_TAG));
        
        

		// Handle CreditBureau Report button -- By BILLY 10Jan2002
		generateBureauViewButton(pg);
		populateDefaultsVALSData(thePage);
		populateFieldValidation(thePage);

    //--BMO_MI_CR--start//
    //// To provide proper parse of page values to Entity (via db property) for the page.
    //// The basic method parsePageValuesToEntity is totally reconsidered and there is no
    //// need for this setting anymore.
    //pg.setPageCondition9(true);
    //--BMO_MI_CR--end//

    if (PropertiesCache.getInstance().getProperty
                    (theSessionState.getDealInstitutionId(), 
                    "com.basis100.ldinsurance.displaylifedisabilitymodule", "N").equals("N"))
    {
      startSupressContent("stIncludeLifeDisLabelsStart", thePage);
      endSupressContent("stIncludeLifeDisLabelsEnd", thePage);
    }
        
        /***** FFATE start *****/
        int liabilitySize = getSubViewModelSize(getCurrNDPage(), "RepeatedLiabilities");
        thePage.setDisplayFieldValue("stLiabilityTileSize", ""+liabilitySize);
        int creditBureauSize = getSubViewModelSize(getCurrNDPage(), "RepeatedCreditBureauLiabilities");
        thePage.setDisplayFieldValue("stCreditBureauTileSize", ""+creditBureauSize);
        /***** FFATE end *****/
	}


	/**
	 *
	 *
	 */




  private boolean EARLY_EXECUTE_SUB_VIEW_MODELS = true; // do we execute model for sub-views in main view?

	private void populateDefaultsVALSData(ViewBean thePage)
	{
    String valsData = "";

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedEmployment") > 0)
      valsData = populateDefaultsVALSDataRepeatedEployment(thePage, valsData);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedOtherIncome") > 0)
      valsData = populateDefaultsVALSDataRepeatedOtherIncome(thePage, valsData);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedAssets") > 0)
      valsData = populateDefaultsVALSDataRepeatedAssets(thePage, valsData);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedLiabilities") > 0)
      valsData = populateDefaultsVALSDataRepeatedLiabilities(thePage, valsData);

    //--> Temp fix to avoid getting PickList everytime displaying the screen.
    //--> The valsData is initialized and cached in the static Variable VALS_DATA
    //--> all valsData setup is commented as //>>>
    //--> By Billy 02Dec2003
		// Setup Income Preoid arrays for calculation of TotalIncome (Calc 53)
		//>>>valsData += "VALScbIncomePeriod = new Array(" + PicklistData.getColumnValuesCDV("INCOMEPERIOD", "INCOMEPERIODID") + ");" + "\n";
		//>>>valsData += "VALSIncomeMultiplier = new Array(" + PicklistData.getColumnValuesCDV("INCOMEPERIOD", "ANNUALINCOMEMULTIPLIER") + ");" + "\n";

		//thePage.setDisplayFieldValue("stVALSData", new String(valsData));
    thePage.setDisplayFieldValue("stVALSData", VALS_DATA);
    //=============================================================================
	}

  private int getSubViewModelSize(ViewBean thePage, String subViewName)
  {
      try
      {
        DatasetModel model = ((RequestHandlingTiledViewBase)thePage.getChild(subViewName)).getPrimaryModel();
        
        int foo = model.getSize();

        return foo; //model.getSize();
      }
      catch (Exception e)
      {
        logger.debug("AEH@getSubViewModelSize:: Unexpected exception getting model size for view = " + subViewName + ", exception = " + e);
      }

      return 0;
  }


  private String populateDefaultsVALSDataRepeatedEployment(ViewBean thePage, String valsData)
  {
		String onChange = "";

		// the OnChange string to be set on the ComboBox
		HtmlDisplayFieldBase df;

		// The VAL String to be populated on the HTML

    //--> Temp fix to avoid getting PickList everytime displaying the screen.
    //--> The valsData is initialized and cached in the static Variable VALS_DATA
    //--> all valsData setup is commented as //>>>
    //--> By Billy 02Dec2003
		// Set IncomeType array
		//>>>valsData += "VALScbIncomeType = new Array(" + PicklistData.getColumnValuesCDV("INCOMETYPE", "INCOMETYPEID") + ");" + "\n";
		// Set EmployPercentageIncludeInGDS array
		//>>>valsData += "VALStxEmployPercentageIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "GDSINCLUSION") + ");" + "\n";
		// Set Hidden % values for double default -- By BILLY 23Feb2001
    ///// BJH - avoid duplication
		///// valsData += "VALShdEmployPercentageIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "GDSINCLUSION") + ");" + "\n";
		//>>>valsData += "VALShdEmployPercentageIncludeInGDS = VALStxEmployPercentageIncludeInGDS;" + "\n";
		// Set EmployIncludeInGDS array
		//>>>valsData += "VALScbEmployIncludeInGDS = new Array(" + PicklistData.getValuesCDS("INCOMETYPE", "GDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
		// Set EmployPercentageIncludeInTDS array
		//>>>valsData += "VALStxEmployPercentageIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "TDSINCLUSION") + ");" + "\n";
		// Set Hidden % values for double default -- By BILLY 23Feb2001
		///// BJH - avoid duplication
    ///// valsData += "VALShdEmployPercentageIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "TDSINCLUSION") + ");" + "\n";
		//>>>valsData += "VALShdEmployPercentageIncludeInTDS = VALStxEmployPercentageIncludeInTDS;" + "\n";
		// Set EmployIncludeInTDS array
		//>>>valsData += "VALScbEmployIncludeInTDS = new Array(" + PicklistData.getValuesCDS("INCOMETYPE", "TDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
    //======================================================================================
    // Setup OnChange event handling to populate default fields
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/cbIncomeType");
		onChange = "onChange=\"setDefaults('cbIncomeType', " + "['txEmployPercentageIncludeInGDS', 'hdEmployPercentageIncludeInGDS', 'cbEmployIncludeInGDS'," + "'txEmployPercentageIncludeInTDS', 'hdEmployPercentageIncludeInTDS', 'cbEmployIncludeInTDS']" + ");" + "calcBorrowerTotalIncome();\"";
		//Online calc 53 -- by BILLY 02Feb2001

		df.setExtraHtml(onChange);

		// Setup OnChange event handling to populate default fields for cbEmployIncludeInGDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/cbEmployIncludeInGDS");
		onChange = "onChange=\"setValueIfMatched(['txEmployPercentageIncludeInGDS'], 'N', '0');"
    // Handle double default -- by BILLY 23Feb2001
    + "setValueIfMatchedFromOtherField(['txEmployPercentageIncludeInGDS'], 'Y', 'hdEmployPercentageIncludeInGDS');"
    + "calcBorrowerTotalIncome();\"";
		//Online calc 53 -- by BILLY 02Feb2001
		df.setExtraHtml(onChange);


		// Setup OnChange event handling to populate default fields for cbEmployIncludeInTDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/cbEmployIncludeInTDS");
		onChange = "onChange=\"setValueIfMatched(['txEmployPercentageIncludeInTDS'], 'N', '0');"
    // Handle double default -- by BILLY 23Feb2001
    + "setValueIfMatchedFromOtherField(['txEmployPercentageIncludeInTDS'], 'Y', 'hdEmployPercentageIncludeInTDS');"
    + "calcBorrowerTotalIncome();\"";
		//Online calc 53 -- by BILLY 02Feb2001
		df.setExtraHtml(onChange);

		// Setup onchange event to populate Employer Name to Income Description -- By BILLY 17Jan2001
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerName");
		onChange = "onBlur=\"populateValueTo(['txIncomeDesc']);\"";
		df.setExtraHtml(onChange);

		// Setup onchange event to Disable GDS, TDS if Previous Employment -- By BILLY 31Jan2001
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/cbEmploymentStatus");
		onChange = "onChange=\"setValueAndDisableIfMatched(['txEmployPercentageIncludeInGDS','txEmployPercentageIncludeInTDS'], " + "'1', " + "'0');" + "setValueAndDisableIfMatched(['cbEmployIncludeInGDS','cbEmployIncludeInTDS'], '1', 'N');" + "calcBorrowerTotalIncome();\"";
		//Online calc 53 -- by BILLY 02Feb2001
		df.setExtraHtml(onChange);

		// Setup onchange event to for Online calc 53 -- By BILLY 02Feb2001
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txIncomeAmount");
		onChange = "onBlur=\"if( isFieldInDecRange(0, 99999999999.99) && isFieldDecimal(2) ) calcBorrowerTotalIncome();\"";
		// Added field valdiation -- by BILLY 05Feb2001
		df.setExtraHtml(onChange);

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/cbIncomePeriod");
		onChange = "onChange=\"calcBorrowerTotalIncome();\"";
		df.setExtraHtml(onChange);

    //--> Temp fix to avoid getting PickList everytime displaying the screen.
    //--> The valsData is initialized and cached in the static Variable VALS_DATA
    //--> all valsData setup is commented as //>>>
    //--> By Billy 02Dec2003
		// ========== Set JobTitle default values (Begin) ==========
		// Set JobTitle (ComboBox) ID array
		//>>>valsData += "VALScbJobTitle = new Array(" + PicklistData.getColumnValuesCDV("JOBTITLE", "JOBTITLEID") + ");" + "\n";
		// Set JobTitle (TextBox) array
		//>>>valsData += "VALStxJobTitle = new Array(" + PicklistData.getColumnValuesCDS("JOBTITLE", "JOBTITLEDESCRIPTION") + ");" + "\n";
		// Set JobTitle (Hidden) array
    //// BJH Avoid repeating job title array (takes approx 3700 bytes! - and is identical!)
		//// valsData += "VALShdJobTitle = new Array(" + PicklistData.getColumnValuesCDS("JOBTITLE", "JOBTITLEDESCRIPTION") + ");" + "\n";
    //>>>valsData += "VALShdJobTitle = VALStxJobTitle;" + "\n";
		// Set Occupation array (in decimal string fromat e.g. '0.0', '1.0', ...)
    //--> It is not necessary to format is as '0.0' in JATO ==> should use 0 (integer) instead -- By Billy 29July2002
		//valsData += "VALScbOccupation = new Array(" + PicklistData.getColumnValuesCDDS("JOBTITLE", "OCCUPATIONID") + ");" + "\n";
    //>>>valsData += "VALScbOccupation = new Array(" + PicklistData.getColumnValuesCDV("JOBTITLE", "OCCUPATIONID") + ");" + "\n";
		// Set IndustrySector array (in decimal string fromat e.g. '0.0', '1.0', ...)
		//valsData += "VALScbIndustrySector = new Array(" + PicklistData.getColumnValuesCDDS("JOBTITLE", "INDUSTRYSECTORID") + ");" + "\n";
    //>>>valsData += "VALScbIndustrySector = new Array(" + PicklistData.getColumnValuesCDV("JOBTITLE", "INDUSTRYSECTORID") + ");" + "\n";
    //=========================================================================================


		// Setup OnChange event handling to populate default fields
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/cbJobTitle");
		onChange = "onChange=\"setDefaults('cbJobTitle', " + "['hdJobTitle','txJobTitle','cbOccupation','cbIndustrySector']" + "); "
    + "setValueAndDisableIfNotMatched(['txJobTitle'], '0', '');\"";
		df.setExtraHtml(onChange);

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/cbJobTitle");
		onChange = "onChange=\"setDefaults('cbJobTitle', " + "['hdJobTitle','txJobTitle','cbOccupation','cbIndustrySector']" + ");\"";
		df.setExtraHtml(onChange);

		// Setup OnChange event handling to populate the textbox value to hidden
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txJobTitle");
		onChange = "onChange=\"populateValueTo(['hdJobTitle']);\"";
		df.setExtraHtml(onChange);
		// ========== Set JobTitle default values (End) ==========
	    		
	    df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerEmailAddress");
	    onChange = "onBlur=\"isEmail();\"";
		df.setExtraHtml(onChange);

    return valsData;
  }

  private String populateDefaultsVALSDataRepeatedOtherIncome(ViewBean thePage, String valsData)
  {
		String onChange = "";

		// the OnChange string to be set on the ComboBox
		HtmlDisplayFieldBase df;

    //--> Temp fix to avoid getting PickList everytime displaying the screen.
    //--> The valsData is initialized and cached in the static Variable VALS_DATA
    //--> all valsData setup is commented as //>>>
    //--> By Billy 02Dec2003
		// Set IncomeType array
		//>>>valsData += "VALScbOtherIncomeType = new Array(" + PicklistData.getColumnValuesCDV("INCOMETYPE", "INCOMETYPEID") + ");" + "\n";
		// Set OtherIncomePercentIncludeInGDS array
		//>>>valsData += "VALStxOtherIncomePercentIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "GDSINCLUSION") + ");" + "\n";
		// Set Hidden % values for double default -- By BILLY 23Feb2001
    ///// BJH - avoid duplication
		///// valsData += "VALShdOtherIncomePercentIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "GDSINCLUSION") + ");" + "\n";
		//>>>valsData += "VALShdOtherIncomePercentIncludeInGDS = VALStxOtherIncomePercentIncludeInGDS;" + "\n";
		// Set OtherIncomeIncludeInGDS array
		//>>>valsData += "VALScbOtherIncomeIncludeInGDS = new Array(" + PicklistData.getValuesCDS("INCOMETYPE", "GDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
		// Set OtherIncomePercentIncludeInTDS array
		//>>>valsData += "VALStxOtherIncomePercentIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "TDSINCLUSION") + ");" + "\n";
		// Set Hidden % values for double default -- By BILLY 23Feb2001
    ///// BJH - avoid duplication
		///// valsData += "VALShdOtherIncomePercentIncludeInTDS = VALStxOtherIncomePercentIncludeInTDS;" + "\n";
		//>>>valsData += "VALShdOtherIncomePercentIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "TDSINCLUSION") + ");" + "\n";
		// Set OtherIncomeIncludeInTDS array
		//>>>valsData += "VALScbOtherIncomeIncludeInTDS = new Array(" + PicklistData.getValuesCDS("INCOMETYPE", "TDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
    //==========================================================================================

    // Setup OnChange event handling to populate default fields
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedOtherIncome/cbOtherIncomeType");

		onChange = "onChange=\"setDefaults('cbOtherIncomeType', " + "['txOtherIncomePercentIncludeInGDS','hdOtherIncomePercentIncludeInGDS','cbOtherIncomeIncludeInGDS'," + "'txOtherIncomePercentIncludeInTDS','hdOtherIncomePercentIncludeInTDS','cbOtherIncomeIncludeInTDS']" + ");" + "calcBorrowerTotalIncome();\"";
		//Online calc 53 -- by BILLY 02Feb2001
		df.setExtraHtml(onChange);

		// Setup OnChange event handling to populate default fields for cbOtherIncomeIncludeInGDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedOtherIncome/cbOtherIncomeIncludeInGDS");
		onChange = "onChange=\"setValueIfMatched(['txOtherIncomePercentIncludeInGDS'], 'N', '0');"
    // Handle double default -- by BILLY 23Feb2001
    + "setValueIfMatchedFromOtherField(['txOtherIncomePercentIncludeInGDS'], 'Y', 'hdOtherIncomePercentIncludeInGDS');"
    + "calcBorrowerTotalIncome();\"";
		//Online calc 53 -- by BILLY 02Feb2001
		df.setExtraHtml(onChange);

		// Setup OnChange event handling to populate default fields for cbEmployIncludeInTDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedOtherIncome/cbOtherIncomeIncludeInTDS");
		onChange = "onChange=\"setValueIfMatched(['txOtherIncomePercentIncludeInTDS'], 'N', '0');"
// Handle double default -- by BILLY 23Feb2001
    + "setValueIfMatchedFromOtherField(['txOtherIncomePercentIncludeInTDS'], 'Y', 'hdOtherIncomePercentIncludeInTDS');"
    + "calcBorrowerTotalIncome();\"";
		//Online calc 53 -- by BILLY 02Feb2001
		df.setExtraHtml(onChange);


		// Setup onchange event to for Online calc 53 -- By BILLY 02Feb2001
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedOtherIncome/txOtherIncomeAmount");
		onChange = "onBlur=\"if( isFieldInDecRange(0, 99999999999.99) && isFieldDecimal(2) ) calcBorrowerTotalIncome();\"";
		// Added field valdiation -- by BILLY 05Feb2001
		df.setExtraHtml(onChange);

		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedOtherIncome/cbOtherIncomePeriod");
		onChange = "onChange=\"calcBorrowerTotalIncome();\"";
		df.setExtraHtml(onChange);

    return valsData;
  }

  private String populateDefaultsVALSDataRepeatedAssets(ViewBean thePage, String valsData)
  {
		String onChange = "";

		// the OnChange string to be set on the ComboBox
		HtmlDisplayFieldBase df;

    //--> Temp fix to avoid getting PickList everytime displaying the screen.
    //--> The valsData is initialized and cached in the static Variable VALS_DATA
    //--> all valsData setup is commented as //>>>
    //--> By Billy 02Dec2003
		// Set AssetType array
		//>>valsData += "VALScbAssetType = new Array(" + PicklistData.getColumnValuesCDV("ASSETTYPE", "ASSETTYPEID") + ");" + "\n";
		// Set PercentageIncludedInNetworth array
		//>>valsData += "VALStxPercentageIncludedInNetworth = new Array(" + PicklistData.getColumnValuesCDS("ASSETTYPE", "NETWORTHINCLUSION") + ");" + "\n";
		// Handle double default -- by BILLY 23Feb2001
    ///// BJH - avoid duplication
		///// valsData += "VALShdPercentageIncludedInNetworth = new Array(" + PicklistData.getColumnValuesCDS("ASSETTYPE", "NETWORTHINCLUSION") + ");" + "\n";
		//>>valsData += "VALShdPercentageIncludedInNetworth = VALStxPercentageIncludedInNetworth;" + "\n";
		// Set AssetIncludeInNetworth array
		//>>valsData += "VALScbAssetIncludeInNetworth = new Array(" + PicklistData.getValuesCDS("ASSETTYPE", "NETWORTHINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
    //=============================================================================================

		// Setup OnChange event handling to populate default fields
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedAssets/cbAssetType");
		onChange = "onChange=\"setDefaults('cbAssetType', " + "['txPercentageIncludedInNetworth','hdPercentageIncludedInNetworth','cbAssetIncludeInNetworth']" + ");\"";
		df.setExtraHtml(onChange);

		// Setup OnChange event handling to populate default fields for cbAssetIncludeInNetworth
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedAssets/cbAssetIncludeInNetworth");
		onChange = "onChange=\"setValueIfMatched(['txPercentageIncludedInNetworth'], 'N', '0');"
    // Handle double default -- by BILLY 23Feb2001
    + "setValueIfMatchedFromOtherField(['txPercentageIncludedInNetworth'], 'Y', 'hdPercentageIncludedInNetworth');"
    + "calcBorrowerTotalAsset();\"";
		df.setExtraHtml(onChange);

    return valsData;
  }

  private String populateDefaultsVALSDataRepeatedLiabilities(ViewBean thePage, String valsData)
  {
		String onChange = "";

		// the OnChange string to be set on the ComboBox
		HtmlDisplayFieldBase df;

    //--> Temp fix to avoid getting PickList everytime displaying the screen.
    //--> The valsData is initialized and cached in the static Variable VALS_DATA
    //--> all valsData setup is commented as //>>>
    //--> By Billy 02Dec2003
		// ========== Set Liability default values (Begin) ==========
		// Set LiabilityType array
		//>>>valsData += "VALScbLiabilityType = new Array(" + PicklistData.getColumnValuesCDV("LIABILITYTYPE", "LIABILITYTYPEID") + ");" + "\n";
		// Set LiabilityPercentageIncludeInGDS array
		//>>>valsData += "VALStxLiabilityPercentageIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("LIABILITYTYPE", "GDSINCLUSION") + ");" + "\n";
		// Handle double default -- by BILLY 23Feb2001
    ///// BJH avoid duplication
		///// valsData += "VALShdLiabilityPercentageIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("LIABILITYTYPE", "GDSINCLUSION") + ");" + "\n";
		//>>>valsData += "VALShdLiabilityPercentageIncludeInGDS = VALStxLiabilityPercentageIncludeInGDS;" + "\n";
		// Set LiabilityIncludeInGDS array
		//>>>valsData += "VALScblLiabilityIncludeInGDS = new Array(" + PicklistData.getValuesCDS("LIABILITYTYPE", "GDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
		// Set LiabilityPercentageIncludeInTDS array
		//>>>valsData += "VALStxLiabilityPercentageIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("LIABILITYTYPE", "TDSINCLUSION") + ");" + "\n";
		// Handle double default -- by BILLY 23Feb2001
    //// BJH - Avoid duplication
		//// valsData += "VALShdLiabilityPercentageIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("LIABILITYTYPE", "TDSINCLUSION") + ");" + "\n";
		//>>>valsData += "VALShdLiabilityPercentageIncludeInTDS = VALStxLiabilityPercentageIncludeInTDS;" + "\n";
		// Set LiabilityIncludeInTDS array
		//>>>valsData += "VALScblLiabilityIncludeInTDS = new Array(" + PicklistData.getValuesCDS("LIABILITYTYPE", "TDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
		// Set LiabilityPaymentQualifier array for online calc LiabilityMonthlyPayment (calc 72 and 55)
		//  -- by BILLY 01Feb2001
		//>>>valsData += "VALSliabilityPaymentQualifier = new Array(" + PicklistData.getColumnValuesCDS("LIABILITYTYPE", "LIABILITYPAYMENTQUALIFIER") + ");" + "\n";
    //=============================================================================

		// Setup OnChange event handling to populate default fields
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedLiabilities/cbLiabilityType");
		onChange = "onChange=\"setDefaults('cbLiabilityType', " + "['txLiabilityPercentageIncludeInGDS','hdLiabilityPercentageIncludeInGDS','cblLiabilityIncludeInGDS'," + "'txLiabilityPercentageIncludeInTDS','hdLiabilityPercentageIncludeInTDS','cblLiabilityIncludeInTDS']" + ");" + "calcLiabilityMonthlyPaymentAndTotal();\"";
		//For online calc 72. 55 -- BILLY 01Feb2001
		df.setExtraHtml(onChange);

		// Setup OnChange event handling to populate default fields for cblLiabilityIncludeInGDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedLiabilities/cblLiabilityIncludeInGDS");
		onChange = "onChange=\"setValueIfMatched(['txLiabilityPercentageIncludeInGDS'], 'N', '0');"
    // Handle double default -- by BILLY 23Feb2001
    + "setValueIfMatchedFromOtherField(['txLiabilityPercentageIncludeInGDS'], 'Y', 'hdLiabilityPercentageIncludeInGDS');\"";
		df.setExtraHtml(onChange);

		// Setup OnChange event handling to populate default fields for cblLiabilityIncludeInTDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedLiabilities/cblLiabilityIncludeInTDS");
		onChange = "onChange=\"setValueIfMatched(['txLiabilityPercentageIncludeInTDS'], 'N', '0');"
    // Handle double default -- by BILLY 23Feb2001
    + "setValueIfMatchedFromOtherField(['txLiabilityPercentageIncludeInTDS'], 'Y', 'hdLiabilityPercentageIncludeInTDS');"
    + "calcLiabilityMonthlyPaymentAndTotal();\"";
		//For online calc 72. 55 -- BILLY 01Feb2001
		df.setExtraHtml(onChange);

		// Setup OnChange event handling for online calc 72, 55 (txLiabilityPercentageIncludeInTDS)-- BILLY 01Feb2001
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedLiabilities/txLiabilityPercentageIncludeInTDS");
		onChange = "onBlur=\"if( isFieldDecimal(2) ) {if( isFieldInDecRange(0, 100) ) calcLiabilityMonthlyPaymentAndTotal();}\"";
		df.setExtraHtml(onChange);

		// Setup OnChange event handling for online calc 72, 55 (txLiabilityAmount) -- BILLY 01Feb2001
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedLiabilities/txLiabilityAmount");
		onChange = "onBlur=\"if( isFieldInDecRange(0, 99999999999.99) && isFieldDecimal(2) ) {calcLiabilityMonthlyPaymentAndTotal();"
      + "addForm('pgApplicantEntry_RepeatedLiabilities[0]_txLiabilityAmount', 'txTotalLiab');}\"";
		df.setExtraHtml(onChange);

		// Setup OnChange event handling for online calc 72, 55 (txLiabilityMonthlyPayment) -- BILLY 01Feb2001
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedLiabilities/txLiabilityMonthlyPayment");
		onChange = "onBlur=\"if( isFieldInDecRange(0, 99999999999.99) && isFieldDecimal(2) ) calcLiabilityMonthlyPaymentAndTotal();\"";
		df.setExtraHtml(onChange);
        
    return valsData;
  }

	/**
	 *
	 *
	 */
	private void populateFieldValidation(ViewBean thePage)
	{
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

		// Validation for Applicant data
		//  Validate Middle Initial
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txMiddleInitials");
		theExtraHtml = "onBlur=\"this.value = this.value.toUpperCase();\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Applicant's DOB
		// Set the compare date to today
		DateFormat theFormat = new SimpleDateFormat("MMM dd yyyy");
		String theDateCompare = theFormat.format(new java.util.Date());

		//  Validate Month
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("cbApplicantDOBMonth");
		theExtraHtml = "onBlur=\"isFieldValidDateInRange('txApplicantDOBYear', 'cbApplicantDOBMonth', 'txApplicantDOBDay', '', '" + theDateCompare + "');\"";
		df.setExtraHtml(theExtraHtml);

		//  Validate Day
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantDOBDay");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + "isFieldValidDateInRange('txApplicantDOBYear', 'cbApplicantDOBMonth', 'txApplicantDOBDay', '', '" + theDateCompare + "');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Year
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantDOBYear");
		theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " + "isFieldValidDateInRange('txApplicantDOBYear', 'cbApplicantDOBMonth', 'txApplicantDOBDay', '', '" + theDateCompare + "');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate SIN #
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txSINNo");
		theExtraHtml = "onBlur=\"isFieldInteger(9);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate # of Dependants
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txNoOfDependants");
		theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
		df.setExtraHtml(theExtraHtml);

		// Valdiate # of Bankrupt
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txNoOfTimesBankrupt");
		theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Home Phone # : AreaCode
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantHomePhoneAreaCode");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Home Phone # : Exchange
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantHomePhoneExchange");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Home Phone # : Route
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantHomePhoneRoute");
		theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
		df.setExtraHtml(theExtraHtml);

		// Valdiate Work Phone # : AreaCode
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantWorkPhoneAreaCode");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Work Phone # : Exchange
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantWorkPhoneExchange");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Work Phone # : Route
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantWorkPhoneRoute");
		theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Work Phone # : Extention
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantWorkPhoneExt");
		theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate FAX Phone # : AreaCode
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantFaxAreaCode");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate FAX Phone # : Exchange
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantFaxExchange");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate FAX Phone # : Route
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantFaxRoute");
		theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
		df.setExtraHtml(theExtraHtml);
        
        // Validate CELL Phone # : AreaCode
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantCellAreaCode");
        theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(theExtraHtml);

        // Validate CELL Phone # : Exchange
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantCellExchange");
        theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
        df.setExtraHtml(theExtraHtml);

        // Validate CELL Phone # : Route
        df = (HtmlDisplayFieldBase)thePage.getDisplayField("txApplicantCellRoute");
        theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
        df.setExtraHtml(theExtraHtml);

		//FXP26805, 4.2GR, Oct 21, 2009 -- start
		// Validate Reference Client #
		// df = (HtmlDisplayFieldBase)thePage.getDisplayField("txReferenceClientNo");
		// theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
		// df.setExtraHtml(theExtraHtml);
		//FXP26805, 4.2GR, Oct 21, 2009 -- end

		// Validate CreditBureau Summary
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("txCreditBureauSummary");
		// Make field readonly -- By BILLY 04March2002
		//theExtraHtml="onBlur=\"isFieldMaxLength(2000);\"";
		theExtraHtml = "readonly";

		df.setExtraHtml(theExtraHtml);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedApplicantAddress") > 0)
	    populateFieldValidationRepeatedApplicantAddress(thePage);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedEmployment") > 0)
	    populateFieldValidationRepeatedEmployment(thePage);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedOtherIncome") > 0)
	    populateFieldValidationRepeatedOtherIncome(thePage);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedAssets") > 0)
	    populateFieldValidationRepeatedAssets(thePage);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedCreditReference") > 0)
	    populateFieldValidationRepeatedCreditReference(thePage);

    if (EARLY_EXECUTE_SUB_VIEW_MODELS == false || getSubViewModelSize(thePage, "RepeatedLiabilities") > 0)
	    populateFieldValidationRepeatedLiabilities(thePage);

		// Convert the Total Text Boxes to UnEditable
		customizeTextBox("txTotalIncome");
		customizeTextBox("txTotalAsset");
		customizeTextBox("txTotalMonthlyLiab");
		customizeTextBox("txTotalLiab");
		customizeTextBox("txCreditBureauTotalLiab");
		customizeTextBox("txCreditBureauTotalMonthlyLiab");
		customizeTextBox("txCreditScore");
	}

/**
 * @version 1.4  <br>
 * Date: 06/27/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  Added the display logic for <br>
 * - txStreetName <br>
 * Modified the display logic for <br>
 * - txApplicantAddressLine1, to make it read-only <br>
 * - txStreetName, to add JavaScript to check if txStreetName field is blank
 */

	private void populateFieldValidationRepeatedApplicantAddress(ViewBean thePage)
  {
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;
		//		***** Change by NBC Impl. Team - Version 1.3 - Start *****//
		//		Changed ApplicantAddressLine1 to read only field
		customizeTextBox("RepeatedApplicantAddress/txApplicantAddressLine1");

	
		// Validate City
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedApplicantAddress/txApplicantCity");
    ////theExtraHtml = "onBlur=\"isFieldAlpha();\"";
    //// Very temp to test.
    theExtraHtml = "onBlur=\"isFieldHasSpecialChar();\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Postal Code : FSA
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedApplicantAddress/txApplicantPostalCodeFSA");
		theExtraHtml = "onBlur=\"isFieldFSA();\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Postal Code : LDU
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedApplicantAddress/txApplicantLDU");
		theExtraHtml = "onBlur=\"isFieldLDU();\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Time at Residence : Yrs.
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedApplicantAddress/txApplicantTimeYears");
		theExtraHtml = "onBlur=\"if(isFieldInteger(0)) " + "isFieldValidDuration('txApplicantTimeYears', 'txApplicantTimeMonths');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Time at Residence : Mths.
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedApplicantAddress/txApplicantTimeMonths");
		theExtraHtml = "onBlur=\"if(isFieldInteger(0)) " + "isFieldValidDuration('txApplicantTimeYears', 'txApplicantTimeMonths');\"";
		df.setExtraHtml(theExtraHtml);
  }

	private void populateFieldValidationRepeatedLiabilities(ViewBean thePage)
  {
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

		// Validation for Liabilities
		// Note : Validation functions of "Liability Amount", "Liability Monthly Payment",
		//        "% included in TDS" were set in the populateDefaultsVALSData method.
		// Validate % Included in GDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedLiabilities/txLiabilityPercentageIncludeInGDS");
		theExtraHtml = "onBlur=\"if( isFieldDecimal(2) ) isFieldInDecRange(0, 100);\"";
		df.setExtraHtml(theExtraHtml);
  }

	private void populateFieldValidationRepeatedCreditReference(ViewBean thePage)
  {
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

		// Note : "Income Amount" were done in the populateDefaultsVALSData method.
		// Validation for Credit Reference
		// Validate Time with Reference : Yrs.
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedCreditReference/txTimeWithRefYears");
		theExtraHtml = "onBlur=\"if(isFieldInteger(0)) " + "isFieldValidDuration('txTimeWithRefYears', 'txTimeWithRefMonths');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Time with Reference : Mths.
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedCreditReference/txTimeWithRefMonths");
		theExtraHtml = "onBlur=\"if(isFieldInteger(0)) " + "isFieldValidDuration('txTimeWithRefYears', 'txTimeWithRefMonths');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Current Balance
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedCreditReference/txCurrentBalance");
		theExtraHtml = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
		df.setExtraHtml(theExtraHtml);
  }

	private void populateFieldValidationRepeatedAssets(ViewBean thePage)
  {
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

		// Validation for Assets
		// Validate Asset Value
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedAssets/txAssetValue");
		theExtraHtml = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99) && isFieldDecimal(2)) calcBorrowerTotalAsset();\"";
		df.setExtraHtml(theExtraHtml);

		// Validate % Included in Net Worth
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedAssets/txPercentageIncludedInNetworth");
		theExtraHtml = "onBlur=\"isFieldInIntRange(0, 100);\"";
		df.setExtraHtml(theExtraHtml);
  }

	private void populateFieldValidationRepeatedOtherIncome(ViewBean thePage)
  {
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

		// Note : Validation functions of "Income Amount" were set in the populateDefaultsVALSData method.
		// Validation for Other Income
		// Validate OtherIncome % Included in GDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedOtherIncome/txOtherIncomePercentIncludeInGDS");
		theExtraHtml = "onBlur=\"isFieldInIntRange(0, 100);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate OtherIncome % Included in TDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedOtherIncome/txOtherIncomePercentIncludeInTDS");
		theExtraHtml = "onBlur=\"isFieldInIntRange(0, 100);\"";
		df.setExtraHtml(theExtraHtml);
  }


	private void populateFieldValidationRepeatedEmployment(ViewBean thePage)
  {
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

		// Validation for Employment
		// Validate Employer Phone # : AreaCode
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerPhoneAreaCode");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Employer Phone # : Exchange
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerPhoneExchange");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Employer Phone # : Route
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerPhoneRoute");
		theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Employer Phone # : Extention
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerPhoneExt");
		theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Employer Fax # : AreaCode
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerFaxAreaCode");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Employer Fax # : Exchange
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerFaxExchange");
		theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Employer Fax # : Route
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerFaxRoute");
		theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Employer City
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerCity");
		theExtraHtml = "onBlur=\"isFieldAlpha();\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Postal Code : FSA
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerPostalCodeFSA");
		theExtraHtml = "onBlur=\"isFieldFSA();\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Postal Code : LDU
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployerPostalCodeLDU");
		theExtraHtml = "onBlur=\"isFieldLDU();\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Time at job : Yrs.
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txTimeAtJobYears");
		theExtraHtml = "onBlur=\"if(isFieldInteger(0)) " + "isFieldValidDuration('txTimeAtJobYears', 'txTimeAtJobMonths');\"";
		df.setExtraHtml(theExtraHtml);

		// Validate Time at job : Mths.
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txTimeAtJobMonths");
		theExtraHtml = "onBlur=\"if(isFieldInteger(0)) " + "isFieldValidDuration('txTimeAtJobYears', 'txTimeAtJobMonths');\"";
	  df.setExtraHtml(theExtraHtml);

		// Validate Employer % Included in GDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployPercentageIncludeInGDS");
		theExtraHtml = "onBlur=\"isFieldInIntRange(0, 100);\"";
		df.setExtraHtml(theExtraHtml);


		// Validate Employer % Included in TDS
		df = (HtmlDisplayFieldBase)thePage.getDisplayField("RepeatedEmployment/txEmployPercentageIncludeInTDS");
		theExtraHtml = "onBlur=\"isFieldInIntRange(0, 100);\"";
		df.setExtraHtml(theExtraHtml);
  }


	//
	// Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	//

	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();
		if (pg.getSetupBeforeGenerationCalled() == true) return;
		pg.setSetupBeforeGenerationCalled(true);

		// -- //
		int dealId = pg.getPageDealId();
		if (dealId <= 0)
		{
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Problem encountered displaying deal Entry - invalid dealId (" + dealId + ")");
			return;
		}

		Integer borrowerid = new Integer(pg.getPageCriteriaId1());
		Integer cspDealId = new Integer(dealId);
		Integer cspAppCPId = new Integer(theSessionState.getCurrentPage().getPageDealCID());

		filterApplicantEntryDataSet(borrowerid, cspAppCPId);
		filterApplicantEntryGSDTSD(cspDealId, cspAppCPId);

    // setup data objects for tiled views

    filterApplicantEntryAssetDataSet(borrowerid,cspAppCPId);
    filterApplicantEntryEmploymentDataSet(borrowerid,cspAppCPId);
    filterApplicantEntryCreditRefDataSet(borrowerid,cspAppCPId);
    filterApplicantEntryAddressDataSet(borrowerid,cspAppCPId);
    filterApplicantEntryLiabilitiesDataSet(borrowerid,cspAppCPId);
    filterApplicantEntryOtherIncomeDataSet(borrowerid,cspAppCPId);
    //--> Bug Fixed : Billy 30April2003
    filterApplicantCreditBureauLiabilities(borrowerid,cspAppCPId);
    //=================================
    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
    filterApplicantEntryIdentificationDataSet(borrowerid,cspAppCPId);
    //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
    // total credit bureau liabilities (from already executed model)
		double totalCreditBureauLiab = countTotalBureauLiabFromDo(0);

		double monthlyCreditBureauLiab = countTotalBureauLiabFromDo(1);

		pg.setTotalCreditBureauLiab(totalCreditBureauLiab);

		pg.setMonthlyCreditBureauLiab(monthlyCreditBureauLiab);

		// Handle CreditBureau Report table setup -- By BILLY 10Jan2002
		Hashtable pst = pg.getPageStateTable();

		if (pst == null)
		{
			pst = new Hashtable();
			pg.setPageStateTable(pst);
		}

		setupCreditBureauReportsTable(pst);

	}

	/**
	 *  Execute select operation on provided model. If <positionModel> == true the model is positioned to the first
   *  row in the model. For models used in a non-repeating context (e.g. associated with a non-repeating view where
   *  only one row of data is to be displayed on the view/page) this consistent with the JATO behavior supported
   *  for auto-executed models. In general for a non-repeating view we want to be positioned at the row to be
   *  displayed on the view. This is true even if there are multiple rows in the underlying result set (of the
   *  primary model for the view) - here we would use the sql execution offset to specify the row offset to
   *  correspond to the 'page' to be displayed.
   *
   *  For models used with tiled views we would NOT typically position the model to the first row in the returned
   *  data. This is because the display logic (e.g. see tag UseTiledView) explicitly controls positioning of the
   *  tiles (and associated data model rows.)
   *
   *  Hence when preparing data models for usage in JATO display logic the following applies:
   *
   *  . non-tiled view -> position at the row to be displayed
   *  . tiled view -> position model in "before first" state (e.g. m.beforeFirst(), usually default after execution)
   *
	 */
  /**
  private void executeModel(SelectQueryModel model, String methodInfoForLog, boolean positionModel)
  {
    try
    {
      ResultSet rs = model.executeSelect(null);  // model will be positioned in "before first" state

      //if(rs == null || model.getSize() <= 0)
        //logger.debug(methodInfoForLog + "::No row returned!!");

      //else if(rs != null && model.getSize() > 0)
        //logger.debug(methodInfoForLog + "::Size of the result set: " + model.getSize());

      if (positionModel == true)
        model.next();
    }
    catch (ModelControlException mce)
    {
      logger.debug(methodInfoForLog + "::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.debug(methodInfoForLog + "::SQLException: " + sqle);
    }
    catch (Exception e)
    {
      logger.debug(methodInfoForLog + "::Unexpected exception executing (or positioning) model, e = " + e);
    }

  }
  **/

	/**
	 *
	 *
	 */
	private void filterApplicantEntryDataSet(Integer borrowerid, Integer cspAppCPId)
	{

		//doApplicantEntryApplicantSelect
		doApplicantEntryApplicantSelectModelImpl dodealappentrymain =(doApplicantEntryApplicantSelectModelImpl)
        (RequestManager.getRequestContext().getModelManager().getModel(doApplicantEntryApplicantSelectModel.class));

		dodealappentrymain.clearUserWhereCriteria();
		dodealappentrymain.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
        dodealappentrymain.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);

    executeModel(dodealappentrymain, "AEH@filterApplicantEntryDataSet", true);

    // used in non-repeating context - pass default 0 row ndx which defines the first (single) row in this case.
    setApplicantMainDisplayFields(dodealappentrymain, 0);
	}


	/**
	 *
	 *
	 */
	private void filterApplicantEntryGSDTSD(Integer cspDealId, Integer cspAppCPId)
	{
    // GDS/TDS section
		doApplicantGSDTSDModelImpl doGDSTDS =(doApplicantGSDTSDModelImpl)
      (RequestManager.getRequestContext().getModelManager().getModel(doApplicantGSDTSDModel.class));

		doGDSTDS.clearUserWhereCriteria();
		doGDSTDS.addUserWhereCriterion("dfDealId", "=", cspDealId);
		doGDSTDS.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);

    executeModel(doGDSTDS, "AEH@filterApplicantEntryGSDTSD", true);
	}


	/**
	 *
	 *
	 */
	private void setApplicantMainDisplayFields(QueryModelBase dobject, int rowIndx)
	{
    fillupMonths("cbApplicantDOBMonth");
		setDateDisplayField(dobject, "cbApplicantDOBMonth", "txApplicantDOBDay", "txApplicantDOBYear", "dfBirthDate", rowIndx);

		setPhoneNoDisplayField(dobject, "txApplicantHomePhoneAreaCode", "txApplicantHomePhoneExchange", "txApplicantHomePhoneRoute", "dfHomePhone", rowIndx);
		setPhoneNoDisplayField(dobject, "txApplicantWorkPhoneAreaCode", "txApplicantWorkPhoneExchange", "txApplicantWorkPhoneRoute", "dfWorkPhone", rowIndx);
		setPhoneNoDisplayField(dobject, "txApplicantFaxAreaCode", "txApplicantFaxExchange", "txApplicantFaxRoute", "dfFaxNumber", rowIndx);
        setPhoneNoDisplayField(dobject, "txApplicantCellAreaCode", "txApplicantCellExchange", "txApplicantCellRoute", "dfCellNumber", rowIndx);

    setCreditScoreLabel(dobject, "stCreditScoreLabel", "dfCreditBureauNameId");

    // Setting up labels of CreditBureau / SOB Liabilities
		//    -- By BILLY 13May2002
		setLiabilitiesLabels();

	}


	public void filterApplicantEntryAddressDataSet(Integer borrowerid, Integer cspAppCPId)
	{
    doApplicantEntryAddressSelectModelImpl dObject =
    (doApplicantEntryAddressSelectModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doApplicantEntryAddressSelectModel.class));

    dObject.clearUserWhereCriteria();
		dObject.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
		dObject.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);
		dObject.addUserWhereCriterion("dfAddrCopyId", "=", cspAppCPId);

    executeModel(dObject, "AEH@filterApplicantEntryAddressDataSet", false);
	}

	public void filterApplicantEntryEmploymentDataSet(Integer borrowerid, Integer cspAppCPId)
	{
    doApplicantEmploymentIncomeSelectModelImpl dObject =
    (doApplicantEmploymentIncomeSelectModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doApplicantEmploymentIncomeSelectModel.class));

    dObject.clearUserWhereCriteria();
    dObject.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
    dObject.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);
    dObject.addUserWhereCriterion("dfContactCopyId", "=", cspAppCPId);
    dObject.addUserWhereCriterion("dfAddrCopyId", "=", cspAppCPId);
    dObject.addUserWhereCriterion("dfIncomeCopyId", "=", cspAppCPId);

    executeModel(dObject, "AEH@filterApplicantEntryEmploymentDataSet", false);
	}

	/**
	 *
	 *
	 */
	public void setApplicantEmploymentDisplayFields(QueryModelBase dobject, int rowNum)
	{
    setPhoneNoDisplayField(dobject, "RepeatedEmployment/txEmployerPhoneAreaCode",
                                    "RepeatedEmployment/txEmployerPhoneExchange",
                                    "RepeatedEmployment/txEmployerPhoneRoute",
                                    "dfEmployerPhoneNo",
                                    rowNum);

		setPhoneNoDisplayField(dobject, "RepeatedEmployment/txEmployerFaxAreaCode",
                                    "RepeatedEmployment/txEmployerFaxExchange",
                                    "RepeatedEmployment/txEmployerFaxRoute",
                                    "dfEmployerFaxNo",
                                    rowNum);

		setTermsDisplayField(dobject, "RepeatedEmployment/txTimeAtJobYears", "RepeatedEmployment/txTimeAtJobMonths", "dfTimeAtJob", rowNum);
  }

	public void filterApplicantEntryAssetDataSet(Integer borrowerid, Integer cspAppCPId)
	{
    doApplicantAssetsSelectModelImpl dObject =
    (doApplicantAssetsSelectModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doApplicantAssetsSelectModel.class));

    dObject.clearUserWhereCriteria();
    dObject.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
    dObject.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);

    executeModel(dObject, "AEH@filterApplicantEntryAssetDataSet", false);
	}

    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	

	public void setApplicantIdentificationDisplayFields(QueryModelBase dobject,
			int rowNum) {
	}

	/**
	 * <p>
	 * This method filters identification type on the basis of borrower id copy
	 * id
	 * </p>
	 * 
	 * @param borrowerid
	 * @param cspAppCPId
	 */
	public void filterApplicantEntryIdentificationDataSet(Integer borrowerid,
			Integer cspAppCPId) {
		
		doApplicantIdentificationTypeModelImpl dObject = 
				(doApplicantIdentificationTypeModelImpl) (RequestManager
				.getRequestContext().getModelManager()
				.getModel(doApplicantIdentificationTypeModel.class));

		dObject.clearUserWhereCriteria();
		dObject.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
		dObject.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);

		executeModel(dObject, "AEH@filterApplicantEntryIdentificationDataSet",
				false);
	}
    // ***** Change by NBC Impl. Team - Version 1.1 - End *****//

	public void filterApplicantEntryCreditRefDataSet(Integer borrowerid, Integer cspAppCPId)
	{
    doApplicantCreditRefsSelectModelImpl dObject =
    (doApplicantCreditRefsSelectModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doApplicantCreditRefsSelectModel.class));

    dObject.clearUserWhereCriteria();
		dObject.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
		dObject.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);

    executeModel(dObject, "AEH@filterApplicantEntryCreditRefDataSet", false);
	}

	/**
	 *
	 *
	 */
	public void setApplicantCreditRefDisplayFields(QueryModelBase dobject, int rowNum)
	{
		setTermsDisplayField(dobject, "RepeatedCreditReference/txTimeWithRefYears", "RepeatedCreditReference/txTimeWithRefMonths", "dfTimeWithReference", rowNum);

	}

	/**
	 *
	 *
	 */
	public void setApplicantAddressDisplayFields(QueryModelBase dobject, int rowNum)
	{
    setTermsDisplayField(dobject, "RepeatedApplicantAddress/txApplicantTimeYears", "RepeatedApplicantAddress/txApplicantTimeMonths", "dfMonthsAtAddress", rowNum);

	}


	public void filterApplicantEntryLiabilitiesDataSet(Integer borrowerid, Integer cspAppCPId)
	{
    doApplicantLiabilitiesSelectModelImpl dObject =
    (doApplicantLiabilitiesSelectModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doApplicantLiabilitiesSelectModel.class));

    dObject.clearUserWhereCriteria();
		dObject.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
		dObject.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);

    //dfPercentOutGDS: null, 0, or 1 for the Liability section now.
    // value 0 means the creation in the Liability section, value 1
    // indicated the export from the Credit Bureau Liability section.

		Integer creditBureauBoundary = new Integer(1);
    dObject.addUserWhereCriterion("dfPercentOutGDS", "<=", creditBureauBoundary);

    executeModel(dObject, "AEH@filterApplicantEntryLiabilitiesDataSet", false);
	}

	public void filterApplicantCreditBureauLiabilities(Integer borrowerid, Integer cspAppCPId)
	{
    doApplicantCreditBureauLiabilitiesModelImpl dObject =
    (doApplicantCreditBureauLiabilitiesModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doApplicantCreditBureauLiabilitiesModel.class));

    dObject.clearUserWhereCriteria();
    dObject.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
    dObject.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);

    //dfPercentOutGDS: 1, 2 or 3 for the Credit Bureau Liability section now
		Integer creditBureauLiabExpModified = new Integer(1);
    dObject.addUserWhereCriterion("dfPercentOutGDS", ">=", creditBureauLiabExpModified);

    executeModel(dObject, "AEH@filterApplicantCreditBureauLiabilities", false);
	}

	/**
	 *
	 *
	 */
	private void populateRowsDisplayed(ViewBean NDPage, PageCursorInfo tds)
	{
		String rowsRpt = "";

		if (tds.getTotalRows() > 0) rowsRpt = "Displaying Credit Bureau Liabilities records " + tds.getFirstDisplayRowNumber() + "-" + tds.getLastDisplayRowNumber() + " of " + tds.getTotalRows();

		NDPage.setDisplayFieldValue("creditBureauLiabDisplayed", new String(rowsRpt));

	}


 	public void filterApplicantEntryOtherIncomeDataSet(Integer borrowerid, Integer cspAppCPId)
	{
    doApplicantOtherIncomesSelectModelImpl dObject =
    (doApplicantOtherIncomesSelectModelImpl)(RequestManager.getRequestContext().getModelManager().getModel(doApplicantOtherIncomesSelectModel.class));

    dObject.clearUserWhereCriteria();
    dObject.addUserWhereCriterion("dfBorrowerId", "=", borrowerid);
    dObject.addUserWhereCriterion("dfCopyId", "=", cspAppCPId);

    /*

      Summary of original two part query other incomes query:

        1) Get borrower incomes (income ids) that are linked to employment records
           (e.g. SELECT INCOMEID FROM EMPLOYMENTHISTORY WHERE BORROWERID = " + bid
        2) Select borrower incomes excluding those linked to employment records
           (e.g. SELECT {Income} where {match bId, cId} and incomeIds not in (list from 1}
        ... actually result of 2) used to set criteria of data object - not direct select

      Now accomplished by more sophisticated query (usage of outer joins with employment history
      table.
    */

    executeModel(dObject, "AEH@filterApplicantEntryOtherIncomeDataSet", false);
	}


	/**
	 *
	 *
	 */
	public void SaveEntity(ViewBean webPage)
		throws Exception
	{
		logger.trace("--T--> @ApplicantEntryHandler.SaveEntity");

		CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());

		;

		try
		{
			String [ ] appkeys =
			{
				"hdApplicantId", "hdApplicantCopyId"			}
			;
			handleUpdateApplicant(APPLICANT_PROP_FILE_NAME, appkeys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] appaddreskeys =
			{
				"RepeatedApplicantAddress/appAddressId", "RepeatedApplicantAddress/appAddressCopyId"			}
			;
			handleUpdateApplicantAddress(APPADDRESS_PROP_FILE_NAME, appaddreskeys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] appaddresAddr =
			{
				"RepeatedApplicantAddress/appAddressAddrId", "RepeatedApplicantAddress/appAddressAddrCopyId"			}
			;
			handleUpdateApplicantAddressAddr(APPADDRESS_ADDR_PROP_FILE_NAME, appaddresAddr, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] otherIncKeys =
			{
				"RepeatedOtherIncome/hdOtherIncomeId", "RepeatedOtherIncome/hdOtherIncomeCopyId"			}
			;
			handleUpdateOtherIncome(OTHRINCOME_PROP_FILE_NAME, otherIncKeys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] creditRefKeys =
			{
				"RepeatedCreditReference/hdCreditRefId", "RepeatedCreditReference/hdCreditRefCopyId"			}
			;
			handleUpdateCreditRef(CREDITREF_PROP_FILE_NAME, creditRefKeys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] assetKeys =
			{
				"RepeatedAssets/hdAssetId", "RepeatedAssets/hdAssetCopyId"			}
			;
			handleUpdateAsset(ASSET_PROP_FILE_NAME, assetKeys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] liabKeys =
			{
				"RepeatedLiabilities/hdLiabilityId", "RepeatedLiabilities/hdLiabilityCopyId"			}
			;
			handleUpdateLiability(LIABILITY_PROP_FILE_NAME, liabKeys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] keys =
			{
				"RepeatedEmployment/hdEmploymentId", "RepeatedEmployment/hdEmploymentCopyId"			}
			;
			handleUpdateEmployment(EMPLOYMENT_PROP_FILE_NAME, keys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] keys =
			{
				"RepeatedEmployment/hdContactId", "RepeatedEmployment/hdContactCopyId"			}
			;
			handleUpdateEmploymentContact(EMP_CONTACT_PROP_FILE_NAME, keys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] keys =
			{
				"RepeatedEmployment/hdIncomeId", "RepeatedEmployment/hdIncomeCopyId"			}
			;
			handleUpdateEmploymentIncome(EMP_INCOME_PROP_FILE_NAME, keys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		try
		{
			String [ ] keys =
			{
				"RepeatedEmployment/hdAddrId", "RepeatedEmployment/hdAddrCopyId"			}
			;
			handleUpdateEmploymentAddr(EMP_ADDR_PROP_FILE_NAME, keys, dcm);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}

		//  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		try {
			String[] keys = { "RepeatedIdentification/hdIdentificationId",
					"RepeatedIdentification/hdIdentificationCopyId" };
			handleUpdateIdentification(IDENTIFICATION_PROP_FILE_NAME, keys, dcm);
		} catch (Exception ex) {
			logger.error(ex);
			throw new Exception("Exception @SaveEntity");
		}
		// ***** Change by NBC Impl. Team - Version 1.1 - End *****//

		try
		{
			doCalculation(dcm);
		}
		catch(Exception ex)
		{
			logger.error("Exception occurred @doCalculation");
			throw new Exception("Exception @SaveEntity");
		}

	}


	/**
	 *
	 *
	 */
	public boolean getRowDisplaySetting(String dobject)
	{
    //logger.trace("@getRowDisplayStringFor Property IN do "+dobject);
		try
		{
			PageEntry pg = theSessionState.getCurrentPage();
			Integer cspBorrowerid = new Integer(pg.getPageCriteriaId1());
			Integer cspCopyId = new Integer(pg.getPageDealCID());
			QueryModelBase chkDoObject =(QueryModelBase)(RequestManager.getRequestContext().getModelManager().getModel(dobject));
			chkDoObject.clearUserWhereCriteria();
			chkDoObject.addUserWhereCriterion("dfBorrowerId", "=", cspBorrowerid);
			chkDoObject.addUserWhereCriterion("dfCopyId", "=", cspCopyId);

      ResultSet rs = chkDoObject.executeSelect(null);
      if(rs != null && chkDoObject.getSize() > 0)
			  return true;
      else
			  return false;
		}
		catch(Exception e)
		{
			logger.error("Exception @getRowDisplayString For Applicant");
			logger.error(e);
			return false;
		}

	}


	/**
	 *
	 *
	 */
	public boolean getRowDisplaySetting(QueryModelBase chkDoObject)
	{

		//logger.trace("@getRowDisplayStringFor Property IN do "+dobject);
		try
		{
			PageEntry pg = theSessionState.getCurrentPage();
			Integer cspBorrowerid = new Integer(pg.getPageCriteriaId1());
			Integer cspCopyId = new Integer(pg.getPageDealCID());
			chkDoObject.clearUserWhereCriteria();
			chkDoObject.addUserWhereCriterion("dfBorrowerId", "=", cspBorrowerid);
			chkDoObject.addUserWhereCriterion("dfCopyId", "=", cspCopyId);

      ResultSet rs = chkDoObject.executeSelect(null);
      if(rs != null && chkDoObject.getSize() > 0)
			  return true;
      else
			  return false;
		}
		catch(Exception e)
		{
			logger.error("Exception @getRowDisplayString For Applicant");
			logger.error(e);
			return false;
		}

	}


	/**
	 *
	 *
	 */
	public boolean getRowDisplaySetting(QueryModelBase chkDoObject, String dfCopyIdName)
	{

		//logger.trace("@getRowDisplayStringFor Property IN do "+dobject);
		try
		{
			PageEntry pg = theSessionState.getCurrentPage();
			Integer cspBorrowerid = new Integer(pg.getPageCriteriaId1());
			Integer cspCopyId = new Integer(pg.getPageDealCID());
			chkDoObject.clearUserWhereCriteria();
			chkDoObject.addUserWhereCriterion("dfBorrowerId", "=", cspBorrowerid);
			chkDoObject.addUserWhereCriterion("dfCopyId", "=", cspCopyId);
			chkDoObject.addUserWhereCriterion(dfCopyIdName, "=", cspCopyId);

			ResultSet rs = chkDoObject.executeSelect(null);
      if(rs != null && chkDoObject.getSize() > 0)
			  return true;
      else
			  return false;
		}
		catch(Exception e)
		{
			logger.error("Exception @getRowDisplayString For Applicant");
			logger.error(e);
			return false;
		}

	}


	/**
	 *
	 *
	 */
	public double countTotalBureauLiabFromDo(int flag)
	{
		SessionResourceKit srk = getSessionResourceKit();
		SysLogger logger = srk.getSysLogger();

		doApplicantCreditBureauLiabilitiesModelImpl doCountCreditBureauLiab = (doApplicantCreditBureauLiabilitiesModelImpl)
      (RequestManager.getRequestContext().getModelManager().getModel(doApplicantCreditBureauLiabilitiesModel.class));

		double totalLiab = 0.0;
		double monthlyLiab = 0.0;

    try
    {
      if (doCountCreditBureauLiab.getSize() <= 0)
        return 0;

      doCountCreditBureauLiab.beforeFirst();

      while (doCountCreditBureauLiab.next())
      {
        totalLiab += TypeConverter.asDouble(doCountCreditBureauLiab.getValue(doCountCreditBureauLiab.FIELD_DFLIABILITYAMOUNT));
        monthlyLiab += TypeConverter.asDouble(doCountCreditBureauLiab.getValue(doCountCreditBureauLiab.FIELD_DFLIABILITYMONTHLYPAYMENT));
      }

      doCountCreditBureauLiab.beforeFirst();
    }
    catch(Exception e)
    {
      logger.error("Exception @ApplicantEntry.countTotalBureauLiabFromDo when executing model : " + e.toString());
    }

		if (flag == 0)
			return totalLiab;

		if (flag == 1)
			return monthlyLiab;

		return 0.0; // exception actually!
	}


	/**
	 *
	 *
	 */
	protected void setCreditBureauLiabTotal(ViewBean pg, ChildDisplayEvent event)
	{
		PageEntry currPage = getTheSessionState().getCurrentPage();

		String creditBureauLiabTotal = "txCreditBureauTotalLiab";

		double cbLiabTotal = currPage.getTotalCreditBureauLiab();


		//0. reset the static field
		//pg.setDisplayFieldValue(creditBureauLiabTotal, new String(""));
    pg.setDisplayFieldValue(creditBureauLiabTotal, new Double(cbLiabTotal));
    //This is not working because the getSource will return the TAG object
    //--> Billy 26July2002
		//DisplayField field =(DisplayField) event.getSource();
    //field.setValue(new Double(cbLiabTotal));

		logger.debug("---D---->AEH.setCreditBureauLiabTotal@ setting the final value");

	}


	/**
	 *
	 *
	 */
	protected void setCreditBureauLiabMonthly(ViewBean pg, ChildDisplayEvent event)
	{
  	PageEntry currPage = theSessionState.getCurrentPage();

		String creditBureauLiabMonthly = "txCreditBureauTotalMonthlyLiab";

		double cbLiabMonthly = currPage.getMonthlyCreditBureauLiab();


		//0. reset the static field
		pg.setDisplayFieldValue(creditBureauLiabMonthly, new Double(cbLiabMonthly));

		//DisplayField field =(DisplayField) event.getSource();

		//field.setValue(new Double(cbLiabMonthly));

		logger.debug("---D---->AEH.setCreditBureauLiabMonthly@ setting the final value");

	}


    /**
     * setup display item and validation for Liabilities section
     * @param dobject
     * @param rowNum
     */
    public void setLiabilitySection(QueryModelBase dobject, int rowNum) {

        fillupMonths("RepeatedLiabilities/"
                + pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.CHILD_CBMONTHS);
        
        setDateDisplayField(
                dobject,
                "RepeatedLiabilities/"
                        + pgApplicantEntryRepeatedLiabilitiesTiledView.CHILD_CBMONTHS,
                "RepeatedLiabilities/"
                        + pgApplicantEntryRepeatedLiabilitiesTiledView.CHILD_TBDAY,
                "RepeatedLiabilities/"
                        + pgApplicantEntryRepeatedLiabilitiesTiledView.CHILD_TBYEAR,
                doApplicantLiabilitiesSelectModelImpl.FIELD_DFMATURITYDATE,
                rowNum);
        
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("RepeatedLiabilities/"
                    + pgApplicantEntryRepeatedLiabilitiesTiledView.CHILD_CBMONTHS);
        String jsValid = " onBlur=\"isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
        df.setExtraHtml(jsValid);
    
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("RepeatedLiabilities/"
                    + pgApplicantEntryRepeatedLiabilitiesTiledView.CHILD_TBDAY);
        jsValid = " onBlur=\"if(isFieldInIntRange(1, 31)) " +
            "isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
        df.setExtraHtml(jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("RepeatedLiabilities/"
                    + pgApplicantEntryRepeatedLiabilitiesTiledView.CHILD_TBYEAR);
        jsValid = " onBlur=\"if(isFieldInIntRange(1800, 2100)) " +
            "isFieldValidDate('tbYear', 'cbMonths', 'tbDay');\"";
        df.setExtraHtml(jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("RepeatedLiabilities/"
                + pgApplicantEntryRepeatedLiabilitiesTiledView.CHILD_TXCREDITBUREAULIABILITYLIMIT);
        jsValid = " onChange=\"isFieldInDecRange(0, 99999999999.99);\"";
        String jsChange = " onBlur=\"isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid + jsChange);
        
        ((StaticTextField) getCurrNDPage().getDisplayField("RepeatedLiabilities/stTileIndex")).setValue("" + rowNum);
    }

	/**
	 *
	 *
	 */
	public void setCreditBureauLiabilitySection(QueryModelBase dobject, int rowNum)
	{

		// Set html layout for each row.
		setCreditBureauLiabilityRow(dobject, "RepeatedCreditBureauLiabilities/txCreditBureauLiabilityType",
      "RepeatedCreditBureauLiabilities/chCreditBureauIndicator",
      "RepeatedCreditBureauLiabilities/txCreditBureauLiabilityDesc",
      "RepeatedCreditBureauLiabilities/txCreditBureauLiabilityLimit",
      "RepeatedCreditBureauLiabilities/txCreditBureauLiabilityAmount",
      "RepeatedCreditBureauLiabilities/txCreditBureauLiabilityMonthlyPayment",
      "RepeatedCreditBureauLiabilities/chCreditBureauLiabilityExport", rowNum);
        
      ((StaticTextField) getCurrNDPage().getDisplayField("RepeatedCreditBureauLiabilities/stTileIndex")).setValue("" + rowNum);
	}


	/**
	 *
	 *
	 */
	protected void setCreditBureauLiabilityRow(QueryModelBase dobject,
            String txLiabType, String chIndicator, String txLiabDesc,  String txLiabLimit,
            String txLiabAmount, String txLiabMnthAmount,
            String chLiabilityExport, int rowNum)
	{
		
        fillupMonths("RepeatedCreditBureauLiabilities/" + pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.CHILD_CBMONTHS);
        setDateDisplayField(dobject, "RepeatedCreditBureauLiabilities/" + pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.CHILD_CBMONTHS, 
                "RepeatedCreditBureauLiabilities/" + pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.CHILD_TBDAY, 
                "RepeatedCreditBureauLiabilities/" + pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.CHILD_TBYEAR, 
                doApplicantCreditBureauLiabilitiesModelImpl.FIELD_DFMATURITYDATE, rowNum);
        
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("RepeatedCreditBureauLiabilities/" + pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.CHILD_CBMONTHS);
        String disableEdition = new String(" disabled");
        df.setExtraHtml(disableEdition);

        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("RepeatedCreditBureauLiabilities/" + pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.CHILD_TBDAY);
        df.setExtraHtml(disableEdition);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().
            getDisplayField("RepeatedCreditBureauLiabilities/" + pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView.CHILD_TBYEAR);
        df.setExtraHtml(disableEdition);
        
        // Getting the corresponding row data from DataObject based on the flag value.
		int correspondence = TypeConverter.asInt(dobject.getValue("dfPercentOutGDS"), 0);

		switch(correspondence)
		{
			case 1 : customizeTextBox(txLiabType);
            closeAccessToCheckBox(chIndicator);
			customTextBoxWithCourier(txLiabDesc);
            customizeTextBox(txLiabLimit);
			customizeTextBox(txLiabAmount);
			customizeTextBox(txLiabMnthAmount);
			closeAccessToCheckBox(chLiabilityExport);
			break;
			case 2 : closeAccessToTextBox(txLiabType);
            closeAccessToCheckBox(chIndicator);
			noAccessToTextBoxWithCourier(txLiabDesc);
            customizeTextBox(txLiabLimit);
			closeAccessToTextBox(txLiabAmount);
			closeAccessToTextBox(txLiabMnthAmount);
			ndCheckBoxLayout(chLiabilityExport);
			break;
			case 3 : customizeTextBox(txLiabType);
            closeAccessToCheckBox(chIndicator);
			customTextBoxWithCourier(txLiabDesc);
            customizeTextBox(txLiabLimit);
			customizeTextBox(txLiabAmount);
			customizeTextBox(txLiabMnthAmount);
			exportedBackChbLayout(chLiabilityExport);
			break;
		}

	}
    
    
	/**
	 *
	 *
	 */
	protected void ndTextBoxLayout(String tbName)
	{
		try
		{
			TextField textbox =(TextField) getCurrNDPage().getDisplayField(tbName);
			String defaultColorStyle = new String("style=\"background-color:ffffff\"");
			textbox.setExtraHtml(defaultColorStyle);
      //--> Cannot find the corresponding method in JATO.
      //--> However, I don't it is necessary to call this anyway.
      //--> Comment by BILLY 24July2002
			//getCurrNDPage().clearBeforeDisplay();
      //================================================
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception ApplicantEntryHandler@ndTextBoxLayout: Problem encountered in combobox customization." + tbName);
		}

	}


	/**
	 *
	 *
	 */
	protected void exportedBackChbLayout(String checkboxName)
	{
		try
		{
			CheckBox checkbox =(CheckBox) getCurrNDPage().getDisplayField(checkboxName);
			String defaultColorStyle = new String("style=\"background-color:ffffff\"");
			checkbox.setExtraHtml(defaultColorStyle);
      //--> Cannot find the corresponding method in JATO.
      //--> However, I don't it is necessary to call this anyway.
      //--> Comment by BILLY 24July2002
			//getCurrNDPage().clearBeforeDisplay();
      //================================================
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception ApplicantEntryHandler@ndCheckBoxLayout: Problem encountered in combobox customization." + checkboxName);
		}

	}


	/**
	 *
	 *
	 */
	protected void ndCheckBoxLayout(String checkboxName)
	{
		try
		{
			CheckBox checkbox =(CheckBox) getCurrNDPage().getDisplayField(checkboxName);
			String defaultColorStyle = new String("");
			checkbox.setExtraHtml(defaultColorStyle);
			//--> Cannot find the corresponding method in JATO.
      //--> However, I don't it is necessary to call this anyway.
      //--> Comment by BILLY 24July2002
			//getCurrNDPage().clearBeforeDisplay();
      //================================================
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception ApplicantEntryHandler@ndCheckBoxLayout: Problem encountered in combobox customization." + checkboxName);
		}

	}


	// For setting up label of Credit Score based on CreditBureauNameId
	//    -- By BILLY 10Jan2002

	protected void setCreditScoreLabel(QueryModelBase dobject, String labelDisplay, String dfName)
	{
		try
		{
			int cbNameId = TypeConverter.asInt(dobject.getValue(dfName), Sc.CREDIT_BUREAU_NAME_NONE);
      //--Release2.1--//
      //// All multilingual labeles should be treanslated via BXResources.
			switch(cbNameId)
			{
        case Sc.CREDIT_BUREAU_NAME_EQUIFAX : getCurrNDPage().setDisplayFieldValue(labelDisplay,
                                              new String(BXResources.getGenericMsg("CREDIT_SCORE_LABEL_EQUIFAX", getTheSessionState().getLanguageId())));
				break;
				case Sc.CREDIT_BUREAU_NAME_TRANSUNION : getCurrNDPage().setDisplayFieldValue(labelDisplay,
                                                new String(BXResources.getGenericMsg("CREDIT_SCORE_LABEL_TRANSUNION", getTheSessionState().getLanguageId())));
				break;
				default : getCurrNDPage().setDisplayFieldValue(labelDisplay,
                                  new String(BXResources.getGenericMsg("CREDIT_SCORE_LABEL_NONE", getTheSessionState().getLanguageId())));
				break;
			}
		}
		catch(Exception e)
		{
			logger.error("Exception @setCreditScoreLabel: df=" + dfName + "), exception=" + e.getMessage());
		}

	}


	// Methods to handle CredirBureau Report Pop-up window
	//  -- By BILLY 10Jan2002

	private void setupCreditBureauReportsTable(Hashtable pst)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			CreditBureauReport report = new CreditBureauReport(srk);
			Vector reports = new Vector();
			Vector vect =(Vector)(report.findByDeal(new DealPK(pg
					.getPageDealId(), pg.getPageDealCID())));
			for(int i = 0; i < vect.size();	++ i)
      reports.add(((CreditBureauReport)(vect.get(i))).getCreditReport());
			setRowGeneratorDOForNRows(reports.size());
			pst.put("reportVector", reports);
		}
		catch(Exception e)
		{
			logger.error("Exception @ApplicantEntryHandler.setupCreditBureauReportsTable()");
			logger.error(e);
			setStandardFailMessage();
			return;
		}

	}


	public boolean populateBureauReport(int ind)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		Vector reports =(Vector) pst.get("reportVector");

		if (reports == null || reports.size() < 1)
		{
			return false;
		}


		// ALERT :: Commented this oout to not doing anything before we fixing the Data problem of the Bureau from Morty
		//          The Report content may contain some hidden char e.g. Char(0) ==> caused JavaScript problems.
		//getCurrNDPage().setDisplayFieldValue("Repeated2.stCreditBureauReport", new String( (String)(reports.get(ind)) ) );
		String tmpStr =(String)(reports.get(ind));




		//tmpStr = this.replaceString("\0", tmpStr, "\\0");
		//logger.debug("BILLY ==> The Orig Str = " + tmpStr);
		//tmpStr = StringTokenizer2.replace(tmpStr, "\0", "\\0");
		tmpStr = convertToHtmString(tmpStr);






		//logger.debug("BILLY ==> The Str After convert = " + tmpStr);
		getCurrNDPage().setDisplayFieldValue("rptCreditBureauReports/stCreditBureauReport", new String(tmpStr));

		return true;

	}

	//--Release2.1--start//
  //// This button image must be generated based on the languageId. The String
  //// generation is moved inside the method.
	////final static String bureauViewButton="<a href=\"javascript:createBureauWindow();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"/images/BureauViewButton.gif\" width=170 height=16 alt=\"\" border=\"0\"></a>";
	//--Release2.1--end//


	/**
	 *
	 *
	 */
	private void generateBureauViewButton(PageEntry pg)
	{
		StaticTextField button =(StaticTextField) getCurrNDPage().getDisplayField("stBureauViewButton");
		//button.setHtmlEscaping(false);

    //--Release2.1--start//
    //// This button image must be generated now based on the languageId.
    String bureauViewButton = "";
    if (theSessionState.getLanguageId() == 0) { //// English
      bureauViewButton="<a href=\"javascript:createBureauWindow();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"../images/BureauViewButton.gif\" width=170 height=16 alt=\"\" border=\"0\"></a>";    }
    if (theSessionState.getLanguageId() == 1) { //// French
      bureauViewButton="<a href=\"javascript:createBureauWindow();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"../images/BureauViewButton_fr.gif\" width=220 height=16 alt=\"\" border=\"0\"></a>";    }
    //--Release2.1--end//

		button.setValue(new String(bureauViewButton));
	}

	// Added GDS TDS Section and recalculate button -- By BILLY 08April2002
	// Basically do nothing - just a refresh
	public void handleRecalculate()
	{
		PageEntry pg = theSessionState.getCurrentPage();
		pg.setLoadTarget(TARGET_APP_LIAB);

		// Set to display 'Cancel Current Changes' button -- By BILLY 03May2002
		pg.setPageCondition3(true);
		if (disableEditButton(pg) == true) return;
	}


	//===============================================================
	// Method to return the display status of the Cancel button
	//  Input : type (Button type)
	//            - 1 (Cancel)
	//            - 2 (Cancel current changes only)
	// Note : getPageCondition3() == false means first entry
	//                            == true means refresh within page
	//
	//  By Billy 03May2002
	//================================================================
  //--> Changed to return boolean -- By BILLY 24July2002
	public boolean displayCancelButton(int type)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		if (type == 1 && pg.getPageCondition3() == true)
		{
			return false;
		}
		if (type == 2 && pg.getPageCondition3() == false)
		{
			return false;
		}
		return displayCancelButton();
	}


	// For setting up labels of CreditBureau / SOB Liabilities
	//    -- By BILLY 13May2002
  //--Release2.1--//
  //// All multilingual labels should be taken from the BXResources.
	protected void setLiabilitiesLabels()
	{
		if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(), 
                                                      "com.basis100.deal.usecreditbureauasdealliabilities", "N").equals("N"))
		{
			// Set labels for CreditBureau Liabilities
      String liabLabel = BXResources.getGenericMsg("CB_LIABILITY_LABEL", getTheSessionState().getLanguageId());
      String totalLiabLabel = BXResources.getGenericMsg("CB_TOTAL_LIABILITY_LABEL", getTheSessionState().getLanguageId());
      String monthPmntLabel = BXResources.getGenericMsg("CB_MONTH_PAYMENT_LABEL", getTheSessionState().getLanguageId());

			getCurrNDPage().setDisplayFieldValue("stLiabiliesLabel", new String(liabLabel));
			getCurrNDPage().setDisplayFieldValue("stTotalLiabiliesLabel", new String(totalLiabLabel));
			getCurrNDPage().setDisplayFieldValue("stMonthPaymentLabel", new String(monthPmntLabel));
		}
		else
		{
			// Set labels for SOB Liabilities
      String sobLiabLabel = BXResources.getGenericMsg("SOB_LIABILITY_LABEL", getTheSessionState().getLanguageId());
      String sobTotalLiabLebel = BXResources.getGenericMsg("SOB_TOTAL_LIABILITY_LABEL", getTheSessionState().getLanguageId());
      String sobMonthPmntLabel = BXResources.getGenericMsg("SOB_MONTH_PAYMENT_LABEL", getTheSessionState().getLanguageId());

			getCurrNDPage().setDisplayFieldValue("stLiabiliesLabel", new String(sobLiabLabel));
			getCurrNDPage().setDisplayFieldValue("stTotalLiabiliesLabel", new String(sobTotalLiabLebel));
			getCurrNDPage().setDisplayFieldValue("stMonthPaymentLabel", new String(sobMonthPmntLabel));

		}
	}

  //=====================================================================================================
  //--> Temp fix to avoid getting PickList everytime displaying the screen.
  //--> By Billy 02Dec2003
  // Define the static variable to store the JavaScript Array values
  private static String VALS_DATA = "";
  // To initial the JavaScript Array values when the Class loaded up the first time user access the screen
  static
  {
System.out.println("BILLY ===> Begin the setup VALS_DATA for ApplicantEntryHandler !!");
    // from populateDefaultsVALSDataRepeatedEployment
    VALS_DATA += "VALScbIncomeType = new Array(" + PicklistData.getColumnValuesCDV("INCOMETYPE", "INCOMETYPEID") + ");" + "\n";
    VALS_DATA += "VALStxEmployPercentageIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "GDSINCLUSION") + ");" + "\n";
    VALS_DATA += "VALShdEmployPercentageIncludeInGDS = VALStxEmployPercentageIncludeInGDS;" + "\n";
		VALS_DATA += "VALScbEmployIncludeInGDS = new Array(" + PicklistData.getValuesCDS("INCOMETYPE", "GDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
		VALS_DATA += "VALStxEmployPercentageIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "TDSINCLUSION") + ");" + "\n";
		VALS_DATA += "VALShdEmployPercentageIncludeInTDS = VALStxEmployPercentageIncludeInTDS;" + "\n";
		VALS_DATA += "VALScbEmployIncludeInTDS = new Array(" + PicklistData.getValuesCDS("INCOMETYPE", "TDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
    // For Job Title
    VALS_DATA += "VALScbJobTitle = new Array(" + PicklistData.getColumnValuesCDV("JOBTITLE", "JOBTITLEID") + ");" + "\n";
		VALS_DATA += "VALStxJobTitle = new Array(" + PicklistData.getColumnValuesCDS("JOBTITLE", "JOBTITLEDESCRIPTION") + ");" + "\n";
		VALS_DATA += "VALShdJobTitle = VALStxJobTitle;" + "\n";
		VALS_DATA += "VALScbOccupation = new Array(" + PicklistData.getColumnValuesCDV("JOBTITLE", "OCCUPATIONID") + ");" + "\n";
		VALS_DATA += "VALScbIndustrySector = new Array(" + PicklistData.getColumnValuesCDV("JOBTITLE", "INDUSTRYSECTORID") + ");" + "\n";

    // from populateDefaultsVALSDataRepeatedOtherIncome
    VALS_DATA += "VALScbOtherIncomeType = new Array(" + PicklistData.getColumnValuesCDV("INCOMETYPE", "INCOMETYPEID") + ");" + "\n";
		VALS_DATA += "VALStxOtherIncomePercentIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "GDSINCLUSION") + ");" + "\n";
		VALS_DATA += "VALShdOtherIncomePercentIncludeInGDS = VALStxOtherIncomePercentIncludeInGDS;" + "\n";
		VALS_DATA += "VALScbOtherIncomeIncludeInGDS = new Array(" + PicklistData.getValuesCDS("INCOMETYPE", "GDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
		VALS_DATA += "VALStxOtherIncomePercentIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "TDSINCLUSION") + ");" + "\n";
		VALS_DATA += "VALShdOtherIncomePercentIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("INCOMETYPE", "TDSINCLUSION") + ");" + "\n";
		VALS_DATA += "VALScbOtherIncomeIncludeInTDS = new Array(" + PicklistData.getValuesCDS("INCOMETYPE", "TDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";

    // from populateDefaultsVALSDataRepeatedAssets
    VALS_DATA += "VALScbAssetType = new Array(" + PicklistData.getColumnValuesCDV("ASSETTYPE", "ASSETTYPEID") + ");" + "\n";
		VALS_DATA += "VALStxPercentageIncludedInNetworth = new Array(" + PicklistData.getColumnValuesCDS("ASSETTYPE", "NETWORTHINCLUSION") + ");" + "\n";
		VALS_DATA += "VALShdPercentageIncludedInNetworth = VALStxPercentageIncludedInNetworth;" + "\n";
		VALS_DATA += "VALScbAssetIncludeInNetworth = new Array(" + PicklistData.getValuesCDS("ASSETTYPE", "NETWORTHINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";

    // from populateDefaultsVALSDataRepeatedLiabilities
    VALS_DATA += "VALScbLiabilityType = new Array(" + PicklistData.getColumnValuesCDV("LIABILITYTYPE", "LIABILITYTYPEID") + ");" + "\n";
		VALS_DATA += "VALStxLiabilityPercentageIncludeInGDS = new Array(" + PicklistData.getColumnValuesCDS("LIABILITYTYPE", "GDSINCLUSION") + ");" + "\n";
		VALS_DATA += "VALShdLiabilityPercentageIncludeInGDS = VALStxLiabilityPercentageIncludeInGDS;" + "\n";
		VALS_DATA += "VALScblLiabilityIncludeInGDS = new Array(" + PicklistData.getValuesCDS("LIABILITYTYPE", "GDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
		VALS_DATA += "VALStxLiabilityPercentageIncludeInTDS = new Array(" + PicklistData.getColumnValuesCDS("LIABILITYTYPE", "TDSINCLUSION") + ");" + "\n";
		VALS_DATA += "VALShdLiabilityPercentageIncludeInTDS = VALStxLiabilityPercentageIncludeInTDS;" + "\n";
		VALS_DATA += "VALScblLiabilityIncludeInTDS = new Array(" + PicklistData.getValuesCDS("LIABILITYTYPE", "TDSINCLUSION", ">", 0.0, "Y", "N") + ");" + "\n";
		VALS_DATA += "VALSliabilityPaymentQualifier = new Array(" + PicklistData.getColumnValuesCDS("LIABILITYTYPE", "LIABILITYPAYMENTQUALIFIER") + ");" + "\n";

    // From populateDefaultsVALSData
    // Setup Income Preoid arrays for calculation of TotalIncome (Calc 53)
    VALS_DATA += "VALScbIncomePeriod = new Array(" + PicklistData.getColumnValuesCDV("INCOMEPERIOD", "INCOMEPERIODID") + ");" + "\n";
		VALS_DATA += "VALSIncomeMultiplier = new Array(" + PicklistData.getColumnValuesCDV("INCOMEPERIOD", "ANNUALINCOMEMULTIPLIER") + ");" + "\n";
System.out.println("BILLY ===> After the setup VALS_DATA for ApplicantEntryHandler !!");
  }
  //======================================================================================================

}

