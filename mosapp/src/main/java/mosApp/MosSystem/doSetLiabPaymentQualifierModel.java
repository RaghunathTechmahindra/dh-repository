package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doSetLiabPaymentQualifierModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfLiabilityTypeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityTypeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabilityDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabilityDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfLiabPaymentQualifier();

	
	/**
	 * 
	 * 
	 */
	public void setDfLiabPaymentQualifier(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFLIABILITYTYPEID="dfLiabilityTypeId";
	public static final String FIELD_DFLIABILITYDESCRIPTION="dfLiabilityDescription";
	public static final String FIELD_DFLIABPAYMENTQUALIFIER="dfLiabPaymentQualifier";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

