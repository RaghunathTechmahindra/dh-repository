package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;

/**
 *
 *  TD_MWQ_CR.
 *  TD wants to see the new summary section counted by TaskName
 *  (currently only two tasks: all open Decision Deal
 *                         and Source Resubmition.
 *
 */
public class doCountTasksByTaskNameModelImpl extends QueryModelBase
	implements doCountTasksByTaskNameModel
{
	/**
	 *
	 *
	 */
	public doCountTasksByTaskNameModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
          //logger = SysLog.getSysLogger("DOCTBP");

          //logger.debug("DOCTBP@afterExecute::Size: " + this.getSize());
          //logger.debug("DOCTBP@afterExecute::SQLTemplate: " + this.getSelectSQL());
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public Integer getDfTaskNameCount()
	{
		return (Integer)getValue(FIELD_DFTASKNAMECOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfTaskNameCount(Integer value)
	{
		setValue(FIELD_DFTASKNAMECOUNT,value);
	}

	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doCountTasksByTaskNameModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

        //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

        //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
        //// (see detailed description in the desing doc as well).
        //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
        //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
        //// accordingly.
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	/**
  	 * BEGIN FIX FOR Performance Issue on Master Work queue [FXP21615]
     */	
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL COUNT(ASSIGNEDTASKSWORKQUEUE.TASKID) SYNTHETICTASKCOUNT " +
	"FROM ASSIGNEDTASKSWORKQUEUE, DEAL, USERPROFILE, GROUPPROFILE, BRANCHPROFILE, CONTACT c1 __WHERE__ ";
	public static final String MODIFYING_QUERY_TABLE_NAME="ASSIGNEDTASKSWORKQUEUE, DEAL, USERPROFILE, GROUPPROFILE, BRANCHPROFILE, CONTACT c1";
	public static final String STATIC_WHERE_CRITERIA =  
		"assignedtasksworkqueue.institutionprofileid = deal.institutionprofileid " + 
		"AND assignedtasksworkqueue.dealid = deal.dealid " + 
		"AND ASSIGNEDTASKSWORKQUEUE.TASKID < 50000 " +
		"AND assignedtasksworkqueue.institutionprofileid = userprofile.institutionprofileid   " +
		"AND assignedtasksworkqueue.userprofileid = userprofile.userprofileid " +
		"AND userprofile.institutionprofileid = groupprofile.institutionprofileid " +
		"AND userprofile.groupprofileid = groupprofile.groupprofileid " +
		"AND branchprofile.institutionprofileid = groupprofile.institutionprofileid " +
		"AND branchprofile.branchprofileid = groupprofile.branchprofileid " +
		"AND userprofile.institutionprofileid = c1.institutionprofileid " +
		"AND userprofile.contactid = c1.contactid " +
		"AND DEAL.SCENARIORECOMMENDED = 'Y' " +
		"AND c1.COPYID = 1 " +
		"AND DEAL.COPYTYPE <> 'T'";
	/**
  	 * END FIX FOR Performance Issue on Master Work queue [FXP21615]
     */
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFTASKNAMECOUNT="ASSIGNEDTASKSWORKQUEUE.SYNTHETICTASKCOUNT";
	public static final String COLUMN_DFTASKNAMECOUNT="SYNTHETICTASKCOUNT";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

	FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTASKNAMECOUNT,
				COLUMN_DFTASKNAMECOUNT,
				QUALIFIED_COLUMN_DFTASKNAMECOUNT,
				Integer.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"COUNT(ASSIGNEDTASKSWORKQUEUE.TASKID)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"COUNT(ASSIGNEDTASKSWORKQUEUE.TASKID)"));
	}

}

