package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.deal.util.BXStringTokenizer;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doAppraisalPropertyModelImpl extends QueryModelBase
  implements doAppraisalPropertyModel
{
  /**
   *
   *
   */
  public doAppraisalPropertyModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);
    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
    setFieldSchema(FIELD_SCHEMA);
    initialize();
  }


  /**
   *
   *
   */
  public void initialize()
  {
  }


  ////////////////////////////////////////////////////////////////////////////////
  // NetDynamics-migrated events
  ////////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   */
  protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
    throws ModelControlException
  {

    return sql;

  }


  /**
   *
   *
   */
  protected void afterExecute(ModelExecutionContext context, int queryType)
    throws ModelControlException
  {
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //This also provide a sample to populate PickList Description in Computed Fiels
    //--> By Billy 22Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
      getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    String tmpStr;
    while(this.next())
    {
      //Convert Descriptions in Computed field
      tmpStr = this.getDfPropertyAddress();
      //Populate StreetType
      tmpStr = BXStringTokenizer.replace(tmpStr, "STREETTYPE.STDESCRIPTION",
        BXResources.getPickListDescription(
          institutionId, "STREETTYPE", this.getDfStreetTypeId().intValue(), languageId));
      //Populate StreetDirection
      tmpStr = BXStringTokenizer.replace(tmpStr, "STREETDIRECTION.SDDESCRIPTION",
        BXResources.getPickListDescription(
          institutionId, "STREETDIRECTION", this.getDfStreetDirectionId().intValue(), languageId));
      this.setDfPropertyAddress(tmpStr);
      //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
      //--> By Billy 16Dec2003
      //Get AppraisalOrder Status Description
      if(getDfAppraisalStatusDesc() != null)
      this.setDfAppraisalStatusDesc(BXResources.getPickListDescription(institutionId, "APPRAISALSTATUS",
          this.getDfAppraisalStatusDesc(), languageId));
      //=================================================================
    }

   //Reset Location
    this.beforeFirst();
  }


  /**
   *
   *
   */
  protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
  {
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfPropertyOccurence()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYOCCURENCE);
  }


  /**
   *
   *
   */
  public void setDfPropertyOccurence(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPROPERTYOCCURENCE,value);
  }


  /**
   *
   *
   */
  public String getDfPrimaryProperty()
  {
    return (String)getValue(FIELD_DFPRIMARYPROPERTY);
  }


  /**
   *
   *
   */
  public void setDfPrimaryProperty(String value)
  {
    setValue(FIELD_DFPRIMARYPROPERTY,value);
  }


  /**
   *
   *
   */
  public String getDfPropertyAddress()
  {
    return (String)getValue(FIELD_DFPROPERTYADDRESS);
  }


  /**
   *
   *
   */
  public void setDfPropertyAddress(String value)
  {
    setValue(FIELD_DFPROPERTYADDRESS,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfPropertyId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPROPERTYID);
  }


  /**
   *
   *
   */
  public void setDfPropertyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPROPERTYID,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfPurchasePrice()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFPURCHASEPRICE);
  }


  /**
   *
   *
   */
  public void setDfPurchasePrice(java.math.BigDecimal value)
  {
    setValue(FIELD_DFPURCHASEPRICE,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfEstimatedAppraisal()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFESTIMATEDAPPRAISAL);
  }


  /**
   *
   *
   */
  public void setDfEstimatedAppraisal(java.math.BigDecimal value)
  {
    setValue(FIELD_DFESTIMATEDAPPRAISAL,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfActualAppraisal()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFACTUALAPPRAISAL);
  }


  /**
   *
   *
   */
  public void setDfActualAppraisal(java.math.BigDecimal value)
  {
    setValue(FIELD_DFACTUALAPPRAISAL,value);
  }


  /**
   *
   *
   */
  public java.sql.Timestamp getDfAppraisalDate()
  {
    return (java.sql.Timestamp)getValue(FIELD_DFAPPRAISALDATE);
  }


  /**
   *
   *
   */
  public void setDfAppraisalDate(java.sql.Timestamp value)
  {
    setValue(FIELD_DFAPPRAISALDATE,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
  }


  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDEALID,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
  }


  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID,value);
  }


  /**
   *
   *
   */
  public java.math.BigDecimal getDfAppraisalSourceId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFAPPRAISALSOURCEID);
  }


  /**
   *
   *
   */
  public void setDfAppraisalSourceId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFAPPRAISALSOURCEID,value);
  }

  //--Release2.1--//
  //Additional fields for Computed field population from ResourceBundle
  //--> By Billy 22Nov2002
  public java.math.BigDecimal getDfStreetTypeId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFSTREETTYPEID);
  }

  public void setDfStreetTypeId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFSTREETTYPEID,value);
  }

  public java.math.BigDecimal getDfStreetDirectionId()
  {
    return (java.math.BigDecimal)getValue(FIELD_DFSTREETDIRECTIONID);
  }

  public void setDfStreetDirectionId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFSTREETDIRECTIONID,value);
  }
  //======================================

  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  // New fields for Appraisal Order Info.
  /**
   * getDfAOContactCellPhoneNum
   *
   * @return String
   */
  public String getDfAOContactCellPhoneNum()
  {
    return (String)getValue(FIELD_DFAOCONTACTCELLPHONENUM);
  }

  /**
   * getDfAOContactHomePhoneNum
   *
   * @return String
   */
  public String getDfAOContactHomePhoneNum()
  {
    return (String)getValue(FIELD_DFAOCONTACTHOMEPHONENUM);
  }

  /**
   * getDfAOContactName
   *
   * @return String
   */
  public String getDfAOContactName()
  {
    return (String)getValue(FIELD_DFAOCONTACTNAME);
  }

  /**
   * getDfAOContactWorkPhoneNum
   *
   * @return String
   */
  public String getDfAOContactWorkPhoneNum()
  {
    return (String)getValue(FIELD_DFAOCONTACTWORKPHONENUM);
  }

  /**
   * getDfAOContactWorkPhoneNumExt
   *
   * @return String
   */
  public String getDfAOContactWorkPhoneNumExt()
  {
    return (String)getValue(FIELD_DFAOCONTACTWORKPHONENUMEXT);
  }

  /**
   * getDfAppraisalStatusDesc
   *
   * @return String
   */
  public String getDfAppraisalStatusDesc()
  {
    return (String)getValue(FIELD_DFAPPRAISALSTATUSDESC);
  }

  /**
   * getDfCommentsForAppraiser
   *
   * @return String
   */
  public String getDfCommentsForAppraiser()
  {
    return (String)getValue(FIELD_DFCOMMENTSFORAPPRAISER);
  }

  /**
   * getDfPropertyOwnerName
   *
   * @return String
   */
  public String getDfPropertyOwnerName()
  {
    return (String)getValue(FIELD_DFPROPERTYOWNERNAME);
  }

  /**
   * getDfUseBorrowerInfo
   *
   * @return String
   */
  public String getDfUseBorrowerInfo()
  {
    return (String)getValue(FIELD_DFUSEBORROWERINFO);
  }

  /**
   * setDfAOContactCellPhoneNum
   *
   * @param value String
   */
  public void setDfAOContactCellPhoneNum(String value)
  {
    setValue(FIELD_DFAOCONTACTCELLPHONENUM,value);
  }

  /**
   * setDfAOContactHomePhoneNum
   *
   * @param value String
   */
  public void setDfAOContactHomePhoneNum(String value)
  {
    setValue(FIELD_DFAOCONTACTHOMEPHONENUM,value);
  }

  /**
   * setDfAOContactName
   *
   * @param value String
   */
  public void setDfAOContactName(String value)
  {
    setValue(FIELD_DFAOCONTACTNAME,value);
  }

  /**
   * setDfAOContactWorkPhoneNum
   *
   * @param value String
   */
  public void setDfAOContactWorkPhoneNum(String value)
  {
    setValue(FIELD_DFAOCONTACTWORKPHONENUM,value);
  }

  /**
   * setDfAOContactWorkPhoneNumExt
   *
   * @param value String
   */
  public void setDfAOContactWorkPhoneNumExt(String value)
  {
    setValue(FIELD_DFAOCONTACTWORKPHONENUMEXT,value);
  }

  /**
   * setDfAppraisalStatusDesc
   *
   * @param value String
   */
  public void setDfAppraisalStatusDesc(String value)
  {
    setValue(FIELD_DFAPPRAISALSTATUSDESC,value);
  }

  /**
   * setDfCommentsForAppraiser
   *
   * @param value String
   */
  public void setDfCommentsForAppraiser(String value)
  {
    setValue(FIELD_DFCOMMENTSFORAPPRAISER,value);
  }

  /**
   * setDfPropertyOwnerName
   *
   * @param value String
   */
  public void setDfPropertyOwnerName(String value)
  {
    setValue(FIELD_DFPROPERTYOWNERNAME,value);
  }

  /**
   * setDfUseBorrowerInfo
   *
   * @param value String
   */
  public void setDfUseBorrowerInfo(String value)
  {
    setValue(FIELD_DFUSEBORROWERINFO,value);
  }
  //=========================================================================


  ////////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////





  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;


  ////////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.

  public static final String DATA_SOURCE_NAME="jdbc/orcl";
  ////public static final String SELECT_SQL_TEMPLATE="SELECT ALL PROPERTY.PROPERTYOCCURENCENUMBER, PROPERTY.PRIMARYPROPERTYFLAG, PROPERTY.PROPERTYSTREETNUMBER || ' ' ||  PROPERTY.PROPERTYSTREETNAME || ' ' ||  STREETTYPE.STDESCRIPTION || ' ' || STREETDIRECTION.SDDESCRIPTION || ' ' || PROVINCE.PROVINCEABBREVIATION ADDRESS, PROPERTY.PROPERTYID, PROPERTY.PURCHASEPRICE, PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.ACTUALAPPRAISALVALUE, PROPERTY.APPRAISALDATEACT, PROPERTY.DEALID, PROPERTY.COPYID, PROPERTY.APPRAISALSOURCEID FROM PROPERTY, STREETTYPE, STREETDIRECTION, PROVINCE  __WHERE__  ";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
  //--> Additional fields for Computed field population from ResourceBundle
  //--> By Billy 22Nov2002
  //public static final String SELECT_SQL_TEMPLATE="SELECT ALL PROPERTY.PROPERTYOCCURENCENUMBER, PROPERTY.PRIMARYPROPERTYFLAG, PROPERTY.PROPERTYSTREETNUMBER || ' ' ||  PROPERTY.PROPERTYSTREETNAME || ' ' ||  STREETTYPE.STDESCRIPTION || ' ' || STREETDIRECTION.SDDESCRIPTION || ' ' || PROVINCE.PROVINCEABBREVIATION SYNTHETICADDRESS, PROPERTY.PROPERTYID, PROPERTY.PURCHASEPRICE, PROPERTY.ESTIMATEDAPPRAISALVALUE, PROPERTY.ACTUALAPPRAISALVALUE, PROPERTY.APPRAISALDATEACT, PROPERTY.DEALID, PROPERTY.COPYID, PROPERTY.APPRAISALSOURCEID FROM PROPERTY, STREETTYPE, STREETDIRECTION, PROVINCE  __WHERE__  ORDER BY PROPERTY.PROPERTYOCCURENCENUMBER ASC";
  public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL PROPERTY.PROPERTYOCCURENCENUMBER, PROPERTY.PRIMARYPROPERTYFLAG, "+
    "PROPERTY.PROPERTYSTREETNUMBER || ' ' ||  INITCAP(PROPERTY.PROPERTYSTREETNAME) || ' ' ||  "+
      //--Release2.1--//--> Converted the Desc. Filed to Static String (by single quote) and use it as the
                      //--> ket words to populate it with the ResourceBundle @ afterExecute.
      "'STREETTYPE.STDESCRIPTION' || ' ' || 'STREETDIRECTION.SDDESCRIPTION' || ', ' || "+
      "INITCAP(PROPERTY.PROPERTYCITY) || ', ' || " +
      "PROVINCE.PROVINCEABBREVIATION SYNTHETICADDRESS, "+
     //--Release2.1--//--> Additional fields for the above computed fields
    "PROPERTY.STREETTYPEID, PROPERTY.STREETDIRECTIONID, "+
    "PROPERTY.PROPERTYID, PROPERTY.PURCHASEPRICE, PROPERTY.ESTIMATEDAPPRAISALVALUE, "+
    "PROPERTY.ACTUALAPPRAISALVALUE, PROPERTY.APPRAISALDATEACT, "+
    "PROPERTY.DEALID, PROPERTY.COPYID, PROPERTY.APPRAISALSOURCEID, "+
    //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
    //--> By Billy 16Dec2003
    // New fields for Appraisal Order Info.
    "to_char(APPRAISALORDER.APPRAISALSTATUSID) APPRAISALSTATUSID_STR, APPRAISALORDER.USEBORROWERINFO, "+
    "APPRAISALORDER.PROPERTYOWNERNAME, APPRAISALORDER.CONTACTNAME, "+
    "APPRAISALORDER.CONTACTWORKPHONENUM, APPRAISALORDER.CONTACTWORKPHONENUMEXT, "+
    "APPRAISALORDER.CONTACTCELLPHONENUM, APPRAISALORDER.CONTACTHOMEPHONENUM, "+
    "APPRAISALORDER.COMMENTSFORAPPRAISER "+
    "FROM PROPERTY, PROVINCE, APPRAISALORDER "+
    //=================================================================
    "__WHERE__  "+
    "ORDER BY PROPERTY.PROPERTYOCCURENCENUMBER ASC";

  //--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="PROPERTY, STREETTYPE, STREETDIRECTION, PROVINCE";
  public static final String MODIFYING_QUERY_TABLE_NAME=
    "PROPERTY, PROVINCE, APPRAISALORDER";
  //--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (PROPERTY.PROVINCEID  =  PROVINCE.PROVINCEID) AND (PROPERTY.STREETDIRECTIONID  =  STREETDIRECTION.STREETDIRECTIONID) AND (PROPERTY.STREETTYPEID  =  STREETTYPE.STREETTYPEID)";
  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  // New fields for Appraisal Order Info.
  // Added outer Joint cause to AppraisalOrder
  public static final String STATIC_WHERE_CRITERIA=
   " PROPERTY.INSTITUTIONPROFILEID = APPRAISALORDER.INSTITUTIONPROFILEID(+) AND " +
   " PROPERTY.PROVINCEID = PROVINCE.PROVINCEID AND " +
   " PROPERTY.PROPERTYID = APPRAISALORDER.PROPERTYID (+) AND " +
   " PROPERTY.COPYID = APPRAISALORDER.COPYID (+) ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
  public static final String QUALIFIED_COLUMN_DFPROPERTYOCCURENCE="PROPERTY.PROPERTYOCCURENCENUMBER";
  public static final String COLUMN_DFPROPERTYOCCURENCE="PROPERTYOCCURENCENUMBER";
  public static final String QUALIFIED_COLUMN_DFPRIMARYPROPERTY="PROPERTY.PRIMARYPROPERTYFLAG";
  public static final String COLUMN_DFPRIMARYPROPERTY="PRIMARYPROPERTYFLAG";
  ////public static final String QUALIFIED_COLUMN_DFPROPERTYADDRESS=".";
  ////public static final String COLUMN_DFPROPERTYADDRESS="";
  public static final String QUALIFIED_COLUMN_DFPROPERTYADDRESS="SYNTHETICADDRESS";
  public static final String COLUMN_DFPROPERTYADDRESS="SYNTHETICADDRESS";
  public static final String QUALIFIED_COLUMN_DFPROPERTYID="PROPERTY.PROPERTYID";
  public static final String COLUMN_DFPROPERTYID="PROPERTYID";
  public static final String QUALIFIED_COLUMN_DFPURCHASEPRICE="PROPERTY.PURCHASEPRICE";
  public static final String COLUMN_DFPURCHASEPRICE="PURCHASEPRICE";
  public static final String QUALIFIED_COLUMN_DFESTIMATEDAPPRAISAL="PROPERTY.ESTIMATEDAPPRAISALVALUE";
  public static final String COLUMN_DFESTIMATEDAPPRAISAL="ESTIMATEDAPPRAISALVALUE";
  public static final String QUALIFIED_COLUMN_DFACTUALAPPRAISAL="PROPERTY.ACTUALAPPRAISALVALUE";
  public static final String COLUMN_DFACTUALAPPRAISAL="ACTUALAPPRAISALVALUE";
  public static final String QUALIFIED_COLUMN_DFAPPRAISALDATE="PROPERTY.APPRAISALDATEACT";
  public static final String COLUMN_DFAPPRAISALDATE="APPRAISALDATEACT";
  public static final String QUALIFIED_COLUMN_DFDEALID="PROPERTY.DEALID";
  public static final String COLUMN_DFDEALID="DEALID";
  public static final String QUALIFIED_COLUMN_DFCOPYID="PROPERTY.COPYID";
  public static final String COLUMN_DFCOPYID="COPYID";
  public static final String QUALIFIED_COLUMN_DFAPPRAISALSOURCEID="PROPERTY.APPRAISALSOURCEID";
  public static final String COLUMN_DFAPPRAISALSOURCEID="APPRAISALSOURCEID";

  //--Release2.1--//
  //Additional fields for Computed field population from ResourceBundle
  //--> By Billy 22Nov2002
  public static final String QUALIFIED_COLUMN_DFSTREETTYPEID="PROPERTY.STREETTYPEID";
  public static final String COLUMN_DFSTREETTYPEID="STREETTYPEID";
  public static final String QUALIFIED_COLUMN_DFSTREETDIRECTIONID="PROPERTY.STREETDIRECTIONID";
  public static final String COLUMN_DFSTREETDIRECTIONID="STREETDIRECTIONID";
  //===================================================================

  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  // New fields for Appraisal Order Info.
  public static final String QUALIFIED_COLUMN_DFAPPRAISALSTATUSDESC="APPRAISALORDER.APPRAISALSTATUSID_STR";
  public static final String COLUMN_DFAPPRAISALSTATUSDESC="APPRAISALSTATUSID_STR";
  public static final String QUALIFIED_COLUMN_DFUSEBORROWERINFO="APPRAISALORDER.USEBORROWERINFO";
  public static final String COLUMN_DFUSEBORROWERINFO="USEBORROWERINFO";
  public static final String QUALIFIED_COLUMN_DFPROPERTYOWNERNAME="APPRAISALORDER.PROPERTYOWNERNAME";
  public static final String COLUMN_DFPROPERTYOWNERNAME="PROPERTYOWNERNAME";
  public static final String QUALIFIED_COLUMN_DFAOCONTACTNAME="APPRAISALORDER.CONTACTNAME";
  public static final String COLUMN_DFAOCONTACTNAME="CONTACTNAME";
  public static final String QUALIFIED_COLUMN_DFAOCONTACTWORKPHONENUM="APPRAISALORDER.CONTACTWORKPHONENUM";
  public static final String COLUMN_DFAOCONTACTWORKPHONENUM="CONTACTWORKPHONENUM";
  public static final String QUALIFIED_COLUMN_DFAOCONTACTWORKPHONENUMEXT="APPRAISALORDER.CONTACTWORKPHONENUMEXT";
  public static final String COLUMN_DFAOCONTACTWORKPHONENUMEXT="CONTACTWORKPHONENUMEXT";
  public static final String QUALIFIED_COLUMN_DFAOCONTACTCELLPHONENUM="APPRAISALORDER.CONTACTCELLPHONENUM";
  public static final String COLUMN_DFAOCONTACTCELLPHONENUM="CONTACTCELLPHONENUM";
  public static final String QUALIFIED_COLUMN_DFAOCONTACTHOMEPHONENUM="APPRAISALORDER.CONTACTHOMEPHONENUM";
  public static final String COLUMN_DFAOCONTACTHOMEPHONENUM="CONTACTHOMEPHONENUM";
  public static final String QUALIFIED_COLUMN_DFCOMMENTSFORAPPRAISER="APPRAISALORDER.COMMENTSFORAPPRAISER";
  public static final String COLUMN_DFCOMMENTSFORAPPRAISER="COMMENTSFORAPPRAISER";
  //==========================================================================================================

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////



  ////////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  ////////////////////////////////////////////////////////////////////////////

  static
  {

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPROPERTYOCCURENCE,
        COLUMN_DFPROPERTYOCCURENCE,
        QUALIFIED_COLUMN_DFPROPERTYOCCURENCE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPRIMARYPROPERTY,
        COLUMN_DFPRIMARYPROPERTY,
        QUALIFIED_COLUMN_DFPRIMARYPROPERTY,
        String.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    //// Improper FieldDescriptor converted by the iMT tool.
    ////FIELD_SCHEMA.addFieldDescriptor(
    ////	new QueryFieldDescriptor(
    ////		FIELD_DFPROPERTYADDRESS,
    ////		COLUMN_DFPROPERTYADDRESS,
    ////		QUALIFIED_COLUMN_DFPROPERTYADDRESS,
    ////		String.class,
    ////		false,
    ////		false,
    ////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
    ////		"",
    ////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
    ////		""));


    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPROPERTYADDRESS,
        COLUMN_DFPROPERTYADDRESS,
        QUALIFIED_COLUMN_DFPROPERTYADDRESS,
        String.class,
        false,
        true,
        QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
        "PROPERTY.PROPERTYSTREETNUMBER || ' ' ||  PROPERTY.PROPERTYSTREETNAME || ' ' ||  STREETTYPE.STDESCRIPTION || ' ' || STREETDIRECTION.SDDESCRIPTION || ' ' || PROVINCE.PROVINCEABBREVIATION",
        QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
        "PROPERTY.PROPERTYSTREETNUMBER || ' ' ||  PROPERTY.PROPERTYSTREETNAME || ' ' ||  STREETTYPE.STDESCRIPTION || ' ' || STREETDIRECTION.SDDESCRIPTION || ' ' || PROVINCE.PROVINCEABBREVIATION"));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPROPERTYID,
        COLUMN_DFPROPERTYID,
        QUALIFIED_COLUMN_DFPROPERTYID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFPURCHASEPRICE,
        COLUMN_DFPURCHASEPRICE,
        QUALIFIED_COLUMN_DFPURCHASEPRICE,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFESTIMATEDAPPRAISAL,
        COLUMN_DFESTIMATEDAPPRAISAL,
        QUALIFIED_COLUMN_DFESTIMATEDAPPRAISAL,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFACTUALAPPRAISAL,
        COLUMN_DFACTUALAPPRAISAL,
        QUALIFIED_COLUMN_DFACTUALAPPRAISAL,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFAPPRAISALDATE,
        COLUMN_DFAPPRAISALDATE,
        QUALIFIED_COLUMN_DFAPPRAISALDATE,
        java.sql.Timestamp.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFDEALID,
        COLUMN_DFDEALID,
        QUALIFIED_COLUMN_DFDEALID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFCOPYID,
        COLUMN_DFCOPYID,
        QUALIFIED_COLUMN_DFCOPYID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFAPPRAISALSOURCEID,
        COLUMN_DFAPPRAISALSOURCEID,
        QUALIFIED_COLUMN_DFAPPRAISALSOURCEID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    //--Release2.1--//
    //Additional fields for Computed field population from ResourceBundle
    //--> By Billy 22Nov2002
    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFSTREETTYPEID,
        COLUMN_DFSTREETTYPEID,
        QUALIFIED_COLUMN_DFSTREETTYPEID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));

    FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
        FIELD_DFSTREETDIRECTIONID,
        COLUMN_DFSTREETDIRECTIONID,
        QUALIFIED_COLUMN_DFSTREETDIRECTIONID,
        java.math.BigDecimal.class,
        false,
        false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
        "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
        ""));
     //==========================================
     //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
     //--> By Billy 16Dec2003
     // New fields for Appraisal Order Info.
     FIELD_SCHEMA.addFieldDescriptor(
       new QueryFieldDescriptor(
       FIELD_DFAPPRAISALSTATUSDESC,
       COLUMN_DFAPPRAISALSTATUSDESC,
       QUALIFIED_COLUMN_DFAPPRAISALSTATUSDESC,
       String.class,
       false,
       false,
       QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
       "",
       QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
       ""));

     FIELD_SCHEMA.addFieldDescriptor(
      new QueryFieldDescriptor(
      FIELD_DFUSEBORROWERINFO,
      COLUMN_DFUSEBORROWERINFO,
      QUALIFIED_COLUMN_DFUSEBORROWERINFO,
      String.class,
      false,
      false,
      QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
      "",
      QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
      ""));

    FIELD_SCHEMA.addFieldDescriptor(
     new QueryFieldDescriptor(
     FIELD_DFPROPERTYOWNERNAME,
     COLUMN_DFPROPERTYOWNERNAME,
     QUALIFIED_COLUMN_DFPROPERTYOWNERNAME,
     String.class,
     false,
     false,
     QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
     "",
     QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
     ""));

   FIELD_SCHEMA.addFieldDescriptor(
     new QueryFieldDescriptor(
     FIELD_DFAOCONTACTNAME,
     COLUMN_DFAOCONTACTNAME,
     QUALIFIED_COLUMN_DFAOCONTACTNAME,
     String.class,
     false,
     false,
     QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
     "",
     QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
     ""));

   FIELD_SCHEMA.addFieldDescriptor(
     new QueryFieldDescriptor(
     FIELD_DFAOCONTACTWORKPHONENUM,
     COLUMN_DFAOCONTACTWORKPHONENUM,
     QUALIFIED_COLUMN_DFAOCONTACTWORKPHONENUM,
     String.class,
     false,
     false,
     QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
     "",
     QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
     ""));

   FIELD_SCHEMA.addFieldDescriptor(
     new QueryFieldDescriptor(
     FIELD_DFAOCONTACTWORKPHONENUMEXT,
     COLUMN_DFAOCONTACTWORKPHONENUMEXT,
     QUALIFIED_COLUMN_DFAOCONTACTWORKPHONENUMEXT,
     String.class,
     false,
     false,
     QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
     "",
     QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
     ""));

   FIELD_SCHEMA.addFieldDescriptor(
     new QueryFieldDescriptor(
     FIELD_DFAOCONTACTCELLPHONENUM,
     COLUMN_DFAOCONTACTCELLPHONENUM,
     QUALIFIED_COLUMN_DFAOCONTACTCELLPHONENUM,
     String.class,
     false,
     false,
     QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
     "",
     QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
     ""));

   FIELD_SCHEMA.addFieldDescriptor(
     new QueryFieldDescriptor(
     FIELD_DFAOCONTACTHOMEPHONENUM,
     COLUMN_DFAOCONTACTHOMEPHONENUM,
     QUALIFIED_COLUMN_DFAOCONTACTHOMEPHONENUM,
     String.class,
     false,
     false,
     QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
     "",
     QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
     ""));

   FIELD_SCHEMA.addFieldDescriptor(
     new QueryFieldDescriptor(
     FIELD_DFCOMMENTSFORAPPRAISER,
     COLUMN_DFCOMMENTSFORAPPRAISER,
     QUALIFIED_COLUMN_DFCOMMENTSFORAPPRAISER,
     String.class,
     false,
     false,
     QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
     "",
     QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
     ""));
    //========================================================
  }

}

