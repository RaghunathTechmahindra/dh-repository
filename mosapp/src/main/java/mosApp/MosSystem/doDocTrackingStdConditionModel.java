package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doDocTrackingStdConditionModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction();


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel();


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value);


	/**
	 *
	 *
	 */
	public String getDfDocumentText();


	/**
	 *
	 *
	 */
	public void setDfDocumentText(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfStdDocTrackId();


	/**
	 *
	 *
	 */
	public void setDfStdDocTrackId(java.math.BigDecimal value);


  /**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLanguagePrederenceId();


	/**
	 *
	 *
	 */
	public void setDfLanguagePrederenceId(java.math.BigDecimal value);



	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFACTION="dfAction";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFDOCUMENTLABEL="dfDocumentLabel";
	public static final String FIELD_DFDOCUMENTTEXT="dfDocumentText";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String FIELD_DFDOCUMENTTEXTLITE="dfDocumentTextLite";
	//================================================================
	public static final String FIELD_DFSTDDOCTRACKID="dfStdDocTrackId";
  //--Release2.1--//
  //// Added LanguagePreferenceId DataField, DfByPass.
	public static final String FIELD_DFLANGUAGEPREFERENCEID="dfLanguagePrederenceId";
  public static final String FIELD_DFBYPASS="dfByPass";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

