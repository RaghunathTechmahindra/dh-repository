package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * Title: doComponentCreditCardModelImpl
 * <p>
 * Description: Implementation of doComponentLoanModel Interface.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 * @version 1.1 16-June-2008 XS_16.15 Modified to add field for Loan component
 *          information.
 * @version 1.2 23-June-2008 XS_16.7 - Modified  STATIC_WHERE_CRITERIA for ordering the join condition.
 * @version 1.3 25-June-2008 XS_16.15 - Added fields to display Actual Payment Term.
 * @version 1.4 25-June-2008 XS_2.37  - Added fields for the Dynamic product feilds on the loan section in CD screen
 */
public class doComponentLoanModelImpl extends QueryModelBase implements
        doComponentLoanModel
{

    public static final String DATA_SOURCE_NAME = "jdbc/orcl";

    public static final String SELECT_SQL_TEMPLATE = "SELECT ALL COMPONENT.DEALID,COMPONENT.COMPONENTID, "
            + "COMPONENT.COPYID, COMPONENT.INSTITUTIONPROFILEID, "
            + "to_char(COMPONENT.COMPONENTTYPEID)COMPONENTTYPEID_STR, "
            + "to_char(COMPONENT.MTGPRODID) MTGPRODID_STR, COMPONENTLOAN.LOANAMOUNT, "
            + "COMPONENTLOAN.NETINTERESTRATE, COMPONENTLOAN.DISCOUNT, COMPONENTLOAN.PREMIUM, "
            + "to_char(MTGPROD.PAYMENTTERMID)PAYMENTTERMID_STR,to_char(COMPONENTLOAN.PAYMENTFREQUENCYID)PAYMENTFREQUENCYID_STR,COMPONENT.POSTEDRATE,  "
            + "COMPONENTLOAN.ACTUALPAYMENTTERM,COMPONENTLOAN.PANDIPAYMENTAMOUNT,COMPONENT.ADDITIONALINFORMATION,  "
            + "COMPONENT.COMPONENTTYPEID, "
            + "COMPONENT.MTGPRODID, "
            + "COMPONENTLOAN.PAYMENTFREQUENCYID, "
            + "COMPONENT.PRICINGRATEINVENTORYID " //XS_2.37(Dynamic drop down lists)
            + " FROM COMPONENT,COMPONENTLOAN, MTGPROD "
            + " __WHERE__  ORDER BY COMPONENT.COMPONENTID ";

    public static final String MODIFYING_QUERY_TABLE_NAME = "COMPONENT, MTGPROD, COMPONENTLOAN";
    /**
     * MCM Impl Team
     * @version 1.2 23-June-2008 XS_16.7 - modified  STATIC_WHERE_CRITERIA for ordering the join condition.
     */
    public static final String STATIC_WHERE_CRITERIA = "COMPONENT.INSTITUTIONPROFILEID= COMPONENTLOAN.INSTITUTIONPROFILEID "
            + " AND COMPONENT.INSTITUTIONPROFILEID=MTGPROD.INSTITUTIONPROFILEID "
            + " AND COMPONENT.COPYID=COMPONENTLOAN.COPYID "
            + " AND COMPONENT.COMPONENTID=COMPONENTLOAN.COMPONENTID  "
            + " AND COMPONENT.MTGPRODID=MTGPROD.MTGPRODID "
            + " AND COMPONENT.COMPONENTTYPEID=3";

    public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

    public static final String QUALIFIED_COLUMN_DFCOMPONENTID = "COMPONENT.COMPONENTID";

    public static final String COLUMN_DFCOMPONENTID = "COMPONENTID";

    public static final String QUALIFIED_COLUMN_DFDEALID = "COMPONENT.DEALID";

    public static final String COLUMN_DFDEALID = "DEALID";

    public static final String QUALIFIED_COLUMN_DFISTITUTIONPROFILEID = "COMPONENT.INSTITUTIONPROFILEID";

    public static final String COLUMN_DFISTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

    public static final String QUALIFIED_COLUMN_DFCOPYID = "COMPONENT.COPYID";

    public static final String COLUMN_DFCOPYID = "COPYID";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTTYPE = "COMPONENT.COMPONENTTYPEID_STR";

    public static final String COLUMN_DFCOMPONENTTYPE = "COMPONENTTYPEID_STR";

    public static final String QUALIFIED_COLUMN_DFPRODUCTNAME = "COMPONENT.MTGPRODID_STR";

    public static final String COLUMN_DFPRODUCTNAME = "MTGPRODID_STR";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTLOANAMOUNT = "COMPONENTLOAN.LOANAMOUNT";

    public static final String COLUMN_DFCOMPONENTLOANAMOUNT = "LOANAMOUNT";

    public static final String QUALIFIED_COLUMN_DFNETINTERESTRATE = "COMPONENTLOAN.NETINTERESTRATE";

    public static final String COLUMN_DFNETINTERESTRATE = "NETINTERESTRATE";

    public static final String QUALIFIED_COLUMN_DFDISCOUNT = "COMPONENTLOAN.DISCOUNT";

    public static final String COLUMN_DFDISCOUNT = "DISCOUNT";

    public static final String QUALIFIED_COLUMN_DFPREMIUM = "COMPONENTLOAN.PREMIUM";

    public static final String COLUMN_DFPREMIUM = "PREMIUM";

    /** **********************MCM XS_16.15 changes starts********************************** */
    public static final String QUALIFIED_COLUMN_DFPPAYMENTTERMID = "MTGPROD.PAYMENTTERMID_STR";

    public static final String COLUMN_DFPPAYMENTTERMID = "PAYMENTTERMID_STR";

    public static final String QUALIFIED_COLUMN_DFPPAYMENTFREQUENCYID = "COMPONENTLOAN.PAYMENTFREQUENCYID_STR";

    public static final String COLUMN_DFPPAYMENTFREQUENCYID = "PAYMENTFREQUENCYID_STR";

    public static final String QUALIFIED_COLUMN_DFPOSTEDRATE = "COMPONENT.POSTEDRATE";

    public static final String COLUMN_DFPOSTEDRATE = "POSTEDRATE";

    public static final String QUALIFIED_COLUMN_DFADDITIONALINFORMATION = "COMPONENT.ADDITIONALINFORMATION";

    public static final String COLUMN_DFADDITIONALINFORMATION = "ADDITIONALINFORMATION";

    public static final String QUALIFIED_COLUMN_DFPANDIPAYMENTAMOUNT = "COMPONENTLOAN.PANDIPAYMENTAMOUNT";

    public static final String COLUMN_DFPANDIPAYMENTAMOUNT = "PANDIPAYMENTAMOUNT";

    public static final String QUALIFIED_COLUMN_DFACTUALPAYMENTTERM = "COMPONENTLOAN.ACTUALPAYMENTTERM";

    public static final String COLUMN_DFACTUALPAYMENTTERM = "ACTUALPAYMENTTERM";
    /** **********************MCM XS_16.15 changes ends********************************** */
    
    /** **********************MCM XS_2.37 changes starts********************************** */
    public static final String QUALIFIED_COLUMN_DFCOMPONENTTYPEID = "COMPONENT.COMPONENTTYPEID";

    public static final String COLUMN_DFCOMPONENTTYPEID = "COMPONENTTYPEID";
    
    public static final String QUALIFIED_COLUMN_DFPRODUCTID = "COMPONENT.MTGPRODID";

    public static final String COLUMN_DFPRODUCTID = "MTGPRODID";
    
    public static final String QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID = "COMPONENTLOAN.PAYMENTFREQUENCYID";
    
    public static final String COLUMN_DFPAYMENTFREQUENCYID = "PAYMENTFREQUENCYID";
    
    /** **********************MCM XS_2.37 changes ends********************************** */
    /*********************MCM Impl Team XS_2.37 (dynamic drop down lists) changes starts******************/
    public static final String QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID = "COMPONENT.PRICINGRATEINVENTORYID";
    public static final String COLUMN_DFPRICINGRATEINVENTORYID = "PRICINGRATEINVENTORYID";
    /*********************MCM Impl Team XS_2.37 (dynamic drop down lists) changes ends******************/
    
    static
    {

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTID, COLUMN_DFCOMPONENTID,
                QUALIFIED_COLUMN_DFCOMPONENTID, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDEALID, COLUMN_DFDEALID, QUALIFIED_COLUMN_DFDEALID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOPYID, COLUMN_DFCOPYID, QUALIFIED_COLUMN_DFCOPYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFISNTITUTIONID, COLUMN_DFISTITUTIONPROFILEID,
                QUALIFIED_COLUMN_DFISTITUTIONPROFILEID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTTYPE, COLUMN_DFCOMPONENTTYPE,
                QUALIFIED_COLUMN_DFCOMPONENTTYPE, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRODUCTNAME, COLUMN_DFPRODUCTNAME,
                QUALIFIED_COLUMN_DFPRODUCTNAME, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFLOANAMOUNT, COLUMN_DFCOMPONENTLOANAMOUNT,
                QUALIFIED_COLUMN_DFCOMPONENTLOANAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFNETINTRESTRATE, COLUMN_DFNETINTERESTRATE,
                QUALIFIED_COLUMN_DFNETINTERESTRATE, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDISCOUNT, COLUMN_DFDISCOUNT,
                QUALIFIED_COLUMN_DFDISCOUNT, java.math.BigDecimal.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPREMIUM, COLUMN_DFPREMIUM, QUALIFIED_COLUMN_DFPREMIUM,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        /******************MCM Impl Team XS_16.15 changes starts***********************************/

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPAYMENTTERMDESCRIPTION, COLUMN_DFPPAYMENTTERMID,
                QUALIFIED_COLUMN_DFPPAYMENTTERMID, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFACTUALPAYMENTTERM, COLUMN_DFACTUALPAYMENTTERM,
                QUALIFIED_COLUMN_DFACTUALPAYMENTTERM, java.math.BigDecimal.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPAYMENTFREQUENCY, COLUMN_DFPPAYMENTFREQUENCYID,
                QUALIFIED_COLUMN_DFPPAYMENTFREQUENCYID, String.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFADDITIONALDETAILS, COLUMN_DFADDITIONALINFORMATION,
                QUALIFIED_COLUMN_DFADDITIONALINFORMATION, String.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALPAYMENT, COLUMN_DFPANDIPAYMENTAMOUNT,
                QUALIFIED_COLUMN_DFPANDIPAYMENTAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPOSTEDRATE, COLUMN_DFPOSTEDRATE,
                QUALIFIED_COLUMN_DFPOSTEDRATE, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        /*************************MCM Impl Team XS_16.15 changes ends************************************************************************************ */
        
        /************************MCM XS_2.37 changes starts***********************************/
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTTYPEID, COLUMN_DFCOMPONENTTYPEID,
                QUALIFIED_COLUMN_DFCOMPONENTTYPEID, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRODUCTID, COLUMN_DFPRODUCTID,
                QUALIFIED_COLUMN_DFPRODUCTID, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPAYMENTFREQUENCYID, COLUMN_DFPAYMENTFREQUENCYID,
                QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID, java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        /************************MCM XS_2.37 changes ends***********************************/
        /*********************MCM Impl Team XS_2.37 (dynamic drop down lists) changes starts******************/
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRICINGRATEINVENTORYID, 
                COLUMN_DFPRICINGRATEINVENTORYID,
                QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID, 
                BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        /*********************MCM Impl Team XS_2.37 (dynamic drop down lists) changes starts******************/
    }

    /**
     * Instantiates a new do component loan model impl.
     */
    public doComponentLoanModelImpl()
    {
        super();
        setDataSourceName(DATA_SOURCE_NAME);

        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

        setFieldSchema(FIELD_SCHEMA);

        initialize();

    }

    /**
     * Initialize.
     */
    public void initialize()
    {

    }

    /**
     * BeforeExecute
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param sql
     *            the sql
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected String beforeExecute(ModelExecutionContext context,
            int queryType, String sql) throws ModelControlException
    {

        return sql;

    }

    /**
     * This method is used to handle any display logic after executing the model
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @version 1.1 16-June-2008 XS_16.15 Added code get the description for the
     *          payment frequency and payment term.
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
            throws ModelControlException
    {

        String defaultInstanceStateName = getRequestContext().getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();
        int institutionId = theSessionState.getDealInstitutionId();

        while (this.next())
        {

            this.setDfComponentType(BXResources.getPickListDescription(
                    institutionId, "COMPONENTTYPE", this.getDfComponentType(),
                    languageId));

            this.setDfProductName(BXResources.getPickListDescription(
                    institutionId, "MTGPROD", this.getDfProductName(),
                    languageId));
            this.setDfPaymentFrequency(BXResources.getPickListDescription(
                    institutionId, "PAYMENTFREQUENCY", this
                            .getDfPaymentFrequency(), languageId));

            this.setDfPaymentTermDescription(BXResources
                    .getPickListDescription(institutionId, "PAYMENTTERM", this
                            .getDfPaymentTermDescription(), languageId));
            //to replace line feed with <br>
//            if(this.getDfAdditionalDetails()!=null)
//             this.setDfAdditionalDetails(this.getDfAdditionalDetails().replaceAll("\n",
//             "<br>"));
            
        }

        // Reset Location
        this.beforeFirst();

    }

    /**
     * OnDatabaseError.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param exception
     *            the exception
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void onDatabaseError(ModelExecutionContext context,
            int queryType, SQLException exception)
    {

    }

    public java.math.BigDecimal getDfComponentId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFCOMPONENTID);

    }

    public void setDfComponentId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFCOMPONENTID, value);

    }

    public java.math.BigDecimal getDfCopyId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);

    }

    public void setDfCopyId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFCOPYID, value);

    }

    public BigDecimal getDfInstitutionId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFISNTITUTIONID);

    }

    public void setDfInstitutionId(BigDecimal value)
    {

        setValue(FIELD_DFISNTITUTIONID, value);

    }

    public void setDfLoanAmount(BigDecimal value)
    {

        setValue(FIELD_DFLOANAMOUNT, value);

    }

    public java.math.BigDecimal getDfLoanAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFLOANAMOUNT);

    }

    public String getDfComponentType()
    {

        return (String) getValue(FIELD_DFCOMPONENTTYPE);

    }

    public BigDecimal getDfDiscount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFDISCOUNT);

    }

    public BigDecimal getDfNetInstrestRate()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFNETINTRESTRATE);

    }

    public BigDecimal getDfPremium()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFPREMIUM);

    }

    public String getDfProductName()
    {

        return (String) getValue(FIELD_DFPRODUCTNAME);

    }

    public void setDfComponentType(String value)
    {

        setValue(FIELD_DFCOMPONENTTYPE, value);

    }

    public void setDfDiscount(BigDecimal value)
    {

        setValue(FIELD_DFDISCOUNT, value);

    }

    public void setDfNetInstrestRate(BigDecimal value)
    {

        setValue(FIELD_DFNETINTRESTRATE, value);

    }

    public void setDfPremium(BigDecimal value)
    {

        setValue(FIELD_DFPREMIUM, value);

    }

    public void setDfProductName(String value)
    {

        setValue(FIELD_DFPRODUCTNAME, value);

    }

    public BigDecimal getDfDealId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFDEALID);

    }

    public void setDfDealId(BigDecimal value)
    {

        setValue(FIELD_DFDEALID, value);

    }

    /**
     * ***************************MCM Impl Team XS_16.15
     * ************************
     */
    public String getDfPaymentTermDescription()
    {

        return (String) getValue(FIELD_DFPAYMENTTERMDESCRIPTION);

    }

    public void setDfPaymentTermDescription(String value)
    {

        setValue(FIELD_DFPAYMENTTERMDESCRIPTION, value);

    }

    public String getDfPaymentFrequency()
    {

        return (String) getValue(FIELD_DFPAYMENTFREQUENCY);

    }

    public void setDfPaymentFrequency(String value)
    {

        setValue(FIELD_DFPAYMENTFREQUENCY, value);

    }

    public java.math.BigDecimal getDfActualPaymentTerm()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFACTUALPAYMENTTERM);

    }

    public void setDfActualPaymentTerm(java.math.BigDecimal value)
    {

        setValue(FIELD_DFACTUALPAYMENTTERM, value);

    }

    public String getDfAdditionalDetails()
    {

        return (String) getValue(FIELD_DFADDITIONALDETAILS);

    }

    public void setDfAdditionalDetails(String value)
    {

        setValue(FIELD_DFADDITIONALDETAILS, value);

    }

    public BigDecimal getDfPostedRate()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFPOSTEDRATE);

    }

    public void setDfPostedRate(java.math.BigDecimal value)
    {

        setValue(FIELD_DFPOSTEDRATE, value);

    }

    public BigDecimal getDfTotalPayment()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFTOTALPAYMENT);

    }

    public void setDfTotalPayment(java.math.BigDecimal value)
    {

        setValue(FIELD_DFTOTALPAYMENT, value);

    }

    /** *************************************************************************** */
    
    /************************MCM XS_2.37 changes starts***********************************/ 
    
    /**
     * This method declaration gets the ComponentTypeId.
     * @param value: the new ComponentTypeId
     */
    public BigDecimal getDfComponentTypeId() {
        return (java.math.BigDecimal) getValue(FIELD_DFCOMPONENTTYPEID);
    }

    /**
     * This method declaration sets the ComponentTypeId.
     * @param value: the new ComponentTypeId
     */
    public void setDfComponentTypeId(java.math.BigDecimal value) {
        setValue(FIELD_DFCOMPONENTTYPEID, value);
    }

    /**
     * This method declaration gets the ProductId.
     * @param value: the new ProductId
     */
    public BigDecimal getDfProductId() {
        return (java.math.BigDecimal) getValue(FIELD_DFPRODUCTID);
    }

    /**
     * This method declaration sets the ProductId.
     * @param value: the new ProductId
     */
    public void setDfProductId(java.math.BigDecimal value) {
        setValue(FIELD_DFPRODUCTID, value);
    }

    /**
     * This method declaration gets the paymentFrequencyId.
     * @param value: the new paymentFrequencyId
     */
    public java.math.BigDecimal getDfPaymentFrequencyId() {
        return (java.math.BigDecimal) getValue(FIELD_DFPAYMENTFREQUENCYID);
    }

    /**
     * This method declaration sets the paymentFrequencyId.
     * @param value: the new paymentFrequencyId
     */
    public void setDfPaymentFrequencyId(java.math.BigDecimal value) {
        setValue(FIELD_DFPAYMENTFREQUENCYID, value);
    }
    /************************MCM XS_2.37 changes ends***********************************/
    
    /*********************MCM Impl Team XS_2.37 (dynamic drop down lists) changes starts******************/
    /**
     * This method declaration returns the value of field the dfPricingrateinventoryid.
     * 
     * @return the dfPricingrateinventoryid
     */
    public BigDecimal getDfPricingrateinventoryid() {
        return (BigDecimal) getValue(FIELD_DFPRICINGRATEINVENTORYID);
    }
    
    /**
     * This method declaration sets the the dfPricingrateinventoryid.
     * 
     * @param value  the new dfPricingrateinventoryid
     */
    public void setDfPricingrateinventoryid(BigDecimal value) {
        setValue(FIELD_DFPRICINGRATEINVENTORYID, value);
        
    }
    /*********************MCM Impl Team XS_2.37 (dynamic drop down lists) changes ends******************/
}
