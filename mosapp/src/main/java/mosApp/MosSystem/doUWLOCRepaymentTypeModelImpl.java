package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doUWLOCRepaymentTypeModelImpl
    extends QueryModelBase
    implements doUWLOCRepaymentTypeModel
{
  /**
   *
   *
   */
  public doUWLOCRepaymentTypeModelImpl()
  {
    super();
    setDataSourceName(DATA_SOURCE_NAME);

    setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

    setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

    setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

    setFieldSchema(FIELD_SCHEMA);

    initialize();

  }

  /**
   *
   *
   */
  public void initialize()
  {
  }

  // //////////////////////////////////////////////////////////////////////////////
  // NetDynamics-migrated events
  // //////////////////////////////////////////////////////////////////////////////

  protected String beforeExecute(ModelExecutionContext context, int queryType,
      String sql) throws ModelControlException
  {
    // TODO: Migrate specific onBeforeExecute code
    return sql;
  }

  protected void afterExecute(ModelExecutionContext context, int queryType)
      throws ModelControlException
  {
  }

  protected void onDatabaseError(ModelExecutionContext context, int queryType,
      SQLException exception)
  {
  }

  public java.math.BigDecimal getDfDealId()
  {
    return (java.math.BigDecimal) getValue(FIELD_DFDEALID);
  }

  public void setDfDealId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFDEALID, value);
  }

  public java.math.BigDecimal getDfCopyId()
  {
    return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);
  }

  public void setDfCopyId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFCOPYID, value);
  }
  
  //-------------------------------------------

  public java.math.BigDecimal getDfLOCRepaymentTypeId()
  {
    return (java.math.BigDecimal) getValue(FIELD_DFLOCREPAYMENTTYPEID);
  }

  public void setDfLOCRepaymentTypeId(java.math.BigDecimal value)
  {
    setValue(FIELD_DFLOCREPAYMENTTYPEID, value);
  }

  public String getDfLOCRepaymentTypeDescription()
  {
    return (String) getValue(FIELD_DFLOCREPAYMENTTYPEDESCRIPTION);
  }

  public void setDfLOCRepaymentTypeDescription(String value)
  {
    setValue(FIELD_DFLOCREPAYMENTTYPEDESCRIPTION, value);
  }

  // //////////////////////////////////////////////////////////////////////////
  // Obsolete Netdynamics Events - Require Manual Migration
  // //////////////////////////////////////////////////////////////////////////

  // //////////////////////////////////////////////////////////////////////////
  // Custom Methods - Require Manual Migration
  // //////////////////////////////////////////////////////////////////////////

  // //////////////////////////////////////////////////////////////////////////
  // Class variables
  // //////////////////////////////////////////////////////////////////////////

  // //////////////////////////////////////////////////////////////////////////////
  // Class variables
  // //////////////////////////////////////////////////////////////////////////////

  public static final String DATA_SOURCE_NAME = "jdbc/orcl";

  public static final String SELECT_SQL_TEMPLATE = "SELECT distinct "
      + " DEAL.DEALID, DEAL.COPYID, "
      + " LOCREPAYMENTTYPE.LOCREPAYMENTTYPEID, "
      + " LOCREPAYMENTTYPE.LOCREPAYMENTTYPEDESCRIPTION "
      + " FROM DEAL, LOCREPAYMENTTYPE __WHERE__ ";

  
  public static final String MODIFYING_QUERY_TABLE_NAME = "DEAL, LOCREPAYMENTTYPE";

  public static final String STATIC_WHERE_CRITERIA = " (DEAL.LOCREPAYMENTTYPEID  =  LOCREPAYMENTTYPE.LOCREPAYMENTTYPEID)";

  public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

  public static final String QUALIFIED_COLUMN_DFDEALID = "DEAL.DEALID";

  public static final String COLUMN_DFDEALID = "DEALID";

  public static final String QUALIFIED_COLUMN_DFCOPYID = "DEAL.COPYID";

  public static final String COLUMN_DFCOPYID = "COPYID";

  public static final String QUALIFIED_COLUMN_DFLOCREPAYMENTTYPEID = "LOCREPAYMENTTYPE.LOCREPAYMENTTYPEID";

  public static final String COLUMN_DFLOCREPAYMENTTYPEID = "LOCREPAYMENTTYPEID";

  public static final String QUALIFIED_COLUMN_DFLOCREPAYMENTTYPEDESCRIPTION = "LOCREPAYMENTTYPE.LOCREPAYMENTTYPEDESCRIPTION";

  public static final String COLUMN_DFLOCREPAYMENTTYPEDESCRIPTION = "LOCREPAYMENTTYPEDESCRIPTION";

  // //////////////////////////////////////////////////////////////////////////
  // Instance variables
  // //////////////////////////////////////////////////////////////////////////

  // //////////////////////////////////////////////////////////////////////////
  // Custom Members - Require Manual Migration
  // //////////////////////////////////////////////////////////////////////////

  static
  {

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFDEALID,
        COLUMN_DFDEALID, QUALIFIED_COLUMN_DFDEALID, java.math.BigDecimal.class,
        false, false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(FIELD_DFCOPYID,
        COLUMN_DFCOPYID, QUALIFIED_COLUMN_DFCOPYID, java.math.BigDecimal.class,
        false, false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        FIELD_DFLOCREPAYMENTTYPEID, COLUMN_DFLOCREPAYMENTTYPEID,
        QUALIFIED_COLUMN_DFLOCREPAYMENTTYPEID, java.math.BigDecimal.class,
        false, false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

    FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
        FIELD_DFLOCREPAYMENTTYPEDESCRIPTION, COLUMN_DFLOCREPAYMENTTYPEDESCRIPTION,
        QUALIFIED_COLUMN_DFLOCREPAYMENTTYPEDESCRIPTION, String.class, false, false,
        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

  }

}
