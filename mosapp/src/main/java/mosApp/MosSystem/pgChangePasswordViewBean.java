package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;
import java.text.*;

import com.basis100.log.*;
import com.basis100.picklist.*;
import com.filogix.express.web.jato.ExpressViewBeanBase; 
/**
 *
 *
 *
 */

public class pgChangePasswordViewBean extends ExpressViewBeanBase


{
	/**
	 *
	 *
	 */
	public pgChangePasswordViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);

		registerChildren();

		initialize();

	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_TBCURRENT))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBCURRENT,
				CHILD_TBCURRENT,
				CHILD_TBCURRENT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBNEW))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBNEW,
				CHILD_TBNEW,
				CHILD_TBNEW_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBVERIFY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBVERIFY,
				CHILD_TBVERIFY,
				CHILD_TBVERIFY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTOK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTOK,
				CHILD_BTOK,
				CHILD_BTOK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCHANGEMESSAGE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCHANGEMESSAGE,
				CHILD_STCHANGEMESSAGE,
				CHILD_STCHANGEMESSAGE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> by BILLY 15July2002
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		super.resetChildren(); 
		getTbCurrent().setValue(CHILD_TBCURRENT_RESET_VALUE);
		getTbNew().setValue(CHILD_TBNEW_RESET_VALUE);
		getTbVerify().setValue(CHILD_TBVERIFY_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getBtOK().setValue(CHILD_BTOK_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getStChangeMessage().setValue(CHILD_STCHANGEMESSAGE_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //=================================================
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren(); 
		registerChild(CHILD_TBCURRENT,TextField.class);
		registerChild(CHILD_TBNEW,TextField.class);
		registerChild(CHILD_TBVERIFY,TextField.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_BTOK,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_STCHANGEMESSAGE,StaticTextField.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    registerChild(CHILD_BTACTMSG,Button.class);
    //===========================================
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

    //logger = SysLog.getSysLogger("CHGPSWD");

    // This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

		ChangePasswordHandler handler =(ChangePasswordHandler) this.handler.cloneSS();


    handler.pageGetState(this);

    //logger.debug("CHPWDPAGE@afterAllModelsExecute: before populatePageDisplayFields()");
		handler.populatePageDisplayFields();
    //logger.debug("CHPWDPAGE@afterAllModelsExecute: after populatePageDisplayFields()");

		handler.pageSaveState();

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbCurrent()
	{
		return (TextField)getChild(CHILD_TBCURRENT);
	}


	/**
	 *
	 *
	 */
	public TextField getTbNew()
	{
		return (TextField)getChild(CHILD_TBNEW);
	}


	/**
	 *
	 *
	 */
	public TextField getTbVerify()
	{
		return (TextField)getChild(CHILD_TBVERIFY);
	}


	/**
	 *
	 *  Original auto-migration, should be changed. It is done below.
   *  Test it again.
	 */
	////public void handleBtCancelRequest(RequestInvocationEvent event)
	public void handleBtCancelRequest(RequestContext requestContext)
		throws ServletException, IOException
	{
		Button field = getBtCancel();
		ViewBean targetView = getViewBean(pgDefaultViewBean.class);

		ChangePasswordHandler handler =(ChangePasswordHandler) this.handler.cloneSS();

    handler.preHandlerProtocol(this);

		handler.handleCancel();

    handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
   //--> Modified to hanle hide or display the Button
   //--> By BILLY 10July2002
	public String endBtCancelDisplay(ChildContentDisplayEvent event)
	{
    //logger = SysLog.getSysLogger("CHGPSWD");
		//logger.debug("pgChgPswd@before allowCancel");
		ChangePasswordHandler handler =(ChangePasswordHandler) this.handler.cloneSS();
		handler.pageGetState(this);
    boolean isAllowed = handler.allowCancel();
    //logger.debug("pgChgPswd@after allowCancel() returns = " + isAllowed);
		handler.pageSaveState();

    if(isAllowed == true)
      // Same as PROCCESS in Netdynamics
		  return event.getContent();
    else
      // Same as SKIP in Netdynamics
      return "";
	}

  /**
	 *
	 * Original auto-conversion signature, should be changed. It is done below.
   * Test it again.
	 */
	////public void handleBtOKRequest(RequestInvocationEvent event)
	public void handleBtOKRequest(RequestContext requestContext)
		throws ServletException, IOException
	{
		Button btOK = getBtOK();

    ChangePasswordHandler handler =(ChangePasswordHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);

    handler.handleBtOKonWebEvent();

    handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtOK()
	{
		return (Button)getChild(CHILD_BTOK);
	}


	/**
	 *
	 *
	 */
  //--> Modified to hanle hide or display the Button
   //--> By BILLY 10July2002
	public String endBtOKDisplay(ChildContentDisplayEvent event)
	{
    //logger = SysLog.getSysLogger("CHGPSWD");
		//logger.debug("pgChgPswd@before allowRetry");
		ChangePasswordHandler handler =(ChangePasswordHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		boolean isAllowed = handler.allowRetry();
		//logger.debug("pgChgPswd@ allowRetry() return = " + isAllowed);
		handler.pageSaveState();

    if(isAllowed == true)
      // Same as PROCCESS in Netdynamics
		  return event.getContent();
    else
      // Same as SKIP in Netdynamics
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStChangeMessage()
	{
		return (StaticTextField)getChild(CHILD_STCHANGEMESSAGE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}
  //===========================================

  public void handleActMessageOK(String[] args)
	{
		ChangePasswordHandler handler =(ChangePasswordHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleActMessageOK(args);
		handler.postHandlerProtocol();
	}

  //==> This is missing -- Debug by Billy
  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       ChangePasswordHandler handler =(ChangePasswordHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }
  //================================================================================

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgChangePassword";
	public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgChangePassword.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBCURRENT="tbCurrent";
	public static final String CHILD_TBCURRENT_RESET_VALUE="";
	public static final String CHILD_TBNEW="tbNew";
	public static final String CHILD_TBNEW_RESET_VALUE="";
	public static final String CHILD_TBVERIFY="tbVerify";
	public static final String CHILD_TBVERIFY_RESET_VALUE="";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
	public static final String CHILD_BTOK="btOK";
	public static final String CHILD_BTOK_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_STCHANGEMESSAGE="stChangeMessage";
	public static final String CHILD_STCHANGEMESSAGE_RESET_VALUE="N";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	private ChangePasswordHandler handler=new ChangePasswordHandler();
	public SysLogger logger;
}

