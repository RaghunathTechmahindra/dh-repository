package mosApp.MosSystem;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import MosSystem.Mc;

import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;

/**
 *
 *
 *
 */
public class pgUWorksheetRepeatApplicantFinancialDetailsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgUWorksheetRepeatApplicantFinancialDetailsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doUWApplicantFinancialDetailsModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getHdFinancialApplicantId().setValue(CHILD_HDFINANCIALAPPLICANTID_RESET_VALUE);
		getHdFinancialApplicanCopyId().setValue(CHILD_HDFINANCIALAPPLICANCOPYID_RESET_VALUE);
		getCbUWPrimaryApplicant().setValue(CHILD_CBUWPRIMARYAPPLICANT_RESET_VALUE);
		getStFinancialApplicanFirstName().setValue(CHILD_STFINANCIALAPPLICANFIRSTNAME_RESET_VALUE);
		getStFinancialApplicanMiddleInitial().setValue(CHILD_STFINANCIALAPPLICANMIDDLEINITIAL_RESET_VALUE);
		getStFinancialApplicanLastName().setValue(CHILD_STFINANCIALAPPLICANLASTNAME_RESET_VALUE);
        getStFinancialApplicanSuffix().setValue(CHILD_STFINANCIALAPPLICANSUFFIX_RESET_VALUE);
		getStFinancialApplicanType().setValue(CHILD_STFINANCIALAPPLICANTYPE_RESET_VALUE);
		getStFinancialApplicanTotalIncome().setValue(CHILD_STFINANCIALAPPLICANTOTALINCOME_RESET_VALUE);
		getStFinancialApplicanTotalLiabilities().setValue(CHILD_STFINANCIALAPPLICANTOTALLIABILITIES_RESET_VALUE);
		getStFinancialApplicanTotalAssets().setValue(CHILD_STFINANCIALAPPLICANTOTALASSETS_RESET_VALUE);
		getStFinancialApplicantNetWorth().setValue(CHILD_STFINANCIALAPPLICANTNETWORTH_RESET_VALUE);
		getBtDeleteApplicant().setValue(CHILD_BTDELETEAPPLICANT_RESET_VALUE);
		getBtApplicantDetails().setValue(CHILD_BTAPPLICANTDETAILS_RESET_VALUE);
		getBtLiabilitiesDetails().setValue(CHILD_BTLIABILITIESDETAILS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_HDFINANCIALAPPLICANTID,HiddenField.class);
		registerChild(CHILD_HDFINANCIALAPPLICANCOPYID,HiddenField.class);
		registerChild(CHILD_CBUWPRIMARYAPPLICANT,ComboBox.class);
		registerChild(CHILD_STFINANCIALAPPLICANFIRSTNAME,StaticTextField.class);
		registerChild(CHILD_STFINANCIALAPPLICANMIDDLEINITIAL,StaticTextField.class);
		registerChild(CHILD_STFINANCIALAPPLICANLASTNAME,StaticTextField.class);
        registerChild(CHILD_STFINANCIALAPPLICANSUFFIX,StaticTextField.class);
		registerChild(CHILD_STFINANCIALAPPLICANTYPE,StaticTextField.class);
		registerChild(CHILD_STFINANCIALAPPLICANTOTALINCOME,StaticTextField.class);
		registerChild(CHILD_STFINANCIALAPPLICANTOTALLIABILITIES,StaticTextField.class);
		registerChild(CHILD_STFINANCIALAPPLICANTOTALASSETS,StaticTextField.class);
		registerChild(CHILD_STFINANCIALAPPLICANTNETWORTH,StaticTextField.class);
		registerChild(CHILD_BTDELETEAPPLICANT,Button.class);
		registerChild(CHILD_BTAPPLICANTDETAILS,Button.class);
		registerChild(CHILD_BTLIABILITIESDETAILS,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--start//
    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

    // 1. Setup the Yes/No/Co-App Options
    String yesStr = BXResources.getGenericMsg("YES_LABEL", handler.getTheSessionState().getLanguageId());
    String noStr = BXResources.getGenericMsg("NO_LABEL", handler.getTheSessionState().getLanguageId());
    String coAppStr = BXResources.getGenericMsg("COAPP_LABEL", handler.getTheSessionState().getLanguageId());

    cbUWPrimaryApplicantOptions.setOptions(new String[]{noStr, yesStr, coAppStr},new String[]{"N", "Y", "C"});
    //--Release2.1--end//

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_HDFINANCIALAPPLICANTID))
		{
			HiddenField child = new HiddenField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_HDFINANCIALAPPLICANTID,
				doUWApplicantFinancialDetailsModel.FIELD_DFBORROWERID,
				CHILD_HDFINANCIALAPPLICANTID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDFINANCIALAPPLICANCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_HDFINANCIALAPPLICANCOPYID,
				doUWApplicantFinancialDetailsModel.FIELD_DFCOPYID,
				CHILD_HDFINANCIALAPPLICANCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBUWPRIMARYAPPLICANT))
		{
			ComboBox child = new ComboBox( this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_CBUWPRIMARYAPPLICANT,
				doUWApplicantFinancialDetailsModel.FIELD_DFPRIMARYFLAG,
				CHILD_CBUWPRIMARYAPPLICANT_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbUWPrimaryApplicantOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STFINANCIALAPPLICANFIRSTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_STFINANCIALAPPLICANFIRSTNAME,
				doUWApplicantFinancialDetailsModel.FIELD_DFFIRSTNAME,
				CHILD_STFINANCIALAPPLICANFIRSTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFINANCIALAPPLICANMIDDLEINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_STFINANCIALAPPLICANMIDDLEINITIAL,
				doUWApplicantFinancialDetailsModel.FIELD_DFBORROWERMIDDLENAME,
				CHILD_STFINANCIALAPPLICANMIDDLEINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFINANCIALAPPLICANLASTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_STFINANCIALAPPLICANLASTNAME,
				doUWApplicantFinancialDetailsModel.FIELD_DFLASTNAME,
				CHILD_STFINANCIALAPPLICANLASTNAME_RESET_VALUE,
				null);
			return child;
		}
        if (name.equals(CHILD_STFINANCIALAPPLICANSUFFIX))
        {
            StaticTextField child = new StaticTextField(this,
                getdoUWApplicantFinancialDetailsModel(),
                CHILD_STFINANCIALAPPLICANSUFFIX,
                doUWApplicantFinancialDetailsModel.FIELD_DFSUFFIX,
                CHILD_STFINANCIALAPPLICANSUFFIX_RESET_VALUE,
                null);
            return child;
        }
		else
		if (name.equals(CHILD_STFINANCIALAPPLICANTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_STFINANCIALAPPLICANTYPE,
				doUWApplicantFinancialDetailsModel.FIELD_DFBORROWERTYPEDESC,
				CHILD_STFINANCIALAPPLICANTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFINANCIALAPPLICANTOTALINCOME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_STFINANCIALAPPLICANTOTALINCOME,
				doUWApplicantFinancialDetailsModel.FIELD_DFTOTALINCOMEAMOUNT,
				CHILD_STFINANCIALAPPLICANTOTALINCOME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFINANCIALAPPLICANTOTALLIABILITIES))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_STFINANCIALAPPLICANTOTALLIABILITIES,
				doUWApplicantFinancialDetailsModel.FIELD_DFTOTALLIABILITYAMOUNT,
				CHILD_STFINANCIALAPPLICANTOTALLIABILITIES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFINANCIALAPPLICANTOTALASSETS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_STFINANCIALAPPLICANTOTALASSETS,
				doUWApplicantFinancialDetailsModel.FIELD_DFTOTALASSETAMOUNT,
				CHILD_STFINANCIALAPPLICANTOTALASSETS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STFINANCIALAPPLICANTNETWORTH))
		{
			StaticTextField child = new StaticTextField(this,
				getdoUWApplicantFinancialDetailsModel(),
				CHILD_STFINANCIALAPPLICANTNETWORTH,
				doUWApplicantFinancialDetailsModel.FIELD_DFBORROWERNETWORTH,
				CHILD_STFINANCIALAPPLICANTNETWORTH_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEAPPLICANT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEAPPLICANT,
				CHILD_BTDELETEAPPLICANT,
				CHILD_BTDELETEAPPLICANT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTAPPLICANTDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTAPPLICANTDETAILS,
				CHILD_BTAPPLICANTDETAILS,
				CHILD_BTAPPLICANTDETAILS_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTLIABILITIESDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTLIABILITIESDETAILS,
				CHILD_BTLIABILITIESDETAILS,
				CHILD_BTLIABILITIESDETAILS_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFinancialApplicantId()
	{
		return (HiddenField)getChild(CHILD_HDFINANCIALAPPLICANTID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdFinancialApplicanCopyId()
	{
		return (HiddenField)getChild(CHILD_HDFINANCIALAPPLICANCOPYID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbUWPrimaryApplicant()
	{
		return (ComboBox)getChild(CHILD_CBUWPRIMARYAPPLICANT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFinancialApplicanFirstName()
	{
		return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFinancialApplicanMiddleInitial()
	{
		return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANMIDDLEINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFinancialApplicanLastName()
	{
		return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANLASTNAME);
	}

    /**
     *
     *
     */
    public StaticTextField getStFinancialApplicanSuffix()
    {
        return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANSUFFIX);
    }
    
	/**
	 *
	 *
	 */
	public StaticTextField getStFinancialApplicanType()
	{
		return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFinancialApplicanTotalIncome()
	{
		return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANTOTALINCOME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFinancialApplicanTotalLiabilities()
	{
		return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANTOTALLIABILITIES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFinancialApplicanTotalAssets()
	{
		return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANTOTALASSETS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStFinancialApplicantNetWorth()
	{
		return (StaticTextField)getChild(CHILD_STFINANCIALAPPLICANTNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void handleBtDeleteApplicantRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btDeleteApplicant_onWebEvent method
    //logger = SysLog.getSysLogger("PGUWPAFDTV");

    UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();

    //logger.debug("pgUWPAFDTV@handleBtDeleteApplicantRequest::Parent: " + (pgUWorksheetViewBean) this.getParent());

    handler.preHandlerProtocol((ViewBean) this.getParent());

    int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
    //logger.debug("pgUWPAFDTV@handleBtDeleteApplicantRequest::TileIndex: " + tileIndx);

		handler.handleDelete(false,
                         1,
                         tileIndx,
                         "RepeatApplicantFinancialDetails/hdFinancialApplicantId",
                         "borrowerId",
                         "RepeatApplicantFinancialDetails/hdFinancialApplicanCopyId",
                         "Borrower");

		//logger.debug("pgUWPAFDTV@handleBtDeleteApplicantRequest::After handleDelete)");

    handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteApplicant()
	{
		return (Button)getChild(CHILD_BTDELETEAPPLICANT);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteApplicantDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteApplicant_onBeforeHtmlOutputEvent method
		UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";

	}


	/**
	 *
	 *
	 */
	public void handleBtApplicantDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleApplicantsView(handler.getRowNdxFromWebEventMethod(event),
                                "RepeatApplicantFinancialDetails/hdFinancialApplicantId",
                                "RepeatApplicantFinancialDetails/hdFinancialApplicanCopyId");

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtApplicantDetails()
	{
		return (Button)getChild(CHILD_BTAPPLICANTDETAILS);
	}


	/**
	 *
	 *
	 */
	public void handleBtLiabilitiesDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		UWorksheetHandler handler =(UWorksheetHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());

		handler.handleApplicantsView(handler.getRowNdxFromWebEventMethod(event),
                                "RepeatApplicantFinancialDetails/hdFinancialApplicantId",
                                "RepeatApplicantFinancialDetails/hdFinancialApplicanCopyId",
                                Mc.TARGET_APP_CREDIT_BUREAU_LIAB);

		handler.postHandlerProtocol();

	}


	/**
	 *
	 *
	 */
	public Button getBtLiabilitiesDetails()
	{
		return (Button)getChild(CHILD_BTLIABILITIESDETAILS);
	}


	/**
	 *
	 *
	 */
	public doUWApplicantFinancialDetailsModel getdoUWApplicantFinancialDetailsModel()
	{
		if (doUWApplicantFinancialDetails == null)
			doUWApplicantFinancialDetails = (doUWApplicantFinancialDetailsModel) getModel(doUWApplicantFinancialDetailsModel.class);
		return doUWApplicantFinancialDetails;
	}


	/**
	 *
	 *
	 */
	public void setdoUWApplicantFinancialDetailsModel(doUWApplicantFinancialDetailsModel model)
	{
			doUWApplicantFinancialDetails = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_HDFINANCIALAPPLICANTID="hdFinancialApplicantId";
	public static final String CHILD_HDFINANCIALAPPLICANTID_RESET_VALUE="";
	public static final String CHILD_HDFINANCIALAPPLICANCOPYID="hdFinancialApplicanCopyId";
	public static final String CHILD_HDFINANCIALAPPLICANCOPYID_RESET_VALUE="";
	public static final String CHILD_CBUWPRIMARYAPPLICANT="cbUWPrimaryApplicant";
	public static final String CHILD_CBUWPRIMARYAPPLICANT_RESET_VALUE="N";

  //--Release2.1--//
  //// This OptionList population is done now in the BeginDisplay via the BXResources
	////private static OptionList cbUWPrimaryApplicantOptions=new OptionList(new String[]{"No", "Yes", "Co-App"},new String[]{"N", "Y", "C"});
	private OptionList cbUWPrimaryApplicantOptions=new OptionList();

	public static final String CHILD_STFINANCIALAPPLICANFIRSTNAME="stFinancialApplicanFirstName";
	public static final String CHILD_STFINANCIALAPPLICANFIRSTNAME_RESET_VALUE="";
	public static final String CHILD_STFINANCIALAPPLICANMIDDLEINITIAL="stFinancialApplicanMiddleInitial";
	public static final String CHILD_STFINANCIALAPPLICANMIDDLEINITIAL_RESET_VALUE="";
	public static final String CHILD_STFINANCIALAPPLICANLASTNAME="stFinancialApplicanLastName";
	public static final String CHILD_STFINANCIALAPPLICANLASTNAME_RESET_VALUE="";
    public static final String CHILD_STFINANCIALAPPLICANSUFFIX="stFinancialApplicanSuffix";
    public static final String CHILD_STFINANCIALAPPLICANSUFFIX_RESET_VALUE="";
	public static final String CHILD_STFINANCIALAPPLICANTYPE="stFinancialApplicanType";
	public static final String CHILD_STFINANCIALAPPLICANTYPE_RESET_VALUE="";
	public static final String CHILD_STFINANCIALAPPLICANTOTALINCOME="stFinancialApplicanTotalIncome";
	public static final String CHILD_STFINANCIALAPPLICANTOTALINCOME_RESET_VALUE="";
	public static final String CHILD_STFINANCIALAPPLICANTOTALLIABILITIES="stFinancialApplicanTotalLiabilities";
	public static final String CHILD_STFINANCIALAPPLICANTOTALLIABILITIES_RESET_VALUE="";
	public static final String CHILD_STFINANCIALAPPLICANTOTALASSETS="stFinancialApplicanTotalAssets";
	public static final String CHILD_STFINANCIALAPPLICANTOTALASSETS_RESET_VALUE="";
	public static final String CHILD_STFINANCIALAPPLICANTNETWORTH="stFinancialApplicantNetWorth";
	public static final String CHILD_STFINANCIALAPPLICANTNETWORTH_RESET_VALUE="";
	public static final String CHILD_BTDELETEAPPLICANT="btDeleteApplicant";
	public static final String CHILD_BTDELETEAPPLICANT_RESET_VALUE=" ";
	public static final String CHILD_BTAPPLICANTDETAILS="btApplicantDetails";
	public static final String CHILD_BTAPPLICANTDETAILS_RESET_VALUE=" ";
	public static final String CHILD_BTLIABILITIESDETAILS="btLiabilitiesDetails";
	public static final String CHILD_BTLIABILITIESDETAILS_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doUWApplicantFinancialDetailsModel doUWApplicantFinancialDetails=null;
  public SysLogger logger;
	private UWorksheetHandler handler=new UWorksheetHandler();

}

