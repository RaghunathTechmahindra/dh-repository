/*
 * @(#)doFixedPriceClosingPackageModelImpl.java    2006-1-26
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;

/**
 * doFixedPriceClosingPackageModelImpl - 
 *
 * @version   1.0 2006-1-26
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class doFixedPriceClosingPackageModelImpl extends QueryModelBase
    implements doFixedPriceClosingPackageModel {

    // The logger
    private static Log _log =
        LogFactory.getLog(doFixedPriceClosingPackageModelImpl.class);

    // the datbase source name.
    public static final String DATA_SOURCE_NAME = "jdbc/orcl";
    // the select SQL template.
    public static final String SELECT_SQL_TEMPLATE =
        "SELECT " +
        "SERVICE_REQUEST.DEALID, " +
        "SERVICE_REQUEST.COPYID, " +
        "to_char(SERVICE_REQUEST.SERVICE_REQUEST_STATUS_ID) STATUSID_STR, " +
        "SERVICE_REQUEST.STATUS_DATE, " +
		"SERVICE_REQUEST_CONTACT.NAME_FIRST, " +
		"SERVICE_REQUEST_CONTACT.NAME_LAST, " +
		"SERVICE_REQUEST_CONTACT.EMAIL_ADDRESS, " +
		"SERVICE_REQUEST_CONTACT.WORK_AREA_CODE, " +
		"SERVICE_REQUEST_CONTACT.WORK_PHONE_NUMBER, " +
		"SERVICE_REQUEST_CONTACT.WORK_EXTENSION, " +
		"SERVICE_REQUEST_CONTACT.HOME_AREA_CODE, " +
		"SERVICE_REQUEST_CONTACT.HOME_PHONE_NUMBER, " +
		"SERVICE_REQUEST_CONTACT.CELL_AREA_CODE, " +
		"SERVICE_REQUEST_CONTACT.CELL_PHONE_NUMBER, " +
		"SERVICE_REQUEST_CONTACT.COMMENTS, " +
		"SERVICE_REQUEST_CONTACT.BORROWERID " +
        "FROM " +
        "SERVICE_REQUEST, SERVICE_REQUEST_CONTACT, SERVICE_PRODUCT, " +
        "SERVICE_TYPE " +
        "__WHERE__ ";

    // the query table names.
    public static final String MODIFYING_QUERY_TABLE_NAME =
        "SERVICE_REQUEST, SERVICE_REQUEST_CONTACT, SERVICE_PRODUCT";
    // the static query criteria.
    public static final String STATIC_WHERE_CRITERIA =
        "";
    // the query field schema for this model.
    public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

    // Column definition for each field.
    public static final String QUALIFIED_COLUMN_DFFPCPDEALID =
        "SERVICE_REQUEST.DEALID";
    public static final String COLUMN_DFFPCPDEALID = "DEALID";

    public static final String QUALIFIED_COLUMN_DFFPCPCOPYID =
        "SERVICE_REQUEST.COPYID";
    public static final String COLUMN_DFFPCPCOPYID = "COPYID";

    // Setting up the relationship between the column definition and the field
    // bound names, which are definited in the interface.
    static {

        FIELD_SCHEMA.addFieldDescriptor (
            new QueryFieldDescriptor(
                FIELD_DFFPCPDEALID,
                COLUMN_DFFPCPDEALID,
                QUALIFIED_COLUMN_DFFPCPDEALID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFFPCPCOPYID,
                COLUMN_DFFPCPCOPYID,
                QUALIFIED_COLUMN_DFFPCPCOPYID,
                BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""
            )
        );
    }

    /**
     * Constructor function
     */
    public doFixedPriceClosingPackageModelImpl() {

        super();
        setDataSourceName(DATA_SOURCE_NAME);
        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);
        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);
        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);
        setFieldSchema(FIELD_SCHEMA);
        initialize();
    }

    /**
     * initializing.  do nothing for now.
     */
    public void initialize() {}

    /**
     * before execute the SQL.
     */
    protected String beforeExecute(ModelExecutionContext context,
                                   int queryType, String sql)
        throws ModelControlException {

        if (_log.isDebugEnabled())
            _log.debug("SQL befor Execute: " + sql);
        return sql;
    }

    /**
     * after execute, we need change some language sensitive value for the
     * result.
     */
    protected void afterExecute(ModelExecutionContext context,
                                int queryType)
        throws ModelControlException {

        // try to get language id from the session state model.
        String defaultInstanceStateName =
            getRequestContext().getModelManager().
            getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl sessionState = (SessionStateModelImpl)
            getRequestContext().getModelManager().
            getModel(SessionStateModel.class,
                     defaultInstanceStateName, true);
        int languageId = sessionState.getLanguageId();
		int institutionId = sessionState.getDealInstitutionId();

        // get the status from the BXResources and set to the field.
        _log.debug("Trying to get the status from BXResources");
        setDfFPCPRequestStatus(
                      BXResources.
                      getPickListDescription(institutionId, "CLOSINGSERVICEREQUESTSTATUS",
                                             getDfFPCPRequestStatus(),
                                             languageId));
    }

    /**
     * getter and setter implementation.
     */
    public BigDecimal getDfFPCPDealId() {
        return (BigDecimal) getValue(FIELD_DFFPCPDEALID);
    }
    public void setDfFPCPDealId(BigDecimal id) {
        setValue(FIELD_DFFPCPDEALID, id);
    }

    public BigDecimal getDfFPCPCopyId() {
        return (BigDecimal) getValue(FIELD_DFFPCPCOPYID);
    }
    public void setDfFPCPCopyId(BigDecimal id) {
        setValue(FIELD_DFFPCPCOPYID, id);
    }

    public String getDfFPCPRequestStatus() {
        return (String) getValue(FIELD_DFFPCPREQUESTSTATUS);
    }
    public void setDfFPCPRequestStatus(String status) {
        setValue(FIELD_DFFPCPREQUESTSTATUS, status);
    }

    public Timestamp getDfFPCPRequestStatusDate() {
        return (Timestamp) getValue(FIELD_DFFPCPREQUESTSTATUSDATE);
    }
    public void setDfFPCPRequestStatusDate(Timestamp value) {
        setValue(FIELD_DFFPCPREQUESTSTATUSDATE, value);
    }

    public String getDfFPCPContactFirstName() {
        return (String) getValue(FIELD_DFFPCPCONTACTFIRSTNAME);
    }
    public void setDfFPCPContactFirstName(String value) {
        setValue(FIELD_DFFPCPCONTACTFIRSTNAME, value);
    }

    public String getDfFPCPContactLastName() {
        return (String) getValue(FIELD_DFFPCPCONTACTLASTNAME);
    }
    public void setDfFPCPContactLastName(String value) {
        setValue(FIELD_DFFPCPCONTACTLASTNAME, value);
    }

    public String getDfFPCPContactEmail() {
        return (String) getValue(FIELD_DFFPCPCONTACTEMAIL);
    }
    public void setDfFPCPContactEmail(String value) {
        setValue(FIELD_DFFPCPCONTACTEMAIL, value);
    }

    public String getDfFPCPContactComments() {
        return (String) getValue(FIELD_DFFPCPCONTACTCOMMENTS);
    }
    public void setDfFPCPContactComments(String value) {
        setValue(FIELD_DFFPCPCONTACTCOMMENTS, value);
    }

    public String getDfFPCPContactWorkPhoneArea() {
        return (String) getValue(FIELD_DFFPCPCONTACTWORKPHONEAREA);
    }
    public void setDfFPCPContactWorkPhoneArea(String value) {
        setValue(FIELD_DFFPCPCONTACTWORKPHONEAREA, value);
    }

    public String getDfFPCPContactWorkPhoneNum() {
        return (String) getValue(FIELD_DFFPCPCONTACTWORKPHONENUM);
    }
    public void setDfFPCPContactWorkPhoneNum(String value) {
        setValue(FIELD_DFFPCPCONTACTWORKPHONENUM, value);
    }

    public String getDfFPCPContactWorkPhoneExt() {
        return (String) getValue(FIELD_DFFPCPCONTACTWORKPHONEEXT);
    }
    public void setDfFPCPContactWorkPhoneExt(String value) {
        setValue(FIELD_DFFPCPCONTACTWORKPHONEEXT, value);
    }

    public String getDfFPCPContactCellPhoneArea() {
        return (String) getValue(FIELD_DFFPCPCONTACTCELLPHONEAREA);
    }
    public void setDfFPCPContactCellPhoneArea(String value) {
        setValue(FIELD_DFFPCPCONTACTCELLPHONEAREA, value);
    }

    public String getDfFPCPContactCellPhoneNum() {
        return (String) getValue(FIELD_DFFPCPCONTACTCELLPHONENUM);
    }
    public void setDfFPCPContactCellPhoneNum(String value) {
        setValue(FIELD_DFFPCPCONTACTCELLPHONENUM, value);
    }

    public String getDfFPCPContactHomePhoneArea() {
        return (String) getValue(FIELD_DFFPCPCONTACTHOMEPHONEAREA);
    }
    public void setDfFPCPContactHomePhoneArea(String value) {
        setValue(FIELD_DFFPCPCONTACTHOMEPHONEAREA, value);
    }

    public String getDfFPCPContactHomePhoneNum() {
        return (String) getValue(FIELD_DFFPCPCONTACTHOMEPHONENUM);
    }
    public void setDfFPCPContactHomePhoneNum(String value) {
        setValue(FIELD_DFFPCPCONTACTHOMEPHONENUM, value);
    }
}
