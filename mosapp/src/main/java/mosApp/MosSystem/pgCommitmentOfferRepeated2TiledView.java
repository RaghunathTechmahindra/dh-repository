package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;

/**
 *
 *
 *
 */
public class pgCommitmentOfferRepeated2TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated2TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(200);
		setPrimaryModelClass( doStdConditionModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getCbStdAction().setValue(CHILD_CBSTDACTION_RESET_VALUE);
		getBtStdDeleteCondition().setValue(CHILD_BTSTDDELETECONDITION_RESET_VALUE);
		getHdStdDocTrackId().setValue(CHILD_HDSTDDOCTRACKID_RESET_VALUE);
		getStStdCondLabel().setValue(CHILD_STSTDCONDLABEL_RESET_VALUE);
		getBtStdDetail().setValue(CHILD_BTSTDDETAIL_RESET_VALUE);
    //--TD_CONDR_CR--start//
		getHdStdDocTrackId().setValue(CHILD_HDSTDDOCTRACKID_RESET_VALUE);
		getHdStdCopyId().setValue(CHILD_HDSTDCOPYID_RESET_VALUE);
		getHdStdRoleId().setValue(CHILD_HDSTDROLEID_RESET_VALUE);
		getHdStdRequestDate().setValue(CHILD_HDSTDREQUESTDATE_RESET_VALUE);
		getHdStdStatusDate().setValue(CHILD_HDSTDSTATUSDATE_RESET_VALUE);
		getHdStdDocStatus().setValue(CHILD_HDSTDDOCSTATUS_RESET_VALUE);
		getHdStdDocText().setValue(CHILD_HDSTDDOCTEXT_RESET_VALUE);
		getHdStdResponsibility().setValue(CHILD_HDSTDRESPONSIBILITY_RESET_VALUE);
		getHdStdDocumentLabel().setValue(CHILD_HDSTDDOCUMENTLABEL_RESET_VALUE);
    //--TD_CONDR_CR--end//
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_CBSTDACTION,ComboBox.class);
		registerChild(CHILD_BTSTDDELETECONDITION,Button.class);
		registerChild(CHILD_HDSTDDOCTRACKID,HiddenField.class);
		registerChild(CHILD_STSTDCONDLABEL,StaticTextField.class);
		registerChild(CHILD_BTSTDDETAIL,Button.class);
    //--TD_CONDR_CR--start//
		registerChild(CHILD_HDSTDDOCTRACKID,HiddenField.class);
		registerChild(CHILD_HDSTDCOPYID,HiddenField.class);
		registerChild(CHILD_HDSTDROLEID,HiddenField.class);
		registerChild(CHILD_HDSTDREQUESTDATE,HiddenField.class);
		registerChild(CHILD_HDSTDSTATUSDATE,HiddenField.class);
		registerChild(CHILD_HDSTDDOCSTATUS,HiddenField.class);
		registerChild(CHILD_HDSTDDOCTEXT,HiddenField.class);
		registerChild(CHILD_HDSTDRESPONSIBILITY,HiddenField.class);
		registerChild(CHILD_HDSTDDOCUMENTLABEL,HiddenField.class);
    //--TD_CONDR_CR--end//
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoStdConditionModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 26Nov2002
    cbStdActionOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			boolean retval = handler.checkDisplayThisRow("Repeated2");
			handler.pageSaveState();
			return retval;
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_CBSTDACTION))
		{
			ComboBox child = new ComboBox( this,
				getdoStdConditionModel(),
				CHILD_CBSTDACTION,
				doStdConditionModel.FIELD_DFACTION,
				CHILD_CBSTDACTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbStdActionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTSTDDELETECONDITION))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSTDDELETECONDITION,
				CHILD_BTSTDDELETECONDITION,
				CHILD_BTSTDDELETECONDITION_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HDSTDDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDDOCTRACKID,
				doStdConditionModel.FIELD_DFSTDDOCTRACKID,
				CHILD_HDSTDDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSTDCONDLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoStdConditionModel(),
				CHILD_STSTDCONDLABEL,
				doStdConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STSTDCONDLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSTDDETAIL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSTDDETAIL,
				CHILD_BTSTDDETAIL,
				CHILD_BTSTDDETAIL_RESET_VALUE,
				null);
				return child;

		}
    //--TD_CONDR_CR--start//
		else
		if (name.equals(CHILD_HDSTDDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDDOCTRACKID,
				doStdConditionModel.FIELD_DFDOCTRACKID,
				CHILD_HDSTDDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDCOPYID,
				doStdConditionModel.FIELD_DFCOPYID,
				CHILD_HDSTDCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDROLEID))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDROLEID,
				doStdConditionModel.FIELD_DFROLEID,
				CHILD_HDSTDROLEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDREQUESTDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDREQUESTDATE,
				doStdConditionModel.FIELD_DFREQUESTDATE,
				CHILD_HDSTDREQUESTDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDSTATUSDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDSTATUSDATE,
				doStdConditionModel.FIELD_DFSTATUSCHANGEDATE,
				CHILD_HDSTDSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDDOCSTATUS))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDDOCSTATUS,
				doStdConditionModel.FIELD_DFSTATUS,
				CHILD_HDSTDDOCSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDDOCTEXT))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDDOCTEXT,
				doStdConditionModel.FIELD_DFDOCUMENTTEXT,
				CHILD_HDSTDDOCTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDRESPONSIBILITY))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDRESPONSIBILITY,
				doStdConditionModel.FIELD_DFRESPONSIBILITY,
				CHILD_HDSTDRESPONSIBILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDDOCUMENTLABEL))
		{
			HiddenField child = new HiddenField(this,
				getdoStdConditionModel(),
				CHILD_HDSTDDOCUMENTLABEL,
				doStdConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_HDSTDDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
  //--TD_CONDR_CR--end//
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbStdAction()
	{
		return (ComboBox)getChild(CHILD_CBSTDACTION);
	}

	/**
	 *
	 *
	 */
	static class CbStdActionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbStdActionOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 26Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(), 
                                                            "DOCUMENTSTATUS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public void handleBtStdDeleteConditionRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btStdDeleteCondition_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleDelete(handler.getRowNdxFromWebEventMethod(event), "btStdDeleteCondition");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtStdDeleteCondition()
	{
		return (Button)getChild(CHILD_BTSTDDELETECONDITION);
	}


	/**
	 *
	 *
	 */
	public String endBtStdDeleteConditionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btStdDeleteCondition_onBeforeHtmlOutputEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDSTDDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStStdCondLabel()
	{
		return (StaticTextField)getChild(CHILD_STSTDCONDLABEL);
	}


	/**
	 *
	 *
	 */
	public void handleBtStdDetailRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btStdDetail_onWebEvent method
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();

		//duplicate conditions
		handler.setDDCUpdate(false);

		handler.preHandlerProtocol(this.getParentViewBean());

    //--TD_CONDR_CR--start--//
    //// PageCondition6(true) forces to go to the DocTracking Detail Screen style
    //// with a possibility to edit both Default and Standard conditions.
    //// Default is "N": original Condtion Details style (read only).
    if (handler.getTheSessionState().getCurrentPage().getPageCondition6() == false) {
		  handler.navigateToDetail(2, handler.getRowNdxFromWebEventMethod(event));
    } else if (handler.getTheSessionState().getCurrentPage().getPageCondition6() == true) {
		    handler.navigateToDetailDocTrackStyle(2, handler.getRowNdxFromWebEventMethod(event));
    }
    //--TD_CONDR_CR--end--//
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtStdDetail()
	{
		return (Button)getChild(CHILD_BTSTDDETAIL);
	}

  //--TD_CONDR_CR--start//
	/**
	 *
	 *
	 */
	public HiddenField getHdStdCopyId()
	{
		return (HiddenField)getChild(CHILD_HDSTDCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdRoleId()
	{
		return (HiddenField)getChild(CHILD_HDSTDROLEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdRequestDate()
	{
		return (HiddenField)getChild(CHILD_HDSTDREQUESTDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdStatusDate()
	{
		return (HiddenField)getChild(CHILD_HDSTDSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdDocStatus()
	{
		return (HiddenField)getChild(CHILD_HDSTDDOCSTATUS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdDocText()
	{
		return (HiddenField)getChild(CHILD_HDSTDDOCTEXT);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdResponsibility()
	{
		return (HiddenField)getChild(CHILD_HDSTDRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdDocumentLabel()
	{
		return (HiddenField)getChild(CHILD_HDSTDDOCUMENTLABEL);
	}
  //--TD_CONDR_CR--end//

	/**
	 *
	 *
	 */
	public doStdConditionModel getdoStdConditionModel()
	{
		if (doStdCondition == null)
			doStdCondition = (doStdConditionModel) getModel(doStdConditionModel.class);
		return doStdCondition;
	}


	/**
	 *
	 *
	 */
	public void setdoStdConditionModel(doStdConditionModel model)
	{
			doStdCondition = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_CBSTDACTION="cbStdAction";
	public static final String CHILD_CBSTDACTION_RESET_VALUE="";
  //--Release2.1--//
	//private static CbStdActionOptionList cbStdActionOptions=new CbStdActionOptionList();
  private CbStdActionOptionList cbStdActionOptions=new CbStdActionOptionList();
	public static final String CHILD_BTSTDDELETECONDITION="btStdDeleteCondition";
	public static final String CHILD_BTSTDDELETECONDITION_RESET_VALUE="Delete Condition";
	public static final String CHILD_HDSTDDOCTRACKID="hdStdDocTrackId";
	public static final String CHILD_HDSTDDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_STSTDCONDLABEL="stStdCondLabel";
	public static final String CHILD_STSTDCONDLABEL_RESET_VALUE="";
	public static final String CHILD_BTSTDDETAIL="btStdDetail";
	public static final String CHILD_BTSTDDETAIL_RESET_VALUE=" ";
  //--TD_CONDR_CR--start--//
	public static final String CHILD_HDSTDCOPYID="hdStdCopyId";
	public static final String CHILD_HDSTDCOPYID_RESET_VALUE="";
	public static final String CHILD_HDSTDROLEID="hdStdRoleId";
	public static final String CHILD_HDSTDROLEID_RESET_VALUE="";
	public static final String CHILD_HDSTDREQUESTDATE="hdStdRequestDate";
	public static final String CHILD_HDSTDREQUESTDATE_RESET_VALUE="";
	public static final String CHILD_HDSTDSTATUSDATE="hdStdStatusDate";
	public static final String CHILD_HDSTDSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_HDSTDDOCSTATUS="hdStdDocStatus";
	public static final String CHILD_HDSTDDOCSTATUS_RESET_VALUE="";
	public static final String CHILD_HDSTDDOCTEXT="hdStdDocText";
	public static final String CHILD_HDSTDDOCTEXT_RESET_VALUE="";
	public static final String CHILD_HDSTDRESPONSIBILITY="hdStdResponsibility";
	public static final String CHILD_HDSTDRESPONSIBILITY_RESET_VALUE="";
	public static final String CHILD_HDSTDDOCUMENTLABEL="hdStdDocumentLabel";
	public static final String CHILD_HDSTDDOCUMENTLABEL_RESET_VALUE="";
  //--TD_CONDR_CR--end--//


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doStdConditionModel doStdCondition=null;
  private ConditionsReviewHandler handler=new ConditionsReviewHandler();

}

