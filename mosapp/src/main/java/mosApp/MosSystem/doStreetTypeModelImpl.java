package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doStreetTypeModelImpl extends QueryModelBase
	implements doStreetTypeModel
{
	/**
	 *
	 *
	 */
	public doStreetTypeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getMOS_STREETTYPE_STREETTYPEID()
	{
		return (java.math.BigDecimal)getValue(FIELD_MOS_STREETTYPE_STREETTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setMOS_STREETTYPE_STREETTYPEID(java.math.BigDecimal value)
	{
		setValue(FIELD_MOS_STREETTYPE_STREETTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getMOS_STREETTYPE_STDESCRIPTION()
	{
		return (String)getValue(FIELD_MOS_STREETTYPE_STDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setMOS_STREETTYPE_STDESCRIPTION(String value)
	{
		setValue(FIELD_MOS_STREETTYPE_STDESCRIPTION,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL STREETTYPE.STREETTYPEID, STREETTYPE.STDESCRIPTION FROM STREETTYPE  __WHERE__  ORDER BY STREETTYPE.STDESCRIPTION  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="STREETTYPE";
	public static final String STATIC_WHERE_CRITERIA=" STREETTYPE.STREETTYPEID <> 0 ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_MOS_STREETTYPE_STREETTYPEID="STREETTYPE.STREETTYPEID";
	public static final String COLUMN_MOS_STREETTYPE_STREETTYPEID="STREETTYPEID";
	public static final String QUALIFIED_COLUMN_MOS_STREETTYPE_STDESCRIPTION="STREETTYPE.STDESCRIPTION";
	public static final String COLUMN_MOS_STREETTYPE_STDESCRIPTION="STDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_STREETTYPE_STREETTYPEID,
				COLUMN_MOS_STREETTYPE_STREETTYPEID,
				QUALIFIED_COLUMN_MOS_STREETTYPE_STREETTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_STREETTYPE_STDESCRIPTION,
				COLUMN_MOS_STREETTYPE_STDESCRIPTION,
				QUALIFIED_COLUMN_MOS_STREETTYPE_STDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

