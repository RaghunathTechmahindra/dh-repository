package mosApp.MosSystem;

/**
 * 18/Oct/2006 DVG #DG526 #4916  DJ 3.1 - Apostrophes appear as "accent graves" when entered in a note
 */


import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

import com.basis100.log.*;
import com.basis100.deal.util.StringUtil;

/**
 *
 *
 *
 */
public class pgDealNotesRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealNotesRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDealNotesInfoModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStNoteCategory().setValue(CHILD_STNOTECATEGORY_RESET_VALUE);
		getStNoteText().setValue(CHILD_STNOTETEXT_RESET_VALUE);
		getStUserName().setValue(CHILD_STUSERNAME_RESET_VALUE);
		getStNoteDate().setValue(CHILD_STNOTEDATE_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STNOTECATEGORY,StaticTextField.class);
		registerChild(CHILD_STNOTETEXT,StaticTextField.class);
		registerChild(CHILD_STUSERNAME,StaticTextField.class);
		registerChild(CHILD_STNOTEDATE,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDealNotesInfoModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}

    // -------- Catherine: #2475 ---------------- begin -----------
    /***
     * The following method was borrowered from PageHandlerCommon
     * Should be placed into StringUtils
     */
    // Method to convert String to Html display format
    //  - Remove all control characters
    //  - Convert Return and LineFeed to "<br>"
    //  - Convert ' and " to `
    //  - Special handle with spaces replace "  " ==> " &nbsp;" and " " ==> " "
    /* #DG526 replaced with     StringUtil.convertToHtmString(tmpStr);
    public String convertToHtmString2(String input)
    {
        StringBuffer retStr = new StringBuffer("");

        int x = 0;

        boolean lastIsSpace = false;

        for(x = 0; x < input.length(); x++)
        {
            char c = input.charAt(x);

            // Convert Return Chars
            if ((c == '?') ||(c == 10))
            {
                retStr.append("<br>");
                lastIsSpace = false;
            }
            else if ((c == '\'') ||(c == '\"'))
            {
                retStr.append('`');
                lastIsSpace = false;
            }
            else if ((c == ' '))
            {
                if (lastIsSpace == false)
                {
                    retStr.append(c);
                    lastIsSpace = true;
                }
                else
                {
                    retStr.append("&nbsp;");
                    lastIsSpace = false;
                }
            }
            else if (! Character.isISOControl(c))
            {
                retStr.append(c);
                lastIsSpace = false;
            }
        }
        return retStr.toString();
	} */
    // -------- Catherine: #2475 ---------------- end -----------

	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
      //// SYNCADD: the whole stuff below.
      int rowNum = this.getTileIndex();
      boolean ret = true;

      DealNotesHandler handler =(DealNotesHandler) this.handler.cloneSS();
      handler.pageGetState(this.getParentViewBean());

      //logger = SysLog.getSysLogger("pgDNRTV");

      //logger.debug("pgDNRTV@nextTile::rowNum: " + rowNum);

      doDealNotesInfoModelImpl theObj =(doDealNotesInfoModelImpl) getdoDealNotesInfoModel();
      //logger.debug("pgDNRTV@nextTile::Size: " + theObj.getSize());

      if (theObj.getNumRows() > 0)
      {
        //logger.debug("pgDNRTV@nextTile::Location: " + theObj.getLocation());
        //String tmpStr = handler.populateDealNotesWin(rowNum, theObj);
    	  String tmpStr = StringUtil.convertToHtmString(theObj.getDfDealNotesText());
        /* #DG526 refactored
        // -------- Catherine: #2475 ---------------- begin -----------
        if (logger == null)
          logger = SysLog.getSysLogger("pgDNRTV");
        doDealNotesInfoModelImpl thisModel = (doDealNotesInfoModelImpl) (RequestManager.getRequestContext()
            .getModelManager().getModel(doDealNotesInfoModel.class));
        Object tmpObj = thisModel.getValue(doDealNotesInfoModel.FIELD_DFDEALNOTESTEXT);
        String tmpStr = null;
        if (tmpObj == null) {
          tmpStr = "";
        } else {
          tmpStr = tmpObj.toString();
      }
        logger.debug("pgDealNotesRepeated1TiledView@nextTile():DealNotesText:: " + tmpStr);
        tmpStr = convertToHtmString2(tmpStr);
        setDisplayFieldValue("stNoteText", new String(tmpStr));
        // -------- Catherine: #2475 ---------------- end -----------*/
        setDisplayFieldValue("stNoteText", tmpStr);
      } else
        ret = false;

      handler.pageSaveState();

      return ret;
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STNOTECATEGORY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealNotesInfoModel(),
				CHILD_STNOTECATEGORY,
				doDealNotesInfoModel.FIELD_DFDEALNOTESCATEGORY,
				CHILD_STNOTECATEGORY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STNOTETEXT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealNotesInfoModel(),
				CHILD_STNOTETEXT,
				doDealNotesInfoModel.FIELD_DFDEALNOTESTEXT,
				CHILD_STNOTETEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealNotesInfoModel(),
				CHILD_STUSERNAME,
				doDealNotesInfoModel.FIELD_DFUSERNAME,
				CHILD_STUSERNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STNOTEDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDealNotesInfoModel(),
				CHILD_STNOTEDATE,
				doDealNotesInfoModel.FIELD_DFDEALNOTESDATE,
				CHILD_STNOTEDATE_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNoteCategory()
	{
		return (StaticTextField)getChild(CHILD_STNOTECATEGORY);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNoteText()
	{
		return (StaticTextField)getChild(CHILD_STNOTETEXT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserName()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNoteDate()
	{
		return (StaticTextField)getChild(CHILD_STNOTEDATE);
	}


	/**
	 *
	 *
	 */
	public doDealNotesInfoModel getdoDealNotesInfoModel()
	{
		if (doDealNotesInfo == null)
			doDealNotesInfo = (doDealNotesInfoModel) getModel(doDealNotesInfoModel.class);
		return doDealNotesInfo;
	}


	/**
	 *
	 *
	 */
	public void setdoDealNotesInfoModel(doDealNotesInfoModel model)
	{
			doDealNotesInfo = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STNOTECATEGORY="stNoteCategory";
	public static final String CHILD_STNOTECATEGORY_RESET_VALUE="";
	public static final String CHILD_STNOTETEXT="stNoteText";
	public static final String CHILD_STNOTETEXT_RESET_VALUE="";
	public static final String CHILD_STUSERNAME="stUserName";
	public static final String CHILD_STUSERNAME_RESET_VALUE="";
	public static final String CHILD_STNOTEDATE="stNoteDate";
	public static final String CHILD_STNOTEDATE_RESET_VALUE="";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDealNotesInfoModel doDealNotesInfo=null;
	private DealNotesHandler handler=new DealNotesHandler();
  public SysLogger logger;

}

