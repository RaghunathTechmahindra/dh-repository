package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealFeeModelImpl extends QueryModelBase
	implements doDealFeeModel
{
	/**
	 *
	 *
	 */
	public doDealFeeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfFeeAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFFEEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfFeeAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFFEEAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfFeePaymentDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFFEEPAYMENTDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfFeePaymentDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFFEEPAYMENTDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfFeePaymentMethodId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFFEEPAYMENTMETHODID);
	}


	/**
	 *
	 *
	 */
	public void setDfFeePaymentMethodId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFFEEPAYMENTMETHODID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfFeeStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFFEESTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfFeeStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFFEESTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealFeeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALFEEID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealFeeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALFEEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfFeeTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFFEETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfFeeTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFFEETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	  public java.math.BigDecimal getDfInstitutionId() {
	      return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
	    }

	    public void setDfInstitutionId(java.math.BigDecimal value) {
	      setValue(FIELD_DFINSTITUTIONID, value);
	    }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEALFEE.DEALID, DEALFEE.FEEAMOUNT, DEALFEE.FEEPAYMENTDATE, DEALFEE.FEEPAYMENTMETHODID, DEALFEE.FEESTATUSID, DEALFEE.DEALFEEID, FEE.FEETYPEID, DEALFEE.COPYID, DEALFEE.INSTITUTIONPROFILEID FROM DEALFEE, FEE  __WHERE__  ORDER BY DEALFEE.DEALFEEID  ASC";

	public static final String MODIFYING_QUERY_TABLE_NAME="DEALFEE, FEE";
	
	public static final String STATIC_WHERE_CRITERIA=" (((FEE.FEEID  =  DEALFEE.FEEID) AND (FEE.INSTITUTIONPROFILEID  =  DEALFEE.INSTITUTIONPROFILEID)) AND FEE.FEEGENERATIONTYPEID = 1) ";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEALFEE.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFFEEAMOUNT="DEALFEE.FEEAMOUNT";
	public static final String COLUMN_DFFEEAMOUNT="FEEAMOUNT";
	public static final String QUALIFIED_COLUMN_DFFEEPAYMENTDATE="DEALFEE.FEEPAYMENTDATE";
	public static final String COLUMN_DFFEEPAYMENTDATE="FEEPAYMENTDATE";
	public static final String QUALIFIED_COLUMN_DFFEEPAYMENTMETHODID="DEALFEE.FEEPAYMENTMETHODID";
	public static final String COLUMN_DFFEEPAYMENTMETHODID="FEEPAYMENTMETHODID";
	public static final String QUALIFIED_COLUMN_DFFEESTATUSID="DEALFEE.FEESTATUSID";
	public static final String COLUMN_DFFEESTATUSID="FEESTATUSID";
	public static final String QUALIFIED_COLUMN_DFDEALFEEID="DEALFEE.DEALFEEID";
	public static final String COLUMN_DFDEALFEEID="DEALFEEID";
	public static final String QUALIFIED_COLUMN_DFFEETYPEID="FEE.FEETYPEID";
	public static final String COLUMN_DFFEETYPEID="FEETYPEID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEALFEE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";

	  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
	      "DEALFEE.INSTITUTIONPROFILEID";
	public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFEEAMOUNT,
				COLUMN_DFFEEAMOUNT,
				QUALIFIED_COLUMN_DFFEEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFEEPAYMENTDATE,
				COLUMN_DFFEEPAYMENTDATE,
				QUALIFIED_COLUMN_DFFEEPAYMENTDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFEEPAYMENTMETHODID,
				COLUMN_DFFEEPAYMENTMETHODID,
				QUALIFIED_COLUMN_DFFEEPAYMENTMETHODID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFEESTATUSID,
				COLUMN_DFFEESTATUSID,
				QUALIFIED_COLUMN_DFFEESTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALFEEID,
				COLUMN_DFDEALFEEID,
				QUALIFIED_COLUMN_DFDEALFEEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFEETYPEID,
				COLUMN_DFFEETYPEID,
				QUALIFIED_COLUMN_DFFEETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	}

}

