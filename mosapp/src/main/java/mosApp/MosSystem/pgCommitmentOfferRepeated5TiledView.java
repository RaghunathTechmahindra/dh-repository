package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;

/**
 *
 *
 *
 */
public class pgCommitmentOfferRepeated5TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgCommitmentOfferRepeated5TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(200);
		setPrimaryModelClass( doSignConditionModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getCbSignDocAction().setValue(CHILD_CBSIGNDOCACTION_RESET_VALUE);
		getStSignDocConditionLabel().setValue(CHILD_STSIGNDOCCONDITIONLABEL_RESET_VALUE);
		getHdSignDocDocTrackId().setValue(CHILD_HDSIGNDOCDOCTRACKID_RESET_VALUE);
		getBtSignDocDetail().setValue(CHILD_BTSIGNDOCDETAIL_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_CBSIGNDOCACTION,ComboBox.class);
		registerChild(CHILD_STSIGNDOCCONDITIONLABEL,StaticTextField.class);
		registerChild(CHILD_HDSIGNDOCDOCTRACKID,HiddenField.class);
		registerChild(CHILD_BTSIGNDOCDETAIL,Button.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoSignConditionModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //--> By Billy 26Nov2002
    cbSignDocActionOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			boolean retval = handler.checkDisplayThisRow("Repeated5");
			handler.pageSaveState();
			return retval;
		}
		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_CBSIGNDOCACTION))
		{
			ComboBox child = new ComboBox( this,
				getdoSignConditionModel(),
				CHILD_CBSIGNDOCACTION,
				doSignConditionModel.FIELD_DFACTION,
				CHILD_CBSIGNDOCACTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbSignDocActionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STSIGNDOCCONDITIONLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoSignConditionModel(),
				CHILD_STSIGNDOCCONDITIONLABEL,
				doSignConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STSIGNDOCCONDITIONLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNDOCDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoSignConditionModel(),
				CHILD_HDSIGNDOCDOCTRACKID,
				doSignConditionModel.FIELD_DFDEFDOCTRACKID,
				CHILD_HDSIGNDOCDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSIGNDOCDETAIL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSIGNDOCDETAIL,
				CHILD_BTSIGNDOCDETAIL,
				CHILD_BTSIGNDOCDETAIL_RESET_VALUE,
				null);
				return child;

		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbSignDocAction()
	{
		return (ComboBox)getChild(CHILD_CBSIGNDOCACTION);
	}

	/**
	 *
	 *
	 */
	static class CbSignDocActionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbSignDocActionOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 26Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection<String[]> c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "DOCUMENTSTATUS", languageId);
        Iterator<String[]> l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public StaticTextField getStSignDocConditionLabel()
	{
		return (StaticTextField)getChild(CHILD_STSIGNDOCCONDITIONLABEL);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignDocDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDSIGNDOCDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void handleBtSignDocDetailRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		ConditionsReviewHandler handler =(ConditionsReviewHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.navigateToDetail(5, handler.getRowNdxFromWebEventMethod(event));
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSignDocDetail()
	{
		return (Button)getChild(CHILD_BTSIGNDOCDETAIL);
	}


	/**
	 *
	 *
	 */
	public doSignConditionModel getdoSignConditionModel()
	{
		if (doSignCondition == null)
			doSignCondition = (doSignConditionModel) getModel(doSignConditionModel.class);
		return doSignCondition;
	}


	/**
	 *
	 *
	 */
	public void setdoSignConditionModel(doSignConditionModel model)
	{
			doSignCondition = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_CBSIGNDOCACTION="cbSignDocAction";
	public static final String CHILD_CBSIGNDOCACTION_RESET_VALUE="";
  //--Release2.1--//
	//private static CbSignDocActionOptionList cbSignDocActionOptions=new CbSignDocActionOptionList();
  private CbSignDocActionOptionList cbSignDocActionOptions=new CbSignDocActionOptionList();
	public static final String CHILD_STSIGNDOCCONDITIONLABEL="stSignDocConditionLabel";
	public static final String CHILD_STSIGNDOCCONDITIONLABEL_RESET_VALUE="";
	public static final String CHILD_HDSIGNDOCDOCTRACKID="hdSignDocDocTrackId";
	public static final String CHILD_HDSIGNDOCDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_BTSIGNDOCDETAIL="btSignDocDetail";
	public static final String CHILD_BTSIGNDOCDETAIL_RESET_VALUE=" ";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doSignConditionModel doSignCondition=null;
  private ConditionsReviewHandler handler=new ConditionsReviewHandler();
}

