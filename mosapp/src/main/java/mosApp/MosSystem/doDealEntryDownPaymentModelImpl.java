package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDealEntryDownPaymentModelImpl extends QueryModelBase
	implements doDealEntryDownPaymentModel
{
	/**
	 *
	 *
	 */
	public doDealEntryDownPaymentModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentSourceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOWNPAYMENTSOURCEID);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentSourceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOWNPAYMENTSOURCEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentSourceTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOWNPAYMENTSOURCETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentSourceTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOWNPAYMENTSOURCETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOWNPAYMENTAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOWNPAYMENTAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDPSDescription()
	{
		return (String)getValue(FIELD_DFDPSDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfDPSDescription(String value)
	{
		setValue(FIELD_DFDPSDESCRIPTION,value);
	}


	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
   /**
        public void setResultSet(ResultSet value)
        {
            logger = SysLog.getSysLogger("METADATA");

            try
            {
                super.setResultSet(value);
                if( value != null)
                {
                    ResultSetMetaData metaData = value.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    logger.debug("===============================================");
                    logger.debug("Testing MetaData Object for new synthetic fields for" +
	                 	            " doDealEntryDownPaymentModel");
                    logger.debug("NumberColumns: " + columnCount);
                    for(int i = 0; i < columnCount ; i++)
                    {
                        logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                    }
                    logger.debug("================================================");
                  }
              }
              catch (SQLException e)
              {
                  logger.debug("Class: " + getClass().getName());
                  logger.debug("SQLException: " + e);
              }
        }
    **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT distinct DOWNPAYMENTSOURCE.DEALID, DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCEID, DOWNPAYMENTSOURCE.COPYID, DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCETYPEID, DOWNPAYMENTSOURCE.AMOUNT, DOWNPAYMENTSOURCE.DPSDESCRIPTION FROM DOWNPAYMENTSOURCE  __WHERE__  ORDER BY DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCEID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="DOWNPAYMENTSOURCE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DOWNPAYMENTSOURCE.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCEID="DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCEID";
	public static final String COLUMN_DFDOWNPAYMENTSOURCEID="DOWNPAYMENTSOURCEID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOWNPAYMENTSOURCE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCETYPEID="DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCETYPEID";
	public static final String COLUMN_DFDOWNPAYMENTSOURCETYPEID="DOWNPAYMENTSOURCETYPEID";
	public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTAMOUNT="DOWNPAYMENTSOURCE.AMOUNT";
	public static final String COLUMN_DFDOWNPAYMENTAMOUNT="AMOUNT";
	public static final String QUALIFIED_COLUMN_DFDPSDESCRIPTION="DOWNPAYMENTSOURCE.DPSDESCRIPTION";
	public static final String COLUMN_DFDPSDESCRIPTION="DPSDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOWNPAYMENTSOURCEID,
				COLUMN_DFDOWNPAYMENTSOURCEID,
				QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOWNPAYMENTSOURCETYPEID,
				COLUMN_DFDOWNPAYMENTSOURCETYPEID,
				QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOWNPAYMENTAMOUNT,
				COLUMN_DFDOWNPAYMENTAMOUNT,
				QUALIFIED_COLUMN_DFDOWNPAYMENTAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDPSDESCRIPTION,
				COLUMN_DFDPSDESCRIPTION,
				QUALIFIED_COLUMN_DFDPSDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

