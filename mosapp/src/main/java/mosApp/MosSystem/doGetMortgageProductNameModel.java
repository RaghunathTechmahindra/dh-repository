package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doGetMortgageProductNameModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfMortgageProductId();

	
	/**
	 * 
	 * 
	 */
	public void setDfMortgageProductId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMortgageProductName();

	
	/**
	 * 
	 * 
	 */
	public void setDfMortgageProductName(String value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);  
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFMORTGAGEPRODUCTID="dfMortgageProductId";
	public static final String FIELD_DFMORTGAGEPRODUCTNAME="dfMortgageProductName";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

