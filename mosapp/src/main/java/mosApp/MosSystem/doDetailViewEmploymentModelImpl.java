package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doDetailViewEmploymentModelImpl extends QueryModelBase
	implements doDetailViewEmploymentModel
{
	/**
	 *
	 *
	 */
	public doDetailViewEmploymentModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 18Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert EMPLOYMENTHISTORYSTATUS Desc.
      this.setDfEmploymentStatus(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "EMPLOYMENTHISTORYSTATUS", this.getDfEmploymentStatus(), languageId));
      //Convert EMPLOYMENTHISTORYTYPE Desc.
      this.setDfEmploymentType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "EMPLOYMENTHISTORYTYPE", this.getDfEmploymentType(), languageId));
      //Convert PROVINCE Desc.
      this.setDfEmployerProvince(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROVINCE", this.getDfEmployerProvince(), languageId));
      //Convert INDUSTRYSECTOR Desc.
      this.setDfIndustrySector(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "INDUSTRYSECTOR", this.getDfIndustrySector(), languageId));
      //Convert OCCUPATION Desc.
      this.setDfOccupation(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "OCCUPATION", this.getDfOccupation(), languageId));
      //Convert JOBTITLE Desc.
      this.setDfJobTitle1(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "JOBTITLE", this.getDfJobTitle1(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerName()
	{
		return (String)getValue(FIELD_DFEMPLOYERNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerName(String value)
	{
		setValue(FIELD_DFEMPLOYERNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmploymentStatus()
	{
		return (String)getValue(FIELD_DFEMPLOYMENTSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentStatus(String value)
	{
		setValue(FIELD_DFEMPLOYMENTSTATUS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmploymentType()
	{
		return (String)getValue(FIELD_DFEMPLOYMENTTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfEmploymentType(String value)
	{
		setValue(FIELD_DFEMPLOYMENTTYPE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerPhoneNo()
	{
		return (String)getValue(FIELD_DFEMPLOYERPHONENO);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerPhoneNo(String value)
	{
		setValue(FIELD_DFEMPLOYERPHONENO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerFaxNo()
	{
		return (String)getValue(FIELD_DFEMPLOYERFAXNO);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerFaxNo(String value)
	{
		setValue(FIELD_DFEMPLOYERFAXNO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerEmailAddress()
	{
		return (String)getValue(FIELD_DFEMPLOYEREMAILADDRESS);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerEmailAddress(String value)
	{
		setValue(FIELD_DFEMPLOYEREMAILADDRESS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerAddressLine1()
	{
		return (String)getValue(FIELD_DFEMPLOYERADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerAddressLine1(String value)
	{
		setValue(FIELD_DFEMPLOYERADDRESSLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerAddressLine2()
	{
		return (String)getValue(FIELD_DFEMPLOYERADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerAddressLine2(String value)
	{
		setValue(FIELD_DFEMPLOYERADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerCity()
	{
		return (String)getValue(FIELD_DFEMPLOYERCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerCity(String value)
	{
		setValue(FIELD_DFEMPLOYERCITY,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerProvince()
	{
		return (String)getValue(FIELD_DFEMPLOYERPROVINCE);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerProvince(String value)
	{
		setValue(FIELD_DFEMPLOYERPROVINCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerPostalFSA()
	{
		return (String)getValue(FIELD_DFEMPLOYERPOSTALFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerPostalFSA(String value)
	{
		setValue(FIELD_DFEMPLOYERPOSTALFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmployerPostalLDU()
	{
		return (String)getValue(FIELD_DFEMPLOYERPOSTALLDU);
	}


	/**
	 *
	 *
	 */
	public void setDfEmployerPostalLDU(String value)
	{
		setValue(FIELD_DFEMPLOYERPOSTALLDU,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIndustrySector()
	{
		return (String)getValue(FIELD_DFINDUSTRYSECTOR);
	}


	/**
	 *
	 *
	 */
	public void setDfIndustrySector(String value)
	{
		setValue(FIELD_DFINDUSTRYSECTOR,value);
	}


	/**
	 *
	 *
	 */
	public String getDfOccupation()
	{
		return (String)getValue(FIELD_DFOCCUPATION);
	}


	/**
	 *
	 *
	 */
	public void setDfOccupation(String value)
	{
		setValue(FIELD_DFOCCUPATION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfJobTittle()
	{
		return (String)getValue(FIELD_DFJOBTITTLE);
	}


	/**
	 *
	 *
	 */
	public void setDfJobTittle(String value)
	{
		setValue(FIELD_DFJOBTITTLE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTimeAtJob()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTIMEATJOB);
	}


	/**
	 *
	 *
	 */
	public void setDfTimeAtJob(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTIMEATJOB,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEmpHistIncomeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFEMPHISTINCOMEID);
	}


	/**
	 *
	 *
	 */
	public void setDfEmpHistIncomeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFEMPHISTINCOMEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfJobTitleId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFJOBTITLEID);
	}


	/**
	 *
	 *
	 */
	public void setDfJobTitleId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFJOBTITLEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEmpWorkPhoneExt()
	{
		return (String)getValue(FIELD_DFEMPWORKPHONEEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfEmpWorkPhoneExt(String value)
	{
		setValue(FIELD_DFEMPWORKPHONEEXT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfJobTitle1()
	{
		return (String)getValue(FIELD_DFJOBTITLE1);
	}


	/**
	 *
	 *
	 */
	public void setDfJobTitle1(String value)
	{
		setValue(FIELD_DFJOBTITLE1,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 18Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT EMPLOYMENTHISTORY.BORROWERID, EMPLOYMENTHISTORY.EMPLOYERNAME, EMPLOYMENTHISTORYSTATUS.EHSDESCRIPTION, EMPLOYMENTHISTORYTYPE.EHTDESCRIPTION, CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTEMAILADDRESS, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, ADDR.CITY, PROVINCE.PROVINCENAME, ADDR.POSTALFSA, ADDR.POSTALLDU, INDUSTRYSECTOR.ISDESCRIPTION, OCCUPATION.ODESCRIPTION, EMPLOYMENTHISTORY.JOBTITLE, EMPLOYMENTHISTORY.MONTHSOFSERVICE, EMPLOYMENTHISTORY.INCOMEID, EMPLOYMENTHISTORY.JOBTITLEID, CONTACT.CONTACTPHONENUMBEREXTENSION, EMPLOYMENTHISTORY.COPYID, JOBTITLE.JOBTITLEDESCRIPTION FROM CONTACT, ADDR, PROVINCE, EMPLOYMENTHISTORY, EMPLOYMENTHISTORYSTATUS, EMPLOYMENTHISTORYTYPE, INDUSTRYSECTOR, OCCUPATION, JOBTITLE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT EMPLOYMENTHISTORY.BORROWERID, EMPLOYMENTHISTORY.EMPLOYERNAME, "+
    "to_char(EMPLOYMENTHISTORY.EMPLOYMENTHISTORYSTATUSID) EMPLOYMENTHISTORYSTATUSID_STR, "+
    "to_char(EMPLOYMENTHISTORY.EMPLOYMENTHISTORYTYPEID) EMPLOYMENTHISTORYTYPEID_STR, "+
    "CONTACT.CONTACTPHONENUMBER, CONTACT.CONTACTFAXNUMBER, CONTACT.CONTACTEMAILADDRESS, "+
    "ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, ADDR.CITY, to_char(ADDR.PROVINCEID) PROVINCEID_STR, "+
    "ADDR.POSTALFSA, ADDR.POSTALLDU, to_char(EMPLOYMENTHISTORY.INDUSTRYSECTORID) INDUSTRYSECTORID_STR, "+
    "to_char(EMPLOYMENTHISTORY.OCCUPATIONID) OCCUPATIONID_STR, EMPLOYMENTHISTORY.JOBTITLE, "+
    "EMPLOYMENTHISTORY.MONTHSOFSERVICE, EMPLOYMENTHISTORY.INCOMEID, "+
    "EMPLOYMENTHISTORY.JOBTITLEID, CONTACT.CONTACTPHONENUMBEREXTENSION, "+
    "EMPLOYMENTHISTORY.COPYID, to_char(EMPLOYMENTHISTORY.JOBTITLEID) JOBTITLEID_STR, "+
    "CONTACT.INSTITUTIONPROFILEID "+
    " FROM CONTACT, ADDR, EMPLOYMENTHISTORY "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, ADDR, PROVINCE, EMPLOYMENTHISTORY, EMPLOYMENTHISTORYSTATUS, EMPLOYMENTHISTORYTYPE, INDUSTRYSECTOR, OCCUPATION, JOBTITLE";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "CONTACT, ADDR, EMPLOYMENTHISTORY";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" CONTACT.ADDRID  =  ADDR.ADDRID (+)  AND CONTACT.CONTACTID (+) =  EMPLOYMENTHISTORY.CONTACTID AND ADDR.PROVINCEID  =  PROVINCE.PROVINCEID (+) AND EMPLOYMENTHISTORY.EMPLOYMENTHISTORYSTATUSID  =  EMPLOYMENTHISTORYSTATUS.EMPLOYMENTHISTORYSTATUSID (+) AND EMPLOYMENTHISTORY.EMPLOYMENTHISTORYTYPEID =  EMPLOYMENTHISTORYTYPE.EMPLOYMENTHISTORYTYPEID (+) AND EMPLOYMENTHISTORY.INDUSTRYSECTORID  =  INDUSTRYSECTOR.INDUSTRYSECTORID (+) AND EMPLOYMENTHISTORY.OCCUPATIONID  =  OCCUPATION.OCCUPATIONID (+) AND  EMPLOYMENTHISTORY.COPYID =  CONTACT.COPYID AND  CONTACT.COPYID = ADDR.COPYID AND EMPLOYMENTHISTORY.JOBTITLEID = JOBTITLE.JOBTITLEID ";
	public static final String STATIC_WHERE_CRITERIA=
    " CONTACT.ADDRID  =  ADDR.ADDRID (+)  AND "+
    "CONTACT.CONTACTID (+) =  EMPLOYMENTHISTORY.CONTACTID AND "+
    "EMPLOYMENTHISTORY.COPYID =  CONTACT.COPYID AND "+
    "CONTACT.COPYID = ADDR.COPYID  AND "+
	   "EMPLOYMENTHISTORY.INSTITUTIONPROFILEID =  CONTACT.INSTITUTIONPROFILEID AND "+
	    "CONTACT.INSTITUTIONPROFILEID = ADDR.INSTITUTIONPROFILEID  ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="EMPLOYMENTHISTORY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERNAME="EMPLOYMENTHISTORY.EMPLOYERNAME";
	public static final String COLUMN_DFEMPLOYERNAME="EMPLOYERNAME";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERPHONENO="CONTACT.CONTACTPHONENUMBER";
	public static final String COLUMN_DFEMPLOYERPHONENO="CONTACTPHONENUMBER";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERFAXNO="CONTACT.CONTACTFAXNUMBER";
	public static final String COLUMN_DFEMPLOYERFAXNO="CONTACTFAXNUMBER";
	public static final String QUALIFIED_COLUMN_DFEMPLOYEREMAILADDRESS="CONTACT.CONTACTEMAILADDRESS";
	public static final String COLUMN_DFEMPLOYEREMAILADDRESS="CONTACTEMAILADDRESS";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERADDRESSLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFEMPLOYERADDRESSLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERADDRESSLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFEMPLOYERADDRESSLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERCITY="ADDR.CITY";
	public static final String COLUMN_DFEMPLOYERCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERPOSTALFSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFEMPLOYERPOSTALFSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFEMPLOYERPOSTALLDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFEMPLOYERPOSTALLDU="POSTALLDU";
	public static final String QUALIFIED_COLUMN_DFJOBTITTLE="EMPLOYMENTHISTORY.JOBTITLE";
	public static final String COLUMN_DFJOBTITTLE="JOBTITLE";
	public static final String QUALIFIED_COLUMN_DFTIMEATJOB="EMPLOYMENTHISTORY.MONTHSOFSERVICE";
	public static final String COLUMN_DFTIMEATJOB="MONTHSOFSERVICE";
	public static final String QUALIFIED_COLUMN_DFEMPHISTINCOMEID="EMPLOYMENTHISTORY.INCOMEID";
	public static final String COLUMN_DFEMPHISTINCOMEID="INCOMEID";
	public static final String QUALIFIED_COLUMN_DFJOBTITLEID="EMPLOYMENTHISTORY.JOBTITLEID";
	public static final String COLUMN_DFJOBTITLEID="JOBTITLEID";
	public static final String QUALIFIED_COLUMN_DFEMPWORKPHONEEXT="CONTACT.CONTACTPHONENUMBEREXTENSION";
	public static final String COLUMN_DFEMPWORKPHONEEXT="CONTACTPHONENUMBEREXTENSION";
	public static final String QUALIFIED_COLUMN_DFCOPYID="EMPLOYMENTHISTORY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";


	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFEMPLOYMENTSTATUS="EMPLOYMENTHISTORYSTATUS.EHSDESCRIPTION";
	//public static final String COLUMN_DFEMPLOYMENTSTATUS="EHSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFEMPLOYMENTSTATUS="EMPLOYMENTHISTORY.EMPLOYMENTHISTORYSTATUSID_STR";
	public static final String COLUMN_DFEMPLOYMENTSTATUS="EMPLOYMENTHISTORYSTATUSID_STR";
	//public static final String QUALIFIED_COLUMN_DFEMPLOYMENTTYPE="EMPLOYMENTHISTORYTYPE.EHTDESCRIPTION";
	//public static final String COLUMN_DFEMPLOYMENTTYPE="EHTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFEMPLOYMENTTYPE="EMPLOYMENTHISTORY.EMPLOYMENTHISTORYTYPEID_STR";
	public static final String COLUMN_DFEMPLOYMENTTYPE="EMPLOYMENTHISTORYTYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFEMPLOYERPROVINCE="PROVINCE.PROVINCENAME";
	//public static final String COLUMN_DFEMPLOYERPROVINCE="PROVINCENAME";
  public static final String QUALIFIED_COLUMN_DFEMPLOYERPROVINCE="ADDR.PROVINCEID_STR";
	public static final String COLUMN_DFEMPLOYERPROVINCE="PROVINCEID_STR";
  //public static final String QUALIFIED_COLUMN_DFINDUSTRYSECTOR="INDUSTRYSECTOR.ISDESCRIPTION";
	//public static final String COLUMN_DFINDUSTRYSECTOR="ISDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFINDUSTRYSECTOR="EMPLOYMENTHISTORY.INDUSTRYSECTORID_STR";
	public static final String COLUMN_DFINDUSTRYSECTOR="INDUSTRYSECTORID_STR";
	//public static final String QUALIFIED_COLUMN_DFOCCUPATION="OCCUPATION.ODESCRIPTION";
	//public static final String COLUMN_DFOCCUPATION="ODESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFOCCUPATION="EMPLOYMENTHISTORY.OCCUPATIONID_STR";
	public static final String COLUMN_DFOCCUPATION="OCCUPATIONID_STR";
  //public static final String QUALIFIED_COLUMN_DFJOBTITLE1="JOBTITLE.JOBTITLEDESCRIPTION";
	//public static final String COLUMN_DFJOBTITLE1="JOBTITLEDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFJOBTITLE1="EMPLOYMENTHISTORY.JOBTITLEID_STR";
	public static final String COLUMN_DFJOBTITLE1="JOBTITLEID_STR";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "CONTACT.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	//[[SPIDER_EVENTS BEGIN

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERNAME,
				COLUMN_DFEMPLOYERNAME,
				QUALIFIED_COLUMN_DFEMPLOYERNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTSTATUS,
				COLUMN_DFEMPLOYMENTSTATUS,
				QUALIFIED_COLUMN_DFEMPLOYMENTSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYMENTTYPE,
				COLUMN_DFEMPLOYMENTTYPE,
				QUALIFIED_COLUMN_DFEMPLOYMENTTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERPHONENO,
				COLUMN_DFEMPLOYERPHONENO,
				QUALIFIED_COLUMN_DFEMPLOYERPHONENO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERFAXNO,
				COLUMN_DFEMPLOYERFAXNO,
				QUALIFIED_COLUMN_DFEMPLOYERFAXNO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYEREMAILADDRESS,
				COLUMN_DFEMPLOYEREMAILADDRESS,
				QUALIFIED_COLUMN_DFEMPLOYEREMAILADDRESS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERADDRESSLINE1,
				COLUMN_DFEMPLOYERADDRESSLINE1,
				QUALIFIED_COLUMN_DFEMPLOYERADDRESSLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERADDRESSLINE2,
				COLUMN_DFEMPLOYERADDRESSLINE2,
				QUALIFIED_COLUMN_DFEMPLOYERADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERCITY,
				COLUMN_DFEMPLOYERCITY,
				QUALIFIED_COLUMN_DFEMPLOYERCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERPROVINCE,
				COLUMN_DFEMPLOYERPROVINCE,
				QUALIFIED_COLUMN_DFEMPLOYERPROVINCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERPOSTALFSA,
				COLUMN_DFEMPLOYERPOSTALFSA,
				QUALIFIED_COLUMN_DFEMPLOYERPOSTALFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPLOYERPOSTALLDU,
				COLUMN_DFEMPLOYERPOSTALLDU,
				QUALIFIED_COLUMN_DFEMPLOYERPOSTALLDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINDUSTRYSECTOR,
				COLUMN_DFINDUSTRYSECTOR,
				QUALIFIED_COLUMN_DFINDUSTRYSECTOR,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFOCCUPATION,
				COLUMN_DFOCCUPATION,
				QUALIFIED_COLUMN_DFOCCUPATION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOBTITTLE,
				COLUMN_DFJOBTITTLE,
				QUALIFIED_COLUMN_DFJOBTITTLE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTIMEATJOB,
				COLUMN_DFTIMEATJOB,
				QUALIFIED_COLUMN_DFTIMEATJOB,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPHISTINCOMEID,
				COLUMN_DFEMPHISTINCOMEID,
				QUALIFIED_COLUMN_DFEMPHISTINCOMEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOBTITLEID,
				COLUMN_DFJOBTITLEID,
				QUALIFIED_COLUMN_DFJOBTITLEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFEMPWORKPHONEEXT,
				COLUMN_DFEMPWORKPHONEEXT,
				QUALIFIED_COLUMN_DFEMPWORKPHONEEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFJOBTITLE1,
				COLUMN_DFJOBTITLE1,
				QUALIFIED_COLUMN_DFJOBTITLE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	      FIELD_SCHEMA.addFieldDescriptor(
	                new QueryFieldDescriptor(
	                  FIELD_DFINSTITUTIONID,
	                  COLUMN_DFINSTITUTIONID,
	                  QUALIFIED_COLUMN_DFINSTITUTIONID,
	                  java.math.BigDecimal.class,
	                  false,
	                  false,
	                  QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	                  "",
	                  QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	                  ""));
	}

}

