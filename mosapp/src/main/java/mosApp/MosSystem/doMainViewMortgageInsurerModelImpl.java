package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doMainViewMortgageInsurerModelImpl extends QueryModelBase
	implements doMainViewMortgageInsurerModel
{
	/**
	 *
	 *
	 */
	public doMainViewMortgageInsurerModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert MORTGAGEINSURER Desc.
      this.setDfMIInsurer(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "MORTGAGEINSURANCECARRIER", this.getDfMIInsurer(), languageId));
      // SEAN DJ SPEC-Progress Advance Type July 25, 2005: Convert Progress
      // Advance Type desc.
      this.setDfProgressAdvanceType(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "PROGRESSADVANCETYPE", this.getDfProgressAdvanceType(), languageId));
      // SEAN DJ SPEC-PAT END.
      //Convert MISTATUS Desc.
      this.setDfMIRequestStatus(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "MISTATUS", this.getDfMIRequestStatus(), languageId));
      //Convert MITYPE Desc.
      this.setDfMITypeDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "MORTGAGEINSURANCETYPE", this.getDfMITypeDesc(), languageId));
      //Convert MIINDICATOR Desc.
      this.setDfMIIndicatorDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "MIINDICATOR", this.getDfMIIndicatorDesc(), languageId));
      //Convert MIPAYOR Desc.
      this.setDfMIPayorDesc(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "MORTGAGEINSURANCEPAYOR", this.getDfMIPayorDesc(), languageId));
      
      //--Release3.1--begins
      //--by Hiro Apr 11, 2006
      
      //Convert LOCREPAYMENTTYPE Desc.
      this.setDfLOCRepayment(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "LOCREPAYMENTTYPE", this.getDfLOCRepayment(), languageId));
      
      //--Release3.1--ends

    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIInsurer()
	{
		return (String)getValue(FIELD_DFMIINSURER);
	}


	/**
	 *
	 *
	 */
	public void setDfMIInsurer(String value)
	{
		setValue(FIELD_DFMIINSURER,value);
	}

	// SEAN DJ SPEC-Progress Advance Type July 25, 2005: getter and setter implementation.
	public String getDfProgressAdvanceType() {

		return (String) getValue(FIELD_DFPROGRESSADVANCETYPE);
	}

	public void setDfProgressAdvanceType(String value) {

		setValue(FIELD_DFPROGRESSADVANCETYPE, value);
	}
	// SEAN DJ SPEC-PAT END

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPremiumAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMIPREMIUMAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfMIPremiumAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMIPREMIUMAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMICertificateNo()
	{
		return (String)getValue(FIELD_DFMICERTIFICATENO);
	}


	/**
	 *
	 *
	 */
	public void setDfMICertificateNo(String value)
	{
		setValue(FIELD_DFMICERTIFICATENO,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIRequestStatus()
	{
		return (String)getValue(FIELD_DFMIREQUESTSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfMIRequestStatus(String value)
	{
		setValue(FIELD_DFMIREQUESTSTATUS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMITypeDesc()
	{
		String str = (String)getValue(FIELD_DFMITYPEDESC);
		if(str == null) str = "0";	//ticket #4097
		return str;
	}


	/**
	 *
	 *
	 */
	public void setDfMITypeDesc(String value)
	{
		setValue(FIELD_DFMITYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIUpfront()
	{
		return (String)getValue(FIELD_DFMIUPFRONT);
	}


	/**
	 *
	 *
	 */
	public void setDfMIUpfront(String value)
	{
		setValue(FIELD_DFMIUPFRONT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIExistingPolicyNumber()
	{
		return (String)getValue(FIELD_DFMIEXISTINGPOLICYNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfMIExistingPolicyNumber(String value)
	{
		setValue(FIELD_DFMIEXISTINGPOLICYNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIRUIntervention()
	{
		return (String)getValue(FIELD_DFMIRUINTERVENTION);
	}


	/**
	 *
	 *
	 */
	public void setDfMIRUIntervention(String value)
	{
		setValue(FIELD_DFMIRUINTERVENTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPreQualificationMICertNum()
	{
		return (String)getValue(FIELD_DFPREQUALIFICATIONMICERTNUM);
	}


	/**
	 *
	 *
	 */
	public void setDfPreQualificationMICertNum(String value)
	{
		setValue(FIELD_DFPREQUALIFICATIONMICERTNUM,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIIndicatorDesc()
	{
		return (String)getValue(FIELD_DFMIINDICATORDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorDesc(String value)
	{
		setValue(FIELD_DFMIINDICATORDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIPayorDesc()
	{
		String str = (String)getValue(FIELD_DFMIPAYORDESC);
		if(str == null) str = "0";	//ticket #4097
		return str;
	}


	/**
	 *
	 *
	 */
	public void setDfMIPayorDesc(String value)
	{
		setValue(FIELD_DFMIPAYORDESC,value);
	}

  
  //--Release3.1--begins
  //--by Hiro Apr 11, 2006

  public void setDfProgressAdvanceInspectionBy(String value)
  {
    setValue(FIELD_DFPROGRESSADVANCEINSPECTIONBY, value);
  }

  public String getDfProgressAdvanceInspectionBy()
  {
    return (String) getValue(FIELD_DFPROGRESSADVANCEINSPECTIONBY);
  }

  public void setDfSelfDirectedRRSP(String value)
  {
    setValue(FIELD_DFSELFDIRECTEDRRSP, value);
  }

  public String getDfSelfDirectedRRSP()
  {
    return (String) getValue(FIELD_DFSELFDIRECTEDRRSP);
  }

  public void setDfCMHCProductTrackerIdentifier(String value)
  {
    setValue(FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER, value);
  }

  public String getDfCMHCProductTrackerIdentifier()
  {
    return (String) getValue(FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER);
  }

  public void setDfLOCRepayment(String value)
  {
    setValue(FIELD_DFLOCREPAYMENT, value);
  }

  public String getDfLOCRepayment()
  {
    return (String) getValue(FIELD_DFLOCREPAYMENT);
  }

  public void setDfRequestStandardService(String value)
  {
    setValue(FIELD_DFREQUESTSTANDARDSERVICE, value);
  }

  public String getDfRequestStandardService()
  {
    return (String) getValue(FIELD_DFREQUESTSTANDARDSERVICE);
  }

  public void setDfLOCAmortization(BigDecimal value)
  {
    setValue(FIELD_DFLOCAMORTIZATION, value);
  }

  public BigDecimal getDfLOCAmortization()
  {
    return (BigDecimal) getValue(FIELD_DFLOCAMORTIZATION);
  }

  public void setDfLOCInterestOnlyMaturityDate(Timestamp value)
  {
    setValue(FIELD_DFLOCINTERESTONLYMATURITYDATE, value);
  }

  public Timestamp getDfLOCInterestOnlyMaturityDate()
  {
    return (Timestamp) getValue(FIELD_DFLOCINTERESTONLYMATURITYDATE);
  }
  
  
  public java.math.BigDecimal getDfInstitutionId() {
	    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
	  }

	  /**
	   *
	   *
	   */
	  public void setDfInstitutionId(java.math.BigDecimal value) {
	    setValue(FIELD_DFINSTITUTIONID, value);
	  }
	  
  //--Release3.1--ends

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT DEAL.DEALID, MORTGAGEINSURER.MORTGAGEINSURERNAME, DEAL.MIPREMIUMAMOUNT, DEAL.MIPOLICYNUMBER, MISTATUS.MISTATUSDESCRIPTION, MORTGAGEINSURANCETYPE.MTDESCRIPTION, DEAL.COPYID, DEAL.MIUPFRONT, DEAL.MIEXISTINGPOLICYNUMBER, DEAL.MIRUINTERVENTION, DEAL.PREQUALIFICATIONMICERTNUM, MIINDICATOR.MIIDESCRIPTION, MORTGAGEINSURANCEPAYOR.MIPDESCRIPTION FROM DEAL, MORTGAGEINSURER, MISTATUS, MORTGAGEINSURANCETYPE, MIINDICATOR, MORTGAGEINSURANCEPAYOR  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT DEAL.DEALID, to_char(DEAL.MORTGAGEINSURERID) MORTGAGEINSURERID_STR, "+
  	// SEAN DJ SPEC-Progress Advance Type July 25, 2005: update the template.
  	"to_char(DEAL.PROGRESSADVANCETYPEID) PROGRESSADVANCETYPEID_STR, " +
  	// SEAN DJ SPEC-PAT END.
  
    //--Release3.1--begins
    //--by Hiro Apr 11, 2006
    "to_char(DEAL.PROGRESSADVANCEINSPECTIONBY) PROGRESSADVANCEINSPECTIONBY_S , " +
    "DEAL.SELFDIRECTEDRRSP," +
    "DEAL.CMHCPRODUCTTRACKERIDENTIFIER, " + 
    "to_char(DEAL.LOCREPAYMENTTYPEID) LOCREPAYMENTTYPEID_STR, " +
    "DEAL.REQUESTSTANDARDSERVICE, " +
    "DEAL.LOCAMORTIZATIONMONTHS, " + 
    "DEAL.LOCINTERESTONLYMATURITYDATE, " +
    //--Release3.1--ends
  
    "DEAL.MIPREMIUMAMOUNT, DEAL.MIPOLICYNUMBER, to_char(DEAL.MISTATUSID) MISTATUSID_STR, "+
    "to_char(DEAL.MITYPEID) MITYPEID_STR, DEAL.COPYID, DEAL.MIUPFRONT, "+
    "DEAL.MIEXISTINGPOLICYNUMBER, DEAL.MIRUINTERVENTION, DEAL.PREQUALIFICATIONMICERTNUM, "+
    "to_char(DEAL.MIINDICATORID) MIINDICATORID_STR, to_char(DEAL.MIPAYORID) MIPAYORID_STR, "+
    "DEAL.INSTITUTIONPROFILEID " +
    " FROM DEAL  "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, MORTGAGEINSURER, MISTATUS, MORTGAGEINSURANCETYPE, MIINDICATOR, MORTGAGEINSURANCEPAYOR";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "DEAL";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (DEAL.MITYPEID  =  MORTGAGEINSURANCETYPE.MORTGAGEINSURANCETYPEID) AND (DEAL.MORTGAGEINSURERID  =  MORTGAGEINSURER.MORTGAGEINSURERID) AND (DEAL.MISTATUSID  =  MISTATUS.MISTATUSID) AND (DEAL.MIINDICATORID  =  MIINDICATOR.MIINDICATORID) AND (DEAL.MIPAYORID  =  MORTGAGEINSURANCEPAYOR.MORTGAGEINSURANCEPAYORID)";
	public static final String STATIC_WHERE_CRITERIA = "";
  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT="DEAL.MIPREMIUMAMOUNT";
	public static final String COLUMN_DFMIPREMIUMAMOUNT="MIPREMIUMAMOUNT";
	public static final String QUALIFIED_COLUMN_DFMICERTIFICATENO="DEAL.MIPOLICYNUMBER";
	public static final String COLUMN_DFMICERTIFICATENO="MIPOLICYNUMBER";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFMIUPFRONT="DEAL.MIUPFRONT";
	public static final String COLUMN_DFMIUPFRONT="MIUPFRONT";
	public static final String QUALIFIED_COLUMN_DFMIEXISTINGPOLICYNUMBER="DEAL.MIEXISTINGPOLICYNUMBER";
	public static final String COLUMN_DFMIEXISTINGPOLICYNUMBER="MIEXISTINGPOLICYNUMBER";
	public static final String QUALIFIED_COLUMN_DFMIRUINTERVENTION="DEAL.MIRUINTERVENTION";
	public static final String COLUMN_DFMIRUINTERVENTION="MIRUINTERVENTION";
	public static final String QUALIFIED_COLUMN_DFPREQUALIFICATIONMICERTNUM="DEAL.PREQUALIFICATIONMICERTNUM";
	public static final String COLUMN_DFPREQUALIFICATIONMICERTNUM="PREQUALIFICATIONMICERTNUM";


	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFMIINSURER="MORTGAGEINSURER.MORTGAGEINSURERNAME";
	//public static final String COLUMN_DFMIINSURER="MORTGAGEINSURERNAME";
  public static final String QUALIFIED_COLUMN_DFMIINSURER="DEAL.MORTGAGEINSURERID_STR";
	public static final String COLUMN_DFMIINSURER="MORTGAGEINSURERID_STR";
	// SEAN DJ SPEC-Progress Advance Type July 25, 2005: define the column...
	public static final String QUALIFIED_COLUMN_DFPROGRESSADVANCETYPE="DEAL.PROGRESSADVANCETYPEID_STR";
	public static final String COLUMN_DFPROGRESSADVANCETYPE="PROGRESSADVANCETYPEID_STR";
	// SEAN DJ SPEC-PAT END
  //public static final String QUALIFIED_COLUMN_DFMIREQUESTSTATUS="MISTATUS.MISTATUSDESCRIPTION";
	//public static final String COLUMN_DFMIREQUESTSTATUS="MISTATUSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFMIREQUESTSTATUS="DEAL.MISTATUSID_STR";
	public static final String COLUMN_DFMIREQUESTSTATUS="MISTATUSID_STR";
  //public static final String QUALIFIED_COLUMN_DFMITYPEDESC="MORTGAGEINSURANCETYPE.MTDESCRIPTION";
	//public static final String COLUMN_DFMITYPEDESC="MTDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFMITYPEDESC="DEAL.MITYPEID";
	public static final String COLUMN_DFMITYPEDESC="MITYPEID_STR";
  //public static final String QUALIFIED_COLUMN_DFMIINDICATORDESC="MIINDICATOR.MIIDESCRIPTION";
	//public static final String COLUMN_DFMIINDICATORDESC="MIIDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFMIINDICATORDESC="DEAL.MIINDICATORID_STR";
	public static final String COLUMN_DFMIINDICATORDESC="MIINDICATORID_STR";
  //public static final String QUALIFIED_COLUMN_DFMIPAYORDESC="MORTGAGEINSURANCEPAYOR.MIPDESCRIPTION";
	//public static final String COLUMN_DFMIPAYORDESC="MIPDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFMIPAYORDESC="DEAL.MIPAYORID_STR";
	public static final String COLUMN_DFMIPAYORDESC="MIPAYORID_STR";

  //--Release3.1--begins
  //--by Hiro Apr 11, 2006
  
  public static final String QUALIFIED_COLUMN_DFPROGRESSADVANCEINSPECTIONBY = "DEAL.PROGRESSADVANCEINSPECTIONBY_S";

  public static final String COLUMN_DFPROGRESSADVANCEINSPECTIONBY = "PROGRESSADVANCEINSPECTIONBY_S";

  public static final String QUALIFIED_COLUMN_DFSELFDIRECTEDRRSP = "DEAL.SELFDIRECTEDRRSP";

  public static final String COLUMN_DFSELFDIRECTEDRRSP = "SELFDIRECTEDRRSP";

  public static final String QUALIFIED_COLUMN_DFCMHCPRODUCTTRACKERIDENTIFIER = "DEAL.CMHCPRODUCTTRACKERIDENTIFIER";

  public static final String COLUMN_DFCMHCPRODUCTTRACKERIDENTIFIER = "CMHCPRODUCTTRACKERIDENTIFIER";

  public static final String QUALIFIED_COLUMN_DFLOCREPAYMENTTYPEID = "DEAL.LOCREPAYMENTTYPEID_STR";

  public static final String COLUMN_DFLOCREPAYMENTTYPEID = "LOCREPAYMENTTYPEID_STR";

  public static final String QUALIFIED_COLUMN_DFREQUESTSTANDARDSERVICE = "DEAL.REQUESTSTANDARDSERVICE";

  public static final String COLUMN_DFREQUESTSTANDARDSERVICE = "REQUESTSTANDARDSERVICE";

  public static final String QUALIFIED_COLUMN_DFLOCAMORTIZATIONMONTHS = "DEAL.LOCAMORTIZATIONMONTHS";

  public static final String COLUMN_DFLOCAMORTIZATIONMONTHS = "LOCAMORTIZATIONMONTHS";

  public static final String QUALIFIED_COLUMN_DFLOCINTERESTONLYMATURITYDATE = "DEAL.LOCINTERESTONLYMATURITYDATE";

  public static final String COLUMN_DFLOCINTERESTONLYMATURITYDATE = "LOCINTERESTONLYMATURITYDATE";

  public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
	    "DEAL.INSTITUTIONPROFILEID";
	  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";
	  
  //--Release3.1--ends
  
  
	// //////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIINSURER,
				COLUMN_DFMIINSURER,
				QUALIFIED_COLUMN_DFMIINSURER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		// SEAN DJ SPEC-Progress Advance Type July 25, 2005: make association.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROGRESSADVANCETYPE,
				COLUMN_DFPROGRESSADVANCETYPE,
				QUALIFIED_COLUMN_DFPROGRESSADVANCETYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
		// SEAN DJ SPEC-PAT END

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPREMIUMAMOUNT,
				COLUMN_DFMIPREMIUMAMOUNT,
				QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMICERTIFICATENO,
				COLUMN_DFMICERTIFICATENO,
				QUALIFIED_COLUMN_DFMICERTIFICATENO,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIREQUESTSTATUS,
				COLUMN_DFMIREQUESTSTATUS,
				QUALIFIED_COLUMN_DFMIREQUESTSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMITYPEDESC,
				COLUMN_DFMITYPEDESC,
				QUALIFIED_COLUMN_DFMITYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIUPFRONT,
				COLUMN_DFMIUPFRONT,
				QUALIFIED_COLUMN_DFMIUPFRONT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIEXISTINGPOLICYNUMBER,
				COLUMN_DFMIEXISTINGPOLICYNUMBER,
				QUALIFIED_COLUMN_DFMIEXISTINGPOLICYNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIRUINTERVENTION,
				COLUMN_DFMIRUINTERVENTION,
				QUALIFIED_COLUMN_DFMIRUINTERVENTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREQUALIFICATIONMICERTNUM,
				COLUMN_DFPREQUALIFICATIONMICERTNUM,
				QUALIFIED_COLUMN_DFPREQUALIFICATIONMICERTNUM,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIINDICATORDESC,
				COLUMN_DFMIINDICATORDESC,
				QUALIFIED_COLUMN_DFMIINDICATORDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMIPAYORDESC,
				COLUMN_DFMIPAYORDESC,
				QUALIFIED_COLUMN_DFMIPAYORDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    
    //--Release3.1--begins
    //--by Hiro Apr 11, 2006
		FIELD_SCHEMA.addFieldDescriptor(
		    new QueryFieldDescriptor(
		        FIELD_DFPROGRESSADVANCEINSPECTIONBY,
		        COLUMN_DFPROGRESSADVANCEINSPECTIONBY,
		        QUALIFIED_COLUMN_DFPROGRESSADVANCEINSPECTIONBY,
		        String.class,
		        false,
		        false,
		        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		        "",
		        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		    ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
		    new QueryFieldDescriptor(
		        FIELD_DFSELFDIRECTEDRRSP,
		        COLUMN_DFSELFDIRECTEDRRSP,
		        QUALIFIED_COLUMN_DFSELFDIRECTEDRRSP,
		        String.class,
		        false,
		        false,
		        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		        "",
		        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		    ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
		    new QueryFieldDescriptor(
		        FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER,
		        COLUMN_DFCMHCPRODUCTTRACKERIDENTIFIER,
		        QUALIFIED_COLUMN_DFCMHCPRODUCTTRACKERIDENTIFIER,
            String.class,
		        false,
		        false,
		        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		        "",
		        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		    ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
		    new QueryFieldDescriptor(
		        FIELD_DFLOCREPAYMENT,
		        COLUMN_DFLOCREPAYMENTTYPEID,
		        QUALIFIED_COLUMN_DFLOCREPAYMENTTYPEID,
		        String.class,
		        false,
		        false,
		        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		        "",
		        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		    ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
		    new QueryFieldDescriptor(
		        FIELD_DFREQUESTSTANDARDSERVICE,
		        COLUMN_DFREQUESTSTANDARDSERVICE,
		        QUALIFIED_COLUMN_DFREQUESTSTANDARDSERVICE,
		        String.class,
		        false,
		        false,
		        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		        "",
		        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		    ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
		    new QueryFieldDescriptor(
		        FIELD_DFLOCAMORTIZATION,
		        COLUMN_DFLOCAMORTIZATIONMONTHS,
		        QUALIFIED_COLUMN_DFLOCAMORTIZATIONMONTHS,
            BigDecimal.class,
		        false,
		        false,
		        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		        "",
		        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		    ""));
		
		FIELD_SCHEMA.addFieldDescriptor(
		    new QueryFieldDescriptor(
		        FIELD_DFLOCINTERESTONLYMATURITYDATE,
		        COLUMN_DFLOCINTERESTONLYMATURITYDATE,
		        QUALIFIED_COLUMN_DFLOCINTERESTONLYMATURITYDATE,
		        Timestamp.class,
		        false,
		        false,
		        QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		        "",
		        QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		    ""));
    //--Release3.1--ends
		
	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));

	}


}

