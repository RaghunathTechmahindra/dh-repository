package mosApp.MosSystem;

import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * 
 *
 *
 */
public class doUnderwriterModelImpl
	extends QueryModelBase
	implements doUnderwriterModel {

	/**
	 * default constructor
	 *
	 */
	public doUnderwriterModelImpl() {
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}

	/**
	 *
	 *
	 */
	public void initialize() {
	}

	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(
		ModelExecutionContext context,
		int queryType,
		String sql)
		throws ModelControlException {

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}

	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException {
	}

	/**
	 *
	 *
	 */
	protected void onDatabaseError(
		ModelExecutionContext context,
		int queryType,
		SQLException exception) {
	}

	/**
	 *
	 *
	 */
	public String getDfUsername() {
		return (String) getValue(FIELD_DFUSERNAME);
	}

	/**
	 *
	 *
	 */
	public void setDfUsername(String value) {
		setValue(FIELD_DFUSERNAME, value);
	}

	/**
	 *
	 *
	 */
	public String getMOS_CONTACT_CONTACTFIRSTNAME() {
		return (String) getValue(FIELD_MOS_CONTACT_CONTACTFIRSTNAME);
	}

	/**
	 *
	 *
	 */
	public void setMOS_CONTACT_CONTACTFIRSTNAME(String value) {
		setValue(FIELD_MOS_CONTACT_CONTACTFIRSTNAME, value);
	}

	/**
	 *
	 *
	 */
	public String getMOS_CONTACT_CONTACTLASTNAME() {
		return (String) getValue(FIELD_MOS_CONTACT_CONTACTLASTNAME);
	}

	/**
	 *
	 *
	 */
	public void setMOS_CONTACT_CONTACTLASTNAME(String value) {
		setValue(FIELD_MOS_CONTACT_CONTACTLASTNAME, value);
	}

	/**
	 *
	 *
	 */
	public Integer getDfUserProfileId() {
		return (Integer) getValue(FIELD_DFUSERPROFILEID);
	}

	/**
	 *
	 *
	 */
	public void setDfUserProfileId(Integer value) {
		setValue(FIELD_DFUSERPROFILEID, value);
	}

	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
	/**
		public void setResultSet(ResultSet value)
		{
			logger = SysLog.getSysLogger("METADATA");
	
			try
			{
	      super.setResultSet(value);
	 			if( value != null)
	 			{
	     			ResultSetMetaData metaData = value.getMetaData();
	     			int columnCount = metaData.getColumnCount();
	     			logger.debug("===============================================");
	          logger.debug("Testing MetaData Object for new synthetic fields for" +
	                        " doUnderwriterModel");
	     			logger.debug("NumberColumns: " + columnCount);
	       		for(int i = 0; i < columnCount ; i++)
	        		{
	               	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
	        		}
	        		logger.debug("================================================");
	    		}
			}
			catch (SQLException e)
			{
	  		    logger.debug("Class: " + getClass().getName());
	                  logger.debug("SQLException: " + e);
			}
		}
	**/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	//// Workaround to solve the bug of the iMT tool for the dataobject 
	//// with a ComputedColumn
	//// (see detailed description in the desing doc as well).
	//// If there is a ComputedColumn in the ND dataobject a "fake", 
	//// synthetic definition of this
	//// field must be included into the model's TEMPLATE and FieldDescriptor 
	//// must be adjusted accordingly.
	
	public static final String DATA_SOURCE_NAME = "jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL MIN(CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1))  USERNAME, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTLASTNAME, MIN(USERPROFILE.USERPROFILEID ) USERPROFILEID FROM CONTACT, USERPROFILE, USERTYPE  __WHERE__  GROUP BY CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTLASTNAME ORDER BY CONTACT.CONTACTLASTNAME  ASC, CONTACT.CONTACTFIRSTNAME  ASC";
	public static final String SELECT_SQL_TEMPLATE =
		"SELECT ALL MIN(CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1)) SYNTHETICUSERNAME, CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTLASTNAME, MIN(USERPROFILE.USERPROFILEID ) SYNTHETICUSERPROFILE FROM CONTACT, USERPROFILE, USERTYPE  __WHERE__  GROUP BY CONTACT.CONTACTFIRSTNAME, CONTACT.CONTACTLASTNAME ORDER BY CONTACT.CONTACTLASTNAME  ASC, CONTACT.CONTACTFIRSTNAME  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME =
		"CONTACT, USERPROFILE, USERTYPE";
	public static final String STATIC_WHERE_CRITERIA =
		" (((CONTACT.CONTACTID  =  USERPROFILE.CONTACTID) AND (CONTACT.INSTITUTIONPROFILEID = USERPROFILE.INSTITUTIONPROFILEID) AND (USERPROFILE.USERTYPEID  =  USERTYPE.USERTYPEID)) AND CONTACT.COPYID = 1) AND userprofile.usertypeid<>99 AND userprofile.usertypeid<>98 AND userprofile.usertypeid<>97 "; //FXP21168: Since there are no rules/properties that define these types of user, hard-coding is the only way.
	public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();
	////public static final String QUALIFIED_COLUMN_DFUSERNAME=".";
	////public static final String COLUMN_DFUSERNAME="";
	public static final String QUALIFIED_COLUMN_DFUSERNAME =
		"CONTACT.SYNTHETICUSERNAME";
	public static final String COLUMN_DFUSERNAME = "SYNTHETICUSERNAME";
	public static final String QUALIFIED_COLUMN_MOS_CONTACT_CONTACTFIRSTNAME =
		"CONTACT.CONTACTFIRSTNAME";
	public static final String COLUMN_MOS_CONTACT_CONTACTFIRSTNAME =
		"CONTACTFIRSTNAME";
	public static final String QUALIFIED_COLUMN_MOS_CONTACT_CONTACTLASTNAME =
		"CONTACT.CONTACTLASTNAME";
	public static final String COLUMN_MOS_CONTACT_CONTACTLASTNAME =
		"CONTACTLASTNAME";
	////public static final String QUALIFIED_COLUMN_DFUSERPROFILEID=".";
	////public static final String COLUMN_DFUSERPROFILEID="";
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID =
		"USERPROFILE.SYNTHETICUSERPROFILE";
	public static final String COLUMN_DFUSERPROFILEID = "SYNTHETICUSERPROFILE";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static {

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFUSERNAME,
		////		COLUMN_DFUSERNAME,
		////		QUALIFIED_COLUMN_DFUSERNAME,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjustment to fix the iMT tool bug for the ComputedColumn.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERNAME,
				COLUMN_DFUSERNAME,
				QUALIFIED_COLUMN_DFUSERNAME,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"MIN(CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1))",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"MIN(CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1))"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_CONTACT_CONTACTFIRSTNAME,
				COLUMN_MOS_CONTACT_CONTACTFIRSTNAME,
				QUALIFIED_COLUMN_MOS_CONTACT_CONTACTFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_MOS_CONTACT_CONTACTLASTNAME,
				COLUMN_MOS_CONTACT_CONTACTLASTNAME,
				QUALIFIED_COLUMN_MOS_CONTACT_CONTACTLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFUSERPROFILEID,
		////		COLUMN_DFUSERPROFILEID,
		////		QUALIFIED_COLUMN_DFUSERPROFILEID,
		////		Integer.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjustment to fix the iMT tool bug for the ComputedColumn.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				Integer.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"MIN(USERPROFILE.USERPROFILEID)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"MIN(USERPROFILE.USERPROFILEID)"));

	}

}
