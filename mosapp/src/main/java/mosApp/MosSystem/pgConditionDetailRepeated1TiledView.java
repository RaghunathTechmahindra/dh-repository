package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.StaticTextField;


/**
 *
 *
 *
 */
public class pgConditionDetailRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgConditionDetailRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doConditionDetailModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStConditionText().setValue(CHILD_STCONDITIONTEXT_RESET_VALUE);
		getStConditionLabel().setValue(CHILD_STCONDITIONLABEL_RESET_VALUE);
		getStLanguage().setValue(CHILD_STLANGUAGE_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STCONDITIONTEXT,StaticTextField.class);
		registerChild(CHILD_STCONDITIONLABEL,StaticTextField.class);
		registerChild(CHILD_STLANGUAGE,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoConditionDetailModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;

	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STCONDITIONTEXT))
		{
			StaticTextField child = new StaticTextField(this,
				getdoConditionDetailModel(),
				CHILD_STCONDITIONTEXT,
				doConditionDetailModel.FIELD_DFDOCUMENTTEXT,
				CHILD_STCONDITIONTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCONDITIONLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoConditionDetailModel(),
				CHILD_STCONDITIONLABEL,
				doConditionDetailModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STCONDITIONLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STLANGUAGE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoConditionDetailModel(),
				CHILD_STLANGUAGE,
				doConditionDetailModel.FIELD_DFLANGUAGE,
				CHILD_STLANGUAGE_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStConditionText()
	{
		return (StaticTextField)getChild(CHILD_STCONDITIONTEXT);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStConditionLabel()
	{
		return (StaticTextField)getChild(CHILD_STCONDITIONLABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStLanguage()
	{
		return (StaticTextField)getChild(CHILD_STLANGUAGE);
	}


	/**
	 *
	 *
	 */
	public doConditionDetailModel getdoConditionDetailModel()
	{
		if (doConditionDetail == null)
			doConditionDetail = (doConditionDetailModel) getModel(doConditionDetailModel.class);
		return doConditionDetail;
	}


	/**
	 *
	 *
	 */
	public void setdoConditionDetailModel(doConditionDetailModel model)
	{
			doConditionDetail = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STCONDITIONTEXT="stConditionText";
	public static final String CHILD_STCONDITIONTEXT_RESET_VALUE="";
	public static final String CHILD_STCONDITIONLABEL="stConditionLabel";
	public static final String CHILD_STCONDITIONLABEL_RESET_VALUE="";
	public static final String CHILD_STLANGUAGE="stLanguage";
	public static final String CHILD_STLANGUAGE_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doConditionDetailModel doConditionDetail=null;

}

