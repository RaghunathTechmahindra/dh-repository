package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

//--> --TOBEREMOVED--
//--> This model was not used any more.  Can be removed later
//--> By Billy 06Dec2002

/**
 *
 *
 *
 */
public class doUserAdminPageAccessTypeModelImpl extends QueryModelBase
	implements doUserAdminPageAccessTypeModel
{
	/**
	 *
	 *
	 */
	public doUserAdminPageAccessTypeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPageId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPAGEID);
	}


	/**
	 *
	 *
	 */
	public void setDfPageId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPAGEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPageName()
	{
		return (String)getValue(FIELD_DFPAGENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfPageName(String value)
	{
		setValue(FIELD_DFPAGENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPageLabel()
	{
		return (String)getValue(FIELD_DFPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfPageLabel(String value)
	{
		setValue(FIELD_DFPAGELABEL,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL PAGE.PAGEID, PAGE.PAGENAME, PAGE.PAGELABEL FROM PAGE  "+
    "__WHERE__  "+
    "ORDER BY PAGE.PAGEID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="PAGE";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPAGEID="PAGE.PAGEID";
	public static final String COLUMN_DFPAGEID="PAGEID";
	public static final String QUALIFIED_COLUMN_DFPAGENAME="PAGE.PAGENAME";
	public static final String COLUMN_DFPAGENAME="PAGENAME";
	public static final String QUALIFIED_COLUMN_DFPAGELABEL="PAGE.PAGELABEL";
	public static final String COLUMN_DFPAGELABEL="PAGELABEL";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAGEID,
				COLUMN_DFPAGEID,
				QUALIFIED_COLUMN_DFPAGEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAGENAME,
				COLUMN_DFPAGENAME,
				QUALIFIED_COLUMN_DFPAGENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAGELABEL,
				COLUMN_DFPAGELABEL,
				QUALIFIED_COLUMN_DFPAGELABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

