package mosApp.MosSystem;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import mosApp.SQLConnectionManagerImpl;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.jato.ExpressViewBeanBase;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryExecutionContext;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.CommandFieldDescriptor;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HREF;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 *
 *
 *
 */
public class pgInterestRateUpdateViewBean extends ExpressViewBeanBase

{
	/**
	 *
	 *
	 */
	public pgInterestRateUpdateViewBean()
	{
		super(PAGE_NAME);
		setDefaultDisplayURL(DEFAULT_DISPLAY_URL);
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		View superReturn = super.createChild(name);  
		if (superReturn != null) {  
		return superReturn;  
		} else if (name.equals(CHILD_TBDEALID)) 
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDEALID,
				CHILD_TBDEALID,
				CHILD_TBDEALID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBPAGENAMES))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES,
				CHILD_CBPAGENAMES_RESET_VALUE,
				null);
			//--Release2.1--//
			//child.setLabelForNoneSelected("Choose a Page");
      child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
			child.setOptions(cbPageNamesOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTPROCEED))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPROCEED,
				CHILD_BTPROCEED,
				CHILD_BTPROCEED_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF1))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF1,
				CHILD_HREF1,
				CHILD_HREF1_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF2))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF2,
				CHILD_HREF2,
				CHILD_HREF2_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF3))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF3,
				CHILD_HREF3,
				CHILD_HREF3_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF4))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF4,
				CHILD_HREF4,
				CHILD_HREF4_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF5))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF5,
				CHILD_HREF5,
				CHILD_HREF5_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF6))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF6,
				CHILD_HREF6,
				CHILD_HREF6_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF7))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF7,
				CHILD_HREF7,
				CHILD_HREF7_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF8))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF8,
				CHILD_HREF8,
				CHILD_HREF8_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF9))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF9,
				CHILD_HREF9,
				CHILD_HREF9_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HREF10))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_HREF10,
				CHILD_HREF10,
				CHILD_HREF10_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL,
				CHILD_STPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME,
				CHILD_STCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTODAYDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE,
				CHILD_STTODAYDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STUSERNAMETITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE,
				CHILD_STUSERNAMETITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSUBMIT))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT,
				CHILD_BTSUBMIT_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTWORKQUEUELINK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK,
				CHILD_BTWORKQUEUELINK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_CHANGEPASSWORDHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF,
				CHILD_CHANGEPASSWORDHREF_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLHISTORY))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY,
				CHILD_BTTOOLHISTORY_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOONOTES))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES,
				CHILD_BTTOONOTES_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLSEARCH))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH,
				CHILD_BTTOOLSEARCH_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTTOOLLOG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG,
				CHILD_BTTOOLLOG_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STERRORFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG,
				CHILD_STERRORFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_DETECTALERTTASKS))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS,
				CHILD_DETECTALERTTASKS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTCANCEL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTCANCEL,
				CHILD_BTCANCEL,
				CHILD_BTCANCEL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_BTPREVTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE,
				CHILD_BTPREVTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STPREVTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL,
				CHILD_STPREVTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTNEXTTASKPAGE))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE,
				CHILD_BTNEXTTASKPAGE_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STNEXTTASKPAGELABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL,
				CHILD_STNEXTTASKPAGELABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STTASKNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STTASKNAME,
				CHILD_STTASKNAME,
				CHILD_STTASKNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_SESSIONUSERID))
		{
			HiddenField child = new HiddenField(this,
				getDefaultModel(),
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID,
				CHILD_SESSIONUSERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STCODE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STCODE,
				CHILD_STCODE,
				CHILD_STCODE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STDESCRIPTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STDESCRIPTION,
				CHILD_STDESCRIPTION,
				CHILD_STDESCRIPTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBSTATUS))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBSTATUS,
				CHILD_CBSTATUS,
				CHILD_CBSTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbStatusOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBMONTH))
		{
			ComboBox child = new ComboBox( this,
				getDefaultModel(),
				CHILD_CBMONTH,
				CHILD_CBMONTH,
				CHILD_CBMONTH_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMonthOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBDAY))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBDAY,
				CHILD_TBDAY,
				CHILD_TBDAY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBYEAR))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBYEAR,
				CHILD_TBYEAR,
				CHILD_TBYEAR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBHOUR))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBHOUR,
				CHILD_TBHOUR,
				CHILD_TBHOUR_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBMINUTE))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBMINUTE,
				CHILD_TBMINUTE,
				CHILD_TBMINUTE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBPOSTEDRATE))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBPOSTEDRATE,
				CHILD_TBPOSTEDRATE,
				CHILD_TBPOSTEDRATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBMAXDISCOUNTRATE))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBMAXDISCOUNTRATE,
				CHILD_TBMAXDISCOUNTRATE,
				CHILD_TBMAXDISCOUNTRATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_TBBESTRATE))
		{
			TextField child = new TextField(this,
				getDefaultModel(),
				CHILD_TBBESTRATE,
				CHILD_TBBESTRATE,
				CHILD_TBBESTRATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE,
				CHILD_STPMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE,
				CHILD_STPMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO,
				CHILD_STPMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE,
				CHILD_STPMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMHASOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMHASOK,
				CHILD_STPMHASOK,
				CHILD_STPMHASOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMTITLE,
				CHILD_STPMTITLE,
				CHILD_STPMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG,
				CHILD_STPMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMONOK))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMONOK,
				CHILD_STPMONOK,
				CHILD_STPMONOK_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGS,
				CHILD_STPMMSGS,
				CHILD_STPMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES,
				CHILD_STPMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMGENERATE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE,
				CHILD_STAMGENERATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE,
				CHILD_STAMHASTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASINFO))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO,
				CHILD_STAMHASINFO_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMHASTABLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE,
				CHILD_STAMHASTABLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMTITLE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMTITLE,
				CHILD_STAMTITLE,
				CHILD_STAMTITLE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMINFOMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG,
				CHILD_STAMINFOMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGS,
				CHILD_STAMMSGS,
				CHILD_STAMMSGS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMMSGTYPES))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES,
				CHILD_STAMMSGTYPES_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMDIALOGMSG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG,
				CHILD_STAMDIALOGMSG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAMBUTTONSHTML))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML,
				CHILD_STAMBUTTONSHTML_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDDUMMY))
		{
			HiddenField child = new HiddenField(this,
				getdoRowGeneratorModel(),
				CHILD_HDDUMMY,
				doRowGeneratorModel.FIELD_DFROWNDX,
				CHILD_HDDUMMY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTOK))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTOK,
				CHILD_BTOK,
				CHILD_BTOK_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STVIEWONLYTAG))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG,
				CHILD_STVIEWONLYTAG_RESET_VALUE,
				null);
			return child;
		}
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> by BILLY 15July2002
    else
    if (name.equals(CHILD_BTACTMSG))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTACTMSG,
				CHILD_BTACTMSG,
				CHILD_BTACTMSG_RESET_VALUE,
				new CommandFieldDescriptor(ActMessageCommand.COMMAND_DESCRIPTOR));
				return child;
    }
    //--Release2.1--//
    //// New link to toggle language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new session
    //// language id throughout the all modulus.
		else
		if (name.equals(CHILD_TOGGLELANGUAGEHREF))
		{
			HREF child = new HREF(
				this,
				getDefaultModel(),
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF,
				CHILD_TOGGLELANGUAGEHREF_RESET_VALUE,
				null);
				return child;
    }
    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    else
    if (name.equals(CHILD_TXTEASERRATE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXTEASERRATE,
        CHILD_TXTEASERRATE,
        CHILD_TXTEASERRATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_CBBASEINDEX))
    {
      ComboBox child = new ComboBox( this,
        getDefaultModel(),
        CHILD_CBBASEINDEX,
        CHILD_CBBASEINDEX,
        CHILD_CBBASEINDEX_RESET_VALUE,
        null);
      child.setLabelForNoneSelected("");
      child.setOptions(cbBaseIndexOptions);
      return child;
    }
    else
    if (name.equals(CHILD_TXBASERATE))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXBASERATE,
        CHILD_TXBASERATE,
        CHILD_TXBASERATE_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_TXBASEADJUSTMENT))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXBASEADJUSTMENT,
        CHILD_TXBASEADJUSTMENT,
        CHILD_TXBASEADJUSTMENT_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_TXTEASERDISCOUNT))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXTEASERDISCOUNT,
        CHILD_TXTEASERDISCOUNT,
        CHILD_TXTEASERDISCOUNT_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_TXTEASERTERMS))
    {
      TextField child = new TextField(this,
        getDefaultModel(),
        CHILD_TXTEASERTERMS,
        CHILD_TXTEASERTERMS,
        CHILD_TXTEASERTERMS_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_HDBASEINDEXID))
    {
      HiddenField child = new HiddenField(this,
        getDefaultModel(),
        CHILD_HDBASEINDEXID,
        CHILD_HDBASEINDEXID,
        CHILD_HDBASEINDEXID_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STBASEINDXLASTUPDATEDTIME))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STBASEINDXLASTUPDATEDTIME,
        CHILD_STBASEINDXLASTUPDATEDTIME,
        CHILD_STBASEINDXLASTUPDATEDTIME_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_BTRECALCRATE))
    {
      Button child = new Button(
        this,
        getDefaultModel(),
        CHILD_BTRECALCRATE,
        CHILD_BTRECALCRATE,
        CHILD_BTRECALCRATE_RESET_VALUE,
        null);
        return child;
    }
    else
    if (name.equals(CHILD_STINCLUDEPRIMEINDXSECTIONSTART))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART,
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART,
        CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE,
        null);
      return child;
    }
    else
    if (name.equals(CHILD_STINCLUDEPRIMEINDXSECTIONEND))
    {
      StaticTextField child = new StaticTextField(this,
        getDefaultModel(),
        CHILD_STINCLUDEPRIMEINDXSECTIONEND,
        CHILD_STINCLUDEPRIMEINDXSECTIONEND,
        CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE,
        null);
      return child;
    }
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		super.resetChildren(); 
		getTbDealId().setValue(CHILD_TBDEALID_RESET_VALUE);
		getCbPageNames().setValue(CHILD_CBPAGENAMES_RESET_VALUE);
		getBtProceed().setValue(CHILD_BTPROCEED_RESET_VALUE);
		getHref1().setValue(CHILD_HREF1_RESET_VALUE);
		getHref2().setValue(CHILD_HREF2_RESET_VALUE);
		getHref3().setValue(CHILD_HREF3_RESET_VALUE);
		getHref4().setValue(CHILD_HREF4_RESET_VALUE);
		getHref5().setValue(CHILD_HREF5_RESET_VALUE);
		getHref6().setValue(CHILD_HREF6_RESET_VALUE);
		getHref7().setValue(CHILD_HREF7_RESET_VALUE);
		getHref8().setValue(CHILD_HREF8_RESET_VALUE);
		getHref9().setValue(CHILD_HREF9_RESET_VALUE);
		getHref10().setValue(CHILD_HREF10_RESET_VALUE);
		getStPageLabel().setValue(CHILD_STPAGELABEL_RESET_VALUE);
		getStCompanyName().setValue(CHILD_STCOMPANYNAME_RESET_VALUE);
		getStTodayDate().setValue(CHILD_STTODAYDATE_RESET_VALUE);
		getStUserNameTitle().setValue(CHILD_STUSERNAMETITLE_RESET_VALUE);
		getBtSubmit().setValue(CHILD_BTSUBMIT_RESET_VALUE);
		getBtWorkQueueLink().setValue(CHILD_BTWORKQUEUELINK_RESET_VALUE);
		getChangePasswordHref().setValue(CHILD_CHANGEPASSWORDHREF_RESET_VALUE);
		getBtToolHistory().setValue(CHILD_BTTOOLHISTORY_RESET_VALUE);
		getBtTooNotes().setValue(CHILD_BTTOONOTES_RESET_VALUE);
		getBtToolSearch().setValue(CHILD_BTTOOLSEARCH_RESET_VALUE);
		getBtToolLog().setValue(CHILD_BTTOOLLOG_RESET_VALUE);
		getStErrorFlag().setValue(CHILD_STERRORFLAG_RESET_VALUE);
		getDetectAlertTasks().setValue(CHILD_DETECTALERTTASKS_RESET_VALUE);
		getBtCancel().setValue(CHILD_BTCANCEL_RESET_VALUE);
		getBtPrevTaskPage().setValue(CHILD_BTPREVTASKPAGE_RESET_VALUE);
		getStPrevTaskPageLabel().setValue(CHILD_STPREVTASKPAGELABEL_RESET_VALUE);
		getBtNextTaskPage().setValue(CHILD_BTNEXTTASKPAGE_RESET_VALUE);
		getStNextTaskPageLabel().setValue(CHILD_STNEXTTASKPAGELABEL_RESET_VALUE);
		getStTaskName().setValue(CHILD_STTASKNAME_RESET_VALUE);
		getSessionUserId().setValue(CHILD_SESSIONUSERID_RESET_VALUE);
		getStCode().setValue(CHILD_STCODE_RESET_VALUE);
		getStDescription().setValue(CHILD_STDESCRIPTION_RESET_VALUE);
		getCbStatus().setValue(CHILD_CBSTATUS_RESET_VALUE);
		getCbMonth().setValue(CHILD_CBMONTH_RESET_VALUE);
		getTbDay().setValue(CHILD_TBDAY_RESET_VALUE);
		getTbYear().setValue(CHILD_TBYEAR_RESET_VALUE);
		getTbHour().setValue(CHILD_TBHOUR_RESET_VALUE);
		getTbMinute().setValue(CHILD_TBMINUTE_RESET_VALUE);
		getTbPostedRate().setValue(CHILD_TBPOSTEDRATE_RESET_VALUE);
		getTbMaxDiscountRate().setValue(CHILD_TBMAXDISCOUNTRATE_RESET_VALUE);
		getTbBestRate().setValue(CHILD_TBBESTRATE_RESET_VALUE);
		getStPmGenerate().setValue(CHILD_STPMGENERATE_RESET_VALUE);
		getStPmHasTitle().setValue(CHILD_STPMHASTITLE_RESET_VALUE);
		getStPmHasInfo().setValue(CHILD_STPMHASINFO_RESET_VALUE);
		getStPmHasTable().setValue(CHILD_STPMHASTABLE_RESET_VALUE);
		getStPmHasOk().setValue(CHILD_STPMHASOK_RESET_VALUE);
		getStPmTitle().setValue(CHILD_STPMTITLE_RESET_VALUE);
		getStPmInfoMsg().setValue(CHILD_STPMINFOMSG_RESET_VALUE);
		getStPmOnOk().setValue(CHILD_STPMONOK_RESET_VALUE);
		getStPmMsgs().setValue(CHILD_STPMMSGS_RESET_VALUE);
		getStPmMsgTypes().setValue(CHILD_STPMMSGTYPES_RESET_VALUE);
		getStAmGenerate().setValue(CHILD_STAMGENERATE_RESET_VALUE);
		getStAmHasTitle().setValue(CHILD_STAMHASTITLE_RESET_VALUE);
		getStAmHasInfo().setValue(CHILD_STAMHASINFO_RESET_VALUE);
		getStAmHasTable().setValue(CHILD_STAMHASTABLE_RESET_VALUE);
		getStAmTitle().setValue(CHILD_STAMTITLE_RESET_VALUE);
		getStAmInfoMsg().setValue(CHILD_STAMINFOMSG_RESET_VALUE);
		getStAmMsgs().setValue(CHILD_STAMMSGS_RESET_VALUE);
		getStAmMsgTypes().setValue(CHILD_STAMMSGTYPES_RESET_VALUE);
		getStAmDialogMsg().setValue(CHILD_STAMDIALOGMSG_RESET_VALUE);
		getStAmButtonsHtml().setValue(CHILD_STAMBUTTONSHTML_RESET_VALUE);
		getHdDummy().setValue(CHILD_HDDUMMY_RESET_VALUE);
		getBtOk().setValue(CHILD_BTOK_RESET_VALUE);
		getStViewOnlyTag().setValue(CHILD_STVIEWONLYTAG_RESET_VALUE);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    getBtActMsg().setValue(CHILD_BTACTMSG_RESET_VALUE);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		getToggleLanguageHref().setValue(CHILD_TOGGLELANGUAGEHREF_RESET_VALUE);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    getTxTeaserRate().setValue(CHILD_TXTEASERRATE_RESET_VALUE);
    getCbBaseIndex().setValue(CHILD_CBBASEINDEX_RESET_VALUE);
    getTxBaseRate().setValue(CHILD_TXBASERATE_RESET_VALUE);
    getTxBaseAdjustment().setValue(CHILD_TXBASEADJUSTMENT_RESET_VALUE);
    getTxTeaserDiscount().setValue(CHILD_TXTEASERDISCOUNT_RESET_VALUE);
    getTxTeaserTerms().setValue(CHILD_TXTEASERTERMS_RESET_VALUE);
    getHdBaseIndexId().setValue(CHILD_HDBASEINDEXID_RESET_VALUE);
    getStBaseIndxLastUpdatedTime().setValue(CHILD_STBASEINDXLASTUPDATEDTIME_RESET_VALUE);
    getBtRecalcRate().setValue(CHILD_BTRECALCRATE_RESET_VALUE);
    getStIncludePrimeIndxSectionStart().setValue(CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE);
    getStIncludePrimeIndxSectionEnd().setValue(CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		super.registerChildren(); 
		registerChild(CHILD_TBDEALID,TextField.class);
		registerChild(CHILD_CBPAGENAMES,ComboBox.class);
		registerChild(CHILD_BTPROCEED,Button.class);
		registerChild(CHILD_HREF1,HREF.class);
		registerChild(CHILD_HREF2,HREF.class);
		registerChild(CHILD_HREF3,HREF.class);
		registerChild(CHILD_HREF4,HREF.class);
		registerChild(CHILD_HREF5,HREF.class);
		registerChild(CHILD_HREF6,HREF.class);
		registerChild(CHILD_HREF7,HREF.class);
		registerChild(CHILD_HREF8,HREF.class);
		registerChild(CHILD_HREF9,HREF.class);
		registerChild(CHILD_HREF10,HREF.class);
		registerChild(CHILD_STPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STTODAYDATE,StaticTextField.class);
		registerChild(CHILD_STUSERNAMETITLE,StaticTextField.class);
		registerChild(CHILD_BTSUBMIT,Button.class);
		registerChild(CHILD_BTWORKQUEUELINK,Button.class);
		registerChild(CHILD_CHANGEPASSWORDHREF,HREF.class);
		registerChild(CHILD_BTTOOLHISTORY,Button.class);
		registerChild(CHILD_BTTOONOTES,Button.class);
		registerChild(CHILD_BTTOOLSEARCH,Button.class);
		registerChild(CHILD_BTTOOLLOG,Button.class);
		registerChild(CHILD_STERRORFLAG,StaticTextField.class);
		registerChild(CHILD_DETECTALERTTASKS,HiddenField.class);
		registerChild(CHILD_BTCANCEL,Button.class);
		registerChild(CHILD_BTPREVTASKPAGE,Button.class);
		registerChild(CHILD_STPREVTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_BTNEXTTASKPAGE,Button.class);
		registerChild(CHILD_STNEXTTASKPAGELABEL,StaticTextField.class);
		registerChild(CHILD_STTASKNAME,StaticTextField.class);
		registerChild(CHILD_SESSIONUSERID,HiddenField.class);
		registerChild(CHILD_STCODE,StaticTextField.class);
		registerChild(CHILD_STDESCRIPTION,StaticTextField.class);
		registerChild(CHILD_CBSTATUS,ComboBox.class);
		registerChild(CHILD_CBMONTH,ComboBox.class);
		registerChild(CHILD_TBDAY,TextField.class);
		registerChild(CHILD_TBYEAR,TextField.class);
		registerChild(CHILD_TBHOUR,TextField.class);
		registerChild(CHILD_TBMINUTE,TextField.class);
		registerChild(CHILD_TBPOSTEDRATE,TextField.class);
		registerChild(CHILD_TBMAXDISCOUNTRATE,TextField.class);
		registerChild(CHILD_TBBESTRATE,TextField.class);
		registerChild(CHILD_STPMGENERATE,StaticTextField.class);
		registerChild(CHILD_STPMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STPMHASINFO,StaticTextField.class);
		registerChild(CHILD_STPMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STPMHASOK,StaticTextField.class);
		registerChild(CHILD_STPMTITLE,StaticTextField.class);
		registerChild(CHILD_STPMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STPMONOK,StaticTextField.class);
		registerChild(CHILD_STPMMSGS,StaticTextField.class);
		registerChild(CHILD_STPMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMGENERATE,StaticTextField.class);
		registerChild(CHILD_STAMHASTITLE,StaticTextField.class);
		registerChild(CHILD_STAMHASINFO,StaticTextField.class);
		registerChild(CHILD_STAMHASTABLE,StaticTextField.class);
		registerChild(CHILD_STAMTITLE,StaticTextField.class);
		registerChild(CHILD_STAMINFOMSG,StaticTextField.class);
		registerChild(CHILD_STAMMSGS,StaticTextField.class);
		registerChild(CHILD_STAMMSGTYPES,StaticTextField.class);
		registerChild(CHILD_STAMDIALOGMSG,StaticTextField.class);
		registerChild(CHILD_STAMBUTTONSHTML,StaticTextField.class);
		registerChild(CHILD_HDDUMMY,HiddenField.class);
		registerChild(CHILD_BTOK,Button.class);
		registerChild(CHILD_STVIEWONLYTAG,StaticTextField.class);
    //--> Hidden ActMessageButton (FINDTHISLATER)
    //--> Test by BILLY 15July2002
    registerChild(CHILD_BTACTMSG,Button.class);
    //--Release2.1--//
    //// New link to toggle the language. When touched this link should reload the
    //// page in opposite language (french versus english) and set this new language
    //// session value id throughout all modulus.
		registerChild(CHILD_TOGGLELANGUAGEHREF,HREF.class);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    registerChild(CHILD_TXTEASERRATE,TextField.class);
    registerChild(CHILD_CBBASEINDEX,ComboBox.class);
    registerChild(CHILD_TXBASERATE,TextField.class);
    registerChild(CHILD_TXBASEADJUSTMENT,TextField.class);
    registerChild(CHILD_TXTEASERDISCOUNT,TextField.class);
    registerChild(CHILD_TXTEASERTERMS,TextField.class);
    registerChild(CHILD_HDBASEINDEXID,HiddenField.class);
    registerChild(CHILD_STBASEINDXLASTUPDATEDTIME,StaticTextField.class);
    registerChild(CHILD_BTRECALCRATE,Button.class);
    registerChild(CHILD_STINCLUDEPRIMEINDXSECTIONSTART,StaticTextField.class);
    registerChild(CHILD_STINCLUDEPRIMEINDXSECTIONEND,StaticTextField.class);
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//
	}

	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoRowGeneratorModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);

    //--Release2.1--//
    //Populate all ComboBoxes manually here
    //Setup Options for cbPageNames
    //cbPageNamesOptions..populate(getRequestContext());
	int langId = handler.getTheSessionState().getLanguageId();
    cbPageNamesOptions.populate(getRequestContext(), langId);

    //Setup NoneSelected Label for cbPageNames
    CHILD_CBPAGENAMES_NONSELECTED_LABEL =
        BXResources.getGenericMsg("CBPAGENAMES_NONSELECTED_LABEL", handler.getTheSessionState().getLanguageId());
    //Check if the cbPageNames is already created
    if(getCbPageNames() != null)
      getCbPageNames().setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);

    //Populate PricingStatus options
    cbStatusOptions.populate(getRequestContext());

    //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
    cbBaseIndexOptions.populate(getRequestContext());
    //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

    handler.pageSaveState();

		super.beginDisplay(event);
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// The following code block was migrated from the this_onBeforeDataObjectExecuteEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.setupBeforePageGeneration();
		handler.pageSaveState();

    return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

		// The following code block was migrated from the this_onBeforeRowDisplayEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		handler.populatePageDisplayFields();
		handler.pageSaveState();
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	/**
	 *
	 *
	 */
	public TextField getTbDealId()
	{
		return (TextField)getChild(CHILD_TBDEALID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbPageNames()
	{
		return (ComboBox)getChild(CHILD_CBPAGENAMES);
	}

	/**
	 *
	 *
	 */
  //--DJ_Ticket661--start--//
  //Modified to sort by PageLabel based on the selected language
  static class CbPageNamesOptionList extends GotoOptionList
 {
    /**
     *
     *
     */
    CbPageNamesOptionList() {

    }


    public void populate(RequestContext rc) {
      Connection c = null;
      try {
        clear();
        SelectQueryModel m = null;

        SelectQueryExecutionContext eContext = new
          SelectQueryExecutionContext(
          (Connection)null,

          DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,

          DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

        if (rc == null) {
          m = new doPageNameLabelModelImpl();
          c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
          eContext.setConnection(c);
        }
        else {
          m = (SelectQueryModel)rc.getModelManager().getModel(doPageNameLabelModel.class);
        }

        m.retrieve(eContext);
        m.beforeFirst();

        // Sort the results from DataObject
        TreeMap sorted = new TreeMap();
        while (m.next()) {
          Object dfPageLabel =
            m.getValue(doPageNameLabelModel.FIELD_DFPAGELABEL);
          String label = (dfPageLabel == null ? "" : dfPageLabel.toString());
          Object dfPageId = m.getValue(doPageNameLabelModel.FIELD_DFPAGEID);
          String value = (dfPageId == null ? "" : dfPageId.toString());
          String[] theVal = new String[2];
          theVal[0] = label;
          theVal[1] = value;
          sorted.put(label, theVal);
        }

        // Set the sorted list to the optionlist
        Iterator theList = sorted.values().iterator();
        String[] theVal = new String[2];
        while (theList.hasNext()) {
          theVal = (String[]) (theList.next());
          add(theVal[0], theVal[1]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (c != null)
            c.close();
        }
        catch (SQLException ex) {
              // ignore
        }
      }
    }
 }
 //--DJ_Ticket661--end--//

	/**
	 *
	 *
	 */
	public void handleBtProceedRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btProceed_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleGoPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtProceed()
	{
		return (Button)getChild(CHILD_BTPROCEED);
	}


	/**
	 *
	 *
	 */
	public void handleHref1Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    // The following code block was migrated from the Href1_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(0);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref1()
	{
		return (HREF)getChild(CHILD_HREF1);
	}


	/**
	 *
	 *
	 */
	public void handleHref2Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href2_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(1);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref2()
	{
		return (HREF)getChild(CHILD_HREF2);
	}


	/**
	 *
	 *
	 */
	public void handleHref3Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href3_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(2);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref3()
	{
		return (HREF)getChild(CHILD_HREF3);
	}


	/**
	 *
	 *
	 */
	public void handleHref4Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    // The following code block was migrated from the Href4_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(3);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref4()
	{
		return (HREF)getChild(CHILD_HREF4);
	}


	/**
	 *
	 *
	 */
	public void handleHref5Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href5_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(4);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref5()
	{
		return (HREF)getChild(CHILD_HREF5);
	}


	/**
	 *
	 *
	 */
	public void handleHref6Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href6_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(5);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref6()
	{
		return (HREF)getChild(CHILD_HREF6);
	}


	/**
	 *
	 *
	 */
	public void handleHref7Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href7_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(6);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref7()
	{
		return (HREF)getChild(CHILD_HREF7);
	}


	/**
	 *
	 *
	 */
	public void handleHref8Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href8_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(7);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref8()
	{
		return (HREF)getChild(CHILD_HREF8);
	}


	/**
	 *
	 *
	 */
	public void handleHref9Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href9_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(8);
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref9()
	{
		return (HREF)getChild(CHILD_HREF9);
	}


	/**
	 *
	 *
	 */
	public void handleHref10Request(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the Href10_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOpenDialogLink(9);
    handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getHref10()
	{
		return (HREF)getChild(CHILD_HREF10);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTodayDate()
	{
		return (StaticTextField)getChild(CHILD_STTODAYDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStUserNameTitle()
	{
		return (StaticTextField)getChild(CHILD_STUSERNAMETITLE);
	}

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  /**
   *
   *
   */
  public TextField getTxTeaserRate()
  {
    return (TextField)getChild(CHILD_TXTEASERRATE);
  }

  /**
   *
   *
   */
  public TextField getTxBaseRate()
  {
    return (TextField)getChild(CHILD_TXBASERATE);
  }

  /**
   *
   *
   */
  public TextField getTxBaseAdjustment()
  {
    return (TextField)getChild(CHILD_TXBASEADJUSTMENT);
  }

  /**
   *
   *
   */
  public TextField getTxTeaserDiscount()
  {
    return (TextField)getChild(CHILD_TXTEASERDISCOUNT);
  }

  /**
   *
   *
   */
  public TextField getTxTeaserTerms()
  {
    return (TextField)getChild(CHILD_TXTEASERTERMS);
  }

  /**
   *
   *
   */
  public StaticTextField getStBaseIndxLastUpdatedTime()
  {
    return (StaticTextField)getChild(CHILD_STBASEINDXLASTUPDATEDTIME);
  }

  /**
   *
   *
   */
  public HiddenField getHdBaseIndexId()
  {
    return (HiddenField)getChild(CHILD_HDBASEINDEXID);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludePrimeIndxSectionStart()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDEPRIMEINDXSECTIONSTART);
  }

  /**
   *
   *
   */
  public StaticTextField getStIncludePrimeIndxSectionEnd()
  {
    return (StaticTextField)getChild(CHILD_STINCLUDEPRIMEINDXSECTIONEND);
  }

  /**
   *
   *
   */
  public void handleBtRecalcRateRequest(RequestInvocationEvent event)
    throws ServletException, IOException
  {
    // The following code block was migrated from the btOk_onWebEvent method
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handleRedisplayPrimeRate();
    handler.postHandlerProtocol();
  }

  /**
   *
   *
   */
  public Button getBtRecalcRate()
  {
    return (Button)getChild(CHILD_BTRECALCRATE_RESET_VALUE);
  }

  /**
   *
   *
   */
  public String endBtRecalcRateDisplay(ChildContentDisplayEvent event)
  {
    // The following code block was migrated from the btSubmit_onBeforeHtmlOutputEvent method
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
    boolean rc = handler.displayRecalcButton();
    handler.pageSaveState();

    if(rc == true)
      return event.getContent();
    else
      return "";
  }

  /**
   *
   *
   */
  public ComboBox getCbBaseIndex()
  {
    return (ComboBox)getChild(CHILD_CBBASEINDEX);
  }

  /**
   *
   *
   */
  static class CbBaseIndexOptionList extends OptionList
  {
    /**
     *
     *
     */
    CbBaseIndexOptionList()
    {
    }

    /**
     *
     *
     */
    public void populate(RequestContext rc)
    {
      try
      {
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"PRIMEINDEXRATEPROFILE", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

	/**
	 *
	 *
	 */
	public void handleBtSubmitRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    // The following code block was migrated from the btSubmit_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleOnSubmit();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtSubmit()
	{
		return (Button)getChild(CHILD_BTSUBMIT);
	}


	/**
	 *
	 *
	 */
	public String endBtSubmitDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btSubmit_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displaySubmitButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtWorkQueueLinkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btWorkQueueLink_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayWorkQueue();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtWorkQueueLink()
	{
		return (Button)getChild(CHILD_BTWORKQUEUELINK);
	}


	/**
	 *
	 *
	 */
	public void handleChangePasswordHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the changePasswordHref_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleChangePassword();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public HREF getChangePasswordHref()
	{
		return (HREF)getChild(CHILD_CHANGEPASSWORDHREF);
	}

  //--Release2.1--start//
  //// Two new methods providing the business logic for the new link to toggle language.
  //// When touched this link should reload the page in opposite language (french versus english)
  //// and set this new session value.
	/**
	 *
	 *
	 */
	public void handleToggleLanguageHrefRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this, true);

		handler.handleToggleLanguage();

		handler.postHandlerProtocol();
	}

	/**
	 *
	 *
	 */
	public HREF getToggleLanguageHref()
	{
		return (HREF)getChild(CHILD_TOGGLELANGUAGEHREF);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public void handleBtToolHistoryRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    // The following code block was migrated from the btToolHistory_onWebEvent method
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayDealHistory();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolHistory()
	{
		return (Button)getChild(CHILD_BTTOOLHISTORY);
	}


	/**
	 *
	 *
	 */
	public void handleBtTooNotesRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btTooNotes_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDisplayDealNotes();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtTooNotes()
	{
		return (Button)getChild(CHILD_BTTOONOTES);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolSearchRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btToolSearch_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleDealSearch();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolSearch()
	{
		return (Button)getChild(CHILD_BTTOOLSEARCH);
	}


	/**
	 *
	 *
	 */
	public void handleBtToolLogRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    // The following code block was migrated from the btToolLog_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleSignOff();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtToolLog()
	{
		return (Button)getChild(CHILD_BTTOOLLOG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStErrorFlag()
	{
		return (StaticTextField)getChild(CHILD_STERRORFLAG);
	}


	/**
	 *
	 *
	 */
	public HiddenField getDetectAlertTasks()
	{
		return (HiddenField)getChild(CHILD_DETECTALERTTASKS);
	}


	/**
	 *
	 *
	 */
	public void handleBtCancelRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btCancel_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleCancelStandard();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtCancel()
	{
		return (Button)getChild(CHILD_BTCANCEL);
	}


	/**
	 *
	 *
	 */
	public String endBtCancelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btCancel_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayCancelButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtPrevTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
    // The following code block was migrated from the btPrevTaskPage_onWebEvent method
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.preHandlerProtocol(this);
    handler.handlePrevTaskPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtPrevTaskPage()
	{
		return (Button)getChild(CHILD_BTPREVTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtPrevTaskPageDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btPrevTaskPage_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generatePrevTaskPage();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPrevTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STPREVTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStPrevTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stPrevTaskPageLabel_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generatePrevTaskPage();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public void handleBtNextTaskPageRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btNextTaskPage_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleNextTaskPage();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtNextTaskPage()
	{
		return (Button)getChild(CHILD_BTNEXTTASKPAGE);
	}


	/**
	 *
	 *
	 */
	public String endBtNextTaskPageDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btNextTaskPage_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateNextTaskPage();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStNextTaskPageLabel()
	{
		return (StaticTextField)getChild(CHILD_STNEXTTASKPAGELABEL);
	}


	/**
	 *
	 *
	 */
	public String endStNextTaskPageLabelDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stNextTaskPageLabel_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateNextTaskPage();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStTaskName()
	{
		return (StaticTextField)getChild(CHILD_STTASKNAME);
	}


	/**
	 *
	 *
	 */
	public String endStTaskNameDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stTaskName_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.generateTaskName();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public HiddenField getSessionUserId()
	{
		return (HiddenField)getChild(CHILD_SESSIONUSERID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStCode()
	{
		return (StaticTextField)getChild(CHILD_STCODE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStDescription()
	{
		return (StaticTextField)getChild(CHILD_STDESCRIPTION);
	}


  /**
   *
   *
   */
  public ComboBox getCbStatus()
  {
    return (ComboBox)getChild(CHILD_CBSTATUS);
  }

	/**
	 *
	 *
	 */
	static class CbStatusOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbStatusOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
    //--> By Billy 27Nov2002
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(
                theSessionState.getDealInstitutionId(),"PRICINGSTATUS", languageId);

        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public ComboBox getCbMonth()
	{
		return (ComboBox)getChild(CHILD_CBMONTH);
	}


	/**
	 *
	 *
	 */
	public TextField getTbDay()
	{
		return (TextField)getChild(CHILD_TBDAY);
	}


	/**
	 *
	 *
	 */
	public TextField getTbYear()
	{
		return (TextField)getChild(CHILD_TBYEAR);
	}


	/**
	 *
	 *
	 */
	public TextField getTbHour()
	{
		return (TextField)getChild(CHILD_TBHOUR);
	}


	/**
	 *
	 *
	 */
	public TextField getTbMinute()
	{
		return (TextField)getChild(CHILD_TBMINUTE);
	}


	/**
	 *
	 *
	 */
	public TextField getTbPostedRate()
	{
		return (TextField)getChild(CHILD_TBPOSTEDRATE);
	}


	/**
	 *
	 *
	 */
	public TextField getTbMaxDiscountRate()
	{
		return (TextField)getChild(CHILD_TBMAXDISCOUNTRATE);
	}


	/**
	 *
	 *
	 */
	public TextField getTbBestRate()
	{
		return (TextField)getChild(CHILD_TBBESTRATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STPMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STPMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STPMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmHasOk()
	{
		return (StaticTextField)getChild(CHILD_STPMHASOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmTitle()
	{
		return (StaticTextField)getChild(CHILD_STPMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STPMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmOnOk()
	{
		return (StaticTextField)getChild(CHILD_STPMONOK);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STPMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmGenerate()
	{
		return (StaticTextField)getChild(CHILD_STAMGENERATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasInfo()
	{
		return (StaticTextField)getChild(CHILD_STAMHASINFO);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmHasTable()
	{
		return (StaticTextField)getChild(CHILD_STAMHASTABLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmTitle()
	{
		return (StaticTextField)getChild(CHILD_STAMTITLE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmInfoMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMINFOMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgs()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmMsgTypes()
	{
		return (StaticTextField)getChild(CHILD_STAMMSGTYPES);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmDialogMsg()
	{
		return (StaticTextField)getChild(CHILD_STAMDIALOGMSG);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStAmButtonsHtml()
	{
		return (StaticTextField)getChild(CHILD_STAMBUTTONSHTML);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdDummy()
	{
		return (HiddenField)getChild(CHILD_HDDUMMY);
	}


	/**
	 *
	 *
	 */
	public void handleBtOkRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btOk_onWebEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleCancelStandard();
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtOk()
	{
		return (Button)getChild(CHILD_BTOK);
	}


	/**
	 *
	 *
	 */
	public String endBtOkDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btOk_onBeforeHtmlOutputEvent method
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.pageGetState(this);
		boolean rc = handler.displayOKButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStViewOnlyTag()
	{
		return (StaticTextField)getChild(CHILD_STVIEWONLYTAG);
	}


	/**
	 *
	 *
	 */
	public doRowGeneratorModel getdoRowGeneratorModel()
	{
		if (doRowGenerator == null)
			doRowGenerator = (doRowGeneratorModel) getModel(doRowGeneratorModel.class);
		return doRowGenerator;
	}


	/**
	 *
	 *
	 */
	public void setdoRowGeneratorModel(doRowGeneratorModel model)
	{
			doRowGenerator = model;
	}


  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public Button getBtActMsg()
	{
		return (Button)getChild(CHILD_BTACTMSG);
	}
  //===========================================

  //--> Addition methods to propulate Href display String
  //--> Test by BILLY 07Aug2002
  public String endHref1Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 0);
		handler.pageSaveState();

    return rc;
  }
  public String endHref2Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 1);
		handler.pageSaveState();

    return rc;
  }
  public String endHref3Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 2);
		handler.pageSaveState();

    return rc;
  }
  public String endHref4Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 3);
		handler.pageSaveState();

    return rc;
  }
  public String endHref5Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 4);
		handler.pageSaveState();

    return rc;
  }
  public String endHref6Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 5);
		handler.pageSaveState();

    return rc;
  }
  public String endHref7Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 6);
		handler.pageSaveState();

    return rc;
  }
  public String endHref8Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 7);
		handler.pageSaveState();

    return rc;
  }
  public String endHref9Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 8);
		handler.pageSaveState();

    return rc;
  }
  public String endHref10Display(ChildContentDisplayEvent event)
	{
    RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
    handler.pageGetState(this);
		String rc = handler.populatePreviousPagesLinksDisplayString(event.getContent(), 9);
		handler.pageSaveState();

    return rc;
  }
  //=====================================================

  //// This method overrides the getDefaultURL() framework JATO method and it is located
  //// in each ViewBean. This allows not to stay with the JATO ViewBean extension,
  //// otherwise each ViewBean a.k.a page should extend its Handler (to be called by the framework)
  //// and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
  //// The full method is still in PHC base class. It should care the
  //// the url="/" case (non-initialized defaultURL) by the BX framework methods.
  public String getDisplayURL()
  {
       RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
       handler.pageGetState(this);

       String url=getDefaultDisplayURL();

       int languageId = handler.theSessionState.getLanguageId();

       //// Call the language specific URL (business delegation done in the BXResource).
       if(url != null && !url.trim().equals("") && !url.trim().equals("/")) {
          url = BXResources.getBXUrl(url, languageId);
       }
       else {
          url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
       }

      return url;
  }

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////



	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	public void handleActMessageOK(String[] args)
	{
		RateAdminHandler handler =(RateAdminHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this);
		handler.handleActMessageOK();
		handler.handleOnDialogButton();
		handler.postHandlerProtocol();
	}




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String PAGE_NAME="pgInterestRateUpdate";
  //// It is a variable now (see explanation in the getDisplayURL() method above.
	////public static final String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgInterestRateUpdate.jsp";
	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_TBDEALID="tbDealId";
	public static final String CHILD_TBDEALID_RESET_VALUE="";
	public static final String CHILD_CBPAGENAMES="cbPageNames";
	public static final String CHILD_CBPAGENAMES_RESET_VALUE="";
	//--Release2.1--//
	//private static CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  private CbPageNamesOptionList cbPageNamesOptions=new CbPageNamesOptionList();
  //Added the Variable for NonSelected Label
  private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";
  //==================================================================
	public static final String CHILD_BTPROCEED="btProceed";
	public static final String CHILD_BTPROCEED_RESET_VALUE="Proceed";
	public static final String CHILD_HREF1="Href1";
	public static final String CHILD_HREF1_RESET_VALUE="";
	public static final String CHILD_HREF2="Href2";
	public static final String CHILD_HREF2_RESET_VALUE="";
	public static final String CHILD_HREF3="Href3";
	public static final String CHILD_HREF3_RESET_VALUE="";
	public static final String CHILD_HREF4="Href4";
	public static final String CHILD_HREF4_RESET_VALUE="";
	public static final String CHILD_HREF5="Href5";
	public static final String CHILD_HREF5_RESET_VALUE="";
	public static final String CHILD_HREF6="Href6";
	public static final String CHILD_HREF6_RESET_VALUE="";
	public static final String CHILD_HREF7="Href7";
	public static final String CHILD_HREF7_RESET_VALUE="";
	public static final String CHILD_HREF8="Href8";
	public static final String CHILD_HREF8_RESET_VALUE="";
	public static final String CHILD_HREF9="Href9";
	public static final String CHILD_HREF9_RESET_VALUE="";
	public static final String CHILD_HREF10="Href10";
	public static final String CHILD_HREF10_RESET_VALUE="";
	public static final String CHILD_STPAGELABEL="stPageLabel";
	public static final String CHILD_STPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STCOMPANYNAME="stCompanyName";
	public static final String CHILD_STCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STTODAYDATE="stTodayDate";
	public static final String CHILD_STTODAYDATE_RESET_VALUE="";
	public static final String CHILD_STUSERNAMETITLE="stUserNameTitle";
	public static final String CHILD_STUSERNAMETITLE_RESET_VALUE="";
	public static final String CHILD_BTSUBMIT="btSubmit";
	public static final String CHILD_BTSUBMIT_RESET_VALUE=" ";
	public static final String CHILD_BTWORKQUEUELINK="btWorkQueueLink";
	public static final String CHILD_BTWORKQUEUELINK_RESET_VALUE=" ";
	public static final String CHILD_CHANGEPASSWORDHREF="changePasswordHref";
	public static final String CHILD_CHANGEPASSWORDHREF_RESET_VALUE="";
	public static final String CHILD_BTTOOLHISTORY="btToolHistory";
	public static final String CHILD_BTTOOLHISTORY_RESET_VALUE=" ";
	public static final String CHILD_BTTOONOTES="btTooNotes";
	public static final String CHILD_BTTOONOTES_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLSEARCH="btToolSearch";
	public static final String CHILD_BTTOOLSEARCH_RESET_VALUE=" ";
	public static final String CHILD_BTTOOLLOG="btToolLog";
	public static final String CHILD_BTTOOLLOG_RESET_VALUE=" ";
	public static final String CHILD_STERRORFLAG="stErrorFlag";
	public static final String CHILD_STERRORFLAG_RESET_VALUE="N";
	public static final String CHILD_DETECTALERTTASKS="detectAlertTasks";
	public static final String CHILD_DETECTALERTTASKS_RESET_VALUE="";
	public static final String CHILD_BTCANCEL="btCancel";
	public static final String CHILD_BTCANCEL_RESET_VALUE=" ";
	public static final String CHILD_BTPREVTASKPAGE="btPrevTaskPage";
	public static final String CHILD_BTPREVTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STPREVTASKPAGELABEL="stPrevTaskPageLabel";
	public static final String CHILD_STPREVTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_BTNEXTTASKPAGE="btNextTaskPage";
	public static final String CHILD_BTNEXTTASKPAGE_RESET_VALUE=" ";
	public static final String CHILD_STNEXTTASKPAGELABEL="stNextTaskPageLabel";
	public static final String CHILD_STNEXTTASKPAGELABEL_RESET_VALUE="";
	public static final String CHILD_STTASKNAME="stTaskName";
	public static final String CHILD_STTASKNAME_RESET_VALUE="";
	public static final String CHILD_SESSIONUSERID="sessionUserId";
	public static final String CHILD_SESSIONUSERID_RESET_VALUE="";
	public static final String CHILD_STCODE="stCode";
	public static final String CHILD_STCODE_RESET_VALUE="";
	public static final String CHILD_STDESCRIPTION="stDescription";
	public static final String CHILD_STDESCRIPTION_RESET_VALUE="";
	public static final String CHILD_CBSTATUS="cbStatus";
	public static final String CHILD_CBSTATUS_RESET_VALUE="";
  //--Release2.1--//
	//private static CbStatusOptionList cbStatusOptions=new CbStatusOptionList();
  private CbStatusOptionList cbStatusOptions=new CbStatusOptionList();
	public static final String CHILD_CBMONTH="cbMonth";
	public static final String CHILD_CBMONTH_RESET_VALUE="";
	private OptionList cbMonthOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_TBDAY="tbDay";
	public static final String CHILD_TBDAY_RESET_VALUE="";
	public static final String CHILD_TBYEAR="tbYear";
	public static final String CHILD_TBYEAR_RESET_VALUE="";
	public static final String CHILD_TBHOUR="tbHour";
	public static final String CHILD_TBHOUR_RESET_VALUE="";
	public static final String CHILD_TBMINUTE="tbMinute";
	public static final String CHILD_TBMINUTE_RESET_VALUE="";
	public static final String CHILD_TBPOSTEDRATE="tbPostedRate";
	public static final String CHILD_TBPOSTEDRATE_RESET_VALUE="";
	public static final String CHILD_TBMAXDISCOUNTRATE="tbMaxDiscountRate";
	public static final String CHILD_TBMAXDISCOUNTRATE_RESET_VALUE="";
	public static final String CHILD_TBBESTRATE="tbBestRate";
	public static final String CHILD_TBBESTRATE_RESET_VALUE="";
	public static final String CHILD_STPMGENERATE="stPmGenerate";
	public static final String CHILD_STPMGENERATE_RESET_VALUE="";
	public static final String CHILD_STPMHASTITLE="stPmHasTitle";
	public static final String CHILD_STPMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STPMHASINFO="stPmHasInfo";
	public static final String CHILD_STPMHASINFO_RESET_VALUE="";
	public static final String CHILD_STPMHASTABLE="stPmHasTable";
	public static final String CHILD_STPMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STPMHASOK="stPmHasOk";
	public static final String CHILD_STPMHASOK_RESET_VALUE="";
	public static final String CHILD_STPMTITLE="stPmTitle";
	public static final String CHILD_STPMTITLE_RESET_VALUE="";
	public static final String CHILD_STPMINFOMSG="stPmInfoMsg";
	public static final String CHILD_STPMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STPMONOK="stPmOnOk";
	public static final String CHILD_STPMONOK_RESET_VALUE="";
	public static final String CHILD_STPMMSGS="stPmMsgs";
	public static final String CHILD_STPMMSGS_RESET_VALUE="";
	public static final String CHILD_STPMMSGTYPES="stPmMsgTypes";
	public static final String CHILD_STPMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMGENERATE="stAmGenerate";
	public static final String CHILD_STAMGENERATE_RESET_VALUE="";
	public static final String CHILD_STAMHASTITLE="stAmHasTitle";
	public static final String CHILD_STAMHASTITLE_RESET_VALUE="";
	public static final String CHILD_STAMHASINFO="stAmHasInfo";
	public static final String CHILD_STAMHASINFO_RESET_VALUE="";
	public static final String CHILD_STAMHASTABLE="stAmHasTable";
	public static final String CHILD_STAMHASTABLE_RESET_VALUE="";
	public static final String CHILD_STAMTITLE="stAmTitle";
	public static final String CHILD_STAMTITLE_RESET_VALUE="";
	public static final String CHILD_STAMINFOMSG="stAmInfoMsg";
	public static final String CHILD_STAMINFOMSG_RESET_VALUE="";
	public static final String CHILD_STAMMSGS="stAmMsgs";
	public static final String CHILD_STAMMSGS_RESET_VALUE="";
	public static final String CHILD_STAMMSGTYPES="stAmMsgTypes";
	public static final String CHILD_STAMMSGTYPES_RESET_VALUE="";
	public static final String CHILD_STAMDIALOGMSG="stAmDialogMsg";
	public static final String CHILD_STAMDIALOGMSG_RESET_VALUE="";
	public static final String CHILD_STAMBUTTONSHTML="stAmButtonsHtml";
	public static final String CHILD_STAMBUTTONSHTML_RESET_VALUE="";
	public static final String CHILD_HDDUMMY="hdDummy";
	public static final String CHILD_HDDUMMY_RESET_VALUE="";
	public static final String CHILD_BTOK="btOk";
	public static final String CHILD_BTOK_RESET_VALUE=" ";
	public static final String CHILD_STVIEWONLYTAG="stViewOnlyTag";
	public static final String CHILD_STVIEWONLYTAG_RESET_VALUE="";
  //--> Hidden ActMessageButton (FINDTHISLATER)
  //--> Test by BILLY 15July2002
  public static final String CHILD_BTACTMSG="btActMsg";
	public static final String CHILD_BTACTMSG_RESET_VALUE="ActMsg";
  //--Release2.1--//
  //// New link to toggle the language. When touched this link should reload the
  //// page in opposite language (french versus english) and set this new language
  //// session id throughout all modulus.
	public static final String CHILD_TOGGLELANGUAGEHREF="toggleLanguageHref";
	public static final String CHILD_TOGGLELANGUAGEHREF_RESET_VALUE="";

  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  public static final String CHILD_CBBASEINDEX="cbBaseIndex";
  public static final String CHILD_CBBASEINDEX_RESET_VALUE="";
  private CbBaseIndexOptionList cbBaseIndexOptions=new CbBaseIndexOptionList();

  public static final String CHILD_TXTEASERRATE="txTeaserRate";
  public static final String CHILD_TXTEASERRATE_RESET_VALUE="";

  public static final String CHILD_TXBASERATE="txBaseRate";
  public static final String CHILD_TXBASERATE_RESET_VALUE="";

  public static final String CHILD_TXBASEADJUSTMENT="txBaseAdjustment";
  public static final String CHILD_TXBASEADJUSTMENT_RESET_VALUE="";

  public static final String CHILD_TXTEASERDISCOUNT="txTeaserDiscount";
  public static final String CHILD_TXTEASERDISCOUNT_RESET_VALUE="";

  public static final String CHILD_TXTEASERTERMS="txTeaserTerms";
  public static final String CHILD_TXTEASERTERMS_RESET_VALUE="";

  public static final String CHILD_HDBASEINDEXID="hdBaseIndexId";
  public static final String CHILD_HDBASEINDEXID_RESET_VALUE="";

  public static final String CHILD_STBASEINDXLASTUPDATEDTIME="stBaseIndxLastUpdateTime";
  public static final String CHILD_STBASEINDXLASTUPDATEDTIME_RESET_VALUE="";

  public static final String CHILD_BTRECALCRATE="btRecalcRate";
  public static final String CHILD_BTRECALCRATE_RESET_VALUE="";

  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONSTART="stIncludePrimeIndxSectionStart";
  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONSTART_RESET_VALUE="";

  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONEND="stIncludePrimeIndxSectionEnd";
  public static final String CHILD_STINCLUDEPRIMEINDXSECTIONEND_RESET_VALUE="";
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doRowGeneratorModel doRowGenerator=null;
	protected String DEFAULT_DISPLAY_URL="/mosApp/MosSystem/pgInterestRateUpdate.jsp";


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	// MigrationToDo : Migrate custom member
	private RateAdminHandler handler=new RateAdminHandler();

}

