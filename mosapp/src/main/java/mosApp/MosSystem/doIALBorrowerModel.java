package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doIALBorrowerModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerMiddleName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerMiddleName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerLastName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerPrimaryFlag();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerPrimaryFlag(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerNetworth();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerNetworth(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfBorrowerType();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerType(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFBORROWERFIRSTNAME="dfBorrowerFirstName";
	public static final String FIELD_DFBORROWERMIDDLENAME="dfBorrowerMiddleName";
	public static final String FIELD_DFBORROWERLASTNAME="dfBorrowerLastName";
	public static final String FIELD_DFBORROWERPRIMARYFLAG="dfBorrowerPrimaryFlag";
	public static final String FIELD_DFBORROWERTDS="dfBorrowerTDS";
	public static final String FIELD_DFBORROWERGDS="dfBorrowerGDS";
	public static final String FIELD_DFBORROWERNETWORTH="dfBorrowerNetworth";
	public static final String FIELD_DFBORROWERTYPE="dfBorrowerType";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

