package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
//--DJ_CR134--19Oct2004--//
public interface doDealNotesBrokerInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDealNotesDate();


	/**
	 *
	 *
	 */
	public void setDfDealNotesDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfDealNotesCategory();


	/**
	 *
	 *
	 */
	public void setDfDealNotesCategory(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesID();


	/**
	 *
	 *
	 */
	public void setDfDealNotesID(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfDealNotesCategoryID();


	/**
	 *
	 *
	 */
	public void setDfDealNotesCategoryID(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealID();


	/**
	 *
	 *
	 */
	public void setDfDealID(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealNotesUserID();


	/**
	 *
	 *
	 */
	public void setDfDealNotesUserID(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Clob getDfDealNotesText();


	/**
	 *
	 *
	 */
	public void setDfDealNotesText(java.sql.Clob value);


	/**
	 *
	 *
	 */
	public String getDfUserName();


	/**
	 *
	 *
	 */
	public void setDfUserName(String value);




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALNOTESDATE="dfDealNotesDate";
	public static final String FIELD_DFDEALNOTESCATEGORY="dfDealNotesCategory";
	public static final String FIELD_DFDEALNOTESID="dfDealNotesID";
	public static final String FIELD_DFDEALNOTESCATEGORYID="dfDealNotesCategoryID";
	public static final String FIELD_DFDEALID="dfDealID";
	public static final String FIELD_DFDEALNOTESUSERID="dfDealNotesUserID";
	public static final String FIELD_DFDEALNOTESTEXT="dfDealNotesText";
	public static final String FIELD_DFUSERNAME="dfUserName";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

