package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doBridgeSalePropertyDetailsModelImpl extends QueryModelBase
	implements doBridgeSalePropertyDetailsModel
{
	/**
	 *
	 *
	 */
	public doBridgeSalePropertyDetailsModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPrimaryBorrowerFlag()
	{
		return (String)getValue(FIELD_DFPRIMARYBORROWERFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value)
	{
		setValue(FIELD_DFPRIMARYBORROWERFLAG,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerAddressId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERADDRESSID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerAddressId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERADDRESSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerTypeDesc()
	{
		return (String)getValue(FIELD_DFBORROWERTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerTypeDesc(String value)
	{
		setValue(FIELD_DFBORROWERTYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerAddressTypeDesc()
	{
		return (String)getValue(FIELD_DFBORROWERADDRESSTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerAddressTypeDesc(String value)
	{
		setValue(FIELD_DFBORROWERADDRESSTYPEDESC,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine1()
	{
		return (String)getValue(FIELD_DFADDRESSLINE1);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine1(String value)
	{
		setValue(FIELD_DFADDRESSLINE1,value);
	}


	/**
	 *
	 *
	 */
	public String getDfAddressLine2()
	{
		return (String)getValue(FIELD_DFADDRESSLINE2);
	}


	/**
	 *
	 *
	 */
	public void setDfAddressLine2(String value)
	{
		setValue(FIELD_DFADDRESSLINE2,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalCodeFSA()
	{
		return (String)getValue(FIELD_DFPOSTALCODEFSA);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalCodeFSA(String value)
	{
		setValue(FIELD_DFPOSTALCODEFSA,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCity()
	{
		return (String)getValue(FIELD_DFCITY);
	}


	/**
	 *
	 *
	 */
	public void setDfCity(String value)
	{
		setValue(FIELD_DFCITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProvinceName()
	{
		return (String)getValue(FIELD_DFPROVINCENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfProvinceName(String value)
	{
		setValue(FIELD_DFPROVINCENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPostalCodeLDU()
	{
		return (String)getValue(FIELD_DFPOSTALCODELDU);
	}


	/**
	 *
	 *
	 */
	public void setDfPostalCodeLDU(String value)
	{
		setValue(FIELD_DFPOSTALCODELDU,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BORROWER.BORROWERID, BORROWER.PRIMARYBORROWERFLAG, BORROWERADDRESS.BORROWERADDRESSID, BORROWERTYPE.BTDESCRIPTION, BORROWERADDRESSTYPE.BATDESCRIPTION, ADDR.ADDRESSLINE1, ADDR.ADDRESSLINE2, ADDR.POSTALFSA, ADDR.CITY, DEAL.DEALID, DEAL.COPYID, PROVINCE.PROVINCENAME, ADDR.POSTALLDU FROM BORROWER, BORROWERADDRESS, BORROWERTYPE, BORROWERADDRESSTYPE, ADDR, DEAL, PROVINCE  __WHERE__  GROUP BY DEAL.DEALID, DEAL.COPYID, BORROWER.BORROWERID, BORROWERADDRESS.BORROWERADDRESSID, BORROWER.PRIMARYBORROWERFLAG, BORROWERADDRESSTYPE.BATDESCRIPTION, BORROWERTYPE.BTDESCRIPTION, PROVINCE.PROVINCENAME, ADDR.CITY, ADDR.ADDRESSLINE2, ADDR.ADDRESSLINE1, ADDR.POSTALFSA, ADDR.POSTALLDU";
	public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER, BORROWERADDRESS, BORROWERTYPE, BORROWERADDRESSTYPE, ADDR, DEAL, PROVINCE";
	public static final String STATIC_WHERE_CRITERIA=" (ADDR.PROVINCEID  =  PROVINCE.PROVINCEID) AND (BORROWER.COPYID  =  ADDR.COPYID) AND (DEAL.COPYID  =  ADDR.COPYID) AND (BORROWERADDRESS.ADDRID  =  ADDR.ADDRID) AND (BORROWERTYPE.BORROWERTYPEID  =  BORROWER.BORROWERTYPEID) AND (BORROWERADDRESS.BORROWERADDRESSTYPEID  =  BORROWERADDRESSTYPE.BORROWERADDRESSTYPEID) AND (BORROWER.BORROWERID  =  BORROWERADDRESS.BORROWERID) AND (DEAL.COPYID  =  BORROWER.COPYID) AND (DEAL.DEALID  =  BORROWER.DEALID) ";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG="BORROWER.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFPRIMARYBORROWERFLAG="PRIMARYBORROWERFLAG";
	public static final String QUALIFIED_COLUMN_DFBORROWERADDRESSID="BORROWERADDRESS.BORROWERADDRESSID";
	public static final String COLUMN_DFBORROWERADDRESSID="BORROWERADDRESSID";
	public static final String QUALIFIED_COLUMN_DFBORROWERTYPEDESC="BORROWERTYPE.BTDESCRIPTION";
	public static final String COLUMN_DFBORROWERTYPEDESC="BTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFBORROWERADDRESSTYPEDESC="BORROWERADDRESSTYPE.BATDESCRIPTION";
	public static final String COLUMN_DFBORROWERADDRESSTYPEDESC="BATDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE1="ADDR.ADDRESSLINE1";
	public static final String COLUMN_DFADDRESSLINE1="ADDRESSLINE1";
	public static final String QUALIFIED_COLUMN_DFADDRESSLINE2="ADDR.ADDRESSLINE2";
	public static final String COLUMN_DFADDRESSLINE2="ADDRESSLINE2";
	public static final String QUALIFIED_COLUMN_DFPOSTALCODEFSA="ADDR.POSTALFSA";
	public static final String COLUMN_DFPOSTALCODEFSA="POSTALFSA";
	public static final String QUALIFIED_COLUMN_DFCITY="ADDR.CITY";
	public static final String COLUMN_DFCITY="CITY";
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFPROVINCENAME="PROVINCE.PROVINCENAME";
	public static final String COLUMN_DFPROVINCENAME="PROVINCENAME";
	public static final String QUALIFIED_COLUMN_DFPOSTALCODELDU="ADDR.POSTALLDU";
	public static final String COLUMN_DFPOSTALCODELDU="POSTALLDU";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRIMARYBORROWERFLAG,
				COLUMN_DFPRIMARYBORROWERFLAG,
				QUALIFIED_COLUMN_DFPRIMARYBORROWERFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERADDRESSID,
				COLUMN_DFBORROWERADDRESSID,
				QUALIFIED_COLUMN_DFBORROWERADDRESSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERTYPEDESC,
				COLUMN_DFBORROWERTYPEDESC,
				QUALIFIED_COLUMN_DFBORROWERTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERADDRESSTYPEDESC,
				COLUMN_DFBORROWERADDRESSTYPEDESC,
				QUALIFIED_COLUMN_DFBORROWERADDRESSTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE1,
				COLUMN_DFADDRESSLINE1,
				QUALIFIED_COLUMN_DFADDRESSLINE1,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFADDRESSLINE2,
				COLUMN_DFADDRESSLINE2,
				QUALIFIED_COLUMN_DFADDRESSLINE2,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALCODEFSA,
				COLUMN_DFPOSTALCODEFSA,
				QUALIFIED_COLUMN_DFPOSTALCODEFSA,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCITY,
				COLUMN_DFCITY,
				QUALIFIED_COLUMN_DFCITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPROVINCENAME,
				COLUMN_DFPROVINCENAME,
				QUALIFIED_COLUMN_DFPROVINCENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTALCODELDU,
				COLUMN_DFPOSTALCODELDU,
				QUALIFIED_COLUMN_DFPOSTALCODELDU,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

