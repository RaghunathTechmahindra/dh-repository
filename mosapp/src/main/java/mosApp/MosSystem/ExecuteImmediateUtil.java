package mosApp.MosSystem;

import java.sql.*;
import java.util.*;
import java.io.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 */
////////////////////////
/***

		public void populate(RequestContext rc)
		{
			Connection c = null;
			try {
				clear();
				SelectQueryModel m = null;

				SelectQueryExecutionContext eContext=new SelectQueryExecutionContext(
					(Connection)null,
					DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
					DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);

				if(rc == null) {
					m = new doTaskStatusModelImpl();
					c = SQLConnectionManagerImpl.obtainConnection(m.getDataSourceName());
					eContext.setConnection(c);
				}
				else {
					m = (SelectQueryModel) rc.getModelManager().getModel(doTaskStatusModel.class);
				}

				m.retrieve(eContext);
				m.beforeFirst();

				while (m.next()) {
					Object MOS_TASKSTATUS_TSDESCRIPTION = m.getValue(doTaskStatusModel.FIELD_MOS_TASKSTATUS_TSDESCRIPTION);
					Object MOS_TASKSTATUS_TASKSTATUSID = m.getValue(doTaskStatusModel.FIELD_MOS_TASKSTATUS_TASKSTATUSID);

					String label = (MOS_TASKSTATUS_TSDESCRIPTION == null?"":MOS_TASKSTATUS_TSDESCRIPTION.toString());

					String value = (MOS_TASKSTATUS_TASKSTATUSID == null?"":MOS_TASKSTATUS_TASKSTATUSID.toString());

					add(label, value);
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null)
						c.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}

	}
***/

////////////////////////

//// Temporarily defined this Utility class abstact to setResultSet
public class ExecuteImmediateUtil
{

	// Migration Helper method to handle executeImmediate()
	public static ResultSet executeImmediateSelect(String dataSourceName,
                                                       String sql,
                                                       com.iplanet.jato.RequestContext rc)
		throws SQLException
	{

 		Statement statement=null;
		Connection connection=null;

		try
		{

			connection = rc.getSQLConnectionManager().getConnection(dataSourceName, "mos", "mos");

			try
			{
				// TODO: Make these constants members that can be
				// changed by developers

				//// Original gives the Scrollable exception (check the driver!!!!).
                                ////        statement=connection.createStatement(
 				////	ResultSet.TYPE_SCROLL_INSENSITIVE,
				////	ResultSet.CONCUR_READ_ONLY);

                                //// Durty connection to check the driver.
				 statement=connection.createStatement();

			}
			catch (UnsupportedOperationException e)
			{
				// If this fails for some reason, we take he default
				// statement type
				statement=connection.createStatement();
			}

			// Execute the query
			ResultSet results=statement.executeQuery(sql);
                        ////setResultSet(results);

			return results;

		}
		finally
		{
			if (statement!=null)
				statement.close();


			if (connection!=null)
				connection.close();
		}
	}


	// Migration Helper method to handle executeImmediate()
	public static int executeImmediateUpdate(String dataSourceName,
											 String sql,
											 com.iplanet.jato.RequestContext rc )
	throws SQLException
	{

 		Statement statement=null;
		Connection connection=null;

		try
		{

			connection = rc.getSQLConnectionManager().getConnection("dataSourceName");
			////connection = rc.getSQLConnectionManager().obtainConnection("dataSourceName");
			statement=connection.createStatement();

			// Execute the update/delete/insert (all are update to JDBC)
			int numAffectedRows = statement.executeUpdate(sql);
			return numAffectedRows;

		}
		finally
		{
			if (statement!=null)
				statement.close();


			if (connection!=null)
				connection.close();
		}
	}
}

