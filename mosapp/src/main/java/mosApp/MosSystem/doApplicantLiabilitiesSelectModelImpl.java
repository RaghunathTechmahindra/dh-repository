package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doApplicantLiabilitiesSelectModelImpl extends QueryModelBase
	implements doApplicantLiabilitiesSelectModel
{
	/**
	 *
	 *
	 */
	public doApplicantLiabilitiesSelectModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLiabilityDesc()
	{
		return (String)getValue(FIELD_DFLIABILITYDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityDesc(String value)
	{
		setValue(FIELD_DFLIABILITYDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityMonthlyPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYMONTHLYPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityMonthlyPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYMONTHLYPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLiabilityPayOfTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLIABILITYPAYOFTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfLiabilityPayOfTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLIABILITYPAYOFTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludeInGDS()
	{
		return (String)getValue(FIELD_DFINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludeInGDS(String value)
	{
		setValue(FIELD_DFINCLUDEINGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINGDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludeInTDS()
	{
		return (String)getValue(FIELD_DFINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludeInTDS(String value)
	{
		setValue(FIELD_DFINCLUDEINTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTINCLUDEDINTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTINCLUDEDINTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPercentOutGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPERCENTOUTGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfPercentOutGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPERCENTOUTGDS,value);
	}

    /**
     * This method declaration returns the value of field the CreditBureauLiabilityLimit.
     * 
     * @return dfCreditBureauLiabilityLimit
     */
    public java.math.BigDecimal getDfCreditBureauLiabilityLimit() {
        return (java.math.BigDecimal) getValue(FIELD_DFCREDITBUREAULIABILITYLIMIT);
    }

    /**
     * This method declaration sets the CreditBureauLiabilityLimit.
     * 
     * @param value: the new CreditBureauLiabilityLimit
     */
    public void setDfCreditBureauLiabilityLimit(java.math.BigDecimal value) {
        setValue(FIELD_DFCREDITBUREAULIABILITYLIMIT, value);
    }

    /**
     * This method declaration returns the value of field the CreditBureauIndicator.
     * 
     * @return dfCreditBureauIndicator
     */
    public String getDfCreditBureauIndicator() {
        return (String) getValue(FIELD_DFCREDITBUREAUINDICATOR);
    }

    /**
     * This method declaration sets the CreditBureauIndicator.
     * 
     * @param value: the new CreditBureauIndicator
     */
    public void setDfCreditBureauIndicator(String value) {
        setValue(FIELD_DFCREDITBUREAUINDICATOR, value);
    }

    /**
     * This method declaration returns the value of field the MaturityDate.
     * 
     * @return dfMaturityDate
     */
    public java.sql.Timestamp getDfMaturityDate() {
        return (java.sql.Timestamp) getValue(FIELD_DFMATURITYDATE);
    }

    /**
     * This method declaration sets the MaturityDate.
     * 
     * @param value: the new dfMaturityDate
     */
    public void setDfMaturityDate(java.sql.Timestamp value) {
        setValue(FIELD_DFMATURITYDATE, value);
    }
    
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE
        ="SELECT DISTINCT LIABILITY.BORROWERID, LIABILITY.LIABILITYID, " +
                "LIABILITY.COPYID, LIABILITY.LIABILITYTYPEID, " +
                "LIABILITY.LIABILITYDESCRIPTION, LIABILITY.LIABILITYAMOUNT, " +
                "LIABILITY.LIABILITYMONTHLYPAYMENT, LIABILITY.LIABILITYPAYOFFTYPEID, " +
                "LIABILITY.INCLUDEINGDS, LIABILITY.PERCENTINGDS, " +
                "LIABILITY.INCLUDEINTDS, LIABILITY.PERCENTINTDS, " +
                "LIABILITY.CREDITLIMIT, LIABILITY.CREDITBUREAURECORDINDICATOR, LIABILITY.MATURITYDATE, " +
                "LIABILITY.PERCENTOUTGDS FROM LIABILITY  __WHERE__  " +
                "ORDER BY LIABILITY.LIABILITYID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="LIABILITY";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="LIABILITY.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYID="LIABILITY.LIABILITYID";
	public static final String COLUMN_DFLIABILITYID="LIABILITYID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="LIABILITY.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYTYPEID="LIABILITY.LIABILITYTYPEID";
	public static final String COLUMN_DFLIABILITYTYPEID="LIABILITYTYPEID";
	public static final String QUALIFIED_COLUMN_DFLIABILITYDESC="LIABILITY.LIABILITYDESCRIPTION";
	public static final String COLUMN_DFLIABILITYDESC="LIABILITYDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFLIABILITYAMOUNT="LIABILITY.LIABILITYAMOUNT";
	public static final String COLUMN_DFLIABILITYAMOUNT="LIABILITYAMOUNT";
	public static final String QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITY.LIABILITYMONTHLYPAYMENT";
	public static final String COLUMN_DFLIABILITYMONTHLYPAYMENT="LIABILITYMONTHLYPAYMENT";
	public static final String QUALIFIED_COLUMN_DFLIABILITYPAYOFTYPEID="LIABILITY.LIABILITYPAYOFFTYPEID";
	public static final String COLUMN_DFLIABILITYPAYOFTYPEID="LIABILITYPAYOFFTYPEID";
	public static final String QUALIFIED_COLUMN_DFINCLUDEINGDS="LIABILITY.INCLUDEINGDS";
	public static final String COLUMN_DFINCLUDEINGDS="INCLUDEINGDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS="LIABILITY.PERCENTINGDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINGDS="PERCENTINGDS";
	public static final String QUALIFIED_COLUMN_DFINCLUDEINTDS="LIABILITY.INCLUDEINTDS";
	public static final String COLUMN_DFINCLUDEINTDS="INCLUDEINTDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS="LIABILITY.PERCENTINTDS";
	public static final String COLUMN_DFPERCENTINCLUDEDINTDS="PERCENTINTDS";
	public static final String QUALIFIED_COLUMN_DFPERCENTOUTGDS="LIABILITY.PERCENTOUTGDS";
	public static final String COLUMN_DFPERCENTOUTGDS="PERCENTOUTGDS";
    public static final String QUALIFIED_COLUMN_DFCREDITBUREAULIABILITYLIMIT="LIABILITY.CREDITLIMIT";
    public static final String COLUMN_DFCREDITBUREAULIABILITYLIMIT="CREDITLIMIT";
    public static final String QUALIFIED_COLUMN_DFCREDITBUREAUINDICATOR="LIABILITY.CREDITBUREAURECORDINDICATOR";
    public static final String COLUMN_DFCREDITBUREAUINDICATOR="CREDITBUREAURECORDINDICATOR";
    public static final String QUALIFIED_COLUMN_DFMATURITYDATE="LIABILITY.MATURITYDATE";
    public static final String COLUMN_DFMATURITYDATE="MATURITYDATE";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYID,
				COLUMN_DFLIABILITYID,
				QUALIFIED_COLUMN_DFLIABILITYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYTYPEID,
				COLUMN_DFLIABILITYTYPEID,
				QUALIFIED_COLUMN_DFLIABILITYTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYDESC,
				COLUMN_DFLIABILITYDESC,
				QUALIFIED_COLUMN_DFLIABILITYDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYAMOUNT,
				COLUMN_DFLIABILITYAMOUNT,
				QUALIFIED_COLUMN_DFLIABILITYAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYMONTHLYPAYMENT,
				COLUMN_DFLIABILITYMONTHLYPAYMENT,
				QUALIFIED_COLUMN_DFLIABILITYMONTHLYPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLIABILITYPAYOFTYPEID,
				COLUMN_DFLIABILITYPAYOFTYPEID,
				QUALIFIED_COLUMN_DFLIABILITYPAYOFTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDEINGDS,
				COLUMN_DFINCLUDEINGDS,
				QUALIFIED_COLUMN_DFINCLUDEINGDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINGDS,
				COLUMN_DFPERCENTINCLUDEDINGDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDEINTDS,
				COLUMN_DFINCLUDEINTDS,
				QUALIFIED_COLUMN_DFINCLUDEINTDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTINCLUDEDINTDS,
				COLUMN_DFPERCENTINCLUDEDINTDS,
				QUALIFIED_COLUMN_DFPERCENTINCLUDEDINTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPERCENTOUTGDS,
				COLUMN_DFPERCENTOUTGDS,
				QUALIFIED_COLUMN_DFPERCENTOUTGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
        
        FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
                FIELD_DFCREDITBUREAULIABILITYLIMIT,
                COLUMN_DFCREDITBUREAULIABILITYLIMIT,
                QUALIFIED_COLUMN_DFCREDITBUREAULIABILITYLIMIT,
                java.math.BigDecimal.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));

        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                FIELD_DFCREDITBUREAUINDICATOR,
                COLUMN_DFCREDITBUREAUINDICATOR,
                QUALIFIED_COLUMN_DFCREDITBUREAUINDICATOR,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
        
        FIELD_SCHEMA.addFieldDescriptor(
                new QueryFieldDescriptor(
                FIELD_DFMATURITYDATE,
                COLUMN_DFMATURITYDATE,
                QUALIFIED_COLUMN_DFMATURITYDATE,
                String.class,
                false,
                false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
                ""));
	}

}

