package mosApp.MosSystem;

import java.lang.reflect.Method;
import java.text.*;
import java.util.*;
import java.math.BigDecimal;
import javax.servlet.http.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import com.basis100.workflow.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.deal.security.*;
import com.basis100.deal.docrequest.DocumentRequest;
//import com.basis100.deal.docrequest.DocCentralInterface;
import com.basis100.deal.history.*;
import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.*;
import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.*;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.*;

import mosApp.*;
import mosApp.MosSystem.models.AVMOrderModel;
import MosSystem.*;

import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.util.service.OSCClosingServiceHelper;
import com.filogix.express.web.util.service.FixPriceClosingHelper;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServiceExecutor;
import java.sql.Timestamp;
//***** Change by NBC/PP Implementation Team - Version 1.3 - Start *****//
import com.basis100.deal.docprep.xsl.XSLMerger;
// ***** Change by NBC/PP Implementation Team - Version 1.3 - End *****//

/**

   Page Generation Details


   PageEntry.pageCondition4 contains indication of solicitor assignmant:

   true  - there is a solicitor assigned to the deal
   false - there is not currently a solicitor asigned to deal

   This property is used during conditional generation as follows:

   btAssignSolicitor - generate if have solicitor (pageCondition4 == true)
   btChangeSolicitor - generate if no solicitor (pageCondition4 == false)

Other page entry properties usage:

1) pageState1

   Hold the solicitor instructions text (i.e. on submit)

**/
public class ClosingActivityHandler extends PageHandlerCommon
    implements Cloneable, Sc
{
    
    private static Log _log =
        LogFactory.getLog(ClosingActivityHandler.class);
    
    private int fpcPrimaryPropertyId;
    private int ocsPrimaryPropertyId;

    /**
     *
     *
     */
    public ClosingActivityHandler cloneSS()
    {
        return(ClosingActivityHandler) super.cloneSafeShallow();
    }
    //////////////////////////////////////////////////////////////////////////

    private static final String INSTRUCTION_PRODUCED_YES_MSG="PRODUCEPACKAGE";

    //This method is used to populate the fields in the header
    //with the appropriate information

    public void populatePageDisplayFields()
    {
        PageEntry pg = getTheSessionState().getCurrentPage();
        ViewBean currNDPage = getCurrNDPage();
        Hashtable pst = pg.getPageStateTable();
        populatePageShellDisplayFields();
        // Quick Link Menu display
        displayQuickLinkMenu();
        populateTaskNavigator(pg);
        populatePageDealSummarySnapShot();
        populatePreviousPagesLinks();
        currNDPage.setDisplayFieldValue("stDateMask", new String(Sc.STANDARD_DATE_INPUT_FORMAT));
        
        /// CUSTOM POPULATION GOES HERE (e.g. set page specific display fields, etc)
        currNDPage.setDisplayFieldValue("stPartyName", (String) pst.get("PartyName"));
        currNDPage.setDisplayFieldValue("stPartyCompanyName", (String) pst.get("PartyCompanyName"));
        
        if (! pg.getPageState1().equals(""))
        {
            currNDPage.setDisplayFieldValue("tbInstructions", new String(pg.getPageState1()));
        }
        else
        {
            currNDPage.setDisplayFieldValue("tbInstructions",(String) pst.get("Instructions"));
        }

        // Setup custom display for Closing Details Section -- BILLY 30Nov2001
        QueryModelBase theClosingDetailsDo =(QueryModelBase)
        (RequestManager.getRequestContext().getModelManager().getModel(doClosingDetailsModel.class));

        // Set IADNumberOfDays display
        String tmpStr = "";
        int IADNumDays = 0;
        IADNumDays = TypeConverter.asInt(theClosingDetailsDo.getValue("dfIADNumberOfDays"), 0);

        logger.debug("The IADNumDaysDesc from DO : " + IADNumDays);

        if (IADNumDays > 0) currNDPage.setDisplayFieldValue("stIADNumDaysDesc", new String(IADNumDays + " Days IAD Period"));

        // Set FirstPaymentDate Monthly desc.
        int payFreqId = 0;
        payFreqId = TypeConverter.asInt(theClosingDetailsDo.getValue("dfPaymantFreqId"), 0);

        // Display only if Payment Frequency not Monthly
          int lang = theSessionState.getLanguageId();    //#DG504 moved
        if (payFreqId != Mc.PAY_FREQ_MONTHLY)
        {
            //#DG504 rewritten to send date instead of string
            Date dt;
            Object obj;
            try
            {
              //SimpleDateFormat df = new SimpleDateFormat("MMM d yyyy");
              dt = (Date)(theClosingDetailsDo.getValue("dfFirstPaymentDateMonthly"));
              //tmpStr = df.format(dt);
              obj = dt;
            }
            catch(Exception e)
            {
              //tmpStr = "";
              obj = "";
            }
            logger.debug("The FirstPaymentDateMthlyDesc from DO : " + obj);

            //// This line has been split into two ones: label (for future translation) and time stamp itself.
            //currNDPage.setDisplayFieldValue("stOneMonthAfterIAD", new String(Mc.ONE_MONTH_AFTER_IAD_LABEL));
            currNDPage.setDisplayFieldValue("stOneMonthAfterIAD",
                                            BXResources.getSysMsg("ONE_MONTH_AFTER_IAD_LABEL", lang));
            currNDPage.setDisplayFieldValue("stFirstPaymentDateMthlyDesc", obj);     // new String(tmpStr));
        }
        else
        {
            // Clear Display
            currNDPage.setDisplayFieldValue("stOneMonthAfterIAD", "");
            currNDPage.setDisplayFieldValue("stFirstPaymentDateMthlyDesc", "");
        }

          //moved up int lang = theSessionState.getLanguageId();
          //#DG504 end

        // Set Maturity Desc.
        if ((PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.calc.forcematuritydatetomonthlyequ", "N")).equals("Y"))
        {
            //--Release2.1--//
            //Modified to grab text from BXResources
            //--> By John 20Dec2002
            currNDPage.setDisplayFieldValue("stForceFirstMonthDesc",
                    new String(BXResources.getSysMsg("CA_MATURITY_DATE_MSG", lang)));
            //--------------//
        }

        // Set Interest Compound message
        // Check Interest Compound Desc

//        tmpStr = theClosingDetailsDo.getValue("dfInterestCompoundDesc").toString();
        tmpStr = theClosingDetailsDo.getValue(doClosingDetailsModel.FIELD_DFINTERESTCOMPOUNDDESC).toString();

        if (tmpStr != null && tmpStr.trim().equals("2"))
        {
            //--Release2.1--//
            //Modified to grab text from BXResources
            //--> By John 20Dec2002
            currNDPage.setDisplayFieldValue("stInterestCompoundMsg",
                    new String(BXResources.getSysMsg("CA_INTEREST_RATE_SEMI_ANNUAL", lang)));
            //--------------//
        }
        else
        {
            //--Release2.1--//
            //Modified to grab text from BXResources
            //--> By John 20Dec2002
            currNDPage.setDisplayFieldValue("stInterestCompoundMsg",
                    new String(BXResources.getSysMsg("CA_INTEREST_RATE_SIMPLE", lang)));
            //--------------//
        }
        

        
        // Jerry Closing hidden control.
        setHiddenClosingSection(getCurrNDPage(), pg);
        // Jerry Closing hidden control END.

        pst.clear();
        pst = null;
    }


    //////////////////////////////////////////////////////////////////////////
    //
    // Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
    //
    public void setupBeforePageGeneration()
    {
        PageEntry pg = getTheSessionState().getCurrentPage();
        if (pg.getSetupBeforeGenerationCalled() == true) return;
        pg.setSetupBeforeGenerationCalled(true);
        setupDealSummarySnapShotDO(pg);
        Hashtable pst = null;
        
        _log.info("Jerry: ClosingActivityHandler@ PageGeneration(): " + theSessionState.getSubmitSaved());

        if (pg.getPageStateTable() == null)
        {
            pst = new Hashtable();
            pg.setPageStateTable(pst);
        }
        else
        {
            pst = pg.getPageStateTable();
            pst.clear();
        }

        int dealId = pg.getPageDealId();
        int copyId = pg.getPageDealCID();

        //Jerry closing start
        QueryModelBase theFPCDo = (QueryModelBase)
            RequestManager.getRequestContext().getModelManager().
            getModel(doFixedPriceClosingInfoModel.class);
        theFPCDo.clearUserWhereCriteria();
        theFPCDo.addUserWhereCriterion("dfFPCDealId", "=", new Integer(dealId));
        theFPCDo.addUserWhereCriterion("dfFPCCopyId", "=", new Integer(copyId));
           //Jerry closing end

        // SEAN Closing Service Jan 23, 2005: add user criteria for OCS model.
        QueryModelBase theOCSDo = (QueryModelBase)
            RequestManager.getRequestContext().getModelManager().
            getModel(doOutsourcedClosingServiceModel.class);
        theOCSDo.clearUserWhereCriteria();
        theOCSDo.addUserWhereCriterion("dfOCSDealId", "=", new Integer(dealId));
        theOCSDo.addUserWhereCriterion("dfOCSCopyId", "=", new Integer(copyId));
        // SEAN Closing Service END
        
        // Added new section to show Closing Details -- By BILLY 30Nov2001
        // Setup Dataobjects
        QueryModelBase theClosingDetailsDo =(QueryModelBase)
            (RequestManager.getRequestContext().getModelManager().getModel(doClosingDetailsModel.class));

        theClosingDetailsDo.clearUserWhereCriteria();
        theClosingDetailsDo.addUserWhereCriterion("dfDealId", "=", new Integer(dealId));
        theClosingDetailsDo.addUserWhereCriterion("dfCopyId", "=", new Integer(copyId));
        
        // clear solicitor (party) contact DO criteria
        QueryModelBase theDO =(QueryModelBase)
            (RequestManager.getRequestContext().getModelManager().getModel(doClosingActivityInfoModel.class));

        theDO.clearUserWhereCriteria();
        _log.info("@ClosingActivity setupBeforePageGeneration1 getPageCondition4() " + pg.getPageCondition4());
        pg.setPageCondition4(false);
        _log.info("@ClosingActivity setupBeforePageGeneration2 getPageCondition4() " + pg.getPageCondition4());


        //// CUTOM STUFF GOES HERE (if any)
        // set the criteria for the populating data object and other state stuff
        try
        {
            SessionResourceKit kit = getSessionResourceKit();
            Deal deal = new Deal(kit, null, dealId, copyId);
            String solicitorspecialinst = deal.getSolicitorSpecialInstructions();
            if (solicitorspecialinst == null)
//  pg.setPageState1("");
                pst.put("Instructions", new String(""));
            else
//  pg.setPageState1(solicitorspecialinst);
                pst.put("Instructions", new String(solicitorspecialinst));
            PartyProfile partyProfile = new PartyProfile(kit);
            try
            {
                partyProfile.setSilentMode(true);

                // no logging if finder fails - not considered an error
                partyProfile = partyProfile.findByDealSolicitor(dealId);

                // contact DO criteria set from party
                Contact contact = new Contact(kit, partyProfile.getContactId(), DealEntity.INITIAL_COPY_ID);
                theDO.addUserWhereCriterion("dfContactId", "=", new Integer(partyProfile.getContactId()));
                String partyName = partyProfile.getPartyName();
                if (partyName == null) pst.put("PartyName", new String(""));
                else pst.put("PartyName", new String(partyName));
                String partyCompanyName = partyProfile.getPartyCompanyName();
                if (partyCompanyName == null) pst.put("PartyCompanyName", new String(""));
                else pst.put("PartyCompanyName", new String(partyCompanyName));
                _log.info("@ClosingActivity setupBeforePageGeneration3 getPageCondition4() " + pg.getPageCondition4());
                pg.setPageCondition4(true);
                _log.info("@ClosingActivity setupBeforePageGeneration4 getPageCondition4() " + pg.getPageCondition4());

            }
            catch(FinderException ex)
            {
                pg.setPageCondition4(false);
                _log.info("@ClosingActivity setupBeforePageGeneration Exep getPageCondition4() " + pg.getPageCondition4());

                pst.put("PartyName", new String(""));
                pst.put("PartyCompanyName", new String(""));
                pg.setPageCondition4(false);
                _log.info("@ClosingActivity setupBeforePageGeneration Exep getPageCondition4() " + pg.getPageCondition4());


                // contact DO criteria set to impossible criteria (ensure not found)
                theDO.addUserWhereCriterion("dfContactId", "=", new Integer(- 1));
            }
            logger.trace("ContactId = " + partyProfile.getContactId());
        }
        catch(Exception ex)
        {
            logger.error("Error @ClosingActivityHandler.setupBeforePageGeneration: Could no locate dealId=" + dealId + ", copyId=" + copyId);
            logger.error(ex);
            setStandardFailMessage();
        }
    }


    /**
     *
     *
     */
    public void saveData(PageEntry pg, boolean calledInTransaction)
    {
        pg.setPageState1(getCurrNDPage().getDisplayFieldValue("tbInstructions").toString());
    }


//////////////////////////////////////////////////////////////////////////
//
// conditional generation support methods
//
// Result: true - element should be generated
//         false    - element should not be generated
    public boolean generateAssignSolicitor()
    {
        PageEntry pe = getTheSessionState().getCurrentPage();
        if (pe.getPageCondition4() == true) return false;

       return true;
    }


//////////////////////////////////////////////////////////////////////////
    public boolean generateChangeSolicitor()
    {
        PageEntry pe = getTheSessionState().getCurrentPage();
        if (pe.getPageCondition4() == false) return false;
        return true;
    }


///////////////////////////////////////////////////////////////////////////
  //// Looks like there is no difference in action with regards to the Submit
  //// button name (Assign versus Change Solicitor). Thus the signature and the
  //// call from the corresponding ViewBean changed accordingly (no string button
  //// name in).
    public void handleAssignSolicitor(String buttonName)
    {
        //Navigate to the Party Search Screen
        try
        {
            PageEntry pg = getTheSessionState().getCurrentPage();
            PageEntry pgEntry = null;

            if (buttonName.equals("btAssignSolicitor"))
                pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_PARTY_SEARCH, true);
            else if (buttonName.equals("btChangeSolicitor"))
                pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_PARTY_SEARCH, true);
            else throw new Exception("");

            Hashtable subPst = pgEntry.getPageStateTable();
            subPst.put(PartySearchHandler.PARTY_TYPE_FILTER_PASSED_PARM, "" + Mc.PARTY_TYPE_SOLICITOR);
            getSavedPages().setNextPage(pgEntry);
            navigateToNextPage();
        }
        catch(Exception ex)
        {
            setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
            logger.error("Exception @ClosingActivityHandler.AssignSolicitor: while navigating to the Party Search subpage");
            logger.error(ex);
        }
    }

	/**
	 * @version 1.1 <br>
	 *          Date: 06/06/006 <br>
	 *          Author: NBC/PP Implementation Team <br>
	 *          Change: <br>
	 *          Added code to handle the pec email functionality. Only one email
	 *          should be sent when the deal status is saved to 17 or 19. <br>
	 * @version 1.2 <br>
	 *          Date: 06/07/006 <br>
	 *          Author: NBC/PP Implementation Team <br>
	 *          Change: <br>
	 *          Removed code for pec email functionality as it is handled only
	 *          in the doc tracking handler
         *          @version 1.3 <br>
	 *          Date: 12/08/006 <br>
	 *          Author: NBC/PP Implementation Team <br>
	 *          Change: <br>
	 *          - place check for NBC specific Lender
	 *          - call NBCrequestSolicitorsPackage() for NBC specific lender
	 *          - call requestSolicitorsPackage() for other lender
	 */
    ///////////////////////////////////////////////////////////////////////////
    public void handleSubmit(boolean regeneratePackageOk)
    {
        PageEntry pg = getTheSessionState().getCurrentPage();
        srk = getSessionResourceKit();
        int dealId = pg.getPageDealId();
        int copyId = pg.getPageDealCID();
        //		 ***** Change by NBC Impl. Team - Version 1.9 - Start *****//    	        
        CalcMonitor dcm         = CalcMonitor.getMonitor(srk);
        try
        {
	        //This code is Sapients, but in order to consolidate the dealId, and copyId local
	        //variables, I have passed them to the getDeal method. - Mike 08.30.2006
	        Deal deal = DBA.getDeal(srk, dealId, copyId, dcm);
	        // Force to trigger calc by totalCostOfBorrowing
	        deal.forceChange("totalCostOfBorrowing");
	       deal.forceChange("interestOverTerm");
	       deal.forceChange("balanceRemainingAtEndOfTerm");
           deal.forceChange("teaserRateInterestSaving");
	       dcm.inputEntity(deal);
           doCalculation(dcm);
        }
        catch (Exception ex)
        {
            logger.error("Exception @handleRecalculate :: " + ex.getMessage());	
	        ex.printStackTrace();
        }
    	// ***** Change by NBC Impl. Team - Version 1.9 - End *****//
        

        String instr = getCurrNDPage().getDisplayFieldValue("tbInstructions").toString().trim();
        
        //String instr = (pg.getPageState1() == null) ? "" : pg.getPageState1().trim();
        if (instr != null && instr.trim().length() > 0)
        {
            pg.setPageState1(instr);
        }
        
        SessionResourceKit srk = getSessionResourceKit();

        String produceSolicitorPkg =
            getCurrNDPage().getDisplayFieldValue("chProduceSolicitorPkg").toString();

        //ticket#3808  -------------- start-----------------  
        if(produceSolicitorPkg.equals("Y")){
        	logger.trace("CAH@handleSubmit(): produceSolicitorPkg = Y -- 1.1");
            try{
                PartyProfile partyProfile = new PartyProfile(srk);
                try
                {   
                    partyProfile.setSilentMode(true);
                    partyProfile = partyProfile.findByDealSolicitor(dealId);
                }
                catch(FinderException ex)
                {   setActiveMessageToAlert(BXResources.getSysMsg
                    ("CLOSING_ACTIVITY_MISSING_SOLICITOR", 
                       theSessionState.getLanguageId()),
                        ActiveMsgFactory.ISCUSTOMCONFIRM);
                return;
                }
            }
            catch(Exception ex)
            {   ex.printStackTrace();
            setStandardFailMessage();
            }
        }
        //ticket#3808  -------------- end -----------------  

        Deal deal = null;

        DocumentRequest SolDocReq = null;
        try
        {   
            String propOCS = PropertiesCache.getInstance().
            getProperty(theSessionState.getDealInstitutionId(),"mosApp.MosSystem.closing.suppressoutsourceddisplay", "Y");
            String propFPC = PropertiesCache.getInstance().
            getProperty(theSessionState.getDealInstitutionId(),"mosApp.MosSystem.closing.suppressfixedpricedisplay", "Y");
            
            boolean rc = (theSessionState.getSubmitSaved());
            if (propOCS.equals("N")) {
                int ocsProductId = -1;
                
                int ocsRequestId = OSCClosingServiceHelper.
                                        getOcsRequests(srk, dealId, copyId);
                ComboBox cbOCSProduct = (ComboBox)getCurrNDPage().
                                            getDisplayField("cbOCSProduct");

                if(cbOCSProduct.getValue()==null)
                    ocsProductId = 0;
                else { 
                    String productIdValue = cbOCSProduct.getValue().toString();
                    ocsProductId = Integer.parseInt(productIdValue);
                }

                if((ocsProductId>0)&&rc) { 
                    handleOcsSubmitSave(ocsRequestId, ocsProductId, pg);
                }
            }

            if (propFPC.equals("N")) {

                int fpcRequestId = -1;
                int fpcContactId = -1;
                int fpcProductId = -1;
                
                Map requestResult = FixPriceClosingHelper.getFpcRequests(srk, dealId, copyId);
                Set ids = requestResult.keySet();
                for (Iterator i = ids.iterator(); i.hasNext(); ) {
                    Integer requestInt = (Integer)i.next();
                    fpcRequestId = requestInt.intValue();
                    Integer contactInt = (Integer)requestResult.get(requestInt);
                    fpcContactId = contactInt.intValue();
                }
                            
                ComboBox cbFPCProduct = (ComboBox)getCurrNDPage().getDisplayField("cbFPCProduct");
                
                if(cbFPCProduct.getValue()==null)
                    fpcProductId = 0;
                else { 
                    String productIdValue = cbFPCProduct.getValue().toString();
                    fpcProductId = Integer.parseInt(productIdValue);
                }

                if((fpcProductId>0)&&rc) {
                    handleFpcSubmitSave(fpcRequestId, fpcContactId, fpcProductId, pg);
                }
            }
            
            srk.beginTransaction();            
            
            
            logger.trace("--T-- @ClosingActivity -- updateForClosing Line 1.1");
            deal = new Deal(srk, null, dealId, copyId);
            String flag = deal.getInstructionProduced();
            boolean alreadyInstructed =(flag == null) ? false :
                ((flag + "N").substring(0, 1).equals("Y"));

            // SEAN Closing service Jan 17, 2006: Produce Solicitor Package
            if (produceSolicitorPkg.equals("Y")) {
                     
            	logger.trace("CAH@handleSubmit(): produceSolicitorPkg = Y -- 1.2");

                // ***** Change by NBC/PP Implementation Team - Version 1.3 - Start *****//
		        String _lenderId = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"NBC_LENDER","N");
	            logger.trace("CAH@handleSubmit(): NBC_LENDER = " + _lenderId);
		        
		        if (_lenderId.equalsIgnoreCase("y") )
		        {
			          SolDocReq = DocumentRequest.requestNBCSolicitorsPackage(srk, deal, XSLMerger.TYPE_PDF);
		        }
		        else
                {      
		            logger.trace("CAH@handleSubmit(): calling DocumentRequest method " + _lenderId);		        	
		        	SolDocReq = DocumentRequest.requestSolicitorsPackage(srk, deal);		        	

                }
                if (regeneratePackageOk == false && alreadyInstructed == true) {
                       setActiveMessageToAlert(BXResources.getSysMsg(
                            "CLOSING_ACTIVITY_SOLICITOR_PACKAGE", 
                            theSessionState.getLanguageId()), 
                            ActiveMsgFactory.ISCUSTOMDIALOG2, 
                            "INSTRUCTION_PRODUCED_YES_MSG", "INSTRUCTION_PRODUCED_NO_MSG");

                    //--Ticket#607--09Sep2004--start--//
                    return;
                    //--Ticket#607--09Sep2004--end--//
                }
                deal.setInstructionProduced("YES"); //ticket#3952
            }

            if (alreadyInstructed == false)
            {
                PassiveMessage pm = runAllClosingActivityBusinessRules(pg, srk);
                if (pm != null)
                {
                    pm.setGenerate(true);
                    getTheSessionState().setPasMessage(pm);

                    ////SYNCADD
                    //Check if allowed th continue if softstop errors -- By BILLY 13June2002
                    if (pm.getCritical() == true || 
                            PropertiesCache.getInstance().
                                getProperty(theSessionState.getDealInstitutionId(),"com.basis100.deal.closing.allowsoftstops", "N").
                                    equals("N"))
                    {
                        logger.debug("@ClosingActivitySubmit:I am returning");
                        return;
                    }
                }
            }
            logger.debug("@ClosingActivitySubmit: before Solisitor package starts");
            
            //Jerry set the closing save flag
            theSessionState.setSubmitSaved(true); //reset the submitsaved flag
            _log.info("Jerry: ClosingActivityHandler@ reset handlesubmit(): " +  theSessionState.getSubmitSaved());
            //Jerry set the closing save flag
            
            
            // New requirement from Product to issue Eznet doc as well -- BILLY 05Sept2001
            // Modified to add parameter to control the generation of the Ezenet upload -- BILLY 20Feb2002
            if ((PropertiesCache.getInstance().
                    getProperty(theSessionState.getDealInstitutionId(),"com.basis100.system.generateezenetupload", "Y")).equals("Y"))
            {
                DocumentRequest.requestEzenet(srk, deal);
            }
            int statusId = deal.getStatusId();
            if (statusId == Mc.DEAL_CONDITIONS_OUT) 
                statusId = Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT;
            else if (statusId == Mc.DEAL_CONDITIONS_SATISFIED) 
                statusId = Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED;
            if (statusId != deal.getStatusId())
            {
                //Add log to deal history -- Billy 16May2001
                DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);
                String msg = hLog.statusChange(deal.getStatusId(), "Closing Activity");
                deal.setStatusId((short) statusId);
                deal.setStatusDate(new Date());
                hLog.log(deal.getDealId(), deal.getCopyId(), 
                        msg, Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), 
                            deal.getStatusId());
            }
            deal.setSolicitorSpecialInstructions(instr);
            //deal.setInstructionProduced("YES"); //ticket#3952
            logger.debug("@ClosingActivitySubmit: before servicing upload");
            
            // Check and send Servicing Upload -- Modified by BILLY 09Nov2001
            CCM ccm = new CCM(srk);
            ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_CLOSING, 
                    theSessionState.getLanguageId());
            deal.ejbStore();

            logger.debug("@ClosingActivitySubmit: before triggerWorkflow");

            // Change to call the workflow with parameter 2 (previously 0) to check for new task
            //  -- Billy 29Nov2001
            workflowTrigger(2, srk, pg, null);
            logger.debug("@ClosingActivitySubmit: after triggerWorkflow");
            
            standardAdoptTxCopy(pg, true);

            navigateToNextPage();
            srk.commitTransaction();
            return;
        }
        catch(Exception e)
        {   //theSessionState.setSubmitSaved(true);
            srk.cleanTransaction();
            logger.error("Exception @ClosingActivityHandler.handleSubmit");
            logger.error(e);
        }
        // an exception has occured - ensure we navigate away from page since we can't guar
        // presence of a tx copy on regeneration

        navigateAwayFromFromPage(true);
    }

    ///////////////////////////////////////////////////////////////////
    public String displayPhoneExtension(String val)
    {
        if (val != null && val.trim().length() != 0)
        {
            return "x" + val;
        }
        return "";
    }

    //////////////////////////////////////////////////////////////////
    public void handleCustomActMessageOk(String[] args)
    {
        //--CervusPhaseII--start--//
        PageEntry pg = theSessionState.getCurrentPage();
        SessionResourceKit srk = getSessionResourceKit();
        ViewBean thePage = getCurrNDPage();
        if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_YES"))
        {
            logger.trace("CAH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
            provideOverrideDealLockActivity(pg, srk, thePage);
            return;
        }

        if (args [ 0 ].equals("OVERRIDE_DEAL_LOCK_NO"))
        {
            logger.trace("CAH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");
            theSessionState.setActMessage(null);
            theSessionState.overrideDealLock(true);
            handleCancelStandard(false);
            return;
        }
        //--CervusPhaseII--end--//
        if (args [ 0 ].equals("INSTRUCTION_PRODUCED_YES_MSG"))
        {   
        	logger.trace("CAH@handleCustomActMessageOk: INSTRUCTION_PRODUCED_YES_MSG: regeneratePackageOk = Ok");
            handleSubmit(true);
            return;
        }
        
        
        if (args [ 0 ].equals("INSTRUCTION_PRODUCED_NO_MSG"))
        {   try
            {
                standardAdoptTxCopy(pg, true);
                navigateToNextPage();                
                logger.debug("handlecancel message @ClosingActivitySubmit: before return");
            }
            catch(Exception e)
            { theSessionState.setSubmitSaved(true);
              srk.cleanTransaction();
              logger.error("Exception in handlecancel message@ClosingActivityHandler.handleSubmit");
            }
            return;
          }
        
    }

    
    //performActMsgBtSubmit

///////////////////////////////////////////////////////////////////
    public PassiveMessage runAllClosingActivityBusinessRules(PageEntry pg, SessionResourceKit srk)
    {
        PassiveMessage pm = new PassiveMessage();
        BusinessRuleExecutor brExec = new BusinessRuleExecutor();

        try
        {
            brExec.BREValidator(getTheSessionState(), pg, srk, pm, "IS-%");
            logger.trace("--T--> @ClosingActivityHandler.runAllClosingActivityBusinessRules: IS rules");
            if (pm.getNumMessages() > 0) return pm;
        }
        catch(Exception e)
        {
            logger.error("Problem encountered during funding, @ClosingActivityHandler.runAllClosingActivityBusinessRules");
            logger.error(e);
            setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
                    ActiveMsgFactory.ISCUSTOMCONFIRM);
        }
        return null;
    }

    
    /**
 *
 *
 */
    public boolean filterDealFeeReptDo(QueryModelBase dObject)
    {
        try
        {
            PageEntry pg = theSessionState.getCurrentPage();
            dObject.clearUserWhereCriteria();
            dObject.addUserWhereCriterion("dfDealId", "=", new Integer(pg.getPageDealId()));
            dObject.addUserWhereCriterion("dfCopyId", "=", new Integer(pg.getPageDealCID()));
            return true;
        }
        catch(Exception e)
        {
            logger.error("Exception @filterDealFeeReptDo For Closing Activity");
            logger.error(e);
            return false;
        }
    }

    
    // Closing display section and primary checkbox setting borrowerid
    private void setHiddenClosingSection(ViewBean thePage, PageEntry pg) 
    {    
        try {
            doFixedPriceClosingInfoModel theObj =(doFixedPriceClosingInfoModel)
            (RequestManager.getRequestContext().getModelManager().getModel(doFixedPriceClosingInfoModel.class));

            if(theObj.getSize()>0) {
                if(theObj.getDfFPCBorrowerId()==null){
                    thePage.setDisplayFieldValue("chFPCBorrowerInfo",  "N");
                }
                else {
                    thePage.setDisplayFieldValue("chFPCBorrowerInfo",  "Y");
                }
            }
            
            String solicitorPackage = PropertiesCache.getInstance().
                getProperty(theSessionState.getDealInstitutionId(),"mosApp.MosSystem.closing.solicitorpackage", "Y");
            if(solicitorPackage.equals("Y"))
                thePage.setDisplayFieldValue("chProduceSolicitorPkg", "Y");

            String propOCS = PropertiesCache.getInstance().
                getProperty(theSessionState.getDealInstitutionId(),"mosApp.MosSystem.closing.suppressoutsourceddisplay", "Y");
            String propFPC = PropertiesCache.getInstance().
                getProperty(theSessionState.getDealInstitutionId(),"mosApp.MosSystem.closing.suppressfixedpricedisplay", "Y");
    
            //displayClosing Section
            int dealId = pg.getPageDealId();
            int copyId = pg.getPageDealCID();
    
            //displayClosing Section end
            
            if (propOCS.equals("N")) {
                 // Unset mask to display the field
                thePage.setDisplayFieldValue("stOCSHiddenStart", "");
                thePage.setDisplayFieldValue("stOCSHiddenEnd", "");
                displayOCSClosingSection(dealId, copyId);   // display two combox for OCS
            } else {
                // Set Mask to make the filed not dispalyed
                thePage.setDisplayFieldValue("stOCSHiddenStart", "<!--");
                thePage.setDisplayFieldValue("stOCSHiddenEnd", "-->");
            }
    
            if (propFPC.equals("N")) {
                // Unset mask to display the field
                thePage.setDisplayFieldValue("stFPCHiddenStart", "");
                thePage.setDisplayFieldValue("stFPCHiddenEnd", "");
                displayFPCClosingSection(dealId, copyId);   // display two combox  binding for FPC
                String propFpcComment = PropertiesCache.getInstance().
                    getProperty(theSessionState.getDealInstitutionId(),"mosApp.MosSystem.closing.fpccommentdisplay", "Y");
                if(propFpcComment.equals("Y"))
                    thePage.setDisplayFieldValue("stFpcCommentHidden", "visible");
                setFPCJSValidation();
                                                            
            } else {
                // Set Mask to make the filed not dispalyed
                thePage.setDisplayFieldValue("stFPCHiddenStart", "<!--");
                thePage.setDisplayFieldValue("stFPCHiddenEnd", "-->");
            }
        }
        catch(Exception e)
        {
            setStandardFailMessage();
            e.printStackTrace();
        }
    }
    
    /**
     * FPC submit save
     * @param requestId
     * @param contactId
     * @param productId
     * @param pg
     * @throws Exception
     */
    private void handleFpcSubmitSave(int requestId, int contactId, int productId, PageEntry pg) throws Exception
    {
        int dealId = pg.getPageDealId();
        _log.info("Jerry: ClosingActivityHandler@ handlesmitsave(): entered!");

        int copyId = pg.getPageDealCID();

        srk.beginTransaction();
        saveFpcRequestEntity(dealId, requestId, copyId, contactId, productId);

        theSessionState.setSubmitSaved(false);
        srk.commitTransaction();
    }
    
    /**
     * OCS submit save
     * @param ocsRequestId
     * @param ocsProductId
     * @param pg
     * @throws Exception
     */
    private void handleOcsSubmitSave(int ocsRequestId, int ocsProductId, PageEntry pg) throws Exception
    {
        int dealId = pg.getPageDealId();
        _log.info("Jerry: ClosingActivityHandler@ handlesmitsave(): entered!");

        int copyId = pg.getPageDealCID();

        srk.beginTransaction();
        saveOcsRequestEntity(dealId, ocsRequestId, copyId, ocsProductId);

        theSessionState.setSubmitSaved(false);
        srk.commitTransaction();
    }

    
    /**
     * Save OCS request entity
     * @param dealId
     * @param requestId
     * @param copyId
     * @param productId
     * @throws RequestProcessException
     */
    private void saveOcsRequestEntity(int dealId, int requestId, 
            int copyId, int productId) throws Exception {
                    
     // should get the primary propery's property id.
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
        for (Iterator i = deal.getProperties().iterator(); i.hasNext(); ) {
        Property property = (Property) i.next();
        if (property.isPrimaryProperty()) {
            if (_log.isDebugEnabled())
                _log.debug("Found the primary property: " +
                          property.getPropertyId());
                ocsPrimaryPropertyId = property.getPropertyId();
            break;
            }
        }

        Request requestEntity = new Request(srk);
        
        if (requestId > 0) {
            requestEntity = requestEntity.
            findByPrimaryKey(new RequestPK(requestId, copyId));
            
            // check the request's status to see what we need.
            switch (requestEntity.getRequestStatusId()) {
                case ServiceConst.REQUEST_STATUS_BLANK:
                case ServiceConst.REQUEST_STATUS_CANCELLATION_CONFIRMED:
                    _log.info("Even through, we got to create a new one!");
                    // for following status, we need create new request.
                    createOcsRequestEntity(requestId, copyId, productId, srk);
                    break;
                default:
                    // for all other status, means update too!
                    requestEntity =
                        updateOcsRequestEntity(productId, requestEntity, srk);
                break;
            }
        } else {
            createOcsRequestEntity(dealId, copyId, productId, srk);
        }
     }    
    
    /**
     * ocs save request.
     */
    private void createOcsRequestEntity(int requestId, int copyId,
                      int productId, SessionResourceKit srk)
        throws Exception {

        try {

            // creating request entity.
            Request requestEntity = new Request(srk);
            requestEntity.create(new DealPK(requestId, copyId),
                                 ServiceConst.REQUEST_STATUS_NOT_SUBMITTED,
                                 new Date(), srk.getExpressState().getUserProfileId());
            if (_log.isDebugEnabled())
                _log.debug("Request Entity Created: " +
                           requestEntity.toString());

            // preparing the channel info for the closing request.
            requestEntity.setChannelId(5);  // CHANNELID_CLOSING_OCS_REQ
            requestEntity.setServiceProductId(productId);
            requestEntity.
                setServiceTransactionTypeId(ServiceConst.
                                            SERVICE_TRANSACTION_TYPE_REQUEST);
            requestEntity.
                setPayloadTypeId(ServiceConst.
                                            SERVICE_PAYLOAD_TYPE_OSC_REQUEST);
            requestEntity.
                setRequestTypeId(ServiceConst.
                                 REQUEST_TYPE_CLOSING_OUTSOURCED_REQUEST_INT);
            requestEntity.setRequestDate(new Date());
            requestEntity.setUserProfileId(srk.getExpressState().getUserProfileId());
            requestEntity.ejbStore();

            // creating servicerequest entity.
            _log.info("creating servicerequest for request: " +
                      requestEntity.getRequestId());
            ServiceRequest serviceRequest = new ServiceRequest(srk);
            serviceRequest = serviceRequest.create(requestEntity.getRequestId(),
                                                   requestEntity.getCopyId());
            // update other info.
            // should get the primary propery's property id.
            serviceRequest.setPropertyId(ocsPrimaryPropertyId);
            serviceRequest.ejbStore();

            _log.info("Successfully created entities!");
        } 
        catch (Exception e) {
            _log.error("create request entity exceptions!", e);
            e.printStackTrace();
        }
    }
        
        
        
    /**
     * update the service request eneity.
     */
    private Request updateOcsRequestEntity(int productId,
                                        Request requestEntity,                               
                                        SessionResourceKit srk)
        throws Exception {

        // generic update.
        requestEntity.setServiceProductId(productId);
        requestEntity.setRequestDate(new Date());
        requestEntity.setUserProfileId(srk.getExpressState().getUserProfileId());
        requestEntity.ejbStore();

        //for servicerequeset nothing to update
        
        return requestEntity;
    }
    
    
    /**
     * save ocs
     */
    
    
    
    // Jerry Display OCS combobox Closing section.
    /**
     * display the closing section.
     */
    public void displayOCSClosingSection(int dealId, int copyId)
        throws Exception {
       
        //populate optionlist for provider
        populateProviders("cbOCSProvider",
                ServiceConst.SERVICE_TYPE_CLOSING,
                ServiceConst.SERVICE_SUB_TYPE_OUTSORCED_CLOSING);
        //provider optionlist populated
        
        QueryModelBase ocsClosingModel = (QueryModelBase)
        (RequestManager.getRequestContext().getModelManager().
            getModel(doOutsourcedClosingServiceModel.class));
        _log.info("OutsourcedClosing ocsClosingModel size: " + 
                ocsClosingModel.getSize());

        ocsClosingModel.clearUserWhereCriteria();
        ocsClosingModel.
            addUserWhereCriterion(doOutsourcedClosingServiceModel.FIELD_DFOCSDEALID,
                new Integer(dealId));
        ocsClosingModel.
            addUserWhereCriterion(doOutsourcedClosingServiceModel.FIELD_DFOCSCOPYID,
                new Integer(copyId));
        ocsClosingModel.executeSelect(null);
    
        _log.info("OutsourcedClosing ocsClosingModel size: " + ocsClosingModel.getSize());

        if (ocsClosingModel.getSize() < 1) {
            // we got nothing.
            // setting up the combobox fields.
        } else {
                
            getCurrNDPage().
            setDisplayFieldValue("cbOCSProvider",
                    ocsClosingModel.
                    getValue(doOutsourcedClosingServiceModel.FIELD_DFOCSPROVIDERID));
            
            String providerId = ocsClosingModel.
                getValue(doOutsourcedClosingServiceModel.FIELD_DFOCSPROVIDERID).toString();

            // populate the product options.
           populateProducts("cbOCSProduct",
                    Integer.parseInt(providerId),
                    ServiceConst.SERVICE_TYPE_CLOSING,
                    ServiceConst.SERVICE_SUB_TYPE_OUTSORCED_CLOSING);
            // set up the product.
            getCurrNDPage().
                setDisplayFieldValue("cbOCSProduct",
                        ocsClosingModel.getValue(doOutsourcedClosingServiceModel.FIELD_DFOCSPRODUCTID));
        }      
    }

    
    /**
     * display FPC combobox Closing Section
     */
    public void displayFPCClosingSection(int dealId, int copyId)
        throws Exception {

        //populate optionlist for provider
        populateProviders("cbFPCProvider",
                ServiceConst.SERVICE_TYPE_CLOSING,
                ServiceConst.SERVICE_SUB_TYPE_FIXED_PRICE_CLOSING);
       
        QueryModelBase fpcClosingModel = (QueryModelBase)
            (RequestManager.getRequestContext().getModelManager().
             getModel(doFixedPriceClosingInfoModel.class));

        _log.info("fpc model in hanlder getsize()" + fpcClosingModel.getSize());

        
        fpcClosingModel.clearUserWhereCriteria();
        fpcClosingModel.addUserWhereCriterion(doFixedPriceClosingInfoModel.FIELD_DFFPCDEALID,
                                            new Integer(dealId));
        fpcClosingModel.addUserWhereCriterion(doFixedPriceClosingInfoModel.FIELD_DFFPCCOPYID,
                                            new Integer(copyId));
        fpcClosingModel.executeSelect(null);
        
        _log.info("fpc model in hanlder getsize()" + fpcClosingModel.getSize());
        
        if (fpcClosingModel.getSize() < 1) {
            // we got nothing.
            // setting up the combobox fields.
        } else {                
            getCurrNDPage().
            setDisplayFieldValue("cbFPCProvider",
                    fpcClosingModel.getValue(doFixedPriceClosingInfoModel.FIELD_DFFPCPROVIDERID));
            
            String providerId = fpcClosingModel.
                getValue(doFixedPriceClosingInfoModel.FIELD_DFFPCPROVIDERID).toString();

            // populate the product options.
            
            populateProducts("cbFPCProduct",
                    Integer.parseInt(providerId),
                    ServiceConst.SERVICE_TYPE_CLOSING,
                    ServiceConst.SERVICE_SUB_TYPE_FIXED_PRICE_CLOSING);
            
            // set up the product.
            getCurrNDPage().
                setDisplayFieldValue("cbFPCProduct",
                        fpcClosingModel.getValue(doFixedPriceClosingInfoModel.FIELD_DFFPCPRODUCTID));
       }
    }
   // Jerry Closing display section END
    
    
    /**
     * return a request entity based on the fpc request object.
     */
    private void saveFpcRequestEntity(int dealId, int requestId, int copyId, int contactId,
                                     int productId)
        throws Exception {

             // should get the primary propery's property id.
            _log.info("Jerry: ClosingActivityHandler@ saveRequestEntity(): entered!");

            Deal deal = new Deal(srk, null);
            deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
            for (Iterator i = deal.getProperties().iterator(); i.hasNext(); ) {
                Property property = (Property) i.next();
                if (property.isPrimaryProperty()) {
                    if (_log.isDebugEnabled())
                        _log.debug("Found the primary property: " +
                                   property.getPropertyId());
                    fpcPrimaryPropertyId = property.getPropertyId();
                    break;
                }
            }

            Request requestEntity = new Request(srk);

            if (requestId > 0) {

                requestEntity = requestEntity.
                    findByPrimaryKey(new RequestPK(requestId, copyId));

                // check the request's status to see what we need.
                switch (requestEntity.getRequestStatusId()) {
                    case ServiceConst.REQUEST_STATUS_NOT_SUBMITTED:
                    case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
                    case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
                    {
                        // for these status, we just use this request entity to
                        // update. also need update the service request contact
                        // record before send out update request.
                        updateRequestEntity(dealId, copyId, productId, contactId, requestEntity);
                        break;
                    }
                    default:
                    {
                        _log.info("Even through, we got to create a new one!");
                        // for following status, we need create new request.
                        createRequestEntity(dealId, copyId, productId, srk);
                        break;
                    }
                }
            } else {
                _log.info("Seems like it is the first time, just create a " +
                          "new request entity!");
                createRequestEntity(dealId, copyId, productId, srk);
            }

    }

    /**
     * create the request entity.
     */
    private void createRequestEntity(int dealId, int copyId, int productId, 
            SessionResourceKit srk)
        throws RequestProcessException {

        _log.info("Trying to create a new Request entity ...");
        try {

            // creating request entity.
            Request requestEntity = new Request(srk);
            requestEntity.create(new DealPK(dealId, copyId),
                                 ServiceConst.REQUEST_STATUS_NOT_SUBMITTED,
                                 new Date(), srk.getExpressState().getUserProfileId());
            if (_log.isDebugEnabled())
                _log.debug("Request Entity Created: " +
                           requestEntity.toString());

            requestEntity.setChannelId(5);  // CHANNELID_CLOSING_FP_REQ
            requestEntity.setServiceProductId(productId);
            requestEntity.
                setServiceTransactionTypeId(ServiceConst.
                                            SERVICE_TRANSACTION_TYPE_REQUEST);
            requestEntity.
                setPayloadTypeId(ServiceConst.
                                    SERVICE_PAYLOAD_TYPE_OSC_REQUEST);
            requestEntity.
                setRequestTypeId(ServiceConst.REQUEST_TYPE_CLOSING_FIXED_REQUEST_INT);

            requestEntity.setStatusDate(new Date());
            requestEntity.setUserProfileId(srk.getExpressState().getUserProfileId());
            requestEntity.ejbStore();

            // creating servicerequest entity.
            _log.info("creating servicerequest for request: " +
                      requestEntity.getRequestId());
            ServiceRequest serviceRequest = new ServiceRequest(srk);
            serviceRequest = serviceRequest.create(requestEntity.getRequestId(),
                                                   requestEntity.getCopyId());
            // set the primary propery's property id.
            serviceRequest.setPropertyId(fpcPrimaryPropertyId);
            serviceRequest.setSpecialInstructions("N/A for Closing");
            serviceRequest.ejbStore();
            _log.info("finish with creating servicerequest for request: " +
                    requestEntity.getRequestId());
            
            String primaryBorrowerInfo =
                getCurrNDPage().getDisplayFieldValue("chFPCBorrowerInfo").toString();

            boolean needPrimary = primaryBorrowerInfo.equals("Y");

            // create servicerequest contact
            ServiceRequestContact contact = new ServiceRequestContact(srk);
            if (needPrimary) {
                contact = createContactByBorrower(dealId, copyId, requestEntity,
                                                  contact, srk);
            } else {
                contact = contact.create(requestEntity.getRequestId(),
                                         requestEntity.getCopyId());
                contact = updateServicRequestContact(contact);
            }
            contact.ejbStore();

        } catch (Exception e) {
            _log.error("create request entity exceptions!", e);
            e.printStackTrace();
        }
    }

    
    /**
     * update the service request eneity.
     */
    private void updateRequestEntity(int dealId, int copyId, int productId, int contactId,
            Request requestEntity)
        throws Exception {
       
        _log.info("Jerry: ClosingActivityHandler@ updateRequestEntity(): entered!");
        requestEntity.setServiceProductId(productId);
        requestEntity.setRequestDate(new Date());
        requestEntity.setUserProfileId(srk.getExpressState().getUserProfileId());
        requestEntity.ejbStore();

        String primaryBorrowerInfo =
            getCurrNDPage().getDisplayFieldValue("chFPCBorrowerInfo").toString();

        boolean needPrimary = primaryBorrowerInfo.equals("Y");
        
        // get the existing contact.
        ServiceRequestContact contact =
            new ServiceRequestContact(srk,
                                      contactId,
                                      requestEntity.getRequestId(),
                                      requestEntity.getCopyId());
        if (needPrimary) {
            _log.info("Updating request entity by primary borrower");
            if (contact.getBorrowerId() > 0) {
                _log.info("we already use primary borrower...");
                // do nothing here...
            } else {
                _log.info("without to with primary borrower create ...");
                // find the borrower info.
                Borrower borrower = null;
                Deal deal = new Deal(srk, null);
                deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
                for (Iterator i = deal.getBorrowers().iterator();
                     i.hasNext(); ) {
                    borrower = (Borrower) i.next();
                    if (borrower.isPrimaryBorrower()) {
                        break;
                    }
                }
                if (borrower == null) {
                    throw new RequestProcessException("No Primary Borrower!");
                }
                requestEntity.createChildAssocs(borrower.getBorrowerId());
                contact.setBorrowerId(borrower.getBorrowerId());
                
                updateContactBorrower(contact, borrower);

                contact.ejbStore();
            }
        } else {
            _log.info("Updating request entity without primary borrower");
            
            // update the contact info.
            contact = updateServicRequestContact(contact);
            contact.ejbStore();
            if (contact.getBorrowerId() > 0) {
                _log.info("with to without primary borrower. remove...");
                contact.cutBorrowerAssoc();
                requestEntity.removeChildAssocs();
            } else {
                _log.info("without to without primary borrower. do thing...");
            }
        }
    }
    
    
    /**
     * update  the contact info based on the request.
     */
    private ServiceRequestContact
        updateServicRequestContact(ServiceRequestContact serviceRequestContactEntity) {
        
        String contactFirstName = getCurrNDPage().getDisplayFieldValue("stFPCContactFirstName").toString().trim();
        serviceRequestContactEntity.setFirstName(contactFirstName);
        String contactLastName = getCurrNDPage().getDisplayFieldValue("stFPCContactLastName").toString().trim();
        serviceRequestContactEntity.setLastName(contactLastName);
        String email = getCurrNDPage().getDisplayFieldValue("tbFPCEmail").toString().trim();
        serviceRequestContactEntity.setEmailAddress(email);
        
        String phoneNumber = getCurrNDPage().getDisplayFieldValue("tbFPCWorkPhoneAreaCode").toString().trim();
        serviceRequestContactEntity.setWorkAreaCode(phoneNumber);
        String firstNum = getCurrNDPage().getDisplayFieldValue("tbFPCWorkPhoneFirstThreeDigits").toString().trim();
        String lastNum = getCurrNDPage().getDisplayFieldValue("tbFPCWorkPhoneLastFourDigits").toString().trim();
        serviceRequestContactEntity.setWorkPhoneNumber(firstNum+lastNum);
        String extension = getCurrNDPage().getDisplayFieldValue("tbFPCWorkPhoneNumExtension").toString().trim();
        serviceRequestContactEntity.setWorkExtension(extension);

        phoneNumber = getCurrNDPage().getDisplayFieldValue("tbFPCCellPhoneAreaCode").toString().trim();
        serviceRequestContactEntity.setCellAreaCode(phoneNumber);
        firstNum = getCurrNDPage().getDisplayFieldValue("tbFPCCellPhoneFirstThreeDigits").toString().trim();
        lastNum = getCurrNDPage().getDisplayFieldValue("tbFPCCellPhoneLastFourDigits").toString().trim();
        serviceRequestContactEntity.setCellPhoneNumber(firstNum+lastNum);
        
        phoneNumber = getCurrNDPage().getDisplayFieldValue("tbFPCHomePhoneAreaCode").toString().trim();
        serviceRequestContactEntity.setHomeAreaCode(phoneNumber);
        firstNum = getCurrNDPage().getDisplayFieldValue("tbFPCHomePhoneFirstThreeDigits").toString().trim();
        lastNum = getCurrNDPage().getDisplayFieldValue("tbFPCHomePhoneLastFourDigits").toString().trim();
        serviceRequestContactEntity.setHomePhoneNumber(firstNum+lastNum);
        
        String comments = getCurrNDPage().getDisplayFieldValue("stFPCComments").toString().trim();
        serviceRequestContactEntity.setComments(comments);

        return serviceRequestContactEntity;
    }
    
    
    /**
     * update the contact inf based on the primary borrower id.
     */
    private ServiceRequestContact
        updateContactBorrower(ServiceRequestContact contact,
                                   Borrower borrower) {
 
        if(_log.isDebugEnabled())
            _log.debug("update contact record based on the borrower info");
        contact.setFirstName(borrower.getBorrowerFirstName());
        contact.setLastName(borrower.getBorrowerLastName());
        // no email address in borrower, make it empty.
        contact.setEmailAddress(borrower.getBorrowerEmailAddress());
        // work phone.
        String phoneNumber = borrower.getBorrowerWorkPhoneNumber();
        if ((phoneNumber != null) && phoneNumber.trim().length() > 0) {
            contact.setWorkAreaCode(phoneNumber.substring(0, 3));
            contact.setWorkPhoneNumber(phoneNumber.substring(3));
        }
        else {
            contact.setWorkAreaCode("");
            contact.setWorkPhoneNumber("");
        }
        contact.setWorkExtension(borrower.getBorrowerWorkPhoneExtension());
        // no cell phone number, make it empty.
        contact.setCellAreaCode("");
        contact.setCellPhoneNumber("");
        // home phone
        phoneNumber = borrower.getBorrowerHomePhoneNumber();
        if ((phoneNumber != null) && phoneNumber.trim().length() > 0) {
            contact.setHomeAreaCode(phoneNumber.substring(0, 3));
            contact.setHomePhoneNumber(phoneNumber.substring(3));
        }
        else {
            contact.setHomeAreaCode("");
            contact.setHomePhoneNumber("");
        }
        // we will not touch the coments.
 
        return contact;
    }
    
    
    /**
     * create service request contact by use the borrower.
     */
    private ServiceRequestContact
        createContactByBorrower(int dealId, int copyId,
                                Request requestEntity,
                                ServiceRequestContact contact,
                                SessionResourceKit srk)
        throws Exception {

        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
        for (Iterator i = deal.getBorrowers().iterator(); i.hasNext(); ) {
            Borrower borrower = (Borrower) i.next();
            if (borrower.isPrimaryBorrower()) {
                contact = contact.
                    createWithBorrowerAssoc(requestEntity.getRequestId(),
                                            borrower.getBorrowerId(),
                                            requestEntity.getCopyId());
                
                updateContactBorrower(contact, borrower);
                break;
            }
        }

        return contact;
    }

    /*
     * new field validation based on new fs
     */
    
    public void setFPCJSValidation()
    {
      // Validate # of Dependants
      HtmlDisplayFieldBase df = null;
      String theExtraHtml = null;

      ViewBean thePage = getCurrNDPage();

      // JS for Work Phone # fields
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCWorkPhoneAreaCode");
      theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
      df.setExtraHtml(theExtraHtml);

      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCWorkPhoneFirstThreeDigits");
      theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
      df.setExtraHtml(theExtraHtml);

      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCWorkPhoneLastFourDigits");
      theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
      df.setExtraHtml(theExtraHtml);

      // JS for Work Phone # Extention
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCWorkPhoneNumExtension");
      theExtraHtml = "onBlur=\"isFieldInteger(0);\"";
      df.setExtraHtml(theExtraHtml);

      // JS for Cell Phone # fields
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCCellPhoneAreaCode");
      theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
      df.setExtraHtml(theExtraHtml);

      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCCellPhoneFirstThreeDigits");
      theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
      df.setExtraHtml(theExtraHtml);

      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCCellPhoneLastFourDigits");
      theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
      df.setExtraHtml(theExtraHtml);

      // JS for Cell Phone # fields
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCHomePhoneAreaCode");
      theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
      df.setExtraHtml(theExtraHtml);

      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCHomePhoneFirstThreeDigits");
      theExtraHtml = "onBlur=\"isFieldInteger(3);\"";
      df.setExtraHtml(theExtraHtml);

      df = (HtmlDisplayFieldBase)thePage.getDisplayField("tbFPCHomePhoneLastFourDigits");
      theExtraHtml = "onBlur=\"isFieldInteger(4);\"";
      df.setExtraHtml(theExtraHtml);
      //==============================================================
    }

    /**
     * populate provider options.
     */
    protected void populateProviders(String fieldName, int type, int subtype) {

        // populate the provider's options.
        Map providers = OSCClosingServiceHelper.
            getValidProviders(getSessionResourceKit(),
                              theSessionState.getLanguageId(),
                              type, subtype);
        OptionList providerOptions = new OptionList();
        Set ids = providers.keySet();
        for (Iterator i = ids.iterator(); i.hasNext(); ) {
            String id = (String) i.next();
            String label = (String) providers.get(id);
            providerOptions.add(new Option(label, id));
        }
        ComboBox cbProvider = (ComboBox) getCurrNDPage().getChild(fieldName);
        cbProvider.setOptions(providerOptions);
    }

    /**
     * populate product options.
     */
    protected void populateProducts(String fieldName, int providerId,
                                    int type, int subtype) {

        // let's prepare the options.
        OptionList theProductOptions = new OptionList();
        Map products = OSCClosingServiceHelper.
            getValidProducts(getSessionResourceKit(),
                             theSessionState.getLanguageId(),
                             type, subtype, providerId);
        for (Iterator i = products.keySet().iterator(); i.hasNext(); ) {
            Integer id = (Integer) i.next();
            String label = (String) products.get(id);
            theProductOptions.add(new Option(label, id.toString()));
        }
        // populate the product options.
        ComboBox cbProduct =
            (ComboBox) getCurrNDPage().getChild(fieldName);
        cbProduct.setOptions(theProductOptions);
    }

    /**
     * clean up the options for the given field.
     */
    protected void cleanOptions(String fieldName) {

        ComboBox box = (ComboBox) getCurrNDPage().getChild(fieldName);
        box.setOptions(new OptionList());
    }

}

