package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doStreetTypeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getMOS_STREETTYPE_STREETTYPEID();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_STREETTYPE_STREETTYPEID(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getMOS_STREETTYPE_STDESCRIPTION();

	
	/**
	 * 
	 * 
	 */
	public void setMOS_STREETTYPE_STDESCRIPTION(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_MOS_STREETTYPE_STREETTYPEID="MOS_STREETTYPE_STREETTYPEID";
	public static final String FIELD_MOS_STREETTYPE_STDESCRIPTION="MOS_STREETTYPE_STDESCRIPTION";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

