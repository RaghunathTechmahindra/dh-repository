package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doIALEmploymentHistoryModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfOccupationId();

	
	/**
	 * 
	 * 
	 */
	public void setDfOccupationId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIndustrySectorId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIndustrySectorId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfEmploymentHistoryId();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentHistoryId(java.math.BigDecimal value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFOCCUPATIONID="dfOccupationId";
	public static final String FIELD_DFINDUSTRYSECTORID="dfIndustrySectorId";
	public static final String FIELD_DFEMPLOYMENTHISTORYID="dfEmploymentHistoryId";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

