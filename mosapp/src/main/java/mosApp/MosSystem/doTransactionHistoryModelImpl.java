package mosApp.MosSystem;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doTransactionHistoryModelImpl extends QueryModelBase
	implements doTransactionHistoryModel
{
	/**
	 *
	 *
	 */
	public doTransactionHistoryModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
    return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //logger = SysLog.getSysLogger("DOTHM");
    ////logger.debug("DODMS@afterExecute::SQLTemplate: " + this.getSelectSQL());

    //// 6. Override all the Description fields (see #1-5 below in SQL_TEMPLATE section
    //// and populate them from the BXResource based on the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			              getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                            SessionStateModel.class,
                                            defaultInstanceStateName,
                                            true);

    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      ////Translate Current Value in case of scenario description.
      this.setDfCurrentValue(translateScenarioDescription(this.getDfCurrentValue(), languageId));

      ////Translate Previous Value in case of scenario description.
      this.setDfPreviousValue(translateScenarioDescription(this.getDfPreviousValue(), languageId));
    }

   //Reset Location
    this.beforeFirst();

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfTransactionDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFTRANSACTIONDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfTransactionDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFTRANSACTIONDATE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfContext()
	{
		return (String)getValue(FIELD_DFCONTEXT);
	}


	/**
	 *
	 *
	 */
	public void setDfContext(String value)
	{
		setValue(FIELD_DFCONTEXT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfUser()
	{
		return (String)getValue(FIELD_DFUSER);
	}


	/**
	 *
	 *
	 */
	public void setDfUser(String value)
	{
		setValue(FIELD_DFUSER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfContextSource()
	{
		return (String)getValue(FIELD_DFCONTEXTSOURCE);
	}


	/**
	 *
	 *
	 */
	public void setDfContextSource(String value)
	{
		setValue(FIELD_DFCONTEXTSOURCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPreviousValue()
	{
		return (String)getValue(FIELD_DFPREVIOUSVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfPreviousValue(String value)
	{
		setValue(FIELD_DFPREVIOUSVALUE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCurrentValue()
	{
		return (String)getValue(FIELD_DFCURRENTVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfCurrentValue(String value)
	{
		setValue(FIELD_DFCURRENTVALUE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfField()
	{
		return (String)getValue(FIELD_DFFIELD);
	}


	/**
	 *
	 *
	 */
	public void setDfField(String value)
	{
		setValue(FIELD_DFFIELD,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEntity()
	{
		return (String)getValue(FIELD_DFENTITY);
	}


	/**
	 *
	 *
	 */
	public void setDfEntity(String value)
	{
		setValue(FIELD_DFENTITY,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfTransactionDate1()
	{
		return (String)getValue(FIELD_DFTRANSACTIONDATE1);
	}


	/**
	 *
	 *
	 */
	public void setDfTransactionDate1(String value)
	{
		setValue(FIELD_DFTRANSACTIONDATE1,value);
	}

    public java.math.BigDecimal getDfInstitutionId() {
        return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
      }

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }

	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doTransactionHistoryModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/

  //// This is a light solution to provide the translation of two basic scenario
  //// descriptions. This field is a free text field and the full scale translation
  //// should take a serious work. The only two basic patterns will be translated:
  //// "Based on Deal on MMM DD YYYY at HH:MM EST" and
  //// "Based on Scenario XX on MMM DD YYYY at HH:MM EST".

  //// Also this is done for Java 1.3 without the RegularExpession extension.
  //// Java 1.4 with precompiled RegExpression facilities
  //// gives much more room for translation. When the JBuilder will be upgrated to Java 1.4
  //// the following method should be re-written to incorporate the RegularExpression
  //// facilities.
  //--> This translation logic should be part of the Business Logic should move to the Handler
  //--> Need to discuss more with Vlad later ????
  //--> Comment by Billy 08April2003
	protected String translateScenarioDescription(String scenarioIn, int langId)
	{
		try
		{
      String based = "";
      String scenarioTimeStamp = "";

      // detect if not present or null
			if (scenarioIn == null || scenarioIn.trim().length() == 0)
        return scenarioIn;

      //// 1. Read the first token and go or not to go. This is very non-generic
      //// approach to parse the only approved for translation patterns. This part
      //// (until #2 should be re-done (similar to #2) if product team wants more
      //// translation. Note that in the parsed line fragment there is no chars
      //// 'o' and 'n' that's why (and only) this token can be used.
      scenarioIn = scenarioIn.trim();
      StringTokenizer st = new StringTokenizer(scenarioIn, "on", false);

      StringBuffer resultBuf = new StringBuffer();

      ////start reading the value read up taking the tokens
      based = st.nextToken();

      String deal = st.nextToken();

			//// Go further only if the first word is "Based"
      //// otherwise return the untranslated input String.
      if(scenarioIn.substring(0, 5).trim().equals("Based"))
      {
        String tmp1 = scenarioIn.substring(0, 13).trim();
        String tmp2 = scenarioIn.substring(0, 17).trim();

        if(tmp1.equals("Based on Deal"))
        {
          String basedOnDeal = BXResources.getGenericMsg("BASED_ON_DEAL_LABEL",  langId);
          resultBuf.append(basedOnDeal);
          resultBuf.append(" ");

          scenarioTimeStamp = scenarioIn.substring(17).trim();
        }

        if(tmp2.equals("Based on Scenario"))
        {
          String basedOnScenario = BXResources.getGenericMsg("BASED_ON_SCENARIO_LABEL",  langId);
          resultBuf.append(basedOnScenario);
          resultBuf.append(" ");

          String scenarioNum = scenarioIn.substring(18, 20);
          //logger.debug("THM@translateScenarioDescription::ScenarioNum: " + scenarioNum);
          resultBuf.append(scenarioNum);
          resultBuf.append(" ");

          scenarioTimeStamp = scenarioIn.substring(21).trim();
        }

        //logger.debug("THM@translateScenarioDescription::ScenarioTimeStamp: " + scenarioTimeStamp);

        //// 2. Finally parse and translate scenario TimeStamp.
        //// Use the standard tokens, i.e. white spaces now.

        String month = "";
        String day = "";
        String year = "";
        String at = "";
        String hourmin = "";
        String timezone = "";
        int counter = 1;

        TreeMap timeParsedTimestmp = new TreeMap();
        StringTokenizer st1 = new StringTokenizer(scenarioTimeStamp);

        while (st1.hasMoreTokens())
        {
           timeParsedTimestmp.put (new Integer(counter), st1.nextToken());
           counter++;
        }

        String on = BXResources.getGenericMsg("ON_LABEL",  langId);

        resultBuf.append(on);
        resultBuf.append(" ");

        Set keys = timeParsedTimestmp.entrySet();
        Iterator iter = keys.iterator();

        while(iter.hasNext())
        {
          Map.Entry entry = (Map.Entry) iter.next();
          Object key = entry.getKey();
          Object value = entry.getValue();

          int translate = ((Integer)key).intValue();

          switch(translate)
          {
            case 1: month = translateMonth (value.toString(), langId);
            case 2: day = value.toString(); // no translation
            case 3: year = value.toString(); // no translation
            case 4: at = BXResources.getGenericMsg("AT_LABEL",  langId);
            case 5: hourmin = value.toString(); // no translation
            case 6: timezone = value.toString(); // no translation
           }
        }

        //// 3. Finish translation.
        resultBuf.append(month);
        resultBuf.append(" ");
        resultBuf.append(day);
        resultBuf.append(" ");
        resultBuf.append(year);
        resultBuf.append(" ");
        resultBuf.append(at);
        resultBuf.append(" ");
        resultBuf.append(hourmin);
        resultBuf.append(" ");
        resultBuf.append(timezone);

        //logger.debug("UWH@translateScenarioDescription:Buffer: " + resultBuf.toString());

        return resultBuf.toString();
      }
      else
        return scenarioIn;
		}
		catch(Exception e)
		{
			System.out.println("--D--> @translateScenarioDescription: in=<" + scenarioIn + "> - use empty scenario desc");
		}

    return scenarioIn;
	}

  private String translateMonth(String monthIn, int languageId)
  {
        String transMonth = "";

        try
        {
          if (monthIn.toUpperCase().trim().equals("JAN"))
          {
            transMonth = BXResources.getGenericMsg("JAN_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("FEB"))
          {
            transMonth = BXResources.getGenericMsg("FEB_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("MAR"))
          {
            transMonth = BXResources.getGenericMsg("MAR_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("APR"))
          {
            transMonth = BXResources.getGenericMsg("APR_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("MAY"))
          {
            transMonth = BXResources.getGenericMsg("MAY_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("JUN"))
          {
            transMonth = BXResources.getGenericMsg("JUN_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("JUL"))
          {
            transMonth = BXResources.getGenericMsg("JUL_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("AUG"))
          {
            transMonth = BXResources.getGenericMsg("AUG_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("SEP"))
          {
            transMonth = BXResources.getGenericMsg("SEP_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("OCT"))
          {
            transMonth = BXResources.getGenericMsg("OCT_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("NOV"))
          {
            transMonth = BXResources.getGenericMsg("NOV_LABEL",  languageId);
          }
          else if (monthIn.toUpperCase().trim().equals("DEC"))
          {
            transMonth = BXResources.getGenericMsg("DEC_LABEL",  languageId);
          }
        }
        catch(NumberFormatException e)
        {
          throw new IllegalArgumentException("UWH@translateScenarioDescription:Untranslated format");
        }
        return transMonth;
  }


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.
  public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT DEALCHANGEHISTORY.APPLICATIONID, DEALCHANGEHISTORY.TRANSACTIONDATE, DEALCHANGEHISTORY.CONTEXTTEXT, SUBSTR(CONTACT.CONTACTFIRSTNAME, 1, 1) || ', ' ||  SUBSTR(CONTACT.CONTACTLASTNAME,1,32) USERNAME, DEALCHANGEHISTORY.CONTEXTTEXTSOURCE, DEALCHANGEHISTORY.PREVIOUSVALUETEXT, DEALCHANGEHISTORY.CURRENTVALUETEXT, DISPLAYFIELD.ATTRIBUTENAME, DISPLAYFIELD.ENTITYNAME, CONTACT.COPYID, SUBSTR(DEALCHANGEHISTORY.TRANSACTIONDATE, 1, 15) FROM DEALCHANGEHISTORY, USERPROFILE, CONTACT, DISPLAYFIELD  __WHERE__  ORDER BY DEALCHANGEHISTORY.TRANSACTIONDATE  DESC";
  //--Release2.1--//
  //// Changed to translate the scenario descriptions.
	public static final String SELECT_SQL_TEMPLATE="SELECT DEALCHANGEHISTORY.APPLICATIONID, " +
  "DEALCHANGEHISTORY.TRANSACTIONDATE, DEALCHANGEHISTORY.CONTEXTTEXT, " +
  "SUBSTR(CONTACT.CONTACTFIRSTNAME, 1, 1) || ', ' ||  SUBSTR(CONTACT.CONTACTLASTNAME,1,32) SYNTHETICUSER, " +
  "DEALCHANGEHISTORY.CONTEXTTEXTSOURCE, DEALCHANGEHISTORY.PREVIOUSVALUETEXT PREVIOUSVALUETEXT, " +
	  "DEALCHANGEHISTORY.CURRENTVALUETEXT CURRENTVALUETEXT, DEALCHANGEHISTORY.ATTRIBUTENAME, " +
	  "DEALCHANGEHISTORY.ENTITYNAME, CONTACT.COPYID, " +
  "SUBSTR(DEALCHANGEHISTORY.TRANSACTIONDATE, 1, 15) SYNTHETICTRANSACDATE, DEALCHANGEHISTORY.INSTITUTIONPROFILEID " +
	  "FROM DEALCHANGEHISTORY, USERPROFILE, CONTACT  " +
  "__WHERE__  ORDER BY DEALCHANGEHISTORY.TRANSACTIONDATE  DESC";

	public static final String MODIFYING_QUERY_TABLE_NAME="DEALCHANGEHISTORY, USERPROFILE, CONTACT ";
	public static final String STATIC_WHERE_CRITERIA="(    (       (USERPROFILE.CONTACTID  =  CONTACT.CONTACTID)        AND         (DEALCHANGEHISTORY.DCHUSERPROFILEID  =  USERPROFILE.USERPROFILEID)      AND         (USERPROFILE.INSTITUTIONPROFILEID  =  CONTACT.INSTITUTIONPROFILEID)         AND         (DEALCHANGEHISTORY.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID))  AND         CONTACT.COPYID = 1  AND         rownum <= 200 ) "; //--> Set Max number of rows = 200
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEALCHANGEHISTORY.APPLICATIONID";
	public static final String COLUMN_DFDEALID="APPLICATIONID";
	public static final String QUALIFIED_COLUMN_DFTRANSACTIONDATE="DEALCHANGEHISTORY.TRANSACTIONDATE";
	public static final String COLUMN_DFTRANSACTIONDATE="TRANSACTIONDATE";
	public static final String QUALIFIED_COLUMN_DFCONTEXT="DEALCHANGEHISTORY.CONTEXTTEXT";
	public static final String COLUMN_DFCONTEXT="CONTEXTTEXT";
	////public static final String QUALIFIED_COLUMN_DFUSER=".";
	////public static final String COLUMN_DFUSER="";
	public static final String QUALIFIED_COLUMN_DFUSER="CONTACT.SYNTHETICUSER";
	public static final String COLUMN_DFUSER="SYNTHETICUSER";
	public static final String QUALIFIED_COLUMN_DFCONTEXTSOURCE="DEALCHANGEHISTORY.CONTEXTTEXTSOURCE";
	public static final String COLUMN_DFCONTEXTSOURCE="CONTEXTTEXTSOURCE";
	public static final String QUALIFIED_COLUMN_DFPREVIOUSVALUE="DEALCHANGEHISTORY.PREVIOUSVALUETEXT";
	public static final String COLUMN_DFPREVIOUSVALUE="PREVIOUSVALUETEXT";

	public static final String QUALIFIED_COLUMN_DFCURRENTVALUE="DEALCHANGEHISTORY.CURRENTVALUETEXT";
	public static final String COLUMN_DFCURRENTVALUE="CURRENTVALUETEXT";

	public static final String QUALIFIED_COLUMN_DFFIELD="DEALCHANGEHISTORY.ATTRIBUTENAME";
	public static final String COLUMN_DFFIELD="ATTRIBUTENAME";
	public static final String QUALIFIED_COLUMN_DFENTITY="DEALCHANGEHISTORY.ENTITYNAME";
	public static final String COLUMN_DFENTITY="ENTITYNAME";
	public static final String QUALIFIED_COLUMN_DFCOPYID="CONTACT.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	////public static final String QUALIFIED_COLUMN_DFTRANSACTIONDATE1=".";
	////public static final String COLUMN_DFTRANSACTIONDATE1="";
	public static final String QUALIFIED_COLUMN_DFTRANSACTIONDATE1="DEALCHANGEHISTORY.SYNTHETICTRANSACDATE";
	public static final String COLUMN_DFTRANSACTIONDATE1="SYNTHETICTRANSACDATE";
    public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
        "DEALCHANGEHISTORY.INSTITUTIONPROFILEID";
  public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTRANSACTIONDATE,
				COLUMN_DFTRANSACTIONDATE,
				QUALIFIED_COLUMN_DFTRANSACTIONDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTEXT,
				COLUMN_DFCONTEXT,
				QUALIFIED_COLUMN_DFCONTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFUSER,
		////		COLUMN_DFUSER,
		////		QUALIFIED_COLUMN_DFUSER,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjustment to fix the iMT tool bug for the ComputedColumn.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSER,
				COLUMN_DFUSER,
				QUALIFIED_COLUMN_DFUSER,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"SUBSTR(CONTACT.CONTACTFIRSTNAME, 1, 1) || ', ' ||  SUBSTR(CONTACT.CONTACTLASTNAME,1,32)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"SUBSTR(CONTACT.CONTACTFIRSTNAME, 1, 1) || ', ' ||  SUBSTR(CONTACT.CONTACTLASTNAME,1,32)"));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCONTEXTSOURCE,
				COLUMN_DFCONTEXTSOURCE,
				QUALIFIED_COLUMN_DFCONTEXTSOURCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREVIOUSVALUE,
				COLUMN_DFPREVIOUSVALUE,
				QUALIFIED_COLUMN_DFPREVIOUSVALUE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCURRENTVALUE,
				COLUMN_DFCURRENTVALUE,
				QUALIFIED_COLUMN_DFCURRENTVALUE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFFIELD,
				COLUMN_DFFIELD,
				QUALIFIED_COLUMN_DFFIELD,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFENTITY,
				COLUMN_DFENTITY,
				QUALIFIED_COLUMN_DFENTITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFTRANSACTIONDATE1,
		////		COLUMN_DFTRANSACTIONDATE1,
		////		QUALIFIED_COLUMN_DFTRANSACTIONDATE1,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjustment to fix the iMT tool bug for the ComputedColumn.
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTRANSACTIONDATE1,
				COLUMN_DFTRANSACTIONDATE1,
				QUALIFIED_COLUMN_DFTRANSACTIONDATE1,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
				"SUBSTR(DEALCHANGEHISTORY.TRANSACTIONDATE, 1, 15)",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"SUBSTR(DEALCHANGEHISTORY.TRANSACTIONDATE, 1, 15)"));

    FIELD_SCHEMA.addFieldDescriptor(
            new QueryFieldDescriptor(
              FIELD_DFINSTITUTIONID,
              COLUMN_DFINSTITUTIONID,
              QUALIFIED_COLUMN_DFINSTITUTIONID,
              java.math.BigDecimal.class,
              false,
              false,
              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
              "",
              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
              ""));
	}

}

