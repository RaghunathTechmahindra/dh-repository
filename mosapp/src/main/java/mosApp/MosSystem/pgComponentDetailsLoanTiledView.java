package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.event.TiledViewRequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * Description: pgComponentDetailsLoanTiledView class is used to display data 
 * from the doComponentLoanModel Model.
 * 
 * @author: MCM Impl Team.
 * @version 1.0 2008-6-17
 * @version 1.1 July 11, 2009: artf741023: Added line separator and section title.
 * @version 1.2 delete Component section - XS_2.32 Modified - handleBtDeleteLoanCompRequest()
 * @version 1.4 July 25, 2008 set MAXCOMPTILE_SIZE = 10 for artf750856 
 */
public class pgComponentDetailsLoanTiledView extends
RequestHandlingTiledViewBase implements TiledView, RequestHandler {

    static final Log _log = LogFactory
    .getLog(pgComponentDetailsLoanTiledView.class);

    /**
     * Description: This Constructor registers the Childs and sets the default
     * URL.
     * 
     */
    public pgComponentDetailsLoanTiledView(View parent, String name) {
        super(parent, name);
        // MCM team for artf750856
        setMaxDisplayTiles(pgComponentDetailsViewBean.MAXCOMPTILE_SIZE);
        setPrimaryModelClass(doComponentLoanModel.class);
        registerChildren();
        initialize();
    }

    /**
     * Description: This method currently has no implementation but can be used
     * to do initializations
     * 
     */
    protected void initialize() {
    }

    /**
     * Description: This method registers the children to the framework.
     * 
     */
    public void resetChildren() {
        getCbComponentType().setValue(CHILD_CBCOMPONENTTYPE_RESET_VALUE);
        getCbProductType().setValue(CHILD_CBPRODUCTTYPE_RESET_VALUE);
        getTbLoanAmount().setValue(CHILD_TBLOANAMOUNT_RESET_VALUE);
        getCbPostedInterestRate().setValue(
                CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE);
        getTbPaymentTermDescription().setValue(
                CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE);
        getTbDiscount().setValue(CHILD_TBDISCOUNT_RESET_VALUE);
        getTxActualPaymentTermMonths().setValue(
                CHILD_TXACTUALPAYMENTTERMMONTHS_RESET_VALUE);
        getTxActualPaymentTermYears().setValue(
                CHILD_TXACTUALPAYMENTTERMYEARS_RESET_VALUE);
        getTbPremium().setValue(CHILD_TBPREMIUM_RESET_VALUE);
        getCbPaymentFrequency().setValue(CHILD_CBPAYMENTFREQUENCY_RESET_VALUE);
        getStNetRate().setValue(CHILD_STNETRATE_RESET_VALUE);
        getStTotalPayment().setValue(CHILD_STTOTALPAYMENT_RESET_VALUE);
        getTbAdditonalInfo().setValue(CHILD_TBADDITIONALINFO_RESET_VALUE);
        getBtRecalcLOCComp().setValue(CHILD_BTRECALCLOCCOMP_RESET_VALUE);
        getBtDeleteLOCComp().setValue(CHILD_BTDELETELOCCOMP_RESET_VALUE);
        getComponentId().setValue(CHILD_HDCOMPONENTID_RESET_VALUE);
        getCopyId().setValue(CHILD_HDCOPYID_RESET_VALUE);
        /***************MCM Impl team changes starts - XS_2.37(dynamic drop down list)*********************/
        getStPostedInterestRate().setValue(CHILD_STINTERESTRATE_RESET_VALUE);
        getStMtgProduct().setValue(CHILD_STMTGPRODUCT_RESET_VALUE);
        getStInitDyanmicListJavaScript().setValue(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE);
        getTxPostedRate().setValue(CHILD_TXPOSTEDRATE_RESET_VALUE);
        /***************MCM Impl team changes ends - XS_2.37(dynamic drop down list)*********************/
        getStTitle().setValue(CHILD_STTITLE_RESET_VALUE);
        getImSectionDevider().setValue(CHILD_IMSECTIONDEVIDER_RESET_VALUE);
        
    }

    /**
     * Description: This method registers the children to the framework.
     * 
     */
    protected void registerChildren() {

        registerChild(CHILD_CBCOMPONENTTYPE, ComboBox.class);
        registerChild(CHILD_CBPRODUCTTYPE, ComboBox.class);

        registerChild(CHILD_TBLOANAMOUNT, TextField.class);
        registerChild(CHILD_CBPOSTEDINTERESTRATE, ComboBox.class);
        registerChild(CHILD_TBPAYMENTTERMDESCRIPTION, TextField.class);
        registerChild(CHILD_TBDISCOUNT, TextField.class);
        registerChild(CHILD_TXACTUALPAYMENTTERMMONTHS, TextField.class);
        registerChild(CHILD_TXACTUALPAYMENTTERMYEARS, TextField.class);
        registerChild(CHILD_TBPREMIUM, TextField.class);
        registerChild(CHILD_CBPAYMENTFREQUENCY, ComboBox.class);
        registerChild(CHILD_STNETRATE, StaticTextField.class);
        registerChild(CHILD_STTOTALPAYMENT, StaticTextField.class);
        registerChild(CHILD_TBADDITIONALINFO, TextField.class);
        registerChild(CHILD_BTRECALCLOCCOMP, Button.class);
        registerChild(CHILD_BTDELETELOCCOMP, Button.class);
        registerChild(CHILD_HDCOMPONENTID,HiddenField.class);
        registerChild(CHILD_HDCOPYID,HiddenField.class);
        /***************MCM Impl team changes starts - XS_2.37(dynamic drop down list)*********************/
        registerChild(CHILD_STINTERESTRATE, TextField.class);
        registerChild(CHILD_STMTGPRODUCT, TextField.class);
        registerChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT, StaticTextField.class);
        registerChild(CHILD_TXPOSTEDRATE, TextField.class);
        /***************MCM Impl team changes ends - XS_2.37(dynamic drop down list)*********************/
        registerChild(CHILD_STTITLE, StaticTextField.class);
        registerChild(CHILD_IMSECTIONDEVIDER, ImageField.class);
    }

    /**
     * Description: This method adds the model to the Model List.
     * 
     * @param executionType
     */
    public Model[] getWebActionModels(int executionType) {
        List modelList = new ArrayList();
        switch (executionType) {
        case MODEL_TYPE_RETRIEVE:
            modelList.add(getdoComponentLoanModel());
            ;
            break;

        case MODEL_TYPE_UPDATE:
            ;
            break;

        case MODEL_TYPE_DELETE:
            ;
            break;

        case MODEL_TYPE_INSERT:
            ;
            break;

        case MODEL_TYPE_EXECUTE:
            ;
            break;
        }
        return (Model[]) modelList.toArray(new Model[0]);
    }

    /**
     * Description: This method creates the handler objects and saves the page
     * state and resets the Tile index
     * 
     * @param event
     */
    public void beginDisplay(DisplayEvent event) throws ModelControlException {

        // This is the analog of NetDynamics repeated_onBeforeDisplayEvent
        // Ensure the primary model is non-null; if null, it would cause havoc
        // with the various methods dependent on the primary model

        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
        .cloneSS();

        handler.pageGetState(this.getParentViewBean());

        if (getPrimaryModel() == null)
            throw new ModelControlException("Primary model is null");
        // Set up all ComboBox options

        cbComponentTypeOptions.populate(getRequestContext());
        cbPaymentFrequencyOptions.populate(getRequestContext());
        super.beginDisplay(event);
        resetTileIndex();
    }

    /**
     * 
     * 
     */

    public boolean nextTile() throws ModelControlException {

        boolean movedToRow = super.nextTile();
        if (movedToRow) {
            ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
            .cloneSS();
            handler.pageGetState(this.getParentViewBean());
            handler.setComponentLoanDisplayFields(getTileIndex());
            handler.pageSaveState();
        }
        return movedToRow;
    }

    /**
     * Description: This method does necessary preparation to the page before
     * model executes.
     * 
     * @param model
     * @param executionContext
     */
    public boolean beforeModelExecutes(Model model, int executionContext) {

        // This is the analog of NetDynamics
        // repeated_onBeforeDataObjectExecuteEvent
        return super.beforeModelExecutes(model, executionContext);

    }

    /**
     * Description: This method call the afterModelExecutes method of the super
     * class.
     * 
     * @param model 
     * @param executionContext 
     */
    public void afterModelExecutes(Model model, int executionContext) {

        // This is the analog of NetDynamics
        // repeated_onAfterDataObjectExecuteEvent
        super.afterModelExecutes(model, executionContext);

    }

    /**
     * Description: This method displays the fields on the page after executing
     * the model.
     * 
     * @param executionContext
     */
    public void afterAllModelsExecute(int executionContext) {

        // This is the analog of NetDynamics
        // repeated_onAfterAllDataObjectsExecuteEvent
        super.afterAllModelsExecute(executionContext);

    }

    /**
     * Description: This method calls the onModelError method of the super
     * class.
     * 
     * @param model
     * @param executionContext
     * @param exception
     */
    public void onModelError(Model model, int executionContext,
            ModelControlException exception) throws ModelControlException {

        // This is the analog of NetDynamics repeated_onDataObjectErrorEvent
        super.onModelError(model, executionContext, exception);

    }

    /**
     * Description: This method creates the Children.
     * 
     * @param name
     */
    protected View createChild(String name) {

        if (name.equals(CHILD_CBCOMPONENTTYPE)) {
            ComboBox child = new ComboBox(this, getdoComponentLoanModel(),
                    CHILD_CBCOMPONENTTYPE,
                    doComponentLoanModel.FIELD_DFCOMPONENTTYPEID,
                    CHILD_CBCOMPONENTTYPE_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbComponentTypeOptions);
            return child;
        } else if (name.equals(CHILD_CBPRODUCTTYPE)) {
            ComboBox child = new ComboBox(this, 
                    getdoComponentLoanModel(),
                    CHILD_CBPRODUCTTYPE,
                    doComponentLoanModel.FIELD_DFPRODUCTID,
                    CHILD_CBPRODUCTTYPE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBLOANAMOUNT)) {
            TextField child = new TextField(this, getdoComponentLoanModel(),
                    CHILD_TBLOANAMOUNT,
                    doComponentLoanModel.FIELD_DFLOANAMOUNT,
                    CHILD_TBLOANAMOUNT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_CBPOSTEDINTERESTRATE)) {
            ComboBox child = new ComboBox(this, 
                    getdoComponentLoanModel(),
                    CHILD_CBPOSTEDINTERESTRATE, 
                    doComponentLoanModel.FIELD_DFPRICINGRATEINVENTORYID,
                    CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBPAYMENTTERMDESCRIPTION)) {
            TextField child = new TextField(this, getdoComponentLoanModel(),
                    CHILD_TBPAYMENTTERMDESCRIPTION,
                    doComponentLoanModel.FIELD_DFPAYMENTTERMDESCRIPTION,
                    CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBDISCOUNT)) {
            TextField child = new TextField(this, getdoComponentLoanModel(),
                    CHILD_TBDISCOUNT, doComponentLoanModel.FIELD_DFDISCOUNT,
                    CHILD_TBDISCOUNT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TXACTUALPAYMENTTERMMONTHS)) {
            TextField child = new TextField(this, getdoComponentLoanModel(),
                    CHILD_TXACTUALPAYMENTTERMMONTHS,
                    CHILD_TXACTUALPAYMENTTERMMONTHS,
                    CHILD_TXACTUALPAYMENTTERMMONTHS_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TXACTUALPAYMENTTERMYEARS)) {
            TextField child = new TextField(this, getdoComponentLoanModel(),
                    CHILD_TXACTUALPAYMENTTERMYEARS,
                    CHILD_TXACTUALPAYMENTTERMYEARS,
                    CHILD_TXACTUALPAYMENTTERMYEARS_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBPREMIUM)) {
            TextField child = new TextField(this, getdoComponentLoanModel(),
                    CHILD_TBPREMIUM, doComponentLoanModel.FIELD_DFPREMIUM,
                    CHILD_TBPREMIUM_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_CBPAYMENTFREQUENCY)) {
            ComboBox child = new ComboBox(this, getdoComponentLoanModel(),
                    CHILD_CBPAYMENTFREQUENCY,
                    doComponentLoanModel.FIELD_DFPAYMENTFREQUENCYID,
                    CHILD_CBPAYMENTFREQUENCY_RESET_VALUE, null);
            child.setLabelForNoneSelected("");
            child.setOptions(cbPaymentFrequencyOptions);
            return child;
        } else if (name.equals(CHILD_STNETRATE)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentLoanModel(), CHILD_STNETRATE,
                    doComponentLoanModel.FIELD_DFNETINTRESTRATE,
                    CHILD_STNETRATE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STTOTALPAYMENT)) {
            StaticTextField child = new StaticTextField(this,
                    getdoComponentLoanModel(), CHILD_STTOTALPAYMENT,
                    doComponentLoanModel.FIELD_DFTOTALPAYMENT,
                    CHILD_STTOTALPAYMENT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_TBADDITIONALINFO)) {
            TextField child = new TextField(this, getdoComponentLoanModel(),
                    CHILD_TBADDITIONALINFO,
                    doComponentLoanModel.FIELD_DFADDITIONALDETAILS,
                    CHILD_TBADDITIONALINFO_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_BTRECALCLOCCOMP)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTRECALCLOCCOMP, CHILD_BTRECALCLOCCOMP,
                    CHILD_BTRECALCLOCCOMP_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_BTDELETELOCCOMP)) {
            Button child = new Button(this, getDefaultModel(),
                    CHILD_BTDELETELOCCOMP, CHILD_BTDELETELOCCOMP,
                    CHILD_BTDELETELOCCOMP_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_HDCOMPONENTID)) {
            HiddenField child = new HiddenField(this,
                    getdoComponentLoanModel(), CHILD_HDCOMPONENTID,
                    doComponentLoanModel.FIELD_DFCOMPONENTID,
                    CHILD_HDCOMPONENTID_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_HDCOPYID)) {
            HiddenField child = new HiddenField(this,
                    getdoComponentLoanModel(), CHILD_HDCOPYID,
                    doComponentLoanModel.FIELD_DFCOPYID,
                    CHILD_HDCOPYID_RESET_VALUE, null);
            return child;

        }
        /***************MCM Impl team changes starts - XS_2.37(dynamic drop down list)*********************/
        else if (name.equals(CHILD_STMTGPRODUCT)) {
            TextField child = new TextField(this, 
                    getDefaultModel(),
                    CHILD_STMTGPRODUCT, 
                    CHILD_STMTGPRODUCT,
                    CHILD_STMTGPRODUCT_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_STINTERESTRATE)) {
            TextField child = new TextField(this, 
                    getDefaultModel(),
                    CHILD_STINTERESTRATE, 
                    CHILD_STINTERESTRATE,
                    CHILD_STINTERESTRATE_RESET_VALUE, null);
            return child;
        } else if (name.equals(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), 
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT,
                    CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE, null);
            return child;
        }
        else if (name.equals(CHILD_TXPOSTEDRATE)) {
            TextField child =
                new TextField(this, getdoComponentLoanModel(),
                        CHILD_TXPOSTEDRATE,
                        doComponentLoanModel.FIELD_DFPOSTEDRATE,
                        CHILD_TXPOSTEDRATE_RESET_VALUE, null);
            return child;
        }
        /***************MCM Impl team changes ends - XS_2.37(dynamic drop down list)*********************/
        else if (name.equals(CHILD_STTITLE)) {
            StaticTextField child = new StaticTextField(this,
                    getDefaultModel(), CHILD_STTITLE, CHILD_STTITLE,
                    CHILD_STTITLE, null);
            return child;
        } else if (name.equals(CHILD_IMSECTIONDEVIDER)) {
            ImageField child = new ImageField(this, getDefaultModel(),
                    CHILD_IMSECTIONDEVIDER, CHILD_IMSECTIONDEVIDER,
                    CHILD_IMSECTIONDEVIDER, null);
            return child;
        }
        else throw new IllegalArgumentException("Invalid child name [" + name + "]");
    }

    public ComboBox getCbProductType() {
        return (ComboBox) getChild(CHILD_CBCOMPONENTTYPE);
    }

    public ComboBox getCbComponentType() {
        return (ComboBox) getChild(CHILD_CBPRODUCTTYPE);
    }

    public TextField getTbLoanAmount() {
        return (TextField) getChild(CHILD_TBLOANAMOUNT);
    }

    public ComboBox getCbPostedInterestRate() {
        return (ComboBox) getChild(CHILD_CBPOSTEDINTERESTRATE);
    }

    public TextField getTbPaymentTermDescription() {
        return (TextField) getChild(CHILD_TBPAYMENTTERMDESCRIPTION);
    }

    public TextField getTbDiscount() {
        return (TextField) getChild(CHILD_TBDISCOUNT);
    }

    public TextField getTxActualPaymentTermMonths() {
        return (TextField) getChild(CHILD_TXACTUALPAYMENTTERMMONTHS);
    }

    public TextField getTxActualPaymentTermYears() {
        return (TextField) getChild(CHILD_TXACTUALPAYMENTTERMYEARS);
    }

    public TextField getTbPremium() {
        return (TextField) getChild(CHILD_TBPREMIUM);
    }

    public ComboBox getCbPaymentFrequency() {
        return (ComboBox) getChild(CHILD_CBPAYMENTFREQUENCY);
    }

    public StaticTextField getStNetRate() {
        return (StaticTextField) getChild(CHILD_STNETRATE);
    }

    public StaticTextField getStTotalPayment() {
        return (StaticTextField) getChild(CHILD_STTOTALPAYMENT);
    }

    public TextField getTbAdditonalInfo() {
        return (TextField) getChild(CHILD_TBADDITIONALINFO);
    }

    public Button getBtRecalcLOCComp() {
        return (Button) getChild(CHILD_BTRECALCLOCCOMP);
    }

    public Button getBtDeleteLOCComp() {
        return (Button) getChild(CHILD_BTDELETELOCCOMP);
    }

    public HiddenField getComponentId() {
        return (HiddenField) getChild(CHILD_HDCOMPONENTID);
    }

    public HiddenField getCopyId() {
        return (HiddenField) getChild(CHILD_HDCOPYID);
    }


    /**
     * <p>handleBtRecalcLOCRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @author MCM Team
     * -- XS 2.27
     */
    public void handleBtRecalcLoanCompRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.trace("Web event invoked: " + getClass().getName()
                + ".BtRecalcLoanComp");
        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
        .cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        handler.handleRecalc();
        handler.postHandlerProtocol();
        return;
    }

    /**
     * <p>handleBtRecalcLOCRequest</p>
     * <p>Description: Handle clicking Recalclate</p>
     * @param event: RequestInvocationEvent
     * @return 
     * @version 1.0 -- XS 2.27
     * @version 1.1 XS 2.32 delete component
     * @author MCM Team
     */
    public void handleBtDeleteLoanCompRequest(RequestInvocationEvent event)
    throws ServletException, IOException {
        _log.trace("Web event invoked: " + getClass().getName()
                + ".BtDeleteLOC");
        ComponentDetailsHandler handler = (ComponentDetailsHandler) this.handler
        .cloneSS();
        handler.preHandlerProtocol(this.getParentViewBean());
        int tileIndx = ((TiledViewRequestInvocationEvent)(event)).getTileNumber();
        handler.handleDeleteLoanComp(tileIndx);
        handler.postHandlerProtocol();
        return;
    }

    /***************MCM Impl team changes starts - XS_2.37(dynamic drop down list)*********************/
    public TextField getStPostedInterestRate() {
        return (TextField) getChild(CHILD_STINTERESTRATE);
    }

    public TextField getStMtgProduct() {
        return (TextField) getChild(CHILD_STMTGPRODUCT);
    }

    public StaticTextField getStInitDyanmicListJavaScript() {
        return (StaticTextField) getChild(CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT);
    }

    public TextField getTxPostedRate() {
        return (TextField) getChild(CHILD_TXPOSTEDRATE);
    }
    /***************MCM Impl team changes ends - XS_2.37(dynamic drop down list)*********************/

    public StaticTextField getStTitle() {
        return (StaticTextField) getChild(CHILD_STTITLE);
    }
    
    public ImageField getImSectionDevider() {
        return (ImageField) getChild(CHILD_IMSECTIONDEVIDER);
    }
    /**
     * Description: This method creates the model.
     *
     * @return
     */
    public doComponentLoanModel getdoComponentLoanModel() {
        if (doComponentDetails == null)
            doComponentDetails = (doComponentLoanModel) getModel(doComponentLoanModel.class);
        return doComponentDetails;
    }

    /**
     * Description: This method sets the model.
     *
     * @param model
     */
    public void setdoComponentLoanModel(doComponentLoanModel model) {
        doComponentDetails = model;
    }

    public static Map FIELD_DESCRIPTORS;

    public static final String CHILD_CBCOMPONENTTYPE = "cbComponentTypeLoan";

    public static final String CHILD_CBCOMPONENTTYPE_RESET_VALUE = "";

    public static final String CHILD_CBPRODUCTTYPE = "cbProductTypeLoan";

    public static final String CHILD_CBPRODUCTTYPE_RESET_VALUE = "";

    public static final String CHILD_TBLOANAMOUNT = "tbLoanAmount";

    public static final String CHILD_TBLOANAMOUNT_RESET_VALUE = "";

    public static final String CHILD_CBPOSTEDINTERESTRATE = "cbPostedInterestRateLoan";

    public static final String CHILD_CBPOSTEDINTERESTRATE_RESET_VALUE = "";

    public static final String CHILD_TBPAYMENTTERMDESCRIPTION = "tbPaymentTermDescriptionLoan";

    public static final String CHILD_TBPAYMENTTERMDESCRIPTION_RESET_VALUE = "";

    public static final String CHILD_TBDISCOUNT = "tbDiscountLoan";

    public static final String CHILD_TBDISCOUNT_RESET_VALUE = "";

    public static final String CHILD_TXACTUALPAYMENTTERMMONTHS = "txActualPayMonthsLoan";

    public static final String CHILD_TXACTUALPAYMENTTERMMONTHS_RESET_VALUE = "";

    public static final String CHILD_TXACTUALPAYMENTTERMYEARS = "txActualPayYearsLoan";

    public static final String CHILD_TXACTUALPAYMENTTERMYEARS_RESET_VALUE = "";

    public static final String CHILD_TBPREMIUM = "tbPremiumLoan";

    public static final String CHILD_TBPREMIUM_RESET_VALUE = "";

    public static final String CHILD_CBPAYMENTFREQUENCY = "cbPaymentFrequencyLoan";

    public static final String CHILD_CBPAYMENTFREQUENCY_RESET_VALUE = "";

    public static final String CHILD_STNETRATE = "stNetRateLoan";

    public static final String CHILD_STNETRATE_RESET_VALUE = "";

    public static final String CHILD_STTOTALPAYMENT = "stTotalPaymentLoan";

    public static final String CHILD_STTOTALPAYMENT_RESET_VALUE = "";

    public static final String CHILD_TBADDITIONALINFO = "tbAdditionalInfoLoan";

    public static final String CHILD_TBADDITIONALINFO_RESET_VALUE = "";

    public static final String CHILD_BTRECALCLOCCOMP = "btRecalcLoanComp";

    public static final String CHILD_BTRECALCLOCCOMP_RESET_VALUE = "N";

    public static final String CHILD_BTDELETELOCCOMP = "btDeleteLoanComp";

    public static final String CHILD_BTDELETELOCCOMP_RESET_VALUE = "N";

    public static final String CHILD_HDCOMPONENTID="hdComponentIDLoan";
    public static final String CHILD_HDCOMPONENTID_RESET_VALUE="";

    public static final String CHILD_HDCOPYID="hdCopyIDLoan";
    public static final String CHILD_HDCOPYID_RESET_VALUE="";

    /***************MCM Impl team changes starts - XS_2.37(dynamic drop down list)*******************/

    public static final String CHILD_STMTGPRODUCT = "stMtgProduct";
    public static final String CHILD_STMTGPRODUCT_RESET_VALUE = "";

    public static final String CHILD_STINTERESTRATE = "stInterestRate";
    public static final String CHILD_STINTERESTRATE_RESET_VALUE = "";

    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT = "stInitJavaScript"; 
    public static final String CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT_RESET_VALUE = "";

    public static final String CHILD_TXPOSTEDRATE = "txPostedRate";
    public static final String CHILD_TXPOSTEDRATE_RESET_VALUE = "";

    public static final String CHILD_STTITLE = "stTitle";
    public static final String CHILD_STTITLE_RESET_VALUE = "";    

    public static final String CHILD_IMSECTIONDEVIDER = "imSectionDevider";
    public static final String CHILD_IMSECTIONDEVIDER_RESET_VALUE = "";

    /***************MCM Impl team changes ends - XS_2.37(dynamic drop down list)*********************/

    private doComponentLoanModel doComponentDetails = null;

    private ComponentDetailsHandler handler = new ComponentDetailsHandler();

    private CbComponentTypeOptionList cbComponentTypeOptions = new CbComponentTypeOptionList();

    private CbPaymentFrequencyOptionList cbPaymentFrequencyOptions = new CbPaymentFrequencyOptionList();

    /**
     * CbComponentTypeOptionList
     * inner Class for COMPONENTTYPE comboBox
     * 
     */
    static class CbComponentTypeOptionList extends OptionList {

        CbComponentTypeOptionList() {
        }

        public void populate(RequestContext rc) {
            try {
                String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
                SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
                int languageId = theSessionState.getLanguageId();

                // Get IDs and Labels from BXResources
                Collection c = BXResources.getPickListValuesAndDesc(
                        theSessionState.getDealInstitutionId(),
                        "COMPONENTTYPE", languageId);
                Iterator l = c.iterator();
                String[] theVal = new String[2];
                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * CbPaymentFrequencyOptionList
     * inner Class for PAYMENTFREQUENCY comboBox
     * 
     */
    static class CbPaymentFrequencyOptionList extends OptionList {

        CbPaymentFrequencyOptionList() {
        }

        public void populate(RequestContext rc) {
            try {
                String defaultInstanceStateName = rc.getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
                SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
                int languageId = theSessionState.getLanguageId();

                // Get IDs and Labels from BXResources
                Collection c = BXResources.getPickListValuesAndDesc(
                        theSessionState.getDealInstitutionId(),
                        "PAYMENTFREQUENCY", languageId);
                Iterator l = c.iterator();
                String[] theVal = new String[2];
                while (l.hasNext()) {
                    theVal = (String[]) (l.next());
                    add(theVal[1], theVal[0]);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}
