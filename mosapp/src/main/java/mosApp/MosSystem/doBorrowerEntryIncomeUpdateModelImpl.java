package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doBorrowerEntryIncomeUpdateModelImpl extends QueryModelBase
	implements doBorrowerEntryIncomeUpdateModel
{
	/**
	 *
	 *
	 */
	public doBorrowerEntryIncomeUpdateModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMETYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMETYPEID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomePeriodId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEPERIODID);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomePeriodId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEPERIODID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludingGDS()
	{
		return (String)getValue(FIELD_DFINCLUDINGGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludingGDS(String value)
	{
		setValue(FIELD_DFINCLUDINGGDS,value);
	}


	/**
	 *
	 *
	 */
	public String getDfIncludingTDS()
	{
		return (String)getValue(FIELD_DFINCLUDINGTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfIncludingTDS(String value)
	{
		setValue(FIELD_DFINCLUDINGTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfIncomeAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfIncomeAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFINCOMEAMOUNT,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="";
	public static final String MODIFYING_QUERY_TABLE_NAME="INCOME";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="INCOME.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFINCOMEID="INCOME.INCOMEID";
	public static final String COLUMN_DFINCOMEID="INCOMEID";
	public static final String QUALIFIED_COLUMN_DFINCOMETYPEID="INCOME.INCOMETYPEID";
	public static final String COLUMN_DFINCOMETYPEID="INCOMETYPEID";
	public static final String QUALIFIED_COLUMN_DFINCOMEPERIODID="INCOME.INCOMEPERIODID";
	public static final String COLUMN_DFINCOMEPERIODID="INCOMEPERIODID";
	public static final String QUALIFIED_COLUMN_DFINCLUDINGGDS="INCOME.INCLUDEINGDS";
	public static final String COLUMN_DFINCLUDINGGDS="INCLUDEINGDS";
	public static final String QUALIFIED_COLUMN_DFINCLUDINGTDS="INCOME.INCLUDEINTDS";
	public static final String COLUMN_DFINCLUDINGTDS="INCLUDEINTDS";
	public static final String QUALIFIED_COLUMN_DFINCOMEAMOUNT="INCOME.INCOMEAMOUNT";
	public static final String COLUMN_DFINCOMEAMOUNT="INCOMEAMOUNT";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEID,
				COLUMN_DFINCOMEID,
				QUALIFIED_COLUMN_DFINCOMEID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMETYPEID,
				COLUMN_DFINCOMETYPEID,
				QUALIFIED_COLUMN_DFINCOMETYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEPERIODID,
				COLUMN_DFINCOMEPERIODID,
				QUALIFIED_COLUMN_DFINCOMEPERIODID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDINGGDS,
				COLUMN_DFINCLUDINGGDS,
				QUALIFIED_COLUMN_DFINCLUDINGGDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCLUDINGTDS,
				COLUMN_DFINCLUDINGTDS,
				QUALIFIED_COLUMN_DFINCLUDINGTDS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFINCOMEAMOUNT,
				COLUMN_DFINCOMEAMOUNT,
				QUALIFIED_COLUMN_DFINCOMEAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

