package mosApp.MosSystem;

import java.io.Serializable;

/**
 * Should be re-implemented as follows.
 * public class PageCursorInfo extends ResultSetModelBase
 */
public class PageCursorInfo extends Object implements Serializable
{
	private String name;
	private String voName;
	private int rowsPerPage;
	private int totalRows=- 1;
	private boolean totalRowsChanged=false;
	private boolean refresh=true;
	private int currPageNdx=0;
	private int relSelectedRowNdx=0;
	private int absSelectedRowNdx=0;
	private boolean lastPage=true;
	private int sortOption=0;
	private boolean[] filterOption=new boolean[10];
	private int filterOptionId=1;
	private String criteria="";
	private boolean criteriaChanged=true;


	/**
	 *
	 *
	 */
	public PageCursorInfo(String name, String voName, int rowsPerPage, int totalRows)
	{
		this.name = name;

		this.voName = voName;

		this.rowsPerPage = rowsPerPage;

		setTotalRows(totalRows);

	}


	/**
	 *
	 *
	 */
	public boolean nextPage()
	{
		if (isLastPage()) return false;

		setCurrPageNdx(currPageNdx + 1);

		return true;

	}


	/**
	 *
	 *
	 */
	public boolean prevPage()
	{
		if (isFirstPage()) return false;

		setCurrPageNdx(currPageNdx - 1);

		return true;

	}


	/**
	 *
	 *
	 */
	public boolean isFirstPage()
	{
		return currPageNdx == 0;

	}


	/**
	 *
	 *
	 */
	public boolean isLastPage()
	{
		return((currPageNdx + 1) * rowsPerPage) >= totalRows;

	}


	/**
	 *
	 *
	 */
	public int getFirstDisplayRowNumber()
	{
		return 1 +(currPageNdx * rowsPerPage);

	}


	/**
	 *
	 *
	 */
	public int getLastDisplayRowNumber()
	{
		return getLastDisplayRowNumber(getFirstDisplayRowNumber());

	}


	/**
	 *
	 *
	 */
	public int getLastDisplayRowNumber(int firstDisplayRowNumber)
	{
		int rc = firstDisplayRowNumber + rowsPerPage - 1;

		if (rc > totalRows) return totalRows;

		return rc;

	}


	/**
	 *
	 *
	 */
	public int getNumberOfDisplayRowsOnPage()
	{
		if (! isLastPage()) return rowsPerPage;

		return 1 +((totalRows - 1) % rowsPerPage);

	}


	/**
	 *
	 *
	 */
	public String getName()
	{
		return name;

	}


	/**
	 *
	 *
	 */
	public String getVoName()
	{
		return voName;

	}


	/**
	 *
	 *
	 */
	public boolean getRefresh()
	{
		return refresh;

	}


	/**
	 *
	 *
	 */
	public void setRefresh(boolean b)
	{
		refresh = b;

	}


	/**
	 *
	 *
	 */
	public int getRowsPerPage()
	{
		return rowsPerPage;

	}


	/**
	 *
	 *
	 */
	public void setRowsPerPage(int num)
	{
		rowsPerPage = num;

	}


	/**
	 *
	 *
	 */
	public int getTotalRows()
	{
		return totalRows;

	}


	/**
	 *
	 *
	 */
	public void setTotalRows(int num)
	{

		// revise number of rows changed flag
		if (totalRows == - 1) totalRowsChanged = false;

		// total never set! don't consider initial a change
		else totalRowsChanged = num != totalRows;

		totalRows = num;

	}


	/**
	 *
	 *
	 */
	public boolean isTotalRowsChanged()
	{
		return totalRowsChanged;

	}


	/**
	 *
	 *
	 */
	public void setTotalRowsChanged(boolean b)
	{
		totalRowsChanged = b;

	}


	/**
	 *
	 *
	 */
	public int getCurrPageNdx()
	{
		return currPageNdx;

	}


	/**
	 *
	 *
	 */
	public void setCurrPageNdx(int ndx)
	{
		currPageNdx = ndx;

		setRelSelectedRowNdx(0);

	}


	/**
	 *
	 *
	 */
	public int getRelSelectedRowNdx()
	{
		return relSelectedRowNdx;

	}


	/**
	 *
	 *
	 */
	public void setRelSelectedRowNdx(int ndx)
	{
		relSelectedRowNdx = ndx;

		absSelectedRowNdx = ndx + rowsPerPage * currPageNdx;

	}


	/**
	 *
	 *
	 */
	public int getAbsSelectedRowNdx()
	{
		return absSelectedRowNdx;

	}


	/**
	 *
	 *
	 */
	public void setAbsSelectedRowNdx(int ndx)
	{
		absSelectedRowNdx = ndx;

		if (ndx == 0)
		{
			currPageNdx = 0;
			relSelectedRowNdx = 0;
			return;
		}

		currPageNdx = ndx / rowsPerPage;

		relSelectedRowNdx = ndx % rowsPerPage;

	}


	/**
	 *
	 *
	 */
	public int getSortOption()
	{
		return sortOption;

	}


	/**
	 *
	 *
	 */
	public void setSortOption(int opt)
	{
		sortOption = opt;

	}


	/**
	 *
	 *
	 */
	public int getFilterOptionId()
	{
		return filterOptionId;

	}


	/**
	 *
	 *
	 */
	public void setFilterOptionId(int optId)
	{
		filterOptionId = optId;

	}


	/**
	 *
	 *
	 */
	public boolean getFilterOption(int ndx)
	{
		return filterOption [ ndx ];

	}


	/**
	 *
	 *
	 */
	public void setFilterOption(int ndx, boolean b)
	{
		filterOption [ ndx ] = b;

	}


	/**
	 *
	 *
	 */
	public String getCriteria()
	{
		return criteria;

	}


	/**
	 *
	 *
	 */
	public void setCriteria(String c)
	{
		if (c == null) c = "";

		criteriaChanged = ! criteria.equals(c);

		criteria = c;

	}


	/**
	 *
	 *
	 */
	public boolean isCriteriaChanged()
	{
		return criteriaChanged;

	}


	/**
	 *
	 *
	 */
	public void setCriteriaChanged(boolean b)
	{
		criteriaChanged = b;

	}

}

