package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doDealEntryEscrowModelImpl extends QueryModelBase
	implements doDealEntryEscrowModel
{
	/**
	 *
	 *
	 */
	public doDealEntryEscrowModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEscrowPaymentId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFESCROWPAYMENTID);
	}


	/**
	 *
	 *
	 */
	public void setDfEscrowPaymentId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFESCROWPAYMENTID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfEscrowPaymentDesc()
	{
		return (String)getValue(FIELD_DFESCROWPAYMENTDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfEscrowPaymentDesc(String value)
	{
		setValue(FIELD_DFESCROWPAYMENTDESC,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEscrowPayment()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFESCROWPAYMENT);
	}


	/**
	 *
	 *
	 */
	public void setDfEscrowPayment(java.math.BigDecimal value)
	{
		setValue(FIELD_DFESCROWPAYMENT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEscrowTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFESCROWTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfEscrowTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFESCROWTYPEID,value);
	}

	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
   /**
        public void setResultSet(ResultSet value)
        {
            logger = SysLog.getSysLogger("METADATA");

            try
            {
                super.setResultSet(value);
                if( value != null)
                {
                    ResultSetMetaData metaData = value.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    logger.debug("===============================================");
                    logger.debug("Testing MetaData Object for new synthetic fields for" +
	                 	            " doDealEntryEscrowModel");
                    logger.debug("NumberColumns: " + columnCount);
                    for(int i = 0; i < columnCount ; i++)
                    {
                        logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                    }
                    logger.debug("================================================");
                  }
              }
              catch (SQLException e)
              {
                  logger.debug("Class: " + getClass().getName());
                  logger.debug("SQLException: " + e);
              }
        }
      **/

	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL ESCROWPAYMENT.DEALID, ESCROWPAYMENT.ESCROWPAYMENTID, ESCROWPAYMENT.COPYID, ESCROWPAYMENT.ESCROWPAYMENTDESCRIPTION, ESCROWPAYMENT.ESCROWPAYMENTAMOUNT, ESCROWPAYMENT.ESCROWTYPEID FROM ESCROWPAYMENT  __WHERE__  ORDER BY ESCROWPAYMENT.ESCROWPAYMENTID  ASC";
	public static final String MODIFYING_QUERY_TABLE_NAME="ESCROWPAYMENT";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="ESCROWPAYMENT.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFESCROWPAYMENTID="ESCROWPAYMENT.ESCROWPAYMENTID";
	public static final String COLUMN_DFESCROWPAYMENTID="ESCROWPAYMENTID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="ESCROWPAYMENT.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFESCROWPAYMENTDESC="ESCROWPAYMENT.ESCROWPAYMENTDESCRIPTION";
	public static final String COLUMN_DFESCROWPAYMENTDESC="ESCROWPAYMENTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFESCROWPAYMENT="ESCROWPAYMENT.ESCROWPAYMENTAMOUNT";
	public static final String COLUMN_DFESCROWPAYMENT="ESCROWPAYMENTAMOUNT";
	public static final String QUALIFIED_COLUMN_DFESCROWTYPEID="ESCROWPAYMENT.ESCROWTYPEID";
	public static final String COLUMN_DFESCROWTYPEID="ESCROWTYPEID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESCROWPAYMENTID,
				COLUMN_DFESCROWPAYMENTID,
				QUALIFIED_COLUMN_DFESCROWPAYMENTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESCROWPAYMENTDESC,
				COLUMN_DFESCROWPAYMENTDESC,
				QUALIFIED_COLUMN_DFESCROWPAYMENTDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESCROWPAYMENT,
				COLUMN_DFESCROWPAYMENT,
				QUALIFIED_COLUMN_DFESCROWPAYMENT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFESCROWTYPEID,
				COLUMN_DFESCROWTYPEID,
				QUALIFIED_COLUMN_DFESCROWTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

