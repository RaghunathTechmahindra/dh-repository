package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doAppraisalPropertyModel extends QueryModel, SelectQueryModel
{
  /**
   *
   *
   */
  public java.math.BigDecimal getDfPropertyOccurence();


  /**
   *
   *
   */
  public void setDfPropertyOccurence(java.math.BigDecimal value);


  /**
   *
   *
   */
  public String getDfPrimaryProperty();


  /**
   *
   *
   */
  public void setDfPrimaryProperty(String value);


  /**
   *
   *
   */
  public String getDfPropertyAddress();


  /**
   *
   *
   */
  public void setDfPropertyAddress(String value);


  /**
   *
   *
   */
  public java.math.BigDecimal getDfPropertyId();


  /**
   *
   *
   */
  public void setDfPropertyId(java.math.BigDecimal value);


  /**
   *
   *
   */
  public java.math.BigDecimal getDfPurchasePrice();


  /**
   *
   *
   */
  public void setDfPurchasePrice(java.math.BigDecimal value);


  /**
   *
   *
   */
  public java.math.BigDecimal getDfEstimatedAppraisal();


  /**
   *
   *
   */
  public void setDfEstimatedAppraisal(java.math.BigDecimal value);


  /**
   *
   *
   */
  public java.math.BigDecimal getDfActualAppraisal();


  /**
   *
   *
   */
  public void setDfActualAppraisal(java.math.BigDecimal value);


  /**
   *
   *
   */
  public java.sql.Timestamp getDfAppraisalDate();


  /**
   *
   *
   */
  public void setDfAppraisalDate(java.sql.Timestamp value);


  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId();


  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value);


  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId();


  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value);


  /**
   *
   *
   */
  public java.math.BigDecimal getDfAppraisalSourceId();


  /**
   *
   *
   */
  public void setDfAppraisalSourceId(java.math.BigDecimal value);

  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  // New fields for Appraisal Order Info.
  public String getDfAppraisalStatusDesc();
  public void setDfAppraisalStatusDesc(String value);

  public String getDfUseBorrowerInfo();
  public void setDfUseBorrowerInfo(String value);

  public String getDfPropertyOwnerName();
  public void setDfPropertyOwnerName(String value);

  public String getDfAOContactName();
  public void setDfAOContactName(String value);

  public String getDfAOContactWorkPhoneNum();
  public void setDfAOContactWorkPhoneNum(String value);

  public String getDfAOContactWorkPhoneNumExt();
  public void setDfAOContactWorkPhoneNumExt(String value);

  public String getDfAOContactCellPhoneNum();
  public void setDfAOContactCellPhoneNum(String value);

  public String getDfAOContactHomePhoneNum();
  public void setDfAOContactHomePhoneNum(String value);

  public String getDfCommentsForAppraiser();
  public void setDfCommentsForAppraiser(String value);

  //===========================================================

  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////

  public static final String FIELD_DFPROPERTYOCCURENCE="dfPropertyOccurence";
  public static final String FIELD_DFPRIMARYPROPERTY="dfPrimaryProperty";
  public static final String FIELD_DFPROPERTYADDRESS="dfPropertyAddress";
  public static final String FIELD_DFPROPERTYID="dfPropertyId";
  public static final String FIELD_DFPURCHASEPRICE="dfPurchasePrice";
  public static final String FIELD_DFESTIMATEDAPPRAISAL="dfEstimatedAppraisal";
  public static final String FIELD_DFACTUALAPPRAISAL="dfActualAppraisal";
  public static final String FIELD_DFAPPRAISALDATE="dfAppraisalDate";
  public static final String FIELD_DFDEALID="dfDealId";
  public static final String FIELD_DFCOPYID="dfCopyId";
  public static final String FIELD_DFAPPRAISALSOURCEID="dfAppraisalSourceId";

  //--Release2.1--//
  //Additional fields for Computed field population from ResourceBundle
  //--> By Billy 22Nov2002
  public static final String FIELD_DFSTREETTYPEID="dfStreetTypeId";
  public static final String FIELD_DFSTREETDIRECTIONID="dfStreetDirectionId";
  //===================================================================

  //--> Ticket#148 : Change Request # DJ006 - Support Appraisal Order
  //--> By Billy 16Dec2003
  // New fields for Appraisal Order Info.
  public static final String FIELD_DFAPPRAISALSTATUSDESC="dfAppraisalStatusDesc";
  public static final String FIELD_DFUSEBORROWERINFO="dfUseBorrowerInfo";
  public static final String FIELD_DFPROPERTYOWNERNAME="dfPropertyOwnerName";
  public static final String FIELD_DFAOCONTACTNAME="dfAOContactName";
  public static final String FIELD_DFAOCONTACTWORKPHONENUM="dfAOContactWorkPhoneNum";
  public static final String FIELD_DFAOCONTACTWORKPHONENUMEXT="dfAOContactWorkPhoneNumExt";
  public static final String FIELD_DFAOCONTACTCELLPHONENUM="dfAOContactCellPhoneNum";
  public static final String FIELD_DFAOCONTACTHOMEPHONENUM="dfAOContactHomePhoneNum";
  public static final String FIELD_DFCOMMENTSFORAPPRAISER="dfCommentsForAppraiser";
  //===================================================================

  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////

}

