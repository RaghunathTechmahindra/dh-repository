package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgDealSummaryRepeatedAssetDetailsTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedAssetDetailsTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(2);
		setPrimaryModelClass( doDetailViewBorrowersModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStADBType().setValue(CHILD_STADBTYPE_RESET_VALUE);
		getStADBPBFlag().setValue(CHILD_STADBPBFLAG_RESET_VALUE);
		getStADBborrSalutation().setValue(CHILD_STADBBORRSALUTATION_RESET_VALUE);
		getStADBFName().setValue(CHILD_STADBFNAME_RESET_VALUE);
		getStADBLName().setValue(CHILD_STADBLNAME_RESET_VALUE);
		getStADBMInitial().setValue(CHILD_STADBMINITIAL_RESET_VALUE);
		getStADBApplicationNetworthASDtls().setValue(CHILD_STADBAPPLICATIONNETWORTHASDTLS_RESET_VALUE);
		getRepeatedAssets().resetChildren();
		getRepeatedCreditRef().resetChildren();
		getStBeginHideAssetSection().setValue(CHILD_STBEGINHIDEASSETSECTION_RESET_VALUE);
		getStBeginHideCredRefSection().setValue(CHILD_STBEGINHIDECREDREFSECTION_RESET_VALUE);
		getStEndHideAssetSection().setValue(CHILD_STENDHIDEASSETSECTION_RESET_VALUE);
		getStEndHideCredRefSection().setValue(CHILD_STENDHIDECREDREFSECTION_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STADBTYPE,StaticTextField.class);
		registerChild(CHILD_STADBPBFLAG,StaticTextField.class);
		registerChild(CHILD_STADBBORRSALUTATION,StaticTextField.class);
		registerChild(CHILD_STADBFNAME,StaticTextField.class);
		registerChild(CHILD_STADBLNAME,StaticTextField.class);
		registerChild(CHILD_STADBMINITIAL,StaticTextField.class);
		registerChild(CHILD_STADBAPPLICATIONNETWORTHASDTLS,StaticTextField.class);
		registerChild(CHILD_REPEATEDASSETS,pgDealSummaryRepeatedAssetsTiledView.class);
		registerChild(CHILD_REPEATEDCREDITREF,pgDealSummaryRepeatedCreditRefTiledView.class);
		registerChild(CHILD_STBEGINHIDEASSETSECTION,StaticTextField.class);
		registerChild(CHILD_STBEGINHIDECREDREFSECTION,StaticTextField.class);
		registerChild(CHILD_STENDHIDEASSETSECTION,StaticTextField.class);
		registerChild(CHILD_STENDHIDECREDREFSECTION,StaticTextField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoDetailViewBorrowersModel());;modelList.add(getdoDetailViewAssetsModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		// The following code block was migrated from the RepeatedAssetDetails_onBeforeDisplayEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		handler.setupNumOfRepeatedAssetDetails(this);
		handler.pageSaveState();

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		  handler.pageGetState(this.getParentViewBean());
      handler.setBorrowerAssets(this.getTileIndex());
      handler.pageSaveState();
		}
		return movedToRow;
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		if (model instanceof doDetailViewAssetsModel) {
			doDetailViewAssetsModel m = (doDetailViewAssetsModel) model;
			DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			int dealId = handler.getTheSessionState().getCurrentPage().getPageDealId();
			int copyId = handler.getTheSessionState().getCurrentPage().getPageDealCID();
			m.clearUserWhereCriteria();
			m.addUserWhereCriterion(doDetailViewAssetsModel.FIELD_DFDEALID, "=", dealId);
			m.addUserWhereCriterion(doDetailViewAssetsModel.FIELD_DFCOPYID, "=", copyId);
		}
        if (model instanceof doDetailViewAssetsModel) {
            doDetailViewAssetsModel m = (doDetailViewAssetsModel) model;
            DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
            handler.pageGetState(this.getParentViewBean());
            int dealId = handler.getTheSessionState().getCurrentPage().getPageDealId();
            int copyId = handler.getTheSessionState().getCurrentPage().getPageDealCID();
            m.clearUserWhereCriteria();
            m.addUserWhereCriterion(doDetailViewAssetsModel.FIELD_DFDEALID, "=", dealId);
            m.addUserWhereCriterion(doDetailViewAssetsModel.FIELD_DFCOPYID, "=", copyId);
        }
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STADBTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STADBTYPE,
				doDetailViewBorrowersModel.FIELD_DFAPPLICANTTYPE,
				CHILD_STADBTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBPBFLAG))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STADBPBFLAG,
				doDetailViewBorrowersModel.FIELD_DFPRIMARYBORROWER,
				CHILD_STADBPBFLAG_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBBORRSALUTATION))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STADBBORRSALUTATION,
				doDetailViewBorrowersModel.FIELD_DFSALUTATION,
				CHILD_STADBBORRSALUTATION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBFNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STADBFNAME,
				doDetailViewBorrowersModel.FIELD_DFFIRSTNAME,
				CHILD_STADBFNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBLNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STADBLNAME,
				doDetailViewBorrowersModel.FIELD_DFLASTNAME,
				CHILD_STADBLNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBMINITIAL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewBorrowersModel(),
				CHILD_STADBMINITIAL,
				doDetailViewBorrowersModel.FIELD_DFMIDDLENAME,
				CHILD_STADBMINITIAL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STADBAPPLICATIONNETWORTHASDTLS))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDetailViewAssetsModel(),
				CHILD_STADBAPPLICATIONNETWORTHASDTLS,
				doDetailViewAssetsModel.FIELD_DFBORROWERNETWORTH,
				CHILD_STADBAPPLICATIONNETWORTHASDTLS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATEDASSETS))
		{
			pgDealSummaryRepeatedAssetsTiledView child = new pgDealSummaryRepeatedAssetsTiledView(this,
				CHILD_REPEATEDASSETS);
			return child;
		}
		else
		if (name.equals(CHILD_REPEATEDCREDITREF))
		{
			pgDealSummaryRepeatedCreditRefTiledView child = new pgDealSummaryRepeatedCreditRefTiledView(this,
				CHILD_REPEATEDCREDITREF);
			return child;
		}
		else
		if (name.equals(CHILD_STBEGINHIDEASSETSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBEGINHIDEASSETSECTION,
				CHILD_STBEGINHIDEASSETSECTION,
				CHILD_STBEGINHIDEASSETSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STBEGINHIDECREDREFSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STBEGINHIDECREDREFSECTION,
				CHILD_STBEGINHIDECREDREFSECTION,
				CHILD_STBEGINHIDECREDREFSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STENDHIDEASSETSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STENDHIDEASSETSECTION,
				CHILD_STENDHIDEASSETSECTION,
				CHILD_STENDHIDEASSETSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STENDHIDECREDREFSECTION))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STENDHIDECREDREFSECTION,
				CHILD_STENDHIDECREDREFSECTION,
				CHILD_STENDHIDECREDREFSECTION_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBType()
	{
		return (StaticTextField)getChild(CHILD_STADBTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBPBFlag()
	{
		return (StaticTextField)getChild(CHILD_STADBPBFLAG);
	}


	/**
	 *
	 *
	 */
	public String endStADBPBFlagDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stADBPBFlag_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.setYesOrNo(event.getContent());
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBborrSalutation()
	{
		return (StaticTextField)getChild(CHILD_STADBBORRSALUTATION);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBFName()
	{
		return (StaticTextField)getChild(CHILD_STADBFNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBLName()
	{
		return (StaticTextField)getChild(CHILD_STADBLNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBMInitial()
	{
		return (StaticTextField)getChild(CHILD_STADBMINITIAL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStADBApplicationNetworthASDtls()
	{
		return (StaticTextField)getChild(CHILD_STADBAPPLICATIONNETWORTHASDTLS);
	}


	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedAssetsTiledView getRepeatedAssets()
	{
		return (pgDealSummaryRepeatedAssetsTiledView)getChild(CHILD_REPEATEDASSETS);
	}


	/**
	 *
	 *
	 */
	public pgDealSummaryRepeatedCreditRefTiledView getRepeatedCreditRef()
	{
		return (pgDealSummaryRepeatedCreditRefTiledView)getChild(CHILD_REPEATEDCREDITREF);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBeginHideAssetSection()
	{
		return (StaticTextField)getChild(CHILD_STBEGINHIDEASSETSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStBeginHideAssetSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stBeginHideAssetSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideBeginAssetSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStBeginHideCredRefSection()
	{
		return (StaticTextField)getChild(CHILD_STBEGINHIDECREDREFSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStBeginHideCredRefSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stBeginHideCredRefSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideBeginCredRefSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEndHideAssetSection()
	{
		return (StaticTextField)getChild(CHILD_STENDHIDEASSETSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStEndHideAssetSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stEndHideAssetSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideEndAssetSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStEndHideCredRefSection()
	{
		return (StaticTextField)getChild(CHILD_STENDHIDECREDREFSECTION);
	}


	/**
	 *
	 *
	 */
	public String endStEndHideCredRefSectionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the stEndHideCredRefSection_onBeforeHtmlOutputEvent method
		DealSummaryHandler handler =(DealSummaryHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		String rc = handler.generateOrHideEndCredRefSect();
		handler.pageSaveState();

		return rc;
	}


	/**
	 *
	 *
	 */
	public doDetailViewBorrowersModel getdoDetailViewBorrowersModel()
	{
		if (doDetailViewBorrowers == null)
			doDetailViewBorrowers = (doDetailViewBorrowersModel) getModel(doDetailViewBorrowersModel.class);
		return doDetailViewBorrowers;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewBorrowersModel(doDetailViewBorrowersModel model)
	{
			doDetailViewBorrowers = model;
	}


	/**
	 *
	 *
	 */
	public doDetailViewAssetsModel getdoDetailViewAssetsModel()
	{
		if (doDetailViewAssets == null)
			doDetailViewAssets = (doDetailViewAssetsModel) getModel(doDetailViewAssetsModel.class);
		return doDetailViewAssets;
	}


	/**
	 *
	 *
	 */
	public void setdoDetailViewAssetsModel(doDetailViewAssetsModel model)
	{
			doDetailViewAssets = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STADBTYPE="stADBType";
	public static final String CHILD_STADBTYPE_RESET_VALUE="";
	public static final String CHILD_STADBPBFLAG="stADBPBFlag";
	public static final String CHILD_STADBPBFLAG_RESET_VALUE="";
	public static final String CHILD_STADBBORRSALUTATION="stADBborrSalutation";
	public static final String CHILD_STADBBORRSALUTATION_RESET_VALUE="";
	public static final String CHILD_STADBFNAME="stADBFName";
	public static final String CHILD_STADBFNAME_RESET_VALUE="";
	public static final String CHILD_STADBLNAME="stADBLName";
	public static final String CHILD_STADBLNAME_RESET_VALUE="";
	public static final String CHILD_STADBMINITIAL="stADBMInitial";
	public static final String CHILD_STADBMINITIAL_RESET_VALUE="";
	public static final String CHILD_STADBAPPLICATIONNETWORTHASDTLS="stADBApplicationNetworthASDtls";
	public static final String CHILD_STADBAPPLICATIONNETWORTHASDTLS_RESET_VALUE="";
	public static final String CHILD_REPEATEDASSETS="RepeatedAssets";
	public static final String CHILD_REPEATEDCREDITREF="RepeatedCreditRef";
	public static final String CHILD_STBEGINHIDEASSETSECTION="stBeginHideAssetSection";
	public static final String CHILD_STBEGINHIDEASSETSECTION_RESET_VALUE="";
	public static final String CHILD_STBEGINHIDECREDREFSECTION="stBeginHideCredRefSection";
	public static final String CHILD_STBEGINHIDECREDREFSECTION_RESET_VALUE="";
	public static final String CHILD_STENDHIDEASSETSECTION="stEndHideAssetSection";
	public static final String CHILD_STENDHIDEASSETSECTION_RESET_VALUE="";
	public static final String CHILD_STENDHIDECREDREFSECTION="stEndHideCredRefSection";
	public static final String CHILD_STENDHIDECREDREFSECTION_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDetailViewBorrowersModel doDetailViewBorrowers=null;
	private doDetailViewAssetsModel doDetailViewAssets=null;

  private DealSummaryHandler handler=new DealSummaryHandler();

}

