package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * Title: doComponentLocModelImpl
 * <p>
 * Description: Implementation of doComponentLocModel Interface.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 * @version 1.1 23-June-2008 XS_16.7 - Modified  STATIC_WHERE_CRITERIA for ordering the join condition.
 * @Version 1.1 24-June-2008 XS_16.7 Removed code for setting the flag in afterExecute() method and moved this formting to tiled view
 * @Version 1.2 24-June-2008 XS_16.14 Updated SELECT_SQL_TEMPLATE,MODIFYING_QUERY_TABLE_NAME,STATIC_WHERE_CRITERIA 
 *                                    - Added POSTEDRATE,INTERNALRATEPERCENTAGE
 * @version 1.3 14-Oct-2008 Bugfix FXP22958: Added new fields to display MI Premium amount in Deal Summary Screen                                   
 */
public class doComponentLocModelImpl extends QueryModelBase implements
doComponentLocModel
{

    public static final String DATA_SOURCE_NAME = "jdbc/orcl";

    public static final String SELECT_SQL_TEMPLATE = "SELECT ALL COMPONENT.DEALID,COMPONENT.COMPONENTID, "
        + "COMPONENT.COPYID, COMPONENT.INSTITUTIONPROFILEID, "
        + "to_char(COMPONENT.COMPONENTTYPEID)COMPONENTTYPEID_STR, "
        + "to_char(COMPONENT.MTGPRODID) MTGPRODID_STR, COMPONENTLOC.TOTALLOCAMOUNT, "
        + "COMPONENTLOC.MIALLOCATEFLAG, COMPONENTLOC.TOTALPAYMENTAMOUNT, "
        + "COMPONENTLOC.PROPERTYTAXALLOCATEFLAG, "
        + "COMPONENTLOC.COMMISSIONCODE, "
        + "COMPONENTLOC.PANDIPAYMENTAMOUNT, "
        + "COMPONENTLOC.ADVANCEHOLD, "
        + "COMPONENTLOC.EXISTINGACCOUNTNUMBER, "
        + "COMPONENTLOC.FIRSTPAYMENTDATE, "
        + "COMPONENT.REPAYMENTTYPEID, "
        + "COMPONENT.COMPONENTTYPEID, "
        + "COMPONENT.MTGPRODID, "
        + "COMPONENT.ADDITIONALINFORMATION, "           
        + "COMPONENTLOC.PAYMENTFREQUENCYID, "
        + "COMPONENTLOC.EXISTINGACCOUNTINDICATOR, "
        + "COMPONENTLOC.PROPERTYTAXESCROWAMOUNT, "
        + "COMPONENTLOC.MIALLOCATEFLAG MIFLAG, "            
        + "COMPONENTLOC.PROPERTYTAXALLOCATEFLAG PROPERTYTAXFLAG, "
        + "COMPONENTLOC.NETINTERESTRATE, COMPONENTLOC.DISCOUNT, COMPONENTLOC.PREMIUM, " 
        + "COMPONENTLOC.LOCAMOUNT, "
        + "DEAL.MIPREMIUMAMOUNT, "
        + "DEAL.MIUPFRONT, "
        + "DEAL.TAXPAYORID, "
        + "COMPONENT.POSTEDRATE, "
        + "PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE, "
        + "COMPONENT.PRICINGRATEINVENTORYID "
        + "FROM COMPONENT,COMPONENTLOC,DEAL,PRICINGRATEINVENTORY  "            
        + "__WHERE__  ORDER BY COMPONENT.COMPONENTID ";

    public static final String MODIFYING_QUERY_TABLE_NAME = "COMPONENT, COMPONENTLOC, DEAL,PRICINGRATEINVENTORY ";

    /**
     * MCM Impl Team
     * @version 1.2 23-June-2008 XS_16.7 - modified  STATIC_WHERE_CRITERIA for ordering the join condition.
     */

    public static final String STATIC_WHERE_CRITERIA = "COMPONENT.INSTITUTIONPROFILEID = DEAL.INSTITUTIONPROFILEID " 
        + "AND COMPONENT.INSTITUTIONPROFILEID= COMPONENTLOC.INSTITUTIONPROFILEID  "
        + "AND COMPONENT.DEALID = DEAL.DEALID "
        + "AND COMPONENT.COPYID = DEAL.COPYID "
        + "AND COMPONENT.COPYID=COMPONENTLOC.COPYID "
        + "AND COMPONENT.COMPONENTID=COMPONENTLOC.COMPONENTID "
        + "AND COMPONENT.COMPONENTTYPEID=2"
        + "AND PRICINGRATEINVENTORY.PRICINGRATEINVENTORYID = COMPONENT.PRICINGRATEINVENTORYID ";

    public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

    public static final String QUALIFIED_COLUMN_DFCOMPONENTID = "COMPONENT.COMPONENTID";
    public static final String COLUMN_DFCOMPONENTID = "COMPONENTID";

    public static final String QUALIFIED_COLUMN_DFDEALID = "COMPONENT.DEALID";
    public static final String COLUMN_DFDEALID = "DEALID";

    public static final String QUALIFIED_COLUMN_DFISTITUTIONPROFILEID = "COMPONENT.INSTITUTIONPROFILEID";
    public static final String COLUMN_DFISTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

    public static final String QUALIFIED_COLUMN_DFCOPYID = "COMPONENT.COPYID";
    public static final String COLUMN_DFCOPYID = "COPYID";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTTYPE = "COMPONENT.COMPONENTTYPEID_STR";
    public static final String COLUMN_DFCOMPONENTTYPE = "COMPONENTTYPEID_STR";

    public static final String QUALIFIED_COLUMN_DFPRODUCTNAME = "COMPONENT.MTGPRODID_STR";
    public static final String COLUMN_DFPRODUCTNAME = "MTGPRODID_STR";

    /** ****************** MCM Impl XS2.33 STARTS****************************************** */
    public static final String QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT = "DEAL.MIPREMIUMAMOUNT";
    public static final String COLUMN_DFMIPREMIUMAMOUNT = "MIPREMIUMAMOUNT";

    public static final String QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID = "COMPONENTLOC.PAYMENTFREQUENCYID";
    public static final String COLUMN_DFPAYMENTFREQUENCYID = "PAYMENTFREQUENCYID";

    public static final String QUALIFIED_COLUMN_DFCOMMISSIONCODE = "COMPONENTLOC.COMMISSIONCODE";
    public static final String COLUMN_DFCOMMISSIONCODE = "COMMISSIONCODE";

    public static final String QUALIFIED_COLUMN_DFPANDIPAYMENTAMOUNT = "COMPONENTLOC.PANDIPAYMENTAMOUNT";
    public static final String COLUMN_DFPANDIPAYMENTAMOUNT = "PANDIPAYMENTAMOUNT";

    public static final String QUALIFIED_COLUMN_DFTOTALLOCAMOUNT = "COMPONENTLOC.TOTALLOCAMOUNT";
    public static final String COLUMN_DFTOTALLOCAMOUNT = "TOTALLOCAMOUNT";

    public static final String QUALIFIED_COLUMN_DFINCLUDEMIPREMIUMFLAG = "COMPONENTLOC.MIALLOCATEFLAG";
    public static final String COLUMN_DFINCLUDEMIPREMIUMFLAG = "MIALLOCATEFLAG";

    public static final String QUALIFIED_COLUMN_DFTOTALPAYMENTAMOUNT = "COMPONENTLOC.TOTALPAYMENTAMOUNT";
    public static final String COLUMN_DFTOTALPAYMENTAMOUNT = "TOTALPAYMENTAMOUNT";

    public static final String QUALIFIED_COLUMN_DFALLOCATETAXESCROW = "COMPONENTLOC.PROPERTYTAXALLOCATEFLAG";
    public static final String COLUMN_DFALLOCATETAXESCROW = "PROPERTYTAXALLOCATEFLAG";

    public static final String QUALIFIED_COLUMN_DFNETINTERESTRATE = "COMPONENTLOC.NETINTERESTRATE";
    public static final String COLUMN_DFNETINTERESTRATE = "NETINTERESTRATE";

    public static final String QUALIFIED_COLUMN_DFDISCOUNT = "COMPONENTLOC.DISCOUNT";
    public static final String COLUMN_DFDISCOUNT = "DISCOUNT";

    public static final String QUALIFIED_COLUMN_DFPREMIUM = "COMPONENTLOC.PREMIUM";
    public static final String COLUMN_DFPREMIUM = "PREMIUM";

    public static final String QUALIFIED_COLUMN_DFADVANCEHOLD = "COMPONENTLOC.ADVANCEHOLD";
    public static final String COLUMN_DFADVANCEHOLD = "ADVANCEHOLD";

    public static final String QUALIFIED_COLUMN_DFEXISTINGACCOUNTNUMBER = "COMPONENTLOC.EXISTINGACCOUNTNUMBER";
    public static final String COLUMN_DFEXISTINGACCOUNTNUMBER = "EXISTINGACCOUNTNUMBER";

    public static final String QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE = "COMPONENTLOC.FIRSTPAYMENTDATE";
    public static final String COLUMN_DFFIRSTPAYMENTDATE = "FIRSTPAYMENTDATE";

    public static final String QUALIFIED_COLUMN_DFREPAYMENTTYPEID = "COMPONENT.REPAYMENTTYPEID";
    public static final String COLUMN_DFREPAYMENTTYPEID = "REPAYMENTTYPEID";

    public static final String QUALIFIED_COLUMN_DFCOMPONENTTYPEID = "COMPONENT.COMPONENTTYPEID";
    public static final String COLUMN_DFCOMPONENTTYPEID = "COMPONENTTYPEID";

    public static final String QUALIFIED_COLUMN_DFMTGPRODID = "COMPONENT.MTGPRODID";
    public static final String COLUMN_DFMTGPRODID = "MTGPRODID";

    public static final String QUALIFIED_COLUMN_DFEXISTINGACCOUNTINDICATOR = "COMPONENTLOC.EXISTINGACCOUNTINDICATOR";
    public static final String COLUMN_DFEXISTINGACCOUNTINDICATOR = "EXISTINGACCOUNTINDICATOR";

    public static final String QUALIFIED_COLUMN_DFLOCAMOUNT = "COMPONENTLOC.LOCAMOUNT";
    public static final String COLUMN_DFLOCAMOUNT = "LOCAMOUNT";

    public static final String QUALIFIED_COLUMN_DFADDITIONALINFORMATION = "COMPONENT.ADDITIONALINFORMATION";
    public static final String COLUMN_DFADDITIONALINFORMATION = "ADDITIONALINFORMATION";

    public static final String QUALIFIED_COLUMN_DFPROPERTYTAXESCROWAMOUNT = "COMPONENTLOC.PROPERTYTAXESCROWAMOUNT";
    public static final String COLUMN_DFPROPERTYTAXESCROWAMOUNT = "PROPERTYTAXESCROWAMOUNT";

    public static final String QUALIFIED_COLUMN_DFMIFLAG = "COMPONENTLOC.MIFLAG";
    public static final String COLUMN_DFMIFLAG = "MIFLAG";

    public static final String QUALIFIED_COLUMN_DFPROPERTYTAXFLAG = "COMPONENTLOC.PROPERTYTAXFLAG";
    public static final String COLUMN_DFPROPERTYTAXFLAG = "PROPERTYTAXFLAG";

    public static final String QUALIFIED_COLUMN_DFMIUPFRONT="DEAL.MIUPFRONT";
    public static final String COLUMN_DFMIUPFRONT="MIUPFRONT";
    
    public static final String QUALIFIED_COLUMN_DFTAXPAYORID = "DEAL.TAXPAYORID";
    public static final String COLUMN_DFTAXPAYORID = "TAXPAYORID";

    /** ****************** MCM Impl XS2.33 ENDS****************************************** */
    
    /** ****************** MCM Impl XS_16.14 STARTS****************************************** */
    
    public static final String QUALIFIED_COLUMN_DFPOSTEDRATE = "COMPONENT.POSTEDRATE";
    public static final String COLUMN_DFPOSTEDRATE = "POSTEDRATE";

    public static final String QUALIFIED_COLUMN_DFINTERNALRATEPERCENTAGE = "PRICINGRATEINVENTORY.INTERNALRATEPERCENTAGE";
    public static final String COLUMN_DFINTERNALRATEPERCENTAGE = "INTERNALRATEPERCENTAGE";
   
    /** ****************** MCM Impl XS_16.14 ENDS****************************************** */
    
    public static final String QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID = "COMPONENT.PRICINGRATEINVENTORYID";
    public static final String COLUMN_DFPRICINGRATEINVENTORYID = "PRICINGRATEINVENTORYID";
    
    static
    {

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTID, COLUMN_DFCOMPONENTID,
                QUALIFIED_COLUMN_DFCOMPONENTID, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDEALID, COLUMN_DFDEALID, QUALIFIED_COLUMN_DFDEALID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOPYID, COLUMN_DFCOPYID, QUALIFIED_COLUMN_DFCOPYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFISNTITUTIONID, COLUMN_DFISTITUTIONPROFILEID,
                QUALIFIED_COLUMN_DFISTITUTIONPROFILEID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTTYPE, COLUMN_DFCOMPONENTTYPE,
                QUALIFIED_COLUMN_DFCOMPONENTTYPE, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRODUCTNAME, COLUMN_DFPRODUCTNAME,
                QUALIFIED_COLUMN_DFPRODUCTNAME, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALLOCAMOUNT, COLUMN_DFTOTALLOCAMOUNT,
                QUALIFIED_COLUMN_DFTOTALLOCAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMIALLOCAGTEFLAG, COLUMN_DFINCLUDEMIPREMIUMFLAG,
                QUALIFIED_COLUMN_DFINCLUDEMIPREMIUMFLAG, String.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALPAYMENTAMOUNT, COLUMN_DFTOTALPAYMENTAMOUNT,
                QUALIFIED_COLUMN_DFTOTALPAYMENTAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPROPERTYTAXALLOCATEFLAG, COLUMN_DFALLOCATETAXESCROW,
                QUALIFIED_COLUMN_DFALLOCATETAXESCROW, String.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFNETINTRESTRATE, COLUMN_DFNETINTERESTRATE,
                QUALIFIED_COLUMN_DFNETINTERESTRATE, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDISCOUNT, COLUMN_DFDISCOUNT,
                QUALIFIED_COLUMN_DFDISCOUNT, java.math.BigDecimal.class, false,
                false, QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
                "", QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPREMIUM, COLUMN_DFPREMIUM, QUALIFIED_COLUMN_DFPREMIUM,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        
        /** ****************** MCM Impl XS2.33 STARTS****************************************** */
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMIPREMIUMAMOUNT, COLUMN_DFMIPREMIUMAMOUNT,
                QUALIFIED_COLUMN_DFMIPREMIUMAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPAYMENTFREQUENCYID, COLUMN_DFPAYMENTFREQUENCYID,
                QUALIFIED_COLUMN_DFPAYMENTFREQUENCYID, java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMMISSIONCODE, COLUMN_DFCOMMISSIONCODE,
                QUALIFIED_COLUMN_DFCOMMISSIONCODE, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPANDIPAYMENTAMOUNT, COLUMN_DFPANDIPAYMENTAMOUNT,
                QUALIFIED_COLUMN_DFPANDIPAYMENTAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFADVANCEHOLD, COLUMN_DFADVANCEHOLD,
                QUALIFIED_COLUMN_DFADVANCEHOLD,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFEXISTINGACCOUNTNUMBER, COLUMN_DFEXISTINGACCOUNTNUMBER,
                QUALIFIED_COLUMN_DFEXISTINGACCOUNTNUMBER,
                String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFFIRSTPAYMENTDATE, COLUMN_DFFIRSTPAYMENTDATE,
                QUALIFIED_COLUMN_DFFIRSTPAYMENTDATE,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFREPAYMENTTYPEID, COLUMN_DFREPAYMENTTYPEID,
                QUALIFIED_COLUMN_DFREPAYMENTTYPEID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOMPONENTTYPEID, COLUMN_DFCOMPONENTTYPEID,
                QUALIFIED_COLUMN_DFCOMPONENTTYPEID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRODUCTID, COLUMN_DFMTGPRODID,
                QUALIFIED_COLUMN_DFMTGPRODID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFEXISTINGACCOUNTINDICATOR, COLUMN_DFEXISTINGACCOUNTINDICATOR,
                QUALIFIED_COLUMN_DFEXISTINGACCOUNTINDICATOR, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFLOCAMOUNT, COLUMN_DFLOCAMOUNT,
                QUALIFIED_COLUMN_DFLOCAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFADDITIONALINFORMATION, COLUMN_DFADDITIONALINFORMATION,
                QUALIFIED_COLUMN_DFADDITIONALINFORMATION, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPROPERTYTAXESCROWAMOUNT, COLUMN_DFPROPERTYTAXESCROWAMOUNT,
                QUALIFIED_COLUMN_DFPROPERTYTAXESCROWAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMIFLAG, COLUMN_DFMIFLAG,
                QUALIFIED_COLUMN_DFMIFLAG, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPROPERTYTAXFLAG, COLUMN_DFPROPERTYTAXFLAG,
                QUALIFIED_COLUMN_DFPROPERTYTAXFLAG, String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFMIUPFRONT, COLUMN_DFMIUPFRONT, 
                QUALIFIED_COLUMN_DFMIUPFRONT,
                String.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTAXPAYORID, COLUMN_DFTAXPAYORID, 
                QUALIFIED_COLUMN_DFTAXPAYORID,
                java.sql.Timestamp.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        /** ****************** MCM Impl XS2.33 ENDS****************************************** */
        
        /** ******************XS_16.14 STARTS****************************************** */
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFNETRATE, COLUMN_DFPOSTEDRATE,
                QUALIFIED_COLUMN_DFPOSTEDRATE, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));


        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPOSTEDRATE, COLUMN_DFINTERNALRATEPERCENTAGE,
                QUALIFIED_COLUMN_DFINTERNALRATEPERCENTAGE, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        /** ******************XS_16.14 ENDS****************************************** */
        
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFPRICINGRATEINVENTORYID, COLUMN_DFPRICINGRATEINVENTORYID, 
                QUALIFIED_COLUMN_DFPRICINGRATEINVENTORYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
    }

    /**
     * Instantiates a new do component loc model impl.
     */
    public doComponentLocModelImpl()
    {
        super();
        setDataSourceName(DATA_SOURCE_NAME);

        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

        setFieldSchema(FIELD_SCHEMA);

        initialize();

    }

    /**
     * Initialize.
     */
    public void initialize()
    {

    }

    /**
     * This method will execute before executing the model..
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param sql
     *            the sql
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     * @Version 1.1 24-June-2008 XS_16.7 Removed code for setting the flag and moved this formting to tiled view
     */
    protected String beforeExecute(ModelExecutionContext context,
            int queryType, String sql) throws ModelControlException
            {

        return sql;

            }

    /**
     * This method will run after executing the model,in this method will handle
     * any display logic.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
    throws ModelControlException
    {

        String defaultInstanceStateName = getRequestContext().getModelManager()
        .getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
        .getModelManager().getModel(SessionStateModel.class,
                defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();
        int institutionId = theSessionState.getDealInstitutionId();

        while (this.next())
        {

            this.setDfComponentType(BXResources.getPickListDescription(
                    institutionId, "COMPONENTTYPE", this.getDfComponentType(),
                    languageId));

            this.setDfProductName(BXResources.getPickListDescription(
                    institutionId, "MTGPROD", this.getDfProductName(),
                    languageId));

            /** ******************XS_16.14 STARTS****************************************** */
            this.setDfRePaymentType(BXResources.getPickListDescription(
                    institutionId, "REPAYMENTTYPE", this.getDfRepaymentTypeId().intValue(),
                    languageId));
            this.setDfPaymentFrequency(BXResources.getPickListDescription(
                    institutionId, "PAYMENTFREQUENCY", this.getDfPaymentFrequencyId().intValue(),
                    languageId));
            /** ******************XS_16.14 ENDS****************************************** */
            /*******************************BUG Fix :FXP22958*****************************/
           //MI Premium Amount
            if("Y".equals(this.getDfMiAllocateFlag()))
                    this.setDfSummaryMiPremiumAmount(this.getDfMiPremiumAmount());
            else
                   this.setDfSummaryMiPremiumAmount(new BigDecimal(0.00));
            //Tax Escrow Amount
            if("Y".equals(this.getDfPropertyTaxAllocateFlag()))
                this.setDfSummaryTaxEscrowAmount(this.getDfPropertyTaxEscrowAmount());
            else
               this.setDfSummaryTaxEscrowAmount(new BigDecimal(0.00));
            /****************************************************************************/

        }



        // Reset Location
        this.beforeFirst();

    }

    /**
     * OnDatabaseError.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param exception
     *            the exception
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void onDatabaseError(ModelExecutionContext context,
            int queryType, SQLException exception)
    {

    }

    public java.math.BigDecimal getDfComponentId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFCOMPONENTID);

    }

    public void setDfComponentId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFCOMPONENTID, value);

    }

    public java.math.BigDecimal getDfCopyId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);

    }

    public void setDfCopyId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFCOPYID, value);

    }

    public BigDecimal getDfInstitutionId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFISNTITUTIONID);

    }

    public void setDfInstitutionId(BigDecimal value)
    {

        setValue(FIELD_DFISNTITUTIONID, value);

    }

    public void setDfTotalLocAmount(BigDecimal value)
    {

        setValue(FIELD_DFTOTALLOCAMOUNT, value);

    }

    public java.math.BigDecimal getDfTotalLocAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFTOTALLOCAMOUNT);

    }

    public String getDfComponentType()
    {

        return (String) getValue(FIELD_DFCOMPONENTTYPE);

    }

    public BigDecimal getDfDiscount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFDISCOUNT);

    }

    public String getDfMiAllocateFlag()
    {

        return (String) getValue(FIELD_DFMIALLOCAGTEFLAG);

    }

    public BigDecimal getDfNetInstrestRate()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFNETINTRESTRATE);

    }

    public BigDecimal getDfPremium()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFPREMIUM);

    }

    public String getDfProductName()
    {

        return (String) getValue(FIELD_DFPRODUCTNAME);

    }

    public String getDfPropertyTaxFlag()
    {

        return (String) getValue(FIELD_DFPROPERTYTAXALLOCATEFLAG);

    }

    public BigDecimal getDfTotalPaymentAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFTOTALPAYMENTAMOUNT);

    }

    public void setDfComponentType(String value)
    {

        setValue(FIELD_DFCOMPONENTTYPE, value);

    }

    public void setDfDiscount(BigDecimal value)
    {

        setValue(FIELD_DFDISCOUNT, value);

    }

    public void setDfMiAllocateFlag(String value)
    {

        setValue(FIELD_DFMIALLOCAGTEFLAG, value);

    }

    public void setDfNetInstrestRate(BigDecimal value)
    {

        setValue(FIELD_DFNETINTRESTRATE, value);

    }

    public void setDfPremium(BigDecimal value)
    {

        setValue(FIELD_DFPREMIUM, value);

    }

    public void setDfProductName(String value)
    {

        setValue(FIELD_DFPRODUCTNAME, value);

    }

    public void setDfPropertyTaxFlag(String value)
    {

        setValue(FIELD_DFPROPERTYTAXALLOCATEFLAG, value);

    }

    public void setDfTotalPaymentAmount(BigDecimal value)
    {

        setValue(FIELD_DFTOTALPAYMENTAMOUNT, value);

    }

    public BigDecimal getDfDealId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFDEALID);

    }

    public void setDfDealId(BigDecimal value)
    {

        setValue(FIELD_DFDEALID, value);

    }

    //XS2.33

    public void setDfMiPremiumAmount(java.math.BigDecimal value)
    {

        setValue(FIELD_DFMIPREMIUMAMOUNT, value);

    }


    public java.math.BigDecimal getDfMiPremiumAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFMIPREMIUMAMOUNT);

    }

    public void setDfPaymentFrequencyId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFPAYMENTFREQUENCYID, value);

    }


    public java.math.BigDecimal getDfPaymentFrequencyId()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFPAYMENTFREQUENCYID);

    }

    public void setDfCommissionCode(String value)
    {

        setValue(FIELD_DFCOMMISSIONCODE, value);

    }


    public String getDfCommissionCode()
    {
        return (String) getValue(FIELD_DFCOMMISSIONCODE);

    }

    public void setDfPandIPaymentAmount(java.math.BigDecimal value)
    {

        setValue(FIELD_DFPANDIPAYMENTAMOUNT, value);

    }


    public java.math.BigDecimal getDfPandIPaymentAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFPANDIPAYMENTAMOUNT);

    }

    public void setDfAdvanceHold(java.math.BigDecimal value)
    {   setValue(FIELD_DFADVANCEHOLD, value);
    }


    public java.math.BigDecimal getDfAdvanceHold()
    {   return (java.math.BigDecimal) getValue(FIELD_DFADVANCEHOLD);
    }    

    public void setDfExistingAccountNumber(String value)
    {   setValue(FIELD_DFEXISTINGACCOUNTNUMBER, value);
    }


    public String getDfExistingAccountNumber()
    {   return (String) getValue(FIELD_DFEXISTINGACCOUNTNUMBER);
    }

    public java.sql.Timestamp getDfFirstPaymentDate() {
        return (java.sql.Timestamp) getValue(FIELD_DFFIRSTPAYMENTDATE);
    }

    public void setDfFirstPaymentDate(java.sql.Timestamp value) {
        setValue(FIELD_DFFIRSTPAYMENTDATE, value);
    }

    public void setDfRepaymentTypeId(java.math.BigDecimal value)
    {   setValue(FIELD_DFREPAYMENTTYPEID, value);
    }


    public java.math.BigDecimal getDfRepaymentTypeId()
    {   return (java.math.BigDecimal) getValue(FIELD_DFREPAYMENTTYPEID);
    }

    public void setDfComponentTypeId(java.math.BigDecimal value)
    {   setValue(FIELD_DFCOMPONENTTYPEID, value);
    }

    public java.math.BigDecimal getDfComponentTypeId()
    {   return (java.math.BigDecimal) getValue(FIELD_DFCOMPONENTTYPEID);
    }

    public void setDfProductId(java.math.BigDecimal value)
    {   setValue(FIELD_DFPRODUCTID, value);
    }

    public java.math.BigDecimal getDfProductId()
    {   return (java.math.BigDecimal) getValue(FIELD_DFPRODUCTID);
    }

    public void setDfExistingAccountIndicator(String value)
    {   setValue(FIELD_DFEXISTINGACCOUNTINDICATOR, value);
    }


    public String getDfExistingAccountIndicator()
    {   return (String) getValue(FIELD_DFEXISTINGACCOUNTINDICATOR);
    }

    public void setDfLocAmount(BigDecimal value)
    {   setValue(FIELD_DFLOCAMOUNT, value);
    }

    public java.math.BigDecimal getDfLocAmount()
    {   return (java.math.BigDecimal) getValue(FIELD_DFLOCAMOUNT);
    }

    public void setDfAdditionalInformation(String value)
    {   setValue(FIELD_DFADDITIONALINFORMATION, value);
    }

    public String getDfAdditionalInformation()
    {   return (String) getValue(FIELD_DFADDITIONALINFORMATION);
    }

    public void setDfPropertyTaxEscrowAmount(BigDecimal value)
    {   setValue(FIELD_DFPROPERTYTAXESCROWAMOUNT, value);
    }

    public java.math.BigDecimal getDfPropertyTaxEscrowAmount()
    {   return (java.math.BigDecimal) getValue(FIELD_DFPROPERTYTAXESCROWAMOUNT);
    }

    public void setDfMIFlag(String value)
    {   setValue(FIELD_DFMIFLAG, value);
    }

    public String getDfMIFlag()
    {   return (String) getValue(FIELD_DFMIFLAG);
    }

    public void setDfPropertyTaxAllocateFlag(String value)
    {   setValue(FIELD_DFPROPERTYTAXFLAG, value);
    }

    public String getDfPropertyTaxAllocateFlag()
    {   return (String) getValue(FIELD_DFPROPERTYTAXFLAG);
    }

    public  String getDfMIUpfront() 
    {   return (String) getValue(FIELD_DFMIUPFRONT);        
    }

    public void setDfMIUpfront(String value) 
    {  setValue(FIELD_DFMIUPFRONT, value);        
    }
    
    public  java.math.BigDecimal getDfTaxPayorId() {
        return (java.math.BigDecimal) getValue(FIELD_DFTAXPAYORID);        
    }

    public void setDfTaxPayorId(java.math.BigDecimal value) {
        setValue(FIELD_DFTAXPAYORID, value);        
    }

    /** ******************XS_16.14 STARTS****************************************** */

    public String getDfPaymentFrequency()
    {
        return (String) getValue(FIELD_DFPAYMENTFREQUENCY); 
    }
    public void setDfRePaymentType(String value)
    {
        setValue(FIELD_DFREPAYMENTTYPE, value);  


    }

    public String getDfRePaymentType()
    {
        return (String) getValue(FIELD_DFREPAYMENTTYPE);
    }

    public void setDfPaymentFrequency(String value)
    {
        setValue(FIELD_DFPAYMENTFREQUENCY, value);  

    }

    public void setDfNetRate(java.math.BigDecimal value)
    {
        setValue(FIELD_DFNETRATE, value);
    }


    public java.math.BigDecimal getDfNetRate()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFNETRATE);
    }

    public void setDfPostedRate(java.math.BigDecimal value)
    {
        setValue(FIELD_DFPOSTEDRATE, value);
    }


    public java.math.BigDecimal getDfPostedRate()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFPOSTEDRATE);
    }

    /** ******************XS_16.14 ENDS****************************************** */
    
    /**
     * This method declaration returns the value of field the PricingRateInventoryId.
     * @return dfPricingRateInventoryId
     */
    public java.math.BigDecimal getDfPricingRateInventoryId() {
        return (java.math.BigDecimal) getValue(FIELD_DFPRICINGRATEINVENTORYID);        
    }

    /**
     * This method declaration sets the PricingInventryId.
     * @param value: the new dfPricingInventryRateId
     */
    public void setDfPricingRateInventoryId(java.math.BigDecimal value) {
        setValue(FIELD_DFPRICINGRATEINVENTORYID, value);        
    }

    public BigDecimal getDfSummaryMiPremiumAmount()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFDEALSUMMARYMIPREMIUMAMOUNT);     
    }

    public void setDfSummaryMiPremiumAmount(BigDecimal value)
    {
        setValue(FIELD_DFDEALSUMMARYMIPREMIUMAMOUNT, value);
        
    }
    public BigDecimal getDfSummaryTaxEscrowAmount()
    {
        return (java.math.BigDecimal) getValue(FIELD_DFDEALSUMMARYTAXESCROWAMOUNT);     
    }

    public void setDfSummaryTaxEscrowAmount(BigDecimal value)
    {
        setValue(FIELD_DFDEALSUMMARYTAXESCROWAMOUNT, value);
        
    }
}
