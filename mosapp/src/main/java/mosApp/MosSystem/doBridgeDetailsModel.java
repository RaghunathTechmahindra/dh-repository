package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doBridgeDetailsModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public String getDfBridgePurpose();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgePurpose(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfRateCode();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateCode(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPostedInterestRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfPostedInterestRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDiscount();

	
	/**
	 * 
	 * 
	 */
	public void setDfDiscount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPremium();

	
	/**
	 * 
	 * 
	 */
	public void setDfPremium(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfNetRate();

	
	/**
	 * 
	 * 
	 */
	public void setDfNetRate(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeLoanAmount();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeLoanAmount(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfMaturityDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfMaturityDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBridgeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBridgeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfRateDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfRateDate(java.sql.Timestamp value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBRIDGEPURPOSE="dfBridgePurpose";
	public static final String FIELD_DFRATECODE="dfRateCode";
	public static final String FIELD_DFPOSTEDINTERESTRATE="dfPostedInterestRate";
	public static final String FIELD_DFDISCOUNT="dfDiscount";
	public static final String FIELD_DFPREMIUM="dfPremium";
	public static final String FIELD_DFNETRATE="dfNetRate";
	public static final String FIELD_DFBRIDGELOANAMOUNT="dfBridgeLoanAmount";
	public static final String FIELD_DFMATURITYDATE="dfMaturityDate";
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFBRIDGEID="dfBridgeId";
	public static final String FIELD_DFRATEDATE="dfRateDate";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

