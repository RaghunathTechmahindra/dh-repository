package mosApp.MosSystem;

import java.math.BigDecimal;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 * <p>Title: doMainViewDealModel</p>
 *
 * <p>Description: interface for deal information on deal summary page</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>

 * @version 1.1 
 * Date: 7/28/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Added cash back amount, % and affiliation program
 *
 *	@Version 1.2 <br>
 *	Date: 14/06/2008
 *	Author: MCM Impl Team <br>
 *	Change Log:  <br>
 *	XS_16.8 -- Modified to add field for RefiAdditionalInformation and RefiExistingMtgNumber<br>
 *		- added declaration for getter/setter for RefiAdditionalInformation and RefiExistingMtgNumber<br>
 *	
 */
public interface doMainViewDealModel extends QueryModel, SelectQueryModel
{
  ////////////////////////////////////////////////////////////////////////////
  // Class variables
  ////////////////////////////////////////////////////////////////////////////
  public static final String FIELD_DFDEALID = "dfDealId";
  public static final String FIELD_DFMORTGAGEINSURERID = "dfMortgageInsurerId";
  public static final String FIELD_DFTOTALAMOUNT = "dfTotalAmount";
  public static final String FIELD_DFPAYMENTTERM = "dfPaymentTerm";
  public static final String FIELD_DFEFFECTIVEAMORTIZATION =
    "dfEffectiveAmortization";
  public static final String FIELD_DFPAYMENTFREQUENCY = "dfPaymentFrequency";
  public static final String FIELD_DFCOMBINEDLTV = "dfCombinedLTV";
  public static final String FIELD_DFRATEDATE = "dfRateDate";
  public static final String FIELD_DFFINANCINGDATE = "dfFinancingDate";
  public static final String FIELD_DFNETRATE = "dfNetRate";
  public static final String FIELD_DFRATELOCK = "dfRateLock";
  public static final String FIELD_DFDISCOUNT = "dfDiscount";
  public static final String FIELD_DFPREMIUM = "dfPremium";
  public static final String FIELD_DFBUYDOWNRATE = "dfBuydownRate";
  public static final String FIELD_DFSPECIALFEATURE = "dfSpecialFeature";
  public static final String FIELD_DFPIPAYMENT = "dfPIPayment";
  public static final String FIELD_DFADDITIONALPRICIPALAMOUNT =
    "dfAdditionalPricipalAmount";
  public static final String FIELD_DFTOTALESCROWPAYMENT =
    "dfTotalEscrowPayment";
  public static final String FIELD_DFTOTALPAYMENT = "dfTotalPayment";
  public static final String FIELD_DFFIRSTPAYMENTDATE = "dfFirstPaymentDate";
  public static final String FIELD_DFMATURITYDATE = "dfMaturityDate";
  public static final String FIELD_SFSECONDMORGAGE = "sfSecondMorgage";
  public static final String FIELD_DFEQUITYAVAILABLE = "dfEquityAvailable";
  public static final String FIELD_DFDEALPURPOSE = "dfDealPurpose";
  public static final String FIELD_DFLOB = "dfLOB";
  public static final String FIELD_DFDEALTYPE = "dfDealType";
  public static final String FIELD_DFPREPAYMENTOPTION = "dfPrePaymentOption";
  public static final String FIELD_DFREFERENCENO = "dfReferenceNo";
  public static final String FIELD_DFESTCLOSINGDATE = "dfEstClosingDate";
  public static final String FIELD_DFACTUALCLOSINGDATE = "dfActualClosingDate";
  public static final String FIELD_DFPRIVILEGEPAYMENTOPTIONS =
    "dfPrivilegePaymentOptions";
  public static final String FIELD_DFSOURCE = "dfSource";
  public static final String FIELD_DFSOURCEFIRM = "dfSourceFirm";
  public static final String FIELD_DFREFSOURCEAPPNO = "dfRefSourceAppNo";
  public static final String FIELD_DFCROSSSELL = "dfCrossSell";
  public static final String FIELD_DFTOTALPURCHASEPRICE =
    "dfTotalPurchasePrice";
  public static final String FIELD_DFTOTALESTIMATEDVALUE =
    "dfTotalEstimatedValue";
  public static final String FIELD_DFACTUALAPPRAISEDVALUE =
    "dfActualAppraisedValue";
  public static final String FIELD_DFLENDER = "dfLender";
  public static final String FIELD_DFINVESTOR = "dfInvestor";
  public static final String FIELD_DFNOOFBORROWERS = "dfNoOfBorrowers";
  public static final String FIELD_DFNOGURANTORS = "dfNoGurantors";
  public static final String FIELD_DFUNDERWRITERUSERID = "dfUnderWriterUserId";
  public static final String FIELD_DFCOMBINEDGDS = "dfCombinedGDS";
  public static final String FIELD_DFCOMBINED3YRGDS = "dfCombined3YrGDS";
  public static final String FIELD_DFCOMBINEDTDS = "dfCombinedTDS";
  public static final String FIELD_DFCOMBINED3YRTDS = "dfCombined3YrTDS";
  public static final String FIELD_DFCOMBINEDTOTALINCOME =
    "dfCombinedTotalIncome";
  public static final String FIELD_DFCOMBINEDTOTALLIABILITIES =
    "dfCombinedTotalLiabilities";
  public static final String FIELD_DFCOMBINEDTOTALASSETS =
    "dfCombinedTotalAssets";
  public static final String FIELD_DFNOOFPROPERTIES = "dfNoOfProperties";
  public static final String FIELD_DFCOMBINEDTOTALPROPERTYEXPENSES =
    "dfCombinedTotalPropertyExpenses";
  public static final String FIELD_DFCOPYID = "dfCopyId";
  public static final String FIELD_DFACTUALPAYMENTTERM = "dfActualPaymentTerm";
  public static final String FIELD_DFBRANCHPROFILEID = "dfBranchProfileId";
  public static final String FIELD_DFPRICINGPROFILEID = "dfPricingProfileId";
  public static final String FIELD_DFMORTGAGEPRODUCTID = "dfMortgageProductId";
  public static final String FIELD_DFPREAPPROVALPURCHASEPRICE =
    "dfPreApprovalPurchasePrice";
  public static final String FIELD_DFREFERENCEDEALTYPEID =
    "dfReferenceDealTypeId";
  public static final String FIELD_DFREQUIREDDOWNPAYMENT =
    "dfRequiredDownPayment";
  public static final String FIELD_DFPAYMENTTERMTYPEDESC =
    "dfPaymentTermTypeDesc";
  public static final String FIELD_DFSOURCECONTACTPHONENUM =
    "dfSourceContactPhoneNum";
  public static final String FIELD_DFSOURCECONTACTFAXNUM =
    "dfSourceContactFaxNum";
  public static final String FIELD_DFSOURCEFULLADDRESS = "dfSourceFullAddress";
  public static final String FIELD_DFSOURCEFULLNAME = "dfSourceFullName";
  public static final String FIELD_DFSOURCECONTACTPHONEEXT =
    "dfSourceContactPhoneExt";
  public static final String FIELD_DFADMINISTRATORID = "dfAdministratorId";
  public static final String FIELD_DFSOURCEFIRMNAME = "dfSourceFirmName";
  public static final String FIELD_DFFINANCINGPROGRAM = "dfFinancingProgram";
  public static final String FIELD_DFPOSTEDRATE = "dfPostedRate";
  public static final String FIELD_DFFUNDERID = "dfFunderId";
  public static final String FIELD_DFPROGRESSADVANCE = "dfProgressAdvance";
  public static final String FIELD_DFADVANCETODATEAMOUNT =
    "dfAdvanceToDateAmount";
  public static final String FIELD_DFADVANCENUMBER = "dfAdvanceNumber";
  public static final String FIELD_DFMIRUINTERVENTION = "dfMIRUIntervention";
  public static final String FIELD_DFPREQUALIFICATIONMICERTNUM =
    "dfPreQualificationMICertNum";
  public static final String FIELD_DFREFIPURPOSE = "dfRefiPurpose";
  public static final String FIELD_DFREFIORIGPURCHASEPRICE =
    "dfRefiOrigPurchasePrice";
  public static final String FIELD_DFREFIBLENDEDAMORIZATION =
    "dfRefiBlendedAmorization";
  public static final String FIELD_DFREFIORIGMTGAMOUNT = "dfRefiOrigMtgAmount";
  public static final String FIELD_DFREFIIMPROVEMENTDESC =
    "dfRefiImprovementDesc";
  public static final String FIELD_DFREFIIMPROVEMENTAMOUNT =
    "dfRefiImprovementAmount";
  public static final String FIELD_DFNEXTADVANCEAMOUNT = "dfNextAdvanceAmount";
  public static final String FIELD_DFREFIORIGPURCHASEDATE =
    "dfRefiOrigPurchaseDate";
  public static final String FIELD_DFREFICURMORTGAGEHOLDER =
    "dfRefiCurMortgageHolder";
  public static final String FIELD_DFREFIOUTSTANDINGMTGAMT =
    "dfRefiOutstandingMtgAmt";
  public static final String FIELD_DFISREFINANCE = "dfIsRefinance";

  //--Release3.1--begins
  //--by Hiro Apr 10, 2006
  public static final String FIELD_DFPRODUCTTYPEID = "dfProductType";
  public static final String FIELD_DFLIENPOSITIONID = "dfLPDescription";
  public static final String FIELD_DFREFIPRODUCTTYPEID = "dfRefiProductTypeId";
  //--Release3.1--ends
  
  //-- ========== DJ#725: begins ========== --//
  //-- By Neil : Nov/30/2004
  // E-mail
  public static final String FIELD_DFCONTACTEMAILADDRESS =
    "dfContactEmailAddress";

  // Status
  public static final String FIELD_DFPSDESCRIPTION = "dfPsDescription";

  // System Type
  public static final String FIELD_DFSYSTEMTYPEDESCRIPTION =
    "dfSystemTypeDescription";

  //	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
  public static final String FIELD_DFCASHBACKINDOLLARS = "dfCashBackInDollars";
  public static final String FIELD_DFCASHBACKINPERCENTAGE = "dfCashBackInPercentage";
  public static final String FIELD_DFAFFILIATIONPROGRAM = "dfAffiliationProgram";

  public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
  public static final String FIELD_DFINSTITUTIONNAME = "dfInstitutionName";
  /***************MCM Impl team changes starts - XS_16.8 *******************/
	
	public static final String FIELD_DFREFIADDITIONALINFORMATION="dfRefiAdditionalInformation";
	public static final String FIELD_DFREFIEXISTINGMTGNUMBER="dfRefiExistingMtgNumber";
	
	/***************MCM Impl team changes ends - XS_16.8 *********************/
    
    // ***** Qualifying Rate change ***** //
    public static final String FIELD_DFQUALIFYRATE = "dfQualifyRate";
    public static final String FIELD_DFOVERRIDEQUALPROD = "dfOverrideQualProd";
    public static final String FIELD_DFQUALIFYINGOVERRIDEFLAG = "dfQualifyingOverrideFlag";
    public static final String FIELD_DFQUALIFYINGOVERRIDERATEFLAG = "dfQualifyingOverrideRateFlag";
    
    // ***** Qualifying Rate change ***** //
    
  /**
  * getDfCashBackInDollars
  *
  * @param None <br>
  *  
  * @return BigDecimal : the result of getDfCashBackInDollars <br>
  */
  public java.math.BigDecimal getDfCashBackInDollars();

  /**
  * setDfCashBackInDollars
  *
  * @param BigDecimal <br>
  *  
  * @return Void : the result of setDfCashBackInDollars <br>
  */
  public void setDfCashBackInDollars(java.math.BigDecimal value);

  /**
  * getDfCashBackInPercentage
  *
  * @param None <br>
  *  
  * @return BigDecimal : the result of getDfCashBackInPercentage <br>
  */
  public java.math.BigDecimal getDfCashBackInPercentage();

  /**
  * setDfCashBackInPercentage
  *
  * @param BigDecimal <br>
  *  
  * @return void : the result of setDfCashBackInPercentage <br>
  */
  public void setDfCashBackInPercentage(java.math.BigDecimal value);

  /**
  * getDfAffiliationProgram
  *
  * @param None <br>
  *  
  * @return String : the result of getDfAffiliationProgram <br>
  */
  public String getDfAffiliationProgram();

  /**
  * setDfAffiliationProgram
  *
  * @param String <br>
  *  
  * @return void : the result of setDfAffiliationProgram <br>
  */
  public void setDfAffiliationProgram(String value);
  //		***** Change by NBC Impl. Team - Version 1.1 - End*****//

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDealId();

  /**
   *
   *
   */
  public void setDfDealId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfMortgageInsurerId();

  /**
   *
   *
   */
  public void setDfMortgageInsurerId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTotalAmount();

  /**
   *
   *
   */
  public void setDfTotalAmount(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfPaymentTerm();

  /**
   *
   *
   */
  public void setDfPaymentTerm(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfEffectiveAmortization();

  /**
   *
   *
   */
  public void setDfEffectiveAmortization(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfPaymentFrequency();

  /**
   *
   *
   */
  public void setDfPaymentFrequency(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombinedLTV();

  /**
   *
   *
   */
  public void setDfCombinedLTV(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.sql.Timestamp getDfRateDate();

  /**
   *
   *
   */
  public void setDfRateDate(java.sql.Timestamp value);
  
  /**
  *
  *
  */
 public java.sql.Timestamp getDfFinancingDate();

 /**
  *
  *
  */
 public void setDfFinancingDate(java.sql.Timestamp value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfNetRate();

  /**
   *
   *
   */
  public void setDfNetRate(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfRateLock();

  /**
   *
   *
   */
  public void setDfRateLock(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfDiscount();

  /**
   *
   *
   */
  public void setDfDiscount(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPremium();

  /**
   *
   *
   */
  public void setDfPremium(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBuydownRate();

  /**
   *
   *
   */
  public void setDfBuydownRate(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfSpecialFeature();

  /**
   *
   *
   */
  public void setDfSpecialFeature(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPIPayment();

  /**
   *
   *
   */
  public void setDfPIPayment(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfAdditionalPricipalAmount();

  /**
   *
   *
   */
  public void setDfAdditionalPricipalAmount(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTotalEscrowPayment();

  /**
   *
   *
   */
  public void setDfTotalEscrowPayment(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTotalPayment();

  /**
   *
   *
   */
  public void setDfTotalPayment(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.sql.Timestamp getDfFirstPaymentDate();

  /**
   *
   *
   */
  public void setDfFirstPaymentDate(java.sql.Timestamp value);

  /**
   *
   *
   */
  public java.sql.Timestamp getDfMaturityDate();

  /**
   *
   *
   */
  public void setDfMaturityDate(java.sql.Timestamp value);

  /**
   *
   *
   */
  public String getSfSecondMorgage();

  /**
   *
   *
   */
  public void setSfSecondMorgage(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfEquityAvailable();

  /**
   *
   *
   */
  public void setDfEquityAvailable(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfDealPurpose();

  /**
   *
   *
   */
  public void setDfDealPurpose(String value);

  /**
   *
   *
   */
  public String getDfLOB();

  /**
   *
   *
   */
  public void setDfLOB(String value);

  /**
   *
   *
   */
  public String getDfDealType();

  /**
   *
   *
   */
  public void setDfDealType(String value);

  /**
   *
   *
   */
  public String getDfPrePaymentOption();

  /**
   *
   *
   */
  public void setDfPrePaymentOption(String value);

  /**
   *
   *
   */
  public String getDfReferenceNo();

  /**
   *
   *
   */
  public void setDfReferenceNo(String value);

  /**
   *
   *
   */
  public java.sql.Timestamp getDfEstClosingDate();

  /**
   *
   *
   */
  public void setDfEstClosingDate(java.sql.Timestamp value);

  /**
   *
   *
   */
  public java.sql.Timestamp getDfActualClosingDate();

  /**
   *
   *
   */
  public void setDfActualClosingDate(java.sql.Timestamp value);

  /**
   *
   *
   */
  public String getDfPrivilegePaymentOptions();

  /**
   *
   *
   */
  public void setDfPrivilegePaymentOptions(String value);

  /**
   *
   *
   */
  public String getDfSource();

  /**
   *
   *
   */
  public void setDfSource(String value);

  /**
   *
   *
   */
  public String getDfSourceFirm();

  /**
   *
   *
   */
  public void setDfSourceFirm(String value);

  /**
   *
   *
   */
  public String getDfRefSourceAppNo();

  /**
   *
   *
   */
  public void setDfRefSourceAppNo(String value);

  /**
   *
   *
   */
  public String getDfCrossSell();

  /**
   *
   *
   */
  public void setDfCrossSell(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTotalPurchasePrice();

  /**
   *
   *
   */
  public void setDfTotalPurchasePrice(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfTotalEstimatedValue();

  /**
   *
   *
   */
  public void setDfTotalEstimatedValue(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfActualAppraisedValue();

  /**
   *
   *
   */
  public void setDfActualAppraisedValue(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfLender();

  /**
   *
   *
   */
  public void setDfLender(String value);

  /**
   *
   *
   */
  public String getDfInvestor();

  /**
   *
   *
   */
  public void setDfInvestor(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfNoOfBorrowers();

  /**
   *
   *
   */
  public void setDfNoOfBorrowers(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfNoGurantors();

  /**
   *
   *
   */
  public void setDfNoGurantors(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfUnderWriterUserId();

  /**
   *
   *
   */
  public void setDfUnderWriterUserId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombinedGDS();

  /**
   *
   *
   */
  public void setDfCombinedGDS(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombined3YrGDS();

  /**
   *
   *
   */
  public void setDfCombined3YrGDS(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombinedTDS();

  /**
   *
   *
   */
  public void setDfCombinedTDS(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombined3YrTDS();

  /**
   *
   *
   */
  public void setDfCombined3YrTDS(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombinedTotalIncome();

  /**
   *
   *
   */
  public void setDfCombinedTotalIncome(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombinedTotalLiabilities();

  /**
   *
   *
   */
  public void setDfCombinedTotalLiabilities(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombinedTotalAssets();

  /**
   *
   *
   */
  public void setDfCombinedTotalAssets(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfNoOfProperties();

  /**
   *
   *
   */
  public void setDfNoOfProperties(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCombinedTotalPropertyExpenses();

  /**
   *
   *
   */
  public void setDfCombinedTotalPropertyExpenses(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfCopyId();

  /**
   *
   *
   */
  public void setDfCopyId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfActualPaymentTerm();

  /**
   *
   *
   */
  public void setDfActualPaymentTerm(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfBranchProfileId();

  /**
   *
   *
   */
  public void setDfBranchProfileId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPricingProfileId();

  /**
   *
   *
   */
  public void setDfPricingProfileId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfMortgageProductId();

  /**
   *
   *
   */
  public void setDfMortgageProductId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPreApprovalPurchasePrice();

  /**
   *
   *
   */
  public void setDfPreApprovalPurchasePrice(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfReferenceDealTypeId();

  /**
   *
   *
   */
  public void setDfReferenceDealTypeId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfRequiredDownPayment();

  /**
   *
   *
   */
  public void setDfRequiredDownPayment(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfPaymentTermTypeDesc();

  /**
   *
   *
   */
  public void setDfPaymentTermTypeDesc(String value);

  /**
   *
   *
   */
  public String getDfSourceContactPhoneNum();

  /**
   *
   *
   */
  public void setDfSourceContactPhoneNum(String value);

  /**
   *
   *
   */
  public String getDfSourceContactFaxNum();

  /**
   *
   *
   */
  public void setDfSourceContactFaxNum(String value);

  /**
   *
   *
   */
  public String getDfSourceFullAddress();

  /**
   *
   *
   */
  public void setDfSourceFullAddress(String value);

  /**
   *
   *
   */
  public String getDfSourceFullName();

  /**
   *
   *
   */
  public void setDfSourceFullName(String value);

  /**
   *
   *
   */
  public String getDfSourceContactPhoneExt();

  /**
   *
   *
   */
  public void setDfSourceContactPhoneExt(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfAdministratorId();

  /**
   *
   *
   */
  public void setDfAdministratorId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfSourceFirmName();

  /**
   *
   *
   */
  public void setDfSourceFirmName(String value);

  /**
   *
   *
   */
  public String getDfFinancingProgram();

  /**
   *
   *
   */
  public void setDfFinancingProgram(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfPostedRate();

  /**
   *
   *
   */
  public void setDfPostedRate(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfFunderId();

  /**
   *
   *
   */
  public void setDfFunderId(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfProgressAdvance();

  /**
   *
   *
   */
  public void setDfProgressAdvance(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfAdvanceToDateAmount();

  /**
   *
   *
   */
  public void setDfAdvanceToDateAmount(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfAdvanceNumber();

  /**
   *
   *
   */
  public void setDfAdvanceNumber(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfMIRUIntervention();

  /**
   *
   *
   */
  public void setDfMIRUIntervention(String value);

  /**
   *
   *
   */
  public String getDfPreQualificationMICertNum();

  /**
   *
   *
   */
  public void setDfPreQualificationMICertNum(String value);

  /**
   *
   *
   */
  public String getDfRefiPurpose();

  /**
   *
   *
   */
  public void setDfRefiPurpose(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfRefiOrigPurchasePrice();

  /**
   *
   *
   */
  public void setDfRefiOrigPurchasePrice(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfRefiBlendedAmorization();

  /**
   *
   *
   */
  public void setDfRefiBlendedAmorization(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfRefiOrigMtgAmount();

  /**
   *
   *
   */
  public void setDfRefiOrigMtgAmount(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfRefiImprovementDesc();

  /**
   *
   *
   */
  public void setDfRefiImprovementDesc(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfRefiImprovementAmount();

  /**
   *
   *
   */
  public void setDfRefiImprovementAmount(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfNextAdvanceAmount();

  /**
   *
   *
   */
  public void setDfNextAdvanceAmount(java.math.BigDecimal value);

  /**
   *
   *
   */
  public java.sql.Timestamp getDfRefiOrigPurchaseDate();

  /**
   *
   *
   */
  public void setDfRefiOrigPurchaseDate(java.sql.Timestamp value);

  /**
   *
   *
   */
  public String getDfRefiCurMortgageHolder();

  /**
   *
   *
   */
  public void setDfRefiCurMortgageHolder(String value);

  /**
   *
   *
   */
  public java.math.BigDecimal getDfRefiOutstandingMtgAmt();

  /**
   *
   *
   */
  public void setDfRefiOutstandingMtgAmt(java.math.BigDecimal value);

  /**
   *
   *
   */
  public String getDfIsRefinance();

  /**
   *
   *
   */
  public void setDfIsRefinance(String value);

  //-- ========== DJ#725: ends ========== --//
  ////////////////////////////////////////////////////////////////////////////
  // Instance variables
  ////////////////////////////////////////////////////////////////////////////
  //-- ========== DJ#725: begins ========== --//
  //-- By Neil : Nov/30/2004

  /**
   * @return
   */
  public String getDfContactEmailAddress();

  /**
   * @return
   */
  public String getDfPsDescription();

  /**
   * @return
   */
  public String getDfSystemTypeDescription();

  /**
   * @param
   */
  public void setDfContactEmailAddress(String value);

  /**
   * @param
   */
  public void setDfPsDescription(String value);

  /**
   * @param
   */
  public void setDfSystemTypeDescription(String value);

  //-- ========== DJ#725: Ends ========== --//
  
  //--Release3.1--begins
  //--by Hiro Apr 10, 2006
  
  public String getDfProductTypeId();
  public void setDfProductTypeId(String value); 
  public String getDfLienPositionId();
  public void setDfLienPositionId(String value); 
 
  public String getDfRefiProductTypeId();
  public void setDfRefiProductTypeId(String value);
  //--Release3.1--ends
  
  public java.math.BigDecimal getDfInstitutionId();
  public void setDfInstitutionId(java.math.BigDecimal id);
  
  public String getDfInstitutionName();
  public void setDfInstitutionName(String id);
  
  /***************MCM Impl team changes starts - XS_16.8 *******************/  
	/**
	 * This method returns DfRefiAdditionalInformation value
	 *
	 * @param None <br>
	 *  
	 * @return String : the result of getDfRefiAdditionalInformation <br>
	 */
	public String getDfRefiAdditionalInformation();

	/**
	 * This method sets value for DfRefiAdditionalInformation
	 *
	 * @param String <br>
	 *  
	 */
	public void setDfRefiAdditionalInformation(String value);
	
	/**
	 * This method returns DfRefiExistingMtgNumber value
	 *
	 * @param None <br>
	 *  
	 * @return String : the result of getDfRefiExistingMtgNumber <br>
	 */
	public String getDfRefiExistingMtgNumber();

	/**
	 * This method sets value for DfRefiExistingMtgNumber
	 *
	 * @param String <br>
	 *  
	 */
	public void setDfRefiExistingMtgNumber(String value);

	/***************MCM Impl team changes ends - XS_16.8 *********************/
    
    public java.math.BigDecimal getDfQualifyRate();
    
    public void setDfQualifyRate(java.math.BigDecimal value);
    
    
    public String getDfOverrideQualProd();
    
    public void setDfOverrideQualProd(String value);
    
    //  ***** Qualify Rate *****//

}
