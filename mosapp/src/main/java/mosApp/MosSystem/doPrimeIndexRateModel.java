package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

//--Ticket#781--XD_ARM/VRM--19Apr2005--//
/**
 *
 *
 *
 */
public interface doPrimeIndexRateModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEffectiveDate();


	/**
	 *
	 *
	 */
	public void setDfEffectiveDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public Double getDfPrimeIndexRate();


	/**
	 *
	 *
	 */
	public void setDfPrimeIndexRate(Double value);


	/**
	 *
	 *
	 */
	public String getDfPrimeIndexName();


	/**
	 *
	 *
	 */
	public void setDfPrimeIndexName(String value);


  /**
   *
   *
   */
  public String getDfPrimeIndexDescription();


  /**
   *
   *
   */
  public void setDfPrimeIndexDescription(String value);


  /**
   *
   *
   */
  public java.math.BigDecimal getDfPrimeIndexRateProfileId();


  /**
   *
   *
   */
  public void setDfPrimeIndexRateProfileId(java.math.BigDecimal value);

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFEFFECTIVEDATE="dfEffectiveDate";
  public static final String FIELD_DFPRIMEINDEXNAME = "dfPrimeIndexName";
  public static final String FIELD_DFPRIMEINDEXRATE = "dfPrimeIndexRate";
  public static final String FIELD_DFPRIMEINDEXDESCRIPTION = "dfPrimeIndexDescription";
  public static final String FIELD_DFPRIMEINDEXRATEPROFILEID = "dfPrimeIndexRateProfileId";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

