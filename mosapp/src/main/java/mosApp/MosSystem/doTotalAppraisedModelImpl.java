package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doTotalAppraisedModelImpl extends QueryModelBase
	implements doTotalAppraisedModel
{
	/**
	 *
	 *
	 */
	public doTotalAppraisedModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALPURCHASEPRICE);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALPURCHASEPRICE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalActualAppraisedValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALACTUALAPPRAISEDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalActualAppraisedValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALACTUALAPPRAISEDVALUE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalEstimatedAppraisedValue()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFTOTALESTIMATEDAPPRAISEDVALUE);
	}


	/**
	 *
	 *
	 */
	public void setDfTotalEstimatedAppraisedValue(java.math.BigDecimal value)
	{
		setValue(FIELD_DFTOTALESTIMATEDAPPRAISEDVALUE,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.DEALID, DEAL.COPYID, DEAL.TOTALPURCHASEPRICE, DEAL.TOTALACTAPPRAISEDVALUE, DEAL.TOTALESTAPPRAISEDVALUE FROM DEAL  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE="DEAL.TOTALPURCHASEPRICE";
	public static final String COLUMN_DFTOTALPURCHASEPRICE="TOTALPURCHASEPRICE";
	public static final String QUALIFIED_COLUMN_DFTOTALACTUALAPPRAISEDVALUE="DEAL.TOTALACTAPPRAISEDVALUE";
	public static final String COLUMN_DFTOTALACTUALAPPRAISEDVALUE="TOTALACTAPPRAISEDVALUE";
	public static final String QUALIFIED_COLUMN_DFTOTALESTIMATEDAPPRAISEDVALUE="DEAL.TOTALESTAPPRAISEDVALUE";
	public static final String COLUMN_DFTOTALESTIMATEDAPPRAISEDVALUE="TOTALESTAPPRAISEDVALUE";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALPURCHASEPRICE,
				COLUMN_DFTOTALPURCHASEPRICE,
				QUALIFIED_COLUMN_DFTOTALPURCHASEPRICE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALACTUALAPPRAISEDVALUE,
				COLUMN_DFTOTALACTUALAPPRAISEDVALUE,
				QUALIFIED_COLUMN_DFTOTALACTUALAPPRAISEDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFTOTALESTIMATEDAPPRAISEDVALUE,
				COLUMN_DFTOTALESTIMATEDAPPRAISEDVALUE,
				QUALIFIED_COLUMN_DFTOTALESTIMATEDAPPRAISEDVALUE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

