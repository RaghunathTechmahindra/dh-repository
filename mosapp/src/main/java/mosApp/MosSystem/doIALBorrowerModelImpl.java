package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doIALBorrowerModelImpl extends QueryModelBase
	implements doIALBorrowerModel
{
	/**
	 *
	 *
	 */
	public doIALBorrowerModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerFirstName()
	{
		return (String)getValue(FIELD_DFBORROWERFIRSTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerFirstName(String value)
	{
		setValue(FIELD_DFBORROWERFIRSTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerMiddleName()
	{
		return (String)getValue(FIELD_DFBORROWERMIDDLENAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerMiddleName(String value)
	{
		setValue(FIELD_DFBORROWERMIDDLENAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerLastName()
	{
		return (String)getValue(FIELD_DFBORROWERLASTNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerLastName(String value)
	{
		setValue(FIELD_DFBORROWERLASTNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerPrimaryFlag()
	{
		return (String)getValue(FIELD_DFBORROWERPRIMARYFLAG);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerPrimaryFlag(String value)
	{
		setValue(FIELD_DFBORROWERPRIMARYFLAG,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerTDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERTDS);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerTDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERTDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerGDS()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERGDS);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerGDS(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERGDS,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerNetworth()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERNETWORTH);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerNetworth(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERNETWORTH,value);
	}


	/**
	 *
	 *
	 */
	public String getDfBorrowerType()
	{
		return (String)getValue(FIELD_DFBORROWERTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerType(String value)
	{
		setValue(FIELD_DFBORROWERTYPE,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL BORROWER.BORROWERID, BORROWER.DEALID, BORROWER.BORROWERFIRSTNAME, BORROWER.BORROWERMIDDLEINITIAL, BORROWER.BORROWERLASTNAME, BORROWER.PRIMARYBORROWERFLAG, BORROWER.TDS, BORROWER.GDS, BORROWER.NETWORTH, BORROWERTYPE.BTDESCRIPTION FROM BORROWER, BORROWERTYPE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="BORROWER, BORROWERTYPE";
	public static final String STATIC_WHERE_CRITERIA=" (BORROWER.BORROWERTYPEID  =  BORROWERTYPE.BORROWERTYPEID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="BORROWER.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFDEALID="BORROWER.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFBORROWERFIRSTNAME="BORROWER.BORROWERFIRSTNAME";
	public static final String COLUMN_DFBORROWERFIRSTNAME="BORROWERFIRSTNAME";
	public static final String QUALIFIED_COLUMN_DFBORROWERMIDDLENAME="BORROWER.BORROWERMIDDLEINITIAL";
	public static final String COLUMN_DFBORROWERMIDDLENAME="BORROWERMIDDLEINITIAL";
	public static final String QUALIFIED_COLUMN_DFBORROWERLASTNAME="BORROWER.BORROWERLASTNAME";
	public static final String COLUMN_DFBORROWERLASTNAME="BORROWERLASTNAME";
	public static final String QUALIFIED_COLUMN_DFBORROWERPRIMARYFLAG="BORROWER.PRIMARYBORROWERFLAG";
	public static final String COLUMN_DFBORROWERPRIMARYFLAG="PRIMARYBORROWERFLAG";
	public static final String QUALIFIED_COLUMN_DFBORROWERTDS="BORROWER.TDS";
	public static final String COLUMN_DFBORROWERTDS="TDS";
	public static final String QUALIFIED_COLUMN_DFBORROWERGDS="BORROWER.GDS";
	public static final String COLUMN_DFBORROWERGDS="GDS";
	public static final String QUALIFIED_COLUMN_DFBORROWERNETWORTH="BORROWER.NETWORTH";
	public static final String COLUMN_DFBORROWERNETWORTH="NETWORTH";
	public static final String QUALIFIED_COLUMN_DFBORROWERTYPE="BORROWERTYPE.BTDESCRIPTION";
	public static final String COLUMN_DFBORROWERTYPE="BTDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERFIRSTNAME,
				COLUMN_DFBORROWERFIRSTNAME,
				QUALIFIED_COLUMN_DFBORROWERFIRSTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERMIDDLENAME,
				COLUMN_DFBORROWERMIDDLENAME,
				QUALIFIED_COLUMN_DFBORROWERMIDDLENAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERLASTNAME,
				COLUMN_DFBORROWERLASTNAME,
				QUALIFIED_COLUMN_DFBORROWERLASTNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERPRIMARYFLAG,
				COLUMN_DFBORROWERPRIMARYFLAG,
				QUALIFIED_COLUMN_DFBORROWERPRIMARYFLAG,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERTDS,
				COLUMN_DFBORROWERTDS,
				QUALIFIED_COLUMN_DFBORROWERTDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERGDS,
				COLUMN_DFBORROWERGDS,
				QUALIFIED_COLUMN_DFBORROWERGDS,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERNETWORTH,
				COLUMN_DFBORROWERNETWORTH,
				QUALIFIED_COLUMN_DFBORROWERNETWORTH,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERTYPE,
				COLUMN_DFBORROWERTYPE,
				QUALIFIED_COLUMN_DFBORROWERTYPE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

