package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doAdministratorModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUserProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUsername();

	
	/**
	 * 
	 * 
	 */
	public void setDfUsername(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFUSERPROFILEID="dfUserProfileId";
	public static final String FIELD_DFUSERNAME="dfUsername";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

