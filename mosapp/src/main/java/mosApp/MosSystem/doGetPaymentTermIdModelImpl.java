package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;


//--Release2.1--//
//// Important!! This model is obsolete now.
//// paymentTermId is passing now via the new hdPaymentTermId field
//// in order to propagate the paymentTermId from the screen
//// to the database via dbprop framework method. The JS families on UW
//// and DE pages have been adjusted correspondingly.

/**
 *
 *
 *
 */
public class doGetPaymentTermIdModelImpl extends QueryModelBase
	implements doGetPaymentTermIdModel
{
	/**
	 *
	 *
	 */
	public doGetPaymentTermIdModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaymentTermId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPAYMENTTERMID);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTermId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPAYMENTTERMID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPaymentTermDescription()
	{
		return (String)getValue(FIELD_DFPAYMENTTERMDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTermDescription(String value)
	{
		setValue(FIELD_DFPAYMENTTERMDESCRIPTION,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL PAYMENTTERM.PAYMENTTERMID, PAYMENTTERM.PTDESCRIPTION FROM PAYMENTTERM  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="PAYMENTTERM";
	public static final String STATIC_WHERE_CRITERIA="";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFPAYMENTTERMID="PAYMENTTERM.PAYMENTTERMID";
	public static final String COLUMN_DFPAYMENTTERMID="PAYMENTTERMID";
	public static final String QUALIFIED_COLUMN_DFPAYMENTTERMDESCRIPTION="PAYMENTTERM.PTDESCRIPTION";
	public static final String COLUMN_DFPAYMENTTERMDESCRIPTION="PTDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTTERMID,
				COLUMN_DFPAYMENTTERMID,
				QUALIFIED_COLUMN_DFPAYMENTTERMID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTTERMDESCRIPTION,
				COLUMN_DFPAYMENTTERMDESCRIPTION,
				QUALIFIED_COLUMN_DFPAYMENTTERMDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

