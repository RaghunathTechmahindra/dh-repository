package mosApp.MosSystem;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ACM;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.PDC;
import com.basis100.deal.security.PasswordEncoder;
import com.basis100.deal.util.StringUtil;
import com.basis100.log.SysLog;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.Page;
import com.filogix.express.web.state.UserStateDataManager;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;

/**
 * Highly discussable implementation with the XXXXHandler extension of the
 * ViewBeanBase JATO class. The sequence diagrams must be verified. On the other
 * hand, This implementation allows to use the basic JATO framework elements
 * such as RequestContext() class. Theoretically, it could allow to override the
 * JATO/iMT auto-migration. Temporarily these fragments are commented out now
 * for the compability with the stand alone MWHC<--PHC<--XXXHandlerName
 * Utility hierarchy. getModel(ClassName.class), etc., could be also rewritten
 * in the Utility hierarchy using this inheritance.
 */
public class SignOnHandler extends PageHandlerCommon implements Cloneable, Sc,
        View, ViewBean {

    static boolean ualogEnable = "Y".equals(PropertiesCache.getInstance().getInstanceProperty
            ("com.filogix.express.userActivityLog", "N"));

    private static Log _log = LogFactory.getLog(SignOnHandler.class);

    // very constuctors to test the version with the ViewBeanBase extension.
    public SignOnHandler() {
        super();
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    public SignOnHandler(String name) {
        this(null, name); // No parent
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    protected SignOnHandler(View parent, String name) {
        super(parent, name);
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * 
     * 
     */
    public SignOnHandler cloneSS() {
        return (SignOnHandler) super.cloneSafeShallow();
    }

    public void populatePageDisplayFields(ViewBean thePage) {
        showDialogMessage(thePage);
        PageEntry cp = new PageEntry();
        theSessionState.setCurrentPage(cp);
        
        setupSignOnPageEntry(cp);
        try {
            String userlogin = (String) RequestManager.getSession()
                    .getAttribute("usSaveUserLogin");
            String enteredLogin = thePage.getDisplayFieldValue("tfUserID").toString();
            if (userlogin != null &&  enteredLogin.equalsIgnoreCase(""))
                thePage.setDisplayFieldValue("tfUserID", userlogin);
        } catch (Exception e) {
            logger.error("Error @SignOn.populatePageDisplayFields. ");
            logger.error(e);
            setStandardFailMessage();
        }
    }

    public void handleLogin(ViewBean ndPage, boolean isTest)
    throws SQLException, ModelControlException {
        // initialize logger here - this is virtually the only page handler that
        // is not preceeded by a call to preHandlerProtocol() !!
        logger = SysLog.getSysLogger("SIGNON");

        // check for 'theSession' exists - if not first contact from user
        // CSpSession session = CSpider.getUserSession();
        // String sessionId = session.getSessionId().toString();
        // SessionState startSession =(SessionState)
        // session.get(US_THE_SESSION);
        HttpSession session = RequestManager.getRequestContext().getRequest()
        .getSession();
        // ???? What the following for ????
        // --> Question By BILLY 04July2002
        String sessionId = session.getId();

        // Should get the SessionState Object here
        // Assume the JATO framework will create a new SessionStateModelImpl
        // object if not created yet
        // !!! need to test and confirm !!!
        // --> Added by BILLY 04July2002
        String defaultInstanceStateName = RequestManager.getRequestContext()
        .getModelManager().getDefaultModelInstanceName(
                SessionStateModel.class);
        theSessionState = (SessionStateModelImpl) RequestManager
        .getRequestContext().getModelManager().getModel(
                SessionStateModel.class, defaultInstanceStateName,
                true, true);

        //STRESSTEST--start--// 
        if (ualogEnable) {
            logger.debug("@SignOnHandler::DefaultInstanceStateName: " + defaultInstanceStateName); 
            logger.debug("@SignOnHandler::DefaultTheSessionState: " + theSessionState); 
            logger.debug("@SignOnHandler::Default LanguageId from SighOnHandler: " + theSessionState.getLanguageId()); 
            logger.debug("@SignOnHandler::SessionStateIsInUse: " + theSessionState.isInUse()); 
            logger.debug("@SignOnHandler::SessionStateUserId: " + theSessionState.getSessionUserId()); 
      
            String msg = "NewSessionId = " + sessionId + ", isInUse = " + theSessionState.isInUse(); 
            UserActivityLogger.logUser(null, msg, "SIN"); 
        }
        //--STRESSTEST--end--// 


        if (theSessionState == null || theSessionState.isInUse() == false) {
            // first contact ... create new session objects
            // // This is done by the container now, no need.
            // // see detailed comment on it in SessionStateModelImpl
            // // CHECK this.

            // Check if theSessionState == null just in case
            // But it shouldn't happen if our assumption (JATO will create on if
            // not exist)
            // --> Added by BILLY 04July2002
            if (theSessionState == null) {
                // --> This may cause memory leak if we store the HTTPSession
                // the SessionState Object
                // --> Modified by Billy 11April2003
                // setTheSessionState(new SessionStateModelImpl(theSession));
                setTheSessionState(new SessionStateModelImpl());
            }

            setSavedPages(new SavedPagesModelImpl());

            theSessionState.setInUse(true);
            theSessionState.setAllowedAuthenticationAttempts(3);
            theSessionState.setAuthenticationAttempts(0);
            theSessionState.setSessionDate(new java.util.Date());
        } else {
            logger
                    .info("@SignOnHandler:handleLogin: existing Session, sessionId = "
                            + sessionId);
            getSessionObjects();

            // detect allowed attempts exceeded previously (quick test)
            if (theSessionState.getAllowedAuthenticationAttempts() == -1) {
                showInvalidLogin(ndPage, "");
                return;
            }
        }

        theSessionState.setActMessage(null);
        setupSessionResourceKit("SIGNON");

        SessionResourceKit srk = getSessionResourceKit();

        
        // get User Id Value and Password from view fields
        String loginID = (String) ndPage.getDisplayFieldValue("tfUserID");
        String passWD = (String) ndPage.getDisplayFieldValue("tfPassword");
        loginID = ClientUtils.getUpperCase(loginID);
        logger.debug("@SignOnHandler:handleLogin: loginId=<"
                + loginID.toString() + ">, password=<" + passWD.toString()
                + ">");
        
        
        // set the messagedisplay field to empty
        ndPage.setDisplayFieldValue("stMessage", new String(""));

        // check for valid user n password if not then set message string
        // to error message n return code to stop at the same page
        if ((!isValidUserIdSyntax(loginID.toString()))
                || (!isValidPasswordSyntax(passWD.toString()))) {
            logger.debug("@SignOnHandler:handleLogin: invalid syntax");
            srk.freeResources();
            showInvalidLogin(ndPage, BXResources.getSysMsg("INVALID_LOGIN",
                    theSessionState.getLanguageId()));
            return;
        }

        // Attempt to get the UserProfile
        UserProfile up = null;
        try {
            up = new UserProfile(srk);
            // set silent mode to avoid error log
            up.setSilentMode(true);
            up.findByLoginId(loginID.toString());
        } catch (Exception e) {
            logger.debug("@SignOnHandler:handleLogin: no match for login id");
            srk.freeResources();
            showInvalidLogin(ndPage, BXResources.getSysMsg("INVALID_LOGIN",
                    theSessionState.getLanguageId()));
            up.setSilentMode(false);
            return;
        }
        
        // Check user is active.
        // Modified to allow SUSPENDED user login -- BILLY 12March2002
        if (up.getProfileStatusId() != Sc.PROFILE_STATUS_ACTIVE
                && up.getProfileStatusId() != Sc.PROFILE_STATUS_SUSPENDED) {
            logger
            .debug("@SignOnHandler:handleLogin: non-active user profile - deny access");
            srk.freeResources();
            showInvalidLogin(ndPage, BXResources.getSysMsg("USER_INACTIVE",
                    theSessionState.getLanguageId()));
            return;
        }

        // Modified to allow SUSPENDED user login -- BILLY 12March2002
        boolean isSuspended = false;
        if (up.getProfileStatusId() == Sc.PROFILE_STATUS_SUSPENDED) {
            isSuspended = true;
        }
        
        // Check if Password Locked
        if (up.isPasswordLocked() == true) {
            logger.debug("@SignOnHandler:handleLogin: password locked");
            srk.freeResources();
            showInvalidLogin(ndPage, BXResources.getSysMsg(
                    "USER_PASSWORD_LOCKED", theSessionState.getLanguageId()));
            return;
        }
        
        String encryptedPW = passWD;
        if (PropertiesCache.getInstance().getInstanceProperty("com.basis100.useradmin.passwordencrypted", "Y").equals("Y"))
            encryptedPW = PasswordEncoder.getEncodedPassword(encryptedPW);

        if (up.getPassword() == null || !up.getPassword().equals(encryptedPW))
            // ====================================================================
        {
            logger.debug("@SignOnHandler:handleLogin: incorrect password");
            // Check if Max Failed Attempts ==> Lock it !
            // Increment Failed Attempts counter
            try {
                srk.beginTransaction();
                up
                .setPasswordFailedAttempts(up
                        .getPasswordFailedAttempts() + 1);
                if (up.getPasswordFailedAttempts() >= up.getMaxFailedAttempts()) {
                    logger
                    .debug(" @SignOnHandler.HandleSignOn (Check Max Failed Attempts) :: Password Locked !!");
                    up.setPasswordAttributes(up.isNewPassword(), true, up
                            .isPasswordReset());
                }
                srk.commitTransaction();
            } catch (Exception e) {
                logger
                .error("Exception @SignOnHandler.HandleSignOn (Check Max Failed Attempts) : "
                        + e.getMessage());
            }
            srk.freeResources();
            showInvalidLogin(ndPage, BXResources.getSysMsg("INVALID_LOGIN",
                    theSessionState.getLanguageId()));
            return;
        }

        // Check if new password expired
        if (up.isNewPassword()) {
            int newPWValidDays = up.getNewPasswordValidDays();
            if (newPWValidDays > 0) {
                Calendar lastChange = Calendar.getInstance();
                lastChange.setTime(up.getLastPasswordChangedDate());
                logger
                        .debug(" @SignOnHandler.HandleSignOn (Check days Valid for new Password) Password Create Date == "
                                + lastChange.getTime().toString());

                // Add the Number of Valid Days
                lastChange.add(Calendar.DATE, newPWValidDays);
                if (lastChange.before(Calendar.getInstance())) {
                    // new password has expired
                    logger
                            .debug(" @SignOnHandler.HandleSignOn (Check days Valid for new Password) :: New Password has expired ==> Locked Password Up !!");
                    try {
                        srk.beginTransaction();
                        up.setPasswordAttributes(up.isNewPassword(), true, up
                                .isPasswordReset());
                        srk.commitTransaction();
                    } catch (Exception e) {
                        logger
                        .error("Exception @SignOnHandler.HandleSignOn (Check days Valid for new Password) : "
                                + e.getMessage());
                    }
                    srk.freeResources();
                    showInvalidLogin(ndPage, BXResources.getSysMsg(
                            "USER_NEWPASSWORD_LOCKED", theSessionState
                            .getLanguageId()));
                    return;
                } else {
                    // Set New password to Current
                    logger
                    .debug(" @SignOnHandler.HandleSignOn (Check days Valid for new Password) :: Set New Password to Current !!");
                    try {
                        srk.beginTransaction();
                        up.setPasswordAttributes(false, up.isPasswordLocked(),
                                up.isPasswordReset());
                        srk.commitTransaction();
                    } catch (Exception e) {
                        logger
                        .error("Exception @SignOnHandler.HandleSignOn (Check days Valid for new Password) : "
                                + e.getMessage());
                    }
                }
            }
        }

        // Password check OK ==> reset Failed Attempts Counter
        try {
            srk.beginTransaction();
            up.setPasswordFailedAttempts(0);
            srk.commitTransaction();
        } catch (Exception e) {
            logger
            .error("Exception @SignOnHandler.HandleSignOn (Resetting Failed Attempts Counter) : "
                    + e.getMessage());
        }

        try {
            // --> Use SessionServuices to track the session
            // --> by BILLY 11July2002
            boolean haveSessionObjects = SessionServices
            .retrieveSavedSessionObjects(loginID.toString(), session);
            if (haveSessionObjects == true) {
                logger
                .debug("@SignOnHandler:handleLogin: session initialized from saved session objects");
                // setTheSessionState((SessionStateModelImpl)
                // session.getAttribute(defaultInstanceStateName));
                // --> Have to refresh it from getModelManager otherwise it will
                // track the wrong SessionState Object
                // --> Not necessary to do this -- By Billy 22April2003
                /*
                 * theSessionState =
                 * (SessionStateModelImpl)RequestManager.getRequestContext().getModelManager().getModel(
                 * SessionStateModel.class, defaultInstanceStateName, true,
                 * true);
                 */
                setSavedPages((SavedPagesModelImpl) session
                        .getAttribute(US_SAVED_PAGES));

                // set 'next' page as last current page
                getSavedPages().setNextPage(
                        getTheSessionState().getCurrentPage());

                // set current page as signon page (e.g. to prevent navigation
                // from detecting current as a sub-page)
                PageEntry signOnPg = new PageEntry();
                // setupSignOnPageEntry(signOnPg);
                getTheSessionState().setCurrentPage(signOnPg);
            }

            // update session from user details (may have changed even when
            // saved objects located!)
            // theSession = getTheSession();
            theSessionState = getTheSessionState();
            
            UserStateDataManager userStateDataManager 
                = UserStateDataManager.loadUserStateData(loginID);
            theSessionState.setUserStateDataManager(userStateDataManager);
            //populate user related data
            theSessionState.switchInstitutionUserData(up.getInstitutionId());
            
            theSessionState.setLanguageId(up.getUserLanguageId());
            theSessionState.setUserLogin(up.getUserLogin());
            
            theSessionState.setSessionDate(new java.util.Date());
            Calendar tmpCal = Calendar.getInstance();
            tmpCal.setTime(up.getLastPasswordChangedDate());
            theSessionState.setLastPasswordChange(tmpCal);
            theSessionState.setAllowedAuthenticationAttempts(
                    up.getAllowedAuthenticationAttempts());
            PageEntry cp = theSessionState.getCurrentPage();
            cp.setPageMosUserId(theSessionState.getSessionUserId());
            cp.setPageMosUserTypeId(theSessionState.getSessionUserType());

            // save the session objects
            updateSessionObjects();

            // check login in use
            // --> Use SessionServuices to track the session
            if (SessionServices.isActiveSession(loginID.toString()) == true) {
                logger
                .debug("@SignOnHandler:handleLogin: login ID in use - confirm login with user");
                srk.freeResources();
                showConfirmLogin(ndPage, BXResources.getSysMsg("LOGIN_IN_USE",
                        theSessionState.getLanguageId()), up);
                return;
            }

            // Check if User suspened -- Billy 12March2002
            if (isSuspended == true) {
                srk.freeResources();
                showConfirmLogin(ndPage, BXResources.getSysMsg(
                        "LOGIN_SUSPENDED", theSessionState.getLanguageId()), up);
                return;
            }

            // find by primary key.
            up = up.findByPrimaryKey(new UserProfileBeanPK(up.getUserProfileId(), 
                    up.getInstitutionId()));
            finishLogin(false, up);
            
        } catch (Exception e) {
            // ... re-load sign-on page --- bad!!!!
            _log.error("Unexpected exception at log-on");
            _log.error(e);
            _log.error(StringUtil.stack2string(e));
            srk.freeResources();
            showInvalidLogin(ndPage, BXResources.getSysMsg(
                    "TECH_FAILURE_LOGIN", theSessionState.getLanguageId()));
            return;
        }

    }

    /**
     * 
     * 
     */
    public void handleCustomActMessageOk(String[] args) {
        if (args[0].equals("CONFIRM_NEW_SESSION")) {
            try {
                getSessionObjects();
                this.setSessionResourceKit(new SessionResourceKit(""
                        + getTheSessionState().getSessionUserId()));
                SessionResourceKit srk = this.getSessionResourceKit();

                // UserProfile up =
                // (UserProfile)((theSession.getActMessage()).getResponseObject());
                int upId = ((Integer) ((theSessionState.getActMessage())
                        .getResponseObject())).intValue();
                int instId = theSessionState.getUserInstitutionId();
                UserProfile up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(upId, instId));

                srk.setLanguageId(up.getUserLanguageId());
                this.setSessionResourceKit(srk);

                finishLogin(true, up);
            } catch (Exception e) {
                // ... re-load sign-on page --- bad!!!!
                logger.error("Unexpected exception at log-on");
                logger.error(e);
                showInvalidLogin(getCurrNDPage(), BXResources.getSysMsg(
                        "TECH_FAILURE_LOGIN", theSessionState.getLanguageId()));
            }
            return;
        }

    }

    /**
     * 
     * 
     */
    private void finishLogin(boolean terminateExistingSessionForUserId,
            UserProfile up) throws Exception {
        HttpSession session = RequestManager.getRequestContext().getRequest()
                .getSession();
        SessionResourceKit srk = getSessionResourceKit();

        _log.debug("@SignOnHandler:finishLogin: userProfId: "
                + up.getUserProfileId() + ", instProfId = "
                + up.getInstitutionId());

        srk.beginTransaction();

        if (terminateExistingSessionForUserId) {
            logger
                    .debug("@SignOnHandler:finishLogin: terminate existing session for userId");
        }

        // associate login Id with session (active user session) check login in use
        // --> Use SessionServuices to track the session
        SessionServices
                .addActiveSessionLoginId(up.getUserLogin(), session, srk);
        logger
                .debug("@SignOnHandler:finishLogin: go to first page, using last = " + false);

        setFirstPage();

        // get users ACM object
        // note that, srk is not set VPD at this moment.
        theSessionState.setAcm(new ACM(this.getSessionResourceKit(),
                theSessionState.getSessionUserType(), up.getUserLogin()));

        clearMessage();

        checkForPasswordChange(up, srk);

        up.setLoginFlag("Y");

        up.setLastLoginTime(new java.util.Date());

        up.ejbStore();
        
        navigateToNextPage();

        srk.commitTransaction();

        srk.freeResources();

        // save the session objects
        updateSessionObjects();

        PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

        String targetViewBeanName = pg.getPageName();

        ViewBean targetView = RequestManager.getRequestContext()
                .getViewBeanManager().getLocalViewBean(targetViewBeanName);

        targetView.forwardTo(RequestManager.getRequestContext());

        logger.debug("SONH@FinishLogin::I finished login!!!");

    }

    /**
     * 
     * 
     */
    private void setupSignOnPageEntry(PageEntry cp) {
        try {
            setupSessionResourceKit("SIGNON");

            Page pgEntity = PDC.getInstance().getPage(Sc.PGNM_SIGN_ON_ID);
            cp.setPageId(pgEntity.getPageId());
            cp.setPageName(pgEntity.getPageName());
            cp.setPageLabel(pgEntity.getPageLabel());
            cp.setDealPage(pgEntity.getDealPage());
        } catch (Exception e) {
            cp.setPageId(Sc.PGNM_SIGN_ON_ID);
            cp.setPageName("pgSignOn");
            cp.setPageLabel("Sign On");
            cp.setDealPage(false);
        }

    }

    private void showInvalidLogin(ViewBean SO, String errorMsg) {

        // test logon attempts exceeded
        theSessionState.setAuthenticationAttempts(theSessionState
                .getAuthenticationAttempts() + 1);

        if (theSessionState.getAuthenticationAttempts() > theSessionState
                .getAllowedAuthenticationAttempts()) {
            errorMsg = BXResources.getSysMsg("LOGIN_ATTEMPTS_EXCEEDED",
                    theSessionState.getLanguageId());
            theSessionState.setAllowedAuthenticationAttempts(-1);
        }

        setActiveMessageToAlert(BXResources.getSysMsg("LOGIN_INVALID_TITLE",
                theSessionState.getLanguageId()), errorMsg,
                ActiveMsgFactory.ISCUSTOMCONFIRM, "NA");

        updateSessionObjects();

        Object val = SO.getDisplayFieldValue("tfUserID");

        // Set the Login UserId to HTTPSession (It's a temp soultion). May change later
        // --> NOTE by BILLY 09July2002
        // --> Modified by Billy 11April2003 -- To prevent memory leak
        // theSessionState.setValue("usSaveUserLogin", val);

        // There is no reason to save wrong User Login in the memory. Also, the textbox
        // is cleaned for typing of the right password.
        // RequestManager.getRequestContext().getRequest().getSession().setAttribute("usSaveUserLogin", val);

        SO.setDisplayFieldValue("tfPassword", new String(""));
        SO.setDisplayFieldValue("tfUserID", new String(""));

        SO.forwardTo(RequestManager.getRequestContext());
    }

    /**
     * 
     * 
     */
    private void showConfirmLogin(ViewBean SO, String msg, UserProfile up) {
        theSessionState.setAuthenticationAttempts(theSessionState
                .getAuthenticationAttempts() + 1);

        if (theSessionState.getAuthenticationAttempts() > theSessionState
                .getAllowedAuthenticationAttempts())
            theSessionState.setAllowedAuthenticationAttempts(-1);

        // set for quick detect
        setActiveMessageToAlert(BXResources.getSysMsg("LOGIN_INVALID_TITLE",
                theSessionState.getLanguageId()), msg,
                ActiveMsgFactory.ISCUSTOMDIALOG, "CONFIRM_NEW_SESSION");

        (theSessionState.getActMessage()).setResponseObject(new Integer(up
                .getUserProfileId()));

        updateSessionObjects();

        Object val = SO.getDisplayFieldValue("tfUserID");

        // Set the Login UserId to HTTPSession (It's a temp soultion). May change later
        // --> NOTE by BILLY 09July2002
        // --> Modified by Billy 11April2003 -- To prevent memory leak
        // theSessionState.setValue("usSaveUserLogin", val);
        RequestManager.getRequestContext().getRequest().getSession()
                .setAttribute("usSaveUserLogin", val);

        SO.setDisplayFieldValue("tfUserID", new String(""));

        SO.setDisplayFieldValue("tfPassword", new String(""));

        SO.forwardTo(RequestManager.getRequestContext());
    }

    private void setFirstPage() throws Exception {

        // set 'next' page as ind. work queue or as search page for passive user
        PageEntry pgEntry = new PageEntry();

        pgEntry.setPageMosUserId(theSessionState.getSessionUserId());

        pgEntry.setPageMosUserTypeId(theSessionState.getSessionUserType());

        PDC pgCache = PDC.getInstance();

        Page pgEntity = null;

        if (theSessionState.getSessionUserType() != Mc.USER_TYPE_PASSIVE) {
            logger.debug("@SignOnHandler.setFirstPage() - work queue");
            pgEntity = pgCache.getPage(Sc.PGNM_INDIV_WORK_QUEUE_ID);
            theSessionState.setDetectAlertTasks("Y");
        } else {
            logger.debug("@SignOnHandler.setFirstPage() - deal search");
            pgEntity = pgCache.getPage(Sc.PGNM_DEAL_SEARCH_ID);
            theSessionState.setDetectAlertTasks("P");
        }

        pgEntry.setPageId(pgEntity.getPageId());

        pgEntry.setPageName(pgEntity.getPageName());

        pgEntry.setPageLabel(pgEntity.getPageLabel());

        pgEntry.setDealPage(pgEntity.getDealPage());
        
        getSavedPages().setNextPage(pgEntry);

    }

    public int generateSubmitButton() {
        // //SessionState theSession = this.getTheSession();

        // //if (theSession.getAllowedAuthenticationAttempts() == - 1) return
        // CSpPage.SKIP;
        if (theSessionState.getAllowedAuthenticationAttempts() == -1)
            return 0;

        return 1;
    }

    private void setDefaultPageAsBackPage(int pageId) {
    }

    private boolean checkForPasswordChange(UserProfile up,
            SessionResourceKit srk) {
        PageEntry pg = null;

        // return false;
        // Check to make Password status "Current" if "New"
        if (up.isNewPassword() == true) {
        }

        // Check if Password reset flag on
        if (up.isPasswordReset() == true) {
            // Set to force to change password
            pg = setupPasswordChangePage(true, getSecurityPolicyValue(srk,
                    Sc.PASSWORD_POLICY_GRACE_LOGINS), null, up);
            if (pg == null)
                return false;

            // set next page for password change page
            storeNextPageAsBackPage();
            getSavedPages().setNextPage(pg);
            return true;
        }

        // Check if Password Expired
        // get the paswsord change policy (number of days before change)
        int changeDays = getSecurityPolicyValue(srk,
                Sc.PASSWORD_POLICY_CHANGE_DAYS);

        // logger.debug(" @SignOnHandler.checkForPasswordChange changeDays == "
        // + changeDays);

        if (changeDays <= 0)
            return false;

        // no policy (i.e. passwords never expire)
        Calendar lastChange = Calendar.getInstance();

        lastChange.setTime(up.getLastPasswordChangedDate());

        // logger.debug(" @SignOnHandler.checkForPasswordChange reading from
        // database change date == " + lastChange.getTime().toString());

        lastChange.add(Calendar.DATE, changeDays);

        java.util.Date lastimepswdchanged = lastChange.getTime();

        if (lastChange.before(Calendar.getInstance())) {
            // password has expired
            pg = setupPasswordChangePage(true, up.getPasswordGraceCount(),
                    null, up);
            if (pg == null)
                return false;

            // set next page for password change page
            storeNextPageAsBackPage();
            getSavedPages().setNextPage(pg);
            return true;
        }

        // check for in warning period - going to expire
        int warnDays = getSecurityPolicyValue(srk, Sc.PASSWORD_POLICY_WARN_DAYS);

        if (warnDays <= 0)
            return false;

        // no policy (i.e. no warning before expiry)
        Calendar expiry = (Calendar) lastChange.clone();

        lastChange = moveBackByDays(lastChange, warnDays);

        if (lastChange.after(Calendar.getInstance()))
            return false;

        // start of warn time in future
        // in warn password has expired
        pg = setupPasswordChangePage(false, 0, expiry, up);

        if (pg == null)
            return false;

        // set next page for password change page
        storeNextPageAsBackPage();

        getSavedPages().setNextPage(pg);

        return true;

    }

    private void storeNextPageAsBackPage() {
        PageEntry pg = getSavedPages().getNextPage();
        getSavedPages().setBackPage(pg);
    }

    private PageEntry setupPasswordChangePage(boolean passwordExpired,
            int graceCountUsed, Calendar expiryDate, UserProfile up) {
        String messageToUser = "";

        boolean passwordMustBeChangedNow = false;

        SessionResourceKit srk = getSessionResourceKit();

        logger
                .debug(" @SignOnHandler.setupPasswordChangePage passwordExpired == "
                        + passwordExpired);

        if (passwordExpired) {
            int graceRemaining = 0;
            try {
                // check for grace policy
                int grace = getSecurityPolicyValue(srk,
                        Sc.PASSWORD_POLICY_GRACE_LOGINS);
                logger
                        .debug(" @SignOnHandler.setupPasswordChangePage getSecurityPolicyValue(Sc.PASSWORD_POLICY_GRACE_LOGINS) --> grace == "
                                + grace);
                if (grace > 0)
                    graceRemaining = grace - graceCountUsed;

                // grace logins remaining
                logger
                        .debug(" @SignOnHandler.setupPasswordChangePage graceRemaining == "
                                + graceRemaining);
            } catch (Exception e) {
                graceRemaining = 0;
            }
            if (graceRemaining <= 0) {
                // means password must be changed.
                passwordMustBeChangedNow = true;
                messageToUser = BXResources.getSysMsg(
                        "PASSWORD_CHANGE_REQUIRED", theSessionState
                                .getLanguageId());
                logger
                        .debug(" @SignOnHandler.setupPasswordChangePage graceRemaining <= 0 ");
            } else {
                logger
                        .debug(" @SignOnHandler.setupPasswordChangePage updateUserSecurityPolicy graceRemaining + 1 ");

                // Increment GraceCount
                // updateUserSecurityPolicy(userprofileid,graceCountUsed+1);
                up.setPasswordGraceCount(graceCountUsed + 1);
                logger
                        .debug(" @SignOnHandler.setupPasswordChangePage  graceRemaining => 1 ");
                if (graceRemaining == 1)
                    messageToUser = BXResources.getSysMsg(
                            "PASSWORD_CHANGE_ONE_GRACE", theSessionState
                                    .getLanguageId());
                else
                    messageToUser = setReplacement(BXResources.getSysMsg(
                            "PASSWORD_CHANGE_MANY_GRACE", theSessionState
                                    .getLanguageId()), "" + graceRemaining);
            }
        } else {
            // in warn period
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            df.setTimeZone(TimeZone.getDefault());
            String dateString = df.format(expiryDate.getTime());
            messageToUser = setReplacement(BXResources.getSysMsg(
                    "PASSWORD_CHANGE_ANNOY_WARNING", theSessionState
                            .getLanguageId()), dateString);
        }

        // set up page entry for transfer to password change page
        Page pDesc = getPage(Mc.PGNM_CHANGE_PASSWORD_ID);

        PageEntry pg = new PageEntry();

        pg.setPageMosUserId(theSessionState.getSessionUserId());

        pg.setPageMosUserTypeId(theSessionState.getSessionUserType());

        pg.setPageAccessType(Sc.PAGE_ACCESS_EDIT);

        pg.setPageId(Mc.PGNM_CHANGE_PASSWORD_ID);

        pg.setPageName(pDesc.getPageName());

        pg.setPageLabel(pDesc.getPageLabel());

        pg.setPageState1(messageToUser);

        pg.setPageCondition1(passwordMustBeChangedNow);

        return pg;
    }

    // don't know if add (on Calendar) with negative days allowed so ...
    private Calendar moveBackByDays(Calendar date, int numDays) {
        for (; numDays > 0; --numDays) {
            int dayOfMonth = date.get(Calendar.DATE);
            if (dayOfMonth > 1) {
                date.set(Calendar.DATE, dayOfMonth - 1);
                date.getTime();
                continue;
            }

            // month roll back
            int month = date.get(Calendar.MONTH);
            if (month > 0) {
                date.set(Calendar.MONTH, month - 1);
                date.getTime();
                date.set(Calendar.DATE, date.getActualMaximum(Calendar.DATE));
                date.getTime();
                continue;
            }

            // year rollback
            date.set(Calendar.YEAR, date.get(Calendar.YEAR) - 1);
            date.set(Calendar.MONTH, 11);
            date.set(Calendar.DATE, 31);
            date.getTime();
        }

        return date;

    }

    /**
     * @isValidUserIdSyntax check is valid syntax of user id currently even
     *                      though it's checks only for empty n null values but
     *                      later if we need to add any user login syntax
     *                      checking we can add it here
     */
    protected boolean isValidUserIdSyntax(String userid) {
        if (ClientUtils.isValidTextValue(userid)) {
            return true;
        }

        return false;
    }

    /**
     * @isValidPasswordSyntax same as
     * @isValidUserIdSyntax for more information
     */
    protected boolean isValidPasswordSyntax(String password) {
        if (ClientUtils.isValidTextValue(password)) {
            return true;
        }

        return false;
    }

    public void setVersionNumber(ViewBean thePage) {
        // Added to show the version number -- BILLY 18March2002
        thePage.setDisplayFieldValue("stVersion", new String(
                "eXpress Version : " + Sc.BX_VERSION));
    }
}
