package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doTransactionHistoryModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfTransactionDate();


	/**
	 *
	 *
	 */
	public void setDfTransactionDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfContext();


	/**
	 *
	 *
	 */
	public void setDfContext(String value);


	/**
	 *
	 *
	 */
	public String getDfUser();


	/**
	 *
	 *
	 */
	public void setDfUser(String value);


	/**
	 *
	 *
	 */
	public String getDfContextSource();


	/**
	 *
	 *
	 */
	public void setDfContextSource(String value);


	/**
	 *
	 *
	 */
	public String getDfPreviousValue();


	/**
	 *
	 *
	 */
	public void setDfPreviousValue(String value);


	/**
	 *
	 *
	 */
	public String getDfCurrentValue();


	/**
	 *
	 *
	 */
	public void setDfCurrentValue(String value);


	/**
	 *
	 *
	 */
	public String getDfField();


	/**
	 *
	 *
	 */
	public void setDfField(String value);


	/**
	 *
	 *
	 */
	public String getDfEntity();


	/**
	 *
	 *
	 */
	public void setDfEntity(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfTransactionDate1();


	/**
	 *
	 *
	 */
	public void setDfTransactionDate1(String value);

    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFTRANSACTIONDATE="dfTransactionDate";
	public static final String FIELD_DFCONTEXT="dfContext";
	public static final String FIELD_DFUSER="dfUser";
	public static final String FIELD_DFCONTEXTSOURCE="dfContextSource";
	public static final String FIELD_DFPREVIOUSVALUE="dfPreviousValue";
	public static final String FIELD_DFCURRENTVALUE="dfCurrentValue";
	public static final String FIELD_DFFIELD="dfField";
	public static final String FIELD_DFENTITY="dfEntity";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFTRANSACTIONDATE1="dfTransactionDate1";

	//--Release2.1--//
  //// Added to provide the scenarios translations.
	public static final String FIELD_DFPREVIOUSVALUETRANSLATED="dfPreviousValueTrans";
	public static final String FIELD_DFCURRENTVALUETRANSLATED="dfCurrentValueTrans";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

