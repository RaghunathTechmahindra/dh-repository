package mosApp.MosSystem;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.*;
import java.util.*;
import com.basis100.resources.*;
import com.basis100.log.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.util.*;
import com.basis100.deal.pk.*;
import com.basis100.deal.security.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.entity.*;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.html.*;

import MosSystem.*;

/**
 *
 *
 */
public class TaskNavigator extends Object
	implements Sc, Serializable
{
	/**
	 *
	 *
	 */
	//--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
	public boolean generateTaskName(SessionStateModelImpl theSessionState)
	{
		////PageEntry pe = theSession.getCurrentPage();
    PageEntry pe = theSessionState.getCurrentPage();

		////if (pe.isTaskNavigateMode() == false) return spider.visual.CSpPage.SKIP;

		////return spider.visual.CSpPage.PROCEED;

		if (pe.isTaskNavigateMode() == false) return false;

		return true;
	}


	/**
	 *
	 *
	 */
  //--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
	public boolean generateNextTaskPage(SessionStateModelImpl theSessionState)
	{
		////PageEntry pe = theSession.getCurrentPage();
    PageEntry pe = theSessionState.getCurrentPage();

		////if (pe.isTaskNavigateMode() == false) return spider.visual.CSpPage.SKIP;

		////if (pe.getNextPageName() == null) return spider.visual.CSpPage.SKIP;

		////return spider.visual.CSpPage.PROCEED;

		if (pe.isTaskNavigateMode() == false) return false;

		if (pe.getNextPageName() == null) return false;

		return true;

	}


	/**
	 *
	 *
	 */
	//--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
	public boolean generatePrevTaskPage(SessionStateModelImpl theSessionState)

	{
		////PageEntry pe = theSession.getCurrentPage();
    PageEntry pe = theSessionState.getCurrentPage();

		////if (pe.isTaskNavigateMode() == false) return spider.visual.CSpPage.SKIP;

		////if (pe.getPrevPageName() == null) return spider.visual.CSpPage.SKIP;

		////return spider.visual.CSpPage.PROCEED;

		if (pe.isTaskNavigateMode() == false) return false;

		if (pe.getPrevPageName() == null) return false;

		return true;

	}


	/**
	 *
	 *
	 */
	//--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
  public boolean generateForwardButtonStd(SessionStateModelImpl theSessionState)
	{
		PageEntry pe = theSessionState.getCurrentPage();

		////if (theSession.getCurrentPage().getPageCondition2() == false)
    if (pe.getPageCondition2() == false)
		{
			////return spider.visual.CSpPage.SKIP;
			return false;
		}

		////return spider.visual.CSpPage.PROCEED;
		return true;

	}


	/**
	 *
	 *
	 */
	//--> Changed the return value to boolean
  //-->   true : PROCEED (enable display)
  //-->   false : SKIP (disable display)
  //--> By BILLY 23July2002
	public boolean generateBackwardButtonStd(SessionStateModelImpl theSessionState)
	{
		PageEntry pe = theSessionState.getCurrentPage();

		////if (theSession.getCurrentPage().getPageCondition1() == false)
		if (pe.getPageCondition1() == false)
		{
			////return spider.visual.CSpPage.SKIP;
			return false;
		}

		////return spider.visual.CSpPage.PROCEED;
		return false;
	}

}

