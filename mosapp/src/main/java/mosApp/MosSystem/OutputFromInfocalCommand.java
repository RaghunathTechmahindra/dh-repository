package mosApp.MosSystem;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import com.iplanet.jato.*;
import com.iplanet.jato.command.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;

import com.basis100.log.*;
import com.basis100.deal.security.*;

import MosSystem.*;

public class OutputFromInfocalCommand extends Object
     implements Command
{
         /**
          *
          *
          */
     public OutputFromInfocalCommand()
     {
         super();
     }


     /**
      *
      *
      */
    public void execute(CommandEvent event)
       throws CommandException
    {
      logger = SysLog.getSysLogger("INFCMND");
      String operationName=event.getOperationName();

      logger.debug("OutputFromInfocalCommand@Command invoked.");
      logger.debug("OutputFromInfocalCommand@OperationName: " + operationName);

      RequestContext requestContext=event.getRequestContext();

      ViewInvocation invocation=((ViewCommandEvent)event).getInvocation();

      logger.debug("OutputFromInfocalCommand@ViewInvocation : " + invocation.toString());

      //Get the Parameters string
      String parameters = requestContext.getRequest().getParameter(invocation.getQualifiedChildName());

///logger.debug("VLAD ==> The Request Parameter = " + parameters);
////logger.debug("VLAD ==> The btActMsg Field value (By getDisplayFieldValue()) = " + ((ViewBean)(invocation.getTargetRequestHandler())).getDisplayFieldValue(invocation.getChildName()));
////logger.debug("VLAD ==> The Request Parameter (" + invocation.getQualifiedChildName() + "):: " + requestContext.getRequest().getParameter(invocation.getQualifiedChildName()));
///logger.debug("VLAD ==> The Request Parameter (" + invocation.getChildName() + "):: " + requestContext.getRequest().getParameter(invocation.getChildName()));
Enumeration tmpEm = requestContext.getRequest().getParameterNames();

for(int iPara = 0 ; tmpEm.hasMoreElements(); iPara++)
{
  String currPara = tmpEm.nextElement().toString();
  logger.debug("VLAd ==> The Request ParameterName[" + iPara + "]::" + currPara);
  logger.debug("VLAD ==> The Request ParameterValue[" + iPara + "]::" + requestContext.getRequest().getParameter(currPara));

  if(currPara.equals("pgLifeDisabilityInsurance_hdOutputFromRTPInfocalCalc"))
  {
      logger.debug("VLAd ==> Got my parameter");
  }

  if(currPara.equals("pgLifeDisabilityInsurance_btRecalculate"))
  {
      logger.debug("VLAd ==> Got my parameter button");
      logger.debug("VLAd ==> The Request ParameterName[" + iPara + "]::" + currPara);
      logger.debug("VLAD ==> The Request ParameterValue[" + iPara + "]::" + requestContext.getRequest().getParameter(currPara));
  }
}

    Method method=null;

    // Try to get the handleActMessageOK(String[] args) metdhod from the Page ViewBean
		try
		{
			method=invocation.getTargetRequestHandler().getClass().getMethod(
				METHOD_NAME ,new Class[] {String[].class});

    logger.debug("VLAd ==> MetodName: " + method.getName());

		}
		catch (NoSuchMethodException e)
		{
			// Method not implemented !!
      String errorMsg = "@handleRecalcInfocal.execute() :: Method \"handleRecalcInfocal(String[] args)\" not found @ " + invocation.getTargetRequestHandler();
      logger.error(errorMsg);
      throw new CommandException(errorMsg);
		}

    String [] args = new String [2];
    if(parameters != null)
    {
      // Convert the Parameters String to String array with delimeter = ','
      StringTokenizer tmpStrToken = new StringTokenizer(parameters, ",");
      try{
        args[0] = tmpStrToken.nextToken();
      }catch(Exception e){
        args[0] = "";
      }
      try{
        args[1] = tmpStrToken.nextToken();
      }catch(Exception e){
        args[1] = "";
      }
  logger.debug("OutputFromInfocalCommand@execute::args[0]: " + args[0]);
  logger.debug("OutputFromInfocalCommand@execute::args[1]: " + args[1]);
   }
    //// Populate the null mode manually.
    else
    {
logger.debug("OutputFromInfocalCommand@execute::Null mode");
      //// Just hardcode for now. May it generic later.
      args[0] = "PARSE_OUTPUT_FROM_DLL";
      args[1] = "61";
     }

      ////Case of handleRecalcInfocal().
      //Try to invoke the Method
      if(!args[0].equals(""))
      {
        try{
          method.invoke(invocation.getTargetRequestHandler(),	new Object[] {args});
        }
        catch(InvocationTargetException ie)
        {
          if(ie.getTargetException() instanceof CompleteRequestException)
          {
            logger.warning("OutputFromInfocalCommand@execute:: got CompleteRequestException : Most likely user logging out !!");
            throw ((CompleteRequestException)(ie.getTargetException()));
          }
          else
          {
            logger.error("OutputFromInfocalCommand@execute:: Handler method \""+METHOD_NAME+ "\" not accessible : " + ie.getTargetException().toString());
            throw new CommandException("Handler method \""+METHOD_NAME+
            "\" not accessible",ie.getTargetException());
          }
        }
        catch(Exception e)
        {
          logger.error("OutputFromInfocalCommand@execute:: Handler method \""+METHOD_NAME+ "\" not accessible : " + e.toString());
          throw new CommandException("Handler method \""+METHOD_NAME+
            "\" not accessible",e);
        }
     } // if
  }

   ////////////////////////////////////////////////////////////////////////////////
   // Class variables
   ////////////////////////////////////////////////////////////////////////////////

  public SysLogger logger;

  public static final String METHOD_NAME = "handleRecalcInfocal";

  public static final CommandDescriptor COMMAND_DESCRIPTOR=
    new CommandDescriptor(OutputFromInfocalCommand.class, Command.DEFAULT_OPERATION_NAME, null);

   ////////////////////////////////////////////////////////////////////////////////
   // Instance variables
   ////////////////////////////////////////////////////////////////////////////////
}


