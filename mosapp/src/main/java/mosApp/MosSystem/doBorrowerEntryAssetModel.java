package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doBorrowerEntryAssetModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssetId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAssetDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAssetValue();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetValue(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAssetIncludingGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetIncludingGDS(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfAssetIncludingTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfAssetIncludingTDS(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFASSETID="dfAssetId";
	public static final String FIELD_DFASSETDESCRIPTION="dfAssetDescription";
	public static final String FIELD_DFASSETVALUE="dfAssetValue";
	public static final String FIELD_DFASSETINCLUDINGGDS="dfAssetIncludingGDS";
	public static final String FIELD_DFASSETINCLUDINGTDS="dfAssetIncludingTDS";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

