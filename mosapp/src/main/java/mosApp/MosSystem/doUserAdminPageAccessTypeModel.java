package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doUserAdminPageAccessTypeModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPageId();

	
	/**
	 * 
	 * 
	 */
	public void setDfPageId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPageName();

	
	/**
	 * 
	 * 
	 */
	public void setDfPageName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfPageLabel();

	
	/**
	 * 
	 * 
	 */
	public void setDfPageLabel(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPAGEID="dfPageId";
	public static final String FIELD_DFPAGENAME="dfPageName";
	public static final String FIELD_DFPAGELABEL="dfPageLabel";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

