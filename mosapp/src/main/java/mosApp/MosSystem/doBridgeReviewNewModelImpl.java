package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public class doBridgeReviewNewModelImpl extends QueryModelBase
	implements doBridgeReviewNewModel
{
	/**
	 *
	 *
	 */
	public doBridgeReviewNewModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public String getDfBridgePurpose()
	{
		return (String)getValue(FIELD_DFBRIDGEPURPOSE);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgePurpose(String value)
	{
		setValue(FIELD_DFBRIDGEPURPOSE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfLender()
	{
		return (String)getValue(FIELD_DFLENDER);
	}


	/**
	 *
	 *
	 */
	public void setDfLender(String value)
	{
		setValue(FIELD_DFLENDER,value);
	}


	/**
	 *
	 *
	 */
	public String getDfProduct()
	{
		return (String)getValue(FIELD_DFPRODUCT);
	}


	/**
	 *
	 *
	 */
	public void setDfProduct(String value)
	{
		setValue(FIELD_DFPRODUCT,value);
	}


	/**
	 *
	 *
	 */
	public String getDfPaymentTermDescription()
	{
		return (String)getValue(FIELD_DFPAYMENTTERMDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfPaymentTermDescription(String value)
	{
		setValue(FIELD_DFPAYMENTTERMDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfRateCode()
	{
		return (String)getValue(FIELD_DFRATECODE);
	}


	/**
	 *
	 *
	 */
	public void setDfRateCode(String value)
	{
		setValue(FIELD_DFRATECODE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPostedInterestRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPOSTEDINTERESTRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfPostedInterestRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPOSTEDINTERESTRATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDiscount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDISCOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfDiscount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDISCOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPremium()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPREMIUM);
	}


	/**
	 *
	 *
	 */
	public void setDfPremium(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPREMIUM,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFNETRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfNetRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFNETRATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgeLoanAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGELOANAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgeLoanAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGELOANAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfMaturityDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFMATURITYDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfMaturityDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFMATURITYDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBridgeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBRIDGEID);
	}


	/**
	 *
	 *
	 */
	public void setDfBridgeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBRIDGEID,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRateDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFRATEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfRateDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFRATEDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfDealRateDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFDEALRATEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfDealRateDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFDEALRATEDATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealPostedRate()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALPOSTEDRATE);
	}


	/**
	 *
	 *
	 */
	public void setDfDealPostedRate(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALPOSTEDRATE,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProductId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFPRODUCTID);
	}


	/**
	 *
	 *
	 */
	public void setDfProductId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFPRODUCTID,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT BRIDGEPURPOSE.BPDESCRIPTION, LENDERPROFILE.LENDERNAME, MTGPROD.MTGPRODNAME, PAYMENTTERM.PTDESCRIPTION, PRICINGPROFILE.RATECODE, BRIDGE.POSTEDINTERESTRATE, BRIDGE.DISCOUNT, BRIDGE.PREMIUM, BRIDGE.NETINTERESTRATE, BRIDGE.NETLOANAMOUNT, BRIDGE.MATURITYDATE, BRIDGE.DEALID, BRIDGE.COPYID, BRIDGE.BRIDGEID, BRIDGE.RATEDATE, DEAL.RATEDATE, DEAL.POSTEDINTERESTRATE, MTGPROD.MTGPRODID FROM BRIDGE, DEAL, BRIDGEPURPOSE, LENDERPROFILE, MONTHS, MTGPROD, PAYMENTTERM, PRICINGPROFILE  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="BRIDGE, DEAL, BRIDGEPURPOSE, LENDERPROFILE, MONTHS, MTGPROD, PAYMENTTERM, PRICINGPROFILE";
	public static final String STATIC_WHERE_CRITERIA=" (BRIDGE.COPYID  =  DEAL.COPYID) AND (DEAL.DEALID  =  BRIDGE.DEALID) AND (BRIDGE.INVESTORPROFILEID  =  DEAL.INVESTORPROFILEID) AND (BRIDGE.BRIDGEPURPOSEID  =  BRIDGEPURPOSE.BRIDGEPURPOSEID) AND (BRIDGE.LENDERPROFILEID  =  LENDERPROFILE.LENDERPROFILEID) AND (BRIDGE.INTERESTTYPEID  =  MTGPROD.INTERESTTYPEID) AND (BRIDGE.PAYMENTTERMID  =  MTGPROD.PAYMENTTERMID) AND (BRIDGE.PRICINGPROFILEID  =  MTGPROD.PRICINGPROFILEID) AND (BRIDGE.PAYMENTTERMID  =  PAYMENTTERM.PAYMENTTERMID) AND (BRIDGE.PRICINGPROFILEID  =  PRICINGPROFILE.PRICINGPROFILEID) AND (DEAL.PRIVILEGEPAYMENTID  =  MTGPROD.PRIVILEGEPAYMENTID) AND (LENDERPROFILE.PROFILESTATUSID  =  PRICINGPROFILE.PROFILESTATUSID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBRIDGEPURPOSE="BRIDGEPURPOSE.BPDESCRIPTION";
	public static final String COLUMN_DFBRIDGEPURPOSE="BPDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFLENDER="LENDERPROFILE.LENDERNAME";
	public static final String COLUMN_DFLENDER="LENDERNAME";
	public static final String QUALIFIED_COLUMN_DFPRODUCT="MTGPROD.MTGPRODNAME";
	public static final String COLUMN_DFPRODUCT="MTGPRODNAME";
	public static final String QUALIFIED_COLUMN_DFPAYMENTTERMDESCRIPTION="PAYMENTTERM.PTDESCRIPTION";
	public static final String COLUMN_DFPAYMENTTERMDESCRIPTION="PTDESCRIPTION";
	public static final String QUALIFIED_COLUMN_DFRATECODE="PRICINGPROFILE.RATECODE";
	public static final String COLUMN_DFRATECODE="RATECODE";
	public static final String QUALIFIED_COLUMN_DFPOSTEDINTERESTRATE="BRIDGE.POSTEDINTERESTRATE";
	public static final String COLUMN_DFPOSTEDINTERESTRATE="POSTEDINTERESTRATE";
	public static final String QUALIFIED_COLUMN_DFDISCOUNT="BRIDGE.DISCOUNT";
	public static final String COLUMN_DFDISCOUNT="DISCOUNT";
	public static final String QUALIFIED_COLUMN_DFPREMIUM="BRIDGE.PREMIUM";
	public static final String COLUMN_DFPREMIUM="PREMIUM";
	public static final String QUALIFIED_COLUMN_DFNETRATE="BRIDGE.NETINTERESTRATE";
	public static final String COLUMN_DFNETRATE="NETINTERESTRATE";
	public static final String QUALIFIED_COLUMN_DFBRIDGELOANAMOUNT="BRIDGE.NETLOANAMOUNT";
	public static final String COLUMN_DFBRIDGELOANAMOUNT="NETLOANAMOUNT";
	public static final String QUALIFIED_COLUMN_DFMATURITYDATE="BRIDGE.MATURITYDATE";
	public static final String COLUMN_DFMATURITYDATE="MATURITYDATE";
	public static final String QUALIFIED_COLUMN_DFDEALID="BRIDGE.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="BRIDGE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFBRIDGEID="BRIDGE.BRIDGEID";
	public static final String COLUMN_DFBRIDGEID="BRIDGEID";
	public static final String QUALIFIED_COLUMN_DFRATEDATE="BRIDGE.RATEDATE";
	public static final String COLUMN_DFRATEDATE="RATEDATE";
	public static final String QUALIFIED_COLUMN_DFDEALRATEDATE="DEAL.RATEDATE";
	public static final String COLUMN_DFDEALRATEDATE="RATEDATE";
	public static final String QUALIFIED_COLUMN_DFDEALPOSTEDRATE="DEAL.POSTEDINTERESTRATE";
	public static final String COLUMN_DFDEALPOSTEDRATE="POSTEDINTERESTRATE";
	public static final String QUALIFIED_COLUMN_DFPRODUCTID="MTGPROD.MTGPRODID";
	public static final String COLUMN_DFPRODUCTID="MTGPRODID";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEPURPOSE,
				COLUMN_DFBRIDGEPURPOSE,
				QUALIFIED_COLUMN_DFBRIDGEPURPOSE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLENDER,
				COLUMN_DFLENDER,
				QUALIFIED_COLUMN_DFLENDER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRODUCT,
				COLUMN_DFPRODUCT,
				QUALIFIED_COLUMN_DFPRODUCT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPAYMENTTERMDESCRIPTION,
				COLUMN_DFPAYMENTTERMDESCRIPTION,
				QUALIFIED_COLUMN_DFPAYMENTTERMDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATECODE,
				COLUMN_DFRATECODE,
				QUALIFIED_COLUMN_DFRATECODE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPOSTEDINTERESTRATE,
				COLUMN_DFPOSTEDINTERESTRATE,
				QUALIFIED_COLUMN_DFPOSTEDINTERESTRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDISCOUNT,
				COLUMN_DFDISCOUNT,
				QUALIFIED_COLUMN_DFDISCOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPREMIUM,
				COLUMN_DFPREMIUM,
				QUALIFIED_COLUMN_DFPREMIUM,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNETRATE,
				COLUMN_DFNETRATE,
				QUALIFIED_COLUMN_DFNETRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGELOANAMOUNT,
				COLUMN_DFBRIDGELOANAMOUNT,
				QUALIFIED_COLUMN_DFBRIDGELOANAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMATURITYDATE,
				COLUMN_DFMATURITYDATE,
				QUALIFIED_COLUMN_DFMATURITYDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBRIDGEID,
				COLUMN_DFBRIDGEID,
				QUALIFIED_COLUMN_DFBRIDGEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRATEDATE,
				COLUMN_DFRATEDATE,
				QUALIFIED_COLUMN_DFRATEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALRATEDATE,
				COLUMN_DFDEALRATEDATE,
				QUALIFIED_COLUMN_DFDEALRATEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALPOSTEDRATE,
				COLUMN_DFDEALPOSTEDRATE,
				QUALIFIED_COLUMN_DFDEALPOSTEDRATE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFPRODUCTID,
				COLUMN_DFPRODUCTID,
				QUALIFIED_COLUMN_DFPRODUCTID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

