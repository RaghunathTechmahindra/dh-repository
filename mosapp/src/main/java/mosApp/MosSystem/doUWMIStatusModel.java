package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doUWMIStatusModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfMIStatusId();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIStatusId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIStatusDescription();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIStatusDescription(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfMIStatusUpdateAllowed();

	
	/**
	 * 
	 * 
	 */
	public void setDfMIStatusUpdateAllowed(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFMISTATUSID="dfMIStatusId";
	public static final String FIELD_DFMISTATUSDESCRIPTION="dfMIStatusDescription";
	public static final String FIELD_DFMISTATUSUPDATEALLOWED="dfMIStatusUpdateAllowed";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

