package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doCountTasksbyPriorityModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public Integer getDfPriorityCount();

	
	/**
	 * 
	 * 
	 */
	public void setDfPriorityCount(Integer value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFPRIORITYCOUNT="dfPriorityCount";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

