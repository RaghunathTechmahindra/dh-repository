package mosApp.MosSystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.commitment.CCM;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.DLM;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ViewBean;


/**
 *
 *
 */
public class SourceResubmissionHandler extends PageHandlerCommon
  implements Cloneable, Sc
{
  /**
   *
   *
   */
  public SourceResubmissionHandler cloneSS()
  {
    return(SourceResubmissionHandler) super.cloneSafeShallow();

  }


  //This method is used to populate the fields in the header
  //with the appropriate information

  public void populatePageDisplayFields()
  {
    ////SessionState theSession = getTheSession();

    PageEntry pg = theSessionState.getCurrentPage();

    populatePageShellDisplayFields();

    populateTaskNavigator(pg);

    populatePageDealSummarySnapShot();

    populatePreviousPagesLinks();

  }


  //
  // Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
  //

  public void setupBeforePageGeneration()
  {
    PageEntry pg = theSessionState.getCurrentPage();

    ViewBean theNDPage = getCurrNDPage();

    if (pg.getSetupBeforeGenerationCalled() == true) return;

    pg.setSetupBeforeGenerationCalled(true);


    // -- //
    setupDealSummarySnapShotDO(pg);

    int dealId = pg.getPageDealId();

    int copyId = pg.getPageDealCID();

    if (dealId <= 0)
    {
      setStandardFailMessage();
      logger.error("Exception @SourceResubmissionHandler.setupBeforePageGenerationProblem: invalid dealId (" + dealId + ") , Copy Id (" + copyId + ")");
      return;
    }


    // -- //
    Integer cspDealId = new Integer(dealId);

    Integer cspCopyId = new Integer(copyId);


/**
    // set criteria for data objects
    // Set up Original Deal DataObject
    CSpCriteriaSQLObject theOrigDealDO =(CSpCriteriaSQLObject) getModel(doOrigDealInfoModel.class);
    theOrigDealDO.clearUserWhereCriteria();
    theOrigDealDO.addUserWhereCriterion("dfDealId", "=", cspDealId);
    theOrigDealDO.addUserWhereCriterion("dfCopyId", "=", cspCopyId);

    // Execute
    theOrigDealDO.execute();

    if (theOrigDealDO.getNumRows() <= 0)
    {
      //getTheSession().setDialogAlertMessage("Wrong Deal No For Query..");
      setActiveMessageToAlert("Wrong Deal No For Query..", ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Problem encountered querying Original Deal Info - invalid dealId (" + dealId + ") , Copy Id (" + copyId + ")");
      return;
    }
**/

    doOrigDealInfoModel theOrigDealDO =
                      (doOrigDealInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doOrigDealInfoModel.class);

    theOrigDealDO.clearUserWhereCriteria();

    theOrigDealDO.addUserWhereCriterion("dfDealId", "=", cspDealId);

    theOrigDealDO.addUserWhereCriterion("dfCopyId", "=", cspCopyId);

    try
    {
      ResultSet rs = theOrigDealDO.executeSelect(null);

      if(rs == null || theOrigDealDO.getSize() < 0)
      {
        logger.debug("SRH@setupBeforePageGeneration::No row returned!!");
        setActiveMessageToAlert("Wrong Deal No For Query..", ActiveMsgFactory.ISCUSTOMCONFIRM);
        logger.error("Problem encountered querying Original Deal Info - invalid dealId (" + dealId + ") , Copy Id (" + copyId + ")");
        return;
      }
    }
    catch (ModelControlException mce)
    {
      logger.debug("SRH@setupBeforePageGeneration::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.debug("SRH@setupBeforePageGeneration::SQLException: " + sqle);
    }

    //Manual Set up display fileds for Original Deal
    // Underwriter Name
    int theUserId = 0;

    UserProfile up = null;

    try
    {
      up = new UserProfile(srk);

      ////theUserId = theOrigDealDO.getValue(0, com.iplanet.jato.util.TypeConverter.asInt("dfUnderwriterProfileId"));
      Object oTheUserId = theOrigDealDO.getValue(theOrigDealDO.FIELD_DFUNDERWRITERPROFILEID);

      if (oTheUserId == null)
      {
        theUserId = 0;
      }
      else if((oTheUserId != null))
      {
        theUserId = (new Integer(oTheUserId.toString())).intValue();
        logger.debug("SRH@setupBeforePageGeneration::UserIdFromModel: " + theUserId);
      }

      if (theUserId > 0)
      {
        up.findByPrimaryKey(new UserProfileBeanPK(theUserId, theSessionState.getDealInstitutionId()));
        theNDPage.setDisplayFieldValue("stOrigUnderwriter", 
                  new String(up.getUserFullName()));
      }
    }
    catch(Exception e)
    {
      theNDPage.setDisplayFieldValue("stOrigUnderwriter", new String("--"));
      logger.warning("@SourceResubmissionHandler problem when setup Original Underwirter Name !!");
    }


    // Administrator Name
    try
    {
      ////theUserId = theOrigDealDO.getValue(0, com.iplanet.jato.util.TypeConverter.asInt("dfAdministratorProfileId"));
      Object oTheUserId = theOrigDealDO.getValue(theOrigDealDO.FIELD_DFADMINISTRATORPROFILEID);

      if (oTheUserId == null)
      {
        theUserId = 0;
      }
      else if((oTheUserId != null))
      {
        theUserId = (new Integer(oTheUserId.toString())).intValue();
        logger.debug("SRH@setupBeforePageGeneration::AdminIdFromModel: " + theUserId);
      }

      if (theUserId > 0)
      {
        up.findByPrimaryKey(new UserProfileBeanPK(theUserId, theSessionState.getDealInstitutionId()));
        theNDPage.setDisplayFieldValue("stOrigAdministrator", new String(up.getUserFullName()));
      }
    }
    catch(Exception e)
    {
      theNDPage.setDisplayFieldValue("stOrigAdministrator", new String("--"));
      logger.warning("@SourceResubmissionHandler problem when setup Original Administrator Name !!");
    }


    // Funder Name
    try
    {
      ////theUserId = theOrigDealDO.getValue(0, com.iplanet.jato.util.TypeConverter.asInt("dfFunderProfileId"));
      Object oTheUserId = theOrigDealDO.getValue(theOrigDealDO.FIELD_DFFUNDERPROFILEID);

      if (oTheUserId == null)
      {
        theUserId = 0;
      }
      else if((oTheUserId != null))
      {
        theUserId = (new Integer(oTheUserId.toString())).intValue();
        logger.debug("SRH@setupBeforePageGeneration::FunderIdFromModel: " + theUserId);
      }

      if (theUserId > 0)
      {
        up.findByPrimaryKey(new UserProfileBeanPK(theUserId, theSessionState.getDealInstitutionId()));
        theNDPage.setDisplayFieldValue("stOrigFunder", new String(up.getUserFullName()));
      }
    }
    catch(Exception e)
    {
      theNDPage.setDisplayFieldValue("stOrigFunder", new String("--"));
      logger.warning("@SourceResubmissionHandler problem when setup Original Funder Name !!");
    }


    ////SYNCADD.
    // Execute the DealNote dataObject -- Added button to popup DealNote Window
    //  -- By BILLY 28May2002
    // set the criteria for the populating data object
    //// BXTODO: Not sure this should be done here. Check this.
    ////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doDealNotesInfoModel.class);
    doDealNotesInfoModel theDO =
                      (doDealNotesInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doDealNotesInfoModel.class);


    theDO.clearUserWhereCriteria();

    theDO.addUserWhereCriterion("dfDealID", "=", cspDealId);


    //=========================================================================
    // Get New DealId and info.
    int newDealId = 0;

    int newCopyId = 0;

    try
    {
      ////newDealId = theOrigDealDO.getValue(0, com.iplanet.jato.util.TypeConverter.asInt("dfNewDealId"));
      Object oNewDealId = theOrigDealDO.getValue(theOrigDealDO.FIELD_DFNEWDEALID);

      if (oNewDealId == null)
      {
        newDealId = 0;
      }
      else if((oNewDealId != null))
      {
        newDealId = (new Integer(oNewDealId.toString())).intValue();
        logger.debug("SRH@setupBeforePageGeneration::NewDealIdFromModel: " + newDealId);
      }

      // Get Gold copyid of the New Deal
      newCopyId =(new MasterDeal(srk, null, newDealId)).getGoldCopyId();
    }
    catch(Exception e)
    {
      //getTheSession().setDialogAlertMessage("Wrong Deal No For Query..");
      setActiveMessageToAlert("Wrong New Deal No", ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Problem encountered when getting New Deal number : " + e.getMessage());
      return;
    }

/**
    // Set up New Deal DataObject
    CSpCriteriaSQLObject theNewDealDO =(CSpCriteriaSQLObject) getModel(doNewDealInfoModel.class);

    theNewDealDO.clearUserWhereCriteria();

    theNewDealDO.addUserWhereCriterion("dfDealId", "=", new Integer(newDealId));

    theNewDealDO.addUserWhereCriterion("dfCopyId", "=", new Integer(newCopyId));


    // Execute
    theNewDealDO.execute();

    if (theNewDealDO.getNumRows() <= 0)
    {
      //getTheSession().setDialogAlertMessage("Wrong Deal No For Query..");
      setActiveMessageToAlert("Wrong New Deal No For Query..", ActiveMsgFactory.ISCUSTOMCONFIRM);
      logger.error("Problem encountered querying New Deal Info - invalid dealId (" + newDealId + ") , Copy Id (" + newCopyId + ")");
      return;
    }
**/
///////////////

    doNewDealInfoModel theNewDealDO =
                      (doNewDealInfoModel) RequestManager.getRequestContext().getModelManager().getModel(doNewDealInfoModel.class);

    theNewDealDO.clearUserWhereCriteria();

    theNewDealDO.addUserWhereCriterion("dfDealId", "=", new Integer(newDealId));

    theNewDealDO.addUserWhereCriterion("dfCopyId", "=", new Integer(newCopyId));

    try
    {
      ResultSet rs = theNewDealDO.executeSelect(null);

      if(rs == null || theNewDealDO.getSize() < 0)
      {
        //getTheSession().setDialogAlertMessage("Wrong Deal No For Query..");
        setActiveMessageToAlert("Wrong New Deal No For Query..", ActiveMsgFactory.ISCUSTOMCONFIRM);
        logger.error("Problem encountered querying New Deal Info - invalid dealId (" + newDealId + ") , Copy Id (" + newCopyId + ")");
        return;
      }
    }
    catch (ModelControlException mce)
    {
      logger.debug("SRH@setupBeforePageGeneration::MCEException: " + mce);
    }

    catch (SQLException sqle)
    {
      logger.debug("SRH@setupBeforePageGeneration::SQLException: " + sqle);
    }

    //Manual Set up display fileds for New Deal
    // Underwriter Name
    try
    {
      ////theUserId = theNewDealDO.getValue(0, com.iplanet.jato.util.TypeConverter.asInt("dfUnderwriterProfileId"));
      Object oTheUserId = theNewDealDO.getValue(theOrigDealDO.FIELD_DFUNDERWRITERPROFILEID);

      if (oTheUserId == null)
      {
        theUserId = 0;
      }
      else if((oTheUserId != null))
      {
        theUserId = (new Integer(oTheUserId.toString())).intValue();
        logger.debug("SRH@setupBeforePageGeneration::UserIdFromModel: " + theUserId);
      }

      if (theUserId > 0)
      {
        up.findByPrimaryKey(new UserProfileBeanPK(theUserId, theSessionState.getDealInstitutionId()));
        theNDPage.setDisplayFieldValue("stNewUnderwriter", new String(up.getUserFullName()));
      }
    }
    catch(Exception e)
    {
      theNDPage.setDisplayFieldValue("stNewUnderwriter", new String("--"));
      logger.warning("@SourceResubmissionHandler problem when setup New Underwirter Name !!");
    }


    // Administrator Name
    try
    {
      ////theUserId = theNewDealDO.getValue(0, com.iplanet.jato.util.TypeConverter.asInt("dfAdministratorProfileId"));
      Object oTheUserId = theNewDealDO.getValue(theOrigDealDO.FIELD_DFADMINISTRATORPROFILEID);

      if (oTheUserId == null)
      {
        theUserId = 0;
      }
      else if((oTheUserId != null))
      {
        theUserId = (new Integer(oTheUserId.toString())).intValue();
        logger.debug("SRH@setupBeforePageGeneration::AdminIdFromModel: " + theUserId);
      }

      if (theUserId > 0)
      {
        up.findByPrimaryKey(new UserProfileBeanPK(theUserId, theSessionState.getDealInstitutionId()));
        theNDPage.setDisplayFieldValue("stNewAdministrator", 
                   new String(up.getUserFullName()));
      }
    }
    catch(Exception e)
    {
      theNDPage.setDisplayFieldValue("stNewAdministrator", new String("--"));
      logger.warning("@SourceResubmissionHandler problem when setup New Administrator Name !!");
    }


    // Funder Name
    try
    {
      ////theUserId = theNewDealDO.getValue(0, com.iplanet.jato.util.TypeConverter.asInt("dfFunderProfileId"));
      Object oTheUserId = theNewDealDO.getValue(theOrigDealDO.FIELD_DFFUNDERPROFILEID);

      if (oTheUserId == null)
      {
        theUserId = 0;
      }
      else if((oTheUserId != null))
      {
        theUserId = (new Integer(oTheUserId.toString())).intValue();
        logger.debug("SRH@setupBeforePageGeneration::FunderIdFromModel: " + theUserId);
      }

      if (theUserId > 0)
      {
        up.findByPrimaryKey(new UserProfileBeanPK(theUserId, theSessionState.getDealInstitutionId()));
        theNDPage.setDisplayFieldValue("stNewFunder", new String(up.getUserFullName()));
      }
    }
    catch(Exception e)
    {
      theNDPage.setDisplayFieldValue("stNewFunder", new String("--"));
      logger.warning("@SourceResubmissionHandler problem when setup New Funder Name !!");
    }

  }


  //// This method is moved to the base PHC class.
  /**
  public int displayViewOnlyTag(DisplayField field)
  {
    ////SessionState theSession = getTheSession();

    PageEntry pg = theSessionState.getCurrentPage();

    if (pg.isEditable() == false)
    {
      field.setHtmlText("<IMG NAME=\"stViewOnlyTag\" SRC=\"/eNet_images/ViewOnly.gif\" ALIGN=TOP>");
      return CSpPage.PROCEED;
    }

    return CSpPage.SKIP;

  }
  **/

  /**
   *
   *
   */
  public void handleUpdateViaDealMod()
  {
    ////SessionState theSession = getTheSession();

    PageEntry pg = theSessionState.getCurrentPage();


    //--Meger--//
    // Product required to check if DealStatus Category = pre-decision then
    // goto UWWorkSheet
    // Modified By Billy 17Sept2002
    try
    {
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
      if (deal.getStatusCategoryId() == Sc.DEAL_STATUS_CATEGORY_DECISION)
      {
        srk.beginTransaction();

        //Clear resubmission flag
        deal.setResubmissionFlag("N");
        deal.ejbStore();
        workflowTrigger(2, srk, pg, null);
        srk.commitTransaction();

        // Set to display UWWorkSheet next
        PageEntry pgEntry = setupMainPagePageEntry(Mc.PGNM_UNDERWRITER_WORKSHEET, 
                                                   pg.getPageDealId(), - 1, 
                                                   pg.getDealInstitutionId());
        getSavedPages().setNextPage(pgEntry);
      }
      else
      {
        // Unlock the deal
        //Perform DecisionModification and clear the resubmission flag
        (new DecisionModificationHandler(this)).perfromDecisionModificationRequest(Mc.DM_NON_CRITICAL_NO_DOC_PREP, pg, srk, false, true);

        // Set Sub-page to Deal Modification
        PageEntry pgEntry = setupMainPagePageEntry(Mc.PGNM_DEAL_MODIFICATION, 
                                                   pg.getPageDealId(), - 1,
                                                   pg.getDealInstitutionId());

        // Make sure not to display Post-Decision Dailogue
        pgEntry.setPageCondition4(true);

        // Set to run MI rules when submit
        pgEntry.setPageCondition6(true);
        getSavedPages().setNextPage(pgEntry);
      }
    }

    //================================================================
    catch(Exception e)
    {
      logger.error("Exception @SourceResubmissionHandler.handleUpdateViaDealMod()");
      logger.error(e);
      setStandardFailMessage();
    }


    // if no message set (no eror encountered) then away we go ...
    if (theSessionState.isActMessageSet() == false)
    {
      navigateToNextPage(true);
    }

  }


  /**
   *
   *
   */
  public void handleReDecision()
  {
    ////SessionState theSession = getTheSession();

    PageEntry pg = theSessionState.getCurrentPage();


    //--Meger--//
    // Product required to check if DealStatus Category <> post-decision then
    // not allowed to do this
    // Modified By Billy 17Sept2002
    try
    {
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
      if (deal.getStatusCategoryId() != Sc.DEAL_STATUS_CATEGORY_POST_DECISION)
      {
        setActiveMessageToAlert(BXResources.getSysMsg("SOB_RESUBMIT_DEAL_CANNOT_REDECISION", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
      }
      else
      {
        //Perform re-underwrite and clear the resubmission flag
        (new DecisionModificationHandler(this)).perfromDecisionModificationRequest(Mc.DM_COMPLETE_RE_UWR_REQUESTED, pg, srk, false, true);

        // Set to display UWWorkSheet next
        PageEntry pgEntry = setupMainPagePageEntry(Mc.PGNM_UNDERWRITER_WORKSHEET, 
                                                   pg.getPageDealId(), - 1,
                                                   pg.getDealInstitutionId());
        getSavedPages().setNextPage(pgEntry);
      }
    }
    catch(Exception e)
    {
      logger.error("Exception @SourceResubmissionHandler.handleReDecision()");
      logger.error(e);
      setStandardFailMessage();
    }


    // if no message set (no eror encountered) then away we go ...
    if (theSessionState.isActMessageSet() == false)
    {
      navigateToNextPage(true);
    }

  }


  /**
   *
   *
   */
  public void handleAdoptNewDeal()
  {
    PageEntry pg = theSessionState.getCurrentPage();

    SessionResourceKit srk = getSessionResourceKit();

    SysLogger logger = srk.getSysLogger();

    String msg;

    logger.trace("BILLY===> @SourceResubmissionHandler.handleAdoptNewDeal");

    try
    {
      DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);

      //Collapse the Original deal without respond to source
      Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());

      // Check if action allowed ?? Necessary ??
      CCM ccm = new CCM(srk);
      if (! ccm.checkIfActionAllowed(deal))
      {
        setActiveMessageToAlert(BXResources.getSysMsg("CCM_ACTION_NOT_ALLOWED_MI_CHECK_FAILED", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
      }
      srk.beginTransaction();
      ccm.collaspeDeal(deal);

      //Clear resubmission flag
      deal.setResubmissionFlag("N");

      //--Meger--//
      // Requested by Dave Krick to set the Denial reason to 20
      deal.setDenialReasonId(Mc.DENIAL_REASON_NEW_DEAL_ADOPTED);

      //==========================================================
      deal.ejbStore();

      //// SYNCADD.
      // Unlock the original deal here -- Bug fix by BIlly 14Aug2002
      DLM.getInstance().unlock(deal.getDealId(), srk.getExpressState().getUserProfileId(), srk, true);

      //=============================================================
      workflowTrigger(2, srk, pg, null);

      //Set New deal.status to Received
      int newDealId = Integer.parseInt(deal.getReferenceDealNumber());
      int newCopyId =(new MasterDeal(srk, null, newDealId)).getGoldCopyId();
      Deal newDeal = DBA.getDeal(srk, newDealId, newCopyId);
      newDeal.setStatusId(Mc.DEAL_RECEIVED);
      newDeal.setStatusDate(new java.util.Date());
      msg = hLog.statusChange(deal.getStatusId(), "SOB Resubmission");
      hLog.log(newDeal.getDealId(), newDeal.getCopyId(), msg, Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), newDeal.getStatusId());

      //// SYNCADD.
      // Changed to clear the DenialReason to 0 -- Requested by Dave Krick
      // -- Modified by Billy 12Aug2002
      //Clear DenialReason
      //newDeal.setDenialReasonId(Mc.DENIAL_REASON_SOURCE_RESUBMISSION);
      newDeal.setDenialReasonId(Mc.DENIAL_REASON_OTHER);

      //===================================================================
      //Copy the Users assigned to the Orig deal to New deal
      if (deal.getUnderwriterUserId() > 0) newDeal.setUnderwriterUserId(deal.getUnderwriterUserId());
      if (deal.getAdministratorId() > 0) newDeal.setAdministratorId(deal.getAdministratorId());
      if (deal.getFunderProfileId() > 0) newDeal.setFunderProfileId(deal.getFunderProfileId());

      //--> Ticket # 130 ==> to preserve the Mortgage Servicing Number
      //--> By Billy 24Nov2003
      newDeal.setServicingMortgageNumber(deal.getServicingMortgageNumber());
      
      // -------------- Catherine, 15Feb05, BMO to CCAPS ---------------
      // copy servicingMortgageNumber
      String val = deal.getServicingMortgageNumber();
      logger.debug("DealEntryHandler.handleResurrect(): servicingMortgageNumber for old deal = " + val);
      newDeal.setServicingMortgageNumber(val);
      logger.debug("DealEntryHandler.handleResurrect(): SERVICINGMORTGAGENUMBER copied over");
      
      //==============================================================
      // bilingual deal notes 
      if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(), Xc.COM_FILOGIX_DEAL_COPY_LENDER_NOTES,"N").equalsIgnoreCase("Y"))
      {
    	  DealNotes note = new DealNotes(srk);
    	  note.copyDealNotes(deal, newDeal, Mc.DEAL_NOTES_CATEGORY_INGESTION_LENDER);
          logger.debug("SourceResubmissionHandler.handleAdoptNewDeal(): DEAL_NOTES_CATEGORY_INGESTION_LENDER added to a new deal");
      }

      newDeal.ejbStore();

      // -------------- Catherine, 15Feb05, BMO to CCAPS debug
      logger.debug("DealEntryHandler.handleResurrect(): servicingMortgageNumber for new deal = " + newDeal.getServicingMortgageNumber());
      
      // Setup to call workflow with the new deal
      pg.setPageDealId(newDealId);
      pg.setPageDealCID(newCopyId);
      pg.setPageDealApplicationId("" + newDealId);

      //Call workflow to reassign users if necessary and start init tasks
      workflowTrigger(1, srk, pg, null);

      //--> Bug fix : to send CapLink after deal reassignment
      //--> Because DOC-54 (CapLink) rule is relied on the BranchProfileId
      //--> By Billy 13June2003
      //--> New Requirement to send CAP link doc.
      //--> By Billy 10Mar2003
      //logger.debug("SRH@RequestTDCapLink: stamp1");
      DocumentRequest.requestTDCTCapLink(srk, newDeal);
      //========================================
      //logger.debug("SRH@RequestTDCapLink: stamp2");
      //--> Ticket#371 :: Change for Cervus :: changes to support send @ all stages
      //--> By Billy 09July2004
      ccm.sendServicingUpload(srk, newDeal, Mc.SERVICING_UPLOAD_TYPE_RESUBMIT, theSessionState.getLanguageId());
      //--> Additional requestment : send upload for the Original deal as well
      //--> Modified by Billy 29July2004
      ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_RESUBMIT, theSessionState.getLanguageId());
      //===========================================================================

      srk.commitTransaction();

      // Set to display WorkSheet with new deal
      PageEntry pgEntry = setupMainPagePageEntry(Mc.PGNM_UNDERWRITER_WORKSHEET, 
                                                 newDealId, - 1,
                                                 newDeal.getInstitutionProfileId());
      getSavedPages().setNextPage(pgEntry);
    }
    catch(Exception e)
    {
      logger.error("Exception @SourceResubmissionHandler.handleAdoptNewDeal()");
      logger.error(e);
      setStandardFailMessage();
    }

    navigateToNextPage(true);

  }

  //// This method is moved to the base PHC class.
  /**
  // Added new button to popup DealNote window -- By BILLY 28May2002
  public boolean populateDealNotesWin(int ind, doDealNotesInfoModel theModel)
  {
    ////CSpDataObject theObj =(CSpDataObject) getModel(doDealNotesInfoModel.class);
    ////if (theObj.getNumRows() <= 0)
    ////    return false;

    // ALERT :: Commented this oout to not doing anything before we fixing the Data problem of the Bureau from Morty
    //          The Report content may contain some hidden char e.g. Char(0) ==> caused JavaScript problems.
    //getCurrNDPage().setDisplayFieldValue("Repeated2.stCreditBureauReport", new String( (String)(reports.get(ind)) ) );
    ////BXTODO: This iteration should be moved to the TiledView calleer and passed from here.

    ////String tmpStr = theObj.getValue(ind, "dfDealNotesText").toString();
    logger.debug("SRH@populateDealNotesWin::Start");

    String tmpStr = (theModel.getValue(theModel.FIELD_DFDEALNOTESTEXT)).toString();
    logger.debug("SRH@populateDealNotesWin::DealNotesText:: " + tmpStr);

    tmpStr = convertToHtmString2(tmpStr);
    logger.debug("SRH@populateDealNotesWin::DealNotesText_AfterConversion: " + tmpStr);

    getCurrNDPage().setDisplayFieldValue("rptDealNoteWin/stNoteText1", new String(tmpStr));
    logger.debug("SRH@populateDealNotesWin::After set the Note");

    return true;
  }
**/
}

