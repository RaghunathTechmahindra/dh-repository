package mosApp.MosSystem;


import MosSystem.Sc;
import com.basis100.resources.PropertiesCache;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;



/**
 * <p> Title: QualifyingDetailsHandler </p>
 *
 * <p> Description: This class handles displaying of data </p>
 *
 * @author MCM Impl team
 * @version 1.0 06-JUN-2008 XS_2.13 Initial Version 
 *
 */



public class QualifyingDetailsHandler extends PageHandlerCommon
implements Cloneable, Sc
{
    /**
     * <p>
     * Description: Constructor that calls the super class constructor and
     * initializes the session state and the saved pages
     * </p>
     * 
     * @version 1.0 06-JUN-2008 XS_2.13Initial version
     */
    public QualifyingDetailsHandler() {
        super();
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * <p>
     * Description: Constructor that calls the super class constructor and
     * initializes the session state and the saved pages by passing the name of
     * the handler to the overloaded constructor
     * </p>
     * 
     * @version 1.0 06-JUN-2008 XS_2.13Initial version
     */
    public QualifyingDetailsHandler(String name) {
        this(null, name);
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * <p>
     * Description: Constructor that calls the super class constructor and
     * initializes the session state and the saved pages by passing the name of
     * the handler and the parent view bean to the overloaded constructor
     * </p>
     * 
     * @version 1.0 06-JUN-2008 XS_2.13Initial version
     */
    public QualifyingDetailsHandler(View parent, String name) {
        super(parent, name);
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * This Constructor returns the instance of this class.
     * 
     * @return
     */
    public QualifyingDetailsHandler cloneSS() {
        return (QualifyingDetailsHandler) super.cloneSafeShallow();

    }

    /**
     * <p>
     * Description :This method is used to populate the fields in the header
     * with the appropriate information
     * </p>
     * 
     * @version 1.0 06-JUN-2008 XS_2.13Initial version
     */

    public void populatePageDisplayFields() {

        ViewBean thePage = getCurrNDPage();
        thePage.setDisplayFieldValue("qualifyingDetailsPagelet", new String(
                TARGET_TAG));
    }


    /**
     * <p>Description: This method initializes the model for display  </p>
     *
     * @version 1.0 06-JUN-2008 XS_2.13 Initial Version 
     */
    public void setupBeforePageGeneration()
    {
        ViewBean thePage = getCurrNDPage();
        PageEntry pg = getTheSessionState().getCurrentPage();
        try
        {
            //Execute the model passing dealId and institutionId
            int dealId=pg.getPageDealId();
            int dealInstitutionId= pg.getDealInstitutionId();

            doQualifyingDetailsModelImpl qualifyingDetailsModel = (doQualifyingDetailsModelImpl)
            RequestManager.getRequestContext().getModelManager().getModel(doQualifyingDetailsModel.class);

            qualifyingDetailsModel.clearUserWhereCriteria();
            qualifyingDetailsModel.addUserWhereCriterion("dfDealId", "=", dealId);
            qualifyingDetailsModel.addUserWhereCriterion("dfInstitutionProfileId", "=", dealInstitutionId);
            qualifyingDetailsModel.executeSelect(null);

            //Set Amortization with years and months seperated 
            if(qualifyingDetailsModel.getDfAmortizationTerm()!=null){
                setTermsDisplayField(qualifyingDetailsModel, "stAmortizationYrs", "stAmortizationMths", qualifyingDetailsModel.FIELD_DFAMORTIZATIONTERM, 0);
            }

            //Read flag for displaying Qualifying details section from sysproperty configuration
            String usePOSQualifyDetail = null;
            usePOSQualifyDetail = PropertiesCache.getInstance().
            getProperty(theSessionState.getDealInstitutionId(),"com.basis100.deal.usePOSQualifyDetail");

            //If 'usePOSQualifyDetail' is "Y" and the current screen is not Deal entry
            //then display Qualifying Details section
            if(pg.isDealEntry()==false && "Y".equals(usePOSQualifyDetail))
            {

                //If qualifying data is present then display it
                if(qualifyingDetailsModel.getDfDealId()!= null)			 		
                {	
                    startSupressContent(pgQualifyingDetailsPageletView.CHILD_STQUALIFYINGDETAILSMESSAGESTART, thePage);
                    endSupressContent(pgQualifyingDetailsPageletView.CHILD_STQUALIFYINGDETAILSMESSAGEEND, thePage);
                }	
                //otherwise display 'Not available' message
                else
                {
                    startSupressContent(pgQualifyingDetailsPageletView.CHILD_STQUALIFYINGDETAILSFIELDSSTART, thePage);
                    endSupressContent(pgQualifyingDetailsPageletView.CHILD_STQUALIFYINGDETAILSFIELDSEND, thePage);
                }
            }
            else
            {
                startSupressContent(pgQualifyingDetailsPageletView.CHILD_STQUALIFYINGDETAILSSECTIONSTART, thePage);
                endSupressContent(pgQualifyingDetailsPageletView.CHILD_STQUALIFYINGDETAILSSECTIONEND, thePage);

            }
        }

        catch (Exception e) {
            logger.error(
            "Exception @doQualifyingDetailsModel.setupBeforePageGeneration()");
            logger.error(e);
            setStandardFailMessage();
            return;
        }


    }


}

