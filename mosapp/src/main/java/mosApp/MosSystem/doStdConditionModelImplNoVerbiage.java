package mosApp.MosSystem;

import java.sql.Clob;
import java.sql.SQLException;

import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.log.SysLog;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

public class doStdConditionModelImplNoVerbiage extends QueryModelBase
	implements doStdConditionModel
{
	/**
	 *
	 *
	 */
	public doStdConditionModelImplNoVerbiage()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    ////To override all the Description fields by populating from BXResource.
    ////Get the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
                                          (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                           SessionStateModel.class,
                                           defaultInstanceStateName,
                                           true);

    int languageId = theSessionState.getLanguageId();

    while(this.next())
    {
      //// Convert Condition Responsibility Role Description.
      this.setDfResponsibility(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
                                                                  "CONDITIONRESPONSIBILITYROLE", this.getDfRoleId().intValue(), languageId));

      //// Convert Document Status Description.
      this.setDfStatus(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
                                                          "DOCUMENTSTATUS", this.getDfStatus(), languageId));

    }
    ////Reset Location
    this.beforeFirst();
	}

	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFACTION);
	}


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value)
	{
		setValue(FIELD_DFACTION,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel()
	{
		return (String)getValue(FIELD_DFDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value)
	{
		setValue(FIELD_DFDOCUMENTLABEL,value);
	}

	/**
	 *
	 *
	 */
	public String getDfDocumentText()
	{
		return (String)getValue(FIELD_DFDOCUMENTTEXT);
	}

	/**
	 *
	 *
	 */
	public void setDfDocumentText(String value)
	{
		setValue(FIELD_DFDOCUMENTTEXT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfStdDocTrackId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFSTDDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void setDfStdDocTrackId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFSTDDOCTRACKID,value);
	}

  //--TD_CONDR_CR--start--//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRoleId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFROLEID);
	}

	/**
	 *
	 *
	 */
	public void setDfRoleId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFROLEID,value);
	}
	/**
	 *
	 *
	 */
	public String getDfResponsibility()
	{
		return (String)getValue(FIELD_DFRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public void setDfResponsibility(String value)
	{
		setValue(FIELD_DFRESPONSIBILITY,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDocTrackId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public void setDfDocTrackId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOCTRACKID,value);
	}
	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRequestDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFREQUESTDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfRequestDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFREQUESTDATE,value);
	}

	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfStatusChangeDate()
	{
		return (java.sql.Timestamp)getValue(FIELD_DFSTATUSCHANGEDATE);
	}


	/**
	 *
	 *
	 */
	public void setDfStatusChangeDate(java.sql.Timestamp value)
	{
		setValue(FIELD_DFSTATUSCHANGEDATE,value);
	}

	/**
	 *
	 *
	 */
	public String getDfStatus()
	{
		return (String)getValue(FIELD_DFSTATUS);
	}


	/**
	 *
	 *
	 */
	public void setDfStatus(String value)
	{
		setValue(FIELD_DFSTATUS,value);
	}
  //--TD_CONDR_CR--end--//

  //--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public java.math.BigDecimal getDfLanguagePrederenceId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFLANGUAGEPREFERENCEID);
	}

	public void setDfLanguagePrederenceId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFLANGUAGEPREFERENCEID,value);
	}


	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //--> Added new LanguageId datafield in order to setup Language Criteria when page displayed
  //--> By Billy 26Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
	//public static final String SELECT_SQL_TEMPLATE="SELECT ALL DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKING.DEALID, DOCUMENTTRACKING.COPYID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKING.DOCUMENTTRACKINGID FROM CONDITION, DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE  __WHERE__  ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";
/* CIBC DTV	
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT ALL DOCUMENTTRACKING.DOCUMENTSTATUSID, DOCUMENTTRACKING.DEALID, "+
    "DOCUMENTTRACKING.COPYID, DOCUMENTTRACKINGVERBIAGE.DOCUMENTLABEL, "+
    "DOCUMENTTRACKINGVERBIAGE.DOCUMENTTEXT, DOCUMENTTRACKING.DOCUMENTTRACKINGID, "+
    //--> Added LanguagePreferenceId
    "DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID, "+
    //--TD_CONDR_CR--start//
    "DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID, " +
    "to_char(DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID) DOCUMENTRESPONSIBILITYROLE_STR, " +
    "DOCUMENTTRACKING.DOCUMENTTRACKINGID, " +
    "DOCUMENTTRACKING.DOCUMENTREQUESTDATE, " +
    "DOCUMENTTRACKING.DSTATUSCHANGEDATE, " +
    "to_char(DOCUMENTTRACKING.DOCUMENTSTATUSID) DOCUMENTSTATUS_STR " +
    //--TD_CONDR_CR--end//
    "FROM CONDITION, DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE  "+
    "__WHERE__  "+
    "ORDER BY DOCUMENTTRACKING.DOCUMENTTRACKINGID  ASC";
*/
	public static final String SELECT_SQL_TEMPLATE=
		"SELECT   documenttracking.documentstatusid, " +  
			"documenttracking.dealid, " +  
			"documenttracking.copyid, " +  
			"NVL(documenttrackingverbiage.DOCUMENTLABEL ,conditionverbiage.conditionlabel)DOCUMENTLABEL, " +  
			"null DOCUMENTTEXT, " +  
			"documenttracking.documenttrackingid, " +  
			"conditionverbiage.languagepreferenceid, " +  
			"documenttracking.documentresponsibilityroleid, " +  
			"TO_CHAR(documenttracking.documentresponsibilityroleid) documentresponsibilityrole_str, " +  
			"documenttracking.documenttrackingid, " +  
			"documenttracking.documentrequestdate, " +  
			"documenttracking.dstatuschangedate, " +  
			"TO_CHAR (documenttracking.documentstatusid) documentstatus_str " +  
		"FROM condition  " +  
			"JOIN  " +  
			"conditionverbiage " +  
			"ON  condition.conditionid = conditionverbiage.conditionid " +  
			"AND condition.institutionprofileid = conditionverbiage.institutionprofileid " + 
			"JOIN  " +  
			"documenttracking " +  
			"ON  condition.conditionid = documenttracking.conditionid " +  
			"AND condition.institutionprofileid = documenttracking.institutionprofileid " + 
			"LEFT OUTER JOIN  " +  
			"documenttrackingverbiage " +  
			"ON  documenttracking.documenttrackingid = documenttrackingverbiage.documenttrackingid " +  
                          "AND documenttracking.copyid = documenttrackingverbiage.copyid " +  
                          "AND conditionverbiage.languagepreferenceid = documenttrackingverbiage.languagepreferenceid " +  
                          "AND conditionverbiage.institutionprofileid = documenttrackingverbiage.institutionprofileid " + 
        "__WHERE__ " +  
        "ORDER BY documenttracking.documenttrackingid"; 

	
  public static final String MODIFYING_QUERY_TABLE_NAME="CONDITION, DOCUMENTTRACKING, DOCUMENTTRACKINGVERBIAGE";
	//--Release2.1--//
  //--> Take out the HardCoded Criteria, DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0
  //public static final String STATIC_WHERE_CRITERIA=" (((DOCUMENTTRACKING.COPYID  =  DOCUMENTTRACKINGVERBIAGE.COPYID) AND (DOCUMENTTRACKING.DOCUMENTTRACKINGID  =  DOCUMENTTRACKINGVERBIAGE.DOCUMENTTRACKINGID) AND (DOCUMENTTRACKING.CONDITIONID  =  CONDITION.CONDITIONID)) AND DOCUMENTTRACKINGVERBIAGE.LANGUAGEPREFERENCEID = 0 AND DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 1) ";
	public static final String STATIC_WHERE_CRITERIA=
    "DOCUMENTTRACKING.DOCUMENTTRACKINGSOURCEID = 1 ";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFACTION="DOCUMENTTRACKING.DOCUMENTSTATUSID";
	public static final String COLUMN_DFACTION="DOCUMENTSTATUSID";
	public static final String QUALIFIED_COLUMN_DFDEALID="DOCUMENTTRACKING.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOCUMENTTRACKING.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";
	public static final String COLUMN_DFDOCUMENTLABEL="DOCUMENTLABEL";
	public static final String QUALIFIED_COLUMN_DFDOCUMENTTEXT="DOCUMENTTEXT";
	public static final String COLUMN_DFDOCUMENTTEXT="DOCUMENTTEXT";
	public static final String QUALIFIED_COLUMN_DFSTDDOCTRACKID="DOCUMENTTRACKING.DOCUMENTTRACKINGID";
	public static final String COLUMN_DFSTDDOCTRACKID="DOCUMENTTRACKINGID";
	//--Release2.1--//
  //--> Added LanguagePreferenceId DataField
  public static final String QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID="CONDITIONVERBIAGE.LANGUAGEPREFERENCEID";
	public static final String COLUMN_DFLANGUAGEPREFERENCEID="LANGUAGEPREFERENCEID";

  //--TD_CONDR_CR--start//
	public static final String QUALIFIED_COLUMN_DFROLEID="DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLEID";
	public static final String COLUMN_DFROLEID="DOCUMENTRESPONSIBILITYROLEID";
  public static final String QUALIFIED_COLUMN_DFRESPONSIBILITY="DOCUMENTTRACKING.DOCUMENTRESPONSIBILITYROLE_STR";
	public static final String COLUMN_DFRESPONSIBILITY="DOCUMENTRESPONSIBILITYROLE_STR";
	public static final String QUALIFIED_COLUMN_DFDOCTRACKID="DOCUMENTTRACKING.DOCUMENTTRACKINGID";
	public static final String COLUMN_DFDOCTRACKID="DOCUMENTTRACKINGID";
	public static final String QUALIFIED_COLUMN_DFREQUESTDATE="DOCUMENTTRACKING.DOCUMENTREQUESTDATE";
	public static final String COLUMN_DFREQUESTDATE="DOCUMENTREQUESTDATE";
	public static final String QUALIFIED_COLUMN_DFSTATUSCHANGEDATE="DOCUMENTTRACKING.DSTATUSCHANGEDATE";
	public static final String COLUMN_DFSTATUSCHANGEDATE="DSTATUSCHANGEDATE";
  public static final String QUALIFIED_COLUMN_DFSTATUS="DOCUMENTTRACKING.DOCUMENTSTATUS_STR";
	public static final String COLUMN_DFSTATUS="DOCUMENTSTATUS_STR";
  //--TD_CONDR_CR--end//

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFACTION,
				COLUMN_DFACTION,
				QUALIFIED_COLUMN_DFACTION,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTLABEL,
				COLUMN_DFDOCUMENTLABEL,
				QUALIFIED_COLUMN_DFDOCUMENTLABEL,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCUMENTTEXT,
				COLUMN_DFDOCUMENTTEXT,
				QUALIFIED_COLUMN_DFDOCUMENTTEXT,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTDDOCTRACKID,
				COLUMN_DFSTDDOCTRACKID,
				QUALIFIED_COLUMN_DFSTDDOCTRACKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

    //--Release2.1--//
    //--> Added LanguagePreferenceId DataField
    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFLANGUAGEPREFERENCEID,
				COLUMN_DFLANGUAGEPREFERENCEID,
				QUALIFIED_COLUMN_DFLANGUAGEPREFERENCEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
    //--TD_CONDR_CR--start--//
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFROLEID,
				COLUMN_DFROLEID,
				QUALIFIED_COLUMN_DFROLEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFRESPONSIBILITY,
				COLUMN_DFRESPONSIBILITY,
				QUALIFIED_COLUMN_DFRESPONSIBILITY,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOCTRACKID,
				COLUMN_DFDOCTRACKID,
				QUALIFIED_COLUMN_DFDOCTRACKID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREQUESTDATE,
				COLUMN_DFREQUESTDATE,
				QUALIFIED_COLUMN_DFREQUESTDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUSCHANGEDATE,
				COLUMN_DFSTATUSCHANGEDATE,
				QUALIFIED_COLUMN_DFSTATUSCHANGEDATE,
				java.sql.Timestamp.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFSTATUS,
				COLUMN_DFSTATUS,
				QUALIFIED_COLUMN_DFSTATUS,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
     //--TD_CONDR_CR--end--//
	}
}