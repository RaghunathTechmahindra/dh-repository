package mosApp.MosSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;


//--Release2.1--TD_DTS_CR//
//// TD requests to add ConditionReview functionalities to the DocumentTracking screen.
/**
 *
 *
 *
 */
public class pgDocTrackingRepeated4TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDocTrackingRepeated4TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(200);
		setPrimaryModelClass( doDocTrackingStdConditionModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getCbStdAction().setValue(CHILD_CBSTDACTION_RESET_VALUE);
		getBtStdDeleteCondition().setValue(CHILD_BTSTDDELETECONDITION_RESET_VALUE);
		getHdStdDocTrackId().setValue(CHILD_HDSTDDOCTRACKID_RESET_VALUE);
		getStStdCondLabel().setValue(CHILD_STSTDCONDLABEL_RESET_VALUE);
		getBtStdDetail().setValue(CHILD_BTSTDDETAIL_RESET_VALUE);
		getStStdSignByPass().setValue(CHILD_STSTDSIGNBYPASS_RESET_VALUE);
		getHdByPass().setValue(CHILD_HDBYPASS_RESET_VALUE);
    getHdStdCopyId().setValue(CHILD_HDSTDCOPYID_RESET_VALUE);
    getStIncludeStdTileStart().setValue(CHILD_STINCLUDESTDTILESTART_RESET_VALUE);
    getStIncludeStdTileEnd().setValue(CHILD_STINCLUDESTDTILEEND_RESET_VALUE);
		registerChild(CHILD_STINCLUDESTDTILESTART,StaticTextField.class);
		registerChild(CHILD_STINCLUDESTDTILEEND,StaticTextField.class);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_CBSTDACTION,ComboBox.class);
		registerChild(CHILD_BTSTDDELETECONDITION,Button.class);
		registerChild(CHILD_HDSTDDOCTRACKID,HiddenField.class);
		registerChild(CHILD_STSTDCONDLABEL,StaticTextField.class);
		registerChild(CHILD_BTSTDDETAIL,Button.class);
		registerChild(CHILD_STSTDSIGNBYPASS,StaticTextField.class);
		registerChild(CHILD_HDBYPASS,HiddenField.class);
		registerChild(CHILD_HDSTDCOPYID,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoDocTrackingStdConditionModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

    //--Release2.1--//
    ////Populate all ComboBoxes manually here
    cbStdActionOptions.populate(getRequestContext());

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{
    // This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();
    boolean ret = true;

    int rowNum = this.getTileIndex();

		if (movedToRow)
		{
      // Put migrated repeated_onBeforeRowDisplayEvent code here
      DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

      handler.pageGetState(this.getParentViewBean());

      logger = SysLog.getSysLogger("pgDTR4TV");
      logger.debug("pgDTR4TV@nextTile::rowNum: " + rowNum);

      ret = handler.checkDisplayThisRow("Repeated4");
      logger.debug("pgDTR4TV@nextTile::RetValue: " + ret);

      doDocTrackingStdConditionModelImpl theObj =(doDocTrackingStdConditionModelImpl) getdoDocTrackingStdConditionModel();

      logger.debug("pgDTR4TV@nextTile::Size: " + theObj.getSize());
      logger.debug("pgMWQ4TV@nextTile::Location: " + theObj.getLocation());

      if (theObj.getNumRows() > 0 && ret == true)
      ////if (ret == true)
      {
          handler.populateDisplayRow(rowNum,
                                      "Repeated4",
                                      doDocTrackingStdConditionModel.class,
                                      DocTrackingHandler.SECTION_STANDARD_CONDITION);
      }

      handler.pageSaveState();

      return ret;
		}

		return movedToRow;
	}
/////////////////////////////////////////
    /**
		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();
			handler.pageGetState(this.getParentViewBean());
			boolean retval = handler.checkDisplayThisRow("Repeated4");
			handler.pageSaveState();
			return retval;
		}
		return movedToRow;
	}
  **/

	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{
		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{
		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_CBSTDACTION))
		{
			ComboBox child = new ComboBox( this,
				getdoDocTrackingStdConditionModel(),
				CHILD_CBSTDACTION,
				doDocTrackingStdConditionModel.FIELD_DFACTION,
				CHILD_CBSTDACTION_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbStdActionOptions);
			return child;
		}
		else
		if (name.equals(CHILD_BTSTDDELETECONDITION))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSTDDELETECONDITION,
				CHILD_BTSTDDELETECONDITION,
				CHILD_BTSTDDELETECONDITION_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HDSTDDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackingStdConditionModel(),
				CHILD_HDSTDDOCTRACKID,
				doDocTrackingStdConditionModel.FIELD_DFSTDDOCTRACKID,
				CHILD_HDSTDDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSTDCONDLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackingStdConditionModel(),
				CHILD_STSTDCONDLABEL,
				doDocTrackingStdConditionModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STSTDCONDLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSTDDETAIL))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSTDDETAIL,
				CHILD_BTSTDDETAIL,
				CHILD_BTSTDDETAIL_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_STSTDSIGNBYPASS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSTDSIGNBYPASS,
				CHILD_STSTDSIGNBYPASS,
				CHILD_STSTDSIGNBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDBYPASS))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackingStdConditionModel(),
				CHILD_HDBYPASS,
				doDocTrackingStdConditionModel.FIELD_DFBYPASS,
				CHILD_HDBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSTDCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackingStdConditionModel(),
				CHILD_HDSTDCOPYID,
				doDocTrackingStdConditionModel.FIELD_DFCOPYID,
				CHILD_HDSTDCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDESTDTILESTART))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINCLUDESTDTILESTART,
				CHILD_STINCLUDESTDTILESTART,
				CHILD_STINCLUDESTDTILESTART_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STINCLUDESTDTILEEND))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINCLUDESTDTILEEND,
				CHILD_STINCLUDESTDTILEEND,
				CHILD_STINCLUDESTDTILEEND_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStStdSignByPass()
	{
		return (StaticTextField)getChild(CHILD_STSTDSIGNBYPASS);
	}

	/**
	 *
	 *
	 */
	public ComboBox getCbStdAction()
	{
		return (ComboBox)getChild(CHILD_CBSTDACTION);
	}

	/**
	 *
	 *
	 */
	static class CbStdActionOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbStdActionOptionList()
		{

		}


		/**
		 *
		 *
		 */
		//--Release2.1--//
    //Convert to populate the Options from ResourceBundle
		public void populate(RequestContext rc)
		{
      try
      {
        //Get Language from SessionState
        //Get the Language ID from the SessionStateModel
        String defaultInstanceStateName =
                    rc.getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState =
                                              (SessionStateModelImpl)rc.getModelManager().getModel(
                                              SessionStateModel.class,
                                              defaultInstanceStateName,
                                              true);
        int languageId = theSessionState.getLanguageId();

        //Get IDs and Labels from BXResources
        Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),
                                                            "DOCUMENTSTATUS", languageId);
        Iterator l = c.iterator();
        String[] theVal = new String[2];
        while(l.hasNext())
        {
          theVal = (String[])(l.next());
          add(theVal[1], theVal[0]);
        }
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
    }
	}



	/**
	 *
	 *
	 */
	public void handleBtStdDeleteConditionRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btStdDeleteCondition_onWebEvent method
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.handleDelete(handler.getRowNdxFromWebEventMethod(event), "btStdDeleteCondition");
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtStdDeleteCondition()
	{
		return (Button)getChild(CHILD_BTSTDDELETECONDITION);
	}


	/**
	 *
	 *
	 */
	public String endBtStdDeleteConditionDisplay(ChildContentDisplayEvent event)
	{
		// The following code block was migrated from the btStdDeleteCondition_onBeforeHtmlOutputEvent method
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		boolean rc = handler.displayEditButton();
		handler.pageSaveState();

		if(rc == true)
		  return event.getContent();
    else
      return "";
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdStdDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDSTDDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStStdCondLabel()
	{
		return (StaticTextField)getChild(CHILD_STSTDCONDLABEL);
	}


	/**
	 *
	 *
	 */
	public void handleBtStdDetailRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		// The following code block was migrated from the btStdDetail_onWebEvent method
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();
		handler.preHandlerProtocol(this.getParentViewBean());
		handler.navigateToStdCondDetail(4, handler.getRowNdxFromWebEventMethod(event));
		handler.postHandlerProtocol();
	}


	/**
	 *
	 *
	 */
	public Button getBtStdDetail()
	{
		return (Button)getChild(CHILD_BTSTDDETAIL);
	}

	/**
	 *
	 *    One one more hidden field added to escape the over-massaging of
   *    an appropriate model in the Handler.
	 */
	public HiddenField getHdByPass()
	{
		return (HiddenField)getChild(CHILD_HDBYPASS);
	}

	/**
	 *
	 *
	 */
	public HiddenField getHdStdCopyId()
	{
		return (HiddenField)getChild(CHILD_HDSTDCOPYID);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeStdTileStart()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDESTDTILESTART);
	}

	/**
	 *
	 *
	 */
	public StaticTextField getStIncludeStdTileEnd()
	{
		return (StaticTextField)getChild(CHILD_STINCLUDESTDTILEEND);
	}

	/**
	 *
	 *
	 */
	public doDocTrackingStdConditionModel getdoDocTrackingStdConditionModel()
	{
		if (doDocTrackingStdCondition == null)
			doDocTrackingStdCondition = (doDocTrackingStdConditionModel) getModel(doDocTrackingStdConditionModel.class);
		return doDocTrackingStdCondition;
	}


	/**
	 *
	 *
	 */
	public void setdoDocTrackingStdConditionModel(doDocTrackingStdConditionModel model)
	{
			doDocTrackingStdCondition = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_CBSTDACTION="cbStdAction";
	public static final String CHILD_CBSTDACTION_RESET_VALUE="";
  //--Release2.1--//
	//private static CbStdActionOptionList cbStdActionOptions=new CbStdActionOptionList();
  private CbStdActionOptionList cbStdActionOptions=new CbStdActionOptionList();
	public static final String CHILD_BTSTDDELETECONDITION="btStdDeleteCondition";
	public static final String CHILD_BTSTDDELETECONDITION_RESET_VALUE="Delete Condition";
	public static final String CHILD_HDSTDDOCTRACKID="hdStdDocTrackId";
	public static final String CHILD_HDSTDDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_STSTDCONDLABEL="stStdCondLabel";
	public static final String CHILD_STSTDCONDLABEL_RESET_VALUE="";
	public static final String CHILD_BTSTDDETAIL="btStdDetail";
	public static final String CHILD_BTSTDDETAIL_RESET_VALUE=" ";
	public static final String CHILD_STSTDSIGNBYPASS="stStdSignByPass";
	public static final String CHILD_STSTDSIGNBYPASS_RESET_VALUE="";
	public static final String CHILD_HDBYPASS="hdByPass";
	public static final String CHILD_HDBYPASS_RESET_VALUE="";
	public static final String CHILD_HDSTDCOPYID="hdStdCopyId";
	public static final String CHILD_HDSTDCOPYID_RESET_VALUE="";

  public static final String CHILD_STINCLUDESTDTILESTART="stIncludeStdTileStart";
	public static final String CHILD_STINCLUDESTDTILESTART_RESET_VALUE="";
	public static final String CHILD_STINCLUDESTDTILEEND="stIncludeStdTileEnd";
	public static final String CHILD_STINCLUDESTDTILEEND_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDocTrackingStdConditionModel doDocTrackingStdCondition=null;
  private DocTrackingHandler handler=new DocTrackingHandler();
  public SysLogger logger;
}

