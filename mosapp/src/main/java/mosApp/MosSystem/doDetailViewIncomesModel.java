package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDetailViewIncomesModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfBorrowerId();

	
	/**
	 * 
	 * 
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncomeType();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeType(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncomeDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfIncomePeriod();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomePeriod(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeAmt();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeAmt(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInGDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInGDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfPercentIncludedInTDS();

	
	/**
	 * 
	 * 
	 */
	public void setDfPercentIncludedInTDS(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfEmploymentRelated();

	
	/**
	 * 
	 * 
	 */
	public void setDfEmploymentRelated(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfIncomeId();

	
	/**
	 * 
	 * 
	 */
	public void setDfIncomeId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);
	
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFINCOMETYPE="dfIncomeType";
	public static final String FIELD_DFINCOMEDESC="dfIncomeDesc";
	public static final String FIELD_DFINCOMEPERIOD="dfIncomePeriod";
	public static final String FIELD_DFINCOMEAMT="dfIncomeAmt";
	public static final String FIELD_DFPERCENTINCLUDEDINGDS="dfPercentIncludedInGDS";
	public static final String FIELD_DFPERCENTINCLUDEDINTDS="dfPercentIncludedInTDS";
	public static final String FIELD_DFEMPLOYMENTRELATED="dfEmploymentRelated";
	public static final String FIELD_DFINCOMEID="dfIncomeId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

