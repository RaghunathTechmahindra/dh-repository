package mosApp.MosSystem;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.duplicate.DupeCheck;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.pk.AddrPK;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PartyProfilePK;
import com.basis100.deal.security.ActiveMessage;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.util.Parameters;
import com.basis100.deal.util.StringUtil;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.ChildContentDisplayEvent;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;

/**
 *
 *
 */
public class PartySearchHandler extends PageHandlerCommon
	implements Sc
{
	/**
	 *
	 *
	 */
	public PartySearchHandler cloneSS()
	{
		return(PartySearchHandler) super.cloneSafeShallow();

	}
	// All party records have a fixed copy id,namely ...

	public static final int PARTY_COPYID=1;
	//
	// PARTY SEARCH
	//

	boolean isPartySearch=false;
	public static final String PARTY_SEARCH_BUFFER="PARTY_SEARCH_BUFFER";
	public static final String SELECTED_ROW_NDX="SELECTED_ROW_NDX";
	public static final String EXISTING_ASSOC_PARTYPROFILE_ID="EXISTING_ASSOC_PARTYPROFILE_ID";
	public static final String NEW_ASSOC_PARTYPROFILE_ID="NEW_ASSOC_PARTYPROFILE_ID";
	public static final String PARTY_TYPE_FILTER_PASSED_PARM="partyTypeId";
	public static final String PROPERTY_ID_FILTER_PASSED_PARM="propertyId";
	public static final String PARTY_ADD_ACTION="PARTY_ADD_ACTION";
	private static final String PARTY_REASSOCIATE_MSG="REASSOCIATE";
	//
	// PARTY ADD/MODIFY
	//

	boolean isPartyAdd=false;
	public static final String PARTY_ID_PASSED_PARM="partyIdPassed";
	public static final String PARTY_TYPE_PASSED_PARM="partyTypePassed";
	// key for edit buffer (in page state table)

	public static final String ADD_EDIT_BUFFER="ADD_EDIT_BUFFER";
	//
	// PARTY REVIEW
	//

	boolean isPartyReview=false;
	// following used as key in page state table to indicate if user has edit permission
	// for party information (this is distinct from add party which is always allowed.

	public static final String USER_CAN_EDIT_PARTY="USER_CAN_EDIT_PARTY";

	//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
	//Protected strings for search SQL statements
	protected static final String SQL_CLIENT_NUM_LIKE = " AND UPPER(partyprofile.ptpbusinessid) LIKE '";
	protected static final String SQL_LOCATION_LIKE = " AND UPPER(partyprofile.location) LIKE '%";
	//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//

	protected void setPageFlag(PageEntry pg)
	{
		isPartySearch = false;
		isPartyAdd = false;

		if (pg.getPageId() == Mc.PGNM_PARTY_SEARCH)
    {
     isPartySearch = true;
    }
		else if (pg.getPageId() == Mc.PGNM_PARTY_ADD)
    {
     isPartyAdd = true;
    }
		else isPartyReview = true;

	}

	/**
	 *
	 *
	 */
	public void preHandlerProtocol(ViewBean currNDPage)
	{
		super.preHandlerProtocol(currNDPage);

		// sefault page load method to display()
		PageEntry pg = theSessionState.getCurrentPage();

		pg.setPageDisplayMethod(Mc.DISPLAY_METHOD_DISP);

 		////SYNCADD
 		// Allow search even if not set to not Editable (Force to call savedata)
 		//  -- By Billy 10June2002
 		setPageFlag(pg);

 		if (pg.isEditable() == false && this.isPartySearch == true)
        this.saveData(pg, false);

	}


	// This method is used to populate the fields in the header
	// with the appropriate information

	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		setPageFlag(pg);

		//pg.show("*************************************************", logger);
		if (isPartySearch) populatePageDisplayFieldsPartySearch();
		else if (isPartyAdd) populatePageDisplayFieldsPartyAdd();
		else populatePageDisplayFieldsPartyReview();
	}


	/**
	 *
	 *
	 */
	public void populatePageDisplayFieldsPartySearch()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();


		//logger.debug("************************* populatePageDisplayFieldsPartySearch()");
		populatePageShellDisplayFields();

		populatePreviousPagesLinks();

		revisePrevNextButtonGeneration(pg, tds);

		populatePageDealSummarySnapShot();


		// populate current search criteria
		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(PARTY_SEARCH_BUFFER);

		efb.populateFields(getCurrNDPage());


		// display Deal Summary Snapshot but only for deal specific page
		displayDSSConditional(pg, getCurrNDPage());


		// Set rows displayed string -- by BILLY 26Jan2001
		populateRowsDisplayed(getCurrNDPage(), tds);

    //// JavaScript client side data validation API implementation.
    try
    {

      String partySearchValidPath = Sc.DVALID_PARTY_SEARCH_PROPS_FILE_NAME;

      //logger.debug("SFEH@partySearchValidPath: " + partySearchValidPath);

      JSValidationRules partySearchValidObj = new JSValidationRules(partySearchValidPath, logger, getCurrNDPage());
      partySearchValidObj.attachJSValidationRules();
    }
    catch( Exception e )
    {
        logger.error("Exception PSH@PopulatePageDisplayFieldsPartySearch: Problem encountered in JS data validation API." + e.getMessage());
    }

	}


	/**
	 *
	 *
	 */
	private void populateRowsDisplayed(ViewBean NDPage, PageCursorInfo tds)
	{
		String rowsRpt = "";

    if (tds.getTotalRows() > 0)
    {
      rowsRpt = BXResources.getSysMsg("DP_PARTYSEARCHROWSDISPLAYED", theSessionState.getLanguageId());

      try
      {
        rowsRpt = com.basis100.deal.util.Format.printf(rowsRpt,
                new Parameters(tds.getFirstDisplayRowNumber())
                .add(tds.getLastDisplayRowNumber())
                .add(tds.getTotalRows()));
      }
      catch (Exception e)
      {
        logger.warning("String Format error @ DealSearchHandler.populateRowsDisplayed: " + e);
      }
    }

		NDPage.setDisplayFieldValue("rowsDisplayed", new String(rowsRpt));

	}


	/**
	 * populatePageDisplayFieldsPartyAdd
	 *
	 * @param none
	 * @return none
	 * @version ?? <br>
	 * 	Date: 05/31/2006<br>
	 * 	Author: NBC/PP Implementation Team <br>
	 *  Change: <br>
	 *  	Add check for service branch party type and customize content
	 *
	 */
	public void populatePageDisplayFieldsPartyAdd()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		//logger.debug("************************* populatePageDisplayFieldsPartyAdd()");
		populatePageShellDisplayFields();

		populatePreviousPagesLinks();

		populatePageDealSummarySnapShot();

		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(ADD_EDIT_BUFFER);

    efb.populateFields(getCurrNDPage());

		displayDSSConditional(pg, getCurrNDPage());

		//--Release2.1--start//
    //// Since this static html label should be translated on fly toggling the
    //// language and pst content is not refreshed on toggle action the label
    //// except text should be set to pst.
    //// Lookup to translate the label.
    String partyAddAction = BXResources.getGenericMsg(new String((String) pst.get(PARTY_ADD_ACTION)),
                                                 theSessionState.getLanguageId());

    getCurrNDPage().setDisplayFieldValue("stAction", partyAddAction);
		//--Release2.1--end//

    //// JavaScript client side data validation API implementation.
    try
    {

      String  partyAddValidPath = Sc.DVALID_PARTY_ADD_PROPS_FILE_NAME;
      //logger.debug("SFEH@partyAddValidPath: " + partyAddValidPath);

      JSValidationRules partyAddValidObj = new JSValidationRules(partyAddValidPath, logger, getCurrNDPage());
      partyAddValidObj.attachJSValidationRules();
    }
    catch( Exception e )
    {
        logger.error("Exception PSH@PopulatePageDisplayFieldsPartyAdd: Problem encountered in JS data validation API." + e.getMessage());
    }

    //--DJ_PT_CR--start//
    ViewBean thePage = getCurrNDPage();
    String partyProfileId =(String) pst.get(PARTY_ID_PASSED_PARM);

    int langId = theSessionState.getLanguageId();
    int partyTypeIndx = getPartyTypeId();

    // Do not edit if Party_Edit mode and Party Type is 'Origination Branch'.
    if (partyProfileId != null && partyProfileId.length() > 0)
    {
      logger.debug("PSH@populatePageDisplayFieldsPartyAdd: is party modify, party profile id = " + partyProfileId);
      logger.debug("PSH@populatePageDisplayFieldsPartyAdd:Type: " + pst.get(ADD_EDIT_BUFFER).equals("PARTY_EDIT_LABEL"));

      //// New request from Product to reduce the content of the Party Type always
      //// (if created). It means that party type can't be changed.
      reduceAccessToComboBox(thePage, "cbAddPartyType", partyTypeIndx, langId, "PARTYTYPE");

      //// Close access to the fields if PartyType is 'Origination Branch' and
      //// the mode is edit. 'Origination Branch' partyType can't be added from
      //// the screen. pst.get(ADD_EDIT_BUFFER).equals("PARTY_EDIT_LABEL") and
      //// Customize the Labels with regards to Party Types.

      if(partyTypeIndx == Mc.PARTY_TYPE_ORIGINATION_BRANCH)
      {
        //// 1. Close access to the certain fields
        //--> Bug fix to preserve the original values on page reload (e.g. toggle language)

        customizeTextBox("txBranchTransitNumCMHC");
        efb.setFieldValue("txBranchTransitNumCMHC", efb.getOriginalValue("txBranchTransitNumCMHC"));
        getCurrNDPage().setDisplayFieldValue("txBranchTransitNumCMHC", efb.getFieldValue("txBranchTransitNumCMHC"));
        customizeTextBox("txBranchTransitNumGE");
        efb.setFieldValue("txBranchTransitNumGE", efb.getOriginalValue("txBranchTransitNumGE"));
        getCurrNDPage().setDisplayFieldValue("txBranchTransitNumGE", efb.getFieldValue("txBranchTransitNumGE"));
        customizeTextBox("tbAddClientNumber");
        efb.setFieldValue("tbAddClientNumber", efb.getOriginalValue("tbAddClientNumber"));
        getCurrNDPage().setDisplayFieldValue("tbAddClientNumber", efb.getFieldValue("tbAddClientNumber"));
        customizeTextBox("tbAddPartyShortName");
        efb.setFieldValue("tbAddPartyShortName", efb.getOriginalValue("tbAddPartyShortName"));
        getCurrNDPage().setDisplayFieldValue("tbAddPartyShortName", efb.getFieldValue("tbAddPartyShortName"));
        //=================================================================================
      }
      //****** Change by NBC/PP Implementation Team - Version 1.1 - START *****//
      else
      if (isServiceBranch(partyTypeIndx))
      {
    	  // 1. Customize the textboxes for Service Branch - make them readonly
          customizeTextBox("tbAddClientNumber");
          efb.setFieldValue("tbAddClientNumber", efb.getOriginalValue("tbAddClientNumber"));
          getCurrNDPage().setDisplayFieldValue("tbAddClientNumber", efb.getFieldValue("tbAddClientNumber"));
          customizeTextBox("tbAddPartyShortName");
          efb.setFieldValue("tbAddPartyShortName", efb.getOriginalValue("tbAddPartyShortName"));
          getCurrNDPage().setDisplayFieldValue("tbAddPartyShortName", efb.getFieldValue("tbAddPartyShortName"));    	  
      }
      //****** Change by NBC/PP Implementation Team - Version 1.1 - END *****//      

      pst.put("PARTYTYPEID", new Integer(partyTypeIndx));
    }

    //// Customize the Labels with regards to Party Types.
    String labelPartyShortName = "";
    String clientNum = "";
    if ( partyTypeIndx == Mc.PARTY_TYPE_ORIGINATION_BRANCH)
    {
      //// 1. Customize the PartyShortName and Client# Labels with regards to PartyType.
      labelPartyShortName = BXResources.getGenericMsg("BRANCH_SHORT_NAME_LABEL",  theSessionState.getLanguageId());
      clientNum = BXResources.getGenericMsg("BRANCH_TRANSIT_NUMBER_LABEL",  theSessionState.getLanguageId());
    }
    //****** Change by NBC/PP Implementation Team - Version 1.1 - START *****//
    else
    if (isServiceBranch(partyTypeIndx))
    {
        //// 1. Customize the PartyShortName and Client# Labels with regards to PartyType.
        labelPartyShortName = BXResources.getGenericMsg("BRANCH_SHORT_NAME_LABEL",  theSessionState.getLanguageId());
        clientNum = BXResources.getGenericMsg("BRANCH_TRANSIT_NUMBER_LABEL",  theSessionState.getLanguageId());
        
        //// 2. Hide the display of BranchTransitNumCMHC and BranchTransitNumGE info.
        startSupressContent("stIncludeBranchTransitInfoStart", thePage);
        endSupressContent("stIncludeBranchTransitInfoEnd", thePage);    	
    }
    //****** Change by NBC/PP Implementation Team - Version 1.1 - END *****//      
    else if ( partyTypeIndx != Mc.PARTY_TYPE_ORIGINATION_BRANCH)
    {
      //// 1. Customize the PartyShortName and Client# Labels with regards to PartyType.
      labelPartyShortName = BXResources.getGenericMsg("PARTY_SHORT_NAME_LABEL",  theSessionState.getLanguageId());
      clientNum = BXResources.getGenericMsg("CLIENT_NUMBER_LABEL",  theSessionState.getLanguageId());

      //// 2. Hide the display of BranchTransitNumCMHC and BranchTransitNumGE info.
      startSupressContent("stIncludeBranchTransitInfoStart", thePage);
      endSupressContent("stIncludeBranchTransitInfoEnd", thePage);
    }

    //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    //--> By Billy 25May2004
    // Check to hide the BankInfo fields
    if (!isIncludeBankInfo())
    {
      startSupressContent("stIncludeBankInfoStart", thePage);
      endSupressContent("stIncludeBankInfoEnd", thePage);
    }
    //====================================================================

    getCurrNDPage().setDisplayFieldValue("stPartyShortNameLabel", labelPartyShortName);
    getCurrNDPage().setDisplayFieldValue("stClientNumberLabel", clientNum);

		populateDefaultValues(getCurrNDPage());
    //--DJ_PT_CR--end//
	}

  //--DJ_PT_CR--start//
	private void populateDefaultValues(ViewBean thePage)
	{

    PageEntry pg = theSessionState.getCurrentPage();
    String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

    //--> Bug fix
    //--> By Billy 26May2004
		df = (HtmlDisplayFieldBase) thePage.getDisplayField("cbAddPartyType");
    theExtraHtml = "onChange=\"populateValueTo(['hdPartyTypeId']);\"";
    //              + " SetTransitNumbersDisplay();\"";

    df.setExtraHtml(theExtraHtml);
	}

  public boolean isDJClient()
  {
    boolean isDisplayBranchOriginationLabel = false; // default.
    // regular execution path
    if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.calc.isdjclient", "N").equals("N"))
    {
       isDisplayBranchOriginationLabel = false;
    }
    // DJ execution path
    else if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.calc.isdjclient", "N").equals("Y"))
    {
       isDisplayBranchOriginationLabel = true;
    }
    logger.debug("DHC@isDisplayBranchOriginationLabel::isDisplayBranchOriginationLabel? " + isDisplayBranchOriginationLabel);

    return isDisplayBranchOriginationLabel;
  }
  //--DJ_PT_CR--end//

 /**
   * isIncludeServiceBranch
   * Returns the property from the configuration file
   * 
   * @param none
   * 
   * @returns boolean : the indicator from the configuration file
   * 	true - show service branch information
   * 	false - hide service branch information
   * @author NBC/PP Implementation Team
   * @version 1.0 (Initial Version - 29 May 2006)
   * 
   */
  public boolean isIncludeServiceBranch()
  {
	boolean isDisplayServiceBranch = false; // default.
	
	// read from properties and compare
	String includeServiceBranchProperty = PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.party.includeServiceBranch", "N");
	isDisplayServiceBranch = includeServiceBranchProperty.equals("Y");

    return isDisplayServiceBranch;
 
  }
  
	/**
	 *
	 *
	 */

  private int getPartyTypeId()
	{
    //--> bug fix : if PartyAdd we should default to 0
    //--> By Billy 05March2004
    Hashtable pst = theSessionState.getCurrentPage().getPageStateTable();
    String partyProfileId =(String) (pst.get(PARTY_ID_PASSED_PARM));
    if (partyProfileId == null || partyProfileId.length() <= 0)
    {
      logger.debug("PSH@getPartyTypeId PartyId = 0 fallback to 0 !");
      return 0;
    }
    //================================================

    //--> Bug fix : should read the Party Type from PST instead
    //--> By Billy 26May2004
    int indx = 0;

    doPartySearchModel theDO = (doPartySearchModel)
                       RequestManager.getRequestContext().getModelManager().getModel(doPartySearchModel.class);

    Object partyType = theDO.getValue(theDO.FIELD_DFPARTYTYPE);
    if(partyType != null)
    {
      indx = new Integer(partyType.toString()).intValue();
      logger.debug("PSH@getPartyTypeId from DO :: " + new Integer(partyType.toString()).intValue());
    }
    else if(isPartyAdd)
    {
      String s = pst.get(PARTY_TYPE_PASSED_PARM).toString();
      if(!s.equals(""))
      {
        indx = new Integer(s).intValue();
        logger.debug("PSH@getPartyTypeId from pst :: " + indx);
      }
    }

    return indx;
  }

	/**
	 * populatePageDisplayFieldsPartyReview
	 *
	 * @param none
	 * @return none
	 * @version ??<br>
	 * Date: 05/31/2006<br>
	 * Author: NBC/PP Implementation Team<br>
	 * Change: <br>
	 * 	Added check for service branch party type to hide content
	 *
	 */
	public void populatePageDisplayFieldsPartyReview()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();


		//logger.debug("************************* populatePageDisplayFieldsPartyReview()");
		// mamually populate display fields -- by BILLY 12Feb2001
		////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doPartySearchModel.class);

    doPartySearchModel theDO = (doPartySearchModel)
                                RequestManager.getRequestContext().getModelManager().getModel(doPartySearchModel.class);


		// Set PhoneNum with extension
		setPhoneNumTextDisplayField(theDO, "stWorkPhoneNumber", theDO.FIELD_DFPHONE, theDO.FIELD_DFPHONENUMBEREXT, 0);

		// Set PhoneNum with extension
		setPhoneNumTextDisplayField(theDO, "stFaxNumber", theDO.FIELD_DFFAX, "", 0);

		populatePageShellDisplayFields();

		populatePreviousPagesLinks();

		populatePageDealSummarySnapShot();

		displayDSSConditional(pg, getCurrNDPage());

    //--DJ_PT_CR--start//
		ViewBean thePage = getCurrNDPage();
    String partyProfileId =(String) pst.get(PARTY_ID_PASSED_PARM);

    int langId = theSessionState.getLanguageId();
    int partyTypeIndx = getPartyTypeId();

    //// Customize the Labels with regards to Party Types.
    String labelPartyShortName = "";
    String clientNum = "";
    
    if ( partyTypeIndx == Mc.PARTY_TYPE_ORIGINATION_BRANCH)
    {
      //// 1. Customize the PartyShortName and Client# Labels with regards to PartyType.
      labelPartyShortName = BXResources.getGenericMsg("BRANCH_SHORT_NAME_LABEL",  theSessionState.getLanguageId());
      clientNum = BXResources.getGenericMsg("BRANCH_TRANSIT_NUMBER_LABEL",  theSessionState.getLanguageId());
    }
    //***** Change by NBC/PP Implementation Team - Version 1.1 - START *****// 
    else
    if (isServiceBranch(partyTypeIndx))
    {
        //// 1. Customize the PartyShortName and Client# Labels with regards to PartyType.
        labelPartyShortName = BXResources.getGenericMsg("BRANCH_SHORT_NAME_LABEL",  theSessionState.getLanguageId());
        clientNum = BXResources.getGenericMsg("BRANCH_TRANSIT_NUMBER_LABEL",  theSessionState.getLanguageId());

        //// 2. Hide the display of BranchTransitNumCMHC and BranchTransitNumGE info.
        startSupressContent("stIncludeBranchTransitInfoStart", thePage);
        endSupressContent("stIncludeBranchTransitInfoEnd", thePage);        
    }
    //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//
    else if ( partyTypeIndx != Mc.PARTY_TYPE_ORIGINATION_BRANCH)
    {
      //// 1. Customize the PartyShortName and Client# Labels with regards to PartyType.
      labelPartyShortName = BXResources.getGenericMsg("PARTY_SHORT_NAME_LABEL",  theSessionState.getLanguageId());
      clientNum = BXResources.getGenericMsg("CLIENT_NUMBER_LABEL",  theSessionState.getLanguageId());

      //// 2. Hide the display of BranchTransitNumCMHC and BranchTransitNumGE info.
      startSupressContent("stIncludeBranchTransitInfoStart", thePage);
      endSupressContent("stIncludeBranchTransitInfoEnd", thePage);
    }

    //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
    //--> By Billy 25May2004
    // Check to hide the BankInfo fields
    if (!isIncludeBankInfo())
    {
      startSupressContent("stIncludeBankInfoStart", thePage);
      endSupressContent("stIncludeBankInfoEnd", thePage);
    }
    //====================================================================

    getCurrNDPage().setDisplayFieldValue("stPartyShortNameLabel", labelPartyShortName);
    getCurrNDPage().setDisplayFieldValue("stClientNumberLabel", clientNum);
    //--DJ_PT_CR--end//


	}

	/**
	 *
	 *
	 */
	public void saveData(PageEntry currPage, boolean calledInTransaction)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		setPageFlag(pg);

		if (isPartySearch)
		{
			Hashtable pst = pg.getPageStateTable();
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(PARTY_SEARCH_BUFFER);
			efb.readFields(getCurrNDPage());

			// we never mark search page as modified - allow leave without fuss
			pg.setModified(false);
		}
		else if (isPartyAdd)
		{
			Hashtable pst = pg.getPageStateTable();
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(ADD_EDIT_BUFFER);
			efb.readFields(getCurrNDPage());
			if (efb.isModified()) pg.setModified(true);
		}

	}


	/**
	 *
	 *
	 */
	private void displayDSSConditional(PageEntry pg, ViewBean thePage)
	{
		String suppressStart = "<!--";

		String suppressEnd = "//-->";

		if (pg == null || pg.getPageDealId() <= 0 || pg.getPageId() == Mc.PGNM_PARTY_ADD)
		{
			// suppress
			//logger.debug("BILLY -- Set to comment it out : pg.getPageDealId() = " + pg.getPageDealId() + " : pg.getPageId() = " + pg.getPageId());
			thePage.setDisplayFieldValue("stIncludeDSSstart", new String(suppressStart));
			thePage.setDisplayFieldValue("stIncludeDSSend", new String(suppressEnd));
			return;
		}


		//logger.debug("BILLY -- Set to display it : pg.getPageDealId() = " + pg.getPageDealId() + " : pg.getPageId() = " + pg.getPageId());
		// display
		thePage.setDisplayFieldValue("stIncludeDSSstart", new String(""));

		thePage.setDisplayFieldValue("stIncludeDSSend", new String(""));

		return;

	}


	/*
	 * Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	 */
	public void setupBeforePageGeneration()
	{
		logger.trace("--T--> @PartySearchHandler.setupBeforePageGeneration:");

		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);


		// -- //
		setPageFlag(pg);

		if (isPartySearch) setupBeforePageGenerationPartySearch();
		else if (isPartyAdd) setupBeforePageGenerationPartyAdd();
		else setupBeforePageGenerationPartyReview();

		setupDealSummarySnapShotDO(pg);

	}


	/**
	 *
	 *
	 */
	public void setupBeforePageGenerationPartySearch()
	{
		logger.trace("--T--> @PartySearchHandler.setupBeforePageGeneration(PartySearch):");

		PageEntry pg = theSessionState.getCurrentPage();

		boolean firstEntry = false;

		try
		{
			PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
			Hashtable pst = pg.getPageStateTable();

 			////SYNCADD.
 			// Add to check if Party search is editable -- By Billy 10June2002
 			// determine if user can modify existing party information
 			logger.debug("BILLY ===> @setupBeforePageGenerationPartySearch getUserAccessType(Mc.PGNM_PARTY_ADD) = " + getUserAccessType(Mc.PGNM_PARTY_ADD) + "  getUserAccessType(Mc.PGNM_PARTY_SEARCH) = " + getUserAccessType(Mc.PGNM_PARTY_SEARCH));
 			if (getUserAccessType(Mc.PGNM_PARTY_ADD) == Sc.PAGE_ACCESS_EDIT &&
          getUserAccessType(Mc.PGNM_PARTY_SEARCH) == Sc.PAGE_ACCESS_EDIT) {

          pst.put(USER_CAN_EDIT_PARTY, USER_CAN_EDIT_PARTY);
      }

 			////================================================================

			if (tds == null)
			{
				// first invocation!
				firstEntry = true;

				// used to avoid a search - never on entry to screen
				// determine number of search results (shown) per display page
				String voName = "Repeated1";
        logger.debug("PSH@setupBeforePageGenPartySearch::VoName: " + voName);

				////int perPage =(getCurrNDPage().getCommonRepeated(voName)).getMaxDisplayRows();
        ////pgPartySearchRepeated1TiledView vo = ((pgPartySearchViewBean)getCurrNDPage()).getRepeated1();

        int perPage =((TiledView)(getCurrNDPage().getChild(voName))).getMaxDisplayTiles();
        logger.debug("PSH@setupBeforePageGenPartySearch::MaxDisplayRows_2: " + perPage);

				// set up and save task display state object
				tds = new PageCursorInfo("DEFAULT", voName, perPage, - 1);
				tds.setRefresh(true);
				tds.setCriteria("");
				pst.put(tds.getName(), tds);

				// initialize saved search criteria
				EditFieldsBuffer efb = new PartySearchBuffer("SearchParms", logger);
				pst.put(PARTY_SEARCH_BUFFER, efb);

				// If usage is as a sub-page check for parameters (filter)
				String ptStrRep = getStringIntValue((String) pst.get(PARTY_TYPE_FILTER_PASSED_PARM));
				if (ptStrRep.length() > 0)
				{
          //--> Bug fixed by BILLY 18Sept2002
          //--> Value not contains .0 for Jato
					//String value = "" + getIntValue(ptStrRep) + ".0";
          String value = "" + getIntValue(ptStrRep);
          //=================================================
					pst.put(PARTY_TYPE_FILTER_PASSED_PARM, value);
					efb.setFieldValue("cbPartyType", value);
					efb.setOriginalAsCurrent();
				}
			}
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(PARTY_SEARCH_BUFFER);

			// set criteria for search results data object
      doPartySearchModel theDO = (doPartySearchModel)
                                RequestManager.getRequestContext().getModelManager().getModel(doPartySearchModel.class);

			tds.setCriteria(efb.getAsCriteria());

			// Add checking if the submit button pressed -- by BILLY 02Feb2001
			if (firstEntry || pg.getPrevPageName() == null)
            setResultsCriteria(theDO, efb, true);
			else
            setResultsCriteria(theDO, efb, false);

      int numRows = 0;

      try
      {
        //logger.debug("MWQH@SQL before execute: " + ((doPartySearchModelImpl)theDO).getSelectSQL());

        ResultSet rs = theDO.executeSelect(null);

        if(rs == null || theDO.getSize() < 0)
        {
          numRows = 0;
        }

        else if(rs != null && theDO.getSize() > 0)
        {
          //logger.debug("PSH@setupBeforePageGeneration(PartySearch)::Size of the result set: " + theDO.getSize());
          numRows = theDO.getSize();
        }
      }
      catch (ModelControlException mce)
      {
        logger.debug("PSH@setupBeforePageGeneration(PartySearch)::MCEException: " + mce);
      }

      catch (SQLException sqle)
      {
        logger.debug("PSH@setupBeforePageGeneration(PartySearch)::SQLException: " + sqle);
      }

			tds.setTotalRows(numRows);

			// store party profile status codes for conditional generation of select buttons
			if (pg.getPageDealId() > 0)
			{
				////spider.database.CSpDBResultTable resultTab = theDO.getLastResults().getResultTable();

				Vector v = new Vector();
        //--> Bug Fixed by Billy 25Aug2003
        if(numRows > 0)
        {
          theDO.beforeFirst();
          while (theDO.next())
          {
            ////v.add((resultTab.getValue(i, 9)).toString());
             v.add((theDO.getValue(theDO.FIELD_DFPARTYSTATUS)).toString());
          }
        }

				pst.put("PARTYSTATUSCODES", v);
				pst.put("ROWNDX", new Integer(0));
			}

			// Check for Alert Messages display, only if the PreviusPageName == thisPageName i.e. "Submit" button pressed
			//    -- By BILLY 29Jan2001
			if (pg.getPrevPageName() != null)
			{
				if (pg.getPrevPageName().equals(getCurrNDPage().getName()))
				{
					if (firstEntry == false && tds.getTotalRows() <= 0 && isSearchCriteria(efb) == true)
            setActiveMessageToAlert(BXResources.getSysMsg("PARTY_SEARCH_NO_MATCH", theSessionState.getLanguageId()),
                ActiveMsgFactory.ISCUSTOMCONFIRM);

					// Display Alert message if not Search Criteria input -- by BILLY 26Jan2001
					if (firstEntry == false && isSearchCriteria(efb) == false)
            setActiveMessageToAlert(BXResources.getSysMsg("PARTY_SEARCH_NO_INPUT", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
					pg.setPrevPageName("");
				}
			}

			// determine if we will force display of first page
			if (tds.isTotalRowsChanged() || tds.isCriteriaChanged())
			{
				// number of rows has changed - unconditional display of first page
				////CSpDataDrivenVisual vo =(CSpDataDrivenVisual)(getCurrNDPage().getCommonRepeated(tds.getVoName()));

        pgPartySearchRepeated1TiledView vo = ((pgPartySearchViewBean)getCurrNDPage()).getRepeated1();
        //logger.debug("IWQH@setupBeforePageGenPartySearch::Repeated1: " + vo);
        //logger.debug("IWQH@setupBeforePageGenPartySearch::Repeated1Name: " + vo.getName());

				tds.setCurrPageNdx(0);

				////vo.goToFirst();
        ////Reset the primary model to the "before first" state and resets the display index.
        try
        {
          vo.resetTileIndex();
        }
        catch (ModelControlException mce)
        {
          logger.debug("PSH@setupBeforePageGenerationPartySearch::Exception: " + mce);
        }

				tds.setTotalRowsChanged(false);
				tds.setCriteriaChanged(false);
			}
		}
		catch(Exception ex)
		{
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Exception @PartySearchHandler.setupBeforePageGeneration(PartySearch)");
			logger.error(ex);
			return;
		}

	}

	/**
	 *
	 *
	 */
	public void setupBeforePageGenerationPartyAdd()
	{
		logger.trace("--T--> @PartySearchHandler.setupBeforePageGeneration(PartyAdd):");

		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			if (pst == null || pst.get(ADD_EDIT_BUFFER) == null)
			{
				logger.trace("--T--> @PartySearchHandler.setupBeforePageGeneration(PartyAdd): first invocation");

				// first invocation!
				if (pst == null)
				{
					logger.trace("--T--> @PartySearchHandler.setupBeforePageGeneration(PartyAdd): creating page state table");
					pst = new Hashtable();
					pg.setPageStateTable(pst);
				}
				EditFieldsBuffer efb = new AddChangePartyBuffer("AddParty", logger);
				pst.put(ADD_EDIT_BUFFER, efb);

				// determine if this is a modify existing vs. add new
        //--Release2.1--//
        //// Not enough to translate the message here (pst is not refreshed on toggle action)
        //// and as a result label's content has not been refreshed.
        //// The label itself should be set here instead.
				//pst.put(PARTY_ADD_ACTION, "Party Add:");
        pst.put(PARTY_ADD_ACTION, "PARTY_ADD_LABEL");

				String partyProfileId =(String) pst.get(PARTY_ID_PASSED_PARM);

////logger.debug("PSH@PartyAdd::PartyProfileId: " + partyProfileId);
				if (partyProfileId != null && partyProfileId.length() > 0)
				{
					logger.trace("--T--> @PartySearchHandler.setupBeforePageGeneration(PartyAdd): is party modify, party profile id = " + partyProfileId);
					// modify!
					//pst.put(PARTY_ADD_ACTION, "Edit Existing Party:");
          //// Not enough to translate the message here (pst is not refreshed on toggle action)
          //// and as a result label's content has not been refreshed.
          //// The label itself should be set here instead.
					pst.put(PARTY_ADD_ACTION, "PARTY_EDIT_LABEL");

					// user data object to populate existing data
					////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doPartySearchModel.class);
          doPartySearchModel theDO = (doPartySearchModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doPartySearchModel.class);

					theDO.clearUserWhereCriteria();
					theDO.addUserWhereCriterion("dfPartyId", "=", new String(partyProfileId));

          ////efb.readFields(theDO);
					efb.readFields( (QueryModelBase) theDO);

					String [ ] parts = phoneSplit(efb.getFieldValue("tbPhoneNumber1"));
					efb.setFieldValue("tbPhoneNumber1", parts [ 0 ]);
					efb.setFieldValue("tbPhoneNumber2", parts [ 1 ]);
					efb.setFieldValue("tbPhoneNumber3", parts [ 2 ]);
					parts = phoneSplit(efb.getFieldValue("tbFaxNumber1"));
					efb.setFieldValue("tbFaxNumber1", parts [ 0 ]);
					efb.setFieldValue("tbFaxNumber2", parts [ 1 ]);
					efb.setFieldValue("tbFaxNumber3", parts [ 2 ]);

          //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
          //--> By Billy 26May2004
          // Set Bank Transit Number
          parts = bankTransitNumSplit(efb.getFieldValue("tbBankABABankNum"));
          efb.setFieldValue("tbBankABABankNum", parts [ 0 ]);
          efb.setFieldValue("tbBankABATransitNum", parts [ 1 ]);
          //====================================================================

					efb.setOriginalAsCurrent();
				}
			}

			// set criteria for our dummy DO - without there would be no DO binding
			// and hence this method would not be called!
			setRowGeneratorDOForNRows(1);
		}
		catch(Exception ex)
		{
			setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
			logger.error("Exception @PartySearchHandler.setupBeforePageGeneration(AppParty)");
			logger.error(ex);
			return;
		}

	}

	/**
	 *
	 *
	 */
	public void setupBeforePageGenerationPartyReview()
	{
		logger.trace("--T--> @PartySearchHandler.setupBeforePageGeneration(PartyReview):");

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();



		// get party profile id
		String pId =(String) pst.get(PARTY_ID_PASSED_PARM);

 		////SYNCADD.
 		// Add to check if Party search is editable -- By Billy 10June2002
 		// determine if user can modify existing party information
 		logger.debug("BILLY ===> @setupBeforePageGenerationPartyReview getUserAccessType(Mc.PGNM_PARTY_ADD) = " +
                getUserAccessType(Mc.PGNM_PARTY_ADD) + "  getUserAccessType(Mc.PGNM_PARTY_SEARCH) = "
                + getUserAccessType(Mc.PGNM_PARTY_SEARCH));

 		if (getUserAccessType(Mc.PGNM_PARTY_ADD) == Sc.PAGE_ACCESS_EDIT)
        pst.put(USER_CAN_EDIT_PARTY, USER_CAN_EDIT_PARTY);

 		////================================================================
		// determine if user can modify existing party information
		if (getUserAccessType(Mc.PGNM_PARTY_ADD) == Sc.PAGE_ACCESS_EDIT) pst.put(USER_CAN_EDIT_PARTY, USER_CAN_EDIT_PARTY);

		////CSpCriteriaSQLObject theDO =(CSpCriteriaSQLObject) getModel(doPartySearchModel.class);
    doPartySearchModel theDO = (doPartySearchModel)
                                            RequestManager.getRequestContext().getModelManager().getModel(doPartySearchModel.class);


		theDO.clearUserWhereCriteria();

		theDO.addUserWhereCriterion("dfPartyId", "=", new String(pId));

	}


	// called to select party (replace party for the given deal and property (in the pst)
	//  for the party selected  from search results)

	public void handleSelectParty(int partyNdx)
	{

		logger.debug("--> PartySearchHandler.handleSelectParty");
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		SessionResourceKit srk = getSessionResourceKit();

		int dealId = pg.getPageDealId();

		int copyId = pg.getPageDealCID();
		logger.debug("--> copy id - " + pg.getPageDealCID() + ", " + copyId);

		try
		{
			PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
			if (tds.getTotalRows() <= 0) return;

			// Bug Fix -- should use relative row index -- by BILLY 26Jan2001
			//int rowNdx = getCursorAbsoluteNdx(tds, partyNdx);
			int rowNdx = partyNdx;
			////int partyId = getIntValue(getRepeatedField(getCurrNDPage().getDisplayFieldValue("Repeated1/hdPartyId"), rowNdx));
			////int partyTypeId = getIntValue(getRepeatedField(getCurrNDPage().getDisplayFieldValue("Repeated1/hdPartyTypeId"), rowNdx));

			int partyId = getIntValue(getRepeatedField(("Repeated1/hdPartyId").toString(), rowNdx));
			int partyTypeId = getIntValue(getRepeatedField(("Repeated1/hdPartyTypeId").toString(), rowNdx));

			PartyProfile party = new PartyProfile(srk);
			party = party.findByPartyProfileId(partyId);
			ArrayList assoc =(ArrayList) party
                  .findByDealAndType(new DealPK(dealId, copyId), partyTypeId);
			int propertyId = getIntValue((String) pst.get(PROPERTY_ID_FILTER_PASSED_PARM));
			if (propertyId <= 0) propertyId = - 1;
			if (propertyId == - 1 && partyTypeId == PARTY_TYPE_APPRAISER)
			{
				// don't allow selection of appraiser unless a property has been passed
				setActiveMessageToAlert(BXResources.getSysMsg("PARTY_ASSOCIATION_NOT_ALLOWED", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}
			if (assoc != null && assoc.size() > 0)
			{
				logger.debug("--> PartySearchHandler.handleSelectParty -- assoc != null && assoc.size() > 0");
				// check for existing association
				PartyProfile pp = findForAssociationMatch(assoc, pg.getPageDealId(), propertyId, partyTypeId);
				if (pp != null)
				{
					logger.debug("--> PartySearchHandler.handleSelectParty -- pp != null");
					// Association for that property already exist: Update it?
					setActiveMessageToAlert(BXResources.getSysMsg("PARTY_REASSOCIATE", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMDIALOG, PARTY_REASSOCIATE_MSG);

					// Store selected row index for continuation
					pst.put(SELECTED_ROW_NDX, "" + rowNdx);
					pst.put(EXISTING_ASSOC_PARTYPROFILE_ID, "" + pp.getPartyProfileId());
					pst.put(NEW_ASSOC_PARTYPROFILE_ID, "" + partyId);
					return;
				}
			}
			srk.beginTransaction();
			party.associate(dealId, propertyId);

			logger.debug("--> PartySearchHandler.handleSelectParty -- Calling Workflow");
			//Ticket 830 - Start
			workflowTrigger(2,srk,pg,null);
			//Ticket 830 - End

			navigateToNextPage();
			srk.commitTransaction();
		


		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Party Search: Error encountered selecting new party ");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
		}

	}


	/**12
	 *
	 *
	 */
	private PartyProfile findForAssociationMatch(ArrayList assoc, int dealId, int propertyId, int partyTypeId)
	{
		int len = assoc.size();

		Iterator iter = assoc.iterator();

		while(iter.hasNext())
		{
			PartyProfile pp =(PartyProfile) iter.next();
			if (propertyId == - 1)
			{
				if (pp.getDealId() == dealId && pp.getPartyTypeId() == partyTypeId) return pp;
			}
			else
			{
				if (pp.getDealId() == dealId && pp.getPropertyId() == propertyId && pp.getPartyTypeId() == partyTypeId) return pp;
			}
		}

		return null;

	}

	/**
	 *
	 *
	 */
  ////public int handleViewPartySelectButton(CSpHtmlOutputEvent event)
  public boolean handleViewPartySelectButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getPageDealId() <= 0)
        return false;

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");


		// trick to keep track of row being generated (cannot get from CSpHtmlOutputEvent
		// object - event that triggers this call!)
    //// Wrong iMT auto-conversion.
		////int rowNdx =((Integer)TypeConverter.asInt(pst.get("ROWNDX")));
		int rowNdx =(((Integer)pst.get("ROWNDX"))).intValue();

		pst.put("ROWNDX", new Integer(rowNdx + 1));

		//logger.debug("**** handleViewPartySelectButton ::::: rowNdx = " + rowNdx + ", absolute = " + getCursorAbsoluteNdx(tds, rowNdx));

		rowNdx = getCursorAbsoluteNdx(tds, rowNdx);

		int partyStatusId = getIntValue((String)((Vector) pst.get("PARTYSTATUSCODES")).elementAt(rowNdx));
    //logger.debug("**** handleViewPartySelectButton ::::: the Party Status = " + partyStatusId);

		if (partyStatusId == Sc.PROFILE_STATUS_ACTIVE || partyStatusId == Sc.PROFILE_STATUS_CAUTION)
          return true;

		return false;
	}


	/**
	 *
	 *
	 */
	public void handleCustomActMessageOk(String[] args)
	{
		logger.debug("**** handleCustomActMessageOk ::::: args[0] = " + args [ 0 ]);

		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		SessionResourceKit srk = getSessionResourceKit();

		int dealId = pg.getPageDealId();

		
		try
		{
			if (args [ 0 ].equals(PARTY_REASSOCIATE_MSG))
			{
				int propertyId = getIntValue((String) pst.get(PROPERTY_ID_FILTER_PASSED_PARM));
				int rowNdx = getIntValue((String) pst.get(SELECTED_ROW_NDX));
				int existPartyProfileId = getIntValue((String) pst.get(EXISTING_ASSOC_PARTYPROFILE_ID));
				int newPartyProfileId = getIntValue((String) pst.get(NEW_ASSOC_PARTYPROFILE_ID));
				if (propertyId <= 0) propertyId = - 1;
				srk.beginTransaction();

				// DISSOCIATE
				PartyProfile party = new PartyProfile(srk);
				party = party.findByPartyProfileId(existPartyProfileId);
				party.disassociate(dealId, propertyId);

				// ASSOCIATE
				PartyProfile newParty = new PartyProfile(srk);
				newParty = party.findByPartyProfileId(newPartyProfileId);
				newParty.associate(dealId, propertyId);

				logger.debug("--> PartySearchHandler.handleCustomActMessageOk -- Calling Workflow");
				//Ticket 830 - Start
				workflowTrigger(2,srk,pg,null);
				//Ticket 830 - End

				navigateToNextPage();
				srk.commitTransaction();
			


			}
			else if (args [ 0 ].equals("EXIT"))
			{
				navigateToNextPage(true);
				return;
			}
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @PartySearchHandler.handleCustomActMessageOk:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}

	}


	/**
	 *
	 *
	 */
	public void handlePerformSearch()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();
logger.debug("PSH@handlePerformSearch:Stamp1");

		if (pst.get(PARTY_TYPE_FILTER_PASSED_PARM) != null)
		{
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(PARTY_SEARCH_BUFFER);

			if (efb.getFieldValueIfModified("cbPartyType") != null)
			{
				logger.debug("--D--> Party Search for deal: cannot change type: selected=" + efb.getFieldValue("cbPartyType") + ", original = " + efb.getOriginalValue("cbPartyType") + ", - restored");
				setActiveMessageToAlert(BXResources.getSysMsg("PARTY_SEARCH_DEAL_MODE_CANNOT_CHANGE_PARTY_TYPE", theSessionState.getLanguageId()),
            ActiveMsgFactory.ISCUSTOMCONFIRM);
				efb.setFieldValue("cbPartyType", efb.getOriginalValue("cbPartyType"));
			}
		}


		// Set Previous Page of the current PageEntry to the name of this page so that Alert Messages
		//   e.g. RecordNotFound, can be display after search and re-display -- by BILLY 29Jan2001
		pg.setPrevPageName(getCurrNDPage().getName());

	}


	/**
	*
	* Navigate to Add Party optionally retrieving party profile id (from selected row
	* where "edit" button pressed - page is then technically "Modify Party" but
	* spec does not call for the name (label) change.
	*
	**/
	public void handleAddParty(boolean isModify)
	{
		logger.trace("--T--> @PartySearchHandler.handleAddParty: isModify=" + isModify);

		PageEntry pg = theSessionState.getCurrentPage();

		PageEntry addPartyPage = setupSubPagePageEntry(pg, Mc.PGNM_PARTY_ADD, true);

		Hashtable subPst = addPartyPage.getPageStateTable();

		if (isModify)
		{
			String partyProfileId = getCurrNDPage().getDisplayFieldValue("hdPartyId").toString();
logger.debug("PSH@handleAddParty::PartyProfileId: " + partyProfileId);
			subPst.put(PARTY_ID_PASSED_PARM, partyProfileId);
			String partyTypeId = getCurrNDPage().getDisplayFieldValue("hdPartyTypeId").toString();
logger.debug("PSH@handleAddParty::PartyTypeId: " + partyTypeId);
			subPst.put(PARTY_TYPE_PASSED_PARM, partyTypeId);
		}
		else
		{
			// ensure not pointing to a deal
			addPartyPage.setPageDealId(0);
		}

		getSavedPages().setNextPage(addPartyPage);

		navigateToNextPage(true);

	}


	/**
	 *
	 *
	 */
	public void handleReview(int rowNdx)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		try
		{
			if (tds.getTotalRows() <= 0)
          return;

			// Bug Fix -- should use to relative row index -- by BILLY 26Jan2001
			//rowNdx = getCursorAbsoluteNdx(tds, rowNdx);
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_PARTY_REVIEW, true);

			////String partyProfileId = getRepeatedField(getCurrNDPage().getDisplayFieldValue("Repeated1/hdPartyId"), rowNdx);
			String partyProfileId = getRepeatedField(("Repeated1/hdPartyId").toString(), rowNdx).toString();
      logger.debug("PSH@handleReview::PartyProfileId: " + partyProfileId);

			Hashtable subPst = pgEntry.getPageStateTable();
			subPst.put(PARTY_ID_PASSED_PARM, partyProfileId);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception @PartySearchHandler.handleReview");
			logger.error(e);
		}
	}

	/**
	*
	* Note: New party added not associated with a deal even when page used as sub-page
	*       (when pointed to a deal!)
	*
	**/
	public void handleAddPartySubmit()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		// Add Validation -- By BILLY 02Feb2001
		// check minimun amout of data present ...
		String errorMsg = validateParty(getCurrNDPage());

		if (! errorMsg.equals(""))
		{
			// Validation Errors found -- set to display Alert Messages
			setActiveMessageToAlert(BXResources.getSysMsg("PARTY_ADD_VALIDATION_ERROR", theSessionState.getLanguageId()) + errorMsg,
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return;
		}

		// check for party modify vs. add new
		PartyProfile theParty;

		String partyProfileId =(String) pst.get(PARTY_ID_PASSED_PARM);

		if (partyProfileId == null || partyProfileId.length() == 0)
		{
			theParty = addNewParty(theSessionState, pg, pst);

			// Change to Modify mode if party successfully created
			// Bug fix by BILLY 03Oct2001
			if (theParty != null)
			{
				pst.put(PARTY_ID_PASSED_PARM, new String("" + theParty.getPartyProfileId()));
			}
		}
		else
		{
			String partyTypeId =(String) pst.get(PARTY_TYPE_PASSED_PARM);
			theParty = saveParty(getIntValue(partyProfileId), getIntValue(partyTypeId), theSessionState, pg, pst);
		}

        // #2276 - Catherine, 21-Oct-05 --- start -----------------------
        try {
          logger.debug("--> PartySearchHandler.handleAddPartySubmit() -- Calling Workflow");
          workflowTrigger(2, srk, pg, null);
        }
        catch(Exception e)
        {
          logger.error("Exception @PartySearchHandler.handleAddPartySubmit():");
          logger.error(e);
          setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMCONFIRM);
          return;
        }
        // #2276 - Catherine, 21-Oct-05 --- end -----------------------
        
		// Check duplicated parties if No error exist
		if (errorMsg.equals("") && theParty != null)
		{
			// Add Duplicate Deal Checking -- by BILLY 07March2001
			try
			{
				//--Release--2.1//
        //// Translation lookup
        ////DupeCheck theDCheck = DupeCheck.checkParty(srk, theParty, null);
        DupeCheck theDCheck = DupeCheck.checkParty(srk, theParty, null, theSessionState.getLanguageId());

				if (theDCheck.isDuplicateFound())
				{
					// Duplicated Deals found -- Add the warning message(s) to am
					ActiveMessage am = setActiveMessageToAlert(BXResources.getSysMsg("DUPECHECK_FOUND_DUPLICATED", theSessionState.getLanguageId()),
              ActiveMsgFactory.ISCUSTOMDIALOG, "EXIT");
					am.addAllMessages(theDCheck.getMessage());
					return;
				}
			}
			catch(Exception e)
			{
				//Nothing to do...
			}
		}


		// if no message set (no eror encountered) then away we go ...
		if (theSessionState.isActMessageSet() == false)
		{
			navigateToNextPage(true);
		}

	}


	/**
	 * validateParty
	 *
	 * @param thePage ViewBean
	 * @return String : the field that has caused an error
	 * @version ?? <br>
	 * 	Date: 05/30/2006<br>
	 *  Author: NBC/PP Implementation Team<br>
	 *  Change:<br>
	 *  	Added check for service branch party type
	 *
	 */
	private String validateParty(ViewBean thePage)
	{
    String retStr = "";

    // Check if ShortName input
    //--> Check only if not Origination Branch
    int partyTypeInx = 0;
    try{
      partyTypeInx = (((Integer)(theSessionState.getCurrentPage().getPageStateTable().get("PARTYTYPEID"))).intValue());
    }catch(Exception e)
    {
      //--> For the case the PARTYTYPEID is not set ==> Adding new party
      partyTypeInx = 0;
    }
    //***** Change by NBC/PP Implementation Team - Version 1.1 - START *****//
    // added check for service branch
    if(partyTypeInx != Mc.PARTY_TYPE_ORIGINATION_BRANCH && !isServiceBranch(partyTypeInx))
    //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//    	
    {
      if(thePage.getDisplayFieldValue("tbAddPartyShortName").toString().trim().equals(""))
      {
        if(!retStr.equals(""))retStr += ", ";
        retStr += "Party Short Name";
      }
    }

		// Check if LastName input
		if (thePage.getDisplayFieldValue("tbAddPartyLastName").toString().trim().equals(""))
		{
			if (! retStr.equals("")) retStr += ", ";
			retStr += "Last Name";
		}

		if (! retStr.equals("")) retStr += ".";

		return(retStr);

	}


	/**
	 *
	 *
	 */
	private PartyProfile addNewParty(SessionStateModel theSessionState, PageEntry pg, Hashtable pst)
	{
		SessionResourceKit srk = getSessionResourceKit();

		try
		{
			srk.beginTransaction();
			EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(ADD_EDIT_BUFFER);
			Contact contact = new Contact(srk);
			contact.create(PARTY_COPYID);
			Addr addr = contact.getAddr();
			String val = null;

			// --- //
			val = efb.getFieldValue("cbAddProvince");
			if (val != null) addr.setProvinceId(getIntValue(val));
			val = efb.getFieldValue("tbAddAddressLine1");
			if (val != null) addr.setAddressLine1(val);
			val = efb.getFieldValue("tbAddAddressLine2");
			if (val != null) addr.setAddressLine2(val);
			val = efb.getFieldValue("tbAddCity");
			if (val != null) addr.setCity(val);
			val = efb.getFieldValue("tbAddPostalCodeFSA");
			if (val != null) addr.setPostalFSA(val);
			val = efb.getFieldValue("tbAddPostalCodeLDU");
			if (val != null) addr.setPostalLDU(val);
			addr.ejbStore();
			val = efb.getFieldValue("tbAddPartyFirstName");
			if (val != null) contact.setContactFirstName(val);
			val = efb.getFieldValue("tbAddPartyMiddleInitial");
			if (val != null) contact.setContactMiddleInitial(val);
			val = efb.getFieldValue("tbAddPartyLastName");
			if (val != null) contact.setContactLastName(val);
			String ph = null;
			ph = phoneJoin(efb.getFieldValue("tbPhoneNumber1"), efb.getFieldValue("tbPhoneNumber2"), efb.getFieldValue("tbPhoneNumber3"));
			if (ph != null && ph.trim().length() != 0) contact.setContactPhoneNumber(ph);
			val = efb.getFieldValue("tbAddWorkPhoneExtension");
			if (val != null) contact.setContactPhoneNumberExtension(val);
			ph = phoneJoin(efb.getFieldValue("tbFaxNumber1"), efb.getFieldValue("tbFaxNumber2"), efb.getFieldValue("tbFaxNumber3"));
			if (ph != null && ph.trim().length() != 0) contact.setContactFaxNumber(ph);
			val = efb.getFieldValue("tbAddEmail");
			if (val != null) contact.setContactEmailAddress(val);

      //--DJ_PREFCOMMETHOD_CR--start--//
			val = efb.getFieldValue("cbPrefDeliveryMethod");
			if (val != null) contact.setPreferredDeliveryMethodId((getIntValue(val)));
      //--DJ_PREFCOMMETHOD_CR--end--//

			contact.ejbStore();
			PartyProfile party = new PartyProfile(srk);
			int contactId = contact.getContactId();
			int partyTypeId = Sc.PARTY_TYPE_BUSINESS_DEVELOPMENT_OFFICER;
			val = efb.getFieldValue("cbAddPartyType");
			if (val != null) partyTypeId = getIntValue(val);
			int profileStatusId = Sc.PROFILE_STATUS_ACTIVE;
			val = efb.getFieldValue("cbAddPartyStatus");
			if (val != null) profileStatusId = getIntValue(val);
			String ptPShortName = efb.getFieldValue("tbAddPartyShortName");
			String partyCompanyName = efb.getFieldValue("tbAddCompanyNameDisplay");
			String ptPBusinessId = efb.getFieldValue("tbAddClientNumber");

			// set PartyName = Firstname + MiddleInitial + LastName
      //--DJ_PT_CR--start//
      //// IMPORTANT: Now the party name is a separate field in the PartyProfile
      //// table. No concantinations of the first and last names any more!!
      //// First and last names serve the contact info purpose only.
      //// The new party name field is consisted of three fields on the screen.
			////String partyName = efb.getFieldValue("tbAddPartyFirstName") + " " + efb.getFieldValue("tbAddPartyMiddleInitial") + " " + efb.getFieldValue("tbAddPartyLastName");
			String partyName = efb.getFieldValue("txPartyNameFirstPart") +
                         efb.getFieldValue("txPartyNameSecondPart") +
                         efb.getFieldValue("txPartyNameThirdPart");

			// Added Special Notes
			String notes = efb.getFieldValue("tbNotes");

			String branchTransitNumCMHC = efb.getFieldValue("txBranchTransitNumCMHC");
			String branchTransitNumGE = efb.getFieldValue("txBranchTransitNumGE");


			////party.create(partyName, profileStatusId, contactId, partyTypeId, ptPShortName, ptPBusinessId, partyCompanyName, notes);
			party.create(partyName, profileStatusId, contactId, partyTypeId,
                   ptPShortName, ptPBusinessId, partyCompanyName,
                   notes, branchTransitNumCMHC, branchTransitNumGE);
      //--DJ_PT_CR--end//
      //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
      //--> By Billy 25May2004
      if (isIncludeBankInfo())
      {
        // Save BankTransit num
        String tmpBankNum = efb.getFieldValue("tbBankABABankNum").trim();
        String tmpTranNum = efb.getFieldValue("tbBankABATransitNum").trim();
        if(tmpBankNum.length() == 0 && tmpTranNum.length() == 0)
          party.setBankTransitNum("");
        else
          party.setBankTransitNum(tmpBankNum + "-" + tmpTranNum);
          // Save Bank Acc Num
        party.setBankAccNum(efb.getFieldValue("tbBankAccountNum"));
        // Save Bank Name
        party.setBankName(efb.getFieldValue("tbBankName"));

        party.ejbStore();
      }
      //====================================================================

			srk.commitTransaction();

			// Set modified flag to false to prevent display meaningless message to user
			pg.setModified(false);
			return party;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @PatrySearchHandler.addNewParty:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return null;
		}

	}


	/**
	 * saveParty
	 *
	 * @param partyProfileId int<br>
	 * 	Id of the party the method will save
	 * @param partyTypeId int<br>
	 * 	Id of the party type of the party that the method will save
	 * @param theSessionState SessionStateModel
	 * @param pg PageEntry <br>
	 * @param pst Hashtable<br>
	 * 
	 * @return PartyProfile : the partyProfile object representing the current party
	 * @version ??<br>
	 * 	Date: 05/31/2006<br>
	 * 	Author: NBC/PP Implementation Team<br>
	 *  Change: <br>
	 *  	Added location information for all parties
	 *  	Added service branch specific changes
	 *
	 */
	private PartyProfile saveParty(int partyProfileId, int partyTypeId, SessionStateModel theSessionState, PageEntry pg, Hashtable pst)
	{
		SessionResourceKit srk = getSessionResourceKit();

		EditFieldsBuffer efb =(EditFieldsBuffer) pst.get(ADD_EDIT_BUFFER);

		int inputPartyType = - 1;

		String s = efb.getFieldValue("cbAddPartyType");

		if (s != null) inputPartyType = getIntValue(s);

		if (inputPartyType != partyTypeId)
		{
			logger.trace("--T--> Passed Party type = " + partyTypeId + ", input = " + inputPartyType + ", match required");
			setActiveMessageToAlert(BXResources.getSysMsg("MODIFY_PARTY_CANNOT_CHANGE_TYPE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			efb.setFieldValue("cbAddPartyType", "" + partyTypeId + ".0");
			return null;
		}

		try
		{
			srk.beginTransaction();
			PartyProfile party = new PartyProfile(srk);
			party = party.findByPrimaryKey(new PartyProfilePK(partyProfileId));
			Contact contact = new Contact(srk);
			contact = contact.findByPrimaryKey(new ContactPK(party.getContactId(), 1));
			Addr addr = new Addr(srk);
			addr = addr.findByPrimaryKey(new AddrPK(contact.getAddrId(), contact.getCopyId()));
			String val = null;

			// --- //
			val = efb.getFieldValue("cbAddProvince");
			if (val != null) addr.setProvinceId(getIntValue(val));
			val = efb.getFieldValue("tbAddAddressLine1");
			if (val != null) addr.setAddressLine1(val);
			val = efb.getFieldValue("tbAddAddressLine2");
			if (val != null) addr.setAddressLine2(val);
			val = efb.getFieldValue("tbAddCity");
			if (val != null) addr.setCity(val);
			val = efb.getFieldValue("tbAddPostalCodeFSA");
			if (val != null) addr.setPostalFSA(val);
			val = efb.getFieldValue("tbAddPostalCodeLDU");
			if (val != null) addr.setPostalLDU(val);
			addr.ejbStore();
			val = efb.getFieldValue("tbAddPartyFirstName");
			if (val != null) contact.setContactFirstName(val);
			val = efb.getFieldValue("tbAddPartyMiddleInitial");
			if (val != null) contact.setContactMiddleInitial(val);
			val = efb.getFieldValue("tbAddPartyLastName");
			if (val != null) contact.setContactLastName(val);
			String ph = null;
			ph = phoneJoin(efb.getFieldValue("tbPhoneNumber1"), efb.getFieldValue("tbPhoneNumber2"), efb.getFieldValue("tbPhoneNumber3"));
			if (ph != null && ph.trim().length() != 0) contact.setContactPhoneNumber(ph);
			val = efb.getFieldValue("tbAddWorkPhoneExtension");
			if (val != null) contact.setContactPhoneNumberExtension(val);
			ph = phoneJoin(efb.getFieldValue("tbFaxNumber1"), efb.getFieldValue("tbFaxNumber2"), efb.getFieldValue("tbFaxNumber3"));
			if (ph != null && ph.trim().length() != 0) contact.setContactFaxNumber(ph);
			val = efb.getFieldValue("tbAddEmail");
			if (val != null) contact.setContactEmailAddress(val);

      //--DJ_PREFCOMMETHOD_CR--start--//
			val = efb.getFieldValue("cbPrefDeliveryMethod");
			if (val != null) contact.setPreferredDeliveryMethodId((getIntValue(val)));
      //--DJ_PREFCOMMETHOD_CR--end--//

			contact.ejbStore();
			party.setPartyTypeId(partyTypeId);
			int profileStatusId = Sc.PROFILE_STATUS_ACTIVE;
			val = efb.getFieldValue("cbAddPartyStatus");
			if (val != null) profileStatusId = getIntValue(val);
			party.setProfileStatusId(profileStatusId);

      //--DJ_PT_CR--start//
      int partyTypeInx = (((Integer)pst.get("PARTYTYPEID"))).intValue();
//logger.debug("EFB@saveParty::PST_PARTYTYPEID: " + partyTypeInx);
      //***** Change by NBC/PP Implementation Team - Version 1.1 - START *****//  
      // Added check for service branch party type
      if(partyTypeInx != Mc.PARTY_TYPE_ORIGINATION_BRANCH && !isServiceBranch(partyTypeInx))
      //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//    	  
      {
        val = efb.getFieldValue("tbAddPartyShortName");
//logger.debug("EFB@saveParty::PartyShortName: " + val);
        if (val != null) party.setPtPShortName(val);

        val = efb.getFieldValue("tbAddClientNumber");
//logger.debug("EFB@saveParty::tbAddClientNumber: " + val);
        if (val != null) party.setPtPBusinessId(val);

        val = efb.getFieldValue("txBranchTransitNumCMHC");
//logger.debug("EFB@BranchTransitNumCMHC: " + val);
        if (val != null) party.setCHMCBranchTransitNum(val);

        val = efb.getFieldValue("txBranchTransitNumGE");
//logger.debug("EFB@BranchTransitNumGE: " + val);
        if (val != null) party.setGEBranchTransitNum(val);
     }
      else if(getPartyTypeId() == Mc.PARTY_TYPE_ORIGINATION_BRANCH)
      {
        party.setPtPShortName(party.getPtPShortName());
        party.setPtPBusinessId(party.getPtPBusinessId());
        party.setCHMCBranchTransitNum(party.getCHMCBranchTransitNum());
        party.setGEBranchTransitNum(party.getGEBranchTransitNum());
      }
      //--DJ_PT_CR--end//
      //***** Change by NBC/PP Implementation Team - Version 1.1 - START *****//
      else
      if (isServiceBranch(partyTypeInx))
      {
    	  // Save the read only fields for PtPShortName & PtPBusinessId
          party.setPtPShortName(party.getPtPShortName());
          party.setPtPBusinessId(party.getPtPBusinessId());
      
      }
      //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//

			val = efb.getFieldValue("tbAddCompanyNameDisplay");
			if (val != null) party.setPartyCompanyName(val);

			// set PartyName = Firstname + MiddleInitial + LastName
      //--DJ_PT_CR--start//
      //// IMPORTANT: Now the party name is a separate field in the PartyProfile
      //// table. No concantinations of the first and last names any more!!
      //// First and last names serve the contact info purpose only.
      //// The new party name field is consisted of three fields (each is 50 characters) on the screen.
			////val = efb.getFieldValue("tbAddPartyFirstName") + " " + efb.getFieldValue("tbAddPartyMiddleInitial") + " " + efb.getFieldValue("tbAddPartyLastName");
			val = efb.getFieldValue("txPartyNameFirstPart") +
            efb.getFieldValue("txPartyNameSecondPart") +
            efb.getFieldValue("txPartyNameThirdPart");

////logger.debug("EFB@CombinedPartyNameNew: " + val);

			if (val != null) party.setPartyName(val);
      //--DJ_PT_CR--end//

			val = efb.getFieldValue("tbNotes");
			if (val != null) party.setNotes(val);

      //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
      //--> By Billy 25May2004
      if (isIncludeBankInfo())
      {
        // Save BankTransit num
        String tmpBankNum = efb.getFieldValue("tbBankABABankNum").trim();
        String tmpTranNum = efb.getFieldValue("tbBankABATransitNum").trim();
        if(tmpBankNum.length() == 0 && tmpTranNum.length() == 0)
          party.setBankTransitNum("");
        else
          party.setBankTransitNum(tmpBankNum + "-" + tmpTranNum);
          // Save Bank Acc Num
        party.setBankAccNum(efb.getFieldValue("tbBankAccountNum"));
        // Save Bank Name
        party.setBankName(efb.getFieldValue("tbBankName"));
      }
      //=====================================================================
      //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//
      // Save location
		val = efb.getFieldValue("tbLocation");
		if (val != null)
		{
			party.setLocation(val);
		}
	  //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//		

			party.ejbStore();
			srk.commitTransaction();

			// Set modified flag to false to prevent display meaningless message to user
			pg.setModified(false);
			return party;
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			logger.error("Exception @PatrySearchHandler.saveParty:");
			logger.error(e);
			setActiveMessageToAlert(BXResources.getSysMsg("TASK_RELATED_UNEXPECTED_FAILURE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMCONFIRM);
			return null;
		}

	}


	/**
	 *
	 *
	 */
	////public int handleViewModifyPartyButton(CSpHtmlOutputEvent event)
	public String handleViewModifyPartyButton(ChildContentDisplayEvent event)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		try
		{
			Hashtable pst = pg.getPageStateTable();
			if (pst.get(USER_CAN_EDIT_PARTY) != null)
          return event.getContent();
		}
		catch(Exception e)
		{
			;
		}

		return "";

	}



	/**
	 * setResultsCriteria<br>
	 * 	Builds "where" criteria for sql query into database
	 *
	 * @param theDO doPartySearchModel<br>
	 * 	Object used to interact with the database<br>
	 * @param efb EditFieldsBuffer<br>
	 * 	Object used to store values retrieved from the JSP 
	 * @param noSearch boolean<br>
	 * 	Indicates if search criterea has been passed
	 *
	 * @return none
	 * @version ??<br>
	 * Date: 05/26/2006<br>
	 * Author: NBC/PP Implementation Team<br>
	 * Change: <br>
	 * 	Added search by location 
	 *  Added call to method to search by transit number alone for service branch only
   *
	 */
	private void setResultsCriteria(doPartySearchModel theDO, EditFieldsBuffer efb, boolean noSearch)
	{
		String sqlStr = "";
    String tmp = "";

    theDO.clearUserWhereCriteria();

		// check for no criteria
		if (isSearchCriteria(efb) == false || noSearch == true)
		{
			// set impossible criteria
			////theDO.addUserWhereCriterion("dfPartyShortName", CSpCriteriaSQLObject.BEGINS_WITH_STR_OPERATOR, new String("*&^%$#@$"));
			theDO.addUserWhereCriterion("dfPartyShortName", "=", new String("*&^%$#@$"));

			return;
		}

		//***** Change by NBC/PP Implementation Team - Version 1.2 - Start *****//
			String partyType = efb.getFieldValue("cbPartyType");
			String clientTransitNum = efb.getFieldValue("tbClientNum");
			
		if (!clientTransitNum.trim().equals(""))
			{
				// add party type as part of the search criteria
		        theDO.addUserWhereCriterion("dfPartyType", "=", partyType);
	
				// create where criteria for service branch					
				tmp = StringUtil.makeQuoteSafe(clientTransitNum.toUpperCase());
				
				StringBuffer tmpSQL = new StringBuffer(SQL_CLIENT_NUM_LIKE);
				tmpSQL.append(tmp);
				tmpSQL.append("%'");
	
				((doPartySearchModelImpl) theDO).setStaticWhereCriteriaString(doPartySearchModelImpl.STATIC_WHERE_CRITERIA
				          + tmpSQL.toString());
			
		    // logger.debug("PSH@setResultsCriteria::SqlStr_Before_WHERE_STATIC_BANKTRANSITNUM_EXEC: " + ((doPartySearchModelImpl) theDO).getStaticWhereCriteriaString());					

				return;
			}
						    
		//***** Change by NBC/PP Implementation Team - Version 1.2 - End *****//
		
		// set criteria
		// To support Non-casesensitive search -- by BILLY 25Jan2001
		// Modified to search by Party Name and use Contains Str search -- Billy 07Dec2001
		if (notEmpty(efb.getFieldValue("tbPartyShortName")))
    {
        ////theDO.addDynamicStrCriterion("UPPER(partyprofile.partyname) LIKE '%" + StringUtil.makeQuoteSafe(efb.getFieldValue("tbPartyShortName").toUpperCase()) + "%'");
        tmp = StringUtil.makeQuoteSafe(efb.getFieldValue("tbPartyShortName").toUpperCase());
        logger.debug("PSH@setResultsCriteria::PartyShortNameToUpperCase: " + tmp);

        //--DJ_PT_CR--//
        //// Bug outcoming from ND. Percetage sign should be eliminated. Done for
        //// each additional where critarion below
        ////sqlStr += " AND UPPER(partyprofile.partyname) LIKE '%" + tmp + "%'";
        sqlStr += " AND UPPER(partyprofile.partyname) LIKE '" + tmp + "%'";

    }

		if (notEmpty(efb.getFieldValue("cbPartyType")))
        theDO.addUserWhereCriterion("dfPartyType", "=", new String(efb.getFieldValue("cbPartyType")));


    //--DJ_PT_CR--start--//
    //// New search criteria based on Client or Branch Transit#

		if (notEmpty(efb.getFieldValue("tbClientNum")))
    {
      tmp = StringUtil.makeQuoteSafe(efb.getFieldValue("tbClientNum").toUpperCase());
      logger.debug("PSH@setResultsCriteria::tbClientOrBranchTransitNum: " + tmp);

      sqlStr += " AND UPPER(partyprofile.ptpbusinessid) LIKE '" + tmp + "%'";

      tmp = StringUtil.makeQuoteSafe(efb.getFieldValue("tbClientNum"));
    }
    //--DJ_PT_CR--end--//

		// use Contains Str search -- Billy 07Dec2001
		if (notEmpty(efb.getFieldValue("tbCompany")))
    {
        ////theDO.addDynamicStrCriterion("UPPER(partyprofile.partycompanyname) LIKE '%" + StringUtil.makeQuoteSafe(efb.getFieldValue("tbCompany").toUpperCase()) + "%'");
        tmp = StringUtil.makeQuoteSafe(efb.getFieldValue("tbCompany").toUpperCase());
        logger.debug("PSH@setResultsCriteria::tbCompanyToUpperCase: " + tmp);

        sqlStr += " AND UPPER(partyprofile.partycompanyname) LIKE '" + tmp + "%'";

        tmp = StringUtil.makeQuoteSafe(efb.getFieldValue("tbCompany").toUpperCase());
     }
		/*
		if (notEmpty(efb.getFieldValue("tbCity")))
			theDO.addUserWhereCriterion("dfCity",
		                        CSpCriteriaSQLObject.CONTAINS_STR_OPERATOR,
		                        new String(efb.getFieldValue("tbCity")));
		*/
		if (notEmpty(efb.getFieldValue("tbCity")))
    {
        ////theDO.addDynamicStrCriterion("UPPER(addr.city) LIKE '" + StringUtil.makeQuoteSafe(efb.getFieldValue("tbCity").toUpperCase()) + "%'");
        tmp = StringUtil.makeQuoteSafe(efb.getFieldValue("tbCity").toUpperCase());
        logger.debug("PSH@setResultsCriteria::CityToUpperCase: " + tmp);

        sqlStr += " AND UPPER(addr.city) LIKE '" + tmp + "%'";
    }

		// Other means ALL province
		if (notEmpty(efb.getFieldValue("cbProvince")) && getIntValue(efb.getFieldValue("cbProvince")) != 0)
        theDO.addUserWhereCriterion("dfProvinceId", "=", new String(efb.getFieldValue("cbProvince")));

	    //***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
		// Add search criteria for the location field if needed
		String tbLocationValue = efb.getFieldValue("tbLocation");
		if (notEmpty(tbLocationValue)) 
		{
			sqlStr += setLocationResultsCriteria(tbLocationValue);
		}
		//***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//
		
		String hmPh = getPhoneSearchString(efb.getFieldValue("tbPhoneNumber1"),
                                        efb.getFieldValue("tbPhoneNumber2"),
                                        efb.getFieldValue("tbPhoneNumber3"));

    logger.debug("PSH@setResultsCriteria::HomePhone: " + hmPh);

		if (notEmpty(hmPh))
		{
			////theDO.addDynamicStrCriterion("CONTACT.CONTACTPHONENUMBER LIKE '" + hmPh + "%'");
			sqlStr += " AND CONTACT.CONTACTPHONENUMBER LIKE '" + hmPh + "%'";

			logger.debug("--D--> PARTY SEARCH PHONE CRITERIA <" + ("CONTACT.CONTACTPHONENUMBER LIKE '" + hmPh + "'%") + ">");
		}

		String faxPh = getPhoneSearchString(efb.getFieldValue("tbFaxNumber1"),
                                        efb.getFieldValue("tbFaxNumber2"),
                                        efb.getFieldValue("tbFaxNumber3"));

    logger.debug("PSH@setResultsCriteria::FaxPhone: " + faxPh);

		if (notEmpty(faxPh))
		{
			////theDO.addDynamicStrCriterion("CONTACT.CONTACTFAXNUMBER LIKE '" + faxPh + "%'");
			sqlStr +=  " AND CONTACT.CONTACTFAXNUMBER LIKE '" + faxPh + "%'";

			logger.debug("--D--> PARTY SEARCH FAX CRITERIA <" + ("CONTACT.CONTACTFAXNUMBER LIKE '" + faxPh + "%'") + ">");
		}

    logger.debug("PSH@setResultsCriteria::SqlStr_Before_WHERE_STATIC_EXEC: " + sqlStr);

    if(!sqlStr.equals("") && sqlStr != null)
    {
    ((doPartySearchModelImpl) theDO).setStaticWhereCriteriaString(doPartySearchModelImpl.STATIC_WHERE_CRITERIA
          + sqlStr);
    }
	}

	/**
	 * setLocationResultsCriteria <br>
	 * 	This method returns the search criteria for the location field
	 * 
	 * @param tbLocationValue String <br>
	 * 	String representing the location field value on a JSP
	 * 
	 * @return string : the search criteria for the location field
	 * @author NBC/PP Implementation Team
	 * @version 1.0 (Initial Version - 26 May 2006)
	 */
	protected String setLocationResultsCriteria(String tbLocationValue) 
	{
		String tmp="";
		StringBuffer sqlStr = new StringBuffer("");
		
		// substring search on location is case insensitive
		tmp = StringUtil.makeQuoteSafe(tbLocationValue.toUpperCase());
		
		sqlStr.append(SQL_LOCATION_LIKE);
		sqlStr.append(tmp);
		sqlStr.append("%'");
		
		String returnSQL = sqlStr.toString();
		return returnSQL;
	}

	/**
	 * setServiceBranchResultsCriteria<br>
	 * 	Checks if we are searching for a service branch by transit number
	 * 
	 * @param partyType String
	 * 	Value of the Party Type field on the JSP
	 * @param clientNum String
	 * 	Value of the Client Number field (transit number) on the JSP
	 * 
	 * @return boolean : true if we are searching for a service branch using transit number
	 * @author NBC/PP Implementation Team
	 * @version 1.0 (Intial Version - 26 May 2006)
	 */
	protected boolean isSearchServiceBranchByTransit(String partyType, String clientNum) {
		boolean isSearchByTransit = false; // default
		
		// Check to see if party type is Service Branch.  
		if (notEmpty(partyType) && (Integer.parseInt(partyType) == Mc.PARTY_TYPE_SERVICE_BRANCH))
		{				
			// Check if transit number field is empty	
			if (notEmpty(clientNum))
			{
				isSearchByTransit = true;
			}
		}

		return isSearchByTransit;
	}

	/**
	 *
	 *
	 */
	private boolean isSearchCriteria(EditFieldsBuffer efb)
	{
		if (notEmpty(efb.getFieldValue("tbPartyShortName"))) return true;

		if (notEmpty(efb.getFieldValue("cbPartyType"))) return true;

    //--DJ_PT_CR--start//
		if (notEmpty(efb.getFieldValue("tbClientNum"))) return true;
    //--DJ_PT_CR--end//

		if (notEmpty(efb.getFieldValue("tbCompany"))) return true;

		if (notEmpty(efb.getFieldValue("tbCity"))) return true;

		if (notEmpty(efb.getFieldValue("cbProvince"))) return true;

//***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//		
		String location = efb.getFieldValue("tbLocaiton");
		if (notEmpty(location)) 
		{
			return true;
		}
//		***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//		

		if (notEmpty(efb.getFieldValue("tbPhoneNumber1"))) return true;

		if (notEmpty(efb.getFieldValue("tbPhoneNumber2"))) return true;

		if (notEmpty(efb.getFieldValue("tbPhoneNumber3"))) return true;

		if (notEmpty(efb.getFieldValue("tbFaxNumber1"))) return true;

		if (notEmpty(efb.getFieldValue("tbFaxNumber2"))) return true;

		if (notEmpty(efb.getFieldValue("tbFaxNumber3"))) return true;

		return false;

	}


	/**
	 *
	 *
	 */
	private boolean notEmpty(String s)
	{
		if (s == null) return false;

		if (s.trim().length() > 0) return true;

		return false;

	}

  public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

    try
    {
        //--> So far we cannot find a way to manually set the Diaplay Offset of the TiledView here.
        //--> The reason is some necessary method is protected (e.g. setWebActionModelOffset()). The
        //--> work around is to create our own method (setCurrentDisplayOffset) in the TiledView class
        //--> (e.g. pgPartySearchRepeated1TiledView).  However, this method have to be implemented in all
        //--> TiledView which have Forward/Backward buttons.
        //--> Need to investigate more later  !!!!
        //--Release2.1--//
        //--> Sync it in any case. -- By Billy 17Dec2002
        ((pgPartySearchRepeated1TiledView)theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
    }
    catch(Exception mce)
    {
      logger.debug("PSH@checkAndSyncDOWithCursorInfo::Exception: " + mce);
    }
	}

	/**
	 *
	 *
	 */
	public void handleBackwardButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		handleForwardBackwardCommon(pg, tds, - 1, false);

		return;

	}


	/**
	 *
	 *
	 */
	public void handleForwardButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		handleForwardBackwardCommon(pg, tds, - 1, true);

		return;

	}


	/**
	 *
	 *
	 */
	private String getStringIntValue(String sNum)
	{
		if (sNum == null) return "";

		if (sNum.trim().length() == 0) return "";


		// now we test for integer value:
		//
		//  . return "" if cannot be converted to int
		//  . return string as is (e.g. "1.0") can be converted
		String resultStr = sNum;

		try
		{
			sNum = sNum.trim();
			int ndx = sNum.indexOf(".");
			if (ndx != - 1) sNum = sNum.substring(0, ndx);
			Integer.parseInt(sNum.trim());
			return resultStr;
		}
		catch(Exception e)
		{
			;
		}

		return "";

	}


	/**
	 *
	 *
	 */
	private String getDFStr(String s)
	{
		if (s == null) return "";

		return s;

	}


	/**
	 *
	 *
	 */
	protected int getIntValue(String sNum)
	{
		if (sNum == null) return 0;

		sNum = sNum.trim();

		if (sNum.length() == 0) return 0;

		try
		{
			// handle decimial (e.g. float type) representation
			int ndx = sNum.indexOf(".");
			if (ndx != - 1) sNum = sNum.substring(0, ndx);
			return Integer.parseInt(sNum.trim());
		}
		catch(Exception e)
		{
			;
		}

		return 0;

	}


	// Method to manually populate the display file values for repeated search results -- By BILLY 12Feb2001
	////public void setPartySearchResultDisplayFields(CSpDataObject dobject, int rowNum)
	public void setPartySearchResultDisplayFields(doPartySearchModel dobject, int rowNum)
	{

		// Set PhoneNum with extension
		////setPhoneNumTextDisplayField(dobject, "*stPhoneNumber", "dfPhone", "dfPhoneNumberExt", rowNum);
		setPhoneNumTextDisplayField(dobject, "Repeated1/stPhoneNumber", "dfPhone", "dfPhoneNumberExt", rowNum);

		// Set PhoneNum with extension
		////setPhoneNumTextDisplayField(dobject, "*stFaxNumber", "dfFax", "", rowNum);
		setPhoneNumTextDisplayField(dobject, "Repeated1/stFaxNumber", "dfFax", "", rowNum);

	}

  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
  //--> By Billy 26May2004
  // New Method to check if supporting Bank Info.
  private boolean isIncludeBankInfo()
  {
    return PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(),"com.basis100.party.includebankinfo", "N").equals("Y");
  }
  //====================================================================

  /**
   * isServiceBranch
   * 	Test to see if passes party type id is service branch
   * 
   * @param partyTypeIdx int <br>
   * @return boolean : the result of isServiceBranch
   * @version 1.0 (Initial Version - 31 May 2006)
   * @author NBC/PP Implementation Team
   */
  protected boolean isServiceBranch(int partyTypeIdx)
  {
	  boolean isServiceBranchPartyType = partyTypeIdx == Mc.PARTY_TYPE_SERVICE_BRANCH;
	  return (isServiceBranchPartyType); 
  }
}

// Class PartySearchHandler
class PartySearchBuffer extends EditFieldsBuffer
	implements Serializable
{
	private static String[][] buf={{"TextBox", "tbPartyShortName"},
                                  {"ComboBox", "cbPartyType"},
                                  //--DJ_PT_CR--//
                                  {"TextBox", "tbClientNum"},
                                  {"TextBox", "tbCompany"},
                                  {"TextBox", "tbCity"},
                                  {"ComboBox", "cbProvince"},
//                                ***** Change by NBC/PP Implementation Team - Version 1.1 - Start *****//
                                  // Add location field to the buffer
                                  {"TextBox", "tbLocation"},
//                                ***** Change by NBC/PP Implementation Team - Version 1.1 - End *****//                                  
                                  {"TextBox", "tbPhoneNumber1"},
                                  {"TextBox", "tbPhoneNumber2"},
                                  {"TextBox", "tbPhoneNumber3"},
                                  {"TextBox", "tbFaxNumber1"},
                                  {"TextBox", "tbFaxNumber2"},
                                  {"TextBox", "tbFaxNumber3"},
                                };
	private String[][] dataBuffer;


	/**
	 *
	 *
	 */
	public PartySearchBuffer(String name, SysLogger logger)
	{
		super(name, logger);
		int len = buf.length;

		setNumberOfFields(len);

		dataBuffer = new String [ len ] [ 2 ];

		for(int i = 0;
		i < len;
		++ i)
		{
			for(int j = 0;
			j < 2;
			j ++) dataBuffer [ i ] [ j ] = "";
		}

	}


	/**
	 *
	 *
	 */
	public String getFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 1 ];

	}


	/**
	 *
	 *
	 */
	public String getDOFieldName(int fldNdx)
	{
		return "";

	}


	/**
	 *
	 *
	 */
	public String getFieldType(int fldNdx)
	{
		return buf [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getOriginalValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 1 ];

	}


	// --- //

	public void setFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 1 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setDOFieldName(int fldNdx, String value)
	{
		return;

	}


	/**
	 *
	 *
	 */
	public void setFieldType(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setOriginalValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 1 ] = value;

	}

}


/**
 *
 *
 */
class AddChangePartyBuffer extends EditFieldsBuffer
	implements Serializable
{
	private static String[][] buf={{"ComboBox", "cbAddPartyType", "dfPartyType"},
                                  {"ComboBox", "cbAddPartyStatus", "dfProfileStatus"},
                                  {"TextBox", "tbAddPartyShortName", "dfPartyShortName"},
                                  {"TextBox", "tbAddClientNumber", "dfClientNumber"},
                                  {"TextBox", "tbAddPartyFirstName", "dfContactFirstName"},
                                  {"TextBox", "tbAddPartyMiddleInitial", "dfContactIntitial"},
                                  {"TextBox", "tbAddPartyLastName", "dfContactLastName"},
                                  {"TextBox", "tbAddCompanyNameDisplay", "dfCompany"},
                                  {"TextBox", "tbPhoneNumber1", "dfPhone"},
                                  {"TextBox", "tbPhoneNumber2", ""},
                                  {"TextBox", "tbPhoneNumber3", ""},
                                  {"TextBox", "tbAddWorkPhoneExtension", "dfPhoneNumberExt"},
                                  {"TextBox", "tbFaxNumber1", "dfFax"},
                                  {"TextBox", "tbFaxNumber2", ""},
                                  {"TextBox", "tbFaxNumber3", ""},
                                  {"TextBox", "tbAddEmail", "dfContactEmail"},
                                  {"TextBox", "tbAddAddressLine1", "dfAddressLine1"},
                                  {"TextBox", "tbAddAddressLine2", "dfAddressLine2"},
                                  {"TextBox", "tbAddCity", "dfCity"},
                                  {"ComboBox", "cbAddProvince", "dfProvinceId"},
                                  {"TextBox", "tbAddPostalCodeFSA", "dfPostalFSA"},
                                  {"TextBox", "tbAddPostalCodeLDU", "dfPostalLDU"},
                                  {"TextBox", "tbNotes", "dfNotes"},
                                  //--DJ_PT_CR--start--//
                                  {"TextBox", "txBranchTransitNumCMHC", "dfBranchTransitNumCMHC"},
                                  {"TextBox", "txBranchTransitNumGE", "dfBranchTransitNumGE"},
                                  {"TextBox", "txPartyNameFirstPart", "dfPartyNameFirstPart"},
                                  {"TextBox", "txPartyNameSecondPart", "dfPartyNameSecondPart"},
                                  {"TextBox", "txPartyNameThirdPart", "dfPartyNameThirdPart"},
                                  //--DJ_PT_CR--end--//
                                  //--DJ_PREFCOMMETHOD_CR--start--//
                                  {"ComboBox", "cbPrefDeliveryMethod", "dfPrefDeliveryMethodId"},
                                  //--DJ_PREFCOMMETHOD_CR--end--//
                                  //--> Ticket#369 :: Change for Cervus :: added New Bank info for Party
                                  //--> By Billy 25May2004
                                  {"TextBox", "tbBankABABankNum", "dfBankTransitNum"},
                                  {"TextBox", "tbBankABATransitNum", "dfBankTransitNum"},
                                  {"TextBox", "tbBankAccountNum", "dfBankAccNum"},
                                  {"TextBox", "tbBankName", "dfBankName"},
                                  //====================================================================
                                  //***** Change by NBC/PP Implementation Team - Version 1.1 - START *****//
                                  {"TextBox", "tbLocation", "dfLocation"},
                                  //***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//                                  
                                  };

	private String[][] dataBuffer;


	/**
	 *
	 *
	 */
	public AddChangePartyBuffer(String name, SysLogger logger)
	{
		super(name, logger);
		int len = buf.length;

		setNumberOfFields(len);

		dataBuffer = new String [ len ] [ 2 ];

		for(int i = 0;
		i < len;
		++ i)
		{
			for(int j = 0;
			j < 2;
			j ++) dataBuffer [ i ] [ j ] = "";
		}

	}


	/**
	 *
	 *
	 */
	public String getFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 1 ];

	}


	/**
	 *
	 *
	 */
	public String getDOFieldName(int fldNdx)
	{
		return buf [ fldNdx ] [ 2 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldType(int fldNdx)
	{
		return buf [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getFieldValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 0 ];

	}


	/**
	 *
	 *
	 */
	public String getOriginalValue(int fldNdx)
	{
		return dataBuffer [ fldNdx ] [ 1 ];

	}


	// --- //

	public void setFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 1 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setDOFieldName(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 2 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldType(int fldNdx, String value)
	{
		buf [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setFieldValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 0 ] = value;

	}


	/**
	 *
	 *
	 */
	public void setOriginalValue(int fldNdx, String value)
	{
		dataBuffer [ fldNdx ] [ 1 ] = value;

	}

}

