package mosApp.MosSystem;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>
 * Title: pgDisclosureONViewBean
 * 
 * Description: View bean for Disclosure ON PAGE Company: Filogix Inc. Date:
 * Removed the sections for all the provinces and making the ViewBean only for Ontario province
 * 11/28/2006
 * 
 * @author NBC/PP Implementation Team
 * @version 3.0
 * 
 */
public class pgDisclosureONViewBean extends pgDisclosureViewBean implements
		ViewBean {
	private String CHILD_CBPAGENAMES_NONSELECTED_LABEL = "";

	/**
	 * pgDisclosureONViewBean() This constructor sets the url of the jsp and
	 * also makes a call to the registerChildren() method Date: 11/28/2006
	 * 
	 * @param name
	 *            This is the name of the child form element to create
	 * 
	 */
	public pgDisclosureONViewBean() {
		super(PAGE_NAME);
		setDefaultDisplayURL(DISPLAY_URL);
		setDefaultDisplayUrl(DISPLAY_URL);
		registerChildren();
	}

	protected void initialize() {
	}

////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * createChild() This method covers creating form elements specific to ON.
	 * The method calls createChild() of the parent base view bean to load the
	 * base elements common to the page<br>
	 * Date: 11/28/2006
	 * 
	 * @param name
	 *            This is the name of the child form element to instantiate.
	 * @return Name of child view if there is a child associated with the container 
	 * 
	 */
	protected View createChild(String name) {
		View childFromParent = super.createChild(name);

		if (childFromParent != null) {
			return childFromParent;
		}
		// the "section 2: is the mortgage lender" part
		// aka section A
		else if (name.equals(CHILD_CBBROKERACTASLENDER)) {
			ComboBox child = new ComboBox(this, getDefaultModel(),
					CHILD_CBBROKERACTASLENDER, CHILD_CBBROKERACTASLENDER,
					CHILD_CBBROKERACTASLENDER_RESET_VALUE, null);
			child.setLabelForNoneSelected(CHILD_CBPAGENAMES_NONSELECTED_LABEL);
			child.setOptions(cbYesNoOptions);
			return child;
		}
		// the "section 2: if yes (to above... part
		// aka section B
		else if (name.equals(CHILD_TBNATUREOFRELATIONSHIP)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TBNATUREOFRELATIONSHIP, CHILD_TBNATUREOFRELATIONSHIP,
					CHILD_TBNATUREOFRELATIONSHIP_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_CBDATETOAPPEARMONTH)) {
			ComboBox child = new ComboBox(this, getDefaultModel(),
					CHILD_CBDATETOAPPEARMONTH, CHILD_CBDATETOAPPEARMONTH,
					CHILD_CBDATETOAPPEARMONTH_RESET_VALUE, null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbMonthOptions);

			return child;
		} else if (name.equals(CHILD_TXDATETOAPPEARDAY)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXDATETOAPPEARDAY, CHILD_TXDATETOAPPEARDAY,
					CHILD_TXDATETOAPPEARDAY_RESET_VALUE, null);

			return child;
		} else if (name.equals(CHILD_TXDATETOAPPEARYEAR)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXDATETOAPPEARYEAR, CHILD_TXDATETOAPPEARYEAR,
					CHILD_TXDATETOAPPEARYEAR_RESET_VALUE, null);

			return child;
		} else if (name.equals(CHILD_TBMORTGAGENOTRENEW)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TBMORTGAGENOTRENEW, CHILD_TBMORTGAGENOTRENEW,
					CHILD_TBMORTGAGENOTRENEW_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_TBOTHERTERMS)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TBOTHERTERMS, CHILD_TBOTHERTERMS,
					CHILD_TBOTHERTERMS_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_TBBRKREGISTRATIONNUM)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TBBRKREGISTRATIONNUM, CHILD_TBBRKREGISTRATIONNUM,
					CHILD_TBBRKREGISTRATIONNUM_RESET_VALUE, null);
			return child;
		} else if (name.equals(CHILD_TXBONUSPAIDBYBORR)) {
			TextField child = new TextField(this, getDefaultModel(),
					CHILD_TXBONUSPAIDBYBORR, CHILD_TXBONUSPAIDBYBORR,
					CHILD_TXBONUSPAIDBYBORR_RESET_VALUE, null);
			return child;
		} else
			throw new IllegalArgumentException("Invalid child name [" + name
					+ "]");
	}

	/**
	 * resetChildren()
	 * This method covers reseting form elements specific to ON
	 * The method calls resetChildren() from the parent base view bean
	 * to reset the base elements common to the page
	 * Date: 11/28/2006
	 *
	 */
	
	public void resetChildren() {
		super.resetChildren();

		getCbBrkActAsLndr().setValue(CHILD_CBBROKERACTASLENDER_RESET_VALUE);
		getTbNatureOfRelation().setValue(
				CHILD_TBNATUREOFRELATIONSHIP_RESET_VALUE);
		getCbDateToAppearMonth()
				.setValue(CHILD_CBDATETOAPPEARMONTH_RESET_VALUE);
		getTxDateToAppearDay().setValue(CHILD_TXDATETOAPPEARDAY_RESET_VALUE);
		getTxDateToAppearYear().setValue(CHILD_TXDATETOAPPEARYEAR_RESET_VALUE);
		getTbMortgageNotRenew().setValue(CHILD_TBMORTGAGENOTRENEW_RESET_VALUE);
		getTbOtherTerms().setValue(CHILD_TBOTHERTERMS_RESET_VALUE);
		getTbBrkRegistrationNum().setValue(
				CHILD_TBBRKREGISTRATIONNUM_RESET_VALUE);
		getTxBonusPaidByBorr().setValue(CHILD_TXBONUSPAIDBYBORR_RESET_VALUE);
	}

	/**
	 * registerChildren()
	 * This method covers regestering form elements specific to ON
	 * The method calls registerChildren() from the parent base view bean
	 * to register the base elements common to the page 
	 * Date: 11/28/2006
	 */
	protected void registerChildren() {
		super.registerChildren();

		registerChild(CHILD_CBBROKERACTASLENDER, ComboBox.class);
		registerChild(CHILD_TBNATUREOFRELATIONSHIP, TextField.class);
		registerChild(CHILD_CBDATETOAPPEARMONTH, ComboBox.class);
		registerChild(CHILD_TXDATETOAPPEARDAY, TextField.class);
		registerChild(CHILD_TXDATETOAPPEARYEAR, TextField.class);
		registerChild(CHILD_TBMORTGAGENOTRENEW, TextField.class);
		registerChild(CHILD_TBOTHERTERMS, TextField.class);
		registerChild(CHILD_TBBRKREGISTRATIONNUM, TextField.class);
		registerChild(CHILD_TXBONUSPAIDBYBORR, TextField.class);
	}

	/**
	 * beginDisplay()
	 * This method renders the jsp
	 * Date: 11/28/2006
	 * @param event DisplayEvent
	 */
	
	public void beginDisplay(DisplayEvent event) throws ModelControlException {

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getdoDisclosureSelectModel() == null)
			throw new ModelControlException("Primary model is null");

		DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());

		// populate the cbYesNoOptions
		String yesStr = BXResources.getGenericMsg("YES_LABEL", handler
				.getTheSessionState().getLanguageId());
		String noStr = BXResources.getGenericMsg("NO_LABEL", handler
				.getTheSessionState().getLanguageId());

		cbYesNoOptions.setOptions(new String[] { noStr, yesStr }, new String[] {
				"N", "Y" });

		// populate the months
		cbMonthOptions.populate(getRequestContext());

		handler.overridePageLabel(62);

		super.beginDisplay(event);
	}

	/**
	 * beforeModelExecutes()
	 * This method is called before rendering the page.
	 * Date: 11/28/2006
	 * @param Model model, int executionContext
	 */
	
	public boolean beforeModelExecutes(Model model, int executionContext) {
		DisclosureHandler handler = this.handler.cloneSS();

		handler.pageGetState(this);

		handler.setupBeforePageGeneration();

		handler.pageSaveState();

		// This is the analog of NetDynamics this_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);
	}

	/**
	 * beforeModelExecutes()
	 * This method is called after rendering the page.
	 * Date: 11/28/2006
	 * @param Model model, int executionContext
	 */
	
	public void afterModelExecutes(Model model, int executionContext) {
		// This is the analog of NetDynamics this_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);
	}

	/**
	 * beforeModelExecutes()
	 * This method is called after rendering the page.
	 * Date: 11/28/2006
	 * @param int executionContext
	 */
	
	public void afterAllModelsExecute(int executionContext) {
		DisclosureHandler handler = this.handler.cloneSS();

		handler.pageGetState(this);

		handler.populatePageDisplayFields();

		handler.pageSaveState();

		// This is the analog of NetDynamics
		// this_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);
	}

	/**
	 * beforeModelExecutes()
	 * This method is called when error occurs while page generation.
	 * Date: 11/28/2006
	 * @param Model model, int executionContext , ModelControlException exception
	 * @throws ModelControlException
	 */
	
	public void onModelError(Model model, int executionContext,
			ModelControlException exception) throws ModelControlException {

		// This is the analog of NetDynamics this_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}

	/**
	 * beforeModelExecutes()
	 * This method is Used to handle the GoTo Page flow..
	 * Date: 11/28/2006
	 * @param RequestInvocationEvent event
	 * @throws ServletException, IOException
	 */
	
	public void handleBtProceedRequest(RequestInvocationEvent event)
			throws ServletException, IOException {

		DisclosureHandler handler = this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleGoPage();

		handler.postHandlerProtocol();

	}

	/** This method overrides the getDefaultURL() framework JATO method and it
	* is located in each ViewBean. This allows not to stay with the JATO ViewBean
	* extension, otherwise each ViewBean a.k.a page should extend its Handler (to be
	* called by the framework) and it is not good. DEFAULT_DISPLAY_URL String is a variable now.
	* The full method is still in PHC base class. It should care the
	* the url="/" case (non-initialized defaultURL) by the BX framework
	* methods.
	* 
	* @return String Display URL
	*/
	
	public String getDisplayURL() {
		DisclosureHandler handler = (DisclosureHandler) this.handler.cloneSS();
		handler.pageGetState(this);

		String url = getDefaultDisplayURL();

		int languageId = handler.theSessionState.getLanguageId();

		// // Call the language specific URL (business delegation done in the
		// BXResource).
		if (url != null && !url.trim().equals("") && !url.trim().equals("/")) {
			url = BXResources.getBXUrl(url, languageId);
		} else {
			url = BXResources.getBXUrl(DEFAULT_DISPLAY_URL, languageId);
		}

		return url;
	}

	/**
	 * getCbBrkActAsLndr() - getter for CHILD_CBBROKERACTASLENDER
	 * 
	 * Date: 11/28/2006
	 * @return ComboBox form field
	 */
	
	public ComboBox getCbBrkActAsLndr() {
		return (ComboBox) getChild(CHILD_CBBROKERACTASLENDER);
	}

	/**
	 * getTbNatureOfRelation() - getter for CHILD_TBNATUREOFRELATIONSHIP
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	
	public TextField getTbNatureOfRelation() {
		return (TextField) getChild(CHILD_TBNATUREOFRELATIONSHIP);
	}

	/**
	 * getCbDateToAppearMonth() - getter for CHILD_CBDATETOAPPEARMONTH
	 * 
	 * Date: 11/28/2006
	 * @return ComboBox form field
	 */
	
	public ComboBox getCbDateToAppearMonth() {
		return (ComboBox) getChild(CHILD_CBDATETOAPPEARMONTH);
	}

	/**
	 * getCbBrkActAsLndr() - getter for CHILD_TXDATETOAPPEARDAY
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	
	public TextField getTxDateToAppearDay() {
		return (TextField) getChild(CHILD_TXDATETOAPPEARDAY);
	}

	/**
	 * getTxDateToAppearYear() - getter for CHILD_TXDATETOAPPEARYEAR
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	
	public TextField getTxDateToAppearYear() {
		return (TextField) getChild(CHILD_TXDATETOAPPEARYEAR);
	}

	/**
	 * getTbMortgageNotRenew() - getter for CHILD_TBMORTGAGENOTRENEW
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	
	public TextField getTbMortgageNotRenew() {
		return (TextField) getChild(CHILD_TBMORTGAGENOTRENEW);
	}

	/**
	 * getTbOtherTerms() - getter for CHILD_TBOTHERTERMS
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	
	public TextField getTbOtherTerms() {
		return (TextField) getChild(CHILD_TBOTHERTERMS);
	}

	/**
	 * getTbBrkRegistrationNum() - getter for CHILD_TBBRKREGISTRATIONNUM
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	
	public TextField getTbBrkRegistrationNum() {
		return (TextField) getChild(CHILD_TBBRKREGISTRATIONNUM);
	}

	/**
	 * getTxBonusPaidByBorr() - getter for CHILD_TXBONUSPAIDBYBORR
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	
	public TextField getTxBonusPaidByBorr() {
		return (TextField) getChild(CHILD_TXBONUSPAIDBYBORR);
	}

	// //////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	// //////////////////////////////////////////////////////////////////////////

	// //////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	// //////////////////////////////////////////////////////////////////////////

	public void handleActMessageOK(String[] args) {
		DisclosureHandler handler = this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.handleActMessageOK(args);

		handler.postHandlerProtocol();
	}

	/**
	 * 
	 * <p>
	 * Title: CbBureauPulledMonthOptionList
	 * </p>
	 * 
	 * <p>
	 * Description: Inner class that represents the options in the month CBs on
	 * this page
	 * </p>
	 * 
	 * <p>
	 * Copyright:
	 * </p>
	 * 
	 * <p>
	 * Company:
	 * </p>
	 * 
	 * @author not attributable
	 * @version 3.0
	 */
	static class CbMonthOptionList extends OptionList {
		CbMonthOptionList() {

		}

		/**
		 * populate() 
		 * populate the option list
		 * @Param -  RequestContext
		 * @return 
		 * Date: 11/28/2006
		 */
		public void populate(RequestContext rc) {
			try {
				// Get Language from SessionState
				// Get the Language ID from the SessionStateModel
				String defaultInstanceStateName = rc.getModelManager()
						.getDefaultModelInstanceName(SessionStateModel.class);
				SessionStateModelImpl theSessionState = (SessionStateModelImpl) rc
						.getModelManager().getModel(SessionStateModel.class,
								defaultInstanceStateName, true);

				int languageId = theSessionState.getLanguageId();

				// Get IDs and Labels from BXResources
				Collection c = BXResources.getPickListValuesAndDesc(theSessionState.getDealInstitutionId(),"MONTHS",
						languageId);
				Iterator l = c.iterator();
				String[] theVal = new String[2];

				// remove all values first
				clear();

				while (l.hasNext()) {
					theVal = (String[]) (l.next());
					add(theVal[1], theVal[0]);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	// //////////////////////////////////////////////////////////////////////////////
	// Class variables
	// //////////////////////////////////////////////////////////////////////////////

	/**
	 * PAGE_NAME - Name of the Ontario JSP
	 * 
	 * Date: 11/28/2006
	 * @return TextField form field
	 */
	
	public static final String PAGE_NAME = "pgDisclosureON";

	// // It is a variable now (see explanation in the getDisplayURL() method
	// above.
	public static Map FIELD_DESCRIPTORS;

	/**
	 * CHILD_CBBROKERACTASLENDER - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_CBBROKERACTASLENDER = "020E";

	/**
	 * CHILD_CBBROKERACTASLENDER_RESET_VALUE - Reset the Text Field to 'N'
	 * 
	 * Date: 11/28/2006
	 * @return String - 'N'
	 */
	
	public static final String CHILD_CBBROKERACTASLENDER_RESET_VALUE = "N";

	// populated in beginDisplay()
	private OptionList cbYesNoOptions = new OptionList();

	/**
	 * CHILD_TBNATUREOFRELATIONSHIP - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_TBNATUREOFRELATIONSHIP = "020F";

	/**
	 * CHILD_TBNATUREOFRELATIONSHIP_RESET_VALUE - Reset the Text Field to Blank
	 * 
	 * Date: 11/28/2006
	 * @return String Blank Value
	 */
	
	public static final String CHILD_TBNATUREOFRELATIONSHIP_RESET_VALUE = "";

	/**
	 * CHILD_CBDATETOAPPEARMONTH - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_CBDATETOAPPEARMONTH = "13UU_MNT";

	/**
	 * CHILD_CBDATETOAPPEARMONTH_RESET_VALUE - Reset the Text Field to Blank
	 * 
	 * Date: 11/28/2006
	 * @return String Blank Value
	 */
	
	public static final String CHILD_CBDATETOAPPEARMONTH_RESET_VALUE = "";

	// populated in beginDisplay()
	private OptionList cbMonthOptions = new CbMonthOptionList();

	/**
	 * CHILD_TXDATETOAPPEARDAY - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_TXDATETOAPPEARDAY = "13UU_DAY";

	/**
	 * CHILD_TXDATETOAPPEARDAY_RESET_VALUE - Reset the Text Field to Blank
	 * 
	 * Date: 11/28/2006
	 * @return String Blank Value
	 */
	
	public static final String CHILD_TXDATETOAPPEARDAY_RESET_VALUE = "";

	/**
	 * CHILD_TXDATETOAPPEARYEAR - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_TXDATETOAPPEARYEAR = "13UU_YEAR";

	/**
	 * CHILD_TXDATETOAPPEARYEAR_RESET_VALUE - Reset the Text Field to Blank
	 * 
	 * Date: 11/28/2006
	 * @return String Blank Value
	 */
	
	public static final String CHILD_TXDATETOAPPEARYEAR_RESET_VALUE = "";

	/**
	 * CHILD_TBMORTGAGENOTRENEW - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_TBMORTGAGENOTRENEW = "08NN";

	/**
	 * CHILD_TBMORTGAGENOTRENEW_RESET_VALUE - Reset the Text Field to Blank
	 * 
	 * Date: 11/28/2006
	 * @return String Blank Value
	 */
	
	public static final String CHILD_TBMORTGAGENOTRENEW_RESET_VALUE = "";

	/**
	 * CHILD_TBOTHERTERMS - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_TBOTHERTERMS = "09OO";

	/**
	 * CHILD_TBOTHERTERMS_RESET_VALUE - Reset the Text Field to Blank
	 * 
	 * Date: 11/28/2006
	 * @return String Blank Value
	 */
	
	public static final String CHILD_TBOTHERTERMS_RESET_VALUE = "";

	/**
	 * CHILD_TBBRKREGISTRATIONNUM - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_TBBRKREGISTRATIONNUM = "11TT";

	/**
	 * CHILD_TBBRKREGISTRATIONNUM_RESET_VALUE - Reset the Text Field to Blank
	 * 
	 * Date: 11/28/2006
	 * @return String Blank Value
	 */
	
	public static final String CHILD_TBBRKREGISTRATIONNUM_RESET_VALUE = "";

	/**
	 * CHILD_TXBONUSPAIDBYBORR - Text field on the JSP
	 * 
	 * Date: 11/28/2006
	 * @return String Text Field Name
	 */
	
	public static final String CHILD_TXBONUSPAIDBYBORR = "060S";

	/**
	 * CHILD_TXBONUSPAIDBYBORR_RESET_VALUE - Reset the Text Field to Blank
	 * 
	 * Date: 11/28/2006
	 * @return String Blank Value
	 */
	
	public static final String CHILD_TXBONUSPAIDBYBORR_RESET_VALUE = "";

	// //////////////////////////////////////////////////////////////////////////
	// Instance variables
	// //////////////////////////////////////////////////////////////////////////

	private String DISPLAY_URL = "/mosApp/MosSystem/pgDisclosureON.jsp";
}
