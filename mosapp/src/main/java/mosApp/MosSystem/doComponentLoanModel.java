package mosApp.MosSystem;

import java.math.BigDecimal;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 * Title: doComponentLoanModel
 * <p>
 * Description: This is the interface class for doComponentLoanModelImpl used to
 * retrieve data from componentloc table.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version Change Log:
 * @version 1.1 16-June-2008 XS_16.15 Added new fields for Loan component
 */
public interface doComponentLoanModel extends QueryModel, SelectQueryModel
{

    public static final String FIELD_DFCOMPONENTID = "dfComponentId";

    public static final String FIELD_DFCOPYID = "dfCopyId";

    public static final String FIELD_DFDEALID = "dfDealId";

    public static final String FIELD_DFISNTITUTIONID = "dfInstitutionId";

    public static final String FIELD_DFLOANAMOUNT = "dfLoanAmount";

    public static final String FIELD_DFNETINTRESTRATE = "dfNetIntrestRate";

    public static final String FIELD_DFDISCOUNT = "dfDiscount";

    public static final String FIELD_DFPREMIUM = "dfPremium";

    public static final String FIELD_DFCOMPONENTTYPE = "dfComponentType";

    public static final String FIELD_DFPRODUCTNAME = "dfProductName";

    /** ************MCM Impl Team XS_16.15 Starts*************************** */
    public static final String FIELD_DFPAYMENTTERMDESCRIPTION = "dfPaymentTermDescription";

    public static final String FIELD_DFPAYMENTFREQUENCY = "dfPaymentFrequency";

    public static final String FIELD_DFPOSTEDRATE = "dfPostedRate";

    public static final String FIELD_DFACTUALPAYMENTTERM = "dfActualPaymentTerm";

    public static final String FIELD_DFTOTALPAYMENT = "dfTotalPayment";

    public static final String FIELD_DFADDITIONALDETAILS = "dfAdditionalDetails";

    /** ******************************************************************** */
    
    
    /**************MCM Impl Team XS_2.37 Starts****************************/

    public static final String FIELD_DFCOMPONENTTYPEID = "dfComponentTypeId";
    
    public static final String FIELD_DFPRODUCTID = "dfProductId";

    public static final String FIELD_DFPAYMENTFREQUENCYID = "dfPaymentFrequencyId";
    
    /**************MCM Impl Team XS_2.37 ends****************************/
    /*****************MCM Impl Team XS_2.37 (dynamic drop down lists) changes start*************************/
    public static final String FIELD_DFPRICINGRATEINVENTORYID = "dfPricingrateinventoryid";
    /*****************MCM Impl Team XS_2.37 (dynamic drop down lists) changes ends*************************/

    /**
     * This method declaration returns the value of field ComponentId.
     * @return BigDecimal componentId from the model.
     */
    public java.math.BigDecimal getDfComponentId();

    /**
     * This method declaration sets the value of field ComponentId
     * @param value -
     *            new value of the field
     */
    public void setDfComponentId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field copyId.
     * @return BigDecimal copyId from the model.
     */
    public java.math.BigDecimal getDfCopyId();

    /**
     * This method declaration sets the value of field copyId
     * @param value -
     *            new value of the field
     */
    public void setDfCopyId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field InstitutionId.
     * @return BigDecimal InstitutionId from the model.
     */
    public java.math.BigDecimal getDfInstitutionId();

    /**
     * This method declaration sets the value of field InstitutionId
     * @param value -
     *            new InstitutionId.
     */
    public void setDfInstitutionId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field dfTotalLoanAmount.
     * @return BigDecimal dfTotalLoanAmount from the model
     */
    public java.math.BigDecimal getDfLoanAmount();

    /**
     * This method declaration sets the dfTotalLoanAmount.
     * @param value
     *            the new dfTotalLoanAmount
     */
    public void setDfLoanAmount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field ComponentType.
     * @return String componentType from the model
     */
    public String getDfComponentType();

    /**
     * This method declaration sets the dfComponentType.
     * @param value
     *            the new dfComponentType
     */
    public void setDfComponentType(String value);

    /**
     * This method declaration returns the value of field the NetInstrestRate.
     * @return the NetInstrestRate
     */
    public java.math.BigDecimal getDfNetInstrestRate();

    /**
     * This method declaration sets the NetInstrestRate.
     * @param value
     *            the new NetInstrestRate
     */
    public void setDfNetInstrestRate(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the Discount.
     * @return the Discount.
     */
    public java.math.BigDecimal getDfDiscount();

    /**
     * This method declaration sets the Discount.
     * @param value
     *            the new dfDiscount
     */
    public void setDfDiscount(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the Premium.
     * @return the Premium
     */
    public java.math.BigDecimal getDfPremium();

    /**
     * This method declaration sets the the Premium.
     * @param value
     *            the new Premium
     */
    public void setDfPremium(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the DealId.
     * @return the DealId
     */
    public java.math.BigDecimal getDfDealId();

    /**
     * This method declaration sets the the dealId.
     * @param value
     *            the new dealId
     */
    public void setDfDealId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ProductType.
     * @return the ProductType
     */
    public String getDfProductName();

    /**
     * This method declaration sets the the dfProductType.
     * @param value
     *            the new dfProductType
     */
    public void setDfProductName(String value);

    /** ***********************MCM Impl Team XS_16.15 *********************** */
    /**
     * This method declaration returns the value of field the Payment Term
     * Descriptions.
     * @return the ProductType
     */
    public String getDfPaymentTermDescription();

    /**
     * This method declaration sets the the dfProductType.
     * @param value
     *            the new dfProductType
     */
    public void setDfPaymentTermDescription(String value);

    /**
     * This method declaration returns the value of field the Payment Frequency.
     * @return the PaymentFrequency
     */
    public String getDfPaymentFrequency();

    /**
     * This method declaration sets the the dfProductType.
     * @param value
     *            the new dfProductType
     */
    public void setDfPaymentFrequency(String value);

    /**
     * This method declaration returns the value of field the TotalPayment.
     * @return the TotalPayment
     */
    public java.math.BigDecimal getDfTotalPayment();

    /**
     * This method declaration sets the the dealId.
     * @param value
     *            the new dealId
     */
    public void setDfTotalPayment(java.math.BigDecimal value);

    /**
     * This method declaration sets the the dfProductType.
     * @param value
     *            the new dfProductType
     */
    public void setDfAdditionalDetails(String value);

    /**
     * This method declaration returns the value of field the AdditionalDetails.
     * @return the AdditionalDetails
     */
    public String getDfAdditionalDetails();

    /**
     * This method declaration sets the the ActualPaymentTerm.
     * @param value
     *            the new ActualPaymentTerm
     */
    public void setDfActualPaymentTerm(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ActualPaymentTerm.
     * @return the ActualPaymentTerm
     */
    public java.math.BigDecimal getDfActualPaymentTerm();

    /** ********************************************************************* */
    
    
    /*************************MCM Impl Team XS_2.37 starts ************************/
    
    /**
     * This method declaration sets the the ComponentTypeId.
     * @param value
     *            the new ComponentTypeId
     */
    public void setDfComponentTypeId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ComponentTypeId.
     * @return the ComponentTypeId
     */
    public java.math.BigDecimal getDfComponentTypeId();
    
    
    /**
     * This method declaration sets the the ProductId.
     * @param value
     *            the new ProductId
     */
    public void setDfProductId(java.math.BigDecimal value);

    /**
     * This method declaration returns the value of field the ProductId.
     * @return the ProductId
     */
    public java.math.BigDecimal getDfProductId();
    
    
    /**
     * This method declaration returns the value of field the Payment Frequency.
     * @return the PaymentFrequency
     */
    public java.math.BigDecimal getDfPaymentFrequencyId();

    /**
     * This method declaration sets the the dfProductType.
     * @param value
     *            the new dfProductType
     */
    public void setDfPaymentFrequencyId(java.math.BigDecimal value);
    
    /*************************MCM Impl Team XS_2.37 ends ************************/

    
    /*********************MCM Impl Team XS_2.37 (dynamic drop down lists) changes starts******************/
    /**
     * This method declaration returns the value of field the dfPricingrateinventoryid.
     * 
     * @return the dfPricingrateinventoryid
     */
    public BigDecimal getDfPricingrateinventoryid();

    /**
     * This method declaration sets the the dfPricingrateinventoryid.
     * 
     * @param value  the new dfPricingrateinventoryid
     */
    public void setDfPricingrateinventoryid(BigDecimal value);
    /*********************MCM Impl Team XS_2.37 (dynamic drop down lists) changes ends******************/

}
