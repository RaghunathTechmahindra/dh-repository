package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;

/**
 *
 *
 *
 */
public class doDealMIStatusModelImpl extends QueryModelBase
	implements doDealMIStatusModel
{
	/**
	 *
	 *
	 */
	public doDealMIStatusModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    ////To override all the Description fields by populating from BXResource.
    ////Get the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);

    SessionStateModelImpl theSessionState =
                                          (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                           SessionStateModel.class,
                                           defaultInstanceStateName,
                                           true);

    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    while(this.next())
    {
      //// Convert MIStatus Desc.
      this.setDfMIStatusDescription(BXResources.getPickListDescription(institutionId, "MISTATUS", this.getDfMIStatusDescription(), languageId));
    }

    //Reset Location
    this.beforeFirst();

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIStatusId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFMISTATUSID);
	}


	/**
	 *
	 *
	 */
	public void setDfMIStatusId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFMISTATUSID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIStatusDescription()
	{
		return (String)getValue(FIELD_DFMISTATUSDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfMIStatusDescription(String value)
	{
		setValue(FIELD_DFMISTATUSDESCRIPTION,value);
	}


	/**
	 *
	 *
	 */
	public String getDfMIStatusUpdateAllowed()
	{
		return (String)getValue(FIELD_DFMISTATUSUPDATEALLOWED);
	}


	/**
	 *
	 *
	 */
	public void setDfMIStatusUpdateAllowed(String value)
	{
		setValue(FIELD_DFMISTATUSUPDATEALLOWED,value);
	}

	/**
	 * MetaData object test method to verify the SQL_TEMPLATE query.
	 *
	 */
   /**
        public void setResultSet(ResultSet value)
        {
            logger = SysLog.getSysLogger("METADATA");

            try
            {
                super.setResultSet(value);
                if( value != null)
                {
                    ResultSetMetaData metaData = value.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    logger.debug("===============================================");
                    logger.debug("Testing MetaData Object for new synthetic fields for" +
	                 	            " doDealMIStatusModel");
                    logger.debug("NumberColumns: " + columnCount);
                    for(int i = 0; i < columnCount ; i++)
                    {
                        logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
                    }
                    logger.debug("================================================");
                  }
              }
              catch (SQLException e)
              {
                  logger.debug("Class: " + getClass().getName());
                  logger.debug("SQLException: " + e);
              }
        }
     **/
	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Adjust Field names definitions to string format.

	public static final String DATA_SOURCE_NAME="jdbc/orcl";

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.DEALID, DEAL.COPYID, DEAL.MISTATUSID, MISTATUS.MISTATUSDESCRIPTION, MISTATUS.UPDATEALLOWED FROM DEAL, MISTATUS  __WHERE__  ";

	public static final String SELECT_SQL_TEMPLATE="SELECT ALL DEAL.DEALID, DEAL.COPYID, " +
  "DEAL.MISTATUSID, to_char(DEAL.MISTATUSID) MISTATUSID_STR, MISTATUS.UPDATEALLOWED " +
  "FROM DEAL, MISTATUS  __WHERE__  ";

	public static final String MODIFYING_QUERY_TABLE_NAME="DEAL, MISTATUS";
	public static final String STATIC_WHERE_CRITERIA=" (DEAL.MISTATUSID  =  MISTATUS.MISTATUSID)";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DEAL.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DEAL.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFMISTATUSID="DEAL.MISTATUSID";
	public static final String COLUMN_DFMISTATUSID="MISTATUSID";

  //--Release2.1--//
  //// 3. Adjust Field names definitions to string format.
	////public static final String QUALIFIED_COLUMN_DFMISTATUSDESCRIPTION="MISTATUS.MISTATUSDESCRIPTION";
	////public static final String COLUMN_DFMISTATUSDESCRIPTION="MISTATUSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFMISTATUSDESCRIPTION="DEAL.MISTATUSID_STR";
	public static final String COLUMN_DFMISTATUSDESCRIPTION="MISTATUSID_STR";

	public static final String QUALIFIED_COLUMN_DFMISTATUSUPDATEALLOWED="MISTATUS.UPDATEALLOWED";
	public static final String COLUMN_DFMISTATUSUPDATEALLOWED="UPDATEALLOWED";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMISTATUSID,
				COLUMN_DFMISTATUSID,
				QUALIFIED_COLUMN_DFMISTATUSID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMISTATUSDESCRIPTION,
				COLUMN_DFMISTATUSDESCRIPTION,
				QUALIFIED_COLUMN_DFMISTATUSDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFMISTATUSUPDATEALLOWED,
				COLUMN_DFMISTATUSUPDATEALLOWED,
				QUALIFIED_COLUMN_DFMISTATUSUPDATEALLOWED,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

