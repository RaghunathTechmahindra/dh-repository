package mosApp.MosSystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.SelectQueryModel;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledViewBase;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.event.RequestInvocationEvent;


/**
*
* The following are subject to conditional on this page, should along with the
* page entry property that controls generation.
*
*   Deal Summary Snap Shot :: pageCondition1 --> default false means DISPLAY
*   Party Details (Review) :: pageCondition2 --> default false means DISPLAY
*   Remove Party (Assoc)   :: pageCondition3 --> default false means DISPLAY
*   Assign To Deal         :: pageCondition4 --> default false means DISPLAY
*
*
**/
public class PartySummaryHandler extends PageHandlerCommon
	implements Cloneable, Sc
{
	/**
	 *
	 *
	 */
	public PartySummaryHandler cloneSS()
	{
		return(PartySummaryHandler) super.cloneSafeShallow();

	}


	/**
	 *
	 *
	 */
	public void populatePageDisplayFields()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		populatePageShellDisplayFields();

		// Quick Link Menu display
		displayQuickLinkMenu();

		populateTaskNavigator(pg);

		populatePageDealSummarySnapShot();

		populatePreviousPagesLinks();

		displayDSSConditional(pg, getCurrNDPage());

		// debug by Billy
		logger.debug("BILLY (PartySummary) ==> the Condition1 = " + pg.getPageCondition1() + "; Condition2 = " + pg.getPageCondition2() + "; Condition3 = " + pg.getPageCondition3() + "; Condition4 = " + pg.getPageCondition4());

	}

	// Modified to display Phone in the required format -- by BILLY 12Feb2001
	public void populateRepeatedFields(doPartySummaryModel dobject, int rowNum)
	{

		// Set PhoneNum with extension
		setPhoneNumTextDisplayField(dobject,
                                "RepeatedResults/stPartyPhoneNumber",
                                dobject.FIELD_DFPARTYPHONE,
                                dobject.FIELD_DFPARTYPHONEEXT,
                                rowNum);

		// Set PhoneNum with extension
		setPhoneNumTextDisplayField(dobject,
                                "RepeatedResults/stPartyFaxNumber",
                                dobject.FIELD_DFPARTYFAX,
                                "",
                                rowNum);

		//--Ticket#1700--14-Jul-2005--start--//
		// Set translated Party Name
		setTranslatedRepTextDisplayField(dobject,
                    		    "RepeatedResults/stPartyType",
                    		    dobject.FIELD_DFPARTYTYPEID,
                    		    rowNum);
		//--Ticket#1700--14-Jul-2005--end--//
	}

  //--Ticket#1700--14-Jul-2005--start--//
  private void setTranslatedRepTextDisplayField(SelectQueryModel dobject,
                                                String repFieldName,
                                                String modelFieldName,
                                                int rowNum)
  {
      try
      {
      String partyType = "";
      String partyTypeId = "";
      
      Object oDfValue = dobject.getValue(modelFieldName);
      if (oDfValue == null)
      {
        partyTypeId = "";
      }
      else if((oDfValue != null))
      {
        partyTypeId = oDfValue.toString();
      }
      
      // Translate the content first.
          partyType = BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"PARTYTYPE",
                                                          partyTypeId,
                                                          theSessionState.getLanguageId());
      
      //logger.debug("PSH@setTranslatedRepTextDisplayField::PartyTypeFromModel: " + oDfValue.toString());
      
      //// Row indexing is done by JATO framework in the nextTile() method in each TiledView bean implementation.
      // Set display field
      getCurrNDPage().setDisplayFieldValue(repFieldName, new String(partyType));

    }
    catch (Exception e)
    {
      logger.error("Exception PSH@setTranslatedRepTextDisplayField:: df=" + modelFieldName +
          "(rowNum=" + rowNum + "), exception=" + e.getMessage());
    }
  }
  //--Ticket#1700--14-Jul-2005--end--//

  //--DJ_PT_CR--start//
  /**
   * handleViewRemovePartyButton
   * 
   * @param none
   * @return boolean : the result of handleViewRemovePartyButton
   * @version ??<br>
   * 	Date: 05/31/2006<br>
   * 	Author: NBC/PP Implementation Team <br>
   * 	Change:<br>
   * 		Added check for service branch party type
   */
  public boolean handleViewRemovePartyButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();
    logger.debug("**** handleViewRemovePartyButton :: PageDealId: " + pg.getPageDealId());

		if (pg.getPageDealId() <= 0)
        return false;

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		// trick to keep track of row being generated
		int rowNdx =(((Integer)pst.get("ROWNDX"))).intValue();
		pst.put("ROWNDX", new Integer(rowNdx + 1));

		logger.debug("**** handleViewRemovePartyButton ::::: rowNdx = " + rowNdx + ", absolute = " + getCursorAbsoluteNdx(tds, rowNdx));

		rowNdx = getCursorAbsoluteNdx(tds, rowNdx);

		int partyTypeId = getIntValue((String)((Vector) pst.get("PARTYTYPEIDS")).elementAt(rowNdx));
    logger.debug("**** handleViewRemovePartyButton ::::: the Party Type = " + partyTypeId);

		if (partyTypeId == Mc.PARTY_TYPE_ORIGINATION_BRANCH)
          return false;
		//****** Change by NBC/PP Implementation Team - Version 1.1 - START *****//
		else
		if(partyTypeId == Mc.PARTY_TYPE_SERVICE_BRANCH)
		{
			return false;
		}
		//***** Change by NBC/PP Implementation Team - Version 1.1 - END *****//
		return true;
	}

  public void checkAndSyncDOWithCursorInfo(RequestHandlingTiledViewBase theRepeat)
	{
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

    try
    {
        //// So far we cannot find a way to manually set the Diaplay Offset of the TiledView here.
        //// The reason is some necessary method is protected (e.g. setWebActionModelOffset()). The
        //// work around is to create our own method (setCurrentDisplayOffset) in the TiledView class
        //// (e.g. pgPartySummaryRepeated1TiledView).  However, this method have to be implemented in all
        //// TiledView which have Forward/Backward buttons.
        //// Need to investigate more later  !!!!
        ((pgPartySummaryRepeatedResultsTiledView)theRepeat).setCurrentDisplayOffset(tds.getFirstDisplayRowNumber() - 1);
    }
    catch(Exception mce)
    {
      logger.debug("PSH@checkAndSyncDOWithCursorInfo::Exception: " + mce);
    }
	}

  //--DJ_PT_CR--end//

	/*
	 * Setup data objects associated with page - e.g. set criteria for deal snap shot, etc.
	 */
	public void setupBeforePageGeneration()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		if (pg.getSetupBeforeGenerationCalled() == true) return;

		pg.setSetupBeforeGenerationCalled(true);

		// -- //
		setupDealSummarySnapShotDO(pg);
		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");
		Hashtable pst = pg.getPageStateTable();

		if (tds == null)
		{
			// first invocation!
			// determine number of parties per page (actually we won't page this stuff!)
			String voName = "RepeatedResults";

      pgPartySummaryRepeatedResultsTiledView vo = ((pgPartySummaryViewBean)getCurrNDPage()).getRepeatedResults();

      int perPage = vo.getMaxDisplayTiles();
      logger.debug("PSUMH@setupBeforePageGenPartySummary::MaxDisplayRows_1: " + perPage);

			// set up and save task display state object
			tds = new PageCursorInfo("DEFAULT", voName, perPage, - 1);
			tds.setRefresh(true);
			tds.setCriteria("");
			pst.put(tds.getName(), tds);
		}

		// set the criteria for the populating data object
		//String applID = pg.getPageDealApplicationId();

    doPartySummaryModel partySummaryDO = (doPartySummaryModel)
                                RequestManager.getRequestContext().getModelManager().getModel(doPartySummaryModel.class);

		partySummaryDO.clearUserWhereCriteria();

		partySummaryDO.addUserWhereCriterion("dfDealId", "=", new String("" + pg.getPageDealId()));

		// determine number of parties
		int numRows = 0;

		try
		{
      try
      {
        ResultSet rs = partySummaryDO.executeSelect(null);

        if(rs == null || partySummaryDO.getSize() <= 0)
        {
          numRows = 0;
        }

        else if(rs != null && partySummaryDO.getSize() > 0)
        {
          logger.debug("PSH@setupBeforePageGeneration(PartySearch)::Size of the result set: " + partySummaryDO.getSize());
          numRows = partySummaryDO.getSize();
        }
      }
      catch (ModelControlException mce)
      {
        logger.debug("PSH@setupBeforePageGeneration(PartySearch)::MCEException: " + mce);
      }

      catch (SQLException sqle)
      {
        logger.debug("PSH@setupBeforePageGeneration(PartySearch)::SQLException: " + sqle);
      }

			pst.put("partyResult", partySummaryDO);

		}
		catch(Exception ex)
		{
			numRows = 0;
			setStandardFailMessage();
			logger.error("Exception @PartySummaryHandler.setupBeforePageGeneration");
			logger.error(ex);
		}

		tds.setTotalRows(numRows);


		// determine if we will force display of first page
		if (tds.isTotalRowsChanged())
		{
			////CSpDataDrivenVisual vo =(CSpDataDrivenVisual)(getCurrNDPage().getCommonRepeated(tds.getVoName()));
      pgPartySummaryRepeatedResultsTiledView vo = ((pgPartySummaryViewBean)getCurrNDPage()).getRepeatedResults();
      logger.debug("PSUMH@setupBeforePageGen::Repeated1: " + vo);
      logger.debug("PSUMH@setupBeforePageGen::Repeated1Name: " + vo.getName());

      tds.setCurrPageNdx(0);

      ////Reset the primary model to the "before first" state and resets the display index.
      try
      {
        vo.resetTileIndex();
      }
      catch (ModelControlException mce)
      {
        logger.debug("PSUMH@setupBeforePageGeneration::Exception: " + mce);
      }

			tds.setTotalRowsChanged(false);
		}

    //--DJ_PT_CR--start//
    logger.debug("PSUMH@setupBeforePageGeneration:Stamp1");
    try
    {
      logger.debug("PSUMH@setupBeforePageGeneration:Stamp2: " + pg.getPageDealId());

      if (pg.getPageDealId() > 0)
      {
        Vector v = new Vector();
        logger.debug("PSUMH@setupBeforePageGeneration:Stamp3: " + numRows);
        if(numRows > 0)
        {
          partySummaryDO.beforeFirst();
          while (partySummaryDO.next())
          {
             v.add((partySummaryDO.getValue(partySummaryDO.FIELD_DFPARTYTYPEID)).toString());
             logger.debug("PSUMH@setupBeforePageGeneration: " + (partySummaryDO.getValue(partySummaryDO.FIELD_DFPARTYTYPEID)).toString());
          }
        }

        pst.put("PARTYTYPEIDS", v);
        pst.put("ROWNDX", new Integer(0));
      }
      //--DJ_PT_CR--end//
    }
    catch (ModelControlException mce)
    {
      logger.debug("PSUMH@setupBeforePageGeneration::Exception: " + mce);
    }
	}


	/**
	 *
	 *
	 */
	public void handleCustomActMessageOk(String[] args)
	{

		// check for "A" response
		if (args [ 0 ].equals("A"))
		{
			// trigger std handler method for this case but with confirmed set
			handleDeleteButton(true, null);
			return;
		}

	}


	/**
	 *
	 *
	 */
	public void handleAssignButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		try
		{
			PageEntry pgEntry = setupSubPagePageEntry(pg, Mc.PGNM_PARTY_SEARCH);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception @PartySummaryHandler.handleAssignButton");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	////public void handleDetailsButton(CSpWebEvent event)
	public void handleDetailsButton(RequestInvocationEvent event)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

    logger.debug("PSUMH@handleDetailsButton::start");

		try
		{
			if (tds.getTotalRows() <= 0) return;
			int ndx = getCursorAbsoluteNdx(tds, getRowNdxFromWebEventMethod(event));
      logger.debug("PSUMH@handleDetailsButton::AbsIndex: " + ndx);

			////CSpHidden hidden =(CSpHidden)(getCurrNDPage().getDisplayField("RepeatedResults.hdPartyProfileId"));
			////String partyProfId =((String) hidden.getValue(ndx)).toString();

      TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild("RepeatedResults");

      logger.debug("PSUMH@handleDetailsButton::TiledView: " + tiledView);

      tiledView.setTileIndex(ndx);

      String partyProfId = (tiledView.getDisplayFieldValue("hdPartyProfileId")).toString();

      logger.debug("PSUMH@handleDetailsButton::partyProfId: " + partyProfId);

			PageEntry pgEntry = setupSubPagePageEntry(pg, Sc.PGNM_PARTY_REVIEW, true);
			Hashtable subPst = pgEntry.getPageStateTable();
			subPst.put(PartySearchHandler.PARTY_ID_PASSED_PARM, partyProfId);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		}
		catch(Exception e)
		{
			setStandardFailMessage();
			logger.error("Exception @PartySummaryHandler.handleDetailsButton");
			logger.error(e);
		}

	}

	/**
	 *
	 *
	 */
	////public void handleDeleteButton(boolean confirmed, CSpWebEvent event)
  public void handleDeleteButton(boolean confirmed, RequestInvocationEvent event)
	{
		PageEntry pg = theSessionState.getCurrentPage();

		PageCursorInfo tds = getPageCursorInfo(pg, "DEFAULT");

		Hashtable pst = pg.getPageStateTable();

		if (tds.getTotalRows() <= 0) return;

		if (! confirmed)
		{
			// must obtain confirmation from user - but first save row number (index)
			pg.setPageCriteriaId1(getCursorAbsoluteNdx(tds, getRowNdxFromWebEventMethod(event)));
			setActiveMessageToAlert(BXResources.getSysMsg("PARTY_APPROVE_DELETE", theSessionState.getLanguageId()),
          ActiveMsgFactory.ISCUSTOMDIALOG, "A");
			return;
		}


		// confirmed so do it!
		try
		{
			int rowNdx = pg.getPageCriteriaId1();
			////CSpHidden hidden =(CSpHidden)(getCurrNDPage().getDisplayField("RepeatedResults/hdPartyProfileId"));
			////String partyProfileId =((String) hidden.getValue(rowNdx)).toString();

      TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild("RepeatedResults");
      tiledView.setTileIndex(rowNdx);

      String partyProfileId = (tiledView.getDisplayFieldValue("hdPartyProfileId")).toString();
      logger.debug("PSUMH@handleDetailsButton::partyProfId: " + partyProfileId);

			int pId = getIntValue(partyProfileId);
			SessionResourceKit srk = getSessionResourceKit();
			srk.beginTransaction();
			PartyProfile pp = new PartyProfile(srk);
			pp = pp.findByPartyProfileId(pId);

			// remove association to deal (we assume one one association but will remove all!)
			pp.disassociate(pg.getPageDealId(), - 2);
			srk.commitTransaction();
		}
		catch(Exception e)
		{
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Exception @PartySummaryHandler.handleDeleteButton");
			logger.error(e);
		}

	}


	/**
	 *
	 *
	 */
	private void displayDSSConditional(PageEntry pg, ViewBean thePage)
	{
		String suppressStart = "<!--";

		String suppressEnd = "//-->";

		if (pg != null && pg.getPageCondition1() == false)
		{
			logger.debug("--T--> displayDSSConditional :: stIncludeDSSstart and stIncludeDSSend allow DSS");
			thePage.setDisplayFieldValue("stIncludeDSSstart", new String(""));
			thePage.setDisplayFieldValue("stIncludeDSSend", new String(""));
			return;
		}


		// suppress
		logger.debug("--T--> displayDSSConditional :: stIncludeDSSstart and stIncludeDSSend suppress DSS");

		thePage.setDisplayFieldValue("stIncludeDSSstart", new String(suppressStart));

		thePage.setDisplayFieldValue("stIncludeDSSend", new String(suppressEnd));

	}


	/**
	 *
	 *
	 */
	////public int reviseDeletePartyButtonGeneration()
  /**
	public boolean reviseDeletePartyButtonGeneration(doPartySummaryModel dobject, int rowindx)
	{

		//PageEntry pg = getTheSession().getCurrentPage();
		//if (pg.getPageCondition3() == false)
		//	return CSpPage.PROCEED;
		//return CSpPage.SKIP;

    PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
    boolean ret = false;

    if(displayRemoveButton(dobject, rowindx) == true)
    {
      if (pg.isEditable() == true)
          ret = true;
      else
          ret = false;
    }
    else if(displayRemoveButton(dobject, rowindx) == false)
    {
	    ret = false;
    }

    return ret;
  }
  **/

	/**
	 *
	 *
	 */
	////public int reviseAssignPartyButtonGeneration()
	public boolean reviseAssignPartyButtonGeneration()
	{

		//PageEntry pg = getTheSession().getCurrentPage();
		//if (pg.getPageCondition4() == false)
		//	return CSpPage.PROCEED;
		//return CSpPage.SKIP;
		return displaySubmitButton();

	}


	/**
	 *
	 *
	 */
	////public int reviseDetailsPartyButtonGeneration()
	public boolean reviseDetailsPartyButtonGeneration()
	{

		//PageEntry pg = getTheSession().getCurrentPage();
		//if (pg.getPageCondition2() == false)
		//	return CSpPage.PROCEED;
		//return CSpPage.SKIP;
		return true;

	}


	/**
	 *
	 *
	 */
	public void handleForwardButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		handleForwardBackwardCommon(pg, tds, - 1, true);

		return;

	}


	/**
	 *
	 *
	 */
	public void handleBackwardButton()
	{
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		PageCursorInfo tds =(PageCursorInfo) pst.get("DEFAULT");

		handleForwardBackwardCommon(pg, tds, - 1, false);

		return;

	}


	/**
	 *
	 *
	 */
	protected int getIntValue(String sNum)
	{
		if (sNum == null) return 0;

		sNum = sNum.trim();

		if (sNum.length() == 0) return 0;

		try
		{
			// handle decimial (e.g. float type) representation
			int ndx = sNum.indexOf(".");
			if (ndx != - 1) sNum = sNum.substring(0, ndx);
			return Integer.parseInt(sNum.trim());
		}
		catch(Exception e)
		{
			;
		}

		return 0;

	}

}

