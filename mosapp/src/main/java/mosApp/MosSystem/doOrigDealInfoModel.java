package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *
 *
 */
public interface doOrigDealInfoModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfCopyId();

	
	/**
	 * 
	 * 
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStatusDesc();

	
	/**
	 * 
	 * 
	 */
	public void setDfStatusDesc(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSourceApplicationId();

	
	/**
	 * 
	 * 
	 */
	public void setDfSourceApplicationId(String value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfSOBProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfSOBProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfApplicationDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfApplicationDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUnderwriterProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUnderwriterProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfAdministratorProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfAdministratorProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfFunderProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfFunderProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSOBFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfSOBFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSOBLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfSOBLastName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSOBPhoneNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfSOBPhoneNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSOBFaxNumber();

	
	/**
	 * 
	 * 
	 */
	public void setDfSOBFaxNumber(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfSOBPhoneExt();

	
	/**
	 * 
	 * 
	 */
	public void setDfSOBPhoneExt(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfNewDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfNewDealId(String value);
	
    public java.math.BigDecimal getDfInstitutionId();
    public void setDfInstitutionId(java.math.BigDecimal id);
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFSTATUSDESC="dfStatusDesc";
	public static final String FIELD_DFSOURCEAPPLICATIONID="dfSourceApplicationId";
	public static final String FIELD_DFSOBPROFILEID="dfSOBProfileId";
	public static final String FIELD_DFAPPLICATIONDATE="dfApplicationDate";
	public static final String FIELD_DFUNDERWRITERPROFILEID="dfUnderwriterProfileId";
	public static final String FIELD_DFADMINISTRATORPROFILEID="dfAdministratorProfileId";
	public static final String FIELD_DFFUNDERPROFILEID="dfFunderProfileId";
	public static final String FIELD_DFSOBFIRSTNAME="dfSOBFirstName";
	public static final String FIELD_DFSOBLASTNAME="dfSOBLastName";
	public static final String FIELD_DFSOBPHONENUMBER="dfSOBPhoneNumber";
	public static final String FIELD_DFSOBFAXNUMBER="dfSOBFaxNumber";
	public static final String FIELD_DFSOBPHONEEXT="dfSOBPhoneExt";
	public static final String FIELD_DFNEWDEALID="dfNewDealId";
	
	public static final String FIELD_DFINSTITUTIONID = "dfInstitutionId";
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

