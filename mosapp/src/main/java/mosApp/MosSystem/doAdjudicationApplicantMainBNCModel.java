package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * <p>Title: doAdjudicationMainBNCModel</p>
 * 
 * <p>Description: Interface class for doAdjudicationMainBNCModelImpl</p>
 * 
 * <p> Copyright: Filogix Inc. (c) 2006 </p>
 *
 * <p> Company: Filogix Inc.</p>
 *
 * @author NBC/PP Implementation Team
 * @version 1.0
 */
public interface doAdjudicationApplicantMainBNCModel extends QueryModel, SelectQueryModel
{

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerTypeId();

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId();


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public void setDfBorrowerTypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfBorrowerType();


	/**
	 *
	 *
	 */
	public void setDfBorrowerType(String value);
	
	/**
	 *
	 *
	 */
	public void setDfBorrowerGDS(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerGDS();
	
	/**
	 *
	 *
	 */
	public void setDfBorrowerTDS(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerTDS();
	
	/**
	 *
	 *
	 */
	public String getDfPrimaryBorrowerFlag();


	/**
	 *
	 *
	 */
	public void setDfPrimaryBorrowerFlag(String value);
	
	/**
	 *
	 *
	 */
	public void setDfAnnualIncomeAmount(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAnnualIncomeAmount();
	
	/**
	 *
	 *
	 */
	public void setDfOtherAnnualIncome(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfOtherAnnualIncome();
	
	/**
	 *
	 *
	 */
	public void setDfTotalAnnualIncome(java.math.BigDecimal value);
	
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalAnnualIncome();
	
	

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	
	public static final String FIELD_DFBORROWERTYPEID="dfBorrowerTypeId";
	public static final String FIELD_DFBORROWERTYPE="dfBorrowerType";
	public static final String FIELD_DFBORROWERGDS="dfBorrowerGDS";
	public static final String FIELD_DFBORROWERTDS="dfBorrowerTDS";
	public static final String FIELD_DFPRIMARYBORROWERFLAG="dfPrimaryBorrowerFlag";
	public static final String FIELD_DFANNUALINCOMEAMOUNT="dfAnnualIncomeAmount";
	public static final String FIELD_DFOTHERANNUALINCOME="dfOtherIncomeAmount";
	public static final String FIELD_DFTOTALANNUALINCOME="dfTotalAnnualIncome";



	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

