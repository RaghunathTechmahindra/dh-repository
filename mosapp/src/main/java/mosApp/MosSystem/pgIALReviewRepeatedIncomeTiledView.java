package mosApp.MosSystem;

import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import com.iplanet.jato.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;
import com.iplanet.jato.util.*;
import com.iplanet.jato.view.*;
import com.iplanet.jato.view.event.*;
import com.iplanet.jato.view.html.*;

/**
 *
 *
 *
 */
public class pgIALReviewRepeatedIncomeTiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgIALReviewRepeatedIncomeTiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doBorrowerEntryIncomeModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStIndex2().setValue(CHILD_STINDEX2_RESET_VALUE);
		getCbIncomType().setValue(CHILD_CBINCOMTYPE_RESET_VALUE);
		getCbIncomePeriod().setValue(CHILD_CBINCOMEPERIOD_RESET_VALUE);
		getTbIncomeAmount().setValue(CHILD_TBINCOMEAMOUNT_RESET_VALUE);
		getBtDeleteIncome().setValue(CHILD_BTDELETEINCOME_RESET_VALUE);
		getHdIncomeBorrowerId().setValue(CHILD_HDINCOMEBORROWERID_RESET_VALUE);
		getHdIncomeId().setValue(CHILD_HDINCOMEID_RESET_VALUE);
		getCbIncomeIncludeInGDS().setValue(CHILD_CBINCOMEINCLUDEINGDS_RESET_VALUE);
		getCbIncomeIncludeInTDS().setValue(CHILD_CBINCOMEINCLUDEINTDS_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STINDEX2,StaticTextField.class);
		registerChild(CHILD_CBINCOMTYPE,ComboBox.class);
		registerChild(CHILD_CBINCOMEPERIOD,ComboBox.class);
		registerChild(CHILD_TBINCOMEAMOUNT,TextField.class);
		registerChild(CHILD_BTDELETEINCOME,Button.class);
		registerChild(CHILD_HDINCOMEBORROWERID,HiddenField.class);
		registerChild(CHILD_HDINCOMEID,HiddenField.class);
		registerChild(CHILD_CBINCOMEINCLUDEINGDS,ComboBox.class);
		registerChild(CHILD_CBINCOMEINCLUDEINTDS,ComboBox.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoBorrowerEntryIncomeModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		}

		return movedToRow;


		// The following code block was migrated from the RepeatedIncome_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("inc @RepeatedIncome "+sumofrowsForIncome);
		sumofrowsForIncome ++;


		/$int rownum = event.getRowIndex(); //get index of the row

				Object index = new Integer(rownum + 1); //number of the row
				CSpRepeated rep = (CSpRepeated)getCommonRepeated("RepeatedIncome");
				rep.setDisplayFieldValue("stIndex2", index); //set value$/
		return;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STINDEX2))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STINDEX2,
				CHILD_STINDEX2,
				CHILD_STINDEX2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBINCOMTYPE))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryIncomeModel(),
				CHILD_CBINCOMTYPE,
				doBorrowerEntryIncomeModel.FIELD_DFINCOMETYPEID,
				CHILD_CBINCOMTYPE_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIncomTypeOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBINCOMEPERIOD))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryIncomeModel(),
				CHILD_CBINCOMEPERIOD,
				doBorrowerEntryIncomeModel.FIELD_DFINCOMEPERIODID,
				CHILD_CBINCOMEPERIOD_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIncomePeriodOptions);
			return child;
		}
		else
		if (name.equals(CHILD_TBINCOMEAMOUNT))
		{
			TextField child = new TextField(this,
				getdoBorrowerEntryIncomeModel(),
				CHILD_TBINCOMEAMOUNT,
				doBorrowerEntryIncomeModel.FIELD_DFINCOMEAMOUNT,
				CHILD_TBINCOMEAMOUNT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTDELETEINCOME))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTDELETEINCOME,
				CHILD_BTDELETEINCOME,
				CHILD_BTDELETEINCOME_RESET_VALUE,
				null);
				return child;

		}
		else
		if (name.equals(CHILD_HDINCOMEBORROWERID))
		{
			HiddenField child = new HiddenField(this,
				getdoBorrowerEntryIncomeModel(),
				CHILD_HDINCOMEBORROWERID,
				doBorrowerEntryIncomeModel.FIELD_DFBORROWERID,
				CHILD_HDINCOMEBORROWERID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDINCOMEID))
		{
			HiddenField child = new HiddenField(this,
				getdoBorrowerEntryIncomeModel(),
				CHILD_HDINCOMEID,
				doBorrowerEntryIncomeModel.FIELD_DFINCOMEID,
				CHILD_HDINCOMEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBINCOMEINCLUDEINGDS))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryIncomeModel(),
				CHILD_CBINCOMEINCLUDEINGDS,
				doBorrowerEntryIncomeModel.FIELD_DFINCOMEINCLUDINGGDS,
				CHILD_CBINCOMEINCLUDEINGDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIncomeIncludeInGDSOptions);
			return child;
		}
		else
		if (name.equals(CHILD_CBINCOMEINCLUDEINTDS))
		{
			ComboBox child = new ComboBox( this,
				getdoBorrowerEntryIncomeModel(),
				CHILD_CBINCOMEINCLUDEINTDS,
				doBorrowerEntryIncomeModel.FIELD_DFINCOMEINCLUDINGTDS,
				CHILD_CBINCOMEINCLUDEINTDS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			child.setOptions(cbIncomeIncludeInTDSOptions);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStIndex2()
	{
		return (StaticTextField)getChild(CHILD_STINDEX2);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbIncomType()
	{
		return (ComboBox)getChild(CHILD_CBINCOMTYPE);
	}

	/**
	 *
	 *
	 */
	static class CbIncomTypeOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbIncomTypeOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object INCOMETYPE_ITDESCRIPTION = results.getObject("ITDESCRIPTION");
					Object INCOMETYPEID = results.getObject("INCOMETYPEID");

					String label = (INCOMETYPE_ITDESCRIPTION == null?"":INCOMETYPE_ITDESCRIPTION.toString());

					String value = (INCOMETYPEID == null?"":INCOMETYPEID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT INCOMETYPE.ITDESCRIPTION, INCOMETYPE.INCOMETYPEID FROM INCOMETYPE";

	}



	/**
	 *
	 *
	 */
	public ComboBox getCbIncomePeriod()
	{
		return (ComboBox)getChild(CHILD_CBINCOMEPERIOD);
	}

	/**
	 *
	 *
	 */
	static class CbIncomePeriodOptionList extends OptionList
	{
		/**
		 *
		 *
		 */
		CbIncomePeriodOptionList()
		{

		}


		/**
		 *
		 *
		 */
		public void populate(RequestContext rc)
		{
			Connection c = null;
      Statement query = null;
			try {
				clear();
				if(rc == null) {
					c = SQLConnectionManagerImpl.obtainConnection("jdbc/orcl");
				}
				else {
					c = rc.getSQLConnectionManager().getConnection("jdbc/orcl");
				}

				query = c.createStatement();
				ResultSet results = query.executeQuery(FETCH_DATA_STATEMENT);

				while(results.next()) {
					Object INCOMEPERIOD_IPDESCRIPTION = results.getObject("IPDESCRIPTION");
					Object INCOMEPERIODID = results.getObject("INCOMEPERIODID");

					String label = (INCOMEPERIOD_IPDESCRIPTION == null?"":INCOMEPERIOD_IPDESCRIPTION.toString());

					String value = (INCOMEPERIODID == null?"":INCOMEPERIODID.toString());

					add(label, value);
				}
        results.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
			finally {
				try {
					if(c != null) c.close();
          if(query != null) query.close();
				}
				catch (SQLException  ex) {
					// ignore
				}
			}
		}
		private static String FETCH_DATA_STATEMENT="SELECT INCOMEPERIOD.IPDESCRIPTION, INCOMEPERIOD.INCOMEPERIODID FROM INCOMEPERIOD";

	}



	/**
	 *
	 *
	 */
	public TextField getTbIncomeAmount()
	{
		return (TextField)getChild(CHILD_TBINCOMEAMOUNT);
	}


	/**
	 *
	 *
	 */
	public Button getBtDeleteIncome()
	{
		return (Button)getChild(CHILD_BTDELETEINCOME);
	}


	/**
	 *
	 *
	 */
	public String endBtDeleteIncomeDisplay(ChildContentDisplayEvent event)
	{

		// The following code block was migrated from the btDeleteIncome_onBeforeHtmlOutputEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		//CSpHtml.sendMessage("@btDeleteIncome_onBeforeHtmlOutputEvent");
		try
		{
			CSpButton buttonHtml =(CSpButton) event.getSource();

			//CSpHtml.sendMessage("Changed2" + buttonHtml);
			String htmlstr = "<INPUT TYPE=IMAGE NAME=\"deleteAction(Income," + sumofrowsForIncome + ")\" VALUE=\"Delete\" SRC=\"/eNet_images/delete.gif\" ALIGN=TOP  width=52 height=15 alt=\"\" border=\"0\">";

			//CSpHtml.sendMessage("htmlstr " + htmlstr);
			buttonHtml.setHtmlText(htmlstr);
		}
		catch(Exception ex)
		{
			CSpHtml.sendMessage("Excp@html output " + ex.toString());
		}

		return;
		*/


		return event.getContent();
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdIncomeBorrowerId()
	{
		return (HiddenField)getChild(CHILD_HDINCOMEBORROWERID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdIncomeId()
	{
		return (HiddenField)getChild(CHILD_HDINCOMEID);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbIncomeIncludeInGDS()
	{
		return (ComboBox)getChild(CHILD_CBINCOMEINCLUDEINGDS);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbIncomeIncludeInTDS()
	{
		return (ComboBox)getChild(CHILD_CBINCOMEINCLUDEINTDS);
	}


	/**
	 *
	 *
	 */
	public doBorrowerEntryIncomeModel getdoBorrowerEntryIncomeModel()
	{
		if (doBorrowerEntryIncome == null)
			doBorrowerEntryIncome = (doBorrowerEntryIncomeModel) getModel(doBorrowerEntryIncomeModel.class);
		return doBorrowerEntryIncome;
	}


	/**
	 *
	 *
	 */
	public void setdoBorrowerEntryIncomeModel(doBorrowerEntryIncomeModel model)
	{
			doBorrowerEntryIncome = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STINDEX2="stIndex2";
	public static final String CHILD_STINDEX2_RESET_VALUE="";
	public static final String CHILD_CBINCOMTYPE="cbIncomType";
	public static final String CHILD_CBINCOMTYPE_RESET_VALUE="";
	private CbIncomTypeOptionList cbIncomTypeOptions=new CbIncomTypeOptionList();
	public static final String CHILD_CBINCOMEPERIOD="cbIncomePeriod";
	public static final String CHILD_CBINCOMEPERIOD_RESET_VALUE="";
	private CbIncomePeriodOptionList cbIncomePeriodOptions=new CbIncomePeriodOptionList();
	public static final String CHILD_TBINCOMEAMOUNT="tbIncomeAmount";
	public static final String CHILD_TBINCOMEAMOUNT_RESET_VALUE="";
	public static final String CHILD_BTDELETEINCOME="btDeleteIncome";
	public static final String CHILD_BTDELETEINCOME_RESET_VALUE="Delete";
	public static final String CHILD_HDINCOMEBORROWERID="hdIncomeBorrowerId";
	public static final String CHILD_HDINCOMEBORROWERID_RESET_VALUE="";
	public static final String CHILD_HDINCOMEID="hdIncomeId";
	public static final String CHILD_HDINCOMEID_RESET_VALUE="";
	public static final String CHILD_CBINCOMEINCLUDEINGDS="cbIncomeIncludeInGDS";
	public static final String CHILD_CBINCOMEINCLUDEINGDS_RESET_VALUE="N";
	private OptionList cbIncomeIncludeInGDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});
	public static final String CHILD_CBINCOMEINCLUDEINTDS="cbIncomeIncludeInTDS";
	public static final String CHILD_CBINCOMEINCLUDEINTDS_RESET_VALUE="N";
	private OptionList cbIncomeIncludeInTDSOptions=new OptionList(new String[]{"Yes", "No"},new String[]{"Y", "N"});


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doBorrowerEntryIncomeModel doBorrowerEntryIncome=null;

}

