package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doUserAdminGetJointAppModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfUserProfileId();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserProfileId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUserLogin();

	
	/**
	 * 
	 * 
	 */
	public void setDfUserLogin(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfContactFirstName();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactFirstName(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfContactLastName();

	
	/**
	 * 
	 * 
	 */
	public void setDfContactLastName(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFUSERPROFILEID="dfUserProfileId";
	public static final String FIELD_DFUSERLOGIN="dfUserLogin";
	public static final String FIELD_DFCONTACTFIRSTNAME="dfContactFirstName";
	public static final String FIELD_DFCONTACTLASTNAME="dfContactLastName";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

