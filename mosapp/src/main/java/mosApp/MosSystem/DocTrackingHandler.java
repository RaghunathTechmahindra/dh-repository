package mosApp.MosSystem;

/**
 * 27/Feb/2007 DVG #DG582 FXP14477: NBC - PecEmail document not produced for deal 600802 
 * 26/Jul/2006 DVG #DG472 #3992  Documents not being generated
        - signed commit docs are good when waived
 * 23/May/2006 DVG #DG426 #3242  apostrophe quadruples when used in notes
 * 07/Apr/2006 DVG #DG402 #2710  DJ - prod - too many documents created ....
 - new check box to prevent creation of docs
 * Nov 13, 2008, FXP23399 MCM changed optionList for Status combo to be populated all the time.
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.commitment.CCM;
import com.basis100.deal.conditions.ConditionHandler;
import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docprep.xsl.XSLMerger;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.ChangeMapValue;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.DBA;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.condition.management.ConditionUpdateRequester;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.TiledViewBase;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.ListBox;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.TextField;

/* ALERT ALERT worry about later:

 // conditiontype table - updated aug 18
 public static final int CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING = 0;
 public static final int CONDITION_TYPE_DOC_TRACKING_ONLY = 1;
 */

////SYNCADD.
//==============================================================================
//PageCondition4 -- true :: user selected "Rejected" on any DocTracking Record(s)
//Have to enable all options for user to fallback the changes
//-- false :: normal
//- By BILLY 24June2002
//==============================================================================
/**
 * 
 * @version 1.2 <br>
 *          Date: 06/06/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          2 methods in this class have changed for pec email functionality -
 *          handleSubmitStandard() and setDealStatus() <br>
 *          <br>*
 * @version 1.3 <br>
 *          Date: 06/07/006 <br>
 *          Author: NBC/PP Implementation Team <br>
 *          Change: <br>
 *          Handle the deal status of closing instruction/condition satisfied in
 *          handleSubmitStandard() method from doc tracking screen <br>
 *  
 */
public class DocTrackingHandler extends PageHandlerCommon implements Cloneable, Sc {

	public static final String REJECT_COND_NO = "REJECT_COND_NO";
	public static final String CONDITION_REVIEW_ADD_TYPE = "CONDITION_REVIEW_ADD_TYPE";
	public static final String CONDITION_REVIEW_DELETE = "CONDITION_REVIEW_DELETE";
	public static final String BUSINESS_RULE_CRITICAL = "BUSINESS_RULE_CRITICAL";
	public static final String DOCUMENT_TRACKING_REJECT_CONDITION = "DOCUMENT_TRACKING_REJECT_CONDITION";
	public static final String CONDITION_REVIEW_POST_DECISION_COMMITMENT_REQ = "CONDITION_REVIEW_POST_DECISION_COMMITMENT_REQ";

	public static final String STATUS_INFO = "STATUS_INFO";

	public static final String OVERRIDE_DEAL_LOCK_YES = "OVERRIDE_DEAL_LOCK_YES";
	public static final String OVERRIDE_DEAL_LOCK_NO = "OVERRIDE_DEAL_LOCK_NO";
	public static final String REJECT_COND_YES = "REJECT_COND_YES";
	public static final String COMMITMENT_REQ_NO = "COMMITMENT_REQ_NO";
	public static final String COMMITMENT_REQ_YES = "COMMITMENT_REQ_YES";

	public static final String COM_BASIS100_CONDITIONS_ISDJDOCUMENTGENERATION = "com.basis100.conditions.isdjdocumentgeneration";
	public static final String COM_BASIS100_DEAL_DOCTRACKING_CKPRODUCECONDDEFAULT = "com.basis100.deal.doctracking.ckproduceconddefault";
	public static final String COM_BASIS100_CONDITIONS_INCLUDESTDCUSTOMSECTIONSONDTS = "com.basis100.conditions.includestdcustomsectionsondts";

	public static final int SECTION_SIGNED_COMMITMENT = 1;
	public static final int SECTION_ORDERED = 2;
	public static final int SECTION_FUNDER = 3;
	public static final int SECTION_STANDARD_CONDITION = 4;
	public static final int SECTION_CUSTOM_CONDITION = 5;

	private static final int ALERT_LABEL_ABSENT = 1;
	private static final int ALERT_TEXT_ABSENT = 2;

	// Special usage of DocumentTracking (Standard and Custom Condition)
	// PageEntry members:
	//
	// 1) pageCondition5
	//
	//    Value - true : Condition are fine - just display - no rebuild
	//
	//          - false: Default conditions to be (re)build upon page generation.
	//
	// 2) pageCondition7. Used during validation of Standard/Custom Condition
	// input
	//
	//      Value - true : Force to produce the commitment letter for the condition
	// changed
	//                   in the DocTrackingDetails screen.
	//          - false : Default status, regular page flow.
	//
	//--Release2.1--TD_DTS_CR--end//
	//--Ticket#304--start--//
	// 2) pageCondition8. Used during checking a condition on Reject status
	//
	//      Value - true : Force to produce the ActiveMessage with the prompt to Deny
	// deal.
	//          - false : Default status, regular page flow.
	//
	//--Ticket#304--end--//

	/**
	 * 
	 *  
	 */
	public DocTrackingHandler cloneSS() {
		return (DocTrackingHandler) super.cloneSafeShallow();
	}

	public void populatePageDisplayFields() {
		PageEntry pg = theSessionState.getCurrentPage();

		Hashtable pst = pg.getPageStateTable();

		populatePageShellDisplayFields();
		// Quick Link Menu display
	    displayQuickLinkMenu();
		populateTaskNavigator(pg);
		populatePageDealSummarySnapShot();
		populatePreviousPagesLinks();

		ViewBean thePage = getCurrNDPage();
		populateFieldValidation(thePage);

		//// Hide of display Standard/Custom Conditions Sections on the
		//// DocumentTracking screen.
		if (PropertiesCache.getInstance().getProperty(
		    theSessionState.getDealInstitutionId(),
				COM_BASIS100_CONDITIONS_INCLUDESTDCUSTOMSECTIONSONDTS, "N")
				.equals("N")) {
			startSupressSection("stIncludeStdCondSectionStart", thePage);
			endSupressSection("stIncludeStdCondSectionEnd", thePage);

			startSupressSection("stIncludeCustCondSectionStart", thePage);
			endSupressSection("stIncludeCustCondSectionEnd", thePage);

			startSupressSection("Repeated4/stIncludeStdTileStart", thePage);
			endSupressSection("Repeated4/stIncludeStdTileEnd", thePage);

			startSupressSection("Repeated5/stIncludeCustTileStart", thePage);
			endSupressSection("Repeated5/stIncludeCustTileEnd", thePage);

		}

		startSupressContent("stDisplayDJCheckBoxStart", thePage);
		endSupressContent("stDisplayDJCheckBoxEnd", thePage);

		// populate the Condition Note which stored in PageState1
		thePage.setDisplayFieldValue("tbConditionNoteText", pg.getPageState1());

		//#DG402 #2710 show only if DJ and statusId ok
		boolean isDJ = isDJClient();
		int theStatusId = -1;
		if (isDJ)
			try {
				theStatusId = new Deal(srk, null).getStatusId(
						pg.getPageDealId(), pg.getPageDealCID());
			} catch (Exception ex) {	}
			if (!(isDJ && (theStatusId == Mc.DEAL_CONDITIONS_SATISFIED || theStatusId == Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED))) {
				startSupressContent("stDispDJProduceDocsStart", thePage);
				endSupressContent("stDispDJProduceDocsEnd", thePage);
			}
			//#DG402 end
	}

	/**
	 * 
	 *  
	 */
	public void setupBeforePageGeneration() {
		PageEntry pg = theSessionState.getCurrentPage();

		TiledView vo2 = null;
		TiledView vo4 = null;
		TiledView vo5 = null;

		if (pg.getSetupBeforeGenerationCalled() == true)
			return;

		pg.setSetupBeforeGenerationCalled(true);

		int numRows = 0;

		try {
			int dealId = pg.getPageDealId();
			int copyId = pg.getPageDealCID();
			MasterDeal md = new MasterDeal(srk, null, dealId);
			ViewBean currNDPage = getCurrNDPage();
			PageCursorInfo tds1 = getPageCursorInfo(pg, "Repeated1");
			PageCursorInfo tds2 = getPageCursorInfo(pg, "Repeated2");

			// Modified to handle Funder Conditions -- By BILLY 11Jan2002
			PageCursorInfo tds3 = getPageCursorInfo(pg, "Repeated3");

			//--Release2.1--TD_DTS_CR--//
			PageCursorInfo tds4 = getPageCursorInfo(pg, "Repeated4");
			PageCursorInfo tds5 = getPageCursorInfo(pg, "Repeated5");

			PageCursorInfo origDocs = getPageCursorInfo(pg, "Repeated2");
			//int origNumOfRows = 0;

			//--Release2.1--TD_DTS_CR--end//

			Hashtable pst = pg.getPageStateTable();

			// ordered ...
// CIBC DTV			
//			doDocTrackOrderedModelImpl dtDo = (doDocTrackOrderedModelImpl) (RequestManager
//					.getRequestContext().getModelManager()
//					.getModel(doDocTrackOrderedModel.class));
			
			QueryModelBase dtDo =(QueryModelBase)
	          (RequestManager.getRequestContext().getModelManager().getModel(doDocTrackOrderedModel.class));

			dtDo.clearUserWhereCriteria();
			dtDo.addUserWhereCriterion("dfDealId", "=", new Integer(dealId));
			dtDo.addUserWhereCriterion("dfCopyId", "=", new Integer(copyId));

			//--Release2.1--start//
			//// Setup current displaying LanguageID
			dtDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
					new Integer(theSessionState.getLanguageId()));

			////logger.debug("DTH@setupBeforePageGeneration::The SQL : " +
			// dtDo.getSelectSQL());
			//--Release2.1--end//

			try {
				ResultSet rs = dtDo.executeSelect(null);

				if (rs == null || dtDo.getSize() <= 0) {
					logger.debug("DTH@setupBeforePageGeneration::No row returned!!");
					numRows = 0;
				} else if (dtDo.getSize() > 0) {
					numRows = dtDo.getSize();
				}
			} catch (ModelControlException mce) {
				logger.debug("DTH@setupBeforePageGeneration::MCEException: "+ mce);
			}

			catch (SQLException sqle) {
				logger.debug("DTH@setupBeforePageGeneration::SQLException: "+ sqle);
			}

			// Trick not to display the new Standard/Custom condition until
			// full page submit logic process is done.
			boolean refresh = false;
			Boolean theRefreshFlag = (Boolean) pst.get("REFRESH");
			if (theRefreshFlag != null)
				refresh = theRefreshFlag.booleanValue();

			if (tds2 == null) {
				String voName2 = "Repeated2";
				tds2 = new PageCursorInfo(voName2, voName2, numRows, numRows);
				tds2.setRefresh(false);
				pst.put(tds2.getName(), tds2);
				pg.setPageCondition3(passAAG(pg, srk));
			} else {
				tds2.setTotalRows(numRows);
				tds2.setRowsPerPage(numRows);
			}

			// BXTODO: JATO framework does this job, we need simply to
			// override these parameters here.
			vo2 = (TiledView) (getCurrNDPage().getChild(tds2.getVoName()));
			vo2.setMaxDisplayTiles(numRows);
// CIBC DTV
//			doDocTrackFunderCondModelImpl dtFunderDo = (doDocTrackFunderCondModelImpl) (RequestManager
//					.getRequestContext().getModelManager()
//					.getModel(doDocTrackFunderCondModel.class));
			
			QueryModelBase dtFunderDo =(QueryModelBase)
	          (RequestManager.getRequestContext().getModelManager().getModel(doDocTrackFunderCondModel.class));

			dtFunderDo.clearUserWhereCriteria();
			dtFunderDo.addUserWhereCriterion("dfDealId", "=", new Integer(
					dealId));
			dtFunderDo.addUserWhereCriterion("dfCopyId", "=", new Integer(
					copyId));

			// Setup current displaying LanguageID
			dtFunderDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
					new Integer(theSessionState.getLanguageId()));

			try {
				ResultSet rs = dtFunderDo.executeSelect(null);

				if (rs == null || dtFunderDo.getSize() <= 0) {
					logger.debug("DTH@setupBeforePageGeneration::No row returned!!");
					numRows = 0;
				} else if (dtFunderDo.getSize() > 0) {
					//logger.debug("DTH@setupBeforePageGeneration::SizeDocTrackFunder:
					// " + dtFunderDo.getSize());
					numRows = dtFunderDo.getSize();
				}
			} catch (ModelControlException mce) {
				logger.debug("DTH@setupBeforePageGeneration::MCEException: "+ mce);
			} catch (SQLException sqle) {
				logger.debug("DTH@setupBeforePageGeneration::SQLException: "+ sqle);
			}

			if (tds3 == null) {
				String voName3 = "Repeated3";
				tds3 = new PageCursorInfo(voName3, voName3, numRows, numRows);
				tds3.setRefresh(true);
				pst.put(tds3.getName(), tds3);

				//pg.setPageCondition3(passAAG(pg,srk));
			} else {
				tds3.setTotalRows(numRows);
				tds3.setRowsPerPage(numRows);
			}

			pgDocTrackingRepeated3TiledView vo3 = ((pgDocTrackingViewBean) getCurrNDPage())
					.getRepeated3();

			//// JATO framework does this job, we need simply to override these
			// parameters here.
			//// It is even not necessary to do.
			////vo3 =(TiledView)(getCurrNDPage().getChild(tds3.getVoName()));
			vo3.setMaxDisplayTiles(numRows);

			// signed ...
// CIBC DTV			
//			doDocTrackSignedCommitmentModelImpl dtCommitmentDo = (doDocTrackSignedCommitmentModelImpl) (RequestManager
//					.getRequestContext().getModelManager()
//					.getModel(doDocTrackSignedCommitmentModel.class));
			
			QueryModelBase dtCommitmentDo =(QueryModelBase)
	          (RequestManager.getRequestContext().getModelManager().getModel(doDocTrackSignedCommitmentModel.class));

			dtCommitmentDo.clearUserWhereCriteria();
			dtCommitmentDo.addUserWhereCriterion("dfDealId", "=", new Integer(
					dealId));
			dtCommitmentDo.addUserWhereCriterion("dfCopyId", "=", new Integer(
					copyId));

			// Setup current displaying LanguageID
			dtCommitmentDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
					new Integer(theSessionState.getLanguageId()));

			////logger.debug("DTH@setupBeforePageGeneration::The SQL : " +
			// dtFunderDo.getSelectSQL());

			try {
				ResultSet rs = dtCommitmentDo.executeSelect(null);

				if (rs == null || dtCommitmentDo.getSize() <= 0) {
					logger.debug("DTH@setupBeforePageGeneration::No row returned!!");
					numRows = 0;
				} else if (dtCommitmentDo.getSize() > 0) {
					logger.debug("DTH@setupBeforePageGeneration::SizeDocTrackSignedCommitment: "
							+ dtCommitmentDo.getSize());
					numRows = dtCommitmentDo.getSize();
				}
			} catch (ModelControlException mce) {
				logger.debug("DTH@setupBeforePageGeneration::MCEException: "+ mce);
			}

			catch (SQLException sqle) {
				logger.debug("DTH@setupBeforePageGeneration::SQLException: "+ sqle);
			}

			if (tds1 == null) {
				String voName1 = "Repeated1";
				tds1 = new PageCursorInfo(voName1, voName1, numRows, numRows);
				tds1.setRefresh(true);
				pst.put(tds1.getName(), tds1);
			} else {
				tds1.setTotalRows(numRows);
				tds1.setRowsPerPage(numRows);
			}
// CIBC DTV
//			doDocTrackingStdConditionModel stdConDo = (doDocTrackingStdConditionModel) (RequestManager
//					.getRequestContext().getModelManager()
//					.getModel(doDocTrackingStdConditionModel.class));
			
			QueryModelBase stdConDo =(QueryModelBase)
	          (RequestManager.getRequestContext().getModelManager().getModel(doDocTrackingStdConditionModel.class));
			
			stdConDo.clearUserWhereCriteria();
			stdConDo.addUserWhereCriterion("dfDealId", "=", new String(""
					+ dealId));
			stdConDo.addUserWhereCriterion("dfCopyId", "=", new String(""
					+ copyId));

			stdConDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
					new Integer(theSessionState.getLanguageId()));
			stdConDo.executeSelect(null);
			numRows = stdConDo.getSize();
			logger.trace("@DocTrackingHandler.setupBeforePageGeneration: Number standard conditions = "
					+ numRows);
			if (tds4 == null) {
				String voName = "Repeated4";
				tds4 = new PageCursorInfo("Repeated4", voName, numRows, numRows);
				tds4.setRefresh(true);
				pst.put(tds4.getName(), tds4);
			} else {
				tds4.setTotalRows(numRows);
				tds4.setRowsPerPage(numRows);
			}

			vo4 = (TiledView) (getCurrNDPage().getChild(tds4.getVoName()));
			vo4.setMaxDisplayTiles(numRows);

			doDocTrackingCusConditionModel cusConDo = (doDocTrackingCusConditionModel) (RequestManager
					.getRequestContext().getModelManager()
					.getModel(doDocTrackingCusConditionModel.class));
			cusConDo.clearUserWhereCriteria();
			cusConDo.addUserWhereCriterion("dfDealId", "=", new String(""
					+ dealId));
			cusConDo.addUserWhereCriterion("dfCopyId", "=", new String(""
					+ copyId));

			//// Setup current displaying LanguageID
			//// As per confirmation from the Product Team Custom Condition
			// should be
			//// created for both languages.
			////conDo.addUserWhereCriterion("dfLanguagePrederenceId", "=", new
			// Integer(0));
			cusConDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
					new Integer(theSessionState.getLanguageId()));
			//=======================================
			cusConDo.executeSelect(null);
			numRows = cusConDo.getSize();
			logger.trace("@DocTrackingHandler.setupBeforePageGeneration: Number custom conditions = "
					+ numRows);
			if (tds5 == null) {
				String voName = "Repeated5";
				tds5 = new PageCursorInfo("Repeated5", voName, numRows, numRows);
				tds5.setRefresh(true);
				pst.put(tds5.getName(), tds5);
			} else {
				tds5.setTotalRows(numRows);
				tds5.setRowsPerPage(numRows);
			}
			vo5 = (TiledView) (getCurrNDPage().getChild(tds5.getVoName()));
			vo5.setMaxDisplayTiles(numRows);
			//--Release2.1--TD_DTS_CR--end//

			if (tds1.isTotalRowsChanged()) {
				// number of rows has changed - unconditional display of first
				// page
				pgDocTrackingRepeated1TiledView vo1 = ((pgDocTrackingViewBean) getCurrNDPage())
						.getRepeated1();
				tds1.setCurrPageNdx(0);
				vo1.resetTileIndex();
				tds1.setTotalRowsChanged(false);
			}
			if (tds2.isTotalRowsChanged()) {
				// number of rows has changed - unconditional display of first
				// page
				vo2 = ((pgDocTrackingViewBean) getCurrNDPage()).getRepeated2();
				tds2.setCurrPageNdx(0);
				////Reset the primary model to the "before first" state and
				// resets the display index.
				vo2.resetTileIndex();
				tds2.setTotalRowsChanged(false);
			}
			if (tds3.isTotalRowsChanged()) {
				// number of rows has changed - unconditional display of first
				// page
				vo3 = ((pgDocTrackingViewBean) getCurrNDPage()).getRepeated3();

				tds3.setCurrPageNdx(0);
				////Reset the primary model to the "before first" state and
				// resets the display index.
				vo3.resetTileIndex();
				tds3.setTotalRowsChanged(false);
			}

			//--Release2.1--TD_DTS_CR--start//
			if (tds4.isTotalRowsChanged()) {
				// number of rows has changed - unconditional display of first
				// page
				vo4 = (TiledView) (getCurrNDPage().getChild(tds4.getVoName()));

				tds4.setCurrPageNdx(0);
				vo4.resetTileIndex();
				tds4.setTotalRowsChanged(false);
			}

			if (tds5.isTotalRowsChanged()) {
				vo5 = (TiledView) (getCurrNDPage().getChild(tds5.getVoName()));

				tds5.setCurrPageNdx(0);
				vo5.resetTileIndex();
				tds5.setTotalRowsChanged(false);
			}

			//// Default value is false: not produce the Commitment Letter (see
			// the class Header).
			pg.setPageCondition7(false);
			//--Release2.1--TD_DTS_CR--end//

			//--Ticket#304--start--//
			pg.setPageCondition8(false);
			//--Ticket#304--start--//

			// Added to set ckProduceCondUpdateDoc based on Parameter
			if (PropertiesCache.getInstance().getProperty(
			        theSessionState.getDealInstitutionId(),
					COM_BASIS100_DEAL_DOCTRACKING_CKPRODUCECONDDEFAULT, "Y")
					.equals("Y")) {
				////logger.debug("DTH@setupBeforePageGeneration::ScenarioT");
				currNDPage.setDisplayFieldValue("ckProduceCondUpdateDoc",
						new String("T"));
			} else {
				////logger.debug("DTH@setupBeforePageGeneration::ScenarioF");
				currNDPage.setDisplayFieldValue("ckProduceCondUpdateDoc","F");
			}
			logger.debug("DTH@setupBeforePageGeneration::ProduceCheckBoxValue: "
					+ currNDPage.getDisplayFieldValue("ckProduceCondUpdateDoc"));

			// <!-- Catherine, 7-Apr-05, pvcs#817 -->
			String val;
			if (pg.getPageState1().equals("")) {
				val = getLastConditionNoteText(pg.getPageDealId());
			} else {
				val = pg.getPageState1();
			}
			pg.setPageState1(val);
			// <!-- Catherine, 7-Apr-05, pvcs#817 end -->

			setupDealSummarySnapShotDO(pg);
		} catch (Exception ex) {
			setStandardFailMessage();
			logger.error("Document Tracking Handler. Problem encountered during Data Object running ");
			logger.error(ex);
		}
	}

	/**
	 * @version 1.2 <br>
	 *          Date: 06/06/006 <br>
	 *          Author: NBC/PP Implementation Team <br>
	 *          Added code for pec email handling for doctracking screen <br>
	 * @version 1.3 <br>
	 *          Date: 06/07/006 <br>
	 *          Author: NBC/PP Implementation Team <br>
	 *          Added code for pec email handling for doctracking screen for
	 *          conditions satisfied closing instr <br>
	 */
	//--Release2.1--TD_DTS_CR--start//
	//// This method is totally re-done in order to re-commit the letter and
	//// re-build the Conditions of Approval Outstanding Letter for:
	//// 1. All changed conditions via DocTrackingDetails screen (changed, not
	// visited!!),
	//// should appear in the Conditions of Approval Outstanding Letter.
	//// 2. All added Standard Conditions (based on their type business logic)
	// should be
	//// added on the screens (DocTracking or/and Condtion) and into the
	// Commitment letter.
	//// 3. All added Custom Conditions (based on their type business logic)
	// should be
	//// added on the screens (DocTracking or/and Condtion) and into the
	// Commitment letter.
	public void handleSubmitStandard() {
		logger.trace("DocTrackingHandler@handleSubmitStandard");

		PageEntry pg = theSessionState.getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();
		Hashtable pst = pg.getPageStateTable();
		StatusInfo si = (StatusInfo) pst.get(STATUS_INFO);

		logger.debug("DTH@handleSubmitStandard:SI_DealStatusId: "
				+ si.dealStatusId);
		logger.debug("DTH@handleSubmitStandard:SI_DealStatusSet: "
				+ si.dealStatusSet);
		logger.debug("DTH@handleSubmitStandard:PageCondition8: "
				+ pg.getPageCondition8());
		logger.debug("DTH@handleSubmitStandard:PageCondition4: "
				+ pg.getPageCondition4());

		try {
			//--Ticket#304--start--//
			if (validateDocumentTracking(pg, srk) != 0){
                return;
			}

			//if (si.dealStatusId == Sc.DEAL_DENIED && si.dealStatusSet ==
			// true)
			if (pg.getPageCondition8() == true) {
				// entry rejected ... dialog to inform user will cause deal to
				// be denied ...
				setActiveMessageToAlert(BXResources.getSysMsg(
						DOCUMENT_TRACKING_REJECT_CONDITION, theSessionState
						.getLanguageId()),
						ActiveMsgFactory.ISCUSTOMDIALOG2, REJECT_COND_YES,
				REJECT_COND_NO);
				return;

				//--Ticket#304--end--//

			} // end if

			logger.debug("DTH@handleSubmitStandard::CheckBoxValue:"
					+ getCurrNDPage().getDisplayFieldValue("ckbDJProduceDocs"));
			if (PropertiesCache.getInstance().getProperty(
			        theSessionState.getDealInstitutionId(),
					COM_BASIS100_CONDITIONS_ISDJDOCUMENTGENERATION, "N").equals("Y")
					&& getCurrNDPage().getDisplayFieldValue("ckbDJProduceDocs").equals("T")) //#DG402 DJ
			{
				//// Place requests for #47 (63), #53(65), #61(70), and 40(62)
				// in case of DJ client.
				//logger.debug("VLAD ===> TEST_Before :: Before handleAdditionalDJDocs!!");
				handleAdditionalDJDocs();
				//logger.debug("VLAD ===> TEST_After :: After handleAdditionalDJDocs!!");
			}
			logger.debug("DTH@handleSubmitStandard:Stamp2");

			//re-Set PageCondition4 just in case
			// - By Billy 24June2002
			pg.setPageCondition4(false);

			logger.debug("DTH@handleSubmitStandard:Stamp3");

			////if (validateDocumentTracking(pg, srk) != 0) return;

			//// #1 (see above).
			updateCondForApprovalOutstandingLetter(pg, pst, "Repeated2");

			int rowRep4Cnt = getTotalNoOfRowsForThisPageVar("Repeated4/hdStdDocTrackId");
			logger.debug("DTH@handleSubmitStandard::RowCount4: " + rowRep4Cnt);
			logger.debug("DTH@handleSubmitStandard::PageCondition7_1: "
					+ pg.getPageCondition7());

			//// #2 (see above).
			if (rowRep4Cnt > 0) {
				submitNewCondtionsFromDocTracking(pg,
						"Repeated4/hdStdDocTrackId", "Repeated4");
				pg.setPageCondition7(true);
			}

			int rowRep5Cnt = getTotalNoOfRowsForThisPageVar("Repeated5/hdCusDocTrackId");
			logger.debug("DTH@handleSubmitStandard::RowCount5: " + rowRep5Cnt);

			//// #3 (see above).
			if (rowRep5Cnt > 0) {
				submitNewCondtionsFromDocTracking(pg,
						"Repeated5/hdCusDocTrackId", "Repeated5");
				pg.setPageCondition7(true);
			}

			logger.debug("DTH@handleSubmitStandard::PageCondition7_final: "
					+ pg.getPageCondition7());

			if (pg.getPageCondition7() == true) {
				// Prompt User if produce a commitment
				setActiveMessageToAlert(BXResources.getSysMsg(
						CONDITION_REVIEW_POST_DECISION_COMMITMENT_REQ,
						theSessionState.getLanguageId()),
						ActiveMsgFactory.ISCUSTOMDIALOG2, COMMITMENT_REQ_YES, COMMITMENT_REQ_NO);
				
				//#DG582 requestPecEmail will be requested later in 'handleCustomActMessageOk'
				
			} else { // if (pg.getPageCondition7() == false) {
				// // Update db with the screen changes.
				logger.debug("DTH@handleSubmitStandard::PageCondition7False_start");
				standardAdoptTxCopy(pg, true);
				// <!-- Catherine, 7-Apr-05, pvcs#817 -->
				// setDealStatus() calls document request, so this should be
				// called before document is requested
				createConditionNote(pg, srk);
				// <!-- Catherine, 7-Apr-05, pvcs#817 end -->
				// ***** Change by NBC Impl. Team - Version 1.3 - Start *****//
				Deal deal = setDealStatus(pg, srk, si);
				requestPecEmail(srk, deal);		//#DG582 after deal status has been updated!				
				workflowTrigger(2, srk, pg, null);
				
				//need to execute insert ConditionUpdateRequeust before navigateToNextPage - 4.2GR
                insertConditionUpdateRequest(pg);
				navigateToNextPage();

				////handleCancelStandard(true);
				////return;
				logger.debug("DTH@handleSubmitStandard::PageCondition7False_end");
			}
		} catch (Exception e) {
			srk.cleanTransaction();
			logger.error("Exception @DoctrackingHandler.handleSubmitStandard");
			logger.error(e);
			navigateAwayFromFromPage(true);
		} // end outer try
	}

	//#DG582 moved from up, and rewritten pecmail document request
	/*
	 * Request pecEmail document, if right conditions and not yet produced
	 */
	private void requestPecEmail(SessionResourceKit srk, Deal deal) throws RemoteException, FinderException, DocPrepException, CreateException {
		// ***** Change by NBC Impl. Team - Version 1.3 - Start *****//

		int theStatusId = deal.getStatusId();
		logger.debug(getClass(), "requestPecEmail: deal.getStatusId()="+theStatusId);
		if (theStatusId != Mc.DEAL_CONDITIONS_SATISFIED 
				&& theStatusId != DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED)
			return;

		WorkflowTrigger workflowTrigger = new WorkflowTrigger(srk);
		workflowTrigger.setSilentMode(true);
		final String wftValue = workflowTrigger.getWorkflowTriggerValue(
				deal.getDealId(), Mc.WORKFLOWTRIGGER_TRIGGERKEY_AUTOPREAPPROVAL);
		if (wftValue != "")
			return;

		DocumentRequest.requestDealSummary(srk, deal,
				Dc.DOCUMENT_PEC_EMAIL_DEAL_SUMMARY,
				XSLMerger.TYPE_PDF, Dc.MESSAGE_PEC_EMAIL);

		workflowTrigger.setWorkflowTriggerValue(deal.getDealId(),
				Mc.WORKFLOWTRIGGER_TRIGGERKEY_AUTOPREAPPROVAL, "Y");
		// ***** Change by NBC Impl. Team - Version 1.3 - End *****//
	}

	protected void submitNewCondtionsFromDocTracking(PageEntry page,
			String qualifiedDocTrackIds, String tiledName) {
		try {
			int rowcnt = getTotalNoOfRowsForThisPageVar(qualifiedDocTrackIds);

			// Preparation for the common data
			// do nothing
			if (rowcnt < 0)
				return;
			
			SessionResourceKit srk1 = new SessionResourceKit(theSessionState.getExpressState());

			for (int i = 0; i < rowcnt; i++) {
				Deal deal = new Deal(srk1, null, page.getPageDealId(), page
						.getPageDealCID());
				int sourceOfBusinessCategory = deal.getSourceOfBusinessProfileId();
				int applicationId = com.iplanet.jato.util.TypeConverter.asInt(
						page.getPageDealApplicationId().trim());
				
				TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(
						tiledName);

				tiledView.setTileIndex(i);

				////New requirement -- if Deal.status = CommitOffered (13) or
				// ConditionOut (16)
				//// Then update the DocumentDuedate and Status as we does when
				//// Approving the deal.
				CalcMonitor dcm = CalcMonitor.getMonitor(srk1);
				deal.setCalcMonitor(dcm);
				int theDealStatusId = deal.getStatusId();
				int theDealSpecialFeatureId = deal.getSpecialFeatureId();

				logger.debug("DTH@addCondForApprovalOutstandingLetter::Row[ "+ rowcnt 
						+ "]:, DealStatusId: " + theDealStatusId);

				//// From DocTracking Page system should create the Commitment
				// letter and
				//// the returnDate for the a deal with the status in the
				// range:
				//// greater than DEAL_PRE_APPROVAL_OFFERED (11)
				//// and less than DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED
				// (19)!!
				if (theDealStatusId == Mc.DEAL_COMMIT_OFFERED
						|| theDealStatusId == Mc.DEAL_PRE_APPROVAL_OFFERED
						|| theDealStatusId == Mc.DEAL_COMMIT_OFFERED
						|| theDealStatusId == Mc.DEAL_OFFER_ACCEPTED
						|| theDealStatusId == Mc.DEAL_PRE_APPROVAL_FIRM
						|| (theDealStatusId == Mc.DEAL_CONDITIONS_OUT && theDealSpecialFeatureId != 1)
						|| theDealStatusId == Mc.DEAL_CONDITIONS_SATISFIED
						|| theDealStatusId == Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT
						|| theDealStatusId == Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED
						|| theDealStatusId == Mc.DEAL_POST_DATED_FUNDING
						|| theDealStatusId == Mc.DEAL_FUNDING_REVERSED) {
				        try {
        					// update document tracking conditions
        	                srk1.beginTransaction();
        					ConditionHandler cd = new ConditionHandler(srk1);
        					cd.updateForPDecision(deal);
        
        					// Prompt User if produce a commitment
        					////setActiveMessageToAlert(BXResources.getSysMsg("CONDITION_REVIEW_POST_DECISION_COMMITMENT_REQ",
        					// theSessionState.getLanguageId()),
        					//// ActiveMsgFactory.ISCUSTOMDIALOG2,
        					// "COMMITMENT_REQ_YES", "COMMITMENT_REQ_NO");
        
        					// Manually triger the calc 60 (DocumentDueDate)
        					//  Should be some other way to do this !!! -- Just a quick
        					// and dirty fix !!!
        					//    by BILLY 25July2001
        					ChangeMapValue changeMapValue = (ChangeMapValue) (deal
        							.getChangeValueMap().get("returnDate"));
        
        					if (changeMapValue != null) {
        						// Set the changed flag to force re-Calc of
        						// DocumentDueDate (Calc60)
        						changeMapValue.setChangeFlag(true);
        					}
        
        					////logger.debug("DTH@addCondForApprovalOutstandingLetter::ReturnDate:
        					// " + deal.getReturnDate());
        
        					dcm.inputEntity(deal);
        					doCalculation(dcm);
        					srk1.commitTransaction();
				        } catch(Exception e) {
				            srk1.cleanTransaction();
				    }
				} //// end if
				srk1.freeResources();
                return;
			} //// end for
		} catch (Exception e) {
			logger.error("Exception @DTH::handleSubmitStandard:addCondForApprovalOutstandingLetter - ignored");
			logger.error(e);
		}
	}

	//// 1. All changed conditions via DocTrackingDetails screen (changed, not
	// visited!!),
	//// should appear in the Conditions of Approval Outstanding Letter.
	protected void updateCondForApprovalOutstandingLetter(PageEntry pg,
			Hashtable pst, String tiledName) {
		try {
			Vector docTrackingIdsFromDetails = theSessionState.getDocTrackingIds();

			////if (docTrackingIdsFromDetails != null)
			////logger.debug("DTH@updateCondForApprovalOutstandingLetter::Size:
			// " + docTrackingIdsFromDetails.size());

			// do nothing
			if (docTrackingIdsFromDetails == null)
				return;

			// do nothing
			if (docTrackingIdsFromDetails != null
					&& docTrackingIdsFromDetails.size() <= 0)
				return;

			// Preparation for the common data
			Deal deal = new Deal(srk, null);
			deal = deal.findByPrimaryKey(new DealPK(pg.getPageDealId(), pg
					.getPageDealCID()));

			int sourceOfBusinessCategory = deal.getSourceOfBusinessProfileId();
			int theDealSpecialFeatureId = deal.getSpecialFeatureId();
			int applicationId = com.iplanet.jato.util.TypeConverter.asInt(pg
					.getPageDealApplicationId().trim());
			////logger.debug("DTH@handleSubmitStandard::ApplicationId: " +
			// applicationId);

			Vector passedToDetailsIds = (Vector) pst.get("DOCDETAILS");
			////logger.debug("DTH@handleSubmitStandard::PassedToDetailsIdsSize:
			// " + passedToDetailsIds.size());
			
			for (int i = 0; i < docTrackingIdsFromDetails.size(); i++) {
				int passedHdId = ((Integer) docTrackingIdsFromDetails.get(i))
						.intValue();
				//FXP26410: null check has to be performed before 
				//looping through passedToDetailsIds.
				if(null != passedToDetailsIds) {
					for (int j = 0; j < passedToDetailsIds.size(); j++) {
						DocumentWrapper dw = (DocumentWrapper) passedToDetailsIds.get(j);
						////logger.debug("DTH@updateCondForApprovalOutstandingLetter::DocIdForDetails:
						// " + dw.getTiledIndexForDocId());
	
						if (passedHdId == dw.getDocTrackingId()) {
							int tiledIdx = dw.getTiledIndexForDocId();
	
							TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(
									tiledName);
							tiledView.setTileIndex(tiledIdx);
	
							////New requirement -- if Deal.status = CommitOffered
							// (13) or ConditionOut (16)
							//// Then update the DocumentDuedate and Status as we
							// does when
							//// Approving the deal.
							CalcMonitor dcm = CalcMonitor.getMonitor(srk);
							deal.setCalcMonitor(dcm);
							int theDealStatusId = deal.getStatusId();
	
							logger.debug("DTH@updateCondForApprovalOutstandingLetter::Row[ "
									+ tiledIdx+ "]:, DocTrackinIdDetailsScreenChanged: "
									+ passedHdId+ ", DealStatusId: "+ theDealStatusId);
	
							//// From DocTracking Page system should create the
							// Commitment letter and
							//// the returnDate for the a deal with the status in
							// the range:
							//// greater than DEAL_PRE_APPROVAL_OFFERED (11)
							//// and less than
							// DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED (19)!!
							if (theDealStatusId == Mc.DEAL_COMMIT_OFFERED
									|| theDealStatusId == Mc.DEAL_PRE_APPROVAL_OFFERED
									|| theDealStatusId == Mc.DEAL_COMMIT_OFFERED
									|| theDealStatusId == Mc.DEAL_OFFER_ACCEPTED
									|| theDealStatusId == Mc.DEAL_PRE_APPROVAL_FIRM
									|| (theDealStatusId == Mc.DEAL_CONDITIONS_OUT && theDealSpecialFeatureId != 1)
									|| theDealStatusId == Mc.DEAL_CONDITIONS_SATISFIED
									|| theDealStatusId == Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT
									|| theDealStatusId == Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED
									|| theDealStatusId == Mc.DEAL_POST_DATED_FUNDING
									|| theDealStatusId == Mc.DEAL_FUNDING_REVERSED) {
							    try {
	    							// update document tracking conditions
	    	                        srk.beginTransaction();
	    							ConditionHandler cd = new ConditionHandler(srk);
	    							cd.updateForPDecision(deal);
	    
	    							// Manually triger the calc 60 (DocumentDueDate)
	    							//  Should be some other way to do this !!! -- Just a
	    							// quick and dirty fix !!!
	    							//    by BILLY 25July2001
	    							ChangeMapValue changeMapValue = (ChangeMapValue) (deal
	    									.getChangeValueMap().get("returnDate"));
	    
	    							if (changeMapValue != null) {
	    								// Set the changed flag to force re-Calc of
	    								// DocumentDueDate (Calc60)
	    								changeMapValue.setChangeFlag(true);
	    							}
	    
	    							////logger.debug("DTH@handleSubmitStandard::ReturnDate:
	    							// " + deal.getReturnDate());
	    
	    							dcm.inputEntity(deal);
	    							doCalculation(dcm);
	    
	    							srk.commitTransaction();
							    } catch (Exception e) {
							        srk.cleanTransaction();
							    }
	
								//// Setup the pageCondition7 to produce the
								// CommitmentLetter.
								pg.setPageCondition7(true);
							} //// end if
						} //// end if equal ids.
					} //// end for
				} //End of if for FXP26410.
			} //// end for

			//// Nullify the DocTracking IDs formed in the DocTrackingDetails
			//// screen during the previous visit.
			docTrackingIdsFromDetails.clear();
			theSessionState.setDocTrackingIds(docTrackingIdsFromDetails);

			////theSessionState.setDocTrackingIds(null);
		} catch (Exception e) {
			logger.error("Exception @DocumentTrackingHandler.handleSubmitStandard:ChangedConditions - ignored");
			logger.error(e);
		}
	}

	//--DJ_DT_CR--start--//
	public void handleAdditionalDJDocs() {
		logger.trace("DocTrackingHandler@handleAdditionalDJDocs");

		PageEntry pg = theSessionState.getCurrentPage();
		SessionResourceKit srk = getSessionResourceKit();
		Hashtable pst = pg.getPageStateTable();
		StatusInfo si = (StatusInfo) pst.get(STATUS_INFO);

		try {
			Deal deal = new Deal(srk, null);
			deal = deal.findByPrimaryKey(new DealPK(pg.getPageDealId(), 
					pg.getPageDealCID()));

			logger.debug("handleAdditionalDJDocs: TickboxValue: "
					+ getCurrNDPage().getDisplayFieldValue(
					"ckbDJProduceCommitment").toString());

			//I. Produce DJ documents for special dealStatuseIds and with all
			// Conditions Approved.
			if ((si.dealStatusId == Mc.DEAL_COMMIT_OFFERED
					|| si.dealStatusId == Mc.DEAL_OFFER_ACCEPTED
					|| si.dealStatusId == Mc.DEAL_PRE_APPROVAL_FIRM || si.dealStatusId == Mc.DEAL_CONDITIONS_OUT)
					&& si.allApproved == true) {
				// it is about to generate docs ... dialog to inform user on it
				// ...
				//// Product wants not to overload user with messaging.
				/**
				 * setActiveMessageToAlert(BXResources.getSysMsg("DOCUMENT_TRACKING_CAISSE_GM_GENERATE",
				 * theSessionState.getLanguageId()),
				 * ActiveMsgFactory.ISCUSTOMDIALOG, new String());
				 */

				try {
					srk.beginTransaction();
					standardAdoptTxCopy(pg, true);
					setDealStatus(pg, srk, si);

					//// Place requests for #47 (63) with attachments: 119(72)
					// and 120(73).
					//logger.debug("VLAD ===> TEST_Before :: Before send the Legal Documents with all Conditions Approved!!");
					//DocumentRequest.requestTransferredMtgFile(srk, deal);
					//DocumentRequest.requestCreditApplication(srk, deal);
					//DocumentRequest.requestCreditAnalysisForm(srk, deal);
					//// New request from Product on Conditions satisfied to
					// send the documents
					//// 1. new 1047 bundle wrappes up the docs: #120 + #119
					//// 2. new 1053 bundle wrappes up the docs: #114 + #115 +
					// #116
					//// 3. #47 itself
					//// 4. #53 itself
					DocumentRequest.requestTransferredMtgFileBundle(srk, deal);
					DocumentRequest.requestMtgFinancingApprovalAndOnBundle(srk,deal);
					DocumentRequest.requestTransferredMtgFile(srk, deal);
					DocumentRequest.requestMtgFinancingApproval(srk, deal);
					//logger.debug("VLAD ===> TEST_After :: Sent the Legal Documents with all Conditions Approved!!");

					//// DJ currently want to see two more docs generated
					// unconditionally:
					//// #40 (without attachments) and #61.
					//logger.debug("VLAD ===> TEST_Before :: Before send the Financial Documents with all Conditions Approved!!");
					DocumentRequest.requestUnConditionalMtgApproval(srk, deal);
					DocumentRequest.requestOfferToFinanceLetterCaisse(srk, deal);
					//logger.debug("VLAD ===> TEST_After :: Sent the Financial Documents with all Conditions Approved!!");

					// New change request from DJ: generate the
					// DealSummaryLiteDJ except #119.
					// Note that #119 is not a part of 1047 bundle.
					//logger.debug("VLAD ===> TEST_Before :: Before send Generic Documents with all Conditions Approved!!");
					DocumentRequest.requestDealSummaryDJOnApprove(srk, deal);
					//logger.debug("VLAD ===> TEST_After :: Sent the Generic Documents with all Conditions Approved!!");

					workflowTrigger(2, srk, pg, null);
					srk.commitTransaction();
				} catch (Exception e) {
					srk.cleanTransaction();

					logger.error("Exception @DocTrackingHandler::handleAdditionalDJDocs");
					logger.error(e);

					setStandardFailMessage();
					return;
				}
			} // end produce always if

			//II. Produce some of documents documents under certain
			// circumstances.
			//// If the DJ new tickbox is checked Place request for #53(65),
			// #61(70), and 40(62)
			//// unconditionally (the rest is done by BREngine. These documents
			// should be
			//// produced only for the SOBCategoryId = 11 (Sales Force).
			else if (getCurrNDPage().getDisplayFieldValue("ckbDJProduceCommitment")
					.toString().equals("T") && si.allApproved != true) {

				try {
					srk.beginTransaction();
					standardAdoptTxCopy(pg, true);

					logger.debug("handleAdditionalDJDocs: Before send the documents driven by TickBox!!");
					////DocumentRequest.requestMtgFinancingApproval(srk, deal);
					// should be requestMtgFinancingApprovalAndOn(...).
					//// New request from Product to send the documents wrapped
					// up into the bundles:
					//// new 1053 bundle is:
					//// #53 (Cover Letter) + #114 + #115 + #116.
					//// DJ canceled this new requirement. The call stays here
					// until the end of UAT.
					//// They currently want to see only two docs generated:
					// #40 (without attachments) and #61.
					////DocumentRequest.requestMtgFinancingApprovalAndOnBundle(srk,
					// deal);

					DocumentRequest.requestUnConditionalMtgApproval(srk, deal);
					DocumentRequest.requestOfferToFinanceLetterCaisse(srk, deal);
					logger.debug("handleAdditionalDJDocs: After send the documents driven by TickBox!!");

					workflowTrigger(2, srk, pg, null);
					srk.commitTransaction();
				} catch (Exception e) {
					srk.cleanTransaction();

					logger.error("Exception @DocTrackingHandler::handleAdditionalDJDocs");
					logger.error(e);

					setStandardFailMessage();
					return;
				}
			} // end else if
		} // end outer try
		catch (Exception e) {
			srk.cleanTransaction();
			logger.error("Exception @DoctrackingHandler.handleAdditionalDJDocs");
			logger.error(e);
			navigateAwayFromFromPage(true);
		}
	}

	//--DJ_DT_CR--end--//

	//// The method below with the indentical one from the DealHandlerCommon
	//// should be moved to the PageHandlerCommon class
	/**
	 * 
	 *  
	 */
	public int getTotalNoOfRowsForThisPageVar(String pageVar) throws Exception {
		try {
			if ((pageVar == null) || (pageVar.equals("")))
				throw new Exception("Invalid Page Variable Requested");

			int tileCounter = 0;

			String[] repFields = new String[2];
			StringTokenizer rep = new StringTokenizer(pageVar, "/");

			repFields[0] = rep.nextToken(); // repeatable Name
			repFields[1] = rep.nextToken(); // repeatable FieldName

			//logger.debug("DHC@getTotalNoOfRowsForThisPageVar::repFields[0]: "
			// + repFields[0]);
			//logger.debug("DHC@getTotalNoOfRowsForThisPageVar::repFields[1]: "
			// + repFields[1]);

			if (repFields[0] == null || repFields[0].trim().equals("")
					|| repFields[1] == null || repFields[1].trim().equals("")) {
				return -1;
			}

			TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(
					repFields[0]);

			//--> Should try to avoid to use nextTile call. Because this will
			// cause the NextTile event
			//--> executed multiple time and will lost some input value as well
			//--> Changed by BILLY 06Aug2002
			//tiledView.resetTileIndex();
			//while(tiledView.nextTile())
			//logger.debug("DHC@getTotalNoOfRowsForThisPageVar::tiledView.getNumTiles()="
			// + tiledView.getNumTiles());
			while (tileCounter < tiledView.getNumTiles()) {
				tiledView.setTileIndex(tileCounter);
				tileCounter++;
				//logger.debug("DHC@getTotalNoOfRowsForThisPageVar::tileCounter:
				// " + tileCounter + " The FieldValue = " +
				// tiledView.getDisplayFieldValue(repFields[1]));

			}
			//--> The above is for debug purpose only. Will be removed later.
			//==============================================================

			/*
			 * if (filedValueList == null) { throw new Exception("Page Display
			 * Field no found (no such)"); } if (filedValueList instanceof
			 * Vector) { return((Vector) filedValueList).size(); }
			 */
			return tileCounter;
		} catch (Exception ex) {
			logger.error("Exception @getTotalNoOfRowsForThisPageVar: DspFldName = <"
					+ pageVar + ">");
			logger.error(ex);
		}

		return -1;

	}

	public int getTileIndexForThisPageVarHiddenId(String pageVar, int Id)
			throws Exception {
		try {
			if ((pageVar == null) || (pageVar.equals("")))
				throw new Exception("Invalid Page Variable Requested");

			int tileCounter = 0;

			String[] repFields = new String[2];
			StringTokenizer rep = new StringTokenizer(pageVar, "/");

			repFields[0] = rep.nextToken(); // repeatable Name
			repFields[1] = rep.nextToken(); // repeatable FieldName

			////logger.debug("DTH@getTileIndexForThisPageVarHiddenId::repFields[0]:
			// " + repFields[0]);
			////logger.debug("DTH@getTileIndexForThisPageVarHiddenId::repFields[1]:
			// " + repFields[1]);

			if (repFields[0] == null || repFields[0].trim().equals("")
					|| repFields[1] == null || repFields[1].trim().equals("")) {
				return -1;
			}

			TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(
					repFields[0]);

			//--> Should try to avoid to use nextTile call. Because this will
			// cause the NextTile event
			//--> executed multiple time and will lost some input value as well
			//--> Changed by BILLY 06Aug2002
			while (tileCounter < tiledView.getNumTiles()) {
				if (tileCounter == Id) {
					tileCounter = Id;
					tiledView.setTileIndex(tileCounter);
					logger.debug("DTH@getTileIndexForThisPageVarHiddenId::Id: "+ Id);
					logger.debug("DTH@getTileIndexForThisPageVarHiddenId::tileCounter: "
							+ tileCounter+ " The FieldValue = "+ tiledView
							.getDisplayFieldValue(repFields[1]));

					return tileCounter;
				}

				tileCounter++;
			}
			////return tileCounter;
		} catch (Exception ex) {
			logger.error("Exception DTH@getTileIndexForThisPageVarHiddenId DspFldName = <"
					+ pageVar + ">");
			logger.error(ex);
		}

		logger.debug("DTH@getTileIndexForThisPageVarHiddenId::TileCounter equals -1");
		return -1;

	}

	//    This is answer 'Yes' to the CustomDialog - yes to deny deal!

	public void handleCustomActMessageOk(String[] args) {
		logger.trace("DocTrackingHandler@handleCustomActMessageOk:");

		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		StatusInfo si = (StatusInfo) pst.get(STATUS_INFO);
		SessionResourceKit srk = getSessionResourceKit();

		//--CervusPhaseII--start--//
		ViewBean thePage = getCurrNDPage();

		if (args[0].equals(OVERRIDE_DEAL_LOCK_YES)) {
			logger.trace("DTH@handleCustomActMessageOk_REJECT_COND_YES: Override deal lock");
			provideOverrideDealLockActivity(pg, srk, thePage);
			return;
		}

		if (args[0].equals(OVERRIDE_DEAL_LOCK_NO)) {
			logger.trace("DTH@handleCustomActMessageOk_REJECT_COND_NO: Return to previous screen");

			theSessionState.setActMessage(null);
			theSessionState.overrideDealLock(true);
			handleCancelStandard(false);

			return;
		}

		if (args[0].equals(REJECT_COND_YES)) {
			logger.trace("DTH@handleCustomActMessageOk_REJECT_COND_YES: proceed to deny deal on condition denied");
			//Set PageCondition4 to indicate user selected Rejected on some
			// DocTracking entries
			// - By Billy 24June2002
			pg.setPageCondition4(true);

			try {
				srk.beginTransaction();
				standardAdoptTxCopy(pg, true);
				setDealStatus(pg, srk, si);

				//moved from the line before srk.commit...
				workflowTrigger(2, srk, pg, null);

				logger.debug("handleCustomActMessageOk: b4 Sent the requestDeclinedMtgLetter Letter and attachmnts!!");
				if (this.isDJClient() ) {
					Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg
							.getPageDealCID());
					if(deal.getStatusId() == Mc.DEAL_DENIED)
						DocumentRequest.requestDeclinedMtgLetter(srk, deal);
					else
						DocumentRequest.requestRateBonusMtgLetter(srk, deal);
					logger.debug("handleCustomActMessageOk:After Sent the requestDeclinedMtgLetter Letter and attachmnts!!");
				}

                //insert ConditionUpdateRequest - 4.2GR
                insertConditionUpdateRequest(pg);
                
				srk.commitTransaction();
				navigateToNextPage(true);
				return;
			} catch (Exception e) {
				srk.cleanTransaction();

				logger.error("Exception @DocTrackingHandle::handleSubmitStandard:DenyDeal");
				logger.error(e);

				setStandardFailMessage();
				return;
			} // end inner try

		} else if (args[0].equals(REJECT_COND_NO)) {
			logger.trace("DTH@handleCustomActMessageOk_REJECT_COND_NO: Return to DocTracking screen");
			handleCancelStandard(true);

			return;
		}

		else if (args[0].equals(COMMITMENT_REQ_YES)) {
			try {
				logger.trace("DTH@handleCustomActMessageOk::User requested to send the Commitment Letter and Broker Upload");

				// User requested to send the Commitment Letter and Broker
				// Upload
				CCM ccm = new CCM(srk);
				Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());
				srk.beginTransaction();

				//// Should be "Y"? Test this in E2E environment.
				deal.setCommitmentProduced("N");
				////deal.setCommitmentProduced("Y");
				ccm.requestCommitmentDoc(srk, deal, false);
				ccm.requestElectronicResponseToBroker(srk, deal);
				deal.ejbStore();

				standardAdoptTxCopy(pg, true);
				deal = setDealStatus(pg, srk, si);
				requestPecEmail(srk, deal);		//#DG582  
				workflowTrigger(2, srk, pg, null);
				
                //insert ConditionUpdateRequest - 4.2GR
                insertConditionUpdateRequest(pg);
                
				srk.commitTransaction();
				navigateToNextPage(true);
				return;

			} catch (Exception e) {
				srk.cleanTransaction();
				logger.error("Exception encountered @DocumentTrackingHandler.handleCustomActMessageOk() - while requesting for Commitment documents.");
				logger.error(e);
				setStandardFailMessage();
			}
			return;
		}

		else if (args[0].equals(COMMITMENT_REQ_NO)) {
			logger.trace("DTH@handleCustomActMessageOk::Commitment Letter and Broker Upload not required");

			// Commitment Letter and Broker Upload not required
			try {
				srk.beginTransaction();
				//// Update db with the screen changes.
				standardAdoptTxCopy(pg, true);
				//#DG582 retrieve deal for this copy
				Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());	 
				requestPecEmail(srk, deal);		
				//#DG582 end 
				
                //insert ConditionUpdateRequest - 4.2GR
				// even user says no for commitment, still condition status may be updated 
                insertConditionUpdateRequest(pg);

				navigateToNextPage();
				srk.commitTransaction();
			} catch (Exception e) {
				// Shouldn't happen !!
				srk.cleanTransaction();
				logger.error("Exception encountered @DocumentTrackingHandler.handleCustomActMessageOk() - ignore.");
				logger.error(e);
			}
			return;
		}
		StringTokenizer tk = new StringTokenizer(args[0], "|");

		String sourceName = tk.nextToken();

		int pageRowNdx = com.iplanet.jato.util.TypeConverter.asInt(tk
				.nextToken());

		if (sourceName.equals("btStdDeleteCondition")) {
			deleteStandardCondition(pageRowNdx);
			//// Reload the page
			theSessionState.getCurrentPage().setLoadTarget(TARGET_CONREVIEW_STD);
			return;
		}
		if (sourceName.equals("btCusDeleteCondition")) {
			deleteCustomCondition(pageRowNdx);
			//// Reload the page
			theSessionState.getCurrentPage().setLoadTarget(
					TARGET_CONREVIEW_CUST);
			return;
		}
		return;
	}

	/**
	 * 
	 *  
	 */
	private int validateDocumentTracking(PageEntry pg, SessionResourceKit srk)
	throws Exception {
		BusinessRuleExecutor brExec = new BusinessRuleExecutor();

		PassiveMessage pm = new PassiveMessage();
		brExec.BREValidator(theSessionState, pg, srk, pm, "DT-%");

		if (pm == null)
			return 0;

		boolean anyMessages = pm.getNumMessages() > 0;

		boolean criticalDataProblems = pm.getCritical();

		//--> Should display Error Messages even if Critical Problem occured
		//--> by Billy 09Jan2003
		//if (anyMessages == true && criticalDataProblems == false)
		if (anyMessages == true) {
			pm.setGenerate(true);
			theSessionState.setPasMessage(pm);
		}
		if (criticalDataProblems == true) {
			setActiveMessageToAlert(BXResources.getSysMsg(
					BUSINESS_RULE_CRITICAL, theSessionState.getLanguageId()),
					ActiveMsgFactory.ISCUSTOMCONFIRM);
			return -1;
		}

		return 0;

	}

	/**
	 * 
	 *  
	 */
	private boolean passAAG(PageEntry pg, SessionResourceKit srk) {
		try {
			BusinessRuleExecutor brExec = new BusinessRuleExecutor();
			PassiveMessage pm = brExec.BREValidator(theSessionState, pg, srk,
					null, "AAG-%");
			if (pm == null)
				return false;
			boolean anyMessages = pm.getNumMessages() > 0;
			boolean criticalDataProblems = pm.getCritical();
			if (anyMessages == true && criticalDataProblems == false)
				return false;
			if (criticalDataProblems == true) {
				setActiveMessageToAlert(BXResources.getSysMsg(
						BUSINESS_RULE_CRITICAL, theSessionState
						.getLanguageId()),
						ActiveMsgFactory.ISCUSTOMCONFIRM);
				return false;
			}
			return true;
		} catch (Exception e) {
			logger.error("Exception @DocTrackingHandler.passAAG()");
			logger.error(e);
			setStandardFailMessage();
		}

		return false;

	}

	/**
	 * @version 1.1 <br>
	 *          Date: 06/06/006 <br>
	 *          Author: NBC/PP Implementation Team <br>
	 *          Changed the return type of this method from void to Deal object
	 *          <br>
	 *          Change is required for Pec email <br>
	 */
	private Deal setDealStatus(PageEntry pg, SessionResourceKit srk,
			StatusInfo si) throws Exception {
		Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

		boolean set = false;

		//log to deal history
		DealHistoryLogger hLog = DealHistoryLogger.getInstance(srk);

		String msg = hLog.statusChange(deal.getStatusId(), "DocTracking");

		logger.debug("DTH@setDealStatus: stamp1: " + si.dealStatusSet);
		logger.debug("DTH@setDealStatus: stamp1: " + si.dealStatusId);

		if (si.dealStatusId == Mc.DEAL_DENIED) {
			logger.debug("DTH@setDealStatus: stamp2: Deal Denied mode");

			CCM ccm = new CCM(srk);
			ccm.denyDeal(deal, 0);
			//--> Ticket#371 :: Change for Cervus :: Send Upload when Deny
			//--> By Billy 15July2004
			ccm.sendServicingUpload(srk, deal, Mc.SERVICING_UPLOAD_TYPE_DENY,
					theSessionState.getLanguageId());
			//================================================================
			set = true;
		} else if (si.allApproved == true) {
			logger.debug("DTH@setDealStatus: stamp3: Deal Not Denied mode");
			logger.debug("DTH@setDealStatus: stamp3: " + si.dealStatusSet);
			logger.debug("DTH@setDealStatus: stamp3: " + si.dealStatusId);

			if (deal.getStatusId() == Mc.DEAL_CONDITIONS_OUT
					|| deal.getStatusId() == Mc.DEAL_APPROVED
					|| deal.getStatusId() == Mc.DEAL_COMMIT_OFFERED) {
				//// SYNCADD.
				//Check if necessary to set CommitmentAcceptDate -- By Billy
				// 12June2002
				if (deal.getCommitmentAcceptDate() == null)
					deal.setCommitmentAcceptDate(new java.util.Date());
				////=====================================================================

				deal.setStatusId(Mc.DEAL_CONDITIONS_SATISFIED);
				set = true;
			} else if (deal.getStatusId() == Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT) {
				//// SYNCADD.
				//Check if necessary to set CommitmentAcceptDate -- By Billy
				// 12June2002
				if (deal.getCommitmentAcceptDate() == null)
					deal.setCommitmentAcceptDate(new java.util.Date());
				////=====================================================================

				deal.setStatusId(Mc.DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED);
				set = true;
			}
		} else if (si.dealStatusId == Mc.DEAL_CONDITIONS_OUT
				&& si.dealStatusSet)
			//for the Signed Commitment
		{
			// Bug fix -- if status > DEAL_CONDITIONS_OUT then don't set it --
			// By BILLY 29Nov2001
			if (deal.getStatusId() < Mc.DEAL_CONDITIONS_OUT) {
				deal.setStatusId(Mc.DEAL_CONDITIONS_OUT);
				deal.setCommitmentAcceptDate(new java.util.Date());
				set = true;
			}
		}

		if (set) {
			deal.setStatusDate(new java.util.Date());
			deal.ejbStore();
			hLog.log(deal.getDealId(), deal.getCopyId(), msg,
					Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId());
		}

		// Addition:Check if need to produce Broker Condition Update Doc. --
		// Billy 14March2002
		////if
		// (getCurrNDPage().getDisplayFieldStringValue("ckProduceCondUpdateDoc").equals("T"))
		////String produceCondUpdate =
		// (String)(getCurrNDPage().getDisplayFieldValue("ckProduceCondUpdateDoc"));
		////if (produceCondUpdate!=null &&
		// produceCondUpdate.trim().equals("T"))

		logger.debug("DTH@setDealStatus::CheckBoxValue:"
				+ getCurrNDPage().getDisplayFieldValue("ckProduceCondUpdateDoc")
						.toString());

		if (getCurrNDPage().getDisplayFieldValue("ckProduceCondUpdateDoc")
				.toString().equals("T")) {
			DocumentRequest.requestConditionsOutstanding(srk, deal);
			//--Ticket#582--start--07Sep--2004--//
			// Record in History Table should be produced whenever Broker
			// Outstanding Condition doc is sent.
			// Users just wanted to track the fact of producing of the document
			// without extra details.
			// TransactionText field on DealHistory screen is not translated
			// anyway on language toggle
			// thus no new method created in DealHistoryLogger class. The
			// message will go either in
			// English or in French based on the SessionLanguageId of a user.
			DealHistoryLogger hLogBroker = DealHistoryLogger.getInstance(srk);
			String msgbroker = BXResources.getSysMsg(
					"BROKER_OUTSTANDING_CONDITIONS_PRODUCED", theSessionState
					.getLanguageId());

			hLogBroker.log(deal.getDealId(), deal.getCopyId(), msgbroker,
					Mc.DEAL_TX_TYPE_EVENT, srk.getExpressState().getUserProfileId(), deal
					.getStatusId());
			//--Ticket#582--end--07Sep--2004--//
		}
		// ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
		return deal;
		// ***** Change by NBC Impl. Team - Version 1.1 - End *****// 
	}

	// Added Funder Conditions handle -- By BILLY 14Jan2002
	public void saveData(PageEntry pg, boolean calledInTransaction) {
		logger.trace("DocTrackingHandler@saveData");

		try {
			SessionResourceKit srk = getSessionResourceKit();
			PageCursorInfo tds1 = getPageCursorInfo(pg, "Repeated1");
			PageCursorInfo tds2 = getPageCursorInfo(pg, "Repeated2");
			PageCursorInfo tds3 = getPageCursorInfo(pg, "Repeated3");

			//--Release2.1--TD_DTS_CR--//
			PageCursorInfo tds4 = getPageCursorInfo(pg, "Repeated4");
			PageCursorInfo tds5 = getPageCursorInfo(pg, "Repeated5");

			StatusInfo si = new StatusInfo();
			Hashtable pst = pg.getPageStateTable();
			pst.put(STATUS_INFO, si);
			if (tds1.getTotalRows() == 0 && tds2.getTotalRows() == 0
					&& tds3.getTotalRows() == 0)
				return;

			////srk.beginTransaction();
			if (!calledInTransaction)
				srk.beginTransaction();

			updateDocTrackStatus(pg, srk, SECTION_SIGNED_COMMITMENT, si);
			logger.debug("DTH@saveData_AfterSectionSignedCommitment");

			updateDocTrackStatus(pg, srk, SECTION_ORDERED, si);
			logger.debug("DTH@saveData_AfterSectionOrdered");

			//--Release2.1--TD_DTS_CR--start//
			updateDocTrackStatus(pg, srk, SECTION_STANDARD_CONDITION, si);
			logger.debug("DTH@saveData_AfterSectionStandardCondition");

			updateDocTrackStatus(pg, srk, SECTION_CUSTOM_CONDITION, si);
			logger.debug("DTH@saveData_AfterSectionCustomCondition");
			//--Release2.1--TD_DTS_CR--end//

			//// This should be recreated as as separate FUNDER update because
			// of
			//// very different implementation of combobox and checkbox objects
			// in JATO.
			updateDocTrackStatusFunder(pg, srk, SECTION_FUNDER, si);
			logger.debug("DTH@saveData_AfterSectionFunderCondition");

			////srk.commitTransaction();
			////if (srk.getModified() == true) pg.setModified(true);

			updateFreeFormCondition(pg, srk);

			// <!-- Catherine, 7-Apr-05, pvcs#817 -->

			String pgText = ((String) getCurrNDPage().getDisplayFieldValue(
			"tbConditionNoteText")).toString();
			// this is a trick to make setupBeforePageGeneration() read page
			// state value in case when that value is blank
			if (pgText.equals("")) {
				pgText = " ";
			}
			pg.setPageState1(pgText);

			// <!-- Catherine, 7-Apr-05, pvcs#817 end -->

			pg.setModified(true);
			if (!calledInTransaction) {
				srk.commitTransaction();
			}
		} catch (Exception e) {
			srk.cleanTransaction();
			logger.error("Exception @DocTrackingHandler.SaveData");
			logger.error(e);
			return;
		}

	}

	private void updateDocTrackStatusFunder(PageEntry pg,
			SessionResourceKit srk, int sectionNumber, StatusInfo si)
	throws Exception {
		int docTrackId = 0;

		int copyId = 0;

		int statusId = 0;

		Vector hdDocTrackId = null;

		Vector hdCopyId = null;

		hdDocTrackId = (Vector) getRepeatedFieldValues("Repeated3/hdFunderDocTrackId");
		logger.debug("DTH@updateDocTrackStatusFunder::DocTrackIdSize: "
				+ hdDocTrackId.size());

		hdCopyId = (Vector) getRepeatedFieldValues("Repeated3/hdFunderCopyId");
		logger.debug("DTH@updateDocTrackStatusFunder::hdCopyIdSize: "
				+ hdCopyId.size());

		// If the Approved CheckBox checked == APPROVED, otherwise ==> REQUESTED
		////CheckBox chStatus = (CheckBox)
		// getCurrNDPage().getDisplayFieldValue("Repeated3/ckFunderDocStatus");
		Vector chStatus = (Vector) getRepeatedFieldValues("Repeated3/ckFunderDocStatus");
		Vector chStatusChecked = (Vector) getSelectedRowsFromCheckBoxes("Repeated3/ckFunderDocStatus");
		logger.debug("DTH@updateDocTrackStatusFunder::chStatusCheckedSize: "
				+ chStatusChecked.size());

		//// temp debug.
		for (int k = 0; k < chStatusChecked.size(); k++) {
			logger.debug("DTH@updateDocTrackStatusFunder::SelectedRow: "
					+ (new Integer(chStatusChecked.elementAt(k).toString())).intValue());
		}

		int vectorSize = ((Vector) hdDocTrackId).size();
		logger.debug("DTH@updateDocTrackStatusFunder::HdVectorSize: "
				+ vectorSize);
		//String checkBoxName = "Repeated3/ckFunderDocStatus";

		for (int i = 0; i < vectorSize; i++) {
			if (hdDocTrackId.elementAt(i) != null
					&& hdCopyId.elementAt(i) != null) {
				docTrackId = (new Integer(hdDocTrackId.elementAt(i).toString()))
						.intValue();
				//logger.debug("DTH@updateDocTrackStatusFunder::DocTrackId: " +
				// docTrackId);

				copyId = (new Integer(hdCopyId.elementAt(i).toString())).intValue();

				DocumentTracking docTrack = new DocumentTracking(srk,
						docTrackId, copyId);
				//logger.debug("DTH@updateDocTrackStatusFunder::DocumentStatusId:
				// " + docTrack.getDocumentStatusId());

				for (int k = 0; k < chStatusChecked.size(); k++) {
					int indx = (new Integer(chStatusChecked.elementAt(k).toString()))
							.intValue();
					if (i == indx) {
						statusId = Mc.DOC_STATUS_APPROVED; // Approved
						logger.debug("DTH@UpdateStatusFunder_Approved::DocTrackId: "
								+ docTrackId+ ", DocumentStatusId: "
								+ docTrack.getDocumentStatusId()
								+ ", with tileIndx/checkedIndx["+ i+ ", "
								+ indx+ "]:: StatusId: "+ statusId);

						// Update document status to 'Approved' for all checked
						// tickboxes.
						if (statusId != docTrack.getDocumentStatusId()) {
							docTrack.setDocumentStatusId(statusId);
							docTrack.setDStatusChangeDate(new java.util.Date());
							docTrack.ejbStore();
							pg.setModified(true);
						}
					}
					else if (i != (new Integer(chStatusChecked.elementAt(k).toString()))
							.intValue()) {
						statusId = (new Integer(getRepeatedField(
								"Repeated3/hdFunderDocStatus", i))).intValue();
						logger.debug("DTH@UpdateStatusFunder_Non_Approved::DocTrackId: "
								+ docTrackId+ ", DocumentStatusId: "
								+ docTrack.getDocumentStatusId()
								+ " with with tileIndx/checkedIndx["+ i+ ", "+ indx
								+ "]:: StatusId: "+ statusId);

						// Do nothing since there are only two states for Funder
						// Conditions: 'Approved' --> Checked and
						// 'Any Other' --> not checked.
					}
				}

				getDealStatus(pg, sectionNumber, statusId, si);

			}
		}
	}

	private Vector getIntersection(Vector fullSet, Vector checkedSubset) {
		Vector retInxs = new Vector();

		for (int i = 0; i < fullSet.size(); i++) {
			for (int j = 0; j < checkedSubset.size(); j++) {
				if ((new Integer(fullSet.elementAt(i).toString())).intValue() == (new Integer(
						checkedSubset.elementAt(j).toString())).intValue()) {
					logger.debug("DTH@getIntersection::Add: "
							+ (Integer) (fullSet.elementAt(i)));
					retInxs.add((Integer) (fullSet.elementAt(i)));
				}
			}
		}
		return retInxs;
	}

	private Vector getAdjunct(Vector fullSetIndx, Vector interscectionIndx) {
		for (int i = 0; i < fullSetIndx.size(); i++) {
			for (int j = 0; j < interscectionIndx.size(); j++) {
				if (((Integer) fullSetIndx.elementAt(i)).intValue() == ((Integer) interscectionIndx
						.elementAt(j)).intValue()) {
					logger.debug("DTH@getAdjunct::Remove: "
							+ (Integer) (fullSetIndx.elementAt(i)));
					fullSetIndx.remove((Integer) (fullSetIndx.elementAt(i)));
				}
			}
		}

		return fullSetIndx;
	}

	private void updateDocTrackStatus(PageEntry pg, SessionResourceKit srk,
			int sectionNumber, StatusInfo si) throws Exception {
		int docTrackId = 0;

		int copyId = 0;

		int statusId = 0;

		Vector hdDocTrackId = null;

		Vector cbStatus = null;

		Vector hdCopyId = null;

		if (sectionNumber == SECTION_SIGNED_COMMITMENT) {
			hdDocTrackId = (Vector) getRepeatedFieldValues("Repeated1/hdSignDocTrackId");
			////logger.debug("DTH@updateDocTrackStatus::DocTrackIdSize_Rep1: "
			// + hdDocTrackId.size());

			hdCopyId = (Vector) getRepeatedFieldValues("Repeated1/hdSignCopyId");
			////logger.debug("DTH@updateDocTrackStatus::hdCopyIdSize_Rep1: " +
			// hdCopyId.size());

			cbStatus = (Vector) getRepeatedFieldValues("Repeated1/cbSignDocStatus");
			////logger.debug("DTH@updateDocTrackStatus::cbStatus_Rep1: " +
			// cbStatus.size());
		} else if (sectionNumber == this.SECTION_ORDERED) {
			hdDocTrackId = (Vector) getRepeatedFieldValues("Repeated2/hdOrdDocTrackId");
			logger.debug("DTH@updateDocTrackStatus::DocTrackIdSize_Rep2: "
					+ hdDocTrackId.size());

			hdCopyId = (Vector) getRepeatedFieldValues("Repeated2/hdOrdCopyId");
			logger.debug("DTH@updateDocTrackStatus::hdCopyIdSize_Rep2: "
					+ hdCopyId.size());

			cbStatus = (Vector) getRepeatedFieldValues("Repeated2/cbOrdDocStatus");
			logger.debug("DTH@updateDocTrackStatus::cbStatus_Rep2: "
					+ cbStatus.size());
		}
		//--Release2.1--TD_DTS_CR--//
		else if (sectionNumber == this.SECTION_STANDARD_CONDITION) {
			hdDocTrackId = (Vector) getRepeatedFieldValues("Repeated4/hdStdDocTrackId");
			////logger.debug("DTH@updateDocTrackStatus_SectionStd::DocTrackIdSize_Rep4:
			// " + hdDocTrackId.size());

			hdCopyId = (Vector) getRepeatedFieldValues("Repeated4/hdStdCopyId");
			////logger.debug("DTH@updateDocTrackStatus_SectionStd::hdCopyIdSize_Rep4:
			// " + hdCopyId.size());

			cbStatus = (Vector) getRepeatedFieldValues("Repeated4/cbStdAction");
			////logger.debug("DTH@updateDocTrackStatus_SectionStd::cbStatus_Rep4:
			// " + cbStatus.size());
		} else if (sectionNumber == this.SECTION_CUSTOM_CONDITION) {
			hdDocTrackId = (Vector) getRepeatedFieldValues("Repeated5/hdCusDocTrackId");
			////logger.debug("DTH@updateDocTrackStatus_SectionCus::DocTrackIdSize_Rep5:
			// " + hdDocTrackId.size());

			hdCopyId = (Vector) getRepeatedFieldValues("Repeated5/hdCusCopyId");
			////logger.debug("DTH@updateDocTrackStatus_SectionCus::hdCopyIdSize_Rep5:
			// " + hdCopyId.size());

			cbStatus = (Vector) getRepeatedFieldValues("Repeated5/cbCusAction");
			////logger.debug("DTH@updateDocTrackStatus_SectionCus::cbStatus_Rep5:
			// " + cbStatus.size());
		}

		// There are multiple row values in this display field
		int vectorSize = ((Vector) hdDocTrackId).size();
		for (int i = 0; i < vectorSize; i++) {
			docTrackId = (new Integer(hdDocTrackId.elementAt(i).toString()))
					.intValue();
			////logger.debug("DTH@updateDocTrackStatus::DocTrackId: " +
			// docTrackId);

			copyId = (new Integer(hdCopyId.elementAt(i).toString())).intValue();
			////logger.debug("DTH@updateDocTrackStatus::CopyId: " + copyId);

			DocumentTracking docTrack = new DocumentTracking(srk, docTrackId,
					copyId);

			//--Release2.1--TD_DTS_CR--//
			//// In order to get the pre-defined combo-option with the
			// "disabled = true" mode.
			if (sectionNumber == this.SECTION_STANDARD_CONDITION
					|| sectionNumber == this.SECTION_CUSTOM_CONDITION) {
				statusId = 0;
				logger.debug("DTH@updateDocTrackStatus::StatusId_hardcoded: "
						+ statusId);
			} else {
				statusId = (new Integer(cbStatus.elementAt(i).toString())).intValue();
				logger.debug("DTH@updateDocTrackStatus::StatusId: " + statusId);
			}

			logger.debug("DTH@updateDocTrackStatus::StatusId: " + statusId);
			logger.debug("DTH@updateDocTrackStatus::DocTrackStatusId: "
					+ docTrack.getDocumentStatusId());

			if (statusId != docTrack.getDocumentStatusId()) {
				//--Ticket#304--start--//
				if (statusId == Sc.DOC_STATUS_REJECTED) {
					logger.debug("DTH@updateDocTrackStatus::BeforeSetActiveMsgToAlert");
					pg.setPageCondition8(true);
					logger.debug("DTH@updateDocTrackStatus::AfterSetActiveMsgToAlert");
				}
				//--Ticket#304--end--//

				docTrack.setDocumentStatusId(statusId);
				docTrack.setDStatusChangeDate(new java.util.Date());
				docTrack.ejbStore();
				pg.setModified(true);
			}

			getDealStatus(pg, sectionNumber, statusId, si);
		}
	}

	/**
	 * 
	 *  
	 */
	private void getDealStatus(PageEntry pg, int section, int docStatusId,
			StatusInfo si) {

		// The Funder Conditions should not affect the DealStatus -- By BILLY
		// 14Jan2002
		if (section == SECTION_FUNDER)
			return;

		if (si.dealStatusId == Sc.DEAL_DENIED)
			return;

		if (docStatusId == Sc.DOC_STATUS_REJECTED) {
			si.dealStatusId = Sc.DEAL_DENIED;
			si.dealStatusSet = true;
			si.allApproved = false;
			return;
		}

		//#DG472 rewritten
		if (docStatusId == Sc.DOC_STATUS_APPROVED || docStatusId == Sc.DOC_STATUS_WAIVED) {
			if (section == SECTION_SIGNED_COMMITMENT) {
				si.dealStatusId = Sc.DEAL_CONDITIONS_OUT;
				si.dealStatusSet = true;
			}
		}
		else
			si.allApproved = false;
	}

	/**
	 * 
	 *  
	 */
	class StatusInfo {
		public int dealStatusId;

		public boolean dealStatusSet;

		public boolean allApproved;

		/**
		 * 
		 *  
		 */
		public StatusInfo() {
			dealStatusId = 0;

			dealStatusSet = false;

			allApproved = true;

		}

	}

	//--Release2.1--TD_DTS_CR--start//
	class DocumentWrapper {
		private String docTrackingName;

		private int docTrackingId;

		private int tiledIndex;

		// "getters" methods.
		public String getDocTrackingName() {
			return docTrackingName;
		}

		public int getDocTrackingId() {
			return docTrackingId;
		}

		public int getTiledIndexForDocId() {
			return tiledIndex;
		}

		// "setters" methods.
		public void setDocTrackingName(String aDocTrackingName) {
			docTrackingName = aDocTrackingName;
		}

		public void setProductId(int aDocTrackingId) {
			docTrackingId = aDocTrackingId;
		}

		public void setTiledIndexForDocId(int aTiledIndex) {
			tiledIndex = aTiledIndex;
		}
	}

	//--Release2.1--TD_DTS_CR--end//

	/**
	 * 
	 *  
	 */
	public void navigateToDetail(int sectionIndex, int rowNdx) {
		logger.trace("DocTrackingHandler@navigateToDetail, rowNdxIn: "
				+ rowNdx);

		String section = null;

		String docTrackIdName = null;

		String hdRoleId = null;

		String hdRequestDate = null;

		String hdStatusDate = null;

		String cbDocStatus = null;

		String hdDocText = null;

		String hdRoleStr = null;

		String hdDocLabel = null;

		String hdCopyId = null;

		try {
			PageEntry pg = theSessionState.getCurrentPage();
			SessionResourceKit srk = getSessionResourceKit();

			if (sectionIndex == SECTION_SIGNED_COMMITMENT) {
				section = "Repeated1";
				docTrackIdName = "Repeated1/hdSignDocTrackId";
				hdRoleId = "Repeated1/hdSignRoleId";
				hdRequestDate = "Repeated1/hdSignRequestDate";
				hdStatusDate = "Repeated1/hdSignStatusDate";
				cbDocStatus = "Repeated1/cbSignDocStatus";
				hdDocText = "Repeated1/hdSignDocText";
				hdRoleStr = "Repeated1/hdSignResponsibility";
				hdDocLabel = "Repeated1/hdSignDocumentLabel";
				hdCopyId = "Repeated1/hdSignCopyId";
			} else if (sectionIndex == this.SECTION_ORDERED) {
				section = "Repeated2";
				docTrackIdName = "Repeated2/hdOrdDocTrackId";
				hdRoleId = "Repeated2/hdOrdRoleId";
				hdRequestDate = "Repeated2/hdOrdRequestDate";
				hdStatusDate = "Repeated2/hdOrdStatusDate";
				cbDocStatus = "Repeated2/cbOrdDocStatus";
				hdDocText = "Repeated2/hdOrdDocText";
				hdRoleStr = "Repeated2/hdOrdResponsibility";
				hdDocLabel = "Repeated2/hdOrdDocumentLabel";
				hdCopyId = "Repeated2/hdOrdCopyId";
			} else if (sectionIndex == this.SECTION_FUNDER) {
				section = "Repeated3";
				docTrackIdName = "Repeated3/hdFunderDocTrackId";
				hdRoleId = "Repeated3/hdFunderRoleId";
				hdRequestDate = "Repeated3/hdFunderRequestDate";
				hdStatusDate = "Repeated3/hdFunderStatusDate";
				cbDocStatus = "Repeated3/hdFunderDocStatus";
				hdDocText = "Repeated3/hdFunderDocText";
				hdRoleStr = "Repeated3/hdFunderResponsibility";
				hdDocLabel = "Repeated3/hdFunderDocumentLabel";
				hdCopyId = "Repeated3/hdFunderCopyId";
			}

			PageCursorInfo tds = getPageCursorInfo(pg, section);
			Hashtable pst = pg.getPageStateTable();

			if (tds.getTotalRows() <= 0)
				return;

			rowNdx = getCursorAbsoluteNdx(tds, rowNdx);

			logger.debug("DTH@navigateToDetail::AbsoluteNdx: " + rowNdx
					+ ", Section: " + section);

			String roleId = getRepeatedField((hdRoleId).toString(), rowNdx)
					.toString();
			logger.debug("DTH@navigateToDetail::RoleId: " + roleId);

			if (roleId == null || roleId.equals(""))
				return;

			PageEntry pgEntry = setupSubPagePageEntry(pg,
					Mc.PGNM_DOC_TRACKING_DETAIL, true);
			Hashtable subPst = pgEntry.getPageStateTable();
			String docTrackId = getRepeatedField((docTrackIdName).toString(),
					rowNdx).toString();
			logger.debug("DTH@navigateToDetail::DocTrackId: " + docTrackId);

			subPst.put("dfDocTrackId", new Integer(getIntValue(docTrackId)));
			subPst.put("dfRoleId", new Integer(getIntValue(roleId)));

			//--Release2.1--TD_DTS_CR--start//
			String fieldStr = "";
			if (hdRequestDate != null) {
				logger.debug("DTH@navigateToDetail::hdRequestDate: "
						+ hdRequestDate);
				if (getRepeatedField((hdRequestDate), rowNdx) != null) {
					fieldStr = getRepeatedField((hdRequestDate).toString(),
							rowNdx).toString();
					logger.debug("DTH@navigateToDetail::FieldStr: " + fieldStr);
				}
			}
			subPst.put("dfRequestDate", fieldStr);

			if (hdStatusDate != null) {
				if (getRepeatedField((hdStatusDate), rowNdx) != null) {
					subPst.put("dfStatusDate", getRepeatedField(
							(hdStatusDate).toString(), rowNdx).toString());
				}
			}

			Vector docDetails = null;
			if (pst.get("DOCDETAILS") == null)
				docDetails = new Vector();
			else
				docDetails = (Vector) pst.get("DOCDETAILS");

			DocumentWrapper dw = new DocumentWrapper();
			dw.setDocTrackingName(docTrackId);
			dw.setTiledIndexForDocId(rowNdx);
			dw.setProductId(new Integer(docTrackId).intValue());

			docDetails.add(dw);

			pst.put("DOCDETAILS", docDetails);
			//--Release2.1--TD_DTS_CR--end//

			int statusId = 0;

			String statusField = getRepeatedField((cbDocStatus).toString(),
					rowNdx).toString();
			////logger.debug("DTH@navigateToDetail::StatusField: " +
			// statusField);

			statusId = new Integer(statusField).intValue();
			////logger.debug("DTH@navigateToDetail::StatusId: " + statusId);

			doDocStatusModelImpl statusDo = (doDocStatusModelImpl) (RequestManager
					.getRequestContext().getModelManager()
					.getModel(doDocStatusModel.class));

			statusDo.clearUserWhereCriteria();
			statusDo.addUserWhereCriterion("dfDocStatusId", "=", new Integer(
					statusId));

			//// Setup current displaying LanguageID
			////statusDo.addUserWhereCriterion("dfLanguagePrederenceId", "=",
			//// new Integer(theSessionState.getLanguageId()));

			////logger.debug("DTH@setupBeforePageGeneration::The SQL : " +
			// statusDo.getSelectSQL());

			String statusLabel = "";

			try {
				ResultSet rs = statusDo.executeSelect(null);

				if (rs == null || statusDo.getSize() <= 0) {
					logger.debug("DTH@navigateToDetail::No row returned!!");
					statusLabel = "";
				} else if (statusDo.getSize() > 0) {
					Object oStatLabel = statusDo.getValue(statusDo.FIELD_DFDOCSTATUSDESC);
					statusLabel = oStatLabel.toString();
					logger.debug("DTH@navigateToDetail::StatLabel: "
							+ oStatLabel.toString());
				}
			} catch (ModelControlException mce) {
				logger.debug("DTH@navigateToDetail::MCEException: " + mce);
			}

			catch (SQLException sqle) {
				logger.debug("DTH@navigateToDetail::SQLException: " + sqle);
			}

			//--Release2.1--TD_DTS_CR--start//
			subPst.put("sectionId", new Integer(sectionIndex));
			//--Release2.1--TD_DTS_CR--end//

			subPst.put("dfDocStatus", statusLabel);

			subPst.put("dfDocText", getRepeatedField((hdDocText).toString(),
					rowNdx).toString());
			subPst.put("dfRoleStr", getRepeatedField((hdRoleStr).toString(),
					rowNdx).toString());
			subPst.put("dfDocLabel", getRepeatedField((hdDocLabel).toString(),
					rowNdx).toString());
			subPst.put("dfCopyId", new Integer(getRepeatedField(
					(hdCopyId).toString(), rowNdx).toString()));

			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		} catch (Exception e) {
			setStandardFailMessage();
			logger.error("Exception @DocumentTrackingHandler.navigateToDetail");
			logger.error(e);
		}
	}

	/**
	 * 
	 *  
	 */
	public void navigateToPartySummary() {
		PageEntry pg = theSessionState.getCurrentPage();

		try {
			PageEntry pgEntry = setupSubPagePageEntry(pg,
					Mc.PGNM_PARTY_SUMMARY, true);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		} catch (Exception e) {
			setStandardFailMessage();
			logger.error("Exception @DocumentTracking.navigateToPartySummary");
			logger.error(e);
		}

	}

	// For a row in repeated populate by-pass field and set options for status
	// dropdowm
	////public void populateDisplayRow(int pageRowNdx, String repeatedName,
	// String doName, int sectionNumber)
	public void populateDisplayRow(int pageRowNdx, String repeatedName,
			Class modelClass, int sectionNumber) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(repeatedName);

		int statusId = 0;

		boolean byPass = false;

		ComboBox statusBox = null;
		//CheckBox checkBox = null;
		//OptionList checkBoxOption = null;
		OptionList statusBoxOption = null;

		// choice box name to populate
		String statusBoxName = null;
		//String checkBoxName = null;
		String byPassFldName = null;
		String byPassHdnName = null;

		// DO data field names (must be common to both data objects)
		//String docStatusName = "dfDocStatusId";
		//String byPassName = "dfByPass";
		//String hdByPass = "hdByPass";

		//--Release2.1--//
		String yes = "Yes"; //// English is default language.
		String no = "No"; //// English is default language.

		try {
			tds.setRelSelectedRowNdx(pageRowNdx);
			logger.debug("DTH@populateDisplayRow::PageRowIndex: " + pageRowNdx);

			int absNdx = tds.getAbsSelectedRowNdx();
			logger.debug("DTH@populateDisplayRow::absNds: " + absNdx);

			int tdsSize = tds.getTotalRows();
			logger.debug("DTH@populateDisplayRow:TdsSize: " + tdsSize);

			//int doFldNdx = 0;
			//String value = null;
			//String statusLabel = "";

			if (sectionNumber == SECTION_SIGNED_COMMITMENT) {
				statusBoxName = "Repeated1/cbSignDocStatus";
				byPassFldName = "Repeated1/stSignByPass";
				byPassHdnName = "Repeated1/hdByPass";
			} else if (sectionNumber == SECTION_ORDERED) {
				statusBoxName = "Repeated2/cbOrdDocStatus";
				byPassFldName = "Repeated2/stOrdByPass";
				byPassHdnName = "Repeated2/hdByPass";
				logger.debug("DTH@populateDisplayRow:TestStamp_ONE");
			} else if (sectionNumber == SECTION_STANDARD_CONDITION) {
				statusBoxName = "Repeated4/cbStdAction";
				byPassHdnName = "Repeated4/hdByPass";
			} else if (sectionNumber == SECTION_CUSTOM_CONDITION) {
				statusBoxName = "Repeated5/cbCusAction";
				byPassHdnName = "Repeated5/hdByPass";
			}

			statusBox = (ComboBox) getCurrNDPage().getDisplayField(
					statusBoxName);

			//FXP23399 MCM Nov 13, 2008, being thread un-safe, this optionList should be populted every time. start
            //statusBoxOption = (OptionList) statusBox.getOptions();
            statusBoxOption = new OptionList(new String[]{},new String[]{});
            statusBox.setOptions(statusBoxOption);
            //FXP23399 end
            
			String comboNdx = getRepeatedField(statusBoxName, pageRowNdx);
			logger.debug("DTH@populateDisplayRow::ComboValue[" + pageRowNdx
					+ "]: " + comboNdx);

			statusBoxOption.clear();

			statusId = (new Integer(comboNdx)).intValue();

			String byPassValue = "N";

			if (getRepeatedField(byPassHdnName, pageRowNdx) != null) {
				byPassValue = getRepeatedField(byPassHdnName, pageRowNdx);
				logger.debug("DTH@populateDisplayRow:TestStamp_TWO: "
						+ byPassValue);
			}

			byPass = byPassValue.toUpperCase().equals("Y") ? true : false;
			logger.debug("DTH@populateDisplayRow::SectionNumber["
					+ sectionNumber + "]: " + "ByPassValue[" + pageRowNdx
					+ "]: " + byPass);

			//--Release2.1--//
			//// Should be taken from the Generic Message file instead
			////String yesOrNo = (byPass == true) ? "Yes" : "No";

			yes = BXResources.getGenericMsg("YES_LABEL", theSessionState
					.getLanguageId());
			no = BXResources.getGenericMsg("NO_LABEL", theSessionState
					.getLanguageId());

			String yesOrNo = (byPass == true) ? yes : no;
			logger.debug("DTH@populateDisplayRow::YesOrNo: " + yesOrNo);
			logger.debug("DTH@populateDisplayRow::PageCondition8: "
					+ pg.getPageCondition8());

			if (pg.getPageCondition8() == false) {
				setStatusOption(statusBox, statusId, theSessionState
						.getSessionUserType(), byPass);
			}

		} catch (Exception e) {
			setStandardFailMessage();
			logger.error("Exception @DocumentTracking.populateRowStatusOption: Obtaining values from data object - ignored (assuming default values)");
			logger.error(e);
			statusId = 0;
			byPass = false;
		}

		//--Release2.1--//
		//// Should be taken from the Generic Message file instead
		////String yesOrNo = (byPass == true) ? "Yes" : "No";
		yes = BXResources.getGenericMsg("YES_LABEL", theSessionState
				.getLanguageId());
		no = BXResources.getGenericMsg("NO_LABEL", theSessionState
				.getLanguageId());

		String yesOrNo = (byPass == true) ? yes : no;
		////logger.debug("DTH@populateDisplayRow::YesOrNo: " + yesOrNo);

		////getCurrNDPage().setDisplayFieldValue(byPassFldName, new
		// String(yesOrNo));
		if (sectionNumber == SECTION_SIGNED_COMMITMENT) {
			getCurrNDPage().setDisplayFieldValue("Repeated1/stSignByPass",yesOrNo);
		} else if (sectionNumber == SECTION_ORDERED) {
			getCurrNDPage().setDisplayFieldValue("Repeated2/stOrdByPass",yesOrNo);
		}
		//// TODO: Check the necessarity of this with the Product Team.
		else if (sectionNumber == SECTION_STANDARD_CONDITION) {
			getCurrNDPage().setDisplayFieldValue("Repeated4/stStdSignByPass",yesOrNo);
		}
		///// Delete this, not necessary almost for sure.
		else if (sectionNumber == SECTION_CUSTOM_CONDITION) {
			getCurrNDPage().setDisplayFieldValue("Repeated5/stCusSignByPass",yesOrNo);
		}
	}

	//// This is the new method separated population of combobox sections and
	//// checkbox section. Implementation of Combobox and CheckBox objects in
	//// JATO is very different comparing with their implementation in ND.
	public void populateDisplayRowFunder(int pageRowNdx, String repeatedName,
			QueryModelBase dobject) {
		PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		PageCursorInfo tds = (PageCursorInfo) pst.get(repeatedName);

		int statusId = 0;

		// choice box name to populate
		String checkBoxName = null;

		try {
			tds.setRelSelectedRowNdx(pageRowNdx);
			logger.debug("DTH@populateDisplayRowFunder::PageRowIndex: "
					+ pageRowNdx);

			int absNdx = tds.getAbsSelectedRowNdx();
			logger.debug("DTH@populateDisplayRowFunder::absNds: " + absNdx);

			int tdsSize = tds.getTotalRows();
			logger.debug("DTH@populateDisplayRow:TdsSize: " + tdsSize);

			// Added handle for Funder Conditions -- Billy 11Jan2002
			checkBoxName = "Repeated3/ckFunderDocStatus";

			statusId = (new Integer(getRepeatedField(
					"Repeated3/hdFunderDocStatus", pageRowNdx))).intValue();
			logger.debug("DTH@populateDisplayRowFunder::StatusId_BeforeFUNDER: "
					+ statusId);

			// Added to handle Funder Conditions -- Billy 11Jan2002
			// If status == Approvaled (3) Check the checkbox
			if (statusId == Sc.DOC_STATUS_APPROVED) {
				logger.debug("DTH@populateDisplayRow::FUNDERStatus Approved Mode");
				setRepeatedField(checkBoxName, pageRowNdx, "T");
			} else {
				logger.debug("DTH@populateDisplayRow::FUNDERStatus Not-Approved Mode");
				setRepeatedField(checkBoxName, pageRowNdx, "F");
			}

			//if (statusId == Sc.DOC_STATUS_APPROVED)
			//{
			logger.debug("DTH@populateDisplayRowFunder::DisableCheckBoxPageRowNdx: "
					+ pageRowNdx);
			logger.debug("DTH@populateDisplayRowFunder::DisableCheckBoxPageRowNdx: "
					+ absNdx);

			setFunderSectionRow(dobject, checkBoxName, pageRowNdx, statusId);
			//}
			//--Ticket#681--end--//

		} catch (Exception e) {
			setStandardFailMessage();
			logger.error("Exception @DocumentTracking.populateRowStatusOption: Obtaining values from data object - ignored (assuming default values)");
			logger.error(e);
			statusId = 0;
		}
	}

	/**
	 * 
	 *  
	 */
	private void setStatusSelectable(ComboBox statusBox, int currValue) {
		////logger.debug("DTH@setStatusSelectable:2Args, statusBox: " +
		// statusBox + " , currValueIn: " + currValue);
		setStatusSelectable(statusBox, currValue, currValue, false);
	}

	/**
	 * 
	 *  
	 */
	private void setApprovedStatusSelectable(ComboBox statusBox, int currValue,
			boolean byPass) {
		////logger.debug("DTH@setApprovedStatusSelectable:23Args, statusBox: "
		// + statusBox + " , currValueIn: " + currValue +
		//// " , byPassIn: " + byPass);

		if (byPass) {
			setStatusSelectable(statusBox, Sc.DOC_STATUS_APPROVED, currValue,
					true);
			setStatusSelectable(statusBox, Sc.DOC_STATUS_REJECTED, currValue,
					true);
		}

	}

	/**
	 * 
	 *  
	 */
	private void setStatusSelectable(ComboBox statusBox, int optValue,
			int currValue) {
		logger.debug("DTH@setStatusSelectable:3Args, statusBox: " + statusBox
				+ " , currValueIn: " + currValue + " , currValueIn: "
				+ currValue);

		setStatusSelectable(statusBox, optValue, currValue, true);
	}

	/**
	 * 
	 *  
	 */
	private void setStatusSelectable(ComboBox statusBox, int optValue,
			int currValue, boolean checkForDuplicate) {
		////logger.debug("DTH@setStatusSelectable:4Args, statusBox: " +
		// statusBox + " , currValueIn: " + currValue);
		////logger.debug("DTH@setStatusSelectable::OptionValueIn: " +
		// optValue);
		////logger.debug("DTH@setStatusSelectable::CheckForDuplicateIn: " +
		// checkForDuplicate);

		//--Release2.1--start//
		//// Get these descriptions from BXResources to obtain in the proper
		// language.
		////String optionDesc = PicklistData.getDescription("DocumentStatus",
		// optValue);
		String optionDesc = BXResources.getPickListDescription(
		        theSessionState.getDealInstitutionId(),
				"DOCUMENTSTATUS", optValue, theSessionState.getLanguageId());

		//logger.debug("DTH@setStatusSelectable::OptionDesc: " + optionDesc);
		//--Release2.1--end//

		if (optionDesc == null) {
			logger.error("Unknown Document Tracking Status Id = " + optValue+ " - ignored");
			return;
		}

		if (!checkForDuplicate || optValue != currValue) {
			OptionList statusBoxOption = (OptionList) statusBox.getOptions();
			//logger.debug("DTH@setStatusSelectable::ComboBoxSize: " +
			// statusBoxOption.size());

			try {
				JdbcExecutor jExec = getSessionResourceKit().getJdbcExecutor();
				String sql = "SELECT DOCUMENTSTATUSDESCRIPTION FROM DOCUMENTSTATUS "
					+ "WHERE DOCUMENTSTATUSID = " + optValue;

				//logger.debug("DTH@setStatusSelectable::SQL_BeforeExec: " +
				// sql);

				int key = jExec.execute(sql);

				for (; jExec.next(key);) {
					//--Release2.1--//
					//// Shoud be taken from the BXResources instead.
					////statusBoxOption.add(jExec.getString(key, 1), (new
					// Integer(optValue)).toString());

					String label = BXResources.getPickListDescription(
					        theSessionState.getDealInstitutionId(),
							"DOCUMENTSTATUS", optValue, theSessionState
							.getLanguageId());
					String value = (new Integer(optValue)).toString();

					//logger.debug("DTH@setStatusSelectable:Label: " + label);
					//logger.debug("DTH@setStatusSelectable:Value: " + value);

					statusBoxOption.add(label, value);
				}
				jExec.closeData(key);

			} catch (Exception e) {
				statusBoxOption.clear();
				statusBoxOption.add("N/A", (new Integer(0)).toString());
			}
		}
	}

	/**
	 * 
	 *  
	 */
	private void setStatusOption(ComboBox statusBox, int currStatId,
			int userTypeId, boolean byPass) {
		PageEntry pg = theSessionState.getCurrentPage();

		logger.debug("DTH@setStatusOption::Start_StatusBoxIN: " + statusBox);
		logger.debug("DTH@setStatusOption::UserTypeId: " + userTypeId);
		logger.debug("DTH@setStatusOption::ByPass: " + byPass);
		logger.debug("DTH@setStatusOption::currStatId: " + currStatId);

		// SEAN Ticket #2497 Dec 08, 2005: keep the original current user type,
		// which is need to
		// decide the options for WAIVED.
		int orgUserTypeId = userTypeId;
		// SEAN Ticket #2497 END

		switch (userTypeId) {
    		/**
             * BEGIN FIX FOR FXP21075 
             * Mohan V Premkumar
             * 04/07/2008
             */
			case Sc.USER_TYPE_JR_UNDERWRITER:
			    userTypeId = Sc.USER_TYPE_JR_UNDERWRITER;
                break;
            
			case Sc.USER_TYPE_UNDERWRITER:
			case Sc.USER_TYPE_SR_UNDERWRITER:
			case Sc.USER_TYPE_SUPERVISOR:
			case Sc.USER_TYPE_BRANCH_MGR:
			case Sc.USER_TYPE_SENIOR_MGR: {
				userTypeId = Sc.USER_TYPE_UNDERWRITER;
				break;
			}
			case Sc.USER_TYPE_JR_ADMIN:
			case Sc.USER_TYPE_ADMIN:
			case Sc.USER_TYPE_SR_ADMIN: {
				userTypeId = Sc.USER_TYPE_ADMIN;
				break;
			}
		}

		// ensure option corresponding to current status is added (precaution)
		setStatusSelectable(statusBox, currStatId);

		switch (currStatId) {
			case Sc.DOC_STATUS_REQUESTED:
			case Sc.DOC_STATUS_INSUFFICIENT: {
			    /**
	             * BEGIN FIX FOR FXP21075 
	             * Mohan V Premkumar
	             * 04/07/2008
	             */
				if ((userTypeId == Sc.USER_TYPE_UNDERWRITER) || (userTypeId == Sc.USER_TYPE_JR_UNDERWRITER)) {
					setStatusSelectable(statusBox, Sc.DOC_STATUS_RECEIVED,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_CAUTION,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_APPROVED,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_INSUFFICIENT,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_WAIVED, currStatId);
					if (userTypeId == Sc.USER_TYPE_UNDERWRITER)
					    setStatusSelectable(statusBox, Sc.DOC_STATUS_REJECTED,
							currStatId);
				}
				/**
                 * END FIX FOR FXP21075
                 */
				if (userTypeId == Sc.USER_TYPE_ADMIN) {
					setStatusSelectable(statusBox, Sc.DOC_STATUS_RECEIVED,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_CAUTION,
							currStatId);
					setApprovedStatusSelectable(statusBox, currStatId, byPass);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_INSUFFICIENT,
							currStatId);
				}
				break;
			}
			case Sc.DOC_STATUS_RECEIVED:
			case Sc.DOC_STATUS_CAUTION: {
			    /**
	             * BEGIN FIX FOR FXP21075 
	             * Mohan V Premkumar
	             * 04/07/2008
	             */
				if ((userTypeId == Sc.USER_TYPE_UNDERWRITER) || (userTypeId == Sc.USER_TYPE_JR_UNDERWRITER)) {
					setStatusSelectable(statusBox, Sc.DOC_STATUS_APPROVED,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_INSUFFICIENT,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_WAIVED, currStatId);
					if (userTypeId == Sc.USER_TYPE_UNDERWRITER)
					    setStatusSelectable(statusBox, Sc.DOC_STATUS_REJECTED,
							currStatId);
				}
				/**
                 * END FIX FOR FXP21075
                 */
				if (userTypeId == Sc.USER_TYPE_ADMIN) {
					setApprovedStatusSelectable(statusBox, currStatId, byPass);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_INSUFFICIENT,
							currStatId);
				}
				break;
			}
			case Sc.DOC_STATUS_WAIVE_REQUESTED: {
			    /**
	             * BEGIN FIX FOR FXP21075 
	             * Mohan V Premkumar
	             * 04/07/2008
	             */
				if ((userTypeId == Sc.USER_TYPE_UNDERWRITER) || (userTypeId == Sc.USER_TYPE_JR_UNDERWRITER)) {
					setStatusSelectable(statusBox, Sc.DOC_STATUS_APPROVED,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_INSUFFICIENT,
							currStatId);
					if (pg.getPageCondition3()) {
						logger.trace("AAG passed: WAIWED instead requested ");
						setStatusSelectable(statusBox, Sc.DOC_STATUS_WAIVED,
								currStatId);
					}
					if (userTypeId == Sc.USER_TYPE_UNDERWRITER)
					    setStatusSelectable(statusBox, Sc.DOC_STATUS_REJECTED,
							currStatId);
					/**
	                 * END FIX FOR FXP21075
	                 */
				} else {
					//No other options for the Admin
				}
				break;
			}
			case Sc.DOC_STATUS_REJECTED: {
				//// SYNCADD.
				//Check if user selected "Rejected" on some DocTracking entries
				// - By Billy 24June2002
				if (pg.getPageCondition4() == false) {
					// Normal
					setStatusSelectable(statusBox, Sc.DOC_STATUS_INSUFFICIENT,
							currStatId);
				} else {
					// Open all options for user to fallback the changes
					setStatusSelectable(statusBox, Sc.DOC_STATUS_RECEIVED,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_CAUTION,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_APPROVED,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_INSUFFICIENT,
							currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_WAIVED, currStatId);
					setStatusSelectable(statusBox, Sc.DOC_STATUS_REQUESTED,
							currStatId);
				}
				////========================================================================

				break;
			}

			// New requirement to add Approved option if Waived -- Billy 14March2002
			case Sc.DOC_STATUS_WAIVED: {
				// SEAN Ticket #2497 Dec 08, 2005: we need check the byPass and
				// populate the
				// options based on the current user type.
				if (byPass) {
					// any adiminstrator can approve.
					setStatusSelectable(statusBox, Sc.DOC_STATUS_APPROVED,
							currStatId);
				} else {
					// only senior admin can do approve.
					if (orgUserTypeId == Sc.USER_TYPE_SR_ADMIN)
						setStatusSelectable(statusBox, Sc.DOC_STATUS_APPROVED,
								currStatId);
				}
				// SEAN Ticket #2497 END
				break;
			}
			case Sc.DOC_STATUS_APPROVED:
			case Sc.DOC_STATUS_OPEN: {
				break;
			}
		}

		return;

	}

	/**
	 * 
	 *  
	 */
	public boolean checkDisplayThisRow(String section) {
		try {
			PageEntry pg = theSessionState.getCurrentPage();

			PageCursorInfo tds = getPageCursorInfo(pg, section);

			logger.debug("DTH@Section: " + section + ", CheckDisplayRow: "
					+ tds.getTotalRows());

			if (tds.getTotalRows() <= 0)
				return false;

			//logger.debug("DTH@CheckDisplayRow:before return true");
			return true;
		} catch (Exception e) {
			logger.error("Exception @checkDisplayThisRow");
			logger.error(e);
			return false;
		}
	}

	//--Release2.1--TD_DTS_CR--start//
	//// All methods below have been added to meet the named above request
	//// (user should be able to edit Standard/Custom conditions via
	// DocumentTracking screen).
	//// ALERT:: BXTODO. Separate a number of methods below (indentical to the
	// ones
	//// of ConditionsReviewHandler in the separate class).
	/**
	 * 
	 *  
	 */
	public void handleDelete(int pageRowNdx, String sourceName) {
		PageEntry pg = theSessionState.getCurrentPage();

		if (disableEditButton(pg) == true)
			return;

		String args = (sourceName + "|" + pageRowNdx);

		setActiveMessageToAlert(BXResources.getSysMsg(
				CONDITION_REVIEW_DELETE, theSessionState.getLanguageId()),
				ActiveMsgFactory.ISCUSTOMDIALOG, args);

	}

	/**
	 * 
	 *  
	 */
	private void updateStandardConditionStatus(PageEntry pg,
			SessionResourceKit srk) throws Exception {
		updateConditionStatus(pg, srk, "Repeated4/hdStdDocTrackId",
		"Repeated4/cbStdAction");
	}

	/**
	 * 
	 *  
	 */
	private void updateConditionStatus(PageEntry pg, SessionResourceKit srk,
			String indexField, String statusField) throws Exception {
		int statusId = 0;
		int docTrackId = 0;
		int copyId = pg.getPageDealCID();

		Vector hdDocTrackId = this.getRepeatedFieldValues(indexField);
		Vector cbStatus = this.getRepeatedFieldValues(statusField);

		if (hdDocTrackId != null && hdDocTrackId.size() > 0) {
			// There are multiple row values in this display field
			int vectorSize = hdDocTrackId.size();
			for (int i = 0; i < vectorSize; i++) {
				docTrackId = com.iplanet.jato.util.TypeConverter.asInt(
						hdDocTrackId.get(i), -1);
				if (docTrackId != -1) {
					DocumentTracking docTrack = new DocumentTracking(srk,
							docTrackId, copyId);
					if (cbStatus.get(i) != null) {
						// IMPORTANT!! This update goes from the DocTrackScreen, so it is
						// up to client already to take care of the document
						// now. As a result on the screen statusId is always
						// unchangable "Open" and the statusId passed to the DocumentTracking has to
						// be "Requesed", statusId = 1;
						//statusId = com.iplanet.jato.util.TypeConverter.asInt(cbStatus.get(i));
						statusId = 1;
						docTrack.setDocumentStatusId(statusId);
					}
					docTrack.ejbStore();
				}
			}
		}
	}

	/**
	 * 
	 *  
	 */
	/**
	 * public void updateFreeFormCondition(PageEntry pg, SessionResourceKit srk)
	 * throws Exception { int docTrackid = 0; int actionId = 0; int roleId = 0;
	 * int copyId = pg.getPageDealCID();
	 * 
	 * Vector hdCusDocTrackId =
	 * getRepeatedFieldValues("Repeated5/hdCusDocTrackId"); Vector
	 * tbCusCondition = getRepeatedFieldValues("Repeated5/tbCusCondition");
	 * Vector tbCusDocumentLabel =
	 * getRepeatedFieldValues("Repeated5/tbCusDocumentLabel"); Vector
	 * cbCusAction = getRepeatedFieldValues("Repeated5/cbCusAction"); Vector
	 * cbCusResponsibility =
	 * getRepeatedFieldValues("Repeated5/cbCusResponsibility");
	 * 
	 * if(hdCusDocTrackId != null && hdCusDocTrackId.size() > 0) { // There are
	 * multiple row values in this display field int vectorSize =
	 * hdCusDocTrackId.size(); for(int i = 0; i < vectorSize; i ++) { docTrackid =
	 * com.iplanet.jato.util.TypeConverter.asInt(hdCusDocTrackId.get(i), -1); if
	 * (docTrackid == -1) continue;
	 * 
	 * //--Release2.1--// //// The Product Team requires the creation of Custom
	 * Condition in both languages. DocumentTracking freeForm = new
	 * DocumentTracking(srk, docTrackid, copyId);
	 * 
	 * String documentText =
	 * com.iplanet.jato.util.TypeConverter.asString(tbCusCondition.get(i));
	 * 
	 * //--Release2.1--// //// The Product Team requires the creation of Custom
	 * Condition in both languages. if (documentText != null &&
	 * documentText.trim().length() > 0) {
	 * freeForm.setDocumentTextForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH,
	 * documentText); } else { //--Release2.1--// //// The Product Team requires
	 * the creation of Custom Condition in both languages.
	 * freeForm.setDocumentTextForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, " ");
	 * 
	 * pg.setPageCondition6(true); pg.setPageCriteriaId1(ALERT_TEXT_ABSENT); }
	 * 
	 * String documentLabel =
	 * com.iplanet.jato.util.TypeConverter.asString(tbCusDocumentLabel.get(i));
	 * //--Release2.1--// //// The Product Team requires the creation of Custom
	 * Condition in both languages. if (documentLabel != null &&
	 * documentLabel.trim().length() > 0) {
	 * freeForm.setDocumentLabelForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH,
	 * documentLabel); } else {
	 * freeForm.setDocumentLabelForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH, "
	 * ");
	 * 
	 * pg.setPageCondition6(true); pg.setPageCriteriaId1(ALERT_LABEL_ABSENT); }
	 * 
	 * if(com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i), -1) ==
	 * -1) { actionId =
	 * com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i));
	 * //--Release2.1--// //// The Product Team requires the creation of Custom
	 * Condition in both languages. freeForm.setDocumentStatusId(actionId); }
	 * if(com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i),
	 * -1) == -1) { roleId =
	 * com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i));
	 * //--Release2.1--// //// The Product Team requires the creation of Custom
	 * Condition in both languages.
	 * freeForm.setDocumentResponsibilityRoleId(roleId);
	 * 
	 * setResponsibleId(srk, pg, freeForm); } //--Release2.1--// //// The
	 * Product Team requires the creation of Custom Condition in both languages.
	 * freeForm.ejbStore(); } // for } // else return; }
	 */

	///////////////////////
	public void updateFreeFormCondition(PageEntry pg, SessionResourceKit srk)
			throws Exception {
		int docTrackid = 0;
		int actionId = 0;
		int roleId = 0;
		int copyId = pg.getPageDealCID();

		Vector hdCusDocTrackId = getRepeatedFieldValues("Repeated5/hdCusDocTrackId");
		//logger.debug("DTH@hdCusDocTrackIdSize: " + hdCusDocTrackId.size());

		Vector tbCusCondition = getRepeatedFieldValues("Repeated5/tbCusCondition");
		Vector tbCusDocumentLabel = getRepeatedFieldValues("Repeated5/tbCusDocumentLabel");
		Vector cbCusAction = getRepeatedFieldValues("Repeated5/cbCusAction");
		Vector cbCusResponsibility = getRepeatedFieldValues("Repeated5/cbCusResponsibility");

		//// temp debug.
		/**
		 * logger.debug("DTH@CusActionSize: " + cbCusAction.size());
		 * logger.debug("DTH@CusResponsibilitySize: " +
		 * cbCusResponsibility.size()); for(int i = 0; i <
		 * hdCusDocTrackId.size(); i ++) { logger.debug("DTH@RespRoleId: " +
		 * com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i)));
		 * logger.debug("DTH@ActionId: " +
		 * com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i)));
		 * logger.debug("DTH@CusDocTrackId: " +
		 * com.iplanet.jato.util.TypeConverter.asInt(hdCusDocTrackId.get(i))); }
		 */

		if (hdCusDocTrackId != null && hdCusDocTrackId.size() > 0) {
			// There are multiple row values in this display field
			int vectorSize = hdCusDocTrackId.size();
			for (int i = 0; i < vectorSize; i++) {
				docTrackid = com.iplanet.jato.util.TypeConverter.asInt(
						hdCusDocTrackId.get(i), -1);
				if (docTrackid == -1)
					continue;

				//--Release2.1--//
				//// The Product Team requires the creation of Custom Condition
				// in both languages.
				////DocumentTracking freeForm = new DocumentTracking(srk,
				// docTrackid, copyId);
				DocumentTracking freeFormEngl = new DocumentTracking(srk,
						docTrackid, copyId);
				DocumentTracking freeFormFrench = new DocumentTracking(srk,
						docTrackid, copyId);

				String documentText = com.iplanet.jato.util.TypeConverter
						.asString(tbCusCondition.get(i));

				////logger.debug("DTH@FreeText: " + documentText);
				//--Release2.1--//
				//// The Product Team requires the creation of Custom Condition
				// in both languages.
				if (documentText != null && documentText.trim().length() > 0) {
					////freeFormEngl.setDocumentTextForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH,
					// documentText);
					freeFormEngl.setDocumentTextForLanguage(0, documentText); ////
					// English
					freeFormFrench.setDocumentTextForLanguage(1, documentText); ////
					// French
				} else {
					//--Release2.1--//
					//// The Product Team requires the creation of Custom
					// Condition in both languages.
					////freeForm.setDocumentTextForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH,
					// " ");
					freeFormEngl.setDocumentTextForLanguage(0, " "); ////English
					freeFormFrench.setDocumentTextForLanguage(1, " "); ////
					// French

					pg.setPageCondition6(true);
					pg.setPageCriteriaId1(ALERT_TEXT_ABSENT);
				}

				String documentLabel = com.iplanet.jato.util.TypeConverter
						.asString(tbCusDocumentLabel.get(i));
				//--Release2.1--//
				//// The Product Team requires the creation of Custom Condition
				// in both languages.
				if (documentLabel != null && documentLabel.trim().length() > 0) {
					////freeForm.setDocumentLabelForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH,
					// documentLabel);
					freeFormEngl.setDocumentLabelForLanguage(0, documentLabel); ////
					// English
					freeFormFrench.setDocumentLabelForLanguage(1, documentLabel); ////
					// French
				} else {
					////freeForm.setDocumentLabelForLanguage(Mc.LANGUAGE_PREFERENCE_ENGLISH,
					// " ");
					freeFormEngl.setDocumentLabelForLanguage(0, " "); ////
					// English
					freeFormFrench.setDocumentLabelForLanguage(1, " "); ////
					// French

					pg.setPageCondition6(true);
					pg.setPageCriteriaId1(ALERT_LABEL_ABSENT);
				}
				actionId = com.iplanet.jato.util.TypeConverter
						.asInt(cbCusAction.get(i));
				////logger.debug("DTH@ActionId_In: " +
				// com.iplanet.jato.util.TypeConverter.asInt(cbCusAction.get(i)));

				//--Release2.1--//
				//// The Product Team requires the creation of Custom Condition
				// in both languages.
				////freeForm.setDocumentStatusId(actionId);
				freeFormEngl.setDocumentStatusId(actionId);
				freeFormFrench.setDocumentStatusId(actionId);

				roleId = com.iplanet.jato.util.TypeConverter.asInt(
						cbCusResponsibility.get(i));
				////logger.debug("DTH@RespRoleId_In: " +
				// com.iplanet.jato.util.TypeConverter.asInt(cbCusResponsibility.get(i)));

				//--Release2.1--//
				//// The Product Team requires the creation of Custom Condition
				// in both languages.
				////freeForm.setDocumentResponsibilityRoleId(roleId);
				freeFormEngl.setDocumentResponsibilityRoleId(roleId);
				freeFormFrench.setDocumentResponsibilityRoleId(roleId);

				////setResponsibleId(srk, pg, freeForm);
				setResponsibleId(srk, pg, freeFormEngl);
				setResponsibleId(srk, pg, freeFormFrench);

				//--Release2.1--//
				//// The Product Team requires the creation of Custom Condition
				// in both languages.
				////freeForm.ejbStore();
				freeFormEngl.ejbStore();
				freeFormFrench.ejbStore();
			} // for
		} // else
		return;
	}

	/**
	 * 
	 *  
	 */
	//--Release2.1--//
	//// Original method for the creation of custom conditions (with one record
	// with
	//// custom preference language in the DocumentTrackingVerbiage table.
	// Product
	//// Team requests now to create a custom condition in a style of Standard
	// condition
	//// (two records in the DocumentTrackingVerbiage table for both
	// languages).
	//// Submit action on update of the documentText should propagate this
	// change
	//// to the BOTH records (English and French) nevertheless what the session
	// language is
	//// it at the moment.
	//// The method below provides this new logic. This method SHOULD STAY here
	// since
	//// it will be renamed and used for the new TD's ChangeRequest (joining
	// Condition
	//// and DocTracking screens).
	public void addFreeFormCondition() {
		SessionResourceKit srk = getSessionResourceKit();
		PageEntry pg = getTheSessionState().getCurrentPage();
		//DocumentTracking freeFormEngl = null;
		//DocumentTracking freeFormFrench = null;

		try {
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated5");

			// Modified to handle multiple selections -- by BILLY 24July2001
			ListBox theStdConditionCtl = (ListBox) (getCurrNDPage()
					.getChild("lbCustCondition"));
			Object theSelectedConds[] = theStdConditionCtl.getValues();
			if (theSelectedConds.length <= 0) {
				setActiveMessageToAlert(BXResources.getSysMsg(
						CONDITION_REVIEW_ADD_TYPE, theSessionState
						.getLanguageId()),
						ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			////logger.debug("DTH@The number of conditions selected = " +
			// theSelectedConds.length);
			// Add all selected Conditions
			int conditionId = 0;

			Deal deal = new Deal(srk, null, pg.getPageDealId(), pg.getPageDealCID());

			for (int i = 0; i < theSelectedConds.length; i++) {
				conditionId = com.iplanet.jato.util.TypeConverter.asInt(
						theSelectedConds[i], 0);

				logger.trace("DTH@Custom conditionId "
						+ "("+ i+ ") = "+ conditionId
						+ "("+ theStdConditionCtl.getOptions()
						.getValueLabel(conditionId + "") + ")");

				srk.beginTransaction();

				//DocumentTracking freeForm = new DocumentTracking(srk);

				ConditionHandler ch = new ConditionHandler(srk);
				if (conditionId == 0) {
					ch.buildFreeFormCondition(conditionId, new DealPK(pg.getPageDealId(), 
							pg.getPageDealCID()), " ", " ");
				} else {
					// Copy from Standard condition
					Condition condition = new Condition(srk, conditionId);

					////int langId = theSessionState.getLanguageId();
					//--Release2.1--TD_DTS_CR--//
					//// Based on the User choice system now should propagate
					// the condition text
					//// for two languages.
					int langId = new Integer(getCurrNDPage()
							.getDisplayFieldValue("cbLanguagePreference")
							.toString()).intValue();

					ch.buildFreeFormCondition(conditionId, new DealPK(pg.getPageDealId(), 
							pg.getPageDealCID()), condition
							.getConditionTextByLanguage(langId), condition
							.getConditionLabelByLanguage(langId));
				}

				srk.commitTransaction();
				tds.setTotalRows(tds.getTotalRows() + 1);
				tds.setTotalRowsChanged(true);
			}

			// A user should be able to edit Standard/Custom conditions via
			// DocumentTracking screen.
			getCurrNDPage().setDisplayFieldValue("stTargetCustCon",TARGET_TAG);
			return;
		} catch (Exception e) {
			srk.cleanTransaction();
			logger.error("DocumentTrackingHandler.Add Free Form Got Error!!!");
			logger.error(e);
			setStandardFailMessage();
			return;
		}
	}

	/**
	 * 
	 *  
	 */
	public void addStandardCondition() {
		SessionResourceKit srk = getSessionResourceKit();

		PageEntry pg = getTheSessionState().getCurrentPage();

		try {
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated4");

			// Modified to handle multiple selections -- by BILLY 24July2001
			ListBox theStdConditionCtl = (ListBox) (getCurrNDPage()
					.getChild("lbStdCondition"));
			Object theSelectedConds[] = theStdConditionCtl.getValues();
			if (theSelectedConds.length <= 0) {
				setActiveMessageToAlert(BXResources.getSysMsg(
						CONDITION_REVIEW_ADD_TYPE, theSessionState
						.getLanguageId()),
						ActiveMsgFactory.ISCUSTOMCONFIRM);
				return;
			}

			////logger.debug("VLAD ==> the number of conditions selected = " +
			// theSelectedConds.size ());
			// Add all selected Conditions
			int conditionId = 0;

			// Preparation for the common data
			Deal deal = new Deal(srk, null, pg.getPageDealId(), 
					pg.getPageDealCID());
			int sourceOfBusinessCategory = deal.getSourceOfBusinessProfileId();
			int applicationId = com.iplanet.jato.util.TypeConverter.asInt(pg
					.getPageDealApplicationId().trim());
			for (int i = 0; i < theSelectedConds.length; i++) {
				conditionId = com.iplanet.jato.util.TypeConverter.asInt(
						theSelectedConds[i], 0);

				logger.trace("@DTH.addStandardCondition:Std conditionId ("+ i+ ") = "
						+ conditionId+ "("+ theStdConditionCtl.getOptions()
						.getValueLabel(conditionId + "") + ")");
				if (conditionId == 0)
					continue;

				srk.beginTransaction();
				Condition condition = new Condition(srk, conditionId);

				//// DocTracking fragment should be moved to the Submit action
				// of the page.
				DocumentTracking dt = new DocumentTracking(srk);

				//// Trick should be done in SetupBeforePageGeneration(), since
				// the Product Team wants to see a new
				//// Standard/Custom condition appeared in the DocTracking
				// section only
				//// after the submit of the whole page and there is no way to
				// distiguish
				//// the new and processed conditions.!!!
				dt.create(condition, pg.getPageDealId(), pg.getPageDealCID(),
						Mc.DOC_STATUS_OPEN,
						Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED);

				//// This will not propagate the Condition into the db.
				////dt.createWithNoVerbiage(condition, pg.getPageDealId(),
				// pg.getPageDealCID(), Mc.DOC_STATUS_OPEN,
				// Mc.DOCUMENT_TRACKING_SOURCE_USER_SELECTED);

				//About Responsibility Role Id :
				// after create() it's been assigned by Condition
				// ResponsibilityRoleId
				// Standard Condition got some additional logic:
				int roleId = dt.getDocumentResponsibilityRoleId();

				////logger.trace("-------- Role Id" +roleId + "(" + i + ")");
				if (roleId == Mc.CRR_SOURCE_OF_BUSINESS) {
					if (sourceOfBusinessCategory == Mc.SOB_CATEGORY_TYPE_DIRECT
							|| sourceOfBusinessCategory == Mc.SOB_CATEGORY_TYPE_INTERNET) {
						dt.setDocumentResponsibilityRoleId(Mc.CRR_USER_TYPE_APPLICANT);

						////logger.trace("--------- USER_TYPE_APPLICANT" + "("
						// + i + ")");
					}
				}
				dt.setApplicationId(applicationId);
				setResponsibleId(srk, pg, dt);
				dt.ejbStore();

				srk.commitTransaction();

				tds.setTotalRows(tds.getTotalRows() + 1);
				tds.setTotalRowsChanged(true);
			}

			// A user should be able to edit Standard/Custom conditions via
			// DocumentTracking screen.
			getCurrNDPage().setDisplayFieldValue("stTargetStdCon",TARGET_TAG);

			return;
		} catch (Exception e) {
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error(e);
			return;
		}
	}

	/**
	 * 
	 *  
	 */
	public int setResponsibleId(SessionResourceKit srk, PageEntry pg,
			DocumentTracking dt) {
		int responsibleId = -1;

		try {
			switch (dt.getDocumentResponsibilityRoleId()) {
				case Mc.CRR_USER_TYPE_APPLICANT: {
					Borrower borrower = new Borrower(srk, null);
					ArrayList borList = (ArrayList) borrower.findByDeal(new DealPK(
							pg.getPageDealId(), pg.getPageDealCID()));
					responsibleId = ((Borrower) borList.get(0)).getBorrowerId();
					break;
				}
				// SEAN Ticket #2497 Dec 09, 2005:
				// comment out because we done have these values in database.
				//case Mc.CRR_USER_TYPE_JR_ADMIN : case Mc.CRR_USER_TYPE_ADMIN :
				// case Mc.CRR_USER_TYPE_SR_ADMIN :
				//{
				//  Deal deal = new Deal(srk, null, pg.getPageDealId(),
				// pg.getPageDealCID());
				//  responsibleId = deal.getAdministratorId();
				//  break;
				//}
				// delete CRR_USER_TYPE_JR_UNDERWRITER and
				// CRR_USER_TYPE_SR_UNDERWRITER, beause we only have
				// CRR_USER_TYPE_UNDERWRITER in database
				case Mc.CRR_USER_TYPE_UNDERWRITER: {
					Deal deal = new Deal(srk, null, pg.getPageDealId(), 
							pg.getPageDealCID());
					responsibleId = deal.getUnderwriterUserId();
					break;
				}
				// comment out because we done have these values in database.
				//case Mc.CRR_USER_TYPE_SUPERVISOR : case
				// Mc.CRR_USER_TYPE_BRANCH_MGR : case Mc.CRR_USER_TYPE_SENIOR_MGR :
				//{
				//  Deal deal = new Deal(srk, null, pg.getPageDealId(),
				// pg.getPageDealCID());
				//  UserProfile up = new UserProfile(srk);
				//  up.findByPrimaryKey(new
				// UserProfileBeanPK(deal.getUnderwriterUserId()));
				//  responsibleId = up.getManagerId();
				//  break;
				//}
				// SEAN Ticket #2497 END
				case Mc.CRR_PARTY_TYPE_SOLICITOR: {
					PartyProfile pp = new PartyProfile(srk);
					pp.setSilentMode(true);
					pp.findByDealSolicitor(pg.getPageDealId());
					responsibleId = pp.getPartyProfileId();
					break;
				}
				case Mc.CRR_PARTY_TYPE_APPRAISER: {
					PartyProfile pp = new PartyProfile(srk);
					ArrayList arrayList = (ArrayList) pp.findByDealAndType(
							new DealPK(pg.getPageDealId(), pg.getPageDealCID()),
							Mc.PARTY_TYPE_APPRAISER);
					if (arrayList.size() > 0)
						responsibleId = ((PartyProfile) arrayList.get(0))
								.getPartyProfileId();
					break;
				}
				case Mc.CRR_SOURCE_OF_BUSINESS: {
					Deal deal = new Deal(srk, null, pg.getPageDealId(), pg
							.getPageDealCID());
					responsibleId = deal.getSourceOfBusinessProfileId();
					break;
				}
			}

			//end of switch
			dt.setDocumentResponsibilityId(responsibleId);
			return responsibleId;
		} catch (FinderException fe) {
			logger.warning("ConditionReview: "
					+ BXResources.getSysMsg(
							"CONDITION_REVIEW_RESPONSIBLE_NOT_FOUND", 0)
							+ ": DealId=" + pg.getPageDealId() + "; CopyId="
							+ pg.getPageDealCID());

			// As requested by Product, not to display any error messages -- By
			// Billy 02May2001
			//setActiveMessageToAlert(Sc.CONDITION_REVIEW_RESPONSIBLE_NOT_FOUND,ActiveMsgFactory.ISCUSTOMCONFIRM);
			// Assign to a PartyId which should never exists
			dt.setDocumentResponsibilityId(-1);
			return -1;
		} catch (Exception e) {
			logger.error("Exception @ConditionReviewHandler.getResponsibleId: Locating responsible party, DealId="
					+ pg.getPageDealId()+ "; CopyId="
					+ pg.getPageDealCID() + " - ignored");
			logger.error(e);
			return -1;
		}
	}

	/**
	 * 
	 *  
	 */
	public void handleAdd(String sourceName) {
		PageEntry pg = theSessionState.getCurrentPage();

		if (disableEditButton(pg) == true)
			return;

		if (sourceName.equals("btStdAddCondition")) {
			addStandardCondition();
			theSessionState.getCurrentPage().setLoadTarget(TARGET_CONREVIEW_STD);
		}

		if (sourceName.equals("btCusAddCondition")) {
			addFreeFormCondition();
			theSessionState.getCurrentPage().setLoadTarget(
					TARGET_CONREVIEW_CUST);
		}
		
		return;
	}
	
	/**
	 * 
	 *  
	 */
	public void navigateToStdCondDetail(int sectionIndex, int rowNdx) {
		String section = null;

		String docTrackIdName = null;

		try {
			if (sectionIndex == 4) {
				section = "Repeated4";
				docTrackIdName = "Repeated4/hdStdDocTrackId";

				theSessionState.getCurrentPage().setLoadTarget(
						TARGET_CONREVIEW_STD);
			}

			PageEntry pg = getTheSessionState().getCurrentPage();
			PageCursorInfo tds = getPageCursorInfo(pg, section);
			Hashtable pst = pg.getPageStateTable();
			if (tds.getTotalRows() <= 0)
				return;
			rowNdx = getCursorAbsoluteNdx(tds, rowNdx);
			PageEntry pgEntry = setupSubPagePageEntry(pg,
					Mc.PGNM_CONDITIONS_DETAIL, true);
			String docTrackId = this.getRepeatedField(docTrackIdName, rowNdx);
			if (docTrackId == null) {
				return;
			}
			if (docTrackId.equals("")) {
				return;
			}
			Hashtable subPst = pgEntry.getPageStateTable();
			subPst.put("dfDocTrackId", docTrackId);
			getSavedPages().setNextPage(pgEntry);
			navigateToNextPage(true);
		} catch (Exception e) {
			setStandardFailMessage();
			logger.error("Exception @ConditionsReviewHandler.navigateToDetail");
			logger.error(e);
		}

	}

	/**
	 * 
	 *  
	 */
	private void deleteCondition(int sectionIndex, int rowIndex) {
		PageEntry pg = getTheSessionState().getCurrentPage();

		int docTrackId = 0;

		try {
			SessionResourceKit srk = getSessionResourceKit();
			Hashtable pst = pg.getPageStateTable();
			PageCursorInfo tds = getPageCursorInfo(pg, "Repeated"
					+ sectionIndex);

			String search = "Repeated" + sectionIndex + "/hd"
			+ (sectionIndex == 4 ? "Std" : "Cus") + "DocTrackId";
			logger.debug("DTH@deleteCondition : SectionIndex = "
					+ sectionIndex + " : searchStr = " + search);

			Vector docTrackObj = this.getRepeatedFieldValues(search);

			if (docTrackObj == null || docTrackObj.size() <= 0) {
				logger.trace("---------- DELETE ID is NULL");
				return;
			}
			String docTrackString = null;
			docTrackString = (String) (docTrackObj.get(rowIndex).toString());
			logger.debug("DTH@deleteCondition : rowIndex = " + rowIndex
					+ " : docTrackString = " + docTrackString);
			docTrackId = com.iplanet.jato.util.TypeConverter.asInt(docTrackObj
					.get(rowIndex), -1);
			if (docTrackId == -1) {
				logger.trace("------------NON_NUMERIC");
				return;
			}

			srk.beginTransaction();
			srk.setInterimAuditOn(false); //4.4 Transaction History
			DocumentTracking dt = new DocumentTracking(srk, docTrackId, pg
					.getPageDealCID());
			dt.ejbRemove();
			srk.restoreAuditOn(); //4.4 Transaction History
			srk.commitTransaction();
			tds.setTotalRows(tds.getTotalRows() - 1);
			tds.setTotalRowsChanged(true);

			pg.setModified(true);
		} catch (Exception e) {
			srk.cleanTransaction();
			setStandardFailMessage();
			logger.error("Problem encountered removing document track: "
					+ "deal Id = " + pg.getPageDealId() + ", copy Id = "
					+ pg.getPageDealCID() + ", doc track Id = " + docTrackId);
			logger.error(e);
			return;
		}

	}

	public void deleteStandardCondition(int rowIndex) {
		deleteCondition(4, rowIndex);

		getCurrNDPage().setDisplayFieldValue("stTargetStdCon",TARGET_TAG);
	}

	/**
	 * 
	 *  
	 */
	public void deleteCustomCondition(int rowIndex) {
		deleteCondition(5, rowIndex);
		getCurrNDPage().setDisplayFieldValue("stTargetCustCon",TARGET_TAG);
	}

	/**
	 * 
	 *  
	 */
	private void populateFieldValidation(ViewBean thePage) {
		String theExtraHtml = "";

		// the OnChange string to be set on the fields
		HtmlDisplayFieldBase df;

		//// Validation for the special characters ('<', '>', '"', '&' since
		//// they scrude up the document generation.
		df = (HtmlDisplayFieldBase) thePage.getDisplayField("Repeated5/tbCusCondition"); // textarea
		theExtraHtml = "onBlur=\"isFieldHasSpecialChar();\"";
		df.setExtraHtml(theExtraHtml);

		df = (HtmlDisplayFieldBase) thePage.getDisplayField("Repeated5/tbCusDocumentLabel"); // textbox
		theExtraHtml = "onBlur=\"isFieldHasSpecialChar();\"";
		df.setExtraHtml(theExtraHtml);
	}

	/**
	 * 
	 *  
	 */
	private void startSupressSection(String stName, ViewBean thePage) {
		String supressStart = "<!--";

		// suppress
		thePage.setDisplayFieldValue(stName, supressStart);

		return;
	}

	/**
	 * 
	 *  
	 */
	private void endSupressSection(String stName, ViewBean thePage) {
		String supressEnd = "//-->";

		// suppress
		thePage.setDisplayFieldValue(stName, supressEnd);

		return;
	}

	//--Release2.1--TD_DTS_CR--end//

	//--Ticket#681--start--//
	/***************************************************************************
	 * protected void setFunderSection(QueryModelBase dobject, int rowNum) { //
	 * Set Checkbox (Approval) html layout for each row. If a condition is
	 * approved // (DocumentStatusId = 3) the checkbox should be greyed out.
	 * setFunderSectionRow(dobject, "Repeated3/ckFunderDocStatus", rowNum); }
	 */

	protected void setFunderSectionRow(QueryModelBase dobject,
			String chFunderApproval, int rowNum, int statusId) {
		// 1. Getting the corresponding row data from DataObject based on
		// DocumentStatusId (3).
		logger.debug("DTH@setFunderSectionRow::DisableCheckBoxForRowNum: "
				+ rowNum);

		// 2. Close access to a checkbox if a document status is approved.
		String[] repFields = new String[2];
		StringTokenizer rep = new StringTokenizer(chFunderApproval, "/");
		try {
			repFields[0] = rep.nextToken(); // repeatable Name
			repFields[1] = rep.nextToken(); // repeatable FieldName
		} catch (NoSuchElementException ex) {
			logger.debug("DTH@setFunderSectionRow::Exception: " + ex);
		}

		if (repFields[0] != null && !repFields[0].trim().equals("")
				&& repFields[1] != null && !repFields[1].trim().equals("")) {
			// It's a TiledView field
			try {
				TiledViewBase tiledView = (TiledViewBase) getCurrNDPage().getChild(
						repFields[0]);
				tiledView.resetTileIndex();

				tiledView.setTileIndex(rowNum);

				if (statusId == Sc.DOC_STATUS_APPROVED) {
					closeAccessToCheckBox(chFunderApproval);
				} else {
					openAccessToCheckBox(chFunderApproval);
				}
			} catch (Exception e) {
				logger.debug("Exception @setFunderSectionRow"
						+ chFunderApproval + ">, row = " + rowNum
						+ " - ignored :: " + e.toString());
			}
		}

		return;
	}

	private void openAccessToCheckBox(String chbName) {
		try {
			CheckBox checkBox = (CheckBox) getCurrNDPage().getDisplayField(
					chbName);

			//do nothing
			String open = " ";
			checkBox.setExtraHtml(open);

		} catch (Exception e) {
			setStandardFailMessage();
			logger.error("Exception DTH@openAccessToCheckBox: Problem encountered in checkbox customization."
					+ e.getMessage());
		}
	}

	//--Ticket#681--end--//

	// <!-- Catherine, 7-Apr-05, pvcs#817 -->
	public void createConditionNote(PageEntry pe, SessionResourceKit srk)
	throws Exception {
		TextField tbTheNoteText = (TextField) getCurrNDPage().getDisplayField(
		"tbConditionNoteText");

		String theNoteText = tbTheNoteText.getValue().toString();
		if ((theNoteText == null) || theNoteText.trim().equals("")) {
			theNoteText = " "; // insert a record with blank verbiage, according
			// to requirements
		}
		logger.debug("DTH@CreateConditionNote::ConditionNoteText: "
				+ theNoteText);

		// insert note
		int dealId = pe.getPageDealId();
		int userId = theSessionState.getSessionUserId();

		DealPK dealPk = new DealPK(dealId, 1);
		DealNotes dealNotes = new DealNotes(srk);

		String noteText = theNoteText.trim();
		String dbText = getLastConditionNoteText(dealId).trim();

		if (noteText.equals(dbText)) {
			return;
		}

		dealNotes.create(dealPk, dealId, Sc.DEAL_NOTES_CATEGORY_CONDITION,
				noteText, null, userId);
	}

	public String getLastConditionNoteText(int dealId) throws RemoteException {
		String lResult = " ";
		try {
			DealNotes dn = new DealNotes(srk);
			// bilingual deal notes
			lResult = dn.findLastConditionNoteText(dealId, srk.getLanguageId());

			dn = null;
		} catch (Exception e) {
			RemoteException re = new RemoteException("DTH@getLastConditionNoteText: exception");
			logger.error(re.getMessage());
			logger.error(e);
			throw re;
		}

		if (lResult == null) {
			lResult = " ";
		}
		return lResult;
	}
	// <!-- Catherine, 7-Apr-05, pvcs#817 end -->
	
	private void insertConditionUpdateRequest(PageEntry pg) {
        logger.debug("DTH@insertConditionUpdateRequest.  Beginning to insert condition update request.");
        try {
            ConditionUpdateRequester requester = new ConditionUpdateRequester(srk);
            
            int result = requester.insertConditionUpdateRequest(pg.getPageDealId(), true);
            if(result < -1) {
                logger.debug("DTH@insertConditionUpdateRequest.  Failed to insert condition update request.");
            }
            else {
                logger.debug("DTH@insertConditionUpdateRequest. Inserted condition update request successfully.");
            }
        } catch (Exception e) {            
            setStandardFailMessage();
            logger.error(e);
            logger.error("DTH@insertConditionUpdateRequest.  Failed to retrieve deal id.");
            return;
        }   
    }
}
