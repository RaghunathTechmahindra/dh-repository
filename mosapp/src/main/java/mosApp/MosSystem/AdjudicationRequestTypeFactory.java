package mosApp.MosSystem;

import java.text.*;
import java.util.*;

import MosSystem.*;



/**
 * <p>Title: AdjudicationRequestTypeFactory.java </p>
 *
 * <p>Description: factory class for Adjudication type constants hierarchy</p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: filogix</p>
 *
 * @author Vlad Ivanov
 * @version 1.0
 */

public class AdjudicationRequestTypeFactory {
  // TODO: move to Mc constant interface
  private static final String pgAdjudicationRequestNBC = "pgAdjudicationRequestNBC";
  private static final String pgAdjudicationRequestDJ = "pgAdjudicationRequestDJ";

            public AdjudicationRequestType getAdjudicationType(String pgName) {
              if (pgName.equals(pgAdjudicationRequestNBC)) {

                return new NBCAdjudicationRequestType();
              }
              else if (pgName.equals(pgAdjudicationRequestDJ)) {
                return new DJAdjudicationRequestType();
              }

              return null;
            }
    }

