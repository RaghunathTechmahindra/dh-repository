package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doMainViewDownPaymentModelImpl extends QueryModelBase
	implements doMainViewDownPaymentModel
{
	/**
	 *
	 *
	 */
	public doMainViewDownPaymentModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
    while(this.next())
    {
      //Convert DOWNPAYMENTSOURCETYPE Desc.
      this.setDfDownPaymentSource(BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),
          "DOWNPAYMENTSOURCETYPE", this.getDfDownPaymentSource(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDEALID);
	}


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDEALID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDownPaymentAmount()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFDOWNPAYMENTAMOUNT);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentAmount(java.math.BigDecimal value)
	{
		setValue(FIELD_DFDOWNPAYMENTAMOUNT,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCOPYID);
	}


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCOPYID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDownPaymentSource()
	{
		return (String)getValue(FIELD_DFDOWNPAYMENTSOURCE);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentSource(String value)
	{
		setValue(FIELD_DFDOWNPAYMENTSOURCE,value);
	}


	/**
	 *
	 *
	 */
	public String getDfDownPaymentDesc()
	{
		return (String)getValue(FIELD_DFDOWNPAYMENTDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfDownPaymentDesc(String value)
	{
		setValue(FIELD_DFDOWNPAYMENTDESC,value);
	}

      public java.math.BigDecimal getDfInstitutionId() {
    	    return (java.math.BigDecimal) getValue(FIELD_DFINSTITUTIONID);
    	  }
    

      public void setDfInstitutionId(java.math.BigDecimal value) {
        setValue(FIELD_DFINSTITUTIONID, value);
      }
		  



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////
  //--Release2.1--//
  //Modified the SQL to get PickList IDs instead Join the PickList Tables.
  //The PickList Descriptions fields will be repopulated @ "afterExecute" handler.
  //For More details please refer to afterExecute method.
  //--> By Billy 19Nov2002
	public static final String DATA_SOURCE_NAME="jdbc/orcl";
  //--Release2.1--//
  //--> Convert all Description to IDs in string format.
  //--> Remove all PickList tables from the SQL.
	//public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT DOWNPAYMENTSOURCE.DEALID, DOWNPAYMENTSOURCE.AMOUNT, DOWNPAYMENTSOURCE.COPYID, DOWNPAYMENTSOURCETYPE.DPSDESCRIPTION, DOWNPAYMENTSOURCE.DPSDESCRIPTION FROM DOWNPAYMENTSOURCE, DOWNPAYMENTSOURCETYPE  __WHERE__  ";
	public static final String SELECT_SQL_TEMPLATE=
    "SELECT DISTINCT DOWNPAYMENTSOURCE.DEALID, DOWNPAYMENTSOURCE.AMOUNT, "+
    "DOWNPAYMENTSOURCE.COPYID, to_char(DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCETYPEID) DOWNPAYMENTSOURCETYPEID_STR, "+
    "DOWNPAYMENTSOURCE.DPSDESCRIPTION, "+
    "DOWNPAYMENTSOURCE.INSTITUTIONPROFILEID " +
    " FROM DOWNPAYMENTSOURCE "+
    "__WHERE__  ";
	//--Release2.1--//
  //--> Remove all PickList tables from the SQL.
  //public static final String MODIFYING_QUERY_TABLE_NAME="DOWNPAYMENTSOURCE, DOWNPAYMENTSOURCETYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME=
    "DOWNPAYMENTSOURCE";
	//--Release2.1--//
  //--> Remove all PickList tables joins from the SQL.
  //public static final String STATIC_WHERE_CRITERIA=" (DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCETYPEID  =  DOWNPAYMENTSOURCETYPE.DOWNPAYMENTSOURCETYPEID)";
	public static final String STATIC_WHERE_CRITERIA=
    "";

  public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFDEALID="DOWNPAYMENTSOURCE.DEALID";
	public static final String COLUMN_DFDEALID="DEALID";
	public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTAMOUNT="DOWNPAYMENTSOURCE.AMOUNT";
	public static final String COLUMN_DFDOWNPAYMENTAMOUNT="AMOUNT";
	public static final String QUALIFIED_COLUMN_DFCOPYID="DOWNPAYMENTSOURCE.COPYID";
	public static final String COLUMN_DFCOPYID="COPYID";
	public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTDESC="DOWNPAYMENTSOURCE.DPSDESCRIPTION";
	public static final String COLUMN_DFDOWNPAYMENTDESC="DPSDESCRIPTION";

	//--Release2.1--//
  //--> Adjused the Field name definitions to computed field name (ID in string format)
	//public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCE="DOWNPAYMENTSOURCETYPE.DPSDESCRIPTION";
	//public static final String COLUMN_DFDOWNPAYMENTSOURCE="DPSDESCRIPTION";
  public static final String QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCE="DOWNPAYMENTSOURCE.DOWNPAYMENTSOURCETYPEID_STR";
	public static final String COLUMN_DFDOWNPAYMENTSOURCE="DOWNPAYMENTSOURCETYPEID_STR";
      public static final String QUALIFIED_COLUMN_DFINSTITUTIONID =
    	    "DOWNPAYMENTSOURCE.INSTITUTIONPROFILEID";
      public static final String COLUMN_DFINSTITUTIONID = "INSTITUTIONPROFILEID";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDEALID,
				COLUMN_DFDEALID,
				QUALIFIED_COLUMN_DFDEALID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOWNPAYMENTAMOUNT,
				COLUMN_DFDOWNPAYMENTAMOUNT,
				QUALIFIED_COLUMN_DFDOWNPAYMENTAMOUNT,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCOPYID,
				COLUMN_DFCOPYID,
				QUALIFIED_COLUMN_DFCOPYID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOWNPAYMENTSOURCE,
				COLUMN_DFDOWNPAYMENTSOURCE,
				QUALIFIED_COLUMN_DFDOWNPAYMENTSOURCE,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFDOWNPAYMENTDESC,
				COLUMN_DFDOWNPAYMENTDESC,
				QUALIFIED_COLUMN_DFDOWNPAYMENTDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	    FIELD_SCHEMA.addFieldDescriptor(
	            new QueryFieldDescriptor(
	              FIELD_DFINSTITUTIONID,
	              COLUMN_DFINSTITUTIONID,
	              QUALIFIED_COLUMN_DFINSTITUTIONID,
	              java.math.BigDecimal.class,
	              false,
	              false,
	              QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
	              "",
	              QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
	              ""));
	        
	}

}

