package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 *
 *
 *
 */
public interface doDealHistoryModel extends QueryModel, SelectQueryModel
{
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfDealId();

	
	/**
	 * 
	 * 
	 */
	public void setDfDealId(java.math.BigDecimal value);
    
//    /**
//     * 
//     * 
//     */
//    public void setDfInstitutionId(java.math.BigDecimal value);
//    
//    /**
//     * 
//     * 
//     */
//    public java.math.BigDecimal getDfInstitutionId();

	
	/**
	 * 
	 * 
	 */
	public java.math.BigDecimal getDfApplicationId();

	
	/**
	 * 
	 * 
	 */
	public void setDfApplicationId(java.math.BigDecimal value);

	
	/**
	 * 
	 * 
	 */
	public String getDfStatus();

	
	/**
	 * 
	 * 
	 */
	public void setDfStatus(String value);

	
	/**
	 * 
	 * 
	 */
	public java.sql.Timestamp getDfTransactionDate();

	
	/**
	 * 
	 * 
	 */
	public void setDfTransactionDate(java.sql.Timestamp value);

	
	/**
	 * 
	 * 
	 */
	public String getDfTransactionText();

	
	/**
	 * 
	 * 
	 */
	public void setDfTransactionText(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfUser();

	
	/**
	 * 
	 * 
	 */
	public void setDfUser(String value);

	
	/**
	 * 
	 * 
	 */
	public String getDfTransactionType();

	
	/**
	 * 
	 * 
	 */
	public void setDfTransactionType(String value);
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////
	
	public static final String FIELD_DFDEALID="dfDealId";
    //public static final String FIELD_DFINSTITUTIONID="dfInstitutionId";
	public static final String FIELD_DFAPPLICATIONID="dfApplicationId";
	public static final String FIELD_DFSTATUS="dfStatus";
	public static final String FIELD_DFTRANSACTIONDATE="dfTransactionDate";
	public static final String FIELD_DFTRANSACTIONTEXT="dfTransactionText";
	public static final String FIELD_DFUSER="dfUser";
	public static final String FIELD_DFTRANSACTIONTYPE="dfTransactionType";
	
	
	
	
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////
	
}

