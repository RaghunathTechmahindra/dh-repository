package mosApp.MosSystem;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 * Title: doComponentSummaryModelImpl
 * <p>
 * Description: Implementation of doComponentSummaryModel Interface.
 * @author MCM Impl Team.
 * @version 1.0 09-June-2008 XS_16.7 Initial version
 */
public class doComponentSummaryModelImpl extends QueryModelBase implements
        doComponentSummaryModel
{

    public static final String DATA_SOURCE_NAME = "jdbc/orcl";

    public static final String SELECT_SQL_TEMPLATE = "SELECT ALL DEAL.TOTALLOANAMOUNT,COMPONENTSUMMARY.DEALID,COMPONENTSUMMARY.COPYID,COMPONENTSUMMARY.INSTITUTIONPROFILEID,COMPONENTSUMMARY.TOTALAMOUNT,COMPONENTSUMMARY.TOTALCASHBACKAMOUNT FROM COMPONENTSUMMARY,DEAL  __WHERE__  ";

    public static final String MODIFYING_QUERY_TABLE_NAME = "COMPONENTSUMMARY,DEAL ";

    public static final String STATIC_WHERE_CRITERIA = "COMPONENTSUMMARY.DEALID=DEAL.DEALID AND COMPONENTSUMMARY.COPYID=DEAL.COPYID ";

    public static final QueryFieldSchema FIELD_SCHEMA = new QueryFieldSchema();

    public static final String QUALIFIED_COLUMN_DFDEALID = "COMPONENTSUMMARY.DEALID";

    public static final String COLUMN_DFDEALID = "DEALID";

    public static final String QUALIFIED_COLUMN_DFISTITUTIONPROFILEID = "COMPONENTSUMMARY.INSTITUTIONPROFILEID";

    public static final String COLUMN_DFISTITUTIONPROFILEID = "INSTITUTIONPROFILEID";

    public static final String QUALIFIED_COLUMN_DFCOPYID = "COMPONENTSUMMARY.COPYID";

    public static final String COLUMN_DFCOPYID = "COPYID";

    public static final String QUALIFIED_COLUMN_DFTOTALAMOUNT = "COMPONENTSUMMARY.TOTALAMOUNT";

    public static final String COLUMN_DFTOTALAMOUNT = "TOTALAMOUNT";

    public static final String QUALIFIED_COLUMN_DFTOTALCASHBACKAMOUNT = "COMPONENTSUMMARY.TOTALCASHBACKAMOUNT";

    public static final String COLUMN_DFTOTALCASHBACKAMOUNT = "TOTALCASHBACKAMOUNT";

    public static final String QUALIFIED_COLUMN_DFUNALLOCATEDAMOUNT = "DEAL.TOTALLOANAMOUNT";

    public static final String COLUMN_DFUNALLOCATEDAMOUNT = "TOTALLOANAMOUNT";

    static
    {

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFDEALID, COLUMN_DFDEALID, QUALIFIED_COLUMN_DFDEALID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));
        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFUNALLOCATEDAMOUNT, COLUMN_DFUNALLOCATEDAMOUNT,
                QUALIFIED_COLUMN_DFUNALLOCATEDAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFCOPYID, COLUMN_DFCOPYID, QUALIFIED_COLUMN_DFCOPYID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFISNTITUTIONID, COLUMN_DFISTITUTIONPROFILEID,
                QUALIFIED_COLUMN_DFISTITUTIONPROFILEID,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALAMOUNT, COLUMN_DFTOTALAMOUNT,
                QUALIFIED_COLUMN_DFTOTALAMOUNT, java.math.BigDecimal.class,
                false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

        FIELD_SCHEMA.addFieldDescriptor(new QueryFieldDescriptor(
                FIELD_DFTOTALCASHBACKAMOUNT, COLUMN_DFTOTALCASHBACKAMOUNT,
                QUALIFIED_COLUMN_DFTOTALCASHBACKAMOUNT,
                java.math.BigDecimal.class, false, false,
                QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE, "",
                QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE, ""));

    }

    /**
     * Instantiates a new do component summary model impl.
     */
    public doComponentSummaryModelImpl()
    {
        super();
        setDataSourceName(DATA_SOURCE_NAME);

        setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

        setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

        setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

        setFieldSchema(FIELD_SCHEMA);

        initialize();

    }

    /**
     * Initialize.
     */
    public void initialize()
    {

    }

    /**
     * BeforeExecute
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param sql
     *            the sql
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected String beforeExecute(ModelExecutionContext context,
            int queryType, String sql) throws ModelControlException
    {

        return sql;

    }

    /**
     * This method is used to handle any display logic after executing the
     * model.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void afterExecute(ModelExecutionContext context, int queryType)
            throws ModelControlException
    {

        String defaultInstanceStateName = getRequestContext().getModelManager()
                .getDefaultModelInstanceName(SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) getRequestContext()
                .getModelManager().getModel(SessionStateModel.class,
                        defaultInstanceStateName, true);
        int languageId = theSessionState.getLanguageId();
        int institutionId = theSessionState.getDealInstitutionId();

        if ((this.getDfUnallocatedAmount() != null)
                && (this.getDfTotalAmount() != null))
        {

            this.setDfUnallocatedAmount(new BigDecimal(this
                    .getDfUnallocatedAmount().doubleValue()
                    - this.getDfTotalAmount().doubleValue()));

        }

        while (this.next())
        {

        }

        // Reset Location
        this.beforeFirst();

    }

    /**
     * OnDatabaseError.
     * @param context
     *            the context
     * @param queryType
     *            the query type
     * @param exception
     *            the exception
     * @return list of model model[]
     * @throws ModelControlException
     *             the model control exception
     * @version 1.0 10-June-2008 XS_16.7 Initial version
     */
    protected void onDatabaseError(ModelExecutionContext context,
            int queryType, SQLException exception)
    {

    }

    public java.math.BigDecimal getDfCopyId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFCOPYID);

    }

    public void setDfCopyId(java.math.BigDecimal value)
    {

        setValue(FIELD_DFCOPYID, value);

    }

    public BigDecimal getDfInstitutionId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFISNTITUTIONID);

    }

    public void setDfInstitutionId(BigDecimal value)
    {

        setValue(FIELD_DFISNTITUTIONID, value);

    }

    public BigDecimal getDfTotalCashBackAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFTOTALCASHBACKAMOUNT);

    }

    public void setDfTotalCashBackAmount(BigDecimal value)
    {

        setValue(FIELD_DFTOTALCASHBACKAMOUNT, value);

    }

    public void setDfTotalAmount(BigDecimal value)
    {

        setValue(FIELD_DFTOTALAMOUNT, value);

    }

    public java.math.BigDecimal getDfTotalAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFTOTALAMOUNT);

    }

    public BigDecimal getDfDealId()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFDEALID);

    }

    public void setDfDealId(BigDecimal value)
    {

        setValue(FIELD_DFDEALID, value);

    }

    public BigDecimal getDfUnallocatedAmount()
    {

        return (java.math.BigDecimal) getValue(FIELD_DFUNALLOCATEDAMOUNT);

    }

    public void setDfUnallocatedAmount(BigDecimal value)
    {

        setValue(FIELD_DFUNALLOCATEDAMOUNT, value);

    }

}
