package mosApp.MosSystem;

import java.util.Hashtable;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.html.StaticTextField;

import static mosApp.MosSystem.MasterWorkQueueSearchHandler.PAGE_STATE_TABLE_KEY_MWQS_CRITERIA;

public class pgMWorkQueueSearchRepeated2TiledView extends RequestHandlingTiledViewBase implements TiledView, RequestHandler {

	public static final String CHILD_STPRIORITYLABEL = "stPriorityLabel";
	public static final String CHILD_STPRIORITYLABEL_RESET_VALUE = "";
	public static final String CHILD_STPRIORITYCOUNT = "stPriorityCount";
	public static final String CHILD_STPRIORITYCOUNT_RESET_VALUE = "";

	private doCountTasksbyPriorityModel2 doCountTasksbyPriority;
	private MasterWorkQueueSearchHandler handler = new MasterWorkQueueSearchHandler();

	public pgMWorkQueueSearchRepeated2TiledView(View parent, String name) {
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass(doCountTasksbyPriorityModel2.class);
		registerChildren();
	}

	public void resetChildren() {
		((StaticTextField) getChild(CHILD_STPRIORITYLABEL)).setValue(CHILD_STPRIORITYLABEL_RESET_VALUE);
		((StaticTextField) getChild(CHILD_STPRIORITYCOUNT)).setValue(CHILD_STPRIORITYCOUNT_RESET_VALUE);
	}

	protected void registerChildren() {
		registerChild(CHILD_STPRIORITYLABEL, StaticTextField.class);
		registerChild(CHILD_STPRIORITYCOUNT, StaticTextField.class);
	}

	public Model[] getWebActionModels(int executionType) {
		switch (executionType) {
		case MODEL_TYPE_RETRIEVE:
			return new Model[] { getdoCountTasksbyPriorityModel2() };
		}
		return new Model[0];
	}

	public boolean beforeModelExecutes(Model model, int executionContext) {
		MasterWorkQueueSearchHandler handler = (MasterWorkQueueSearchHandler) this.handler.cloneSS();
		handler.pageGetState(this.getParentViewBean());
		PageEntry pg = handler.theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		StringBuffer sb = new StringBuffer();
		String filterCriteria = (String) pst.get(PAGE_STATE_TABLE_KEY_MWQS_CRITERIA);
		if (filterCriteria != null && filterCriteria.trim().length() > 0)
			sb.append(filterCriteria);
		String pageConditions = handler.getPageConditions(pg, pst);
		if (pageConditions != null && pageConditions.trim().length() > 0) {
			if (sb.length() > 0)
				sb.append(" AND ");
			sb.append(pageConditions);
		}
		if (sb.length() > 0) {
			doCountTasksbyPriorityModel2Impl m = (doCountTasksbyPriorityModel2Impl) model;
			m.setStaticWhereCriteriaString(doCountTasksbyPriorityModel2Impl.STATIC_WHERE_CRITERIA + " AND " + sb.toString());
		}
		handler.pageSaveState();
		return super.beforeModelExecutes(model, executionContext);
	}

	protected View createChild(String name) {
		if (CHILD_STPRIORITYLABEL.equals(name)) {
			return new StaticTextField(this, getdoCountTasksbyPriorityModel2(), CHILD_STPRIORITYLABEL, doCountTasksbyPriorityModel2.FIELD_DFPRIORITYLABEL, CHILD_STPRIORITYLABEL_RESET_VALUE, null);
		} else if (CHILD_STPRIORITYCOUNT.equals(name)) {
			return new StaticTextField(this, getdoCountTasksbyPriorityModel2(), CHILD_STPRIORITYCOUNT, doCountTasksbyPriorityModel2.FIELD_DFPRIORITYCOUNT, CHILD_STPRIORITYCOUNT_RESET_VALUE, null);
		}
		return super.createChild(name);
	}

	private doCountTasksbyPriorityModel2 getdoCountTasksbyPriorityModel2() {
		if (doCountTasksbyPriority == null)
			doCountTasksbyPriority = (doCountTasksbyPriorityModel2) getModel(doCountTasksbyPriorityModel2.class);
		return doCountTasksbyPriority;
	}

}
