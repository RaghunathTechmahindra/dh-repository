package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *   WARNING - ALERT
 *
 *   BJH - Nov 18, 2002
 *
 *   Model not referenced in application .. i.e. not used is dead!
 *
 */
public interface doDealRateLockModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


    /**
     *
     *
     */
    public java.math.BigDecimal getDfCopyId();


    /**
     *
     *
     */
    public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfRateLock();


	/**
	 *
	 *
	 */
	public void setDfRateLock(String value);




	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFRATELOCK="dfRateLock";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

