package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;


/**
 *
 *
 *
 */
public interface doCusConditionModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfCusCondition();


	/**
	 *
	 *
	 */
	public void setDfCusCondition(String value);


	/**
	 *
	 *
	 */
	public String getDfDocumentLabel();


	/**
	 *
	 *
	 */
	public void setDfDocumentLabel(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAction();


	/**
	 *
	 *
	 */
	public void setDfAction(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfResponsibility();


	/**
	 *
	 *
	 */
	public void setDfResponsibility(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCusDocTrackId();


	/**
	 *
	 *
	 */
	public void setDfCusDocTrackId(java.math.BigDecimal value);


  /**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLanguagePrederenceId();


	/**
	 *
	 *
	 */
	public void setDfLanguagePrederenceId(java.math.BigDecimal value);
	

	public String getDfDefCondition();
	public void setDfDefCondition(String value);
	public java.math.BigDecimal getDfConditionId();
	public void setDfConditionId(java.math.BigDecimal value);
	public String getDfUseDefaultCondition();
	public void setDfUseDefaultCondition(String value);
	public String getDfDisplayDefault();
	public void setDfDisplayDefault(String value);


	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFCUSCONDITION="dfCusCondition";
//================================================================
//CLOB Performance Enhancement June 2010
	public static final String FIELD_DFCUSCONDITIONLITE="dfCusConditionVarchar";
//================================================================
	public static final String FIELD_DFDOCUMENTLABEL="dfDocumentLabel";
	public static final String FIELD_DFACTION="dfAction";
	public static final String FIELD_DFRESPONSIBILITY="dfResponsibility";
	public static final String FIELD_DFCUSDOCTRACKID="dfCusDocTrackId";
	//--Release2.1--//
  //--> Added LanguagePreferenceId DataField
	public static final String FIELD_DFLANGUAGEPREFERENCEID="dfLanguagePrederenceId";
	
	//4.3 GR
	public static final String FIELD_DFDEFCONDITION = "dfDefCondition";
	//================================================================
	//CLOB Performance Enhancement June 2010
	public static final String FIELD_DFDEFCONDITION_VARCHAR="dfDefConditionVarchar";
	public static final String FIELD_DFDEFCONDITION_LENGTH="dfDefConditionLength";
	//================================================================
	public static final String FIELD_DFCONDITIONID = "dfConditionId";
	public static final String FIELD_DFUSEDEFAULT = "dfUseDefaultCondition";
	public static final String FIELD_DFDISPLAYDEFAULT = "dfDisplayDefault";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

