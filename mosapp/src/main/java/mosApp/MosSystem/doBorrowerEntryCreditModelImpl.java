package mosApp.MosSystem;

import java.sql.SQLException;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;


/**
 *
 *
 *
 */
public class doBorrowerEntryCreditModelImpl extends QueryModelBase
	implements doBorrowerEntryCreditModel
{
	/**
	 *
	 *
	 */
	public doBorrowerEntryCreditModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFBORROWERID);
	}


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFBORROWERID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditRefId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITREFID);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditRefId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITREFID,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditRefTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITREFTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditRefTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITREFTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCreditRefInstitutionName()
	{
		return (String)getValue(FIELD_DFCREDITREFINSTITUTIONNAME);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditRefInstitutionName(String value)
	{
		setValue(FIELD_DFCREDITREFINSTITUTIONNAME,value);
	}


	/**
	 *
	 *
	 */
	public String getDfCreditRefAccountNumber()
	{
		return (String)getValue(FIELD_DFCREDITREFACCOUNTNUMBER);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditRefAccountNumber(String value)
	{
		setValue(FIELD_DFCREDITREFACCOUNTNUMBER,value);
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditRefCurrentBalance()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFCREDITREFCURRENTBALANCE);
	}


	/**
	 *
	 *
	 */
	public void setDfCreditRefCurrentBalance(java.math.BigDecimal value)
	{
		setValue(FIELD_DFCREDITREFCURRENTBALANCE,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	public static final String SELECT_SQL_TEMPLATE="SELECT DISTINCT CREDITREFERENCE.BORROWERID, " +
            "CREDITREFERENCE.CREDITREFERENCEID, CREDITREFERENCE.CREDITREFTYPEID, " +
            "CREDITREFERENCE.INSTITUTIONNAME, CREDITREFERENCE.ACCOUNTNUMBER, " +
            "CREDITREFERENCE.CURRENTBALANCE FROM CREDITREFERENCE, BORROWER, DEAL  __WHERE__  ";
	public static final String MODIFYING_QUERY_TABLE_NAME="CREDITREFERENCE";
	public static final String STATIC_WHERE_CRITERIA
        ="(CREDITREFERENCE.INSTITUTIONPROFILEID (+) = BORROWER.INSTITUTIONPROFILEID) " +
                "AND (DEAL.INSTITUTIONPROFILEID = BORROWER.INSTITUTIONPROFILEID";
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFBORROWERID="CREDITREFERENCE.BORROWERID";
	public static final String COLUMN_DFBORROWERID="BORROWERID";
	public static final String QUALIFIED_COLUMN_DFCREDITREFID="CREDITREFERENCE.CREDITREFERENCEID";
	public static final String COLUMN_DFCREDITREFID="CREDITREFERENCEID";
	public static final String QUALIFIED_COLUMN_DFCREDITREFTYPEID="CREDITREFERENCE.CREDITREFTYPEID";
	public static final String COLUMN_DFCREDITREFTYPEID="CREDITREFTYPEID";
	public static final String QUALIFIED_COLUMN_DFCREDITREFINSTITUTIONNAME="CREDITREFERENCE.INSTITUTIONNAME";
	public static final String COLUMN_DFCREDITREFINSTITUTIONNAME="INSTITUTIONNAME";
	public static final String QUALIFIED_COLUMN_DFCREDITREFACCOUNTNUMBER="CREDITREFERENCE.ACCOUNTNUMBER";
	public static final String COLUMN_DFCREDITREFACCOUNTNUMBER="ACCOUNTNUMBER";
	public static final String QUALIFIED_COLUMN_DFCREDITREFCURRENTBALANCE="CREDITREFERENCE.CURRENTBALANCE";
	public static final String COLUMN_DFCREDITREFCURRENTBALANCE="CURRENTBALANCE";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFBORROWERID,
				COLUMN_DFBORROWERID,
				QUALIFIED_COLUMN_DFBORROWERID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITREFID,
				COLUMN_DFCREDITREFID,
				QUALIFIED_COLUMN_DFCREDITREFID,
				java.math.BigDecimal.class,
				true,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITREFTYPEID,
				COLUMN_DFCREDITREFTYPEID,
				QUALIFIED_COLUMN_DFCREDITREFTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITREFINSTITUTIONNAME,
				COLUMN_DFCREDITREFINSTITUTIONNAME,
				QUALIFIED_COLUMN_DFCREDITREFINSTITUTIONNAME,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITREFACCOUNTNUMBER,
				COLUMN_DFCREDITREFACCOUNTNUMBER,
				QUALIFIED_COLUMN_DFCREDITREFACCOUNTNUMBER,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFCREDITREFCURRENTBALANCE,
				COLUMN_DFCREDITREFCURRENTBALANCE,
				QUALIFIED_COLUMN_DFCREDITREFCURRENTBALANCE,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

