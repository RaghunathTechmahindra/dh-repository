package mosApp.MosSystem;
/**
 * Nov 13, 2008, FXP23399 MCM changed optionList for Status combo to be populated all the time.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.basis100.log.SysLogger;
import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.event.RequestInvocationEvent;
import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;


public class pgDocTrackingRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDocTrackingRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doDocTrackSignedCommitmentModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStSignDocumentLabel().setValue(CHILD_STSIGNDOCUMENTLABEL_RESET_VALUE);
		getCbSignDocStatus().setValue(CHILD_CBSIGNDOCSTATUS_RESET_VALUE);
		getStSignDocStatusDate().setValue(CHILD_STSIGNDOCSTATUSDATE_RESET_VALUE);
		getStSignByPass().setValue(CHILD_STSIGNBYPASS_RESET_VALUE);
		getStSignDueDate().setValue(CHILD_STSIGNDUEDATE_RESET_VALUE);
		getStSignResponsibility().setValue(CHILD_STSIGNRESPONSIBILITY_RESET_VALUE);
		getHdSignDocTrackId().setValue(CHILD_HDSIGNDOCTRACKID_RESET_VALUE);
		getHdSignCopyId().setValue(CHILD_HDSIGNCOPYID_RESET_VALUE);
		getHdSignRoleId().setValue(CHILD_HDSIGNROLEID_RESET_VALUE);
		getHdSignRequestDate().setValue(CHILD_HDSIGNREQUESTDATE_RESET_VALUE);
		getHdSignStatusDate().setValue(CHILD_HDSIGNSTATUSDATE_RESET_VALUE);
		getHdSignDocStatus().setValue(CHILD_HDSIGNDOCSTATUS_RESET_VALUE);
		getHdSignDocText().setValue(CHILD_HDSIGNDOCTEXT_RESET_VALUE);
		getHdSignResponsibility().setValue(CHILD_HDSIGNRESPONSIBILITY_RESET_VALUE);
		getHdSignDocumentLabel().setValue(CHILD_HDSIGNDOCUMENTLABEL_RESET_VALUE);
		getBtSignDetails().setValue(CHILD_BTSIGNDETAILS_RESET_VALUE);
    //// One one more hidden field added to escape the over-massaging of
    //// an appropriate model in the Handler.
		getHdByPass().setValue(CHILD_HDBYPASS_RESET_VALUE);

	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STSIGNDOCUMENTLABEL,StaticTextField.class);
		registerChild(CHILD_CBSIGNDOCSTATUS,ComboBox.class);
		registerChild(CHILD_STSIGNDOCSTATUSDATE,StaticTextField.class);
		registerChild(CHILD_STSIGNBYPASS,StaticTextField.class);
		registerChild(CHILD_STSIGNDUEDATE,StaticTextField.class);
		registerChild(CHILD_STSIGNRESPONSIBILITY,StaticTextField.class);
		registerChild(CHILD_HDSIGNDOCTRACKID,HiddenField.class);
		registerChild(CHILD_HDSIGNCOPYID,HiddenField.class);
		registerChild(CHILD_HDSIGNROLEID,HiddenField.class);
		registerChild(CHILD_HDSIGNREQUESTDATE,HiddenField.class);
		registerChild(CHILD_HDSIGNSTATUSDATE,HiddenField.class);
		registerChild(CHILD_HDSIGNDOCSTATUS,HiddenField.class);
		registerChild(CHILD_HDSIGNDOCTEXT,HiddenField.class);
		registerChild(CHILD_HDSIGNRESPONSIBILITY,HiddenField.class);
		registerChild(CHILD_HDSIGNDOCUMENTLABEL,HiddenField.class);
		registerChild(CHILD_BTSIGNDETAILS,Button.class);
    //// One one more hidden field added to escape the over-massaging of
    //// an appropriate model in the Handler.
		registerChild(CHILD_HDBYPASS,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
//				modelList.add(getdoDocTrackSignedCommitmentModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();
    boolean ret = true;

    int rowNum = this.getTileIndex();

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.pageGetState(this.getParentViewBean());

    //logger = SysLog.getSysLogger("pgDTR1TV");
    //logger.debug("pgDTR1TV@nextTile::rowNum: " + rowNum);

    ret = handler.checkDisplayThisRow("Repeated1");
    //logger.debug("pgDTR1TV@nextTile::RetValue: " + ret);

    QueryModelBase theObj =(QueryModelBase) getdoDocTrackSignedCommitmentModel();

    //logger.debug("pgDTR1TV@nextTile::Size: " + theObj.getSize());
    //logger.debug("pgMWQ1TV@nextTile::Location: " + theObj.getLocation());

    if (theObj.getNumRows() > 0 && ret == true)
    ////if (ret == true)
    {
        handler.populateDisplayRow(rowNum,
                                    "Repeated1",
                                    doDocTrackSignedCommitmentModel.class,
                                    DocTrackingHandler.SECTION_SIGNED_COMMITMENT);
		}

		handler.pageSaveState();

    return ret;
		}

		return movedToRow;


		// The following code block was migrated from the Repeated1_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		int retval = SKIP;

		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		if ((retval = handler.checkDisplayThisRow("Repeated1")) == PROCEED) handler.populateDisplayRow(event.getRowIndex(), "Repeated1", "doDocTrackSignedCommitment", DocTrackingHandler.SECTION_SIGNED_COMMITMENT);

		handler.pageSaveState();

		return retval;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STSIGNDOCUMENTLABEL))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_STSIGNDOCUMENTLABEL,
				doDocTrackSignedCommitmentModel.FIELD_DFDOCUMENTLABEL,
				CHILD_STSIGNDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_CBSIGNDOCSTATUS))
		{
			ComboBox child = new ComboBox( this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_CBSIGNDOCSTATUS,
				doDocTrackSignedCommitmentModel.FIELD_DFDOCSTATUSID,
				CHILD_CBSIGNDOCSTATUS_RESET_VALUE,
				null);
			child.setLabelForNoneSelected("");
			//FXP23399 MCM Nov 13, 2008, being thread un-safe, this OptionList should be populted by handler
			//child.setOptions(cbSignDocStatusOptions);
			return child;
		}
		else
		if (name.equals(CHILD_STSIGNDOCSTATUSDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_STSIGNDOCSTATUSDATE,
				doDocTrackSignedCommitmentModel.FIELD_DFSTATUSCHANGEDATE,
				CHILD_STSIGNDOCSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSIGNBYPASS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STSIGNBYPASS,
				CHILD_STSIGNBYPASS,
				CHILD_STSIGNBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSIGNDUEDATE))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_STSIGNDUEDATE,
				doDocTrackSignedCommitmentModel.FIELD_DFDOCDUEDATE,
				CHILD_STSIGNDUEDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STSIGNRESPONSIBILITY))
		{
			StaticTextField child = new StaticTextField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_STSIGNRESPONSIBILITY,
				doDocTrackSignedCommitmentModel.FIELD_DFRESPONSIBILITY,
				CHILD_STSIGNRESPONSIBILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNDOCTRACKID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNDOCTRACKID,
				doDocTrackSignedCommitmentModel.FIELD_DFDOCTRACKID,
				CHILD_HDSIGNDOCTRACKID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNCOPYID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNCOPYID,
				doDocTrackSignedCommitmentModel.FIELD_DFCOPYID,
				CHILD_HDSIGNCOPYID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNROLEID))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNROLEID,
				doDocTrackSignedCommitmentModel.FIELD_DFROLEID,
				CHILD_HDSIGNROLEID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNREQUESTDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNREQUESTDATE,
				doDocTrackSignedCommitmentModel.FIELD_DFREQUESTDATE,
				CHILD_HDSIGNREQUESTDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNSTATUSDATE))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNSTATUSDATE,
				doDocTrackSignedCommitmentModel.FIELD_DFSTATUSCHANGEDATE,
				CHILD_HDSIGNSTATUSDATE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNDOCSTATUS))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNDOCSTATUS,
				doDocTrackSignedCommitmentModel.FIELD_DFSTATUS,
				CHILD_HDSIGNDOCSTATUS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNDOCTEXT))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNDOCTEXT,
				doDocTrackSignedCommitmentModel.FIELD_DFDOCUMENTTEXT,
				CHILD_HDSIGNDOCTEXT_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNRESPONSIBILITY))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNRESPONSIBILITY,
				doDocTrackSignedCommitmentModel.FIELD_DFRESPONSIBILITY,
				CHILD_HDSIGNRESPONSIBILITY_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDSIGNDOCUMENTLABEL))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDSIGNDOCUMENTLABEL,
				doDocTrackSignedCommitmentModel.FIELD_DFDOCUMENTLABEL,
				CHILD_HDSIGNDOCUMENTLABEL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_BTSIGNDETAILS))
		{
			Button child = new Button(
				this,
				getDefaultModel(),
				CHILD_BTSIGNDETAILS,
				CHILD_BTSIGNDETAILS,
				CHILD_BTSIGNDETAILS_RESET_VALUE,
				null);
				return child;

		}
    //// One one more hidden field added to escape the over-massaging of
    //// an appropriate model in the Handler.
		else
		if (name.equals(CHILD_HDBYPASS))
		{
			HiddenField child = new HiddenField(this,
				getdoDocTrackSignedCommitmentModel(),
				CHILD_HDBYPASS,
				doDocTrackSignedCommitmentModel.FIELD_DFBYPASS,
				CHILD_HDBYPASS_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSignDocumentLabel()
	{
		return (StaticTextField)getChild(CHILD_STSIGNDOCUMENTLABEL);
	}


	/**
	 *
	 *
	 */
	public ComboBox getCbSignDocStatus()
	{
		return (ComboBox)getChild(CHILD_CBSIGNDOCSTATUS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSignDocStatusDate()
	{
		return (StaticTextField)getChild(CHILD_STSIGNDOCSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSignByPass()
	{
		return (StaticTextField)getChild(CHILD_STSIGNBYPASS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSignDueDate()
	{
		return (StaticTextField)getChild(CHILD_STSIGNDUEDATE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStSignResponsibility()
	{
		return (StaticTextField)getChild(CHILD_STSIGNRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignDocTrackId()
	{
		return (HiddenField)getChild(CHILD_HDSIGNDOCTRACKID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignCopyId()
	{
		return (HiddenField)getChild(CHILD_HDSIGNCOPYID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignRoleId()
	{
		return (HiddenField)getChild(CHILD_HDSIGNROLEID);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignRequestDate()
	{
		return (HiddenField)getChild(CHILD_HDSIGNREQUESTDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignStatusDate()
	{
		return (HiddenField)getChild(CHILD_HDSIGNSTATUSDATE);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignDocStatus()
	{
		return (HiddenField)getChild(CHILD_HDSIGNDOCSTATUS);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignDocText()
	{
		return (HiddenField)getChild(CHILD_HDSIGNDOCTEXT);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignResponsibility()
	{
		return (HiddenField)getChild(CHILD_HDSIGNRESPONSIBILITY);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdSignDocumentLabel()
	{
		return (HiddenField)getChild(CHILD_HDSIGNDOCUMENTLABEL);
	}

	/**
	 *
	 *    One one more hidden field added to escape the over-massaging of
   *    an appropriate model in the Handler.
	 */
	public HiddenField getHdByPass()
	{
		return (HiddenField)getChild(CHILD_HDBYPASS);
	}

	/**
	 *
	 *
	 */
	public void handleBtSignDetailsRequest(RequestInvocationEvent event)
		throws ServletException, IOException
	{
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this.getParentViewBean());

		handler.navigateToDetail(DocTrackingHandler.SECTION_SIGNED_COMMITMENT,
                              handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();


		// The following code block was migrated from the btSignDetails_onWebEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		DocTrackingHandler handler =(DocTrackingHandler) this.handler.cloneSS();

		handler.preHandlerProtocol(this);

		handler.navigateToDetail(DocTrackingHandler.SECTION_SIGNED_COMMITMENT, handler.getRowNdxFromWebEventMethod(event));

		handler.postHandlerProtocol();

		return;
		*/
	}


	/**
	 *
	 *
	 */
	public Button getBtSignDetails()
	{
		return (Button)getChild(CHILD_BTSIGNDETAILS);
	}


	/**
	 *
	 *
	 */
	public doDocTrackSignedCommitmentModel getdoDocTrackSignedCommitmentModel()
	{
		if (doDocTrackSignedCommitment == null)
			doDocTrackSignedCommitment = (doDocTrackSignedCommitmentModel) getModel(doDocTrackSignedCommitmentModel.class);
		return doDocTrackSignedCommitment;
	}


	/**
	 *
	 *
	 */
	public void setdoDocTrackSignedCommitmentModel(doDocTrackSignedCommitmentModel model)
	{
			doDocTrackSignedCommitment = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STSIGNDOCUMENTLABEL="stSignDocumentLabel";
	public static final String CHILD_STSIGNDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_CBSIGNDOCSTATUS="cbSignDocStatus";
	public static final String CHILD_CBSIGNDOCSTATUS_RESET_VALUE="";
	////private static OptionList cbSignDocStatusOptions=new OptionList(new String[]{},new String[]{});
	//FXP23399 MCM Nov 13, 2008, being thread un-safe, this OptionList should be populted by handler
	//protected static OptionList cbSignDocStatusOptions=new OptionList(new String[]{},new String[]{});
	public static final String CHILD_STSIGNDOCSTATUSDATE="stSignDocStatusDate";
	public static final String CHILD_STSIGNDOCSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_STSIGNBYPASS="stSignByPass";
	public static final String CHILD_STSIGNBYPASS_RESET_VALUE="";
	public static final String CHILD_STSIGNDUEDATE="stSignDueDate";
	public static final String CHILD_STSIGNDUEDATE_RESET_VALUE="";
	public static final String CHILD_STSIGNRESPONSIBILITY="stSignResponsibility";
	public static final String CHILD_STSIGNRESPONSIBILITY_RESET_VALUE="";
	public static final String CHILD_HDSIGNDOCTRACKID="hdSignDocTrackId";
	public static final String CHILD_HDSIGNDOCTRACKID_RESET_VALUE="";
	public static final String CHILD_HDSIGNCOPYID="hdSignCopyId";
	public static final String CHILD_HDSIGNCOPYID_RESET_VALUE="";
	public static final String CHILD_HDSIGNROLEID="hdSignRoleId";
	public static final String CHILD_HDSIGNROLEID_RESET_VALUE="";
	public static final String CHILD_HDSIGNREQUESTDATE="hdSignRequestDate";
	public static final String CHILD_HDSIGNREQUESTDATE_RESET_VALUE="";
	public static final String CHILD_HDSIGNSTATUSDATE="hdSignStatusDate";
	public static final String CHILD_HDSIGNSTATUSDATE_RESET_VALUE="";
	public static final String CHILD_HDSIGNDOCSTATUS="hdSignDocStatus";
	public static final String CHILD_HDSIGNDOCSTATUS_RESET_VALUE="";
	public static final String CHILD_HDSIGNDOCTEXT="hdSignDocText";
	public static final String CHILD_HDSIGNDOCTEXT_RESET_VALUE="";
	public static final String CHILD_HDSIGNRESPONSIBILITY="hdSignResponsibility";
	public static final String CHILD_HDSIGNRESPONSIBILITY_RESET_VALUE="";
	public static final String CHILD_HDSIGNDOCUMENTLABEL="hdSignDocumentLabel";
	public static final String CHILD_HDSIGNDOCUMENTLABEL_RESET_VALUE="";
	public static final String CHILD_BTSIGNDETAILS="btSignDetails";
	public static final String CHILD_BTSIGNDETAILS_RESET_VALUE=" ";
  //// One one more hidden field added to escape the over-massaging of
  //// an appropriate model in the Handler.
	public static final String CHILD_HDBYPASS="hdByPass";
	public static final String CHILD_HDBYPASS_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doDocTrackSignedCommitmentModel doDocTrackSignedCommitment=null;
	private DocTrackingHandler handler=new DocTrackingHandler();
  public SysLogger logger;

}

