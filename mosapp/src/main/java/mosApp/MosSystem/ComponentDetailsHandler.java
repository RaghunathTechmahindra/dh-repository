package mosApp.MosSystem;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.security.PassiveMessage;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.pk.ComponentSummaryPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.ActiveMsgFactory;
import com.basis100.deal.security.PDC;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.entity.Page;
import com.filogix.express.core.ExpressRuntimeException;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.iplanet.jato.util.TypeConverter;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.CheckBox;
import com.iplanet.jato.view.html.ComboBox;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.Option;
import com.iplanet.jato.view.html.OptionList;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * <p>
 * Title: ComponentDetailsHandler.java
 * </p>
 * <p>
 * Description: Handler Class for Component Details screen with Jato Framework
 * </p>
 * 
 * 
 * @author MCM Team
 * @version 1.0 (Initial Version � June 10, 2008)
 * @version 1.1 added LOC section - XS_2.33
 * @version 1.2 added Mortgage section - XS_2.27
 * @version 1.3 fixed MI Allocate Flag enable/disable functionality - XS_2.27
 * @version 1.4 added SaveEntity method for XS_2.30, fixed bugs
 * @version 1.5 modified SaveEntity method not to use methods for each tiled
 *          section
 * @version 1.5.1 June 26, 2008: XS_2.28, XS_2.29: Dynamic drop down for MTG
 *          section and base function for dynamic drop down list added.
 * @version 1.6 Deleted handleRecalcMtgComp , handleRecalcLOCComodified as
 *          they are not needed
 * @version 1.7 fixed artf732409
 * @version 1.7.1 July 02, 2008: XS_2.34: Dynamic drop down for LOC section
 *          added.
 * @version 1.8 03-July-2008 XS_2.25 setupBeforePageGeneration()method is
 *          modified to set the where criteria for doComponentSummaryModelImpl.
 * @version 1.9 July 03, 2008: XS_2.38: Dynamic drop down for Overdraft section
 *          added.
 * @version 1.9 fixed artf733712, artf733710
 * @version 1.10 handleRecalc() method has been updated for calling the
 *          calculation.
 * @version 1.11 fixed artf736687
 * @version 1.12 added MIAllocation/PropertyTaxAllocation JS Handling
 * @version 1.13 July 8, 2008 XS_2.36, XS_2.37: Dynamic drop down for Loan and
 *          CreditCard section added.
 * @version 1.13 Modified SaveEntity(..) method
 * @version 1.14 check box error
 * @version 1.15 prepareComponentLOCDisplayFields - Changed the JS for
 *          tbAdditionalInfoLOC : artf736674
 * @version 1.16 prepareComponentMTGDisplayFields - Changed the JS for Amort
 *          period, Effect Amort, Additional Info, Actual Payment Term, Rate
 *          Guuarantee Period : artf740067
 * @version 1.17 added Loan section - XS_2.37 Modified
 *          setupBeforePageGeneration, addDynamicDropDownExtraHtmlForLoan,
 *          executeCriteriaLoanModel, SaveEntity
 * @version 1.18 July 10, 2008: artf738341: deleted usage of
 *          PageHandlerCommon.customizeTextBox for payment term description
 *          because it is not necessary. Making payment term description
 *          read-only is done by JSP.
 * @version 1.19 July 11, 2008: artf741023 Added line separator for LOC section
 *          and Credit Card section. Plus, aligned cosmetic look and feel.
 * @version 1.20 July 11, 2008: artf742087: added fix for Credit Card Additional
 *          Info Field Has No 256 Limit
 * @version 1.21 fixed artf742919
 * @version 1.22 July 11, 2008: XS 2.39/40 modified HandleCheckRule
 * @version 1.23 added Add Component section - XS_2.26 Modified - 
 *			Added - handleAddComponent() , createMortgageComponent(Component comp), createLocComponent(Component comp)
 *					createLoanComponent(Component comp),createCreditCardComponent(Component comp),createOverdraftComponent(Component comp)
 * @version 1.24 moved location of setting condition3 = true after Add Component (XS 2.31)
 *               deleted unused code
 * @version 1.25 modified prepareComponentLoanDisplayFields for artf745007
 * @version 1.26 modified error/warning message popup condition based on artf743632
 * @version 1.27 delete Component section - XS_2.32 Modified - 
 *          changed - handleDeleteCCComp() , handleDeleteLoanComp(), handleDeleteMTGComp()
 *                  handleDeleteOverDraftomp(Component comp), handleCustomActMessageOk(String[] args), deleteComponentSummary()
 * @version 1.28 XS_2.32 change add CONFIRMDELETE, CONFIRM_DELETE_COMPONENT
 * @version 1.29 fixed for artf751756: change to call handleCustomActMessageOk
 * @version 1.30 artf763316 Aug 21, 2008 : replacing XS_2.60.
 * @version 1.31 artf763359, artf763351, artf763355 Aug 26, 2008
 * @version 1.32 artf771395, roll backed missing line which was deleted with rev.3613
 * @version 1.33 Oct 7, 2008 : FXP22741, changed business rule validation for compoents.
 */

public class ComponentDetailsHandler extends DealHandlerCommon
implements Cloneable, Sc {

    private final static Log _log = 
        LogFactory.getLog(ComponentDetailsHandler.class);
    /**
     * <p>constructor</p>
     */
    public ComponentDetailsHandler() {
        super();
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * <p>constructor</p>
     * @param name:String
     */
    public ComponentDetailsHandler(String name) {
        this(null,name); // No parent
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * <p>constructor</p>
     * @param parent: View
     * @param name: String
     */
    protected ComponentDetailsHandler(View parent, String name) {
        super(parent,name);
        theSessionState = super.theSessionState;
        savedPages = super.savedPages;
    }

    /**
     * <p>cloneSS</p>
     */
    public ComponentDetailsHandler cloneSS() {
        return(ComponentDetailsHandler) super.cloneSafeShallow();
    }

    /**
     * <p>SaveEntity</p>
     * <p>save screen data for each component to the db<p>
     * 
     * since MCM XS_2.30, XS_2.33
     * @version 1.1 08-Jul-2008 uncommented the doCalculation method call
     * @version 1.2 10-Jul-2008 Modified to save Loan, Credit card, Overdraft component data
     * 
     * @param webPage: ViewBean
     */
    public void SaveEntity(ViewBean webPage) throws Exception {

        SessionResourceKit srk = getSessionResourceKit();

        CalcMonitor dcm = CalcMonitor.getMonitor(srk);

        //-- ComponentMortgage Tile section starts --//
        try {
            String mtgPrefix = pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP + "/";

            String [ ] compPropkeys = {  
                    mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_HDCOMPONENTIDMTG, 
                    mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_HDCOPYIDMTG 
            };
            handleUpdateComponentMTG(compPropkeys, dcm);
        } catch (Exception ex) {
            _log.error("Exception @ComponentDetailsHandler:SaveData:save Component in MTG ");
            _log.error(ex);
            _log.error(StringUtil.stack2string(ex));
            throw ex;
        }
        //-- ComponentMortgage Tile section ends --//

        //-- ComponentLOC Tile section starts --//
        try {
            String [ ] complockeys = {
                    "RepeatedLOC/hdComponentIDLOC", "RepeatedLOC/hdCopyIDLOC"
            };
            handleUpdateComponentLOC(complockeys, dcm);
        } catch(Exception ex)  {
            _log.error("Exception @ComponentDetailsHandler:SaveData:save Component in LOC ");
            _log.error(ex);
            _log.error(StringUtil.stack2string(ex));
            throw ex;
        }
        //-- ComponentLOC Tile section ends --//

        //-- ComponentCreditCard Tile section starts --//
        try {
            String ccPrefix = pgComponentDetailsViewBean.CHILD_TILEDCCCOMP + "/";

            String [ ] compPropkeys = {  
                    ccPrefix + pgComponentCreditCardTiledView.CHILD_HDCOMPONENTID, 
                    ccPrefix + pgComponentCreditCardTiledView.CHILD_HDCOPYID 
            };
            handleUpdateComponentCC(compPropkeys, dcm);
        } catch(Exception ex)  {
            _log.error("Exception @ComponentDetailsHandler:SaveData:save Component in Credit Card ");
            _log.error(ex);
            _log.error(StringUtil.stack2string(ex));
            throw ex;
        }


        /***************MCM Impl team changes starts - XS_2.38*********************/
        try {
            String ccPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDOVERDRAFT + "/";

            String [ ] compPropkeys = {  
                    ccPrefix + pgComponentDetailsOverdraftTiledView.CHILD_HDCOMPONENTID, 
                    ccPrefix + pgComponentDetailsOverdraftTiledView.CHILD_HDCOPYID 
            };
            handleUpdateComponentOverdraft(compPropkeys, dcm);

        } catch(Exception ex)  {
            _log.error("Exception @ComponentDetailsHandler:SaveData:save Component in Overdraft ");
            _log.error(ex);
            _log.error(StringUtil.stack2string(ex));
            throw ex;
        }
        /***************MCM Impl team changes ends - XS_2.38*********************/

        //-- ComponentLoan Tile section XS 2.37 starts--//
        try {
            String [ ] complockeys = {
                    "RepeatedLoan/hdComponentIDLoan", "RepeatedLoan/hdCopyIDLoan"
            };
            handleUpdateComponentLoan(complockeys, dcm);
        } catch(Exception ex)  {
            _log.error("Exception @ComponentDetailsHandler:SaveData:save Component in Loan ");
            _log.error(ex);
            _log.error(StringUtil.stack2string(ex));
            throw ex;
        }
        //-- ComponentLOC Tile section XS 2.37ends --//

        //-- do calc
        // ---Version 1.1 uncommented the doCalculation method Starts---//
        try {         
            doCalculation(dcm);
            //---Version 1.1 uncommented the doCalculation method Ends ---//
        } catch(Exception ex)  {
            _log.error("Exception @ComponentDetailsHandler:SaveData: doCald");
            _log.error(ex);
            _log.error(StringUtil.stack2string(ex));
            throw ex;
        }

    }

    public void saveMtgComps(ViewBean bean, CalcMonitor dcm)
    throws Exception {

    }
    /**
     * <p>populatePageDisplayFields</p>
     * <p>Description: set values for common fields</p>
     * @version 1.1 artf763359, artf763351, artf763355 Aug 26, 2008
     */
    public void populatePageDisplayFields() {

        PageEntry pg = theSessionState.getCurrentPage();
        Hashtable pst = pg.getPageStateTable();
        populatePageShellDisplayFields();
        //logger.debug("PEH@populatePageDisplayFields::after populatePageShellDisplayFields");

        populateTaskNavigator(pg);
        //logger.debug("PEH@populatePageDisplayFields::after populateTaskNavigator");

        populatePageDealSummarySnapShot();
        //logger.debug("PEH@populatePageDisplayFields::after populatePageDealSummarySnapShot");

        populatePreviousPagesLinks();
        //logger.debug("PEH@populatePageDisplayFields::after populatePreviousPageLinks");

        ViewBean thePage = getCurrNDPage();
        int dealId = pg.getPageDealId();
        int copyId = pg.getPageDealCID();

        setCCAPSServicingMortgageNumber();

        /***************MCM Impl team changes starts - XS_2.28*********************/
        Deal deal = null;
        try {
            deal = new Deal(srk, null, dealId, copyId);
        } catch (Exception e) {
            _log.error(e);
            _log.error(StringUtil.stack2string(e));
            throw new ExpressRuntimeException(e);
        }

        thePage.setDisplayFieldValue(
                pgComponentDetailsViewBean.CHILD_STLENDERPROFILEID, 
                deal.getLenderProfileId());
        /***************MCM Impl team changes ends - XS_2.28*********************/

        /****** artf763359, artf763351, artf763355 starts *******/
        DealPK dealPk = new DealPK(dealId, copyId);
        Collection componentColl = null;
        try {
            Component comp = new Component(srk);
            componentColl= comp.findByDeal(dealPk);
        } catch (Exception e) {
            e.printStackTrace();
        } 
        if(componentColl.size() < 1) {
            double totalAmount = deal.getTotalLoanAmount();           
            thePage.setDisplayFieldValue(pgComponentDetailsViewBean.CHILD_STUNALLOCATEDAMOUNT, Double
                    .toString(totalAmount));
            thePage.setDisplayFieldValue(pgComponentDetailsViewBean.CHILD_STTOTALAMOUNT, Double
                    .toString(0.0));
            thePage.setDisplayFieldValue(pgComponentDetailsViewBean.CHILD_STTOTALCASHBACKAMOUNT, Double
                    .toString(0.0));
        }
        /****** artf763359, artf763351, artf763355 ends *******/
        
        //FXP23869, MCM/4.1, Jan 20 2009, saving/resetting screen scroll position - start.
        reapplyWindowScrollPosition();
        //FXP23869, MCM/4.1, Jan 20 2009, saving/resetting screen scroll position - end.
        
    }

    //Method to populate default value data to the HTML
    private void populateDefaultsVALSData(ViewBean thePage) {
        //String onChange = "";

        // the OnChange string to be set on the ComboBox
        //HtmlDisplayFieldBase df;
        thePage.setDisplayFieldValue("stVALSData", VALS_DATA);
    }
    /**
     * 
     * @version 1.8 03-July-2008 XS_2.25 Modified to add user where cretiria for doComponentSummaryModelImpl.
     * @version 1.17 10-July-2008 XS_2.37, XS 2.36, 2.38 Modified to add user where cretiria for Loan, Credit card and Overdraft
     */

    public void setupBeforePageGeneration() {
        PageEntry pg = theSessionState.getCurrentPage();

        if (pg.getSetupBeforeGenerationCalled() == true) return;
        pg.setSetupBeforeGenerationCalled(true);

        setupUWDealSummarySnapShotDO(pg);

        int dealId = pg.getPageDealId();
        int copyId = theSessionState.getCurrentPage().getPageDealCID();

        if (dealId <= 0)
        {
            setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
            logger.error("Problem encountered displaying deal Entry - invalid dealId (" + dealId + ")");
            return;
        }
        if (copyId <= 0)
        {
            setActiveMessageToAlert(ActiveMsgFactory.ISFATAL);
            logger.error("Problem encountered displaying deal Entry - invalid copyId (" + copyId + ")");
            return;
        }

        /***************MCM Impl team changes starts - XS_2.27, 2.28a *******************/
        executeCriteriaMTGModel(pg, dealId, copyId );
        /***************MCM Impl team changes ends - XS_2.27, 2.28a *******************/


        /**2.33**/
        QueryModelBase theComponentDo = (QueryModelBase) RequestManager
        .getRequestContext().getModelManager().getModel(
                doComponentLocModel.class);
        theComponentDo.clearUserWhereCriteria();
        theComponentDo.addUserWhereCriterion("dfDealId", "=", new Integer(
                dealId));
        theComponentDo.addUserWhereCriterion("dfCopyId", "=", new Integer(
                copyId));
        /**2.33**/

        /***************MCM Impl team changes starts - XS_2.36 *******************/
        executeCriteriaCreditCardModel(pg, dealId, copyId );
        /***************MCM Impl team changes ends - XS_2.36 *******************/

        /***************MCM Impl team changes starts - XS_2.38 *******************/
        executeCriteriaOverdraftModel(pg, dealId, copyId );
        /***************MCM Impl team changes ends - XS_2.38 *******************/

        /***************MCM Impl team changes starts - XS_2.37 *******************/
        executeCriteriaLoanModel(pg, dealId, copyId );
        /***************MCM Impl team changes ends - XS_2.37 *******************/

        logger.trace("--T--> @ComponentDetailsHandler.setupBeforePageGeneration:: DealId=" + dealId + ", copyId=" + copyId);
        /*******************MCM Impl Team XS_2.25 changes starts***************************************/
        QueryModelBase componentSummary = (QueryModelBase) RequestManager
        .getRequestContext().getModelManager().getModel(
                doComponentSummaryModel.class);
        componentSummary.clearUserWhereCriteria();
        componentSummary.addUserWhereCriterion("dfDealId", "=", new Integer(
                dealId));
        componentSummary.addUserWhereCriterion("dfCopyId", "=", new Integer(
                copyId));
        /*******************MCM Impl Team XS_2.25 changes ends***************************************/
    }

    private static String VALS_DATA = "";
    // To initial the JavaScript Array values when the Class loaded up the first time user access the screen

    /**
     * <p>executeCriteriaMTGModel</p>
     * <p>Description: execute Component Mortgage Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * XS_2.27
     */
    protected void executeCriteriaMTGModel(PageEntry pg, int dealId, int copyId) {

        doComponentMortgageModel theDO = (
                doComponentMortgageModel)
                (RequestManager.getRequestContext().getModelManager().getModel(
                        doComponentMortgageModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(theDO.FIELD_DFDEALID, "=",
                String.valueOf(dealId));
        theDO.addUserWhereCriterion(theDO.FIELD_DFCOPYID, "=",
                String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaMTGModel", false);
    }

    /**
     * <p>setMTGCompDisplayFields</p>
     * <p>Description: setup component Mortgage Tile view </p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * XS_2.27
     */
    protected void setMTGCompDisplayFields(int rowNum)
    {
        doComponentMortgageModel dobject = (doComponentMortgageModel) (RequestManager
                .getRequestContext().getModelManager()
                .getModel(doComponentMortgageModel.class));
        try{ 
            dobject.setLocation(rowNum);  
        } catch (Exception e){
            _log.error(StringUtil.stack2string(e));
            throw new ExpressRuntimeException(e.getMessage(), e);
        }

        String tileViewPrefix = pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP + "/"; 
        //String tileViewPrefix0 = pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP; 
        //String viewBeanClass = "pgComponentDetails_";
        
        /***************MCM Impl team changes starts - artf741023 *********************/
        populateSectionTitleAndSectionDevider(
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_STMTGTITLE, 
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_IMMTGSECTIONDEVIDER, 
                rowNum, Sc.COMPONENT_TYPE_MORTGAGE);
        /***************MCM Impl team changes ends - artf741023 *********************/

        CheckBox chMIAllocate = (CheckBox)getCurrNDPage().getDisplayField(
                pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP + "/" 
                + pgComponentDetailsMTGTiledView.CHILD_CHALLOCATEMIPREM);
        String miUpfront = (String)dobject.getValue(
                doComponentMortgageModelImpl.FIELD_DFMIUPFRONT);
        String miAllocateFlag = (String)dobject.getValue(doComponentMortgageModelImpl.FIELD_DFMIALLOCAGTEFLAG);
        Object miPremiumO = dobject.getValue(doComponentMortgageModelImpl.FIELD_DFMIPREMIUM);
        boolean miAllocateChecked = false;
        if ("N".equalsIgnoreCase(miUpfront)){
            miAllocateChecked = "Y".equalsIgnoreCase(miAllocateFlag);
            chMIAllocate.setChecked(miAllocateChecked);
        } else {
            chMIAllocate.setChecked(false);
            chMIAllocate.setExtraHtml("disabled='true' class='greyedDisabled' ");
        }
        StaticTextField stMIPremium = (StaticTextField)getCurrNDPage().getDisplayField(
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_STMIPREMIUM);
        if (miAllocateChecked && miPremiumO!=null) {
            stMIPremium.setValue(new BigDecimal(miPremiumO.toString()));
        } else {
            stMIPremium.setValue("0.0");
        }

        setTermsDisplayField(dobject, tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_TXAMORTIZATIONPERIODYEARS,
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_TXAMORTIZATIONPERIODMONTHS, 
                doComponentMortgageModelImpl.FIELD_DFAMORTIZATIONTERM, rowNum);

        setTermsDisplayField(dobject, tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_TXEFFECTAPYEARS, 
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_TXEFFECTAPMONTHS, 
                doComponentMortgageModelImpl.FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS, rowNum);

        setTermsDisplayField(dobject, tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_TXACTUALPAYMENTTERMYEARS, 
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_TXACTUALPAYMENTTERMMONTHS, 
                doComponentMortgageModelImpl.FIELD_DFACTUALPAYMENTTERM, rowNum);

        fillupMonths(tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_CBFIRSTPAYMENTDATEMONTH);
        setDateDisplayField(dobject, tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_CBFIRSTPAYMENTDATEMONTH, 
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEDAY, 
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEYEAR, 
                doComponentMortgageModel.FIELD_DFFIRSTPAYMENTDATE, rowNum);

        //delete options in component type combo box except for "mtg" - starts
        ComboBox cbComponentType = (ComboBox) getCurrNDPage().getDisplayField( 
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_CBCOMPONENTTYPE);
        populateComponentCbComponentTypeCommon(cbComponentType, Sc.COMPONENT_TYPE_MORTGAGE);
        //delete options in component type combo box except for "mtg" - ends

        CheckBox chAllocateTaxEscrow = (CheckBox)getCurrNDPage().getDisplayField(
                pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP + "/" 
                + pgComponentDetailsMTGTiledView.CHILD_CHALLOCATETAXESCROW);

        BigDecimal taxPayorId = (BigDecimal)dobject.getValue(
                doComponentMortgageModelImpl.FIELD_DFTAXPAYORID);
        String allocateEscrow = (String)dobject.getValue(
                doComponentMortgageModelImpl.FIELD_DFPROPERTYTAXALLOCATEFLAG);
        boolean taxEscrowChecked = false;
        //artf732409 

        if (taxPayorId != null 
                && Sc.TAX_PAYOR_LENDER == taxPayorId.intValue() ) {
            taxEscrowChecked = "Y".equalsIgnoreCase(allocateEscrow);
            chAllocateTaxEscrow.setChecked(taxEscrowChecked);
        } else {
            chAllocateTaxEscrow.setChecked(false);
            chAllocateTaxEscrow.setExtraHtml("disabled='true' class='greyedDisabled' ");
        }
        Object propertyTaxO = dobject.getValue(doComponentMortgageModelImpl.FIELD_DFESCROWPAYMENTAMOUNT);
        StaticTextField stPropertyTax = (StaticTextField)getCurrNDPage().getDisplayField(
                tileViewPrefix + pgComponentDetailsMTGTiledView.CHILD_STPROPERTYTAX);
        if (taxEscrowChecked && propertyTaxO != null) {
            stPropertyTax.setValue((BigDecimal)propertyTaxO);
        } else {
            stPropertyTax.setValue("0.0");
        }

//      MCM Impl Team: artf736687 STARTS 
        TextField stMtgProd = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix 
                + pgComponentDetailsMTGTiledView.CHILD_STMTGPRODUCT);
        Object dfMtgProdId = dobject.getValue(doComponentMortgageModelImpl.FIELD_DFMTGPRODTID);
        stMtgProd.setValue((BigDecimal)dfMtgProdId);

        TextField stPostedInterestRate = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix 
                + pgComponentDetailsMTGTiledView.CHILD_STPOSTEDINTERESTRATE);
        Object dfPostedInterestedRate = dobject.getValue(doComponentMortgageModelImpl.FIELD_DFPRICINGRATEINVENTORYID);
        stPostedInterestRate.setValue((BigDecimal)dfPostedInterestedRate);

        TextField stRepaymentType = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix 
                + pgComponentDetailsMTGTiledView.CHILD_STREPAYMENTTYPE);
        Object dfRepaymentTypeId = dobject.getValue(doComponentMortgageModelImpl.FIELD_DFREPAYMENTTYPEID);
        stRepaymentType.setValue((BigDecimal)dfRepaymentTypeId);

        TextField stRateLock = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix 
                + pgComponentDetailsMTGTiledView.CHILD_STRATELOCK);
        Object dfRateLock = dobject.getValue(doComponentMortgageModelImpl.FIELD_DFRATELOCK);
        stRateLock.setValue((String)dfRateLock);
//      MCM Impl Team: artf736687 ENDS 


        prepareComponentMTGDisplayFields(rowNum);       

        /***************MCM Impl team changes starts - XS_2.28*********************/
        addDynamicDropDownExtraHtmlForMtg(rowNum);
        /***************MCM Impl team changes ends - XS_2.28*********************/
    }

    /**
     * <p>displayCancelButton</p>
     * <p>determin if each cancel button should be displayed or not being called 
     * by endBtCancelDisplay@pgComponentDetailsViewBean and 
     * endBtCurrentCancelDisplay@pgComponentDetailsViewBean</p>
     * <p>PageCondtion3 is used for it: false means first entry
     *                                  true means refresh within page
     * @param type: int 1 (Cancel), 2 (Cancel current changes only) 
     * @return boolean: ture display, false not display
     * 
     * @see pgComponentDetailsViewBean
     */
    public boolean displayCancelButton(int type) {

        PageEntry pg = theSessionState.getCurrentPage();
        Page cdPageEntity = getPageEntity( pg.getPageId() );
        if (type == 1 && (!cdPageEntity.getEditViaTxCopy() && pg.getPageCondition3())) {
            return false;
        }
        if (type == 2 && (cdPageEntity.getEditViaTxCopy() || !pg.getPageCondition3())) {
            return false;
        }
        return displayCancelButton();
    }

    /**
     * <p>executeCriteriaOverdraftModel</p>
     * <p>Description: execute Component Overdraft Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * XS_2.38
     */
    protected void executeCriteriaOverdraftModel(PageEntry pg, int dealId,
            int copyId) {

        doComponentOverDraftModel theDO = (doComponentOverDraftModel) 
        (RequestManager.getRequestContext().getModelManager().getModel(
                doComponentOverDraftModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(doComponentOverDraftModel.FIELD_DFDEALID,
                "=", String.valueOf(dealId));
        theDO.addUserWhereCriterion(doComponentOverDraftModel.FIELD_DFCOPYID,
                "=", String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaOverdraftModel", false);
    }

    /**
     * <p>prepareComponentMTGDisplayFields</p>
     * <p>Description: pupulate javascript validation component Motrgage Section
     * 
     * @param int rowIndex
     * @version 1.1 fixed artf733712, artf733710
     * @version 1.2 fixed artf740067
     */
    protected void prepareComponentMTGDisplayFields(int rowIndex) {

        String mtgPrefix = pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP + "/";

        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField(mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXMORTGAGEAMOUNT);
        String jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid);

        // fix for artf740067 - Starts//
        df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField(mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXAMORTIZATIONPERIODYEARS);
        jsValid = "onBlur=\"if(isFieldInteger(0)) isFieldValidDuration('"
            + pgComponentDetailsMTGTiledView.CHILD_TXAMORTIZATIONPERIODYEARS + "','" 
            + pgComponentDetailsMTGTiledView.CHILD_TXAMORTIZATIONPERIODMONTHS +"');\"";
        df.setExtraHtml(jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField(mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXAMORTIZATIONPERIODMONTHS);
        jsValid = "onBlur=\"if(isFieldInteger(0)) isFieldValidDuration('"
            + pgComponentDetailsMTGTiledView.CHILD_TXAMORTIZATIONPERIODYEARS + "','" 
            + pgComponentDetailsMTGTiledView.CHILD_TXAMORTIZATIONPERIODMONTHS +"');\"";
        df.setExtraHtml(jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField(mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXEFFECTAPYEARS);


        df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField(mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXACTUALPAYMENTTERMYEARS);
        jsValid = "onBlur=\"if(isFieldInteger(0)) isFieldValidDuration('"
            + pgComponentDetailsMTGTiledView.CHILD_TXACTUALPAYMENTTERMYEARS + "','" 
            + pgComponentDetailsMTGTiledView.CHILD_TXACTUALPAYMENTTERMMONTHS +"');\"";
        df.setExtraHtml(jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField(mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXACTUALPAYMENTTERMMONTHS);
        jsValid = "onBlur=\"if(isFieldInteger(0)) isFieldValidDuration('"
            + pgComponentDetailsMTGTiledView.CHILD_TXACTUALPAYMENTTERMYEARS + "','" 
            + pgComponentDetailsMTGTiledView.CHILD_TXACTUALPAYMENTTERMMONTHS +"');\"";
        df.setExtraHtml(jsValid);
        //fix for artf740067 - Ends//
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_CBFIRSTPAYMENTDATEMONTH);
        String extraId = "id=' " + pgComponentDetailsMTGTiledView.CHILD_CBFIRSTPAYMENTDATEMONTH + rowIndex + "'";
        jsValid = " onBlur=\"isFieldValidDate('" 
            + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEYEAR + "', "
            + "'" + pgComponentDetailsMTGTiledView.CHILD_CBFIRSTPAYMENTDATEMONTH + "', "
            + "'" + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEDAY + "');\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEDAY);
        extraId = "id='" + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEDAY + rowIndex + "'";
        jsValid = " onBlur=\"if(isFieldInIntRange(1, 31)) "
            + "isFieldValidDate('"
            + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEYEAR + "', "
            + "'" + pgComponentDetailsMTGTiledView.CHILD_CBFIRSTPAYMENTDATEMONTH + "', "
            + "'" + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEDAY + "');\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEYEAR);
        extraId = "id='" + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEYEAR + rowIndex + "'";
        jsValid = " onBlur=\"if(isFieldInIntRange(1800, 2100)) "
            + "isFieldValidDate('"
            + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEYEAR + "', "
            + "'" + pgComponentDetailsMTGTiledView.CHILD_CBFIRSTPAYMENTDATEMONTH + "', "
            + "'" + pgComponentDetailsMTGTiledView.CHILD_TXFIRSTPAYMENTDATEDAY + "');\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXCASHBACKINDOLLARS);
        extraId = "id='" + pgComponentDetailsMTGTiledView.CHILD_TXCASHBACKINDOLLARS + rowIndex + "'";
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXCASHBACKINPERCENTAGE);
        extraId = "id='" + pgComponentDetailsMTGTiledView.CHILD_TXCASHBACKINPERCENTAGE + rowIndex + "'";
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 100)) isFieldDecimal(2);\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXDISCOUNT);
        extraId = "id='" + pgComponentDetailsMTGTiledView.CHILD_TXDISCOUNT + rowIndex + "'";
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 100)) isFieldDecimal(3);\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXPREMIUM);
        extraId = "id='" + pgComponentDetailsMTGTiledView.CHILD_TXPREMIUM + rowIndex + "'";
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 100)) isFieldDecimal(3);\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXBUYDOWN);
        extraId = "id='" + pgComponentDetailsMTGTiledView.CHILD_TXBUYDOWN + rowIndex + "'";
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 100)) isFieldDecimal(3);\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXHOLDBACKAMOUNT);
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXADDITIONALPIPAY);
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXADDITIONALINFO);
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() && isFieldMaxLength(256));\"";
        df.setExtraHtml(jsValid);
        
        //fix for artf740067 - Starts//
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXRATEGUARANTEEPERIOD);
        extraId = "id='" + pgComponentDetailsMTGTiledView.CHILD_TXRATEGUARANTEEPERIOD + rowIndex + "'";
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 999)) isFieldInteger(0);\"";
        df.setExtraHtml(extraId + jsValid);
        //fix for artf740067 - Ends//
       
        /**
         * BugFix: artf763640 Starts
         */
        //validation for CommisionCode
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXCOMMISIONCODE);
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() );\"";
        df.setExtraHtml(jsValid);
        
        //validation for existing account reference
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                mtgPrefix + pgComponentDetailsMTGTiledView.CHILD_TXEXISTINGACOUNTREF);
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() );\"";
        df.setExtraHtml(jsValid);
        
        /**
         * BugFix: artf763640 Ends
         */
    }

    /**
     * <p>setCCAPSServicingMortgageNumber</p>
     * <p>Description: set data into CCAPS and ServicingMortgageNumber if BMO. 
     * called by pgComonentDetailsViewBean</p>
     * 
     * @param  
     * @return 
     * 
     * @see pgComponentDetailsViewBean
     */
    protected void setCCAPSServicingMortgageNumber() {
        doUWDealSummarySnapShotModel myModel = (doUWDealSummarySnapShotModel)(RequestManager.getRequestContext().getModelManager().
                getModel(doUWDealSummarySnapShotModel.class));
        String servicingMortgageNumber = null;

        try {
            if (myModel.getSize() > 0) {
                Object myObj = myModel.getDfServicingMortgageNumber();
                if (myObj == null) {
                    servicingMortgageNumber = "";
                } else {
                    servicingMortgageNumber = myObj.toString();
                }
            } else {
                logger.error(
                "UWH@setCCAPSServicingMortgageNumber(): Problem when getting ServicingMortgageNumber info !");
            }

        } catch (ModelControlException mce) {
            String strError = "UWH@setCCAPSServicingMortgageNumber(): " + mce.getMessage();
            logger.error(mce);
            logger.error(strError);
        }

        logger.debug("UWH@setCCAPSServicingMortgageNumber(): servicingMortgageNumber = " + servicingMortgageNumber);

        boolean isBMO = false;
        if (PropertiesCache.getInstance().getProperty(theSessionState.getDealInstitutionId(), COM_BASIS100_FXP_CLIENTID_BMO, "N").equals("Y")) {
            isBMO = true;
        }

        if (isBMO) {
            getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STCCAPS, "CCAPS: ");
            getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STSERVICINGMORTGAGENUMBER, servicingMortgageNumber);
        } else {
            getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STCCAPS, "");
            getCurrNDPage().setDisplayFieldValue(pgUWorksheetViewBean.CHILD_STSERVICINGMORTGAGENUMBER, "");
        }
    }

    /**
     * <p>handleCheckRule</p>
     * <p>Description: Handle clicking Check Rule.
     * PageCondition3 controls display Cancel button  
     * 
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @see pgComponentDetailsViewBean
     * 
     * @version 1.1 July 11, 2008 XS 2.39/40 
     * @version 1.2 July 16, modified error/warning message popup condition based on artf743632
     */
    public void handleCheckRule() {

        PageEntry pg = theSessionState.getCurrentPage();
        if (pg.editable) {
            try {
                Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID());  
                pg.setPageCondition3(true);
                /***************MCM Impl team changes starts - FXP22741*******************/
                // PassiveMessage pm = validateComponent(pg, srk);
                PassiveMessage pm = validateComponents(pg, srk);
                /***************MCM Impl team changes ends - FXP22741*********************/
                // Display the Rule message if any
                if (pm != null && pm.getNumMessages() > 0) {
                    pm.setGenerate(true);
                    theSessionState.setPasMessage(pm);
                }
                return;
            } catch(Exception e) {
                srk.cleanTransaction();
                logger.error("Exception @handleConditionsReview().");
                logger.error(e);
                setStandardFailMessage();
            }
        }

        setActiveMessageToAlert(BXResources.getSysMsg(DISALLOWED_DURING_VIEW_ONLY_MODE, theSessionState.getLanguageId()),
                ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
    }



    /**
     * <p>handleDeleteMTGComp</p>
     * <p>Description: Handle clicking handleDeleteMTGComp.
     * PageCondition3 controls display Cancel button  
     * 
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @see pgComponentDetailsViewBean
     */
    public void handleDeleteMTGComp(int index) {
        PageEntry pg = theSessionState.getCurrentPage();
        if (pg.editable) {
            pg.setPageCondition3(true);
            
            /****************MCM Impl Team XS_2.32 changes starts********************/                 
            String ccPrefix = pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP  + "/";
            
            handleDelete(false,  Sc.CONFIRM_DELETE_COMPONENT,
                                 index,
                                 ccPrefix + pgComponentDetailsMTGTiledView.CHILD_HDCOMPONENTIDMTG,
                                 "ComponentId",
                                 ccPrefix + pgComponentDetailsMTGTiledView.CHILD_HDCOPYIDMTG ,
                                 "Component");
            /****************MCM Impl Team XS_2.32 changes ends********************/  
            return;
        }

        setActiveMessageToAlert(BXResources.getSysMsg(DISALLOWED_DURING_VIEW_ONLY_MODE, theSessionState.getLanguageId()),
                ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
    }

    /**
     * <p>handleDeleteLOCComp</p>
     * <p>Description: Handle clicking handleDeleteLOCComp.
     * PageCondition3 controls display Cancel button  
     * 
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @see pgComponentDetailsViewBean
     */
    public void handleDeleteLOCComp(int index) {
        PageEntry pg = theSessionState.getCurrentPage();
        if (pg.editable) {
            pg.setPageCondition3(true);
            
            
            /****************MCM Impl Team XS_2.32 changes starts********************/                 
            String ccPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDLOC + "/";
            
            handleDelete(false,  Sc.CONFIRM_DELETE_COMPONENT,
                                 index,
                                 ccPrefix + pgComponentDetailsLOCTiledView.CHILD_HDCOMPONENTID,
                                 "ComponentId",
                                 ccPrefix + pgComponentDetailsLOCTiledView.CHILD_HDCOPYID,
                                 "Component");
            /****************MCM Impl Team XS_2.32 changes ends********************/  
            return;
        }

        setActiveMessageToAlert(BXResources.getSysMsg(DISALLOWED_DURING_VIEW_ONLY_MODE, theSessionState.getLanguageId()),
                ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
    }

    /**
     * <p>handleDeleteOverdraftComp</p>
     * <p>Description: Handle clicking delete button in overdraft section.
     * PageCondition3 controls display Cancel button  
     * 
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @see pgComponentDetailsViewBean
     */
    public void handleDeleteOverdraftComp(int index) {
        PageEntry pg = theSessionState.getCurrentPage();
        if (pg.editable) {
            pg.setPageCondition3(true);
            
            /****************MCM Impl Team XS_2.32 changes starts********************/                 
            String ccPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDOVERDRAFT + "/";
            
            handleDelete(false,  Sc.CONFIRM_DELETE_COMPONENT,
                                 index,
                                 ccPrefix + pgComponentDetailsOverdraftTiledView.CHILD_HDCOMPONENTID,
                                 "ComponentId",
                                 ccPrefix + pgComponentDetailsOverdraftTiledView.CHILD_HDCOPYID,
                                 "Component");
            /****************MCM Impl Team XS_2.32 changes ends********************/  
            return;
        }

        setActiveMessageToAlert(BXResources.getSysMsg(
                DISALLOWED_DURING_VIEW_ONLY_MODE, theSessionState
                .getLanguageId()), ActiveMsgFactory.ISCUSTOMCONFIRM);
    }

    /**
     * <p>handleDeleteCCComp</p>
     * <p>Description: Handle clicking handleDeleteCCComp.
     * PageCondition3 controls display Cancel button  
     * 
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @see pgComponentDetailsViewBean
     */
    public void handleDeleteCCComp(int index) {
        PageEntry pg = theSessionState.getCurrentPage();
        if (pg.editable) {
            pg.setPageCondition3(true);
            
            /****************MCM Impl Team XS_2.32 changes starts********************/      
            String ccPrefix = pgComponentDetailsViewBean.CHILD_TILEDCCCOMP + "/";
            
            handleDelete(false,  Sc.CONFIRM_DELETE_COMPONENT,
                                 index,
                                 ccPrefix + pgComponentCreditCardTiledView.CHILD_HDCOMPONENTID,
                                 "ComponentId",
                                 ccPrefix + pgComponentCreditCardTiledView.CHILD_HDCOPYID ,
                                 "Component");
            /****************MCM Impl Team XS_2.32 changes ends********************/  
            return;
        }

        setActiveMessageToAlert(BXResources.getSysMsg(DISALLOWED_DURING_VIEW_ONLY_MODE, theSessionState.getLanguageId()),
                ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
    }



    /**
     * <p>handleDeleteLoanComp</p>
     * <p>Description: Handle clicking handleDeleteLoanComp.
     * PageCondition3 controls display Cancel button  
     * 
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @see pgComponentDetailsViewBean
     */
    public void handleDeleteLoanComp(int index) {
        PageEntry pg = theSessionState.getCurrentPage();
        if (pg.editable) {
            pg.setPageCondition3(true);
            
            /****************MCM Impl Team XS_2.32 changes starts********************/                 
            String ccPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDLOAN + "/";
            
            handleDelete(false,  Sc.CONFIRM_DELETE_COMPONENT,
                                 index,
                                 ccPrefix + pgComponentDetailsLoanTiledView.CHILD_HDCOMPONENTID,
                                 "ComponentId",
                                 ccPrefix + pgComponentDetailsLoanTiledView.CHILD_HDCOPYID,
                                 "Component");
            /****************MCM Impl Team XS_2.32 changes ends********************/  
            return;
        }

        setActiveMessageToAlert(BXResources.getSysMsg(DISALLOWED_DURING_VIEW_ONLY_MODE, theSessionState.getLanguageId()),
                ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
    }


    public void handleSubmit() {

    }

    /**
     * <p>handleRecalc</p>
     * <p>Description: Handle clicking Recalc.
     * PageCondition3 controls display Cancel button  
     * 
     * @param event: RequestInvocationEvent
     * @return 
     * 
     * @see pgComponentDetailsViewBean
     * @version 2.0 Added dcm.calc() for calling the calulation.
     */
    public void handleRecalc() {
        PageEntry pg = theSessionState.getCurrentPage();

        if (pg.editable) {
        	
            //FXP23869, MCM/4.1, Jan 20 2009, saving/resetting screen scroll position - start.
            saveWindowScrollPosition();
            //FXP23869, MCM/4.1, Jan 20 2009, saving/resetting screen scroll position - end.
            //FXP24256, MCM/4.1, Feb 5, 2009, extracted a method to fix Calc entity sync problem temporally - start.
            forceRecalc();
            //FXP24256, MCM/4.1, Feb 5, 2009, extracted a method to fix Calc entity sync problem temporally - end.

            pg.setPageCondition3(true);
            return;
        }

        setActiveMessageToAlert(BXResources.getSysMsg(DISALLOWED_DURING_VIEW_ONLY_MODE, theSessionState.getLanguageId()),
                ActiveMsgFactory.ISCUSTOMCONFIRM);
        return;
    }

    //FXP24256, MCM/4.1, Feb 5, 2009, extracted a method to fix Calc entity sync problem temporally - start.
    protected void forceRecalc() {
    	try
    	{
    		CalcMonitor dcm = CalcMonitor.getMonitor(srk);     
    		PageEntry pg = theSessionState.getCurrentPage();
    		Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg.getPageDealCID(), dcm);
    		// Force to trogger calc by TotalLoanAmount

    		//FXP23117, MCM Nov 1, 2008 - start
    		//inputCreatedEntity method will set all fields in the entity to CalcMonitor.
    		//this fix will trigger all components which depend on component entity and its sub entities.
    		//deal.forceChange("NumberOfComponents"); 
    		//dcm.inputEntity(deal);
    		for(Iterator<ComponentSummary> it = deal.getComponentSummary().iterator();it.hasNext();){
    			ComponentSummary cs = it.next();
    			dcm.inputCreatedEntity(cs);
    		}
    		for(Component comp : deal.getComponents()){
    			dcm.inputCreatedEntity(comp); //inputCreatedEntity will forceChange on all columns

    			List children = comp.getChildren();
    			for(Iterator<DealEntity> it = children.iterator();it.hasNext();){
    				dcm.inputCreatedEntity(it.next()); //inputCreatedEntity will forceChange on all columns
    			}
    		}
    		//FXP23117, MCM Nov 1, 2008 - end              

    		dcm.calc();
    	}
    	catch (Exception e)
    	{
    		_log.error("Exception @forceRecalc :: " + e.getMessage(), e);
    	}
    }
    //FXP24256, MCM/4.1, Feb 5, 2009, extracted a method to fix Calc entity sync problem temporally - end.
    
    /**
     * <p>getPageEntity</p>
     * <p>Description: get Page entity class by pageId
     * called from displayCancelButton to identify page setting EditViaTxCopy
     * 
     * @param pageId: int 
     * @return Page
     */
    private Page getPageEntity(int pageId) {
        Page pageEntity = null;
        try {
            PDC pageCache = PDC.getInstance();
            pageEntity = pageCache.getPage(pageId);
        } catch(Exception e0) {
            pageEntity = null;
        }
        return pageEntity;
    }


    /**
     * populate display fields for LOC
     */
    protected void setComponentLOCDisplayFields(int rowIndx) {

        doComponentLocModelImpl dobject = (doComponentLocModelImpl) (RequestManager
                .getRequestContext().getModelManager()
                .getModel(doComponentLocModel.class));
        String tileViewPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDLOC + "/"; 
        logger.debug("ComponentDetailsHandler::RowNum: " + rowIndx
                + " , ModelRowIndx: " + dobject.getRowIndex());

        //delete options in component type combo box except for "overdraft" - starts
        ComboBox cbComponentType = (ComboBox) getCurrNDPage().getDisplayField( 
                tileViewPrefix + pgComponentDetailsLOCTiledView.CHILD_CBCOMPONENTTYPE);
        populateComponentCbComponentTypeCommon(cbComponentType, Sc.COMPONENT_TYPE_LOC);
        //delete options in component type combo box except for "overdraft" - ends

        //change for Allocate MI Premium
        CheckBox chMIAllocate = (CheckBox) getCurrNDPage().getDisplayField(
                tileViewPrefix + "chAllocateMIPremiumLOC");

        String miUpfront = (String)dobject.getValue(
                doComponentLocModel.FIELD_DFMIUPFRONT);
        String miAllocateFlag = (String)dobject.getValue(doComponentLocModel.FIELD_DFMIALLOCAGTEFLAG);
        boolean miAllocateChecked = false;
        if ("N".equalsIgnoreCase(miUpfront)){
            miAllocateChecked = "Y".equalsIgnoreCase(miAllocateFlag);
            chMIAllocate.setChecked(miAllocateChecked);
        } else {
            chMIAllocate.setChecked(false);
            chMIAllocate.setExtraHtml("disabled='true' class='greyedDisabled' ");
        }
        //changes for Allocate MI Premium

        //change for Allocate Tax Escrow
        CheckBox chAllocateTaxEscrow = (CheckBox) getCurrNDPage().getDisplayField(
                tileViewPrefix + "chAllocateTaxEscrowLOC");
        BigDecimal taxPayorId = (BigDecimal)dobject.getValue(
                doComponentLocModel.FIELD_DFTAXPAYORID);
        String allocateEscrow = (String)dobject.getValue(
                doComponentLocModel.FIELD_DFPROPERTYTAXALLOCATEFLAG);
        boolean taxEscrowChecked = false;
        if (taxPayorId != null 
                && Sc.TAX_PAYOR_LENDER == taxPayorId.intValue() ) {
            taxEscrowChecked = "Y".equalsIgnoreCase(allocateEscrow);
            chAllocateTaxEscrow.setChecked(taxEscrowChecked);
        } else {
            chAllocateTaxEscrow.setChecked(false);
            chAllocateTaxEscrow.setExtraHtml("disabled='true' class='greyedDisabled' ");
        }
        //change for Allocate Tax Escrow

        fillupMonths(tileViewPrefix + "cbFirstPaymentMonthLOC");

        setDateDisplayField(dobject, tileViewPrefix + "cbFirstPaymentMonthLOC",
                tileViewPrefix + "txFirstPaymentDayLOC", tileViewPrefix + "txFirstPaymentYearLOC",
                "dfFirstPaymentDate", rowIndx);

        String allocateMIPremium = dobject.getValue("dfMIFlag").toString();

        if (allocateMIPremium.equalsIgnoreCase("N")
                || dobject.getValue("dfMiPremiumAmount") == null) {
            getCurrNDPage().setDisplayFieldValue(tileViewPrefix + "stMIPremiumLOC",
            "0.00");
        }

        String allocateTaxEscrow = dobject.getValue("dfPropertyTaxFlag")
        .toString();

        if (allocateTaxEscrow.equalsIgnoreCase("N"))
            getCurrNDPage().setDisplayFieldValue(tileViewPrefix + "stTaxEscrowLOC",
            "0.00");


        // MCM Impl Team: artf736687 STARTS 
        TextField stMtgProd = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix
                + pgComponentDetailsLOCTiledView.CHILD_STPRODUCT);
        Object dfProdId = dobject.getValue(doComponentLocModel.FIELD_DFPRODUCTID);
        stMtgProd.setValue((BigDecimal)dfProdId);

        TextField stPostedInterestRate = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix
                + pgComponentDetailsLOCTiledView.CHILD_STPOSTEDINTERESTRATE);
        Object dfPostedInterestedRate = dobject.getValue(doComponentLocModel.FIELD_DFPRICINGRATEINVENTORYID);
        stPostedInterestRate.setValue((BigDecimal)dfPostedInterestedRate);

        TextField stRepaymentType = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix
                + pgComponentDetailsLOCTiledView.CHILD_STREPAYMENTTYPE);
        Object dfRepaymentTypeId = dobject.getValue(doComponentLocModel.FIELD_DFREPAYMENTTYPEID);
        stRepaymentType.setValue((BigDecimal)dfRepaymentTypeId);

        // MCM Impl Team: artf736687 ENDS 
        
        // line and section title 
        /***************MCM Impl team changes starts - artf741023 *********************/
        populateSectionTitleAndSectionDevider(
                tileViewPrefix + pgComponentDetailsLOCTiledView.CHILD_STTITLE, 
                tileViewPrefix + pgComponentDetailsLOCTiledView.CHILD_IMSECTIONDEVIDER, 
                rowIndx, Sc.COMPONENT_TYPE_LOC);
        /***************MCM Impl team changes ends - artf741023 *********************/

        prepareComponentLOCDisplayFields(rowIndx);

        /***************MCM Impl team changes starts - XS_2.34*********************/
        addDynamicDropDownExtraHtmlForLOC(rowIndx);
        /***************MCM Impl team changes ends - XS_2.34*********************/
    }


    /**
     * pupulate javascript validation for LOC Section
     * Changed the JS for tbAdditionalInfoLOC : artf736674
     * 
     */
    protected void prepareComponentLOCDisplayFields(int rowIndex) {

        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField("RepeatedLOC/tbLOCAmount");
        String jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/cbFirstPaymentMonthLOC");
        String extraId = "id='cbFirstPaymentMonthLOC" + rowIndex + "'";
        jsValid = " onBlur=\"isFieldValidDate('txFirstPaymentYearLOC', "
            + "'cbFirstPaymentMonthLOC', 'txFirstPaymentDayLOC');\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/txFirstPaymentDayLOC");
        extraId = "id='txFirstPaymentDayLOC" + rowIndex + "'";
        jsValid = " onBlur=\"if(isFieldInIntRange(1, 31)) "
            + "isFieldValidDate('txFirstPaymentYearLOC', 'cbFirstPaymentMonthLOC', "
            + "'txFirstPaymentDayLOC');\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/txFirstPaymentYearLOC");
        extraId = "id='txFirstPaymentYearLOC" + rowIndex + "'";
        jsValid = " onBlur=\"if(isFieldInIntRange(1800, 2100)) "
            + "isFieldValidDate('txFirstPaymentYearLOC', 'cbFirstPaymentMonthLOC',"
            + " 'txFirstPaymentDayLOC');\"";
        df.setExtraHtml(extraId + jsValid);


        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/tbDiscountLOC");
        extraId = "id='tbDiscountLOC" + rowIndex + "'";
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 100)) isFieldDecimal(3);\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/tbPremiumLOC");
        extraId = "id='tbPremium" + rowIndex + "'";
        String jsChange = "onBlur=\"if(isFieldInDecRange(0, 100)) isFieldDecimal(3);\"";
        df.setExtraHtml(extraId + jsChange);


        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/tbHoldBackAmountLOC");
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/tbAdditionalInfoLOC");
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() && isFieldMaxLength(256));\"";
        df.setExtraHtml(jsValid);

        /**
         * BugFix: artf763640 Starts
         */
        //validation for CommisionCode
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/tbCommissionCodeLOC");
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() );\"";
        df.setExtraHtml(jsValid);
        
        //validation for existing account reference
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLOC/tbExistingAccountRefLOC");
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() );\"";
        df.setExtraHtml(jsValid);
        
        /**
         * BugFix: artf763640 Ends
         */
    }

    /**
     * <P> addDynamicDropDownExtraHtmlForMtg </p>
     * <p> add id and javaScirpt to the dynamic drop down lists
     *  in mortgage component section </p>
     * MCM XS_2.38
     * 
     * @param index - index of tileViewBean
     * @author MCM team
     */
    private void addDynamicDropDownExtraHtmlForMtg(int index){

        final String mtgPrefix = pgComponentDetailsViewBean.CHILD_TILEDMTGCOMP + "/";

        //product drop down list
        String fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_CBMTGPRODUCT;
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "product"));
        appendExtraHtml(df, "onChange='handleMTGProductChange(" + Mc.COMPONENT_TYPE_MORTGAGE + " ," + index + ")'");

        //posted interest rate drop down list
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_CBPOSTEDINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "rate"));
        appendExtraHtml(df, "onChange='handleMTGRateChange(" + Mc.COMPONENT_TYPE_MORTGAGE + " ," + index + ")'");

        //payment term description
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_TBPAYMENTTERMDESCRIPTION;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "paymentDescription"));

        //repayment type
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_CBREPAYMENTTYPE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "repaymentType"));

        //initial product 
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_STMTGPRODUCT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "initProduct"));

        //initial posted interest rate 
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_STPOSTEDINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "initRate"));

        //initial repayment type
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_STREPAYMENTTYPE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "initRepaymentType"));

        //rate lock
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_STRATELOCK;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "rateLock"));

        //hidden update item : postedRate
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_TXPOSTEDRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_MORTGAGE, index, "txPostedRate"));

        // init javascript
        StringBuffer sb = new StringBuffer();
        sb.append("<script type=\"text/javascript\">");
        sb.append("initMortgage(");
        sb.append(Mc.COMPONENT_TYPE_MORTGAGE);
        sb.append(",");
        sb.append(index);
        sb.append(");");
        sb.append("</script>");
        fieldName = mtgPrefix +  pgComponentDetailsMTGTiledView.CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setValue(sb.toString());
    }


    /**
     * 
     * <P> makeLabelForDyanamicDropDown </p>
     * <p> this method makes String value of "ID" for Dynamic Drop down lists</p>
     * MCM XS_2.28 - Jul 9, 2008
     * 
     * @param componentTypeid
     * @param index
     * @param idName
     * @return String value of ID
     * @author MCM team
     */
    private String makeLabelForDyanamicDropDown(int componentTypeid, int index, String idName){
        return "id='" + idName + "#comp" + componentTypeid + "#idx" + index + "'";
    }

    /**
     * <P> appendExtraHtml </p>
     * <p> this method grabs HtmlDisplayFieldBase object based on the 
     * parameter <displayField> and  appends any String value <extraHtml> into 
     *  HtmlDisplayFieldBase.externalHtml </p>
     * 
     * @param displayField - displayField name
     * @param extraHtml
     * @author MCM team
     */
    protected void appendExtraHtml(String displayField, String extraHtml){
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(displayField);
        appendExtraHtml(df, extraHtml);
    }

    /**
     * <P> appendExtraHtml </p>
     * <p> this method appends any String value <extraHtml> into 
     *  HtmlDisplayFieldBase.externalHtml </p>
     * 
     * @param df
     * @param extraHtml
     * @author MCM team
     */
    protected void appendExtraHtml(HtmlDisplayFieldBase df, String extraHtml){

        if(df.getExtraHtml() == null || df.getExtraHtml().length() <= 0){ 
            df.setExtraHtml(extraHtml);
        }else{
            df.setExtraHtml(df.getExtraHtml() + " " + extraHtml);
        }
    }

    /* **************MCM Impl team changes ends - XS_2.28*********************/

    
    /**
     * <P> addDynamicDropDownExtraHtmlForLOC </p>
     * <p> add id and javaScirpt to the dynamic drop down lists
     *  in LOC component section </p>
     * MCM XS_2.34
     * 
     * @param index - index of tileViewBean
     * @author MCM team
     */
    private void addDynamicDropDownExtraHtmlForLOC(int index){
        final String mtgPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDLOC + "/";

        //product drop down list
        String fieldName = mtgPrefix +  pgComponentDetailsLOCTiledView.CHILD_CBPRODUCTTYPE;
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOC, index, "product"));
        appendExtraHtml(df, "onChange='handleLOCProductChange(" + Mc.COMPONENT_TYPE_LOC + " ," + index + ")'");


        //posted interest rate drop down list
        fieldName = mtgPrefix +  pgComponentDetailsLOCTiledView.CHILD_CBPOSTEDINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOC, index, "rate"));
        appendExtraHtml(df, "onChange='handleLOCRateChange(" + Mc.COMPONENT_TYPE_LOC + " ," + index + ")'");

        //repayment type
        fieldName = mtgPrefix +  pgComponentDetailsLOCTiledView.CHILD_CBREPAYMENTTYPE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOC, index, "repaymentType"));

        //initial product 
        fieldName = mtgPrefix +  pgComponentDetailsLOCTiledView.CHILD_STPRODUCT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOC, index, "initProduct"));

        //initial posted interest rate 
        fieldName = mtgPrefix +  pgComponentDetailsLOCTiledView.CHILD_STPOSTEDINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOC, index, "initRate"));

        //initial repayment type
        fieldName = mtgPrefix +  pgComponentDetailsLOCTiledView.CHILD_STREPAYMENTTYPE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOC, index, "initRepaymentType"));

        //hidden update item : postedRate
        fieldName = mtgPrefix +  pgComponentDetailsLOCTiledView.CHILD_TXPOSTEDRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOC, index, "txPostedRate"));

        // init javascript
        StringBuffer sb = new StringBuffer();
        sb.append("<script type=\"text/javascript\">");
        sb.append("initLOC(");
        sb.append(Mc.COMPONENT_TYPE_LOC);
        sb.append(",");
        sb.append(index);
        sb.append(");");
        sb.append("</script>");

        fieldName = mtgPrefix +  pgComponentDetailsLOCTiledView.CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setValue(sb.toString());


    }


    /**
     * <P> handleUpdateComponentMTG </p>
     * <p> save component mortgage section data into DB</p>
     * 
     * @param keyValues String[]
     * @param dcm CalcMonitor
     *
     */
    public void handleUpdateComponentMTG(String[] keyValues, CalcMonitor dcm)
        throws Exception {

        try {
            Hashtable propList = getPropertiesForThisPage(COMPONENTINMTG_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For Component Update");
            updateForRepeatedRows(propList, keyValues, "Component", dcm);
        } catch(Exception ex) {
            logger.error("Exception @ComponentDetailsHandler:SaveData component in MTG");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }

        try {
            Hashtable propList = getPropertiesForThisPage(COMPONENTMTG_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For Component Update");
            updateForRepeatedRows(propList, keyValues, "ComponentMtg", dcm);
        } catch(Exception ex)  {
            logger.error("Exception @ComponentDetailsHandler:SaveData:save MTG ");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }
    }

    /**
     * <P> handleUpdateComponentLOC </p>
     * <p> save component LOC section data into DB</p>
     * 
     * @param keyValues String[]
     * @param dcm CalcMonitor
     *
     */
    public void handleUpdateComponentLOC(String[] keyValues, CalcMonitor dcm)
        throws Exception {
        try
        {
            Hashtable propList = getPropertiesForThisPage(COMPONENT_LOC_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For Component");
            updateForRepeatedRows(propList, keyValues, "Component", dcm);

            Hashtable propList1 = getPropertiesForThisPage(COMPONENT_LOC_CHILD_PROP_FILE_NAME);
            if (propList1 == null) throw new Exception("Null Property File For ComponentLOC");
            updateForRepeatedRows(propList1, keyValues, "ComponentLOC", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateApplicantAddress");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }
    }

    /**
     * <P> handleUpdateComponentCC </p>
     * <p> save component CC section data into DB</p>
     * 
     * @param keyValues String[]
     * @param dcm CalcMonitor
     *
     */
    public void handleUpdateComponentCC(String[] keyValues, CalcMonitor dcm)
    throws Exception {

        try {
            Hashtable propList = getPropertiesForThisPage(COMPONENTINCC_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For Component in Credir Card Update");
            updateForRepeatedRows(propList, keyValues, "Component", dcm);
        } catch(Exception ex) {
            logger.error("Exception @ComponentDetailsHandler:SaveData component in MTG");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }

        try {
            Hashtable propList = getPropertiesForThisPage(COMPONENTCC_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For ComponentCreditCard Update");
            updateForRepeatedRows(propList, keyValues, "ComponentCreditCard", dcm);
        } catch(Exception ex)  {
            logger.error("Exception @ComponentDetailsHandler:SaveData:save CreditCard ");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }
    }

    /**
     * <P> handleUpdateComponentOverdraft </p>
     * <p> save component overdraft section data into DB</p>
     * MCM XS_2.38
     *  
     * @param keyValues String[]
     * @param dcm CalcMonitor
     *
     */
    public void handleUpdateComponentOverdraft(String[] keyValues, CalcMonitor dcm)
    throws Exception {

        try {
            Hashtable propList = getPropertiesForThisPage(COMPONENT_IN_OVERDRAFT_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For Component in Credir Card Update");
            updateForRepeatedRows(propList, keyValues, "Component", dcm);
        } catch(Exception ex) {
            _log.error("Exception @ComponentDetailsHandler:SaveData component in Overdraft");
            _log.error(StringUtil.stack2string(ex));
            throw new Exception("Update entity handler exception", ex);
        }

        try {
            Hashtable propList = getPropertiesForThisPage(COMPONENT_OVERDRAFT_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For ComponentCreditCard Update");
            updateForRepeatedRows(propList, keyValues, "ComponentOverdraft", dcm);
        } catch(Exception ex)  {
            _log.error("Exception @ComponentDetailsHandler:SaveData:save Overdraft ");
            _log.error(StringUtil.stack2string(ex));
            throw new Exception("Update entity handler exception", ex);
        }
    }

    /**
     * <P> setOverdraftCompDisplayFields </p>
     * <p> set default value to the overdraft component section </p>
     * MCM XS_2.38 
     * 
     * @param tileIndex - index of tile
     * @author MCM team
     */
    protected void setOverdraftCompDisplayFields(int tileIndex) {

        String tileViewPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDOVERDRAFT + "/";

        //delete options in component type combo box except for "overdraft" - starts
        ComboBox cbComponentType = (ComboBox) getCurrNDPage().getDisplayField( 
                tileViewPrefix + pgComponentDetailsOverdraftTiledView.CHILD_CBCOMPONENTTYPE);
        populateComponentCbComponentTypeCommon(cbComponentType, Sc.COMPONENT_TYPE_OVERDRAFT);
        //delete options in component type combo box except for "overdraft" - ends

        doComponentOverDraftModel model = (doComponentOverDraftModel) (RequestManager
                .getRequestContext().getModelManager()
                .getModel(doComponentOverDraftModel.class));

        // adding initial data into jato dynamic combo box items - mtgProdid
        TextField stMtgProd = (TextField) getCurrNDPage() .getDisplayField( 
                tileViewPrefix + pgComponentDetailsOverdraftTiledView.CHILD_STMTGPRODUCT);
        stMtgProd.setValue(model.getValue(doComponentOverDraftModel.FIELD_DFMTGPRODID));

        // adding initial data into jato dynamic combo box items - interest rate
        TextField stPostedInterestRate = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix + pgComponentDetailsOverdraftTiledView.CHILD_STINTERESTRATE);
        stPostedInterestRate.setValue(model.getValue(doComponentOverDraftModel.FIELD_DFPRICINGRATEINVENTORYID));


        // adding JS validation to Overdraft Amount
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField( tileViewPrefix
                + pgComponentDetailsOverdraftTiledView.CHILD_TXOVERDRAFTAMOUNT);
        String jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid);

        // adding JS validation to Additional Info
        df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField( tileViewPrefix
                + pgComponentDetailsOverdraftTiledView.CHILD_TXADDITIONALINFO);
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() && isFieldMaxLength(256));\"";
        df.setExtraHtml(jsValid);

        // adding dynamic pricing data drop down to overdraft section
        addDynamicDropDownExtraHtmlForOverdraft(tileIndex);

        // line and section title 
        /***************MCM Impl team changes starts - artf741023 *********************/
        populateSectionTitleAndSectionDevider(
                tileViewPrefix + pgComponentDetailsOverdraftTiledView.CHILD_STTITLE, 
                tileViewPrefix + pgComponentDetailsOverdraftTiledView.CHILD_IMSECTIONDEVIDER, 
                tileIndex, Sc.COMPONENT_TYPE_OVERDRAFT);
        /***************MCM Impl team changes ends - artf741023 *********************/
    }

    /**
     * <P> addDynamicDropDownExtraHtmlForOverdraft </p>
     * <p> add id and javaScirpt to the dynamic drop down lists
     *  in overdraft component section </p>
     * MCM XS_2.38 
     * 
     * @param index - index of tile
     * @author MCM team
     */
    private void addDynamicDropDownExtraHtmlForOverdraft(int index){

        final String mtgPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDOVERDRAFT + "/";

        //product drop down list
        String fieldName = mtgPrefix +  pgComponentDetailsOverdraftTiledView.CHILD_CBPRODUCT;
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_OVERDRAFT, index, "product"));
        appendExtraHtml(df, "onChange='handleOverdraftProductChange(" + Mc.COMPONENT_TYPE_OVERDRAFT + " ," + index + ")'");

        //posted interest rate drop down list
        fieldName = mtgPrefix +  pgComponentDetailsOverdraftTiledView.CHILD_CBINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_OVERDRAFT, index, "rate"));
        appendExtraHtml(df, "onChange='handleOverdraftRateChange(" + Mc.COMPONENT_TYPE_OVERDRAFT + " ," + index + ")'");

        //initial product 
        fieldName = mtgPrefix +  pgComponentDetailsOverdraftTiledView.CHILD_STMTGPRODUCT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_OVERDRAFT, index, "initProduct"));

        //initial posted interest rate 
        fieldName = mtgPrefix +  pgComponentDetailsOverdraftTiledView.CHILD_STINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_OVERDRAFT, index, "initRate"));

        //hidden update item : postedRate
        fieldName = mtgPrefix +  pgComponentDetailsOverdraftTiledView.CHILD_TXPOSTEDRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_OVERDRAFT, index, "txPostedRate"));


        // init javascript
        StringBuffer sb = new StringBuffer();
        sb.append("<script type=\"text/javascript\">");
        sb.append("initOverdraft(");
        sb.append(Mc.COMPONENT_TYPE_OVERDRAFT);
        sb.append(",");
        sb.append(index);
        sb.append(");");
        sb.append("</script>");

        fieldName = mtgPrefix +  pgComponentDetailsOverdraftTiledView.CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setValue(sb.toString());
    }

    /**
     * <p>executeCriteriaCreditCardModel</p>
     * <p>Description: execute Component CreditCard Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * XS_2.36
     */
    protected void executeCriteriaCreditCardModel(PageEntry pg, int dealId, int copyId) {

        doComponentCreditCardModel theDO = (
                doComponentCreditCardModel)
                (RequestManager.getRequestContext().getModelManager().getModel(
                        doComponentCreditCardModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(doComponentCreditCardModel.FIELD_DFDEALID, "=",
                String.valueOf(dealId));
        theDO.addUserWhereCriterion(doComponentCreditCardModel.FIELD_DFCOPYID, "=",
                String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaCreditCardModel", false);
    }

    /**
     * <p>setCreditCardCompDisplayFields</p>
     * <p>Description: setup component creditCard Tile view </p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * XS_2.27
     */
    protected void setCreditCardCompDisplayFields(int rowNum)
    {
        doComponentCreditCardModel dobject = (doComponentCreditCardModel) (RequestManager
                .getRequestContext().getModelManager()
                .getModel(doComponentCreditCardModel.class));
        try{ 
            dobject.setLocation(rowNum);  
        } catch (Exception e){
            _log.error(StringUtil.stack2string(e));
            throw new ExpressRuntimeException(e.getMessage(), e);
        }

        String tileViewPrefix = pgComponentDetailsViewBean.CHILD_TILEDCCCOMP + "/"; 

        /***************MCM Impl team changes starts - artf741023 *********************/
        populateSectionTitleAndSectionDevider(
                tileViewPrefix + pgComponentCreditCardTiledView.CHILD_STTITLE, 
                tileViewPrefix + pgComponentCreditCardTiledView.CHILD_IMSECTIONDEVIDER, 
                rowNum, Sc.COMPONENT_TYPE_CREDITCARD);
        /***************MCM Impl team changes ends - artf741023 *********************/

        //delete options in component type combo box except for "credit card" - starts
        ComboBox cbComponentType = (ComboBox) getCurrNDPage().getDisplayField( 
                tileViewPrefix + pgComponentCreditCardTiledView.CHILD_CBCOMPONENTTYPE);
        populateComponentCbComponentTypeCommon(cbComponentType, Sc.COMPONENT_TYPE_CREDITCARD);
        //delete options in component type combo box except for "credit card" - ends

        // adding initial data into jato dynamic combo box items - mtgProdid
        TextField stMtgProd = (TextField) getCurrNDPage() .getDisplayField( 
                tileViewPrefix + pgComponentCreditCardTiledView.CHILD_STMTGPRODUCT);
        stMtgProd.setValue(dobject.getValue(doComponentCreditCardModel.FIELD_DFMTGPRODID));

        // adding initial data into jato dynamic combo box items - interest rate
        TextField stPostedInterestRate = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix + pgComponentCreditCardTiledView.CHILD_STINTERESTRATE);
        stPostedInterestRate.setValue(dobject.getValue(doComponentCreditCardModel.FIELD_DFPRICINGRATEINVENTORYID));

        // adding dynamic pricing data drop down to credit card section
        addDynamicDropDownExtraHtmlForCreditCard(rowNum);

        prepareComponentCCDisplayFields(rowNum);       
    }



    /**
     * <P> addDynamicDropDownExtraHtmlForCreditCard </p>
     * <p> add id and javaScirpt to the dynamic drop down lists
     *  in Credit card component section </p>
     * MCM XS_2.36, version 1.13
     * 
     * @param index - index of tile
     * @author MCM team
     */
    private void addDynamicDropDownExtraHtmlForCreditCard(int index){

        final String mtgPrefix = pgComponentDetailsViewBean.CHILD_TILEDCCCOMP + "/";

        //product drop down list
        String fieldName = mtgPrefix +  pgComponentCreditCardTiledView.CHILD_CBCCPRODUCT;
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_CREDITCARD, index, "product"));
        appendExtraHtml(df, "onChange='handleCreditCardProductChange(" + Mc.COMPONENT_TYPE_CREDITCARD + " ," + index + ")'");

        //posted interest rate drop down list
        fieldName = mtgPrefix +  pgComponentCreditCardTiledView.CHILD_CBCCINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_CREDITCARD, index, "rate"));
        appendExtraHtml(df, "onChange='handleCreditCardRateChange(" + Mc.COMPONENT_TYPE_CREDITCARD + " ," + index + ")'");

        //initial product 
        fieldName = mtgPrefix +  pgComponentCreditCardTiledView.CHILD_STMTGPRODUCT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_CREDITCARD, index, "initProduct"));

        //initial posted interest rate 
        fieldName = mtgPrefix +  pgComponentCreditCardTiledView.CHILD_STINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_CREDITCARD, index, "initRate"));

        //hidden update item : postedRate
        fieldName = mtgPrefix +  pgComponentCreditCardTiledView.CHILD_TXPOSTEDRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_CREDITCARD, index, "txPostedRate"));


        // init javascript
        StringBuffer sb = new StringBuffer();
        sb.append("<script type=\"text/javascript\">");
        sb.append("initCreditCard(");
        sb.append(Mc.COMPONENT_TYPE_CREDITCARD);
        sb.append(",");
        sb.append(index);
        sb.append(");");
        sb.append("</script>");

        fieldName = mtgPrefix +  pgComponentCreditCardTiledView.CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setValue(sb.toString());
    }

    /**
     * <p>prepareComponentMTGDisplayFields</p>
     * <p>Description: pupulate javascript validation component Motrgage Section
     * 
     * @param int rowIndex
     * 
     */
    protected void prepareComponentCCDisplayFields(int rowIndex) {

        String ccPrefix = pgComponentDetailsViewBean.CHILD_TILEDCCCOMP + "/";

        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase)getCurrNDPage().getDisplayField(
                ccPrefix + pgComponentCreditCardTiledView.CHILD_TXCREDITCARDAMOUNT);
        String jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid);

        //fix for artf742087 : Credit Card Additional Info Field Has No 256 Limit 
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
                ccPrefix + pgComponentCreditCardTiledView.CHILD_TXADDITIONALINFO);
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() && isFieldMaxLength(256));\"";
        df.setExtraHtml(jsValid);
    }


    /**
     * populate display fields for Loan
     */
    protected void setComponentLoanDisplayFields(int rowIndx) {

        doComponentLoanModelImpl dobject = (doComponentLoanModelImpl) (RequestManager
                .getRequestContext().getModelManager()
                .getModel(doComponentLoanModel.class));
        String tileViewPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDLOAN + "/"; 

        logger.debug("ComponentDetailsHandler::RowNum: " + rowIndx
                + " , ModelRowIndx: " + dobject.getRowIndex());

        setTermsDisplayField(dobject,
                tileViewPrefix + pgComponentDetailsLoanTiledView.CHILD_TXACTUALPAYMENTTERMYEARS,
                tileViewPrefix + pgComponentDetailsLoanTiledView.CHILD_TXACTUALPAYMENTTERMMONTHS, 
                doComponentLoanModel.FIELD_DFACTUALPAYMENTTERM, rowIndx);

        //delete options in component type combo box except for "Loan" - starts
        ComboBox cbComponentType = (ComboBox) getCurrNDPage().getDisplayField( 
                tileViewPrefix + pgComponentDetailsLoanTiledView.CHILD_CBCOMPONENTTYPE);
        populateComponentCbComponentTypeCommon(cbComponentType, Sc.COMPONENT_TYPE_LOAN);
        //delete options in component type combo box except for "Loan" - ends

        // adding initial data into jato dynamic combo box items - mtgProdid
        TextField stMtgProd = (TextField) getCurrNDPage() .getDisplayField( 
                tileViewPrefix + pgComponentDetailsLoanTiledView.CHILD_STMTGPRODUCT);
        stMtgProd.setValue(dobject.getValue(doComponentLoanModel.FIELD_DFPRODUCTID));

        // adding initial data into jato dynamic combo box items - interest rate
        TextField stPostedInterestRate = (TextField)getCurrNDPage().getDisplayField(
                tileViewPrefix + pgComponentDetailsLoanTiledView.CHILD_STINTERESTRATE);
        stPostedInterestRate.setValue(dobject.getValue(doComponentLoanModel.FIELD_DFPRICINGRATEINVENTORYID));
        
        // line and section title 
        /***************MCM Impl team changes starts - artf741023 *********************/
        populateSectionTitleAndSectionDevider(
                tileViewPrefix + pgComponentDetailsLoanTiledView.CHILD_STTITLE, 
                tileViewPrefix + pgComponentDetailsLoanTiledView.CHILD_IMSECTIONDEVIDER, 
                rowIndx, Sc.COMPONENT_TYPE_LOAN);
        /***************MCM Impl team changes ends - artf741023 *********************/

        prepareComponentLoanDisplayFields(rowIndx);

        // adding dynamic pricing data drop down to loan section
        addDynamicDropDownExtraHtmlForLoan(rowIndx);
    }

    /**
     * <P> addDynamicDropDownExtraHtmlForLoan </p>
     * <p> add id and javaScirpt to the dynamic drop down lists
     *  in loan component section </p>
     * MCM XS_2.37, version 1.13
     * 
     * @param index - index of tile
     * @author MCM team
     */
    private void addDynamicDropDownExtraHtmlForLoan(int index){

        final String mtgPrefix = pgComponentDetailsViewBean.CHILD_REPEATEDLOAN + "/";

        //product drop down list
        String fieldName = mtgPrefix +  pgComponentDetailsLoanTiledView.CHILD_CBPRODUCTTYPE;
        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOAN, index, "product"));
        appendExtraHtml(df, "onChange='handleLoanProductChange(" + Mc.COMPONENT_TYPE_LOAN + " ," + index + ")'");

        //posted interest rate drop down list
        fieldName = mtgPrefix +  pgComponentDetailsLoanTiledView.CHILD_CBPOSTEDINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOAN, index, "rate"));
        appendExtraHtml(df, "onChange='handleLoanRateChange(" + Mc.COMPONENT_TYPE_LOAN + " ," + index + ")'");

        //payment term description
        fieldName = mtgPrefix +  pgComponentDetailsLoanTiledView.CHILD_TBPAYMENTTERMDESCRIPTION;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOAN, index, "paymentDescription"));

        //initial product 
        fieldName = mtgPrefix +  pgComponentDetailsLoanTiledView.CHILD_STMTGPRODUCT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOAN, index, "initProduct"));

        //initial posted interest rate 
        fieldName = mtgPrefix +  pgComponentDetailsLoanTiledView.CHILD_STINTERESTRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOAN, index, "initRate"));

        //hidden update item : postedRate
        fieldName = mtgPrefix +  pgComponentDetailsLoanTiledView.CHILD_TXPOSTEDRATE;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setExtraHtml(makeLabelForDyanamicDropDown(Mc.COMPONENT_TYPE_LOAN, index, "txPostedRate"));


        // init javascript
        StringBuffer sb = new StringBuffer();
        sb.append("<script type=\"text/javascript\">");
        sb.append("initLoan(");
        sb.append(Mc.COMPONENT_TYPE_LOAN);
        sb.append(",");
        sb.append(index);
        sb.append(");");
        sb.append("</script>");

        fieldName = mtgPrefix +  pgComponentDetailsLoanTiledView.CHILD_INIT_DYNAMIC_LIST_JAVASCRIPT;
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(fieldName);
        df.setValue(sb.toString());
    }

    /*
     * <P> handleUpdateComponentLoan </p>
     * <p> save component Loan section data into DB</p>
     * 
     * @param keyValues String[]
     * @param dcm CalcMonitor
     *
     */
    public void handleUpdateComponentLoan(String[] keyValues, CalcMonitor dcm)
    throws Exception {
        try
        {
            Hashtable propList = getPropertiesForThisPage(COMPONENT_LOAN_PROP_FILE_NAME);
            if (propList == null) throw new Exception("Null Property File For Component");
            updateForRepeatedRows(propList, keyValues, "Component", dcm);

            Hashtable propList1 = getPropertiesForThisPage(COMPONENT_LOAN_CHILD_PROP_FILE_NAME);
            if (propList1 == null) throw new Exception("Null Property File For ComponentLOC");
            updateForRepeatedRows(propList1, keyValues, "ComponentLoan", dcm);
        }
        catch(Exception ex)
        {
            logger.error("Exception @handleUpdateApplicantAddress");
            logger.error(ex);
            throw new Exception("Update entity handler exception");
        }
    }


    /**
     * <p>executeCriteriaLoanModel</p>
     * <p>Description: execute Component Loan Model</p>
     * 
     * @param pg
     * @param dealId
     * @param copyId
     * 
     * XS_2.37
     */
    protected void executeCriteriaLoanModel(PageEntry pg, int dealId, int copyId) {

        doComponentLoanModel theDO = (
                doComponentLoanModel)
                (RequestManager.getRequestContext().getModelManager().getModel(
                        doComponentLoanModel.class));

        theDO.clearUserWhereCriteria();
        theDO.addUserWhereCriterion(doComponentLoanModel.FIELD_DFDEALID, "=",
                String.valueOf(dealId));
        theDO.addUserWhereCriterion(doComponentLoanModel.FIELD_DFCOPYID, "=",
                String.valueOf(copyId));
        executeModel(theDO, "CDH@@executeCriteriaLoanModel", false);
    }

    /**
    * pupulate javascript validation for Loan Section
    * @version 1.1 - modified for artf745007
    */
    protected void prepareComponentLoanDisplayFields(int rowIndex) {

        HtmlDisplayFieldBase df = (HtmlDisplayFieldBase) getCurrNDPage()
        .getDisplayField("RepeatedLoan/tbLoanAmount");
        String jsValid = "onBlur=\"if(isFieldInDecRange(0, 99999999999.99)) isFieldDecimal(2);\"";
        df.setExtraHtml(jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLoan/tbDiscountLoan");
        String extraId = "id='tbDiscountLOC" + rowIndex + "'";
        jsValid = "onBlur=\"if(isFieldInDecRange(0, 100)) isFieldDecimal(3);\"";
        df.setExtraHtml(extraId + jsValid);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLoan/tbPremiumLoan");
        extraId = "id='tbPremium" + rowIndex + "'";
        String jsChange = "onBlur=\"if(isFieldInDecRange(0, 100)) isFieldDecimal(3);\"";
        df.setExtraHtml(extraId + jsChange);

        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLoan/tbAdditionalInfoLoan");
        jsValid = "onBlur=\"return (isFieldHasSpecialChar() && isFieldMaxLength(256));\"";
        df.setExtraHtml(jsValid);
        
        /***************MCM Impl team changes starts - artf745007 *********************/
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLoan/txActualPayYearsLoan");
        jsValid = "onBlur=\"if(isFieldInteger(0)) isFieldValidDuration('txActualPayYearsLoan','txActualPayMonthsLoan');\"";
        df.setExtraHtml(jsValid);
        
        df = (HtmlDisplayFieldBase) getCurrNDPage().getDisplayField(
        "RepeatedLoan/txActualPayMonthsLoan");
        jsValid = "onBlur=\"if(isFieldInteger(0)) isFieldValidDuration('txActualPayYearsLoan','txActualPayMonthsLoan');\"";
        df.setExtraHtml(jsValid);
        /***************MCM Impl team changes starts - artf745007 *********************/

    }

    /**
     * this method populates component type combobox based on componenttypeid
     * @param cbComponentType
     * @param componentTypeid
     */
    private void populateComponentCbComponentTypeCommon(
            ComboBox cbComponentType, int componentTypeid) {

        SessionResourceKit srk = this.getSessionResourceKit();
        int langId = theSessionState.getLanguageId();
        int instId = srk.getExpressState().getDealInstitutionId();
        String description = BXResources.getPickListDescription(instId, "COMPONENTTYPE", componentTypeid, langId);
        Option op = new Option(description, String.valueOf(componentTypeid));

        OptionList options = new OptionList();
        options.add(op);
        cbComponentType.setOptions(options);
    }


    /**
     * <P>
     * populateSectionTitleAndSectionDevider
     * </p>
     * <p>
     * This method populates each section's title and lines (section divider). 
     * A section title should be displayed only when the "titleIndex" is 0 and
     * also it has to be translated to either English or French. A section
     * divider has to display only when the "titleIndex" is 0, or it has to
     * display tile divider line instead.
     * 
     * MCM artf741023 - Jul 11, 2008
     * 
     * @param childTitleName
     * @param childSectionDeviderName
     * @param tileIndex
     * @param componentTypeid
     * @author MCM team
     */
    private void populateSectionTitleAndSectionDevider(String childTitleName,
            String childSectionDeviderName, int tileIndex, int componentTypeid) {

        // line and section title 
        StaticTextField titleField = (StaticTextField) getCurrNDPage()
                .getDisplayField(childTitleName);

        ImageField sectionBarField = (ImageField) getCurrNDPage()
                .getDisplayField(childSectionDeviderName);

        if (tileIndex == 0) {
            sectionBarField.setValue("../images/dark_bl.gif");
            StringBuffer title = new StringBuffer();
            title.append(BXResources.getPickListDescription(theSessionState
                    .getDealInstitutionId(), PKL_CDSECTIONTITLE,
                    componentTypeid, theSessionState.getLanguageId()));
            title.append("<br><br>");
            titleField.setValue(title.toString());
            sectionBarField.setExtraHtml("width=\"100%\" height=\"2\"");
        } else {
            //mtgSectionBar.setValue("../images/blue_line.gif");
            sectionBarField.setValue("../images/dark_bl.gif");
            sectionBarField.setExtraHtml("width=\"100%\" height=\"1\"");
            titleField.setValue(""); //artf771395
        }

    }

    
    
    /****************MCM Impl Team XS_2.26 changes starts********************/
    /**
     * Description: Handle AddComponent Button
     * @version 1.0 Initial version
     * @throws Exception
     * Added doCalculation call while adding components And forceChange("estimatedClosingDate")
     *   
     */
    public void handleAddComponent() throws Exception {

        PageEntry pg = getTheSessionState().getCurrentPage();
        
        /******** MCM Impl team changes starts -  artf742919 *****/ 
        if (disableEditButton(pg) == true) return;
        /******** MCM Impl team changes starts -  artf742919 *****/ 
                
        srk = getSessionResourceKit();
        CalcMonitor dcm = CalcMonitor.getMonitor(srk);//FXP23117, MCM Nov 1, 2008
        
        int dealId = pg.getPageDealId();
        int copyId = pg.getPageDealCID();

        int componentTypeId = 0;
        int mtgprodId = 0;

        ComboBox comboBox = (ComboBox) getCurrNDPage().getDisplayField(
                "cbComponentType");
        Object val = comboBox.getValue();
        if ((val != null) && (val.toString().length() != 0)) {
            componentTypeId = TypeConverter.asInt(val);
        }

        comboBox = (ComboBox) getCurrNDPage().getDisplayField(
                "cbComponentProduct");
        val = comboBox.getValue();
        if ((val != null) && (val.toString().length() != 0)) {
            mtgprodId = TypeConverter.asInt(val);
        }

        _log.info("Handle AddComponent Component TypeId: " + componentTypeId);
        _log.info("Handle AddComponent MtgProdId: " + mtgprodId);

        srk.beginTransaction();
        Component comp = new Component(srk, dcm); //FXP23117, MCM Nov 1, 2008, added dcm
        comp = comp.create(dealId, copyId, componentTypeId, mtgprodId);

        ComponentSummary compSummary = new ComponentSummary(srk, dcm); //FXP23117, MCM Nov 1, 2008, added dcm
        ComponentSummaryPK compSummaryPk = new ComponentSummaryPK(dealId, copyId);
        
        // create component summary
        try {
            compSummary.setSilentMode(true);//when adding first component, avoid exception log. 
            compSummary = compSummary.findByPrimaryKey(compSummaryPk);
        } catch (FinderException fe) {
            _log.warn("IGNORED:: Can not find the Component Summary: dealId =" +
                       dealId + ", copyId=" + copyId);
            compSummary = compSummary.create(dealId, copyId);
        }
        
        switch (componentTypeId) {
            case Mc.COMPONENT_TYPE_MORTGAGE: {
                createMortgageComponent(comp, dcm);//FXP23117, MCM Nov 1, 2008, added dcm
                break;
            }
            case Mc.COMPONENT_TYPE_LOC: {
                createLocComponent(comp, dcm);//FXP23117, MCM Nov 1, 2008, added dcm
                break;
            }
            case Mc.COMPONENT_TYPE_LOAN: {
                createLoanComponent(comp, dcm);//FXP23117, MCM Nov 1, 2008, added dcm
                break;
            }
            case Mc.COMPONENT_TYPE_CREDITCARD: {
                createCreditCardComponent(comp, dcm);//FXP23117, MCM Nov 1, 2008, added dcm
                break;
            }
            case Mc.COMPONENT_TYPE_OVERDRAFT: {
                createOverdraftComponent(comp, dcm);//FXP23117, MCM Nov 1, 2008, added dcm
                break;
            }
            default: {
                _log.info("Component type zero, we don't create new component");
                // for default we don't create new component.
                break;
            }
        }

        if (pg.editable) {
            pg.setPageCondition3(true);
        }
        
        //FXP23117, MCM Nov 1, 2008, start
        try {         
            doCalculation(dcm);
        } catch(Exception ex)  {
            _log.error("Exception @ComponentDetailsHandler:handleAddComponent: doCald");
            _log.error(ex);
            _log.error(StringUtil.stack2string(ex));
            throw ex;
        }
        //FXP23117, MCM Nov 1, 2008, end

        // MCM FIXED - artf751756 STARTS
//        Collection comps = comp.findByDeal(new DealPK (dealId, copyId));
//        CalcMonitor dcm   = CalcMonitor.getMonitor(srk);
//        compSummary.setCalcMonitor(dcm);
//        compSummary.setNumberOfComponents(comps.size());
//        compSummary.ejbStore();
        
        srk.commitTransaction();
        
        // BUG FIX - XS_11.15 MCM IMPL STARTS
//        try
//        {
//          doCalculation(dcm);
//        }
//        catch (Exception ex)
//        {
//          logger.error("Exception @handleAddComponent :: " + ex.getMessage());
//        }
        // BUG FIX - XS_11.15 MCM IMPL ENDS
        // MCM FIXED - artf751756 ENDS
    
    }

    /**
     * Create MortgageComponent
     * @param comp
     * @throws Exception
     * FXP23117, MCM Nov 1, 2008, added dcm
     */
    private void createMortgageComponent(Component comp, CalcMonitor dcm) throws Exception {
        _log.info("Create Mortgage Component");
        ComponentMortgage componentMortgage = new ComponentMortgage(srk, dcm);
        componentMortgage.create(comp.getComponentId(), comp.getCopyId());
    }

    /**
     *Create Loc Component
     * @param comp
     * @throws Exception
     * FXP23117, MCM Nov 1, 2008, added dcm
     */
    private void createLocComponent(Component comp, CalcMonitor dcm) throws Exception {
        _log.info("Create Loc Component");
        ComponentLOC componentLoc = new ComponentLOC(srk, dcm);
        componentLoc.create(comp.getComponentId(), comp.getCopyId());
    }

    /**
     * Create Loan Component
     * @param comp
     * @throws Exception
     * FXP23117, MCM Nov 1, 2008, added dcm
     */
    private void createLoanComponent(Component comp, CalcMonitor dcm) throws Exception {
        _log.info("Create Loan Component");
        ComponentLoan componentLoan = new ComponentLoan(srk, dcm);
        componentLoan.create(comp.getComponentId(), comp.getCopyId());
    }

    /**
     * Create Credit Card Component
     * @param comp
     * @throws Exception
     * FXP23117, MCM Nov 1, 2008, added dcm
     */
    private void createCreditCardComponent(Component comp, CalcMonitor dcm) throws Exception {
        _log.info("Create Credit Card Component");
        ComponentCreditCard componentCreditCard = new ComponentCreditCard(srk, dcm);
        componentCreditCard.create(comp.getComponentId(), comp.getCopyId());
    }

    /**
     * Create Overdraft Component
     * @param comp
     * @throws Exception
     * FXP23117, MCM Nov 1, 2008, added dcm
     */
    private void createOverdraftComponent(Component comp, CalcMonitor dcm) throws Exception {
        _log.info("Create Overdraft Component");
        ComponentOverdraft componentOverdraft = new ComponentOverdraft(srk, dcm);
        componentOverdraft.create(comp.getComponentId(), comp.getCopyId());
    }
    
    /****************MCM Impl Team XS_2.26 changes ends********************/
    
    /****************MCM Impl Team XS_2.32 changes starts********************/
    /**
     * method to remove componentsummary
     * @throws Exception
     */
    public void updateComponentSummary() throws Exception {

        PageEntry pg = getTheSessionState().getCurrentPage();
        int dealId = pg.getPageDealId();
        int copyId = pg.getPageDealCID();

        DealPK dealPk = new DealPK(dealId, copyId);
        Component comp = new Component(srk);
        Collection c = comp.findByDeal(dealPk);
        CalcMonitor dcm = CalcMonitor.getMonitor(getSessionResourceKit());
        ComponentSummary compSummary = new ComponentSummary(srk);
        ComponentSummaryPK compSummaryPk = new ComponentSummaryPK(dealId,
                copyId);
        compSummary.findByPrimaryKey(compSummaryPk);
        
        SessionResourceKit srk = getSessionResourceKit();
        srk.beginTransaction();
        if (c.size() == 0) {
            compSummary.ejbRemove();
        } 
//        else {
//            compSummary.setCalcMonitor(dcm);
//            compSummary.setNumberOfComponents(c.size());
//            compSummary.ejbStore();
//        }
        srk.commitTransaction();
//        doCalculation(dcm);
    }
    
    /**
     * <p>handleCustomActMessageOk</p>
     * <p>Handle custom confirm message</p>
     * 
     * @param args String[]
     * @see mosApp.MosSystem.DealHandlerCommon#handleCustomActMessageOk(java.lang.String[])
     * 
     * @version 1.29 fixed for artf751756: change to set trigger for calc
     */
    public void handleCustomActMessageOk(String[] args) {
        if (args == null)
            return;

        if (args[0].equals(Sc.CONFIRMDELETE)) {
            handleDelete((theSessionState.getActMessage()).getResponseObject());
            try {
                updateComponentSummary();
            } catch (Exception e) {
                // log
            }
        }
    }
    
    /****************MCM Impl Team XS_2.32 changes ends********************/
 
    //FXP23869, MCM/4.1, Jan 20 2009, saving/resetting screen scroll position - start.
    private void saveWindowScrollPosition(){

    	ViewBean thePage = getCurrNDPage();
    	TextField tx = (TextField)thePage.getDisplayField(
    			pgComponentDetailsViewBean.CHILD_TXWINDOWSCROLLPOSITION);
    	String scrollPos = (String)tx.getValue();
    	if(scrollPos == null || scrollPos.length() < 1)return; // do nothing
    	
    	PageEntry pg = theSessionState.getCurrentPage();
		Hashtable pst = pg.getPageStateTable();
		if (pst == null)
		{
			pst = new Hashtable();
			pg.setPageStateTable(pst);
		}
    	pst.put("SCROLL_POSITION", scrollPos);
    	
    }
    
    private void reapplyWindowScrollPosition(){
    	ViewBean thePage = getCurrNDPage();
    	TextField tx = (TextField)thePage.getDisplayField(
    			pgComponentDetailsViewBean.CHILD_TXWINDOWSCROLLPOSITION);
    	tx.setValue("");
    	
    	PageEntry pg = theSessionState.getCurrentPage();
    	Hashtable pst = pg.getPageStateTable();
    	if (pst == null) return; // do nothing
    	String scrollPos = (String)pst.get("SCROLL_POSITION");
    	if (scrollPos == null || scrollPos.length() < 1) return; //do nothing
    	tx.setValue(scrollPos);
    	pst.put("SCROLL_POSITION", ""); // clean
    	
    }
    //FXP23869, MCM/4.1, Jan 20 2009, saving/resetting screen scroll position - end.
    
    
}

