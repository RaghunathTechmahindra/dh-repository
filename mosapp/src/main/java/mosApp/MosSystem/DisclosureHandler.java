package mosApp.MosSystem;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletResponse;
import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.docprep.Dc;
import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docprep.xsl.PDFDocumentFactory;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DisclosureQuestion;
import com.basis100.deal.entity.MailDestinationType;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.DealStatusManager;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.html.HtmlDisplayFieldBase;

/**
 * <p>Title: DisclosureHandler.java </p>
 *
 * <p>Description: Disclosure Handler (for all provinces) </p>
 *
 * <p>Copyright: </p>
 *
 * <p>Company: </p>
 *
 * @author Cartland Chiu
 * @version 1.0
 */

//===================================================================================
//
//  PageCondition1 : true - indicates that user responded to "Disclosure form is available" dialog
//===================================================================================
//
//  PageCondition2 : true - indicates that user wants to use saved Disclosure form info (response to "Disclosure form is available" form)
//===================================================================================

public class DisclosureHandler extends PageHandlerCommon
    implements Cloneable, Sc {

  private Deal deal;
  private DisclosureTypeFactory dtf = new DisclosureTypeFactory();

	public int provinceId;

  public DisclosureHandler() {
    super();
  }
  public DisclosureHandler(String name) {
      super(name);
    }
  // defines the mapping between a pgViewBean element and a disclosure question in the DB
  private class DisclosureQuestionBean {
    // the name used in the JSP
    public String questionCode;
    // Used to construct the getter method name in the pgXXXViewBean
    public String pgViewGetterMethodName;
    // the name of the static member in the pgXXXViewBean
    public String pgViewMemberName;
    // is this field a resource bundle key? see DB schema
    public boolean isResourceBundleKey;
  }

  // hierarchy of constants classes, one class per disclosure type
  private abstract class DisclosureType {
    private List questionList = new ArrayList();
    public abstract int getDocumentTypeId();
    private Iterator it = null;

    public void addQuestion(String questionCode, 
String pgViewGetterMethodName, String pgViewMemberName, boolean isResourceBundleKey) {
      DisclosureQuestionBean dq = new DisclosureQuestionBean();
      dq.questionCode = questionCode;
      dq.pgViewGetterMethodName = pgViewGetterMethodName;
      dq.pgViewMemberName = pgViewMemberName;
      dq.isResourceBundleKey = isResourceBundleKey;

      questionList.add(dq);
    }

    public DisclosureQuestionBean getNextQuestion() {
      if (it == null) {
        it = questionList.iterator();
      }

      return (DisclosureQuestionBean)it.next();
    }

    public boolean moreQuestions() {
      return it.hasNext();
    }

    public void resetList() {
      it = questionList.iterator();
    }

    public int numberOfQuestions() {
      return questionList.size();
    }
    
    public abstract String getProvinceAbbrev();
  }
  /**
	 * <p>
	 * Title: PEIDisclosureType
	 * <p>
	 * Description: Inner class for "Prince Edward Island" (PEI) This class
	 * extends the abstract class DisclosureType
	 * <p>
	 * Company: Filogix Inc. <br>
	 * Date: 11/28/2006
	 * 
	 * @author NBC Implementation Team
	 * @version 1.0
	 */
	private class PEIDisclosureType extends DisclosureType {
		/**
		 * PEIDisclosureType(int)- constructor which adds the question ids for
		 * all the fields in the page to load the base elements common to the
		 * page
		 * 
		 * @param -
		 *            provId int, provinceid of the Prince Edward Island called from the
		 *            <code>DisclosureTypeFactory</code><br>
		 *            Date: 11/28/2006
		 */
		public PEIDisclosureType(int provId) {
			addQuestion("106Q010A_M", "getCbDateToAppearMonthPE",
					"CHILD_CBDATETOAPPEARMONTH_PE", true);
			addQuestion("106Q010A_D", "getTxDateToAppearDayPE",
					"CHILD_TXDATETOAPPEARDAY_PE", false);
			addQuestion("106Q010A_Y", "getTxDateToAppearYearPE",
					"CHILD_TXDATETOAPPEARYEAR_PE", false);
			addQuestion("106Q080V", "getTxTermLoanVar", "CHILD_TXTERMLOANVAR",
					false);
			addQuestion("106Q10AB", "getTxActPercentVar", "CHILD_TXACTPERCVAR",
					false);
			addQuestion("106Q11AC", "getTxTermsConds", "CHILD_TXTERMSCONDS",
					false);
			addQuestion("106Q12AD", "getTxCharges", "CHILD_TXCHARGES", false);
		}

		/**
		 * getDocumentTypeId <br>
		 * This method is the implementation for the abstract method defined in
		 * DisclosureType <br>
		 * Returns the documenttypeid for PEI disclosure document. <br>
		 * Date: 11/28/2006
		 * 
		 * @return The document type ID.
		 */
		public int getDocumentTypeId() {
			int docTypeId = Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_PE;

			return docTypeId;
		}

		/**
		 * getProvinceAbbrev <br>
		 * This method is the implementation for the abstract method defined in
		 * DisclosureType <br>
		 * Returns the province name abbreviation for PEI disclosure document.
		 * <br>
		 * Date: 11/28/2006
		 * 
		 * @return Province name abbreviation.
		 */
		public String getProvinceAbbrev() {
			return Dc.PROVINCE_ABBR_DISCLOSURE_PEI;
		}

	}

	/**
	 * <p>
	 * Title: NLDisclosureType
	 * <p>
	 * Description: Inner class for "NewFoundLand and Labrador" This class
	 * extends the abstract class DisclosureType
	 * <p>
	 * Company: Filogix Inc. <br>
	 * Date: 11/28/2006
	 * 
	 * @author NBC Implementation Team
	 * @version 1.0
	 */
	private class NLDisclosureType extends DisclosureType {

		/**
		 * NLDisclosureType <br>
		 * Constructor for class NLDisclosureType. <br>
		 * Date: 11/28/2006
		 */
		public NLDisclosureType(int provId) {
			addQuestion("103Q050U", "getTxAnnualPercentNL",
					"CHILD_TXANNPERCRATE_NL", true);
			addQuestion("103Q080X", "getTxPaymentVar", "CHILD_TXPAYVAR", true);
			addQuestion("103Q12AE", "getTxRightRepay", "CHILD_TXARIGHTREPAY",
					true);
		}

		/**
		 * getDocumentTypeId <br>
		 * This method is the implementation for the abstract method defined in
		 * DisclosureType <br>
		 * Returns the documenttypeid for NL disclosure document. <br>
		 * Date: 11/28/2006
		 * 
		 * @return The document type ID.
		 */
		public int getDocumentTypeId() {
			return Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_NL;
		}

		/**
		 * getProvinceAbbrev <br>
		 * This method is the implementation for the abstract method defined in
		 * DisclosureType <br>
		 * Returns the province name abbreviation for NL disclosure document.
		 * <br>
		 * Date: 11/28/2006
		 * 
		 * @return Province name abbreviation.
		 */
		public String getProvinceAbbrev() {
			return Dc.PROVINCE_ABBR_DISCLOSURE_NL;
		}
	}

	/**
	 * ABDisclosureType Description: This is a subclass for AB [implementation
	 * of a sub class - part of the factory pattern]
	 * 
	 * Company: Filogix Inc.<br>
	 * Date 11/28/2006
	 * 
	 * @author NBC Implementation Team
	 * 
	 */

	private class ABDisclosureType extends DisclosureType {
		/**
		 * ABDisclosureType(int)- constructor which adds the question ids for
		 * all the fields in the page to load the base elements common to the
		 * page
		 * 
		 * @param -
		 *            provId int, provinceid of the Alberta called from the
		 *            <code>DisclosureTypeFactory</code><br>
		 *            Date: 11/28/2006
		 */
		public ABDisclosureType(int provId) {
			// AB
			addQuestion("101Q090V", "getTxBonusDiscount",
					"CHILD_TXBONUSDISCOUNT", true);
			addQuestion("101Q080U", "getTxMortgageTerm", "CHILD_TXTERMMORT",
					true);
			addQuestion("101Q10AC", "getTxAnnualPercent",
					"CHILD_TXANNPERCRATE", true);
			addQuestion("101Q10AD", "getTxTermsCondsAB",
					"CHILD_TXTERMSCONDS_AB", true);
		}

		/**
		 * getDocumentTypeId() This method returns the documenttypeid for
		 * province Alberta(AB)'s Disclosure Document<br>
		 * Date: 11/28/2006
		 * 
		 * @return - int returns the documenttypeid from the documenttype table
		 */
		public int getDocumentTypeId() {
			return Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_AB;
		}

		/**
		 * getProvinceAbbrev() This method returns the province name for
		 * province Alberta(AB)'s Disclosure Document in abbreviatred fopr<br>
		 * Date: 11/28/2006
		 * 
		 * @return - String <br>
		 *         province name in abbreviation
		 * 
		 */
		public String getProvinceAbbrev() {
			return Mc.PROVINCE_ABBR_DISCLOSURE_AB;
		}

	}

	/**
	 * NSDisclosureType Description: This is a subclass for NS [implementation
	 * of a sub class - part of the factory pattern].
	 * 
	 * Company: Filogix Inc. Date 28/Nov/2006
	 * 
	 * @author NBC/PP Implementation Team
	 * 
	 */
	private class NSDisclosureType extends DisclosureType {
		
		/**
		 * NSDisclosureType(int)- constructor which adds the question ids for
		 * all the fields in the page to load the base elements common to the
		 * page
		 * 
		 * @param -
		 *            provId int, provinceid of the Nova Scotia called from the
		 *            <code>DisclosureTypeFactory</code><br>
		 *Date: 11/28/2006
		 */

		public NSDisclosureType(int provId) {
			
			addQuestion("104Q010A_M", "getCbDateToAppearMonthNS",
					"CHILD_CBDATETOAPPEARMONTH_NS", true);
			addQuestion("104Q010A_D", "getTxDateToAppearDayNS",
					"CHILD_TXDATETOAPPEARDAY_NS", true);
			addQuestion("104Q010A_Y", "getTxDateToAppearYearNS",
					"CHILD_TXDATETOAPPEARYEAR_NS", true);
			addQuestion("104Q090W", "getTxCalcInt", "CHILD_TXCALCINTEREST",
					true);
			addQuestion("104Q14AE_Y", "getCkConstMortYes",
					"CHILD_CKCONSTMORTYES", true);
			addQuestion("104Q14AE_N", "getCkConstMortNo",
					"CHILD_CKCONSTMORTNO", true);
			addQuestion("104Q15AF", "getTxRateVar", "CHILD_TXRATEVAR", true);
			addQuestion("104Q16AG", "getTxRepayBefMat", "CHILD_TXREPBEFMAT",
					true);
			addQuestion("104Q17AH", "getTxLoanNotRepd", "CHILD_TXLOANNOTREPD",
					true);
			addQuestion("104Q17AI_Y", "getCkTaxesPaidYes",
					"CHILD_CKTAXESPAIDYES", true);
			addQuestion("104Q17AI_N", "getCkTaxesPaidNo",
					"CHILD_CKTAXESPAIDNO", true);
			addQuestion("104Q18AJ", "getTxIntPaidManner",
					"CHILD_TXINTPAIDMANNER", true);
			
		}

		/**
		 * getDocumentTypeId() This method returns the documenttypeid for NS
		 * disclosure document. Date: 11/28/2006
		 * 
		 * @return - int <br>
		 *         documenttypeid
		 * 
		 */
		public int getDocumentTypeId() {
			// this is the id in the DOCUMENTTYPE table
			
			return Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_NS;
		}

		/**
		 * getProvinceAbbrev() This method returns the province name in
		 * abbreviation for NS disclosure document. Date: 11/28/2006
		 * 
		 * @return province name in abbreviation.
		 * 
		 */
		public String getProvinceAbbrev() {
			
			return Mc.PROVINCE_ABBR_DISCLOSURE_NS;
		}

	}
  private class ONDisclosureType extends DisclosureType {
		int provId;

		public ONDisclosureType(int provId) {
			this.provId = provId;
			if (provId == Dc.PROVINCE_ONTARIO) {
				addQuestion("020E", "getCbBrkActAsLndr",
						"CHILD_CBBROKERACTASLENDER", true);
				addQuestion("020F", "getTbNatureOfRelation",
						"CHILD_TBNATUREOFRELATIONSHIP", false);
				addQuestion("13UU_MNT", "getCbDateToAppearMonth",
						"CHILD_CBDATETOAPPEARMONTH", true);
				addQuestion("13UU_DAY", "getTxDateToAppearDay",
						"CHILD_TXDATETOAPPEARDAY", false);
				addQuestion("13UU_YEAR", "getTxDateToAppearYear",
						"CHILD_TXDATETOAPPEARYEAR", false);
				addQuestion("08NN", "getTbMortgageNotRenew",
						"CHILD_TBMORTGAGENOTRENEW", false);
				addQuestion("09OO", "getTbOtherTerms", "CHILD_TBOTHERTERMS",
						false);
				addQuestion("11TT", "getTbBrkRegistrationNum",
						"CHILD_TBBRKREGISTRATIONNUM", false);
				addQuestion("060S", "getTxBonusPaidByBorr",
						"CHILD_TXBONUSPAIDBYBORR", false);
			} 
    }

    public int getDocumentTypeId() {
      // this is the id in the DOCUMENTTYPE table
			return Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_ON; // Catherine
					
    }

	public String getProvinceAbbrev() {
		
	
		return "ON";
	}
    
    
  }
  /**
	 * DisclosureTypeFactory Description: Disclosure Handler (for all provinces)
	 * <p>
	 * Company:
	 * </p>
	 * 
	 * @author Cartland Chiu
	 * @version 1.0
	 * 
	 * @version 22
	 * @author Mohammadi Date : 11/24/2006 Updated to use the factory. This
	 *         returns a disclosure type based on the provinceId
	 */

	private class DisclosureTypeFactory {
		public DisclosureType getDisclosureType(String pgName, Deal dealObj,
				SessionResourceKit srk) {
			int copyId = dealObj.getCopyId();

			Property prop;
			try {
				prop = new Property(srk);
				prop = prop.findByPrimaryProperty(dealObj
            .getDealId(), copyId, dealObj.getInstitutionProfileId());
				provinceId = prop.getProvinceId();
				
			} catch (RemoteException e) {
				logger
						.error("ERROR: RemoteException @getDisclosureType(pgName,dealObj)"
								+ e.getMessage());

			} catch (FinderException e) {
				logger
						.error("ERROR: FinderException @getDisclosureType(pgName,dealObj)"
								+ e.getMessage());
			}

			if (provinceId == Dc.PROVINCE_ALBERTA) {
				return new ABDisclosureType(provinceId);
			} else if (provinceId == Dc.PROVINCE_PRINCE_EDWARD_ISLAND) {
				return new PEIDisclosureType(provinceId);
			} else if (provinceId == Dc.PROVINCE_ONTARIO) {
				return new ONDisclosureType(provinceId);
			} else if (provinceId == Dc.PROVINCE_NOVA_SCOTIA) {
				return new NSDisclosureType(provinceId);
			} else if (provinceId == Dc.PROVINCE_NEWFOUNDLAND) {
				return new NLDisclosureType(provinceId);
			} else {
				return new ONDisclosureType(provinceId);
			}

		}
	}

    public DisclosureHandler(PageHandlerCommon other) {
      super();
      initFromOther(other);
    }

    public DisclosureHandler cloneSS() {
      return (DisclosureHandler)super.cloneSafeShallow();
    }

    /**
     * 	This method is used to populate the fields in the header with the appropriate information
     */
    public void populatePageDisplayFields()
    {
      PageEntry pg = theSessionState.getCurrentPage();

      Hashtable pst = pg.getPageStateTable();

      populatePageShellDisplayFields();

      // Quick Link Menu display
      displayQuickLinkMenu();

      populateTaskNavigator(pg);

      populatePageDealSummarySnapShot();

      populatePreviousPagesLinks();
      
      //solve the cancel button losing user input problem
	String sInstr = "";
	// Added by Docs Team for smooth merge --- START
	if (getCurrNDPage().getDisplayFieldValue("hdInstr") != null) {
		sInstr = ((String) getCurrNDPage().getDisplayFieldValue("hdInstr"))
			.toString();
	}
	// Added by Docs Team for smooth merge --- END
	if (sInstr.equalsIgnoreCase("cancel")) {
		DisclosureType disclosureDetails = dtf.getDisclosureType(pg
				.getPageName(), getDeal(), srk);
    	 ViewBean currentViewBean = getCurrNDPage();

	 currentViewBean.setDisplayFieldValue("hdCopyId", String.valueOf(pg
					.getPageDealCID()));
    	if (disclosureDetails.numberOfQuestions() > 0) {

    	        disclosureDetails.resetList();

    	        // use reflection to bind disclosure questions to pgViewBeans
    	        while (disclosureDetails.moreQuestions()) {

    	          DisclosureQuestionBean question = disclosureDetails
.getNextQuestion();

    	          doDisclosureSelectModel discloseDO = (doDisclosureSelectModel) RequestManager
.getRequestContext().    	              getModelManager().getModel(
doDisclosureSelectModel.class);

    	          // use reflection to set the element on the ViewBean
    	          Class c = currentViewBean.getClass();
    	          String methodName = question.pgViewGetterMethodName;

    	          try {
    	            Method m = c.getMethod(methodName, new Class[] {});
    	            
    	            DisplayField pgViewBeanElement = (DisplayField)m
.invoke(currentViewBean, new Object[] {});
    	            String _value =((String) getCurrNDPage()
.getDisplayFieldValue(
pgViewBeanElement.getName())).toString();
    	            pgViewBeanElement.setValue(_value);
    	          }
    	          catch (Exception ex) {
    	            logger
.error("ERROR @showPageFields(PageEntry) -- Check " 
+ c.toString() 
+ " class. It may be missing the method " 
+ methodName 
+ " and/or the RESET VALUE for " 
+ question.pgViewMemberName);
    	            ex.printStackTrace();
    	          }
    	        }
    	      }
      }
      else
      {
	      /// CUSTOM POPULATION GOES HERE (e.g. set page specific display fields, etc)
	      showPageFields(pg);
      }
	
      //ticket #3096
      setJSValidation();
    }

    public void setJSValidation()
    {
    	
      HtmlDisplayFieldBase df = null;
      String theExtraHtml = null;

      ViewBean thePage = getCurrNDPage();
      

      if (provinceId == Dc.PROVINCE_ONTARIO) {

      //  Validate Month
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("13UU_MNT");
      theExtraHtml = "onBlur=\"isFieldValidDate('13UU_YEAR', '13UU_MNT', '13UU_DAY');\"";
      df.setExtraHtml(theExtraHtml);

      //  Validate Day
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("13UU_DAY");
      theExtraHtml = "onBlur=\"isFieldValidDate('13UU_YEAR', '13UU_MNT', '13UU_DAY');\"";
//      theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " +
//      					"isFieldValidDate('13UU_YEAR', '13UU_MNT', '13UU_DAY');\"";
      df.setExtraHtml(theExtraHtml);

      // Validate Year
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("13UU_YEAR");
      theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) " +
        				"isFieldValidDate('13UU_YEAR', '13UU_MNT', '13UU_DAY');\"";
      df.setExtraHtml(theExtraHtml);

      // Validate [Section 11: Broker Registration No.]
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("11TT");
      theExtraHtml = "onBlur=\"isFieldMaxLength(10);\"";
      df.setExtraHtml(theExtraHtml);
      
//    Validate [Section 2.]
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("020F");
      theExtraHtml = "onKeyPress=\"return limitLength(700);\" onBlur=\"alertMsg(700);\"";
      df.setExtraHtml(theExtraHtml);  

//    Validate [Section 8.]
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("08NN");
      theExtraHtml = "onKeyPress=\"return limitLength(430);\" onBlur=\"alertMsg(430);\"";
      df.setExtraHtml(theExtraHtml);  

//    Validate [Section 9.]
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("09OO");
      theExtraHtml = "onKeyPress=\"return limitLength(480);\" onBlur=\"alertMsg(480);\"";
      df.setExtraHtml(theExtraHtml);  
      
//    Validate [Section 6.]
      df = (HtmlDisplayFieldBase)thePage.getDisplayField("060S");
      theExtraHtml = "onBlur=\"isPositiveNumeric(isDecimal(this.value)); \"";
      df.setExtraHtml(theExtraHtml);
			// PEI Fields
		} else if (provinceId == Dc.PROVINCE_PRINCE_EDWARD_ISLAND) {

			// Validate Month
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("106Q010A_M");
			theExtraHtml = "onBlur=\"isFieldValidDate('106Q010A_Y', '106Q010A_M', '106Q010A_D');\"";
			df.setExtraHtml(theExtraHtml);
			// Tar : 1696 - Modified the Java Script for Day field validation. 
			// Validate Day
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("106Q010A_D");
			theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " +  
				" isFieldValidDate('106Q010A_Y', '106Q010A_M', '106Q010A_D');\"";
			// theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " +
			// "isFieldValidDate('13UU_YEAR', '13UU_MNT', '13UU_DAY');\"";
			df.setExtraHtml(theExtraHtml);

			// Validate Year
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("106Q010A_Y");
			theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) "
					+ "isFieldValidDate('106Q010A_Y', '106Q010A_M', '106Q010A_D');\"";
			df.setExtraHtml(theExtraHtml);

			// Validate [Section 8.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("106Q080V");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);

			// Validate [Section 10.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("106Q10AB");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);

			// Validate [Section 1.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("106Q11AC");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);

			// Validate [Section 12.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("106Q12AD");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);

		} else if (provinceId == Dc.PROVINCE_ALBERTA) {
			//	Validate [Section 9.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("101Q090V");
			theExtraHtml = "onBlur=\"isFieldInDecRange(0, 99999999999.99);isFieldDecimal(2);\"";
			df.setExtraHtml(theExtraHtml);
			// Validate [Section 8.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("101Q080U");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);
			// Validate [Section 10.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("101Q10AC");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);
			// Validate [Section 11.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("101Q10AD");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);

		} else if (com.basis100.deal.docprep.Dc.PROVINCE_NEWFOUNDLAND == provinceId) {
			// Validate [Section 5.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("103Q050U");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);
			// Validate [Section 8.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("103Q080X");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);
			// Validate [Section 12.]
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("103Q12AE");
			theExtraHtml = "onKeyPress=\"return limitLength(2000);\" onBlur=\"alertMsg(2000);\"";
			df.setExtraHtml(theExtraHtml);
		} else if (provinceId == Dc.PROVINCE_NOVA_SCOTIA) {
			

			// Validate Month
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("104Q010A_M");
			theExtraHtml = "onBlur=\"isFieldValidDate('104Q010A_Y', '104Q010A_M', '104Q010A_D');\"";
			df.setExtraHtml(theExtraHtml);
			// Tar : 1696 - Modified the Java Script for Day field validation. 
			// Validate Day
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("104Q010A_D");
			theExtraHtml = "onBlur=\"if(isFieldInIntRange(1, 31)) " + 
				"isFieldValidDate('104Q010A_Y', '104Q010A_M', '104Q010A_D');\"";
			df.setExtraHtml(theExtraHtml);

			// Validate Year
			df = (HtmlDisplayFieldBase) thePage.getDisplayField("104Q010A_Y");
			theExtraHtml = "onBlur=\"if(isFieldInIntRange(1800, 2100)) "
					+ "isFieldValidDate('104Q010A_Y', '104Q010A_M', '104Q010A_D');\"";
			df.setExtraHtml(theExtraHtml);
		}
    }
    
    public void handleCustomActMessageOk(String[] args) {
      PageEntry pg = theSessionState.getCurrentPage();
      // called after user responds OK to "Disclosure form is available" dialog.
      if (args[0].equals("DISCLOSUREY") || args[0].equals("DISCLOSUREN")) {
        if (args[0].equals("DISCLOSUREY")) {
          pg.setPageCondition2(true);
          logger.debug("DisclosureHandler@handleCustomActMessageOk.  User clicked OK on disclosure form available dialog ");
        }
        else {
          pg.setPageCondition2(false);
          logger.debug("DisclosureHandler@DisclosureHandler.  User clicked Cancel on disclosure form available dialog ");
        }
        // continue execution from where we left off
        showPageFields(pg);
      }
    }

    /**
     * Generic method to bind disclsoure questions to pgViewBean elements
     * When adding new disclosure types (provinces), you do not have to modify this method.  Simply extend DisclosureType
     * @param pg PageEntry
     */
    private void showPageFields(PageEntry pg) {

	// determine the document type constant from a factory method, using the
	// page name as the parameter
        

	DisclosureType disclosureDetails = dtf.getDisclosureType(pg
			.getPageName(), getDeal(), srk);
	
      ViewBean currentViewBean = getCurrNDPage();
  	

      currentViewBean.setDisplayFieldValue("hdCopyId", String.valueOf(pg.getPageDealCID()));

      // force the PHC to display the confirmation dialog. [ticket 3106]
      pg.setModified(true);      
     

      // do this for each question
      // page condition 2 refers to the "disclosure form is available" dialog
      if (disclosureDetails.numberOfQuestions() > 0) {
    	    

        disclosureDetails.resetList();
        

        // use reflection to bind disclosure questions to pgViewBeans
        while (disclosureDetails.moreQuestions()) {

          DisclosureQuestionBean question = disclosureDetails.getNextQuestion();

          doDisclosureSelectModel discloseDO =
              (doDisclosureSelectModel) RequestManager.getRequestContext().
              getModelManager().getModel(doDisclosureSelectModel.class);

          discloseDO.clearUserWhereCriteria();
          // add where criterias
          discloseDO.addUserWhereCriterion("dfDealId", "=",
                                           new Integer(pg.getPageDealId()));

          discloseDO.addUserWhereCriterion("dfDocumentTypeId", "=",
                                           new Integer(disclosureDetails.
                                                       getDocumentTypeId()));
          // add the question id
          discloseDO.addUserWhereCriterion("dfQuestionCode", "=",
                                           new String(question.questionCode));

          try {
            discloseDO.executeSelect(null);
            // since confirmaion page is deleted from FS and retreive page data from DB - Midori
            pg.setPageCondition1(true);
            pg.setPageCondition2(true);
            // there is a record.  advance it and ask the user if we should load from the DB
            // this value is used later to bind to the pgViewBean.  see below.
            if (discloseDO.next()) {

              // has this message been prompted already?
              if (!pg.getPageCondition1()) {
//                Deleted: FS has been changed                 
//                pg.setPageCondition1(true);
//                // set up message
//                setActiveMessageToAlert(BXResources.getSysMsg("DISCLOSURE_RETRIEVE_DATA_CONFIRM",
//                    theSessionState.getLanguageId()), ActiveMsgFactory.ISCUSTOMDIALOG2, "DISCLOSUREY", "DISCLOSUREN");
                return;
              }
            }
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }

          // use reflection to set the element on the ViewBean
          Class c = currentViewBean.getClass();
          String methodName = question.pgViewGetterMethodName;

          try {
            Method m = c.getMethod(methodName, new Class[] {});
            DisplayField pgViewBeanElement = (DisplayField)m.invoke(currentViewBean, new Object[] {});
            // get the value from the DB (model)
            if (pg.getPageCondition2()) {
                pgViewBeanElement.setValue(discloseDO.getDfResponse());
            }
            // reset the value
            else {
              Field f = c.getField(question.pgViewMemberName + "_RESET_VALUE");
              Object value = f.get(null);
              pgViewBeanElement.setValue(value);
            }
	} catch (Exception ex) {
		logger.error("ERROR @showPageFields(PageEntry) -- Check "
			+ c.toString()
			+ " class. It may be missing the method "
			+ methodName + " and/or the RESET VALUE for "
			+ question.pgViewMemberName);
            ex.printStackTrace();
          }
        }
      }
      


    }

    public void setDeal(Deal deal) {
      this.deal = deal;
    }

    /**
     * Like a singleton but at a method level
     * @param pg PageEntry
     * @return Deal
     */
    public Deal getDeal() {
      PageEntry pg = theSessionState.getCurrentPage();

      if (deal == null) {
        int dealId = pg.getPageDealId();
        int copyId = pg.getPageDealCID();

        try {
          deal = DBA.getDeal(getSessionResourceKit(), dealId, copyId);
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }

      return deal;
    }
    
    private Deal getDeal(SessionResourceKit srk, int dealId, int copyId) {
        Deal deal = null;
    	
    	try {
          deal = DBA.getDeal(srk, dealId, copyId);
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
        
        return deal;
    }

    /**
     * Generic Method to update the fields on the pgViewBean to the DB
     * When adding new disclosure types (provinces), you do not have to modify this method.  Simply extend DisclosureType
     */
    private void updateData() {
      PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
      SessionResourceKit srk = getSessionResourceKit();
      SysLogger logger = srk.getSysLogger();

       // determine the document type constant from a factory method, using the
       // page name as the parameter
          DisclosureType disclosureDetails = dtf.getDisclosureType(pg
                          .getPageName(), getDeal(), srk);

      deal = getDeal();

      disclosureDetails.resetList();

      // for each field in the DisclosureType
      while (disclosureDetails.moreQuestions()) {
        DisclosureQuestionBean dq = disclosureDetails.getNextQuestion();

        // first try to look up in the DB.  This is if the user is updating a record
        try {
          DisclosureQuestion disclosureQuestionEntity = new DisclosureQuestion(srk);
          String questionCode = dq.questionCode;

				/*
				 * Fix for ticket 4616 - Mike M Sept 9, 2006
				 * 
				 * Removed the null check for the return of the
				 * disclosureQuestionEntity.findByDealIdAndQuestionCode. The
				 * find was throwing an exception so the code never created the
				 * new entries.
				 * 
				 */

          // look into DB
          try {
            disclosureQuestionEntity = disclosureQuestionEntity
                  .findByDealIdAndQuestionCode(deal.getDealId(), 
                      questionCode);
          }
          catch (Exception finderException) {
            // not found.  we need to insert first
            disclosureQuestionEntity = disclosureQuestionEntity.create(
                new DealPK(deal.getDealId(), deal.getCopyId()), 
                disclosureDetails.getDocumentTypeId(), 
                questionCode, dq.isResourceBundleKey ? "Y": "N");
          }
				/*
				 * End of Fix for Ticket 4616
				 */
          ViewBean currentViewBean = getCurrNDPage();

          // use reflection to set the response on the entity
          Class c = currentViewBean.getClass();
          String methodName = dq.pgViewGetterMethodName;
          // set the field from the pg view bean
          Method m = c.getMethod(methodName, new Class[] {});
          DisplayField pgViewBeanElement = (DisplayField)m.invoke(currentViewBean, new Object[] {});

          disclosureQuestionEntity.setDocumentTypeId(disclosureDetails.getDocumentTypeId());
          disclosureQuestionEntity.setQuestionCode(questionCode);
          disclosureQuestionEntity.setIsResourceBundleKey(dq.isResourceBundleKey);
          disclosureQuestionEntity.setResponse(pgViewBeanElement.stringValue());

          // now update disclosureQuestionEntity
          disclosureQuestionEntity.performUpdate();
        }
        catch (Exception ex) {
          logger.error("ERROR DisclosureHandler@updateData(). In updating or inserting into DB");
          ex.printStackTrace();
        }
      }

    }

    /**
     * Determines if the Submit button should be accessible to the user
     * @return boolean
     */
    public boolean displaySubmitButton() {
      DealStatusManager dsm = DealStatusManager.getInstance(srk);
      int cs = getDeal().getStatusId();

      if (cs == Mc.DEAL_COLLAPSED || cs == Mc.DEAL_DENIED) {
        return false;
      }

      return true;
    }

    /**
     * Call DocPrep to create the document for e-mail, etc...
     * @throws DocPrepException 
     */
    private void callDocPrep(int documentTypeId) throws DocPrepException {
      
      switch (documentTypeId) {
        case Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_AB: {
          
          break;
        } 
        case Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_BC: {
          
          break;
        }  
        case Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_NL: {
          
          break;
        }  
        case Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_NS: {
          
          break;
        }  
        case Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_ON: {
          DocumentRequest.requestDisclosureON(this.srk, this.deal);
          break;
        } 
        case Dc.DOCUMENT_GENERAL_TYPE_DISCLOSURE_PE: {
          
          break;
        }  
        default:
          break;
        }
    }

    private void generateDocument() {
    	
        try {
          Deal d = getDeal();
          int dealId = d.getDealId();
          //int copyId = d.getCopyId();
          int langId = theSessionState.getLanguageId();
          
          PageEntry pg = theSessionState.getCurrentPage();
			DisclosureType disclosureType = dtf.getDisclosureType(pg
					.getPageName(), d, srk);

          int iDocType =  disclosureType.getDocumentTypeId();
          String prov = disclosureType.getProvinceAbbrev();

          //Ticket #4365 
          if(prov.equalsIgnoreCase("ON"))
        	  langId = 0;
          
  		PDFDocumentFactory pdfDocFactory;
  		Vector vPdfBytes = null;
  		try {
  			pdfDocFactory = new PDFDocumentFactory(dealId, langId, iDocType, srk);
  			vPdfBytes = pdfDocFactory.produceDocuments();
  		} catch (Exception e) {
  			e.printStackTrace();
  		}
  	
  		byte pdf[] = null;
  		Iterator it = vPdfBytes.iterator();
  		while( it.hasNext()) {
  			pdf = (byte[])it.next();
  		}
  		
          HttpServletResponse response = RequestManager.getRequestContext().getResponse();

          response.setContentType("application/pdf");
          response.setHeader("Content-disposition", "attachment; filename=disclosure.pdf");
          response.setContentLength(pdf.length);
          response.getOutputStream().write(pdf);
          response.getOutputStream().flush(); 
          response.getOutputStream().close();
        }
        catch (Exception ex) {
          ex.printStackTrace();
          logger.error("Exception @DisclosureHandler.generateDocument()" + ex.getMessage());
        }
      }

    public void handleSubmit() {
		PageEntry pg = (PageEntry) theSessionState.getCurrentPage();

		try {
			Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg
					.getPageDealCID());
			Property prop = new Property(srk);
			prop = prop.findByPrimaryProperty(deal.getDealId(), deal
					.getCopyId(), deal.getInstitutionProfileId());
			provinceId = prop.getProvinceId();
		} catch (Exception e) {
		}
		// Code added for Invoking Calculation Engine - Starts
		if (provinceId == Mc.PROVINCE_ONTARIO
				|| provinceId == Mc.PROVINCE_PRINCE_EDWARD_ISLAND
				|| provinceId == Mc.PROVINCE_ALBERTA
				|| provinceId == Mc.PROVINCE_NEWFOUNDLAND) {
			SessionResourceKit srk = getSessionResourceKit();
			CalcMonitor dcm = CalcMonitor.getMonitor(srk);
			try {
				Deal deal = DBA.getDeal(srk, pg.getPageDealId(), pg
						.getPageDealCID(), dcm);
				// Force to trigger calc by totalCostOfBorrowing
				deal.forceChange("totalCostOfBorrowing");
				deal.forceChange("balanceRemainingAtEndOfTerm");
				dcm.inputEntity(deal);
				doCalculation(dcm);
			} catch (Exception ex) {
				logger.error("Exception @handleRecalculate :: "
						+ ex.getMessage());
				ex.printStackTrace();
			}
		}
		// Code added for Invoking Calculation Engine - Ends
		logger.debug("@DisclosureHandler.handleSubmit() inside handleSubmit");
	  
		updateData();

		logger.debug("@DisclosureHandler.handleSubmit() after updateData");	  

      // Catherine: 23-Mar-06  ------ begin ------   
      boolean isScreen = true;
      
        int documentTypeId = dtf.getDisclosureType(pg.getPageName(), getDeal(),
                srk).getDocumentTypeId();
        
		logger.debug("@DisclosureHandler.handleSubmit() after getDisclosureType");	  	  

      try {
        MailDestinationType md = new MailDestinationType(srk);

        isScreen = md.isDestinationScreen(documentTypeId);
        
      } catch (Exception e) {
        logger.error("Exception @DisclosureHandler.handleSubmit()" + e.getMessage());
      }
      
      if (isScreen){

		// set the flag to true so the JS onload function is executed
		// which generates the document (see JSP)
		ViewBean currentViewBean = getCurrNDPage();
		generateDocument();
      } else {
        try {
          callDocPrep(documentTypeId);
        } catch (Exception e) {
          logger.error(StringUtil.stack2string(e));
          logger.error("Exception @DisclosureHandler.handleSubmit()" + e.getMessage());
          setStandardFailMessage();
        }
      }
    }

    public void handleSaveAndExit() {
      updateData();
      // go to the worksheet
      navigateToNextPage(true);
    }

    public void setupBeforePageGeneration() {
      PageEntry pg = theSessionState.getCurrentPage();

      try {
        if (pg.getSetupBeforeGenerationCalled() == true)return;
        pg.setSetupBeforeGenerationCalled(true);
        setupDealSummarySnapShotDO(pg);
    // Add any custom model binding here
    //or and any field customization logic here
    // Setup cursor parameters here, if necessary
    //........................................
      }
      catch (Exception e) {
        logger.error("Exception @DisclosureHandler.setupBeforePageGeneration()");
        logger.error(e);
        setStandardFailMessage();
        return;
      }

    }

    /**
     *
     * Override the DocTrackingDetails page label if necessary.
     */
    public void overridePageLabel(int pageId) {
      ViewBean thePage = getCurrNDPage();
      PageEntry pe = (PageEntry) theSessionState.getCurrentPage();

      int languageId = theSessionState.getLanguageId();

      String pageLabel = BXResources.getPickListDescription(theSessionState.getDealInstitutionId(),"PAGELABEL", pageId, languageId);

      thePage.setDisplayFieldValue("stPageLabel", new String(pageLabel));
    }
}
