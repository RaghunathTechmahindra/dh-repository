package mosApp.MosSystem;

import com.iplanet.jato.model.sql.QueryModel;
import com.iplanet.jato.model.sql.SelectQueryModel;

/**
 *
 *  BJH - Nov 20, 2002 : Add field for MI Indicator Description (to eliminate need for doUWMIIndicatorModel -
 *                       translation of description can be accomplished via resourse bundle in normal fashion later)
 *
 *                       Note, field could be safely added since is non-null with appropriate integrity constraint.
 * @version 1.2  <br>
 * Date: 06/26/006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	added 2 methods getDfRateGuaranteePeriod(), setDfRateGuaranteePeriod(String value) <br>
 *	- added one constant FIELD_DFRATEGUARANTEEPERIOD  <br>
 *	 <br>
 *
 * @version 1.3  <br>
 * Date: 07/28/2006 <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *	Modified to add field for affiliation program Id<br>
 *	- added declaration for getter/setter for affiliation program id<br>
 *
 *	@Version 1.4 <br>
 *	Date: 06/09/2008
 *	Author: MCM Impl Team <br>
 *	Change Log:  <br>
 *	XS_2.46 -- 06/06/2008 -- Modified to add field for RefiAdditionalInformation<br>
 *		- added declaration for getter/setter for RefiAdditionalInformation<br>
 *  @Version 1.5 <br>
 *  Date: 16/07/2008
 *  Author: MCM Impl Team <br>
 *  Change Log:  <br>
 *  Bug Fix: Modified data type for getDfRateDate() from timestamp to Date data type.
 */
public interface doUWDealSelectMainModel extends QueryModel, SelectQueryModel
{
//	 ***** Change by NBC Impl. Team - Version 1.2 - Start *****//

	public static final String FIELD_DFCASHBACKINDOLLARS = "dfCashBackInDollars";

	public static final String FIELD_DFCASHBACKINPERCENTAGE = "dfCashBackInPercentage";

	public static final String FIELD_DFCASHBACKOVERRIDEINDOLLARS = "dfCashBackOverrideInDollars";

	// ***** Change by NBC Impl. Team - Version 1.2 - End*****//
    
    
    // ***** Qualifying Rate *****//
    public static final String FIELD_DFQUALIFYINGOVERRIDEFLAG = "dfQualifyingOverrideFlag";
    public static final String FIELD_DFQUALIFYRATE = "dfQualifyRate";
    public static final String FIELD_DFOVERRIDEQUALPROD = "dfOverrideQualProd";
    public static final String FIELD_DFQUALIFYINGOVERRIDERATEFLAG = "dfQualifyingOverrideRateFlag";
    // ***** Qualifying Rate *****//

    //QC702
    public static final String FIELD_DFCOMBINEDLENDINGVALUE = "dfCombinedLendingValue";

//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//

	/**
	 * getDfCashBackInDollars
	 *
	 * @param None <br>
	 *  
	 * @return BigDecimal : the result of getDfCashBackInDollars <br>
	 */
	public java.math.BigDecimal getDfCashBackInDollars();
	/**
	 * setDfCashBackInDollars
	 *
	 * @param BigDecimal <br>
	 *  
	 * @return Void : the result of setDfCashBackInDollars <br>
	 */
	public void setDfCashBackInDollars(java.math.BigDecimal value);
	/**
	 * getDfCashBackInPercentage
	 *
	 * @param None <br>
	 *  
	 * @return BigDecimal : the result of getDfCashBackInPercentage <br>
	 */
	public java.math.BigDecimal getDfCashBackInPercentage();
	/**
	 * setDfCashBackInPercentage
	 *
	 * @param BigDecimal <br>
	 *  
	 * @return void : the result of setDfCashBackInPercentage <br>
	 */
	public void setDfCashBackInPercentage(java.math.BigDecimal value);
	/**
	 * getDfCashBackOverrideInDollars
	 *
	 * @param None <br>
	 *  
	 * @return String : the result of getDfCashBackOverrideInDollars <br>
	 */
	public String getDfCashBackOverrideInDollars();
	/**
	 * setDfCashBackOverrideInDollars
	 *
	 * @param String <br>
	 *  
	 * @return void : the result of setDfCashBackOverrideInDollars <br>
	 */
	public void setDfCashBackOverrideInDollars(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


    /**
     *
     *
     */
    public java.math.BigDecimal getDfCopyId();


    /**
     *
     *
     */
    public void setDfCopyId(java.math.BigDecimal value);

    /**
     *
     *
     */
    public java.math.BigDecimal getDfInstitutionId();


    /**
     *
     *
     */
    public void setDfInstitutionId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSourceFirmProfileId();


	/**
	 *
	 *
	 */
	public void setDfSourceFirmProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfScenarioNumber();


	/**
	 *
	 *
	 */
	public void setDfScenarioNumber(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfScenarioDesc();


	/**
	 *
	 *
	 */
	public void setDfScenarioDesc(String value);


	/**
	 *
	 *
	 */
	public String getDfScenarioRecomended();


	/**
	 *
	 *
	 */
	public void setDfScenarioRecomended(String value);


	/**
	 *
	 *
	 */
	public String getDfScenarioLocked();

	/**
	 *
	 *
	 */
	public void setDfScenarioLocked(String value);
//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//
	/**
	 * getDfRateGuaranteePeriod
	 *
	 * @param  <br>
	 *
	 * @return String : the result of getDfRateGuaranteePeriod <br>
	 */
	public String getDfRateGuaranteePeriod();
	/**
	 * setDfRateGuaranteePeriod
	 *
	 * @param oneInput String <br>
	 *
	 * @return <br>
	 */
		public void setDfRateGuaranteePeriod(String value);

//		***** Change by NBC Impl. Team - Version 1.2 - End*****//


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedGDS();


	/**
	 *
	 *
	 */
	public void setDfCombinedGDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombined3YearGDS();


	/**
	 *
	 *
	 */
	public void setDfCombined3YearGDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedBorrowerGDS();


	/**
	 *
	 *
	 */
	public void setDfCombinedBorrowerGDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTDS();


	/**
	 *
	 *
	 */
	public void setDfCombinedTDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombined3YearTDS();


	/**
	 *
	 *
	 */
	public void setDfCombined3YearTDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedBorrowerTDS();


	/**
	 *
	 *
	 */
	public void setDfCombinedBorrowerTDS(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPurchasePrice();


	/**
	 *
	 *
	 */
	public void setDfTotalPurchasePrice(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLienPositionId();


	/**
	 *
	 *
	 */
	public void setDfLienPositionId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealDownPaymentAmount();


	/**
	 *
	 *
	 */
	public void setDfDealDownPaymentAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLOBId();


	/**
	 *
	 *
	 */
	public void setDfLOBId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLenderProfileId();


	/**
	 *
	 *
	 */
	public void setDfLenderProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMTGProductId();


	/**
	 *
	 *
	 */
	public void setDfMTGProductId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedLTV();


	/**
	 *
	 *
	 */
	public void setDfCombinedLTV(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPremiumAmount();


	/**
	 *
	 *
	 */
	public void setDfMIPremiumAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealDiscount();


	/**
	 *
	 *
	 */
	public void setDfDealDiscount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLoanAmount();


	/**
	 *
	 *
	 */
	public void setDfTotalLoanAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealPremium();


	/**
	 *
	 *
	 */
	public void setDfDealPremium(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAmortizationTerm();


	/**
	 *
	 *
	 */
	public void setDfAmortizationTerm(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBuyDownRate();


	/**
	 *
	 *
	 */
	public void setDfBuyDownRate(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEffectiveAmortizationInMonths();


	/**
	 *
	 *
	 */
	public void setDfEffectiveAmortizationInMonths(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetRate();


	/**
	 *
	 *
	 */
	public void setDfNetRate(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfPaymentTermDesc();


	/**
	 *
	 *
	 */
	public void setDfPaymentTermDesc(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPIPaymentAmount();


	/**
	 *
	 *
	 */
	public void setDfPIPaymentAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfActualPaymentTerm();


	/**
	 *
	 *
	 */
	public void setDfActualPaymentTerm(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPaymentFrequencyId();


	/**
	 *
	 *
	 */
	public void setDfPaymentFrequencyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfEscrowPaymentAmount();


	/**
	 *
	 *
	 */
	public void setDfEscrowPaymentAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPaymentAmount();


	/**
	 *
	 *
	 */
	public void setDfTotalPaymentAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPrivilagePaymentId();


	/**
	 *
	 *
	 */
	public void setDfPrivilagePaymentId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfExistingLoanAmount();


	/**
	 *
	 *
	 */
	public void setDfExistingLoanAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealPurposeId();


	/**
	 *
	 *
	 */
	public void setDfDealPurposeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBranchProfileId();


	/**
	 *
	 *
	 */
	public void setDfBranchProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealTypeId();

	// SEAN GECF IV Document Type drop down: getter and seeter for dfIVDocumentTypeId.
	// IV stands for Income Verification
	public java.math.BigDecimal getDfIVDocumentTypeId();
	public void setDfIVDocumentTypeId(java.math.BigDecimal value);
	// END GECF IV Document Type drop down

	// SEAN DJ SPEC-Progress Advance Type July 21, 2005: the getter and setter
	// methods.
	public java.math.BigDecimal getDfProgressAdvanceTypeId();
	public void setDfProgressAdvanceTypeId(java.math.BigDecimal value);
	// SEAN DJ SPEC-PAT END

	/**
	 *
	 *
	 */
	public void setDfDealTypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCrossellProfileId();


	/**
	 *
	 *
	 */
	public void setDfCrossellProfileId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSpecialFeatureId();


	/**
	 *
	 *
	 */
	public void setDfSpecialFeatureId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfExistingMTGNumber();


	/**
	 *
	 *
	 */
	public void setDfExistingMTGNumber(String value);


	/**
	 *
	 *
	 */
	public String getDfRateLock();


	/**
	 *
	 *
	 */
	public void setDfRateLock(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfReferenceDealTypeId();


	/**
	 *
	 *
	 */
	public void setDfReferenceDealTypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRePaymentTypeId();


	/**
	 *
	 *
	 */
	public void setDfRePaymentTypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfReferenceDealNumber();


	/**
	 *
	 *
	 */
	public void setDfReferenceDealNumber(String value);


	/**
	 *
	 *
	 */
	public String getDfSecondaryFinancing();


	/**
	 *
	 *
	 */
	public void setDfSecondaryFinancing(String value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfEstimatedClosingDate();


	/**
	 *
	 *
	 */
	public void setDfEstimatedClosingDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfFinanceProgramId();


	/**
	 *
	 *
	 */
	public void setDfFinanceProgramId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfFirstPaymentDate();


	/**
	 *CR03
	 *
	 */
	public java.sql.Timestamp getDfCommitmentExpirationDate();

	/**
	 *
	 *
	 */
	public void setDfFirstPaymentDate(java.sql.Timestamp value);


	/**
	 * CR03
	 *
	 */
	public void setDfCommitmentExpirationDate(java.sql.Timestamp value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealTaxPayorId();


	/**
	 *
	 *
	 */
	public void setDfDealTaxPayorId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfMaturityId();


	/**
	 *
	 *
	 */
	public void setDfMaturityId(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfSourceApplicationId();


	/**
	 *
	 *
	 */
	public void setDfSourceApplicationId(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTotalIncome();


	/**
	 *
	 *
	 */
	public void setDfCombinedTotalIncome(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTotalLiabilities();


	/**
	 *
	 *
	 */
	public void setDfCombinedTotalLiabilities(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombinedTotalAssets();


	/**
	 *
	 *
	 */
	public void setDfCombinedTotalAssets(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalPropertyExpenses();


	/**
	 *
	 *
	 */
	public void setDfTotalPropertyExpenses(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaximumPricipalAllowed();


	/**
	 *
	 *
	 */
	public void setDfMaximumPricipalAllowed(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMinimumIncomeRequired();


	/**
	 *
	 *
	 */
	public void setDfMinimumIncomeRequired(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaximumTDSExpenseAllowed();


	/**
	 *
	 *
	 */
	public void setDfMaximumTDSExpenseAllowed(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNetLoanAmount();


	/**
	 *
	 *
	 */
	public void setDfNetLoanAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdvanceHold();


	/**
	 *
	 *
	 */
	public void setDfAdvanceHold(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRateDate();


	/**
	 *
	 *
	 */
	public void setDfRateDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPAPurchasePrice();


	/**
	 *
	 *
	 */
	public void setDfPAPurchasePrice(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIStatus();


	/**
	 *
	 *
	 */
	public void setDfMIStatus(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIPayorId();


	/**
	 *
	 *
	 */
	public void setDfMIPayorId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMITypeId();


	/**
	 *
	 *
	 */
	public void setDfMITypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMIUpFrontId();


	/**
	 *
	 *
	 */
	public void setDfMIUpFrontId(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMIIndicatorId();


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorId(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfMIIndicatorDescription();


	/**
	 *
	 *
	 */
	public void setDfMIIndicatorDescription(String value);



	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMInsurerId();


	/**
	 *
	 *
	 */
	public void setDfMInsurerId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMIPolicyNumber();

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 17July2003
	public String getDfMIPolicyNumberCMHC();
  public String getDfMIPolicyNumberGE();
    public String getDfMIPolicyNumberAIGUG();
    public String getDfMIPolicyNumberPMI();
  //===============================================================

	/**
	 *
	 *
	 */
	public void setDfMIPolicyNumber(String value);

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 17July2003
  public void setDfMIPolicyNumberCMHC(String value);
  public void setDfMIPolicyNumberGE(String value);
  public void setDfMIPolicyNumberAIGUG(String value);
  public void setDfMIPolicyNumberPMI(String value);
  //===============================================================

	/**
	 *
	 *
	 */
	public String getDfMIExistingPolicyNumber();


	/**
	 *
	 *
	 */
	public void setDfMIExistingPolicyNumber(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdditionalPrincipal();


	/**
	 *
	 *
	 */
	public void setDfAdditionalPrincipal(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfPrePaymentOptionId();


	/**
	 *
	 *
	 */
	public void setDfPrePaymentOptionId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRequiredDownpayment();


	/**
	 *
	 *
	 */
	public void setDfRequiredDownpayment(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCombainedTotalPropertyExpence();


	/**
	 *
	 *
	 */
	public void setDfCombainedTotalPropertyExpence(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfReturnDate();


	/**
	 *
	 *
	 */
	public void setDfReturnDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfProgressAdvance();

	/**
	 *
	 *
	 */
	public void setDfProgressAdvance(String value);

	/**
	 *CR03
	 *
	 */
	public String getDfAutoCalcCommitExpiryDate();

	/**
	 *CR03
	 *
	 */
	public void setDfAutoCalcCommitExpiryDate(String value);

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdvanceToDateAmount();


	/**
	 *
	 *
	 */
	public void setDfAdvanceToDateAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfAdvanceNumber();


	/**
	 *
	 *
	 */
	public void setDfAdvanceNumber(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfMIRUIntervention();


	/**
	 *
	 *
	 */
	public void setDfMIRUIntervention(String value);


	/**
	 *
	 *
	 */
	public String getDfPreQualificationCertNum();


	/**
	 *
	 *
	 */
	public void setDfPreQualificationCertNum(String value);


	/**
	 *
	 *
	 */
	public String getDfRefiPurpose();


	/**
	 *
	 *
	 */
	public void setDfRefiPurpose(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiOrigPurchasePrice();


	/**
	 *
	 *
	 */
	public void setDfRefiOrigPurchasePrice(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfRefiBlendedAmortization();


	/**
	 *
	 *
	 */
	public void setDfRefiBlendedAmortization(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiOrigMtgAmount();


	/**
	 *
	 *
	 */
	public void setDfRefiOrigMtgAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfRefiImprovementsDesc();


	/**
	 *
	 *
	 */
	public void setDfRefiImprovementsDesc(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfRefiImprovementValue();


	/**
	 *
	 *
	 */
	public void setDfRefiImprovementValue(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNextAdvanceAmount();


	/**
	 *
	 *
	 */
	public void setDfNextAdvanceAmount(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfRefiOrigPurchaseDate();


	/**
	 *
	 *
	 */
	public void setDfRefiOrigPurchaseDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public String getDfRefiCurMortgageHolder();


	/**
	 *
	 *
	 */
	public void setDfRefiCurMortgageHolder(String value);


	/**
	 *
	 *
	 */
	public String getDfChannelMedia();


	/**
	 *
	 *
	 */
	public void setDfChannelMedia(String value);

  //-- FXLink Phase II --//
  //--> New Field for Market Type Indicator
  //--> By Billy 18Nov2003
  public String getDfMccMarketType();
	public void setDfMccMarketType(String value);
  //=======================================

  //--DJ_LDI_CR--start--//
	/**
	 *
	 *
	 */
	public void setDfPmntPlusLifeDisability(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
  public java.math.BigDecimal getDfPmntPlusLifeDisability();
  //--DJ_LDI_CR--end--//

  //--DJ_CR010--start//
	/**
	 *
	 *
	 */
	public String getDfCommisionCode();

	/**
	 *
	 *
	 */
	public void setDfCommisionCode(String value);
  //--DJ_CR010--end//

  //--DJ_CR203.1--start//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfProprietairePlusLOC();

	/**
	 *
	 *
	 */
	public void setDfProprietairePlusLOC(java.math.BigDecimal value);

	/**
	 *
	 *
	 */
	public String getDfMultiProject();

	/**
	 *
	 *
	 */
	public void setDfMultiProject(String value);

	/**
	 *
	 *
	 */
	public String getDfProprietairePlus();

	/**
	 *
	 *
	 */
	public void setDfProprietairePlus(String value);
  //--DJ_CR203.1--end//
	
	/***************MCM Impl team changes starts - XS_2.46 *******************/  
	/**
	 * This method returns DfRefiAdditionalInformation value
	 *
	 * @param None <br>
	 *  
	 * @return String : the result of getDfRefiAdditionalInformation <br>
	 */
	public String getDfRefiAdditionalInformation();

	/**
	 * This method sets value for DfRefiAdditionalInformation
	 *
	 * @param String <br>
	 *  
	 */
	public void setDfRefiAdditionalInformation(String value);
	
	/***************MCM Impl team changes ends - XS_2.46 *******************/
    
    //***** Qualify Rate *****//
    public String getDfQualifyingOverrideFlag();
    
    public void setDfQualifyingOverrideFlag(String value);
    
    
    public java.math.BigDecimal getDfQualifyRate();
    
    public void setDfQualifyRate(java.math.BigDecimal value);
    
    
    public String getDfOverrideQualProd();
    
    public void setDfOverrideQualProd(String value);
    
    public String getDfQualifyingOverrideRateFlag();
    
    public void setDfQualifyingOverrideRateFlag(String value);
    //  ***** Qualify Rate *****//
	
  //--Release3.1--begins
  //--by Hiro Apr 13, 2006
  public java.math.BigDecimal getDfProductTypeId();
  public void setDfProductTypeId(java.math.BigDecimal value);

  public java.math.BigDecimal getDfRefiProductTypeId();
  public void setDfRefiProductTypeId(java.math.BigDecimal value);

  public String getDfProgressAdvanceInspectionBy();
  public void setDfProgressAdvanceInspectionBy(String value);
  
  public String getDfSelfDirectedRRSP();
  public void setDfSelfDirectedRRSP(String value);
  
  public String getDfCmhcProductTrackerIdentifier();
  public void setDfCmhcProductTrackerIdentifier(String value);
  
  public java.math.BigDecimal getDfLOCRepaymentTypeId();
  public void setDfLOCRepaymentTypeId(java.math.BigDecimal value);
  
  public String getDfRequestStandardService();
  public void setDfRequestStandardService(String value);

  public java.math.BigDecimal getDfLOCAmortizationMonths();
  public void setDfLOCAmortizationMonths(java.math.BigDecimal value);

  public java.sql.Timestamp getDfLOCInterestOnlyMaturityDate();
  public void setDfLOCInterestOnlyMaturityDate(java.sql.Timestamp value);
  // --Release3.1--ends
  
  // Ticket 267
  public void setDfDealStatusId(String value);
  public String getDfDealStatusId();

    //    ***** Change by NBC Impl. Team - Version 1.3 - Start *****//

	public void setDfAffiliationProgramId(java.math.BigDecimal value);
	
	public java.math.BigDecimal getDfAffiliationProgramId();
	
    //    ***** Change by NBC Impl. Team - Version 1.3 - End *****//

	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
    public static final String FIELD_DFCOPYID="dfCopyId";
    public static final String FIELD_DFINSTITUTIONID="dfInstitutionId";
	public static final String FIELD_DFSOURCEFIRMPROFILEID="dfSourceFirmProfileId";
	public static final String FIELD_DFSCENARIONUMBER="dfScenarioNumber";
	public static final String FIELD_DFSCENARIODESC="dfScenarioDesc";
	public static final String FIELD_DFSCENARIORECOMENDED="dfScenarioRecomended";
	public static final String FIELD_DFSCENARIOLOCKED="dfScenarioLocked";
	public static final String FIELD_DFCOMBINEDGDS="dfCombinedGDS";
	public static final String FIELD_DFCOMBINED3YEARGDS="dfCombined3YearGDS";
	public static final String FIELD_DFCOMBINEDBORROWERGDS="dfCombinedBorrowerGDS";
	public static final String FIELD_DFCOMBINEDTDS="dfCombinedTDS";
	public static final String FIELD_DFCOMBINED3YEARTDS="dfCombined3YearTDS";
	public static final String FIELD_DFCOMBINEDBORROWERTDS="dfCombinedBorrowerTDS";
	public static final String FIELD_DFTOTALPURCHASEPRICE="dfTotalPurchasePrice";
	public static final String FIELD_DFLIENPOSITIONID="dfLienPositionId";
	public static final String FIELD_DFDEALDOWNPAYMENTAMOUNT="dfDealDownPaymentAmount";
	public static final String FIELD_DFLOBID="dfLOBId";
	public static final String FIELD_DFLENDERPROFILEID="dfLenderProfileId";
	public static final String FIELD_DFMTGPRODUCTID="dfMTGProductId";
	public static final String FIELD_DFCOMBINEDLTV="dfCombinedLTV";
	public static final String FIELD_DFMIPREMIUMAMOUNT="dfMIPremiumAmount";
	public static final String FIELD_DFDEALDISCOUNT="dfDealDiscount";
	public static final String FIELD_DFTOTALLOANAMOUNT="dfTotalLoanAmount";
	public static final String FIELD_DFDEALPREMIUM="dfDealPremium";
	public static final String FIELD_DFAMORTIZATIONTERM="dfAmortizationTerm";
	public static final String FIELD_DFBUYDOWNRATE="dfBuyDownRate";
	public static final String FIELD_DFEFFECTIVEAMORTIZATIONINMONTHS="dfEffectiveAmortizationInMonths";
	public static final String FIELD_DFNETRATE="dfNetRate";
	public static final String FIELD_DFPAYMENTTERMDESC="dfPaymentTermDesc";
	public static final String FIELD_DFPIPAYMENTAMOUNT="dfPIPaymentAmount";
	public static final String FIELD_DFACTUALPAYMENTTERM="dfActualPaymentTerm";
	public static final String FIELD_DFPAYMENTFREQUENCYID="dfPaymentFrequencyId";
	public static final String FIELD_DFESCROWPAYMENTAMOUNT="dfEscrowPaymentAmount";
	public static final String FIELD_DFTOTALPAYMENTAMOUNT="dfTotalPaymentAmount";
	public static final String FIELD_DFPRIVILAGEPAYMENTID="dfPrivilagePaymentId";
	public static final String FIELD_DFEXISTINGLOANAMOUNT="dfExistingLoanAmount";
	public static final String FIELD_DFDEALPURPOSEID="dfDealPurposeId";
	public static final String FIELD_DFBRANCHPROFILEID="dfBranchProfileId";
	public static final String FIELD_DFDEALTYPEID="dfDealTypeId";
	public static final String FIELD_DFCROSSELLPROFILEID="dfCrossellProfileId";
	public static final String FIELD_DFSPECIALFEATUREID="dfSpecialFeatureId";
	public static final String FIELD_DFEXISTINGMTGNUMBER="dfExistingMTGNumber";
	public static final String FIELD_DFRATELOCK="dfRateLock";
	public static final String FIELD_DFREFERENCEDEALTYPEID="dfReferenceDealTypeId";
	public static final String FIELD_DFREPAYMENTTYPEID="dfRePaymentTypeId";
	public static final String FIELD_DFREFERENCEDEALNUMBER="dfReferenceDealNumber";
	public static final String FIELD_DFSECONDARYFINANCING="dfSecondaryFinancing";
	public static final String FIELD_DFESTIMATEDCLOSINGDATE="dfEstimatedClosingDate";
	public static final String FIELD_DFFINANCEPROGRAMID="dfFinanceProgramId";
	public static final String FIELD_DFFIRSTPAYMENTDATE="dfFirstPaymentDate";
    
    //FFATE start
    public static final String FIELD_DFFINANCINGWAIVERDATE="dfFinancingWaiverDate";
    //FFATE end
    
    //CR03
	public static final String FIELD_DFCOMMITMENTEXPIRATIONDATE="dfCommitmentExpirationDate";
    //CR03
	public static final String FIELD_DFDEALTAXPAYORID="dfDealTaxPayorId";
	public static final String FIELD_DFMATURITYID="dfMaturityId";
	public static final String FIELD_DFSOURCEAPPLICATIONID="dfSourceApplicationId";
	public static final String FIELD_DFCOMBINEDTOTALINCOME="dfCombinedTotalIncome";
	public static final String FIELD_DFCOMBINEDTOTALLIABILITIES="dfCombinedTotalLiabilities";
	public static final String FIELD_DFCOMBINEDTOTALASSETS="dfCombinedTotalAssets";
	public static final String FIELD_DFTOTALPROPERTYEXPENSES="dfTotalPropertyExpenses";
	public static final String FIELD_DFMAXIMUMPRICIPALALLOWED="dfMaximumPricipalAllowed";
	public static final String FIELD_DFMINIMUMINCOMEREQUIRED="dfMinimumIncomeRequired";
	public static final String FIELD_DFMAXIMUMTDSEXPENSEALLOWED="dfMaximumTDSExpenseAllowed";
	public static final String FIELD_DFNETLOANAMOUNT="dfNetLoanAmount";
	public static final String FIELD_DFADVANCEHOLD="dfAdvanceHold";
	public static final String FIELD_DFRATEDATE="dfRateDate";
	public static final String FIELD_DFPAPURCHASEPRICE="dfPAPurchasePrice";
	public static final String FIELD_DFMISTATUS="dfMIStatus";
	public static final String FIELD_DFMIPAYORID="dfMIPayorId";
	public static final String FIELD_DFMITYPEID="dfMITypeId";
	public static final String FIELD_DFMIUPFRONTID="dfMIUpFrontId";
	public static final String FIELD_DFMIINDICATORID="dfMIIndicatorId";
	public static final String FIELD_DFMIINDICATORDESCRIPTION="dfMIIndicatorDescription";
	public static final String FIELD_DFMINSURERID="dfMInsurerId";
	public static final String FIELD_DFMIPOLICYNUMBER="dfMIPolicyNumber";
	public static final String FIELD_DFMIEXISTINGPOLICYNUMBER="dfMIExistingPolicyNumber";
	public static final String FIELD_DFADDITIONALPRINCIPAL="dfAdditionalPrincipal";
	public static final String FIELD_DFPREPAYMENTOPTIONID="dfPrePaymentOptionId";
	public static final String FIELD_DFREQUIREDDOWNPAYMENT="dfRequiredDownpayment";
	public static final String FIELD_DFCOMBAINEDTOTALPROPERTYEXPENCE="dfCombainedTotalPropertyExpence";
	public static final String FIELD_DFRETURNDATE="dfReturnDate";
	public static final String FIELD_DFPROGRESSADVANCE="dfProgressAdvance";
    //CR03
	public static final String FIELD_DFAUTOCALCCOMMITEXPIRYDATE="dfAutoCalcCommitExpiryDate";
    //CR03
	public static final String FIELD_DFADVANCETODATEAMOUNT="dfAdvanceToDateAmount";
	public static final String FIELD_DFADVANCENUMBER="dfAdvanceNumber";
	public static final String FIELD_DFMIRUINTERVENTION="dfMIRUIntervention";
	public static final String FIELD_DFPREQUALIFICATIONCERTNUM="dfPreQualificationCertNum";
	public static final String FIELD_DFREFIPURPOSE="dfRefiPurpose";
	public static final String FIELD_DFREFIORIGPURCHASEPRICE="dfRefiOrigPurchasePrice";
	public static final String FIELD_DFREFIBLENDEDAMORTIZATION="dfRefiBlendedAmortization";
	public static final String FIELD_DFREFIORIGMTGAMOUNT="dfRefiOrigMtgAmount";
	public static final String FIELD_DFREFIIMPROVEMENTSDESC="dfRefiImprovementsDesc";
	public static final String FIELD_DFREFIIMPROVEMENTVALUE="dfRefiImprovementValue";
	public static final String FIELD_DFNEXTADVANCEAMOUNT="dfNextAdvanceAmount";
	public static final String FIELD_DFREFIORIGPURCHASEDATE="dfRefiOrigPurchaseDate";
	public static final String FIELD_DFREFICURMORTGAGEHOLDER="dfRefiCurMortgageHolder";
	public static final String FIELD_DFCHANNELMEDIA="dfChannelMedia";

    //--Release2.1--start//
  public static final String FIELD_DFSCENARIODESCTRANSLATED="dfScenarioDescTranslated";

  //===============================================================
  // New fields to handle MIPolicyNum problem :
  //  the number lost if user cancel MI and re-send again later.
  // -- Modified by Billy 14July2003
  public static final String FIELD_DFMIPOLICYNUMBERCMHC="dfMIPolicyNumberCMHC";
  public static final String FIELD_DFMIPOLICYNUMBERGE="dfMIPolicyNumberGE";
  public static final String FIELD_DFMIPOLICYNUMBERAIGUG="dfMIPolicyNumberAIGUG";
  public static final String FIELD_DFMIPOLICYNUMBERPMI="dfMIPolicyNumberPMI";
  //===============================================================

  //=============================================================================
  //-- FXLink Phase II --//
  //--> New Field for Market Type Indicator
  //--> By Billy 18Nov2003
  public static final String FIELD_DFMCCMARKETTYPE="dfMccMarketType";
  //==============================================================

  //--DJ_LDI_CR--start--//
  public static final String FIELD_DFPMNTPLUSLIFEDISABILITY="dfPmntPlusLifeDisability";
  //--DJ_LDI_CR--end--//

  //--DJ_CR010--start//
  public static final String FIELD_DFCOMMISIONCODE="dfCommisionCode";
  //--DJ_CR010--end//

  //--DJ_CR203.1--start//
  public static final String FIELD_DFPROPRIETAIREPLUSLOC="dfProprietairePlusLOC";
  public static final String FIELD_DFPROPRIETAIREPLUS="dfProprietairePlus";
  public static final String FIELD_DFMULTIPROJECT="dfMultiProject";
  //--DJ_CR203.1--end//

	// SEAN GECF IV Document Type drop down: define the field.
	public static final String FIELD_DFIVDOCUMENTTYPEID="dfIVDocumentTypeId";
	// END GECF IV Document Type drop down

	// SEAN DJ SPEC-Progress Advance Type July 21, 2005: Define the field.
	public static final String FIELD_DFPROGRESSADVANCETYPEID="dfProgressAdvanceTypeId";
	// SEAN DJ SPEC-PAT END

  //--Release3.1--begins
  //--by Hiro Apr 13, 2006
  public static final String FIELD_DFPRODUCTTYPEID = "dfProductTypeId";
  public static final String FIELD_DFREFIPRODUCTTYPEID = "dfRefiProductTypeid";
  public static final String FIELD_DFPROGRESSADVANCEINSPECTIONBY = "dfProgressAdvanceInspectionBy";
  public static final String FIELD_DFSELFDIRECTEDRRSP = "dfSelfDirectedRRSP";
  public static final String FIELD_DFCMHCPRODUCTTRACKERIDENTIFIER = "dfCmhcProductTrackerIdentifier";
  public static final String FIELD_DFLOCREPAYMENTTYPEID = "dfLOCRepaymentTypeId";
  public static final String FIELD_DFREQUESTSTANDARDSERVICE = "dfRequestStandardService";
  public static final String FIELD_DFLOCAMORTIZATIONMONTHS = "dfLOCAmortizationMonths";
  public static final String FIELD_DFLOCINTERESTONLYMATURITYDATE = "dfLOCInterestOnlyMaturityDate";
  //--Release3.1--ends

//	***** Change by NBC Impl. Team - Version 1.2 - Start *****//

	public static final String 
	FIELD_DFRATEGUARANTEEPERIOD = "dfRateGuaranteePeriod";

//	***** Change by NBC Impl. Team - Version 1.2 - End*****//

//	***** Change by NBC Impl. Team - Version 1.3 - Start*****//
	
	public static final String FIELD_DFAFFILIATIONPROGRAMID = "dfAffiliationProgramId";
	
//	***** Change by NBC Impl. Team - Version 1.3 - End*****//
	
	/***************MCM Impl team changes starts - XS_2.46 *******************/
	
	public static final String FIELD_DFREFIADDITIONALINFORMATION="dfRefiAdditionalInformation";
	
	/***************MCM Impl team changes ends - XS_2.46 *********************/
	
	// Ticket 267
	public static final String FIELD_DFDEALSTATUSID = "dfDealStatusId";
	
  ////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

