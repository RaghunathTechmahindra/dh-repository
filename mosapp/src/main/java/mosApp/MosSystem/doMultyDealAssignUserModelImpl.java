package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mosApp.*;
import MosSystem.Mc;

import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

import com.basis100.log.*;
import com.basis100.picklist.BXResources;


/**
 *
 *
 *
 */
public class doMultyDealAssignUserModelImpl extends QueryModelBase
	implements doMultyDealAssignUserModel
{
	/**
	 *
	 *
	 */
	public doMultyDealAssignUserModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;

	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //logger = SysLog.getSysLogger("DODAUM");
    //logger.debug("DODAUM@afterExecute::SQLTemplate: " + this.getSelectSQL());
    //logger.debug("DODAUM@afterExecute::Size: " + this.getSize());

    //// 6. Override all the Description fields (see #1-5 below in SQL_TEMPLATE section
    //// and populate them from the BXResource based on the Language ID from the SessionStateModel.
    String defaultInstanceStateName =
			              getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState = (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                                            SessionStateModel.class,
                                            defaultInstanceStateName,
                                            true);

    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();

    //logger.debug("DODAUM@afterExecute::LanguageId: " + languageId);

    while(this.next())
    {
      //Convert Task Status Descriptions
      this.setDfUserDescription(BXResources.getPickListDescription(
          institutionId, "USERTYPE", this.getDfUserTypeId().intValue(), languageId));
    }

   //Reset Location
    this.beforeFirst();

	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}

  //--Release2.1--start//
  //// 7. In addition to the regular 6 steps to adjust the computed column of fly
  //// another step should be done as well: the 'fake' extra fields should be created
  //// to override the UserTypeDescripton in the afterExecute() method.
	/**
	 *
	 *
	 */
	public String getDfUserDescription()
	{
		return (String)getValue(FIELD_DFUTDESCRIPTION);
	}


	/**
	 *
	 *
	 */
	public void setDfUserDescription(String value)
	{
		setValue(FIELD_DFUTDESCRIPTION,value);
	}

	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUserTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERTYPEID,value);
	}

	/**
	 *
	 *
	 */
	public String getDfUserLogin()
	{
		return (String)getValue(FIELD_DFUSERLOGIN);
	}


	/**
	 *
	 *
	 */
	public void setDfUserLogin(String value)
	{
		setValue(FIELD_DFUSERLOGIN,value);
	}

  //--Release2.1--end//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfUserProfileId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFUSERPROFILEID);
	}


	/**
	 *
	 *
	 */
	public void setDfUserProfileId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFUSERPROFILEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfNameAndType()
	{
		return (String)getValue(FIELD_DFNAMEANDTYPE);
	}


	/**
	 *
	 *
	 */
	public void setDfNameAndType(String value)
	{
		setValue(FIELD_DFNAMEANDTYPE,value);
	}

	/**
	 * MetaData object test method to verify the SYNTHETIC new
	 * field for the reogranized SQL_TEMPLATE query.
	 *
	 */
  /**
	public void setResultSet(ResultSet value)
	{
  		logger = SysLog.getSysLogger("METADATA");

  		try
  		{
        super.setResultSet(value);
   			if( value != null)
   			{
       			ResultSetMetaData metaData = value.getMetaData();
       			int columnCount = metaData.getColumnCount();
       			logger.debug("===============================================");
            logger.debug("Testing MetaData Object for new synthetic fields for" +
                          " doMultyDealAssignUserModel");
       			logger.debug("NumberColumns: " + columnCount);
         		for(int i = 0; i < columnCount ; i++)
          		{
                 	logger.debug("column ["+i+"] |"+ metaData.getColumnName(i+1) + "|");
          		}
          		logger.debug("================================================");
      		}
  		}
  		catch (SQLException e)
  		{
    		    logger.debug("Class: " + getClass().getName());
                    logger.debug("SQLException: " + e);
  		}
 	}
  **/



	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

  //public SysLogger logger;

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

  //// Workaround to solve the bug of the iMT tool for the dataobject with a ComputedColumn
  //// (see detailed description in the desing doc as well).
  //// If there is a ComputedColumn in the ND dataobject a "fake", synthetic definition of this
  //// field must be included into the model's TEMPLATE and FieldDescriptor must be adjusted
  //// accordingly.

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.
  //// 3. Remove all joins which include the tables from the PickList.
  //// 4. Remove all PickList tables from the SQL_TEMPLATE.
  //// 5. Since this is computed field redefine the fields follow the #1,2 in the
  //// FIELD_SCHEMA.addFieldDescriptor() methods.
  //// 6. Repopulate the PickList Description fields manually in the afterExecute method (see above).

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	////public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) || ' (' || USERTYPE.UTDESCRIPTION || ') ' || USERPROFILE.USERLOGIN USERNAME FROM CONTACT, USERPROFILE, USERTYPE  __WHERE__  ORDER BY CONTACT.CONTACTLASTNAME  ASC, CONTACT.CONTACTFIRSTNAME  ASC, USERTYPE.USERTYPEID  ASC, USERPROFILE.USERLOGIN  ASC";

  //--Release2.1--//
  //// 1. Replace all Descriptions from the tables included into the PickList with
  //// their joins' counterparts in the SQL_TEMPLATE.
  //// 2. Convert all these new fragments (IDs) into the string format.

  ///!!|| USERPROFILE.USERLOGIN
  ////public static final String SELECT_SQL_TEMPLATE=
  ////"SELECT ALL USERPROFILE.USERPROFILEID,
  ////CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1)
  ////|| ' (' || USERTYPE.UTDESCRIPTION || ') ' || USERPROFILE.USERLOGIN
  ////SYNTHETICNAMETYPE FROM CONTACT, USERPROFILE, USERTYPE
  ////__WHERE__  ORDER BY CONTACT.CONTACTLASTNAME  ASC,
  ////CONTACT.CONTACTFIRSTNAME  ASC, USERTYPE.USERTYPEID
  ////ASC, USERPROFILE.USERLOGIN  ASC";
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL USERPROFILE.USERPROFILEID, " +
  "CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) SYNTHETICNAMETYPE, " +
  "to_char(USERPROFILE.USERTYPEID) USERTYPEID_STR, USERPROFILE.USERTYPEID, USERPROFILE.USERLOGIN " +
  "FROM CONTACT, USERPROFILE " +
  "__WHERE__  ORDER BY CONTACT.CONTACTLASTNAME  ASC, " +
  "CONTACT.CONTACTFIRSTNAME  ASC, " +
  "USERPROFILE.USERLOGIN  ASC";


  //--Release2.1--//
  //// 4. Remove all PickList tables from the SQL_TEMPLATE.
	////public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, USERPROFILE, USERTYPE";
	public static final String MODIFYING_QUERY_TABLE_NAME="CONTACT, USERPROFILE";

  //--Release2.1--//
  //// 3. Remove all joins which include the tables from the PickList.
	////public static final String STATIC_WHERE_CRITERIA="
  ////(((CONTACT.CONTACTID  =  USERPROFILE.CONTACTID)
  ////AND (USERPROFILE.USERTYPEID  =  USERTYPE.USERTYPEID)) AND CONTACT.COPYID = 1) ";
	public static final String STATIC_WHERE_CRITERIA=" (((CONTACT.CONTACTID  =  USERPROFILE.CONTACTID) AND " +
          " (CONTACT.INSTITUTIONPROFILEID  =  USERPROFILE.INSTITUTIONPROFILEID)) " +
		  " AND CONTACT.COPYID = 1) AND userprofile.usertypeid <> " + Mc.USER_TYPE_AUTO +
		  " AND userprofile.usertypeid <> "+ Mc.USER_TYPE_ECM +
		  " AND userprofile.usertypeid <> " + Mc.USER_TYPE_DATAX  +
		  " AND userprofile.usertypeid <> "+ Mc.USER_TYPE_SYSTEM;
	//FXP21168: Since there are no rules/properties that define these types of user, hard-coding is the only way.
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFUSERPROFILEID="USERPROFILE.USERPROFILEID";
	public static final String COLUMN_DFUSERPROFILEID="USERPROFILEID";
	////public static final String QUALIFIED_COLUMN_DFNAMEANDTYPE=".";
	////public static final String COLUMN_DFNAMEANDTYPE="";
	public static final String QUALIFIED_COLUMN_DFNAMEANDTYPE="CONTACT.SYNTHETICNAMETYPE";
	public static final String COLUMN_DFNAMEANDTYPE="SYNTHETICNAMETYPE";
  ///////////

  //--Release2.1--//
  //// 7. In addition to the regular 6 steps to adjust the computed column of fly
  //// another step should be done as well: the computedColumn field (constructed
  //// from different fields) should be split into the distinct elements corresponding
  //// an appropriate table.
  public static final String QUALIFIED_COLUMN_UTDESCRIPTION="USERPROFILE.USERTYPEID_STR";
	public static final String COLUMN_UTDESCRIPTION="USERTYPEID_STR";

  public static final String QUALIFIED_COLUMN_USERTYPEID="USERPROFILE.USERTYPEID";
	public static final String COLUMN_USERTYPEID="USERTYPEID";

  public static final String QUALIFIED_COLUMN_USERLOGIN="USERPROFILE.USERLOGIN";
	public static final String COLUMN_USERLOGIN="USERLOGIN";

	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERPROFILEID,
				COLUMN_DFUSERPROFILEID,
				QUALIFIED_COLUMN_DFUSERPROFILEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		////FIELD_SCHEMA.addFieldDescriptor(
		////	new QueryFieldDescriptor(
		////		FIELD_DFNAMEANDTYPE,
		////		COLUMN_DFNAMEANDTYPE,
		////		QUALIFIED_COLUMN_DFNAMEANDTYPE,
		////		String.class,
		////		false,
		////		false,
		////		QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
		////		"",
		////		QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
		////		""));

		//// Adjustment to fix the improper conversion of the iMT tool for the Computed
    //// Column.

    //--Release2.1--//
    //// 5. Since this is computed field redefine the fields follow the #1,2 in the
    //// FIELD_SCHEMA.addFieldDescriptor() methods.
    //// 6. Repopulate the PickList Description fields manually in the afterExecute method (see above).


    FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFNAMEANDTYPE,
				COLUMN_DFNAMEANDTYPE,
				QUALIFIED_COLUMN_DFNAMEANDTYPE,
				String.class,
				false,
				true,
				QueryFieldDescriptor.FORMULA_INSERT_VALUE_SOURCE,
        "CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) || ' (' || to_char(USERPROFILE.USERTYPEID) USERTYPEID_STR ||') ' || USERPROFILE.USERLOGIN",
				QueryFieldDescriptor.ON_EMPTY_VALUE_USE_FORMULA,
				"CONTACT.CONTACTLASTNAME || ', ' ||  SUBSTR(CONTACT.CONTACTFIRSTNAME,1,1) || ' (' || to_char(USERPROFILE.USERTYPEID) USERTYPEID_STR ||') ' || USERPROFILE.USERLOGIN"
        ));

    //--Release2.1--start//
    //// 7. In addition to the regular 6 steps to adjust the computed column of fly
    //// another step should be done as well: the computedColumn field (constructed
    //// from different fields) should be split into the distinct elements corresponding
    //// an appropriate table.
		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUTDESCRIPTION,
				COLUMN_UTDESCRIPTION,
				QUALIFIED_COLUMN_UTDESCRIPTION,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERTYPEID,
				COLUMN_USERTYPEID,
				QUALIFIED_COLUMN_USERTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFUSERLOGIN,
				COLUMN_USERLOGIN,
				QUALIFIED_COLUMN_USERLOGIN,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));
      //--Release2.1--end//

     }

}

