package mosApp.MosSystem;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.iplanet.jato.model.*;
import com.iplanet.jato.model.sql.*;

/**
 * <p>
 * Title: doApplicantEntryApplicantSelectModel
 * </p>
 *
 * <p>
 * Description: Model interface for Applicant Entry screen
 * </p>
 * 
 * <p>
 * Copyright: Filogix Inc. (c) 2006
 * </p>
 *
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author NBC/PP Implementation Team
 * @version 1.1
 * Change: added getters & setters for following fields <br>
 * 		<p> smoke status, solicitation, employee number, preferred contact method
 *	Added methods for <br>
 *	- Added following methods <br>
 *		getDfSmokeStatusId()
 *		setDfSmokeStatusId(java.math.BigDecimal value)
 *		getDfSolicitationId()
 *		setDfSolicitationId(java.math.BigDecimal value)
 *		getDfEmployeeNumber ()
 *		setDfEmployeeNumber(String value)
 *		getDfPrefContactMethodId ()
 *		setDfPrefContactMethodId(java.math.BigDecimal value)
 *
 */
public interface doApplicantEntryApplicantSelectModel extends QueryModel, SelectQueryModel
{
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfDealId();


	/**
	 *
	 *
	 */
	public void setDfDealId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerId();


	/**
	 *
	 *
	 */
	public void setDfBorrowerId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCopyId();


	/**
	 *
	 *
	 */
	public void setDfCopyId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfSalutationId();


	/**
	 *
	 *
	 */
	public void setDfSalutationId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfFirstName();


	/**
	 *
	 *
	 */
	public void setDfFirstName(String value);


	/**
	 *
	 *
	 */
	public String getDfMiddleName();


	/**
	 *
	 *
	 */
	public void setDfMiddleName(String value);


	/**
	 *
	 *
	 */
	public String getDfLastName();


	/**
	 *
	 *
	 */
	public void setDfLastName(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerTypeId();


	/**
	 *
	 *
	 */
	public void setDfBorrowerTypeId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.sql.Timestamp getDfBirthDate();


	/**
	 *
	 *
	 */
	public void setDfBirthDate(java.sql.Timestamp value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfMaritalstatusId();


	/**
	 *
	 *
	 */
	public void setDfMaritalstatusId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfSIN();


	/**
	 *
	 *
	 */
	public void setDfSIN(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCitizenshipId();


	/**
	 *
	 *
	 */
	public void setDfCitizenshipId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNoOfDependants();


	/**
	 *
	 *
	 */
	public void setDfNoOfDependants(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfExistingClient();


	/**
	 *
	 *
	 */
	public void setDfExistingClient(String value);


	/**
	 *
	 *
	 */
	public String getDfClientReferenceNumber();


	/**
	 *
	 *
	 */
	public void setDfClientReferenceNumber(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfNoOfBankCrupts();


	/**
	 *
	 *
	 */
	public void setDfNoOfBankCrupts(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBankruptcyStatusId();


	/**
	 *
	 *
	 */
	public void setDfBankruptcyStatusId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfStaffOfLender();


	/**
	 *
	 *
	 */
	public void setDfStaffOfLender(String value);


	/**
	 *
	 *
	 */
	public String getDfHomePhone();


	/**
	 *
	 *
	 */
	public void setDfHomePhone(String value);


	/**
	 *
	 *
	 */
	public String getDfWorkPhone();


	/**
	 *
	 *
	 */
	public void setDfWorkPhone(String value);


	/**
	 *
	 *
	 */
	public String getDfWorkPhoneExtension();


	/**
	 *
	 *
	 */
	public void setDfWorkPhoneExtension(String value);


	/**
	 *
	 *
	 */
	public String getDfFaxNumber();


	/**
	 *
	 *
	 */
	public void setDfFaxNumber(String value);


	/**
	 *
	 *
	 */
	public String getDfEmailAddress();


	/**
	 *
	 *
	 */
	public void setDfEmailAddress(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfLanguagePrefId();


	/**
	 *
	 *
	 */
	public void setDfLanguagePrefId(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalAsset();


	/**
	 *
	 *
	 */
	public void setDfTotalAsset(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalIncome();


	/**
	 *
	 *
	 */
	public void setDfTotalIncome(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLiab();


	/**
	 *
	 *
	 */
	public void setDfTotalLiab(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfTotalLiabPayment();


	/**
	 *
	 *
	 */
	public void setDfTotalLiabPayment(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfFirstTimeBuyer();


	/**
	 *
	 *
	 */
	public void setDfFirstTimeBuyer(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditScore();


	/**
	 *
	 *
	 */
	public void setDfCreditScore(java.math.BigDecimal value);


	/**
	 *
	 *
	 */
	public String getDfCreditBureauSummary();


	/**
	 *
	 *
	 */
	public void setDfCreditBureauSummary(String value);


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfCreditBureauNameId();


	/**
	 *
	 *
	 */
	public void setDfCreditBureauNameId(java.math.BigDecimal value);


  //--DJ_LDI_CR--start--//
	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfBorrowerGenderId();

	/**
	 *
	 *
	 */
	public void setDfBorrowerGenderId(java.math.BigDecimal value);
  //--DJ_LDI_CR--end--//


  //--DJ_CR201.2--start//
	/**
	 *
	 *
	 */
	public String getDfGuarantorOtherLoans();

	/**
	 *
	 *
	 */
	public void setDfGuarantorOtherLoans(String value);
  //--DJ_CR201.2--end//

    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	
	/**
	 * <p>returns smoke status id</p>
	 * @return - returns smoke status id
	 */
	public java.math.BigDecimal getDfSmokeStatusId();

	/**
	 * <p>sets smoke status id</p>
	 * @param - sets smoke status id
	 */
	public void setDfSmokeStatusId(java.math.BigDecimal value); 
	
	/**
	 * <p>returns solicitation id</p>
	 * @return - returns solicitation id
	 */
	public java.math.BigDecimal getDfSolicitationId();
	
	/**
	 * <p>sets smoke status id</p>
	 * @param - sets smoke status id
	 */
	public void setDfSolicitationId(java.math.BigDecimal value); 
	
	/**
	 * <p>returns Employee Number</p>
	 * @return - returns Employee Number
	 */
	public String getDfEmployeeNumber ();
	
	/**
	 * <p>sets Employee Number</p>
	 * @param - sets Employee Number
	 */
	public void setDfEmployeeNumber(String value); 
	
	/**
	 * <p>returns preferred method contact id</p>
	 * @return - returns preferred method contact id
	 */
	public java.math.BigDecimal getDfPrefContactMethodId ();
	
	/**
	 * <p>sets preferred method contact id</p>
	 * @param - sets preferred method contact id
	 */
	public void setDfPrefContactMethodId(java.math.BigDecimal value); 



    //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
    
    /**
     * <p>returns suffix id</p>
     * @return - returns suffix id
     */
    public java.math.BigDecimal getDfSuffixId();

    /**
     * <p>sets suffix id</p>
     * @param - sets suffix id
     */
    public void setDfSuffixId(java.math.BigDecimal value); 
    

    /**
     *
     *
     */
    public String getDfCellNumber();


    /**
     *
     *
     */
    public void setDfCellNumber(String value);
    
	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	public static final String FIELD_DFDEALID="dfDealId";
	public static final String FIELD_DFBORROWERID="dfBorrowerId";
	public static final String FIELD_DFCOPYID="dfCopyId";
	public static final String FIELD_DFSALUTATIONID="dfSalutationId";
	public static final String FIELD_DFFIRSTNAME="dfFirstName";
	public static final String FIELD_DFMIDDLENAME="dfMiddleName";
	public static final String FIELD_DFLASTNAME="dfLastName";
	public static final String FIELD_DFBORROWERTYPEID="dfBorrowerTypeId";
	public static final String FIELD_DFBIRTHDATE="dfBirthDate";
	public static final String FIELD_DFMARITALSTATUSID="dfMaritalstatusId";
	public static final String FIELD_DFSIN="dfSIN";
	public static final String FIELD_DFCITIZENSHIPID="dfCitizenshipId";
	public static final String FIELD_DFNOOFDEPENDANTS="dfNoOfDependants";
	public static final String FIELD_DFEXISTINGCLIENT="dfExistingClient";
	public static final String FIELD_DFCLIENTREFERENCENUMBER="dfClientReferenceNumber";
	public static final String FIELD_DFNOOFBANKCRUPTS="dfNoOfBankCrupts";
	public static final String FIELD_DFBANKRUPTCYSTATUSID="dfBankruptcyStatusId";
	public static final String FIELD_DFSTAFFOFLENDER="dfStaffOfLender";
	public static final String FIELD_DFHOMEPHONE="dfHomePhone";
	public static final String FIELD_DFWORKPHONE="dfWorkPhone";
	public static final String FIELD_DFWORKPHONEEXTENSION="dfWorkPhoneExtension";
	public static final String FIELD_DFFAXNUMBER="dfFaxNumber";
	public static final String FIELD_DFEMAILADDRESS="dfEmailAddress";
	public static final String FIELD_DFLANGUAGEPREFID="dfLanguagePrefId";
	public static final String FIELD_DFTOTALASSET="dfTotalAsset";
	public static final String FIELD_DFTOTALINCOME="dfTotalIncome";
	public static final String FIELD_DFTOTALLIAB="dfTotalLiab";
	public static final String FIELD_DFTOTALLIABPAYMENT="dfTotalLiabPayment";
	public static final String FIELD_DFFIRSTTIMEBUYER="dfFirstTimeBuyer";
	public static final String FIELD_DFCREDITSCORE="dfCreditScore";
	public static final String FIELD_DFCREDITBUREAUSUMMARY="dfCreditBureauSummary";
	public static final String FIELD_DFCREDITBUREAUNAMEID="dfCreditBureauNameId";

  //--DJ_LDI_CR--start--//
	public static final String FIELD_DFBORROWERGENDERID="dfBorrowerGenderId";
  //--DJ_LDI_CR--end--//

  //--DJ_CR201.2--start//
	public static final String FIELD_DFGUARANTOROTHERLOANS="dfGuarantorOtherLoans";
  //--DJ_CR201.2--end//

    //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
	public static final String FIELD_DFSMOKESTATUSID = "dfSmokeStatusId";
	public static final String FIELD_DFSOLICITATIONID = "dfSolicitationId";
	public static final String FIELD_DFEMPLOYEENUMBER = "dfEmployeeNumber";
	public static final String FIELD_DFPREFCONTACTMETHODID = "dfPrefContactMethodId";
    //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//

    public static final String FIELD_DFSUFFIXID = "dfSuffixId";
    public static final String FIELD_DFCELLNUMBER="dfCellNumber";
	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

}

