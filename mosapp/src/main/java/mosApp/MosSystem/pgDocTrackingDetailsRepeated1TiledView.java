package mosApp.MosSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.iplanet.jato.RequestHandler;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.TiledView;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.StaticTextField;


/**
 *
 *
 *
 */
public class pgDocTrackingDetailsRepeated1TiledView extends RequestHandlingTiledViewBase
	implements TiledView, RequestHandler
{
	/**
	 *
	 *
	 */
	public pgDocTrackingDetailsRepeated1TiledView(View parent, String name)
	{
		super(parent, name);
		setMaxDisplayTiles(10);
		setPrimaryModelClass( doRowGeneratorModel.class );
		registerChildren();
		initialize();
	}


	/**
	 *
	 *
	 */
	protected void initialize()
	{
	}


	/**
	 *
	 *
	 */
	public void resetChildren()
	{
		getStApprPhone().setValue(CHILD_STAPPRPHONE_RESET_VALUE);
		getStApprContactName().setValue(CHILD_STAPPRCONTACTNAME_RESET_VALUE);
		getStPropertyAddress().setValue(CHILD_STPROPERTYADDRESS_RESET_VALUE);
		getStApprFax().setValue(CHILD_STAPPRFAX_RESET_VALUE);
		getStApprEmail().setValue(CHILD_STAPPREMAIL_RESET_VALUE);
		getStApprAddrLine1().setValue(CHILD_STAPPRADDRLINE1_RESET_VALUE);
		getStApprBusinessId().setValue(CHILD_STAPPRBUSINESSID_RESET_VALUE);
		getStApprPartyType().setValue(CHILD_STAPPRPARTYTYPE_RESET_VALUE);
		getStApprContactCompanyName().setValue(CHILD_STAPPRCONTACTCOMPANYNAME_RESET_VALUE);
		getStApprAddrLine2().setValue(CHILD_STAPPRADDRLINE2_RESET_VALUE);
		getHdEmptyLink().setValue(CHILD_HDEMPTYLINK_RESET_VALUE);
	}


	/**
	 *
	 *
	 */
	protected void registerChildren()
	{
		registerChild(CHILD_STAPPRPHONE,StaticTextField.class);
		registerChild(CHILD_STAPPRCONTACTNAME,StaticTextField.class);
		registerChild(CHILD_STPROPERTYADDRESS,StaticTextField.class);
		registerChild(CHILD_STAPPRFAX,StaticTextField.class);
		registerChild(CHILD_STAPPREMAIL,StaticTextField.class);
		registerChild(CHILD_STAPPRADDRLINE1,StaticTextField.class);
		registerChild(CHILD_STAPPRBUSINESSID,StaticTextField.class);
		registerChild(CHILD_STAPPRPARTYTYPE,StaticTextField.class);
		registerChild(CHILD_STAPPRCONTACTCOMPANYNAME,StaticTextField.class);
		registerChild(CHILD_STAPPRADDRLINE2,StaticTextField.class);
		registerChild(CHILD_HDEMPTYLINK,HiddenField.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Model management methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public Model[] getWebActionModels(int executionType)
	{
		List modelList=new ArrayList();
		switch(executionType)
		{
			case MODEL_TYPE_RETRIEVE:
				modelList.add(getdoRowGeneratorModel());;
				break;

			case MODEL_TYPE_UPDATE:
				;
				break;

			case MODEL_TYPE_DELETE:
				;
				break;

			case MODEL_TYPE_INSERT:
				;
				break;

			case MODEL_TYPE_EXECUTE:
				;
				break;
		}
		return (Model[])modelList.toArray(new Model[0]);
	}


	////////////////////////////////////////////////////////////////////////////////
	// View flow control methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	public void beginDisplay(DisplayEvent event)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeDisplayEvent
		// Ensure the primary model is non-null; if null, it would cause havoc
		// with the various methods dependent on the primary model
		if (getPrimaryModel() == null) throw new ModelControlException("Primary model is null");

		super.beginDisplay(event);
		resetTileIndex();
	}


	/**
	 *
	 *
	 */
	public boolean nextTile()
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onBeforeRowDisplayEvent
		boolean movedToRow = super.nextTile();
    int rowNum = this.getTileIndex();
    boolean ret = true;

		if (movedToRow)
		{
			// Put migrated repeated_onBeforeRowDisplayEvent code here
      DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

      handler.pageGetState(this.getParentViewBean());

      ret = handler.checkDisplayThisRow("Repeated1");

      if (rowNum > 0 && ret == true)
      {
        handler.populateAppraiser(rowNum);
      }

      handler.pageSaveState();

      return ret;
		}

		return movedToRow;

		// The following code block was migrated from the Repeated1_onBeforeRowDisplayEvent method

		// MigrationToDo : THIS CODE MUST BE MANUALLY ADJUSTED
		/*
		int retval = SKIP;

		DocTrackDetailHandler handler =(DocTrackDetailHandler) this.handler.cloneSS();

		handler.pageGetState(this);

		if (event.getRowIndex() > 0 ||(retval = handler.checkDisplayThisRow("Repeated1")) == PROCEED)
		{
			handler.populateAppraiser(event.getRowIndex());
		}

		handler.pageSaveState();

		return retval;
		*/
	}


	/**
	 *
	 *
	 */
	public boolean beforeModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onBeforeDataObjectExecuteEvent
		return super.beforeModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterModelExecutes(Model model, int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterDataObjectExecuteEvent
		super.afterModelExecutes(model, executionContext);

	}


	/**
	 *
	 *
	 */
	public void afterAllModelsExecute(int executionContext)
	{

		// This is the analog of NetDynamics repeated_onAfterAllDataObjectsExecuteEvent
		super.afterAllModelsExecute(executionContext);

	}


	/**
	 *
	 *
	 */
	public void onModelError(Model model, int executionContext, ModelControlException exception)
		throws ModelControlException
	{

		// This is the analog of NetDynamics repeated_onDataObjectErrorEvent
		super.onModelError(model, executionContext, exception);

	}


	////////////////////////////////////////////////////////////////////////////////
	// Child manipulation methods
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected View createChild(String name)
	{
		if (name.equals(CHILD_STAPPRPHONE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPRPHONE,
				CHILD_STAPPRPHONE,
				CHILD_STAPPRPHONE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPRCONTACTNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPRCONTACTNAME,
				CHILD_STAPPRCONTACTNAME,
				CHILD_STAPPRCONTACTNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STPROPERTYADDRESS))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STPROPERTYADDRESS,
				CHILD_STPROPERTYADDRESS,
				CHILD_STPROPERTYADDRESS_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPRFAX))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPRFAX,
				CHILD_STAPPRFAX,
				CHILD_STAPPRFAX_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPREMAIL))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPREMAIL,
				CHILD_STAPPREMAIL,
				CHILD_STAPPREMAIL_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPRADDRLINE1))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPRADDRLINE1,
				CHILD_STAPPRADDRLINE1,
				CHILD_STAPPRADDRLINE1_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPRBUSINESSID))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPRBUSINESSID,
				CHILD_STAPPRBUSINESSID,
				CHILD_STAPPRBUSINESSID_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPRPARTYTYPE))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPRPARTYTYPE,
				CHILD_STAPPRPARTYTYPE,
				CHILD_STAPPRPARTYTYPE_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPRCONTACTCOMPANYNAME))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPRCONTACTCOMPANYNAME,
				CHILD_STAPPRCONTACTCOMPANYNAME,
				CHILD_STAPPRCONTACTCOMPANYNAME_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_STAPPRADDRLINE2))
		{
			StaticTextField child = new StaticTextField(this,
				getDefaultModel(),
				CHILD_STAPPRADDRLINE2,
				CHILD_STAPPRADDRLINE2,
				CHILD_STAPPRADDRLINE2_RESET_VALUE,
				null);
			return child;
		}
		else
		if (name.equals(CHILD_HDEMPTYLINK))
		{
			HiddenField child = new HiddenField(this,
				getdoRowGeneratorModel(),
				CHILD_HDEMPTYLINK,
				doRowGeneratorModel.FIELD_DFROWNDX,
				CHILD_HDEMPTYLINK_RESET_VALUE,
				null);
			return child;
		}
		else
			throw new IllegalArgumentException("Invalid child name [" + name + "]");
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprPhone()
	{
		return (StaticTextField)getChild(CHILD_STAPPRPHONE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprContactName()
	{
		return (StaticTextField)getChild(CHILD_STAPPRCONTACTNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStPropertyAddress()
	{
		return (StaticTextField)getChild(CHILD_STPROPERTYADDRESS);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprFax()
	{
		return (StaticTextField)getChild(CHILD_STAPPRFAX);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprEmail()
	{
		return (StaticTextField)getChild(CHILD_STAPPREMAIL);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprAddrLine1()
	{
		return (StaticTextField)getChild(CHILD_STAPPRADDRLINE1);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprBusinessId()
	{
		return (StaticTextField)getChild(CHILD_STAPPRBUSINESSID);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprPartyType()
	{
		return (StaticTextField)getChild(CHILD_STAPPRPARTYTYPE);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprContactCompanyName()
	{
		return (StaticTextField)getChild(CHILD_STAPPRCONTACTCOMPANYNAME);
	}


	/**
	 *
	 *
	 */
	public StaticTextField getStApprAddrLine2()
	{
		return (StaticTextField)getChild(CHILD_STAPPRADDRLINE2);
	}


	/**
	 *
	 *
	 */
	public HiddenField getHdEmptyLink()
	{
		return (HiddenField)getChild(CHILD_HDEMPTYLINK);
	}


	/**
	 *
	 *
	 */
	public doRowGeneratorModel getdoRowGeneratorModel()
	{
		if (doRowGenerator == null)
			doRowGenerator = (doRowGeneratorModel) getModel(doRowGeneratorModel.class);
		return doRowGenerator;
	}


	/**
	 *
	 *
	 */
	public void setdoRowGeneratorModel(doRowGeneratorModel model)
	{
			doRowGenerator = model;
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Child accessors
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Child rendering methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Repeated event methods
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static Map FIELD_DESCRIPTORS;
	public static final String CHILD_STAPPRPHONE="stApprPhone";
	public static final String CHILD_STAPPRPHONE_RESET_VALUE="";
	public static final String CHILD_STAPPRCONTACTNAME="stApprContactName";
	public static final String CHILD_STAPPRCONTACTNAME_RESET_VALUE="";
	public static final String CHILD_STPROPERTYADDRESS="stPropertyAddress";
	public static final String CHILD_STPROPERTYADDRESS_RESET_VALUE="";
	public static final String CHILD_STAPPRFAX="stApprFax";
	public static final String CHILD_STAPPRFAX_RESET_VALUE="";
	public static final String CHILD_STAPPREMAIL="stApprEmail";
	public static final String CHILD_STAPPREMAIL_RESET_VALUE="";
	public static final String CHILD_STAPPRADDRLINE1="stApprAddrLine1";
	public static final String CHILD_STAPPRADDRLINE1_RESET_VALUE="";
	public static final String CHILD_STAPPRBUSINESSID="stApprBusinessId";
	public static final String CHILD_STAPPRBUSINESSID_RESET_VALUE="";
	public static final String CHILD_STAPPRPARTYTYPE="stApprPartyType";
	public static final String CHILD_STAPPRPARTYTYPE_RESET_VALUE="";
	public static final String CHILD_STAPPRCONTACTCOMPANYNAME="stApprContactCompanyName";
	public static final String CHILD_STAPPRCONTACTCOMPANYNAME_RESET_VALUE="";
	public static final String CHILD_STAPPRADDRLINE2="stApprAddrLine2";
	public static final String CHILD_STAPPRADDRLINE2_RESET_VALUE="";
	public static final String CHILD_HDEMPTYLINK="hdEmptyLink";
	public static final String CHILD_HDEMPTYLINK_RESET_VALUE="";


	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////

	private doRowGeneratorModel doRowGenerator=null;

	private DocTrackDetailHandler handler=new DocTrackDetailHandler();

}

