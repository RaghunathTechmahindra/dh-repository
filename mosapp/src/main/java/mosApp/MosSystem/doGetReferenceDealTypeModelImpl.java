package mosApp.MosSystem;

import java.sql.SQLException;

import com.basis100.picklist.BXResources;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryFieldDescriptor;
import com.iplanet.jato.model.sql.QueryFieldSchema;
import com.iplanet.jato.model.sql.QueryModelBase;

/**
 *
 *
 *
 */
public class doGetReferenceDealTypeModelImpl extends QueryModelBase
	implements doGetReferenceDealTypeModel
{
	/**
	 *
	 *
	 */
	public doGetReferenceDealTypeModelImpl()
	{
		super();
		setDataSourceName(DATA_SOURCE_NAME);

		setSelectSQLTemplate(SELECT_SQL_TEMPLATE);

		setStaticWhereCriteriaString(STATIC_WHERE_CRITERIA);

		setModifyingQueryTableName(MODIFYING_QUERY_TABLE_NAME);

		setFieldSchema(FIELD_SCHEMA);

		initialize();

	}


	/**
	 *
	 *
	 */
	public void initialize()
	{
	}


	////////////////////////////////////////////////////////////////////////////////
	// NetDynamics-migrated events
	////////////////////////////////////////////////////////////////////////////////
	/**
	 *
	 *
	 */
	protected String beforeExecute(ModelExecutionContext context, int queryType, String sql)
		throws ModelControlException
	{

		// TODO: Migrate specific onBeforeExecute code
		return sql;
	}


	/**
	 *
	 *
	 */
	protected void afterExecute(ModelExecutionContext context, int queryType)
		throws ModelControlException
	{
    //--Release2.1--//
    //To override all the Description fields by populating from BXResource
    //--> By Billy 19Nov2002
    //Get the Language ID from the SessionStateModel
    String defaultInstanceStateName =
			getRequestContext().getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
    SessionStateModelImpl theSessionState =
        (SessionStateModelImpl)getRequestContext().getModelManager().getModel(
                        SessionStateModel.class,
                        defaultInstanceStateName,
                        true);
    int languageId = theSessionState.getLanguageId();
	int institutionId = theSessionState.getDealInstitutionId();
    while(this.next())
    {
      //Convert ReferenceDealType Desc.
      this.setDfReferenceDealTypeDesc(BXResources.getPickListDescription(
          institutionId, "REFERENCEDEALTYPE", this.getDfReferenceDealTypeId().intValue(), languageId));
    }
    //Reset Location
    this.beforeFirst();
	}


	/**
	 *
	 *
	 */
	protected void onDatabaseError(ModelExecutionContext context, int queryType, SQLException exception)
	{
	}


	/**
	 *
	 *
	 */
	public java.math.BigDecimal getDfReferenceDealTypeId()
	{
		return (java.math.BigDecimal)getValue(FIELD_DFREFERENCEDEALTYPEID);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceDealTypeId(java.math.BigDecimal value)
	{
		setValue(FIELD_DFREFERENCEDEALTYPEID,value);
	}


	/**
	 *
	 *
	 */
	public String getDfReferenceDealTypeDesc()
	{
		return (String)getValue(FIELD_DFREFERENCEDEALTYPEDESC);
	}


	/**
	 *
	 *
	 */
	public void setDfReferenceDealTypeDesc(String value)
	{
		setValue(FIELD_DFREFERENCEDEALTYPEDESC,value);
	}




	////////////////////////////////////////////////////////////////////////////
	// Obsolete Netdynamics Events - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Methods - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Class variables
	////////////////////////////////////////////////////////////////////////////////

	public static final String DATA_SOURCE_NAME="jdbc/orcl";
	
	public static final String SELECT_SQL_TEMPLATE="SELECT ALL REFERENCEDEALTYPE.REFERENCEDEALTYPEID, REFERENCEDEALTYPE.RDTDESCRIPTION FROM REFERENCEDEALTYPE  __WHERE__  ";
	
	public static final String MODIFYING_QUERY_TABLE_NAME="REFERENCEDEALTYPE";
	
	public static final String STATIC_WHERE_CRITERIA="";
	
	public static final QueryFieldSchema FIELD_SCHEMA=new QueryFieldSchema();
	public static final String QUALIFIED_COLUMN_DFREFERENCEDEALTYPEID="REFERENCEDEALTYPE.REFERENCEDEALTYPEID";
	public static final String COLUMN_DFREFERENCEDEALTYPEID="REFERENCEDEALTYPEID";
	public static final String QUALIFIED_COLUMN_DFREFERENCEDEALTYPEDESC="REFERENCEDEALTYPE.RDTDESCRIPTION";
	public static final String COLUMN_DFREFERENCEDEALTYPEDESC="RDTDESCRIPTION";




	////////////////////////////////////////////////////////////////////////////
	// Instance variables
	////////////////////////////////////////////////////////////////////////////





	////////////////////////////////////////////////////////////////////////////
	// Custom Members - Require Manual Migration
	////////////////////////////////////////////////////////////////////////////

	static
	{

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCEDEALTYPEID,
				COLUMN_DFREFERENCEDEALTYPEID,
				QUALIFIED_COLUMN_DFREFERENCEDEALTYPEID,
				java.math.BigDecimal.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

		FIELD_SCHEMA.addFieldDescriptor(
			new QueryFieldDescriptor(
				FIELD_DFREFERENCEDEALTYPEDESC,
				COLUMN_DFREFERENCEDEALTYPEDESC,
				QUALIFIED_COLUMN_DFREFERENCEDEALTYPEDESC,
				String.class,
				false,
				false,
				QueryFieldDescriptor.APPLICATION_INSERT_VALUE_SOURCE,
				"",
				QueryFieldDescriptor.ON_EMPTY_VALUE_EXCLUDE,
				""));

	}

}

