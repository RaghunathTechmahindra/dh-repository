/*
 * @(#)ExpressApplicationServlet.java    2007-1-30
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.iplanet.jato.RequestManager;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.NavigationException;
import com.iplanet.jato.CompleteRequestException;
import com.iplanet.jato.view.ViewBean;

import mosApp.MosSystem.MosSystemServlet;
import mosApp.MosSystem.SessionServices;
import mosApp.MosSystem.SessionStateModel;
import mosApp.MosSystem.SessionStateModelImpl;

import com.basis100.deal.entity.simplecache.ThreadLocalEntityCache;
import com.filogix.express.web.ajax.JatoAJAXDispatching;
import com.filogix.express.web.util.SessionObjectHolder;
import com.filogix.express.web.util.service.MosappSessionListener;

/**
 * ExpressApplicationServlet the core servlet for Express web application.
 *
 * @version   1.0 2007-1-30
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressApplicationServlet extends MosSystemServlet {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ExpressApplicationServlet.class);

    /**
     * Constructor function
     */
    public ExpressApplicationServlet() {

        super();
    }

    /**
     * This method contains the main implementation of JATO's request
     * handling and dispatch mechanism.
     *
     * We need override it to fit our new framework's requirement.
     */
    protected void processRequest(String pageName,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
        throws ServletException, IOException {

        RequestContext requestContext=null;
        try
        {
            // We need to set the servlet in the request attributes to allow
            // code that needs to access the servlet (e.g. ViewBeanManager)
            // to access it.
            request.setAttribute(HANDLING_SERVLET_ATTRIBUTE_NAME,this);

            // Create the request handler
            requestContext=createRequestContext(request,response);

            // Note, we have to put the request context into the request 
            // attributes in order to make it accessible to classes "outside" 
            // of JATO, for example, tag handlers or other servlets
            request.setAttribute(REQUEST_CONTEXT_ATTRIBUTE_NAME,requestContext);

            // Allow subclasses to initialize the request handler
            initializeRequestContext(requestContext); 

            // Set a flag for use later in the tag library
            if (isShowMessageBuffer())
            {
                request.setAttribute(SHOW_MESSAGE_BUFFER_ATTRIBUTE_NAME,
                    new Boolean(true));
            }

            // Cache the request context for this request
            RequestManager.setRequestContext(requestContext);

            // Fire the pre-request event
            fireBeforeRequestEvent(requestContext);

            // Add any necessary headers to the response
            addResponseHeaders(requestContext);

            // 4.4GR, Clear simple entity cache before using
            ThreadLocalEntityCache.clearCache();
            
            // The minimal information we require is the handler (source)
            // bean name--check that we have it
            if (pageName==null)
            {
                //  Try to get a sensible default.  If a default wasn't
                // found, fire an event notifying the developer
                pageName=getDefaultHandlerName(request);
                if (pageName==null)
                    fireRequestHandlerNotSpecifiedEvent(requestContext);
            }

            // Fire any required session management events
            fireSessionEvents(requestContext);
            
            HttpSession session = requestContext.getRequest().getSession();            
            String pgSessionEnded = "pgSessionEnded";
            String sessionId = session.getId(), previousSessionId = requestContext.getRequest().getRequestedSessionId();
            SessionObjectHolder holder = (SessionObjectHolder) this.getServletContext().getAttribute(MosappSessionListener.LANGUAGE_MAP);
            if (holder!= null && previousSessionId != null && previousSessionId.trim().length() > 0 && !sessionId.equals(previousSessionId)) {
                Integer languageId = holder.getLanguage(previousSessionId);
                if (languageId != null && languageId.intValue() == Mc.LANGUAGE_PREFERENCE_FRENCH) {
                    String defaultInstanceStateName =
                        requestContext.getModelManager().
                        getDefaultModelInstanceName(SessionStateModel.class);
                    SessionStateModelImpl sessionState = (SessionStateModelImpl)
                        requestContext.getModelManager().
                        getModel(SessionStateModel.class,
                                 defaultInstanceStateName, true);
                    sessionState.setLanguageId(Mc.LANGUAGE_PREFERENCE_FRENCH);
                }
                SessionServices.previouslyUsedSession(previousSessionId);
                requestContext.getResponse().sendRedirect(pgSessionEnded + ".jsp");
                return;
            }

            if (_log.isDebugEnabled())
                _log.debug("Working on page: " + pageName);
            // SEAN AJAX Dispaching
            if (pageName.equals(JatoAJAXDispatching.PAGE_NAME_FOR_AJAX)) {
            	
                JatoAJAXDispatching.dispatchAJAXRequest(getServletContext(),
                                                        request, response);
            } else {

                ViewBean viewBean=getViewBeanInstance(pageName,requestContext);
    
                try
                {
                    // Dispatch the event to the bean
                    dispatchRequest(viewBean,requestContext);
                }
                catch (NavigationException e)
                {
                    // Navigation exceptions normally wrap the two
                    // types of exceptions generated during forwarding
                    // and including.  If these types of exceptions are
                    // the root cause, we unwrap and throw them.
                    if (e.getRootCause() instanceof IOException)
                        throw (IOException)e.getRootCause();
                    else
                    if (e.getRootCause() instanceof ServletException)
                        throw (ServletException)e.getRootCause();
                    else
                        throw e;
                }
                
                
            }
            // SEAN AJAX Dispaching End.
        }
        catch (CompleteRequestException e)
        {
            // Ignore silently
        }
        catch (Exception e)
        {
            com.iplanet.jato.Log.log(com.iplanet.jato.Log.ERROR,
                                     "Uncaught application exception",e);
            fireUncaughtException(requestContext,e);
        }
        finally
        {
            /**
             * BEGIN FIX FOR FXP21251
             * Mohan V Premkumar
             * 04/17/2008
             */
            try {
            // Notify the request completion listeners
                notifyRequestCompletionListeners(requestContext);
            } catch (IllegalStateException ise) {
                _log.info(ise);
            }
            /**
             * END FIX FOR FXP21251
             */
            // Fire the post-request event
            fireAfterRequestEvent(requestContext);
        }
        

    }
}
