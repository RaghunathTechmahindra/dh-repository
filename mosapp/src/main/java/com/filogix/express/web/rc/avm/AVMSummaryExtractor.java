/*
 * @(#)AVMSummaryExtractor.java    2006-5-2
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.avm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.entity.AvmSummary;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;

import com.filogix.express.web.rc.util.TextResponseExtractor;
import com.filogix.express.web.rc.util.ExtractReportException;

/**
 * AVMSummaryExtractor will extract the avm summary report from the backend
 * persistence layer.
 *
 * @version   1.0 2006-5-2
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AVMSummaryExtractor extends TextResponseExtractor {

    // The logger
    private final static Log _log =
        LogFactory.getLog(AVMSummaryExtractor.class);

    /**
     * Constructor function
     */
    public AVMSummaryExtractor() {
    }

    /**
     * extract the AVM summary by using the entities.
     */
    public String extractTextReport() throws ExtractReportException {

        SessionResourceKit srk = new SessionResourceKit("viewer");
        int responseId = getResponseId();
        try {
            if (_log.isDebugEnabled())
                _log.debug("Trying to get AVM Summary for response: " +
                           responseId);
            AvmSummary summary = new AvmSummary(srk, responseId);
            String report = summary.getAvmReport();
            return report;
        } catch (RemoteException re) {
            _log.error("Remote exception: ", re);
            throw new ExtractReportException("Can not find reprt for:" +
                                             responseId, re);
        } catch (FinderException fe) {
            _log.error("Finder exception: ", fe);
            throw new ExtractReportException("Can not find reprt for:" +
                                             responseId, fe);
        } finally {
            srk.freeResources();
        }
    }
}
