package com.filogix.express.web.rc.miresponse;

import com.filogix.express.web.rc.GenericRequestObject;


public class MIResponseAlertRequestObject extends GenericRequestObject{
	
	public MIResponseAlertRequestObject(){
	}

    /**
     * time out
     */
    public boolean getTimeOut() {

        return ((Boolean) get("timeOut")).booleanValue();
    }
}
