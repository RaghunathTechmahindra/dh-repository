package com.filogix.express.web.rc.migeneric;

import java.net.SocketTimeoutException;

import mosApp.MosSystem.MorgageInsHandler;
import mosApp.MosSystem.PageEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.ChannelRequestConnectivityException;
import com.filogix.externallinks.services.mi.MIRequestService;
import com.filogix.externallinks.services.mi.MIRequestServiceFactory;

public class MIProcessor extends AbstractRequestProcessorJato {

    private final static Logger logger =
        LoggerFactory.getLogger(MIProcessor.class);

    public ResponseObject processRequest(RequestObject requestObj,
        ResponseObject responeObj) throws RequestProcessException {

        logger.debug("MIProcessor (Generic) start");
        
        MIRequestObject reqObj = (MIRequestObject) requestObj;
        MIResponseObject resObj = (MIResponseObject) responeObj;

        MorgageInsHandler handler = new MorgageInsHandler();
        initializeHandler(handler);

        ServiceInfo info = new ServiceInfo();
        try {

            SessionResourceKit srk = handler.getSessionResourceKit();

            PageEntry pe = handler.getTheSessionState().getCurrentPage();
            
            
            //adopt copy and change current "T" copy to "S". 
            //rest of the steps will use "S" copy. 
            //so that this record won't be removed when user click cancel button.
            adoptTransactionCopy(handler);
            
            int copyid = forceAdoptScenarioRecommended(
            		pe.getPageDealId(), getDealEditControl(), pe.getPageDealCID(), srk);
            
            Deal deal = new Deal(srk, null, pe.getPageDealId(), copyid);
            
            logger.debug("Copyid = " + copyid);
            logger.debug("MortgageInsurerId=" + deal.getMortgageInsurerId());
            logger.debug("MIIndicatorId=" + deal.getMIIndicatorId());
            logger.debug("MIStatusId=" + deal.getMIStatusId());
            logger.debug("cancel?=" + reqObj.isCancel());
            
            info.setSrk(srk);
            info.setLanguageId(getLanguageId());
            info.setDeal(deal);

            MIRequestServiceFactory factory = MIRequestServiceFactory.getInstance();
            MIRequestService mirequest = factory.getMIRequestService(deal.getMortgageInsurerId());

            try {
				mirequest.service(info);
				resObj.setResponseStatus(RequestObject.STATUS_RESPONSE_SUCCESS);
			} catch (ChannelRequestConnectivityException crce) {
				//connectivity error
				logger.error("failed to send MI request", crce);
	            resObj.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
	            if(crce.getFaultCode() > 0){
	            	resObj.setResponseErrors(String.valueOf(crce.getFaultCode()));
	            }
			}
            catch(SocketTimeoutException ste) {
            	logger.error("failed to send MI request", ste);
            	resObj.setResponseStatus(RequestObject.STATUS_RESPONSE_TIMEOUT);
            }

            // For now, this class won't execute workflow because it's slow
            // WFETrigger.workflowTrigger(2, srk, deal.getUnderwriterUserId(),
            // deal.getDealId(), deal.getCopyId(), -1, null);

        } catch (Exception e) {
            logger.error("Exception in MIProcessor", e);
            resObj.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
            //resObj.setResponseErrors(e.getMessage());
        } finally {
            handler.unsetSessionResourceKit();
        }
        
        logger.debug("MIProcessor (Generic) end");
        return resObj;
    }

}
