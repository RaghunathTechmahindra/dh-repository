/*
 * @(#)BERequestObject.java    2006-6-21
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.closing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericRequestObject;

/**
 * BERequestObject - 
 *
 * @version   1.0 2006-6-21
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class BERequestObject extends GenericRequestObject {

    // The logger
    private final static Log _log = LogFactory.getLog(BERequestObject.class);

    /**
     * Constructor function
     */
    public BERequestObject() {
    }

    /**
     * the dealid,
     */
    public int getDealId() {

        return ((Integer) get("dealId")).intValue();
    }

    /**
     * the copy id.
     */
    public int getCopyId() {

        return ((Integer) get("copyId")).intValue();
    }

    /**
     * the request id.
     */
    public int getRequestId() {

        return ((Integer) get("requestId")).intValue();
    }

    /**
     * the service request contact id.
     */
    public int getServiceRequestContactId() {

        return ((Integer) get("serviceRequestContactId")).intValue();
    }

    /**
     * need primary borrower or not.
     */
    public boolean getNeedPrimary() {

        return ((Boolean) get("needPrimary")).booleanValue();
    }
}
