package com.filogix.express.web.rc.lpr.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basis100.mtgrates.ProductInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: LegacyProductConverter
 * <p>
 * Description: Legacy CSV like data converter for product 
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 * @version 1.1 Sep 23, 2008 FXP22579, display order change
 */
public class LegacyProductConverter extends AbstractLegacyCSVConverter implements
IConverter<ProductInfo> {

    /**
     * this method converts a result data list from PricingLigc into a legacy CSV like String
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<ProductInfo> list, SessionResourceKit srk) {

        int institutionId = srk.getExpressState().getDealInstitutionId();
        int languageId = srk.getLanguageId();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

        /***************MCM Impl team changes starts - FXP22579*******************/
        if(list != null)
            for (ProductInfo prod : list) {
                Map<String, Object> map = new HashMap<String, Object>();

                String productName = BXResources.getPickListDescription(
                        institutionId, "MTGPROD", prod.getMtgProdId(), languageId);

                Integer order = BXResources.getPickListSortOrder(institutionId, "MTGPROD", prod.getMtgProdId()+"", languageId);

                map.put("prodId", prod.getMtgProdId());
                map.put("prodDesc", productName);
                map.put("order", order);
                resultList.add(map);
            }

        Collections.sort(resultList, new Comparator<Map<String, Object>>() {
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                Integer order1 = (Integer) o1.get("order"); 
                Integer order2 = (Integer) o2.get("order");
                return order1.compareTo(order2);
            }
        });

        StringBuffer products = new StringBuffer();
        if(resultList != null)
            for (Map<String, Object> prod : resultList) {
                products.append("'" + prod.get("prodDesc") + "'").append(COLUMN_DELIMITER);
                products.append("'" + prod.get("prodId") + "'").append(LINE_DELIMITER);
            }
        res.setProductString(products.toString());
        /***************MCM Impl team changes ends - FXP22579*********************/

        return res;
    }

}
