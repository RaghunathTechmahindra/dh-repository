package com.filogix.express.web.rc.lpr.adapter;

import com.basis100.mtgrates.IPricingLogic;
import com.filogix.express.web.rc.lpr.converter.IConverter;

/**
 * Title: AbstractPricingLogicAdapter
 * <p>
 * Description: adapter 
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public abstract class AbstractPricingLogicAdapter implements
        IPricingLogicAdapter {

    protected IPricingLogic pricingLogic;
    protected IConverter converter;
    
    /**
     * @see com.filogix.express.web.rc.lpr.adapter.IPricingLogicAdapter#getPricingLogic()
     */
    public IPricingLogic getPricingLogic() {
        return pricingLogic;
    }

    /**
     * @see com.filogix.express.web.rc.lpr.adapter.IPricingLogicAdapter#setPricingLogic(com.basis100.mtgrates.IPricingLogic)
     */
    public void setPricingLogic(IPricingLogic logic) {
        pricingLogic = logic;
    }

    /**
     * @see com.filogix.express.web.rc.lpr.adapter.IPricingLogicAdapter#getConverter()
     */
    public IConverter getConverter() {
        return converter;
    }

    /**
     * @see com.filogix.express.web.rc.lpr.adapter.IPricingLogicAdapter#setConverter(com.filogix.express.web.rc.lpr.converter.IConverter)
     */
    public void setConverter(IConverter converter) {
        this.converter = converter;
    }

}
