package com.filogix.express.web.rc.lpr.dao;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;

/**
 * Title: ComponentEligibleCheckDaoImpl
 * <p>
 * Description: Data access object for Component Eligible check 
 * @author MCM Impl Team.
 * @version 1.0 Aug 07, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public class ComponentEligibleCheckDaoImpl implements ComponentEligibeCheckDao {

    protected String componentEligibleFlag = "N";

    public String getComponentEligibleFlag() {
        return componentEligibleFlag;
    }

    public void load(SessionResourceKit srk, LPRRequestObject request)
    throws Exception {
        
        MtgProd mtgProd = new MtgProd(srk, null, request.getProductId());
        this.componentEligibleFlag = mtgProd.getComponentEligibleFlag();
    }

}
