package com.filogix.express.web.rc.prioritytask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericRequestObject;

/**
 * Request Object for unacknowledged tasks checking.
 * @author bzhang
 */
public class PTaskRequestObject extends GenericRequestObject{
	private static Log _log = LogFactory.getLog(PTaskRequestObject.class);

	public PTaskRequestObject(){
	}

//	public String getUserId(){
//		return (String) get("userId");
//	}
    
    /**
     * time out
     */
    public boolean getTimeOut() {

        return ((Boolean) get("timeOut")).booleanValue();
    }
}
