package com.filogix.express.web.util.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;

import mosApp.MosSystem.QuickLinksHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QuickLinkFaviconFinder {
    
    private final static Logger  logger = LoggerFactory.getLogger(QuickLinkFaviconFinder.class);

    public String getFaviconURL(String quickLinkURL) {
        
        String contentOriginal = getContent(quickLinkURL);
        if (contentOriginal == null) return null;
        String content = contentOriginal.toLowerCase();
        int keyIndex = content.indexOf("favicon.ico");
        if (keyIndex < 0) {
            return quickLinkURL;
        } else {
            int endIndex = keyIndex+11;
            String sContent = content.substring(0, endIndex);
            int startIndex = sContent.lastIndexOf("http"); 
            sContent = content.substring(startIndex, endIndex);
            return sContent;
        }
    }
    
    public String getContent(String url){
        
        try {
            
            URL site = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) site.openConnection();
            InputStream stream = connection.getInputStream();

            InputStreamReader reader = new InputStreamReader(stream);
            BufferedReader in = new BufferedReader(reader);

          String inputLine;
          StringBuffer htmlResponse = new StringBuffer();
          while ((inputLine = in.readLine()) != null) {
              htmlResponse.append(inputLine);
          }
            
            in.close(); 
            return htmlResponse.toString();
        } catch (Exception e) {
            logger.info(url + " Couldn't acces");
            logger.info(e.toString());            
            return null;
        }        
    }    
}
