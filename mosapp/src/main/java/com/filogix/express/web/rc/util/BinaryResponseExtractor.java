/*
 * @(#)BinaryResponseExtractor.java    2006-7-4
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * BinaryResponseExtractor - 
 *
 * @version   1.0 2006-7-4
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class BinaryResponseExtractor extends AbstractReportExtractor {

    // The logger
    private final static Log _log =
        LogFactory.getLog(BinaryResponseExtractor.class);

    // the http response.
    private HttpServletResponse _httpResponse;

    /**
     * set the http servlet response.
     */
    public void setHttpResponse(HttpServletResponse httpResponse) {

        _httpResponse = httpResponse;
    }

    /**
     * extract binary report as byte array.
     */
    public abstract byte[] extractBinaryReport() throws ExtractReportException;

    /**
     * returns the binary report's content type.
     */
    public abstract String getReportContentType();

    /**
     * extract the report.
     */
    public Object extractReport() throws ExtractReportException {

        byte[] report = extractBinaryReport();
        try {
            _httpResponse.setContentType("application/" +
                                         getReportContentType());
            _httpResponse.setHeader("Content-disposition",
                                    "attachment; filename=report." +
                                    getReportContentType());
            _httpResponse.setContentLength(report.length);
            // write to the response.
            _httpResponse.getOutputStream().write(report, 0, report.length);
            _httpResponse.getOutputStream().flush();
            _httpResponse.getOutputStream().close();
        } catch (IOException ie) {
            String message = ie.getMessage();
            if (message.startsWith("ClientAbortException")) {
                _log.warn("Cancel from client, ignore...");
            } else {
                _log.error("Can not write to HTTP response.", ie);
                throw new ExtractReportException(ie.getMessage(), ie);
            }
        }

        // no need to return anything.
        return null;
    }
}
