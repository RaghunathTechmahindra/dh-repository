package com.filogix.express.web.rc.lpr.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basis100.mtgrates.LenderInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: JSONLenderConverter
 * <p>
 * Description: JSON version of implementation of converter for lender 
 * @author MCM Impl Team.
 * @version 1.0 Jul 30, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public class JSONLenderConverter implements IConverter<LenderInfo> {

    /**
     * this method converts a result data list from PricingLigc into a JSON String.
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<LenderInfo> list, SessionResourceKit srk) {

        int institutionId = srk.getExpressState().getDealInstitutionId();
        int languageId = srk.getLanguageId();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

        if(list != null)
        for (LenderInfo lender : list) {

            String lenderName = BXResources.getPickListDescription(
                    institutionId, "LENDERPROFILE", lender.getLenderProfileId(), languageId);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("lenderId", lender.getLenderProfileId());
            map.put("lenderDesc", lenderName);
            resultList.add(map);
        }
        res.setLenders(resultList);
        return res;
    }

}
