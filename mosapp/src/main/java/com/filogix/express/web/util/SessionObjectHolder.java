package com.filogix.express.web.util;

import java.io.Serializable;
import java.util.HashMap;

public class SessionObjectHolder implements Serializable {
    HashMap<String, Integer> languagePreferences = new HashMap();
    
    public void setLanguage(int userId, int institutionProfileID, int languageId) {
        languagePreferences.put(userId + "#" + institutionProfileID, new Integer(languageId));
    }
    
    public int getLanguage(int userId, int institutionProfileID) {
        Integer language;
        language = languagePreferences.get(userId + "#" + institutionProfileID);
        return language.intValue();
    }
    
    public void setLanguage(String sessionId, int languageId) {
        languagePreferences.put(sessionId, new Integer(languageId));
    }
    
    public Integer getLanguage(String sessionId) {
        Integer language;
        language = languagePreferences.get(sessionId);
        return language;
    }
}
