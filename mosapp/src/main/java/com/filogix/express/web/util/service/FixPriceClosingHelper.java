/*
 * @(#)FixPriceClosingHelper.java    2006-8-14
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.util.service;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ServiceConst;

/**
 * FixPriceClosingHelper - 
 *
 * @version   1.0 2006-8-14
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class FixPriceClosingHelper extends Helper4RequestWithContact {

    // The logger
    private final static Log _log =
        LogFactory.getLog(FixPriceClosingHelper.class);

    /**
     * Constructor function
     */
    public FixPriceClosingHelper() {
    }
    
    /**
     * returns requestId and contactId for FPC.
     */
    public static Map getFpcRequests(SessionResourceKit srk,
                                       int dealId, int copyId) {

        Map request = new TreeMap();
        
        //populate optionlist for provider
        // prepare the query SQL.
        String querySQL =
            "SELECT DISTINCT " +
            "request.REQUESTDATE, request.REQUESTID, " +
            "contact.SERVICEREQUESTCONTACTID " +
            "FROM " +
            "REQUEST request, SERVICEREQUESTCONTACT contact, " +
            "SERVICEREQUEST servicereq, SERVICEPRODUCT prod " +
            "WHERE " +
            "contact.REQUESTID = request.REQUESTID AND " +
            "request.DEALID = " + dealId + " AND " +
            "request.COPYID = " + copyId + " AND " +
            "servicereq.REQUESTID = request.REQUESTID AND " +
            "request.SERVICEPRODUCTID = prod.SERVICEPRODUCTID AND " +
            "prod.SERVICETYPEID = " + ServiceConst.SERVICE_TYPE_CLOSING + 
            " AND prod.SERVICESUBTYPEID = "+ 
            ServiceConst.SERVICE_SUB_TYPE_FIXED_PRICE_CLOSING +
            " ORDER BY request.REQUESTDATE DESC";
                    
        _log.info("Query SQL: " + querySQL);

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (; jExec.next(key);) {
                Integer requestId = 
                    new Integer(jExec.getInt(key, "REQUESTID"));
                Integer contactId = 
                    new Integer(jExec.getInt(key, "SERVICEREQUESTCONTACTID"));
                
                request.put(requestId, contactId);
                break;
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
        }
        return request;
    }
    
    
}
