/*
 * @(#)ViewReportProcessor.java    2006-4-25
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.entity.AvmSummary;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;

import com.filogix.express.web.rc.AbstractRequestProcessor;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;

/**
 * ViewReportProcessor - 
 *
 * @version   1.0 2006-4-25
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ViewReportProcessor extends AbstractRequestProcessor {

    // The logger
    private static Log _log = LogFactory.getLog(ViewReportProcessor.class);

    /**
     * Constructor function
     */
    public ViewReportProcessor() {
    }

    /**
     * process the request by using entities.
     */
    public ResponseObject processRequest(RequestObject request,
                                         ResponseObject response)
        throws RequestProcessException {

        // the response id.
        int responseId = ((Integer) request.get("responseId")).intValue();

        SessionResourceKit srk = new SessionResourceKit("viewer");
        try {

            AvmSummary summary = new AvmSummary(srk, responseId);
            String report = summary.getAvmReport();
            response.put("report", report);

        } catch (RemoteException re) {
            _log.error("Remote exception: ", re);
            // TODO: multi-language support.
            response.put(ResponseObject.RC_RESPONSE_ERRORS,
                         "Can not find report for" + responseId);
            throw new RequestProcessException("Can not find reprt for:" +
                                              responseId, re);
        } catch (FinderException fe) {
            _log.error("Finder exception: ", fe);
            // TODO: multi-language support.
            response.put(ResponseObject.RC_RESPONSE_ERRORS,
                         "Can not find report for" + responseId);
            throw new RequestProcessException("Can not find reprt for:" +
                                              responseId, fe);
        } finally {
            srk.freeResources();
        }

        return response;
    }
}
