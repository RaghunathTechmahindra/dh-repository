/*
 * @(#)StringHelper.java    2006-12-13
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * StringHelper is the helper class to hadle string related function for express
 * Web application.
 *
 * @version   1.0 2006-12-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class StringHelper {

    // The logger
    private final static Log _log = LogFactory.getLog(StringHelper.class);

    /**
     * escape HTML special characters from a string.
     *
     * @param input the input string.
     * @param escapeBlank if true, change blank space to &nbsp;
     * @param escapeTag if true, cahgne < and > to &lt; and &gt;
     */
    public static final String string2HTMLString(String input,
                                                 boolean escapeBlank,
                                                 boolean escapeTag) {

        if (_log.isDebugEnabled())
            _log.debug("input string: " + input);

        int charLength = input.length();
        StringBuffer ret = new StringBuffer(charLength);
        boolean previousCharBlank = false;
        char ch;

        for (int i = 0; i < charLength; i ++) {

            ch = input.charAt(i);
            if (ch == ' ') {
                // the blank space.
                if (escapeBlank) {
                    // escape blank to &nbsp; to keep the word breaking.
                    if (previousCharBlank) {
                        previousCharBlank = false;
                        // &#160;
                        ret.append("&nbsp;");
                    } else {
                        previousCharBlank = true;
                        ret.append(ch);
                    }
                } else {
                    // just add the blank space.
                    ret.append(ch);
                }
            } else {
                // set up for next char.
                previousCharBlank = false;

                if (ch == '"') {
                    // &quot;
                    ret.append("&#34;");
                } else if (ch == '\'') {
                    // Apostrophe
                    ret.append("&#39;");
                } else if (ch == '<') {
                    // &lt;
                    if (escapeTag)
                        ret.append("&#60;");
                    else
                        ret.append(ch);
                } else if (ch == '>') {
                    // &gt;
                    if (escapeTag)
                        ret.append("&#62;");
                    else
                        ret.append(ch);
                } else {
                    int chInt = 0xffff & ch;
                    if (chInt < 160) {
                        // basic standard char.
                        ret.append(ch);
                    } else {
                        // using unicode system is more reliable.
                        ret.append("&#").
                            append((new Integer(chInt)).toString()).
                            append(";");
                    }
                }
            }
        }

        if (_log.isDebugEnabled())
            _log.debug("HTML String: " + ret.toString());
        return ret.toString();
    }

    /**
     * simple signature with escapeBlank as false and escapeTag as false.
     */
    public static final String string2HTMLString(String input) {

        return string2HTMLString(input, false, false);
    }
}
