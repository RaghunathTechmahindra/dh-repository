/*
 * @(#)RequestObject.java    2006-2-28
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.util.Map;

/**
 * RequestObject defines the generic interface to access a service request.
 *
 * @version   1.0 2006-2-28
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface RequestObject extends Map {

    // defines some convenient interface here.
    /**
     * returns the service type.
     * the available service types are specified in the
     * ServicePorcessorFactory.
     */
    public String getRequestType();

    /**
     * returns the request object's properties as a Properties object.
     */
    public Map getRequestProperties();

    /**
     * this reserved key name is used by Rich client javascript to pass by the
     * request type to serve side.
     */
    public final static String RC_REQUEST_TYPE = "rcRequestType";
    public final static String RC_REQUEST_ACTION = "rcRequestAction";
    public final static String RC_REQUEST_CONTENT = "rcRequestContent";
    // this key name is reserved for the response object.
    public final static String RC_RESPONSE_OBJECT = "rcResponseObject";

    /**
     * Available request acitons definition.  There are the same definitions in
     * the JavaScript class RequestObject.  Make sure to keep them identical. 
     */
    public static final int ACTION_CREATE_REQUEST = 100;
    public static final int ACTION_CREATE_RESPONSE = 110; // for test only.
    public static final int ACTION_CHECK_RESPONSE = 200;
    // the status.
    public static final int STATUS_CREATE_REQUEST_SUCCESS = 300;
    public static final int STATUS_CREATE_RESPONSE_SUCCESS = 310;  // for test only.
    public static final int STATUS_TXCOPY_ADOPTION_FAILURE = 320;
    public static final int STATUS_BUSINESS_RULE_FAILURE = 330;
    public static final int STATUS_CREATE_REQUEST_FAILURE = 400;
    public static final int STATUS_RESPONSE_TIMEOUT = 455;
    public static final int STATUS_RESPONSE_FAILURE = 500;
    public static final int STATUS_RESPONSE_SUCCESS = 600;
    public static final int STATUS_CHECKING_RESPONSE = 700;
    public static final int STATUS_SERIALIZE_FAILURE = 800;

    /**
     * we got to override Map's clone behaviour.
     */
    public Object clone();
}
