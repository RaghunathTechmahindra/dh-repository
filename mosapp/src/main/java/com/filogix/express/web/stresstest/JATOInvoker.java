package com.filogix.express.web.stresstest;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.PageHandlerCommon;
import mosApp.MosSystem.SessionStateModel;
import mosApp.MosSystem.SessionStateModelImpl;
import mosApp.MosSystem.UserActivityLogger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.iplanet.jato.RequestManager;

/*
 * Invoker class invoke stress test log info
 */

public class JATOInvoker {

	static boolean userActivityLogEnabled 
		= "Y".equals(PropertiesCache.getInstance().getInstanceProperty( 
		"com.filogix.express.userActivityLog", "N"));
    
    // The logger
    private final static Log _log = LogFactory.getLog(JATOInvoker.class);

    // the language id for current express session.
    protected int _languageId = 0;

    // the session state
    protected SessionStateModelImpl _sessionState;

    // the session user id.
    protected int _sessionUserId = 0;

    // the SessionResourceKit;
    protected SessionResourceKit _srk;

    protected PageHandlerCommon handler;

    /*
     * constructor
     */
    public JATOInvoker() {
        initInvoker();
    }

    /**
     * initialize Invoker
     */
    public void initInvoker() {
    	if(userActivityLogEnabled == false) return;
    	
        handler = new PageHandlerCommon();
        // initializing the handler.
        _log.info("Initializing the handler in stress test invoker...");
        handler.memberInit();
        handler.getSessionObjects();

        // setting up the sessionresourcekit.
        handler.setupSessionResourceKit("STRESS");
        // return the session resource kit.
        _srk = handler.getSessionResourceKit();
        _sessionState = handler.getTheSessionState();
        // setting up the language id.
        _languageId = handler.getTheSessionState().getLanguageId();

        // set up the session user id.
        _sessionUserId = handler.getTheSessionState().getSessionUserId();
        _log.info("Current Session user's id is: " + _sessionUserId);

    }

    /**
     * log current user or deal info
     * 
     * @param totalTime
     */
    public void handleDoFilterLog(String totalTime) {
    	
    	if(userActivityLogEnabled == false) return;
    	
        String defaultInstanceStateName = RequestManager.getRequestContext()
                .getModelManager().getDefaultModelInstanceName(
                        SessionStateModel.class);
        SessionStateModelImpl theSessionState = (SessionStateModelImpl) RequestManager
                .getRequestContext().getModelManager().getModel(
                        SessionStateModel.class, defaultInstanceStateName,
                        true, true);

        if (theSessionState.getUserInstitutionId() > -1) {
            SessionResourceKit srk = handler.getSessionResourceKit();

            // handler should be already initialized.
            PageEntry pg = (PageEntry) theSessionState.getCurrentPage();
            String pageName = pg.getPageName();

            String msg = "CurrentPage<"
                    + theSessionState.getPreviousPage()
                    + ">, "
                    + "ButtonName<"
                    + theSessionState.getInvokeButtonName()
                    + ">, "
                    + "RequestedPage<"
                    + pageName
                    + ">, "
                    + "PageDisplayLangId<"
                    + handler.getTheSessionState().getLanguageId()
                    + ">, exec: "
                    + totalTime
                    + "ms, "
                    + ", HttpSessionId=<"
                    + RequestManager.getRequestContext().getRequest()
                            .getSession().getId() + ">";

            UserProfile up = null;
            try {
                up = new UserProfile(srk);
                up.findByPrimaryKey(new UserProfileBeanPK(theSessionState
                        .getSessionUserId(), theSessionState
                        .getUserInstitutionId()));
                Deal deal = new Deal(srk, null);

                if (pg.getPageDealId() >= 0) {
                    if (pg.getPageDealCID() != -1) {
                        deal.setSilentMode(true); //FXP28578
                        deal.findByPrimaryKey(new DealPK(pg.getPageDealId(), pg
                                .getPageDealCID()));
                        UserActivityLogger.log(up, deal, msg, "ACTION");
                    }
                    // non-deal page case do not send deal info
                    if (pg.getPageDealCID() == -1)
                        UserActivityLogger.log(up, msg, "ACTION");
                }
            } catch (RemoteException re) {
                UserActivityLogger.log(up, re.getMessage(), "ACTION");
                _log.error("@MultiFormServlet:RemoteException: " + re);
            } catch (FinderException fe) {
                UserActivityLogger.log(up, fe.getMessage(), "ACTION");
                _log.error("@MultiFormServlet:FinderException: " + fe);
            } finally {
                _srk.freeResources();
                _srk = null;
            }
        }
    }

    /**
     * set button in sessionstate
     * 
     * @param name
     */
    public void setInvokeButtonName(String name) {
    	
    	if(userActivityLogEnabled == false) return;
    	
        _sessionState.setInvokeButtonName(name);
        _srk.freeResources();
        _srk = null;
    }

}
