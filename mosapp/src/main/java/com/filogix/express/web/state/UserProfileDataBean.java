package com.filogix.express.web.state;


public class UserProfileDataBean {

    private int userProfileId;
    private int userTypeId;
    private int groupProfileId;
    private String standardAccess;
    private String bilingual;
    private int secondApproverUserId;
    private int jointApproverUserId;
    private int contactId;
    private int profileStatusId;
    private int partnerUserId;
    private int standInUserId;
    private int managerId;
    private String upBusinessId;
    private String userLogin;
    private String taskAlert;
    private int institutionId;

    private String userTypeDescription;
    private int userTimeZoneId;
    private int userLanguageId;
    private String userFullName;

    public int getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(int userProfileId) {
        this.userProfileId = userProfileId;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public int getGroupProfileId() {
        return groupProfileId;
    }

    public void setGroupProfileId(int groupProfileId) {
        this.groupProfileId = groupProfileId;
    }

    public String getStandardAccess() {
        return standardAccess;
    }

    public void setStandardAccess(String standardAccess) {
        this.standardAccess = standardAccess;
    }

    public String getBilingual() {
        return bilingual;
    }

    public void setBilingual(String bilingual) {
        this.bilingual = bilingual;
    }

    public int getSecondApproverUserId() {
        return secondApproverUserId;
    }

    public void setSecondApproverUserId(int secondApproverUserId) {
        this.secondApproverUserId = secondApproverUserId;
    }

    public int getJointApproverUserId() {
        return jointApproverUserId;
    }

    public void setJointApproverUserId(int jointApproverUserId) {
        this.jointApproverUserId = jointApproverUserId;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public int getProfileStatusId() {
        return profileStatusId;
    }

    public void setProfileStatusId(int profileStatusId) {
        this.profileStatusId = profileStatusId;
    }

    public int getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(int partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public int getStandInUserId() {
        return standInUserId;
    }

    public void setStandInUserId(int standInUserId) {
        this.standInUserId = standInUserId;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public String getUpBusinessId() {
        return upBusinessId;
    }

    public void setUpBusinessId(String upBusinessId) {
        this.upBusinessId = upBusinessId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getTaskAlert() {
        return taskAlert;
    }

    public void setTaskAlert(String taskAlert) {
        this.taskAlert = taskAlert;
    }

    public int getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(int institutionId) {
        this.institutionId = institutionId;
    }

    public String getUserTypeDescription() {
        return userTypeDescription;
    }

    public void setUserTypeDescription(String userTypeDescription) {
        this.userTypeDescription = userTypeDescription;
    }

    public int getUserTimeZoneId() {
        return userTimeZoneId;
    }

    public void setUserTimeZoneId(int userTimeZoneId) {
        this.userTimeZoneId = userTimeZoneId;
    }

    public int getUserLanguageId() {
        return userLanguageId;
    }

    public void setUserLanguageId(int userLanguageId) {
        this.userLanguageId = userLanguageId;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }
}
