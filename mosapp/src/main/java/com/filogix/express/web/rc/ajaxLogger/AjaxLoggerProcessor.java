package com.filogix.express.web.rc.ajaxLogger;

import java.net.URLDecoder;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.PageHandlerCommon;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericRequestObject;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;

/**
 * ajax logger
 * 
 * this class write messages sent from javascript into the log file. this
 * feature is useful when you want to keep client side's event.
 * 
 * usage: code "ajaxLogger.debug(msg)", "ajaxLogger.error(msg)", or
 * "ajaxLogger.info(msg)" in your javascript file you can set any kinds of
 * object as message for those javascript's methods. this class just write all
 * message into the log file.
 * 
 */
public class AjaxLoggerProcessor extends AbstractRequestProcessorJato {

	private static Log _log = LogFactory.getLog(AjaxLoggerProcessor.class);

	public static int LOG_LEVEL_DEBUG = 0;
	public static int LOG_LEVEL_INFO = 1;
	public static int LOG_LEVEL_ERROR = 2;

	//dummy: this class doesn't use this method.
	public ResponseObject processRequest(RequestObject requestObj,
			ResponseObject responeObj) throws RequestProcessException {

		try {
			GenericRequestObject req = (GenericRequestObject)requestObj;

			int logLevel = NumberUtils.toInt((String)req.get("logLevel"), 2);
			
			if(logLevel == LOG_LEVEL_DEBUG && _log.isDebugEnabled() == false) return responeObj;
			if(logLevel == LOG_LEVEL_INFO && _log.isInfoEnabled() == false) return responeObj;
			if(logLevel == LOG_LEVEL_ERROR && _log.isErrorEnabled() == false) return responeObj;

			String message = (String)req.get("message");
			if(StringUtils.isNotEmpty(message)) message = URLDecoder.decode(message, "UTF-8");

			PageHandlerCommon handler = new PageHandlerCommon();
			initializeHandler(handler);

			StringBuilder sb = new StringBuilder();
			sb.append("[ajax-log] ").append("message:[").append(message)
			.append("], ").append("userLogin:").append(
					handler.getTheSessionState().getUserLogin()).append(",");
			PageEntry pe = handler.getTheSessionState().getCurrentPage();
			if(pe != null){
				sb.append("currPage:").append(pe.getPageId()).append("(").append(pe.getPageName()).append("),")
				.append("institution:").append(pe.getDealInstitutionId()).append(",")
				.append("dealId:").append(pe.getPageDealId()).append(",")
				.append("copyId:").append(pe.getPageDealCID());
			}

			if (logLevel == LOG_LEVEL_DEBUG){
				_log.debug(sb.toString());
			} else if (logLevel == LOG_LEVEL_INFO) {
				_log.info(sb.toString());
			} else {
				String stackTrace = (String)req.get("stackTrace");
				if(StringUtils.isNotEmpty(stackTrace))
					sb.append(", stackTrace:[").append(URLDecoder.decode(stackTrace, "UTF-8")).append("]");
				_log.error(sb.toString());
			}

		} catch (Throwable t) {
			_log.error("Exception @AjaxLoggerProcessor - ignored", t);
		}
		// this is one way service, client side should never receive the
		// response.
		return responeObj;

	}

}

