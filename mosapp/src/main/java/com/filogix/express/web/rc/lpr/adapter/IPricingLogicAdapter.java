package com.filogix.express.web.rc.lpr.adapter;

import com.basis100.mtgrates.IPricingLogic;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.IConverter;

/**
 * Title: IPricingLogicAdapter
 * <p>
 * Description: interface of adapter
 * 
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public interface IPricingLogicAdapter {

    /**
     * this is a controller of pricinglogic and converter. this method first
     * executes pricingLogic implementation class and extracts result data list.
     * and then, executes converter implementation class.
     * 
     * @param srk
     * @param request
     * @param response
     * @return response object
     */
    public LPRResponseObject process(SessionResourceKit srk,
            LPRRequestObject request, LPRResponseObject response);

    /**
     * this method returns object which implements IPricingLogic interface. 
     * @return object which implements IPricingLogic
     */
    public IPricingLogic getPricingLogic();

    /**
     * this method accepts object which implements IPricingLogic interface.
     * @param logic - object which implements IPricingLogic interface.
     */
    public void setPricingLogic(IPricingLogic logic);

    /**
     * this method returns object which implements IConverter interface. 
     * @return object which implements IConverter interface
     */
    public IConverter getConverter();

    /**
     * this method accepts object which implements IConverter interface.
     * @param converter - object which implements IConverter interface
     */
    public void setConverter(IConverter converter);
}
