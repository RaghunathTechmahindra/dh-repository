package com.filogix.express.web.rc.lpr.converter;

import java.util.Calendar;
import java.util.TimeZone;

import com.basis100.mtgrates.RateInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
/**
 * Title: PricingLogicConverterUtil
 * <p>
 * Description: Utility class for converter
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class PricingLogicConverterUtil {

    /**
     * this method prepends "0" 
     * to the parameter "in" untill the length 
     * of expression becomes the parameter "length"
     * 
     * @param in 
     * @param length 
     * @return edited in
     */
    public static String fit( String in , int length )
    {
        length = length - in.length ();
        for ( int i = 0 ; i< length ; i++ )
            in = "0" + in ;
        return in ;
    }

    
    /**
     * this method makes "Post Interest Rate"'s description from rateInfor.
     * 
     * @param rateInfo
     * @param srk
     * @return Post Interest Rate Description
     */
    public static String makePricingRateDetailedDescription(RateInfo rateInfo, SessionResourceKit srk){
        
        
        Calendar effectDate =  Calendar.getInstance () ;
        effectDate.setTime ( rateInfo.getEffectiveDate () );

        int languageId = srk.getLanguageId();
        int institutionId = srk.getExpressState().getDealInstitutionId();

        String discountDescription = "";
        if (rateInfo.getMaxDiscount() > 0) {
            discountDescription = BXResources.getGenericMsg(
                    "LPR_MAX_DISCOUNT_LABEL", languageId);
        } else {
            discountDescription = BXResources.getGenericMsg(
                    "LPR_CAPP_DIFF_LABEL", languageId);
        }

        StringBuffer sb = new StringBuffer();
        sb.append(rateInfo.getPostedRate () + "% | " + discountDescription + " | ");
        sb.append(rateInfo.getMaxDiscount () + "% ");
        sb.append(BXResources.getPickListDescription(institutionId, 
                "MONTHS", effectDate.get( Calendar.MONTH ) + 1, languageId) + " ");
        sb.append(fit ( effectDate.get ( Calendar.DAY_OF_MONTH ) + "" , 2 ) + " ");
        sb.append(effectDate.get ( Calendar.YEAR ) + " | ");
        sb.append(fit ( effectDate.get ( Calendar.HOUR_OF_DAY ) + "" , 2 )  + ":");
        sb.append(fit ( effectDate.get ( Calendar.MINUTE  )+ "" ,2 ) + " ");
        sb.append(effectDate.getTimeZone ().getDisplayName (false, TimeZone.SHORT));

        return sb.toString();
    }
}
