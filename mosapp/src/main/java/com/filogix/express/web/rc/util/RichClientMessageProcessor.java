/*
 * @(#)RichClientMessageProcessor.java    2006-5-9
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;

import com.filogix.express.web.util.StringHelper;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.GeneralRequestProcessorJato;

/**
 * RichClientMessageProcessor is extract the i18n messages from server side for
 * rich client components.
 *
 * @version   1.0 2006-5-9
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class RichClientMessageProcessor extends GeneralRequestProcessorJato {

    // The logger
    private final static Log _log =
        LogFactory.getLog(RichClientMessageProcessor.class);

    /**
     * Constructor function
     */
    public RichClientMessageProcessor() {
    }

    /**
     * process the request.
     */
    public ResponseObject processRequest(RequestObject reqObject,
                                         ResponseObject respObj)
        throws RequestProcessException {

        // initializing JATO context.
        initializeContext();

        // the language id.
        int languageId = getLanguageId();
        List messageKeys = (List) reqObject.get("msgKeys");
        List messages = new ArrayList();
        for (Iterator i = messageKeys.iterator(); i.hasNext(); ) {

            String key = (String) i.next();
            if (_log.isDebugEnabled())
                _log.debug("Getting i18n message for key: " + key);
            String value =
                StringHelper.
                string2HTMLString(BXResources.getRichClientMsg(key,
                                                               languageId));
            if (_log.isDebugEnabled())
                _log.debug("      Here is value for HTML: " + value);
            messages.add(value);
        }

        respObj.put("msgKeys", messageKeys);
        respObj.put("values", messages);
        return respObj;
    }
}
