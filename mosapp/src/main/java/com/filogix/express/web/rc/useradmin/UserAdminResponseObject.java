package com.filogix.express.web.rc.useradmin;

import com.filogix.express.web.rc.GenericResponseObject;

public class UserAdminResponseObject extends GenericResponseObject {

	public UserAdminResponseObject() {
	}

	public void setUserExist(boolean value) {
		put("userFound", value);
	}

    public void setClearBox(boolean value) {
        put("hardStop", value);
    }
}
