/*
 * @(#)AbstractReportProcessor.java    2006-5-3
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import java.util.Map;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.jato.GeneralRequestProcessorJato;

/**
 * AbstractReportProcessor is a helper class for AJAX processors who will
 * extract report from persistence layer.
 *
 * @version   1.0 2006-5-3
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractReportProcessor
    extends GeneralRequestProcessorJato {

    // The logger
    private static Log _log = LogFactory.getLog(AbstractReportProcessor.class);

    // the report extractor's class names.
    protected Map _extractors = new HashMap();

    /**
     * Constructor function
     */
    public AbstractReportProcessor() {
    }

    /**
     * set the extractors.
     */
    public void setExtractors(Map extractors) {

        _extractors = extractors;
    }

    /**
     * returns an instance of extractor corresponding to the given name.
     */
    public Object getExtractor(String extractorName)
        throws ExtractorNotFoundException {

        if (!_extractors.keySet().contains(extractorName)) {
            _log.error("No extractor named: " + extractorName);
            throw new ExtractorNotFoundException("Can not find extractor for ["
                                                 + extractorName + "].");
        }

        // get the class name.
        String className = (String) _extractors.get(extractorName);
        try {
            Object extractor = Class.forName(className).newInstance();
            return extractor;
        } catch (ClassNotFoundException cnfe) {
            _log.error("Class not found!", cnfe);
            throw new ExtractorNotFoundException("Can not find class!", cnfe);
        } catch (InstantiationException ie) {
            _log.error(ie);
            throw new ExtractorNotFoundException("Can not instantiat!", ie);
        } catch (IllegalAccessException iae) {
            _log.error(iae);
            throw new ExtractorNotFoundException("Illegal Access!", iae);
        }
    }
}
