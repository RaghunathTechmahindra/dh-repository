/*
 * @(#)RequestProcessor.java    2006-2-28
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import javax.servlet.http.HttpServletResponse;

/**
 * RequestProcessor defines the interface to process the service request in
 * Express system.
 *
 * @version   1.0 2006-2-28
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface RequestProcessor {

    /**
     * This method is entry point to process an AJAX request.  The request will
     * be specified as a String and the response will be return as a String
     * too.  The processor's implementation will take care of parsing the
     * request content to a Java object and serializing the response Java object
     * to a String.
     * <br/>
     * We are not throw out exception here.  We are going to handle the
     * exception and prepare the error message, send them back to client in the
     * response String.
     */
    public String processRequest(String request,
                                 HttpServletResponse httpResponse);

    /**
     * separate this method to make it more flexable!?
     */
    public ResponseObject processRequest(RequestObject requestObj,
                                         ResponseObject responeObj)
        throws RequestProcessException;

    /**
     * process request with the httpd response.
     */
    public ResponseObject processRequest(RequestObject requestObj,
                                         ResponseObject responeObj,
                                         HttpServletResponse httpResponse)
        throws RequestProcessException;
}
