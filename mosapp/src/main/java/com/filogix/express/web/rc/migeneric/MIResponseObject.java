package com.filogix.express.web.rc.migeneric;

import com.filogix.express.web.rc.GenericResponseObject;

public class MIResponseObject extends GenericResponseObject {

    private static final long serialVersionUID = 1L;

    public void setResponseStatus(int status) {
        put(RC_RESPONSE_STATUS, new Integer(status));
    }
    
    public void setResponseErrors(String errorMessage) {
        put(RC_RESPONSE_ERRORS, errorMessage);
    }
    
    
}
