package com.filogix.express.web.rc.mipendingresponse;

import mosApp.MosSystem.MorgageInsHandler;
import mosApp.MosSystem.PageEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.util.DBA;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;
import com.filogix.externallinks.services.mi.MIResponseProcessHelper;

public class PendingMIProcessor extends AbstractRequestProcessorJato {

    private final static Logger logger =
        LoggerFactory.getLogger(PendingMIProcessor.class);

    public ResponseObject processRequest(RequestObject requestObj,
        ResponseObject responeObj) throws RequestProcessException {

        //PendingMIRequestObject reqObj = (PendingMIRequestObject) requestObj;
        PendingMIResponseObject resObj = (PendingMIResponseObject) responeObj;

        MorgageInsHandler handler = new MorgageInsHandler();
        initializeHandler(handler);

        try {
            SessionResourceKit srk = handler.getSessionResourceKit();

            PageEntry pe = handler.getTheSessionState().getCurrentPage();
            Deal deal = DBA.getDeal(srk, pe.getPageDealId(), pe.getPageDealCID(), null);

            MIResponseProcessHelper.processPendingAsyncMIResponse(srk, deal);


            resObj.setResponseStatus(RequestObject.STATUS_RESPONSE_SUCCESS);

        } catch (Exception e) {
            logger.error("Exception in PendingMIProcessor", e);
            resObj.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
            resObj.setResponseErrors(e.getMessage());
        } finally {
            handler.unsetSessionResourceKit();
        }
        
        return resObj;
    }
}
