package com.filogix.express.web.rc.lpr.dao;

import java.util.List;

import com.basis100.resources.SessionResourceKit;

/**
 * Title: ProductTypeDao
 * <p>
 * Description: Data access object for ProductType 
 * @author MCM Impl Team.
 * @version 1.0 Aug 07, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public interface ProductTypeDao extends IPricingLogicDao {

    public int getProductTypeId();
    public void setProductTypeId(int productTypeId);
    
    public List<ProductTypeDao> findProductTypeListByProductId(SessionResourceKit srk, int productId) throws Exception;
}
