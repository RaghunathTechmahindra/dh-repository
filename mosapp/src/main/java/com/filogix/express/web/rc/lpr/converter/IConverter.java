package com.filogix.express.web.rc.lpr.converter;

import java.util.List;

import com.basis100.mtgrates.IPricingLogicDataBean;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: IConverter
 * <p>
 * Description: interface of Converter
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public interface IConverter<T extends IPricingLogicDataBean> {

    /**
     * this method takes pricinglogic's result list and convert it into String, 
     * and store the data into response object.
     * 
     * @param res
     * @param list
     * @param srk
     * @return same instance as parameter res.
     */
    public LPRResponseObject convert
        (LPRResponseObject res, List<T> list, SessionResourceKit srk);
}
