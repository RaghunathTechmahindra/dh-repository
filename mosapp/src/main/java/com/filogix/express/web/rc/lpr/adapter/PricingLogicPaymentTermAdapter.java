package com.filogix.express.web.rc.lpr.adapter;

import java.util.List;

import com.basis100.mtgrates.IPricingLogicPaymentTerm;
import com.basis100.mtgrates.PaymentTermInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: PricingLogicPaymentTermAdapter
 * <p>
 * Description: implementation of adapter for PaymentTerm
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 * @version 1.1 Aug 15, 2008 artf762702, added isUmbrella parameter to pricingLogic
 */
public class PricingLogicPaymentTermAdapter extends AbstractPricingLogicAdapter {

    /**
     * @see com.filogix.express.web.rc.lpr.adapter.IPricingLogicAdapter#process(com.basis100.resources.SessionResourceKit, com.filogix.express.web.rc.RequestObject, com.filogix.express.web.rc.ResponseObject)
     */
    public LPRResponseObject process(SessionResourceKit srk,
            LPRRequestObject req, LPRResponseObject res) {

        List<PaymentTermInfo> list = ((IPricingLogicPaymentTerm) pricingLogic)
                .getProductPaymentTerms(srk, req.getComponentTypeId(), req.isUmbrella());

        return this.converter.convert(res, list, srk);

    }

}
