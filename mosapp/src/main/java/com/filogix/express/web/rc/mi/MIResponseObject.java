package com.filogix.express.web.rc.mi;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericResponseObject;
import com.filogix.express.web.rc.avm.AVMResponseObject;

public class MIResponseObject extends GenericResponseObject{

//	 The logger
    private static Log log = LogFactory.getLog(MIResponseObject.class);

    /**
     * Constructor function
     */
    public MIResponseObject() {
    }

    /**
     * set the response status.
     */
    public void setResponseStatus(int status) {

        put(RC_RESPONSE_STATUS, new Integer(status));
    }

    /**
     * set the request id for the given 
     */
    public void setRequestId(int id) {

        put("requestId", new Integer(id));
    }

    /**
     * set the copy id for transaction management.
     */
    public void setCopyId(int id) {

        put("copyId", new Integer(id));
    }
    
    /**
     * set the copy id for transaction management.
     */
    public void setInstitutionProfileId(int id) {

        put("institutionProfileId", new Integer(id));
    }
    
    public void setMiResponse(String miResponse) {
    	put("miResponse", miResponse);
    }
    
    public void setMiPremiumAmount(double miPremiumAmount) {
    	put("miPremiumAmount", new Double(miPremiumAmount));
    }
    
    public void setMiStatusId(int id) {
    	put("miStatusId", new Integer(id));
    }
    
    public void setMiStatusMsg(String miStatusMsg) {
    	put("miStatusMsg", miStatusMsg);
    }
    
    public void setMiPolicyNumber(String miPolicyNumber) {
    	put("miPolicyNumber", miPolicyNumber);
    }
	
}
