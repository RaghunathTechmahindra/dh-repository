package com.filogix.express.web.rc.lpr.converter;

import java.util.List;

import com.basis100.mtgrates.RateInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: LegacyProductRateCodeConverter
 * <p>
 * Description: Legacy CSV like data converter for product rate code
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class LegacyProductRateCodeConverter extends AbstractLegacyCSVConverter implements
        IConverter<RateInfo> {

    /**
     * this method converts a result data list from PricingLigc into a legacy CSV like String
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<RateInfo> list, SessionResourceKit srk) {

        
        StringBuffer rateResult = new StringBuffer();
        if(list != null)
        for (RateInfo rateInfo : list) {
            
            StringBuffer sb = new StringBuffer();
            sb.append("'" + rateInfo.getRateCode () + "'").append(COLUMN_DELIMITER);
            sb.append("'" + rateInfo.getRateCode () + "'");
            sb.append(LINE_DELIMITER);
            
            rateResult.append(sb.toString());
        }
        
        res.setRateCodeString(rateResult.toString());
        return res;
    }

}
