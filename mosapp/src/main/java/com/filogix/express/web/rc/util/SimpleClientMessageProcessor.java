package com.filogix.express.web.rc.util;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.picklist.BXResources;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.GeneralRequestProcessorJato;

/**
 * SimpleClientMessageProcessor is almost the same as RichClientMessageProcessor
 * this class handles only one message at a time.
 * 
 * @version 1.0 Nov 14, 2011
 */
public class SimpleClientMessageProcessor extends GeneralRequestProcessorJato {

    private final static Logger logger = LoggerFactory.getLogger(SimpleClientMessageProcessor.class);

    /**
     * Constructor function
     */
    public SimpleClientMessageProcessor() {
    }

    /**
     * process the request.
     */
    @SuppressWarnings("unchecked")
    public ResponseObject processRequest(RequestObject reqObject, ResponseObject respObj) throws RequestProcessException {

        // initializing JATO context.
        initializeContext();

        int languageId = getLanguageId();

        String key = (String) reqObject.get("msgKey");
        String escape = (String) reqObject.get("escape");
        String value = BXResources.getRichClientMsg(key, languageId);
        
        if(StringUtils.isBlank(escape) || Boolean.valueOf(escape)){
        	value = StringEscapeUtils.escapeHtml(value);
        }

        logger.debug("message : key = " + key + " value = " + value);
        respObj.put("value", value);

        return respObj;
    }
}
