/*
 * @(#)GenericJSObjectDescriptor.java    2006-3-2
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * GenericJSObjectDescriptor the generic implementation for (@link
 * JSObjectDescriptor).
 *
 * @version   1.0 2006-3-2
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class GenericJSObjectDescriptor implements JSObjectDescriptor {

    // The logger
    private static Log _log = LogFactory.getLog(GenericJSObjectDescriptor.class);

    // the descriptor map.
    private Map _descriptor;

    /**
     * Constructor function
     */
    public GenericJSObjectDescriptor() {
    }

    /**
     * properties description.
     */
    public void setObjectProperties(Map desc) {

        _descriptor = desc;
    }

    /**
     * get properties description.
     */
    public Map getObjectProperties() {

        return _descriptor;
    }
}
