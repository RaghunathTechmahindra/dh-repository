package com.filogix.express.web.rc.mitypefilter;

import com.filogix.express.web.rc.GenericRequestObject;

public class MITypeFilterRequestObject extends GenericRequestObject {

    private static final long serialVersionUID = 1L;

    public int getMortgageInsurerId() {
        return (Integer) (get("mortgageInsurerId"));
    }
}
