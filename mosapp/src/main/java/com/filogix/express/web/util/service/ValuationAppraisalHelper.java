/*
 * @(#)ValuationAppraisalHelper.java    2006-7-7
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.util.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.picklist.BXResources;
import com.filogix.externallinks.framework.ServiceConst;

/**
 * ValuationAppraisalHelper is the helper class for appraisal request.
 *
 * @version   1.0 2006-7-7
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ValuationAppraisalHelper extends Helper4RequestWithContact {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ValuationAppraisalHelper.class);

    /**
     * Constructor function
     */
    public ValuationAppraisalHelper() {
    }

    /**
     * returns the formated appraisal status message as explained in the spec.
     *
     * TODO: DATE format.
     */
    public static String getAppraisalStatusMessage(Request requestEntity,
                                                   int institutionId,
                                                   int languageId) {

        _log.debug("Preparing appraisal status message...");
        StringBuffer message = new StringBuffer();

        String providerName = "";
        try {
            _log.debug("Trying to find the provider info...");
            int providerId =
                requestEntity.getServiceProduct().getServiceProviderId();
            providerName =
                BXResources.getPickListDescription(institutionId,
                                                   "SERVICEPROVIDER",
                                                   providerId, languageId);
        } catch (Exception e) {
            // no need throw out for now.
            _log.warn("Can not find provider Name:", e);
        }

        try {
            Collection responses = requestEntity.getResponses();
            if (responses.isEmpty()) {
                _log.debug("No response found! Use the request itself.");
                message.append("**\nMessage at [").
                    append(requestEntity.getStatusDate()).append("]\n").
                    append(requestEntity.getStatusMessage()).
                    append(" \n**End of Message**");
            } else {
                for (Iterator i = responses.iterator(); i.hasNext(); ) {

                    Response response = (Response) i.next();
                    message.append("**\nMessage received at [").
                        append(response.getStatusDate()).append("] from [").
                        append(providerName).append("]\n").
                        append(response.getStatusMessage()).
                        append(" \n**End of Message**\n");
                }
            }
        } catch (Exception e) {
            // swallow here for not break the main business logic.
            _log.warn("Swallow exception: ", e);
        }

        return message.toString();
    }

    public static String getAppraisalStatusMessage(SessionResourceKit srk,
                                                   int requestId, int copyId,
                                                   int languageId)
        throws Exception {

        Request requestEntity = new Request(srk, requestId, copyId);
        return getAppraisalStatusMessage(requestEntity,
                                         srk.getExpressState().getDealInstitutionId(),
                                         languageId);
    }

    /**
     * returns requestId and contactId for Appraisal.
     */
    public static Map getAppraisalRequests(SessionResourceKit srk,
                                       int dealId, int copyId, int propertyId) {

        Map request = new TreeMap();
        
        //populate optionlist for provider
        // prepare the query SQL.
        String querySQL =
            "SELECT DISTINCT " +
            "request.REQUESTDATE, request.REQUESTID, " +
            "contact.SERVICEREQUESTCONTACTID " +
            "FROM " +
            "REQUEST request, SERVICEREQUESTCONTACT contact, " +
            "SERVICEREQUEST servicereq, SERVICEPRODUCT prod " +
            "WHERE " +
            "contact.REQUESTID = request.REQUESTID AND " +
            "request.DEALID = " + dealId + " AND " +
            "request.COPYID = " + copyId + " AND " +
            "servicereq.REQUESTID = request.REQUESTID AND " +
            "request.SERVICEPRODUCTID = prod.SERVICEPRODUCTID AND " +
            "servicereq.PROPERTYID = " + propertyId +
            " AND prod.SERVICETYPEID = " + ServiceConst.SERVICE_TYPE_VALUATION + 
            " AND prod.SERVICESUBTYPEID = "+ 
            ServiceConst.SERVICE_SUB_TYPE_APPRAISAL +
            " ORDER BY request.REQUESTDATE DESC";
                    
        _log.info("Query SQL: " + querySQL);

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (; jExec.next(key);) {
                Integer requestId = 
                    new Integer(jExec.getInt(key, "REQUESTID"));
                Integer contactId = 
                    new Integer(jExec.getInt(key, "SERVICEREQUESTCONTACTID"));
                
                request.put(requestId, contactId);
                break;
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
        }
        return request;
    }
    
    /**
     * returns requestId and contactId for READ Appraisal.
     */
    public static Map getReadAppraisalRequests(SessionResourceKit srk,
                                       int dealId, int copyId, int propertyId) {

        Map request = new TreeMap();
        
        //populate optionlist for provider
        // prepare the query SQL.
        String querySQL =
            "SELECT DISTINCT " +
            "request.REQUESTDATE, request.REQUESTID, " +
            "contact.SERVICEREQUESTCONTACTID " +
            "FROM " +
            "REQUEST request, SERVICEREQUESTCONTACT contact, " +
            "SERVICEREQUEST servicereq, SERVICEPRODUCT prod " +
            "WHERE " +
            "contact.REQUESTID = request.REQUESTID AND " +
            "request.DEALID = " + dealId + " AND " +
            "request.COPYID = " + copyId + " AND " +
            "servicereq.REQUESTID = request.REQUESTID AND " +
            "request.SERVICEPRODUCTID = prod.SERVICEPRODUCTID AND " +
            "servicereq.PROPERTYID = " + propertyId +
            " AND prod.SERVICETYPEID = " + ServiceConst.SERVICE_TYPE_VALUATION + 
            " AND prod.SERVICESUBTYPEID = "+ 
            ServiceConst.SERVICE_SUB_TYPE_READ_APPRAISAL +
            " ORDER BY request.REQUESTDATE DESC";
                    
        _log.info("Query SQL: " + querySQL);

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (; jExec.next(key);) {
                Integer requestId = 
                    new Integer(jExec.getInt(key, "REQUESTID"));
                Integer contactId = 
                    new Integer(jExec.getInt(key, "SERVICEREQUESTCONTACTID"));
                
                request.put(requestId, contactId);
                break;
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
        }
        return request;
    }

    
}
