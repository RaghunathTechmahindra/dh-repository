package com.filogix.express.web.rc.mitypefilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.PageHandlerCommon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MortgageInsurer;
import com.basis100.deal.util.StringUtil;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;

/**
 * Processor for MI Type filtering. This Processor is triggered by MI Insurer
 * selection.
 * 
 * @author Hiro
 */
public class MITypeFilterProcessor extends AbstractRequestProcessorJato {

    private final static Logger logger = LoggerFactory
    .getLogger(MITypeFilterProcessor.class);

    public ResponseObject processRequest(RequestObject requestObj,
            ResponseObject responeObj) {

        logger.debug("MITypeFilterProcessor: start ---------");

        MITypeFilterRequestObject reqObj = (MITypeFilterRequestObject) requestObj;
        MITypeFilterResponseObject respObj = (MITypeFilterResponseObject) responeObj;

        PageHandlerCommon handler = new PageHandlerCommon();

        try {
            initializeHandler(handler);
        } catch (RuntimeException e) {
            logger.warn("MITypeFilterProcessor: Failed to initialize handler. "
                    + "Most likely this request is from abandoned screen. ", e);
            return respObj;
        }

        List<Map<String, Object>> myTypes = new ArrayList<Map<String, Object>>();

        try {
            SessionResourceKit srk = handler.getSessionResourceKit();

            // prepare all available mi type ids
            MortgageInsurer miInsurer = new MortgageInsurer(srk);
            List<Integer> miTypeIDList = miInsurer
            .findMITypesByMortgageInsurer(reqObj.getMortgageInsurerId());

            for (int miTypeid : miTypeIDList) {
                String miTypeName = BXResources.getPickListDescription(
                        getDealInstitutionId(), "MORTGAGEINSURANCETYPE",
                        miTypeid, getLanguageId());

                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put("miTypeId", miTypeid);
                entry.put("miTypeName", miTypeName);

                myTypes.add(entry);
            }
            respObj.setMITypes(myTypes);

            // prepare currently selected mi type id
            PageEntry pe = handler.getTheSessionState().getCurrentPage();
            Deal deal = new Deal(srk, null, pe.getPageDealId(),
                    pe.getPageDealCID());
            if (deal.getMortgageInsurerId() != Mc.MI_INSURER_BLANK
                    && deal.getMortgageInsurerId() == reqObj
                    .getMortgageInsurerId()) {

                respObj.setSelectedMITypeId(deal.getMITypeId());
            }
            
        } catch (Exception e) {
            logger.error("Exception @MITypeFilterProcessor - ignored");
            logger.error(StringUtil.stack2string(e));
        } finally {
            handler.unsetSessionResourceKit();
        }

        logger.debug("MITypeFilterProcessor: end ---------");
        return respObj;
    }

}
