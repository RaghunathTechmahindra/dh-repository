/*
 * @(#)AbstractRPFactory.java    2006-3-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.util.Map;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * AbstractRPFactory provides the generic behavours for any specific
 * RequestProcessorFactory implementation.
 *
 * @version   1.0 2006-3-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractRPFactory implements RequestProcessorFactory {

    // The logger
    private static Log _log = LogFactory.getLog(AbstractRPFactory.class);

    // this property will keep a map of (ServiceType, RequestProcessor
    // Implementations).  The values will be injected through SpringFramework.
    private Map _requestProcessors = new HashMap();

    /**
     * the setter method.  SpringFramework will use it to inject the values.
     */
    public void setRequestProcessors(Map processors) {

        _requestProcessors = processors;
    }

    /**
     * returns the RequestProcessor implementation class name based on the
     * specified service type. 
     */
    protected Object getRequestProcessorImpl(String requestType)
        throws RPFactoryException {

        if (_requestProcessors.keySet().contains(requestType)) {
            if (_log.isDebugEnabled())
                _log.debug("Implementation for [" + requestType + "] is: "
                           + _requestProcessors.get(requestType));
            AbstractRequestProcessor processor =
                (AbstractRequestProcessor) _requestProcessors.get(requestType);
            if (_log.isDebugEnabled())
                _log.debug("The result is: " + processor);
            return processor;
        } else {
            _log.error("Cannot find the implementation for request type: "
                       + requestType);
            throw new RPFactoryException("Request Type: [" + requestType +
                                         "] not supported!");
        }
    }
}
