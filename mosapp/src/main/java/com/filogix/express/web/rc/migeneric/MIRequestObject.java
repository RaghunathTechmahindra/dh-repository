package com.filogix.express.web.rc.migeneric;

import com.filogix.express.web.rc.GenericRequestObject;

public class MIRequestObject extends GenericRequestObject {

    private static final long serialVersionUID = 1L;

    public int getMIInsurerId() {
        return ((Integer) get("miinsurerid")).intValue();
    }
    
    public boolean isCancel(){
        return Boolean.parseBoolean((String) get("cancel"));
    }

}
