/*
 * @(#)Helper4RequestWithContact.java    2006-8-14
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.util.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.entity.Borrower;
import com.filogix.express.web.rc.request.ContactInfo4Request;

/**
 * Helper4RequestWithContact - 
 *
 * @version   1.0 2006-8-14
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class Helper4RequestWithContact extends ServiceRequestHelper {

    // The logger
    private final static Log _log =
        LogFactory.getLog(Helper4RequestWithContact.class);

    /**
     * Constructor function
     */
    public Helper4RequestWithContact() {
    }

    /**
     * update  the contact info based on the request.
     */
    public static ServiceRequestContact
        updateServiceRequestContact(ServiceRequestContact contact,
                                    ContactInfo4Request request) {

        contact.setFirstName(request.getFirstName());
        contact.setLastName(request.getLastName());
        contact.setEmailAddress(request.getEmailAddress());
        contact.setWorkAreaCode(request.getWorkPhoneArea());
        contact.setWorkPhoneNumber(request.getWorkPhoneNumber());
        contact.setWorkExtension(request.getWorkPhoneExt());
        contact.setCellAreaCode(request.getCellPhoneArea());
        contact.setCellPhoneNumber(request.getCellPhoneNumber());
        contact.setHomeAreaCode(request.getHomePhoneArea());
        contact.setHomePhoneNumber(request.getHomePhoneNumber());

        updateOptionalFields(contact, request);

        return contact;
    }

    /**
     * update the contact inf based on the primary borrower id.
     */
    public static ServiceRequestContact
        updateServiceRequestContact(ServiceRequestContact contact,
                                    Borrower borrower) {

        if(_log.isDebugEnabled())
            _log.debug("update contact record based on the borrower info");
        contact.setFirstName(borrower.getBorrowerFirstName());
        contact.setLastName(borrower.getBorrowerLastName());
        // email address.
        contact.setEmailAddress(borrower.getBorrowerEmailAddress());
        // work phone.
        String phoneNumber = borrower.getBorrowerWorkPhoneNumber();
        if ((phoneNumber != null) && phoneNumber.trim().length() > 0) {
            contact.setWorkAreaCode(phoneNumber.substring(0, 3));
            contact.setWorkPhoneNumber(phoneNumber.substring(3));
        } else {
            contact.setWorkAreaCode("");
            contact.setWorkPhoneNumber("");
        }
        contact.setWorkExtension(borrower.getBorrowerWorkPhoneExtension());
        // no cell phone number, make it empty.
        contact.setCellAreaCode("");
        contact.setCellPhoneNumber("");
        // home phone
        phoneNumber = borrower.getBorrowerHomePhoneNumber();
        if ((phoneNumber != null) && phoneNumber.trim().length() > 0) {
            contact.setHomeAreaCode(phoneNumber.substring(0, 3));
            contact.setHomePhoneNumber(phoneNumber.substring(3));
        } else {
            contact.setHomeAreaCode("");
            contact.setHomePhoneNumber("");
        }

        return contact;
    }

    /**
     * update the contact inf based on the primary borrower id.
     */
    public static ServiceRequestContact
        updateServiceRequestContact(ServiceRequestContact contact,
                                    Borrower borrower,
                                    ContactInfo4Request request) {

        ServiceRequestContact theContact =
            updateServiceRequestContact(contact, borrower);

        return updateOptionalFields(theContact, request);
    }

    /**
     * update optional fields.
     */
    private static ServiceRequestContact
        updateOptionalFields(ServiceRequestContact contact,
                             ContactInfo4Request request) {

        // not every body has the comments and propety owner name.
        if (request.getComments() != null) {
            contact.setComments(request.getComments());
        }
        if (request.getPropertyOwnerName() != null) {
            contact.setPropertyOwnerName(request.getPropertyOwnerName());
        }

        return contact;
    }
}
