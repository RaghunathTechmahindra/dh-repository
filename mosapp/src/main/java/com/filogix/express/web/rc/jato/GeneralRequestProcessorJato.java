/*
 * @(#)GeneralRequestProcessorJato.java    2006-5-9
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.jato;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mosApp.MosSystem.PageHandlerCommon;

/**
 * GeneralRequestProcessorJato is a request processor to talk to JATO world
 * generally.
 *
 * @version   1.0 2006-5-9
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class GeneralRequestProcessorJato
    extends AbstractRequestProcessorJato {

    // The logger
    private final static Log _log =
        LogFactory.getLog(GeneralRequestProcessorJato.class);

    /**
     * Constructor function
     */
    public GeneralRequestProcessorJato() {
    }

    /**
     * initializing JATO's request context, we don't need a handler here.
     */
    protected void initializeContext() {

        PageHandlerCommon handler = new PageHandlerCommon();
        initializeHandler(handler);
    }
}
