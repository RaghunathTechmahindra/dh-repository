/*
 * @(#)LPRResponseObject.java Nov 28, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.web.rc.lpr;

import java.util.List;
import java.util.Map;

import com.filogix.express.web.rc.GenericResponseObject;

/**
 * LPRResponseObject
 * 
 * @version 1.0 Nov 28, 2007
 * @author Bzhang, Hiro
 * 
 * @version 1.1 Aug 15, 2008 artf762702, added componentType
 */
public class LPRResponseObject extends GenericResponseObject {


	public LPRResponseObject() {
	}

	public void setLendersString(String lendersString) {
		put("lendersString", lendersString);
	}

	public void setProductString(String productString) {
		put("productString", productString);
	}

	public void setRateCodeString(String rateCodeString) {
		put("rateCodeString", rateCodeString);
	}

	public void setLenderProductRateString(String lenderProductRateString) {
		put("lenderProductRateString", lenderProductRateString);
	}

	public void setPaymentTermString(String paymentTermString) {
		put("paymentTermString", paymentTermString);
	}

	//MCM
    public void setLenders(List<Map<String, Object>> lenders) {
        put("lenders", lenders);
    }
    
    public void setProducts(List<Map<String, Object>> products) {
        put("products", products);
    }

    public void setRates(List<Map<String, Object>> rates) {
        put("rates", rates);
    }

    public void setPaymentTerms(List<Map<String, Object>> paymentTerms) {
        put("paymentTerms", paymentTerms);
    }

    public void setRepaymentTypes(List<Map<String, Object>> repaymentTypes) {
        put("repaymentTypes", repaymentTypes);
    }
    
    public void setComponentEligibleFlag(Map<String, Object> compEligibleChk){
        put("componentEligibleFlag", compEligibleChk);
    }
    
    public void setCountComponents(Map<String, Object> compEligibleChk){
        put("countComponents", compEligibleChk);
    }
    
    public void setProductType(List<Map<String, Object>> productType){
        put("productType", productType);
    }
    
    public void setRateCode(Map<String, Object> rateCode){
        put("rateCode", rateCode);
    }
    
    public void setComponentType(Map<String, Object> componentType){
        put("componentType", componentType);
    }
    
	public void setQualifyProductString(String productString) {
		put("qualifyProductString", productString);
	}

	public void setQualifyRateString(String rateString) {
		put("qualifyProductRateString", rateString);
	}

}
