package com.filogix.express.web.util.service;

import java.text.SimpleDateFormat;
import java.util.*;

import MosSystem.Mc;

import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
//import com.basis100.mtgrates.PricingLogic2;
import com.basis100.deal.entity.Deal;
//import com.basis100.deal.entity.Request;
//import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.picklist.BXResources;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.util.Xc;
//import com.filogix.externallinks.framework.ServiceConst;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class QualifyingRateHelper extends ServiceRequestHelper {

	//logger
    private final static Log _log =
        LogFactory.getLog(QualifyingRateHelper.class);
	
    public static Map getValidQualifyingProducts(SessionResourceKit srk,
            int languageId, Deal deal) 
    {
		
		Map products = new TreeMap();
		Date applicationDate = null;
        Date preApprovalDate = null;
        if (deal != null) {
            try {
              
                applicationDate = deal.getApplicationDate();
                
                /***** GR 3.2.2 Firm-up PreApproval change starts *****/
                preApprovalDate = getPreApprovalDate(deal, applicationDate, srk);
                /***** GR 3.2.2 Firm-up PreApproval change endd *****/

            } catch (Exception ex) {
                _log.warn(ex);
                _log.warn(StringUtil.stack2string(ex));
            }
        }
        if (applicationDate == null)
         	applicationDate = new Date();

        if (preApprovalDate == null)
         	preApprovalDate = new Date();
		try {
	        Date conditionDate = getConditionDate(srk, preApprovalDate);
	
	        String sql = "SELECT mtg.mtgProdId, mtg.repaymenttypeid, p.ratecode "
	            + " FROM PRICINGPROFILE P, PRICINGRATEINVENTORY I, mtgprod mtg, mtgprodlenderinvestorassoc pass "
	            + " WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID "
	            + " AND mtg.MtgProdId = pass.MtgProdId "
	            + " AND mtg.pricingprofileid=i.pricingprofileid "
	            + " AND mtg.pricingprofileid=p.pricingprofileid "
	            + " AND (I.INDEXEXPIRYDATE >= "
	            + sqlStringFromDate((java.util.Date) conditionDate)
	            + " OR I.INDEXEXPIRYDATE IS NULL) AND I.INDEXEFFECTIVEDATE <= sysdate"
	            + " AND (P.RATEDISABLEDDATE >= "
	            + sqlStringFromDate((java.util.Date) applicationDate)
	            + " OR P.RATEDISABLEDDATE IS NULL)"
	            + " AND pass.lenderProfileId = "
	            + deal.getLenderProfileId()
	            + " AND mtg.institutionProfileid = pass.institutionProfileid "
	            + " AND mtg.institutionProfileid = i.institutionProfileid "
	            + " AND mtg.institutionProfileid = p.institutionProfileid "
	            + " AND mtg.QualifyingRateEligibleFlag = 'Y'";
	
	
	        //sql += " group by mtg.MtgProdName, mtg.MtgProdId, pass.lenderProfileId, p.ratecode ";
	        sql += " group by mtg.mtgProdId, mtg.repaymenttypeid, p.ratecode ";
	        sql += " order by p.ratecode desc";
			
	
			JdbcExecutor jExec = srk.getJdbcExecutor();
			int key = jExec.execute(sql);
			for (; jExec.next(key); ) {
			
			Integer id = new Integer(jExec.getInt(key, 1));
			// get the product description from the BXResources based on the
			// language id.
			String prodName = BXResources.
			getPickListDescription(srk.getExpressState().getDealInstitutionId(),
			                "MTGPROD", id.toString(),
			                languageId);
			products.put(id, prodName);
		}
		jExec.closeData(key);
		} catch (Exception e) {
		_log.error("JDBC Executor ERROR: ", e);
		// TODO ....
		}
		
		return products;
    }

    public static Map getValidQualifyingProductsDealEntry(SessionResourceKit srk,
            int languageId, String lenderId) 
    {
		
		Map products = new TreeMap();
		
		try {
	
	        String sql = "SELECT mtg.mtgProdId, mtg.repaymenttypeid, p.ratecode "
	            + " FROM PRICINGPROFILE P, PRICINGRATEINVENTORY I, mtgprod mtg, mtgprodlenderinvestorassoc pass "
	            + " WHERE P.PRICINGPROFILEID = I.PRICINGPROFILEID "
	            + " AND mtg.MtgProdId = pass.MtgProdId "
	            + " AND mtg.pricingprofileid=i.pricingprofileid "
	            + " AND mtg.pricingprofileid=p.pricingprofileid "
	            + " AND (I.INDEXEXPIRYDATE >= sysdate"
	            + " OR I.INDEXEXPIRYDATE IS NULL) AND I.INDEXEFFECTIVEDATE <= sysdate"
	            + " AND (P.RATEDISABLEDDATE >= sysdate"
	            + " OR P.RATEDISABLEDDATE IS NULL)"
	            + " AND pass.lenderProfileId = "
	            + lenderId
	            + " AND mtg.institutionProfileid = pass.institutionProfileid "
	            + " AND mtg.institutionProfileid = i.institutionProfileid "
	            + " AND mtg.institutionProfileid = p.institutionProfileid "
	            + " AND mtg.QualifyingRateEligibleFlag = 'Y'";
	
	
	        //sql += " group by mtg.MtgProdName, mtg.MtgProdId, pass.lenderProfileId, p.ratecode ";
	        sql += " group by mtg.mtgProdId, mtg.repaymenttypeid, p.ratecode ";
	        sql += " order by p.ratecode desc";
			
	
			JdbcExecutor jExec = srk.getJdbcExecutor();
			int key = jExec.execute(sql);
			for (; jExec.next(key); ) {
			
			Integer id = new Integer(jExec.getInt(key, 1));
			// get the product description from the BXResources based on the
			// language id.
			String prodName = BXResources.
			getPickListDescription(srk.getExpressState().getDealInstitutionId(),
			                "MTGPROD", id.toString(),
			                languageId);
			products.put(id, prodName);
		}
		jExec.closeData(key);
		} catch (Exception e) {
		_log.error("JDBC Executor ERROR: ", e);
		// TODO ....
		}
		
		return products;
    }
    
    private static String sqlStringFromDate(java.util.Date d) throws Exception {
        String dateString = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            dateString = " to_date('" + df.format(d) + "','yyyy-mm-dd') ";
        } catch (Exception e) {
            throw new Exception(
                    "Exception @PricingLogic.sqlStringFromDate: constructing sql string from date."
                    + e.getMessage());
        }

        return dateString;
    }
    
    
    //borrowed methods from PricingLogic2...
    
    private static Date getPreApprovalDate(Deal deal, Date applicationDate, SessionResourceKit srk) throws Exception {

        boolean mosproperty = false;
        
        /***** FXP24050 fix start ******/
        if (deal == null) {
            int dealId = srk.getExpressState().getDealId();
            int copyId = srk.getExpressState().getDealCopyId();
            deal = new Deal(srk, null).findByPrimaryKey(new DealPK(dealId, copyId));
        }                 
        /***** FXP24050 fix end   ******/
        
        String property = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),
                "deal.allow.preapproval.rate.on.firmup.preapproval", "N");
        if (property.equalsIgnoreCase("Y"))
            mosproperty = true;

        Date preApprovalDate = applicationDate;
        if (deal != null && deal.getPreApproval()!=null
        		&& deal.getPreApproval().equalsIgnoreCase("Y")
                && deal.getSpecialFeatureId() != Mc.SPECIAL_FEATURE_PRE_APPROVAL) {
            if (mosproperty) {
                deal = deal.findLastPreApproval();
                preApprovalDate = deal.getApplicationDate();
            }
        }
        return preApprovalDate;
    }

    private static Date getConditionDate(SessionResourceKit srk, Date applicationDate)
    throws Exception {
        JdbcExecutor jExec = srk.getJdbcExecutor();

        String sql = "SELECT REBACKDATELIMIT FROM INSTITUTIONPROFILE";

        int key = jExec.execute(sql);

        int reBackDateLimit = -999;

        if (jExec.next(key))
            reBackDateLimit = jExec.getInt(key, 1);

        jExec.closeData(key);

        if (reBackDateLimit == -999)
            throw new Exception("Error: No Institution");

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(applicationDate);
        calendar.add(Calendar.DATE, reBackDateLimit * (-1));
        Date conditionDate = calendar.getTime();

        return conditionDate;
    }

    /**
     * returns requestId and contactId for Appraisal.
     */
    public static Map getDefaultQualify(SessionResourceKit srk, Deal deal) {

        Map defaultQualify = new TreeMap();
        final PropertiesCache pc = PropertiesCache.getInstance();
        String theRateCodeToUse = "";
        final double ltv = deal.getCombinedLTV();
        final double ltvThreshold = Double.parseDouble(pc.getProperty(deal.getInstitutionProfileId(), Xc.ING_LTV_THRESHOLD, "80"));
        boolean allowOverrideRate = pc.getProperty(deal.getInstitutionProfileId(), "com.filogix.qualrateoverride", "Y").equalsIgnoreCase("Y");
        final int miIndId = deal.getMIIndicatorId();

            if(ltv > ltvThreshold 
                    && (miIndId == Xc.MII_REQUIRED_STD_GUIDELINES 
                            || miIndId == Xc.MII_UW_REQUIRED 
                            || miIndId == Xc.MII_MI_PRE_QUALIFICATION))
                theRateCodeToUse = "com.basis100.calc.gdstds3yratecode";
            else
                theRateCodeToUse = "com.basis100.calc.gdstds.cmhclowratioratecode";
        
        theRateCodeToUse =  pc.getProperty(deal.getInstitutionProfileId(), theRateCodeToUse, "00");
        
        // prepare the query SQL.
        String querySQL =
            "SELECT " +
            "m.MTGPRODID, pri.INTERNALRATEPERCENTAGE, m.institutionprofileid " +
            "FROM " +
            "mtgprod m, pricingprofile pp, pricingrateinventory pri " +
            "WHERE " +
            "m.pricingprofileid = pp.pricingprofileid and m.institutionprofileid = pp.institutionprofileid AND " +
            "pp.pricingprofileid = pri.pricingprofileid and pp.institutionprofileid = pri.institutionprofileid AND " +
            "pri.indexexpirydate is null AND " +
            "pp.ratecode = '" + theRateCodeToUse + "' " +
            " ORDER BY pri.indexeffectivedate desc";
                    
        _log.info("Query SQL: " + querySQL);

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (; jExec.next(key);) {
                Integer prodcutId = 
                    new Integer(jExec.getInt(key, "MTGPRODID"));
                Integer qualifyRate = 
                    new Integer(jExec.getInt(key, "INTERNALRATEPERCENTAGE"));
                
                defaultQualify.put(prodcutId, qualifyRate);
                break;
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
        }
        return defaultQualify;
    }
    
}
