package com.filogix.express.web.rc.lpr.dao;

import com.basis100.mtgrates.IPricingLogicDataBean;

/**
 * Title: ComponentEligibleCheckerImpl
 * <p>
 * Description: Data access object for Component Eligible check 
 * @author MCM Impl Team.
 * @version 1.0 Aug 07, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public interface IPricingLogicDao extends IPricingLogicDataBean {

}
