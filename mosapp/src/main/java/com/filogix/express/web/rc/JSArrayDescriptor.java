/*
 * @(#)JSArrayDescriptor.java    2006-3-2
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

/**
 * JSArrayDescriptor is the descriptor for a JavaScript Array object.
 *
 * @version   1.0 2006-3-2
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface JSArrayDescriptor {

    /**
     * set the each array element's type.
     */
    public void setElementType(Object type);

    /**
     * get the element type for the array.
     */
    public Object getElementType();
}
