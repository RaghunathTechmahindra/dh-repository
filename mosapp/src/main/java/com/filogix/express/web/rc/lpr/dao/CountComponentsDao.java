package com.filogix.express.web.rc.lpr.dao;

import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;

/**
 * Title: ComponentEligibeChecker
 * <p>
 * Description: Data access object for Component Eligible check 
 * @author MCM Impl Team.
 * @version 1.0 Aug 07, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public interface CountComponentsDao extends IPricingLogicDao {

    public int getComponentSize();

    public void load(SessionResourceKit srk, LPRRequestObject request) throws Exception ;
}
