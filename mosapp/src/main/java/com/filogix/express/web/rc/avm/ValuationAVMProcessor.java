/*
 * @(#)ValuationAVMProcessor.java    2006-3-28
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.avm;

import java.util.Date;
import java.util.Calendar;
import java.util.Vector;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iplanet.jato.util.HtmlUtil;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.entity.CreateException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.AvmSummary;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.AppraisalHandler;
import mosApp.MosSystem.PageHandlerCommon;

import com.filogix.externallinks.framework.ServiceExecutor;
import com.filogix.externallinks.framework.ServiceConst;

import com.filogix.express.web.util.service.ValuationAVMHelper;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;

/**
 * ValuationAVMProcessor is a RequestProcessor implementation for handling AVM
 * request on the Web application side.
 *
 * @version   1.0 2006-3-28
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @see RequestProcessor
 */
public class ValuationAVMProcessor extends AbstractRequestProcessorJato {

    // The logger
    private static Log _log = LogFactory.getLog(ValuationAVMProcessor.class);

    // the handler from JATO.
    AppraisalHandler _handler = new AppraisalHandler();

    /**
     * Constructor function
     */
    public ValuationAVMProcessor() {
    }

    /**
     * process the request.
     */
    public ResponseObject processRequest(RequestObject request,
                                         ResponseObject response)
        throws RequestProcessException {

        AVMRequestObject avmRequestObj = (AVMRequestObject) request;
        // get an instance of response object.
        AVMResponseObject avmRespObject = (AVMResponseObject) response;

        // initializing the handler first.
        AppraisalHandler handler= _handler.cloneSS();
        initializeHandler(handler);

        // the purpose of this requst.
        // for each purpose.
        switch (avmRequestObj.getRequestAction()) {
        case RequestObject.ACTION_CREATE_REQUEST:
            avmRespObject = createRequestRecord(handler, avmRequestObj,
                                                avmRespObject);
            break;
        case RequestObject.ACTION_CHECK_RESPONSE:
            avmRespObject = checkResponse(handler, avmRequestObj,
                                          avmRespObject);
            break;
        }

        return avmRespObject;
    }

    /**
     * create a new request. and return the request id to client.
     */
    private AVMResponseObject createRequestRecord(AppraisalHandler handler,
                                                  AVMRequestObject request,
                                                  AVMResponseObject response)
        throws RequestProcessException {

        SessionResourceKit srk = getSessionResourceKit();
        try {
            // create request entities first.
            Request requestEntity = createEntities(request, srk);

            // adopt the transaction copy first.
            // TODO: Move this logic to a utility class to make is reusable.
            // get the adopted copy id.
            int newTxCopyId = adoptTransactionCopy(handler);
            createEntities4SourceCopy(request, requestEntity, srk);
            // set the new transaction copy for update the screen. need update
            // the copy id in the page no matter what happen!
            response.setCopyId(newTxCopyId);

            // setup the request id in the response object.
            response.setRequestId(requestEntity.getRequestId());
            // update the request date.
            String strRequestDate =
            HtmlUtil.format(requestEntity.getStatusDate(),
                            HtmlUtil.DATETIME_FORMAT_TYPE,
                            "MMM dd yyyy",
                            new Locale(BXResources.
                                       getLocaleCode(getLanguageId()),
                                       "")
                            );
            response.setAvmDate(strRequestDate);
            // then the request status.
            String status = BXResources.
                getPickListDescription(getDealInstitutionId(), "REQUESTSTATUS",
                                       requestEntity.getRequestStatusId(),
                                       getLanguageId());
            response.setAvmStatus(status);

            PassiveMessage pm = validateBusinessRules(handler, requestEntity.getRequestId(),
                                                      request.getPropertyId());
            if (pm != null) {
                _log.info("Got Business Rule validation errors!");
                response.setResponseStatus(RequestObject.
                                           STATUS_BUSINESS_RULE_FAILURE);
                // prepare the error message and send it back to JavaScript
                // client.
                response.put(ResponseObject.RC_RESPONSE_ERRORS,
                             preparePassiveMessage(pm));
            } else {
                // everyting looks fine.
                // set the response status.
                response.setResponseStatus(RequestObject.
                                           STATUS_CREATE_REQUEST_SUCCESS);
    
                _log.info("sending out the request to Datx for Request: " +
                          requestEntity.getRequestId());
                ServiceExecutor executor = new ServiceExecutor(srk);
                int result =
                    executor.processSyncRequest(requestEntity.getRequestId(),
                                                request.getCopyId(),
                                                getLanguageId());
                _log.info("Got the response id: " + result);
                // handle the result from external service.
                response = handleExtServiceResult(requestEntity, response,
                                                  getLanguageId(), result);
            }
        } catch (Exception e) {
            _log.error("JDBC Executor Error: ", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        } finally {
            srk.freeResources();
        }

        _log.info("Response Object: " + response.toString());
        return response;
    }

    /**
     * create AVM entities based on the request object.
     */
    private Request createEntities(AVMRequestObject request,
                                   SessionResourceKit srk)
        throws RequestProcessException {

        try {
            _log.info("Creating entities for AVM request...");
            // creating request entity.
            Request requestEntity = createRequestEntity(request, srk);

            // creating servicerequest entity.
            createServiceRequestEntity(request, requestEntity, srk);

            _log.info("Successfully created entities!");
            return requestEntity;
        } catch (CreateException ce) {
            _log.error("Can not create entities!", ce);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            _log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (FinderException fe) {
            _log.error("Can not create entities!", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, fe);
        }
    }

    /**
     * create entities for source scenario copy.
     */
    private Request createEntities4SourceCopy(AVMRequestObject request,
                                              Request requestEntity,
                                              SessionResourceKit srk)
        throws RequestProcessException {

        if (_log.isDebugEnabled())
            _log.debug("Creating request entity tree for source " +
                       "scenario copy ...");
        try {
            int sourceCopyId = getDealEditControl().getSourceCID();
            if (sourceCopyId == requestEntity.getCopyId()) {
                _log.info("We are working on the source scenario copy " +
                          "right now!");
                // no need to update, just return.
                return null;
            }

            _log.info("We are NOT working on the source scenario copy!");
            _log.info("Creating request entities for source scenario copy ...");
            Request requestEntity4Source = createRequestEntity(request, srk);
            // revise the request id and copy id.
            requestEntity4Source.setRequestId(requestEntity.getRequestId());
            requestEntity4Source.setCopyId(sourceCopyId);
            requestEntity4Source.ejbStore();
            // creating service request.
            createServiceRequestEntity(request, requestEntity4Source, srk);

            _log.info("Successfully created the request for source " +
                      "scenario copy");
            return requestEntity4Source;
        } catch (CreateException ce) {
            _log.error("Can not create entities!", ce);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            _log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (FinderException fe) {
            _log.error("Can not create entities!", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, fe);
        } catch (Exception e) {
            _log.error("Can not create entities!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
    }

    /**
     * create request entity.
     */
    private Request createRequestEntity(AVMRequestObject request,
                                        SessionResourceKit srk)
        throws CreateException, RemoteException, FinderException {

        _log.info("creating request for deal: [dealid=" +
                  request.getDealId() +
                  ", copyid=" + request.getCopyId() + "]...");
        Request requestEntity = new Request(srk);
        requestEntity.create(new DealPK(request.getDealId(), request.getCopyId()),
                             ServiceConst.REQUEST_STATUS_NOT_SUBMITTED,
                             new Date(), getSessionUserId());
        if (_log.isDebugEnabled())
            _log.debug("Request Entity Created: " +
                       requestEntity.toString());

        // preparing the channel info for the avm request.
        Channel channel = new Channel(srk);
        channel = channel.
            findByServiceProductAndTransactionType(request.getProductId(),
                                                   ServiceConst.
                                                   SERVICE_TRANSACTION_TYPE_REQUEST);
        // updating the request entity.
        requestEntity.setServiceProductId(request.getProductId());
        requestEntity.
            setServiceTransactionTypeId(ServiceConst.
                                        SERVICE_TRANSACTION_TYPE_REQUEST);
        requestEntity.setPayloadTypeId(ServiceConst.
                                       SERVICE_PAYLOAD_TYPE_SRVC_REQUEST);
        requestEntity.setRequestDate(new Date());
        requestEntity.setChannelId(channel.getChannelId());
        requestEntity.ejbStore();
        if (_log.isDebugEnabled())
            _log.debug("Request Entity created: " + requestEntity.toString());

        return requestEntity;
    }

    /**
     * create service request entity.
     */
    private ServiceRequest createServiceRequestEntity(AVMRequestObject request,
                                                      Request requestEntity,
                                                      SessionResourceKit srk)
        throws CreateException, RemoteException {

        _log.info("creating servicerequest for request: " +
                  requestEntity.getRequestId());
        ServiceRequest serviceRequest = new ServiceRequest(srk);
        serviceRequest = serviceRequest.create(requestEntity.getRequestId(),
                                               requestEntity.getCopyId());
        // update other info.
        serviceRequest.setPropertyId(request.getPropertyId());
        serviceRequest.ejbStore();
        if (_log.isDebugEnabled())
            _log.debug("ServiceRequest Created: " +
                       serviceRequest.toString());
        return serviceRequest;
    }

    /**
     * trigger business rule engine and return the passive message aboject.
     */
    private PassiveMessage validateBusinessRules(AppraisalHandler handler,
                                                 int requestId, 
                                                 int propertyId)
        throws Exception {

        // create a Business Rule Executor.
        BusinessRuleExecutor brExecutor = new BusinessRuleExecutor();
        brExecutor.setCurrentProperty(propertyId);
        brExecutor.setCurrentRequest(requestId);
        brExecutor.setCurrentBorrower(-1);

        // the business rule prefixes
        Vector rulePrefixes = new Vector();
        rulePrefixes.add("AVR-%");
        PassiveMessage passiveMessage =
            brExecutor.BREValidator(handler.getTheSessionState(),
                                    handler.
                                    getTheSessionState().getCurrentPage(),
                                    handler.getSessionResourceKit(),
                                    null,
                                    rulePrefixes);

        return passiveMessage;
    }

    /**
     * handle the external service result
     */
    private AVMResponseObject handleExtServiceResult(Request requestEntity,
                                                     AVMResponseObject response,
                                                     int languageId,
                                                     int responseId)
        throws RequestProcessException {

        try {
            // reload the request entity for checking the status!
            requestEntity.
                findByPrimaryKey(new RequestPK(requestEntity.getRequestId(),
                                               requestEntity.getCopyId()));
        } catch (FinderException fe) {
            _log.error("Can not find the request [requestId=" +
                       requestEntity.getRequestId() + ", copyId=" +
                       requestEntity.getCopyId() + "]", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, fe);
        }

        // 
        int requestStatusId = requestEntity.getRequestStatusId();
        String statusMsg = requestEntity.getStatusMessage();
        switch (requestStatusId) {
        case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
        case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
        case ServiceConst.REQUEST_STATUS_FAILED:
            _log.info("handling the failure response!");
            // prepare the error message.
            Object[] params = {statusMsg};
            String errorMsg =
                BXResources.getRichClientMsg("SERVICE_AVM_ERROR_PROMPT",
                                             languageId, params);
            // setting up the response object.
            response.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
            response.put(ResponseObject.RC_RESPONSE_ERRORS, errorMsg);
            break;
        default:
            _log.info("We got the response successfully!");
            try {
                _log.info("And have to set the scenario recommended flag!");
                forceAdoptScenarioRecommended(requestEntity.getDealId(),
                                              getDealEditControl(),
                                              requestEntity.getCopyId(),
                                              getSessionResourceKit());
                // the service request.
                ServiceRequest serviceRequest =
                    new ServiceRequest(getSessionResourceKit(),
                                       requestEntity.getRequestId(),
                                       requestEntity.getCopyId());
                // the the avmsummary.
                AvmSummary avmSummary =
                    new AvmSummary(getSessionResourceKit(), responseId);
                // also need set up avm value, and date.
                //valuation month, day, year.
                response.setAvmValue(avmSummary.getAvmValue());
                Calendar valuationDate = Calendar.getInstance();
                if (avmSummary.getValuationDate() == null) {
                    valuationDate.setTime(requestEntity.getRequestDate());
                } else {
                    valuationDate.setTime(avmSummary.getValuationDate());
                }
                if (_log.isDebugEnabled())
                    _log.debug("Set the actual appraisal date: " +
                               valuationDate.toString());
                response.setValuationDay(valuationDate.
                                         get(Calendar.DAY_OF_MONTH));
                response.setValuationMonth(valuationDate.get(Calendar.MONTH));
                response.setValuationYear(valuationDate.get(Calendar.YEAR));
            } catch (FinderException fe) {
                _log.error("Can not find the request [requestId=" +
                           requestEntity.getRequestId() + ", copyId=" +
                           requestEntity.getCopyId() + "]", fe);
                String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                          getLanguageId());
                throw new RequestProcessException(msg, fe);
            } catch (RemoteException re) {
                _log.error("JDBC exception!", re);
                String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                          getLanguageId());
                throw new RequestProcessException(msg, re);
            } catch (Exception e) {
                _log.error("All other errors!", e);
                String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                          getLanguageId());
                throw new RequestProcessException(msg, e);
            }
            // do nothing to the response object.
            break;
        }

        // set the info for updating page DOM.  These are for all cases:
        // including success and failure.
        String strAvmDate =
            HtmlUtil.format(requestEntity.getStatusDate(),
                            HtmlUtil.DATETIME_FORMAT_TYPE,
                            "MMM dd yyyy",
                            new Locale(BXResources.
                                       getLocaleCode(getLanguageId()),
                                       "")
                            );
        response.setAvmDate(strAvmDate);
        response.setAvmStatusMsg(statusMsg);
        String status = BXResources.
            getPickListDescription(getDealInstitutionId(),
                                   "REQUESTSTATUS",
                                   requestEntity.getRequestStatusId(),
                                   languageId);
        response.setAvmStatus(status);
        return response;
    }

    /**
     * try to check the response.
     */
    private AVMResponseObject checkResponse(AppraisalHandler handler,
                                            AVMRequestObject request,
                                            AVMResponseObject response)
        throws RequestProcessException {

        // check the request id first, we must have a request to check the
        // response, otherwise that means somthing wrong!
        if (request.getRequestId() < 0) {
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg);
        }

        // prepare the query SQL.
        String querySQL =
            "SELECT " +
            "response.RESPONSEDATE, response.RESPONSEID, " +
            "prod.SERVICEPROVIDERID, " +
            "avm.AVMVALUE, avm.CONFIDENCELEVEL, " +
            "avm.HIGHAVMRANGE, avm.LOWAVMRANGE, avm.VALUATIONDATE " +
            "FROM " +
            "RESPONSE response, REQUEST request, " +
            "SERVICEREQUEST servicereq, SERVICEPRODUCT prod, AVMSUMMARY avm " +
            "WHERE " +
            "avm.RESPONSEID = response.RESPONSEID AND " +
            "servicereq.REQUESTID = request.REQUESTID AND " +
            "servicereq.REQUESTID = " + request.getRequestId() + " AND " +
            "response.REQUESTID = request.REQUESTID AND " +
            "request.SERVICEPRODUCTID = prod.SERVICEPRODUCTID AND " +
            "prod.SERVICETYPEID = 6 AND prod.SERVICESUBTYPEID = 1";
        if (_log.isDebugEnabled()) {
            _log.debug("Query SQL: " + querySQL);
        }

        // keep the request id.
        response.setRequestId(request.getRequestId());
        // set the response status.
        response.setResponseStatus(RequestObject.
                                   STATUS_CHECKING_RESPONSE);

        SessionResourceKit srk = getSessionResourceKit();
        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (; jExec.next(key); ) {
                Date responseDate = jExec.getDate(key, "VALUATIONDATE");
                if (responseDate == null) {
                    responseDate = jExec.getDate(key, "RESPONSEDATE");
                }
                int responseId =
                    jExec.getInt(key, "RESPONSEID");
                int providerId =
                    jExec.getInt(key, "SERVICEPROVIDERID");
                double avmValue =
                    jExec.getDouble(key, "AVMVALUE");
                String confidence =
                    jExec.getString(key, "CONFIDENCELEVEL");
                double highRange =
                    jExec.getDouble(key, "HIGHAVMRANGE");
                double lowRange =
                    jExec.getDouble(key, "LOWAVMRANGE");

                Locale locale = new Locale(BXResources.
                                           getLocaleCode(getLanguageId()),
                                           "");
                // the datetime format.
                String strResponseDate =
                    HtmlUtil.format(responseDate,
                                    HtmlUtil.DATETIME_FORMAT_TYPE,
                                    "MMM dd yyyy", locale);
                String currencyFmt = locale.getLanguage().equals(BXResources.FRENCH_CODE)?
                    "#,##0.00$; (-#)":"$#,##0.00; (-#)";
                String strAvmValue =
                    HtmlUtil.format(new Double(avmValue),
                                    HtmlUtil.CURRENCY_FORMAT_TYPE,
                                    currencyFmt, locale);
                String strHighRange = 
                    HtmlUtil.format(new Double(highRange),
                                    HtmlUtil.CURRENCY_FORMAT_TYPE,
                                    currencyFmt, locale);
                String strLowRange = 
                    HtmlUtil.format(new Double(lowRange),
                                    HtmlUtil.CURRENCY_FORMAT_TYPE,
                                    currencyFmt, locale);

                response.setResponseDate(strResponseDate);
                // get the provider name.
                String providerName = BXResources.
                    getPickListDescription(getDealInstitutionId(), "SERVICEPROVIDER", providerId,
                                           getLanguageId());
                response.setProviderName(providerName);

                response.setResponseId(responseId);
                response.setStrAvmValue(strAvmValue);
                confidence = confidence != null ?
                    confidence :
                    BXResources.
                    getRichClientMsg("SERVICE_AVM_CONFIDENCE_NOT_AVAILABLE",
                                     getLanguageId());
                response.setConfidenceLevel(confidence);
                response.setHighAvmRange(strHighRange);
                response.setLowAvmRange(strLowRange);

                // set the response status.
                response.setResponseStatus(RequestObject.
                                           STATUS_RESPONSE_SUCCESS);
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        } finally {
            srk.freeResources();
        }

        _log.info("Response Object: " + response.toString());
        return response;
    }
}
