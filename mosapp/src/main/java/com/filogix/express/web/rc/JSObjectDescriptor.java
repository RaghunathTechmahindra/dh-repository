/*
 * @(#)JSObjectDescriptor.java    2006-3-2
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.util.Map;

/**
 * JSObjectDescriptor is the descriptor for a JavaScript Object.
 *
 * @version   1.0 2006-3-2
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface JSObjectDescriptor {

    /**
     * properties description.
     */
    public void setObjectProperties(Map desc);

    /**
     * get properties description.
     */
    public Map getObjectProperties();
}
