/*
 * @(#)ServiceProcessorFactory.java    2006-3-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.AbstractRPFactory;
import com.filogix.express.web.rc.RPFactoryException;
import com.filogix.express.web.rc.RequestProcessor;

/**
 * ServiceProcessorFactory is the 
 *
 * @version   1.0 2006-3-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ServiceProcessorFactory extends AbstractRPFactory {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ServiceProcessorFactory.class);

    /**
     * Constructor function
     */
    public ServiceProcessorFactory() {
    }

    /**
     * implementation.
     */
    public RequestProcessor getRequestProcessor(String serviceType)
        throws RPFactoryException {

        return (RequestProcessor) getRequestProcessorImpl(serviceType);
    }
}

