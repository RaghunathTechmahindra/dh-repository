/*
 * @(#)GenericJSArrayDescriptor.java    2006-3-2
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * GenericJSArrayDescriptor - 
 *
 * @version   1.0 2006-3-2
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class GenericJSArrayDescriptor implements JSArrayDescriptor {

    // The logger
    private static Log _log = LogFactory.getLog(GenericJSArrayDescriptor.class);

    // the element type.
    Object _elementType;

    /**
     * Constructor function
     */
    public GenericJSArrayDescriptor() {
    }

    /**
     * set the each array element's type.
     */
    public void setElementType(Object type) {

        _elementType = type;
    }

    /**
     * get the element type for the array.
     */
    public Object getElementType() {

        return _elementType;
    }
}
