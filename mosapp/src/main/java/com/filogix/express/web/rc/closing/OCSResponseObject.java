/*
 * @(#)OCSResponseObject.java    2006-3-7
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */

package com.filogix.express.web.rc.closing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.filogix.express.web.rc.GenericResponseObject;

/**
 * OCSResponseObject - 
 *
 * @version   1.0 2006-3-7
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class OCSResponseObject extends GenericResponseObject {

    // The logger
    private static Log _log = LogFactory.getLog(OCSResponseObject.class);

    /**
     * Constructor function
     */
    public OCSResponseObject() {
    }

    /**
     * set the response status.
     */
    public void setResponseStatus(int status) {

        put(RC_RESPONSE_STATUS, new Integer(status));
    }
    
    /**
     * set the request id for the given 
     */
    public void setRequestId(int id) {

        put("requestId", new Integer(id));
    }
    
    /**
     * set the copy id for transaction management.
     */
    public void setCopyId(int id) {

        put("copyId", new Integer(id));
    }
    
    /**
     * set the response id
     */
    public void setResponseId(int id) {

        put("responseId", new Integer(id));
    }
    
    /**
     * set requestStatus
     * @param value
     */
    public void setOcsRequestStatus(String value) {

        put("ocsRequestStatus", value);
    }   
    
    /**
     * Set requeststatusId
     * @param value
     */
    public void setOcsRequestStatusId(int value) {

        put("ocsRequestStatusId", new Integer(value));
    }  
    
    /**
     * the servicerequest ref
     */
    public void setOcsServiceProviderRefNum(String ocsServiceProviderRefNum) {
        put("ocsRequestRef", ocsServiceProviderRefNum);
    }
    
    /**
     * the servicerequest date
     */
    public void setOcsRequestDate(String value) {

        put("ocsRequestDate", value);
    }

    /**
     * the request msg.
     */
    public void setOcsRequestStatusMsg(String value) {

        put("ocsRequestStatusMsg", value);
    }
    
    /**
     * set providerId
     * @param value
     */
    public void setProviderId(int value) {

        put("resproviderId", new Integer(value));
    }

    /**
     * set productId
     * @param value
     */
    public void setProductId(int value) {

        put("resproductId", new Integer(value));
    }

    /**
     * the responseDate as string.
     */
    public void setResponseDate(String date) {

        put("responseDate", date);
    }

}


