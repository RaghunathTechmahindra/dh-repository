/*
 * @(#)CommunicatorNotFoundException.java    2006-4-24
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

/**
 * CommunicatorNotFoundException throws out whenever there is no communicator
 * available.
 *
 * @version   1.0 2006-4-24
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class CommunicatorNotFoundException extends Exception {

    /**
     * Constructor function
     */
    public CommunicatorNotFoundException() {
        super();
    }

    /**
     * with message.
     */
    public CommunicatorNotFoundException(String msg) {
        super(msg);
    }

    /**
     * with both message and throwable.
     */
    public CommunicatorNotFoundException(String msg, Throwable th) {
        super(msg, th);
    }

    /**
     * with throwable.
     */
    public CommunicatorNotFoundException(Throwable th) {
        super(th);
    }
}
