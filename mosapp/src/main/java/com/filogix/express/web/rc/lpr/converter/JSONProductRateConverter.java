package com.filogix.express.web.rc.lpr.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basis100.mtgrates.RateInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: JSONProductRateConverter
 * <p>
 * Description: JSON version of implementation of converter for rate 
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class JSONProductRateConverter extends AbstractLegacyCSVConverter
        implements IConverter<RateInfo> {

    /**
     * this method converts a result data list from PricingLigc into a JSON String.
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<RateInfo> list, SessionResourceKit srk) {

        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        if(list != null)
        for (RateInfo rateInfo : list) {
            String desc = PricingLogicConverterUtil
                    .makePricingRateDetailedDescription(rateInfo, srk);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("rateId", rateInfo.getRateId());
            map.put("rateDesc", desc);
            map.put("postedRate", rateInfo.getPostedRate());
            resultList.add(map);
        }
        res.setRates(resultList);
        return res;
    }

}
