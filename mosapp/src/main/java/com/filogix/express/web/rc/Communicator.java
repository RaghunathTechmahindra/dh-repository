/*
 * @(#)Communicator.java    2006-2-21
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

/**
 * Communicator defines interfaces, which will talk to rich client runing on the
 * browser.
 *
 * @version   1.0 2006-2-21
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface Communicator {

    /**
     * parse the request from a serialized string to a Java object.  The
     * serialized string may be a XML document, a JSON string, or SOAP string
     * ... normally the request from rich client should be a Map object,
     * sometime it is an Array.
     */
    public RequestObject parseRequest(String request,
                                      RequestObject requestObject)
        throws CommunicatorParseException;

    /**
     * serialize the response object as a string, 
     */
    public String serializeResponse(ResponseObject obj)
        throws CommunicatorSerializeException;

    /**
     * returns the message when failed to serialize the response object.
     */
    public String getSerializeFailureMsg();
}
