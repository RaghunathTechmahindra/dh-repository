/*
 * @(#)ValuationAppraisalProcessor.java    2006-6-29
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.appraisal;

import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import mosApp.MosSystem.AppraisalHandler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;
import com.filogix.express.web.rc.request.ContactInfo4Request;
import com.filogix.express.web.util.service.ValuationAppraisalHelper;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServiceExecutor;
import com.iplanet.jato.util.HtmlUtil;

/**
 * ValuationAppraisalProcessor - 
 *
 * @version   1.0 2006-6-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ValuationAppraisalProcessor extends AbstractRequestProcessorJato {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ValuationAppraisalProcessor.class);

    // the appraisal handler.
    private AppraisalHandler _handler = new AppraisalHandler();

    /**
     * Constructor function
     */
    public ValuationAppraisalProcessor() {
    }

    public ResponseObject processRequest(RequestObject request,
                                         ResponseObject response)
        throws RequestProcessException {

        AppraisalRequestObject requestObj = (AppraisalRequestObject) request;
        // get an instance of response object.
        AppraisalResponseObject responseObj =
            (AppraisalResponseObject) response;

        // initializing the handler first.
        AppraisalHandler handler= _handler.cloneSS();
        initializeHandler(handler);
        SessionResourceKit srk = getSessionResourceKit();

        // get the releated request entity or create a new request.
        Request requestEntity = getRequestEntity(requestObj, responseObj, srk);

        try {
            // adopt the transaction copy.
            int newTxCopyId = adoptTransactionCopy(handler);
            // have to update the scenario recommended copy, RIGHT AFTER adopt
            // the TX copy successfully!
            updateRequestEntity4SourceCopy(requestObj, requestEntity, srk);
            // update response object to keep consistent.
            responseObj.setCopyId(newTxCopyId);
            
            responseObj.setRequestId(requestEntity.getRequestId());
            responseObj = updateResponseObject(requestEntity, responseObj);
            // run the business rules.
            PassiveMessage pm =
                validateBusinessRules(handler, requestEntity.getRequestId(),
                                      requestObj.getPropertyId(),
                                      requestObj.getIsCancel());
            if (pm != null) {
                _log.info("Got Business Rule Validation Errors!");
                responseObj.setResponseStatus(RequestObject.
                                              STATUS_BUSINESS_RULE_FAILURE);
                responseObj.put(ResponseObject.RC_RESPONSE_ERRORS,
                                preparePassiveMessage(pm));
                
//              Ticket #4462 
                int reqStatusId = requestEntity.getRequestStatusId();
                boolean isCancel = requestObj.getIsCancel();
                
                int requestStatus = ServiceConst.REQUEST_STATUS_NOT_SUBMITTED;
                
                if(isCancel)
                //    requestStatus = getStatusAfterCancelOrderButtonBizRuleFailed(requestStatus);
                    requestStatus = getStatusAfterCancelOrderButtonBizRuleFailed(reqStatusId);
                        //change from requestStatus to reqStatusId
                
                requestStatus = getStatusAfterBizRuleFailed(reqStatusId);
                
                String status = BXResources.
                    getPickListDescription(getDealInstitutionId(), "REQUESTSTATUS", requestStatus, getLanguageId());
                
                responseObj.setAppraisalStatus(status);
                
                //4417
                requestEntity.setRequestStatusId(requestStatus);
                requestEntity.ejbStore();

                Request requestTx = new Request(srk);
                requestTx = requestTx.findByPrimaryKey(
                        new RequestPK(requestEntity.getRequestId(), newTxCopyId));
                requestTx.setRequestStatusId(requestStatus);
                requestTx.ejbStore();                
                
                _log.info("isCancel = " + isCancel + ", AppraisalStatus is set to " + status);
                
            } else {
                responseObj.setResponseStatus(RequestObject.
                                              STATUS_CREATE_REQUEST_SUCCESS);
                if (requestObj.getIsCancel()) {
                    _log.info("Appraisal Cancel Request!  " +
                              "Updating the request for Cancel...");
                    requestEntity.
                        setServiceTransactionTypeId(ServiceConst.
                                                    SERVICE_TRANSACTION_TYPE_CANCEL);
                    requestEntity.
                        setPayloadTypeId(ServiceConst.
                                         SERVICE_PAYLOAD_TYPE_APP_CANCEL);
                } else {
                    _log.info("Appraisal Request!  " +
                              "Updating the request for Request...");
                    requestEntity.
                        setServiceTransactionTypeId(ServiceConst.
                                                    SERVICE_TRANSACTION_TYPE_REQUEST);
                    requestEntity.
                        setPayloadTypeId(ServiceConst.
                                         SERVICE_PAYLOAD_TYPE_APP_REQUEST);
                }
                requestEntity.ejbStore();
                _log.info("sending out the request entity to Datx for Request" +
                          " [" + requestEntity.getRequestId() + ", " +
                          requestEntity.getCopyId() + "]");
                ServiceExecutor executor = new ServiceExecutor(srk);
                int result =
                    executor.processSyncRequest(requestEntity.getRequestId(),
                                                requestEntity.getCopyId(),
                                                getLanguageId());
                _log.info("Got the response result: " + result);
                // handle the result from external service.
                responseObj = handleExtServiceResult(requestEntity, responseObj,
                                                     getLanguageId(), result,
                                                     srk);
            }
        } catch (Exception e) {
            _log.error("Sawallowed Exception: ", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }

        return responseObj;
    }

    /**
     * return a request entity based on the Appraisal request object.
     */
    private Request getRequestEntity(AppraisalRequestObject request,
                                     AppraisalResponseObject response,
                                     SessionResourceKit srk)
        throws RequestProcessException {

        _log.info("Trying to get a Request entity ...");
        try {
            Request requestEntity = new Request(srk);

            if (request.getRequestId() > 0) {
                _log.info("We are working on a existing request [" +
                          request.getRequestId() + "]");
                requestEntity = requestEntity.
                    findByPrimaryKey(new RequestPK(request.getRequestId(),
                                                   request.getCopyId()));

                // check the request's status to see what we need.
                switch (requestEntity.getRequestStatusId()) {
                case ServiceConst.REQUEST_STATUS_NOT_SUBMITTED:
                case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
                case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
                case ServiceConst.REQUEST_STATUS_CANCELLATION_REQUESTED:
                    // for these status, we just use this request entity to
                    // update. also need update the service request contact
                    // record before send out update request.
                    _log.info("Update request entity for status: " +
                              requestEntity.getRequestStatusId());
                    requestEntity =
                        updateRequestEntity(request, requestEntity, srk);
                    break;
                case ServiceConst.REQUEST_STATUS_REQUEST_ACCEPTED:
                case ServiceConst.REQUEST_STATUS_JOB_ASSIGNED:
                case ServiceConst.REQUEST_STATUS_JOB_DELAYED:
                    // this is a cancel request. only need update the request
                    // entity's SERVICETRANSACTIONTYPE and PAYLOADID.
                    _log.info("Update request entry for CANCEL");
                    requestEntity =
                        updateRequestEntity(request, requestEntity, srk);
                    break;
                default:
                    if (request.getIsCancel()) {
                        _log.info("trying to cancel a request on wrong " +
                                  "status!  just update.");
                        requestEntity =
                            updateRequestEntity(request, requestEntity, srk);
                    } else {
                        _log.info("Even through, we got to create a new one!"
                                  + " For status: "
                                  + requestEntity.getRequestStatusId());
                        // for following status, we need create new request.
                        requestEntity =
                            createRequestEntityTree(request, srk);
                    }
                    break;
                }
            } else {
                _log.info("Seems like it is the first time, just create a " +
                          "new request entity!");
                requestEntity = createRequestEntityTree(request, srk);
            }

            return requestEntity;
//        } catch (CreateException ce) {
//            _log.error("Can not create entities!", ce);
//            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
//                                                      getLanguageId());
//            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            _log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (FinderException fe) {
            _log.error("Can not create entities!", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, fe);
        } catch (Exception e) {
            _log.error("Any other exceptions!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
    }

    /**
     * create the request entity.
     */
    private Request createRequestEntityTree(AppraisalRequestObject request,
                                            SessionResourceKit srk)
        throws RequestProcessException {

        _log.info("Trying to create a new Request entity ...");
        try {
            srk.beginTransaction();

            // creating request entity.
            _log.info("creating request entity tree ...");
            Request requestEntity = createRequestEntity(request, srk);

            // creating service request entity
            ServiceRequest serviceRequest =
                createServiceRequestEntity(request, requestEntity, srk);

            // create servicerequest contact
            ServiceRequestContact contact =
                createServiceRequestContactEntity(request, requestEntity, srk);

            srk.commitTransaction();

            _log.info("Successfully created entities!");
            return requestEntity;
        } catch (CreateException ce) {
            _log.error("Can not create entities!", ce);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            _log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (JdbcTransactionException jte) {
            _log.error("Can not create entities!", jte);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, jte);
        } catch (Exception e) {
            _log.error("Any other exceptions!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
    }

    /**
     * create request entity.
     */
    private Request createRequestEntity(AppraisalRequestObject request,
                                        SessionResourceKit srk)
        throws CreateException, RemoteException {

        // creating request entity.
        _log.info("creating request for deal: [dealid=" +
                  request.getDealId() +
                  ", copyid=" + request.getCopyId() + "]...");
        Request requestEntity = new Request(srk);
        requestEntity.create(new DealPK(request.getDealId(), request.getCopyId()),
                             ServiceConst.REQUEST_STATUS_NOT_SUBMITTED,
                             new Date(), srk.getExpressState().getUserProfileId());
        if (_log.isDebugEnabled())
            _log.debug("Request Entity Created: " +
                       requestEntity.toString());
 
        requestEntity.setChannelId(3);  // CHANNELID_appraisal
        requestEntity.setServiceProductId(request.getProductId());
        requestEntity.
            setServiceTransactionTypeId(ServiceConst.
                                        SERVICE_TRANSACTION_TYPE_REQUEST);
        requestEntity.
            setPayloadTypeId(ServiceConst.
                             SERVICE_PAYLOAD_TYPE_APP_REQUEST);
        requestEntity.
            setRequestTypeId(ServiceConst.
                             REQUEST_TYPE_APPRAISAL_REQUEST_INT);
        requestEntity.setRequestDate(new Date());
        // current user profile id.
        requestEntity.setUserProfileId(getSessionUserId());
        requestEntity.ejbStore();

        return requestEntity;
    }
    
    /**
     * private create service request entity.
     */
    private ServiceRequest
        createServiceRequestEntity(AppraisalRequestObject request,
                                   Request requestEntity,
                                   SessionResourceKit srk)
        throws CreateException, RemoteException {

        // creating servicerequest entity.
        _log.info("creating servicerequest for request: " +
                  requestEntity.getRequestId());
        ServiceRequest serviceRequest = new ServiceRequest(srk);
        serviceRequest = serviceRequest.create(requestEntity.getRequestId(),
                                               requestEntity.getCopyId());
        // update other info.
        // should get the primary propery's property id.
        serviceRequest.setPropertyId(request.getPropertyId());
        serviceRequest.setSpecialInstructions(request.getInstruction());
        serviceRequest.ejbStore();
        if (_log.isDebugEnabled())
            _log.debug("ServiceRequest Created: " +
                       serviceRequest.toString());

        return serviceRequest;
    }

    /**
     * create service request contact entity.
     */
    private ServiceRequestContact
        createServiceRequestContactEntity(AppraisalRequestObject request,
                                          Request requestEntity,
                                          SessionResourceKit srk)
        throws CreateException, RemoteException, Exception {

        // create servicerequest contact
        ServiceRequestContact contact = new ServiceRequestContact(srk);
        if (request.getNeedPrimary()) {
            contact = createContactByBorrower(request, requestEntity,
                                              contact, srk);
        } else {
            contact = contact.create(requestEntity.getRequestId(),
                                     requestEntity.getCopyId());
            contact = updateServiceRequestContact(contact, request);
        }
        contact.ejbStore();
        if (_log.isDebugEnabled())
            _log.debug("ServiceRequestContact Created: " +
                       contact.toString());

        return contact;
    }

    /**
     * create service request contact by use the borrower.
     */
    private ServiceRequestContact
        createContactByBorrower(AppraisalRequestObject request,
                                Request requestEntity,
                                ServiceRequestContact contact,
                                SessionResourceKit srk)
        throws Exception {

        if (_log.isDebugEnabled())
            _log.debug("Creating contact based on the borrower info ...");
        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey(new DealPK(request.getDealId(), request.getCopyId()));
        for (Iterator i = deal.getBorrowers().iterator(); i.hasNext(); ) {
            Borrower borrower = (Borrower) i.next();
            if (borrower.isPrimaryBorrower()) {
                contact = contact.
                    createWithBorrowerAssoc(requestEntity.getRequestId(),
                                            borrower.getBorrowerId(),
                                            requestEntity.getCopyId());
                updateServiceRequestContact(contact, borrower);
                break;
            }
        }

        return contact;
    }

    /**
     * update the service request entity tree for the scenario recommended copy
     */
    private Request
        updateRequestEntity4SourceCopy(AppraisalRequestObject request,
                                       Request requestEntity,
                                       SessionResourceKit srk)
        throws Exception {

        if (_log.isDebugEnabled())
            _log.debug("Updating request entity tree for source scenario " +
                       "copy ...");
        int sourceCopyId = getDealEditControl().getSourceCID();
        if (sourceCopyId == requestEntity.getCopyId()) {
            _log.info("We are working on the source scenario copy " +
                      "right now!");
            // no need to update, just return.
            return requestEntity;
        }

        _log.info("We are NOT working on the source scenario copy!");
        Request requestEntitySource = new Request(srk);
        try {
            requestEntitySource =
                requestEntitySource.
                findByPrimaryKey(new RequestPK(requestEntity.getRequestId(),
                                               sourceCopyId));
            requestEntitySource = updateRequestEntity(request,
                                                      requestEntitySource,
                                                      srk);
            _log.info("Successfully updated the source scenario copy!");
        } catch (FinderException fe) {
            _log.info("There is no source scenario copy for request!");
            _log.info("Trying to create one ...");
            // creating request.
            requestEntitySource = createRequestEntity(request, srk);
            // revise the request id.
            requestEntitySource.setRequestId(requestEntity.getRequestId());
            requestEntitySource.setCopyId(sourceCopyId);
            requestEntitySource.ejbStore();
            
            RequestPK _pk = new RequestPK(requestEntity.getRequestId(), sourceCopyId);
            requestEntitySource = requestEntitySource.findByPrimaryKey(_pk);
            
            // creating service request.
            createServiceRequestEntity(request, requestEntitySource, srk);
            // creating service request contact
            ServiceRequestContact contact =
                createServiceRequestContactEntity(request,
                                                  requestEntitySource, srk);

            ServiceRequestContact src = requestEntitySource.getServiceRequestContact();
            
            if(src != null)
                contact.setServiceRequestContactId(src.getServiceRequestContactId());
            
            contact.ejbStore();
            _log.info("Successfully created request entities for " +
                      "source scenario copy.");
        }

        return requestEntitySource;
    }

    /**
     * update the service request entity tree for  the given request entity.
     */
    private Request updateRequestEntity(AppraisalRequestObject request,
                                        Request requestEntity,
                                        SessionResourceKit srk)
        throws Exception {

        if (_log.isDebugEnabled())
            _log.debug("Updating request entity...");
        // generic update.
        requestEntity.setServiceProductId(request.getProductId());
        requestEntity.setRequestDate(new Date());
        requestEntity.setUserProfileId(getSessionUserId());
        requestEntity.ejbStore();

        if (_log.isDebugEnabled())
            _log.debug("Updating ServiceRequest...");
        // get the existing ServiceRequest.
        ServiceRequest serviceRequest = requestEntity.getServiceRequest();
        if (serviceRequest != null) {
            serviceRequest.setSpecialInstructions(request.getInstruction());
            serviceRequest.ejbStore();
        }

        if (_log.isDebugEnabled())
            _log.debug("Updating ServiceRequestContact...");
        // get the existing contact.
        ServiceRequestContact contact =
            new ServiceRequestContact(srk,
                                      request.getServiceRequestContactId(),
                                      requestEntity.getRequestId(),
                                      requestEntity.getCopyId());

        if (request.getNeedPrimary()) {
            _log.info("Updating request entity by primary borrower");
            if (contact.getBorrowerId() > 0) {
                _log.info("we already use primary borrower...");
                // do nothing here...
            } else {
                _log.info("without to with primary borrower create ...");
                // find the borrower info.
                Borrower borrower = null;
                Deal deal = new Deal(srk, null);
                deal = deal.findByPrimaryKey(new DealPK(request.getDealId(), request.getCopyId()));
                for (Iterator i = deal.getBorrowers().iterator();
                     i.hasNext(); ) {
                    borrower = (Borrower) i.next();
                    if (borrower.isPrimaryBorrower()) {
                        break;
                    }
                }
                if (borrower == null) {
                    throw new RequestProcessException("No Primary Borrower!");
                }
                requestEntity.createChildAssocs(borrower.getBorrowerId());
                contact.setBorrowerId(borrower.getBorrowerId());
                updateServiceRequestContact(contact, borrower);
            }
            contact.ejbStore();
        } else {
            _log.info("Updating request entity without primary borrower");
            // update the contact info.
            contact = updateServiceRequestContact(contact, request);
            contact.ejbStore();
            if (contact.getBorrowerId() > 0) {
                _log.info("with to without primary borrower. remove...");
                contact.cutBorrowerAssoc();
                requestEntity.removeChildAssocs();
            } else {
                _log.info("without to without primary borrower. do thing...");
            }
        }

        return requestEntity;
    }

    /**
     * trigger business rule engine and return the passive message aboject.
     */
    private PassiveMessage validateBusinessRules(AppraisalHandler handler,
                                                 int requestId,
                                                 int propertyId,
                                                 boolean isCancel)
        throws Exception {

        // create a Business Rule Executor.
        BusinessRuleExecutor brExecutor = new BusinessRuleExecutor();
        brExecutor.setCurrentBorrower(-1);
        // need set up the property id, should be the primary property id.
        brExecutor.setCurrentProperty(propertyId);
        brExecutor.setCurrentRequest(requestId);
        // the business rule prefixes
        Vector rulePrefixes = new Vector();
        if (isCancel) {
            rulePrefixes.add("ARC-%");
        } else {
            rulePrefixes.add("ARR-%");
        }

        PassiveMessage passiveMessage =
            brExecutor.BREValidator(handler.getTheSessionState(),
                                    handler.
                                    getTheSessionState().getCurrentPage(),
                                    handler.getSessionResourceKit(),
                                    null,
                                    rulePrefixes);

        return passiveMessage;
    }

    /**
     * handle the external service result.
     */
    private AppraisalResponseObject
        handleExtServiceResult(Request requestEntity,
                               AppraisalResponseObject response,
                               int languageId, int responseId,
                               SessionResourceKit srk)
        throws RequestProcessException {

        try {
            // reload the request entity.
            requestEntity.
                findByPrimaryKey(new RequestPK(requestEntity.getRequestId(),
                                               requestEntity.getCopyId()));
        } catch (FinderException fe) {
            _log.error("Can not find the request [requestId=" +
                       requestEntity.getRequestId() + ", copyId=" +
                       requestEntity.getCopyId() + "]", fe);
            throw new RequestProcessException("Can not find request!", fe);
        }
        int requestStatusId = requestEntity.getRequestStatusId();
        String statusMsg = requestEntity.getStatusMessage();
        switch (requestStatusId) {
        case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
        case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
        case ServiceConst.REQUEST_STATUS_FAILED:
            Object[] params = {statusMsg};
            String errorMsg =
                BXResources.getRichClientMsg("SERVICE_APPRAISAL_ERROR_PROMPT",
                                             languageId, params);
            // setting up the response object.
            response.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
            response.put(ResponseObject.RC_RESPONSE_ERRORS,
                         errorMsg);
            break;
        default:
            _log.info("We got the response successfully!");
            response.setResponseStatus(RequestObject.STATUS_RESPONSE_SUCCESS);
            try {
                forceAdoptScenarioRecommended(requestEntity.getDealId(),
                                              getDealEditControl(),
                                              requestEntity.getCopyId(),
                                              getSessionResourceKit());
            } catch (Exception e) {
                _log.error("All other errors!", e);
                throw new
                    RequestProcessException("Can not set scenario recommended",
                                            e);
            }
            break;
        }

        response = updateResponseObject(requestEntity, response);

        // got to update the transaction copy's status.
        try {
            _log.info("Trying to update the transaction copy's status...");
            Request requestEntityCopy = new Request(srk);
            requestEntityCopy.
                findByPrimaryKey(new RequestPK(requestEntity.getRequestId(),
                                               getNewTxCopyId()));
            requestEntityCopy.
                setRequestStatusId(requestEntity.getRequestStatusId());
            requestEntityCopy.
                setStatusDate(requestEntity.getStatusDate());
            requestEntityCopy.
                setStatusMessage(requestEntity.getStatusMessage());
            requestEntityCopy.ejbStore();
        } catch (FinderException fe) {
            // should not happen.
            _log.error("Can not find the request [requestId=" +
                       requestEntity.getRequestId() + ", copyId=" +
                       getNewTxCopyId() + "]", fe);
            throw new RequestProcessException("Can not find request!", fe);
        } catch (RemoteException re) {
            _log.error("Can not update the new transaction copy!", re);
            throw new RequestProcessException("Can not update request!", re);
        }

        return response;
    }

    /**
     * update response object.
     */
    private AppraisalResponseObject
        updateResponseObject(Request requestEntity,
                             AppraisalResponseObject response) {

        // set the info for updating page DOM.
        String strRequestDate =
            HtmlUtil.format(requestEntity.getStatusDate(),
                            HtmlUtil.DATETIME_FORMAT_TYPE,
                            "MMM dd yyyy HH:mm",
                            new Locale(BXResources.
                                       getLocaleCode(getLanguageId()),
                                       "")
                            );
        response.setRequestDate(strRequestDate);

        response.setRequestId(requestEntity.getRequestId());
        response.setAppraisalStatusId(requestEntity.getRequestStatusId());
        String status = BXResources.
            getPickListDescription(getDealInstitutionId(), "REQUESTSTATUS",
                                   requestEntity.getRequestStatusId(),
                                   getLanguageId());
        response.setAppraisalStatus(status);

        // ref number from service request.
        ServiceRequest servicRequest = requestEntity.getServiceRequest();
        if (servicRequest != null) {
            response.setProviderRefNum(servicRequest.getServiceProviderRefNo());
        }

        // status messages. format message.
        response.
            setAppraisalStatusMsg(ValuationAppraisalHelper.
                                  getAppraisalStatusMessage(requestEntity,
                                                            getDealInstitutionId(),
                                                            getLanguageId()));

        // the servicerequestcontact.
        ServiceRequestContact contact =
            requestEntity.getServiceRequestContact();
        if (contact != null) {
            response.setServiceRequestContactId(contact.
                                                getServiceRequestContactId());
        }

        return response;
    }

    /**
     * update  the contact info based on the request.
     */
    private ServiceRequestContact
        updateServiceRequestContact(ServiceRequestContact contact,
                                    AppraisalRequestObject request) {

        return
            ValuationAppraisalHelper.
            updateServiceRequestContact(contact,
                                        (ContactInfo4Request) request);
    }

    /**
     * update the contact inf based on the primary borrower id.
     */
    private ServiceRequestContact
        updateServiceRequestContact(ServiceRequestContact contact,
                                    Borrower borrower) {

        return
            ValuationAppraisalHelper.
            updateServiceRequestContact(contact, borrower);
    }
    
    //Ticket #4462
    private int getStatusAfterCancelOrderButtonBizRuleFailed(int currentStatusId)
    {
        int statusId = ServiceConst.REQUEST_STATUS_NOT_SUBMITTED;
        
        switch (currentStatusId) {
            case ServiceConst.REQUEST_STATUS_REQUEST_ACCEPTED :
            case ServiceConst.REQUEST_STATUS_JOB_ASSIGNED :
            case ServiceConst.REQUEST_STATUS_JOB_DELAYED :
            case ServiceConst.REQUEST_STATUS_CANCELLATION_NOT_SUBMITTED :
            case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR :
            case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR :
                statusId = ServiceConst.REQUEST_STATUS_CANCELLATION_NOT_SUBMITTED;
                break;
                
            case ServiceConst.REQUEST_STATUS_CANCELLATION_REQUESTED :
                statusId = ServiceConst.REQUEST_STATUS_CANCELLATION_REQUESTED;
                break;
                
            case ServiceConst.REQUEST_STATUS_CANCELLATION_REJECTED :
                statusId = ServiceConst.REQUEST_STATUS_CANCELLATION_REJECTED;
                break;

            default :
                break;
        }
        
        return statusId;
    }
    
    /**
     * 4417
     */
    private int getStatusAfterBizRuleFailed(int currentStatusId)
    {
        int statusId = ServiceConst.REQUEST_STATUS_NOT_SUBMITTED;
        
        switch (currentStatusId) {
            case ServiceConst.REQUEST_STATUS_REQUEST_ACCEPTED :
            case ServiceConst.REQUEST_STATUS_JOB_ASSIGNED :
            case ServiceConst.REQUEST_STATUS_JOB_DELAYED :
            case ServiceConst.REQUEST_STATUS_CANCELLATION_NOT_SUBMITTED :
            case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR :
            case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR :
            case ServiceConst.REQUEST_STATUS_CANCELLATION_REJECTED :
            case ServiceConst.REQUEST_STATUS_CANCELLATION_REQUESTED :
                statusId = currentStatusId;
                break;
            default :
                break;
        }
        
        return statusId;
    }
}
