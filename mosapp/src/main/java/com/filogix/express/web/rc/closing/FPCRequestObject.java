/*
 * @(#)FPCRequestObject.java    2006-3-7
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.closing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.request.ServiceRequestObject;
import com.filogix.express.web.rc.request.ContactInfo4Request;

/**
 * PilotFPCRequestObject - 
 *
 * @version   1.0 2006-3-7
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */

public class FPCRequestObject extends ServiceRequestObject
    implements ContactInfo4Request {

    // The logger
    private final static Log _log = LogFactory.getLog(OCSRequestObject.class);

    /**
     * Constructor function
     */
    public FPCRequestObject() {
    }

    /**
     * return the servicerequestcontact id.
     */
    public int getServiceRequestContactId() {

        return ((Integer) get("serviceRequestContactId")).intValue();
    }

    /**
     * returns the providerId.
     */
    public int getProviderId() {

        return ((Integer) get("providerId")).intValue();
    }

    /**
     * returns the product id.
     */
    public int getProductId() {

        return ((Integer) get("productId")).intValue();
    }

    /**
     * the borrower id.
     */
    public int getBorrowerId() {

        return ((Integer) get("borrowerId")).intValue();
    }

    /**
     * need primary borrower or not.
     */
    public boolean getNeedPrimary() {

        return ((Boolean) get("needPrimary")).booleanValue();
    }

    public String getFirstName() {
        return((String) get("firstName"));
    }

    public String getLastName() {
        return((String) get("lastName"));
    }

    public String getEmailAddress() {
        return((String) get("emailAddress"));
    }

    public String getWorkPhoneArea() {
        return((String) get("workAreaCode"));
    }

    public String getWorkPhoneNumber() {
        return((String) get("workPhoneNumber"));
    }

    public String getWorkPhoneExt() {
        return((String) get("workPhoneExt"));
    }

    public String getCellPhoneArea() {
        return((String) get("cellAreaCode"));
    }

    public String getCellPhoneNumber() {
        return((String) get("cellPhoneNumber"));
    }

    public String getHomePhoneArea() {
        return((String) get("homeAreaCode"));
    }

    public String getHomePhoneNumber() {
        return((String) get("homePhoneNumber"));
    }

    public String getComments() {
        return((String) get("comments"));
    }

    public String getPropertyOwnerName() {
        return null;
    }
}
