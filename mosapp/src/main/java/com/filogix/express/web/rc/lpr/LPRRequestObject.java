/*
 * @(#)LPRRequestObject.java Nov 28, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.web.rc.lpr;

import com.filogix.express.web.rc.GenericRequestObject;

/**
 * LPRRequestObject
 * 
 * @version 1.0 Nov 28, 2007
 * @author Bzhang, Hiro
 * 
 * @version 1.1 Aug 15, 2008 artf762702, added isUmbrella
 */
public class LPRRequestObject extends GenericRequestObject{
//	private static Log _log = LogFactory.getLog(LPRRequestObject.class);
	
	public LPRRequestObject(){
	}

	public String getSubRequestType() {
		return ((String) get("subRequestType"));
	}
	
	public int getDealId() {
		return ((Integer) get("dealId")).intValue();
	}
	
	public void setDealId(Integer dealId){
	    put("dealId", dealId);
	}
	
	public int getDealCopyId() {
		return ((Integer) get("dealCopyId")).intValue();
	}
	
	public void setDealCopyId(Integer copyId){
	    put("dealCopyId", copyId);
	}
	
	public int getLenderProfileId() {
        return ((Integer) get("lenderProfileId"));
    }
	
	public int getProductId() {
		return ((Integer) get("productId"));
	}
	
	public int getDealRateStatusId() {
		return ((Integer) get("dealRateStatusId"));
	}
	
	public String getApplicationDate(){
		return (String) get("applicationDate");
	}

	public String getRateDisabledDate(){
		return (String) get("rateDisabledDate");
	}
	
	public String getRateFormat(){
		return (String) get("rateFormat");
	}
	
	public int getLanguageId(){
		return ((Integer) get("languageId")).intValue();
	}
	
	public void setLanguageId(Integer languageId){
	    put("languageId", languageId);
	}
	
	public int getComponentTypeId(){
	    Integer compTypeId = (Integer)get("componentTypeId");
	    if(compTypeId == null)
	        return -1;
	    else
	        return compTypeId.intValue();
	}
	
	public boolean isUmbrella(){
	    return "Y".equals(get("isUmbrella"));
	}
	
	
}
