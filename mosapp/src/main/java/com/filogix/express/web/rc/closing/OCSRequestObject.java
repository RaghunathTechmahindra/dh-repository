/*
 * @(#)OCSRequestObject.java    2006-3-7
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.closing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericRequestObject;

/**
 * OCSRequestObject - 
 *
 * @version   1.0 2006-3-7
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class OCSRequestObject extends GenericRequestObject {

    // The logger
    private final static Log _log = LogFactory.getLog(OCSRequestObject.class);

    /**
     * Constructor function
     */
    public OCSRequestObject() {
    }

    /**
     * returns the request action.
     */
    public int getRequestAction() {

        return ((Integer) get(RC_REQUEST_ACTION)).intValue();
    }

    /**
     * returns the providerId.
     */
    public int getProviderId() {

        return ((Integer) get("providerId")).intValue();
    }

    /**
     * returns the product id.
     */
    public int getProductId() {

        return ((Integer) get("productId")).intValue();
    }
    
    /**
     * returns the Deal id.
     */
    public int getDealId() {

        return ((Integer) get("dealId")).intValue();
    }

    /**
     * returns the copy id.
     */
    public int getCopyId() {

        return ((Integer) get("copyId")).intValue();
    }
    
    /**
     * return the request id.
     */
    public int getRequestId() {

        return ((Integer) get("requestId")).intValue();
    }
    
    /**
     * return isCancel request
     */
    public boolean getIsCancel() {
        return ((Boolean) get("isCancel")).booleanValue();
    }

    /**
     * Get the value of the special instructions
     */
    public String getInstructions() {
        return (String) get("instructions");
    }
}
