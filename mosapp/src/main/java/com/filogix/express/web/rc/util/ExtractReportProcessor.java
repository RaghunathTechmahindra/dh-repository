/*
 * @(#)ExtractReportProcessor.java    2006-7-4
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;

/**
 * ExtractReportProcessor - 
 *
 * @version   1.0 2006-7-4
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExtractReportProcessor extends AbstractReportProcessor {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ExtractReportProcessor.class);

    /**
     * Constructor function
     */
    public ExtractReportProcessor() {
    }

    /**
     */
    public ResponseObject processRequest(RequestObject request,
                                         ResponseObject response)
        throws RequestProcessException {

        // do nothing here.
        return response;
    }

    /**
     * handle the request.
     */
    public ResponseObject processRequest(RequestObject request,
                                         ResponseObject response,
                                         HttpServletResponse httpResponse)
        throws RequestProcessException {

        // the response id.
        int responseId = ((Integer) request.get("responseId")).intValue();
        String reportType = (String) request.get("reportType");

        _log.info("Trying to get the report[" + reportType +
                  "] for [" + responseId + "]");

        try {
            // the the extractor...
            AbstractReportExtractor extractor =
                (AbstractReportExtractor) getExtractor(reportType);
            // set the response id.
            extractor.setResponseId(responseId);
            // extract the report.
            if (extractor instanceof TextResponseExtractor) {
                String report =
                    ((TextResponseExtractor) extractor).extractTextReport();
                // put it to the response.
                response.put("report", report);
            } else if (extractor instanceof BinaryResponseExtractor) {
                ((BinaryResponseExtractor) extractor).
                    setHttpResponse(httpResponse);
                extractor.extractReport();
            } else {
                // do nothing for now.
            }
        } catch (ExtractorNotFoundException ee) {
            _log.error("Can not find extractor for: " + reportType, ee);
            response.put(ResponseObject.RC_RESPONSE_ERRORS,
                         "Can not find extractor for: " + reportType);
            throw new RequestProcessException("Can not find extractor for:" +
                                              reportType, ee);
        } catch (ExtractReportException ere) {
            _log.error("Can not extract report for: " + responseId, ere);
            response.put(ResponseObject.RC_RESPONSE_ERRORS,
                         "Can not extract report for: " + responseId);
            throw new RequestProcessException("Can not extract reprt for:" +
                                              responseId, ere);
        } finally {
            return response;
        }
    }
}
