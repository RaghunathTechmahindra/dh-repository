/*
 * @(#)ExtractorNotFoundException.java    2006-5-3
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

/**
 * ExtractorNotFoundException - 
 *
 * @version   1.0 2006-5-3
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExtractorNotFoundException extends Exception {

    /**
     * Constructor function
     */
    public ExtractorNotFoundException() {

        super();
    }

    /**
     * with message.
     */
    public ExtractorNotFoundException(String message) {

        super(message);
    }

    /**
     * with message and throwable.
     */
    public ExtractorNotFoundException(String message, Throwable th) {

        super(message, th);
    }

    /**
     * with throwable.
     */
    public ExtractorNotFoundException(Throwable th) {

        super(th);
    }
}
