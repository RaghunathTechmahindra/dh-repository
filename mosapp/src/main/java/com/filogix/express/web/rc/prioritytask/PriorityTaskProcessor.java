package com.filogix.express.web.rc.prioritytask;

import java.sql.PreparedStatement;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mosApp.MosSystem.PageHandlerCommon;
import mosApp.MosSystem.SessionStateModelImpl;

import com.basis100.deal.util.StringUtil;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;

/**
 * Processor for checking unacknowledged tasks.
 * 
 * @author bzhang, Hiro
 */
public class PriorityTaskProcessor extends AbstractRequestProcessorJato {

	private static Log _log = LogFactory.getLog(PriorityTaskProcessor.class);

	private static final String sqltmplate = "SELECT taskid from " +
	"ASSIGNEDTASKSWORKQUEUE WHERE ACKNOWLEDGED = 'N' " +
	"AND VISIBLEFLAG <> 0 AND ";
	private static final String PTASK_NOT_FOUND = "N";
	private static final String PTASK_FOUND = "Y";


	public ResponseObject processRequest(RequestObject requestObj,
			ResponseObject responeObj) {
		_log.debug("PriorityTaskProcessor: start ---------");
		PTaskRequestObject reqObj = (PTaskRequestObject) requestObj;
		PTaskResponseObject respObj = (PTaskResponseObject) responeObj;
		respObj.setTaskFoundString(PTASK_NOT_FOUND); //default

		PageHandlerCommon handler = new PageHandlerCommon();
		
		try {
			initializeHandler(handler);
		} catch (RuntimeException e) {
			_log.warn("PriorityTaskProcessor: Failed to initialize handler. " +
					"Most likely this request is from abandoned screen. " + e);
			respObj.setTaskFoundString(PTASK_FOUND); //'Y' will stop request process in JS 
			return respObj;
		}

		SessionResourceKit srk = null;

		try{
			//in order to grab all institution's data, 
			// this method uses srk that is not set VPD
            
            if (reqObj.getTimeOut()) {
                respObj.setSessionTimeOut(true);
                handler.handleSignOff(true, true);
            } else {
    			srk = new SessionResourceKit("PTCHK");
    			SessionStateModelImpl theSessionState  = handler.getTheSessionState();
    			JdbcExecutor jExec = srk.getJdbcExecutor();
    			
    			PreparedStatement pstmt = jExec.getPreparedStatement(sqltmplate);
    			String sql = sqltmplate + makeUserWhereCriteria(theSessionState);
    			if(_log.isDebugEnabled()){
    				_log.debug("PriorityTaskProcessor: SQL = " + sql);
    				_log.debug("PriorityTaskProcessor: VPD = " + srk.getActualVPDStateForDebug());
    			}
    			int key = jExec.execute(sql);
    			if(jExec.next(key) == true){
    				respObj.setTaskFoundString(PTASK_FOUND);
    				_log.debug("PriorityTaskProcessor: Priority task found : returns 'Y'");
    			}else{
    				respObj.setTaskFoundString(PTASK_NOT_FOUND);
    				_log.debug("PriorityTaskProcessor: Priority task not found : returns 'N'");
    			}
    			jExec.closeData(key);
    			pstmt.close();
            }

		}catch(Exception e){
			_log.error("Exception @PriorityTaskProcessor - ignored");
			_log.error(StringUtil.stack2string(e));
		}finally{
			if(srk != null)
				srk.freeResources();
		}
		_log.debug("PriorityTaskProcessor: end ---------");
		return respObj;
	}

	/**
	 * returns part of where sentence which contains all available 
	 * combination of user's institutionProfileId and userProfileId.
	 * f.e.  " (institutionProfileID, userProfileID) in ((0, 1001),(1, 2002)) " 
	 * @param theSessionState
	 * @return part of where sentence.
	 */
	private String makeUserWhereCriteria(SessionStateModelImpl theSessionState) {

		StringBuffer userCriteria = new StringBuffer();
		userCriteria.append(" (institutionProfileID, userProfileID) in (");
		List<Integer> instList = theSessionState.institutionList();
		for (int i = 0; i < instList.size(); i++) {
			int institutionProfileId = instList.get(i);
			if (i > 0)
				userCriteria.append(",");
			userCriteria.append("(");
			userCriteria.append(institutionProfileId);
			userCriteria.append(", ");
			userCriteria.append(
					theSessionState.getUserProfileId(institutionProfileId));
			userCriteria.append(")");
		}
		userCriteria.append(")");
		return userCriteria.toString();
	}
}
