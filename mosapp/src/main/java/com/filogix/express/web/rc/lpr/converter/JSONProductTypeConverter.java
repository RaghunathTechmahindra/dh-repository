package com.filogix.express.web.rc.lpr.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.dao.ProductTypeDao;

/**
 * Title: JSONProductTypeConverter
 * <p>
 * Description: JSON version of implementation of converter for ProductType 
 * @author MCM Impl Team.
 * @version 1.0 Aug 07, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public class JSONProductTypeConverter implements IConverter<ProductTypeDao> {

    /**
     * this method converts a result data list from PricingLigc into a JSON String.
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<ProductTypeDao> list, SessionResourceKit srk) {

        int institutionId = srk.getExpressState().getDealInstitutionId();
        int languageId = srk.getLanguageId();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        
        if(list != null)
        for (ProductTypeDao productType : list) {

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("productTypeId", productType.getProductTypeId());
            
            String productTypeDesc = BXResources.getPickListDescription(
                    institutionId, "PRODUCTTYPE", productType.getProductTypeId(), languageId);
            map.put("productTypeDesc", productTypeDesc);
            
            resultList.add(map);
        }
        res.setProductType(resultList);
        return res;
    }
}
