/*
 * @(#)AbstractAJAXDispatcher.java    2006-4-23
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.ajax;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.RequestProcessorFactory;

/**
 * AbstractAJAXDispatcher is an abastract implementation for AJAXDispatcher.
 *
 * @version   1.0 2006-4-23
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractAJAXDispatcher implements AJAXDispatcher {

    // The logger
    private static Log _log = LogFactory.getLog(AbstractAJAXDispatcher.class);

    // the request processor factory.
    protected RequestProcessorFactory _requestProcessorFactory;

    /**
     * set the request processor factory.
     */
    public void setRequestProcessorFactory(RequestProcessorFactory factory) {

        _requestProcessorFactory = factory;
    }
}
