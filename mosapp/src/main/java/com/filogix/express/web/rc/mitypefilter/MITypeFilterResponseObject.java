package com.filogix.express.web.rc.mitypefilter;

import java.util.List;
import java.util.Map;

import com.filogix.express.web.rc.GenericResponseObject;

public class MITypeFilterResponseObject extends GenericResponseObject{

    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unchecked")
    public void setMITypes(List<Map<String, Object>> miTypes) {
        put("miTypes", miTypes);
    }
    
    @SuppressWarnings("unchecked")
    public void setSelectedMITypeId(int miTypeId){
        put("selectedMITypeId", miTypeId);
    }
}
