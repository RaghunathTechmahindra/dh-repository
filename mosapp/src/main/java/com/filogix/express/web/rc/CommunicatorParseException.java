/*
 * @(#)CommunicatorParseException.java    2006-2-28
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

/**
 * CommunicatorParseException throw out whenever got a error during parse.
 *
 * @version   1.0 2006-2-28
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class CommunicatorParseException extends Exception {

    /**
     * Constructor function
     */
    public CommunicatorParseException() {
        super();
    }

    /**
     * with message.
     */
    public CommunicatorParseException(String msg) {

        super(msg);
    }

    /**
     * with message and a throwable.
     */
    public CommunicatorParseException(String msg, Throwable th) {

        super(msg, th);
    }

    /**
     * with throwable,
     */
    public CommunicatorParseException(Throwable th) {

        super(th);
    }
}
