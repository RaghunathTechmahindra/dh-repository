/*
 * @(#)ReportExtractor.java    2006-5-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import java.util.List;

/**
 * ReportExtractor defines the interface to extract the report from persistence
 * layer.
 *
 * @version   1.0 2006-5-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface ReportExtractor {

    /**
     * prepare criterias for extracting the right report.
     */
    public List prepareCriterias(List criterias);

    /**
     * extract the report from the persistence layer based on the specified
     * criteria.
     */
    public Object extractReport() throws ExtractReportException;
}
