/*
 * @(#)ServiceRequestObject.java    2006-6-29
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.request;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericRequestObject;

/**
 * ServiceRequestObject the generic request object for service request.
 *
 * @version   1.0 2006-6-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ServiceRequestObject extends GenericRequestObject {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ServiceRequestObject.class);

    /**
     * Constructor function
     */
    public ServiceRequestObject() {
    }

    /**
     * returns the request action.
     */
    public int getRequestAction() {

        return ((Integer) get(RC_REQUEST_ACTION)).intValue();
    }

    /**
     * returns the deal id.
     */
    public int getDealId() {

        return ((Integer) get("dealId")).intValue();
    }

    /**
     * returns the copy id.
     */
    public int getCopyId() {

        return ((Integer) get("copyId")).intValue();
    }

    /**
     * returns the request id.
     */
    public int getRequestId() {

        return ((Integer) get("requestId")).intValue();
    }
}
