package com.filogix.express.web.rc.lpr.adapter;

import java.util.List;

import com.basis100.mtgrates.IPricingLogicLenderProfile;
import com.basis100.mtgrates.LenderInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: PricingLogicLenderAdapter
 * <p>
 * Description: implementation of adapter for lender
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class PricingLogicLenderAdapter extends AbstractPricingLogicAdapter {

    /**
     * @see com.filogix.express.web.rc.lpr.adapter.IPricingLogicAdapter#process(com.basis100.resources.SessionResourceKit, com.filogix.express.web.rc.RequestObject, com.filogix.express.web.rc.ResponseObject)
     */
    public LPRResponseObject process(SessionResourceKit srk,
            LPRRequestObject req, LPRResponseObject res) {

        List<LenderInfo> list 
            = ((IPricingLogicLenderProfile) pricingLogic).getLenderProfiles(srk);
        
        return this.converter.convert(res, list, srk);

    }

}
