package com.filogix.express.web.rc.lpr.dao;

import com.basis100.deal.entity.MtgProd;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;

/**
 * Title: ComponentTypeDaoImpl
 * <p>
 * Description: Data access object for Component Description
 * 
 * @author MCM Impl Team.
 * @version 1.0 Aug 14, 2008 artf762702 Initial version
 */
public class ComponentTypeDaoImpl implements ComponentTypeDao {

    int componentTypeid;
    String componentTypeDescription;

    public String getComponentTypeDescription() {
        return componentTypeDescription;
    }
    
    public int getComponentTypeId(){
        return componentTypeid;
    }

    public void load(SessionResourceKit srk, LPRRequestObject request)
            throws Exception {

        int institutionId = srk.getExpressState().getDealInstitutionId();
        int languageId = srk.getLanguageId();
        int productId = request.getProductId();
        
        MtgProd mtgProd = new MtgProd(srk, null, productId);
        componentTypeid = mtgProd.getComponentTypeId();
        componentTypeDescription = BXResources.getPickListDescription(
                institutionId, "COMPONENTTYPE", componentTypeid, languageId);

    }

}
