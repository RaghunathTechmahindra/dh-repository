/*
 * @(#)ServiceResponseObject.java    2006-6-29
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericResponseObject;

/**
 * ServiceResponseObject the generic response object service request.
 *
 * @version   1.0 2006-6-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ServiceResponseObject extends GenericResponseObject {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ServiceResponseObject.class);

    /**
     * Constructor function
     */
    public ServiceResponseObject() {
    }

    /**
     * set the response status.
     */
    public void setResponseStatus(int status) {

        put(RC_RESPONSE_STATUS, new Integer(status));
    }

    /**
     * set the request id for the given 
     */
    public void setRequestId(int id) {

        put("requestId", new Integer(id));
    }

    /**
     * set the copy id for transaction management.
     */
    public void setCopyId(int id) {

        put("copyId", new Integer(id));
    }

    /**
     * set the response id
     */
    public void setResponseId(int id) {

        put("responseId", new Integer(id));
    }
}
