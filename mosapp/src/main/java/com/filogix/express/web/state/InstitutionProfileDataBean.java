package com.filogix.express.web.state;

public class InstitutionProfileDataBean {

    private int institutionProfileId;
    private String institutionName;
    private String brandName;
    private int contactId;
    private int profileStatusId;
    private String CCAPSId;
    private String CMHCId;
    private String GEId;
    private int timeZoneEntryId;
    private double tranRate;
    private String ECNILenderId;
    private String equifaxId;
    private String mortyLenderId;
    private int interestCompoundId;
    private boolean threeYearRateCompareDiscount;
    private String CSClientNumber;
    private String CSMortgageNumber;
    private String ESClientNumber;
    private String ESMortgageNumber;
    private int REBackDateLimit;
    private int REEffectiveDateLimit;
    private boolean enableDupeCheckFlag;
    private int dupeCheckTimeFrame;
    private String AIGUGId;
    private String institutionAbbr;

    public int getInstitutionProfileId() {
        return institutionProfileId;
    }

    public void setInstitutionProfileId(int institutionProfileId) {
        this.institutionProfileId = institutionProfileId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public int getProfileStatusId() {
        return profileStatusId;
    }

    public void setProfileStatusId(int profileStatusId) {
        this.profileStatusId = profileStatusId;
    }

    public String getCCAPSId() {
        return CCAPSId;
    }

    public void setCCAPSId(String id) {
        CCAPSId = id;
    }

    public String getCMHCId() {
        return CMHCId;
    }

    public void setCMHCId(String id) {
        CMHCId = id;
    }

    public String getGEId() {
        return GEId;
    }

    public void setGEId(String id) {
        GEId = id;
    }

    public int getTimeZoneEntryId() {
        return timeZoneEntryId;
    }

    public void setTimeZoneEntryId(int timeZoneEntryId) {
        this.timeZoneEntryId = timeZoneEntryId;
    }

    public double getTranRate() {
        return tranRate;
    }

    public void setTranRate(double tranRate) {
        this.tranRate = tranRate;
    }

    public String getECNILenderId() {
        return ECNILenderId;
    }

    public void setECNILenderId(String lenderId) {
        ECNILenderId = lenderId;
    }

    public String getEquifaxId() {
        return equifaxId;
    }

    public void setEquifaxId(String equifaxId) {
        this.equifaxId = equifaxId;
    }

    public String getMortyLenderId() {
        return mortyLenderId;
    }

    public void setMortyLenderId(String mortyLenderId) {
        this.mortyLenderId = mortyLenderId;
    }

    public int getInterestCompoundId() {
        return interestCompoundId;
    }

    public void setInterestCompoundId(int interestCompoundId) {
        this.interestCompoundId = interestCompoundId;
    }

    public boolean isThreeYearRateCompareDiscount() {
        return threeYearRateCompareDiscount;
    }

    public void setThreeYearRateCompareDiscount(
            boolean threeYearRateCompareDiscount) {
        this.threeYearRateCompareDiscount = threeYearRateCompareDiscount;
    }

    public String getCSClientNumber() {
        return CSClientNumber;
    }

    public void setCSClientNumber(String clientNumber) {
        CSClientNumber = clientNumber;
    }

    public String getCSMortgageNumber() {
        return CSMortgageNumber;
    }

    public void setCSMortgageNumber(String mortgageNumber) {
        CSMortgageNumber = mortgageNumber;
    }

    public String getESClientNumber() {
        return ESClientNumber;
    }

    public void setESClientNumber(String clientNumber) {
        ESClientNumber = clientNumber;
    }

    public String getESMortgageNumber() {
        return ESMortgageNumber;
    }

    public void setESMortgageNumber(String mortgageNumber) {
        ESMortgageNumber = mortgageNumber;
    }

    public int getREBackDateLimit() {
        return REBackDateLimit;
    }

    public void setREBackDateLimit(int backDateLimit) {
        REBackDateLimit = backDateLimit;
    }

    public int getREEffectiveDateLimit() {
        return REEffectiveDateLimit;
    }

    public void setREEffectiveDateLimit(int effectiveDateLimit) {
        REEffectiveDateLimit = effectiveDateLimit;
    }

    public boolean isEnableDupeCheckFlag() {
        return enableDupeCheckFlag;
    }

    public void setEnableDupeCheckFlag(boolean enableDupeCheckFlag) {
        this.enableDupeCheckFlag = enableDupeCheckFlag;
    }

    public int getDupeCheckTimeFrame() {
        return dupeCheckTimeFrame;
    }

    public void setDupeCheckTimeFrame(int dupeCheckTimeFrame) {
        this.dupeCheckTimeFrame = dupeCheckTimeFrame;
    }

    public String getAIGUGId() {
        return AIGUGId;
    }

    public void setAIGUGId(String id) {
        AIGUGId = id;
    }

    public String getInstitutionAbbr() {
        return institutionAbbr;
    }

    public void setInstitutionAbbr(String institutionAbbr) {
        this.institutionAbbr = institutionAbbr;
    }
}
