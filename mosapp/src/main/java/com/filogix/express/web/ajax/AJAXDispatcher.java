/*
 * @(#)AJAXDispatcher.java    2006-4-23
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AJAXDispatcher defines the interfaces for processing a AJAX request.
 *
 * @version   1.0 2006-4-23
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface  AJAXDispatcher {

    /**
     * Dispatch an AJAX request to a AJAX request processor based on the given
     * HttpServletRequest and write the AJAX response back to the
     * HttpServletResponse.
     */
    public void processAJAXRequest(HttpServletRequest request,
                                   HttpServletResponse response)
        throws ServletException, IOException;
}
