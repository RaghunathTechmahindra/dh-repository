package com.filogix.express.web.rc.lpr.dao;

import java.util.ArrayList;
import java.util.List;

import com.basis100.deal.entity.MtgProd;
import com.basis100.resources.SessionResourceKit;

/**
 * Title: ProductTypeDaoImpl
 * <p>
 * Description: Data access object for ProductType 
 * @author MCM Impl Team.
 * @version 1.0 Aug 07, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public class ProductTypeDaoImpl implements ProductTypeDao {

    private int productTypeId;

    public int getProductTypeId() {
        return productTypeId;
    }
    
    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    /**
     * this method returns list, however it contains only 1 recored 
     * @see com.filogix.express.web.rc.lpr.dao.ProductTypeDao#findProductTypeListByProductId(com.basis100.resources.SessionResourceKit, int)
     */
    public List<ProductTypeDao> findProductTypeListByProductId(
            SessionResourceKit srk, int productId) throws Exception {

        ProductTypeDao pt = new ProductTypeDaoImpl();
        List<ProductTypeDao> list = new ArrayList<ProductTypeDao>();
        
        MtgProd mtgProd = new MtgProd(srk, null, productId);
        pt.setProductTypeId(mtgProd.getProductTypeId());
        list.add(pt);
        return list;
    }



}
