package com.filogix.express.web.rc.lpr.dao;

import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;

/**
 * Title: ComponentTypeDao
 * <p>
 * Description: Data access object for Component Description 
 * @author MCM Impl Team.
 * @version 1.0 Aug 14, 2008 artf762702 Initial version
 */
public interface ComponentTypeDao extends IPricingLogicDao {

    public String getComponentTypeDescription();
    public int getComponentTypeId();
    public void load(SessionResourceKit srk, LPRRequestObject request) throws Exception ;
}
