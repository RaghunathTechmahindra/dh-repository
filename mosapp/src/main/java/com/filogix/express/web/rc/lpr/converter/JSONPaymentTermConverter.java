package com.filogix.express.web.rc.lpr.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basis100.mtgrates.PaymentTermInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: JSONPaymentTermConverter
 * <p>
 * Description: JSON version of implementation of converter for paymentterm 
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class JSONPaymentTermConverter extends AbstractLegacyCSVConverter
        implements IConverter<PaymentTermInfo> {

    /**
     * this method converts a result data list from PricingLigc into a JSON String.
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<PaymentTermInfo> list, SessionResourceKit srk) {

        int institutionId = srk.getExpressState().getDealInstitutionId();
        int languageId = srk.getLanguageId();

        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        if(list != null)
        for (PaymentTermInfo payment : list) {

            String paymentTerm = BXResources.getPickListDescription(
                    institutionId, "PAYMENTTERM", payment.getPaymentTermId(),
                    languageId);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("productId", payment.getMtgProdId());
            map.put("paymentTermId", payment.getPaymentTermId());
            map.put("paymentTerm", paymentTerm);
            resultList.add(map);
        }
        res.setPaymentTerms(resultList);
        return res;
    }

}
