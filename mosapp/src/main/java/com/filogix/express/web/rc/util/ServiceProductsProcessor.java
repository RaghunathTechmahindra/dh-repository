/*
 * @(#)ServiceProductsProcessor.java    2006-4-25
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;

import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.GeneralRequestProcessorJato;

/**
 * ServiceProductsProcessor - 
 *
 * @version   1.0 2006-4-25
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ServiceProductsProcessor extends GeneralRequestProcessorJato {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ServiceProductsProcessor.class);

    /**
     * Constructor function
     */
    public ServiceProductsProcessor() {
    }

    /**
     * process the request.
     */
    public ResponseObject processRequest(RequestObject reqObject,
                                         ResponseObject respObj)
        throws RequestProcessException {

        // initializing JATO context.
        initializeContext();

        int languageId = getLanguageId();
        int providerId = ((Integer) reqObject.get("providerId")).intValue();
        int prodTypeId = ((Integer) reqObject.get("productTypeId"))
            .intValue();
        int prodSubTypeId = ((Integer) reqObject.get("productSubTypeId"))
            .intValue();

        // search the database to found out the available product ids.
        // TODO: using the sessionresourcekit: create a new one or using the
        // existing one?  Using the Entity?
        String querySQL =
            "SELECT SERVICEPRODUCTID FROM SERVICEPRODUCT " +
            "WHERE " +
            "SERVICEPROVIDERID = " + providerId + " AND " +
            "SERVICETYPEID = " + prodTypeId + " AND " +
            "SERVICESUBTYPEID = " + prodSubTypeId;
        if(_log.isDebugEnabled())
            _log.debug("Query SQL: " + querySQL);

        // the response object is a JavaScript array.
        ArrayList products = new ArrayList();
        SessionResourceKit srk = getSessionResourceKit();
        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (; jExec.next(key); ) {

                HashMap prod = new HashMap();
                Integer id = new Integer(jExec.getInt(key, 1));
                prod.put("value", id);
                // get the product description from the BXResources based on the
                // language id.
                String prodName = BXResources.
                    getPickListDescription(getDealInstitutionId(),
                                           "SERVICEPRODUCT", id.toString(),
                                           languageId);
                prod.put("label", prodName);

                products.add(prod);
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
            throw new RequestProcessException("JDBC Error!", e);
        } finally {
            srk.freeResources();
        }

        respObj.put("products", products);
        _log.info("Response Object: " + respObj.toString());
        return respObj;
    }
}
