/*
 * @(#)ExtractTextReportProcessor.java    2006-5-3
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;

/**
 * ExtractTextReportProcessor is an AJAX processor to extract text report from
 * persistence layer and send it to AJAX Rich Client.
 *
 * @version   1.0 2006-5-3
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExtractTextReportProcessor extends AbstractReportProcessor {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ExtractTextReportProcessor.class);

    /**
     * Constructor function
     */
    public ExtractTextReportProcessor() {
    }

    /**
     * extract the report and put it to response object.
     */
    public ResponseObject processRequest(RequestObject request,
                                         ResponseObject response)
        throws RequestProcessException {

        // the response id.
        int responseId = ((Integer) request.get("responseId")).intValue();
        String reportType = (String) request.get("reportType");

        _log.info("Trying to get the report[" + reportType +
                  "] for [" + responseId + "]");
        try {
            // the the extractor...
            TextResponseExtractor extractor =
                (TextResponseExtractor) getExtractor(reportType);
            // set the response id.
            extractor.setResponseId(responseId);
            // extract the report.
            String report = extractor.extractTextReport();
            // put it to the response.
            response.put("report", report);
        } catch (ExtractorNotFoundException ee) {
            _log.error("Can not find extractor for: " + reportType, ee);
            response.put(ResponseObject.RC_RESPONSE_ERRORS,
                         "Can not find extractor for: " + reportType);
            throw new RequestProcessException("Can not find extractor for:" +
                                              reportType, ee);
        } catch (ExtractReportException ere) {
            _log.error("Can not extract report for: " + responseId, ere);
            response.put(ResponseObject.RC_RESPONSE_ERRORS,
                         "Can not extract report for: " + responseId);
            throw new RequestProcessException("Can not extract reprt for:" +
                                              responseId, ere);
        } finally {
            return response;
        }
    }
}
