package com.filogix.express.web.rc.util;

import MosSystem.Mc;

public class PageInstitutionUtil {

    // The logger
    //private static Log _log = LogFactory.getLog(PageInstitutionUtil.class);

    public enum InstitutionUsage {
        DEAL_INSTITUION, ONE_OF_THE_USER_INSTITUTIONS, ALL_OF_THE_USER_INSTITUTIONS
    }

    public static InstitutionUsage getInstitutionUsage(int pageId) {

        switch (pageId) {
        case Mc.PGNM_INDIV_WORK_QUEUE_ID: //IWQ
        case Mc.PGNM_MASTER_WORK_QUEUE_ID: //MWQ
        case Mc.PGNM_MASTER_WORK_QUEUE_SEARCH_ID:
        case Mc.PGNM_CHANGE_PASSWORD_ID: //change password
        case Mc.PGNM_SIGN_ON_ID: //login 
        case Mc.PGNM_SIGNOFF_ID: //logout
        case Mc.PGNM_DEAL_SEARCH_ID:
            return InstitutionUsage.ALL_OF_THE_USER_INSTITUTIONS;
        case Mc.PGNM_DEAL_ENTRY_ID: // Deal Entry
        case Mc.PGNM_INTEREST_RATES_ADMIN: // Interest Rate Admin
        case Mc.PGNM_PARTY_SEARCH: // party search
        case Mc.PGNM_SOB_SEARCH: // source of business search
        case Mc.PGNM_USER_ADMININISTRATION: // User administration.
        case Mc.PGNM_QUICKLINK_ADMININISTRATION:	// quick link administration
            return InstitutionUsage.ONE_OF_THE_USER_INSTITUTIONS;
        default:
            return InstitutionUsage.DEAL_INSTITUION;
        }
    }

    public static boolean isTheScreenNeedsDealId(int pageId) {
        return (getInstitutionUsage(pageId) == InstitutionUsage.DEAL_INSTITUION);
    }
}
