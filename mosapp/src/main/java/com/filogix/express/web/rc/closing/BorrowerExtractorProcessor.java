/*
 * @(#)BorrowerExtractorProcessor.java    2006-6-21
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.closing;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.pk.DealPK;

import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.GeneralRequestProcessorJato;

/**
 * BorrowerExtractorProcessor will extract the primary borrower for the given
 * deal.
 *
 * @version   1.0 2006-6-21
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class BorrowerExtractorProcessor extends GeneralRequestProcessorJato {

    // The logger
    private static Log _log = LogFactory.getLog(BorrowerExtractorProcessor.class);

    /**
     * Constructor function
     */
    public BorrowerExtractorProcessor() {
    }

    /**
     * extract the borrow info.
     */
    public ResponseObject processRequest(RequestObject reqObject,
                                         ResponseObject respObj)
        throws RequestProcessException {

        // initializing...
        initializeContext();
        BERequestObject beRequest = (BERequestObject) reqObject;
        BEResponseObject beResponse = (BEResponseObject) respObj;

        SessionResourceKit srk = getSessionResourceKit();
        try {
            if (beRequest.getNeedPrimary()) {
                // find out the primary borrower.
                _log.info("Trying to find out the primary borrower's info");
                Deal deal = new Deal(srk, null);
                deal = deal.findByPrimaryKey(new DealPK(beRequest.getDealId(), beRequest.getCopyId()));
                for (Iterator i = deal.getBorrowers().iterator();
                     i.hasNext(); ) {
                    Borrower borrower = (Borrower) i.next();
                    if (borrower.isPrimaryBorrower()) {
                        _log.info("Found the primary borrower ["
                                  + borrower.getBorrowerId() + "]");
                        beResponse.setBorrowerId(borrower.getBorrowerId());
                        beResponse.
                            setFirstName(borrower.getBorrowerFirstName());
                        beResponse.
                            setLastName(borrower.getBorrowerLastName());

                        //work phone.
                        String phoneNumber = borrower.getBorrowerWorkPhoneNumber();
                        if(phoneNumber!=null) {
                            beResponse.setWorkAreaCode(phoneNumber.substring(0, 3));
                            beResponse.setWorkPhoneNumber(phoneNumber.substring(3));                            
                        }
                        beResponse.setWorkPhoneExt(borrower.getBorrowerWorkPhoneExtension());
                        
                        //home phone.
                        phoneNumber = borrower.getBorrowerHomePhoneNumber();
                        if(phoneNumber!=null) {
                            beResponse.setHomeAreaCode(phoneNumber.substring(0,3));
                            beResponse.setHomePhoneNumber(phoneNumber.substring(3));
                        }
                        //email
                        beResponse.setEmailAddress(borrower.getBorrowerEmailAddress());
                        
                        break;
                    }
                }
            } else {
                // we don't need the primary borrower info.
                int requestId = beRequest.getRequestId();
                int contactId = beRequest.getServiceRequestContactId();
                if(requestId > 0 && contactId > 0) {
                    ServiceRequestContact contact =
                        new ServiceRequestContact(srk, contactId, requestId,
                                                  beRequest.getCopyId());
                    //beResponse.set
                    // do nothing here.
                }
            }
        } catch (Exception e) {
            _log.error("", e);
            throw new RequestProcessException("", e);
        } finally {
            srk.freeResources();
        }

        return beResponse;
    }
}
