/*
 * @(#)RequestProcessException.java    2006-2-28
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

/**
 * RequestProcessException - 
 *
 * @version   1.0 2006-2-28
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class RequestProcessException extends Exception {

    /**
     * Constructor function
     */
    public RequestProcessException() {

        super();
    }

    /**
     * with message.
     */
    public RequestProcessException(String message) {

        super(message);
    }

    /**
     * with message and throwable.
     */
    public RequestProcessException(String message, Throwable th) {

        super(message, th);
    }

    /**
     * with throwable.
     */
    public RequestProcessException(Throwable th) {

        super(th);
    }
}
