package com.filogix.express.web.rc.lpr.converter;

/**
 * Title: AbstractLegacyCSVConverter
 * <p>
 * Description: utility class for legacy CSV converters
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public abstract class AbstractLegacyCSVConverter {

    public static final String LINE_DELIMITER = "^";
    public static final String COLUMN_DELIMITER = ",";


}
