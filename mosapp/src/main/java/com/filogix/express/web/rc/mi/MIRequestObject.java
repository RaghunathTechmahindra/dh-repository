package com.filogix.express.web.rc.mi;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericRequestObject;
import com.filogix.express.web.rc.avm.AVMRequestObject;

public class MIRequestObject extends GenericRequestObject{

//	 The logger
    private static Log _log = LogFactory.getLog(AVMRequestObject.class);
    
    /**
     * Constructor function
     */
    public MIRequestObject() {
    }

    /**
     * returns the request action.
     */
    public int getRequestAction() {

        return ((Integer) get(RC_REQUEST_ACTION)).intValue();
    }

//    /**
//     * return the request id.
//     */
    public int getRequestId() {

        return ((Integer) get("requestId")).intValue();
    }

    /**
     * returns the product id.
     */
    public int getProductId() {

        return ((Integer) get("productId")).intValue();
    } 

    /**
     * returns the Deal id.
     */
    public int getDealId() {

        return ((Integer) get("dealId")).intValue();
    }

    /**
     * returns the copy id.
     */
    public int getCopyId() {

        return ((Integer) get("copyId")).intValue();
    }

    /**
     * returns the language id.
     */
    public int getLanguageId() {

        return ((Integer) get("languageId")).intValue();
    }
    
    public int getDealStatusId() {

        return ((Integer) get("dealStatusId")).intValue();
    }
    
    public int getMiStatusId() {

        return ((Integer) get("miStatusId")).intValue();
    }
    
    /**
     * returns the institution profile id.
     */
    public int getInstitutionProfileId() {
        return ((Integer) get("institutionProfileId")).intValue();
    }
	
}
