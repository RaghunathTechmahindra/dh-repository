package com.filogix.express.web.rc.lpr.converter;

import java.util.List;

import com.basis100.mtgrates.LenderInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: LegacyLenderConverter
 * <p>
 * Description: Legacy CSV like data converter for lender 
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class LegacyLenderConverter extends AbstractLegacyCSVConverter implements
        IConverter<LenderInfo> {

    /**
     * this method converts a result data list from PricingLigc into a legacy CSV like String
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<LenderInfo> list, SessionResourceKit srk) {

        int institutionId = srk.getExpressState().getDealInstitutionId();
        int languageId = srk.getLanguageId();
        
        StringBuffer sb = new StringBuffer();
        if(list != null)
        for (LenderInfo lender : list) {
            
            String lenderName = BXResources.getPickListDescription(
                    institutionId, "LENDERPROFILE", lender.getLenderProfileId(), languageId);

            sb.append("'" + lenderName + "'").append(COLUMN_DELIMITER);
            sb.append("'" + lender.getLenderProfileId() + "'").append(LINE_DELIMITER);

        }
        res.setLendersString(sb.toString());
        return res;
    }

}
