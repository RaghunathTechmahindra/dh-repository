package com.filogix.express.web.rc.lpr.adapter;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.dao.ComponentTypeDao;

/**
 * Title: ComponentTypeAdapter
 * <p>
 * Description: implementation of adapter
 * @author MCM Impl Team.
 * @version 1.0 Aug 15, 2008: artf762702, Initial version
 */
public class ComponentTypeAdapter extends AbstractPricingLogicAdapter {
    
    private final static Log _log = LogFactory.getLog(ComponentTypeAdapter.class);
    
    public LPRResponseObject process(SessionResourceKit srk,
            LPRRequestObject request, LPRResponseObject response) {

        ComponentTypeDao dao = (ComponentTypeDao) pricingLogic;
        try {
            dao.load(srk, request);
        } catch (Exception e) {
          _log.error("failed in extracting data : " + e.getMessage());
          _log.error(StringUtil.stack2string(e));
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("compTypeDesc", dao.getComponentTypeDescription());
        map.put("compTypeId", dao.getComponentTypeId());
        

        response.setComponentType(map);
        return response;

    }

}
