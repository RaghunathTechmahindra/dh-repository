/*
 * @(#)AppraisalSummaryExtractor.java    2006-7-10
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.appraisal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.entity.AppraisalSummary;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;

import com.filogix.express.web.rc.util.BinaryResponseExtractor;
import com.filogix.express.web.rc.util.ExtractReportException;

/**
 * AppraisalSummaryExtractor - 
 *
 * @version   1.0 2006-7-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AppraisalSummaryExtractor extends BinaryResponseExtractor {

    // The logger
    private static Log _log = LogFactory.getLog(AppraisalSummaryExtractor.class);

    /**
     * Constructor function
     */
    public AppraisalSummaryExtractor() {
    }

    /**
     * returns the report type.
     */
    public String getReportContentType() {

        return "pdf";
    }

    /**
     * extract binary report.
     */
    public byte[] extractBinaryReport() throws ExtractReportException {

        SessionResourceKit srk = new SessionResourceKit("viewer");
        int responseId = getResponseId();
        try {
            if(_log.isDebugEnabled())
                _log.debug("Trying to get appraisal summary for response: [" +
                           responseId + "]");
            AppraisalSummary summary = new AppraisalSummary(srk, responseId);
            return summary.getAppraisalReport();
        } catch (RemoteException re) {
            _log.error("Remote exception: ", re);
            throw new ExtractReportException("Can not find reprt for:" +
                                             responseId, re);
        } catch (FinderException fe) {
            _log.error("Finder exception: ", fe);
            throw new ExtractReportException("Can not find reprt for:" +
                                             responseId, fe);
        } finally {
            srk.freeResources();
        }
    }
}
