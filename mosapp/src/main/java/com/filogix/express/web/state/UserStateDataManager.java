package com.filogix.express.web.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;

public class UserStateDataManager {

    private final static Log _log = LogFactory.getLog(UserStateDataManager.class);
    private Map<Integer, UserProfileDataBean> userProfileDataBeanMap = new HashMap<Integer, UserProfileDataBean>();
    private Map<Integer, InstitutionProfileDataBean> InstitutionProfileDataBeanMap = new HashMap<Integer, InstitutionProfileDataBean>();
    private List<Integer> institutionList = new ArrayList<Integer>();
    
    private UserStateDataManager(){
    }

    protected void addUserProfileDataBean(Integer institutionProfileId,
            UserProfileDataBean userProfileDataBean) {
        userProfileDataBeanMap.put(institutionProfileId, userProfileDataBean);
    }

    protected void addInstitutionProfileDataBean(Integer institutionProfileId,
            InstitutionProfileDataBean institutionProfileDataBean) {
        InstitutionProfileDataBeanMap.put(
                institutionProfileId, institutionProfileDataBean);
    }

    public UserProfileDataBean getUserProfileDataBean(Integer institutionProfileId){
        if(institutionList.contains(institutionProfileId) == false){
            throw new ExpressRuntimeException("invalid institutionId : " 
                    + institutionProfileId);
        }
        return userProfileDataBeanMap.get(institutionProfileId);
    }

    public InstitutionProfileDataBean getInstitutionProfileDataBean(Integer institutionProfileId){
        if(institutionList.contains(institutionProfileId) == false){
            throw new ExpressRuntimeException("invalid institutionId : " 
                    + institutionProfileId);
        }
        return InstitutionProfileDataBeanMap.get(institutionProfileId);
    }

    public Map<Integer, UserProfileDataBean> getUserProfileDataBeanMap() {
        return userProfileDataBeanMap;
    }

    public Map<Integer, InstitutionProfileDataBean> getInstitutionProfileDataBeanMap() {
        return InstitutionProfileDataBeanMap;
    }

    public boolean isMultiAccess() {
        return institutionList.size() > 1;
    }

    public List<Integer> institutionList(){
        return institutionList;
    }
    
    public static UserStateDataManager loadUserStateData(String userLogin){

        UserStateDataManager manager = new UserStateDataManager(); 
        SessionResourceKit srk = new SessionResourceKit(userLogin);
        srk.getExpressState().cleanAllIds();
        try{
            PropertyUtilsBean util = new PropertyUtilsBean();
            //findByLoginIdAllData: data order is decided by InstitutionProfile.instituutionSequence
            for(UserProfile up: new UserProfile(srk).findByLoginIdAllData(userLogin)){
                
                manager.institutionList.add(up.getInstitutionId());
                InstitutionProfile ip = 
                    new InstitutionProfile(srk).findByPrimaryKey(
                            new InstitutionProfilePK(up.getInstitutionId()));

                InstitutionProfileDataBean ipBean = new InstitutionProfileDataBean();
                util.copyProperties(ipBean, ip);
                manager.addInstitutionProfileDataBean(up.getInstitutionId(), ipBean);

                UserProfileDataBean upBean = new UserProfileDataBean();
                util.copyProperties(upBean, up);
                manager.addUserProfileDataBean(up.getInstitutionId(), upBean);
            }

        } catch (Exception e) {
            String msg = "Faild to load user state data";
            _log.error(msg);
            _log.error(e.getMessage());
            _log.error(StringUtil.stack2string(e));
            throw new ExpressRuntimeException(msg, e);
        }finally{
            srk.cleanTransaction();
            srk.freeResources();
        }
        return manager;
    }





}
