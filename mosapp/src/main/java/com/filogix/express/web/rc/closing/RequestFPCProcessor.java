/*
 * @(#)RequestFPCProcessor.java    2006-3-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.closing;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iplanet.jato.util.HtmlUtil;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.entity.CreateException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ServiceRequestContactPK;
import com.basis100.deal.pk.ServiceRequestPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;

import mosApp.MosSystem.ClosingActivityHandler;
import mosApp.MosSystem.PageHandlerCommon;

import com.filogix.externallinks.framework.ServiceExecutor;
import com.filogix.externallinks.framework.ServiceConst;

import com.filogix.express.web.util.service.FixPriceClosingHelper;
import com.filogix.express.web.rc.RequestProcessor;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;
import com.filogix.express.web.rc.request.ContactInfo4Request;

/**
 * RequestFPCProcessorEntity -
 *
 * @version   1.0 2006-3-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class RequestFPCProcessor extends AbstractRequestProcessorJato {


    // The logger
    private final static Log _log =
        LogFactory.getLog(RequestOCSProcessor.class);

    // the handler from JATO.
    private ClosingActivityHandler _handler = new ClosingActivityHandler();

    // the primary property id.
    private int _primaryPropertyId;

    /**
     * Constructor function
     */
    public RequestFPCProcessor() {
    }

    /**
     * process the request.
     */
    public ResponseObject processRequest(RequestObject request,
                                         ResponseObject response)
        throws RequestProcessException {

        FPCRequestObject fpcRequestObj = (FPCRequestObject) request;
        // get an instance of response object.
        FPCResponseObject fpcRespObject = (FPCResponseObject) response;

        // initializing the handler first.
        ClosingActivityHandler handler= _handler.cloneSS();
        initializeHandler(handler);
        SessionResourceKit srk = getSessionResourceKit();

        // get the releated request entity or create a new request.
        Request requestEntity = getRequestEntity(fpcRequestObj,
                                                 fpcRespObject, srk);

        try {
            // adopt the transaction copy.
            int newTxCopyId = adoptTransactionCopy(handler);
            fpcRespObject.setCopyId(newTxCopyId);
            fpcRespObject.setRequestId(requestEntity.getRequestId());
            fpcRespObject = updateResponseObject(requestEntity, fpcRespObject);

            // run the business rules.
            PassiveMessage pm = validateBusinessRules(handler, requestEntity.getRequestId(),
                                                      _primaryPropertyId);
            if (pm != null) {
                _log.info("Got Business Rule Validation Errors!");
                fpcRespObject.setResponseStatus(RequestObject.
                                                STATUS_BUSINESS_RULE_FAILURE);
                fpcRespObject.put(ResponseObject.RC_RESPONSE_ERRORS,
                                  preparePassiveMessage(pm));
                // TODO: update the requestentity..
            } else {
                fpcRespObject.setResponseStatus(RequestObject.
                                                STATUS_CREATE_REQUEST_SUCCESS);
                _log.info("sending out the request entity to Datx for Request" +
                          " [" + requestEntity.getRequestId() + ", " +
                          requestEntity.getCopyId() + "]");
                ServiceExecutor executor = new ServiceExecutor(srk);
                int result =
                    executor.processSyncRequest(requestEntity.getRequestId(),
                                                requestEntity.getCopyId(),
                                                getLanguageId());
                _log.info("Got the response result: " + result);
                // handle the result from external service.
                fpcRespObject = handleExtServiceResult(requestEntity,
                                                       fpcRespObject,
                                                       getLanguageId(), result,
                                                       srk);
            }
        } catch (Exception e) {
            _log.error("Sawallowed Exception: ", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }

        return fpcRespObject;
    }

    /**
     * return a request entity based on the fpc request object.
     */
    private Request getRequestEntity(FPCRequestObject request,
                                     FPCResponseObject response,
                                     SessionResourceKit srk)
        throws RequestProcessException {

        _log.info("Trying to get a Request entity ...");
        try {

            // should get the primary propery's property id.
            Deal deal = new Deal(srk, null);
            deal = deal.findByPrimaryKey(new DealPK(request.getDealId(),
                                                    request.getCopyId()));
            for (Iterator i = deal.getProperties().iterator(); i.hasNext(); ) {
                Property property = (Property) i.next();
                if (property.isPrimaryProperty()) {
                    if (_log.isDebugEnabled())
                        _log.debug("Found the primary property: " +
                                   property.getPropertyId());
                    _primaryPropertyId = property.getPropertyId();
                    break;
                }
            }

            Request requestEntity = new Request(srk);

            if (request.getRequestId() > 0) {
                _log.info("We are working on a existing request [" +
                          request.getRequestId() + "]");
                requestEntity = requestEntity.
                    findByPrimaryKey(new RequestPK(request.getRequestId(),
                                                   request.getCopyId()));

                // check the request's status to see what we need.
                switch (requestEntity.getRequestStatusId()) {
                case ServiceConst.REQUEST_STATUS_NOT_SUBMITTED:
                case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
                case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
                    // for these status, we just use this request entity to
                    // update. also need update the service request contact
                    // record before send out update request.
                    _log.info("Update request entity for status: " +
                              requestEntity.getRequestStatusId());
                    requestEntity =
                        updateRequestEntity(request, requestEntity, srk);
                    break;
                default:
                    _log.info("Even through, we got to create a new one!"
                              + " For status: "
                              + requestEntity.getRequestStatusId());
                    // for following status, we need create new request.
                    requestEntity = createRequestEntity(request, response, srk);
                    break;
                }
            } else {
                _log.info("Seems like it is the first time, just create a " +
                          "new request entity!");
                requestEntity = createRequestEntity(request, response, srk);
            }

            return requestEntity;
//        } catch (CreateException ce) {
//            _log.error("Can not create entities!", ce);
//            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
//                                                      getLanguageId());
//            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            _log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (FinderException fe) {
            _log.error("Can not create entities!", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, fe);
        } catch (Exception e) {
            _log.error("Any other exceptions!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
    }

    /**
     * create the request entity.
     */
    private Request createRequestEntity(FPCRequestObject request,
                                        FPCResponseObject response,
                                        SessionResourceKit srk)
        throws RequestProcessException {

        _log.info("Trying to create a new Request entity ...");
        try {
            srk.beginTransaction();

            // creating request entity.
            _log.info("creating request for deal: [dealid=" +
                      request.getDealId() +
                      ", copyid=" + request.getCopyId() + "]...");
            Request requestEntity = new Request(srk);
            requestEntity.create(new DealPK(request.getDealId(),
                                            request.getCopyId()),
                                 ServiceConst.REQUEST_STATUS_NOT_SUBMITTED,
                                 new Date(), srk.getExpressState().getUserProfileId());
            if (_log.isDebugEnabled())
                _log.debug("Request Entity Created: " +
                           requestEntity.toString());

            // preparing the channel info for the closing request.
//            Channel channel = new Channel(srk);
//            channel = channel.
//                findByServiceProductAndTransactionType(request.getProductId(),
//                                                       ServiceConst.
//                                                       SERVICE_TRANSACTION_TYPE_REQUEST);
            // updating the request entity.
//            requestEntity.setChannelId(channel.getChannelId());
            requestEntity.setChannelId(5);  // CHANNELID_CLOSING_FP_REQ
            requestEntity.setServiceProductId(request.getProductId());
            requestEntity.
                setServiceTransactionTypeId(ServiceConst.
                                            SERVICE_TRANSACTION_TYPE_REQUEST);
            requestEntity.
                setPayloadTypeId(ServiceConst.
                        SERVICE_PAYLOAD_TYPE_OSC_REQUEST);
            requestEntity.
                setRequestTypeId(ServiceConst.
                                 REQUEST_TYPE_CLOSING_FIXED_REQUEST_INT);
            requestEntity.setRequestDate(new Date());
            requestEntity.setUserProfileId(getSessionUserId());
            requestEntity.ejbStore();

            // creating servicerequest entity.
            _log.info("creating servicerequest for request: " +
                      requestEntity.getRequestId());
            ServiceRequest serviceRequest = new ServiceRequest(srk);
            serviceRequest = serviceRequest.create(requestEntity.getRequestId(),
                                                   requestEntity.getCopyId());
            // update other info.
            // should get the primary propery's property id.
            serviceRequest.setPropertyId(_primaryPropertyId);
            serviceRequest.ejbStore();
            if (_log.isDebugEnabled())
                _log.debug("ServiceRequest Created: " +
                           serviceRequest.toString());

            // create servicerequest contact
            ServiceRequestContact contact = new ServiceRequestContact(srk);
            if (request.getNeedPrimary()) {
                contact = createContactByBorrower(request, requestEntity,
                                                  contact, srk);
            } else {
                contact = contact.create(requestEntity.getRequestId(),
                                         requestEntity.getCopyId());
                contact = updateServiceRequestContact(contact, request);
            }
            contact.ejbStore();
            // we need update the page.
            response.
                setServiceRequestContactId(contact.
                                           getServiceRequestContactId());

            srk.commitTransaction();

            _log.info("Successfully created entities!");
            return requestEntity;
        } catch (CreateException ce) {
            _log.error("Can not create entities!", ce);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            _log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (JdbcTransactionException jte) {
            _log.error("Can not create entities!", jte);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, jte);
        } catch (Exception e) {
            _log.error("Any other exceptions!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
    }

    /**
     * create service request contact by use the borrower.
     */
    private ServiceRequestContact
        createContactByBorrower(FPCRequestObject request,
                                Request requestEntity,
                                ServiceRequestContact contact,
                                SessionResourceKit srk)
        throws Exception {

        Deal deal = new Deal(srk, null);
        deal = deal.findByPrimaryKey(new DealPK(request.getDealId(),
                                                request.getCopyId()));
        for (Iterator i = deal.getBorrowers().iterator(); i.hasNext(); ) {
            Borrower borrower = (Borrower) i.next();
            if (borrower.isPrimaryBorrower()) {
                contact = contact.
                    createWithBorrowerAssoc(requestEntity.getRequestId(),
                                            borrower.getBorrowerId(),
                                            requestEntity.getCopyId());
                updateServiceRequestContact(contact, borrower, request);
                break;
            }
        }

        return contact;
    }

    /**
     * update the service request eneity.
     */
    private Request updateRequestEntity(FPCRequestObject request,
                                        Request requestEntity,
                                        SessionResourceKit srk)
        throws Exception {

        // generic update.
        requestEntity.setServiceProductId(request.getProductId());
        requestEntity.setRequestDate(new Date());
        requestEntity.setUserProfileId(getSessionUserId());
        requestEntity.ejbStore();

        // get the existing contact.
        ServiceRequestContact contact =
            new ServiceRequestContact(srk,
                                      request.getServiceRequestContactId(),
                                      requestEntity.getRequestId(),
                                      requestEntity.getCopyId());
        if (request.getNeedPrimary()) {
            _log.info("Updating request entity by primary borrower");
            if (contact.getBorrowerId() > 0) {
                _log.info("we already use primary borrower...");
                // do nothing here...
            } else {
                _log.info("without to with primary borrower create ...");
                // find the borrower info.
                Borrower borrower = null;
                Deal deal = new Deal(srk, null);
                deal = deal.findByPrimaryKey(new DealPK(request.getDealId(),
                                                        request.getCopyId()));
                for (Iterator i = deal.getBorrowers().iterator();
                     i.hasNext(); ) {
                    borrower = (Borrower) i.next();
                    if (borrower.isPrimaryBorrower()) {
                        break;
                    }
                }
                if (borrower == null) {
                    throw new RequestProcessException("No Primary Borrower!");
                }
                requestEntity.createChildAssocs(borrower.getBorrowerId());
                contact.setBorrowerId(borrower.getBorrowerId());
                updateServiceRequestContact(contact, borrower, request);
                // don't touch comments for now.
                contact.ejbStore();
            }
        } else {
            _log.info("Updating request entity without primary borrower");
                // update the contact info.
            contact = updateServiceRequestContact(contact, request);
            contact.ejbStore();
            if (contact.getBorrowerId() > 0) {
                _log.info("with to without primary borrower. remove...");
                contact.cutBorrowerAssoc();
                requestEntity.removeChildAssocs();
            } else {
                _log.info("without to without primary borrower. do thing...");
            }
        }

        return requestEntity;
    }

    /**
     * trigger business rule engine and return the passive message aboject.
     */
    private PassiveMessage validateBusinessRules(ClosingActivityHandler handler,
                                                int requestId,
                                                 int propertyId)
        throws Exception {

        // create a Business Rule Executor.
        BusinessRuleExecutor brExecutor = new BusinessRuleExecutor();
        brExecutor.setCurrentBorrower(-1);
        // need set up the property id, should be the primary property id.
        brExecutor.setCurrentProperty(propertyId);
        // added for olga's problem - Sep 27, 2006 - Midori
        brExecutor.setCurrentRequest(requestId);

        // the business rule prefixes
        Vector rulePrefixes = new Vector();
        rulePrefixes.add("FPC-%");

        PassiveMessage passiveMessage =
            brExecutor.BREValidator(handler.getTheSessionState(),
                                    handler.
                                    getTheSessionState().getCurrentPage(),
                                    handler.getSessionResourceKit(),
                                    null,
                                    rulePrefixes);

        return passiveMessage;
    }

    /**
     * handle the external service result.
     */
    private FPCResponseObject handleExtServiceResult(Request requestEntity,
                                                     FPCResponseObject response,
                                                     int languageId,
                                                     int responseId,
                                                     SessionResourceKit srk)
        throws RequestProcessException {

        try {
            // reload the request entity.
            requestEntity.
                findByPrimaryKey(new RequestPK(requestEntity.getRequestId(),
                                               requestEntity.getCopyId()));
        } catch (FinderException fe) {
            _log.error("Can not find the request [requestId=" +
                       requestEntity.getRequestId() + ", copyId=" +
                       requestEntity.getCopyId() + "]", fe);
            throw new RequestProcessException("Can not find request!", fe);
        }
        int requestStatusId = requestEntity.getRequestStatusId();
        String statusMsg = requestEntity.getStatusMessage();
        switch (requestStatusId) {
        case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
        case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
        case ServiceConst.REQUEST_STATUS_FAILED:
            // setting up the response object.
            response.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
            response.put(ResponseObject.RC_RESPONSE_ERRORS,
                         "Can not get response from Datx: <br/>" +
                         statusMsg);
            break;
        default:
            _log.info("We got the response successfully!");
            // do nothing to the response object.
            response.setResponseStatus(RequestObject.STATUS_RESPONSE_SUCCESS);
            break;
        }

        response = updateResponseObject(requestEntity, response);

        // got to update the transaction copy's status.
        try {
            _log.info("Trying to update the transaction copy's status...");
            Request requestEntityCopy = new Request(srk);
            requestEntityCopy.
                findByPrimaryKey(new RequestPK(requestEntity.getRequestId(),
                                               getNewTxCopyId()));
            requestEntityCopy.
                setRequestStatusId(requestEntity.getRequestStatusId());
            requestEntityCopy.
                setStatusDate(requestEntity.getStatusDate());
            requestEntityCopy.
                setStatusMessage(requestEntity.getStatusMessage());
            requestEntityCopy.ejbStore();
        } catch (FinderException fe) {
            // should not happen.
            _log.error("Can not find the request [requestId=" +
                       requestEntity.getRequestId() + ", copyId=" +
                       getNewTxCopyId() + "]", fe);
            throw new RequestProcessException("Can not find request!", fe);
        } catch (RemoteException re) {
            _log.error("Can not update the new transaction copy!", re);
            throw new RequestProcessException("Can not update request!", re);
        }

        return response;
    }

    /**
     * update response object.
     */
    private FPCResponseObject updateResponseObject(Request requestEntity,
                                                   FPCResponseObject response) {

        // set the info for updating page DOM.
        String strRequestDate =
            HtmlUtil.format(requestEntity.getStatusDate(),
                            HtmlUtil.DATETIME_FORMAT_TYPE,
                            "MMM dd yyyy HH:mm",
                            new Locale(BXResources.
                                       getLocaleCode(getLanguageId()),
                                       "")
                            );
        response.setFpcRequestDate(strRequestDate);
        String status = BXResources.
            getPickListDescription(getDealInstitutionId(), "REQUESTSTATUS",
                                   requestEntity.getRequestStatusId(),
                                   getLanguageId());
        response.setFpcRequestStatus(status);

        return response;
    }

    /**
     * update  the contact info based on the request.
     */
    private ServiceRequestContact
        updateServiceRequestContact(ServiceRequestContact contact,
                                    FPCRequestObject request) {

        return
            FixPriceClosingHelper.
            updateServiceRequestContact(contact,
                                        (ContactInfo4Request) request);
    }

    /**
     * update the contact inf based on the primary borrower id.
     */
    private ServiceRequestContact
        updateServiceRequestContact(ServiceRequestContact contact,
                                    Borrower borrower,
                                    FPCRequestObject request) {

        return
            FixPriceClosingHelper.
            updateServiceRequestContact(contact, borrower,
                                        (ContactInfo4Request) request);
    }
}
