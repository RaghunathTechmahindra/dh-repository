package com.filogix.express.web.rc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;
import com.filogix.externallinks.services.ServiceInfo;

/**
 * This class was created as a middle layer between an AbstractRequestProcessor
 * and a specific Processor implementation, for the Simultanious requests.
 * In addition to clone RequestObject and ResponseObject in the super classs, we
 * need to clone our value object ServiceInfo. Otherwise, it will be shared between 
 * all clones.
 *
 * @since 3.3
 */
public abstract class AbstractSimultaniousRequestProcessor extends AbstractRequestProcessorJato {
    
    private static Log log = LogFactory.getLog(AbstractSimultaniousRequestProcessor.class);
    
    /*
     * ServiceInfo instance is now injected through the Spring Application 
     * Context ajaxProcessorContext-aigug.xml!!!
     */
    private ServiceInfo serviceInfo;
    
    
    public ServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    public void setServiceInfo(ServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }
}
