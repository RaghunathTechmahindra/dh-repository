package com.filogix.express.web.rc.useradmin;

import com.filogix.express.web.rc.GenericRequestObject;


public class UserAdminRequestObject extends GenericRequestObject{
	
	public UserAdminRequestObject(){
	}

	public String getUserLogin() {
		return ((String) get("userLoginId"));
	}
	
	public int getInstitutionId(){
		return ((Integer) get("institutionId")).intValue();
	}
	
}
