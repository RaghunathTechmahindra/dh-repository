/*
 * @(#)RichClientMessenger.java    2006-5-17
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.text.MessageFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * RichClientMessenger - 
 *
 * @version   1.0 2006-5-17
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class RichClientMessenger {

    // The logger
    private static final Log _log =
        LogFactory.getLog(RichClientMessenger.class);

    // the singlton instance.
    private static RichClientMessenger _instance;

    // some system error keys.
    // fatal session end.
    public static final String FATAL_ERROR = "FATAL_SESSION_ENDED";

    // initialie the singlton instance.
    static {
        if (_instance == null) {

            _instance =
                new com.filogix.express.web.rc.jato.JatoRichClientMessenger();
        }
    }

    /**
     * the static metods.
     */
    public static String getRichClientMsg(String key, int languageId) {

        return _instance.getRichClientMsg0(key, languageId);
    }

    public String getRichClientMsg(String key, int languageId,
                                   Object[] params) {

        return _instance.getRichClientMsg0(key, languageId, params);
    }

    /**
     * return the rich client message.
     */
    protected String getRichClientMsg0(String key, int languageId) {

        return key;
    }

    /**
     * return the rich client message with params populated.
     */
    protected String getRichClientMsg0(String key, int languageId,
                                   Object[] params) {

        String msg = getRichClientMsg(key, languageId);
        try {
            return MessageFormat.format(msg, params);
        } catch (IllegalArgumentException e) {
            _log.error(e.toString(), e);
            return msg;
        }
    }
}
