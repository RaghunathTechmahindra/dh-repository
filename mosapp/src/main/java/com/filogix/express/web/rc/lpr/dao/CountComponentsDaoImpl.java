package com.filogix.express.web.rc.lpr.dao;

import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;

/**
 * Title: ComponentEligibleCheckDaoImpl
 * <p>
 * Description: Data access object for counting Components
 * 
 * @author MCM Impl Team.
 * @version 1.0 Aug 07, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public class CountComponentsDaoImpl implements CountComponentsDao {

    protected int componentSize = 0;

    public int getComponentSize() {
        return componentSize;
    }

    public void load(SessionResourceKit srk, LPRRequestObject request)
            throws Exception {

        Deal deal = new Deal(srk, null, request.getDealId(), request
                .getDealCopyId());
        this.componentSize = deal.getComponents().size();

    }

}
