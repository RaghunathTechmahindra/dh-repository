/*
 * @(#)ExpressSQLConnectionManager.java    2007-8-1
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.jato;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iplanet.jato.RequestManager;
import com.iplanet.jato.SQLConnectionManagerBase;

import mosApp.MosSystem.SessionStateModel;
import mosApp.MosSystem.SessionStateModelImpl;

import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.datasource.DataSourceConnectionFactory;
import com.filogix.express.state.ExpressStateFactory;
import com.filogix.express.state.vpd.VPDStateDetectable;
import com.filogix.express.web.ExpressWebContext;

/**
 * ExpressSQLConnectionManager - 
 *
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressSQLConnectionManager extends SQLConnectionManagerBase {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressSQLConnectionManager.class);

    /**
     * Constructor function
     */
    public ExpressSQLConnectionManager() {
    }

    // the switcher for using IoC datasource or not.
    private static boolean _usingContext = true;

    /**
     * Set whether or not the IoC datasource is the one.
     */
    public static void setUsingContext(boolean value) {

        _usingContext = value;
    }

    /**
     * return current setting
     */
    public static boolean isUsingContext() {

        return _usingContext;
    }


    /**
     * Return a JDBC <code>Connection</code> from the specified datasource 
     */
    @Override
    public Connection getConnection(String dataSource) throws SQLException {


        // get datasource name from the manager.
        dataSource = getMappedDataSourceName(dataSource);
        _log.info("Using DataSourceFactory: " + dataSource);

        // trying to get express state in the session state.
        DataSourceConnectionFactory factory =
            (DataSourceConnectionFactory) ExpressWebContext.
            getService(dataSource);

        try {
            if (_log.isDebugEnabled())
                _log.debug("Trying to access the valid JATO session ...");
            // we need access the HTTPSession created by JATO framework.
            String defaultInstanceStateName =
                RequestManager.getRequestContext().getModelManager().
                getDefaultModelInstanceName(SessionStateModel.class);
            SessionStateModel theSessionState =
                (SessionStateModelImpl) RequestManager.getRequestContext().
                getModelManager().getModel(SessionStateModel.class,
                                           defaultInstanceStateName,
                                           true, true);
            return factory.getConnection((VPDStateDetectable)
                                         theSessionState.getExpressState());
        } catch (Exception e) {
            _log.warn("Could NOT get valid Session! REASON: " + e.getMessage());
            _log.warn("Using a void Express state for testing ...");
            ExpressStateFactory stateFactory =
                (ExpressStateFactory) ExpressWebContext.
                getService(ExpressWebContext.EXPRESS_STATE_FACTORY_KEY);
            return factory.getConnection((VPDStateDetectable)
                                         stateFactory.getExpressState());
        }
    }

    /**
     * Return a JDBC <code>Connection</code> from the specified datasource 
     */
    @Override
    public Connection getConnection(String dataSource, String user, 
        String password) throws SQLException {

        return getConnection(dataSource);
    }
}