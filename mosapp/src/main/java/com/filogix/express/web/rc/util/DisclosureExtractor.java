/*
 * @(#)DisclosureExtractor.java    2006-7-4
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;

/**
 * DisclosureExtractor - 
 *
 * @version   1.0 2006-7-4
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class DisclosureExtractor extends BinaryResponseExtractor {

    // The logger
    private static Log _log = LogFactory.getLog(DisclosureExtractor.class);

    /**
     * Constructor function
     */
    public DisclosureExtractor() {
    }

    /**
     * returns the report type.
     */
    public String getReportContentType() {

        return "pdf";
    }

    /**
     * extract binary report.
     */
    public byte[] extractBinaryReport() throws ExtractReportException {

        byte[] bytes;
        //SessionResourceKit srk = new SessionResourceKit("viewer");
        try {
            bytes = null;
        } catch (Exception e) {
            _log.error("Extracting Exception", e);
            throw new ExtractReportException(e.getMessage(), e);
        }

        return bytes;
    }
}
