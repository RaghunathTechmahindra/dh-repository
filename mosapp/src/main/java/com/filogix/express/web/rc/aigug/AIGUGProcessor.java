package com.filogix.express.web.rc.aigug;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import mosApp.MosSystem.MorgageInsHandler;
import mosApp.MosSystem.PageEntry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.basis100.resources.SessionResourceWrapper;
import com.filogix.express.web.rc.AbstractSimultaniousRequestProcessor;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.util.service.ValuationAVMHelper;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.services.ServiceDelegate;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.RequestService;
import com.iplanet.jato.RequestManager;

public class AIGUGProcessor extends AbstractSimultaniousRequestProcessor {//AbstractRequestProcessorJato

	private static Log log = LogFactory.getLog(AIGUGProcessor.class);

	private final int MI_INDICATOR_NOTREQUIRED = 0;
	
//	public ServiceInfo info = new ServiceInfo();

	private MorgageInsHandler _handler = new MorgageInsHandler();
	
	private int channelId;

	private int payloadTypeId;

	private int serviceTransactionTypeId;

	private int serviceProductId;
	
	private DealPK pk;
	
	private AIGUGResponseObject aigugResponeObject;
	private AIGUGRequestObject aigugRequestObject;

	public ResponseObject processRequest(RequestObject requestObj,
			ResponseObject responeObj) throws RequestProcessException {

		aigugResponeObject = (AIGUGResponseObject) responeObj;
		aigugRequestObject = (AIGUGRequestObject) requestObj;
		
		MorgageInsHandler handler = _handler.cloneSS();
		this.initializeHandler(handler);

		ServiceDelegate serviceDelegate = new ServiceDelegate();
		
		this.setServiceInfo(aigugRequestObject, handler);

		try {

			pk = new DealPK( getServiceInfo().getDealId(),
						getServiceInfo().getCopyId());
			// when deal's stausid in (23, 24) - Collapsed or denied
			// the deal's miindicator field has to be updated to 0 
			// for canselation request.
			updateDealStatusId(getServiceInfo());
			Request requestEntity = null;
			requestEntity = this.createRequest(getServiceInfo());
				
			// adopt the transaction copy
			int newTxCopyId = adoptTransactionCopy(handler);
			// set the adopted copy id
			createEntities4SR(aigugRequestObject, requestEntity, getServiceInfo().getSrk());
			getServiceInfo().setLanguageId(handler.theSessionState.getLanguageId());
			serviceDelegate.sendMIRequest(getServiceInfo());
			aigugResponeObject.setResponseStatus(RequestObject.STATUS_RESPONSE_SUCCESS);
			handleExtServiceResult(pk, aigugResponeObject);
		} catch (Exception e) {
			String message = StringUtil.stack2string(e);
			String detailedMessage = e.getMessage();
			log.info(message, e);
			String msg = null;
				
			if(RequestService.DATX_ERROR.equals(detailedMessage))
				msg = BXResources.getRichClientMsg("AIGUG_ERROR_SYSTEM_ANAVAILABLE",
							                           getLanguageId());
			else
				msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
						                              getLanguageId());
			aigugResponeObject
					.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
			aigugResponeObject.put(ResponseObject.RC_RESPONSE_ERRORS, msg);
			throw new RequestProcessException(msg, e);
			}

		return aigugResponeObject;
	}

	// helper methods

	private void setServiceInfo(AIGUGRequestObject aigugRequestObject, 
			MorgageInsHandler handler) {
		
		log.info("===== AIGUGProcessor:: setServiceInfo() start =====");
		
		PageEntry pg = handler.getTheSessionState().getCurrentPage();
		int dealId = pg.getPageDealId();
		int copyId = pg.getPageDealCID();
		SessionResourceKit srk = getSessionResourceKit();
		
		Deal deal;
		try {
			deal = new Deal(srk, null, dealId,copyId);
			int dealStatusId = deal.getStatusId();
			int miStatusId = deal.getMIStatusId();
			int productId = deal.getMortgageInsurerId();
			
			getServiceInfo().setDealId(new Integer(dealId));
			getServiceInfo().setCopyId(new Integer(copyId));
			getServiceInfo().setSrk(new SessionResourceWrapper(srk));
			getServiceInfo().setLanguageId(this.getLanguageId());
			getServiceInfo().setProductType(new Integer(productId));
			getServiceInfo().setDealStatusId(new Integer(dealStatusId));
			getServiceInfo().setMiStatusId(new Integer(miStatusId));
			
		} catch (RemoteException e) {
			log.error("RemoteException @AIGUGProcessor.setServiceInfo", e);
		} catch (FinderException e) {
			log.error("FinderException @AIGUGProcessor.setServiceInfo", e);
		} catch (Exception e) {
            log.error("Exception occured @AIGUGProcessor.setServiceInfo", e);
		}

		log.info("===== AIGUGProcessor:: setServiceInfo() end =====");
	}

	private int getPropertyId(SessionResourceKit srk)
			throws RequestProcessException {
		int propertyId = -1;

		Property property = null;
		try {
			property = new Property(srk);
			Collection properties = property.findByDealIdAndPrimaryFlag(getServiceInfo()
					.getDealId().intValue(), 'Y');
			Iterator it = properties.iterator();
			while (it.hasNext()) {
				Property prop = (Property) it.next();
				if (prop.getCopyId() == getServiceInfo().getCopyId().intValue()) {
					propertyId = prop.getPropertyId();
					break;
				}
			}
		} catch (Exception exception) {
			log.error("Can not find property by dealId = "
					+ getServiceInfo().getDealId().intValue(), exception);
			throw new RequestProcessException(exception.getMessage(), exception);
		}

		return propertyId;
	}

	private AIGUGResponseObject handleExtServiceResult(DealPK pk,
			AIGUGResponseObject aigugResponseObject)
			throws Exception {

		SessionResourceKit srk = getSessionResourceKit();
		Request reqEnt = new Request(srk);
		
		try {
			// reload the request entity for checking the status!			
			reqEnt = reqEnt.findLastRequest(pk);

		} catch (FinderException fe) {
			log.error("Can not find the request [requestId="
					+ reqEnt.getRequestId() + ", copyId=" + reqEnt.getCopyId()
					+ "]", fe);
			String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
					getLanguageId());
			throw new RequestProcessException(msg, fe);
		}

		int requestStatusId = reqEnt.getRequestStatusId();
		String statusMsg = reqEnt.getStatusMessage();
		switch (requestStatusId) {
		case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
        case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
		case ServiceConst.REQUEST_STATUS_FAILED:
			log.info("handling the failure response!");
			// prepare the error message.
			Object[] params = { statusMsg };
			String errorMsg = BXResources.getRichClientMsg(
					"SERVICE_AVM_ERROR_PROMPT", this.getLanguageId(), params);
			// setting up the response object.
			aigugResponseObject
					.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
			aigugResponseObject
					.put(ResponseObject.RC_RESPONSE_ERRORS, errorMsg);
			break;

		default:
			log.info("WE'VE GOT THE RESPONSE SUCCESSFULLY");
			// TODO ASK ME haw to handle response in default way (NO ERRORS)
			break;
		}
		return aigugResponseObject;
	}
	
	private AIGUGResponseObject checkResponse(ServiceInfo info, AIGUGResponseObject responseObject) throws RequestProcessException {
		if(log.isDebugEnabled())
			log.debug("=== AIGUGProcessor:: checkResponse() method started ===");
		
		if (info.getRequestId() < 0) {
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg);
        }
		
		responseObject.setResponseStatus(RequestObject.STATUS_CHECKING_RESPONSE);
		try {
			// find the DEAL entity into the DB by PK.
		    Deal deal = new Deal(info.getSrk(), info.getCalcMonitor());
		    DealPK dealPK = new DealPK(info.getDeal().getDealId(), 
                         			   info.getDeal().getCopyId());
		    deal = deal.findByPrimaryKey(dealPK);
		    
		    if(deal == null) {
		    	throw new RequestProcessException("ERROR:: DEAL NOT FOUND");
		    }
		    
		    // populate AIGUGResponseObject with the values from the DEAL entity.
		    responseObject.setMiResponse(deal.getMortgageInsuranceResponse());
		    responseObject.setMiPremiumAmount(deal.getMIPremiumAmount());
		    responseObject.setMiPolicyNumber(deal.getMIPolicyNumber());
		    responseObject.setCopyId(deal.getCopyId());
		    
		   log.info("***** MortgageInsuranceResponse: " + deal.getMortgageInsuranceResponse());
		   log.info("***** MIPremiumAmount: " + deal.getMIPremiumAmount());
		   log.info("***** MIPolicyNumber(): " + deal.getMIPolicyNumber());
            
		    // find the RESPONSE entity from the DB by ChannelTransactionKey
		    Response response = new Response(info.getSrk());
		    response = response.findByChannelTransactionKey(info.getChannelTransactionKey());
		    
		    if(response == null) {
		    	throw new RequestProcessException("ERROR:: RESPONSE NOT FOUND");
		    }
		    
		    // populate AIGUGResponseObject with the values from RESPONSE entity.
		    String statusMessage = response.getStatusMessage();
		    log.info("***** MIStstusMessage(): " + statusMessage);
		    if(statusMessage == null)
		    	statusMessage = "";
		    responseObject.setMiStatusMsg(statusMessage);
		    // find the id for the miStatusMessage in the PickList and populate 
		    // the AIGUGResponseObject with that value.
//		    String miStatusId = BXResources.getPickListDescriptionId("MISTATUS", statusMessage, info.getLanguageId());
		    int miStatusId = deal.getMIStatusId();
		    log.info("***** MIStatusId(): " + miStatusId);
		    responseObject.setMiStatusId(miStatusId);
		    
		    // populate AIGUGResponseObject with aditional values.
		    responseObject.setRequestId(info.getRequestId());
		    responseObject.setResponseStatus(RequestObject.STATUS_RESPONSE_SUCCESS);
		}
		catch(FinderException fe) {
			log.error("FINDEREXCEPTION: ", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED", getLanguageId());
            throw new RequestProcessException(msg, fe);
		}
		catch(RemoteException re) {
			log.error("REMOTEEXCEPTION: ", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED", getLanguageId());
            throw new RequestProcessException(msg, re);
		}
		catch(Exception e) {
			log.error("EXCEPTION: ", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED", getLanguageId());
            throw new RequestProcessException(msg, e);
		}
		
		if(log.isDebugEnabled())
			log.debug("=== AIGUGProcessor:: checkResponse() method finished ===");
		
		return responseObject;
	}
	
	/**
	 * Create a record in REQUEST Table
	 */
	protected Request createRequest(ServiceInfo info) throws Exception {
		Request request = null;
		SessionResourceKit srk = info.getSrk();
		try {
			Deal aDeal = new Deal(info.getSrk(), null);

			DealPK dealpk = new DealPK(info.getDealId().intValue(), info
					.getCopyId().intValue());
			aDeal.findByPrimaryKey(dealpk);
			request = new Request(srk);

			srk.beginTransaction();
			request.create(dealpk, 0, new Date(), aDeal
					.getUnderwriterUserId());
			request.setRequestTypeId(0);

			request.setDealId(info.getDealId().intValue());
			request.setCopyId(info.getCopyId().intValue());

			// ug setting
			request.setChannelId(getChannelId());
			request.setPayloadTypeId(getPayloadTypeId());
			request.setServiceTransactionTypeId(getServiceTransactionTypeId());
			request.setServiceProductId(getServiceProductId());

			request.ejbStore();
			srk.commitTransaction();

			info.setRequestId(request.getRequestId());
		} catch (Exception e) {
			srk.cleanTransaction();
			log.error("Can not create entities!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED", info.getLanguageId());
            throw new RequestProcessException(msg, e);
		}
		
		return request;
	}
	
	/**
     * create entities for scenario recommended copy.
     */
    private Request createEntities4SR(AIGUGRequestObject request,
                                      Request requestEntity,
                                      SessionResourceKit srk)
        throws RequestProcessException {

        if (log.isDebugEnabled())
            log.debug("Creating request entity tree for scenario " +
                       "recommended copy ...");
        try {
            int srCopyId =
                getCopyId4ScenarioRecommended(requestEntity.getDealId(),
                                              requestEntity.getCopyId(), srk);
            if (srCopyId == requestEntity.getCopyId()) {
                log.info("We are working on the scenario recommended copy " +
                          "right now!");
                // no need to update, just return.
                return null;
            } else {
                log.info("We are NOT working on the scenario recommenden copy!");
                srCopyId =
                    forceAdoptScenarioRecommended(requestEntity.getDealId(),
                                                  getDealEditControl(),
                                                  srCopyId, srk);
            }

            log.info("Creating for scenario recommended copy ...");
            Request requestEntitySR = createRequest(getServiceInfo());
            // revise the request id and copy id.
            //requestEntitySR.setRequestId(requestEntity.getRequestId());
            requestEntitySR.setCopyId(srCopyId);
            requestEntitySR.ejbStore();

            log.info("Successfully created the request for scenario " +
                      "recommended copy");
            getServiceInfo().setCopyId(new Integer(srCopyId));
            aigugResponeObject.setCopyId(srCopyId);
            return requestEntitySR;
        } catch (CreateException ce) {
            log.error("Can not create entities!", ce);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (FinderException fe) {
            log.error("Can not create entities!", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, fe);
        } catch (Exception e) {
            log.error("Can not create entities!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
    }
    
    private void updateDealStatusId(ServiceInfo info) throws Exception {
    	
    	if((Mc.DEAL_COLLAPSED == info.getDealStatusId().intValue() || 
    			Mc.DEAL_DENIED == info.getDealStatusId().intValue())
    			/* The cancelation requirements have been changed.
    			 * Now we have to be able to send the canselateion
    			 * request not only for these 3 mistatuses.
    			&& 
    			(Mc.MI_STATUS_APPROVED == info.getMiStatusId().intValue() || 
    			 Mc.MI_STATUS_APPROVAL_RECEIVED == info.getMiStatusId().intValue() || 
    			 Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED == info.getMiStatusId().intValue())*/) {
    		
    		SessionResourceKit srk = info.getSrk();
    		try {
    			Deal deal = new Deal(srk, null, info.getDealId().intValue(), info.getCopyId().intValue());
    			srk.beginTransaction();
    			deal.setMIIndicatorId(MI_INDICATOR_NOTREQUIRED);
    			deal.ejbStore();
    		}
    		catch(Exception e) {
    			srk.cleanTransaction();
    			log.error("Can not create entities!", e);
                String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED", info.getLanguageId());
                throw new RequestProcessException(msg, e);
    		}
    	}
    	
    }

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public int getPayloadTypeId() {
		return payloadTypeId;
	}

	public void setPayloadTypeId(int payloadTypeId) {
		this.payloadTypeId = payloadTypeId;
	}

	public int getServiceProductId() {
		return serviceProductId;
	}

	public void setServiceProductId(int serviceProductId) {
		this.serviceProductId = serviceProductId;
	}

	public int getServiceTransactionTypeId() {
		return serviceTransactionTypeId;
	}

	public void setServiceTransactionTypeId(int serviceTransactionTypeId) {
		this.serviceTransactionTypeId = serviceTransactionTypeId;
	}
	
}
