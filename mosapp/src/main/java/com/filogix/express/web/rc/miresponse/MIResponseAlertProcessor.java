package com.filogix.express.web.rc.miresponse;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.PageHandlerCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.security.DLM;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.GeneralRequestProcessorJato;

/**
 * 
 * MIResponseAlertProcessor
 * 
 * @version 1.0
 */
public class MIResponseAlertProcessor extends GeneralRequestProcessorJato {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(MIResponseAlertProcessor.class);

    /**
     * Constructor function
     */
    public MIResponseAlertProcessor() {
    }

    /**
     * Process Request
     */
    public ResponseObject processRequest(RequestObject requestObj,
            ResponseObject responseObj) {
        
        MIResponseAlertRequestObject miRequest = (MIResponseAlertRequestObject) requestObj;
        MIResponseAlertResponseObject resObj = (MIResponseAlertResponseObject) responseObj;

        PageHandlerCommon handler = new PageHandlerCommon();
        initializeHandler(handler);
        SessionResourceKit srk = handler.getSessionResourceKit();

        if (miRequest.getTimeOut()) {
            resObj.setSessionTimeOut(true);
            handler.handleSignOff(true, true);
        } else {
            try {
                PageEntry pg = handler.getTheSessionState().getCurrentPage();
                int dealId = pg.getPageDealId();

                boolean miResponse = false;
                miResponse = checkResponseExpected(dealId, _sessionUserId, srk);
                resObj.setPendingMIResponse(miResponse);

                boolean locked = false;
                if (DLM.getInstance().isLockedByUser(dealId, _sessionUserId,
                        srk)) {
                    locked = true;
                }
                resObj.setDealLock(locked);

            } catch (Exception e) {
                _logger.error(
                        "Error in MIResponseAlertProcessor processRequest", e);
            } finally {
                if (srk != null)
                    srk.freeResources();
            }
        }

        return resObj;
    }

    /**
     * MI response expected for this deal
     * 
     * @param dealId
     * @param userProfileId
     * @param srk
     * @return
     * @throws Exception
     */
    public boolean checkResponseExpected(int dealId, int userProfileId,
            SessionResourceKit srk) throws Exception {

        String query = "SELECT MIRESPONSEEXPECTED FROM DEALLOCKS WHERE DEALID = "
                + dealId + " AND USERPROFILEID = " + userProfileId;

        String result = "N";
        _logger.info("Query SQL: " + query);

        JdbcExecutor jExec = srk.getJdbcExecutor();
        int key = jExec.execute(query);
        for (; jExec.next(key);) {
            result = jExec.getString(key, 1);
            break;
        }
        jExec.closeData(key);

        return ("Y".equals(result) ? true : false);
    }

}
