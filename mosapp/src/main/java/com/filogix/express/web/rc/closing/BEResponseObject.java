/*
 * @(#)BEResponseObject.java    2006-6-21
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.closing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericResponseObject;

/**
 * BEResponseObject - 
 *
 * @version   1.0 2006-6-21
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class BEResponseObject extends GenericResponseObject {

    // The logger
    private final static Log _log = LogFactory.getLog(BEResponseObject.class);

    /**
     * Constructor function
     */
    public BEResponseObject() {
    }

    /**
     * borrower id.
     */
    public void setBorrowerId(int id) {

        put("borrowerId", new Integer(id));
    }

    /**
     * first name.
     */
    public void setFirstName(String value) {

        put("firstName", value);
    }

    /**
     * last name.
     */
    public void setLastName(String value) {

        put("lastName", value);
    }

    /**
     * email address.
     */
    public void setEmailAddress(String value) {

        put("emailAddress", value);
    }

    /**
     * workAreaCode.
     */
    public void setWorkAreaCode(String value) {

        put("workAreaCode", value);
    }

    /**
     * workPhoneNumber
     */
    public void setWorkPhoneNumber(String value) {

        put("workPhoneNumber", value);
    }

    /**
     * workPhoneExt
     */
    public void setWorkPhoneExt(String value) {

        put("workPhoneExt", value);
    }

    /**
     * cellAreaCode
     */
    public void setCellAreaCode(String value) {

        put("cellAreaCode", value);
    }

    /**
     * cellPhoneNumber
     */
    public void setCellPhoneNumber(String value) {

        put("cellPhoneNumber", value);
    }

    /**
     * homeAreaCode.
     */
    public void setHomeAreaCode(String value) {

        put("homeAreaCode", value);
    }

    /**
     * homePhoneNumber
     */
    public void setHomePhoneNumber(String value) {

        put("homePhoneNumber", value);
    }

    /**
     * comments.
     */
    public void setComments(String comments) {

        put("comments", comments);
    }
}
