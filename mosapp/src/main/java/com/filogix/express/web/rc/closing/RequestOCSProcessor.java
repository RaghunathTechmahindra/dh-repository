/*
 * @(#)RequestOCSProcessor.java    2006-3-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.closing;

import java.util.Date;
import java.util.Vector;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iplanet.jato.util.HtmlUtil;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.entity.CreateException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ServiceRequestPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;

import mosApp.MosSystem.ClosingActivityHandler;
import com.filogix.externallinks.framework.ServiceExecutor;
import com.filogix.externallinks.framework.ServiceConst;

import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;

/**
 * RequestOCSProcessorEntity -
 *
 * @version   1.0 2006-3-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class RequestOCSProcessor extends AbstractRequestProcessorJato {

    // The logger
    private final static Log _log =
        LogFactory.getLog(RequestOCSProcessor.class);

    // the handler from JATO.
    private ClosingActivityHandler _handler = new ClosingActivityHandler();

    // the primary property's id.
    private int _primaryPropertyId;

    // the primary borrower's id.
    private int _primaryBorrowerId;

    /**
     * Constructor function
     */
    public RequestOCSProcessor() {
    }

    /**
     * process the request.
     */
    public ResponseObject processRequest(RequestObject request,
            ResponseObject response)
        throws RequestProcessException {

        OCSRequestObject ocsRequestObj = (OCSRequestObject)request;
        // get an instance of response object.
        OCSResponseObject ocsRespObject = (OCSResponseObject)response;

        // initializing the handler first.
        ClosingActivityHandler handler= _handler.cloneSS();
        initializeHandler(handler);
        SessionResourceKit srk = getSessionResourceKit();

        // get the releated request entity or create a new request.
        Request requestEntity = getRequestEntity(ocsRequestObj, srk);

        try {
            // adopt the transaction copy.
            int newTxCopyId = adoptTransactionCopy(handler);
            ocsRespObject.setCopyId(newTxCopyId);
            ocsRespObject.setRequestId(requestEntity.getRequestId());
            ocsRespObject = updateResponseObject(requestEntity, ocsRespObject);

            if (ocsRequestObj.getIsCancel()) {
                _log.info("Closing Cancel Request!  " +
                          "Updating the request for Cancel...");
                requestEntity.
                    setServiceTransactionTypeId(ServiceConst.
                                                SERVICE_TRANSACTION_TYPE_CANCEL);
                requestEntity.
                    setPayloadTypeId(ServiceConst.
                            SERVICE_PAYLOAD_TYPE_OSC_CANCEL);
                requestEntity.setServiceTransactionTypeId(ServiceConst.
                        SERVICE_TRANSACTION_TYPE_CANCEL);
                
                requestEntity.ejbStore();
            }
            
            int requestStatus = requestEntity.getRequestStatusId();
            ServiceRequest serviceRequestEntity = 
                new ServiceRequest(getSessionResourceKit());
            serviceRequestEntity = serviceRequestEntity.
                findByPrimaryKey(new ServiceRequestPK(requestEntity.
                                                      getRequestId(),
                                                      requestEntity.
                                                      getCopyId()));            
            String buttonName = "request";

            switch (requestStatus) {
            case ServiceConst.REQUEST_STATUS_BLANK:
            case ServiceConst.REQUEST_STATUS_NOT_SUBMITTED:
            case ServiceConst.REQUEST_STATUS_CANCELLATION_CONFIRMED:
                buttonName = "request";
                break;
            case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
            case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
                if(serviceRequestEntity.getServiceProviderRefNo() == null ) {
                    buttonName = "request";
                }
                else {
                    buttonName = "update";
                }
                break;
            default:
                buttonName = "update";
                break;
            }

            // run the business rules.
            PassiveMessage pm = validateBusinessRules(handler,requestEntity.getRequestId(),
                                                        buttonName,
                                                      _primaryPropertyId, _primaryBorrowerId, 
                                                      ocsRequestObj.getIsCancel());
            if (pm != null) {
                _log.info("Got Business Rule Validation Errors!");
                ocsRespObject.setResponseStatus(RequestObject.
                                                STATUS_BUSINESS_RULE_FAILURE);
                ocsRespObject.put(ResponseObject.RC_RESPONSE_ERRORS,
                                  preparePassiveMessage(pm));
                
                
                if(buttonName.equalsIgnoreCase("request")){
                ocsRespObject.setOcsRequestStatusId(ServiceConst.
                                REQUEST_STATUS_NOT_SUBMITTED);

                String status = BXResources.
                    getPickListDescription(getDealInstitutionId(),
                                           "REQUESTSTATUS",
                                           ServiceConst.REQUEST_STATUS_NOT_SUBMITTED,
                                           getLanguageId());

                ocsRespObject.setOcsRequestStatus(status);
                requestEntity.setRequestStatusId(ServiceConst.
                                REQUEST_STATUS_NOT_SUBMITTED);

                requestEntity.ejbStore();

                Request requestTx = new Request(srk);
                requestTx = requestTx.findByPrimaryKey(
                        new RequestPK(requestEntity.getRequestId(), newTxCopyId));
                        requestTx.setRequestStatusId(ServiceConst.
                                REQUEST_STATUS_NOT_SUBMITTED);
                requestTx.ejbStore();
                }
                // TODO: prepare the status for rich client.
            } else {
                ocsRespObject.setResponseStatus(RequestObject.
                                                STATUS_CREATE_REQUEST_SUCCESS);
                _log.info("sending out the request entity to Datx for Request: " +
                          requestEntity.getRequestId());
                
                ServiceExecutor executor = new ServiceExecutor(srk);
                int result =
                    executor.processSyncRequest(requestEntity.getRequestId(),
                                                requestEntity.getCopyId(),
                                                getLanguageId());
                _log.info("Got the response entity id: " + result);
                
                ocsRespObject = handleExtServiceResult(requestEntity,
                                                       ocsRespObject,
                                                       getLanguageId(), result);
            }
        } catch (Exception e) {
            _log.error("Sawallowed Exception: ", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
        return ocsRespObject;
    }

    /**
     * return a request entity based on the ocs request object.
     */
    private Request getRequestEntity(OCSRequestObject request,
                                     SessionResourceKit srk)
        throws RequestProcessException {

        try {
            // should get the primary propery's property id.
            Deal deal = new Deal(srk, null);
            deal = deal.findByPrimaryKey(new DealPK(request.getDealId(),
                                                    request.getCopyId()));
            for (Iterator i = deal.getProperties().iterator(); i.hasNext(); ) {
                Property property = (Property) i.next();
                if (property.isPrimaryProperty()) {
                    if (_log.isDebugEnabled())
                        _log.debug("Found the primary property: " +
                                   property.getPropertyId());
                    _primaryPropertyId = property.getPropertyId();
                    break;
                }
            }

            // Ticket #5081: save the special instructions.
            deal.setSolicitorSpecialInstructions(request.getInstructions());
            deal.ejbStore();

            for (Iterator i = deal.getBorrowers().iterator(); i.hasNext(); ) {
                Borrower borrwer = (Borrower) i.next();
                if (borrwer.isPrimaryBorrower()) {
                    if (_log.isDebugEnabled())
                        _log.debug("Found the primary property: " +
                                borrwer.getBorrowerId());
                    _primaryBorrowerId = borrwer.getBorrowerId();
                    break;
                }
            }

            Request requestEntity = new Request(srk);
            
            if (request.getRequestId() > 0) {
                _log.info("We are working on a existing request [" +
                          request.getRequestId() + "]");
                requestEntity = requestEntity.
                    findByPrimaryKey(new RequestPK(request.getRequestId(),
                                                   request.getCopyId()));

                // check the request's status to see what we need.
                switch (requestEntity.getRequestStatusId()) {
                case ServiceConst.REQUEST_STATUS_BLANK:
                case ServiceConst.REQUEST_STATUS_CANCELLATION_CONFIRMED:
                    _log.info("Even through, we got to create a new one!");
                    // for following status, we need create new request.
                    requestEntity = createRequestEntity(request, srk);
                    break;
                default:
                    // for all other status, means update too!
                    requestEntity =
                        updateRequestEntity(request, requestEntity, srk);
                    break;
                }
            } else {
                _log.info("Seems like it is the first time, just create a " +
                          "new request entity!");
                requestEntity = createRequestEntity(request, srk);
            }

            return requestEntity;
//        } catch (CreateException ce) {
//            _log.error("Can not create entities!", ce);
//            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
//                                                      getLanguageId());
//            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            _log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (FinderException fe) {
            _log.error("Can not create entities!", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, fe);
        } catch (Exception e) {
            _log.error("Any other exceptions!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
    }

    /**
     * create the request entity.
     */
    private Request createRequestEntity(OCSRequestObject request,
                                        SessionResourceKit srk)
        throws RequestProcessException {

        try {
            srk.beginTransaction();

            // creating request entity.
            _log.info("creating request for deal: [dealid=" +
                      request.getDealId() +
                      ", copyid=" + request.getCopyId() + "]...");
            Request requestEntity = new Request(srk);
            requestEntity.create(new DealPK(request.getDealId(),
                                            request.getCopyId()),
                                 ServiceConst.REQUEST_STATUS_NOT_SUBMITTED,
                                 new Date(), getSessionUserId());
            if (_log.isDebugEnabled())
                _log.debug("Request Entity Created: " +
                           requestEntity.toString());

            // preparing the channel info for the closing request.
            requestEntity.setChannelId(5);  // CHANNELID_CLOSING_OCS_REQ
            requestEntity.setServiceProductId(request.getProductId());
            requestEntity.
                setServiceTransactionTypeId(ServiceConst.
                                            SERVICE_TRANSACTION_TYPE_REQUEST);
            requestEntity.
                setPayloadTypeId(ServiceConst.
                                    SERVICE_PAYLOAD_TYPE_OSC_REQUEST);
            requestEntity.setServiceTransactionTypeId(ServiceConst.
                    SERVICE_TRANSACTION_TYPE_REQUEST);
            requestEntity.setRequestDate(new Date());
            requestEntity.setUserProfileId(getSessionUserId());
            requestEntity.ejbStore();

            // creating servicerequest entity.
            _log.info("creating servicerequest for request: " +
                      requestEntity.getRequestId());
            ServiceRequest serviceRequest = new ServiceRequest(srk);
            serviceRequest = serviceRequest.create(requestEntity.getRequestId(),
                                                   requestEntity.getCopyId());
            // update other info.
            // should get the primary propery's property id.
            serviceRequest.setPropertyId(_primaryPropertyId);
            serviceRequest.ejbStore();

            srk.commitTransaction();

            if (_log.isDebugEnabled())
                _log.debug("ServiceRequest Created: " +
                           serviceRequest.toString());

            _log.info("Successfully created entities!");
            return requestEntity;
        } catch (CreateException ce) {
            _log.error("Can not create entities!", ce);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, ce);
        } catch (RemoteException re) {
            _log.error("Can not create entities!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        } catch (JdbcTransactionException jte) {
            _log.error("Can not create entities!", jte);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, jte);
        } catch (Exception e) {
            _log.error("Any other exceptions!", e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, e);
        }
    }

    /**
     * trigger business rule engine and return the passive message aboject.
     */
    private PassiveMessage validateBusinessRules(ClosingActivityHandler handler,
                                                int requestId,
                                                 String buttonName,
                                                 int propertyId, int borrowerId, 
                                                 boolean isCancel)
        throws Exception {
        // create a Business Rule Executor.
        BusinessRuleExecutor brExecutor = new BusinessRuleExecutor();
        brExecutor.setCurrentBorrower(borrowerId);
        // got to find out the primary property id.
        brExecutor.setCurrentProperty(propertyId);
        // added for olga's problem - Sep 27, 2006 - Midori
        brExecutor.setCurrentRequest(requestId);
        // the business rule prefixes
        Vector rulePrefixes = new Vector();
        if (isCancel)
            rulePrefixes.add("OCC-%");
        else if(buttonName.equalsIgnoreCase("request"))
            rulePrefixes.add("OCR-%");
        else if(buttonName.equalsIgnoreCase("update"))
            rulePrefixes.add("OCU-%");

        _log.info("OCS Biz Rule Cancel Button = " + isCancel);
        _log.info("OCS Biz Rule button" + buttonName);
        
        PassiveMessage passiveMessage =
            brExecutor.BREValidator(handler.getTheSessionState(),
                                    handler.
                                    getTheSessionState().getCurrentPage(),
                                    handler.getSessionResourceKit(),
                                    null,
                                    rulePrefixes);

        return passiveMessage;
    }

    /**
     * handle the external service result.
     */
    private OCSResponseObject handleExtServiceResult(Request requestEntity,
                                                     OCSResponseObject response,
                                                     int languageId,
                                                     int responseId)
        throws RequestProcessException {

        ServiceRequest serviceRequestEntity = null;

        try {
            // reload the request entity.
            requestEntity.
                findByPrimaryKey(new RequestPK(requestEntity.getRequestId(),
                                               requestEntity.getCopyId()));

            // get the related serviceRequestEntity.
            serviceRequestEntity = new ServiceRequest(getSessionResourceKit());
            serviceRequestEntity.
                findByPrimaryKey(new ServiceRequestPK(requestEntity.
                                                      getRequestId(),
                                                      requestEntity.
                                                      getCopyId()));
        } catch (FinderException fe) {
            _log.error("Can not find the request [requestId=" +
                    requestEntity.getRequestId() + ", copyId=" +
                    requestEntity.getCopyId() + "]", fe);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, fe);
        } catch (RemoteException re) {
            _log.error("Can not get the service request entity!", re);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED",
                                                      getLanguageId());
            throw new RequestProcessException(msg, re);
        }

        int requestStatusId = requestEntity.getRequestStatusId();
        String statusMsg = requestEntity.getStatusMessage();
        switch (requestStatusId) {
            case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
            case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
            case ServiceConst.REQUEST_STATUS_FAILED:
                Object[] params = {statusMsg};
                String errorMsg =
                    BXResources.getRichClientMsg("SERVICE_OCS_ERROR_PROMPT",
                                                 languageId, params);
                // setting up the response object.
                response.setResponseStatus(RequestObject.STATUS_RESPONSE_FAILURE);
                response.put(ResponseObject.RC_RESPONSE_ERRORS, errorMsg);
                break;
            default:
                _log.info("We got the response successfully!");
                response.setResponseStatus(RequestObject.STATUS_RESPONSE_SUCCESS);
                // do nothing to the response object.
                break;
        }

        // set the info for updating page DOM.
        String strRequestDate =
            HtmlUtil.format(requestEntity.getStatusDate(),
                            HtmlUtil.DATETIME_FORMAT_TYPE,
                            "MMM dd yyyy HH:mm",
                            new Locale(BXResources.
                                       getLocaleCode(getLanguageId()),
                                       "")
                            );
        response.setOcsRequestDate(strRequestDate);
        response.setOcsRequestStatusMsg(statusMsg);
        String status = BXResources.
            getPickListDescription(getDealInstitutionId(),
                                   "REQUESTSTATUS",
                                   requestEntity.getRequestStatusId(),
                                   languageId);
        response.setOcsRequestStatus(status);
        response.
            setOcsRequestStatusId(requestEntity.getRequestStatusId());
        response.
            setOcsServiceProviderRefNum(
                    serviceRequestEntity.getServiceProviderRefNo());

        return response;
    }
    
    /**
     * update the service request eneity.
     */
    private Request updateRequestEntity(OCSRequestObject request,
                                        Request requestEntity,                               
                                        SessionResourceKit srk)
        throws Exception {

        // generic update.
        requestEntity.
            setPayloadTypeId(ServiceConst.
                            SERVICE_PAYLOAD_TYPE_OSC_REQUEST);
        requestEntity.setServiceProductId(request.getProductId());
        requestEntity.setRequestDate(new Date());
        requestEntity.setUserProfileId(getSessionUserId());

        ServiceRequest serviceRequestEntity = new ServiceRequest(srk);
        serviceRequestEntity = serviceRequestEntity.
            findByPrimaryKey(new ServiceRequestPK(requestEntity.
                                                  getRequestId(),
                                                  requestEntity.
                                                  getCopyId()));
        // the transaction type.
        switch (requestEntity.getRequestStatusId()) {
        case ServiceConst.REQUEST_STATUS_BLANK:
        case ServiceConst.REQUEST_STATUS_NOT_SUBMITTED:
        case ServiceConst.REQUEST_STATUS_CANCELLATION_CONFIRMED:
            requestEntity.
                setServiceTransactionTypeId(ServiceConst.
                                            SERVICE_TRANSACTION_TYPE_REQUEST);
            break;
        case ServiceConst.REQUEST_STATUS_SYSTEM_ERROR:
        case ServiceConst.REQUEST_STATUS_PROCESSING_ERROR:
            if(serviceRequestEntity.getServiceProviderRefNo() == null ) {
                requestEntity.
                    setServiceTransactionTypeId(ServiceConst.
                                                SERVICE_TRANSACTION_TYPE_REQUEST);
            } else {
                requestEntity.
                    setServiceTransactionTypeId(ServiceConst.
                                                SERVICE_TRANSACTION_TYPE_UPDATE);
            }
            break;
        default:
            requestEntity.
                setServiceTransactionTypeId(ServiceConst.
                                            SERVICE_TRANSACTION_TYPE_UPDATE);
            break;
        }
        requestEntity.ejbStore();

        return requestEntity;
    }
    
    /**
     * update response object.
     */
    private OCSResponseObject updateResponseObject(Request requestEntity,
                                                   OCSResponseObject response) {

        // set the info for updating page DOM.
        String strRequestDate =
            HtmlUtil.format(requestEntity.getStatusDate(),
                            HtmlUtil.DATETIME_FORMAT_TYPE,
                            "MMM dd yyyy HH:mm",
                            new Locale(BXResources.
                                       getLocaleCode(getLanguageId()),
                                       "")
                            );
        response.setOcsRequestDate(strRequestDate);
        String status = BXResources.
            getPickListDescription(getDealInstitutionId(),
                                   "REQUESTSTATUS",
                                   requestEntity.getRequestStatusId(),
                                   getLanguageId());
        response.setOcsRequestStatus(status);
        response.setOcsRequestStatusId(requestEntity.getRequestStatusId());

        return response;
    }

}
