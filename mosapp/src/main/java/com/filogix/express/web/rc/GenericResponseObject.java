/*
 * @(#)GenericResponseObject.java    2006-3-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.util.HashMap;
import java.util.Properties;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * GenericResponseObject is the generic implementation for (@link
 * ResponseObject). 
 *
 * @version   1.0 2006-3-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class GenericResponseObject extends HashMap
    implements ResponseObject {

    // The logger
    private static Log _log = LogFactory.getLog(GenericResponseObject.class);

    // the response object properties' defintion.
    protected Map _responseProperties = null;

    /**
     * Constructor function
     */
    public GenericResponseObject() {
    }

    /**
     * set the response object properties' definition.
     */
    public void setResponseProperties(Map props) {

        _responseProperties = props;
    }

    /**
     * returns a generic propertie
     */
    public Map getResponseProperties() {

        if (_responseProperties == null) {
            _log.info("Response Object's properties are not defined! " +
                      "Using the default one!");
            _responseProperties = new Properties();
            _responseProperties.put("content", "String");
        }

        return _responseProperties;
    }

    /**
     * clone mehtod.
     */
    public Object clone() {

        GenericResponseObject result = (GenericResponseObject) super.clone();
        result.setResponseProperties(this.getResponseProperties());

        return result;
    }
}
