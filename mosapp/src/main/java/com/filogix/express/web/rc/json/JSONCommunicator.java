/*
 * @(#)JSONCommunicator.java    2006-2-21
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.json;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.json.JSONStringer;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.filogix.express.web.rc.AbstractCommunicator;
import com.filogix.express.web.rc.CommunicatorParseException;
import com.filogix.express.web.rc.CommunicatorSerializeException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.GenericRequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.GenericResponseObject;
import com.filogix.express.web.rc.JSObjectDescriptor;
import com.filogix.express.web.rc.JSArrayDescriptor;

/**
 * JSONCommunicator is the JSON implementation for @link(Communicator).
 *
 * @version   1.0 2006-2-21
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
 public class JSONCommunicator extends AbstractCommunicator {

    // The logger
    private static Log _log = LogFactory.getLog(JSONCommunicator.class);

    /**
     * Constructor function
     */
    public JSONCommunicator() {
    }

    /**
     * parse the request to a java object.
     */
    public RequestObject parseRequest(String request,
                                      RequestObject requestObj)
        throws CommunicatorParseException {

        try {
            if (_log.isDebugEnabled())
                _log.debug("Parsing request: " + request);
            // trying to get the reserved values.
            JSONObject jsonObj = new JSONObject(request);
            // the request action is not mandatory.
            if (jsonObj.has(RequestObject.RC_REQUEST_ACTION)) {
                int requestAction =
                    jsonObj.getInt(RequestObject.RC_REQUEST_ACTION);
                _log.info("set the rcRequestAction to: " + requestAction);
                requestObj.put(RequestObject.RC_REQUEST_ACTION,
                               new Integer(requestAction));
            }

            // parsing the content.
            JSONObject requestContent =
                jsonObj.getJSONObject(RequestObject.RC_REQUEST_CONTENT);
            // only pick what we get!
            Iterator i = requestContent.keys();
            Set propertiesSet = requestObj.getRequestProperties().keySet();
            while (i.hasNext()) {
                // get the property name from request content.
                String prop = (String) i.next();
                // the default type is String.
                Object type = "String";
                if (!propertiesSet.contains(prop)) {
                    _log.info("No definition for property: " + prop +
                              "!  Consider it as String!");
                } else {
                    // the property type.
                    type = requestObj.getRequestProperties().get(prop);
                }

                if (type instanceof String) {
                    // handle the primary type.
                    requestObj.put(prop,
                                   parseJSPrimary(requestContent,
                                                  prop, (String) type));
                } else if (type instanceof JSObjectDescriptor) {
                    // handle a JavaScript Object.
                    requestObj.put(prop,
                                   parseJSObject(requestContent.
                                                 getJSONObject(prop),
                                                 (JSObjectDescriptor) type));
                } else if (type instanceof JSArrayDescriptor) {
                    // handle a JavaScript Array.
                    requestObj.put(prop,
                                   parseJSArray(requestContent.
                                                getJSONArray(prop),
                                                (JSArrayDescriptor) type));
                } else {
                    // none supported type.
                    _log.warn("Property [" + prop +
                               "] has NOT Supported type: " + type +
                               "! Ignored!");
                    //throw new CommunicatorParseException(msg);
                }
            }
            return requestObj;
         } catch (JSONException je) {
            _log.error("JSON Exception: ", je);
            throw new
                CommunicatorParseException("Error during createing a JSON object.",
                                           je);
        }

    }

    /**
     * parse a JavaScript primary type.
     */
    protected Object parseJSPrimary(JSONObject jsonObject, String prop,
                                    String type)
        throws JSONException {

        if (type.equals("int")) {
            return new Integer(jsonObject.getInt(prop));
        } else if (type.equals("double")) {
            return new Double(jsonObject.getDouble(prop));
        } else if (type.equals("long")) {
            return new Long(jsonObject.getLong(prop));
        } else if (type.equals("boolean")) {
            return new Boolean(jsonObject.getBoolean(prop));
        } else {
            // default is String.
            return jsonObject.getString(prop);
        }
    }

    /**
     * parse a JavaScript Object.
     */
    protected Object parseJSObject(JSONObject jsonObj,
                                   JSObjectDescriptor desc)
        throws JSONException {

        // use a HashMap for a JavaScript object.
        Map retObj = new HashMap();
        Iterator i = jsonObj.keys();
        Set propertiesSet = desc.getObjectProperties().keySet();
        while (i.hasNext()) {
            String prop = (String) i.next();
            if (!propertiesSet.contains(prop)) {
                _log.info("No definition for property: " + prop +
                          "!  Just skip it!");
                continue;
            }
            Object type = desc.getObjectProperties().get(prop);
            if (type instanceof String) {
                // handle the primary type.
                retObj.put(prop,
                           parseJSPrimary(jsonObj,
                                          prop, (String) type));
            } else if (type instanceof JSObjectDescriptor) {
                // handle a JavaScript Object.
                retObj.put(prop,
                           parseJSObject(jsonObj.getJSONObject(prop),
                                         (JSObjectDescriptor) type));
            } else if (type instanceof JSArrayDescriptor) {
                // handle a JavaScript Array.
                retObj.put(prop,
                           parseJSArray(jsonObj.getJSONArray(prop),
                                        (JSArrayDescriptor) type));
            } else {
                // none supported type.
                _log.warn("Property [" + prop +
                           "] has NOT Supported type: " + type +
                           "! Ignored!");
                //throw new CommunicatorParseException(msg);
            }
        }
        return retObj;
    }

    /**
     * parse a JavaScript Array.
     */
    protected Object parseJSArray(JSONArray jsonArray,
                                  JSArrayDescriptor desc)
        throws JSONException {

        Object elementType = desc.getElementType();
        List retObj = new ArrayList();
        for (int i = 0; i < jsonArray.length(); i ++) {
            if (elementType instanceof JSObjectDescriptor) {
                retObj.add(parseJSObject(jsonArray.getJSONObject(i),
                                         (JSObjectDescriptor) elementType));
            } else if (elementType instanceof JSArrayDescriptor) {
                retObj.add(parseJSArray(jsonArray.getJSONArray(i),
                                        (JSArrayDescriptor) elementType));
            } else if (elementType instanceof String) {

                String type = (String) elementType;

                if (type.equals("int")) {
                    retObj.add(new Integer(jsonArray.getInt(i)));
                } else if (type.equals("double")) {
                    retObj.add(new Double(jsonArray.getDouble(i)));
                } else if (type.equals("long")) {
                    retObj.add(new Long(jsonArray.getLong(i)));
                } else if (type.equals("boolean")) {
                    retObj.add(new Boolean(jsonArray.getBoolean(i)));
                } else {
                    // default is String.
                    retObj.add(jsonArray.getString(i));
                }
            } else {
                // none supported type.
                _log.warn("NOT Supported type: " + elementType +
                           "! Ignored!");
            }
        }

        return retObj;
    }

    /**
     * serialize the response as a JSON string.
     */
    public String serializeResponse(ResponseObject respObject)
        throws CommunicatorSerializeException {

        if (respObject.isEmpty()){
            return "";
        }

        if (_log.isDebugEnabled())
            _log.debug("Trying to serialize response object: "
                       + respObject.toString());

        try {
            // create a JSON Stringer to serialize the response object.
            JSONStringer jsonStr = new JSONStringer();
            jsonStr.object();
            // serialize the reserve fields.
            jsonStr = serializeReservedFields(jsonStr, respObject);
            // the response content.
            jsonStr.key(ResponseObject.RC_RESPONSE_CONTENT)
                .object();
            // get the response object's properties.
            // we only pick the one from the Java response object.
            Iterator i = respObject.keySet().iterator();
            Set propertiesSet = respObject.getResponseProperties().keySet();
            while (i.hasNext()) {
                String propName = (String) i.next();
                _log.debug("Serializing property: " + propName);
                if (!propertiesSet.contains(propName)) {
                    _log.info("No definition for property: " + propName +
                              "!  Just skip it");
                    continue;
                }
                Object propValue = respObject.get(propName);
                if (propValue == null) {
                    _log.info("NULL value! Skip it...");
                    continue;
                }
                Object propType =
                    respObject.getResponseProperties().get(propName);
                if (propType instanceof String) {
                    // it is a primary type.
                    jsonStr.key(propName);
                    serializeJSPrimary(jsonStr, propValue, (String) propType);
                } else if (propType instanceof JSArrayDescriptor) {
                    // It is a JavaScript Array type.
                    jsonStr.key(propName);
                    List array = (List) propValue;
                    serializeJSArray(jsonStr, array,
                                     (JSArrayDescriptor) propType);
                } else if (propType instanceof JSObjectDescriptor) {
                    // it is a JavaScript Object type.
                    jsonStr.key(propName);
                    Map obj = (Map) propValue;
                    serializeJSObject(jsonStr, obj,
                                      (JSObjectDescriptor) propType);
                }
            }
            jsonStr.endObject()
                .endObject();

            if (_log.isDebugEnabled()) {
                _log.debug("Serialized Response Object: " +
                           jsonStr.toString() + ".");
            }
            return jsonStr.toString();
        } catch (JSONException je) {
            _log.error("JSON Exception: ", je);
            throw
                new CommunicatorSerializeException("Error on serializing a JSON object.", je);
        }
    }

    /**
     * serializing the reserved fields.
     */
    private JSONStringer serializeReservedFields(JSONStringer jsonStringer,
                                                 ResponseObject respObject)
        throws JSONException {

        if (respObject.keySet().contains(ResponseObject.RC_RESPONSE_STATUS)) {
            // found RC_RESPONSE_STATUS.
            if (_log.isDebugEnabled()) {
                _log.debug("Found property: " +
                           ResponseObject.RC_RESPONSE_STATUS + ".");
            }
            jsonStringer.key(ResponseObject.RC_RESPONSE_STATUS)
                .value(((Integer) respObject.get(ResponseObject.
                                                 RC_RESPONSE_STATUS))
                       .intValue());
        }

        if (respObject.keySet().contains(ResponseObject.RC_RESPONSE_ERRORS)) {
            // found RC_RESPONSE_ERRORS
            if (_log.isDebugEnabled()) {
                _log.debug("Found property: " +
                           ResponseObject.RC_RESPONSE_ERRORS + ".");
            }
            jsonStringer.key(ResponseObject.RC_RESPONSE_ERRORS)
                .value((String) respObject.
                       get(ResponseObject.RC_RESPONSE_ERRORS));
        }

        return jsonStringer;
    }

    /**
     * serialize a JavaScript primary type.
     */
    protected JSONStringer serializeJSPrimary(JSONStringer jsonStringer,
                                              Object obj, String type)
        throws JSONException {

        if (type.equals("int")) {
            Integer value = (Integer) obj;
            jsonStringer.value(value.intValue());
        } else if (type.equals("double")) {
            Double value = (Double) obj;
            jsonStringer.value(value.doubleValue());
        } else if (type.equals("long")) {
            Long value = (Long) obj;
            jsonStringer.value(value.longValue());
        } else if (type.equals("boolean")) {
            Boolean value = (Boolean) obj;
            jsonStringer.value(value.booleanValue());
        } else {
            // default is String.
            jsonStringer.value(obj.toString());
        }

        return jsonStringer;
    }

    /**
     * serialize a JavaScript Array object.
     */
    protected JSONStringer serializeJSArray(JSONStringer jsonStringer,
                                            List array,
                                            JSArrayDescriptor desc)
        throws JSONException {

        Object elementType = desc.getElementType();
        // array start
        jsonStringer.array();
        if (array == null) {
            // do nothing here, just return a empty javascript array.
        } else if (elementType instanceof JSArrayDescriptor) {
            // if it is an array of array.
            for (Iterator i = array.iterator(); i.hasNext(); ) {
                // TODO: should check the value type.
                serializeJSArray(jsonStringer, (List) i.next(),
                                 (JSArrayDescriptor) elementType);
            }
        }  else if (elementType instanceof JSObjectDescriptor) {
            // if it is an array of object.
            for (Iterator i = array.iterator(); i.hasNext(); ) {
                // TODO: should be a Map.
                serializeJSObject(jsonStringer, (Map) i.next(),
                                  (JSObjectDescriptor) elementType);
            }
        } else {
            // the default is JavaScript primary type
            for (Iterator i = array.iterator(); i.hasNext(); ) {
                serializeJSPrimary(jsonStringer, i.next(),
                                   (String) elementType);
            }
        }
        // array end.
        jsonStringer.endArray();

        return jsonStringer;
    }

    /**
     * serialize a JavaScript Object.
     */
    protected JSONStringer serializeJSObject(JSONStringer jsonStringer,
                                             Map object,
                                             JSObjectDescriptor desc)
        throws JSONException {

        jsonStringer.object();
        // get the response object's properties.
        // only pick what we have.
        Iterator i = object.keySet().iterator();
        Set propertiesSet = desc.getObjectProperties().keySet();
        while (i.hasNext()) {
            String propName = (String) i.next();
            if (!propertiesSet.contains(propName)) {
                _log.warn("No definition for property: " + propName +
                          "!  Just skip it");
                continue;
            }
            Object propType =
                desc.getObjectProperties().get(propName);
            if (propType instanceof String) {
                // it is a primary type.
                jsonStringer.key(propName);
                serializeJSPrimary(jsonStringer, object.get(propName),
                                   (String) propType);
            } else if (propType instanceof JSArrayDescriptor) {
                // It is a JavaScript Array type.
                jsonStringer.key(propName);
                List array = (List) object.get(propName);
                serializeJSArray(jsonStringer, array,
                                 (JSArrayDescriptor) propType);
            } else if (propType instanceof JSObjectDescriptor) {
                // it is a JavaScript Object type.
                jsonStringer.key(propName);
                Map jsObj = (Map) object.get(propName);
                serializeJSObject(jsonStringer, jsObj,
                                  (JSObjectDescriptor) propType);
            }
        }
        jsonStringer.endObject();

        return jsonStringer;
    }
}
