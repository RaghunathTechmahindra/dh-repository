/*
 * @(#)AVMResponseObject.java    2006-3-7
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.avm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.response.ServiceResponseObject;

/**
 * AVMResponseObject is the helper to set the info to response.
 *
 * @version   1.0 2006-3-7
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AVMResponseObject extends ServiceResponseObject {

    // The logger
    private final static Log _log = LogFactory.getLog(AVMResponseObject.class);

    /**
     * Constructor function
     */
    public AVMResponseObject() {
    }

    /**
     * the provider name.
     */
    public void setProviderName(String name) {

        put("providerName", name);
    }

    /**
     * the responseDate as string.
     */
    public void setResponseDate(String date) {

        put("responseDate", date);
    }

    /**
     * the avm value.
     */
    public void setAvmValue(double value) {

        put("avmValue", new Double(value));
    }

    /**
     * the formated avm value.
     */
    public void setStrAvmValue(String value) {

        put("strAvmValue", value);
    }

    /**
     * the confidencelevel.
     */
    public void setConfidenceLevel(String level) {

        put("confidenceLevel", level);
    }

    /**
     * the high AVM range.
     */
    public void setHighAvmRange(String value) {

        put("highAvmRange", value);
    }

    /**
     * the low AVM range.
     */
    public void setLowAvmRange(String value) {

        put("lowAvmRange", value);
    }

    /**
     * the AVM status
     */
    public void setAvmStatus(String value) {

        put("avmStatus", value);
    }

    /**
     * the AVM status message.
     */
    public void setAvmStatusMsg(String value) {

        put("avmStatusMsg", value);
    }

    /**
     * the AVM request date.
     */
    public void setAvmDate(String value) {

        put("avmDate", value);
    }

    /**
     * the valuation day.
     */
    public void setValuationDay(int value) {

        put("valuationDay", new Integer(value));
    }

    /**
     * the valuation month.
     */
    public void setValuationMonth(int value) {

        put("valuationMonth", new Integer(value));
    }

    /**
     * the valuation year.
     */
    public void setValuationYear(int value) {

        put("valuationYear", new Integer(value));
    }
}
