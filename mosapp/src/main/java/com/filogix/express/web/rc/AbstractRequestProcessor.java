/*
 * @(#)AbstractRequestProcessor.java    2006-4-24
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * AbstractRequestProcessor is a helper class to provide some general methods
 * for an implementation of RequestProcessor.
 *
 * @version   1.0 2006-4-24
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public abstract class AbstractRequestProcessor implements RequestProcessor {

    // The logger
    private final static Logger _log =
        LoggerFactory.getLogger(AbstractRequestProcessor.class);

    /**
     * an implementation of RequestObject.
     */
    protected RequestObject _requestObject;

    /**
     * get the request Object.
     */
    public RequestObject getRequestObject() {

        if (_requestObject == null) {
            _log.info("No RequestObject specified, using the " +
                      "GenericRequestObject!");
            _requestObject = new GenericRequestObject();
        }

        return _requestObject;
    }

    /**
     * set up the request object.
     */
    public void setRequestObject(RequestObject requesObject) {

        _requestObject = requesObject;
    }

    /**
     * an implementation of ResponseObject.
     */
    protected ResponseObject _responseObject;

    /**
     * return the response object.
     */
    public ResponseObject getResponseObject() {

        if (_responseObject == null) {
            _log.info("No Response Object specifice. " +
                      "using the GenericResponseObject!");
            _responseObject = new GenericResponseObject();
        }

        return _responseObject;
    }

    /**
     * set up the response object.
     */
    public void setResponseObject(ResponseObject responseObject) {

        _responseObject = responseObject;
    }

    /**
     * we need a communicator to take care of parsing and serializing.
     */
    protected Communicator _communicator;

    /**
     * set up the communicator.
     */
    public void setCommunicator(Communicator communicator) {

        if (_log.isDebugEnabled())
            _log.debug("The Communicator: [{}]", communicator);
        _communicator = communicator;
    }

    /**
     * get the communicator
     */
    public Communicator getCommunicator()
        throws CommunicatorNotFoundException {

        if (_communicator == null) {
            _log.error("Communicator not specified! " +
                       "A Communicator must be specified!");
            throw new
                CommunicatorNotFoundException("No Communicator available!");
        }

        return _communicator;
    }

    /**
     * process the request.
     */
    public String processRequest(String request,
                                 HttpServletResponse httpResponse) {

        try {
            // parse teh request object first.
            RequestObject requestObj =
                getCommunicator().parseRequest(request,
                                               getRequestObject());
            processRequest(requestObj, getResponseObject(),
                           httpResponse);
        } catch (CommunicatorNotFoundException ce) {
            _log.error("Can not find Communicator!", ce);
            // setting up the error message to response object.
        } catch (CommunicatorParseException cpe) {
            _log.error("Parse Request Error!", cpe);
            // responseObj = new GenericResponseObject();
        } catch (RequestProcessException rpe) {
            _log.error("Processing Request Error!", rpe);
            getResponseObject().put(ResponseObject.RC_RESPONSE_ERRORS,
                                    rpe.getMessage());
        }

        // the return response string.
        String response = "";
        try {
            // serialize the response object.
            response = _communicator.serializeResponse(getResponseObject());
        } catch (CommunicatorSerializeException cse) {
            _log.error("Error when try to Serialize Response Object: " +
                       getResponseObject().toString());
            _log.error("Serialize Response Error!", cse);
            response = _communicator.getSerializeFailureMsg();
        }

        return response;
    }

    /**
     * the default implementaion.
     */
    public ResponseObject processRequest(RequestObject request,
                                         ResponseObject response,
                                         HttpServletResponse httpResponse)
        throws RequestProcessException {

        // by default we don't need the http response.
        return processRequest(request, response);
    }
}
