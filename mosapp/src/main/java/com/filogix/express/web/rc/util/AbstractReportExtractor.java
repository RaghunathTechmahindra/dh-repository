/*
 * @(#)AbstractReportExtractor.java    2006-7-4
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * AbstractReportExtractor - 
 *
 * @version   1.0 2006-7-4
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractReportExtractor implements ReportExtractor {

    // The logger
    private final static Log _log =
        LogFactory.getLog(AbstractReportExtractor.class);

    // the response id
    protected int _responseId;

    /**
     * set the response id.
     */
    public void setResponseId(int id) {

        _responseId = id;
    }

    /**
     * get response id.
     */
    public int getResponseId() {

        return _responseId;
    }

    /**
     * prepare criterias for extracting the right report.
     */
    public List prepareCriterias(List criterias) {

        // do nothing for now.
        return null;
    }

    /**
     * public extract repost.
     */
    public Object extractReport() throws ExtractReportException {

        return null;
    }
}
