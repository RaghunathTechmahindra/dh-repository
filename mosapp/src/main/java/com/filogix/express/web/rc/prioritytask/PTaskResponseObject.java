package com.filogix.express.web.rc.prioritytask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericResponseObject;

/**
 * Response Object for unacknowledged tasks checking.
 * @author bzhang
 */
public class PTaskResponseObject extends GenericResponseObject{

	private static Log _log = LogFactory.getLog(PTaskResponseObject.class);

	public PTaskResponseObject(){
	}
	
	public void setTaskFoundString(String taskFoundString) {
		put("taskFoundString", taskFoundString);
	}
    
    public void setSessionTimeOut(boolean sessionTimeOut) {
        put("sessionTimeOut", sessionTimeOut);
    }
}
