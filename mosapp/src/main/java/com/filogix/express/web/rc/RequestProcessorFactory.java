/*
 * @(#)RequestProcessorFactory.java    2006-2-28
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

/**
 * RequestProcessorFactory implements the factory pattern to get the right
 * {@link ServiceProcessor} instance based on the input service type, or
 * something else...
 *
 * @version   1.0 2006-2-28
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface RequestProcessorFactory {

    /**
     * returns a service processor instance for the given service type name.
     */
    public RequestProcessor getRequestProcessor(String requestType)
        throws RPFactoryException;
}
