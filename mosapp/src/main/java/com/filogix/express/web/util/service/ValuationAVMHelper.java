/*
 * @(#)ValuationAVMHelper.java    2006-9-8
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.util.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ValuationAVMHelper - 
 *
 * @version   1.0 2006-9-8
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ValuationAVMHelper extends ServiceRequestHelper {

    // The logger
    private static final Log _log = LogFactory.getLog(ValuationAVMHelper.class);

    /**
     * Constructor function
     */
    public ValuationAVMHelper() {
    }
}
