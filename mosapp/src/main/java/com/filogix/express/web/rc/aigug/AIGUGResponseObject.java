package com.filogix.express.web.rc.aigug;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.GenericResponseObject;
import com.filogix.express.web.rc.avm.AVMResponseObject;

public class AIGUGResponseObject extends GenericResponseObject{

//	 The logger
    private static Log log = LogFactory.getLog(AIGUGResponseObject.class);

    /**
     * Constructor function
     */
    public AIGUGResponseObject() {
    }

    /**
     * set the response status.
     */
    public void setResponseStatus(int status) {

        put(RC_RESPONSE_STATUS, new Integer(status));
    }

    /**
     * set the request id for the given 
     */
    public void setRequestId(int id) {

        put("requestId", new Integer(id));
    }

    /**
     * set the copy id for transaction management.
     */
    public void setCopyId(int id) {

        put("copyId", new Integer(id));
    }

    /**
     * set the response id
     */
    public void setResponseId(int id) {

        put("responseId", new Integer(id));
    }

    /**
     * the provider name.
     */
    public void setProviderName(String name) {

        put("providerName", name);
    }

    /**
     * the responseDate as string.
     */
    public void setResponseDate(String date) {

        put("responseDate", date);
    }

    /**
     * the AIGUG status
     */
    public void setAIGUGStatus(String value) {

        put("aigugStatus", value);
    }

    /**
     * the SIGUG status message.
     */
    public void setAIGUGStatusMsg(String value) {

        put("aigugStatusMsg", value);
    }

    /**
     * the AIGUG request date.
     */
    public void setAIGUGDate(String value) {

        put("aigugDate", value);
    }
    
    public void setMiResponse(String miResponse) {
    	put("miResponse", miResponse);
    	log.info("AIGUGResponseObject:: setMiResponse: " + miResponse);
    }
    
    public void setMiPremiumAmount(double miPremiumAmount) {
    	put("miPremiumAmount", new Double(miPremiumAmount));
    }
    
    public void setMiStatusId(int id) {
    	put("miStatusId", new Integer(id));
    }
    
    public void setMiStatusMsg(String miStatusMsg) {
    	put("miStatusMsg", miStatusMsg);
    }
    
    public void setMiPolicyNumber(String miPolicyNumber) {
    	put("miPolicyNumber", miPolicyNumber);
    }
	
}
