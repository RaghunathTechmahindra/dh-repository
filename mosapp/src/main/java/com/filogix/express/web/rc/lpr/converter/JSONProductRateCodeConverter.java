package com.filogix.express.web.rc.lpr.converter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basis100.mtgrates.RateInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: JSONProductRateCodeConverter
 * <p>
 * Description: JSON version of implementation of converter for rateCode 
 * @author MCM Impl Team.
 * @version 1.0 Jul 31, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public class JSONProductRateCodeConverter extends AbstractLegacyCSVConverter
        implements IConverter<RateInfo> {

    /**
     * this method converts a result data list from PricingLigc into a JSON String.
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<RateInfo> list, SessionResourceKit srk) {

        Map<String, Object> map = new HashMap<String, Object>();
        if(list != null)
        for (RateInfo rateInfo : list) {
            String desc = PricingLogicConverterUtil
                    .makePricingRateDetailedDescription(rateInfo, srk);
            map.put("rateCode", rateInfo.getRateCode());
            break; // record is only one
        }
        res.setRateCode(map);
        return res;
    }

}
