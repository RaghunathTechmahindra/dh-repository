package com.filogix.express.web.rc.miresponse;

import com.filogix.express.web.rc.GenericResponseObject;

public class MIResponseAlertResponseObject extends GenericResponseObject {

	public MIResponseAlertResponseObject() {
	}

    public void setDealLock(boolean dealLock) {
        put("dealLock", dealLock);
    }
	
    public void setPendingMIResponse(boolean pendingMIResponse) {
        put("pendingMIResponse", pendingMIResponse);
    }
    
    public void setSessionTimeOut(boolean sessionTimeOut) {
        put("sessionTimeOut", sessionTimeOut);
    }
    

}
