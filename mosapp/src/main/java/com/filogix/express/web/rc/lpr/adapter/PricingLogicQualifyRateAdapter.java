package com.filogix.express.web.rc.lpr.adapter;

import java.util.List;

import com.basis100.mtgrates.IPricingLogicProduct;
import com.basis100.mtgrates.IPricingLogicProductRate;
import com.basis100.mtgrates.ProductInfo;
import com.basis100.mtgrates.RateInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

public class PricingLogicQualifyRateAdapter extends AbstractPricingLogicAdapter {

    public LPRResponseObject process(SessionResourceKit srk,
            LPRRequestObject req, LPRResponseObject res) {

        List<RateInfo> list = ((IPricingLogicProductRate) pricingLogic)
        	.getLenderProductRates( srk, 
        							req.getProductId(), 
        							req.getDealId(), 
        							req.getDealCopyId());

        return this.converter.convert(res, list, srk);

    }

}
