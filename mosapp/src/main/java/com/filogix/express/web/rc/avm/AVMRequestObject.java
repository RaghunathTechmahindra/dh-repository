/*
 * @(#)AVMRequestObject.java    2006-3-7
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.avm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.request.ServiceRequestObject;

/**
 * AVMRequestObject is a helper to got access the request info.
 *
 * @version   1.0 2006-3-7
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AVMRequestObject extends ServiceRequestObject {

    // The logger
    private final static Log _log = LogFactory.getLog(AVMRequestObject.class);

    /**
     * Constructor function
     */
    public AVMRequestObject() {
    }

    /**
     * returns the providerId.
     */
    public int getProviderId() {

        return ((Integer) get("providerId")).intValue();
    }

    /**
     * returns the product id.
     */
    public int getProductId() {

        return ((Integer) get("productId")).intValue();
    }

    /**
     * returns the property id.
     */
    public int getPropertyId() {

        return ((Integer) get("propertyId")).intValue();
    }

    /**
     * returns the language id.
     */
    public int getLanguageId() {

        return ((Integer) get("languageId")).intValue();
    }
}
