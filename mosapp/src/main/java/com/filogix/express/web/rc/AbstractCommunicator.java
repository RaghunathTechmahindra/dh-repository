/*
 * @(#)AbstractCommunicator.java    2006-3-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * AbstractCommunicator is a helper class for (@link Communicator)'s
 * implementation.  Spring will inject the available service request and service
 * response object.
 *
 * @version   1.0 2006-3-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractCommunicator implements Communicator {

    // The logger
    private static Log _log = LogFactory.getLog(AbstractCommunicator.class);

    /**
     * returns this message when failed to serialize the response object with
     * JSON's format. 
     */
    public String getSerializeFailureMsg() {

        StringBuffer ret = new StringBuffer();
        ret.append("{\"").
            append(ResponseObject.RC_RESPONSE_STATUS).
            append("\":").
            append(RequestObject.STATUS_SERIALIZE_FAILURE).
            append(", \"").
            append(ResponseObject.RC_RESPONSE_CONTENT).
            append("\":{\"message\":\"Can NOT serialize the response object! ").
            append("Please contact your system administrator!\"}}");

        _log.info("Serialize Failure Message: " + ret.toString());
        return ret.toString();
    }
}
