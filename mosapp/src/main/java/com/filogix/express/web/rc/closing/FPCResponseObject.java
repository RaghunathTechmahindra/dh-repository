/*
 * @(#)FPCResponseObject.java    2006-3-7
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.closing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.response.ServiceResponseObject;

/**
 * FPCResponseObject - 
 *
 * @version   1.0 2006-3-7
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class FPCResponseObject extends ServiceResponseObject {

    // The logger
    private final static Log _log = LogFactory.getLog(OCSResponseObject.class);

    /**
     * Constructor function
     */
    public FPCResponseObject() {
    }

    /**
     * set the servicerequesetcontactId
     */
    public void setServiceRequestContactId(int id) {

        put("serviceRequestContactId", new Integer(id));
    }

    /**
     * the request date
     */
    public void setFpcRequestDate(String value) {

        put("fpcRequestDate", value);
    }

    /**
     * the request status.
     */
    public void setFpcRequestStatus(String value) {

        put("fpcRequestStatus", value);
    }
}


