/*
 * @(#)ExtractReportException.java    2006-5-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

/**
 * ExtractReportException - 
 *
 * @version   1.0 2006-5-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExtractReportException extends Exception {

    /**
     * Constructor function
     */
    public ExtractReportException() {

        super();
    }

    /**
     * with message.
     */
    public ExtractReportException(String message) {

        super(message);
    }

    /**
     * with message and throwable.
     */
    public ExtractReportException(String message, Throwable th) {

        super(message, th);
    }

    /**
     * with throwable.
     */
    public ExtractReportException(Throwable th) {

        super(th);
    }
}
