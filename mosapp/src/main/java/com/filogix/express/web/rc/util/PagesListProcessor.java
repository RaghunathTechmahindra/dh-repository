/*
 * @(#)PagesListProcessor.java    2007-12-17
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mosApp.MosSystem.PageHandlerCommon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.UserProfile;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.GeneralRequestProcessorJato;

/**
 * PagesListProcessor - 
 *
 * @version   1.0 2007-12-17
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * 
 * FXP33172 v5.0_XpS_Quick Links - Quick Links display when Quick Links aren't enabled
 */
public class PagesListProcessor extends GeneralRequestProcessorJato {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(PagesListProcessor.class);

    // the instution where clause,
//    StringBuffer _institutionWhere = new StringBuffer();

    /**
     * Constructor function
     */
    public PagesListProcessor() {
    }

    /**
     * process the request.
     */
    public ResponseObject processRequest(RequestObject reqObject,
                                         ResponseObject respObj)
        throws RequestProcessException {

        // initializing JATO context.
        PageHandlerCommon handler = new PageHandlerCommon();
        initializeHandler(handler);

        // get the deal id from request object.
        int dealId = ((Integer) reqObject.get("dealId")).intValue();

        // srk,
        //FXP23168, stop using srk from session.
        //SessionResourceKit srk = getSessionResourceKit();
        // because this class erase institution setting
        SessionResourceKit srk = new SessionResourceKit("PageList");
        
        // clean vpd first.
        srk.getExpressState().cleanAllIds();

        List<Map> pages = new ArrayList<Map>();

        try {

            // search available institution list for current user.
            List<Map> allInstitutions = getInstitutions4User(handler);

            // search available institutions list for given deal.
            List<Map> dealInstitutions = getInstitutions4Deal(dealId, srk, handler);

            // search available pages list, apply the vpd based on user
            // institution profile id.
           
            StringBuilder sql4Pages = new StringBuilder();
            sql4Pages.append(" SELECT DISTINCT PAGEID FROM PAGE WHERE " )
                	 .append(" INCLUDEINGOTO = 'Y' AND " )
                	 .append(makeInstitutionWhereClouse(handler.getTheSessionState().institutionList()));
            
            // FXP33172
            PropertiesCache cache = PropertiesCache.getInstance();
//    		String quickLinkEnabledForInstitution = (cache.getProperty(srk.getExpressState().getUserInstitutionId(),
//    												"com.filogix.ingestion.quicklinks.enabled", "Y"));

            boolean qlEnabledAtAll = false;
            Iterator<Map> itAllInst = allInstitutions.iterator();
            for (Map i : allInstitutions)
            {
            	Integer id = (Integer) i.get("id");
            	qlEnabledAtAll = qlEnabledAtAll || "Y".equalsIgnoreCase((cache.getProperty(id,
    												"com.filogix.ingestion.quicklinks.enabled", "Y")));
            }
            
            if (_logger.isDebugEnabled())
                _logger.debug("Query available pages: {}", sql4Pages);

            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(sql4Pages.toString());
            for (; jExec.next(key); ) {

                Map<String, Object> onePage = new HashMap<String, Object>();
                // page id.
                Integer pageId = new Integer(jExec.getInt(key, 1));
                
                //FXP33172
                if(/*"N".equals(quickLinkEnabledForInstitution)*/ !qlEnabledAtAll && 
                		pageId.intValue() == Mc.PGNM_QUICKLINK_ADMININISTRATION)
                	continue;
                
                onePage.put("id", pageId);
                // page name.
                String pageName = BXResources.
                    getPickListDescription(getUserInstitutionId(), "PAGELABEL",
                                           pageId.toString(),
                                           getLanguageId());                               
                onePage.put("label", pageName);
                /*
                 * FXP19908 - FIX BEGIN
                 */
                // sort order.
                Integer sortOrder = BXResources.
                    getPickListSortOrder(getUserInstitutionId(), "PAGELABEL",
                                           pageId.toString(),
                                           getLanguageId());
                onePage.put("order", sortOrder);
                /*
                 * FXP19908 - FIX END
                 */
                // figure out institutions for each page.
                onePage.put("institutions",
                            getInstitutions4Page(pageId,
                                                 allInstitutions,
                                                 dealInstitutions));
                
                onePage.put("dealNecessity",
                        PageInstitutionUtil.isTheScreenNeedsDealId(pageId) ? "Y" : "N");
                
                // all to page list.
                pages.add(onePage);
            }
        } catch (Exception e) {
            _logger.error("TERRIBLE Things are happing:", e);
        } finally {
            srk.freeResources();
        }
        
        /*
         * FXP19908 - FIX BEGIN
         */
        Collections.sort(pages, new Comparator<Map>() {
            public int compare(Map o1, Map o2) {
                Integer order1 = (Integer) o1.get("order");
                Integer order2 = (Integer) o2.get("order");
                return order1.compareTo(order2);
            }
        });
        /*
         * FXP19908 - FIX END
         */
        respObj.put("pages", pages);
        if (_logger.isDebugEnabled())
            _logger.debug("Response Pages List: " + respObj.toString());
        return respObj;
    }

    /**
     * return all institutions for the given user, at less we have one, which is
     * the current user's institution.
     */
    private List<Map> getInstitutions4User(PageHandlerCommon handler)
            throws Exception {

        List<Map> allInstitutions = new ArrayList<Map>();

        // the order of institutionData depends on InstitutionProfile.InstitutionSequence
        for (Integer ins : handler.getTheSessionState().institutionList()) {

            Map<String, Object> institution = new HashMap<String, Object>();
            // institution id.
            institution.put("id", ins);
            // institution name
            String name = BXResources.getInstitutionName(ins);
            institution.put("label", name);

            if (_logger.isDebugEnabled())
                _logger.debug("Found institution [{}, {}] ", new String[] {
                        ins.toString(), name });
            // add to the list.
            allInstitutions.add(institution);
        }

        return allInstitutions;
    }

    /**
     * return all institutions for the given deal, may be null.
     */
    private List<Map> getInstitutions4Deal(int dealId, SessionResourceKit srk, PageHandlerCommon handler)
        throws RemoteException, FinderException, Exception {

        List<Map> dealInstitutions = new ArrayList<Map>();
        List<Integer> institutionList = handler.getTheSessionState().institutionList();
        if (dealId > 0) {
            StringBuffer sql4Deal = new StringBuffer();
            sql4Deal.append("SELECT DISTINCT DEAL.INSTITUTIONPROFILEID, ")
                .append("INSTITUTIONPROFILE.INSTITUTIONSEQUENCE FROM ")
                .append("DEAL, INSTITUTIONPROFILE WHERE ")
                .append("DEAL.INSTITUTIONPROFILEID = INSTITUTIONPROFILE.INSTITUTIONPROFILEID ")
                .append("AND DEAL.DEALID = ").append(dealId).append(" AND ")
                .append("DEAL.").append(makeInstitutionWhereClouse(institutionList))
                .append(" ORDER BY INSTITUTIONPROFILE.INSTITUTIONSEQUENCE");
            if (_logger.isDebugEnabled())
                _logger.debug("Search institution for deal: {}",
                              sql4Deal.toString());

            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(sql4Deal.toString());
            for (; jExec.next(key); ) {

                Map<String, Object> institution = new HashMap<String, Object>();
                // the id.
                Integer instId = new Integer(jExec.getInt(key, 1));
                institution.put("id", instId);
                // the name.
                String instName = BXResources.getInstitutionName(instId);
                institution.put("label", instName);

                String borrower = getBorrower4Deal(dealId, instId.intValue(),
                        srk);
                institution.put("borrower", borrower);

                if (_logger.isDebugEnabled())
                    _logger.debug("Found institution [{}, {}, {}] for Deal: {}",
                                  new String[] {instId.toString(), instName,
                                                borrower, String.valueOf(dealId)});
                
                // add to the list.
                dealInstitutions.add(institution);
            }
        }

        return dealInstitutions;
    }

    /**
     * returns the institutions list for specific page, all institutions or all
     * deal institutions.
     */
    private List<Map> getInstitutions4Page(int pageId,
                                           List<Map> allInstitutions,
                                           List<Map> dealInstitutions) {

        switch (PageInstitutionUtil.getInstitutionUsage(pageId)){
        case ALL_OF_THE_USER_INSTITUTIONS:
            return null;
        case ONE_OF_THE_USER_INSTITUTIONS:
            return allInstitutions;
        case DEAL_INSTITUION:
        default:
            return dealInstitutions;
        }
        
    }
    
    /**
     * get borrower name for this deal
     */
    private String getBorrower4Deal(int dealId, int institutionId,
            SessionResourceKit srk) {

        int copyId = 0;
        String borrowerName = "";

        if (dealId > 0) {
            /**
             * BEGIN FIX FOR FXP21058 
             * Mohan V Premkumar
             * 04/07/2008
             */
            srk.getExpressState().setDealInstitutionId(institutionId);
            /**
             * END FIX FOR FXP21058
             */
            String sql = "SELECT COPYID FROM DEAL WHERE " + "DEALID = "
                    + dealId + " AND scenarioRecommended = 'Y'" 
                    + " AND institutionProfileId = " + institutionId;
            
            if (_logger.isDebugEnabled())
                _logger.debug("Search copyid  for deal sql: ", sql);

            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key;
            try {
                key = jExec.execute(sql);

                for (; jExec.next(key);) {
                    Integer copyInt = new Integer(jExec.getInt(key, 1));
                    copyId = copyInt.intValue();
                    break;
                }
                Borrower borrower = new Borrower(srk, null);
                borrower = borrower.findByPrimaryBorrower(dealId, copyId);
                borrowerName = borrower.getBorrowerFirstName() + " "
                        + borrower.getBorrowerLastName();
                /**
                 * BEGIN FIX FOR FXP21058 
                 * Mohan V Premkumar
                 * 04/07/2008
                 */
                srk.getExpressState().cleanDealIds();
                /**
                 * END FIX FOR FXP21058
                 */
            } catch (Exception e) {
                _logger.error("Error finding BorrowerName for Deal", e);
            }
        }

        return borrowerName;
    }
    
    private String makeInstitutionWhereClouse(List<Integer> institutionList){
        StringBuffer sb = new StringBuffer();
        sb.append("INSTITUTIONPROFILEID IN (");
        for(Iterator<Integer> inst = institutionList.iterator();inst.hasNext();){
            sb.append(inst.next());
            if(inst.hasNext()) sb.append(", ");
        }
        sb.append(")");
        return sb.toString();
    }
}