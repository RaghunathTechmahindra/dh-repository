package com.filogix.express.web.rc.lpr.adapter;

import java.util.List;

import com.basis100.mtgrates.IPricingLogicProductRate;
import com.basis100.mtgrates.RateInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: PricingLogicProductRateAdapter
 * <p>
 * Description: implementation of adapter for rate
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class PricingLogicProductRateAdapter extends AbstractPricingLogicAdapter {

    /**
     * @see com.filogix.express.web.rc.lpr.adapter.IPricingLogicAdapter#process(com.basis100.resources.SessionResourceKit, com.filogix.express.web.rc.RequestObject, com.filogix.express.web.rc.ResponseObject)
     */
    public LPRResponseObject process(SessionResourceKit srk,
            LPRRequestObject req, LPRResponseObject res) {

        List<RateInfo> list = ((IPricingLogicProductRate) pricingLogic)
                .getLenderProductRates(srk, req.getProductId(), req.getDealId(), 
                        req.getDealCopyId());

        return this.converter.convert(res, list, srk);

    }

}
