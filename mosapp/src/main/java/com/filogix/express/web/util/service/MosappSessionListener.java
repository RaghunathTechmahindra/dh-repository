package com.filogix.express.web.util.service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import mosApp.MosSystem.SessionStateModel;
import mosApp.MosSystem.SessionServices;

import com.filogix.express.web.util.SessionObjectHolder;

public class MosappSessionListener implements HttpSessionListener {
    
    public final static String LANGUAGE_MAP = "languageMap";
        
    public void sessionCreated(HttpSessionEvent se) {
      ServletContext sc = se.getSession().getServletContext();
    }
    
    public void sessionDestroyed(HttpSessionEvent se) {
      SessionObjectHolder holder;
      ServletContext context = se.getSession().getServletContext();
      holder = (SessionObjectHolder) context.getAttribute(LANGUAGE_MAP);
      if (holder == null) {
          holder = new SessionObjectHolder();
          context.setAttribute(LANGUAGE_MAP, holder);
      }
      SessionStateModel session = (SessionStateModel) se.getSession().getAttribute("mosApp.MosSystem.SessionStateModel");
      if (session != null)
          holder.setLanguage(se.getSession().getId(), session.getLanguageId());
      
      //Clean deal lock if there is any.
      SessionServices.removeActiveSession(se.getSession(), se.getSession().getId(), true);
    }
}
