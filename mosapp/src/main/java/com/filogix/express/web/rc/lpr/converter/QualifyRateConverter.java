package com.filogix.express.web.rc.lpr.converter;

import java.util.List;

import com.basis100.mtgrates.RateInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

public class QualifyRateConverter extends AbstractLegacyCSVConverter implements
	IConverter<RateInfo> {

    public LPRResponseObject convert(LPRResponseObject res,
            List<RateInfo> list, SessionResourceKit srk) {

        
        StringBuffer rateResult = new StringBuffer();
        if(list != null)
        for (RateInfo rateInfo : list) {
            
            StringBuffer sb = new StringBuffer();

//            sb.append("'");
            sb.append(rateInfo.getPostedRate());
//            sb.append("'");
            sb.append(LINE_DELIMITER);

            rateResult.append(sb.toString());
        }
        
        res.setQualifyRateString(rateResult.toString());
        return res;
    }
}
