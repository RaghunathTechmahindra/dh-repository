/*
 * @(#)ResponseObject.java    2006-3-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.util.Map;

/**
 * ResponseObject represents the response object.
 *
 * @version   1.0 2006-3-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface ResponseObject extends Map {

    // the response status.
    public final static String RC_RESPONSE_STATUS = "rcResponseStatus";
    // this key nake is reserved for error message.
    public final static String RC_RESPONSE_ERRORS = "rcResponseErrors";
    // this key name is reserved for the response object.
    public final static String RC_RESPONSE_CONTENT = "rcResponseContent";

    /**
     * the response object's properties.  For each property, the key is the
     * property's name, the value is the property's type.
     */
    public Map getResponseProperties();

    /**
     * we got to override Map's clone behaviour.
     */
    public Object clone();
}
