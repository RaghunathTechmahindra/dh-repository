/*
 * @(#)JatoRichClientMessenger.java    2006-5-17
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.jato;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.picklist.BXResources;

import com.filogix.express.web.rc.RichClientMessenger;

/**
 * JatoRichClientMessenger - 
 *
 * @version   1.0 2006-5-17
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class JatoRichClientMessenger extends RichClientMessenger  {

    // The logger
    private static Log _log = LogFactory.getLog(JatoRichClientMessenger.class);

    /**
     * Constructor function
     */
    public JatoRichClientMessenger() {
    }

    /**
     * return the rich client message.
     */
    protected String getRichClientMsg0(String key, int languageId) {

        return BXResources.getRichClientMsg(key, languageId);
    }
}
