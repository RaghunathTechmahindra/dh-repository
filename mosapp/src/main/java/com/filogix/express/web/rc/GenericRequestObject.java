/*
 * @(#)GenericRequestObject.java     2006-3-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import java.util.HashMap;
import java.util.Properties;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * GenericRequestObject is the generic implementation of (@link
 * RequestObject).
 *
 * @version   1.0 2006-3-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class GenericRequestObject extends HashMap
    implements RequestObject {

    // The logger
    private static Log _log = LogFactory.getLog(GenericRequestObject.class);

    // the request object properties' definition.
    protected Map _requestProperties = null;

    /**
     * Constructor function
     */
    public GenericRequestObject() {
    }

    /**
     * returns the service type.
     */
    public String getRequestType() {

        return (String) get(RC_REQUEST_TYPE);
    }

    /**
     * set the properties' definition.
     */
    public void setRequestProperties(Map props) {

        _requestProperties = props;
    }

    /**
     * returns a generic propertie
     */
    public Map getRequestProperties() {

        if (_requestProperties == null) {
            _log.info("Request Object's properties are not defined! " +
                      "Using the default one!");
            _requestProperties = new Properties();
            _requestProperties.put("content", "String");
        }

        return _requestProperties;
    }

    /**
     * clone method.
     */
    public Object clone() {

        GenericRequestObject result = (GenericRequestObject) super.clone();
        result.setRequestProperties(this.getRequestProperties());

        return result;
    }
}
