/*
 * @(#)ContactInfo4Request.java    2006-8-14
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.request;

/**
 * ContactInfo4Request - 
 *
 * @version   1.0 2006-8-14
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface ContactInfo4Request {

    /**
     * name;
     */
    public String getFirstName();

    public String getLastName();

    /**
     * email
     */
    public String getEmailAddress();

    /**
     * work phone.
     */
    public String getWorkPhoneArea();

    public String getWorkPhoneNumber();

    public String getWorkPhoneExt();

    /**
     * cell phone.
     */
    public String getCellPhoneArea();

    public String getCellPhoneNumber();

    /**
     * home phone.
     */
    public String getHomePhoneArea();

    public String getHomePhoneNumber();

    /**
     * owner name.
     */
    public String getPropertyOwnerName();

    /**
     * comments.
     */
    public String getComments();
}
