package com.filogix.express.web.rc.lpr.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basis100.mtgrates.RepaymentTypeInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: JSONRepaymentTypeConverter
 * <p>
 * Description: JSON version of implementation of converter for repaymenttype 
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class JSONRepaymentTypeConverter implements IConverter<RepaymentTypeInfo> {

    /**
     * this method converts a result data list from PricingLigc into a JSON String.
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<RepaymentTypeInfo> list, SessionResourceKit srk) {

        int institutionId = srk.getExpressState().getDealInstitutionId();
        int languageId = srk.getLanguageId();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        
        if(list != null)
        for (RepaymentTypeInfo repayment : list) {

            String repaymentTypeName = BXResources.getPickListDescription(
                    institutionId, "REPAYMENTTYPE", repayment.getRepaymentTypeId(), languageId);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("productId", repayment.getMtgProdId());
            map.put("repaymentId", repayment.getRepaymentTypeId());
            map.put("repaymentDesc", repaymentTypeName);
            resultList.add(map);
        }
        res.setRepaymentTypes(resultList);
        return res;
    }

}
