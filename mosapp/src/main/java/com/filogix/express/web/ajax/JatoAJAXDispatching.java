/*
 * @(#)JatoAJAXDispatching.java    2006-5-1
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.ajax;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.web.ExpressWebContext;

/**
 * JatoAJAXDispatching is a helper class used by JATO framework's dispatcher
 * servlet to dispatch AJAX request to AJAX dispatcher.  It is implemented as a
 * singlton class.
 *
 * @version   1.0 2006-5-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class JatoAJAXDispatching {

    // The logger
    private static final Log _log =
        LogFactory.getLog(JatoAJAXDispatching.class);

    // the JATO page name for AJAX request.
    public static final String PAGE_NAME_FOR_AJAX = "AJAXDispatching";

    // The AJAX dispatcher bean name for spring framework.
    public static final String AJAX_DISPATHER_BEAN_NAME = "ajaxDispatcherImpl";

    // the singlton instance.
    private static JatoAJAXDispatching _instance;

    /**
     * Constructor function
     */
    private JatoAJAXDispatching() {
    }

    static {
        // the simple implementation for now.
        _instance = new JatoAJAXDispatching();
    }

    /**
     * dispatch the AJAX request to AJAX dispatcher.
     */
    public static void dispatchAJAXRequest(ServletContext context,
                                           HttpServletRequest request,
                                           HttpServletResponse response)
        throws ServletException, IOException {

            try {
                // get the AJAX dispatcher implementation.
                AJAXDispatcher ajaxDispather =
                    (AJAXDispatcher) ExpressWebContext.
                    getService(AJAX_DISPATHER_BEAN_NAME);
                ajaxDispather.processAJAXRequest(request, response);
            } catch (ExpressServiceException be) {
                _log.error("Can not find bean: " +
                           AJAX_DISPATHER_BEAN_NAME, be);
                throw new ServletException("Can not find bean: " +
                                           AJAX_DISPATHER_BEAN_NAME, be);
            }
     }
}