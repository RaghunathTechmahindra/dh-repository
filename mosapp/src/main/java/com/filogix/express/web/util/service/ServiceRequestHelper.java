/*
 * @(#)ServiceRequestHelper.java    2006-8-14
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.util.service;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.SessionResourceKit;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;

/**
 * ServiceRequestHelper - 
 *
 * @version   1.0 2006-8-14
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ServiceRequestHelper {

    // The logger
    private final static Log _log =
        LogFactory.getLog(ServiceRequestHelper.class);

    /**
     * Constructor function
     */
    public ServiceRequestHelper() {
    }

    /**
     * helps to get the valid providers for the given type, an array of type,
     * subtype, subsubtype,...
     */
    public static Map getValidProviders(SessionResourceKit srk,
                                        int languageId,
                                        int type, int subtype) {

        Map providers = new TreeMap();
        String querySQL =
            "SELECT DISTINCT SERVICEPROVIDERID " +
            "FROM SERVICEPRODUCT  WHERE " +
            "SERVICETYPEID = " + type + " AND " +
            "SERVICESUBTYPEID = " + subtype + " ORDER BY SERVICEPROVIDERID";
        if(_log.isDebugEnabled())
            _log.debug("Query SQL: " + querySQL);

        // put the empty option.
        providers.put("0", "");

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);

            for (; jExec.next(key); ) {

                String providerId = jExec.getString(key, "SERVICEPROVIDERID");
                String providerLabel = BXResources.
                    getPickListDescription(srk.getExpressState().getDealInstitutionId(),
                                           "SERVICEPROVIDER", providerId,
                                           languageId);
                providers.put(providerId, providerLabel);
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
        }

        return providers;
    }

    /**
     * returns all products for the given provider.
     */
    public static Map getValidProducts(SessionResourceKit srk,
                                       int languageId, int type, int subtype,
                                       int providerId) {

        Map products = new TreeMap();

        String querySQL =
            "SELECT SERVICEPRODUCTID FROM SERVICEPRODUCT " +
            "WHERE " +
            "SERVICEPROVIDERID = " + providerId + " AND " +
            "SERVICETYPEID = " + type + " AND " +
            "SERVICESUBTYPEID = " + subtype;
        if(_log.isDebugEnabled())
            _log.debug("Query SQL: " + querySQL);

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (; jExec.next(key); ) {

                Integer id = new Integer(jExec.getInt(key, 1));
                // get the product description from the BXResources based on the
                // language id.
                String prodName = BXResources.
                    getPickListDescription(srk.getExpressState().getDealInstitutionId(),
                                           "SERVICEPRODUCT", id.toString(),
                                           languageId);
                products.put(id, prodName);
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
            // TODO ....
        }

        return products;
    }
}
