/*
 * @(#)AppraisalRequestObject.java    2006-6-29
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.appraisal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.request.ServiceRequestObject;
import com.filogix.express.web.rc.request.ContactInfo4Request;

/**
 * AppraisalRequestObject - 
 *
 * @version   1.0 2006-6-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AppraisalRequestObject extends ServiceRequestObject
    implements ContactInfo4Request {

    // The logger
    private final static Log _log =
        LogFactory.getLog(AppraisalRequestObject.class);

    /**
     * Constructor function
     */
    public AppraisalRequestObject() {
    }

    /**
     * returns the propertyId.
     */
    public int getPropertyId() {

        return ((Integer) get("propertyId")).intValue();
    }

    /**
     * returns the service request contact id.
     */
    public int getServiceRequestContactId() {

        return ((Integer) get("serviceRequestContactId")).intValue();
    }

    /**
     * returns the product id.
     */
    public int getProductId() {

        return ((Integer) get("productId")).intValue();
    }

    /**
     * is cancel or not.
     */
    public boolean getIsCancel() {

        return ((Boolean) get("isCancel")).booleanValue();
    }

    /**
     * need primary borrower or not.
     */
    public boolean getNeedPrimary() {

        return ((Boolean) get("needPrimary")).booleanValue();
    }

    public String getFirstName() {
        return ((String) get("firstName"));
    }

    public String getLastName() {
        return ((String) get("lastName"));
    }

    public String getPropertyOwnerName() {

        return ((String) get("ownerName"));
    }

    public String getEmailAddress() {
        return ((String) get("emailAddress"));
    }

    public String getWorkPhoneArea() {
        return ((String) get("workPhoneArea"));
    }

    public String getWorkPhoneNumber() {
        return ((String) get("workPhoneNumber"));
    }

    public String getWorkPhoneExt() {
        return ((String) get("workPhoneExt"));
    }

    public String getCellPhoneArea() {
        return ((String) get("cellPhoneArea"));
    }

    public String getCellPhoneNumber() {
        return ((String) get("cellPhoneNumber"));
    }

    public String getHomePhoneArea() {
        return ((String) get("homePhoneArea"));
    }

    public String getHomePhoneNumber() {
        return ((String) get("homePhoneNumber"));
    }

    public String getInstruction() {
        return ((String) get("instruction"));
    }

    public String getComments() {
        return null;
    }
}
