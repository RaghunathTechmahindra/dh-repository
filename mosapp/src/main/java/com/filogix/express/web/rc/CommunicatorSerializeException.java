/*
 * @(#)CommunicatorSerializeException.java    2006-2-28
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

/**
 * CommunicatorSerializeException - throw out when got error during serializing
 * the response object.
 *
 * @version   1.0 2006-2-28
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class CommunicatorSerializeException extends Exception {

    /**
     * Constructor function
     */
    public CommunicatorSerializeException() {

        super();
    }

    /**
     * with message.
     */
    public CommunicatorSerializeException(String message) {

        super(message);
    }

    /**
     * with message and throwable.
     */
    public CommunicatorSerializeException(String message, Throwable th) {

        super(message, th);
    }

    /**
     * with throwable.
     */
    public CommunicatorSerializeException(Throwable th) {

        super(th);
    }
}
