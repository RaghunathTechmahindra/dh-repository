/*
 * @(#)AbstractRequestProcessorJato.java    2006-4-26
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.jato;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.basis100.entity.CreateException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.picklist.BXResources;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.security.DealEditControl;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.PageHandlerCommon;

import com.filogix.express.web.rc.AbstractRequestProcessor;

/**
 * AbstractRequestProcessorJato will implement some generic behaviors for an
 * AJAX reuqest processor which need communicate with JATO world or Express's
 * legacy system.
 *
 * @version   1.0 2006-4-26
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractRequestProcessorJato
    extends AbstractRequestProcessor {

    // The logger
    private final static Log _log =
        LogFactory.getLog(AbstractRequestProcessorJato.class);

    // the SessionResourceKit;
    protected SessionResourceKit _srk;

    // the language id for current express session.  the default one is 0,
    // English.
    protected int _languageId = 0;

    // the session user id.
    protected int _sessionUserId = 0;

    // current user's institution profile id.
    protected int _userInstitutionId = 0;

    // the dealeditcontrol.
    protected DealEditControl _dealEditControl;

    // the new copy id.
    protected int _newTxCopyId = 0;

    /**
     * Constructor function
     */
    public AbstractRequestProcessorJato() {
    }

    /**
     * return the SessionResourceKit instance.
     */
    protected SessionResourceKit getSessionResourceKit() {

        // TODO: asset?
        return _srk;
    }

    /**
     * return the language id.
     */
    protected int getLanguageId() {

        return _languageId;
    }

    /**
     * return the user's session id.
     */
    protected int getSessionUserId() {

        return _sessionUserId;
    }

    /**
     * return the user's institution profile id.
     */
    protected int getUserInstitutionId() {

        return _userInstitutionId;
    }

    /**
     * return the deal's institution profile id.
     */
    protected int getDealInstitutionId() {

        return _srk.getExpressState().getDealInstitutionId();
    }

    /**
     * returns the dealeditcontrol
     */
    protected DealEditControl getDealEditControl() {

        return _dealEditControl;
    }

    /**
     * return the new transaction copy id.
     */
    protected int getNewTxCopyId() {

        return _newTxCopyId;
    }

    /**
     * initializing the handler to get the session state in JATO's
     * RequestManager.  Will also return an instance of SessionResourceKit from
     * the Session.
     * <br/>
     * May also set up the processor's language id based on the session.
     */
    protected PageHandlerCommon initializeHandler(PageHandlerCommon handler) {

        // initializing the handler.
        _log.info("Initializing the handler ...");
        handler.memberInit();
        handler.getSessionObjects();

        //FXP23418, Processor is accessed by multiple thread from one user.
        // since ConcurrentModificationException happens, I will stop using enforceInstitutionId here
        // handler.enforceInstitutionId(handler.getTheSessionState().getCurrentPage());
        
        // setting up the sessionresourcekit.
        _log.info("Setting up the SessionResourceKit ...");
        handler.setupSessionResourceKit("Service");
        // return the session resource kit.
        _srk = handler.getSessionResourceKit();

        // setting up the language id.
        _languageId = handler.getTheSessionState().getLanguageId();
        _log.info("Current Session State's language id is: " + _languageId);

        // set up the session user id.
        _sessionUserId = handler.getTheSessionState().getSessionUserId();
        _log.info("Current Session user's user profile id is: " +
                  _sessionUserId);
        _userInstitutionId =
            handler.getTheSessionState().getUserInstitutionId();
        _log.info("Current Session user's institution profile id is: " +
                  _userInstitutionId);

        // set up the dealeditcontrol.
        if(handler.getTheSessionState().getCurrentPage().isEditable()) {
            _dealEditControl = handler.getTheSessionState().getDec();
            if(_dealEditControl != null){
                _log.info("Current DealEditControl is: " + _dealEditControl.toString());
                _dealEditControl.show(_srk, "");
            }
        }
        
        return handler;
    }

    /**
     * try to adopt the transaction copy based on the context in HttpSession.
     * returns the adopted copy id.
     *
     * TODO: Complete the exception handling logic!
     */
    protected int adoptTransactionCopy(PageHandlerCommon handler)
        throws Exception {

        // handler should be already initialized.
        PageEntry pageEntry = handler.getTheSessionState().getCurrentPage();
        // current copyid,
        int currentCID = pageEntry.getPageDealCID();

        _log.info("Trying to adopt transaction copy ["
                  + currentCID
                  + "] for deal [" + pageEntry.getPageDealId() + "]");

        String pageName = pageEntry.getPageName();
        _log.info("        Page Name: " + pageName);
        handler.getSessionResourceKit().beginTransaction();

        //VLAD-AVM: it should be true since it is a main page!!
        handler.standardAdoptTxCopy(pageEntry, true);

        // VLAD-AVM: the current transactional copy id should be passed in order
        // to setup the page!!!
        if (pageEntry.getPrevPE() == null) {
            // this is main page.
            _log.info("We are in the main page!");
            pageEntry = handler.
                setupMainPagePageEntry(pageEntry.getPageId(),
                                       pageEntry.getPageDealId(),
                                       pageEntry.getPageDealCID(),
                                       pageEntry.getDealInstitutionId());
        } else {
            _log.info("We are in the sub page!");
            _log.info("Trying to update parent page's copy id.");
            pageEntry.getPrevPE().setPageDealCID(currentCID);
            pageEntry = handler.
                setupSubPagePageEntry(pageEntry.getPrevPE(),
                                      pageEntry.getPageId());
        }
        handler.getSavedPages().setNextPage(pageEntry);
        handler.navigateToNextPage();
        handler.getSessionResourceKit().commitTransaction();

        // the new Transaction copy id.
        _newTxCopyId = pageEntry.getPageDealCID();
        _log.info("The New Transaction copy Id: " + _newTxCopyId);
        return _newTxCopyId;
    }

    /**
     * adopt the current scenario as scenario recommended if necessary.
     *
     * @return the new scenario recommended copy id.
     */
    protected int forceAdoptScenarioRecommended(int dealId, DealEditControl dec,
                                                int currentCopyId,
                                                SessionResourceKit srk)
        throws Exception {

        int currentSRCopyId =
            getCopyId4ScenarioRecommended(dealId, currentCopyId, srk);
        int sourceCID = dec.getSourceCID();
        if (sourceCID == currentSRCopyId) {
            _log.info("Current Scenario is Secario Recommended.");
            return currentSRCopyId;
        }

        _log.info("Trying to adopt current scenario as scenario recommended.");
        srk.beginTransaction();

        // set the current SR to non-SR.
        Deal currentSRDeal = new Deal(srk, null, dealId, currentSRCopyId);
        currentSRDeal.setScenarioRecommended("N");
        currentSRDeal.ejbStore();

        // set the source scenario to SR.
        int[] ids = dec.getAllCIDs();
        for (int i = 0; i < ids.length; i ++) {

            Deal newSRDeal = new Deal(srk, null, dealId, ids[i]);
            newSRDeal.setScenarioRecommended("Y");
            newSRDeal.ejbStore();
        }

        srk.commitTransaction();

        // now the source secnario is the new SR.
        return sourceCID;
    }

    /**
     * returns the copy id for the scenario recommended copy.
     *
     * TODO: Exception handling?
     */
    public static int getCopyId4ScenarioRecommended(int dealId, int copyId,
                                                    SessionResourceKit srk)
        throws Exception {

        DealPK dealPk = new DealPK(dealId, copyId);
        Deal deal = new Deal(srk, null);
        deal = deal.findByRecommendedScenario(dealPk, true);
        // the copy for scenario rcommended.
        _log.info("Got the scenario recommaned copy id: " + deal.getCopyId());
        return deal.getCopyId();
    }

    /**
     * prepare the passive message for the rich client.
     */
    protected String preparePassiveMessage(PassiveMessage pm) {

        StringBuffer msgs = new StringBuffer();

        msgs.append("<table border=\"0\" width=\"100%\" bgcolor=\"d1ebff\"")
            .append(" cellpadding=\"0\" cellspacing=\"0\"><tbody>")
            .append("<tr><td colspan=3><br><img src=\"../images/light_blue.gif\" width=1 height=2 alt=\"\" border=\"0\"></td></tr>")
            .append("<tr><td colspan=\"3\">")
            .append("<img src=\"../images/dark_bl.gif\" width=\"100%\" height=\"3\" alt=\"\" border=\"0\"></td></tr>");
        for (int i = 0; i < pm.getMsgs().size(); i ++) {

            int msgType = ((Integer) pm.getMsgTypes().elementAt(i)).intValue();
            String msg = (String) pm.getMsgs().elementAt(i);

            msgs.append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>")
                .append("<td valign=top width=20>");
            switch (msgType) {
            case PassiveMessage.CRITICAL:  // the critical error.
                msgs.append("<img src='/images/critical.gif' width=15 height=15 border=0>");
                break;
            case PassiveMessage.NONCRITICAL:
                msgs.append("<img src='/images/warning.gif' width=15 height=15 border=0>");
                break;
            default:
                msgs.append("&nbsp;&nbsp;");
                break;
            }
            msgs.append("</td><td valign=top><font size=2>")
                .append(msg)
                .append("</font></td></tr>");
        }
        msgs.append("<tr><td colspan=\"3\">")
            .append("<img src=\"/images/dark_bl.gif\" width=\"100%\" height=\"3\" alt=\"\" border=\"0\"></td></tr>")
            .append("<tr><td colspan=3><br><img src=\"/images/light_blue.gif\" width=1 height=2 alt=\"\" border=\"0\"></td></tr>")
            .append("</tbody></table>");

        if (_log.isDebugEnabled()) {
            _log.debug("Error Message: " + msgs.toString());
        }

        return msgs.toString();
    }
}
