/*
 * @(#)DefaultAJAXDispatcher.java    2006-4-23
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.web.rc.RPFactoryException;
import com.filogix.express.web.rc.RequestProcessor;

/**
 * DefaultAJAXDispatcher is default implementation of the AJAXDispatcher.
 *
 * @version   1.0 2006-4-23
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class DefaultAJAXDispatcher extends AbstractAJAXDispatcher {

    // The logger
    private static final Logger _log =
        LoggerFactory.getLogger(DefaultAJAXDispatcher.class);

    /**
     * Constructor function
     */
    public DefaultAJAXDispatcher() {
    }

    /**
     * dispatch the httprequest to the corresponding processor and write the
     * reponse the the HTTPResponse.
     */
    public void processAJAXRequest(HttpServletRequest request,
                                   HttpServletResponse response)
        throws ServletException, IOException {

    	// fixing garbled characters for AJAX - hiro, Jan 9, 2008 -- start
    	// note: for HttpServletRequest, if we should use encode() 
    	//       or encodeURI() in JavaScript, we will have to set utf-8.  
    	request.setCharacterEncoding("ISO-8859-1");
    	response.setContentType("application/json; charset=utf-8");
    	// fixing garbled characters for AJAX - hiro, Jan 9, 2008 -- end
    	
        // get the AJAX request type and request context from the HTTP request.
        String requestType = request.getParameter("requestType");
        String requestContent = request.getParameter("requestContent");
        if (_log.isDebugEnabled())
            _log.debug("   Request Type [{}] Request Content [{}]",
                       requestType, requestContent);

        String responseContent = "";
        try {
            _log.info("Creating processor for the request type: " + requestType);
            RequestProcessor processor =
                _requestProcessorFactory.getRequestProcessor(requestType);
            _log.info("Processing Request: " + requestContent);
            responseContent = processor.processRequest(requestContent,
                                                       response);
        } catch (RPFactoryException fe) {
            _log.error("Request Factory Error!", fe);
            throw new ServletException("Can not find processor!", fe);
        }

        if (_log.isDebugEnabled())
            _log.debug("Response Content: " + responseContent);
        if (!responseContent.equals("")) {
            response.getWriter().write(responseContent);
        }
        return;
    }
}
