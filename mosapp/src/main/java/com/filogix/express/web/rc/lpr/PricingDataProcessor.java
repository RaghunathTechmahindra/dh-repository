/*
 * @(#)LPRProcessor.java Nov 28, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.web.rc.lpr;

import java.util.Map;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.PageHandlerCommon;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.RequestProcessException;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.AbstractRequestProcessorJato;
import com.filogix.express.web.rc.lpr.adapter.IPricingLogicAdapter;

/**
 * PricingDataProcessor
 * 
 * @version 1.0 Nov 28, 2007
 * @author Hiro
 * 
 *  
 * @version 2.0 Jun 25, 2008, MCM Impl Team - XS_2.28, XS_2.29 changed drastically. 
 *
 */
public class PricingDataProcessor extends AbstractRequestProcessorJato {

    private final static Log _log = LogFactory.getLog(PricingDataProcessor.class);

    private Map<String, IPricingLogicAdapter> adapters;

    public PricingDataProcessor() {
    }

    /**
     * this is the entry point of the dynamic product rate service.
     * this method executes the class which implements IPriingLogicAdapter interface.
     * Implementation class is decided by the SubRequestType in request object.
     * 
     * @see com.filogix.express.web.rc.RequestProcessor#processRequest(com.filogix.express.web.rc.RequestObject, com.filogix.express.web.rc.ResponseObject)
     */
    public ResponseObject processRequest(RequestObject requestObj,
            ResponseObject responeObj) throws RequestProcessException {

        LPRRequestObject reqObj = (LPRRequestObject) requestObj;
        LPRResponseObject resObj = (LPRResponseObject) responeObj;
        _log.debug("PricingDataProcessor.processRequest: " +
        "starts --------------------------------------------");
        _log.debug("SubRequestType=[" + reqObj.getSubRequestType() + "]");
        _log.debug("PricingDataProcessor.processRequest: RequestObject = " + reqObj);
        SessionResourceKit srk = null;
        try{
            // grab target adapter using subRequestType
            IPricingLogicAdapter adapter = getTargetAdapter(reqObj.getSubRequestType());

            // initialize session data etc..
            PageHandlerCommon handler = new PageHandlerCommon();
            initializeHandler(handler);
            srk = handler.getSessionResourceKit();
            
            
            if(_log.isDebugEnabled()){
                srk.getConnection();
                _log.debug("VPD = " + srk.getActualVPDStateForDebug());
            }
            
            //languageId
            srk.setLanguageId(getLanguageId());

            //set extra request parameters
            PageEntry pe = handler.getTheSessionState().getCurrentPage();
            reqObj.setDealId(pe.getPageDealId());
            reqObj.setDealCopyId(pe.getPageDealCID());
            reqObj.setLanguageId(srk.getLanguageId());

            //execute pricing logic
            resObj = adapter.process(srk, reqObj, resObj);

        }catch(Exception e){
            _log.error(e);
            _log.error(StringUtil.stack2string(e));
            throw new RequestProcessException(e);
        }finally{
            if(srk != null)
            srk.freeResources();
        }
        _log.debug("PricingDataProcessor.processRequest: ResponseObject = " + resObj);
        _log.debug("PricingDataProcessor.processRequest: " +
        "ends --------------------------------------------");
        return resObj;
    }

    /**
     * this method will decide which Adapter implementation class should be used 
     * based on the parameter <subRequestType> and returns the object which 
     * implements IPricingLogicAdapter interface.
     * this association between subRequestType and Adapter are tied externally.
     * assuming usage of Spring framework.  
     * @param subRequestTypes
     * @return target Adapter
     */
    private IPricingLogicAdapter getTargetAdapter(String subRequestType) {
        IPricingLogicAdapter adapter = adapters.get(subRequestType);
        _log.debug("adapter = " + adapter);
        if (adapter == null) {
            throw new ExpressRuntimeException(
                    "no adapter instance: invalid sub request type = "
                    + subRequestType);
        }
        //converter can be null
        _log.debug("converter = " + adapter.getConverter());
        return adapter;
    }

    /**
     * this method returns the map object which contains 
     * objects implementing IPricingLogicAdapter interface.
     * @return map
     */
    public Map<String, IPricingLogicAdapter> getAdapters() {
        return adapters;
    }

    /**
     * this method accepts the map object which contains 
     * String as keys and objects implementing IPricingLogicAdapter interface as values.
     * @param adapters
     */
    public void setAdapters(Map<String, IPricingLogicAdapter> adapters) {
        this.adapters = adapters;
    }
}
