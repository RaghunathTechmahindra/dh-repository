package com.filogix.express.web.util.service;



import java.util.Collection;
import java.util.Iterator;
import java.util.*;
import java.util.TreeMap;

import com.basis100.resources.SessionResourceKit;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.picklist.BXResources;
import com.filogix.externallinks.framework.ServiceConst;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ClosinglHelper is the helper class for 
 *
 * @version   1.0 2006-7-7
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class OSCClosingServiceHelper extends ServiceRequestHelper {
    // The logger
    private final static Log _log =
        LogFactory.getLog(OSCClosingServiceHelper.class);
    
    /**
     * Constructor function
     */
    public OSCClosingServiceHelper() {
    }
    

    
    /**
     * returns requestId for OCS
     */
    public static int getOcsRequests(SessionResourceKit srk,
                                       int dealId, int copyId) {

        //populate optionlist for provider
        // prepare the query SQL.
        int requestId = -1;
        
        String querySQL =
            "SELECT DISTINCT " +
            "request.REQUESTDATE, request.REQUESTID " +
            "FROM " +
            "REQUEST request, SERVICEREQUESTCONTACT contact, " +
            "SERVICEREQUEST servicereq, SERVICEPRODUCT prod " +
            "WHERE " +
            "servicereq.REQUESTID = request.REQUESTID AND " +
            "request.DEALID = " + dealId + " AND " +
            "request.COPYID = " + copyId + " AND " +
            "request.SERVICEPRODUCTID = prod.SERVICEPRODUCTID AND " +
            "prod.SERVICETYPEID = " + ServiceConst.SERVICE_TYPE_CLOSING + 
            " AND prod.SERVICESUBTYPEID = "+ 
            ServiceConst.SERVICE_SUB_TYPE_OUTSORCED_CLOSING +
            " ORDER BY request.REQUESTDATE DESC";
                    
        _log.info("Query SQL: " + querySQL);

        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            int key = jExec.execute(querySQL);
            for (; jExec.next(key);) {
                requestId = jExec.getInt(key, "REQUESTID");
                break;
            }
            jExec.closeData(key);
        } catch (Exception e) {
            _log.error("JDBC Executor ERROR: ", e);
        }
        return requestId;
    }
    
}
