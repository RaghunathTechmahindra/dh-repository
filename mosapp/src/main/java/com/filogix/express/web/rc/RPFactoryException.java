/*
 * @(#)RPFactoryException.java    2006-2-28
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

/**
 * RPFactoryException - 
 *
 * @version   1.0 2006-2-28
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class RPFactoryException extends Exception {

    /**
     * Constructor function
     */
    public RPFactoryException() {

        super();
    }

    /**
     * with message.
     */
    public RPFactoryException(String message) {

        super(message);
    }

    /**
     * with message and throwable.
     */
    public RPFactoryException(String message, Throwable th) {

        super(message, th);
    }

    /**
     * with throwable.
     */
    public RPFactoryException(Throwable th) {

        super(th);
    }
}
