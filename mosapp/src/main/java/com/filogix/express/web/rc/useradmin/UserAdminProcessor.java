package com.filogix.express.web.rc.useradmin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mosApp.MosSystem.PageHandlerCommon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.UserProfile;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.RequestObject;
import com.filogix.express.web.rc.ResponseObject;
import com.filogix.express.web.rc.jato.GeneralRequestProcessorJato;
/**
 * 
 * UserAdminProcessor 
 *
 * @version 1.0
 * @version 1.1 Nov 14 2008, FXP23418, FXP23424, stop using enforceInstitutionId, 
 *              since ConcurrentModificationException might happen. 
 * @author    Jerry
 */
public class UserAdminProcessor extends GeneralRequestProcessorJato {

    // The logger
    private final static Logger _logger = LoggerFactory
            .getLogger(UserAdminProcessor.class);

    /**
     * Constructor function
     */
    public UserAdminProcessor() {
    }

    /**
     * Process Request
     */
    public ResponseObject processRequest(RequestObject requestObj,
            ResponseObject responseObj) {

        UserAdminRequestObject reqObj = (UserAdminRequestObject) requestObj;
        UserAdminResponseObject resObj = (UserAdminResponseObject) responseObj;

        PageHandlerCommon handler = new PageHandlerCommon();
        initializeHandler(handler);

        // initializing JATO context.
        //initializeContext();

        SessionResourceKit srk = null;
        try{
            //FXP23418, FXP23424 create temporally srk. 
            srk = new SessionResourceKit("UserAdmin");

            boolean userFound = false;
            int institutionId = handler.getTheSessionState().getExpressState().getDealInstitutionId();
            boolean enterprise = isEnterpriseML(institutionId);

            srk.getExpressState().cleanAllIds();
            List<Map> users = new ArrayList<Map>();
            UserProfile up = null;

            try {
                up = new UserProfile(srk);
                up = up.findByLoginId(reqObj.getUserLogin());
                userFound = true;
            } catch (RemoteException e) {
                _logger.error("Error in findByLoginId RemoteException", e);
            } catch (FinderException e) {
                userFound = false;
            }

            resObj.put("userFound", userFound);

            if (userFound) {

                Map<Integer, Integer> userMap = up.getInstIdToUPId();
                if (userMap.containsKey(institutionId))
                    resObj.put("hardStop", true);

                if (enterprise) {
                    resObj.put("enterPrise", true);
                    Iterator userIter = userMap.keySet().iterator();
                    while (userIter.hasNext()) {
                        Map<String, Object> oneUser = new HashMap<String, Object>();
                        int instId = Integer.parseInt(userIter.next().toString());

                        String instName = BXResources
                        .getInstitutionName(instId);
                        oneUser.put("institutionName", instName);

                        int userId = Integer.parseInt(userMap.get(instId)
                                .toString());
                        srk.getExpressState().setDealInstitutionId(instId);
                        Contact contact = null;
                        try {
                            contact = new Contact(srk);
                            contact = contact.findByUserProfileId(userId);
                        } catch (RemoteException e) {
                            _logger.error(
                                    "Error in findByUserProfileId RemoteException",
                                    e);
                        } catch (FinderException e) {
                            _logger.error(
                                    "Error in findByUserProfileId FinderException",
                                    e);
                        }
                        oneUser.put("loginId", up.getUserLogin());

                        String userName = contact.getContactLastName() + ", "
                        + contact.getContactFirstName();
                        oneUser.put("userName", userName);

                        String jobTitle = contact.getContactJobTitle();
                        oneUser.put("jobTitle", jobTitle);
                        users.add(oneUser);
                    }
                }
                else {
                    resObj.put("hardStop", true);
                    resObj.put("enterPrise", false);
                }
                resObj.put("userIds", users);
            }
            //FXP23418, FXP23424
            //handler.enforceInstitutionId(handler.getTheSessionState()
            //        .getCurrentPage());
        }finally{
            if(srk != null) srk.freeResources();
        }
        return responseObj;
    }
    
    /*
     * method to determine enterprise ml
     */
    public boolean isEnterpriseML(int institutionId) {

        if ((PropertiesCache.getInstance().getProperty(institutionId,
                "com.basis100.multilender.classtype", " ")).equals("M")) {
            return true;
        }
        return false;
    }

}
