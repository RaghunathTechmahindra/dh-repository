/*
 * @(#)ExpressViewBeanBase.java    2007-12-24
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.jato;

import mosApp.MosSystem.doDCFolderModel;
import mosApp.MosSystem.doDCFolderModelImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;
import com.iplanet.jato.view.ViewBeanBase;

import com.iplanet.jato.view.html.Button;
import com.iplanet.jato.view.html.HiddenField;
import com.iplanet.jato.view.html.ImageField;
import com.iplanet.jato.view.html.StaticTextField;
import com.iplanet.jato.view.html.TextField;

/**
 * ExpressViewBeanBase - 
 *
 * @version   1.0 2007-12-24
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressViewBeanBase extends ViewBeanBase implements ViewBean {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(ExpressViewBeanBase.class);

    /**
     * Constructor function
     */
    public ExpressViewBeanBase() {
    }

    /**
     */
    public ExpressViewBeanBase(String pageName) {

        super(pageName);
    }

    // the page id from goto tool bar
    public static final String CHILD_TBPAGEID = "tbPageId";
    public static final String CHILD_TBPAGEID_RESET_VALUE = "";

    // the institution id from goto tool bar.
    public static final String CHILD_TBINSTITUTIONID = "tbInstitutionId";
    public static final String CHILD_TBINSTITUTIONID_RESET_VALUE = "";

    //Exchange Link
	public static final String CHILD_FOLDERCODE = "hdFolderCode";
	public static final String CHILD_FOLDERCODE_RESET_VALUE = "";
	private doDCFolderModel doDCFolder = null;
	
    public static final String CHILD_STRESPONSE = "stMIResponse";
    public static final String CHILD_STRESPONSE_RESET_VALUE = "";
    
    public static final String CHILD_STHDFREQUENCY = "stHDFrequency";
    public static final String CHILD_STHDFREQUENCY_RESET_VALUE = "";
    
    public static final String CHILD_STHDCHMCALERT = "stHDChmcAlert";
    public static final String CHILD_STHDCHMCALERT_RESET_VALUE = "";
    
    public static final String CHILD_STHDGENWORTHALERT = "stHDGenworthAlert";
    public static final String CHILD_STHDGENWORTHALERT_RESET_VALUE = "";

    public static final String CHILD_STHDDEALLOCKED = "stHDDealLocked";
    public static final String CHILD_STHDDEALLOCKED_RESET_VALUE = "";
    
    public static final String CHILD_STHMIRESPONSESTATUSID = "stHDMIResponseStatusId";
    public static final String CHILD_STHMIRESPONSESTATUSID_RESET_VALUE = "";
    
    public static final String CHILD_STHDMIPROVIDERID = "stHDMIProviderId";
    public static final String CHILD_STHDMIPROVIDERID_RESET_VALUE = "";
	
    public static final String CHILD_STHDSESSIONTIMEOUT = "stHDSessionTimeOut";
    public static final String CHILD_STHDSESSIONTIMEOUT_RESET_VALUE = "";
    
    // Quick Link Menus
    public static final String CHILD_HDQUICKLINKENABLED="hdQuickLinkEnableDisplay";
    public static final String CHILD_HDQUICKLINKENABLED_RESET_VALUE="";
    	
	public static final String CHILD_IMQL1="imQL1";
	public static final String CHILD_IMQL1_RESET_VALUE="";
	public static final String CHILD_IMQL2="imQL2";
	public static final String CHILD_IMQL2_RESET_VALUE="";
	public static final String CHILD_IMQL3="imQL3";
	public static final String CHILD_IMQL3_RESET_VALUE="";
		
	public static final String CHILD_STQUICKLINKLIST="stQuickLinkList";
	public static final String CHILD_STQUICKLINKLIST_RESET_VALUE="";
	
	public static final String CHILD_STINSTPROFILELIST="stInstProfileList";
	public static final String CHILD_STINSTPROFILELIST_RESET_VALUE="";
	
	public static final String CHILD_STSELECTEDINSTPROFILE="stSelectedInstProfile";
	public static final String CHILD_STSELECTEDINSTPROFILE_RESET_VALUE="";
	
	public static final String CHILD_HDSELECTEDINSTPROFILEID="hdSelectedInstProfileId";
	public static final String CHILD_HDSELECTEDINSTPROFILEID_RESET_VALUE="";

	public static final String CHILD_BTINSTITUTIONSELECTED = "btInstSelected";
	public static final String CHILD_BTINSTITUTIONSELECTED_RESET_VALUE = "";
	
    // create children.
    protected View createChild(String name) {

        if (name.equals(CHILD_TBPAGEID)) {
            TextField child =
                new TextField(this, getDefaultModel(),
                              CHILD_TBPAGEID,
                              CHILD_TBPAGEID,
                              CHILD_TBPAGEID_RESET_VALUE,
                              null);
            return child;
        } else if (name.equals(CHILD_TBINSTITUTIONID)) {
            TextField child =
                new TextField(this, getDefaultModel(),
                              CHILD_TBINSTITUTIONID,
                              CHILD_TBINSTITUTIONID,
                              CHILD_TBINSTITUTIONID_RESET_VALUE,
                              null);
            return child;
        }
    //Exchange Link
        else if(name.equals(CHILD_FOLDERCODE))
        {
          HiddenField child = new HiddenField(this,
            getdoDCFolderModel(),
            CHILD_FOLDERCODE,
            CHILD_FOLDERCODE,//doDCFolder.FIELD_DFDCFOLDERCODE,
            CHILD_FOLDERCODE_RESET_VALUE,
            null);
          return child;
        }
        else if (name.equals(CHILD_STRESPONSE)) {
        	TextField child =
        		new TextField(this, getDefaultModel(),
                          CHILD_STRESPONSE,
                          CHILD_STRESPONSE,
                          CHILD_STRESPONSE_RESET_VALUE,
                          null);
        	return child;
        } else if (name.equals(CHILD_STHDFREQUENCY)) {
        	TextField child =
        		new TextField(this, getDefaultModel(),
                          CHILD_STHDFREQUENCY,
                          CHILD_STHDFREQUENCY,
                          CHILD_STHDFREQUENCY_RESET_VALUE,
                          null);
        	return child;
        } else if (name.equals(CHILD_STHDCHMCALERT)) {
        	TextField child =
        		new TextField(this, getDefaultModel(),
                          CHILD_STHDCHMCALERT,
                          CHILD_STHDCHMCALERT,
                          CHILD_STHDCHMCALERT_RESET_VALUE,
                          null);
        	return child;
        } else if (name.equals(CHILD_STHDGENWORTHALERT)) {
        	TextField child =
        		new TextField(this, getDefaultModel(),
                          CHILD_STHDGENWORTHALERT,
                          CHILD_STHDGENWORTHALERT,
                          CHILD_STHDGENWORTHALERT_RESET_VALUE,
                          null);
        	return child;
        } else if (name.equals(CHILD_STHDDEALLOCKED)) {
        	TextField child =
        		new TextField(this, getDefaultModel(),
                          CHILD_STHDDEALLOCKED,
                          CHILD_STHDDEALLOCKED,
                          CHILD_STHDDEALLOCKED_RESET_VALUE,
                          null);
        	return child;
        } else if (name.equals(CHILD_STHMIRESPONSESTATUSID)) {
        	TextField child =
        		new TextField(this, getDefaultModel(),
                          CHILD_STHMIRESPONSESTATUSID,
                          CHILD_STHMIRESPONSESTATUSID,
                          CHILD_STHMIRESPONSESTATUSID_RESET_VALUE,
                          null);
        	return child;
        } else if (name.equals(CHILD_STHDMIPROVIDERID)) {
        	TextField child =
        		new TextField(this, getDefaultModel(),
                          CHILD_STHDMIPROVIDERID,
                          CHILD_STHDMIPROVIDERID,
                          CHILD_STHDMIPROVIDERID_RESET_VALUE,
                          null);
        	return child;
        } else if (name.equals(CHILD_STHDSESSIONTIMEOUT)) {
            TextField child =
                new TextField(this, getDefaultModel(),
                          CHILD_STHDSESSIONTIMEOUT,
                          CHILD_STHDSESSIONTIMEOUT,
                          CHILD_STHDSESSIONTIMEOUT_RESET_VALUE,
                          null);
            return child;
        } // quick Link
        else if (name.equals(CHILD_HDQUICKLINKENABLED)) {
	        HiddenField child =
	            new HiddenField(this, getDefaultModel(),
	            		CHILD_HDQUICKLINKENABLED,
	            		CHILD_HDQUICKLINKENABLED,
	            		CHILD_HDQUICKLINKENABLED_RESET_VALUE,
	                      null);
	        return child;
	    }
        else if (name.equals(CHILD_IMQL1)) {
	        ImageField child =
	            new ImageField(this, getDefaultModel(),
	            		CHILD_IMQL1,
	            		CHILD_IMQL1,
	            		CHILD_IMQL1_RESET_VALUE,
	                      null);
	        return child;
	    }
        else if (name.equals(CHILD_IMQL2)) {
        	ImageField child =
	            new ImageField(this, getDefaultModel(),
	            		CHILD_IMQL2,
	            		CHILD_IMQL2,
	            		CHILD_IMQL2_RESET_VALUE,
	                      null);
	        return child;
	    }
        else if (name.equals(CHILD_IMQL3)) {
        	ImageField child =
	            new ImageField(this, getDefaultModel(),
	            		CHILD_IMQL3,
	            		CHILD_IMQL3,
	            		CHILD_IMQL3_RESET_VALUE,
	                      null);
	        return child;
	    }  else if (name.equals(CHILD_STQUICKLINKLIST)) {
	    	StaticTextField child =
	            new StaticTextField(this, getDefaultModel(),
	            		CHILD_STQUICKLINKLIST,
	            		CHILD_STQUICKLINKLIST,
	            		CHILD_STQUICKLINKLIST_RESET_VALUE,
	                      null);
	        return child;
	    } else if (name.equals(CHILD_STINSTPROFILELIST)) {
	    	StaticTextField child =
	            new StaticTextField(this, getDefaultModel(),
	            		CHILD_STINSTPROFILELIST,
	            		CHILD_STINSTPROFILELIST,
	            		CHILD_STINSTPROFILELIST_RESET_VALUE,
	                      null);
	        return child;
	    } else if (name.equals(CHILD_STSELECTEDINSTPROFILE)) {
	    	StaticTextField child =
	            new StaticTextField(this, getDefaultModel(),
	            		CHILD_STSELECTEDINSTPROFILE,
	            		CHILD_STSELECTEDINSTPROFILE,
	            		CHILD_STSELECTEDINSTPROFILE_RESET_VALUE,
	                      null);
	        return child;
	    }  else if (name.equals(CHILD_HDSELECTEDINSTPROFILEID)) {
	    	HiddenField child =
	            new HiddenField(this, getDefaultModel(),
	            		CHILD_HDSELECTEDINSTPROFILEID,
	            		CHILD_HDSELECTEDINSTPROFILEID,
	            		CHILD_HDSELECTEDINSTPROFILEID_RESET_VALUE,
	                      null);
	        return child;
	    }  else if (name.equals(CHILD_BTINSTITUTIONSELECTED)) {
			Button child = new Button(this, getDefaultModel(), CHILD_BTINSTITUTIONSELECTED,
					CHILD_BTINSTITUTIONSELECTED, CHILD_BTINSTITUTIONSELECTED_RESET_VALUE, null);

			return child;
		} else {

            return null;
        }
    }

    // reseet children.
    public void resetChildren() {

        getTbPageId().setValue(CHILD_TBPAGEID_RESET_VALUE);
        getTbInstitutionId().setValue(CHILD_TBINSTITUTIONID_RESET_VALUE);
        //Exchange Link
        getHdFolderCode().setValue(CHILD_FOLDERCODE_RESET_VALUE);
        
        getStResponse().setValue(CHILD_STRESPONSE_RESET_VALUE);
        getHdFrequency().setValue(CHILD_STHDFREQUENCY_RESET_VALUE);
        getHdCmhcAlert().setValue(CHILD_STHDCHMCALERT_RESET_VALUE);
        getHdGenworthAlert().setValue(CHILD_STHDGENWORTHALERT_RESET_VALUE);
        getHdDealLocked().setValue(CHILD_STHDDEALLOCKED_RESET_VALUE);
        getHdMIResponseStatusId().setValue(CHILD_STHMIRESPONSESTATUSID_RESET_VALUE);
        getHdMIProviderId().setValue(CHILD_STHDMIPROVIDERID_RESET_VALUE);
        getHdSessionTimeOut().setValue(CHILD_STHDSESSIONTIMEOUT_RESET_VALUE);


        
        
        // Quick Link
        getHdQuickLinkEnabled().setValue(CHILD_HDQUICKLINKENABLED_RESET_VALUE);
       
        getImQL1().setValue(CHILD_IMQL1_RESET_VALUE);
        getImQL2().setValue(CHILD_IMQL2_RESET_VALUE);
        getImQL3().setValue(CHILD_IMQL3_RESET_VALUE);
              
        getStQuickLinkList().setValue(CHILD_STQUICKLINKLIST_RESET_VALUE);
      
        getStInstProfileList().setValue(CHILD_STINSTPROFILELIST_RESET_VALUE);
        getStSelectedInstProfile().setValue(CHILD_STSELECTEDINSTPROFILE_RESET_VALUE);
        getHdSelectedInstProfileId().setValue(CHILD_HDSELECTEDINSTPROFILEID_RESET_VALUE);
        getBtInstSelected().setValue(CHILD_BTINSTITUTIONSELECTED_RESET_VALUE);
    }

    // 
    protected void registerChildren() {

        registerChild(CHILD_TBPAGEID, TextField.class);
        registerChild(CHILD_TBINSTITUTIONID, TextField.class);
        //Exchange Link
        registerChild(CHILD_FOLDERCODE, HiddenField.class);
        
        registerChild(CHILD_STRESPONSE, TextField.class);
        registerChild(CHILD_STHDFREQUENCY, TextField.class);
        registerChild(CHILD_STHDCHMCALERT, TextField.class);
        registerChild(CHILD_STHDGENWORTHALERT, TextField.class);
        registerChild(CHILD_STHDDEALLOCKED, TextField.class);
        registerChild(CHILD_STHMIRESPONSESTATUSID, TextField.class);
        registerChild(CHILD_STHDMIPROVIDERID, TextField.class);
        registerChild(CHILD_STHDSESSIONTIMEOUT, TextField.class);
        
        // Quick Link
        registerChild(CHILD_HDQUICKLINKENABLED, HiddenField.class);
        
        registerChild(CHILD_IMQL1, ImageField.class);
        registerChild(CHILD_IMQL2, ImageField.class);
        registerChild(CHILD_IMQL3, ImageField.class);
        
        registerChild(CHILD_STQUICKLINKLIST, StaticTextField.class);
        
        registerChild(CHILD_STINSTPROFILELIST, StaticTextField.class);
        registerChild(CHILD_STSELECTEDINSTPROFILE, StaticTextField.class);
        registerChild(CHILD_HDSELECTEDINSTPROFILEID, HiddenField.class);
        registerChild(CHILD_BTINSTITUTIONSELECTED, Button.class);
    }

    // return the deal id text field.
    public TextField getTbPageId() {
        return (TextField) getChild(CHILD_TBPAGEID);
    }

    // return the institution id text field.
    public TextField getTbInstitutionId() {
        return (TextField) getChild(CHILD_TBINSTITUTIONID);
    }
    
    //Exchange Link
    public HiddenField getHdFolderCode()
    {
      return (HiddenField) getChild(CHILD_FOLDERCODE);
    }
    
    public doDCFolderModel getdoDCFolderModel()
    {
      if(doDCFolder == null)
      	doDCFolder = (doDCFolderModel)getModel(doDCFolderModelImpl.class);
      return doDCFolder;
    }
    public void setdoDCFolderModel(doDCFolderModel model)
    {
  	  doDCFolder = model;
    }

    // return the deal id text field.
    public TextField getStResponse() {
        return (TextField) getChild(CHILD_STRESPONSE);
    }
    
    // return the miresponse frequcncy hidden field.
    public TextField getHdFrequency() {
        return (TextField) getChild(CHILD_STHDFREQUENCY);
    }
    
    public TextField getHdCmhcAlert() {
        return (TextField) getChild(CHILD_STHDCHMCALERT);
    }
    
    public TextField getHdGenworthAlert() {
        return (TextField) getChild(CHILD_STHDGENWORTHALERT);
    }
    
    public TextField getHdDealLocked() {
        return (TextField) getChild(CHILD_STHDDEALLOCKED);
    }
    
    public TextField getHdMIResponseStatusId() {
        return (TextField) getChild(CHILD_STHMIRESPONSESTATUSID);
    }

    public TextField getHdMIProviderId() {
        return (TextField) getChild(CHILD_STHDMIPROVIDERID);
    }
    
    public TextField getHdSessionTimeOut() {
        return (TextField) getChild(CHILD_STHDSESSIONTIMEOUT);
    }
    
    //quick link menu
       
    public HiddenField getHdQuickLinkEnabled() {
        return (HiddenField) getChild(CHILD_HDQUICKLINKENABLED);
    }

    
    public ImageField getImQL1() {
        return (ImageField) getChild(CHILD_IMQL1);
    }
    
    
    public ImageField getImQL2() {
        return (ImageField) getChild(CHILD_IMQL2);
    }
    
    
    public ImageField getImQL3() {
        return (ImageField) getChild(CHILD_IMQL3);
    }
    
    public StaticTextField getStQuickLinkList() {
        return (StaticTextField) getChild(CHILD_STQUICKLINKLIST_RESET_VALUE);
    }
    
    public StaticTextField getStInstProfileList() {
        return (StaticTextField) getChild(CHILD_STINSTPROFILELIST_RESET_VALUE);
    }
    public StaticTextField getStSelectedInstProfile() {
        return (StaticTextField) getChild(CHILD_STSELECTEDINSTPROFILE_RESET_VALUE);
    }
    public HiddenField getHdSelectedInstProfileId() {
        return (HiddenField) getChild(CHILD_HDSELECTEDINSTPROFILEID_RESET_VALUE);
    }
   
	public Button getBtInstSelected() {
		return (Button) getChild(CHILD_BTINSTITUTIONSELECTED);
	}
}