package com.filogix.express.web.rc.lpr.converter;

import java.util.List;

import com.basis100.mtgrates.PaymentTermInfo;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRResponseObject;

/**
 * Title: LegacyPaymentTermConverter
 * <p>
 * Description: Legacy CSV like data converter for paymentterm 
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 */
public class LegacyPaymentTermConverter extends AbstractLegacyCSVConverter
        implements IConverter<PaymentTermInfo> {

    /**
     * this method converts a result data list from PricingLigc into a legacy CSV like String
     * @see com.filogix.express.web.rc.lpr.converter.IConverter#convert(com.filogix.express.web.rc.lpr.LPRResponseObject, java.util.List, com.basis100.resources.SessionResourceKit)
     */
    public LPRResponseObject convert(LPRResponseObject res,
            List<PaymentTermInfo> list, SessionResourceKit srk) {

        int institutionId = srk.getExpressState().getDealInstitutionId();
        int languageId = srk.getLanguageId();

        StringBuffer sb = new StringBuffer();
        if(list != null)
        for (PaymentTermInfo payment : list) {

            String paymentTerm = BXResources.getPickListDescription(
                    institutionId, "PAYMENTTERM", payment.getPaymentTermId(),
                    languageId);

            sb.append("'" + paymentTerm + "'").append(COLUMN_DELIMITER);
            sb.append("'" + payment.getMtgProdId() + "':");
            sb.append("'" + payment.getPaymentTermId() + "'").append(LINE_DELIMITER);

        }
        res.setPaymentTermString(sb.toString());
        return res;
    }

}
