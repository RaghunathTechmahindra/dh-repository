/*
 * @(#)ExpressWebContext.java    2007-7-31
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.context.ExpressContext;
import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.context.ExpressContextDirector;

/**
 * ExpressWebContext is the helper class to get the services from Express web
 * context.
 *
 * @version   1.0 2007-7-31
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressWebContext implements ExpressContext {

    // the key to get a service factory for web context.
    private static final String EXPRES_WEB_CONTEXT_KEY =
        "com.filogix.express.web";

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressWebContext.class);

    /**
     * returns a service instance from Express web context.
     *
     * @param name the service name.
     * @return an instance of the service.
     * @throw ExpressServiceException if failed to create an instance for the
     * service.
     */
    synchronized public static Object getService(String name)
        throws ExpressServiceException {

        return ExpressContextDirector.getDirector().
            getServiceFactory(EXPRES_WEB_CONTEXT_KEY).
            getService(name);
    }
}