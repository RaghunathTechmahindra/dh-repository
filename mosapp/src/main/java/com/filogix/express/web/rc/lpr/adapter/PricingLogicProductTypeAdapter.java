package com.filogix.express.web.rc.lpr.adapter;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.dao.ProductTypeDao;

/**
 * Title: PricingLogicProductTypeAdapter
 * <p>
 * Description: implementation of adapter
 * @author MCM Impl Team.
 * @version 1.0 Aug 07, 2008 XS_2.1, XS_2.2, XS_2.4 Initial version
 */
public class PricingLogicProductTypeAdapter extends AbstractPricingLogicAdapter {
    
    private final static Log _log = LogFactory.getLog(PricingLogicProductTypeAdapter.class);
    
    public LPRResponseObject process(SessionResourceKit srk,
            LPRRequestObject request, LPRResponseObject response) {

        ProductTypeDao dao = (ProductTypeDao) pricingLogic;
        List<ProductTypeDao> list = null;
        try {
            list = dao.findProductTypeListByProductId(srk, request.getProductId());
        } catch (Exception e) {
          _log.error("failed in extracting data : " + e.getMessage());
          _log.error(StringUtil.stack2string(e));
        }

        return this.converter.convert(response, list, srk);
        
    }

}
