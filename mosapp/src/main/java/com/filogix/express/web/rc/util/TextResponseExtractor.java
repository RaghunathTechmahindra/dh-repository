/*
 * @(#)TextResponseExtractor.java    2006-5-2
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * TextResponseExtractor is the helper class for extract the text base response
 * from persistence layer.
 *
 * @version   1.0 2006-5-2
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class TextResponseExtractor extends AbstractReportExtractor {

    // The logger
    private final static Log _log =
        LogFactory.getLog(TextResponseExtractor.class);

    /**
     * just a helper method.
     */
    public abstract String extractTextReport() throws ExtractReportException;
}
