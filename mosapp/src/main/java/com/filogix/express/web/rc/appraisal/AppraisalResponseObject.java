/*
 * @(#)AppraisalResponseObject.java    2006-6-29
 *
 * Copyright (C) 2006 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc.appraisal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.web.rc.response.ServiceResponseObject;

/**
 * AppraisalResponseObject - 
 *
 * @version   1.0 2006-6-29
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class AppraisalResponseObject extends ServiceResponseObject {

    // The logger
    private final static Log _log =
        LogFactory.getLog(AppraisalResponseObject.class);

    /**
     * Constructor function
     */
    public AppraisalResponseObject() {
    }

    /**
     * set the servicerequesetcontactId
     */
    public void setServiceRequestContactId(int id) {

        put("serviceRequestContactId", new Integer(id));
    }

    /**
     * set the appraisal request status.
     */
    public void setAppraisalStatus(String value) {

        put("appraisalStatus", value);
    }

    /**
     * the appraisal status id.
     */
    public void setAppraisalStatusId(int id) {

        put("appraisalStatusId", new Integer(id));
    }

    /**
     * set the request date,
     */
    public void setRequestDate(String date) {

        put("requestDate", date);
    }

    /**
     * set the appraisal request status message.
     */
    public void setAppraisalStatusMsg(String value) {

        put("appraisalStatusMsg", value);
    }

    /**
     * set the provider reference number.
     */
    public void setProviderRefNum(String value) {

        put("providerRefNum", value);
    }
}
