package com.filogix.externallinks.adjudication.requestprocessor;

import MosSystem.Mc;

import com.basis100.deal.entity.AdjudicationApplicantRequest;
import com.basis100.deal.entity.BncAdjudicationApplRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.pk.BncAdjudicationApplRequestPK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.adjudication.BncGcdServiceInfo;
import com.filogix.externallinks.adjudication.util.BorrowerToBureau;
import com.filogix.externallinks.services.ServiceDelegate;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.SessionStateModel;
import mosApp.MosSystem.SessionStateModelImpl;

/**
 * This class will generate a new Request record, run business rules, 
 * write to the BNCADJUDICATIONAPPLICANTREQUEST table, and call the 
 * outbound web service flow.
 * @author jcheun
 *
 */
public class CreditDecisionHelper {
	private static SysLogger logger;
	
	public CreditDecisionHelper() {
		
	}
	
	/**
	 * This method is the point of entry to the BncGcdProcessor.  
	 * @param srk used to contain session information, passed on to the ServiceDelegate.
	 * @param dealID used to retrieve a Deal.
	 * @param copyID used to retrieve a Deal.
	 * @param languageID 0 or 1.
	 * @param borrowerToBureau Map of borrower IDs to bureau IDs.
	 */
	public void processRequest(SessionResourceKit srk, DealPK dpk, int languageId, SessionStateModel theSessionState, PageEntry pe, List borrowerToBureau) throws Exception {
		String logString = "CreditDecisionHelper.processRequest() - ";
		logger = srk.getSysLogger();
		try {
			logger.debug(logString + "Entering method for deal ID: " + dpk.getId() + " and copy ID: " + dpk.getCopyId());
			srk.beginTransaction();
			Deal deal = retrieveDeal(srk, dpk);
			Request newRequest = createRequest(srk, deal);
			writeBNCAdjudicationApplRequest(srk, newRequest, borrowerToBureau);
			invokeWebService(srk, newRequest,deal, languageId);
			srk.commitTransaction();
		} catch (Exception e) {
			logger.error(logString + "Exception occurred: " + e.getMessage());
			srk.cleanTransaction(); //rollback, and propagate error upwards.
			throw e;
		}
	}
	
	/**
	 * Helper method for finding a Deal object.
	 * @param srk
	 * @param dealId
	 * @param copyId
	 * @return
	 * @throws RemoteException
	 * @throws FinderException
	 */
	private Deal retrieveDeal(SessionResourceKit srk, DealPK dpk) throws RemoteException, FinderException {
		String logString = "CreditDecisionHelper.retrieveDeal - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method for deal ID: " + dpk.getId() + " and copy ID: " + dpk.getCopyId());
        Deal aDeal = new Deal(srk, null);

        aDeal.findByPrimaryKey(dpk);
        return aDeal;
	}
	
	/**
	 * This method will generate a new Request object (new record in the database).  This requires the instantiation of a DealPK.
	 * @return Populated Request object.
	 */
	public Request createRequest(SessionResourceKit srk, Deal deal) throws Exception {
		String logString = "CreditDecisionHelper.createRequest - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method for deal ID: " + deal.getDealId() + " and copy ID: " + deal.getCopyId());
        Request request = new Request(srk);

        request.create(new DealPK(deal.getDealId(), deal.getCopyId()),
                       Mc.REQUEST_STATUS_ID_NOT_SUBMITTED, Calendar.getInstance().getTime(),
                       deal.getUnderwriterUserId());
        request.setRequestTypeId(0);
        request.setDealId(deal.getDealId());
        request.setCopyId(deal.getCopyId());
        request.setServiceProductId(Mc.SERVICE_PRODUCT_ID_GCD);
        request.setChannelId(Mc.GCD_CHANNEL_ID);
        request.setServiceTransactionTypeId(Mc.REQUEST_SERVICE_TRANSACTION_TYPE_ID);
        request.setPayloadTypeId(Mc.BNC_GCD_REQUEST_PAYLOAD_TYPE);
        request.setRequestDate(new Date());
        request.ejbStore();
        
        logger.debug(logString + "Created request " + request.getRequestId());
        
		return request;
	}
	
	/**
	 * This method will take the Map of borrower to bureau and write new records in the BNCADJUDICATIONAPPLREQUEST table.
	 *
	 */
	public void writeBNCAdjudicationApplRequest(SessionResourceKit srk, Request request, List borrowerToBureau) throws Exception {
		String logString = "CreditDecisionHelper.writeBNCAdjudicationApplRequest - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method for request ID: " + request.getRequestId());
		Iterator iterator = borrowerToBureau.iterator();
		int borrowerId;
		int bureauId;
		BncAdjudicationApplRequestPK pk;
		BncAdjudicationApplRequest bncAdjudicationApplRequest;
		RequestPK rpk;
		BorrowerPK bpk;
		int applicantNumber = 0;
		String applicantCurrentInd="Y";
		
		//Create a BNCADJUDICATIONAPPLREQUEST record for each borrower-bureau mapping
		while (iterator.hasNext()) {
			BorrowerToBureau btb = (BorrowerToBureau)iterator.next();
			if (btb != null) {
				try
				{
					borrowerId = btb.getBorrowerId();
					bureauId = btb.getBureauId();

					pk = new BncAdjudicationApplRequestPK(request.getRequestId(), applicantNumber, request.getCopyId());
					bncAdjudicationApplRequest = new BncAdjudicationApplRequest(srk);
					
					rpk = new RequestPK(request.getRequestId(), request.getCopyId());
					bpk = new BorrowerPK(borrowerId, request.getCopyId());
					
					//create parent record first
					AdjudicationApplicantRequest adjApplRequest = new AdjudicationApplicantRequest(srk);
					adjApplRequest.create(rpk, bpk, applicantNumber, applicantCurrentInd);
					adjApplRequest.ejbStore();
					
					//then create BNCADJUDICATIONAPPLREQUEST record
					bncAdjudicationApplRequest.create(pk, bureauId);
					bncAdjudicationApplRequest.ejbStore();
					
					//bncAdjudicationApplRequest.createWithAdjudicationApplRequest(pk, rpk, bpk, applicantNumber, applicantCurrentInd, bureauId);
				} catch (CreateException ex)
				{
					logger.error(logString + "Error occurred while saving BncAdjudicationApplRequest entity: " + ex.getMessage());
					throw ex;
				}
			}
			applicantNumber++;
		}
	}
	
	/**
	 * This method will start the outbound web service flow.
	 *
	 */
	public void invokeWebService(SessionResourceKit srk, Request request, Deal deal, int languageId) throws Exception {
		String logString = "CreditDecisionHelper.invokeWebService - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method for request ID: " + request.getRequestId());
		BncGcdServiceInfo serviceInfo = new BncGcdServiceInfo();
		serviceInfo.setRequest(request);
		serviceInfo.setDeal(deal);
		serviceInfo.setRequestId(request.getRequestId());
		serviceInfo.setCopyId(new Integer(deal.getCopyId()));
		serviceInfo.setDealId(new Integer(deal.getDealId()));
		serviceInfo.setLanguageId(languageId);
		serviceInfo.setRequestType(Mc.GCD_SPRING_REQUEST_TYPE);
		serviceInfo.setSrk(srk);
		
		ServiceDelegate delegate = new ServiceDelegate();
		
		logger.debug(logString + "Calling ServiceDelegate.sendGCDRequest");
		delegate.sendGCDRequest(serviceInfo);
	}
}
