package com.basis100.perfmonitor;

import java.util.*;
import java.io.*;

import config.*;
import com.basis100.log.*;
import com.basis100.resources.*;

/**
 * Useful class for monitoring performance
 */
public class PerfMonitor {
  private   static final String  generalClass = "General";
  protected static final int     initialSize  = 10000;
  private   static final int     deltaSize    = 1000;
  private static boolean wrapBuffer    = true;
  private static int   size       = initialSize;
  private static int   count      = 0;
  private static long  time       = 0;
  private static long  interval[] = new long[initialSize];
  private static Vector registry   = null;

  private static PerfMonitor monitor[] =
  new PerfMonitor[initialSize];

  /**
    * Set the flag for buffer wrapping.
    * flag whether to wrap the buffer when full
    * flag=true for buffer wrapping
    * flag=false for buffer extending
    */
  public static void setWrapBuffer(boolean flag) {
    wrapBuffer = flag;
  }

  /**
    * Resets the statistics maintained by each registered
    * PerfMonitor.
    **/
  public synchronized static void clearMonitors() {
    int size = registry.size();
    for (int i = 0; i < size; i++) {
      PerfMonitor mon = (PerfMonitor)(registry.elementAt(i));
      mon.clearMonitor();
    }
    time = 0;
  }

  /**
    * Processes the history of posted events, updating the
    * statistics in the PerfMonitors that have events logged.
    **/
  public synchronized static void processMonitors() {
    synchronized (interval) {
      for (int i = 0; i < count; i++) {
        monitor[i].processMonitor(interval[i]);
        time = time + interval[i];
      }
      count = 0;
    }
  }

  /**
   * Prints the statistics of each PerfMonitor after
   * processing the history of posted events and updating
   * the statistics for each PerfMonitor that had events
   * logged.
   *
   **/
  public synchronized static void printStats(PrintWriter out){
    processMonitors();
    int size = registry.size();
    for (int i = 0; i < size; i++) {
      PerfMonitor mon = (PerfMonitor)(registry.elementAt(i));
      if (mon.getEventCount() > 0) {
        mon.printMonitor(out);
        out.flush();
      }
    }
  }

  /**
    * Prints the statistics of each PerfMonitor to System.out
    * after processing the history of posted events and
    * updating the statistics for each PerfMonitor that had
    * events logged. Output sent to System.out.
    **/
  public synchronized static void printThenClearStats() {
    printStats(new PrintWriter(System.out));
    count = 0;
    time = 0;
    clearMonitors();
  }

  /* Non Static code begin */

  private String monitorClass;
  private String monitorId;
  private int    eventCount;
  private long   accumTime;
  private double sumSquares;
  private long startTime;         // used for timing

  /**
    * Constructs a PerfMonitor with the specified major id
    * and minor id.
    * @param classid major id used to identify the PerfMonitor
    * @param id minor id used to identify the PerfMonitor
    **/
  public PerfMonitor (String classId, String id) {
    monitorClass = classId;
    monitorId = id;
    eventCount = 0;
    accumTime  = 0;
    sumSquares = 0.0;

    registerMonitor(this);
  }

  /**
    * Constructs a PerfMonitor that has a major id of ""
    * and a minor id specified as input.
    * @param id minor id used to identify the PerfMonitor
    **/
  public PerfMonitor (String id) {
    monitorClass = "";
    monitorId = id;
    eventCount = 0;
    accumTime  = 0;
    sumSquares = 0.0;

    registerMonitor(this);
  }

  /**
    * Constructs a PerfMonitor that has a major id of
    * "General" and an empty string for the minor id.
    **/
  public PerfMonitor () {
    monitorClass = generalClass;
    monitorId = "";
    eventCount = 0;
    accumTime  = 0;
    sumSquares = 0.0;

    registerMonitor(this);
  }

  /**
   * Clears the statistics for an individual monitor.
   */
  public void clearMonitor() {
    eventCount = 0;
    accumTime = 0;
    sumSquares = 0.0;
    startTime = 0;
  }

  /**
   * Return the accumulated time for an individual monitor.
   */
  public long getTime() {
    return accumTime;
  }

  /**
   * Return the number of recorded events for an individual
   * monitor.
   */
  public int getEventCount() {
    return eventCount;
  }

  /**
   * Print the gathered data of an individual monitor.
   * Override this method to print your own statistics.
   */
  public void printMonitor(PrintWriter out) {
    if (eventCount > 0) {
      System.out.println(monitorClass + ':' + monitorId
                  + " Count " + eventCount
                  + " Time " + accumTime
                  + " Ave " + monitorMeanTime());
      out.flush();
    }
  }

//  public void printMonitor(SysLogger logger) {
//    if (eventCount > 0) {
//      logger.debug(monitorClass + ':' + monitorId
//                  + " Count " + eventCount
//                  + " Time " + accumTime
//                  + " Ave " + monitorMeanTime());
//    }
//  }

  /**
   * Start recording the time for a monitor.
   * Override this method to record your own statistics
   * at the start of a timing loop. You can pass
   * any object you need as data to your overridden
   * method.
   */
  public void startTiming(Object input) {
    startTime = System.currentTimeMillis();
  }

  /**
   * Stop recording the time for a monitor.
   * Override this method to record your own statistics
   * at the stop of a timing loop.You can pass
   * any object you need as data to your overridden
   * method.
   */
  public long stopTiming(Object input) {

    long transaction_time = System.currentTimeMillis() 
                            - startTime;
    startTime = 0;
    postMonitor(transaction_time);
    return transaction_time;
  }

  /* Private methods */

  /** 
    * Record the event in a history buffer for processing 
    * at a later time. The duration (time in milliseconds) 
    * of the event being recorded is passed in.
    **/
  private synchronized void postMonitor(long delta) {
    synchronized (interval) {
      try {
        interval[count] = delta;
        monitor [count] = this;
        count++;
      } catch (ArrayIndexOutOfBoundsException e) {
        if (wrapBuffer) {
          processMonitors();

          count = 0;

          interval[count] = delta;
          monitor [count] = this;
          count++;

        } else {
          long tempInterval[] = interval;
          PerfMonitor tempMonitor[] = monitor;
          int oldSize = size;

          size = size + deltaSize;
          interval = new long[size];
          monitor  = new PerfMonitor[size];

          try {
            System.arraycopy(tempInterval, 0, 
                             interval, 0, oldSize);
            System.arraycopy(tempMonitor, 0, monitor, 
                             0, oldSize);

            interval[count] = (int)delta;
            monitor [count] = this;
            count++;
          } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
          }
        }
      }
    }
  }

  private void processMonitor(long eventTime) {
    eventCount++;
    accumTime = accumTime + eventTime;

    sumSquares = sumSquares + (((double)eventTime) * 
                               ((double)eventTime));
  }

  private final double monitorMeanTime() {
    double result = 0.0;
    if (eventCount > 0) {
      result = (((double)accumTime) / eventCount);
    }

    return (result);
  }

  private final double monitorStdDev() {
    double result = 0.0;
    if (eventCount > 1) {
      double mean = monitorMeanTime();
      double variance = (sumSquares - 
                         (mean * mean * eventCount)) / 
                         (eventCount - 1);
      result = Math.sqrt(variance);
    }

    return (result);
  }

  private synchronized static 
     void registerMonitor(PerfMonitor monitor) {
    if (registry == null)
      registry = new Vector(30);

    registry.addElement(monitor);
  }
}
