package com.basis100.deal.security;

import java.io.*;
import java.util.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

import com.basis100.entity.*;
import com.basis100.resources.*;
import com.basis100.log.*;

import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;

import mosApp.MosSystem.PageEntry;


/**
* Support for undo is accomplishing by manipulating a copy of original deal data.
* If the changes are submitted the copy is adopted as the true copy, otherwise the
* temporary (transactional) copy is simply abandoned and remove the record in the
* database of original data untouched.
*
* Copies are created via copy() in MasterDeal. A saved scenario is a copy of
* of the deal used by the an underwriter to creates copies of a deal to
* manipulate indepedent of the original data.
*
* DealEditControl is used to create and manipulate copies of a deal to support
* the edit and undo behaviors. basically, before an edit page is displayed
* a DealEditControl object is created and stored in the PageEntry Object. The
* object provides methods to create, track, and manipulate transaction copied of a deal.
*
*
* New transactional copy created by:
*   1) constructors  & 2) newCopy()
* In both cases common() is called to create the copy.
*
* Audit Details
* -------------
*
* Audit record are stored using an audit Id that matched the copy id of the deal tree being manipulated.
* This allows support for submit/undo behavior as follows:
*
*   . if a transactional copy of a deal is abandoned any audit records associated with the copy
*     should also be removed (i.e. delete those with audit id == copy id dropped)
*
*   . if a transaction copy is adopted any audit records associated with the copy remain and audit
*     records asociated with the source should be 'absorbed' into the adopted copy; this includes the
*     special case when the tx copy adopted is the sole copy hence upon adoption becoming the true copy
*     replacing tyhe 'source' copy; all audit for true copy stored with audit id = 0 indicating real audit activity
*     (e.g. not tracsactional subject to adoption of tx copy used during tenative changes).
*
* No audit records should be created to reflect the database activity occuring during the creation of a transactional
* copy, nor should the removal of a copy (source of transactional) generate audit records.
*
*
*
**/


public class DealEditControl implements java.io.Serializable
{
  private CopyInfo source;
  private boolean diagMode = false;

  private Vector transactionCopies = null;    // all Tx copy list of the given deal

  public DealEditControl(MasterDeal md, int copyId, SessionResourceKit srk) throws Exception
  {
      diagMode = (PropertiesCache.getInstance().getProperty(-1, "com.basis100.deal.edit.control.diag.mode", "N")).equals("Y");

      SysLogger logger = srk.getSysLogger();

      if (diagMode == false)
        logger.setDebugTraceJdbcTrace(false, false, false);

      try
      {
        common(md, md.getDealId(), copyId, srk, true);
        if (diagMode == false)
          logger.setDebugTraceJdbcTraceRestore();

        show(srk, "Upon Object Construction [4]");
      }
      catch (Exception e)
      {
          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          throw e;
      }
  }

  public DealEditControl(int dealId, int copyId, boolean isDealEntry, SessionResourceKit srk) throws Exception
  {
      diagMode = (PropertiesCache.getInstance().getProperty(-1, "com.basis100.deal.edit.control.diag.mode", "N")).equals("Y");

      SysLogger logger = srk.getSysLogger();

      if (diagMode == false)
        logger.setDebugTraceJdbcTrace(false, false, false);

      try
      {
          if (!isDealEntry)
          {
            common(null, dealId, copyId, srk, true);

            if (diagMode == false)
              logger.setDebugTraceJdbcTraceRestore();

            show(srk, "Upon Object Construction [1]");

            return;
          }

          // special constructor for deal entry only - the deal is by definition transactional ...

          // source copy id == -1
          source = new CopyInfo(dealId, -1, true, "G");

          // deal is tx copy
          transactionCopies = new Vector();
          transactionCopies.add(new CopyInfo(dealId, copyId, true, "T"));  // create the list

          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          show(srk, "Upon Object Construction [1] DEAL ENTRY VERSION]");
      }
      catch (Exception e)
      {
          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          throw e;
      }

  }

  public DealEditControl(int dealId, int copyId, SessionResourceKit srk) throws Exception
  {
      diagMode = (PropertiesCache.getInstance().getProperty(-1, "com.basis100.deal.edit.control.diag.mode", "N")).equals("Y");

      SysLogger logger = srk.getSysLogger();

      if (diagMode == false)
        logger.setDebugTraceJdbcTrace(false, false, false);

      try
      {
          common(null, dealId, copyId, srk, true);

          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          show(srk, "Upon Object Construction [2]");
      }
      catch (Exception e)
      {
          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          throw e;
      }

  }

  public DealEditControl(int dealId, SessionResourceKit srk) throws Exception
  {
      diagMode = (PropertiesCache.getInstance().getProperty(-1, "com.basis100.deal.edit.control.diag.mode", "N")).equals("Y");

      SysLogger logger = srk.getSysLogger();

      if (diagMode == false)
        logger.setDebugTraceJdbcTrace(false, false, false);

      try
      {
          common(null, dealId, -1, srk, true);

          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          show(srk, "Upon Object Construction [2]");
      }
      catch (Exception e)
      {
          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          throw e;
      }
  }

  //
  // Methods
  //


  public boolean isTxCopy()
  {
    return transactionCopies != null;
  }

  public int getNumTxCopies()
  {
    if (transactionCopies == null)
      return 0;

    return transactionCopies.size();
  }

  // adoptTxLevel:
  //
  // Level indicates the ordinal (e.g. 1 = Fst) of the existing tx copy that is to be adopted. If additional copies
  // exist beyond the level to adopt the additional copies are dropped automatically. The nunber of tx copies after
  // operation is normally level - 1 (i.e. 0, or none if level 1 adopted).
  //
  // If no copy existed for the level specified (e.g. level = 2 when there is only one tx copy) the call is ignored.
  //
  // If a page entry is provided the method will update its <txCopyLevel> property with the number of tx copies remaining
  // after the adopt, or with -1 if none.

  public void adoptTxLevel(int level, SessionResourceKit srk, PageEntry pg) throws Exception
  {
      show(srk, "DEC.adoptTxLevel(" + level + ") -- IN");

      if (level > getNumTxCopies())
        return;

      SysLogger logger = srk.getSysLogger();

      if (diagMode == false)
        logger.setDebugTraceJdbcTrace(false, false, false);

      try
      {
          while(level < getNumTxCopies())
          {
              logger.debug("@DealEditControl.adoptTxLevel: level = <" + level + ">, automatically dropping addition tx copy number = <" + getNumTxCopies() + ">");
              dropCopy(srk);
          }

          adoptCopy(srk, logger);

          if (pg != null)
          {
            int newLevel = (getNumTxCopies()) > 0 ? getNumTxCopies() : -1;
            pg.setTxCopyLevel(newLevel);
          }

          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          show(srk, "DEC.adoptTxLevel() -- OUT");
      }
      catch (Exception e)
      {
          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          throw e;
      }
  }


  // dropToTxLevel:
  //
  // Level is the number (at most) of tx copies that should be present after call; e.g. 2 = two tx copies.
  // If the number of copies is greater than the desired level the addition copies are dropped automatically.
  //
  // Note, 0 is a valid value for level upon call - this would cause all tx copies (if any) to be dropped.
  //
  // If a page entry is provided the method will update its <txCopyLevel> property with the number of tx copies remaining
  // after the adopt, or with -1 if none.


  public void dropToTxLevel(int level, SessionResourceKit srk, PageEntry pg) throws Exception
  {
      show(srk, "DEC.dropToLevel(" + level + ") -- IN");

      if (level >= getNumTxCopies())
          return;

      SysLogger logger = srk.getSysLogger();

      if (diagMode == false)
        logger.setDebugTraceJdbcTrace(false, false, false);

      try
      {
        logger.debug("@DealEditControl.dropToTxLevel: level=<" + level + ">, existing number tx copies=<" + getNumTxCopies() + ">");

        while(level < getNumTxCopies())
        {
            dropCopy(srk);
        }

        if (pg != null)
        {
          int newLevel = (getNumTxCopies()) > 0 ? getNumTxCopies() : -1;
          pg.setTxCopyLevel(newLevel);
        }

        if (diagMode == false)
          logger.setDebugTraceJdbcTraceRestore();

        show(srk, "DEC.dropToLevel() -- OUT");
      }
      catch (Exception e)
      {
          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          throw e;
      }
  }



  // A new Tx copy is created based upon the last present Tx copy, or based on the
  // source if the copy list is empty.
  public int newCopy(SessionResourceKit srk) throws Exception
  {
      show(srk, "DEC.newCopy() -- IN");

      SysLogger logger = srk.getSysLogger();

      if (diagMode == false)
        logger.setDebugTraceJdbcTrace(false, false, false);

      try
      {
          CopyInfo ci = source;

          if (transactionCopies != null)       //
              ci = (CopyInfo)transactionCopies.lastElement();    // ci is the new copy?

          common(null, ci.getDealId(), ci.getCopyId(), srk, false);  //list is empty!

          ci = (CopyInfo)transactionCopies.lastElement();     // ci is the new

          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          show(srk, "DEC.newCopy() -- OUT");

          return ci.getCopyId();
      }
      catch (Exception e)
      {
          if (diagMode == false)
            logger.setDebugTraceJdbcTraceRestore();

          throw e;
      }

  }

  //The last Tx copy is adopted and the previous copy should be removed
  private void adoptCopy(SessionResourceKit srk, SysLogger logger) throws Exception
  {
      if (transactionCopies == null)
        return;

      int lastNdx = transactionCopies.size() - 1;

      CopyInfo adopted = (CopyInfo)transactionCopies.lastElement();
      transactionCopies.remove(lastNdx);
      lastNdx--;

      MasterDeal md = new MasterDeal(srk, null);
      md.findByPrimaryKey(new MasterDealPK(source.getDealId()));

      // update audit records
      int newAid = 0;
      try
      {
        JdbcExecutor jExec = srk.getJdbcExecutor();

        // new audit id is audit id of adopted copy except when adopted copy is only trnasactional copy
        // in which case sudit is 0 (i.e. noe permanent since copy becomes true data)
        boolean hasPrevCopy = lastNdx >= 0;
        newAid = (hasPrevCopy) ? adopted.getCopyId() : 0;
        int oldAid = (hasPrevCopy) ? ((CopyInfo)transactionCopies.lastElement()).getCopyId() : adopted.getCopyId();

        String sql = "Update " +  this.getEntityTableName() +
                     " Set AID = " + newAid +
                     " Where DChUserProfileID = " + srk.getExpressState().getUserProfileId() + " AND AID = " + oldAid;

        jExec.executeUpdate(sql);

      }
      catch(Exception e)
      {
          String msg =  "DEC.adoptCopy(): Exception updating audit records: ";
          msg += "AuditID: " + srk.getAuditId();
          logger.error(msg);
          logger.error(e);

          throw e;
      }

      if (lastNdx >= 0)       // prev Tx copy exists
      {
          // adopt the last copy where there is more than one copy - delete 2nd to last

          CopyInfo dropped = (CopyInfo)transactionCopies.lastElement();

          transactionCopies.remove(lastNdx);    //remove the previous copy

          transactionCopies.add(adopted);    // and add the adopted copy

          srk.setInterimAuditOn(false);
          md.deleteCopy(dropped.getCopyId());
          srk.restoreAuditOn();

          md.ejbStore();

          return;
      }

      //No previous Tx copy (i.e. submit main page) lone transactional copy being adopted ...
      transactionCopies = null;
      int delCopyId = source.getCopyId();

      // delete source (but update master record first if source was gold copy)
      if (source.isGoldCopy() == true)
      {
          md.setGoldCopyId(adopted.getCopyId());
          // md.ejbStore();  not necessary!!!
      }

      if (delCopyId != -1)
      {
          srk.setInterimAuditOn(false);
          md.deleteCopy(delCopyId);  // otherwise was deal entry (i.e. there was no source copy)
          srk.restoreAuditOn();
      }

      md.ejbStore();

      source.setCopyId(adopted.getCopyId());

      // set copy type for adopted copy
//      JdbcExecutor jExec = srk.getJdbcExecutor();
//      String sql = "Update deal set COPYTYPE = '" + source.getCopyType() + "' where DEALID = " + source.getDealId() + " and COPYID = " + adopted.getCopyId();
//      jExec.executeUpdate(sql);
      
      //
      Deal deal = new Deal(srk, null, source.getDealId(), adopted.getCopyId());
      deal.setCopyType(source.getCopyType());
      deal.ejbStore();

      // update audit id
      srk.setAuditId(newAid);
  }

  // Need to delete the record in DealChangeHistory table with
  // auditId = srk.getAuditId(), and assign the auditId = copyId of
  // previous Tx copy
  private void dropCopy(SessionResourceKit srk) throws Exception
  {
      if (transactionCopies == null)
        return;

      int numCopies = transactionCopies.size();
      CopyInfo dropped = (CopyInfo)transactionCopies.lastElement();

      if (numCopies == 1)
        transactionCopies = null;               // last transaction copy dropped - now empty list
      else
        transactionCopies.remove(numCopies-1);  // more than one copy - drop last

      MasterDeal md = new MasterDeal(srk, null);
      md.findByPrimaryKey(new MasterDealPK(source.getDealId()));

      srk.setInterimAuditOn(false);
      md.deleteCopy(dropped.getCopyId());
      srk.restoreAuditOn();

      if (md.getNumberOfCopies() >= 1)
        md.ejbStore(); // the else is that md self destructed (e.g. called its own ebjRemove())

      try
      {
        // delete the record with auditId == srk.getAuditId();

        JdbcExecutor jExec = srk.getJdbcExecutor();

        String sql = "Delete From " +  this.getEntityTableName() + " Where AID = " +
                    dropped.getCopyId() + " And DChUserProfileId = " + srk.getExpressState().getUserProfileId();

        jExec.executeUpdate(sql);

        if (transactionCopies == null)
          srk.setAuditId(0);
        else
        {
          CopyInfo preCopy = (CopyInfo)transactionCopies.lastElement();
          srk.setAuditId(preCopy.getCopyId());
        }
      }
      catch(Exception e)
      {
          SysLogger logger = srk.getSysLogger();
          String msg =  "DEC.dropCopy(): Exception removing audit records: ";
          msg += "AuditID: " + srk.getAuditId();
          logger.error(msg);
          logger.error(e);
          throw e;
      }
  }


  public int getCID()
  {
      CopyInfo ci = source;

      if (transactionCopies != null)
          ci = (CopyInfo)transactionCopies.lastElement();

      return ci.getCopyId();
  }

  // get audit id
  public int getAID()
  {
      if (transactionCopies != null)
      {
          CopyInfo ci = (CopyInfo)transactionCopies.lastElement();
          return ci.getCopyId();
      }

      return 0;
  }

    /**
     * returns the source copy id.
     */
    public int getSourceCID() {

        return source.getCopyId();
    }

    /**
     * returns all copyids as a array.
     */
    public int[] getAllCIDs() {

        int[] ret = new int[getNumTxCopies() + 1];

        ret[0] = source.getCopyId();
        for (int i = 0; i < transactionCopies.size(); i++) {
            CopyInfo ci = (CopyInfo)transactionCopies.elementAt(i);
            ret[i + 1] = ci.getCopyId();
        }

        return ret;
    }

  //
  // Implementation
  //

  // helper class

   // All the constructors and newCopy() call this method to create a new copy of the deal
   // So, assign the auditId = copyId of the new copy
  private void common(MasterDeal md, int dealId, int copyId, SessionResourceKit srk, boolean constructor) throws RemoteException, FinderException, DealCopyException
  {
      if (md == null)
      {
          md = new MasterDeal(srk, null);
          md = md.findByPrimaryKey(new MasterDealPK(dealId));
      }

      if (copyId == -1)     //CID is not correct!
          copyId = md.getGoldCopyId();

      Deal deal = new Deal(srk, null);
      deal = deal.findByPrimaryKey(new DealPK(md.getDealId(), copyId));

      if (constructor)
        source = new CopyInfo(md.getDealId(), copyId, copyId == md.getGoldCopyId(), deal.getCopyType());

      // create a new Tx copy
      srk.setInterimAuditOn(false);
      int newCopyId = md.copy(copyId);
      srk.restoreAuditOn();

      md.ejbStore();

      // set copy type to transactional
      try
      {
//        JdbcExecutor jExec = srk.getJdbcExecutor();
//        String sql = "Update deal set COPYTYPE = 'T' where DEALID = " + source.getDealId() + " and COPYID = " + newCopyId;
//        jExec.executeUpdate(sql);
        
        deal = new Deal(srk, null,  source.getDealId(), newCopyId);
        deal.setCopyType("T");
        deal.ejbStore();
      }
      catch (Exception e)
      {
        SysLogger logger = srk.getSysLogger();
        logger.error("Exception @DealEditControl.common(): setting copy type of new copy to T");
        logger.error(e);

        throw new RemoteException(e.getMessage());
      }

      // create/add to copies list
      if (transactionCopies == null)
          transactionCopies = new Vector();

       // the Tx copy list is updated.
      transactionCopies.add(new CopyInfo(md.getDealId(), newCopyId, source.isGoldCopy(), deal.getCopyType()));

      srk.setAuditId(newCopyId);
  }

    public void show(SessionResourceKit srk, String msg)
  {
      SysLogger logger = srk.getSysLogger();

      if (msg == null || msg.length() == 0) msg = "";  else  msg = ":: " + msg;

      logger.debug("Deal Edit Control Object Trace " + msg);

      logger.debug("SOURCE");

      CopyInfo ci = source;

      if (ci == null)
      {
        logger.debug("NO SOURCE OBJECT (is null) - ABNORMAL!");
      }
      else
      {
        logger.debug("     :: deal id   = " + ci.getDealId());
        logger.debug("     :: copy id   = " + ci.getCopyId());
        logger.debug("     :: is gold   = " + ci.isGoldCopy());
        logger.debug("     :: copy type = " + ci.getCopyType());
      }

      int lim = 0;
      if (transactionCopies == null || (lim = transactionCopies.size()) == 0)
      {
        logger.debug("NO TRANSACTIONAL COPIES");
        return;
      }

      for (int i = 0; i < lim; ++i)
      {
          ci = (CopyInfo)transactionCopies.elementAt(i);

          logger.debug("TRANSACTION COPY[" + i + "]");

          logger.debug("     :: deal id   = " + ci.getDealId());
          logger.debug("     :: copy id   = " + ci.getCopyId());
          logger.debug("     :: is gold   = " + ci.isGoldCopy());
          logger.debug("     :: copy type = " + ci.getCopyType());
      }
   }

   public String getEntityTableName()
   {
      return "DealChangeHistory";
   }
}
