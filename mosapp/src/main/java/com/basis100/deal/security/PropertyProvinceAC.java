/**
 * <p>Title: ProvinceAC.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Mar 21, 2006)
 *
 */

package com.basis100.deal.security;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.SavedPagesModel;
import mosApp.MosSystem.SessionStateModel;
import MosSystem.Mc;
import MosSystem.Sc;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;

public class PropertyProvinceAC implements Cloneable
{
    private int provinceCode=Mc.PROVINCE_OTHER;
    private Deal deal = null;
    
    public PropertyProvinceAC(Deal deal) throws Exception
    {
        this.deal = deal;
        Collection c = deal.getProperties();
        Property prop = null;
        Iterator it = c.iterator();

        // loop until a primary (subject) property is found.
        while (it.hasNext())
        {
          prop = (Property) it.next();

          if (prop.isPrimaryProperty())
          {
            break;
          }
        }
        
        if (prop != null)
        {
            provinceCode = prop.getProvinceId();
        }
    }
    
    public int accessDisclosure(SessionStateModel theSessionState)  throws Exception
    {
        if (deal == null ) return Sc.PAGE_ACCESS_DISALLOWED;
        //int dealStatus = deal.getStatusId();
        int langId = theSessionState.getLanguageId();
        
        switch (provinceCode)
        {
            case Mc.PROVINCE_ONTARIO:
                return Sc.PAGE_ACCESS_EDIT;
            case Mc.PROVINCE_NEWFOUNDLAND:
            case Mc.PROVINCE_ALBERTA:
            case Mc.PROVINCE_PRINCE_EDWARD_ISLAND:
            case Mc.PROVINCE_NOVA_SCOTIA:
              if(langId == Mc.LANGUAGE_PREFERENCE_ENGLISH)
            	return Sc.PAGE_ACCESS_EDIT;
              else
            	return Sc.PAGE_ACCESS_DISALLOWED;
            case Mc.PROVINCE_OTHER:
                return Sc.PAGE_ACCESS_DISALLOWED;
            default:
                return Sc.PAGE_ACCESS_NOT_FOUND;
        }
    }
}
