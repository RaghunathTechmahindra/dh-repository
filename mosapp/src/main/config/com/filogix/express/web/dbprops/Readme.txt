Sample Format Lines
===================

To attach a method with Page Variables
======================================
creditReferenceId=RepeatedCreditReference.hdCreditRefId!getFormattedId()

Combine two Page Variables for One Entity Fields
================================================
creditRefTypeId=RepeatedCreditReference.cbCreditRefsType+RepeatedCreditReference.txReferenceDesc

All inclusive
=============
copyId=RepeatedCreditReference.hdCreditRefCopyId!oneMethod()+secondHdFld!twoMethod()@allMthod()

Delim Definitions
===============
If there is more than one Page Variable involved then both of them can be combined
with '+' delimater

If there is any method need to be used to parse particular page value then that method
name can be attached with page Variable Name and '!' delimater

If there is any method need to be used to parse the all page Values for single enity then 
the method name can be attached with Page Variable(s) name and '@' delimater

Only one method name can be attached (not inluding general parser) for one Page variable
The delimaters(+,!,@) I used here are invalid characters for Netdynamics Page Variable Name

//--Release2.1--//
One method name option was fixed. Currently for the Release2.1 for translation purposes this call
has been excluded since the new mechanism in place, but the '!' option works!!!


when reading
============

loadThisProperties(pathName, proopertyFileName)
{
  Check for List holder whether Property file has been loaded already;

  if  loaded then 
    return the Hashtable list which has been loaded already

  if not
    load the property file for the given name // which contains entity property as key value and Page variable with defined tag and method needed to be operated on the Page values

  loop thru property contents
    get key value 			//entity property name
    get the Value for this key 		//page variable + method need to be use to parse the page values seperated with defined tags
    send the value to parseParameterToArray() and get String array Holder
    put in to Hashtable List (key Value and String Array Holder contains parsed Value)
  end loop

  return hash table list
}


parseParameterToArray(String paramName)
{
  check any general Method Name attached to the line
  if then
    seperate the param Name Line and method Name
  
  assign the general method name to return array holder in index 0 column 1
	assign empty string value to index 0 column 2 I don't use this index
  tokenize the rest of the line with delim '+' to get multiple page variable if any
  loop thru tokens
    seperate the Page variable Name and individual parsing method name
    assign page variable name in to column 1 and individual parsing method name in to column 2
  end loop

  return array holder
 
}

So at the end loading updatePropList the List will be like below

Hashtable updatePropList
---------------------------------------------------
key			|	Value
----------------------------------------------------
propertyfilename	| Hashtable thisNameFileList
:			|
:			|
:			|
----------------------------------------------------


Hashtable thisNameFileList
----------------------------------------------------
key			|	Value
----------------------------------------------------
entityPropertyName	| [0..n Rows] [2 Cols] array
			| - [0,0] index holds general 
			|   method Name
			| - [1..n,0] holds Page 
			|   Variable Name
			| - [1..n,1] holds individual 
			|   Parse Method
:			|
:			|
:			|
-----------------------------------------------------

During Update
=============

  //This method should be called by single row page
  updateForMainRows( Hashtable propList,
			String[] keyValues,
  			String entityName, 
  			SessionResourceKit srkit,
			SessionState theSession ) throws Exception
  {
    get the primary id and copy id value from current page assume that primary var name assigned in 
    index 0 of the keyvalues and index 1 for copy id

    Construct the entity instance using enitituy builder
    parsePageValues To Entity for single row //pass row index value -1
  }


  updateForRepeatedRows( Hashtable propList,
 			String[] keyValues,
    			String entityName, 
    			SessionResourceKit srkit,
 			SessionState theSession ) throws Exception
  {
    get No Of Rows for for the page
    if the noofrows <0
    then
      call updateForMainRows	//bcose is similiar to update single row

    else
      loop thru until noofrows
        get the primary key value and copy id value for this row
        Construct the entity instance using enitituy builder
        parsePageValues To Entity for multiple row //pass row index value loop index value
      end loop
    endif
  }


  parsePageValuesToEntity(Hashtable fieldList, DealEntity entity,int rowindex)
  {
    loop thru fieldlist contents
      get entity properties which is key value
      get Page Variable Contents which is attached value for entity properties
      get Page Variable Value from getThisDisplayFieldValue()
      setEntity Field Value
    end loop

    return entity Instance

  }

  getThisDisplayFieldValue(String[][] pageVarContents, int rowindex
  {
    get General Method Name from Index 0 of array Holder
    init paramarray with size equal to page var contents.size() -1
           //new array size is less than 1 bcose first index is used to hold the method name
    init util class instance which contains all parsing methods

    loop thru array holder from row index 1
      get page var name from [i,0] where i is current index
      get Value for this Page Var from the current Page
      get individual parse method for this Page Var from [i,1] where i is current index

      if there's method name then
        call individual parse method with parameter of page var value

      store the returned ie parsed value to paramarray
    end loop

    if there's general method name then
      call general method pass paramarray for final parsing

    return final String Value
  }



===================

To attach a method with Page Variables
======================================
creditReferenceId=RepeatedCreditReference.hdCreditRefId!getFormattedId()

Combine two Page Variables for One Entity Fields
================================================
creditRefTypeId=RepeatedCreditReference.cbCreditRefsType+RepeatedCreditReference.txReferenceDesc

All inclusive
=============
copyId=RepeatedCreditReference.hdCreditRefCopyId!oneMethod()+secondHdFld!twoMethod()@allMthod()

Delim Definitions
===============
If there is more than one Page Variable involved then both of them can be combined
with '+' delimater

If there is any method need to be used to parse particular page value then that method
name can be attached with page Variable Name and '!' delimater

If there is any method need to be used to parse the all page Values for single enity then 
the method name can be attached with Page Variable(s) name and '@' delimater

Only one method name can be attached (not inluding general parser) for one Page variable
The delimaters(+,!,@) I used here are invalid characters for Netdynamics Page Variable Name


when reading
============

loadThisProperties(pathName, proopertyFileName)
{
  Check for List holder whether Property file has been loaded already;

  if  loaded then 
    return the Hashtable list which has been loaded already

  if not
    load the property file for the given name // which contains entity property as key value and Page variable with defined tag and method needed to be operated on the Page values

  loop thru property contents
    get key value 			//entity property name
    get the Value for this key 		//page variable + method need to be use to parse the page values seperated with defined tags
    send the value to parseParameterToArray() and get String array Holder
    put in to Hashtable List (key Value and String Array Holder contains parsed Value)
  end loop

  return hash table list
}


parseParameterToArray(String paramName)
{
  check any general Method Name attached to the line
  if then
    seperate the param Name Line and method Name
  
  assign the general method name to return array holder in index 0 column 1
	assign empty string value to index 0 column 2 I don't use this index
  tokenize the rest of the line with delim '+' to get multiple page variable if any
  loop thru tokens
    seperate the Page variable Name and individual parsing method name
    assign page variable name in to column 1 and individual parsing method name in to column 2
  end loop

  return array holder
 
}

So at the end loading updatePropList the List will be like below

Hashtable updatePropList
---------------------------------------------------
key			|	Value
----------------------------------------------------
propertyfilename	| Hashtable thisNameFileList
:			|
:			|
:			|
----------------------------------------------------


Hashtable thisNameFileList
----------------------------------------------------
key			|	Value
----------------------------------------------------
entityPropertyName	| [0..n Rows] [2 Cols] array
			| - [0,0] index holds general 
			|   method Name
			| - [1..n,0] holds Page 
			|   Variable Name
			| - [1..n,1] holds individual 
			|   Parse Method
:			|
:			|
:			|
-----------------------------------------------------

During Update
=============

  //This method should be called by single row page
  updateForMainRows( Hashtable propList,
			String[] keyValues,
  			String entityName, 
  			SessionResourceKit srkit,
			SessionState theSession ) throws Exception
  {
    get the primary id and copy id value from current page assume that primary var name assigned in 
    index 0 of the keyvalues and index 1 for copy id

    Construct the entity instance using enitituy builder
    parsePageValues To Entity for single row //pass row index value -1
  }


  updateForRepeatedRows( Hashtable propList,
 			String[] keyValues,
    			String entityName, 
    			SessionResourceKit srkit,
 			SessionState theSession ) throws Exception
  {
    get No Of Rows for for the page
    if the noofrows <0
    then
      call updateForMainRows	//bcose is similiar to update single row

    else
      loop thru until noofrows
        get the primary key value and copy id value for this row
        Construct the entity instance using enitituy builder
        parsePageValues To Entity for multiple row //pass row index value loop index value
      end loop
    endif
  }


  parsePageValuesToEntity(Hashtable fieldList, DealEntity entity,int rowindex)
  {
    loop thru fieldlist contents
      get entity properties which is key value
      get Page Variable Contents which is attached value for entity properties
      get Page Variable Value from getThisDisplayFieldValue()
      setEntity Field Value
    end loop

    return entity Instance

  }

  getThisDisplayFieldValue(String[][] pageVarContents, int rowindex
  {
    get General Method Name from Index 0 of array Holder
    init paramarray with size equal to page var contents.size() -1
           //new array size is less than 1 bcose first index is used to hold the method name
    init util class instance which contains all parsing methods

    loop thru array holder from row index 1
      get page var name from [i,0] where i is current index
      get Value for this Page Var from the current Page
      get individual parse method for this Page Var from [i,1] where i is current index

      if there's method name then
        call individual parse method with parameter of page var value

      store the returned ie parsed value to paramarray
    end loop

    if there's general method name then
      call general method pass paramarray for final parsing

    return final String Value
  }



