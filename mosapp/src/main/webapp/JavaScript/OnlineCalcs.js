// =====================================================================================================================
// Modifications : Some function has been modified for JATO naming format -- By BILLY 25July2002
// Note : all functions only used on Applicant Entry screen
// =====================================================================================================================

// ======= Function for the calculation of BorrowerTotalIncome (Calc 53) -- By BILLY 25July2002 (Begin) ======
//		-- Function to sum up the Employment Income Amounts (CURRENT) and all OtherIncome Amounts
//			then populate the result to "txTotalIncome"
//===========================================================================================================
function calcBorrowerTotalIncome()
{
	var theTotalIncome = 0;

	// Add up all Employment Income if Employment Status = 0 (CURRENT)
	var numOfEmployment = getNumOfRow("pgApplicantEntry_RepeatedEmployment[0]_txIncomeAmount");
  var name1stPart = get1stPartName("pgApplicantEntry_RepeatedEmployment[0]_txIncomeAmount");

	// Handle multiple rows
  for( x=0; x < numOfEmployment ; ++x )
	{
    inputRowNdx = x+"]_";

		//var currEmployStatusID = eval("document.forms[0].cbEmploymentStatus" + inputRowNdx +".value");
    var currEmployStatusID = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbEmploymentStatus" + "']" + ".value");
		//var includeGDS = eval("document.forms[0].cbEmployIncludeInGDS" + inputRowNdx +".value");
    var includeGDS = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbEmployIncludeInGDS" + "']" + ".value");
		//var includeTDS = eval("document.forms[0].cbEmployIncludeInTDS" + inputRowNdx +".value");
    var includeTDS = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbEmployIncludeInTDS" + "']" + ".value");

		//alert("The currEmployStatusID = " + currEmployStatusID + " RowNum = " + x);
		// Calc only if Status = 0 (CURRENT)
		if(currEmployStatusID == 0)
		{
			if( (includeGDS == "Y") || (includeTDS == "Y") )
			{
				//var currIncomePeriodID = eval("document.forms[0].cbIncomePeriod" + inputRowNdx + ".value");
        var currIncomePeriodID = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbIncomePeriod" + "']" + ".value");
				var theIndex = getDefaultNdx(currIncomePeriodID , "VALScbIncomePeriod");
				var currIncomeMultiplier = VALSIncomeMultiplier[theIndex];
				//alert("The currIncomeMultiplier = " + currIncomeMultiplier + " RowNum = " + x);
				//var currIncomeAmount = eval("document.forms[0].txIncomeAmount" + inputRowNdx + ".value");
        var currIncomeAmount = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txIncomeAmount" + "']" + ".value");
				//alert("The currIncomeAmount = " + currIncomeAmount + " RowNum = " + x);
				// Add to total
				theTotalIncome = theTotalIncome + (currIncomeAmount * currIncomeMultiplier);
				//alert("The theTotalIncome = " + theTotalIncome + " RowNum = " + x);
			}
		}
	}

	// Add up all Other Incomes if IncludeGDS or IncludeTDS = Y
	var numOfOtherIncome = getNumOfRow("pgApplicantEntry_RepeatedOtherIncome[0]_txOtherIncomeAmount");
  name1stPart = get1stPartName("pgApplicantEntry_RepeatedOtherIncome[0]_txOtherIncomeAmount");

	// Handle multiple rows
	for(x=0; x<numOfOtherIncome ; ++x)
	{
		inputRowNdx = x+"]_";
		//var includeGDS = eval("document.forms[0].cbOtherIncomeIncludeInGDS" + inputRowNdx +".value");
    var includeGDS = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbOtherIncomeIncludeInGDS" + "']" + ".value");
		//var includeTDS = eval("document.forms[0].cbOtherIncomeIncludeInTDS" + inputRowNdx +".value");
    var includeTDS = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbOtherIncomeIncludeInTDS" + "']" + ".value");

		if( (includeGDS == "Y") || (includeTDS == "Y") )
		{
			//var currIncomePeriodID = eval("document.forms[0].cbOtherIncomePeriod" + inputRowNdx + ".value");
      var currIncomePeriodID = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbOtherIncomePeriod" + "']" + ".value");
			var theIndex = getDefaultNdx(currIncomePeriodID , "VALScbIncomePeriod");
			var currIncomeMultiplier = VALSIncomeMultiplier[theIndex];
			//alert("The currIncomeMultiplier (Other) = " + currIncomeMultiplier + " RowNum = " + x);
			//var currIncomeAmount = eval("document.forms[0].txOtherIncomeAmount" + inputRowNdx + ".value");
      var currIncomeAmount = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txOtherIncomeAmount" + "']" + ".value");
			//alert("The currIncomeAmount (Other) = " + currIncomeAmount + " RowNum = " + x);
			// Add to total
			theTotalIncome = theTotalIncome + (currIncomeAmount * currIncomeMultiplier);
			//alert("The theTotalIncome (Other) = " + theTotalIncome + " RowNum = " + x);
		}
	}

	//Populate result to TotalIncome
	document.forms[0].pgApplicantEntry_txTotalIncome.value = round_decimals(theTotalIncome, 2);
}
// ======= Function for the calculation of BorrowerTotalIncome (Calc 53) -- By BILLY 25July2002 (End) ======

// ======= Function for the calculation of LiabilityMonthlyPaymentAndTotal (Calc 72, Calc 55) -- By BILLY 25July2002 (Begin) ======
//		1) Calc 72 : if(LiabilityPaymentQualifier == "V") LiabilityMonthlyPayment = LiabilityAmount * TDSInPercent
//		2) Calc 55 : if(LiabilityTypeId != 14{Closing Costs}) TotalLiabilityMonthlyPayment += LiabilityMonthlyPayment
function calcLiabilityMonthlyPaymentAndTotal()
{
	var TotalLiabilityMonthlyPayment = 0;

	// Get number of Liability rows
	var numOfLiability = getNumOfRow("pgApplicantEntry_RepeatedLiabilities[0]_txLiabilityAmount");
  var name1stPart = get1stPartName("pgApplicantEntry_RepeatedLiabilities[0]_txLiabilityAmount");

	// Handle multiple rows
	for(x=0; x<numOfLiability; ++x)
	{
		inputRowNdx = x+"]_";

		//var currLiabilityType = eval("document.forms[0].cbLiabilityType" + inputRowNdx +".value");
    var currLiabilityType = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbLiabilityType" + "']" + ".value");
		var theIndex = getDefaultNdx(currLiabilityType , "VALScbLiabilityType");
		var currLiabilityPaymentQualifier = VALSliabilityPaymentQualifier[theIndex];

		// Check for Calc 72
		if(currLiabilityPaymentQualifier == "V")
		{
			//var currLiabilityAmount = eval("document.forms[0].txLiabilityAmount" + inputRowNdx + ".value");
      var currLiabilityAmount = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txLiabilityAmount" + "']" + ".value");
			//var currLiabilityPercentageIncludeInTDS = eval("document.forms[0].txLiabilityPercentageIncludeInTDS" + inputRowNdx + ".value");
			var currLiabilityPercentageIncludeInTDS = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txLiabilityPercentageIncludeInTDS" + "']" + ".value");
      //alert("The currLiabilityAmount = " + currLiabilityAmount + " currLiabilityPercentageIncludeInTDS  = "
			//			+ currLiabilityPercentageIncludeInTDS  + " RowNum = " + x);
			var currLiabilityMonthlyPayment = currLiabilityAmount * currLiabilityPercentageIncludeInTDS / 100.00;
			//alert("The currLiabilityMonthlyPayment = " + currLiabilityMonthlyPayment + " RowNum = " + x);
			// Set to corresponding control
			//var theLiabilityMonthlyPaymentCtl = eval("document.forms[0].txLiabilityMonthlyPayment" + inputRowNdx);
      var theLiabilityMonthlyPaymentCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txLiabilityMonthlyPayment" + "']");
			theLiabilityMonthlyPaymentCtl.value = round_decimals(currLiabilityMonthlyPayment, 2);
		}

		// Check for calc 55
		if(currLiabilityType != "14.0" && currLiabilityType != "14" && currLiabilityType != 14 )
		{
			//var theLiabilityMonthlyPaymentCtl = eval("document.forms[0].txLiabilityMonthlyPayment" + inputRowNdx);
      var theLiabilityMonthlyPaymentCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txLiabilityMonthlyPayment" + "']");
			TotalLiabilityMonthlyPayment = TotalLiabilityMonthlyPayment + (theLiabilityMonthlyPaymentCtl.value * 1.00);
		}
	}

	//Populate result to TotalLiabilityMonthlyPayment field
	document.forms[0].pgApplicantEntry_txTotalMonthlyLiab.value = round_decimals(TotalLiabilityMonthlyPayment, 2);
}
// ======= Function for the calculation of LiabilityMonthlyPaymentAndTotal (Calc 72, Calc 55) -- By BILLY 25July2002 (Begin) ======

// ======= Function for the calculation of BorrowerTotalAsset (Calc 52) -- By BILLY 25July2002 (Begin) ======
//		-- Function to sum up the Employment Asset Amount if IncludeInNetWorth == true
function calcBorrowerTotalAsset()
{
	var theTotalAsset = 0;

	var numOfAsset = getNumOfRow("pgApplicantEntry_RepeatedAssets[0]_txAssetValue");
  	var name1stPart = get1stPartName("pgApplicantEntry_RepeatedAssets[0]_txAssetValue");

	// Handle multiple rows
	for(x=0; x<numOfAsset; ++x)
	{
		inputRowNdx = x+"]_";

		//var currAssetIncludeInNetworth = eval("document.forms[0].cbAssetIncludeInNetworth" + inputRowNdx +".value");
    var currAssetIncludeInNetworth = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbAssetIncludeInNetworth" + "']" + ".value");
    //var currAssetValue = eval("document.forms[0].txAssetValue" + inputRowNdx +".value");
    var currAssetValue = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txAssetValue" + "']" + ".value");

    // Add Asset Value to total if AssetIncludeInNetworth == Y
		if( (currAssetIncludeInNetworth == "Y") )
		{
			// Add to total
			theTotalAsset = theTotalAsset + (currAssetValue * 1.00);
			//alert("The theTotalAsset = " + theTotalAsset + " RowNum = " + x);
		}
	}

	//Populate result to TotalAsset
	document.forms[0].pgApplicantEntry_txTotalAsset.value = round_decimals(theTotalAsset, 2);
}
// ======= Function for the calculation of BorrowerTotalAsset (Calc 52) -- By BILLY 25July2002 (End) ======

// ======= Adjusted to the JATO framework by VLAD 30Jul2002.
function calcTotalPropertyExpenses()
{
	var theTotal = 0;
	
	var numOfExpense = getNumOfRow("pgPropertyEntry_RepeatedPropertyExpenses[0]_txExpenseAmount");
	var name1stPart = get1stPartName("pgPropertyEntry_RepeatedPropertyExpenses[0]_txExpenseAmount");
	
	////alert("name1stPart: " + name1stPart);

	// Handle multiple rows
	for(x=0; x<numOfExpense ; ++x)
	{
		inputRowNdx = x+"]_";
				
		////var currExpensePeriodID = eval("document.forms[0].cbExpensePeriod" + inputRowNdx + ".value");
		var currExpensePeriodID = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbExpensePeriod" + "']" + ".value");
		                
		////alert("currExpensePeriodID: " + currExpensePeriodID);
		
		var theIndex = getDefaultNdx(currExpensePeriodID , "VALScbExpensePeriod");
		var currExpensePeriodMultiplier = VALSExpenseMultiplier[theIndex];
		
		////alert("theIndex: " + theIndex);
				
		////alert("The currExpensePeriodMultiplier = " + currExpensePeriodMultiplier + " RowNum = " + x);
		////var currExpenseAmount = eval("document.forms[0].txExpenseAmount" + inputRowNdx + ".value");
		var currExpenseAmount = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txExpenseAmount" + "']" + ".value");
		
		////alert("The currExpenseAmount = " + currExpenseAmount + " RowNum = " + x);			
		// Add to total 
		theTotal = theTotal + (currExpenseAmount * currExpensePeriodMultiplier);
		//alert("The theTotal = " + theTotal + " RowNum = " + x);	
	}
	
	//Populate result to TotalExpense
	document.forms[0].pgPropertyEntry_txTotalPropertyExp.value = round_decimals(theTotal, 2);
}
// ======= Function for the calculation of TotalPropertyExpenses (Calc 89) -- By BILLY 27Feb2001 (End) ======

