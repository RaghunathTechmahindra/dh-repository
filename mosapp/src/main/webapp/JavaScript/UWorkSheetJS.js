// ==================== UW Worksheet Specific JavaScript Functions (Begin)========================================
//  Note : this file must be included in UW Worksheet page
// ================================================================================================

function hideSubProgressAdvance()
{

  //[Progress Advance Type] & [Progress Advance Inspection By]
 var varProgressAdvanceCtl = eval("document.forms[0]." + pgName + "_rbProgressAdvance");
 var varProgressAdvance = getSelectedRadioValue(varProgressAdvanceCtl);
 
 var eDivProgressAdvanceType = document.getElementById("divProgressAdvanceType");
 var eDivProgressAdvanceInspectionBy = document.getElementById("divProgressAdvanceInspectionBy");
 

 //hide if deal.progressAdvance <> Y
 if(varProgressAdvance != "Y" && varProgressAdvance != "y")
 {
  eDivProgressAdvanceType.style.display="none";
  eDivProgressAdvanceInspectionBy.style.display="none";
 }
 else
 {
  eDivProgressAdvanceType.style.display="";
  eDivProgressAdvanceInspectionBy.style.display="";
 }
 var hdForceProgressAdvanceValue = eval("document.forms[0]." + pgName + "_hdForceProgressAdvance.value");
 
  if(hdForceProgressAdvanceValue== 'Y') {
   eDivProgressAdvanceType.style.display="";
   }  
}


//CR03



function changeExpirationDate()
{   
    var varAutoCalculateCtl = eval("document.forms[0]." + pgName + "_rbAutoCalculateCommitmentDate");
    var varAutoCalculate = getSelectedRadioValue(varAutoCalculateCtl);

    var eExpirationDateMonth = document.getElementById("cbUWCommitmentExpirationDateMonth");
    var eExpirationDateDay = document.getElementById("txUWCommitmentExpirationDateDay");
    var eExpirationDateYear = document.getElementById("txUWCommitmentExpirationDateYear");

    if(varAutoCalculate == "Y") {
        eExpirationDateMonth.disabled = true;
        eExpirationDateDay.disabled = true;
        eExpirationDateYear.disabled = true;
    }
    else  {
        eExpirationDateMonth.disabled = false;
        eExpirationDateDay.disabled = false;
        eExpirationDateYear.disabled = false;
    }
}



function checkCommitDate(theYear, theMonth, theDay)
{  

    isFieldValidDate(theYear, theMonth, theDay);

    var commitExpiryDate =  document.getElementById("hdCommitmentExpiryDate").value;

    var eExpirationDateYear = 0;
    var eExpirationDateMonth = 0;
    var eExpirationDateDay = 0; 

    var mosProperty = document.getElementById("hdMosProperty").value;
    if (mosProperty) {
        var year = commitExpiryDate.substring(0,4);
        var month = commitExpiryDate.substring(5,7);
        var date = commitExpiryDate.substring(8,10);

        var autoDate = new Date(year, month-1, date);
        var autoCom = autoDate.getTime();
        
        if(document.getElementById(theYear))
            eExpirationDateYear = document.getElementById(theYear).value;
        if(document.getElementById(theMonth))
            eExpirationDateMonth = document.getElementById(theMonth).value;
        if(document.getElementById(theDay))
            eExpirationDateDay = document.getElementById(theDay).value;

        var manuDate = new Date(eExpirationDateYear, eExpirationDateMonth-1, eExpirationDateDay);
        var manuCom = manuDate.getTime();

        if(manuCom!=autoCom) {
             alert(COMMITMENT_EXPIRY_DATE);
        }
     }
    return true;
}


//5.0 MI -- start
/**
 * event handling method for btMIExpand
 */
var handleMIResponseExpand = function() {
    
	var jsessionid = document.forms[0].action;
    if (jsessionid.indexOf(";") > 0) {
        jsessionid = jsessionid.substr(jsessionid.indexOf(";"));
    } else {
        jsessionid = "";
    }
    
    var miResponsePopup = window.open("pgMIResponsePopup" + jsessionid, "miResponse",
                    "resizable,scrollbars,status=yes,titlebar=no,width=800,height=680,hotkeys");
    miResponsePopup.moveTo(0, 0);
    miResponsePopup.focus();
};

//5.0 MI -- end