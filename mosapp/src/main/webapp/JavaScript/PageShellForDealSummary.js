// ==================== Page Shell Common JavaScript Functions -- DealSummary (Begin)========================================
//  Note : this file should be included in every MOS page
//        Except some special pages like iWorkQueue which have different page Shell section(s)
// ================================================================================================
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';


function tool_click(n) {
	var itemtomove = (NTCP) ? document.pagebody : document.all.pagebody.style;
	var alertchange = (NTCP) ? document.alertbody:document.all.alertbody.style;
	var borrSub = (NTCP)?document.dealsumborr:document.all.dealsumborr.style;
	var asstSub = (NTCP)?document.dealsumasst:document.all.dealsumasst.style;
	var incSub = (NTCP)?document.dealsuminc:document.all.dealsuminc.style;
	var liabSub = (NTCP)?document.dealsumliab:document.all.dealsumliab.style;
	var propSub = (NTCP)?document.dealsumprop:document.all.dealsumprop.style;
	var gsdtdsSub = (NTCP)?document.gdstds:document.all.gdstds.style;
	var propexpSub = (NTCP)?document.dealsumpropexp:document.all.dealsumpropexp.style;
	var pageHide = (NTCP)?document.pagebody:document.all.pagebody.style;
	if(n==1){
		var imgchange = "tool_goto"; var hide1img="tool_prev"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.gotobar : document.all.gotobar.style;
		var hideother1 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==2){
		var imgchange = "tool_prev"; var hide1img="tool_goto"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.dialogbar : document.all.dialogbar.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==3){
		var imgchange = "tool_tools"; var hide1img="tool_goto"; var hide2img="tool_prev";
		var itemtochange = (NTCP) ? document.toolpop : document.all.toolpop.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
	}
	if(n==1 || n==2 || n==3){
		if (itemtochange.visibility==VISIBLE) {
			itemtochange.visibility=HIDDEN;
			//itemtomove.top = (NTCP) ? 180:190;
			//borrSub.top = (NTCP) ? 180:190;
			//asstSub.top = (NTCP) ? 180:190;
			//incSub.top = (NTCP) ? 180:190;
			//liabSub.top = (NTCP) ? 180:190;
			//propSub.top = (NTCP) ?180:190;
			//gsdtdsSub.top = (NTCP) ?180:190;
			//propexpSub.top = (NTCP) ?180:190;
			changeImg(imgchange,'off');
			//if(alertchange.visibility==VISIBLE){alertchange.top = (NTCP) ? 180:190;}
		}
		else {
			itemtochange.visibility=VISIBLE;
			changeImg(imgchange,'on');
			//if(n!=2)
      //{
      //itemtomove.top=240;
			//borrSub.top = 240;
			//asstSub.top = 240;
			//incSub.top = 240;
			//liabSub.top = 240;
			//propSub.top = 240;
			//propexpSub.top = 240;
			//gsdtdsSub.top = 240;
      //}
			//else{
      //itemtomove.top=290;
			//borrSub.top = 290;
			//asstSub.top = 290;
			//incSub.top = 290;
			//liabSub.top = 290;
			//propSub.top = 290;
			//propexpSub.top = 290;
			//gsdtdsSub.top = 290;
      //}
			//if(alertchange.visibility==VISIBLE){
			//	if(n!=2){alertchange.top =  240;}
			//	else{alertchange.top = 290;}
			//}
			if(hideother1.visibility==VISIBLE){hideother1.visibility=HIDDEN; changeImg(hide1img,'off');}
			if(hideother2.visibility==VISIBLE){ hideother2.visibility=HIDDEN;changeImg(hide2img,'off');}
		}
	}
	if(n==4){
		var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
		itemtoshow.visibility=VISIBLE;
	}
	if(n==5){
		var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
		var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
		itemtoshow.visibility=VISIBLE;
		if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
	}
}

if(document.images){
  //--> Modified by Billy to get the Image based on Language
  if(LANGUAGE == "fr")
	{
	tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on_fr.gif";
	tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto_fr.gif";
	tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on_fr.gif";
	tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev_fr.gif";
	tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on_fr.gif";
	tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools_fr.gif";
  }
  else
  {
  tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on.gif";
	tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto.gif";
	tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on.gif";
	tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev.gif";
	tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on.gif";
	tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools.gif";
  }
}

///////////////////////////////////////////////////////////////////
function changeImg(imgNam,onoff){
	if(document.images){
		document.images[imgNam].src = eval (imgNam+onoff+'.src');
	}
}

////////////////////////////////////////////////////////////////

function viewSub(subsect){
var borrSub = (NTCP)?document.dealsumborr:document.all.dealsumborr.style;
var asstSub = (NTCP)?document.dealsumasst:document.all.dealsumasst.style;
var incSub = (NTCP)?document.dealsuminc:document.all.dealsuminc.style;
var liabSub = (NTCP)?document.dealsumliab:document.all.dealsumliab.style;
var propSub = (NTCP)?document.dealsumprop:document.all.dealsumprop.style;
var gsdtdsSub = (NTCP)?document.gdstds:document.all.gdstds.style;
var propexpSub = (NTCP)?document.dealsumpropexp:document.all.dealsumpropexp.style;
var pageHide = (NTCP)?document.pagebody:document.all.pagebody.style;

if(subsect=="borr"){borrSub.visibility=VISIBLE;self.scroll(0,0);}
if(subsect=="asst"){asstSub.visibility=VISIBLE;self.scroll(0,0);}
if(subsect=="inc"){incSub.visibility=VISIBLE;self.scroll(0,0);}
if(subsect=="liab"){liabSub.visibility=VISIBLE;self.scroll(0,0);}
if(subsect=="prop"){propSub.visibility=VISIBLE;self.scroll(0,0);}
if(subsect=="gdstds") {gsdtdsSub.visibility=VISIBLE;self.scroll(0,0);}
if(subsect=="expen") {propexpSub.visibility=VISIBLE;self.scroll(0,0);}


if(subsect=="main"){
	borrSub.visibility=HIDDEN;
	asstSub.visibility=HIDDEN;
	incSub.visibility=HIDDEN;
	liabSub.visibility=HIDDEN;
	propSub.visibility=HIDDEN;
	gsdtdsSub.visibility=HIDDEN;
	propexpSub.visibility=HIDDEN;
	pageHide.visibility=VISIBLE;
}
else{
	pageHide.visibility=HIDDEN;
}
}

function OnTopClick(){
	self.scroll(0,0);
}

var pmWin;
var pmCont = '';

function openPMWin()
{

pmCont+='<head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  for(z=0;z<=numberRows-1;z++)
  {
	  if(pmMsgTypes[z]==0){var pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
	  if(pmMsgTypes[z]==1){var pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
	  writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
	  writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close.gif" width=84 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].pgDealSummary_sessionUserId.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');

  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
	pmWin.document.open();
	pmWin.document.write(pmCont);
	pmWin.document.close();
	pmWin.focus();
}


function generateAM()
{
var numberRows = amMsgs.length;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
	amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	var writeRows='';

	for(z=0;z<=numberRows-1;z++)
  {
		if(amMsgTypes[z]==0){var amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
		if(amMsgTypes[z]==1){var amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
		writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
		writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}

var isGotoMeunDown = false;

function handleGotoKetPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
//alert("The key = " + keycode);
  if (keycode == 13)
  {
    performGoto();
    return false;
  }

  return true;
}

function performGoto()
{
	setSubmitFlag(true);
	//document.forms[0].action="../MosSystem/" + document.forms[0].name + ".btProceed_onWebEvent(btProceed)";
  document.forms[0].action += "?pgDealSummary_btProceed=";
	//alert("document.forms[0].action= " + document.forms[0].action);
  // check if any outstanding request
  if(IsSubmitButton())
	  document.forms[0].submit();
}

function toggleMenuDownFlag()
{
  // For testing -- BILLY 01Apr2002
  //var theField =(NTCP) ? event.target : event.srcElement;

	if(isGotoMeunDown)
  {
		//theField.size = 0;
    isGotoMeunDown = false;
  }
	else
  {
		//theField.size = 20;
    isGotoMeunDown = true;
  }
}

// Added the Validation functions for Deal # validation
function isDigitGoto(inp)
{
	if(inp == '0' || inp == '1' || inp == '2'  || inp == '3' || inp == '4' ||
   	   inp == '5' || inp == '6' || inp == '7'  || inp == '8' || inp == '9')
	{
	return true;
	}

	return false;
}

// Function to check if the input value is an Integer
function isIntegerGoto(inValue)
{
	for(var i=0;i<inValue.length;i++)
	{
		if(  isDigitGoto(inValue.charAt(i)) == false )
			return false;
	}
	return true;
}

// Function to check if the attached field value is an Integer with requestLength
//		Note : if requestLength = 0 means any length
function isFieldValideDealNum()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

  var requestLength = 0;

	if( !isIntegerGoto(theFieldValue) )
	{
		alert(DEAL_NUM_NUMERIC_ONLY);
		var tmp = theField.value;
		theField.focus();
		theField.select();
		return false;
	}
	else
	{
		// Check Length
		if( requestLength > 0 )
		{
			if( theFieldValue.length != requestLength )
			{
				alert(printf(INPUT_NUM_DIGITS, requestLength));
				theField.focus();
				theField.select();
				return false;
			}
		}
	}
	return true;
}

// Also included the default button stuffes here
var isSubmitFlag = true;
var isSubmited = false;

function setSubmitFlag(flag)
{
	isSubmitFlag = flag;
}

function setSubmitedFlag(flag)
{
	isSubmited = flag;
}

function IsSubmitButton()
{
  if(!isSubmited && isSubmitFlag)
  {
    // Set the flag to prevent submit again
    isSubmited = true;
	  return(true);
  }
  else
  {
    if(isSubmited)
      alert(NO_SECOND_SELECTION);
    return(false);
  }
}

function IsSubmited()
{
  if(isSubmited)
  {
    alert(NO_SECOND_SELECTION);
    return(false);
  }
  else
  {
    return(true);
  }
}

//Test by BILLY 15July2002
function performActMsgBtSubmit(theParameters)
{
	setSubmitFlag(true);
	document.forms[0].action += "?pgDealSummary_btActMsg=" + theParameters;
	//alert("document.forms[0].action= " + document.forms[0].action);
  // check if any outstanding request
  if(IsSubmitButton())
	  document.forms[0].submit();
}

// ==================== Page Shell Common JavaScript Functions (End)========================================
