function sendAJAXMIRequest(param) {

    document.forms[0].action += "?" + PAGE_NAME + "_btActMsg=" + param;

    var miInsurer = document.getElementById("cbMIInsurer");
    var selectedInsurer = miInsurer.options[miInsurer.selectedIndex].value;

    if(selectedInsurer == 3) {
        // CG request
        sendCGRequest();
    } else {
        if(param.substring(0,1) === "D") {
            // generic mi cancel request
            sendMIRequest(true);
        } else {
            // generic mi request
            sendMIRequest(false);
        }
    }
}

function sendMIRequest(cancel) {

    var miService = new filogix.express.service.MIService(cancel);

    var insurer = document.getElementById("cbMIInsurer");
    var miOption = insurer.options[insurer.selectedIndex];

    var request = new Object();
    request.rcRequestContent = new Object();
    request.rcRequestContent.miinsurerid = miOption.value;
    request.rcRequestContent.cancel = cancel;
    miService.requestContent = request;
    miService.miProviderName = miOption.text;

    miService.execute();

}

// MI request for Canada Guaranty
function sendCGRequest() {

    var aigugOrderRequest = new filogix.express.service.AIGUGOrderRequest();

    var insurer = document.getElementById("cbMIInsurer");
    var miOption = insurer.options[insurer.selectedIndex];

    var request = new Object();
    request.rcRequestContent = new Object();
    request.rcRequestContent.miinsurerid = miOption.value;
    aigugOrderRequest.requestContent = request;
    aigugOrderRequest.miProviderName = miOption.text;
    aigugOrderRequest.execute();

}
