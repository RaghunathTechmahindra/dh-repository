// ==================== Generic Common JavaScript Functions (Begin)========================================
//  Note : this file must be included DocTracking, DocTrackingDetails, and CommitmentOffer pages.
// ================================================================================================

// A user should be able to edit Standard/Custom conditions via DocumentTracking screen.

//--Release2.1--_DTS_CR--start.
// A user should be able to edit Standard/Custom conditions via DocumentTracking screen.
// This new set of functions should be combined with the pgCommitmentOffer's one and separated into
/// the new JavaScript import.


// Global variable for Field Validation
var PAGE_NAME;

//==== Local JavaScript functions ================
function openAddConditionDialog(which)
{
	if (document.all)
	{		
		if(which == 1)
		{
		 document.all.addConditionDialogStd.style.visibility='visible';
		 document.all.pgDocTracking_lbStdCondition.size=10;
		}
		else
		{
		 document.all.addConditionDialogCust.style.visibility='visible';
		 document.all.pgDocTracking_lbCustCondition.size=10;
		 self.scroll(0,10000);
		}
		 
	}
    if (document.layers)
	{
		if(which == 1)
		{
		 document.addConditionDialogStd.visibility='show';		 
		 document.pgDocTracking_lbStdCondition.size=10;
		}
		else
		{
		 document.addConditionDialogCust.visibility='show';
		 document.pgDocTracking_lbCustCondition.size=10;
		 self.scroll(0,10000);
		}
	}

}

function closeAddConditionDialog(which)
{
	if (document.all)
	{
		if(which == 1)
		{
		 document.all.addConditionDialogStd.style.visibility='hidden';		 
		 document.all.pgDocTracking_lbStdCondition.size=1;
		}
		else
		{
		 document.all.addConditionDialogCust.style.visibility='hidden';		 		
		 document.all.pgDocTracking_lbCustCondition.size=1;
		}
	}
	if (document.layers)
	{
		if(which == 1)
		{
		 document.addConditionDialogStd.style.visibility='hide';		 
		 document.pgDocTracking_lbStdCondition.size=1;
		}
		else
		{
		 document.addConditionDialogCust.style.visibility='hide';
		 document.pgDocTracking_lbCustCondition.size=1;
		}

	}
}

function moreConditionDialog(which)
{
	if (document.all)
	{
		if(which == 1)
		{
			document.all.pgDocTracking_lbStdCondition.size *=2;
			if(document.all.pgDocTracking_lbStdCondition.size > document.all.pgDocTracking_lbStdCondition.options.length)
				document.all.pgDocTracking_lbStdCondition.size = document.all.pgDocTracking_lbStdCondition.options.length;
		}
		else
		{
		 	document.all.pgDocTracking_lbCustCondition.size *=2;
		 	if(document.all.pgDocTracking_lbCustCondition.size > document.all.pgDocTracking_lbCustCondition.options.length)
				document.all.pgDocTracking_lbCustCondition.size = document.all.pgDocTracking_lbCustCondition.options.length;
			self.scroll(0,10000);
		}
		
	}
    if (document.layers)
	{
		if(which == 1)
		{
		 document.pgDocTracking_lbStdCondition.size *=2;
		 if(document.pgDocTracking_lbStdCondition.size > document.pgDocTracking_lbStdCondition.options.length)
				document.pgDocTracking_lbStdCondition.size = document.pgDocTracking_lbStdCondition.options.length;
		}
		else
		{
		 document.pgDocTracking_lbCustCondition.size *=2;
		 if(document.pgDocTracking_lbCustCondition.size > document.pgDocTracking_lbCustCondition.options.length)
				document.pgDocTracking_lbCustCondition.size = document.pgDocTracking_lbCustCondition.options.length;
		 self.scroll(0,10000);
		}
		
	}
}

function moreCustCondition(isMore)
{
	var kickedCtl =(NTCP) ? event.target : event.srcElement;
	
	// determine row (if repeated)
	var rowNdx = getRowNdx(kickedCtl);
	////alert("RowNdx: " + rowNdx);	

	inputRowNdx = "";
	if (rowNdx != -1)
		inputRowNdx = "[" + rowNdx + "]_";
		
	//var theTextAreaCtl = eval("document.forms[0].tbCusCondition" + inputRowNdx);
	var theTextAreaCtl = eval("document.forms[0].elements['pgDocTracking_Repeated5" + inputRowNdx + "tbCusCondition" + "']");
	////var theTextAreaCtl = eval("document.forms[0].elements[' + PAGE_NAME + "_" + "Repeated5" + inputRowNdx + "tbCusCondition" + "']");
		
	if(isMore == true)
	{
		theTextAreaCtl.rows += 3;
	}
	else
	{
		if(theTextAreaCtl.rows > 3)
			theTextAreaCtl.rows -= 3;
	}
}

function getRowNdx(changedSelect)
{
	var numElements = eval("document.forms[0]." + changedSelect.name + ".length");
	
	//Note : "undefined" not support fro JavaScript 1.2 or later versions 
	// if (numElements === undefined) 
	if (numElements == null)
		return 0;

	if (numElements == 1)
		return 0;

	for (i = 0; i < numElements; ++i)
	{
		var rowSelectInput = eval("document.forms[0]." + changedSelect.name + "[" + i + "]");

		if (rowSelectInput == changedSelect)
			return i;

	}
	
	return -1;
}

// Function to check if the input value includes a special characters
// that causes troubles for the BXP document processing.
// Currently Product requests to filter four special characters.
function isSpecialCharacter(inValue)
{
	for(var i=0;i<inValue.length;i++)
	{
		if(  inValue.charAt(i) == '>' || inValue.charAt(i) == '<' ||
		     inValue.charAt(i) == '\"' || inValue.charAt(i) == '\&')
			return false;
	}
	return true;
}

// Function to check if the attached field value is an Alpha string with any length
function isFieldHasSpecialChar()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	////alert("theField: " + theField);	

	var theFieldValue = theField.value;
	////alert("theFieldValue: " + theFieldValue);	
	
	var theFieldName = theField.name;
	////alert("theFieldName: " + theFieldName);

	var currFieldToValidate = "";
	
	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( !isSpecialCharacter(theFieldValue) )
	{		
		alert(DO_NOT_USE_SPECIAL_CHARACTERS);
		theField.focus();
		theField.select();
		return false;
	}
	currFieldToValidate = "";
	return true;
 }
 

