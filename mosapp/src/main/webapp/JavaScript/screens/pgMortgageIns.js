//#### Anonymous function start ####
(function(){

	//====== onLoad method start ====== 
	window.attachEvent("onload", function(event) {

		//change display/hide main section 
		controleMainSectionOnAlert();

		//no enter key
		document.onkeypress = noEneterKey;

		// Set MI Premium Display
		SetMIPremiumDisplay();
		// Set MIPolicyNo
		SetMIPolicyNoDisplay();

		document.getElementById("txMIPreQCertNum").disabled = true;
		document.getElementById("cbMIStatus").disabled = true

		// 3.1 MI
		hideHtmlInputs();
		hideRequestStandardService();

		initMIIndicatorOnDenied();


		//5.0 MI -- start
		//filter
		var miTypeFilter = MITypeFilterFactory(
				document.getElementById("cbMIInsurer"), 
				document.getElementById("cbMIType"));
		miTypeFilter();

		//custom message on processMI button
		var btProcessMI = document.getElementById("btProcessMI");
		btProcessMI.attachEvent("onclick", function(){
			if(btProcessMI.clicked)return true;
			
			btProcessMI.clicked = true;
			var pendingMIProcessor = new filogix.express.service.PendingMIProcessor(btProcessMI);
			pendingMIProcessor.execute();
		});

		//disable/enable MIinsurer
// 	QC 353	initMIInsurer();

		//5.0 MI -- end


	});	
	//====== onLoad method end ======

	//====== event handler on each element start ======
	document.getElementById("cbMIInsurer").attachEvent("onchange", function(event) {
		SetMIPolicyNoDisplay();
		hideRequestStandardService();
	});

	document.getElementById("cbMIType").attachEvent("onchange", function(event) {
		SetMIPremiumDisplay();
		hideRequestStandardService();
	});

	document.getElementById("txMIPremium").attachEvent("onblur", function(event) {
		if (isFieldInDecRange(0, 99999999999.99) && isFieldDecimal(2)) {
			populateValueTo([ 'hdMIPremium' ]);
		}
	});

	document.getElementById("txMIComments").attachEvent("onblur", function(event) {
		isFieldMaxLength(400);
	});

	document.getElementById("txMIPolicyNo").attachEvent("onblur", function(event) {
		SetMIPolicyNoDisplay();
	});

	document.getElementById("btSubmit").attachEvent("onclick", function(event) {
		if (blankDropDownCheck("cbMIInsurer",MI_PROVIDER_EMPTY) == false) return false;
		//QC-Ticket-268- Start
		if (checkMIInsurer("hdSpecialFeatureId",
							"cbMIIndicator",
							"cbMIInsurer",
							SPECIAL_FEATURE_PRE_APPROVAL,
							MI_APPLICATION_LOAN_ASSESMENT,
							MI_PRE_QUALIFICATION) == false) return false; 
		//QC-Ticket-268- End
		
		if (blankDropDownCheck("cbMIType",MI_TYPE_EMPTY) == false) return false;
		setSubmitFlag(true);
	});

	//====== event handler on each element end ======


	//5.0 MI - control MIInsurer
/*QC 353	
	function initMIInsurer(){

		if(document.getElementById("hdMIStatusUpdateFlag").value == "N") {
			//5.0 MI: when previous MIIndicator is 4 or 5, do not lock MI status
			var previousMIIndicator = document.getElementById("hdPreviousMIIndicatorId").value;
			if(previousMIIndicator != 4 && previousMIIndicator != 5){
				document.getElementById("cbMIInsurer").disabled = true;
			}
		}
	}
*/
	//lock miindicator, mitype and miinsurer when dealstatus is collapsed or denied - Ticket 267
	function initMIIndicatorOnDenied(){

		if (document.getElementsByName("stViewOnlyTag").length > 0) {
			var DEAL_STATUS_COLLAPSED = "23";
			var DEAL_STATUS_DENIED = "24";

			var miIndicator = document.getElementById("cbMIIndicator");
			var miType = document.getElementById("cbMIType");
			var miInsurer = document.getElementById("cbMIInsurer");
			var dealStatusId = document.getElementById("hdDealStatusId").value;

			if ((DEAL_STATUS_COLLAPSED == dealStatusId 
					|| DEAL_STATUS_DENIED == dealStatusId)) {

				miIndicator.selectedIndex = 0;
				miIndicator.disabled = true;

				miType.disabled = true;
				
				miInsurer.disabled = true;
			}
		}
	}

	//return false when user clicks enter key
	function noEneterKey(e) {
		var key = e ? e.which : window.event.keyCode;
		return key != 13;
	}

	//moved form jsp file.
	//when isAlert = 'Y', AML is displayed. executing tool_click(4), hide main section.
	//isFatal is probably not used, I'll keep it for now anyways.
	function controleMainSectionOnAlert() {

		if (document.forms[0].isAlert.value == "Y"
			|| document.forms[0].isFatal.value == "Y") {
			tool_click(4);
		} else {
			tool_click(5);
		}
	}

	function SetMIPremiumDisplay() {

		var theMIStatusId = document.getElementById("hdMIStatusId").value;
		var theLienPositionId = document.getElementById("hdLienPosition").value;
		var theMITypeId = document.getElementById("cbMIType").value;

		var theMIPremiumCtl = document.getElementById("txMIPremium");

		if (theMIStatusId != 16 && (theLienPositionId != 0 || theMITypeId == 3)) {
			theMIPremiumCtl.removeAttribute("readOnly");
			theMIPremiumCtl.className="";
		} 
	}


	//	Method to set MIPolicyNo field based on the selected MI-Insurer
	//	-- By Billy 16July2003
	function SetMIPolicyNoDisplay() {
		var theMIInsurer = document.getElementById("cbMIInsurer").value;
		var theMIPolicyNoCtl = document.getElementById("txMIPolicyNo");

		if (theMIInsurer == 1) // --> CMHC
		{
			theMIPolicyNoCtl.value = document.getElementById("hdMIPolicyNoCMHC").value;
		} else if (theMIInsurer == 2) // --> GE
		{
			theMIPolicyNoCtl.value = document.getElementById("hdMIPolicyNoGE").value;
		} else if (theMIInsurer == 3) // --> AIGUG
		{
			theMIPolicyNoCtl.value = document.getElementById("hdMIPolicyNoAIGUG").value;
		} else if (theMIInsurer == 4) // --> PMI
		{
			theMIPolicyNoCtl.value = document.getElementById("hdMIPolicyNoPMI").value;
		} else // --> Clear it if not selected
		{
			theMIPolicyNoCtl.value = "";
		}

		theMIPolicyNoCtl.disabled = true;

	}


	function hideHtmlInputs() {
		// [Progress Advance Type] & [Progress Advance Inspection By]
		var varProgressAdvance = document.getElementById("hdProgressAdvance").value;
		var varProgressAdvanceHidden = document.getElementById("hdProgressAdvanceHidden").value;
		var eDivProgressAdvanceType = document.getElementById("divProgressAdvanceType");
		var eDivProgressAdvanceInspectionBy = document.getElementById("divProgressAdvanceInspectionBy");

		// hide if deal.progressAdvance <> Y
		if (varProgressAdvance != "Y" && varProgressAdvance != "y") {
			eDivProgressAdvanceType.style.display = "none";
			eDivProgressAdvanceInspectionBy.style.display = "none";
		} else {
			eDivProgressAdvanceType.style.display = "";
			eDivProgressAdvanceInspectionBy.style.display = "";
		}

		// Added condition to override the screen value if Mossys.Properties value
		// is "Y" for Progress Advance Type bug# 1679 - start
		if (varProgressAdvanceHidden == "Y") {
			eDivProgressAdvanceType.style.display = "";
		}
		// code fix for bug # 1679 - end
	}

	function hideRequestStandardService() {
		var MIInsurerId = document.getElementById("cbMIInsurer").value;
		var theMITypeId = document.getElementById("cbMIType").value;

		// ----------------------------------------------------------------------
		// [Request Standard Service]
		var eDivReqStdSvc = document.getElementById("divReqStdSvc");

		// hide [Request Standard Service] if NOT GE,
		// or if GE and (basic or full service)
		// LEN215466: reset rbRequestStandardService by unchecked all
		if (MIInsurerId != 2
				|| ((MIInsurerId == 2) && (theMITypeId == 1 || theMITypeId == 2))) {
			eDivReqStdSvc.style.display = "none";
			var rbRequestStandardServices = filogix.util.getElementsByClass("rbRequestStandardService", "input"); 
			for (i = 0; i < rbRequestStandardServices.length; i++)
				if (rbRequestStandardServices[i].checked)
					rbRequestStandardServices[i].checked = false;
		} else
			eDivReqStdSvc.style.display = "";
	}

})();
//#### Anonymous function end ####



