/*******************************************************************************
 * Anonymous function start
 ******************************************************************************/
(function() {

	// ====== onLoad method start ======
	window.attachEvent("onload", function(event) {

		// change display/hide main section
		controleMainSectionOnAlert();

		if (pmGenerate == "Y") {
			openPMWin();
		}		
	});

	// ====== onLoad method end ======

	// moved form jsp file.
	// when isAlert = 'Y', AML is displayed. executing tool_click(4), hide main
	// section. isFatal is probably not used, I'll keep it for now anyways.
	function controleMainSectionOnAlert() {

		if (document.forms[0].isAlert.value == "Y"
				|| document.forms[0].isFatal.value == "Y") {
			tool_click(4);
		} else {
			tool_click(5);
		}
	}

})();

/*******************************************************************************
 * Anonymous function end
 ******************************************************************************/

// ========= Local JavaScript -- modified by Billy for JATO field format --
// 26July2002 ============
function cbApprovalTypeChanged() {
	fm = document.pgDealResolution;
	if (fm.pgDealResolution_rbApproveDeal != null) {
		fm.pgDealResolution_rbApproveDeal.checked = true;
	}
	approveDealClicked();
	dismissHoldEntries();
	dismissDenyEntries();
}

function cbDenialOptionsChanged() {
	fm = document.pgDealResolution;
	if (fm.pgDealResolution_rbDeny != null) {
		fm.pgDealResolution_rbDeny.checked = true;
	}
	denyClicked();
	dismissHoldEntries();
	dismissApprovalTypeEntries();
}

function holdUntilMonthChanged() {
	holdUntilClicked();
}

function holdForDaysFocused() {
	fm = document.pgDealResolution;
	fm.pgDealResolution_tbHoldForDays.select();
}

function holdForDaysChanged() {
	holdForClicked();
}

function holdUntilYearFocused() {
	fm = document.pgDealResolution;
	fm.pgDealResolution_tbHoldUntilYear.select();
}

function holdUntilYearChanged() {
	holdUntilClicked();
}

function holdUntilDayFocused() {
	fm = document.pgDealResolution;
	fm.pgDealResolution_tbHoldUntilDay.select();
}

function holdUntilDayChanged() {
	holdUntilClicked();
}

function holdUntilClicked() {
	fm = document.pgDealResolution;
	if (fm.pgDealResolution_rbHoldUntil != null) {
		fm.pgDealResolution_rbHoldUntil.checked = true;
	}
	if (fm.pgDealResolution_tbHoldForDays != null) {
		fm.pgDealResolution_tbHoldForDays.value = "";
	}
	if (fm.pgDealResolution_rbApproveDeal != null) {
		fm.pgDealResolution_rbApproveDeal.checked = false;
	}
	if (fm.pgDealResolution_rbHoldPendingInfo != null) {
		fm.pgDealResolution_rbHoldPendingInfo.checked = true;
	}
	if (fm.pgDealResolution_rbDeny != null) {
		fm.pgDealResolution_rbDeny.checked = false;
	}
	if (fm.pgDealResolution_rbCollapseDeal != null) {
		fm.pgDealResolution_rbCollapseDeal.checked = false;
	}
	if (fm.pgDealResolution_rbHoldFor != null) {
		fm.pgDealResolution_rbHoldFor.checked = false;
	}
	dismissHoldForEntries();
	dismissApprovalTypeEntries();
	dismissDenyEntries();

	displayDefaultBrokerNoteForHold(true); // 5.0
}

function holdForClicked() {
	fm = document.pgDealResolution;

	if (fm.pgDealResolution_rbHoldFor != null) {
		fm.pgDealResolution_rbHoldFor.checked = true;
	}
	if (fm.rbApproveDeal != null) {
		fm.pgDealResolution_rbApproveDeal.checked = false;
	}
	if (fm.pgDealResolution_rbHoldPendingInfo != null) {
		fm.pgDealResolution_rbHoldPendingInfo.checked = true;
	}
	if (fm.pgDealResolution_rbDeny != null) {
		fm.pgDealResolution_rbDeny.checked = false;
	}
	if (fm.pgDealResolution_rbCollapseDeal != null) {
		fm.pgDealResolution_rbCollapseDeal.checked = false;
	}
	if (fm.pgDealResolution_rbHoldUntil != null) {
		fm.pgDealResolution_rbHoldUntil.checked = false;
	}
	dismissHoldUntilEntries();
	dismissApprovalTypeEntries();
	dismissDenyEntries();

	displayDefaultBrokerNoteForHold(true); // 5.0
}

function holdForClickedInit() {
	holdForClicked();
	// Set default to hold for 3 hrs
	if (fm.pgDealResolution_rbHoldFor != null) {
		// by default it is true
		fm.pgDealResolution_rbHoldFor.checked = true;
		// Set default to hold for 3 hrs
		document.pgDealResolution.pgDealResolution_tbHoldForDays.value = '';
		document.pgDealResolution.pgDealResolution_tbHoldForHrs.value = '3';
		document.pgDealResolution.pgDealResolution_tbHoldForMins.value = '0';
	}
}

function approveDealClicked() {
	fm = document.pgDealResolution;
	if (fm.pgDealResolution_rbHoldPendingInfo != null) {
		fm.pgDealResolution_rbHoldPendingInfo.checked = false;
	}
	if (fm.pgDealResolution_rbDeny != null) {
		fm.pgDealResolution_rbDeny.checked = false;
	}
	if (fm.pgDealResolution_rbCollapseDeal != null) {
		fm.pgDealResolution_rbCollapseDeal.checked = false;
	}
	if (fm.pgDealResolution_rbHoldUntil != null) {
		fm.pgDealResolution_rbHoldUntil.checked = false;
	}
	if (fm.pgDealResolution_rbHoldFor != null) {
		fm.pgDealResolution_rbHoldFor.checked = false;
	}
	dismissHoldEntries();
	dismissDenyEntries();
}

function holdPendingInfoClicked() {
	fm = document.pgDealResolution;

	if (fm.pgDealResolution_rbApproveDeal != null) {
		fm.pgDealResolution_rbApproveDeal.checked = false;
	}
	if (fm.pgDealResolution_rbDeny != null) {
		fm.pgDealResolution_rbDeny.checked = false;
	}
	if (fm.pgDealResolution_rbCollapseDeal != null) {
		fm.pgDealResolution_rbCollapseDeal.checked = false;
	}

	// =======================================
	// if holdUntil is already checked
	// just leave. this covers the case
	// when user clicks on already checked
	// radio button.
	// =======================================
	if (fm.pgDealResolution_rbHoldUntil != null
			&& fm.pgDealResolution_rbHoldUntil.checked == true) {
		dismissApprovalTypeEntries();
		dismissDenyEntries();
		dismissHoldForEntries();
		return;
	}
	if (fm.pgDealResolution_rbHoldUntil != null) {
		fm.pgDealResolution_rbHoldUntil.checked = false;
	}

	// Don't do anything if HoldFor already clicked
	if (fm.pgDealResolution_rbHoldFor != null
			&& fm.pgDealResolution_rbHoldFor.checked == false) {
		// by default it is true
		fm.pgDealResolution_rbHoldFor.checked = true;
		// Set default to hold for 3 hrs
		document.pgDealResolution.pgDealResolution_tbHoldForDays.value = '';
		document.pgDealResolution.pgDealResolution_tbHoldForHrs.value = '3';
		document.pgDealResolution.pgDealResolution_tbHoldForMins.value = '0';
	}
	dismissApprovalTypeEntries();
	dismissDenyEntries();
	dismissHoldUntilEntries();

	displayDefaultBrokerNoteForHold(true); // 5.0
}

function denyClicked() {
	fm = document.pgDealResolution;
	if (fm.pgDealResolution_rbApproveDeal != null) {
		fm.pgDealResolution_rbApproveDeal.checked = false;
	}
	if (fm.pgDealResolution_rbHoldPendingInfo != null) {
		fm.pgDealResolution_rbHoldPendingInfo.checked = false;
	}
	if (fm.pgDealResolution_rbCollapseDeal != null) {
		fm.pgDealResolution_rbCollapseDeal.checked = false;
	}
	if (fm.pgDealResolution_rbHoldUntil != null) {
		fm.pgDealResolution_rbHoldUntil.checked = false;
	}
	if (fm.pgDealResolution_rbHoldFor != null) {
		fm.pgDealResolution_rbHoldFor.checked = false;
	}
	dismissHoldEntries();
	dismissApprovalTypeEntries();
}

function collapseDealClicked() {
	fm = document.pgDealResolution;
	if (fm.pgDealResolution_rbApproveDeal != null) {
		fm.pgDealResolution_rbApproveDeal.checked = false;
	}
	if (fm.pgDealResolution_rbHoldPendingInfo != null) {
		fm.pgDealResolution_rbHoldPendingInfo.checked = false;
	}
	if (fm.pgDealResolution_rbDeny != null) {
		fm.pgDealResolution_rbDeny.checked = false;
	}
	if (fm.pgDealResolution_rbHoldUntil != null) {
		fm.pgDealResolution_rbHoldUntil.checked = false;
	}
	if (fm.pgDealResolution_rbHoldFor != null) {
		fm.pgDealResolution_rbHoldFor.checked = false;
	}
	dismissHoldEntries();
	dismissApprovalTypeEntries();
	dismissDenyEntries();
}

function dismissApprovalTypeEntries() {
	fm = document.pgDealResolution;

	if (fm.pgDealResolution_cbHoldUntilMonth != null) {
		if (fm.pgDealResolution_cbApprovalType != null) {
			fm.pgDealResolution_cbApprovalType.selectedIndex = -1;
		}
	}
}

function dismissDenyEntries() {
	fm = document.pgDealResolution;

	if (fm.pgDealResolution_cbHoldUntilMonth != null) {
		fm.pgDealResolution_cbDenialOptions.selectedIndex = -1;
	}
}

function dismissHoldUntilEntries() {
	fm = document.pgDealResolution;

	if (fm.pgDealResolution_tbHoldUntilDay != null) {
		fm.pgDealResolution_tbHoldUntilDay.value = "";
	}
	if (fm.pgDealResolution_tbHoldUntilYear != null) {
		fm.pgDealResolution_tbHoldUntilYear.value = "";
	}
	if (fm.pgDealResolution_cbHoldUntilMonth != null) {
		fm.pgDealResolution_cbHoldUntilMonth.selectedIndex = -1;
	}
}

function dismissHoldForEntries() {
	fm = document.pgDealResolution;

	if (fm.pgDealResolution_tbHoldForDays != null) {
		fm.pgDealResolution_tbHoldForDays.value = "";
	}
	if (fm.pgDealResolution_tbHoldForHrs != null) {
		fm.pgDealResolution_tbHoldForHrs.value = "";
	}
	if (fm.pgDealResolution_tbHoldForMins != null) {
		fm.pgDealResolution_tbHoldForMins.value = "";
	}
	if (fm.pgDealResolution_cbHoldReason != null) {
		fm.pgDealResolution_cbHoldReason.selectedIndex = -1;
	}
}

function dismissHoldEntries() {
	dismissHoldForEntries();
	dismissHoldUntilEntries();

	displayDefaultBrokerNoteForHold(false); // 5.0
}

// 5.0, FXP27305
function displayDefaultBrokerNoteForHold(display) {

	var note = document.getElementById("tbBrokerNoteText");
	if (!note.value === display) {
		// (note exist & display=false) or (note doesn't exit & display=true)
		note.value = (display) ? filogix.msgBox.getClientMsg(
				"DEAL_RESOLUTION_DEFAULT_BROKER_NOTE_ON_HOLD", null, false) : "";
	}
}
