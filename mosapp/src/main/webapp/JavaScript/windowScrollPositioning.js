
//******************************************************************************
// functions for scrool positioning 
// FXP23869, MCM/4.1, Jan 20 2009, saving/reapplying screen scroll position
//******************************************************************************

/**
 * saveScrollPosition
 *  this method expects to be called when you want to save the scroll position.
 *  the position will be saved into Cookie
 */
function saveScrollPosition() {
	getTxWindowScrollPositionObj().value = document.body.scrollLeft + "," + document.body.scrollTop;
}

/**
 * applyScrollPosition
 *
 *   This method expects to be called when the screen is reloaded. 
 *   This method tries to retrieve position info from txWindowScrollPosition text box, and
 *   if it can find the data, it applies the position to the reloaded screen.
 *   The txWindowScrollPosition's data will be cleared every time.
 */
function applyScrollPosition() {

	var scrollLeft = getScrollLeft();
	var scrollTop = getScrollTop();
    
    //if no data was found, do nothing
	if (scrollLeft == null || scrollLeft < 0) return;
	if (scrollTop == null || scrollTop < 0)	return;
    
    //alert("----debug----n" + "scrollTop=" + scrollTop + "n" + "scrollLeft=" + scrollLeft);
    
    //scroll to the position
	window.scrollTo(scrollLeft, scrollTop);
    
    //clean up position
	getTxWindowScrollPositionObj().value = "";
}

/**
 * getTxWindowScrollPositionObj
 *  get txWindowScrollPosition text box object.
 */
function getTxWindowScrollPositionObj() {
	return document.getElementById("txWindowScrollPosition");
}

/**
 * getWindowScrollLeftAndTop
 *  this is a utility method that is expected to be used only inside this file
 *  get scrollLeft or scrollTop from  txWindowScrollPosition
 *  @param element - 0: returns scrollLeft, 1: returns scrollTop
 */
function getWindowScrollLeftAndTop(element) {
	var txWinScrPos = getTxWindowScrollPositionObj().value;
	if (txWinScrPos == null || txWinScrPos.indexOf(",", 0) < 1) {
		return 0;
	}
	return txWinScrPos.split(",")[element];
}

/**
 * getScrollLeft
 * return saved scrollLeft position
 */
function getScrollLeft() {
	return getWindowScrollLeftAndTop(0);
}

/**
 * getScrollTop
 * return saved scrollTop position
 */
function getScrollTop() {
	return getWindowScrollLeftAndTop(1);
}

