// ==================== Generic Common JavaScript Functions (Begin)========================================
//  Note : this file must be included in every MOS page
// ================================================================================================

//New Function to get first part of Ccontrol name in JATO repeated format
// Input -- control full name e.g. "pgPageName_RepeatName[2]_controlName"
// Output -- the first part of name e.g. "pgPageName_RepeatName["
//  -- By Billy 08July2002
function get1stPartName(control_Full_Name)
{
	var pos1 = control_Full_Name.lastIndexOf("[");
	var pos2 = control_Full_Name.lastIndexOf("]");
	//alert("get1stPartName::pos1::" + pos1 + "  pos2::" + pos2);

  //Check if not Repeated (Note: if not repeat format just return the fullname)
  if(pos1 < 0 || pos2 < 0 || pos1 >= pos2)
    return control_Full_Name

	var name1stPart = control_Full_Name.substring(0, pos1 + 1);
  return name1stPart;
}

//New Function to get second part of Ccontrol name in JATO repeated format
// Input -- control full name e.g. "pgPageName_RepeatName[2]_controlName"
// Output -- the first part of name e.g. "pgPageName_RepeatName["
//  -- By Billy 08July2002
function get2ndPartName(control_Full_Name)
{
	var pos1 = control_Full_Name.lastIndexOf("[");
	var pos2 = control_Full_Name.lastIndexOf("]");
	//alert("get2ndPartName::pos1::" + pos1 + "  pos2::" + pos2);

  //Check if not Repeated (Note: if not repeat format just return "")
  if(pos1 < 0 || pos2 < 0 || pos1 >= pos2)
    return ""

	var name2ndPart = control_Full_Name.substring(pos2, control_Full_Name.length);
  return name2ndPart;
}

//New Function to get the total number of Rows in JATO repeated format
// Input -- control full name e.g. "pgPageName_RepeatName[2]_controlName"
// Output -- Total number of rows OR 0 if control not found
//  -- By Billy 08July2002
function getNumOfRow(control_Full_Name)
{
	var theCtl;
	var numElements = 0;

  var name1stPart = get1stPartName(control_Full_Name);
  var name2ndPart = get2ndPartName(control_Full_Name);

  //alert("getNumOfRow::name1stPart=" + name1stPart + "name2ndPart=" + name2ndPart);

	for(;numElements<100;numElements++)
	{

		theCtl = eval("document.forms[0].elements['" + name1stPart + numElements + name2ndPart + "']");
		if(theCtl == null)
			return numElements;

	}
	return 0;
}

function getRepeatRowId(myCtl)
{
	//alert("getRepeatRowId::start");

	var tmpCtlName = myCtl.name;
	//alert("getRepeatRowId::tmpCtlName::" + tmpCtlName);

	var pos1 = tmpCtlName.lastIndexOf("[");
	var pos2 = tmpCtlName.lastIndexOf("]");
	//alert("getRepeatRowId::pos1::" + pos1 + "  pos2::" + pos2);

  //Check if not Repeated
  if(pos1 < 0 || pos2 < 0 || pos1 >= pos2)
    return -1

	var tmpRowNum = -1;
	tmpRowNum = tmpCtlName.substring(pos1+1, pos2) * 1;
	//alert("getRepeatRowId::the substring = " + tmpCtlName.substring(pos1+1, pos2));
	//alert("getRepeatRowId::CurRowNum::" + tmpRowNum);

	return tmpRowNum;
}


//Adjusted for JATO format
// input -- elementFullName : full name of the fields to add e.g. "pgPageName_RepeatName[2]_controlName"
//          totname : short field name to set the result to
//                    (e.g. "txTotalDown", But the actual name will be "pgPageName_txTotalDown"
// -- by BILLY 08July2002
function addForm(elementFullName,totname)
{
  var totVal = 0;
  var totRows = getNumOfRow(elementFullName);

  var name1stPart = get1stPartName(elementFullName);
  var name2ndPart = get2ndPartName(elementFullName);

	//alert("addForm::name1stPart=" + name1stPart + "  name2ndPart=" + name2ndPart);

  /*to add total for more than one field*/
  for (el=0;el<totRows;el++){
    //var elValue = eval('fm.'+elname+'['+el+'].value*1.0');
    var elValue = eval("document.forms[0].elements['" + name1stPart + el + name2ndPart + "']" + ".value*1.0");
    if(isNaN(elValue)==true){
      elValue=0;
    }
    if(elValue!="" && elValue!=" "){
      totVal += elValue;
    }
  }

  //FXP23701, MCM, Dec 3, 2008 --  65000.00 + 1000.01 didn't work.
  //totVal = Math.floor(totVal*100);
  totVal = Math.round(totVal*100);
  
  var totString = totVal.toString();
  var preDec = totString.substring(0,totString.length-2);
  var postDec = totString.substring(totString.length-2,totString.length);
  totString = preDec+"."+postDec;
  var totalField= eval('document.forms[0].' + PAGE_NAME + '_' + totname);
  totalField.value = totString;
}

//==================================
//  Code block required for printf()
//==================================

var newline = (navigator.appVersion.lastIndexOf('Win') != -1) ? "\r\n" : "\n"
var string_constant_blank = "                                  "
var string_constant_zero  = "0000000000000000000000000000000000"

function err100(string)
{
  alert(string)
}

function isDigit(c)
{
  return "0123456789.".indexOf(c) == -1 ? 0 : 1
}

function printf(arg)
{
  var i, c, c2, c3, ww, pointer = 1, w = 0, d = 0, left, period
  var format = printf.arguments[0]
  var line = ""

  for (i = 0; i < format.length; i++) 
  {
    c = format.charAt(i)
   
    if (c == "\n") 
    {
      if (line == "") line = " "
      line += newline
    }
    else if (c == "%") 
    {
      left = 0
      i++
      c2 = format.charAt(i)
   
      if (c2 == "%") 
      {
        line += c2
        continue
      }
      else if (c2 == "-") 
      {
        left = 1
        c2 = format.charAt(++i)
      }
      
      if (isDigit(c2)) 
      {
        period = 0
        w = 0
        d = 0
      
        while ((c3 = format.charAt(i)) != "s" && c3 != "f" && c3 != "g" && c3 != "i" && c3 != "d") 
        {
          if (c3 == ".") 
          {
            period = 1
          }
          else if (isDigit(c3) == 0) 
          {
            err100("non digit after % ("+c3+")")
          }
          else if (period == 0) 
          {
            w = w*10+eval(c3)
          }
          else if (period == 1) 
          {
            d = d*10+eval(c3)
          }
          i++
        }
      
        if (c3 == "s") 
        {
          line += print_string(left, w, printf.arguments[pointer++])
        }
        else if (c3 == "g" || c3 == "f") 
        {
          line += print_number(c3, w, d, printf.arguments[pointer++])
        }
        else if (c3 == "i" || c3 == "d") 
        {
          line += print_number(c3, w, 0, printf.arguments[pointer++])
        }
      }
      else if (c2 == "s") 
      {
        line += print_string(left, 0, printf.arguments[pointer++])
      }
      else if (c2 == "f") 
      {
        line += print_number(c2, 0, 3, printf.arguments[pointer++])
      }
      else if (c2 == "g") 
      {
        line += print_number(c2, 0, 6, printf.arguments[pointer++])
      }
      else if (c2 == "i" || c2 == "d") 
      {
        line += print_number(c2, 0, 0, printf.arguments[pointer++])
      }
    }
    else 
    {
      line += c
    }
  }
  return line
}

function my_length(str)
{
  var i, c
  var retv = 0

  for (i = 0; i < 200; i++) 
  {
    if ((c = str.charAt(i)) == "") 
    {
      break
    }
    else if (c > "~") 
    {
      retv += 2
    }
    else 
    {
      retv++
    }
  }
  return retv
}

function print_string(left, w, arg)
{
  var ww, temp
  var line = ""
  
  if (w != 0) 
  {
    ww = my_length(""+arg)
    if (left == 1) 
    {
      line += (ww > w) ? arg : arg+string_constant_blank.substring(0, w-ww)
    }
    else 
    {
      line += (ww > w) ? arg : string_constant_blank.substring(0, w-ww)+arg //@@
    }
  }
  else 
  {
    line += arg
  }
  
  return line
}

function print_number(gf, w, d, number)
{
  var num
  
  if (gf == "g") 
  {
    num = print_number_g(w, d, number)
  }
  else if (gf == "f") 
  {
    num = print_number_f(w, d, number)
  }
  else if (gf == "i" || gf == "d") 
  {
    num = print_number_i(w, d, number)
  }
  
  return num
}

function print_number_i(w, d, number)
{
  var s, p, ww
  s = fixed2(number, d)
  ww = (""+s).length
  if (ww < w) 
  {
    s = string_constant_blank.substring(0, w-ww)+s
  }
  return s
}

function print_number_f(w, d, number)
{
  var s, p, ww
  s = fixed2(number, d)

  if ((""+s).charAt(0) == ".") 
  {
    s = "0"+s
  }
  else if ((""+s).indexOf("-.") != -1) 
  {
    s = "-0"+(""+s).substring(1, (""+s).length)
  }

  if ((""+s).indexOf(".") == -1) 
    s += "."

  ww = (""+s).length-1
  p = (""+s).indexOf(".")

  if (ww-p < d) 
    s += string_constant_zero.substring(0, d-(ww-p))

  ww = (""+s).length

  if (ww < w) 
    s = string_constant_blank.substring(0, w-ww)+s

  return s
}

function print_number_g(w, d, number)
{
  var abs = Math.abs(number)

  if (number == 0 || (0.001 <= abs && abs < Math.pow(10, d))) 
  {
    return print_number_g2(w, d, number)
  }
  else 
  {
    return print_number_e(w, d, number)
  }
}

function print_number_g2(w, d, number)
{
  var s, p, ww, abs, temp
  abs = Math.abs(number)
  if (number == 0) 
  {
    temp = 1
  }
  else if (abs >= 1) 
  {
    temp = d-(""+Math.floor(abs)).length
  }
  else 
  {
    temp = d-1

    while (abs < 1) 
    {
      temp++
      abs *= 10
    }
  }
  s = fixed2(number, temp)
  if ((""+s).charAt(0) == ".") 
  {
    s = "0"+s
  }
  else if ((""+s).indexOf("-.") != -1) 
  {
    s = "-0"+(""+s).substring(1, (""+s).length)
  }

  if ((""+s).indexOf(".") == -1) 
    s += "."
  
  ww = (""+s).length
  ww -= (number < 0 ? 1 : 0)+(Math.abs(number) < 1 ? 1 : 0)+1
  
  if (ww < d) 
    s += string_constant_zero.substring(0, d-ww)

  ww = (""+s).length
  
  if (ww < w) 
    s = string_constant_blank.substring(0, w-ww)+s

  return s
}

function print_number_e(w, d, number)
{
  var s, p, ww, exponent, sign, abs, temp
  w -= 4
  exponent = 0
  abs = Math.abs(number)

  while (abs < 1) 
  {
    exponent--
    abs *= 10
  }
  
  while (abs >= 10) 
  {
    exponent++
    abs /= 10
  }

  temp = (number < 0 ? -1 : 1)*abs
  return print_number_f(w, d-1, temp)+"e"+(exponent < 0 ? "-" : "+")+rec(exponent)
}

function rec(expo)
{
  expo = Math.abs(expo)
  return (expo < 10) ? "0"+expo : ""+expo
}

function fixed2(x, d)
{
  return Math.round(x*Math.pow(10,d))/Math.pow(10,d)
}
//======================================
//  End code block required for printf()
//======================================



// ==================== Generic Common JavaScript Functions (End)========================================
