// ===================== Functions for Online Field Validations -- (Begin) ==================================
// Global variable for Field Validation

/*
11/Apr/2006 DVG #DG404 #  2820  Open  Appraiser notes character limit
*/

var currFieldToValidate = "";

function isDigit(inp)
{
	if(inp == '0' || inp == '1' || inp == '2'  || inp == '3' || inp == '4' ||
   	   inp == '5' || inp == '6' || inp == '7'  || inp == '8' || inp == '9')
	{
	return true;
	}

	return false;
}

// Function to check if the input value is an Integer
function isInteger(inValue)
{
	for(var i=0;i<inValue.length;i++)
	{
		if(  isDigit(inValue.charAt(i)) == false )
			return false;
	}
	return true;
}

function isAlpha(inValue)
{
	for(var i=0;i<inValue.length;i++)
	{
		if(  isDigit(inValue.charAt(i)) == true )
			return false;
	}
	return true;
}

// Function to check if the input value includes a special characters
// that causes troubles for the BXP document processing.
function isSpecialCharacter(inValue)
{
	for(var i=0;i<inValue.length;i++)
	{
		if(  inValue.charAt(i) == '>' || inValue.charAt(i) == '<' ||
		     inValue.charAt(i) == '\"' || inValue.charAt(i) == '\&')
			return false;
	}
	return true;
}

// Function to check if the input value is a Decimal
function isDecimal(inValue)
{
	decimalFound = false;
	
//--Ticket468--start--15July2004//
	//// In order to support negative decimal validation.
	negativeValue = false;
	
	if(inValue.charAt(0) == '-')
	{
	   negativeValue = true;
	}
	
	var i = 0; //default
	
	if (negativeValue == true) 
	{
	    i = 1;
	}
//--Ticket468--end--15July2004//
	    

	for(i; i<inValue.length;i++)
	{		
		if( isDigit(inValue.charAt(i)) == false)
		{
			if( inValue.charAt(i) == '.' && decimalFound == false)
			{
				decimalFound = true;
			}
			else
			{
				return false;
			}
		}
		
	}
	return true;
}

// Function to round a Value to a Specified Number of Decimals and pad Zero(s)
function round_decimals(original_number, decimals)
{
	var result1 = original_number * Math.pow(10, decimals);
	var result2 = Math.round(result1);
	var result3 = result2 / Math.pow(10, decimals);
   return pad_with_zeros(result3, decimals);
}

function pad_with_zeros(rounded_value, decimal_places)
{
	// Convert the number to a string
	var value_string = rounded_value.toString();

	// Locate the decimal point
	var decimal_location = value_string.indexOf(".");
	// Is there a decimal point?
	if (decimal_location == -1)
	{
		// If no, then all decimal places will be padded with 0s
		decimal_part_length = 0;
		// If decimal_places is greater than zero, tack on a decimal point
       value_string += decimal_places > 0 ? "." : "";
	}
   	else
   	{
		// If yes, then only the extra decimal places will be padded with 0s
       decimal_part_length = value_string.length - decimal_location - 1;
    }

    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length;

    if (pad_total > 0)
    {
		// Pad the string with 0s
		for (var counter = 1; counter <= pad_total; counter++)
			value_string += "0";
	}
	return value_string;
}


// Function to check if the year is a leap year
function isLeapYear(intYear) {
	if (intYear % 100 == 0)
	{
		if (intYear % 400 == 0) { return true; }
	}
	else
	{
		if ((intYear % 4) == 0) { return true; }
	}
	return false;
}

function getMonthVal(str)
{
		if(str == "" || str == "0.0")
			return 0;
		if(str == "Jan" || str == "January"  || str == "1.0" )
			return 1;
		if(str == "Feb" || str == "February"  || str == "2.0")
			return 2;
		if(str == "Mar" || str == "March"  || str == "3.0")
			return 3;
		if(str == "Apr" || str == "April"  || str == "4.0")
			return 4;
		if(str == "May"  || str == "5.0")
			return 5;
		if(str == "Jun" || str == "June"  || str == "6.0")
			return 6;
		if(str == "Jul" || str == "July"  || str == "7.0")
			return 7;
		if(str == "Aug" || str == "August"  || str == "8.0")
			return 8;
		if(str == "Sep" || str == "September"  || str == "9.0")
			return 9;
		if(str == "Oct" || str == "October"  || str == "10.0")
			return 10;
		if(str == "Nov" || str == "November"  || str == "11.0")
			return 11;
		if(str == "Dec" || str == "December"  || str == "12.0")
			return 12;

    // else return 0
    return 0;
}


// Function to check if the attached field value is an Integer with requestLength
//		Note : if requestLength = 0 means any length
function isFieldInteger(requestLength)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( !isInteger(theFieldValue) )
	{
		alert(NUMERIC_ONLY);
		var tmp = theField.value;
		theField.focus();
		theField.select();
		return false;
	}
	else
	{
		// Check Length
		if( requestLength > 0 )
		{
			if( theFieldValue.length != requestLength )
			{
				alert(printf(INPUT_NUM_DIGITS, requestLength));
				theField.focus();
				theField.select();
				return false;
			}
		}
	}
	currFieldToValidate = "";
	return true;
}

// Function to check if the attached field value is within the range (integer)
function isFieldInIntRange(min, max)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( !isInteger(theFieldValue) )
	{
		alert(NUMERIC_ONLY);
		var tmp = theField.value;
		theField.focus();
		theField.select();
		return false;
	}
	else
	{
		// Check Length
		if( theFieldValue < min || theFieldValue > max )
		{
			alert(printf(INPUT_VALUE_RANGE, min, max));
			theField.focus();
			theField.select();
			return false;
		}
	}
	currFieldToValidate = "";
	return true;
}

// Function to check if the attached field value is within the range (decimal)
function isFieldInDecRange(min, max)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( !isDecimal(theFieldValue) )
	{
		alert(INVALID_DECIMAL);
		var tmp = theField.value;
		theField.focus();
		theField.select();
		return false;
	}
	else
	{
		// Check Length
		if( theFieldValue < min || theFieldValue > max )
		{
			alert(printf(INPUT_VALUE_RANGE, min, max));
			theField.focus();
			theField.select();
			return false;
		}
	}
	currFieldToValidate = "";
	return true;
}

//--Ticket468--start--15July2004//
function isFieldInFloatRange(min, max)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( !isDecimal(theFieldValue) )
	{
		alert(INVALID_DECIMAL);
		var tmp = theField.value;
		theField.focus();
		theField.select();
		return false;
	}
	else
	{
		// Check Length
		if( theFieldValue < min || theFieldValue > max )
		{
			alert(printf(INPUT_DIGIT_FLOAT_RANGE, min, max));			
			theField.focus();
			theField.select();
			return false;
		}
	}
	currFieldToValidate = "";
	return true;
}
//--Ticket468--end--15July2004//

// Function to check if the attached field value is an Alpha string with any length
function isFieldAlpha()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;
	
	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( !isAlpha(theFieldValue) )
	{
		alert(ALPHA_ONLY);
		theField.focus();
		theField.select();
		return false;
	}
	currFieldToValidate = "";
	return true;
}

// Function to check if the attached field value is an Alpha string with any length
function isFieldHasSpecialChar()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;
	
	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( !isSpecialCharacter(theFieldValue) )
	{		
		alert(DO_NOT_USE_SPECIAL_CHARACTERS);
		theField.focus();
		theField.select();
		return false;
	}
	currFieldToValidate = "";
	return true;
}

// Function to check and convert the attached fieldvalue to a Decimal number with # of decimal place = numDecPlace
function isFieldDecimal(numDecPlace)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( !isDecimal(theFieldValue) )
	{
		alert(INVALID_DECIMAL);
		theField.focus();
		theField.select();
		return false;
	}

	// Check for precision
	var theSplitedValues = theFieldValue.split(".");
	if(theSplitedValues.length > 1)
	{
		if(theSplitedValues[1].length > numDecPlace)
		{
			alert(printf(MAX_PRECISION, numDecPlace));
			theField.focus();
			theField.select();
			return false;
		}
	}

	// Convert field to correct format
	theField.value = round_decimals(theField.value, numDecPlace);
	currFieldToValidate = "";
	return true;
}

// Function to Check Postal Code : FSA
function isFieldFSA()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if(theFieldValue.length != 3)
   {
		alert(INVALID_POSTAL_CODE);
      	theField.focus();
		theField.select();
		return false;
   }

   if(theFieldValue.length == 3)
   {
   		var fletter = theFieldValue.charAt(0);
       var sdigit = theFieldValue.charAt(1);
       var tletter = theFieldValue.charAt(2);

       var regexp = /^[a-zA-Z]\d[a-zA-Z]$/;
       if (!regexp.test(theFieldValue))
		//if(isDigit(fletter) || !isDigit(sdigit) || isDigit(tletter))
       {
			alert(INVALID_POSTAL_CODE);
           theField.focus();
			theField.select();
			return false;
   	   	}

   }
   theField.value = theFieldValue.toUpperCase();
   currFieldToValidate = "";
   return true;
}

// Function to Check Postal Code : LDU
function isFieldLDU()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if(theFieldValue.length != 3)
   {
		alert(INVALID_POSTAL_CODE);
      	theField.focus();
		theField.select();
		return false;
   }

	if(theFieldValue.length == 3)
   {
   		var lletter = theFieldValue.charAt(0);
       var ddigit = theFieldValue.charAt(1);
       var uletter = theFieldValue.charAt(2);

       var regexp = /^\d[a-zA-Z]\d$/;
       if (!regexp.test(theFieldValue))
		//if(!isDigit(lletter) || isDigit(ddigit) || !isDigit(uletter))
       {
			alert(INVALID_POSTAL_CODE);
      theField.focus();
			theField.select();
			return false;
   	   	}

   }
   theField.value = theFieldValue.toUpperCase();
   currFieldToValidate = "";
   return true;
}

// Function to check for the Valid Date
//		Note : this function assumed the Date ranges are corrected e.g. Month(1-12), Day(1-31), Year(1800-2100)
function isFieldValidDate(theYear, theMonth, theDay)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	// Get the RowId if ti is in a repeated : assume the Day field is a TextBox
  // Adjusted by Billy for JATO format -- 08July2002
	var rowId = getRepeatRowId(theField);
  var name1stPart = get1stPartName(theFieldName);
	var inputRowNdx = "";
	if (rowId != -1) inputRowNdx = rowId+"]_";

	var theYearCtl, theMonthCtl, theDayCtl;
  if (rowId != -1)
  {
    theYearCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theYear + "']");
    theMonthCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theMonth + "']");
    theDayCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theDay + "']");
  }
  else
  {
    theYearCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theYear + "']");
    theMonthCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theMonth + "']");
    theDayCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theDay + "']");
  }
  var theYearValue = theYearCtl.value;
	var theMonthValue = theMonthCtl.value;
	if(!isInteger(theMonthValue)) theMonthValue = getMonthVal(theMonthValue);
	var theDayValue = theDayCtl.value;
  //===============End of Chamges (Billy) ==========================

	if(!isInteger(theDayValue)) 
	{
		alert(INTEGER_ONLY);
		theField.focus();
		theField.select();
		return false;
	}

	//alert("theYearValue = " + theYearValue + " theMonthValue = " + theMonthValue + " theDayValue " + theDayValue);

	// Check if any Date field is missing
	if(theMonthValue == "" || theMonthValue == 0)
	{
		//alert('Please input Month !');
    currFieldToValidate = "";
    return true;

	}
	if(theDayValue == "")
	{
    //alert('Please input Day !');
    currFieldToValidate = "";
    return true;
	}
	if(theYearValue == "")
	{
		//alert('Please input Year !');
    currFieldToValidate = "";
    return true;
	}

	// Check 31 day month
	if((theMonthValue == 4 || theMonthValue == 6 || theMonthValue == 9 || theMonthValue == 11) && (theDayValue > 30 || theDayValue < 1))
	{
		  //alert(INVALID_NUM_DAYS_31); 
		  alert(DATE_MUST_FALL_BETWEEN_1_AND_30);
		  theField.focus();
		  return false;
	}

	// Check 31 day month
	if((theMonthValue == 1 || theMonthValue == 3 || theMonthValue == 5 || theMonthValue == 7 || theMonthValue == 8 || theMonthValue == 10 || theMonthValue == 12) && (theDayValue > 31 || theDayValue < 1))
	{
		alert(DATE_MUST_FALL_BETWEEN_1_AND_31); 
		theField.focus();
		return false;
	}

	// Check day for Feb
	if( theMonthValue == 2)
	{
		if (isLeapYear(theYearValue ) == true)
		{
			if (theDayValue<1 || theDayValue > 29)
			{
				alert(DATE_MUST_FALL_BETWEEN_1_AND_29);
				theField.focus();
				return false;
			}
		}
		else
		{
			if (theDayValue<1 || theDayValue > 28)
			{
				alert(DATE_MUST_FALL_BETWEEN_1_AND_28);
				theField.focus();
				return false;
			}
		}
	}

	currFieldToValidate = "";
	return true;

}

// Function to check for the Valid Date with date Range
//		theFromDateToCompare, theFromDateToCompare :
//							-- Date to compare. i.e. the date input must less than or equal to this date
//								It supports the following formats :
//								mm-dd-yyyy, mm/dd/yyyy, mmm dd yyyy
//							-- empty string ("") means not compare
//		Note : this function assumed the Date ranges are corrected e.g. Month(1-12), Day(1-31), Year(1800-2100)
function isFieldValidDateInRange(theYear, theMonth, theDay, theFromDateToCompare, theToDateToCompare)
{
	// Valid if the date if valid first and out if invalid date input
	if(!isFieldValidDate(theYear, theMonth, theDay))
	{
		return false;
	}

	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	// Get the RowId if ti is in a repeated : assume the Day field is a TextBox
  // Adjusted by Billy for JATO format -- 08July2002
	var rowId = getRepeatRowId(theField);
  var name1stPart = get1stPartName(theFieldName);
	var inputRowNdx = "";
	if (rowId != -1) inputRowNdx = rowId+"]_";

	var theYearCtl, theMonthCtl, theDayCtl;
  if (rowId != -1)
  {
    theYearCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theYear + "']");
    theMonthCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theMonth + "']");
    theDayCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theDay + "']");
  }
  else
  {
    theYearCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theYear + "']");
    theMonthCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theMonth + "']");
    theDayCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theDay + "']");
  }
  var theYearValue = theYearCtl.value;
	var theMonthValue = theMonthCtl.value;
	if(!isInteger(theMonthValue)) theMonthValue = getMonthVal(theMonthValue);
	var theDayValue = theDayCtl.value;
  //===============End of Chamges (Billy) ==========================

	if(theToDateToCompare != "")
	{
		// Compare the date and report error if the input date > theToDateToCompare
		var theDateInput = new Date(theMonthValue + "/" + theDayValue + "/" + theYearValue);
		var theDateCompare = new Date(theToDateToCompare );
		//alert('The theDateInput = ' + theDateInput.toLocaleString() + 'The theToDateToCompare = ' + theToDateToCompare.toLocaleString());
		if(theDateInput > theDateCompare)
		{
			if(!confirm('Invalid Date -- Please input a date before ' + theToDateToCompare + ' !\n  Do you want to override it ?'))
      {
        theField.focus();
        // Commented it out for IE4.0 users in MCAP -- IE4.0 doesn't support
        /*
        try{
        theField.select();
        }
        catch(exception)
        {
          // Nothing to do here
        }
        */
        return false;
      }

		}
	}
	if(theFromDateToCompare != "")
	{
		// Compare the date and report error if the input date > theToDateToCompare
		var theDateInput = new Date(theMonthValue + "/" + theDayValue + "/" + theYearValue);
		var theDateCompare = new Date(theFromDateToCompare);
		//alert('The theDateInput = ' + theDateInput.toLocaleString() + 'The theFromDateToCompare= ' + theFromDateToCompare.toLocaleString());
		if(theDateInput < theDateCompare)
		{
			if(!confirm('Invalid Date -- Please input a date after ' + theFromDateToCompare + ' !\n  Do you want to override it ?'))
      {
        theField.focus();
        // Commented it out for IE4.0 users in MCAP -- IE4.0 doesn't support
        /*
        try{
        theField.select();
        }
        catch(exception)
        {
          // Nothing to do here
        }
        */
        return false;
      }

		}
	}
	currFieldToValidate = "";
   return true;
}

// Function to check for the Duration (# of Year and # of Month)
//		Month should be 0 to 11 if Year input. Otherwise Month can be 0 to 99.
function isFieldValidDuration(theYear, theMonth)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate != "" )
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	// Get the RowId if ti is in a repeated : assume the Year field is a TextBox
  // Adjusted by Billy for JATO format -- 08July2002
	var rowId = getRepeatRowId(theField);
  var name1stPart = get1stPartName(theFieldName);
	var inputRowNdx = "";
	if (rowId != -1) inputRowNdx = rowId+"]_";

	var theYearCtl, theMonthCtl;
  if (rowId != -1)
  {
    theYearCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theYear + "']");
    theMonthCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theMonth + "']");
  }
  else
  {
    theYearCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theYear + "']");
    theMonthCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theMonth + "']");
  }
  var theYearValue = theYearCtl.value;
	var theMonthValue = theMonthCtl.value;
	//===============End of Chamges (Billy) ==========================

	// Check if Year input > 0
	if(theYearValue > 0)
  {
      //FXP20516 Popup message appears twice
      if (theYearCtl==theField) {
      // Check # of years 0-80
      if(theYearValue > 80)
      {
        alert(NUM_YEARS);
        theYearCtl.focus();
			  theYearCtl.select();
//#5441       currFieldToValidate = theYear;
			  return false;
      }
      }
      //FXP20516 Popup message appears twice
      if (theMonthCtl==theField) {
      // Check # of Month 0-11
   		if(theMonthValue > 11)
   		{
			  alert(NUM_MONTHS);
        theMonthCtl.focus();
			  theMonthCtl.select();
//#5441       currFieldToValidate = theMonth;
			  return false;
   	  }
      }
	}
	currFieldToValidate = "";
  return true;
}

// Function to check if the max length of the field
function isFieldMaxLength(maxLength)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( theFieldValue.length > maxLength )
	{
		alert(printf(MAX_CHARS, maxLength));
		theField.focus();
		theField.select();
		return false;
	}
	currFieldToValidate = "";
	return true;
}

//#DG404
/* Function to check if the max length of the field and select the excess
 usage:
    <jato:textArea name="tbCommentsForAppraiser" rows="4" cols="80"
      extraHtml="onChange='return isFieldMaxLengthSel(15)' "/>
*/
function isFieldMaxLengthSel(maxLength)
{
  var theField =(NTCP) ? event.target : event.srcElement;
  var theFieldValue = theField.value;

  if( theFieldValue.length > maxLength )
  {
    alert(printf(MAX_CHARS, maxLength));
    var reg = theField.createTextRange();
    //var excs = theFieldValue.length - maxLength ;

    // will have to adjust for <cr><lf> counts as only 1 character here!
    var adj = maxLength;
    for (var i = 0; i < maxLength; i++)
      if (theFieldValue.charAt(i) == '\r')
        adj--;
    reg.moveStart("character", adj);
    //     alert("'"+reg.text+"' len: "+reg.text.length);
    reg.select();
    theField.focus();
    return false;
  }
  return true;
}

// ***** Change by NBC Impl. Team - Version 1.2 - Start *****//
// unction to check if street name is required in applicant information
function isStreetNameBlank()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
		if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
		{
		currFieldToValidate = theFieldName;
		}
		else
		{
				return true;
		}

	if( isFieldEmpty(theFieldValue) )
	{		
		//alert(theFieldValue is required);
		alert(BORROWER_ADDRESS_FIELD_REQUIRED);
		theField.focus();
		theField.select();
		return false;
	}
	else
	return true;
}

// Function to check if the field is empty
function isFieldEmpty(value)
{
	if(value == "")
	{
		return true;
	}
	else
	{
	   // Check Length
	  if( ltrim(value).length == 0 )
	  {
		  return true;
	  }
	}
	return false;
}

//Fuction to validate the StreetName field of the Borrower Address
//If there are more than one Address each StreetName field of the Addresses will be checked
function isStreetNameEntered()
{
  //alert("Street names are being validated");
  var numOfAddress = getNumOfRow("pgApplicantEntry_RepeatedApplicantAddress[0]_txStreetName");
  var name1stPart = get1stPartName("pgApplicantEntry_RepeatedApplicantAddress[0]_txStreetName");

  // Handle multiple rows
  for( x=0; x < numOfAddress; ++x )
	{
       inputRowNdx = x+"]_";

	 var theField = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txStreetName" + "']");
       var currAddressStreetName = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txStreetName" + "']" + ".value");
       //alert("The currAddressStreetName = " + currAddressStreetName + " RowNum = " + x);

       if(isFieldEmpty(currAddressStreetName)){
         alert(BORROWER_ADDRESS_FIELD_REQUIRED);
         theField.focus();
         return false;
       }
  }
   //alert("returnig true");
   return true;
}
//MCM Impl Team. XS_2.7 - Added special character validation
function isAnySpecialChar(obj){
	var inValue=obj.value;
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}
	for(var i=0;i<inValue.length;i++)
		{
				if(inValue.charAt(i) == '>' || inValue.charAt(i) == '<' || inValue.charAt(i) == '\"' 
					|| inValue.charAt(i) == '\&' || inValue.charAt(i) == '!' || inValue.charAt(i) == '@' 
					|| inValue.charAt(i) == '#' || inValue.charAt(i) == '$' || inValue.charAt(i) == '%' 
					|| inValue.charAt(i) == '^' || inValue.charAt(i) == '&' || inValue.charAt(i) == '*' 
					|| inValue.charAt(i) == '(' || inValue.charAt(i) == ')' || inValue.charAt(i) == '`' 
					|| inValue.charAt(i) == '~' || inValue.charAt(i) == '/' || inValue.charAt(i) == '|'
					|| inValue.charAt(i) == '.' || inValue.charAt(i) == ',' || inValue.charAt(i) == "?" 
					|| inValue.charAt(i) == "?" || inValue.charAt(i) == "=" || inValue.charAt(i) == "+"
					|| inValue.charAt(i) == "-" || inValue.charAt(i) == "_" || inValue.charAt(i) == "'" 
					|| inValue.charAt(i) == "\\" || inValue.charAt(i) == "}" || inValue.charAt(i) == "{"
					|| inValue.charAt(i) == "]" || inValue.charAt(i) == "[" || inValue.charAt(i) == ":"
					|| inValue.charAt(i) == ";"){
						
					alert('Warning : this text contains one or more prohibited characters;please remove or replace before continuing');
					obj.focus();
					obj.select();
					return false;
				}
		}
	currFieldToValidate = "";
	return true;
}
//this function will perform an Alphamumeric check.
function isAlphaNumeric(obj){
	var inputVal=obj.value;
	var validString="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for(var i=0;i<inputVal.length;i++){
			if((validString.indexOf(inputVal.charAt(i))) < 0){
				alert('Warning : this text contains one or more non alpha/numeric characters;please remove or replace before continuing');
				obj.focus();
				obj.select();
				return false;
			}
		}
}
function checkRange(obj,min,max){
	if(obj.value.length > max || obj.value.length < min){
		alert(printf(INPUT_VALUE_TEXT_RANGE, min, max));
		obj.focus();
		obj.select();
		return false;
	}

}
function specialCharCheck(obj){
	var inputString=obj.value;
	var invalidString='>\"&<';
	for(var i=0;i<inputString.length;i++){
		if((invalidString.indexOf(inputString.charAt(i))) >= 0){
			alert(printf(DO_NOT_USE_SPECIAL_CHARACTERS));
			obj.focus();
			obj.select();
			return false;
		}
	}
}

//***** Change by NBC Impl. Team - Version 1.2 - End*****//

//Function to validate the line of business
//***** 3.2.2GR LOB to Allow $0 *****//
function isFieldValidLob()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		return true;
	}

    var userType = document.getElementsByName("pgUserAdministration_cbUserTypes");
    var userTypeId = userType[0].options[userType[0].selectedIndex].value;

    if (theFieldValue == "") {
        if (userTypeId >= 6 && userTypeId <= 12) {
			alert(LOB_DECISION_USERTYPE);
		    return false;
		}
    }
    else {
        if(isFieldDecimal(2) && theFieldValue < 0 ) {
		    if (userTypeId >= 6 && userTypeId <= 12) {
			    alert(LOB_DECISION_USERTYPE);
		        return false;
			}
			else if (userTypeId > 0 && userTypeId < 20) {
			    alert(LOB_NONDECISION_USERTYPE);
		        return false;
			}
		}
	}

	currFieldToValidate = "";
	return true;

}
// ===================== Functions for Online Field Validations -- (End) ==================================

// added for FXP24428
function blankDropDownCheck(field,message){
 	var mi = document.getElementById(field); 
 	var opt = mi.selectedIndex;
	  if(mi[opt].value==0)
	  {
		alert(message);
		mi.focus();
		return false;
      }
      return true;
}

// Function to validate Email Address 
function isEmail ()
{ 
    var inpTextbox =(NTCP) ? event.target : event.srcElement;
    var s = inpTextbox.value;
    var inpName = inpTextbox.name;

	if(s.length != 0)
	{
	    if (isWhitespace(s))
	    {
			alert(INVALID_EMAIL);
			inpTextbox.select();    		 
			return false;
	    }

	    if (!isvalidEmailChar(s)) 
	    {
			alert(INVALID_EMAIL);
			inpTextbox.select();    		 
			return false;
	    }

	    atOffset = s.lastIndexOf('@');

	    if ( atOffset < 1 )
	    {
			alert(INVALID_EMAIL);
			inpTextbox.select();    		 
			return false;
	    }   
	    else 
	    {
			dotOffset = s.indexOf('.', atOffset);
			
			if ( dotOffset < atOffset + 2 || dotOffset > s.length - 2 ) 
			{
				alert(INVALID_EMAIL);
				inpTextbox.select();    		 
				return false;
			}
			
			// There must be at least 2 characters after the last �.�
			if((s.length) <=  (dotOffset+2))
			{
				alert(INVALID_EMAIL);
				inpTextbox.select();    		 
				return false;
			}
		}
	}
   return true;
}

// Validate for whitespace
function isWhitespace (s)
{   

	var i;
	var whitespace = " \t\n\r";
    if (isEmpty(s)) return true;

    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    return true;
}

//Validate for valid email character
function isvalidEmailChar (s)
{   var i;

    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);

        if (! (isLetter(c) || isDigit(c) || (c=='@') || (c=='.') || (c=='_') || (c=='-') || (c=='+')
              || (c=='\\')  )) 
	    {
       		return false;
	    }
    }

    return true;
}

// isEmpty
function isEmptyField(val)
{
	if(val == "" )
		return true;
	return false;
}	
function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}

function isLetter (c)
{   return ( ((c >= "a") && (c <= "z")) || ((c >= "A") && (c <= "Z")) )
}

//QC-Ticket-268- Start
function checkMIInsurer(field1,field2,field3,message1,message2,message3){
 	
 	var specialFeatureID=document.getElementById(field1).value;
 	var miIndicator = document.getElementById(field2);
 	var optMiIndicator = miIndicator.selectedIndex;
 	var miInsurer = document.getElementById(field3);
 	var optMiInsurer = miInsurer.selectedIndex;

	//Pre Approval
	if(specialFeatureID == 1 && miInsurer[optMiInsurer].value != 0 ) {
 		alert(message1+" "+miInsurer.options[optMiInsurer].text+".");
 		return false;
 	}

 	//Application for Loan Assessment
 	if(miIndicator[optMiIndicator].value == 4 && miInsurer[optMiInsurer].value == 3 ) {
 		 alert(miInsurer.options[optMiInsurer].text+" "+message2);
 		 return false;
 	}
 	//Mortgage Insurance Pre Qualification
 	if(miIndicator[optMiIndicator].value == 5) {
 		if(miInsurer[optMiInsurer].value == 2 || miInsurer[optMiInsurer].value == 3) {
 			 alert(miInsurer.options[optMiInsurer].text+" "+message3);
 			 return false;
 		}
 	}
		return true;
}
//QC-Ticket-268- End
