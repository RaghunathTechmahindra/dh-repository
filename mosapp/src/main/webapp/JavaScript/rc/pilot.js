/**************************************
 * class ProgressBarTestingDialog
 * this dialog is for the rich client side testing.
 */

/**
 * constructor.
 */
filogix.pilot.ProgressBarTestingDialog =
    function(promptIconFile, promptMsg, barClassName) {

    if (arguments.length == 0) {
        return;
    }

    this.base = filogix.gadget.ProgressBarDialog;
    this.base(promptIconFile, promptMsg, barClassName);

    var buttonTr = document.createElement("tr");
    var buttonTd = document.createElement("td");
    buttonTd.setAttribute("vAlign", "middle");
    buttonTd.setAttribute("colSpan", "2");
    buttonTd.setAttribute("align", "center");

    // add buttons to simulate the response.

    // the button to simulate the error response.
    var errorButton = document.createElement("button");
    errorButton.style.fontSize = "10pt";
    errorButton.style.width = "60px";
    errorButton.style.margin = "0px 5px";
    errorButton.innerHTML = "If Error";
    // the behaviour to simulate the error response.
    errorButton.onclick = avmErrorResponse;
    buttonTd.appendChild(errorButton);

    // the button to simulate the success response.
    var successButton = document.createElement("button");
    successButton.style.fontSize = "10pt";
    successButton.style.width = "100px";
    successButton.style.margin = "0px 5px";
    successButton.innerHTML = "If Success";
    // the behaviour to simulate the success response.
    successButton.onclick = avmSuccessResponse;
    buttonTd.appendChild(successButton);

    buttonTr.appendChild(buttonTd);
    this.progressTbody.appendChild(buttonTr);
}

filogix.pilot.ProgressBarTestingDialog.prototype =
    new filogix.gadget.ProgressBarDialog();

/**
 * class ProgressBarTestingDialog END
 **************************************/
