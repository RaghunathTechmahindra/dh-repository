(function() {
	
	var STATUS_RESPONSE_TIMEOUT = 455;

    var msgBox = filogix.msgBox;

    var alertCallback = function() {
        IsSubmitButton();
        document.forms[0].submit();
        //just submit HTTP request. this is not ajax way...
    };
    var MIService = function(cancel) {
        this.requestType = "express.service.MIService";
        this.timeoutTimeMillSec = 90000;
        this.cancel = cancel;
        this.miProviderName;
    };
    //inherit from SimpleXHRService
    MIService.prototype = new filogix.express.service.SimpleXHRService();

    MIService.prototype.handleRuntimeError = function(err) {
        if(this.cancel) {
            msgBox.alert("#MI_GENERIC_CANCEL_REQUEST_FAILURE_TITLE#", "#MI_GENERIC_SYSTEM_ERROR_MSG#", alertCallback);
        } else {
            msgBox.alert("#MI_GENERIC_REQUEST_FAILURE_TITLE#", "#MI_GENERIC_SYSTEM_ERROR_MSG#", alertCallback);
        }
    };

    MIService.prototype.handleHttpError = function(responseObj) {

        if(this.cancel) {
            msgBox.alert("#MI_GENERIC_CANCEL_REQUEST_FAILURE_TITLE#", "#MI_GENERIC_SYSTEM_ERROR_MSG#", alertCallback);
        } else {
            msgBox.alert("#MI_GENERIC_REQUEST_FAILURE_TITLE#", "#MI_GENERIC_SYSTEM_ERROR_MSG#", alertCallback);
        }
    };

    MIService.prototype.handleFailureReceived = function(responseObj) {

    	var msg;
    	if(responseObj.rcResponseErrors)
		{
			if (responseObj.rcResponseErrors == 455)
			{
				//show specific error message for MI_GENERIC_REQUEST_TIMEOUT_MSG
				msg = msgBox.getClientMsg("MI_GENERIC_REQUEST_TIMEOUT_MSG", [responseObj.rcResponseErrors], false);
			}
			else if (responseObj.rcResponseErrors == 480)
			{
				//show specific error message for MI_GENERIC_REQUEST_DELAYED
				msg = msgBox.getClientMsg("MI_GENERIC_REQUEST_DELAYED_MSG", [responseObj.rcResponseErrors], false);
			}
			else
			{
				//show standard message
				msg = msgBox.getClientMsg("MI_GENERIC_SYSTEM_ERROR_MSG_WITH_ERROR_CODE", [responseObj.rcResponseErrors], false);
			}
    	} 
		else 
		{
    		msg = msgBox.getClientMsg("MI_GENERIC_SYSTEM_ERROR_MSG", null, false);
    	}
    	
        if(this.cancel) {
        	// in case of timeout
        	var statusId = responseObj.rcResponseStatus;
        	if(statusId == STATUS_RESPONSE_TIMEOUT) {
        		msgBox.alert("#MI_GENERIC_REQUEST_TIMEOUT_TITLE#", "#MI_GENERIC_REQUEST_TIMEOUT_MSG#", alertCallback);
        	}
        	else
        		msgBox.alert("#MI_GENERIC_CANCEL_REQUEST_FAILURE_TITLE#", msg, alertCallback);
        	
        } else {
        	// in case of timeout
        	var statusId = responseObj.rcResponseStatus;
        	if(statusId == STATUS_RESPONSE_TIMEOUT) {
        		msgBox.alert("#MI_GENERIC_REQUEST_TIMEOUT_TITLE#", "#MI_GENERIC_REQUEST_TIMEOUT_MSG#", alertCallback);
        	}
        	else
        		msgBox.alert("#MI_GENERIC_REQUEST_FAILURE_TITLE#", msg, alertCallback);
        }
    };

    MIService.prototype.handleSuccessReceived = function(responseObj) {
        if(this.cancel) {
            msgBox.alert("#MI_GENERIC_CANCEL_REQUEST_SUCCESS_TITLE#", "#MI_GENERIC_CANCEL_REQUEST_SUCCESS_MSG#", alertCallback);
        } else {
            msgBox.alert("#MI_GENERIC_REQUEST_SUCCESS_TITLE#", "#MI_GENERIC_REQUEST_SUCCESS_MSG#", alertCallback);
        }
    };

    MIService.prototype.createMainDialog = function() {

        var dialog = new filogix.gadget.SimplePreloaderDialog();

        if(this.cancel) {
            dialog.setTitle(msgBox.getClientMsg("MI_GENERIC_CANCEL_REQUEST_SENDING_TITLE", [this.miProviderName]));
            dialog.setPromptMsg(msgBox.getClientMsg("MI_GENERIC_CANCEL_REQUEST_SENDING_MSG"));
        } else {
            dialog.setTitle(msgBox.getClientMsg("MI_GENERIC_REQUEST_SENDING_TITLE", [this.miProviderName]));
            dialog.setPromptMsg(msgBox.getClientMsg("MI_GENERIC_REQUEST_SENDING_MSG"));
        }

        dialog.setPreloaderImage("preloader.gif");
        return dialog;

    };
    
    //expose to the global scope
    filogix.express.service.MIService = MIService;

})();
