var confirmCancel = null;

function customConfirmAction(flag, recallBtnName)
{
	var confirmBox = document.getElementById("customConfirmBox");
	document.body.removeChild(confirmBox);
	hideSelectElements(false);
	if(flag)
	{
		
		confirmCancel = true;
		document.getElementsByName(recallBtnName)[0].click();
	}	
}



function customConfirm(text, btnName1, btnName2, recallBtnName)
{
	if(confirmCancel)
	{
		return true;
	}
	
	var width = 0;
	var height = 0;
	
	//find the browser height position
	if( typeof( window.innerWidth ) == 'number' ) 
	{
    		//Non-IE
	    	width = window.innerWidth;
	} 
	else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) 
	{
    		//IE 6+ in 'standards compliant mode'
		width = document.documentElement.clientWidth;
 	} 
	else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) 
	{
    		//IE 4 compatible
		width = document.body.clientWidth;
  	}
	//determine the height position by the cancel button
	var button = document.getElementsByName(recallBtnName)[0];
	while(button != null)
	{
		height += button.offsetTop;
		button = button.offsetParent;
	}

	var textStringArray = text.split("\n");	
	var outerDiv = document.createElement('div');
	outerDiv.id = "customConfirmBox"; 
	outerDiv.style.background = "rgb(212,208,200)";
	outerDiv.style.border = "1px solid #3b3131";
	outerDiv.style.position = "absolute";
	outerDiv.style.visibility = "visible";
	outerDiv.style.textAlign = "left";		
	outerDiv.style.top =height-350;
	outerDiv.style.left= (width - 760)/2 ;
	outerDiv.style.width = "760px"		
	outerDiv.style.zIndex=100;
	
	//generating the toolBar
	var barDiv = document.createElement('div');
	barDiv.style.width = "100%";
	barDiv.style.height = "20";
	barDiv.style.background = "rgb(10,36,106)";
	
	outerDiv.appendChild(barDiv);
	
	//generating TEXT area div
	var textDiv = document.createElement('div');
	textDiv.style.fontSize = 13;
	textDiv.style.fontFamily = "Tahoma";
	textDiv.style.padding = "20 20 20 20";
	for(i = 0; i < textStringArray.length; i++)
	{
		var textNode = document.createTextNode(textStringArray[i]);
		var lineBreak = document.createElement("br");
		textDiv.appendChild(textNode);
		textDiv.appendChild(lineBreak);
	}
	outerDiv.appendChild(textDiv);
	
	//generating BUTTON area div
	var btnDiv = document.createElement('div');
	btnDiv.style.textAlign = "center";
	btnDiv.style.paddingBottom = 20;
	var btn1 = document.createElement('input');
	var btn2 = document.createElement('input');
	btn1.type = "button";
	btn2.type = "button";
	btn1.value = btnName1;
	btn2.value = btnName2;
	btn1.style.width = 80;
	btn2.style.width = 80;
	btn1.style.height = 25;
	btn2.style.height = 25;
	btn1.fontSize = "8pt";
	btn2.fontSize = "8pt";
	btn1.style.marginRight = "5px";
	btn1.onclick = function(){customConfirmAction(true, recallBtnName);};
	btn2.onclick = function(){customConfirmAction(false, recallBtnName);};
	
	btnDiv.appendChild(btn1);

	btnDiv.appendChild(btn2);

	outerDiv.appendChild(btnDiv);
	
	document.body.appendChild(outerDiv);
	
	hideSelectElements(true);
	
	return false;
		
}
