/*******************************************************************************
 * class UserAdminDialog pop up the institution selection.
 */
/**
 * the constructor.
 */
filogix.gadget.UserAdminDialog =
    function(cancelImgFile, okImgFile) {
       
    this.okImgFile = "../images/close.gif";
    if (okImgFile != null) {
        this.okImgFile = okImgFile;
    }

    this.init(true, true);
}

// inheritance
filogix.gadget.UserAdminDialog.prototype = new filogix.gadget.DialogBase();
filogix.gadget.UserAdminDialog.prototype.constructor =
    filogix.gadget.UserAdminDialog;
filogix.gadget.UserAdminDialog.superclass = filogix.gadget.DialogBase.prototype;

/**
 * prototype.
 */
filogix.gadget.UserAdminDialog.prototype.init = function(isModal, autoClose) {

    filogix.gadget.UserAdminDialog.superclass.init.call(this, isModal,
                                                         autoClose);
                                                         
      this.contentArea.style.backgroundColor = "#D1EBFF";
    // prompt message area.
    this.htmlDiv = document.createElement("DIV");
    this.htmlDiv.style.padding = "20px 20px";
    // add to the content area.
    this.contentArea.appendChild(this.htmlDiv);

    // the botton area.
    this.buttonDIV = document.createElement('div');
    this.buttonDIV.setAttribute("align", "center");
    this.buttonDIV.style.margin = "10px 0px 0px 0px";
    // add to the content area.
    this.contentArea.appendChild(this.buttonDIV);

    // the OK button.
    var okHref = document.createElement("a");
    okHref.setAttribute("href", "JavaScript: function(){return;}");
    this.okButton = document.createElement("img");
    this.okButton.setAttribute("border", "0");
    this.okButton.src = this.okImgFile;
    this.okButton.style.margin = "5px 5px";
    this.okButton.linkId = 1;
    this.okButton.parentDialog = this;
    this.okButton.onclick = this.clickButton;
    okHref.appendChild(this.okButton);
    this.buttonDIV.appendChild(okHref);
}

/**
 * set the content for the html viewer dialog.
 */
filogix.gadget.UserAdminDialog.prototype.setPrompt =
    function(htmlContent) {

    this.htmlDiv.innerHTML = htmlContent;
}

/**
 * handle the click button event.
 */
filogix.gadget.UserAdminDialog.prototype.clickButton = function(event) {

    if (!event) event = window.event;

    var theSource = event.target ? event.target : event.srcElement;
    var linkId = theSource.linkId;
    if (theSource.parentDialog) {
        switch (linkId) {
        case 1:
            // handle ok.
            theSource.parentDialog.hide(true);
            break;
        case 2:
            // hancle cancel.
            theSource.parentDialog.hide();
            break;
        }
        return false;
    }
    return false;
}

filogix.gadget.UserAdminDialog.prototype.hide = function(ok) {

    filogix.gadget.UserAdminDialog.superclass.hide.call(this, ok);
}

filogix.gadget.UserAdminDialog.prototype.show = function() {

    filogix.gadget.UserAdminDialog.superclass.show.call(this);
    this.okButton.focus();
}


/**
 * class UserAdminDialog END
 ******************************************************************************/



filogix.express.service.UserAdminProcessor = function(){

    this.requestType = "express.service.UserAdminRequest";
    this.userLoginId = "";
}


filogix.express.service.UserAdminProcessor.prototype = {

    sendUserAdminRequest:function() {
        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
        // preparing the request object, set values
        var request = new Object();

        request.rcRequestContent = new Object();
        request.rcRequestContent.userLoginId = this.userLoginId;
                
        // send out the request.
        xmlhttp.send("requestType=" + this.requestType +
                     "&requestContent=" + request.toJSONString());
                
        var resp = xmlhttp.responseText.parseJSON().rcResponseContent;
        
        return resp;
    }
}


/*
 * check this userlogin exist in the instance
 */
function checkLoginId()
{
    var userAdmin = new filogix.express.service.UserAdminProcessor();
    var hdLogin = document.getElementsByName("pgUserAdministration_hdLoginId")[0].value;
    var userName = document.getElementsByName("pgUserAdministration_cbUserNames")[0];
    userAdmin.userLoginId = getTbLoginId().value;
    userAdmin.institutionid = getTbInstitutionId().value;
    
    if ( userName==null) {
        var result = userAdmin.sendUserAdminRequest();
       
        if(result.userFound) {
            performUserAdmin(result);
        }
    }
    else if( userName.value.length > 0 &&  hdLogin != getTbLoginId().value) {
        var result = userAdmin.sendUserAdminRequest();
        if(result.userFound) {
            performUserAdmin(result);
        }
    }
}

/*
 * get userlogin
 */
function getTbLoginId() {

    return document.getElementById("tbLoginId");
}

/*
 *  generate dialog popup
 */
function performUserAdmin(result) {

    var selectionDlg = new filogix.gadget.UserAdminDialog();
    selectionDlg.setTitle("User Administration"); 

    if (result.hardStop) {
        getTbLoginId().value = "";
        var promptStr = "Please enter a different login ID:";
        promptStr += "<table border=0 width=100%>";
        promptStr += "<br><tr>";
        promptStr += "<td colspan=2><img src='../images/dark_bl.gif' width=100% height=1 border=0></td>";
        promptStr += "</tr>";
        promptStr += "<tr>";
        promptStr += "<td valign=top><img src='../images/critical.gif' width=15 height=15 border=0></td>";
        promptStr += "<td valign=top><font size=2>"
        if(result.enterPrise) {
        	promptStr += "User LoginID already exists for this institution.";
        }
        else {
        	promptStr += "User LoginID already exists in the system.";
        }
        promptStr += "</td></tr>";
        promptStr += "<tr>";
        promptStr += "<td colspan=2><img src='../images/dark_bl.gif' width=100% height=1 border=0></td>";
        promptStr += "</tr>";
        promptStr += "</table>";
    }
    else {
        var userIds = result.userIds;
        var promptStr = "The Following Login ID(s) already exists in the System:";
        promptStr += "<table border=0 width=100%>";
        promptStr += "<br><tr>";
        promptStr += "<td colspan=2><img src='../images/dark_bl.gif' width=100% height=1 border=0></td>";
        promptStr += "</tr>";
        for (var j = 0; j < userIds.length; j ++) {
            promptStr += "<tr>";
            promptStr += "<td valign=top><img src='../images/warning.gif' width=15 height=15 border=0></td>";
            var userName = userIds[j].userName;
            var loginId = userIds[j].loginId;
            promptStr += "<td valign=top><font size=2>" + loginId + " " + userName;
            var jobTitle = userIds[j].jobTitle;
            promptStr += " - " + jobTitle;
            var institutionName = userIds[j].institutionName;
            promptStr += " - " + institutionName;
            promptStr += "</td>";
            promptStr += "</tr>";
            promptStr += "<tr>";
            promptStr += "<td colspan=2><img src='../images/dark_bl.gif' width=100% height=1 border=0></td>";
            promptStr += "</tr>";
        }
        promptStr += "</table>";
    }
    
    selectionDlg.setPrompt(promptStr);
    selectionDlg.show();
    selectionDlg.moveTo(-1, -1);
    filogix.gadget.DialogHelper.activeDialog = selectionDlg;
}


