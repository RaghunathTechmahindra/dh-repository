filogix.gadget.DialogBase.imagePath = "../JavaScript/rc/images/";
// the message keys.
filogix.util.RichClientMessages.msgKeys = new Array();
filogix.util.RichClientMessages.msgKeys =
    //["SERVICE_AIGUG_TITLE", "SERVICE_AIGUG_PROMPT", "CHOOSE_PAGE"]; //FXP23889, deleted, Jan 19 2009
    ["SERVICE_AIGUG_TITLE", "SERVICE_AIGUG_PROMPT", "CHOOSE_PAGE", "SERVICE_MI_TITLE", "CHOOSE_PAGE"]; //FXP23889, added, Jan 19 2009
    
var AIGUG_CANCELATION_SUBMITTED = false;

/**
 * returns the hidden dealid.
 */
function getDealId() {

    return document.getElementById("hdDealId").value;
}

/**
 * returns the hidden copyid.
 */
function getCopyId() {

    return document.getElementById("hdDealCopyId").value;
}

/**
 * returns the hidden property id.
 */
function getPropertyId() {

    return document.getElementById("hdPropertyId").value;
}

/**
 * set copy id.
 */
function setCopyId(newId) {

    document.getElementById("hdDealCopyId").value = newId;
}

function getDealStatusId() {
    return document.getElementsByName("pgMortgageIns_hdDealStatusId")[0].value;
}

function getMIStatusId() {
    var miStatuses = document.getElementById("cbMIStatus");
    return miStatuses.options[miStatuses.options.selectedIndex].value;
}
    

/**************************************
 * class AIGUGOrderRequest
 */

filogix.express.service.AIGUGOrderRequest = function() {
    
    // setting up the request type.
    this.requestType = "express.service.AIGUGOrderRequest";
};

filogix.express.service.AIGUGOrderRequest.prototype =
    new filogix.express.service.XHRServiceRequest();
filogix.express.service.AIGUGOrderRequest.superclass =
    filogix.express.service.XHRServiceRequest.prototype;
    
    
/**
 * update the transaction copy id.
 */
filogix.express.service.AIGUGOrderRequest.prototype.updateTransactionCopyId =
    function(newId) {

    // update the DOM of current page.
    setCopyId(newId);
}

/**
 * handle success response.
 */
filogix.express.service.AIGUGOrderRequest.prototype.handleSuccess =
    function(responseObj) {
    // updateing the dom.   
    var miResponse = document.getElementById("miResponseArea");
    var miPremiumAmount = document.getElementById("txMIPremium");
    var miStatusMsg = document.getElementById("cbMIStatus");
    var miPolicyNumber = document.getElementById("miPolicyNumber");
    var mICertificate = document.getElementById("txMIPolicyNo");
    
    if(responseObj.rcResponseContent.miResponse) {
        miResponse.value = responseObj.rcResponseContent.miResponse;
    }
    if(responseObj.rcResponseContent.miPremiumAmount) {
        miPremiumAmount.value = responseObj.rcResponseContent.miPremiumAmount;
    }
    if(responseObj.rcResponseContent.miStatusId) {
        var option = miStatusMsg.options[responseObj.rcResponseContent.miStatusId];
        option.selected = true;
    }
    if(responseObj.rcResponseContent.miPolicyNumber) {
        miPolicyNumber.value = responseObj.rcResponseContent.miPolicyNumber;
    }
    if(responseObj.rcResponseContent.miPolicyNumAIGUG) {
        mICertificate.value = responseObj.rcResponseContent.miPolicyNumAIGUG;
    }
    
    // call the super class mehtod to close the progress bar.
    filogix.express.service.AIGUGOrderRequest.
        superclass.handleSuccess.call(this, responseObj);
        
//    document.forms[0].submit();
        
//    var img = document.getElementsByName("pgMortgageIns_btCancel")[0];
//    img.clicked;
    
//    tool_click(5);

    var dlg = filogix.gadget.DialogHelper.activeDialog;
    var button = dlg.buttonDIV.getElementsByTagName("BUTTON")[0];
    button.onclick = function() {
       if(AIGUG_COLLAPSED_CANCELATION == true) { 
           AIGUG_COLLAPSED_CANCELATION == false
            dlg.hide();
            return(false);
       }
       document.forms[0].submit();
//  setSubmitFlag(true);
//      button.onclick = filogix.gadget.AlertDialogBox.clickButton;

    }
    
//  dlg.buttonDIV.getElementsByTagName("BUTTON")[0].onclick = tool_click(5);
}

filogix.express.service.AIGUGOrderRequest.prototype.handleTimeout =
    function() {
        filogix.express.service.AIGUGOrderRequest.
            superclass.handleTimeout.call(this);
            
        var dlg = filogix.gadget.DialogHelper.activeDialog;
        var button = dlg.buttonDIV.getElementsByTagName("BUTTON")[0];
        button.onclick = function() {
            if(AIGUG_COLLAPSED_CANCELATION == true) { 
               AIGUG_COLLAPSED_CANCELATION == false
               dlg.hide();
               return(false);
           }
           document.forms[0].submit();

        }
}


filogix.express.service.AIGUGOrderRequest.prototype.handleError =
    function(responseObj) {
    filogix.express.service.AIGUGOrderRequest.
            superclass.handleError.call(this, responseObj);
            
        var dlg = filogix.gadget.DialogHelper.activeDialog;
        var button = dlg.buttonDIV.getElementsByTagName("BUTTON")[0];
            button.onclick = function() {
                if(AIGUG_COLLAPSED_CANCELATION == true) { 
                    AIGUG_COLLAPSED_CANCELATION == false
                    dlg.hide();
                    return(false);
                }
                document.forms[0].submit();
        }   
}


filogix.express.service.AIGUGOrderRequest.prototype.handleHttpError =
    function(responseObj) {
        filogix.express.service.AIGUGOrderRequest.
            superclass.handleHttpError.call(this, responseObj);
            
        var dlg = filogix.gadget.DialogHelper.activeDialog;
        var button = dlg.buttonDIV.getElementsByTagName("BUTTON")[0];
            button.onclick = function() {
            if(AIGUG_COLLAPSED_CANCELATION == true) { 
               AIGUG_COLLAPSED_CANCELATION == false
               dlg.hide();
               return(false);
           }
            document.forms[0].submit();
        }   
}

filogix.express.service.AIGUGOrderRequest.prototype.reloadPage=function() {
    window.location.reload(true);
}

filogix.express.service.AIGUGOrderRequest.prototype.updatePageDOM =
    function(responseObj) {
/*
 * The dom updating is perdormed now in handleSuccess function right
 * before hiding the progress bur and show the success message.
 */     
//    var miResponse = document.getElementById("miResponseArea");
//    var miPremiumAmount = document.getElementById("txMIPremium");
//    var miStatusMsg = document.getElementById("cbMIStatus");
//    var miPolicyNumber = document.getElementById("miPolicyNumber");
//    
//    if(responseObj.rcResponseContent.miResponse) {
//      miResponse.innerHTML = responseObj.rcResponseContent.miResponse;
//    }
//   if(responseObj.rcResponseContent.miPremiumAmount) {
//      miPremiumAmount.innerHTML = responseObj.rcResponseContent.miPremiumAmount;
//    }
//    if(responseObj.rcResponseContent.miStatusId) {
//      var option = miStatusMsg.options[responseObj.rcResponseContent.setMiStatusId];
//      option.selected = true;
//    }
//    if(responseObj.rcResponseContent.miPolicyNumber) {
//      miPolicyNumber.innerHTML = responseObj.rcResponseContent.miPolicyNumber;
//    }
}

/**
 * class AIGUGOrderRequest END
 **************************************/  
 
 
 /**
 * AVM service request.
 */
function sendAIGUGRequest() {
//  var keyPressed = window.event.keyCode;
    
    // do not send the request if any of keyboard buttons was clicked
//  alert("SERGHEI:: sendAIGUGRequest:: keyPressed: " + keyPressed);
//  if(keyPressed == 0) {
        var aigugRequest = new filogix.express.service.AIGUGOrderRequest();
        
        var messages = new filogix.util.RichClientMessages();
        aigugRequest.dialogTitle = messages.getMessage("SERVICE_AIGUG_TITLE") + " - Express";
//      aigugRequest.dialogPromptMsg = messages.getMessage("SERVICE_AIGUG_PROMPT", null);
        aigugRequest.dialogPromptMsg = "";
        // prepare the request object here.
        var request = new Object();
        request.rcRequestContent = new Object();
        
        // deal id,
        request.rcRequestContent.dealId = getDealId();
        request.rcRequestContent.copyId = getCopyId();
        
        var insurers = document.getElementById("cbMIInsurer");
        var insurer = insurers.options[insurers.options.selectedIndex].value;
        request.rcRequestContent.productId = insurer;
        
        request.rcRequestContent.dealStatusId = getDealStatusId();
        request.rcRequestContent.miStatusId = getMIStatusId();
        // property id.
    //    request.rcRequestContent.propertyId = getPropertyId();
        // set the requestId if exist one!
        request.rcRequestContent.requestId = -1;
        request.rcRequestContent.languageId = 0;
        aigugRequest.requestContent = request;
        aigugRequest.send();
//  }
} 
