(function() {

    /**
     * Constructor
     */
    var SimpleXHRService = function() {
        //default 90 seconds
        this.timeoutTimeMillSec = 90000;
        //this.requestType = "child has to overwrite";
        //this.requestContent = "child has to overwrite";
        this.isTimeout = false;
        // this.SimpleXHRService
    };
    /**
     * methods in prototype
     */
    SimpleXHRService.prototype = {

        /**
         * start ajax request sequence
         */
        execute : function() {

            var self = this;
            try {
                this.showMainDialog();

                if(!this.xmlhttp) {
                    this.xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject(true, null);
                }

                this.startTimer();

                // call back function
                this.xmlhttp.onreadystatechange = function() {
                    if(self.xmlhttp.readyState == 4 && self.isTimeout == false) {

                        self.stopTimer();
                        self.hideMainDialog();

                        var respObj = self.createResponseObject(self.xmlhttp);
                        if(self.xmlhttp.status == 200) {
                            self.handleHttpResponse(respObj);
                        } else {
                            self.handleHttpError(respObj);
                        }
                        self.xmlhttp = null;
                    }
                };

                this.xmlhttp.send("requestType=" + this.requestType + "&requestContent=" + this.requestContent.toJSONString());

            } catch (e) {
                this.hideMainDialog();
                this.stopTimer();
                this.handleRuntimeError(e);
                ajaxLogger.error(e);
            }
        },
        /**
         * show main dialog box
         */
        showMainDialog : function() {

            this.dialog = this.createMainDialog();
            filogix.gadget.DialogHelper.activeDialog = this.dialog;
            this.dialog.show();
            this.dialog.moveTo(-1, -1);
        },
        /**
         * hide main dialog box
         */
        hideMainDialog : function() {
            if(this.dialog && this.dialog.hide)
                this.dialog.hide();
        },
        /**
         * start timer for the timeout in client side
         */
        startTimer : function() {
            var self = this;
            self.timeoutTimeMillSec = self.timeoutTimeMillSec || 90000;

            this.timeOutTimer = window.setTimeout(function() {
                self.handleTimeout();
            }, self.timeoutTimeMillSec);
            this.isTimeout = false;
        },
        /**
         * stop timer for the timeout in client side
         */
        stopTimer : function() {
            if(this.timeOutTimer) {
                window.clearTimeout(this.timeOutTimer);
                this.timeOutTimer = null;
            }
        },
        /**
         * create response object from xhr object
         */
        createResponseObject : function(obj) {

            var respObj = {};
            respObj.statusText = obj.statusText;
            respObj.responseText = obj.responseText;
            respObj.responseXML = obj.responseXML;

            return respObj;
        },
        /**
         * executed when successful response arrives 
         */
        handleHttpResponse : function(responseObj) {

            var obj = responseObj.responseText.parseJSON();
            if(obj.rcResponseStatus === filogix.express.service.SimpleXHRService.STATUS_RESPONSE_SUCCESS) {
                this.handleSuccessReceived(obj);
            } else {
                this.handleFailureReceived(obj);
            }
        },
        /**
         * executed when time-out happens within javascript (default 90 seconds)
         */
        handleTimeout : function() {
            this.isTimeout = true;
            if(this.xmlhttp)
                this.xmlhttp.abort();
            this.handleRuntimeError(new Error("Timeout in Javascript"));
        },
        
        /***************************************************************************
         * child class has to override methods after here
         **************************************************************************/
        createMainDialog : function() {
        },
        handleFailureReceived : function(responseObj) {
        },
        handleSuccessReceived : function(responseObj) {
        },
        handleHttpError : function(responseObj) {
        },
        handleRuntimeError : function(err) {
        }
    };

    //expose to the global scope
    filogix.express.service.SimpleXHRService = SimpleXHRService;
    filogix.express.service.SimpleXHRService.STATUS_RESPONSE_FAILURE = 500;
    filogix.express.service.SimpleXHRService.STATUS_RESPONSE_SUCCESS = 600;

})();
