(function() {

    var msgBox = filogix.msgBox;

    var alertCallback = function() {
        IsSubmitButton();
        document.forms[0].submit();
        //just submit HTTP request. this is not ajax way...
    };
    var AIGUGOrderRequest = function(cancel) {
        this.requestType = "express.service.AIGUGOrderRequest";
        this.timeoutTimeMillSec = 90000;
        this.miProviderName;
    };
    //inherit from SimpleXHRService
    AIGUGOrderRequest.prototype = new filogix.express.service.SimpleXHRService();

    AIGUGOrderRequest.prototype.handleRuntimeError = function(err) {
    
        msgBox.alert("#MI_GENERIC_REQUEST_FAILURE_TITLE#", "#MI_GENERIC_SYSTEM_ERROR_MSG#", alertCallback);
    };

    AIGUGOrderRequest.prototype.handleHttpError = function(responseObj) {

        msgBox.alert("#SYSTEM_ERROR_TITLE#", "#HTTP_ERROR_MESSAGE#", alertCallback);
    };

    AIGUGOrderRequest.prototype.handleFailureReceived = function(responseObj) {

        msgBox.alert("#SYSTEM_ERROR_TITLE#", "#MI_ERROR_SYSTEM_ANAVAILABLE#", alertCallback);
    };

    AIGUGOrderRequest.prototype.handleSuccessReceived = function(responseObj) {
            
        msgBox.alert("#SYSTEM_SUCCESS_TITLE#", "#SYSTEM_SUCCESS_MESSAGE#", alertCallback);
    };

    AIGUGOrderRequest.prototype.createMainDialog = function() {

        var dialog = new filogix.gadget.SimplePreloaderDialog();

        dialog.setTitle(msgBox.getClientMsg("MI_GENERIC_REQUEST_SENDING_TITLE", [this.miProviderName]));
        dialog.setPromptMsg(msgBox.getClientMsg("MI_GENERIC_REQUEST_SENDING_MSG"));

        dialog.setPreloaderImage("preloader.gif");
        return dialog;

    };
    
    //expose to the global scope
    filogix.express.service.AIGUGOrderRequest = AIGUGOrderRequest;

})();
