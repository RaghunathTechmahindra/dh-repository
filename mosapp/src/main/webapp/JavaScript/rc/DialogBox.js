(function() {

    //dialog window handle
    var dialog;
    //simple rich client message object cache
    var clientMsg;

    /**
     * get RichClientMessage value for the key if it is enclosed with '#'
     *
     * @param args can be one of followings.
     * 1. normal string e.g. "Some string" => "Some string"
     * 2. resource bundle key (put '#' head and tail) e.g. "#MSG_KEY# => "piece of cake - {0} {1}"
     * 3. resource bundle key + parameters in array e.g. ["#MSG_KEY#", "ABC", "XYZ"] => "piece of cake ABC DEF"
     * if one of params is enclosed with '#', it's also transformed
     */
    function format(args) {

        if(!args)
            return "";

        var key;
        var params = null;
        var type = typeof args;
        
        if(type == "string") {
            key = args;
        } else if(type == "object") {
            key = args[0];
            if(args.length > 1) {
                params = args.slice(1);
                if(params) {
                    for(var i in params) {
                        params[i] = valueFor(params[i]);
                    }
                }
            }
        }

        return valueFor(key, params);
    }

    /**
     * check if the key is supposed to be transformed.
     * if so, it gets client message
     */
    function valueFor(key, params) {
        var matched = (/^#(.*)#$/.exec(key) || [])[1];
        if(matched) {
            return getClientMsg(matched, params, false); //it doesn't escape message
        } else {
            return key;
        }
    }

    /**
     * get translation from RichClient message
     */
    function getClientMsg(key, params, escape) {
    	
        if(!clientMsg) {
            clientMsg = new filogix.util.SimpleClientMessage();
        }
        return clientMsg.getMessage(key, params, escape);
    }

    /**
     * close shown dialog box
     */
    function closeDialog() {
        if(dialog) {
            if(dialog.hide)
                dialog.hide();
            dialog = null;
        }
    }

    /**
     * this closure function will execute given call back function
     * and after that, closes shown dialog box
     */
    function autoClose(func) {
        return function() {
            if(func) {
                func();
            }
            closeDialog();
        };
    }

    /**
     * base of message box
     */
    function baseMsgBox(title, msg, buttons) {

        closeDialog();
        dialog = new filogix.gadget.SimpleAlertDialogBox();
        dialog.setTitle(format(title));
        dialog.setContent(format(msg));

        if( typeof buttons == "object") {
            for(var i = 0; i < buttons.length; i++) {
                dialog.addButton(format(buttons[i].label), autoClose(buttons[i].callback));
            }
        } else if( typeof buttons == "string") {
            dialog.addButton(format(buttons), autoClose());
        }

        dialog.show();
        dialog.moveTo(-1, -1);
    }

    /**
     * simple alert message box
     * title, message, OK button will be translated
     */
    function alert(title, msg, callback) {
        baseMsgBox(title, msg, [{
            label : "#LABEL_OK#",
            callback : callback
        }]);
    }

    /**
     * simple confirm message box
     * title, message, OK button, Cancel button will be translated
     */
    function confirm(title, msg, okCallback, cancelCallback) {

        baseMsgBox(title, msg, [{
            label : "#LABEL_OK#",
            callback : okCallback
        }, {
            label : "#LABEL_CANCEL#",
            callback : cancelCallback
        }]);
    }

    // expose to the global object
    filogix.msgBox = {};
    filogix.msgBox.alert = alert;
    filogix.msgBox.confirm = confirm;
    filogix.msgBox.baseMsgBox = baseMsgBox;
    filogix.msgBox.getClientMsg = getClientMsg;
    filogix.msgBox.closeDialog = closeDialog;

})();
