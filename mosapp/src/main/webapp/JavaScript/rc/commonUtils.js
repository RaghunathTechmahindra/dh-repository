//anonimous function
(function(){
    /**
     * utility method. returns array of objects which has class attribute as
     * <serchClass>
     * 
     * @param searchClass :
     *            class name to find.
     * @param tag :
     *            tag name to nallow down serch criteria. default: '*'
     * @param node :
     *            node to find. default: document.
     */
    function getElementsByClass(searchClass, tag /* optional */, node /* optional */) {
        var classElements = new Array();
        node = node ? node : document;
        tag = tag ? tag : '*';
        var els = node.getElementsByTagName(tag);
        var elsLen = els.length;
        var pattern = new RegExp('.*' + searchClass + '.*');
        for (i = 0, j = 0; i < elsLen; i++) {
            if (pattern.test(els[i].className)) {
                classElements[j] = els[i];
                j++;
            }
        }
        return classElements;
    }

    //expose to the global object
    filogix.util.getElementsByClass = getElementsByClass;

})(); //execute immediately


