/*
 * DialogBase.js - DHTML modal Dialog box base.
 * 
 * @version 
 */

/**
 * DialogBase Constructor.
 */
DialogBase = function(isModal, autoClose) {

    if (arguments.length == 0) {
        return;
    }

    this.init(isModal, autoClose);
}

/**
 * static variable for DialogBase.
 */
DialogBase.imagePath = "images/";
DialogBase.className = "DialogBase";
DialogBase.closeIcon = null;
DialogBase.veilOverlay = null;
DialogBase.veilZ = 1000;

/**
 * the init method for...
 */
DialogBase.prototype.init = function(isModal, autoClose) {

    this.isModal = isModal;
    this.autoClose = autoClose;

    //There are some static variables.
    if (DialogBase.veilOverlay == null) {
        // once per page.?

        DialogBase.veilOverlay = document.createElement("div");
        DialogBase.veilOverlay.className = "dialogBaseVeil";
        DialogBase.veilOverlay.style.zIndex = DialogBase.veilZ;
        DialogBase.veilOverlay.innerHTML = "&nbsp;";
        DialogBase.setVeilSize();
        DialogBase.addListener(window, "resize", DialogBase.setVeilSize);
        document.body.appendChild(DialogBase.veilOverlay);

        // the close icon for this dialog.
        // TODO: make it can be set by the application.
        DialogBase.closeIcon = document.createElement("img");
        DialogBase.closeIcon.src = DialogBase.imagePath + "window_close.gif";
    }

    // setting the container of this dialog, it is a div.
    this.container = document.createElement("div");
    // TODO: the CSS style class name.
    this.container.className = DialogBase.className;
    this.container.dialogBox = this;

    // begine draw the dialog, it is a table.
    var dialogTable = document.createElement("table");
    dialogTable.setAttribute("cellSpacing", "0");
    dialogTable.setAttribute("cellPadding", "0");
    dialogTable.setAttribute("border", "0");
    var dialogTbody = document.createElement("tbody");

    // this row is for the title bar of the dialog.
    var dialogTr = document.createElement("tr");
    var dialogTd = document.createElement("td");

    // the title bar of the dialog, it is also a table.
    var dlgTitleTable = document.createElement("table");
    dlgTitleTable.setAttribute("cellSpacing", "0");
    dlgTitleTable.setAttribute("cellPadding", "0");
    dlgTitleTable.setAttribute("border", "0");
    dlgTitleTable.setAttribute("width", "100%");
    var dlgTitleTbody = document.createElement("tbody");
    var dlgTitleTr = document.createElement("tr");
    // title has 3 cells: left, title, right.
    // -- left cell is nothing for now.
    var dlgTitleTdLeft = document.createElement("td");
    dlgTitleTdLeft.className = "dlgTitleTdLeft";
    dlgTitleTr.appendChild(dlgTitleTdLeft);
    // -- title cell contains the title of this dialog.
    // ------ make the title cell public, we will set the title on the fly.
    this.dlgTitleTdTitle = document.createElement("td");
    this.dlgTitleTdTitle.className = "dlgTitleTdTitle";
    dlgTitleTr.appendChild(this.dlgTitleTdTitle);
    // -- right cell contains the close icon of this dialog.
    var dlgTitleTdRight = document.createElement("td");
    dlgTitleTdRight.className = "dlgTitleTdRight";
    if (this.autoClose) {
        // ------ preparing the close icon.
        this.closeIcon = document.createElement("img");
        this.closeIcon.src = DialogBase.closeIcon.src;
        this.closeIcon.setAttribute("border", "0");
    
        // ------ preparing the link for the close icon.
        var closeIconLink = document.createElement("a");
        closeIconLink.setAttribute("href", "#");
        closeIconLink.appendChild(this.closeIcon);
        closeIconLink.dialogBox = this;
        closeIconLink.onclick = DialogBase.closeBox;
        dlgTitleTdRight.appendChild(closeIconLink);
    }
    dlgTitleTr.appendChild(dlgTitleTdRight);
    // ...
    dlgTitleTbody.appendChild(dlgTitleTr);
    dlgTitleTable.appendChild(dlgTitleTbody);

    // add the title bar to the dialog.
    dialogTd.appendChild(dlgTitleTable);
    dialogTr.appendChild(dialogTd);
    dialogTbody.appendChild(dialogTr);

    // one more row for the content of the dialog.
    dialogTr = document.createElement("tr");
    dialogTd = document.createElement("td");
    dialogTd.className = "dialogMainPanel";
    // the content area is div, make is public.
    this.contentArea = document.createElement("div");
    this.contentArea.className = "dialogContentArea";
    dialogTd.appendChild(this.contentArea);
    dialogTr.appendChild(dialogTd);
    dialogTbody.appendChild(dialogTr);

    dialogTable.appendChild(dialogTbody);
    this.container.appendChild(dialogTable);
    document.body.appendChild(this.container);

    // make the dialog can be drag and drop.
    Drag.init(this.dlgTitleTdTitle, this.container, 0, null, 0);
}

/**
 * show this dialog.
 */
DialogBase.prototype.show = function() {

    // hide the select boxes.
    hideSelectElements(true);
    // set the container div to block.
    this.container.style.display = "block";
    // make sure the z index is the toppest.
    this.container.style.zIndex = DialogBase.veilZ + 1;
    // set the position of the dialog.
    centerOnScreen(this.container);
    if (this.isModal) {
        DialogBase.veilOverlay.style.display = "block";
    }
}

/**
 * hide the dialog.
 */
DialogBase.prototype.hide = function(ok) {

    this.container.style.display = "none";
    if (this.isModal) {
        DialogBase.veilOverlay.style.display = "none";
    }
    // show the select boxes.
    hideSelectElements(false);

    // TODO handle multiple dialog opening.

    if (ok) {
        if (this.callOk) {
            if (this.returnData) {
                this.callOk(this.returnData);
            } else {
                this.callOk();
            }
        }
    } else if (this.callCancel) {
        this.callCancel();
    }
}

/**
 * move to the new position.
 */
DialogBase.prototype.moveTo = function(x, y) {

    //alert("body.clientWidth: " + document.body.clientWidth + "\n" +
    //      "body.clientHeight: " + document.body.clientHeight);

    if (x == -1) {
        // move to the center of the x
        x = Math.round((document.body.clientWidth -
                        this.container.offsetWidth) / 2);
    }
    if (y == -1) {
        // move to the center of the y.
        y = Math.round((document.body.clientHeight -
                        this.container.offsetHeight) / 2 +
                       document.body.scrollTop);
    }

    //alert("left: " + x + "\n" +
    //      "right: " + y);
    this.container.style.left = x + "px";
    this.container.style.top = y + "px";
}

/**
 * set the title for the dialog.
 */
DialogBase.prototype.setTitle = function(title) {

    // it is a td.
    this.dlgTitleTdTitle.innerHTML = title;
}

/**
 * set the content of the dialog.
 */
DialogBase.prototype.setContent = function(htmlContent) {

    // it is a div.
    this.contentArea.innerHTML = htmlContent;
}

/**
 * set the width of the dialog.
 */
DialogBase.prototype.setWidth = function(width) {

    // TODO: should use the container?
    this.contentArea.style.width = width + "px";
}

/**
 * set the method to handle ok click.
 */
DialogBase.prototype.setCallOk = function(callOk) {

    this.callOk = callOk;
}

/**
 * set the method to handle cancel click.
 */
DialogBase.prototype.setCallCancel = function(callCancel) {

    this.callCancel = callCancel;
}

/**
 * the close box method.
 */
DialogBase.closeBox = function(e) {

    // check for different browser.
    if (!e) {
        e = window.event;
    }
    var theSource = e.target ? e.target : e.srcElement;
    var count = 0;
    while ((theSource != null) && (count < 3)) {
        if (theSource.dialogBox) {
            //alert("find the dialog box: " + count);
            theSource.dialogBox.hide();
            return false;
        }
        theSource = theSource.parentNode;
        count ++;
    }
    return false;
}

// some class method.

/**
 * set the veil overlay's size.
 */
DialogBase.setVeilSize = function () {

    DialogBase.veilOverlay.style.width = document.body.scrollWidth;
    DialogBase.veilOverlay.style.height = document.body.scrollHeight;
    //moveTo(-1, -1);
}

/**
 * add listener to the specified object.
 */
DialogBase.addListener = function(obj, eventType, func) {

    if (obj.addEventListener) {
        obj.addEventListener(eventType, func, false);
        return true;
    } else if (obj.attachEvent) {
        return obj.attachEvent("on" + eventType, func);
    } else {
        return false;
    }
}

// some util functions.

/**
 * setting the position for the given object on the center of the screen.
 */
function centerOnScreen(object) {

    var position = getOffsetPoint(object);
    var centerX = position.x;
    var centerY = position.y;
    // caculating...
    if ((position.x + object.offsetWidth - document.body.scrollLeft) >
        document.body.clientWidth) {
        centerX = document.body.scrollLeft + document.body.clientWidth -
            object.offsetWidth;
    }
    if (position.x < document.body.scrollLeft) {
        centerX = document.body.scrollLeft;
    }
    if ((position.y + object.offsetHeight - document.body.scrollTop) >
        document.body.clientHeight) {
        centerY = document.body.scrollTop + document.body.clientHeight -
            object.offsetHeight;
    }
    if (position.y < document.body.scrollTop) {
        centerY = document.body.scrollTop;
    }

    // move to center.
    if ((centerX != position.x) || (centerY != position.y)) {
        moveToPoint(object, centerX, centerY);
    }
}

/**
 * the point class cotains a the x,y 
 */
function Point(x, y) {
    this.x = x;
    this.y = y;
}

/**
 * return the given object's offset as a Point.
 */
function getOffsetPoint(object) {

    var posX = object.offsetLeft;
    var posY = object.offsetTop;
    var posParent = object.offsetParent;
    while (posParent != null) {
        posX += posParent.offsetLeft;
        posY += posParent.offsetTop;
        posParent = posParent.offsetParent;
    }

    return new Point(posX, posY);
}

/**
 * move the given object to the specified point.
 */
function moveToPoint(obj, newX, newY) {

    obj.style.left = newX + "px";
    obj.style.top = newY + "px";
}

/**
 * utility class for hiding/showing all select box in current dom document.
 */
function hideSelectElements(hidden) {
    var selectElements = document.getElementsByTagName("select");
    if (selectElements.length < 1) {
        return;
    }

    for (var i = 0; i < selectElements.length; i ++) {
        if (hidden) {
            selectElements[i].style.visibility = "hidden";
        } else {
            selectElements[i].style.visibility = "visible";
        }
    }
}
