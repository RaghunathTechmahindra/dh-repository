/*******************************************************************************
 * class DialogBase.
 */
/**
 * DialogBase Constructor.
 */
filogix.gadget.DialogBase = function(isModal, autoClose) {

    if (arguments.length == 0) {
        return;
    }

    this.init(isModal, autoClose);
};

/**
 * static variable for DialogBase.
 */
filogix.gadget.DialogBase.imagePath = "images/";
filogix.gadget.DialogBase.className = "DialogBase";
filogix.gadget.DialogBase.closeIcon = null;
filogix.gadget.DialogBase.veilOverlay = null;
filogix.gadget.DialogBase.veilZ = 1000;

/**
 * the init method for...
 */
filogix.gadget.DialogBase.prototype.init = function(isModal, autoClose) {

    this.isModal = isModal;
    this.autoClose = autoClose;

    // There are some static variables.
    if (filogix.gadget.DialogBase.veilOverlay == null) {
        // once per page.?

        filogix.gadget.DialogBase.veilOverlay = document.createElement("div");
        filogix.gadget.DialogBase.veilOverlay.className = "dialogBaseVeil";
        filogix.gadget.DialogBase.veilOverlay.style.zIndex =
            filogix.gadget.DialogBase.veilZ;
        filogix.gadget.DialogBase.veilOverlay.innerHTML = "&nbsp;";
        filogix.gadget.DialogBase.setVeilSize();
        filogix.gadget.DialogBase.
            addListener(window, "resize",
                        filogix.gadget.DialogBase.setVeilSize);
        document.body.appendChild(filogix.gadget.DialogBase.veilOverlay);

        // the close icon for this dialog.
        // TODO: make it can be set by the application.
        filogix.gadget.DialogBase.closeIcon = document.createElement("img");
        filogix.gadget.DialogBase.closeIcon.src =
            filogix.gadget.DialogBase.imagePath + "window_close.gif";
    }

    // setting the container of this dialog, it is a div.
    this.container = document.createElement("div");
    // TODO: the CSS style class name.
    this.container.className = filogix.gadget.DialogBase.className;
    this.container.dialogBox = this;

    // begine draw the dialog, it is a table.
    var dialogTable = document.createElement("table");
    dialogTable.setAttribute("cellSpacing", "0");
    dialogTable.setAttribute("cellPadding", "0");
    dialogTable.setAttribute("border", "0");
    var dialogTbody = document.createElement("tbody");

    // this row is for the title bar of the dialog.
    var dialogTr = document.createElement("tr");
    var dialogTd = document.createElement("td");

    // the title bar of the dialog, it is also a table.
    var dlgTitleTable = document.createElement("table");
    dlgTitleTable.setAttribute("cellSpacing", "0");
    dlgTitleTable.setAttribute("cellPadding", "0");
    dlgTitleTable.setAttribute("border", "0");
    dlgTitleTable.setAttribute("width", "100%");
    var dlgTitleTbody = document.createElement("tbody");
    var dlgTitleTr = document.createElement("tr");
    // title has 3 cells: left, title, right.
    // -- left cell is nothing for now.
    var dlgTitleTdLeft = document.createElement("td");
    dlgTitleTdLeft.className = "dlgTitleTdLeft";
    dlgTitleTr.appendChild(dlgTitleTdLeft);
    // -- title cell contains the title of this dialog.
    // ------ make the title cell public, we will set the title on the fly.
    this.dlgTitleTdTitle = document.createElement("td");
    this.dlgTitleTdTitle.className = "dlgTitleTdTitle";
    dlgTitleTr.appendChild(this.dlgTitleTdTitle);
    // -- right cell contains the close icon of this dialog.
    var dlgTitleTdRight = document.createElement("td");
    dlgTitleTdRight.className = "dlgTitleTdRight";
    if (this.autoClose) {
        // ------ preparing the close icon.
        this.closeIcon = document.createElement("img");
        this.closeIcon.src = filogix.gadget.DialogBase.closeIcon.src;
        this.closeIcon.setAttribute("border", "0");
    
        // ------ preparing the link for the close icon.
        var closeIconLink = document.createElement("a");
        closeIconLink.setAttribute("href", "#");
        closeIconLink.appendChild(this.closeIcon);
        closeIconLink.dialogBox = this;
        closeIconLink.onclick = filogix.gadget.DialogBase.closeBox;
        dlgTitleTdRight.appendChild(closeIconLink);
    }
    dlgTitleTr.appendChild(dlgTitleTdRight);
    // ...
    dlgTitleTbody.appendChild(dlgTitleTr);
    dlgTitleTable.appendChild(dlgTitleTbody);

    // add the title bar to the dialog.
    dialogTd.appendChild(dlgTitleTable);
    dialogTr.appendChild(dialogTd);
    dialogTbody.appendChild(dialogTr);

    // one more row for the content of the dialog.
    dialogTr = document.createElement("tr");
    dialogTd = document.createElement("td");
    dialogTd.className = "dialogMainPanel";
    // the content area is div, make is public.
    this.contentArea = document.createElement("div");
    this.contentArea.className = "dialogContentArea";
    dialogTd.appendChild(this.contentArea);
    dialogTr.appendChild(dialogTd);
    dialogTbody.appendChild(dialogTr);

    dialogTable.appendChild(dialogTbody);
    this.container.appendChild(dialogTable);
    document.body.appendChild(this.container);

    // make the dialog can be drag and drop.
    Drag.init(this.dlgTitleTdTitle, this.container, 0, null, 0);
};

/**
 * show this dialog.
 */
filogix.gadget.DialogBase.prototype.show = function() {

    // hide the select boxes.
    hideSelectElements(true);
    // set the container div to block.
    this.container.style.display = "block";
    // make sure the z index is the toppest.
    this.container.style.zIndex = filogix.gadget.DialogBase.veilZ + 1;
    // set the position of the dialog.
    centerOnScreen(this.container);
    if (this.isModal) {
        filogix.gadget.DialogBase.veilOverlay.style.display = "block";
    }
};

/**
 * hide the dialog.
 */
filogix.gadget.DialogBase.prototype.hide = function(ok) {

    this.container.style.display = "none";
    if (this.isModal) {
        filogix.gadget.DialogBase.veilOverlay.style.display = "none";
    }
    // show the select boxes.
    hideSelectElements(false);

    // clean the active dialog.
    filogix.gadget.DialogHelper.activeDialog = null;

    // TODO handle multiple dialog opening.

    if (ok) {
        if (this.callOk) {
            if (this.returnData) {
                this.callOk(this.returnData);
            } else {
                this.callOk();
            }
        }
    } else if (this.callCancel) {
        this.callCancel();
    }
};

/**
 * move to the new position.
 */
filogix.gadget.DialogBase.prototype.moveTo = function(x, y) {

    // alert("body.clientWidth: " + document.body.clientWidth + "\n" +
    // "body.clientHeight: " + document.body.clientHeight);

    if (x == -1) {
        // move to the center of the x
        x = Math.round((document.body.clientWidth -
                        this.container.offsetWidth) / 2);
    }
    if (y == -1) {
        // move to the center of the y.
        y = Math.round((document.body.clientHeight -
                        this.container.offsetHeight) / 2 +
                       document.body.scrollTop);
    }

    // alert("left: " + x + "\n" +
    // "right: " + y);
    this.container.style.left = x + "px";
    this.container.style.top = y + "px";
};

/**
 * set the title for the dialog.
 */
filogix.gadget.DialogBase.prototype.setTitle = function(title) {
    // it is a td.
    this.dlgTitleTdTitle.innerHTML = title;
};

/**
 * set the content of the dialog.
 */
filogix.gadget.DialogBase.prototype.setContent = function(htmlContent) {

    // it is a div.
    this.contentArea.innerHTML = htmlContent;
};

/**
 * set the width of the dialog.
 */
filogix.gadget.DialogBase.prototype.setWidth = function(width) {

    // TODO: should use the container?
    this.contentArea.style.width = width + "px";
};

/**
 * set the method to handle ok click.
 */
filogix.gadget.DialogBase.prototype.setCallOk = function(callOk) {

    this.callOk = callOk;
};

/**
 * set the method to handle cancel click.
 */
filogix.gadget.DialogBase.prototype.setCallCancel = function(callCancel) {

    this.callCancel = callCancel;
};

/**
 * the close box method.
 */
filogix.gadget.DialogBase.closeBox = function(e) {

    // check for different browser.
    if (!e) {
        e = window.event;
    }
    var theSource = e.target ? e.target : e.srcElement;
    var count = 0;
    while ((theSource != null) && (count < 3)) {
        if (theSource.dialogBox) {
            // alert("find the dialog box: " + count);
            theSource.dialogBox.hide();
            return false;
        }
        theSource = theSource.parentNode;
        count ++;
    }
    return false;
};

// some class method.

/**
 * set the veil overlay's size.
 */
filogix.gadget.DialogBase.setVeilSize = function () {

    filogix.gadget.DialogBase.veilOverlay.style.width =
        document.body.scrollWidth;
    filogix.gadget.DialogBase.veilOverlay.style.height =
        document.body.scrollHeight;
    // moveTo(-1, -1);
};

/**
 * add listener to the specified object.
 */
filogix.gadget.DialogBase.addListener = function(obj, eventType, func) {

    if (obj.addEventListener) {
        obj.addEventListener(eventType, func, false);
        return true;
    } else if (obj.attachEvent) {
        return obj.attachEvent("on" + eventType, func);
    } else {
        return false;
    }
};
/**
 * class DialogBase End.
 ******************************************************************************/


/*******************************************************************************
 * class DialogHelper
 */
filogix.gadget.DialogHelper = {

    // the reference to active dialog.
    activeDialog:null
};
/**
 * class DialogHelper END.
 ******************************************************************************/


/*******************************************************************************
 * utility functions to help handle the dialog behaviours.
 */
/**
 * setting the position for the given object on the center of the screen.
 */
function centerOnScreen(object) {

    var position = getOffsetPoint(object);
    var centerX = position.x;
    var centerY = position.y;
    // caculating...
    if ((position.x + object.offsetWidth - document.body.scrollLeft) >
        document.body.clientWidth) {
        centerX = document.body.scrollLeft + document.body.clientWidth -
            object.offsetWidth;
    }
    if (position.x < document.body.scrollLeft) {
        centerX = document.body.scrollLeft;
    }
    if ((position.y + object.offsetHeight - document.body.scrollTop) >
        document.body.clientHeight) {
        centerY = document.body.scrollTop + document.body.clientHeight -
            object.offsetHeight;
    }
    if (position.y < document.body.scrollTop) {
        centerY = document.body.scrollTop;
    }

    // move to center.
    if ((centerX != position.x) || (centerY != position.y)) {
        moveToPoint(object, centerX, centerY);
    }
}

/**
 * the point class cotains a the x,y
 */
function Point(x, y) {
    this.x = x;
    this.y = y;
}

/**
 * return the given object's offset as a Point.
 */
function getOffsetPoint(object) {

    var posX = object.offsetLeft;
    var posY = object.offsetTop;
    var posParent = object.offsetParent;
    while (posParent != null) {
        posX += posParent.offsetLeft;
        posY += posParent.offsetTop;
        posParent = posParent.offsetParent;
    }

    return new Point(posX, posY);
}

/**
 * move the given object to the specified point.
 */
function moveToPoint(obj, newX, newY) {

    obj.style.left = newX + "px";
    obj.style.top = newY + "px";
}


/**
 * utility class for hiding/showing all select box in current dom document.
 * FXP23668, MCM, Dec 8, 2008,changed to consider if the select was originaly visible. 
 */
function hideSelectElements(hidden) {
    var selectElements = document.getElementsByTagName("select");
    if (selectElements.length < 1) {
        return;
    }
    
    //when alert mode, just skip this method
    var pbody = document.getElementById("pagebody");
    if (pbody && isVisible(pbody) == false) return;

    for (var eachName in selectElements) {
        var obj = selectElements[eachName];
        if (typeof (obj) != "object") continue;
        if (obj.parentNode.name == "hidden")  continue;

        if (hidden == true) {
            //save current style sheet setting directory into the dom obj         
            if(isVisible(obj)){
                obj.style.visibility = "hidden";
                obj['expressVisibleFlag'] = true;
            }else{
            	obj['expressVisibleFlag'] = false;
            }
        } else {
            //only set 'visible' to the fileds that ware visible originally
            if (obj['expressVisibleFlag'] && obj['expressVisibleFlag'] == true)
                obj.style.visibility = "visible";
        }
    }
}

/**
 * check if the dom element is visible or not 
 * this method considers target element's ancestors.
 * @return true: visible 
 * FXP23668, MCM, Dec 8, 2008,added 
 */
function isVisible(obj) {
    if (obj == window.document) {
        return true;
    }
    if (obj && obj.currentStyle) {
        if (obj.currentStyle.display == "none") {
            return false;
        }
        if (obj.currentStyle.visibility == "hidden") {
            return false;
        }
    }
    //recursive call
    return isVisible(obj.parentNode);
}

/**
 * invoked by <body> tag's onresize, onscroll, on mousewheel events to reset the
 * active dialog's position to center of the screen.
 */
function resetPosition() {

    if (filogix.gadget.DialogHelper.activeDialog != null) {

        filogix.gadget.DialogHelper.activeDialog.moveTo(-1, -1);
    }
}
/*******************************************************************************
 * utility functions end.
 ******************************************************************************/


/*******************************************************************************
 * class AlertDialogBox.
 */

/**
 * the constructor.
 */
filogix.gadget.AlertDialogBox = function(icon) {

    if (arguments.length == 0) {
        return;
    }

    this.init(true, true, icon);
};

/**
 * setting up the inheritance.
 */
filogix.gadget.AlertDialogBox.prototype = new filogix.gadget.DialogBase();
filogix.gadget.AlertDialogBox.prototype.constructor =
    filogix.gadget.AlertDialogBox;
filogix.gadget.AlertDialogBox.superclass = filogix.gadget.DialogBase.prototype;

/**
 * init method.
 */
filogix.gadget.AlertDialogBox.prototype.init =
    function(isModal, autoClose, icon) {

    filogix.gadget.AlertDialogBox.superclass.init.call(this, isModal,
                                                       autoClose);

    // create the alert table
    var alertTable = document.createElement('table');
    alertTable.setAttribute('cellSpacing', '0');
    alertTable.setAttribute('cellPadding', '0');
    alertTable.setAttribute('border', '0');
  
    var alertTbody = document.createElement('tbody');
    var alertTr = document.createElement('tr');
    var alertTd = document.createElement('td');

    // the icon td.
    if(icon != ".none")
	{
		alertTd.setAttribute("vAlign", "top");
		this.alertIconImage = document.createElement('img');
		this.alertIconImage.style.margin = "0px 10px 0px 0px";
		this.setAlertIcon(icon);
		alertTd.appendChild(this.alertIconImage);
		alertTr.appendChild(alertTd);
	}
    // the alert content td.
    this.alertContentTd = document.createElement('td');
    this.alertContentTd.className = "dialogContentArea";
    alertTr.appendChild(this.alertContentTd);

    alertTbody.appendChild(alertTr);
    alertTable.appendChild(alertTbody);

    // add to the base dialog's content.
    this.contentArea.appendChild(alertTable);

    this.buttonDIV = document.createElement('div');
    this.buttonDIV.setAttribute("align", "center");
    this.buttonDIV.style.margin = "10px 0px 0px 0px";
    this.contentArea.appendChild(this.buttonDIV);

    filogix.gadget.AlertDialogBox.
        addButton(this, filogix.gadget.AlertDialogBox.okLabel, 1);
};

/**
 * some static variants.
 */
filogix.gadget.AlertDialogBox.warningIconFile = "icons/warning.gif";
filogix.gadget.AlertDialogBox.errorIconFile = "icons/error.gif";
filogix.gadget.AlertDialogBox.infoIconFile = "icons/info.gif";
filogix.gadget.AlertDialogBox.okLabel = "Ok";
filogix.gadget.AlertDialogBox.cancelLabel = "cancel";

/**
 * set the content for the alert dialog.
 */
filogix.gadget.AlertDialogBox.prototype.setContent = function(htmlContent) {

    this.alertContentTd.innerHTML = htmlContent;
};

/**
 * set the alert icon.
 */
filogix.gadget.AlertDialogBox.prototype.setAlertIcon = function(iconFile) {

    this.alertIconImage.src = filogix.gadget.DialogBase.imagePath + iconFile;
};

/**
 * add button to the alert dialog box.
 */
filogix.gadget.AlertDialogBox.addButton =
    function(parent, buttonLabel, buttonId) {

    var theButton = document.createElement("button");
    theButton.style.fontSize = "10pt";
    theButton.style.width = "60px";
    theButton.style.margin = "0px 5px";
    theButton.innerHTML = buttonLabel;
    theButton.linkId = buttonId;
    theButton.parentDialog = parent;
    theButton.onclick = filogix.gadget.AlertDialogBox.clickButton;
    parent.buttonDIV.appendChild(theButton);
};

/**
 * handle the click button event.
 */
filogix.gadget.AlertDialogBox.clickButton = function(event) {

    if (!event) event = window.event;

    var theSource = event.target ? event.target : event.srcElement;
    var linkId = theSource.linkId;

    if (theSource.parentDialog) {
        switch (linkId) {
        case 1:
            theSource.parentDialog.hide(true);
            break;
        }
        return false;
    }
    return false;
};
/**
 * class AlertDialogBox END.
 ******************************************************************************/


/*******************************************************************************
 * class BusinessRuleDialog.
 */

/**
 * the constructor.
 */
filogix.gadget.BusinessRuleDialog = function() {

    this.init(true, true);
};

/**
 * setting up the inheritance.
 */
filogix.gadget.BusinessRuleDialog.prototype = new filogix.gadget.DialogBase();
filogix.gadget.BusinessRuleDialog.prototype.constructor =
    filogix.gadget.BusinessRuleDialog;
filogix.gadget.BusinessRuleDialog.superclass =
    filogix.gadget.DialogBase.prototype;

/**
 * init method.
 */
filogix.gadget.BusinessRuleDialog.prototype.init =
    function(isModal, autoClose) {

    filogix.gadget.BusinessRuleDialog.superclass.init.call(this, isModal,
                                                           autoClose);
    // seting the width of the dialog.
    this.container.style.width = "418px";
    // the message content area.
    this.contentArea.style.height = "288px";
    this.contentArea.style.overflow = "auto";
    this.contentArea.style.backgroundColor = "#D1EBFF";

    // create the alert table
    var contentTable = document.createElement('table');
    contentTable.setAttribute('cellSpacing', '0');
    contentTable.setAttribute('cellPadding', '0');
    contentTable.setAttribute('border', '0');
    contentTable.style.backgroundColor = "#D1EBFF";

    // tbody
    var contentTbody = document.createElement('tbody');
    // the caption tr.
    var captionTr = document.createElement('tr');
    this.captionTd = document.createElement('td');
    this.captionTd.style.padding = "5px 0px 0px 0px";
    this.captionTd.setAttribute("vAlign", "top");
    this.captionTd.innerHTML=" <center><B>Business Rules Errors!</B></center>";
    captionTr.appendChild(this.captionTd);
    // add to tbody
    contentTbody.appendChild(captionTr);
    // message tr
    var msgTr = document.createElement('tr');
    // the alert content td.
    this.messageTd = document.createElement('td');
    msgTr.appendChild(this.messageTd);
    // add to tbody
    contentTbody.appendChild(msgTr);

    // add to table.
    contentTable.appendChild(contentTbody);

    // add to the base dialog's content.
    this.contentArea.appendChild(contentTable);

    this.buttonDIV = document.createElement('div');
    this.buttonDIV.setAttribute("align", "center");
    this.buttonDIV.style.margin = "0px";
    this.buttonDIV.style.padding = "0px 0px 5px 0px";
    this.buttonDIV.style.backgroundColor = "#D1EBFF";
    this.contentArea.appendChild(this.buttonDIV);

    // add the ok button as image.
    var href = document.createElement("a");
    href.setAttribute("href", "JavaScript: function(){return;}");
    var okButton = document.createElement("img");
    okButton.setAttribute("border", "0");
    okButton.src = "../images/ok.gif";
    okButton.style.margin = "5px 5px";
    okButton.linkId = 1;
    okButton.parentDialog = this;
    okButton.onclick = this.clickButton;
    href.appendChild(okButton);
    this.buttonDIV.appendChild(href);
};

/**
 * set the content for the alert dialog.
 */
filogix.gadget.BusinessRuleDialog.prototype.setContent = function(htmlContent) {

    this.messageTd.innerHTML = htmlContent;
};

/**
 * set the caption for the business rule error.
 */
filogix.gadget.BusinessRuleDialog.prototype.setCaption = function(htmlCaption) {

    this.captionTd.innerHTML = htmlCaption;
};

/**
 * handle the click button event.
 */
filogix.gadget.BusinessRuleDialog.prototype.clickButton = function(event) {

    if (!event) event = window.event;

    var theSource = event.target ? event.target : event.srcElement;
    var linkId = theSource.linkId;

    if (theSource.parentDialog) {
        switch (linkId) {
        case 1:
            theSource.parentDialog.hide(true);
            break;
        }
        return false;
    }
    return false;
};
/**
 * class BusinessRuleDialog END.
 ******************************************************************************/


/*******************************************************************************
 * class HTMLViewerDialog view a html file.
 */
/**
 * the constructor.
 */
filogix.gadget.HTMLViewerDialog = function(bPrintable, printImageFile) {
    
    this.printable = true;
    if(bPrintable != null){
        this.printable = bPrintable;
    }

    this.printImageFile = "../images/ok.gif";
    if(printImageFile != null) {
        this.printImageFile = printImageFile;
    }
    
    this.init(true, true);
};

// inheritance
filogix.gadget.HTMLViewerDialog.prototype = new filogix.gadget.DialogBase();
filogix.gadget.HTMLViewerDialog.prototype.constructor =
    filogix.gadget.HTMLViewerDialog;
filogix.gadget.HTMLViewerDialog.superclass = filogix.gadget.DialogBase.prototype;

/**
 * prototype.
 */
filogix.gadget.HTMLViewerDialog.prototype.init = function(isModal, autoClose) {

    filogix.gadget.HTMLViewerDialog.superclass.init.call(this, isModal,
                                                         autoClose);
    this.htmlDiv = document.createElement("DIV");
    this.htmlDiv.style.overflow = "auto";
    this.htmlDiv.style.width = document.body.clientWidth * 2 / 3;
    this.htmlDiv.style.height = document.body.clientHeight * 2 / 3;
    // add to the content area.
    this.contentArea.appendChild(this.htmlDiv);

    this.buttonDIV = document.createElement('div');
    this.buttonDIV.setAttribute("align", "center");
    this.buttonDIV.style.margin = "10px 0px 0px 0px";
    // add to the content area.
    this.contentArea.appendChild(this.buttonDIV);

    // the OK button.
    var href = document.createElement("a");
    href.setAttribute("href", "JavaScript: function(){return;}");
    var okButton = document.createElement("img");
    okButton.setAttribute("border", "0");
    okButton.src = "../images/ok.gif";
    okButton.style.margin = "5px 5px";
    okButton.linkId = 1;
    okButton.parentDialog = this;
    okButton.onclick = this.clickButton;
    href.appendChild(okButton);
    this.buttonDIV.appendChild(href);

    // the Print button.
    if(this.printable == true) {
        var hrefPrint = document.createElement("a");
        hrefPrint.setAttribute("href", "JavaScript: function(){return;}");
        var printButton = document.createElement("img");
        printButton.setAttribute("border", "0");
        printButton.src = this.printImageFile;
        printButton.style.margin = "5px 5px";
        printButton.linkId = 2;
        printButton.parentDialog = this;
        printButton.onclick = this.clickButton;
        hrefPrint.appendChild(printButton);
        this.buttonDIV.appendChild(hrefPrint);
    }
};

/**
 * set the content for the html viewer dialog.
 */
filogix.gadget.HTMLViewerDialog.prototype.setContent = function(htmlContent) {

    this.htmlDiv.innerHTML = htmlContent;
};

/**
 * handle the click button event.
 */
filogix.gadget.HTMLViewerDialog.prototype.clickButton = function(event) {

    if (!event) event = window.event;

    var theSource = event.target ? event.target : event.srcElement;
    var linkId = theSource.linkId;
    if (theSource.parentDialog) {
        switch (linkId) {
        case 1:
            theSource.parentDialog.hide(true);
            break;
        case 2:
            theSource.parentDialog.printReport();
            break;
        }
        return false;
    }
    return false;
};

/**
 * print report content.
 */

filogix.gadget.HTMLViewerDialog.prototype.printReport = function() {

    var printWindow = window.open();
    printWindow.document.body.innerHTML = this.htmlDiv.innerHTML;
    printWindow.print();
    printWindow.close();
};

/**
 * class HTMLViewerDialog END
 ******************************************************************************/

/*******************************************************************************
 * class InstitutionSelectionDialog pop up the institution selection.
 */
/**
 * the constructor.
 */
filogix.gadget.InstitutionSelectionDialog =
    function(pageId, institutions, cancelImgFile, okImgFile) {

    this.title = "Institutions Selection - Express";

    this.uid = (new Date()).getMilliseconds();
    this.institutions = institutions;

    this.returnData = new Object();
    this.returnData.pageId = pageId;

    this.cancelImgFile = "../images/cancel.gif";
    if (cancelImgFile != null) {
        this.cancelImgFile = cancelImgFile;
    }

    this.okImgFile = "../images/ok.gif";
    if (okImgFile != null) {
        this.okImgFile = okImgFile;
    }

    this.init(true, true);
};

// inheritance
filogix.gadget.InstitutionSelectionDialog.prototype = new filogix.gadget.DialogBase();
filogix.gadget.InstitutionSelectionDialog.prototype.constructor =
    filogix.gadget.InstitutionSelectionDialog;
filogix.gadget.InstitutionSelectionDialog.superclass = filogix.gadget.DialogBase.prototype;

/**
 * prototype.
 */
filogix.gadget.InstitutionSelectionDialog.prototype.init = function(isModal, autoClose) {

    filogix.gadget.InstitutionSelectionDialog.superclass.init.call(this, isModal,
                                                         autoClose);

    this.contentArea.style.backgroundColor = "#D1EBFF";
    // prompt message area.
    this.htmlDiv = document.createElement("DIV");
    this.htmlDiv.style.padding = "5px 10px";
    // add to the content area.
    this.contentArea.appendChild(this.htmlDiv);
    // the focus href.
    this.focusHref = new Object();

    // institution selection area.
    var selectionDiv = document.createElement("DIV");
    selectionDiv.id = "institutionSelection";
    selectionDiv.style.margin = "5px 15px 5px 15px";
    for (var i = 0; i < this.institutions.length; i ++) {
        var strong = document.createElement("span");
        strong.style.fontWeight = "bold";
        var institutionHref = document.createElement("a");
        institutionHref.setAttribute("href", "JavaScript: function(){return;}");
        institutionHref.linkId = 1;
        institutionHref.institutionId = this.institutions[i].id;
        institutionHref.parentDialog = this;
        institutionHref.onclick = this.clickButton;

        var institutionLabel = this.institutions[i].label;
        if (this.institutions[i].borrower) {
            institutionLabel = institutionLabel + " - " + this.institutions[i].borrower;
        }
        institutionHref.appendChild(document.createTextNode(institutionLabel));

        // set the focus href, suppose to be the first one.
        if (i == 0) {
            this.focusHref = institutionHref;
        }

        strong.appendChild(institutionHref);
        selectionDiv.appendChild(strong);

        selectionDiv.appendChild(document.createElement("br"));
    }
    // add to the content area.
    this.contentArea.appendChild(selectionDiv);

    // the botton area.
    this.buttonDIV = document.createElement('div');
    this.buttonDIV.setAttribute("align", "center");
    this.buttonDIV.style.margin = "10px 0px 0px 0px";
    // add to the content area.
    this.contentArea.appendChild(this.buttonDIV);

    // the Cancel button.
    var hrefCancel = document.createElement("a");
    hrefCancel.setAttribute("href", "JavaScript: function(){return;}");
    var cancelButton = document.createElement("img");
    cancelButton.setAttribute("border", "0");
    cancelButton.src = this.cancelImgFile;
    cancelButton.style.margin = "5px 5px";
    cancelButton.linkId = 2;
    cancelButton.parentDialog = this;
    cancelButton.onclick = this.clickButton;
    hrefCancel.appendChild(cancelButton);
    this.buttonDIV.appendChild(hrefCancel);
};

/**
 * set the content for the html viewer dialog.
 */
filogix.gadget.InstitutionSelectionDialog.prototype.setPrompt =
    function(htmlContent) {

    this.htmlDiv.innerHTML = htmlContent;
};

/**
 * handle the click button event.
 */
filogix.gadget.InstitutionSelectionDialog.prototype.clickButton = function(event) {
    //alert(" goto.js -- filogix.gadget.InstitutionSelectionDialog.prototype.clickButton");
    if (!event) event = window.event;

    var theSource = event.target ? event.target : event.srcElement;
    var linkId = theSource.linkId;
    if (theSource.parentDialog) {
        switch (linkId) {
        case 1:
            // handle ok.
            theSource.parentDialog.hide(true, theSource.institutionId);
            break;
        case 2:
            // hancle cancel.
            theSource.parentDialog.hide();
            break;
        }
        return false;
    }
    return false;
};

filogix.gadget.InstitutionSelectionDialog.prototype.hide = function(ok, institutionId) {
    //alert(" goto.js --filogix.gadget.InstitutionSelectionDialog.prototype.hide");
    if (ok) {
        this.returnData.institutionId = institutionId;
    }

    filogix.gadget.InstitutionSelectionDialog.superclass.hide.call(this, ok);
};

filogix.gadget.InstitutionSelectionDialog.prototype.show = function() {

    filogix.gadget.InstitutionSelectionDialog.superclass.show.call(this);
};

/**
 * class InstitutionSelectionDialog END
 ******************************************************************************/


/*******************************************************************************
 * class ProgressBarDialog. trying to show a progress bar as a dialog.
 */

/**
 * the constructor.
 */
filogix.gadget.ProgressBarDialog =
    function(promptIconFile, promptMsg, barClassName) {

    if (arguments.length == 0) {
        return;
    }

    this.base = filogix.gadget.DialogBase;
    // turn the close icon off.
    this.base(true, false);

    // create the progress bar table.
    var progressTable = document.createElement("table");
    progressTable.setAttribute("cellSpacing", "0");
    progressTable.setAttribute("cellPadding", "0");
    progressTable.setAttribute("border", "0");
    progressTable.setAttribute("width", "100%");
    progressTable.style.margin = "5px";
    this.progressTbody = document.createElement("tbody");

    // the prompt message row.
    var promptMsgTr = document.createElement("tr");
    // -- icon for the prompt message.
    var promptIconTd = document.createElement("td");
    promptIconTd.setAttribute("vAlign", "top");
    promptIconTd.setAttribute("width", "10%");
    this.promptIconImage = document.createElement("img");
    this.promptIconImage.style.margin = "0px 10px 0px 0px";
    if (promptIconFile) {
        this.setPromptIcon(promptIconFile);
    } else {
        // TODO: should set the default icon.
    }
    promptIconTd.appendChild(this.promptIconImage);
    promptMsgTr.appendChild(promptIconTd);
    // -- the prompt message.
    this.promptMsgTd = document.createElement("td");
    if (promptMsg) {
        this.setPromptMsg(promptMsg);
    } else {
        // TODO: set the default message.
        this.setPromptMsg("Put your prompt messgae here.");
    }
    promptMsgTr.appendChild(this.promptMsgTd);

    // add to the progress table.
    this.progressTbody.appendChild(promptMsgTr);

    // the progress bar row.
    var progressTr = document.createElement("tr");
    var progressTd = document.createElement("td");
    progressTd.setAttribute("vAlign", "middle");
    progressTd.setAttribute("colSpan", "2");
    progressTd.setAttribute("align", "center");
    
    // -- draw the progress bar by using the divs.
    // the default demension.
    this.pixels = 250;
    this.fontSize = 15;
    // ---- the outer div
    this.outerDIV = document.createElement("div");
    this.outerDIV.style.border = "1px solid #000000";
    this.outerDIV.style.background = "#FFFFFF";
    this.outerDIV.style.fontFamily = "Arial,Verdana";
    this.outerDIV.style.fontSize = this.fontSize + "px";
    this.outerDIV.style.width = (this.pixels + 2) + "px";
    this.outerDIV.style.textAlign = "left";
    progressTd.appendChild(this.outerDIV);
    // ---- the inner div
    this.fillDIV = document.createElement("div");
    this.fillDIV.style.textAlign = "right";
    this.fillDIV.style.overflow = "hidden";
    this.fillDIV.innerHTML = "x";
    this.fillDIV.style.width = "0px";
    if (barClassName) {
        this.fillDIV.className = barClassName;
    } else {
        // using the default stylsheet.
        this.fillDIV.style.background = "#00008b";
        this.fillDIV.style.border = "1px solid #FFFFFF";
        this.fillDIV.style.color = "#FFFFFF";
    }
    this.outerDIV.appendChild(this.fillDIV);
    progressTr.appendChild(progressTd);

    // add to the progress table.
    this.progressTbody.appendChild(progressTr);
    progressTable.appendChild(this.progressTbody);

    // add to the dialogbase's content area.
    this.contentArea.appendChild(progressTable);
    this.setWidth(this.pixels + 60);
};

// setting up the inheritance.
filogix.gadget.ProgressBarDialog.prototype = new filogix.gadget.DialogBase();

/**
 * set the progress percent.
 */
filogix.gadget.ProgressBarDialog.prototype.setProgressPercent =
    function(percent) {

    var fillPixs;
    if (percent < 1.0) {
        fillPixs = Math.round(this.pixels * percent);
    } else {
        percent = 1.0;
        fillPixs = this.pixels;
    }
    this.fillDIV.innerHTML = Math.round(100 * percent) + "%";
    this.fillDIV.style.width = fillPixs + "px";
};

/**
 * set the icon file for the prompt message icon, should be in the DialogBase
 * imagePath.
 */
filogix.gadget.ProgressBarDialog.prototype.setPromptIcon = function(iconFile) {

    this.promptIconImage.src = filogix.gadget.DialogBase.imagePath + iconFile;
};

/**
 * set the prompt message.
 */
filogix.gadget.ProgressBarDialog.prototype.setPromptMsg = function(msg) {

    this.promptMsgTd.innerHTML = msg;
};

/**
 * class ProgressBarDialog END.
 ******************************************************************************/



/*******************************************************************************
 * class SimplePreloaderDialog Start
 ******************************************************************************/
filogix.gadget.SimplePreloaderDialog = function() {

    this.base = filogix.gadget.DialogBase;
    this.base(true, false);
    
    var dialogMainDiv = document.createElement("div");
    dialogMainDiv.setAttribute("id", "preloaderDialogMain");
    this.dialogMsgDiv = document.createElement("p");
    this.dialogMsgDiv.setAttribute("id", "preloaderDialogMessage");
    this.dialogImage = document.createElement("img");
    this.dialogImage.setAttribute("id", "preloaderDialogImage");
    dialogMainDiv.appendChild(this.dialogMsgDiv);
    dialogMainDiv.appendChild(this.dialogImage);
    this.contentArea.appendChild(dialogMainDiv);
};

filogix.gadget.SimplePreloaderDialog.prototype = new filogix.gadget.DialogBase();

filogix.gadget.SimplePreloaderDialog.prototype.setPreloaderImage = function(iconFile) {
    if(iconFile) this.dialogImage.src = filogix.gadget.DialogBase.imagePath + iconFile;
};

filogix.gadget.SimplePreloaderDialog.prototype.setPromptMsg = function(msg) {
    if(msg) this.dialogMsgDiv.innerHTML = msg;
};
/*******************************************************************************
 * class SimplePreloaderDialog END.
 ******************************************************************************/

/*******************************************************************************
 * class SimpleAlertDialogBox.
 ******************************************************************************/
filogix.gadget.SimpleAlertDialogBox = function() {
    // no [x] button, no icon images
    this.init(true, false, ".none"); 
    this.drow();
};
/**
 * setting up the inheritance.
 */
filogix.gadget.SimpleAlertDialogBox.prototype = new filogix.gadget.DialogBase();
/**
 * init method.
 */
filogix.gadget.SimpleAlertDialogBox.prototype.drow = function(isModal, autoClose, icon) {

    // the alert content td.
    var saMainDiv = document.createElement('div');
    saMainDiv.setAttribute("id", "SimpleAlertMain");
    var saMessageP  = document.createElement('p');
    saMessageP.setAttribute("id", "SimpleAlertMessage");
    saMainDiv.appendChild(saMessageP);
    var saButtonsDiv = document.createElement('div');
    saButtonsDiv.setAttribute("id", "SimpleAlertButtons");
    saMainDiv.appendChild(saButtonsDiv);
    
    this.saMainDiv = saMainDiv;
    this.saMessageP = saMessageP;
    this.saButtonsDiv = saButtonsDiv;
    this.contentArea.appendChild(saMainDiv);
};
/**
 * set the content for the alert dialog.
 */
filogix.gadget.SimpleAlertDialogBox.prototype.setContent = function(htmlContent) {
    this.saMessageP.innerHTML = htmlContent;
};
/**
 * add button to the alert dialog box.
 */
filogix.gadget.SimpleAlertDialogBox.prototype.addButton =
    function(buttonLabel, callbackFunction) {

    var theButton = document.createElement("button");
    theButton.style.fontSize = "10pt";
    theButton.style.width = "60px";
    theButton.style.margin = "0px 5px";
    theButton.innerHTML = buttonLabel;
    if(callbackFunction){
        theButton.onclick = callbackFunction;
    }
    this.saButtonsDiv.appendChild(theButton);
};
/*******************************************************************************
 * class SimpleAlertDialogBox END.
 ******************************************************************************/
