

/**
 * the contact's first name.
 */
function setContactFirstName(contactFirstName) {
    document.getElementById("contactFirstName").value = contactFirstName;
}

function setContactLastName(contactLastName) {
    document.getElementById("contactLastName").value = contactLastName;
}

function setContactEmail(contactEmail) {
    document.getElementById("contactEmail").value = contactEmail;
}

function setContactWorkArea(contactWorkArea) {
    document.getElementById("contactWorkArea").value = contactWorkArea;
}

function setContactWorkFirst(contactWorkFirst) {
    document.getElementById("contactWorkFirst").value = contactWorkFirst;
}

function setContactWorkLast(contactWorkLast) {
    document.getElementById("contactWorkLast").value = contactWorkLast;
}

function setContactWorkExt(contactWorkExt) {
    document.getElementById("contactWorkExt").value = contactWorkExt;
}

function setContactCellArea(contactCellArea) {
    document.getElementById("contactCellArea").value = contactCellArea;
}

function setContactCellFirst(contaceCellFirst) {
    document.getElementById("contactCellFirst").value = contactCellFirst;
}

function setContactCellLast(contactCellLast) {
    document.getElementById("contactCellLast").value = contactCellLast;
}

function setContactHomeArea(contactHomeArea) {
    document.getElementById("contactHomeArea").value = contactHomeArea;
}

function setContactHomeFirst(contactHomeFirst) {
    document.getElementById("contactHomeFirst").value = contactHomeFirst;
}

function setContactHomeLast(contactHomeLast) {
    document.getElementById("contactHomeLast").value =contactHomeLast;
}




/**************************************
 * class PrimaryBorrowerRequest
 */
filogix.express.service.PrimaryBorrowerRequest = function() {

    // setting up the request type.
    this.requestType = "PrimaryBorrower";
}

filogix.express.service.PrimaryBorrowerRequest.prototype =
    new filogix.express.service.XHRServiceRequest();
filogix.express.service.PrimaryBorrowerRequest.superclass =
    filogix.express.service.XHRServiceRequest.prototype;


/**
 * update the transaction copy id.
 */
filogix.express.service.PrimaryBorrowerRequest.prototype.updateTransactionCopyId =
    function(newId) {

    // update the DOM.
    setCopyId(newId);
}

/**
 * handle success response button report?.
 */
filogix.express.service.PrimaryBorrowerRequest.prototype.handleSuccess =
    function(responseObj) {

    // call the super class mehtod to close the progress bar.
    filogix.express.service.PrimaryBorrowerRequest.
        superclass.handleSuccess.call(this, responseObj);
}
/**
 * class PrimaryBorrowerRequest  END
 **************************************/


/**
 * primary borrower checkbox extract function.
 */
function borrowerRequest() {

    var primaryBorrowerRequest = new filogix.express.service.BorrowerExtractor();
    primaryBorrowerRequest.dealId = getDealId();
    primaryBorrowerRequest.copyId = getCopyId();
    primaryBorrowerRequest.requestId = getFPCRequestId();
    primaryBorrowerRequest.serviceRequestContactId = getFPCContactId();
    primaryBorrowerRequest.needPrimary = getFPCBorrowerCheckbox().checked;
    primaryBorrowerRequest.extract();
}




/**************************************
 * class BorrowerExtractor
 * try to extract the borrower information for the specified deal id
 * and copy id.
 */
filogix.express.service.BorrowerExtractor = function() {

    /**
     * the post data.
     */
    this.requestType = "express.service.BorrowerExtractor";
    this.dealId;
    this.copyId;
    this.requestId;
    this.serviceRequestContactId;
    // need the primary or not.
    this.needPrimary = true;
}

filogix.express.service.BorrowerExtractor.prototype = {

    /**
     * extract the information.
     */
    extract:function() {

        // get a XMLHttpRequest object!
        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
        var request = new Object();
        request.rcRequestContent = new Object();
        request.rcRequestContent.dealId = this.dealId;
        request.rcRequestContent.copyId = this.copyId;
        request.rcRequestContent.serviceRequestContactId =
            this.serviceRequestContactId;
        request.rcRequestContent.needPrimary = this.needPrimary;
        // send the post data.  the JSON implementation.
        xmlhttp.send("requestType=" + this.requestType +
                     "&requestContent=" + request.toJSONString());

        // handle the HTTP response status, update the request monitor.
        var httpStatus = xmlhttp.status;
        if (httpStatus == 200) {
            var borrowerInfo =
                xmlhttp.responseText.parseJSON().rcResponseContent;
            this.updatePage(borrowerInfo);
        } else {
            // here are some http error!
            var alertDlg = new filogix.gadget.AlertDialogBox(".none");
            var messages = new filogix.util.RichClientMessages();
            alertDlg.setTitle(messages.getSystemMessage("SYSTEM_ERROR_TITLE"));
            var params = new Array();
            params[0] = xmlhttp.statusText;
            alertDlg.setContent(messages.getSystemMessage("HTTP_ERROR_MESSAGE",
                                                          params));
            alertDlg.show();
            alertDlg.moveTo(-1, -1);
            filogix.gadget.DialogHelper.activeDialog = alertDlg;
        }
    },

    /**
     * update current page.
     */
    updatePage:function(borrowerInfo) {
        // update borrower id, for both cases.
        if(borrowerInfo.borrowerId) {
            setFPCBorrowerId(borrowerInfo.borrowerId);
        }

        // we need update the servicerequestcontact on the screen

        // get info from the response object.
        if (borrowerInfo.firstName) {
            setContactFirstName(borrowerInfo.firstName);
        }

        if (borrowerInfo.lastName) {
            setContactLastName(borrowerInfo.lastName);
        }

        if (borrowerInfo.emailAddress) {
            setContactEmail(borrowerInfo.emailAddress);
        }

        if (borrowerInfo.workAreaCode) {
            setContactWorkArea(borrowerInfo.workAreaCode);
        }

        if (borrowerInfo.workPhoneNumber) {
            setContactWorkFirst(borrowerInfo.workPhoneNumber.substring(0,3));
            setContactWorkLast(borrowerInfo.workPhoneNumber.substring(3));
        }

        if (borrowerInfo.workPhoneExt) {
            setContactWorkExt(borrowerInfo.workPhoneExt);
        }

        if (borrowerInfo.cellAreaCode) {
            setContactCellArea(borrowerInfo.cellAreaCode);
        }

        if (borrowerInfo.cellPhoneNumber) {
            setContactCellFirst(borrowerInfo.cellPhoneNumber.substring(0,3));
            setContactCellLast(borrowerInfo.cellPhoneNumber.substring(3));
        }

        if (borrowerInfo.homeAreaCode) {
            setContactHomeArea(borrowerInfo.homeAreaCode);
        }

        if (borrowerInfo.homePhoneNumber) {
            setContactHomeFirst(borrowerInfo.homePhoneNumber.substring(0,3));
            setContactHomeLast(borrowerInfo.homePhoneNumber.substring(3));
        }
    }
}
/**
 * class BorrowerExtractor END
 *************************************/
