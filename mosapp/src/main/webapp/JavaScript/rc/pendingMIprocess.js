(function() {

    var msgBox = filogix.msgBox;

    var alertCallback = function() {
        //IsSubmitButton();
        //click btProcessMI button
        btProcessMI.click();
    };
    
    var btProcessMI;
    
    var PendingMIProcessor = function(processMIBtn) {
        this.requestType = "express.service.PendingMIProcessor";
        this.timeoutTimeMillSec = 90000;
        
        //btProcess button
        btProcessMI = processMIBtn;
        
        this.requestContent = {};
        this.requestContent.rcRequestContent = {};
    };

    //inherit from SimpleXHRService
    PendingMIProcessor.prototype = new filogix.express.service.SimpleXHRService();

    PendingMIProcessor.prototype.handleRuntimeError = function(err) {
        msgBox.alert("#MI_GENERIC_PENDING_RESPONSE_FAILURE_TITLE#", "#MI_GENERIC_SYSTEM_ERROR_MSG#", alertCallback);
    };

    PendingMIProcessor.prototype.handleHttpError = function(responseObj) {
        this.handleRuntimeError();
    };

    PendingMIProcessor.prototype.handleFailureReceived = function(responseObj) {
        this.handleRuntimeError();
    };

    PendingMIProcessor.prototype.handleSuccessReceived = function(responseObj) {
        msgBox.alert("#MI_GENERIC_PENDING_RESPONSE_PROCESED_TITLE#", "#MI_GENERIC_PENDING_RESPONSE_PROCESED_MSG#", alertCallback);
    };

    PendingMIProcessor.prototype.createMainDialog = function() {

        var dialog = new filogix.gadget.SimplePreloaderDialog();

        dialog.setTitle(msgBox.getClientMsg("MI_GENERIC_PENDING_RESPONSE_PROCESSING_TITLE"));
        dialog.setPromptMsg(msgBox.getClientMsg("MI_GENERIC_PENDING_RESPONSE_PROCESSING_MSG"));
        dialog.setPreloaderImage("preloader.gif");
        return dialog;

    };
    
    //expose to the global scope
    filogix.express.service.PendingMIProcessor = PendingMIProcessor;

})();
