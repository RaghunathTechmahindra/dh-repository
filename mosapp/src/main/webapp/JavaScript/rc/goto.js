filogix.gadget.DialogBase.imagePath = "../JavaScript/rc/images/";

/**
 * FXP22783 fix for 'Goto menu has english words.'
 */
filogix.util.RichClientMessages.msgKeys = new Array();
filogix.util.RichClientMessages.msgKeys =
    ["CHOOSE_PAGE","INSTITUTION_SELECTION","INSTITUTION_PAGE","INSTITUTION_DEAL","INSTITUTION_DEAL2"];

// all available pages.
var allAvailablePages;

//Timeout page.
var timeoutPage = 'pgSessionEnded.jsp';

/**
 * returns the pages list dropdown.
 */
function getPagesListElement() {

    return document.getElementById("cbPageList");
}

/**************************************
 * class PagesListBuilder
 * trying to extract the pages list from server for the dealid. 
 */
/**
 * constructor.
 */
filogix.express.service.PagesListBuilder = function() {

    this.requestType = "express.service.PagesListRequest";
    // the default deal id is -1, if no dealid input.
    this.dealId = -1;
};

/**
 * the prototype for pages list builder class.
 */
filogix.express.service.PagesListBuilder.prototype = {

    /**
     * send request, get response from server.
     */
    queryPages:function() {
        // get a xmlhttp object.
        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
        // preparing the request object.
        var request = new Object();

        request.rcRequestContent = new Object();
        request.rcRequestContent.dealId = this.dealId;

        // send out the request.
        xmlhttp.send("requestType=" + this.requestType +
                     "&requestContent=" + request.toJSONString());

        if (xmlhttp.status != 200 || xmlhttp.responseText.indexOf("<TITLE>Session Ended</TITLE>") != -1) {
            //Error happened.
        	//Redirect to timeout page.
            window.location = timeoutPage;

            return null;
        }

        var response = xmlhttp.responseText.parseJSON().rcResponseContent;
        return response.pages;
    },

    /**
     * build the pages list.
     *
     * returns the Pages list element.
     */
    buildPagesList:function() {

        var pagesSelection = getPagesListElement();
        for (var i = pagesSelection.options.length; i > 0; i --) {
            pagesSelection.remove(0);
        }

        var pages = this.queryPages();
        if (pages == null) {
            // error happend,  TODO: how to handle this?
            return;
        }

        // the hint option, it is always be the first one.
        var hint = document.createElement("option");
        pagesSelection.options[0] = hint;
        hint.setAttribute("value", -1);
        // TODO: get the label from RichClientMessage ...
        
        //FXP 22774: fix for 'Goto menu has english words. '
        var messages = new filogix.util.RichClientMessages();
        hint.appendChild(document.
                         createTextNode(messages.getMessage("CHOOSE_PAGE")));
        // for each page.
        for (var j = 0; j < pages.length; j ++) {

            var pageLabel = pages[j].label;
            var pageIndex = j;
            // option for each page.
            var aPage = document.createElement("option");
            pagesSelection.options[j + 1] = aPage;
            aPage.setAttribute("value", pageIndex);
            aPage.appendChild(document.createTextNode(pageLabel));
        }

        // reset the selected option.
        pagesSelection.selectedIndex = 0;

        // return the pages list.
        return pages;
    }
}

function getInputDealId() {

    var theId = parseInt(document.getElementById("tbDealId").value);
    //alert("Parsed Deal Id: " + theId);
    if (theId == null || isNaN(theId)) {
        //alert("not a number!");
        return -1;
    } else {
        return theId;
    }
}

function getTbPageId() {

    return document.getElementById("tbPageId");
}

function getTbInstitutionId() {

    return document.getElementById("tbInstitutionId");
}

/**
 * load available pages list.
 */
function loadPagesList() {

    var builder = new filogix.express.service.PagesListBuilder();
    builder.dealId = getInputDealId();
    allAvailablePages = builder.buildPagesList();
}

/**
 * option changes.
 */
function pageOptionChange() {

    if (!isGotoMeunDown) {
        // suppose do nothing ...
        return;
    }

    performPageSelect();
}

/**
 * enter key pressed.
 */
function pageEnterProcessed() {

    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;
    //alert("The key = " + keycode);

    if (keycode == 13) {
        performPageSelect();
        return false;
    }

    return true;
}

/**
 * perform page select.
 */
function performPageSelect() {

    // set up the page id and institution profile id.
    var pagesSelection = getPagesListElement("cbPageList");
    var pageIndex = pagesSelection.options[pagesSelection.selectedIndex].value;
    if (pageIndex < 0) {
        // no page selected, do nothing.
        return;
    }

    // here is the page id.
    var pageId = allAvailablePages[pageIndex].id;
    var institutions = allAvailablePages[pageIndex].institutions;
   
    // figure out the institution id.
    if (institutions == null || institutions.length == 0) {
        handlePageClick(pageId);
    } else if (institutions.length == 1) {
        // go directly.
        handlePageClick(pageId, institutions[0].id);
    } else {
        // pop up the institution selection dialog box.
        var pageLabel = allAvailablePages[pageIndex].label;
        var dealNecessity =  allAvailablePages[pageIndex].dealNecessity;
        
        performInstitutionSelect(getInputDealId(), pageId, pageLabel,
                                 institutions, dealNecessity);
    }
}

function handlePageClick(pageId, institutionId) {

    //alert("page id: " + pageId + ", Institution Id: " + institutionId);
    // set page id and institution id.
    getTbPageId().value = pageId;
    if (institutionId == null) {
        getTbInstitutionId().value = -1;
    } else {
        getTbInstitutionId().value = institutionId;
    }

    // invoke the performGoto
    performGoto();
}

function performInstitutionSelect(dealId, pageId, pageLabel, institutions, dealNecessity) {

    tool_click(1);
	
	var messages = new filogix.util.RichClientMessages();
    var caption = messages.getMessage("INSTITUTION_SELECTION");
	var captionPage = messages.getMessage("INSTITUTION_PAGE");
	var captionDeal = messages.getMessage("INSTITUTION_DEAL");
	var captionDeal2 = messages.getMessage("INSTITUTION_DEAL2");
	var cancelPath;
	if (LANGUAGECODE == 0)
		cancelPath = "../images/cancel.gif";
	else
		cancelPath = "../images/cancel_fr.gif";

    var selectionDlg =
        new filogix.gadget.InstitutionSelectionDialog(pageId, institutions, cancelPath);
		
	selectionDlg.setTitle(caption);
	//selectionDlg.setTitle("Institution Selection - Express");
    selectionDlg.callOk = handleConfirm;
    
    if (dealId < 0 ||  (dealNecessity == "N")) {
        selectionDlg.setPrompt(captionPage + " [" + pageLabel + "]:");
    } else {
        selectionDlg.setPrompt(captionDeal + " [" +
                           dealId + "] " + captionDeal2 + " [" + pageLabel + "]:");
    }
    selectionDlg.show();
    selectionDlg.moveTo(-1, -1);
    selectionDlg.focusHref.focus();
    filogix.gadget.DialogHelper.activeDialog = selectionDlg;
}

function handleConfirm(something) {

    handlePageClick(something.pageId, something.institutionId);
}