/**************************************
 * class MIResponse
 * 
 */
 
//MI STATUS ID

filogix.express.service.Request = new Object();

filogix.express.service.Request.EXPRESS_MISTATUS_BLANK = 0;
filogix.express.service.Request.EXPRESS_MISTATUS_INITIAL_PENDING = 1;
filogix.express.service.Request.EXPRESS_MISTATUS_INITIAL_SUBMITTED = 2;
filogix.express.service.Request.EXPRESS_MISTATUS_CRITICAL_REC = 3;
filogix.express.service.Request.EXPRESS_MISTATUS_CRITICAL_FOUND = 4;
filogix.express.service.Request.EXPRESS_MISTATUS_ERROR_RECEIVED = 5;
filogix.express.service.Request.EXPRESS_MISTATUS_ERROR_REPORTED = 6;
filogix.express.service.Request.EXPRESS_MISTATUS_ERROR_CORRECTED = 7;
filogix.express.service.Request.EXPRESS_MISTATUS_CORRECTIONS_SUBMITTED = 8;
filogix.express.service.Request.EXPRESS_MISTATUS_MANUAL_REPLY = 9;
filogix.express.service.Request.EXPRESS_MISTATUS_MANUAL_REQUIRED = 10;
filogix.express.service.Request.EXPRESS_MISTATUS_CANCEL_PENDING = 11;
filogix.express.service.Request.EXPRESS_MISTATUS_CANCEL_SUBMITTED = 12;
filogix.express.service.Request.EXPRESS_MISTATUS_CANCEL_CONFIRM = 13;
filogix.express.service.Request.EXPRESS_MISTATUS_CANCEL = 14;
filogix.express.service.Request.EXPRESS_MISTATUS_APPROVAL_RECEIVED = 15;
filogix.express.service.Request.EXPRESS_MISTATUS_APPROVED = 16;
filogix.express.service.Request.EXPRESS_MISTATUS_DENIED_RECEIVED = 17;
filogix.express.service.Request.EXPRESS_MISTATUS_DENIED = 18;
filogix.express.service.Request.EXPRESS_MISTATUS_RISKED_RECEIVED = 19;
filogix.express.service.Request.EXPRESS_MISTATUS_RISKED = 20;
filogix.express.service.Request.EXPRESS_MISTATUS_CHANGE_PENDING = 21;
filogix.express.service.Request.EXPRESS_MISTATUS_CHANGES_SUBMITTED = 22;
filogix.express.service.Request.EXPRESS_MISTATUS_APPROVAL_RECEIVED = 23;
//MI INSURER ID
filogix.express.service.Request.EXPRESS_MIINSURER_CMHC = 1;
filogix.express.service.Request.EXPRESS_MIINSURER_GENWORTH = 2;

var miResponseInterval = null;
/**
 * constructor.
 */
filogix.express.service.MIResponse = function(){
    this.requestType = "express.service.MIResponse";
}


filogix.express.service.MIResponse.prototype = {

    sendMIResponse:function() {
    
        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
        // preparing the request object, set values
        var request = new Object();

        request.rcRequestContent = new Object();
        request.rcRequestContent.timeOut = this.timeOut;
        // send out the request.
        xmlhttp.send("requestType=" + this.requestType +
                     "&requestContent=" + request.toJSONString());

        if (xmlhttp.status != 200 || xmlhttp.responseText.indexOf("pgSessionEnded") != -1) {
            return null;
        }

        var resp = xmlhttp.responseText.parseJSON().rcResponseContent;
        return resp;
    }
}


/**
 * Check send out miresponse alert checking.
 */
function sendMIResponseRequest() {

    var pagebodyItem = document.all.pagebody.style;

    if( pagebodyItem.visibility == 'visible') {
        switch ((new Number(getMIResponseStatusId())).valueOf()) {
            case filogix.express.service.Request.EXPRESS_MISTATUS_INITIAL_PENDING: 
            case filogix.express.service.Request.EXPRESS_MISTATUS_INITIAL_SUBMITTED:
            case filogix.express.service.Request.EXPRESS_MISTATUS_ERROR_CORRECTED:
            case filogix.express.service.Request.EXPRESS_MISTATUS_CORRECTIONS_SUBMITTED:
            case filogix.express.service.Request.EXPRESS_MISTATUS_MANUAL_REPLY:
            case filogix.express.service.Request.EXPRESS_MISTATUS_MANUAL_REQUIRED:
            case filogix.express.service.Request.EXPRESS_MISTATUS_CANCEL_PENDING:
            case filogix.express.service.Request.EXPRESS_MISTATUS_CANCEL_SUBMITTED:
            case filogix.express.service.Request.EXPRESS_MISTATUS_RISKED_RECEIVED:
            case filogix.express.service.Request.EXPRESS_MISTATUS_RISKED:
            case filogix.express.service.Request.EXPRESS_MISTATUS_CHANGE_PENDING:
            case filogix.express.service.Request.EXPRESS_MISTATUS_CHANGES_SUBMITTED: {
                if ( getCmhcAlert() == true && 
                        getMIProviderId() == filogix.express.service.Request.EXPRESS_MIINSURER_CMHC) {
                    checkMIResponse();
                } else {
                    if( getGenworthAlert() == true &&
                            getMIProviderId() == filogix.express.service.Request.EXPRESS_MIINSURER_GENWORTH) {
                        checkMIResponse();
                    }
                }
                break;
            }
            default: 
                break;
        }
    }
}

/**
 * Check MIResponse every .. seconds.
 */
function sendTimingMIResponse() {
    miResponseInterval = setInterval("sendMIResponseRequest()", getFrequency());
}


/**
 * Clear Checking MIResponse.
 */
function clearTimingMIResponse() {
    
    if (miResponseInterval) clearInterval(miResponseInterval);
    var extractor = new filogix.express.service.MIResponse();
    extractor.timeOut = true;
    var miResponse = extractor.sendMIResponse();
}


/**
 * Check MIResponse every .. seconds.
 */
function checkMIResponseLoop() {
    sendMIResponseRequest();
    sendTimingMIResponse();
    setTimeout("clearTimingMIResponse()", getSessionTimeOut());
}

/**
 * Check 'checkMIResponse'
 */
function checkMIResponse(){

    if(getDealLockedStatus()) {
        var extractor = new filogix.express.service.MIResponse();
        extractor.timeOut = false;
        var miResponse = extractor.sendMIResponse();
        if (miResponse == null) {
            return;
        }

        var responseDvi = getResponseDiv();
        if(miResponse.pendingMIResponse) {
            responseDvi.style.visibility="visible";
            if (miResponseInterval) clearInterval(miResponseInterval);
        }
        
        //clear request when no deal lock exist
        if(!miResponse.dealLock) {
            setDealLockedStatus(miResponse.dealLock);
            if (miResponseInterval) clearInterval(miResponseInterval);
        }
    }
}


/**
 * returns the miresponse div
 */
function getResponseDiv() {
    return document.getElementById("miResponse");
}

/**
 * returns the frequency
 */
function getFrequency() {
    return document.getElementById("hdFrequency").value;
}

/**
 * returns the chmc alert proerty
 */
function getCmhcAlert() {
    var cmhc = document.getElementById("hdChmcAlert").value;
    if(cmhc == "false")
        return false;
    else 
        return true;
}

/**
 * returns the genworth alert property
 */
function getGenworthAlert() {
    var genWorth = document.getElementById("hdGenworthAlert").value;
    if(genWorth == "false")
        return false;
    else
        return true;
}

/**
 * set deal lock status.
 */
function getDealLockedStatus() {
    return document.getElementById("hdDealLocked").value;
}

/**
 * set deal lock status.
 */
function setDealLockedStatus(status) {
    document.getElementById("hdDealLocked").value = status;
}

/**
 * returns the mistatusid
 */
function getMIResponseStatusId() {
    return document.getElementById("hdMIResponseStatusId").value;
}

/**
 * returns the mi providerid
 */
function getMIProviderId() {
    return document.getElementById("hdMIProviderId").value;
}

/**
 * returns the session timeout value
 */
function getSessionTimeOut() {
    return document.getElementById("hdSessionTimeOut").value;
}