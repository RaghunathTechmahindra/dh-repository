filogix.gadget.DialogBase.imagePath = "../JavaScript/rc/images/";
// the message keys.
filogix.util.RichClientMessages.msgKeys = new Array();
filogix.util.RichClientMessages.msgKeys =
    ["SERVICE_AVM_TITLE", "SERVICE_AVM_PROMPT",
     "SERVICE_APPRAISAL_TITLE", "SERVICE_APPRAISAL_PROMPT",
     "SERVICE_APPRAISAL_REQUEST_BTN_FILE", "SERVICE_APPRAISAL_APPROVE_BTN_FILE",
     "SERVICE_MUST_PROVIDER_PRODUCT", "CHOOSE_PAGE"];

/**
 * returns the hidden dealid.
 */
function getDealId() {

    return document.getElementById("hdDealId").value;
}

/**
 * returns the hidden copyid.
 */
function getCopyId() {

    return document.getElementById("hdDealCopyId").value;
}

/**
 * set copy id.
 */
function setCopyId(newId) {

    document.getElementById("hdDealCopyId").value = newId;
}

/**
 * returns the hidden property id.
 */
function getPropertyId(index) {

    return document.getElementById("hdPropertyId" + index).value;
}

/**
 * returns the AVM provider select element.
 */
function getAVMProvider(index) {

    return document.getElementById("cbAVMProvider" + index);
}

/**
 * returns the AVM Product select element.
 */
function getAVMProduct(index) {

    return document.getElementById("cbAVMProduct" + index);
}

/**
 * handle the AVM Provider change.
 */
function avmProviderChange(index) {
    
    if(getViewOnlyTag()==null) {
        var extractor = new filogix.express.service.ServiceProductsExtractor();
        extractor.provider = getAVMProvider(index);
        extractor.product = getAVMProduct(index);
        // the AVM product
        extractor.productTypeId = 6;
        extractor.productSubTypeId = 1;
        extractor.handleProviderChange();
    }
    else {
        showDisallowedViewOnly();
    }
}

/**************************************
 * class AVMOrderRequest
 */

filogix.express.service.AVMOrderRequest = function() {

    // setting up the request type.
    this.requestType = "express.service.AVMOrderRequest";
};

filogix.express.service.AVMOrderRequest.prototype =
    new filogix.express.service.XHRServiceRequest();
filogix.express.service.AVMOrderRequest.superclass =
    filogix.express.service.XHRServiceRequest.prototype;

/**
 * update the transaction copy id.
 */
filogix.express.service.AVMOrderRequest.prototype.updateTransactionCopyId =
    function(newId) {

    // update the DOM of current page.
    setCopyId(newId);
}

/**
 * update the AVM section based on the response object.
 */
filogix.express.service.AVMOrderRequest.prototype.updatePageDOM =
    function(responseObj) {

    // for avm section we need update the avmStatus, avmDate, and avmStatusMsg.
    var avmStatus = document.getElementById("avmStatus" + this.tileIndex);
    var avmDate = document.getElementById("avmDate" + this.tileIndex);
    var avmStatusMsg = document.getElementById("avmStatusMsg" + this.tileIndex);
    var actualAppraisalValue = document.getElementById("tbActualAppraisal" +
                                                       this.tileIndex);

    // get info from the response object.
    if (responseObj.rcResponseContent.avmStatus) {
        avmStatus.innerHTML = responseObj.rcResponseContent.avmStatus;
    }
    if (responseObj.rcResponseContent.avmDate) {
        avmDate.innerHTML = responseObj.rcResponseContent.avmDate;
    }
    if (responseObj.rcResponseContent.avmStatusMsg) {
        avmStatusMsg.innerHTML = responseObj.rcResponseContent.avmStatusMsg;
    }
    if (responseObj.rcResponseContent.avmValue) {
        var actValue = (new Number(actualAppraisalValue.value)).valueOf();
        if (isNaN(actValue) || actValue <= 0) {
            actualAppraisalValue.value = responseObj.rcResponseContent.avmValue;
            // update the date use current date.
            var cbMonths = document.getElementById("cbMonths" + this.tileIndex);
            var tbDay = document.getElementById("tbDay" + this.tileIndex);
            var tbYear = document.getElementById("tbYear" + this.tileIndex);
    
            tbDay.value = responseObj.rcResponseContent.valuationDay;
            tbYear.value = responseObj.rcResponseContent.valuationYear;
            cbMonths.selectedIndex =
                responseObj.rcResponseContent.valuationMonth + 1;
        }
    }
}

/**
 * handle success response.
 */
filogix.express.service.AVMOrderRequest.prototype.handleSuccess =
    function(responseObj) {

    // call the super class mehtod to close the progress bar.
    filogix.express.service.AVMOrderRequest.
        superclass.handleSuccess.call(this, responseObj);

    // update the AVM Summary section.
    var tableAvmSummary =
        document.getElementById("avmSummary" + this.tileIndex);
    var reportTr = document.createElement("tr");
    reportTr.style.padding = "0px 0px 5px 0px";

    // report button.
    // TODO: add the implementation for viewing report.
    var buttonTd = document.createElement("td");
    buttonTd.setAttribute("valign", "top");
    buttonTd.setAttribute("align", "center");
    var href = document.createElement("a");
    href.setAttribute("href", "JavaScript: " + "viewAvmReport(" +
                      responseObj.rcResponseContent.responseId + ")");
    var reportImg = document.createElement("img");
    reportImg.setAttribute("src", "../images/view_report.gif");
    reportImg.setAttribute("alt", "View Report");
    reportImg.setAttribute("border", "0");
    href.appendChild(reportImg);
    buttonTd.appendChild(href);
    reportTr.appendChild(buttonTd);

    // provider name.
    var td = document.createElement("td");
    td.setAttribute("valign", "top");
    var font = document.createElement("font");
    font.setAttribute("size", "2");
    font.innerHTML = "&nbsp;&nbsp;" +
        responseObj.rcResponseContent.providerName;
    td.appendChild(font);
    reportTr.appendChild(td);

    // avm value.
    td = document.createElement("td");
    td.setAttribute("valign", "top");
    font = document.createElement("font");
    font.setAttribute("size", "2");
    font.innerHTML = "&nbsp;&nbsp;" +
        responseObj.rcResponseContent.strAvmValue;
    td.appendChild(font);
    reportTr.appendChild(td);

    // confidence level.
    td = document.createElement("td");
    td.setAttribute("valign", "top");
    font = document.createElement("font");
    font.setAttribute("size", "2");
    font.innerHTML = "&nbsp;&nbsp;" +
        responseObj.rcResponseContent.confidenceLevel;
    td.appendChild(font);
    reportTr.appendChild(td);

    // low and high avm range.
    td = document.createElement("td");
    td.setAttribute("valign", "top");
    font = document.createElement("font");
    font.setAttribute("size", "2");
    font.innerHTML = "&nbsp;&nbsp;" +
        responseObj.rcResponseContent.highAvmRange + "/" + 
        responseObj.rcResponseContent.lowAvmRange;
    td.appendChild(font);
    reportTr.appendChild(td);

    // response date.
    td = document.createElement("td");
    td.setAttribute("valign", "top");
    font = document.createElement("font");
    font.setAttribute("size", "2");
    font.innerHTML = "&nbsp;&nbsp;" +
        responseObj.rcResponseContent.responseDate;
    td.appendChild(font);
    reportTr.appendChild(td);

    var latestReportTr =
        document.getElementById("latestAVMReport" + this.tileIndex);
    tableAvmSummary.insertBefore(reportTr, latestReportTr);
    if (latestReportTr != null) {
        latestReportTr.setAttribute("id", null);
    }
    reportTr.setAttribute("id", "latestAVMReport" + this.tileIndex);
}
/**
 * class AVMOrderRequest END
 **************************************/

/**
 * AVM service request.
 */
function sendAVMRequest(index) {
    if(getViewOnlyTag()==null) {

        var provider = getAVMProvider(index);
        var product = getAVMProduct(index);
        if ((product.options.length < 1) ||
            (product.options[product.selectedIndex].value <= 0) ||
            (provider.selectedIndex <= 0)) {
            // show up the error message dialog.
            var msgs = new filogix.util.RichClientMessages();
            var alertDlg = new filogix.gadget.AlertDialogBox(".none");
            alertDlg.setTitle(msgs.getSystemMessage("SYSTEM_ERROR_TITLE"));
            alertDlg.setContent(msgs.getMessage("SERVICE_MUST_PROVIDER_PRODUCT"));
            alertDlg.show();
            alertDlg.moveTo(-1, -1);
            filogix.gadget.DialogHelper.activeDialog = alertDlg;
            return;
        }
    
        var avmRequest = new filogix.express.service.AVMOrderRequest();
        avmRequest.tileIndex = index;
    
        var messages = new filogix.util.RichClientMessages();
    
        avmRequest.dialogTitle = messages.getMessage("SERVICE_AVM_TITLE") +
            " - Express";
        var params = new Array();
        params[0] = provider.options[provider.selectedIndex].text;
        params[1] = product.options[product.selectedIndex].text;
        avmRequest.dialogPromptMsg = messages.getMessage("SERVICE_AVM_PROMPT",
                                                         params);
        // prepare the request object here.
        var request = new Object();
        request.rcRequestContent = new Object();
        // we don't really need provider id!
        request.rcRequestContent.providerId =
            provider.options[provider.selectedIndex].value;
        request.rcRequestContent.productId =
            product.options[product.selectedIndex].value;
        // deal id,
        request.rcRequestContent.dealId = getDealId();
        request.rcRequestContent.copyId = getCopyId();
        // property id.
        request.rcRequestContent.propertyId = getPropertyId(index);
        // set the requestId if exist one!
        request.rcRequestContent.requestId = -1;
        request.rcRequestContent.languageId = 0;
        avmRequest.requestContent = request;
        avmRequest.send();
    }
    else {
        showDisallowedViewOnly();
    }
}

/**
 * view the AVM report.
 */
function viewAvmReport(responseId) {
    if(getViewOnlyTag()==null) {

        // get a report viewer.
        var viewer = new filogix.express.service.ReportViewer();
        // prepare the request object.
        var request = new Object();
        request.rcRequestContent = new Object();
        request.rcRequestContent.responseId = responseId;
        request.rcRequestContent.reportType = "AVMSUMMARY";
    
        viewer.requestContent = request;
        viewer.viewReport();
    }
    else {
        showDisallowedViewOnly();
    }
}

/**************************************
 * utility functions to simulate the response.
 */

// current request id.
var currentRequestId = -1;

/**
 * simulate a error response.
 */
function avmErrorResponse() {

    // simulate a error response.
}

/**
 * simulate a success response.  For test only!
 */
function avmSuccessResponse() {

    var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
    // preparing the request object.
    var request = new Object();
    request.rcRequestAction =
        filogix.express.service.ServiceRequest.ACTION_CREATE_RESPONSE;
    request.rcRequestContent = new Object();
    request.rcRequestContent.requestId = currentRequestId;
    request.rcRequestContent.dealId = getDealId();
    request.rcRequestContent.copyId = getCopyId();
    // send out the request.
    xmlhttp.send("request=" + request.toJSONString());
    // we don't need the response.
}

/**
 *
 *************************************/

/**************************************************************************
 * appriasal functions.
 */

/**
 * get the appraisal provider.
 */
function getAppraisalProvider(index) {

    return document.getElementById("cbAppraisalProvider" + index);
}

/**
 * get the appraisal product.
 */
function getAppraisalProduct(index) {

    return document.getElementById("cbAppraisalProduct" + index);
}

/**
 * handle the appraisal provider change.
 * Change: Added prop parameter to function and call to toggle contact section
 */
function appraisalProviderChange(index, prop) {
    if(getViewOnlyTag()==null) {

        var extractor = new filogix.express.service.ServiceProductsExtractor();
        extractor.provider = getAppraisalProvider(index);
        extractor.product = getAppraisalProduct(index);
        // for appraisal product only.
        extractor.productTypeId = 6;
        extractor.productSubTypeId = 2;
        extractor.handleProviderChange();

        // added prop for READ functionality
        toggleAppraisalContactInfoSection(index, prop);
    }
    else {
        showDisallowedViewOnly();
    }
}

/**
 * get the primary borrower check box.
 */
function getPrimaryBorrowerCheckbox(index) {

    return document.getElementById("chPrimaryBorrowerInfo" + index);
}

/**
 * get property owner name.
 */
function getPropertyOwnerName(index) {

    return document.getElementById("stAOPropertyOwnerName" + index);
}

/**
 * get special instruction.
 */
function getSpecialInstruction(index) {

    return document.getElementById("tbAppraisalSpecialInst" + index);
}

/**
 * get appraisal request id.
 */
function getAppraisalRequestId(index) {

    var id = document.getElementById("stAppraisalRequestId" + index).value;
    return (new Number(id)).valueOf();
}

/**
 * set the appraisal requestid.
 */
function setAppraisalRequestId(index, id) {

    document.getElementById("stAppraisalRequestId" + index).value = id;
}

/**
 * get appraisal status id.
 */
function getAppraisalStatusId(index) {

    var id = document.getElementById("stAppraisalStatusId" + index).value;
    return (new Number(id)).valueOf();
}

/**
 * set the appraisal status id.
 */
function setAppraisalStatusId(index, id) {

    document.getElementById("stAppraisalStatusId" + index).value = id;
}

/**
 * get the servic contact id.
 */
function getAppraisalContactId(index) {

    var id = document.getElementById("stAppraisalContactId" + index).value;
    return (new Number(id)).valueOf();
}

/**
 * set appriasal contact id.
 */
function setAppraisalContactId(index, id) {

    document.getElementById("stAppraisalContactId" + index).value = id;
}

/**
 * get the borrower id.
 */
function getAppraisalBorrowerId(index) {

    var id = document.getElementById("stAppraisalBorrowerId" + index).value;
    return (new Number(id)).valueOf();
}

/**
 * set appriasal borrowerid
 */
function setAppraisalBorrowerId(index, id) {

    document.getElementById("stAppraisalBorrowerId" + index).value = id;
}

/*************************************
 * class AppraisalOrderRequest
 */
filogix.express.service.AppraisalOrderRequest = function() {

    // request type.
    this.requestType = "express.service.AppraisalOrderRequest";
}

filogix.express.service.AppraisalOrderRequest.prototype =
    new filogix.express.service.XHRServiceRequest();
filogix.express.service.AppraisalOrderRequest.superclass =
    filogix.express.service.XHRServiceRequest.prototype;

/**
 * update the copy id.
 */
filogix.express.service.AppraisalOrderRequest.prototype.updateTransactionCopyId =
    function(newId) {

    // update the DOM of current page.
    setCopyId(newId);
}

/**
 * update current page.
 */
filogix.express.service.AppraisalOrderRequest.prototype.updatePageDOM =
    function(responseObj) {

    // update ids.
    if (responseObj.rcResponseContent.requestId) {
        setAppraisalRequestId(this.tileIndex,
                              responseObj.rcResponseContent.requestId);
    }
    if (responseObj.rcResponseContent.appraisalStatusId) {
        setAppraisalStatusId(this.tileIndex,
                             responseObj.rcResponseContent.appraisalStatusId);
    }
    if (responseObj.rcResponseContent.serviceRequestContactId) {
        setAppraisalContactId(this.tileIndex,
                              responseObj.rcResponseContent.
                              serviceRequestContactId);
    }

    // update current page.
    var appraisalStatus = document.getElementById("appraisalStatus" +
                                                  this.tileIndex);
    var appraisalDate = document.getElementById("appraisalDate" +
                                                this.tileIndex);
    var appraisalRefNum = document.getElementById("appraisalRefNum" +
                                                  this.tileIndex);
    var appraisalStatusMsg =
        document.getElementById("tbAppraisalStatusMessages" + this.tileIndex);
    appraisalStatus.innerHTML = "";
    appraisalDate.innerHTML = "";
    appraisalRefNum.innerHTML = "";

    if (responseObj.rcResponseContent.appraisalStatus) {
        appraisalStatus.innerHTML =
            responseObj.rcResponseContent.appraisalStatus;
    }
    if (responseObj.rcResponseContent.requestDate) {
        appraisalDate.innerHTML = responseObj.rcResponseContent.requestDate;
    }
    if (responseObj.rcResponseContent.providerRefNum) {
        appraisalRefNum.innerHTML =
            responseObj.rcResponseContent.providerRefNum;
    }
    if (responseObj.rcResponseContent.appraisalStatusMsg) {
        appraisalStatusMsg.value =
            responseObj.rcResponseContent.appraisalStatusMsg;
    }
}

/**
 * handle success response.
 */
filogix.express.service.AppraisalOrderRequest.prototype.handleSuccess =
    function(responseObj) {

    // call the super class mehtod to close the progress bar.
    filogix.express.service.AVMOrderRequest.
        superclass.handleSuccess.call(this, responseObj);
}

/**
 * class AppraisalOrderRequest END
 **************************************/

/**
 * the appraisal request button.
 */
function getAppraisalRequestButton(index) {

    return document.getElementById("appraisalRequestButton" + index);
}

/**
 * show request.
 */
function showAppraisalRequest(index) {

    if (getAppraisalRequestButton(index)) {
        var message = new filogix.util.RichClientMessages();
        getAppraisalRequestButton(index).src = "../images/" +
            message.getMessage("SERVICE_APPRAISAL_REQUEST_BTN_FILE");
    }
}

/**
 * show request.
 */
function showAppraisalApprove(index) {

    if (getAppraisalRequestButton(index)) {
        var message = new filogix.util.RichClientMessages();
        getAppraisalRequestButton(index).src = "../images/" +
            message.getMessage("SERVICE_APPRAISAL_APPROVE_BTN_FILE");
    }
}

/**
 * toggle the appraisal image buttons.
 */
function toggleAppraisalButtons(index) {

    switch((new Number(getAppraisalStatusId(index))).valueOf()) {
    case filogix.express.service.ServiceRequest.EXPRESS_STATUS_QUOTATION_RECEIVED:
        showAppraisalApprove(index);
        break;
    default:
        showAppraisalRequest(index);
        break;
    }
}

/**
 * Appraisal order request.
 */
function sendAppraisalRequest(index, isCancel) {
    if(getViewOnlyTag()==null) {
        var provider = getAppraisalProvider(index);
        var product = getAppraisalProduct(index);
        if ((product.options.length < 1) ||
            (product.options[product.selectedIndex].value <= 0) ||
            (provider.selectedIndex <= 0)) {
            // show up the error message dialog.
            var msgs = new filogix.util.RichClientMessages();
            var alertDlg = new filogix.gadget.AlertDialogBox(".none");
            alertDlg.setTitle(msgs.getSystemMessage("SYSTEM_ERROR_TITLE"));
            alertDlg.setContent(msgs.getMessage("SERVICE_MUST_PROVIDER_PRODUCT"));
            alertDlg.show();
            alertDlg.moveTo(-1, -1);
            filogix.gadget.DialogHelper.activeDialog = alertDlg;
            return;
        }
    
        if (! isCancel) {
            isCancel = false;
        }
    
        var appraisalRequest = new filogix.express.service.AppraisalOrderRequest();
        appraisalRequest.tileIndex = index;
        var messages = new filogix.util.RichClientMessages();
        appraisalRequest.dialogTitle = messages.getMessage("SERVICE_APPRAISAL_TITLE")
            + " - Express";
        var params = new Array();
        params[0] = provider.options[provider.selectedIndex].text;
        params[1] = product.options[product.selectedIndex].text;
        appraisalRequest.dialogPromptMsg =
            messages.getMessage("SERVICE_APPRAISAL_PROMPT", params);
    
        // preparing the request object.
        var request = new Object();
        request.rcRequestContent = new Object();
        // ids.
        request.rcRequestContent.dealId = getDealId(index);
        request.rcRequestContent.copyId = getCopyId(index);
        request.rcRequestContent.propertyId = getPropertyId(index);
        request.rcRequestContent.requestId = getAppraisalRequestId(index);
        request.rcRequestContent.productId =
            product.options[product.selectedIndex].value;
        request.rcRequestContent.serviceRequestContactId =
            getAppraisalContactId(index);
        // cancel or not.
        request.rcRequestContent.isCancel = isCancel;
        // contact info.
        var borrowerCheckBox = getPrimaryBorrowerCheckbox(index);
        var contactInfo = borrowerCheckBox.parent;
        request.rcRequestContent.needPrimary = borrowerCheckBox.checked;
        request.rcRequestContent.firstName = contactInfo.firstName.value;
        request.rcRequestContent.lastName = contactInfo.lastName.value;
        request.rcRequestContent.ownerName = getPropertyOwnerName(index).value;
        request.rcRequestContent.emailAddress = contactInfo.email.value;
        // phone numbers.
        request.rcRequestContent.workPhoneArea = contactInfo.workPhoneArea.value;
        request.rcRequestContent.workPhoneNumber = contactInfo.workPhoneFirst.value +
            contactInfo.workPhoneLast.value;
        request.rcRequestContent.workPhoneExt = contactInfo.workPhoneExt.value;
        request.rcRequestContent.cellPhoneArea = contactInfo.cellPhoneArea.value;
        request.rcRequestContent.cellPhoneNumber = contactInfo.cellPhoneFirst.value +
            contactInfo.cellPhoneLast.value;
        request.rcRequestContent.homePhoneArea = contactInfo.homePhoneArea.value;
        request.rcRequestContent.homePhoneNumber = contactInfo.homePhoneFirst.value +
            contactInfo.homePhoneLast.value;
        // instruction.
        request.rcRequestContent.instruction = getSpecialInstruction(index).value;
    
        appraisalRequest.requestContent = request;
        appraisalRequest.send();
    }
    else {
        showDisallowedViewOnly();
    }
}

/**
 * view the Appraisal report.
 */
function viewAppraisalReport(responseId) {
    if(getViewOnlyTag()==null) {
    
        // get a report viewer.
        var viewer = new filogix.express.service.ReportViewer();
        // prepare the request object.
        var request = new Object();
        request.rcRequestContent = new Object();
        request.rcRequestContent.responseId = responseId;
        request.rcRequestContent.reportType = "APPRAISALSUMMARY";
    
        viewer.requestContent = request;
        viewer.contentType = "binary";
        viewer.viewReport();
    }
    else {
        showDisallowedViewOnly();
    }
}

/**
 * init contact.
 */
function initContact(index) {

    var contact = new filogix.express.cmps.ContactInfo();
    contact.firstName = document.getElementById("stAOContactFirstName" + index);
    contact.lastName = document.getElementById("stAOContactLastName" + index);
    // email.
    contact.email = document.getElementById("tbAOEmail" + index);
    // work phone.
    contact.workPhoneArea = document.getElementById("tbAOWorkPhoneAreaCode"
                                                    + index);
    contact.workPhoneFirst =
        document.getElementById("tbAOWorkPhoneFirstThreeDigits" + index);
    contact.workPhoneLast =
        document.getElementById("tbAOWorkPhoneLastFourDigits" + index);
    contact.workPhoneExt = document.getElementById("tbAOWorkPhoneNumExtension"
                                                   + index);
    // cell phone.
    contact.cellPhoneArea = document.getElementById("tbAOCellPhoneAreaCode"
                                                    + index);
    contact.cellPhoneFirst =
        document.getElementById("tbAOCellPhoneFirstThreeDigits" + index);
    contact.cellPhoneLast =
        document.getElementById("tbAOCellPhoneLastFourDigits" + index);
    // home phone
    contact.homePhoneArea = document.getElementById("tbAOHomePhoneAreaCode"
                                                    + index);
    contact.homePhoneFirst =
        document.getElementById("tbAOHomePhoneFirstThreeDigits" + index);
    contact.homePhoneLast =
        document.getElementById("tbAOHomePhoneLastFourDigits" + index);

    contact.propertyOwnerName = 
        document.getElementById("stAOPropertyOwnerName" + index);

    // the primary borrower check box.

    contact.primaryBorrowerCheckbox = getPrimaryBorrowerCheckbox(index);
    contact.init();
}

/**
 * hide display for the contact info section.
 **Change : Modified the method to take 'prop' as input parameter for Sumbit to Read functionality 18-Oct-2006
 */
function toggleAppraisalContactInfoSection(index, prop) {

    var apprTypeElement = document.getElementById("cbAppraisalProduct" + index);

    var divApprContactInfo = document.getElementById("apprContactInfoTable" + index );

    if(apprTypeElement.value == 4 || prop ) {

        divApprContactInfo.style.display='inline';
    } else {

        divApprContactInfo.style.display='none';
    }
}

/**
 * initializing each tile.
 */
function initialize(index, prop) {
    initContact(index);
    if(getViewOnlyTag()==null) {
        if(document.getElementById("cbAppraisalProduct" + index)){
            // NBC/PP - Defect #1419, added prop parameter to call to toggle contact section
            toggleAppraisalContactInfoSection(index, prop);
            toggleAppraisalButtons(index);
        }
    }
    else {
        showViewOnlyButton(index);
    }
        
}   
