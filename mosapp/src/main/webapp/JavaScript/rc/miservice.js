filogix.gadget.DialogBase.imagePath = "../JavaScript/rc/images/"; 
// the message keys.
filogix.util.RichClientMessages.msgKeys = new Array();
filogix.util.RichClientMessages.msgKeys = ["SERVICE_MI_TITLE", "CHOOSE_PAGE"];
    
var MI_REQUEST_TYPE = "miProcessor";
    
var MI_CANCELATION_SUBMITTED = false;

/**
 * returns the hidden dealid.
 */
function getDealId() {

    return document.getElementById("hdDealId").value;
}

/**
 * returns the hidden copyid.
 */
function getCopyId() {

    return document.getElementById("hdDealCopyId").value;
}

/**
 * returns the hidden property id.
 */
function getPropertyId() {

    return document.getElementById("hdPropertyId").value;
}

/**
 * set copy id.
 */
function setCopyId(newId) {

    document.getElementById("hdDealCopyId").value = newId;
}

function getDealStatusId() {
	return document.getElementsByName("pgMortgageIns_hdDealStatusId")[0].value;
}

function getMIStatusId() {
	var miStatuses = document.getElementById("cbMIStatus");
	return miStatuses.options[miStatuses.options.selectedIndex].value;
}
    

/**************************************
 * class AIGUGOrderRequest
 */

filogix.express.service.MIOrderRequest = function() {
	
    // setting up the request type.
    this.requestType = MI_REQUEST_TYPE;
};

filogix.express.service.MIOrderRequest.prototype =
    new filogix.express.service.XHRServiceRequest();
filogix.express.service.MIOrderRequest.superclass =
    filogix.express.service.XHRServiceRequest.prototype;
    
    
/**
 * update the transaction copy id.
 */
filogix.express.service.MIOrderRequest.prototype.updateTransactionCopyId =
    function(newId) {

    // update the DOM of current page.
    setCopyId(newId);
}

/**
 * handle success response.
 */
filogix.express.service.MIOrderRequest.prototype.handleSuccess =
    function(responseObj) {
    // updateing the dom.	
    var miResponse = document.getElementById("miResponseArea");
    var miPremiumAmount = document.getElementById("txMIPremium");
    var miStatusMsg = document.getElementById("cbMIStatus");
    var miPolicyNumber = document.getElementById("miPolicyNumber");
    var mICertificate = document.getElementById("txMIPolicyNo");
    
    if(responseObj.rcResponseContent.miResponse) {
    	miResponse.value = responseObj.rcResponseContent.miResponse;
    }
   	if(responseObj.rcResponseContent.miPremiumAmount) {
    	miPremiumAmount.value = responseObj.rcResponseContent.miPremiumAmount;
    }
    if(responseObj.rcResponseContent.miStatusId) {
    	var option = miStatusMsg.options[responseObj.rcResponseContent.miStatusId];
    	option.selected = true;
    }
    if(responseObj.rcResponseContent.miPolicyNumber) {
    	miPolicyNumber.value = responseObj.rcResponseContent.miPolicyNumber;
    }
    if(responseObj.rcResponseContent.miPolicyNumAIGUG) {
    	mICertificate.value = responseObj.rcResponseContent.miPolicyNumAIGUG;
    }
    
    // call the super class mehtod to close the progress bar.
    filogix.express.service.MIOrderRequest.
        superclass.handleSuccess.call(this, responseObj);
        

	var dlg = filogix.gadget.DialogHelper.activeDialog;
	var button = dlg.buttonDIV.getElementsByTagName("BUTTON")[0];
	button.onclick = function() {
	   if(MI_COLLAPSED_CANCELATION == true) { 
	       MI_COLLAPSED_CANCELATION == false
	  		dlg.hide();
	  		return(false);
	   }
	   document.forms[0].submit();

	}

    // close the dialog by clicking on X close button.
	var closeLink = dlg.container.getElementsByTagName("a")[0];
	closeLink.onclick = function() {
		document.forms[0].submit();
	}
	
//	dlg.buttonDIV.getElementsByTagName("BUTTON")[0].onclick = tool_click(5);
}

filogix.express.service.MIOrderRequest.prototype.handleTimeout =
	function() {
		filogix.express.service.MIOrderRequest.
            superclass.handleTimeout.call(this);
            
        var dlg = filogix.gadget.DialogHelper.activeDialog;
		var button = dlg.buttonDIV.getElementsByTagName("BUTTON")[0];
		button.onclick = function() {
		    if(MI_COLLAPSED_CANCELATION == true) { 
	           MI_COLLAPSED_CANCELATION == false
	  		   dlg.hide();
	  		   return(false);
	       }
		   document.forms[0].submit();

		}
		
		// close the dialog by clicking on X close button.
		var closeLink = dlg.container.getElementsByTagName("a")[0];
	    closeLink.onclick = function() {
		    document.forms[0].submit();
	    }
}


filogix.express.service.MIOrderRequest.prototype.handleError =
	function(responseObj) {
	filogix.express.service.MIOrderRequest.
            superclass.handleError.call(this, responseObj);
            
        var dlg = filogix.gadget.DialogHelper.activeDialog;
		var button = dlg.buttonDIV.getElementsByTagName("BUTTON")[0];
		    button.onclick = function() {
		    	if(MI_COLLAPSED_CANCELATION == true) { 
	                MI_COLLAPSED_CANCELATION == false
	  		        dlg.hide();
	  		        return(false);
	            }
				document.forms[0].submit();
		}	
		
		// close the dialog by clicking on X close button.
		var closeLink = dlg.container.getElementsByTagName("a")[0];
	    closeLink.onclick = function() {
		    document.forms[0].submit();
	    }
}


filogix.express.service.MIOrderRequest.prototype.handleHttpError =
	function(responseObj) {
		filogix.express.service.MIOrderRequest.
            superclass.handleHttpError.call(this, responseObj);
            
        var dlg = filogix.gadget.DialogHelper.activeDialog;
		var button = dlg.buttonDIV.getElementsByTagName("BUTTON")[0];
		    button.onclick = function() {
		    if(MI_COLLAPSED_CANCELATION == true) { 
	           MI_COLLAPSED_CANCELATION == false
	  		   dlg.hide();
	  		   return(false);
	       }
		    document.forms[0].submit();
		}
		
		// close the dialog by clicking on X close button.
		var closeLink = dlg.container.getElementsByTagName("a")[0];
	    closeLink.onclick = function() {
		    document.forms[0].submit();
	    }	
}

filogix.express.service.MIOrderRequest.prototype.reloadPage=function() {
	window.location.reload(true);
}

filogix.express.service.MIOrderRequest.prototype.updatePageDOM =
    function(responseObj) {
/*
 * The dom updating is perdormed now in handleSuccess function right
 * before hiding the progress bur and show the success message.
 */    	
//    var miResponse = document.getElementById("miResponseArea");
//    var miPremiumAmount = document.getElementById("txMIPremium");
//    var miStatusMsg = document.getElementById("cbMIStatus");
//    var miPolicyNumber = document.getElementById("miPolicyNumber");
//    
//    if(responseObj.rcResponseContent.miResponse) {
//    	miResponse.innerHTML = responseObj.rcResponseContent.miResponse;
//    }
//   if(responseObj.rcResponseContent.miPremiumAmount) {
//    	miPremiumAmount.innerHTML = responseObj.rcResponseContent.miPremiumAmount;
//    }
//    if(responseObj.rcResponseContent.miStatusId) {
//    	var option = miStatusMsg.options[responseObj.rcResponseContent.setMiStatusId];
//    	option.selected = true;
//    }
//    if(responseObj.rcResponseContent.miPolicyNumber) {
//    	miPolicyNumber.innerHTML = responseObj.rcResponseContent.miPolicyNumber;
//    }
}

/**
 * These two overriden methods will be used for imlementig AJAX 
 * requests to the server without the AJAX roundtrip.
 * AIGUGProcessor class has been changed so, that it is a stateless
 * pooled object;
 * Serghei:: April 24, 2007 
 */
filogix.express.service.MIOrderRequest.prototype.checkProgress = 
function() {
	
	var self = this;
        // clear the timeout for each waiting, and update the progress.
        window.clearInterval(this.requestMonitor.timerId);
        this.requestMonitor.currentPercent += this.requestMonitor.percentStep;
        this.pbDialog.setProgressPercent(this.requestMonitor.currentPercent);
        
        if (this.requestMonitor.currentPercent < 1.0) {
        
        	if(filogix.express.service.RequestMonitor.STATUS_COMPLETE == this.requestMonitor.status) {
        	    this.requestMonitor.currentPercent = 1.0 - this.requestMonitor.percentStep;
        	    return;
        	}
        
        	this.requestMonitor.timerId = window.setInterval(function() {self.checkProgress();}, this.requestMonitor.timeInterval);
        	this.sendRequest();
        }
        else {
            this.handleTimeout();
        }
        
        
}

filogix.express.service.MIOrderRequest.prototype.handleHttpResponse = 
function(responseObj) {
	
	 var obj = responseObj.responseText.parseJSON();
	 
	 this.updatePageDOM(obj);
	 
	 switch(obj.rcResponseStatus) {
	 	case filogix.express.service.ServiceRequest.STATUS_RESPONSE_SUCCESS:

            this.requestMonitor.status = filogix.express.service.RequestMonitor.STATUS_COMPLETE;
	 		// create the request successfully.
	     	// get the request id from response object and set it to the request
	     	// content.
	     	this.requestContent.rcRequestContent.requestId = obj.rcResponseContent.requestId;
	     	// we also need to get the new transaction copy id, and update it on
	     	// the current page
	     	this.requestContent.rcRequestContent.copyId = obj.rcResponseContent.copyId;
	     	this.updateTransactionCopyId(obj.rcResponseContent.copyId);
	     
	     	this.handleSuccess(obj);
	     	
	     	break;
	     	
	     case filogix.express.service.ServiceRequest.STATUS_RESPONSE_FAILURE:
	     
			this.requestMonitor.status = filogix.express.service.RequestMonitor.STATUS_COMPLETE;
	     	this.handleError(obj);
            break;
            
	 }
}


/**
 * class MIOrderRequest END
 **************************************/  
 
 
 /**
 * MI service request.
 */
function sendMIRequest() {
        
        // prepare the request object
		var miRequest = new filogix.express.service.MIOrderRequest();

        // get title name		
		var insurers = document.getElementById("cbMIInsurer");
		var params = new Array();
        params[0] = insurers.options[insurers.options.selectedIndex].text;
		var messages = new filogix.util.RichClientMessages();
		miRequest.dialogTitle = messages.getMessage("SERVICE_MI_TITLE", params);
		miRequest.dialogPromptMsg = "";
		
		// set data
		var request = new Object();
		request.rcRequestContent = new Object();
		request.rcRequestContent.dealId = getDealId();
		request.rcRequestContent.copyId = getCopyId();
		var insurer = insurers.options[insurers.options.selectedIndex].value;
		request.rcRequestContent.productId = insurer;
		request.rcRequestContent.dealStatusId = getDealStatusId();
		request.rcRequestContent.miStatusId	= getMIStatusId();
		request.rcRequestContent.requestId = -1;
		request.rcRequestContent.languageId = 0;
		miRequest.requestContent = request;
		
		// off you go
		miRequest.send();
} 
