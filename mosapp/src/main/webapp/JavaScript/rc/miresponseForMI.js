(function() {

    var timer = null;
    var requestType = "express.service.MIResponse";

    function checkAsyncMIResponse(timeOut) {

        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();

        var request = {};
        request.rcRequestContent = {};
        request.rcRequestContent.timeOut = timeOut;

        xmlhttp.send("requestType=" + requestType + "&requestContent=" + request.toJSONString());

        if(xmlhttp.status != 200 || xmlhttp.responseText.indexOf("pgSessionEnded") != -1) {
            return null;
        }

        var resp = xmlhttp.responseText.parseJSON().rcResponseContent;
        return resp;
    }

    /**
     * Check send out miresponse alert checking.
     */
    function checkAsyncMIResponseRequest() {

        var pagebodyItem = document.all.pagebody.style;
        if(pagebodyItem.visibility == 'visible') {

            var miResponse = checkAsyncMIResponse(false);
            if(!miResponse) {
                return;
            }

            if(miResponse.pendingMIResponse) {
                // display Process Pending MI button
                var processMIBtn = document.getElementById("btProcessMI");
                if(processMIBtn) {
                    processMIBtn.style.visibility = "visible";
                }
                // display notification label
                if(checkAlert()) {
                    var miLabel = document.getElementById("miResponse");
                    if(miLabel) {
                        miLabel.style.visibility = "visible";
                    }
                }
                // hide all elements with class="offWithMIResponse"
                var arrElements = filogix.util.getElementsByClass("offWithMIResponse", "input");
                for(var i = 0; i < arrElements.length; i++) {
                    arrElements[i].style.visibility = "hidden";
                }
                if(timer) {
                    clearInterval(timer);
                }
            }
        }
    }

    /**
     * returns the chmc and ge alert proerty
     */
    function checkAlert() {
        var cmhc = document.getElementById("hdChmcAlert").value;
        var genworth = document.getElementById("hdGenworthAlert").value;
        return (cmhc != "false" || genworth != "false");
    }

    /**
     * Clear Checking AsyncMIResponse.
     */
    function clearTimingMIResponse() {
        if(timer) {
            clearInterval(timer);
        }
        checkAsyncMIResponse(true);
    }

    function main() {
        checkAsyncMIResponseRequest();
        timer = setInterval(checkAsyncMIResponseRequest, document.getElementById("hdFrequency").value);
        setTimeout(clearTimingMIResponse, document.getElementById("hdSessionTimeOut").value);
    }

    // execute main with onload event
    window.attachEvent("onload", main);

})();
