var ajaxLogger = {//ajaxLogger

	isDebugMode: false,
	
	/** write debug log. */
	debug:function(message){ //ajaxLogger.debug
		if(!this.isDebugMode)return;
		this._send(message, 0); 
	},
		
	/** write info log. */
	info:function(message){ //ajaxLogger.info
		this._send(message, 1);
	},
	
	/** write error log. */
	error:function(message){ //ajaxLogger.error
		this._send(message, 2);
	},

	/** XMLHttpRequest object */
	xhrObj:null,
	
	/** sends ajax request */
	_send:function(msgObj, type) { //ajaxLogger._send

		if(msgObj == "" || msgObj == null) return;
		if(type==null) type = 2; //default error

		try{
			if(!this.xhrObj){

				try {
					this.xhrObj = new XMLHttpRequest(); //for non IE browsers.
				} catch(e) {
					var msXMLActiveXIds = 
						[ 'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP.4.0',
							'MSXML2.XMLHTTP.5.0', 'MSXML2.XMLHTTP', 'Microsoft.XMLHTTP' ];
					for(var i=0; i < msXMLActiveXIds.length; ++i) {
						try {
							this.xhrObj = new ActiveXObject(msXMLActiveXIds[i]); //for IE
						} catch(e) {
							// do nothing here, just go to next one.
						}
					}
				}
			}

			// getting session id
			var jsessionid = document.forms[0].action;
			if (jsessionid && jsessionid.indexOf(";") > 0) {
				jsessionid = jsessionid.substr(jsessionid.indexOf(";"));
			} else {
				jsessionid = "";
			}
			// for JATO.
			var requestUrl = "AJAXDispatching" + jsessionid;
			//send as sync, otherwise it maight brake other ajax requests
			this.xhrObj.open("POST", requestUrl, false); //false=sync
			this.xhrObj.setRequestHeader("Content-Type",
			"application/x-www-form-urlencoded; charset=UTF-8");

			var request = new Object();
			request.rcRequestContent = new Object();
			request.rcRequestContent.logLevel = type;
			if(type === 2){
				//adding stacktrace on error
				request.rcRequestContent.stackTrace = 
					stackTrace(arguments.callee.caller.caller).toJSONString();
			}
			request.rcRequestContent.message = msgObj.toJSONString();

			//send message directly without using JSON
			this.xhrObj.send("requestType=ajaxLogger&requestContent=" + request.toJSONString());

		}catch(e){
			//just ignore exceptions. 
		}
	}
};

/**
 * returns stack trace from event starting method to this method
 * (not to the method error happend).
 * 
 * It is probably impossible to get Error's stack trace with IE. 
 * Instead, this method tries to collect caller functions of this 
 * method, and thus it can only show the trace from event 
 * starting method to this method. So, don�t be confused. this 
 * method doesn�t describe where error happened.
 * 
 */
function stackTrace(){
	try{
		var isEmpty = function(s){
			return (s==null) || (s.length == 0);
		};
		var funcName = function(f){
			try{
				//this way returns pure function name.
				//var s = ("" + f).match(/function (\w*)/)[1];

				//this way returns 1st line of function including comment
				var fStr = "" + f;
				var name = fStr.substring(0, fStr.indexOf('\n')); 
			}catch(ignore){}
			return isEmpty(name) ? "[anonymous]" : name;
		};
		var stack = "";
		for(var c = arguments.callee.caller; c != null; c = c.caller){
			stack = " >> " + funcName(c) + stack ;
		}
		return stack;
	}catch(e){
		return "n/a";	
	}
}

//catch all errors
//window.onerror = function(msg,url,line){
//    ajaxLogger.error("ajaxLogger - window.onerror caugth JavaScript Error :"
//            + msg + " file:" + url + " line:" + line);
//    return false;
//};
