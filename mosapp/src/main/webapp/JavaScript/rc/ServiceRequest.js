DialogBase.imagePath = "../rc/images/";

// the progress bar dialog.
var pbDialog = null;

// for the progress bar.
var timerId;
var totalPercent = 0;
var percentIncres = 0.05;
var interval = 1000; //unit is ms.

var response;
var ocsurl = "../try/ocsRequest?providerId=1";

var cbProvider;
var cbProduct;

/**
 * outsourced closing service request.
 */
function sendOCSRequest() {

    response = "";
    cbProvider = document.getElementById("cbOCSProvider");
    cbProduct = document.getElementById("cbOCSProduct");
    // show the progress bar first.
    if (pbDialog == null) {
        pbDialog = new ProgressBarDialog(AlertDialogBox.infoIconFile);
        pbDialog.setTitle("Send Request - Express");
    }
    pbDialog.setPromptMsg("Sending out the request. Please wait for the response...");
    totalPercent = 0;
    // disable the combobox.
    disableSelectElements(true);
    //cbProvider.disabled = true;
    //cbProduct.disabled = true;
    // show the progress bar.
    pbDialog.show();
    pbDialog.moveTo(-1, -1);
    // check the response.
    checkResponse();
}

/**
 * send out the xmlhttp request.
 */
function sendXMLHttpRequest() {

    var d = new Date();
    xmlhttp.open("POST", ocsurl, false);
    //xmlhttp.onreadystatechange = populateResponse;
    xmlhttp.send("timestamp=" + d.getMilliseconds());
    var strResult = xmlhttp.responseText;
    if (strResult == "nothing") {
        // keep checking.
        //sendXMLHttpRequest();
        response = "";
    } else {
        // stop the progressbar and populate the response to page.
        response = "done";
        if (pbDialog) {
            pbDialog.setProgressPercent(1.0);
            pbDialog.hide();
        }
        // enable the combobox.
        disableSelectElements(false);
        //cbProvider.disabled = false;
        //cbProduct.disabled = false;
        alert(strResult);
    }
}

/**
 * check the response.
 */
function checkResponse() {

    clearTimeout(timerId);
    totalPercent += percentIncres;

    if (pbDialog) {
        pbDialog.setProgressPercent(totalPercent);
    }

    if (totalPercent < 1.0) {
        if (response != "done") {
            timerId = setTimeout("checkResponse()", interval);
            sendXMLHttpRequest();
        }
    } else {
        pbDialog.hide();
        // enable the combobox.
        disableSelectElements(false);
        //cbProvider.disabled = false;
        //cbProduct.disabled = false;
    }
}

/**
 * populate the response to the page.
 */
function populateResponse() {

    // parse the response text and populate the response to the page.
    if (xmlhttp.readyState == 4) {
        var strResult = xmlhttp.responseText;
        if (strResult == "nothing") {
            // keep checking.
            //sendXMLHttpRequest();
            response = "";
        } else {
            // stop the progressbar and populate the response to page.
            response = "done";
            if (pbDialog) {
                pbDialog.setProgressPercent(1.0);
                pbDialog.hide();
            }
            alert(strResult);
        }
    }
}
