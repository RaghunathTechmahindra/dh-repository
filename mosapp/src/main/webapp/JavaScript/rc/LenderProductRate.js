 /*
 * @(#)LenderProductRate.js (LPR) Nov 28, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 *
 * @author MCM Impl Team.
 * @version 1.0 Initial version from Filogix.
 * @version 1.1 Aug 15, 2008 artf762702, added isUmbrella parameter
 */
filogix.express.service.LPRProcessorExtractor = function(){

    this.requestType = "express.service.LenderProductRateRequest";
    this.subRequestType;    
    this.dealId;
    this.dealCopyId;
    this.lenderProfileId;
    this.productId;
    this.dealRateStatusId;
    this.applicationDate;
    this.rateDisabledDate;
    this.rateFormat;
    this.isUmbrella = "Y";
}


filogix.express.service.LPRProcessorExtractor.prototype = {

    getLendersString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "getLendersString";
        return this.sendLPRRequest(request).lendersString;
    },

    getProductsString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "getProductsString";
        return this.sendLPRRequest(request).productString;
    },
	
	getQualifyingProductsString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "getQualifyingProductsString";
        return this.sendLPRRequest(request).qualifyingProductString;
    },

    getQualifyProductRateString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "getQualifyingProductRateString";
        return this.sendLPRRequest(request).qualifyProductRateString;
    },
	
    getPaymentTermString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "getPaymentTermString";
        return this.sendLPRRequest(request).paymentTermString
    },

    getRateCodeString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "getRateCodeString";
        return this.sendLPRRequest(request).rateCodeString;
    },

    getLenderProductRateString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "getLenderProductRateString";
        return this.sendLPRRequest(request).lenderProductRateString;
    },
    /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4*******************/
    /**
     *  extracts component eligible check result
     */
    getComponentEligibleCheckString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.componentEligibleFlag";
        return this.sendLPRRequest(request).componentEligibleFlag; 
    },
    /**
     *  extracts components size
     */
    getCountComponentsString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.countComponents";
        return this.sendLPRRequest(request).countComponents;
    },
   /**
     *  extracts productType data for productType combobox
     */
    getProductTypeString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.productType";
        return this.sendLPRRequest(request).productType;
    },
    /**
     *  extracts repaymenttype data for repayemnttype combobox
     */
    getRepaymentTypeString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.repaymentType";
        return this.sendLPRRequest(request).repaymentTypes;
    },
     /**
     * the XMLHttpRequest object.
     */
    xmlHttpLPR:null,
    
    sendLPRRequest: function(request){
    	
		if(!this.xmlHttpLPR){ 
			this.xmlHttpLPR = filogix.util.ConnectManager.getJatoXmlHttpObject();
		}

        // send out the request.
        this.xmlHttpLPR.send("requestType=" + this.requestType + "&requestContent=" + request.toJSONString());
        
        if (this.xmlHttpLPR.status != 200) {
            ajaxLogger.error("LenderProductRate.js faild in sendLPRRequest, http status = "
            	+ this.xmlHttpLPR.status + ", requestType=" + this.requestType + ", content=" + request.toJSONString());
        }
        
        var resp = this.xmlHttpLPR.responseText.parseJSON().rcResponseContent;
        return resp;
    },

    createDefaultRequestObject: function(){
        var request = new Object();
        request.rcRequestContent = new Object();
        request.rcRequestContent.subRequestType = "";
        request.rcRequestContent.languageId = LANGUAGECODE;
        request.rcRequestContent.lenderProfileId = this.lenderProfileId;
        request.rcRequestContent.productId = this.productId;
        request.rcRequestContent.dealId = this.dealId;
        request.rcRequestContent.dealCopyId = this.dealCopyId;
        request.rcRequestContent.dealRateStatusId = this.dealRateStatusId;
        request.rcRequestContent.applicationDate = this.applicationDate;
        request.rcRequestContent.rateDisDate = this.rateDisabledDate;
        request.rcRequestContent.rateFormat = this.rateFormat;
        request.rcRequestContent.isUmbrella = this.isUmbrella;
        return request;
    }
}
