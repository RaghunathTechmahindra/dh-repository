// the message keys.
filogix.util.RichClientMessages.sysMsgKeys = new Array();
filogix.util.RichClientMessages.sysMsgKeys =
    ["SYSTEM_ERROR_TITLE", "SYSTEM_TIMEOUT_TITLE", "SYSTEM_TIMEOUT_MESSAGE",
     "SYSTEM_SUCCESS_TITLE", "SYSTEM_SUCCESS_MESSAGE", "HTTP_ERROR_MESSAGE",
     "ERRORS_AND_WARNINGS_WINDOW_TITLE",
     "NO_PRODUCTS_FOR_PROVIDER", "SERVICE_PRINT_BTN_FILE",
     "DISALLOWED_DURING_VIEW_ONLY_MODE", "MI_ERROR_SYSTEM_ANAVAILABLE"];

/**************************************
 * class XHRServiceRequest
 */

/**
 * constructor.
 */
filogix.express.service.XHRServiceRequest = function() {

    this.type = "service request";

    this.dialogTitle = "title";

    this.dialogPromptMsg = "msg"; /** * default is english: 0;
     * it's value should be consistent with the language id in HttpSession on
     * server side.
     */
    this.languageId = 0;

    /**
     * the post data.
     */
    this.requestType = null;
    this.requestContent = null;
};

filogix.express.service.XHRServiceRequest.prototype = {

    /**
     * send out the request.
     */
    send:function() {

        // create the progress bar dialog, set up the title, prompt message.
        if (this.pbDialog == null) {
            this.pbDialog =
                new filogix.gadget.ProgressBarDialog(
                               filogix.gadget.AlertDialogBox.infoIconFile);
            filogix.gadget.DialogHelper.activeDialog = this.pbDialog;
        }
        this.pbDialog.setTitle(this.dialogTitle);
        this.pbDialog.setPromptMsg(this.dialogPromptMsg);

        // create a request monitor.
        // TODO: how to set the time out and interval?
        this.requestMonitor =
            new filogix.express.service.RequestMonitor(0, 0.0055, 1000);
    
        // show up the progress bar,
        this.pbDialog.show();
        this.pbDialog.moveTo(-1, -1);

        // check the progress.
        this.checkProgress();
    },

    /**
     * send out an asynchronous XMLHttpRequest.
     */
    sendRequest:function() {

        if (this.xmlhttp == null) {
            // get a XMLHttpRequest object!
            this.xmlhttp =
                filogix.util.ConnectManager.getJatoXmlHttpObject(true);
            // send the post data.  the JSON implementation.
            this.xmlhttp.send("requestType=" + this.requestType +
                              "&requestContent=" + this.requestContent.toJSONString());
        }

        // checking the ready state.
        if (this.xmlhttp.readyState == 4) {
            // got the response from server.
            // handle the HTTP response status, update the request monitor.
            var httpStatus = this.xmlhttp.status;
            if (httpStatus == 200) {
                var respObj = this.createResponseObject(this.xmlhttp);
                this.handleHttpResponse(respObj);
            } else {
                // here are some http error!
                var respObj = this.createResponseObject(this.xmlhttp);
                this.handleHttpError(respObj);
            }
            // clean the xmlhttp object.
            this.xmlhttp = null;
        }
    },

    /**
     * send out a synchronous request.
     */
    sendRequestSync:function() {

        // get a XMLHttpRequest object!
        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
        // send the post data.  the JSON implementation.
        this.xmlhttp.send("requestType=" + this.requestType +
                          "&requestContent=" +
                          this.requestContent.toJSONString());

        // handle the HTTP response status, update the request monitor.
        var httpStatus = xmlhttp.status;
        if (httpStatus == 200) {
            var respObj = this.createResponseObject(xmlhttp);
            this.handleHttpResponse(respObj);
        } else {
            // here are some http error!
            var respObj = this.createResponseObject(xmlhttp);
            this.handleHttpError(respObj);
        }
    },

    /**
     * the progress bar will keep checking this until time out.
     */
    checkProgress:function() {

        var self = this;

        // clear the timeout for each waiting, and update the progress.
        window.clearInterval(this.requestMonitor.timerId);
        this.requestMonitor.currentPercent += this.requestMonitor.percentStep;
        this.pbDialog.setProgressPercent(this.requestMonitor.currentPercent);

        // handle the request status.
        if (this.requestMonitor.currentPercent < 1.0) {
            switch (this.requestMonitor.status) {
            case filogix.express.service.RequestMonitor.STATUS_INITIAL:
                // set up timeout.
                this.requestMonitor.timerId = window.setInterval(
                    function() {
                        self.checkProgress();
                    }, this.requestMonitor.timeInterval / 2);
                // do nothing just skip it. in order to let the dialog show up.
                this.requestMonitor.status =
                    filogix.express.service.RequestMonitor.STATUS_CREATING;
                break;
            case filogix.express.service.RequestMonitor.STATUS_CREATING:
                this.requestMonitor.timerId = window.setInterval(
                    function() {
                        self.checkProgress();
                    }, this.requestMonitor.timeInterval);
                // try to create a new requst.
                this.requestContent.rcRequestAction =
                    filogix.express.service.ServiceRequest.ACTION_CREATE_REQUEST;
                this.sendRequest();
                break;
            case filogix.express.service.RequestMonitor.STATUS_WAITING:
                this.requestMonitor.timerId = window.setInterval(
                    function() {
                        self.checkProgress();
                    }, this.requestMonitor.timeInterval);
                // Try to check the result.
                this.requestContent.rcRequestAction =
                    filogix.express.service.ServiceRequest.ACTION_CHECK_RESPONSE;
                this.sendRequest();
                break;
            case filogix.express.service.RequestMonitor.STATUS_ERROR:
            case filogix.express.service.RequestMonitor.STATUS_COMPLETE:
                this.requestMonitor.currentPercent =
                    1.0 - this.requestMonitor.percentStep;
                break;
            }
        } else {
            // this is time out.
            this.handleTimeout();
        }
    },

    /**
     * create response object based on the XMLHttpRequest response.
     */
    createResponseObject:function(obj) {

        var respObj = {};
        respObj.statusText = obj.statusText;
        respObj.responseText = obj.responseText;
        respObj.responseXML = obj.responseXML;

        return respObj;
    },

    /**
     * update the transaction copy id to sychronize with the session status on
     * server side.
     */
    updateTransactionCopyId:function(newId) {

        // should update the value for a hidden field which stores the
        // transaction copy id.
        // subclass should override this method.
    },

    /**
     * method to update current page info: static text, fields, etc. based on
     * the specified response object.
     */
    updatePageDOM:function(obj) {

        // implements the logic here to update your pages info.
    },

    /**
     * handle the timeout.
     */
    handleTimeout:function() {

        // show up the time out message, and provide options.
        this.pbDialog.hide();
        var alertDlg = new filogix.gadget.AlertDialogBox(".none");
        var messages = new filogix.util.RichClientMessages();
        alertDlg.setTitle(messages.getSystemMessage("SYSTEM_TIMEOUT_TITLE"));
        alertDlg.setContent(messages.
                            getSystemMessage("SYSTEM_TIMEOUT_MESSAGE"));
        alertDlg.show();
        alertDlg.moveTo(-1, -1);
        filogix.gadget.DialogHelper.activeDialog = alertDlg;
        return;
    },

    /**
     * handle the http error.
     */
    handleHttpError:function(responseObj) {

        // implement the logic for http errors.
        this.requestMonitor.status =
            filogix.express.service.RequestMonitor.STATUS_ERROR;
        this.pbDialog.hide();
        var alertDlg = new filogix.gadget.AlertDialogBox(".none");
        var messages = new filogix.util.RichClientMessages();
        alertDlg.setTitle(messages.getSystemMessage("SYSTEM_ERROR_TITLE"));
        var params = new Array();
        params[0] = responseObj.statusText;
        alertDlg.setContent(messages.getSystemMessage("HTTP_ERROR_MESSAGE",
                                                      params));
        alertDlg.show();
        alertDlg.moveTo(-1, -1);
        filogix.gadget.DialogHelper.activeDialog = alertDlg;
        return;
    },

    /**
     * handle the http success response.
     */
    handleHttpResponse:function(responseObj) {

        // the JSON emplementation.
        var obj = responseObj.responseText.parseJSON();
        // the response object must have a status property.
        // and the value must be one of the constants defined in
        // ServiceRequest.
        this.updatePageDOM(obj);
        switch(obj.rcResponseStatus) {
        case filogix.express.service.ServiceRequest.STATUS_CREATE_REQUEST_SUCCESS:
            // create the request successfully.
            // get the request id from response object and set it to the request
            // content.
            this.requestContent.rcRequestContent.requestId =
                obj.rcResponseContent.requestId;
            // we also need to get the new transaction copy id, and update it on
            // the current page
            this.requestContent.rcRequestContent.copyId =
                obj.rcResponseContent.copyId;
            this.updateTransactionCopyId(obj.rcResponseContent.copyId);
            // set the request id to current request id. it is just for
            // testing.
            // TODO: should be removed!
            currentRequestId = obj.rcResponseContent.requestId;
            // change the monitor's status to waiting.
            this.requestMonitor.status =
                filogix.express.service.RequestMonitor.STATUS_WAITING;
            break;
        case filogix.express.service.ServiceRequest.STATUS_CREATE_REQUEST_FAILURE:
            // Failed to create the request.
            break;
        case filogix.express.service.ServiceRequest.STATUS_CHECKING_RESPONSE:
            // do nothing, just keep checking the service response!
            this.requestMonitor.status =
                filogix.express.service.RequestMonitor.STATUS_WAITING;
            break;
        case filogix.express.service.ServiceRequest.STATUS_RESPONSE_SUCCESS:
            // got the response successfully.
            this.requestMonitor.status =
                filogix.express.service.RequestMonitor.STATUS_COMPLETE;
            if (obj.rcResponseContent.copyId != null) {
                this.updateTransactionCopyId(obj.rcResponseContent.copyId);
            }
            this.handleSuccess(obj);
            break;
        case filogix.express.service.ServiceRequest.STATUS_RESPONSE_FAILURE:
            // failed to get the response.
            // HOWEVER, we got to update current page's copy id, if the response
            // has the new copyid.
            if (obj.rcResponseContent.copyId != null) {
                this.updateTransactionCopyId(obj.rcResponseContent.copyId);
            }
            this.requestMonitor.status =
                filogix.express.service.RequestMonitor.STATUS_ERROR;
            this.handleError(obj);
            break;
        case filogix.express.service.ServiceRequest.STATUS_BUSINESS_RULE_FAILURE:
            // failed to get the response.
            // HOWEVER, we got to update current page's copy id, if the response
            // has the new copyid.
            if (obj.rcResponseContent.copyId != null) {
                this.updateTransactionCopyId(obj.rcResponseContent.copyId);
            }
            this.requestMonitor.status =
                filogix.express.service.RequestMonitor.STATUS_ERROR;
            this.handleError(obj, true);
            break;
        default:
            // failed to get the response.
            // HOWEVER, we got to update current page's copy id, if the response
            // has the new copyid.
            if (obj.rcResponseContent.copyId != null) {
                this.updateTransactionCopyId(obj.rcResponseContent.copyId);
            }
            this.requestMonitor.status =
                filogix.express.service.RequestMonitor.STATUS_ERROR;
            this.handleError(obj);
            break;
        }
    },

    /**
     * handle the Error response.
     *
     * @param responseObj the response object.
     */
    handleError:function(responseObj, isBrError) {

        // hide the progress bar.
        this.pbDialog.setProgressPercent(1.0);
        this.pbDialog.hide();

        // show up the error message dialog.
        var messages = new filogix.util.RichClientMessages();
        var alertDlg;
        if ((isBrError != null) && isBrError) {
            alertDlg = new filogix.gadget.BusinessRuleDialog();
            alertDlg.
                setCaption("<center><b>" +
                           messages.getSystemMessage("ERRORS_AND_WARNINGS_WINDOW_TITLE")
                           + "</b></center>");
        } else {
            alertDlg = new filogix.gadget.AlertDialogBox(".none");
        }
        alertDlg.setTitle(messages.getSystemMessage("SYSTEM_ERROR_TITLE"));
        if (responseObj.rcResponseErrors != null) {
            alertDlg.setContent(responseObj.rcResponseErrors);
        } else {
            // should not go to here!  If it does, please prepare the message on
            // the server side.
            alertDlg.setContent(messages.getSystemMessage("MI_ERROR_SYSTEM_ANAVAILABLE"));
        }
        alertDlg.show();
        alertDlg.moveTo(-1, -1);
        filogix.gadget.DialogHelper.activeDialog = alertDlg;
        return;
    },

    /**
     * handle the success response.
     */
    handleSuccess:function(responseObj) {
        // implement the success response.  this method is just for closing the
        // progress bar dialog.  the real success logic should be provided by
        // the inheritance class.
        this.pbDialog.setProgressPercent(1.0);
        this.pbDialog.hide();
        // preparing the alert message.
        var messages = new filogix.util.RichClientMessages();
        var alertDlg = new filogix.gadget.AlertDialogBox(".none");
        alertDlg.setTitle(messages.getSystemMessage("SYSTEM_SUCCESS_TITLE"));
        alertDlg.setContent(messages.
                            getSystemMessage("SYSTEM_SUCCESS_MESSAGE"));
        alertDlg.show();
        alertDlg.moveTo(-1, -1);
        filogix.gadget.DialogHelper.activeDialog = alertDlg;
        return;
    }
};

/*
 * class XHRServiceRequest END
 ************************************/

/**************************************
 * class RequestMonitor
 */

/**
 * constructor.
 */
filogix.express.service.RequestMonitor =
    function(currentPercent, percentStep, timeInterval) {

    this.currentPercent = currentPercent;
    this.percentStep = percentStep;
    this.timeInterval = timeInterval;

    this.status = filogix.express.service.RequestMonitor.STATUS_INITIAL;
    this.timerId = null;
};

/**
 * status constants.
 */
filogix.express.service.RequestMonitor.STATUS_INITIAL = 0;
filogix.express.service.RequestMonitor.STATUS_CREATING = 5;
filogix.express.service.RequestMonitor.STATUS_WAITING = 10;
filogix.express.service.RequestMonitor.STATUS_ERROR = 20;
filogix.express.service.RequestMonitor.STATUS_COMPLETE = 30;

/**
 * class RequestMonitor END
 *************************************/

/**************************************
 * Action and status definitions.  Make sure to keep identical with the
 * definitions in Java class com.filogix.express.web.rc.RequestObject.
 */
filogix.express.service.ServiceRequest = new Object();

filogix.express.service.ServiceRequest.ACTION_CREATE_REQUEST = 100;
filogix.express.service.ServiceRequest.ACTION_CREATE_RESPONSE = 110; // for test only.
filogix.express.service.ServiceRequest.ACTION_CHECK_RESPONSE = 200;
// the status.
filogix.express.service.ServiceRequest.STATUS_CREATE_REQUEST_SUCCESS = 300;
filogix.express.service.ServiceRequest.STATUS_CREATE_RESPONSE_SUCCESS = 310;  // for test only.
filogix.express.service.ServiceRequest.STATUS_TXCOPY_ADOPTION_FAILURE = 320;
filogix.express.service.ServiceRequest.STATUS_BUSINESS_RULE_FAILURE = 330;
filogix.express.service.ServiceRequest.STATUS_CREATE_REQUEST_FAILURE = 400;
filogix.express.service.ServiceRequest.STATUS_RESPONSE_FAILURE = 500;
filogix.express.service.ServiceRequest.STATUS_RESPONSE_SUCCESS = 600;
filogix.express.service.ServiceRequest.STATUS_CHECKING_RESPONSE = 700;
filogix.express.service.ServiceRequest.STATUS_SERIALIZE_FAILURE = 800;
// the following status ...
filogix.express.service.ServiceRequest.EXPRESS_STATUS_INITIAL = -1;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_BLANK = 0;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_NOT_SUBMITTED = 1;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_REQUEST_ACCEPTED = 2;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_JOB_ASSIGNED = 3;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_JOB_DELAYED = 4;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_INSPECTION_COMPLETED = 5;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_COMPLETED = 6;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_COMPLETED_CONFIRMED = 7;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_CLOSING_CONFIRMED = 8;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_SYSTEM_ERROR = 9;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_PROCESSING_ERROR = 10;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_CANCELLATION_NOT_SUBMITTED = 11;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_CANCELLATION_REQUESTED = 12;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_CANCELLATION_ACCEPTED = 13;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_CANCELLATION_CONFIRMED = 14;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_CANCELLATION_REJECTED = 15;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_QUOTATION_RECEIVED = 16;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_QUOTATION_CONFIRMED = 17;
filogix.express.service.ServiceRequest.EXPRESS_STATUS_FAILED = 18;

/**
 *
 *************************************/

/**************************************
 * class ReportViewer
 */
filogix.express.service.ReportViewer = function() {

    /**
     * the title.
     */
    this.title = "Report Viewer";
    this.contentType = "text";

    /**
     * the post data.
     */
    this.requestType = "express.service.ReportViewer";
    this.requestContent = null;
}

filogix.express.service.ReportViewer.prototype = {

    /**
     * view report
     */
    viewReport:function() {

        switch (this.contentType) {
        case "text":
            this.viewTextReport();
            break;
        case "binary":
            this.viewBinaryReport();
            break;
        default:
            // do nothing.
            break;
        }
    },

    viewBinaryReport:function() {

        // get a XMLHttpRequest object!
        var xmlhttp =
            filogix.util.ConnectManager.getJatoXmlHttpObject(false,
                                                             this.contentType);
        location.assign(xmlhttp + "?requestType=" +
                        this.requestType + "&requestContent=" +
                        this.requestContent.toJSONString());
    },

    viewTextReport:function() {

        // get a XMLHttpRequest object!
        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();

        // send the post data.  the JSON implementation.
        xmlhttp.send("requestType=" + this.requestType +
                     "&requestContent=" + this.requestContent.toJSONString());

        // handle the HTTP response status, update the request monitor.
        var httpStatus = xmlhttp.status;
        if (httpStatus == 200) {
            var report = xmlhttp.responseText.parseJSON().
                rcResponseContent.report;
            var message = new filogix.util.RichClientMessages();
            var imgFile = filogix.express.IMAGE_ROOT +
                message.getSystemMessage("SERVICE_PRINT_BTN_FILE");
            var viewerDlg = new filogix.gadget.HTMLViewerDialog(true, imgFile);
            viewerDlg.setTitle(this.title);
            viewerDlg.setContent(report);
            viewerDlg.show();
            viewerDlg.moveTo(-1, -1);
            filogix.gadget.DialogHelper.activeDialog = viewerDlg;
        } else {
            // here are some http error!
            var alertDlg = new filogix.gadget.AlertDialogBox(".none");
            var messages = new filogix.util.RichClientMessages();
            alertDlg.setTitle(messages.getSystemMessage("SYSTEM_ERROR_TITLE"));
            var params = new Array();
            params[0] = xmlhttp.statusText;
            alertDlg.setContent(messages.getSystemMessage("HTTP_ERROR_MESSAGE",
                                                          params));
            alertDlg.show();
            alertDlg.moveTo(-1, -1);
            filogix.gadget.DialogHelper.activeDialog = alertDlg;
        }
    }
}
/**
 * class ReportViewer END
 *************************************/


/**************************************
 * class ServiceProductsExtractor
 * trying to extract the products from server for the specified provider. 
 */
/**
 * constructor.
 */
filogix.express.service.ServiceProductsExtractor = function() {

    this.requestType = "express.service.ServiceProductsExtractor";

    // the provider combobox element.
    this.provider = null;
    // the product combobox element.
    this.product = null;
    // the product types.
    this.productTypeId;
    this.productSubTypeId;
    this.productSubSubTypeId;
}

/**
 * the prototype.
 */
filogix.express.service.ServiceProductsExtractor.prototype = {

    /**
     * handle provider change.
     */
    handleProviderChange:function() {

        if (this.provider.selectedIndex == 0) {
            // clear the product combobox and disable it.
            for (var i = this.product.options.length; i > 0; i --) {
                this.product.remove(0);
            }
            this.product.selectedIndex = 0;
            this.product.disabled = true;
        } else {
            this.product.disabled = false;

            var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
            // preparing the request object.
            var request = new Object();
            request.rcRequestContent = new Object();
            request.rcRequestContent.providerId =
                this.provider.options[this.provider.selectedIndex].value;
            // setting up product types.
            request.rcRequestContent.productTypeId = this.productTypeId;
            request.rcRequestContent.productSubTypeId =
                this.productSubTypeId;
            // send out the request.
            xmlhttp.send("requestType=" + this.requestType +
                         "&requestContent=" + request.toJSONString());
            if (xmlhttp.status != 200) {
                var alertDlg =
                    new filogix.gadget.AlertDialogBox(".none");
                var messages = new filogix.util.RichClientMessages();
                alertDlg.setTitle(messages.
                                  getSystemMessage("SYSTEM_ERROR_TITLE"));
                var params = new Array();
                params[0] =
                    this.provider.options[this.provider.selectedIndex].text;
                alertDlg.setContent(messages.
                                    getSystemMessage("NO_PRODUCTS_FOR_PROVIDER",
                                                     params));
                alertDlg.show();
                alertDlg.moveTo(-1, -1);
                return;
            }

            var prods = xmlhttp.responseText.parseJSON().rcResponseContent;
            // remove the previous products first.
            for (var i = this.product.options.length; i > 0; i --) {
                this.product.remove(0);
            }

            if (prods.products.length < 1) {
                this.product.disabled = true;
                return;
            }
            for(var n = 0; n < prods.products.length; n ++) {
                var prod = document.createElement("option");
                this.product.options[n] = prod;
                prod.setAttribute("value", prods.products[n].value);
                prod.appendChild(document.
                                 createTextNode(prods.products[n].label));
            }
            this.product.selectedIndex = 0;
        }
    }
}
/**
 * class ServiceProductsExtractor END
 *************************************/

 
 
 
/**
 * returns the viewonly tag
 */
function getViewOnlyTag() {

    return document.getElementById("stViewOnlyTag");
}

/**
 * show view only disallowed dialog
 */
function showDisallowedViewOnly() {
    var msgs = new filogix.util.RichClientMessages();
    var alertDlg = new filogix.gadget.AlertDialogBox(".none");
    alertDlg.setTitle(msgs.getSystemMessage("SYSTEM_ERROR_TITLE"));
    alertDlg.setContent(msgs.getSystemMessage("DISALLOWED_DURING_VIEW_ONLY_MODE"));
    alertDlg.show();
    alertDlg.moveTo(-1, -1);
    filogix.gadget.DialogHelper.activeDialog = alertDlg;
}
