/**
 * Title: MITypeFilter.js
 * <p>
 * Description: JavaScript functions for Dynamic filtering of MIType drop down list
 * @author HIRO
 * @version 1.0 Aug 12, 2011
 */
var MITypeFilterFactory = function(MIComboBox, MITypeComboBox) {

    var func = function() {
        try{
            // exit if MI is empty
            if(!MIComboBox) return;
            if(!MITypeComboBox) return;
            if(!MIComboBox.value) return;

            // clear MI Type
            MITypeComboBox.length = 0;

            var request = {};
            request.rcRequestContent = {};
            request.rcRequestContent.mortgageInsurerId = MIComboBox.value;

            // create XHR object for async request
            var xhr = filogix.util.ConnectManager.getJatoXmlHttpObject(true);

            // call back function
            xhr.onreadystatechange = function(){
                if(xhr.readyState == 4){

                    if(!xhr.responseText) return; //error
                    var responseContext = xhr.responseText.parseJSON().rcResponseContent;
                    if(responseContext == null) return; //no data found

                    // populate MI Type
                    populateSelectObjByJSON(MITypeComboBox, 
                            responseContext.miTypes, "miTypeId", "miTypeName");

                    // select MI type
                    if(responseContext.selectedMITypeId){
                        selectRow(MITypeComboBox, responseContext.selectedMITypeId);
                    }
                }
            };

            xhr.send("requestType=express.service.MITypeFilter&requestContent=" + request.toJSONString());
        }catch(e){
            ajaxLogger.error(e);
        }
        var populateSelectObjByJSON = function(selectObj, jsonObj, keyColumn, DisplayColumn){
            selectObj.options.length = 0;
            for (var i in jsonObj) {
                selectObj.options[i] = new Option(jsonObj[i][DisplayColumn], jsonObj[i][keyColumn]);
            }
        };
        var selectRow = function (selectObj, selectValue){
            for (var i = 0; i < selectObj.options.length; i++) {
                if (selectObj.options[i].value == selectValue) 
                    selectObj.options[i].selected = true;
            }
        };
    };

    //attach event to MI Combo box (IE way)
    if(MIComboBox){
        MIComboBox.attachEvent("onchange", func);
    }
    return func;
};
