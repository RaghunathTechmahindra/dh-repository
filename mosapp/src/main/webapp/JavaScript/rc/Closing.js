filogix.gadget.DialogBase.imagePath = "../JavaScript/rc/images/";
filogix.util.RichClientMessages.msgKeys = new Array();
filogix.util.RichClientMessages.msgKeys =
    ["SERVICE_OCS_TITLE", "SERVICE_OCS_PROMPT",
     "SERVICE_OCS_REQUEST_BTN_FILE", "SERVICE_OCS_UPDATE_BTN_FILE",
     "SERVICE_MUST_PROVIDER_PRODUCT", "SERVICE_FPC_TITLE",
     "SERVICE_FPC_PROMPT", "CHOOSE_PAGE"];

/**
 * returns the hidden dealid.
 */
function getDealId() {
    return document.getElementById("hdDealId").value;
}

/**
 * returns the hidden copyid.
 */
function getCopyId() {
    return document.getElementById("hdDealCopyId").value;
}

/**
 * set copy id.
 */
function setCopyId(newId) {
    document.getElementById("hdDealCopyId").value = newId;
}

/**
 * returns the hidden OCS requestId
 */
function getOCSRequestId() {
    var id = document.getElementById("hdOcsRequestId").value;
    return (new Number(id)).valueOf();
}

function setOCSRequestId(newId) {
    document.getElementById("hdOcsRequestId").value = newId;
}

/**
 * returns the OCS provider select element.
 */
function getOCSProvider() {
    return document.getElementById("cbOCSProvider");
}

/**
 * returns the OCS Product select element.
 */
function getOCSProduct() {
    return document.getElementById("cbOCSProduct");
}

/**
 * returns the ocs instruction, using the solicator special instruction for now.
 */
function getOCSInstructions() {
    return document.getElementById("tbInstructions");
}

/**
 * returns the OCS request status id.
 */
function getOCSStatusId() {

    return document.getElementById("hdOCSRequestStatusId").value;
}

/**
 * set the OCS request status id.
 */
function setOCSStatusId(id) {

    document.getElementById("hdOCSRequestStatusId").value = id;
}

/**
 * set service provider reference number.
 */
function getOCSReferenceNum() {
    return document.getElementById("hdRefNum").value;
}

/**
 * set service provider reference number.
 */
function setOCSReferenceNum(num) {
    document.getElementById("hdRefNum").value = num;
}

/**
 * handle the OCS Provider change.
 */
function ocsProviderChange() {
    if(getViewOnlyTag()==null) {
        var extractor = new filogix.express.service.ServiceProductsExtractor();
        extractor.provider = getOCSProvider();
        extractor.product = getOCSProduct();
        // the OCS product
        extractor.productTypeId = 3;
        extractor.productSubTypeId = 6;
        extractor.handleProviderChange();
    }
    else {
        showDisallowedViewOnly();
    }
}

/**
 * returns the Outsource Closing service request button.
 */
function getOCSRequestButton() {

    return document.getElementById("ocsRequestButton");
}

/**
 * show up request button for OCS.
 */
function showOCSRequest() {

    var message = new filogix.util.RichClientMessages();
    getOCSRequestButton().src = "../images/" +
        message.getMessage("SERVICE_OCS_REQUEST_BTN_FILE");
        
    getOCSProvider().disabled = false;
    getOCSProduct().disabled = false;    
}

/**
 * show up update button for OCS.
 */
function showOCSUpdate() {

    var message = new filogix.util.RichClientMessages();
    getOCSRequestButton().src = "../images/" +
        message.getMessage("SERVICE_OCS_UPDATE_BTN_FILE");
    getOCSProvider().disabled = true;
    getOCSProduct().disabled = true;  
}

// =====================
// utilities functions for fix price.
// ====================
/**
 * the fpc request id.
 */
function getFPCRequestId() {
    var id = document.getElementById("hdFpcRequestId").value;
    return (new Number(id)).valueOf();
}

function setFPCRequestId(newId) {
    document.getElementById("hdFpcRequestId").value = newId;
}

/**
 * the fpc service request contact id.
 */
function getFPCContactId() {
    var id = document.getElementById("hdFpcContactId").value;
    return (new Number(id)).valueOf();
}
function setFPCContactId(newId) {
    document.getElementById("hdFpcContactId").value = newId;
}

/**
 * the primary borrower id
 */
function getFPCBorrowerId() {
    var id = document.getElementById("hdFpcBorrowerId").value;
    return (new Number(id)).valueOf();
}

function setFPCBorrowerId(newId) {
    document.getElementById("hdFpcBorrowerId").value = newId;
}

/**
 * the primary borrower check box.
 */
function getFPCBorrowerCheckbox() {
    return document.getElementById("fpcBorrowerCheckbox");
}

/**
 * the contact's first name.
 */
function getContactFirstName() {
    return document.getElementById("contactFirstName");
}

function getContactLastName() {
    return document.getElementById("contactLastName");
}

function getContactEmail() {
    return document.getElementById("contactEmail");
}

function getContactWorkArea() {
    return document.getElementById("contactWorkArea");
}

function getContactWorkFirst() {
    return document.getElementById("contactWorkFirst");
}

function getContactWorkLast() {
    return document.getElementById("contactWorkLast");
}

function getContactWorkExt() {
    return document.getElementById("contactWorkExt");
}

function getContactCellArea() {
    return document.getElementById("contactCellArea");
}

function getContactCellFirst() {
    return document.getElementById("contactCellFirst");
}

function getContactCellLast() {
    return document.getElementById("contactCellLast");
}

function getContactHomeArea() {
    return document.getElementById("contactHomeArea");
}

function getContactHomeFirst() {
    return document.getElementById("contactHomeFirst");
}

function getContactHomeLast() {
    return document.getElementById("contactHomeLast");
}

function getFPCComments() {
    return document.getElementById("fpcComments");
}

/**
 * returns the FPC Product select element.
 */
function getFPCProduct() {
    return document.getElementById("cbFPCProduct");
}

/**
 * returns the FPC provider select element.
 */
function getFPCProvider() {
    return document.getElementById("cbFPCProvider");
}

/**
 * handle the FPC Provider change.
 */
function fpcProviderChange() {
    if(getViewOnlyTag()==null) {
        var extractor = new filogix.express.service.ServiceProductsExtractor();
        extractor.provider = getFPCProvider();
        extractor.product = getFPCProduct();
        // the Fix Price closing product
        extractor.productTypeId = 3;
        extractor.productSubTypeId = 3;
        extractor.handleProviderChange();
    }
    else {
        showDisallowedViewOnly();
    }
}

/**************************************
 * class OCSRequest
 */
filogix.express.service.OCSRequest = function() {

    // setting up the request type.
    this.requestType = "express.service.OCSRequest";
}

filogix.express.service.OCSRequest.prototype =
    new filogix.express.service.XHRServiceRequest();
filogix.express.service.OCSRequest.superclass =
    filogix.express.service.XHRServiceRequest.prototype;

/**
 * update the transaction copy id.
 */
filogix.express.service.OCSRequest.prototype.updateTransactionCopyId =
    function(newId) {

    // update the DOM.
    setCopyId(newId);
}

/**
 * update the OCS section based on the response object.
 */
filogix.express.service.OCSRequest.prototype.updatePageDOM =
    function(responseObj) {

    // for OCS section we need update the ocsStatus, ocsDate, and ocsStatusMsg.
    var ocsRequestRef = document.getElementById("ocsRequestRef");
    var ocsStatus = document.getElementById("ocsStatus");
    var ocsDate = document.getElementById("ocsDate");
    var ocsStatusMsg = document.getElementById("ocsStatusMsg");

    // get info from the response object.
    if (responseObj.rcResponseContent.ocsRequestRef) {
        ocsRequestRef.innerHTML = responseObj.rcResponseContent.ocsRequestRef;
    }
    if (responseObj.rcResponseContent.ocsRequestStatus) {
        ocsStatus.innerHTML = responseObj.rcResponseContent.ocsRequestStatus;
    }
    if (responseObj.rcResponseContent.ocsRequestDate) {
        ocsDate.innerHTML = responseObj.rcResponseContent.ocsRequestDate;
    }
    if (responseObj.rcResponseContent.ocsRequestStatusMsg) {
        ocsStatusMsg.innerHTML =
            responseObj.rcResponseContent.ocsRequestStatusMsg;
    }
    
    if (responseObj.rcResponseContent.requestId) {
        setOCSRequestId(responseObj.rcResponseContent.requestId);
    }
}

/**
 * handle success response button report
 */
filogix.express.service.OCSRequest.prototype.handleSuccess =
    function(responseObj) {

    // call the super class mehtod to close the progress bar.
    filogix.express.service.OCSRequest.
        superclass.handleSuccess.call(this, responseObj);

    // update ocs response section
    // for OCS section we need update the ocsStatus, ocsDate, and ocsStatusMsg.
    var ocsRequestRef = document.getElementById("ocsRequestRef");
    var ocsStatus = document.getElementById("ocsStatus");
    var ocsDate = document.getElementById("ocsDate");   
    var ocsStatusMsg = document.getElementById("ocsStatusMsg");
    
    ocsRequestRef.innerHTML = "";
    ocsStatus.innerHTML = "";
    ocsDate.innerHTML = "";
    ocsStatusMsg.innerHTML = "";
    
    // get info from the response object.
    if (responseObj.rcResponseContent.ocsRequestRef) {
        ocsRequestRef.innerHTML = responseObj.rcResponseContent.ocsRequestRef;
    }

    if (responseObj.rcResponseContent.ocsRequestStatus) {
        ocsStatus.innerHTML = responseObj.rcResponseContent.ocsRequestStatus;
    }
    if (responseObj.rcResponseContent.ocsRequestDate) {
        ocsDate.innerHTML = responseObj.rcResponseContent.ocsRequestDate;
    }
    if (responseObj.rcResponseContent.ocsRequestStatusMsg) {
        ocsStatusMsg.innerHTML =
            responseObj.rcResponseContent.ocsRequestStatusMsg;
    }
    //check status

    setOCSStatusId(responseObj.rcResponseContent.ocsRequestStatusId);
    setOCSReferenceNum(responseObj.rcResponseContent.ocsRequestRef);
    //setProviderId(responseObj.rcResponseContent.resproviderId);
    //setProductId(responseObj.rcResponseContent.resproductId);
    
    taggleOCSButtons();

}
/**
 * class OCSRequest END
 **************************************/

/**
 * toggle the button's image.
 */
function taggleOCSButtons() {
    
    if(getViewOnlyTag()==null) {
        if(getFPCBorrowerCheckbox()) {
            primaryBorrowerClicked();
            //disableContactInfoSection(getFPCBorrowerCheckbox().checked);
        }
        
        if (getOCSRequestButton() == null) {
            // OCS Button not exist, just return.
            return;
        }
        switch ((new Number(getOCSStatusId())).valueOf()) {
        case filogix.express.service.ServiceRequest.EXPRESS_STATUS_INITIAL:
        case filogix.express.service.ServiceRequest.EXPRESS_STATUS_BLANK:
        case filogix.express.service.ServiceRequest.EXPRESS_STATUS_NOT_SUBMITTED:
        case filogix.express.service.ServiceRequest.EXPRESS_STATUS_CANCELLATION_CONFIRMED:
            // show up the request button.
            showOCSRequest();
            break;
        case filogix.express.service.ServiceRequest.EXPRESS_STATUS_SYSTEM_ERROR:
        case filogix.express.service.ServiceRequest.EXPRESS_STATUS_PROCESSING_ERROR:
            if(getOCSReferenceNum()=="" ) {
                showOCSRequest();
            }
            else {
                showOCSUpdate();
            }
            break;
        default:
            // show up the update button.  disable the provider and product.
            showOCSUpdate();
            break;
        }
    }
    else { 
        showViewOnlyButton();
    }
}


/**
 * send out ocs request.
 */
function sendOCSRequest(isCancel) {
    
    if(getViewOnlyTag()==null) {

        var provider = getOCSProvider();
        var product = getOCSProduct();
    
        if ((product.options.length < 1) ||
            (product.options[product.selectedIndex].value <= 0) ||
            (provider.selectedIndex <= 0)) {
            // show up the error message dialog.
            var msgs = new filogix.util.RichClientMessages();
            var alertDlg = new filogix.gadget.AlertDialogBox(".none");
            alertDlg.setTitle(msgs.getSystemMessage("SYSTEM_ERROR_TITLE"));
            alertDlg.setContent(msgs.getMessage("SERVICE_MUST_PROVIDER_PRODUCT"));
            alertDlg.show();
            alertDlg.moveTo(-1, -1);
            filogix.gadget.DialogHelper.activeDialog = alertDlg;
            return;
        }
    
        if (! isCancel) {
            isCancel = false;
        }
    
        // preparing the prompt message.
        var messages = new filogix.util.RichClientMessages();
        var ocsRequest = new filogix.express.service.OCSRequest();
        ocsRequest.dialogTitle = messages.getMessage("SERVICE_OCS_TITLE") +
            " - Express";
        var params = new Array();
        params[0] = provider.options[provider.selectedIndex].text;
        params[1] = product.options[product.selectedIndex].text;
        ocsRequest.dialogPromptMsg = messages.getMessage("SERVICE_OCS_PROMPT",
                                                         params);
    
        // preparing the request object and send it out.
        var request = new Object();
        request.rcRequestContent = new Object();
        // we don't really need provider id!
        request.rcRequestContent.providerId =
            provider.options[provider.selectedIndex].value;
        request.rcRequestContent.productId =
            product.options[product.selectedIndex].value;
        
        //cancel button
        request.rcRequestContent.isCancel = isCancel;
    
        // deal id,
        request.rcRequestContent.dealId = getDealId();
        request.rcRequestContent.copyId = getCopyId();
        // set the requestId if exist one!
        request.rcRequestContent.requestId = getOCSRequestId();
        request.rcRequestContent.instructions = getOCSInstructions().value;
        ocsRequest.requestContent = request;
        ocsRequest.send();
    } else {
        showDisallowedViewOnly();
    }
}

/**************************************
 * class FPCRequest
 */
filogix.express.service.FPCRequest = function() {

    // setting up the request type.
    this.requestType = "express.service.FPCRequest";
}

filogix.express.service.FPCRequest.prototype =
    new filogix.express.service.XHRServiceRequest();
filogix.express.service.FPCRequest.superclass =
    filogix.express.service.XHRServiceRequest.prototype;

/**
 * update the transaction copy id.
 */
filogix.express.service.FPCRequest.prototype.updateTransactionCopyId =
    function(newId) {
    // update the DOM.
    setCopyId(newId);
}

/**
 * update the FPC section based on the response object.
 */
filogix.express.service.FPCRequest.prototype.updatePageDOM =
    function(responseObj) {
    // for FPC section we need update the fpcStatus, fpcDate.
    var fpcStatus = document.getElementById("fpcStatus");
    var fpcDate = document.getElementById("fpcDate");
    // get info from the response object.
    if (responseObj.rcResponseContent.fpcRequestStatus) {
        fpcStatus.innerHTML = responseObj.rcResponseContent.fpcRequestStatus;
    }

    if (responseObj.rcResponseContent.fpcRequestDate) {
        fpcDate.innerHTML = responseObj.rcResponseContent.fpcRequestDate;
    }

    if (responseObj.rcResponseContent.requestId) {
        setFPCRequestId(responseObj.rcResponseContent.requestId);
    }

    if (responseObj.rcResponseContent.serviceRequestContactId) {
        setFPCContactId(responseObj.rcResponseContent.serviceRequestContactId);
    }
}

/**
 * handle success response button report?.
 */
filogix.express.service.FPCRequest.prototype.handleSuccess =
    function(responseObj) {
    // call the super class mehtod to close the progress bar.
    filogix.express.service.FPCRequest.
        superclass.handleSuccess.call(this, responseObj);
}
/**
 * class FPCRequest  END
 **************************************/


/**
 * handle the primary borrower check box.
 */
function primaryBorrowerClicked() {

    if(getFPCBorrowerCheckbox().checked){
        borrowerRequest();
    }
    disableContactInfoSection(getFPCBorrowerCheckbox().checked);

/*
    //using component.js start
    var contact = new filogix.express.cmps.ContactInfo();
    contact.firstName = getContactFirstName();
    contact.lastName = getContactLastName();
    contact.email = getContactEmail();
    contact.workPhoneArea = getContactWorkPhoneArea();
    contact.workPhoneFirst = getContactWorkPhoneFirst();
    contact.workPhoneLast = getContactWorkPhoneLast();
    contact.workPhoneExt = getContactWorkPhoneExt();
    contact.cellPhoneArea = getContactCellPhoneArea();
    contact.cellPhoneFirst = getContactCellPhoneFirst();
    contact.cellPhoneLast = getContactCellPhoneLast();
    contact.homePhoneArea = getContactHomePhoneArea();
    contact.homePhoneFirst = getContactHomePhoneFirst();
    contact.homePhoneLast = getContactHomePhoneLast();

    contact.primaryBorrowerCheckbox = getFPCBorrowerCheckbox();

    contact.init();
    borrowerRequest();
*/
    //using component.js end

}

/**
 * disable or enable contact info section.
 */
function disableContactInfoSection(disable) {

    getContactFirstName().disabled = disable;
    getContactLastName().disabled = disable;
    getContactEmail().disabled = disable;
    getContactWorkArea().disabled = disable;
    getContactWorkFirst().disabled = disable;
    getContactWorkLast().disabled = disable;
    getContactWorkExt().disabled = disable;
    getContactCellArea().disabled = disable;
    getContactCellFirst().disabled = disable;
    getContactCellLast().disabled = disable;
    getContactHomeArea().disabled = disable;
    getContactHomeFirst().disabled = disable;
    getContactHomeLast().disabled = disable;
}

/**
 * FPC service submitreferral button.
 */
function sendFPCRequest() {
    if(getViewOnlyTag()==null) {
        var provider = getFPCProvider();
        var product = getFPCProduct();
        var indexProvider = provider.selectedIndex;
        var valueProvider = provider.options[provider.selectedIndex].value;
        var lengthProvider = provider.options.length;
        var primaryClicked = document.getElementById("pgClosing_chFPCBorrowerInfo");
    
        if ((product.disabled)||(provider.disalbed)||(product.options.length < 1) ||
            (product.options[product.selectedIndex].value < 0) ||
            (provider.selectedIndex < 0)){
            // show up the error message dialog.
            // show up the error message dialog.
            var msgs = new filogix.util.RichClientMessages();
            var alertDlg = new filogix.gadget.AlertDialogBox(".none");
            alertDlg.setTitle(msgs.getSystemMessage("SYSTEM_ERROR_TITLE"));
            alertDlg.setContent(msgs.getMessage("SERVICE_MUST_PROVIDER_PRODUCT"));
            alertDlg.show();
            alertDlg.moveTo(-1, -1);
            filogix.gadget.DialogHelper.activeDialog = alertDlg;
            return;
        }
    
        var fpcRequest = new filogix.express.service.FPCRequest();
        // preparing the prompt message.
        var messages = new filogix.util.RichClientMessages();
        fpcRequest.dialogTitle = messages.getMessage("SERVICE_FPC_TITLE") +
            " - Express";
        var params = new Array();
        params[0] = provider.options[provider.selectedIndex].text;
        params[1] = product.options[product.selectedIndex].text;
        fpcRequest.dialogPromptMsg = messages.getMessage("SERVICE_FPC_PROMPT", params);     
    
        var request = new Object();
        request.rcRequestContent = new Object();
        // privider product id.
        request.rcRequestContent.providerId =
              provider.options[provider.selectedIndex].value;
        request.rcRequestContent.productId =
              product.options[product.selectedIndex].value;
        // deal id,
        request.rcRequestContent.dealId = getDealId();
        request.rcRequestContent.copyId = getCopyId();
        // set the requestId if exist one!
        request.rcRequestContent.requestId = getFPCRequestId();
        request.rcRequestContent.serviceRequestContactId = getFPCContactId();
        // contact info.
        // TODO: the borrower id.
        request.rcRequestContent.needPrimary = getFPCBorrowerCheckbox().checked;
        request.rcRequestContent.firstName = getContactFirstName().value;
        request.rcRequestContent.lastName = getContactLastName().value;
        request.rcRequestContent.emailAddress = getContactEmail().value;
        request.rcRequestContent.workAreaCode = getContactWorkArea().value;
        request.rcRequestContent.workPhoneNumber = getContactWorkFirst().value +
            getContactWorkLast().value;
        request.rcRequestContent.workPhoneExt = getContactWorkExt().value;
        request.rcRequestContent.cellAreaCode = getContactCellArea().value;
        request.rcRequestContent.cellPhoneNumber = getContactCellFirst().value +
            getContactCellLast().value;
        request.rcRequestContent.homeAreaCode = getContactHomeArea().value;
        request.rcRequestContent.homePhoneNumber = getContactHomeFirst().value +
            getContactHomeLast().value;
        request.rcRequestContent.comments = getFPCComments().value;
    
        // send it out.
        fpcRequest.requestContent = request;
        fpcRequest.send();
    }
    else {
        showDisallowedViewOnly();
    }
}
