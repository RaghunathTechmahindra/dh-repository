/**************************************
 * class CheckPriorityTaskProcessor
 * 
 */

/**
 * constructor.
 */
filogix.express.service.CheckPriorityTaskProcessor = function() {

    this.requestType = "express.service.PriorityTaskRequest";
}

filogix.express.service.CheckPriorityTaskProcessor.prototype =  {

    getFindPTaskString:function() {
        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
        // preparing the request object, set values
        var request = new Object();

        request.rcRequestContent = new Object();
        request.rcRequestContent.timeOut = this.timeOut;
        request.rcRequestContent.subRequestType = "getFindPTaskString";

        // send out the request.
        xmlhttp.send("requestType=" + this.requestType +
                     "&requestContent=" + request.toJSONString());
        
        if (xmlhttp.status != 200 || xmlhttp.responseText.indexOf("pgSessionEnded") != -1) {
            return "N"; // when http error, returns no p-task found. 
        }
                
        var resp = xmlhttp.responseText.parseJSON().rcResponseContent;
        return resp.taskFoundString;
    }
}

var imagesPath = "../images/";
function changeImage(found){
    var ptaskImg = "";
  
    if(found){
        if(LANGUAGECODE == "0"){
            ptaskImg = imagesPath + "yes_priority.gif";
        } else {
            ptaskImg = imagesPath + "yes_priority_fr.gif";
        }
    } else {
        if(LANGUAGECODE == "0"){
            ptaskImg = imagesPath + "no_priority.gif";
        } else {
            ptaskImg = imagesPath + "no_priority_fr.gif";
        }
    }
    getHtmlElement("pTaskImage").src = ptaskImg;
}

var pTaskTimer;
var ptaskFound = false;

/**
 * Check 'PriorityTasksUnacknowledged', then change the image accordingly.
 * Set hidden field detectAlertTasks = "N" to indicate
 * further checking fo unacknowledged tasks unnecessary.
 */
function checkPriorityTasks(){

    var extractor = new filogix.express.service.CheckPriorityTaskProcessor();
    var detectAlertTasks = convertToBool(getDetectAlertTasks().value);
    
    if((detectAlertTasks == true) || (ptaskFound == false)){
        //send AJAX request
        extractor.timeOut = false;
        try {
            ptaskFound = convertToBool(extractor.getFindPTaskString());
        } catch (e) {
            ptaskFound = false;
        }
    }

    if(ptaskFound == true){
        if((detectAlertTasks == true)) {
            // task detected for the first time
            playSound();
        }
        changeImage(true);
        getDetectAlertTasks().value = "N";
        clearInterval(pTaskTimer); //stop loop
    }else{
        changeImage(false);
        getDetectAlertTasks().value = "Y";
    }
}

function convertToBool(str){
    if(str == "N" || str == "n")
       return false;
    else
       return true;  
}

/*
function playSound(){
    document.all['BGSOUND_ID'].src='/images/drip.au';
}
*/

function playSound(){
    var str = "";
    str = str + "<EMBED id = 'pTaskSound'";
    str = str + " SRC='" + imagesPath + "drip.au'";
    str = str + " AUTOSTART='true'";
    str = str + " HIDDEN='true'>";
    getHtmlElement("pTaskSound").innerHTML = str;
}

/**
 * returns the html element by ID.
 */
function getHtmlElement(elementId) {
    return document.getElementById(elementId);
}

/**
 * Check Priority tasks every 20 seconds.
 */
function checkPTaskLoop() {
    changeImage(!convertToBool(getDetectAlertTasks().value));
    pTaskTimer = setInterval("checkPriorityTasks()", 60000);
    setTimeout("clearTimingPTask()", getSessionTimeOut());
}

/**
 * Clear timely PriorityTask
 */
function clearTimingPTask() {
    
    if (pTaskTimer) clearInterval(pTaskTimer);
    var extractor = new filogix.express.service.CheckPriorityTaskProcessor();
    extractor.timeOut = true;
    var pTask = extractor.getFindPTaskString();
}

/**
 * returns the session timeout value
 */
function getSessionTimeOut() {
    return document.getElementById("hdSessionTimeOut").value;
}