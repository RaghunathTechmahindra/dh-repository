/**
 * the global namespace for filogix JavaScrip/DHTML rich client.
 */
var filogix = {

    // the util package.
    util:{},

    // the gadget package.
    gadget:{},

    // the express package.
    express:{},

    // the testing package.
    pilot:{}
};

/**
 * define the sub package for the filogix.express.
 */
filogix.express = {
    // image root.
    IMAGE_ROOT:"../images/",
    // express components package.
    cmps:{},
    // the express service package.
    service:{}
};

/**
 * the XMLHttpRequest Connection Manager.
 */
filogix.util.ConnectManager = {

    /**
     * the possible Microsoft ActiveX Object ids for XMLHttpRequest.
     */
    msXMLActiveXIds:[
        'MSXML2.XMLHTTP.3.0',
        'MSXML2.XMLHTTP.4.0',
        'MSXML2.XMLHTTP.5.0',
        'MSXML2.XMLHTTP',
        'Microsoft.XMLHTTP'
    ],

    /**
     * the XMLHttpRequest object.
     */
    xmlHttpObject:null,

    /**
     * create a XMLHttpRequest object.
     */
    createXMLHttpRequest:function() {

        var xmlhttp;

        try {
            // Instantiates XMLHttpRequest in non-IE browsers.
            xmlhttp = new XMLHttpRequest();
        } catch(e) {
            for(var i=0; i < this.msXMLActiveXIds.length; ++i) {

                try {
                    // Instantiates XMLHttpRequest for IE.
                    xmlhttp = new ActiveXObject(this.msXMLActiveXIds[i]);
                } catch(e) {
                    // do nothing here, just go to next one.
                }
            }
        } finally {

            return xmlhttp;
        }
    },

    /**
     * returns the XMLHttpRequest object.
     */
    getXmlHttpObject:function() {

        if (this.xmlHttpObject == null) {

            this.xmlHttpObject = this.createXMLHttpRequest();
        }

        return this.xmlHttpObject;
    },

    /**
     * return the XMLHttpRequest object with the JATO settings.
     */
    getJatoXmlHttpObject:function(bAsyn, object) {

        if(bAsyn == null) {
            // default is a synchronous request.
            bAsyn = false;
        }

        // JATO supposes to have only one form for each page.
        var jsessionid = document.forms[0].action;
        if (jsessionid.indexOf(";") > 0) {
            jsessionid = jsessionid.substr(jsessionid.indexOf(";"));
        } else {
            jsessionid = "";
        }
        // for JATO.
        var requestUrl = "AJAXDispatching" + jsessionid;
        
        //reuse if xmlHttpRequest object was given
        var xhrObj = object || this.createXMLHttpRequest();
        
        // set up the request header.
        xhrObj.open("POST", requestUrl, bAsyn);
        xhrObj.setRequestHeader("Content-Type",
        	"application/x-www-form-urlencoded; charset=UTF-8" );

        return xhrObj;

    }
};

/**************************************
 * class RichClientMessages
 * this class will keep all i18n message from server side.
 */

/**
 * constructor.
 */
filogix.util.RichClientMessages = function() {

    this.requestType = "util.RichClientMessages";
};

// the system message keys.
filogix.util.RichClientMessages.sysMsgKeys = new Array();
// the system message values.
filogix.util.RichClientMessages.sysMsgValues = null;

// the client message keys.
filogix.util.RichClientMessages.msgKeys = new Array();
// the client message values.
filogix.util.RichClientMessages.msgValues = null;

/**
 * the prototype.
 */
filogix.util.RichClientMessages.prototype = {

    /**
     * initialize the message,
     * it is a private function.
     */
    initMessages:function(keys) {
        // extract the messages from server.
        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject();
        // preparing the request object.
        var request = new Object();
        request.rcRequestContent = new Object();
        request.rcRequestContent.msgKeys = keys;
        // send out the request.
        xmlhttp.send("requestType=" + this.requestType +
                     "&requestContent=" + request.toJSONString());
        if (xmlhttp.status != 200) {
            // terrible thing happened!
            return keys;
        }

        var response = xmlhttp.responseText.parseJSON().rcResponseContent;
        keys = response.msgKeys;
        return response.values;
    },

    /**
     * the private generic method to get the value for the given key from the
     * specified keys array and values array.
     */
    getMsgValue:function(key, params, keys, values) {

        // default is empty string.
        var value = "";
        if (key == null) {
            return value;
        }

        // then change the default value to the key.
        value = key;

        // trying to find the value for the given key.
        for (var i = 0; i < keys.length; i ++) {

            if (key == keys[i]) {
                value = values[i];
                break;
            }
        }

        // trying to populate the params' value for the message.
        if (params != null) {

            for (var c = 0; c < params.length; c ++) {
                value = value.replace("{" + c + "}", params[c]);
            }
        }

        return value;
    },

    /**
     * get the i18n message for AJAX client.
     */
    getMessage:function(key, params) {

        if (filogix.util.RichClientMessages.msgValues == null) {

            // initialize the values.
            filogix.util.RichClientMessages.msgValues =
                this.initMessages(filogix.util.RichClientMessages.msgKeys);
        }

        return this.getMsgValue(key, params,
                                filogix.util.RichClientMessages.msgKeys,
                                filogix.util.RichClientMessages.msgValues);
    },

    /**
     * get system message for AJAX client.
     */
    getSystemMessage:function(key, params) {

        if (filogix.util.RichClientMessages.sysMsgValues == null) {

            // initialize the values.
            filogix.util.RichClientMessages.sysMsgValues =
                this.initMessages(filogix.util.RichClientMessages.sysMsgKeys);
        }

        return this.getMsgValue(key, params,
                                filogix.util.RichClientMessages.sysMsgKeys,
                                filogix.util.RichClientMessages.sysMsgValues);
    }
};


/*******************************************************************************
 * class SimpleClientMessage
 * 
 * RichClientMessages has a problem that it relies on
 * filogix.util.RichClientMessages.msgKeys object to get message keys. however,
 * the scope of this object is global, so if you have more than two java script
 * services that use RichClientMessages in one screen it code doesn't work
 * properly.
 * 
 * SimpleClientMessage class just grabs one message when requested, nothing
 * is special. performance wise, probably it's not very good, but, you don't
 * have to worry about the confliction.
 * 
 * hiro, Nov 14, 2011
 * 
 ******************************************************************************/

/**
 * constructor.
 */
filogix.util.SimpleClientMessage = function() {

    this.requestType = "util.SimpleClientMessage";
    this.messageMap = {};
    var xmlhttp = null;
};

filogix.util.SimpleClientMessage.prototype = {
    /**
     * initialize the message,
     * it is a private function.
     */
    sendRequest:function(key, escape) {
        // extract the messages from server.

        var xmlhttp = filogix.util.ConnectManager.getJatoXmlHttpObject(false, xmlhttp);
        
        // preparing the request object.
        var request = new Object();
        request.rcRequestContent = new Object();
        request.rcRequestContent.msgKey = key;
        request.rcRequestContent.escape = !(escape === false); //default=true
        
        // send out the request.
        xmlhttp.send("requestType=" + this.requestType +
                     "&requestContent=" + request.toJSONString());
        if (xmlhttp.status != 200) {
            // terrible thing happened!
            return key;
        }

        var response = xmlhttp.responseText.parseJSON().rcResponseContent;
        //cache the result
        this.messageMap[key] = response.value;
        
        return response.value;
    },

    replaceParams:function(rawMsg, params) {

        if (params != null) {
            for (var c = 0; c < params.length; c ++) {
                rawMsg = rawMsg.replace("{" + c + "}", params[c]);
            }
        }

        return rawMsg;
    },

    /**
     * get the i18n message for AJAX client.
     */
    getMessage:function(key, params, escape) {

        if(key == null) return "?"; 
        var msg = this.messageMap[key];
        if(!msg){
            msg = this.sendRequest(key, escape);
        }
        if(!msg){
            return key;
        }
        return this.replaceParams(msg, params);
    }

};