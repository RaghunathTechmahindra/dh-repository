/*************************************
 * class ContactInfo
 */
filogix.express.cmps.ContactInfo = function() {

    // attributes text input field.
    this.firstName;
    this.lastName;
    // email.
    this.email;
    // work phone.
    this.workPhoneArea;
    this.workPhoneFirst;
    this.workPhoneLast;
    this.workPhoneExt;
    // cell phone.
    this.cellPhoneArea;
    this.cellPhoneFirst;
    this.cellPhoneLast;
    // home phone
    this.homePhoneArea;
    this.homePhoneFirst;
    this.homePhoneLast;

    this.propertyOwnerName = null;

    // the primary borrower check box.
    this.primaryBorrowerCheckbox = null;
}

/**
 * the prototype.
 */
filogix.express.cmps.ContactInfo.prototype = {

    /**
     * initializing this component.
     */
    init:function() {
        if (this.primaryBorrowerCheckbox != null) {
            this.primaryBorrowerCheckbox.onclick = this.primaryBorrowerClicked;
            this.primaryBorrowerCheckbox.parent = this;
            this.disable(this.primaryBorrowerCheckbox.checked);
        }
    },

    /**
     * disable all the fields.
     */
    disable:function(disable) {

        // name
        this.firstName.disabled = disable;
        this.lastName.disabled = disable;
        // email.
        this.email.disabled = disable;
        // work phone.
        this.workPhoneArea.disabled = disable;
        this.workPhoneFirst.disabled = disable;
        this.workPhoneLast.disabled = disable;
        this.workPhoneExt.disabled = disable;
        // cell phone.
        this.cellPhoneArea.disabled = disable;
        this.cellPhoneFirst.disabled = disable;
        this.cellPhoneLast.disabled = disable;
        // home phone
        this.homePhoneArea.disabled = disable;
        this.homePhoneFirst.disabled = disable;
        this.homePhoneLast.disabled = disable;

        //primary borrower name
        if(this.propertyOwnerName != null) {
            this.propertyOwnerName.disabled = disable;
        }
    },

    /**
     * handle the primary borrower clicked.
     */
    primaryBorrowerClicked:function(event) {

        if (!event) {
            event = window.event;
        }

        var source = event.target ? event.target : event.srcElement;
        if (source != null) {
            source.parent.disable(source.checked);
        }
        // TODO: extract the info from server side.
    }
}
/**
 * class ContactInfo END
 **************************************/
