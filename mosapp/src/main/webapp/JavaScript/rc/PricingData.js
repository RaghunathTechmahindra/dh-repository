/**
 * Title: PricingData.js
 * <p>
 * Description: JavaScript functions for Dynamic Drop down lest change for PricingLogic
 * @author MCM Impl Team.
 * @version 1.0 Jun 25, 2008 XS_2.28, XS_2.29 Initial version
 * @version 1.1 Aug 15, 2008 artf762702
 */

/******************************************************************
 * CONSTANT value 
 ******************************************************************/
var CONST_JSON_COL = {
    PRODUCT: {
        PRODUCTID: "prodId",
        PRODUCTDESC: "prodDesc"
    },
    RATE: {
        RATEID: "rateId",
        RATEDESC: "rateDesc",
        POSTEDRATE: "postedRate"
    },
    PAYMENTTERM: {
        PRODUCTID: "productId",
        PAYMENTTERMID: "paymentTermId",
        PAYMENTTERMDESC: "paymentTerm"
    },
    REPAYMENTTYPE: {
        PRODUCTID: "productId",
        REPAYMENTID: "repaymentId",
        REPAYMENTDESC: "repaymentDesc"
    },
    COMPONENTTYPE: {
        COMP_TYPE_ID: "compTypeId",
        COMP_TYPE_DESC: "compTypeDesc"
    }
};
/******************************************************************
 * data extracter
 ******************************************************************/
filogix.express.service.ComponentPricingDataExtractor = function(){

    this.requestType = "express.service.LenderProductRateRequest";
    this.subRequestType;
    this.lenderProfileId=-1;
    this.productId=-1;
    this.componentTypeId=-1;
    this.isUmbrella = "N";
}

filogix.express.service.ComponentPricingDataExtractor.prototype = {
    /**
     *  extracts product data for product combobox
     */
    getProductsString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.product";
        return this.sendLPRRequest(request).products;
    },
    /**
     *  extracts paymemntterm data for product term description combobox
     */
    getPaymentTermString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.paymentTerm";
        return this.sendLPRRequest(request).paymentTerms
    },
    /**
     *  extracts rate data for posted rate interest rate combobox
     */
    getRateString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.rate";
        return this.sendLPRRequest(request).rates;
    },
        /**
     *  extracts rate data for posted rate interest rateParcentage combobox
     */
    getRateParcentageString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.rateParcentage";
        return this.sendLPRRequest(request).rates;
    },
    /**
     *  extracts repaymenttype data for repayemnttype combobox
     */
    getRepaymentTypeString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.repaymentType";
        return this.sendLPRRequest(request).repaymentTypes;
    },
    /**
     *  extracts repaymenttype data for repayemnttype combobox
     */
    getComponentTypeString: function(){
        var request = this.createDefaultRequestObject();
        request.rcRequestContent.subRequestType = "json.componentType";
        return this.sendLPRRequest(request).componentType;
    },
    /**
     * the XMLHttpRequest object.
     */
    xmlHttpLPR:null,
    /**
     * executes Ajax request and resive response.
     */
    sendLPRRequest: function(request){
    	
        if(!this.xmlHttpLPR){ 
			this.xmlHttpLPR = filogix.util.ConnectManager.getJatoXmlHttpObject();
		}
    			
        // send out the request.
        this.xmlHttpLPR.send("requestType=" + this.requestType + "&requestContent=" + request.toJSONString());
        
        if (this.xmlHttpLPR.status != 200) {
            ajaxLogger.error("PricingData.js faild in sendLPRRequest, http status = "
            	+ this.xmlHttpLPR.status + ", requestType=" + this.requestType + ", content=" + request.toJSONString());
        }
                
        var resp = this.xmlHttpLPR.responseText.parseJSON().rcResponseContent;
        return resp;
    },
    /**
     * creates request data object
     */
    createDefaultRequestObject: function(){
        var request = new Object();
        request.rcRequestContent = new Object();
        request.rcRequestContent.subRequestType = "";
        request.rcRequestContent.lenderProfileId = this.lenderProfileId;
        request.rcRequestContent.productId = this.productId;
        request.rcRequestContent.componentTypeId = this.componentTypeId;
        request.rcRequestContent.isUmbrella = this.isUmbrella;
        return request;
    }
}

/******************************************************************
 * HTML element grabber
 ******************************************************************/
// par screen -----
function isPageEditable(){
    return document.getElementById("isPageEditable").value == "Y";
}

function getInitLenderProfileIdValue(){
    return document.getElementById("lenderProfileId").value;
}

// par component -----
function getFxElementById(id, params){
    return document.getElementById(
        id+"#comp"+params.componentTypeId+"#idx"+params.indexOfTiledview);
}

function isRateLocked(params){
    return getFxElementById("rateLock", params).value == "Y";
}

function getCbProductObj(params){
    return getFxElementById("product", params);
}

function getCbRateInventoryObj(params){
    return getFxElementById("rate", params);
}

function getCbRepeymentTypeObj(params){
    return getFxElementById("repaymentType", params);
}


function getInitProductIdValue(params){
    return getFxElementById("initProduct", params).value;
}

function getInitRateInventoryIdValue(params){
    return getFxElementById("initRate", params).value;
}

function getInitRepaymentTypeIdValue(params){
    return getFxElementById("initRepaymentType", params).value;
}

function getTxPaymentDescriptionObj(params){
    return getFxElementById("paymentDescription", params);
}

function getTxPostedRateObj(params){
    return getFxElementById("txPostedRate", params);
}

/******************************************************************
 * the methods which executes ComponentPricingDataExtractor
 ******************************************************************/

function getProducts(params){
    var extractor = new filogix.express.service.ComponentPricingDataExtractor();
    extractor.lenderProfileId = getInitLenderProfileIdValue();
    extractor.componentTypeId = params.componentTypeId;
    return extractor.getProductsString();
}

function getPaymentTerms(params){
    var extractor = new filogix.express.service.ComponentPricingDataExtractor();
    extractor.componentTypeId = params.componentTypeId;
    return extractor.getPaymentTermString();
}

function getInterestRates(params){
    var extractor = new filogix.express.service.ComponentPricingDataExtractor();
    extractor.productId = getCbProductObj(params).value;
    return extractor.getRateString();
}
     
function getInterestPercentage(params){
    var extractor = new filogix.express.service.ComponentPricingDataExtractor();
    extractor.productId = getCbProductObj(params).value;
    return extractor.getRateParcentageString();
}
    
function getRePaymentTypes(params){
    var extractor = new filogix.express.service.ComponentPricingDataExtractor();
    extractor.productId = getCbProductObj(params).value;
    return extractor.getRepaymentTypeString();
}

function getComponentTypeForAdd(){
    var extractor = new filogix.express.service.ComponentPricingDataExtractor();
    extractor.productId = getCbProductObjForAdd().value;
    return extractor.getComponentTypeString();
}

/******************************************************************
 * methods which populate Html item
 ******************************************************************/
function populateProduct(params, jsonArray){
    populateSelectObjByJSON(getCbProductObj(params), jsonArray, 
    CONST_JSON_COL.PRODUCT.PRODUCTID, CONST_JSON_COL.PRODUCT.PRODUCTDESC);
}

function populateRate(params, jsonArray){
    populateSelectObjByJSON(getCbRateInventoryObj(params), jsonArray, 
    CONST_JSON_COL.RATE.RATEID, CONST_JSON_COL.RATE.RATEDESC);
}
     
function populatePaymentTerm(params, paymentTermArray){
    var aPaymentTerm = findTargetRowFromJSONArray(paymentTermArray, 
    CONST_JSON_COL.PAYMENTTERM.PRODUCTID, getCbProductObj(params).value);
    
    if (aPaymentTerm) {
        getTxPaymentDescriptionObj(params).value = 
            aPaymentTerm[CONST_JSON_COL.PAYMENTTERM.PAYMENTTERMDESC];
    }
    else {
        getTxPaymentDescriptionObj(params).value = ""
    }
}

function populateRepaymentType(params, jsonArray){
    populateSelectObjByJSON(getCbRepeymentTypeObj(params), jsonArray, 
        CONST_JSON_COL.REPAYMENTTYPE.REPAYMENTID, 
        CONST_JSON_COL.REPAYMENTTYPE.REPAYMENTDESC);
}

/******************************************************************
 * utilities
 ******************************************************************/
/**
 *  selects specific row in <selectObj> which has the same value as <selectValue>
 *  if no row is fund, nothing is selected.
 * 
 * param <selectObj> - html select object
 * param <selectValue> - the value you want to select
 */
function selectRow(selectObj, selectValue){
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value == selectValue) 
            selectObj.options[i].selected = true;
    }
}

/**
 *  selects specific row in <selectObj> which has the same value as <selectValue> and delete other rows.
 *  if no row is fond, all rows are deleted. 
 * 
 * param <selectObj> - html select object
 * param <selectValue> - the value you don't want to delete
 */
function lockComboBox(selectObj, selectValue){
    var op = null;
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value == selectValue) {
            op = selectObj.options[i];
            break;
        }
    }
    selectObj.length = 0;
    if (!op) return;
    selectObj.options[0] = op;
}

/**
 *  populate html select object : <selectObj> using <jsonObj>.
 *  <keyColumn>and<DesplayColumn> must exist in <jsonObj>
 *  <keyColumn> data in <jsonObj> is set to <selectObj> as "value"
 *  <DesplayColumn> data in <jsonObj> is set to <selectObj> as desplay value
 * 
 *  param <selectObj> : html select object
 *  param <jsonObj> : list data of JSON data
 *  param <keyColumn> : column name to be used for value of select object
 *  param <DisplayColumn>: column name to be used for display data of select object
 */
function populateSelectObjByJSON(selectObj, jsonObj, keyColumn, DisplayColumn){
    selectObj.options.length = 0;
    for (var i in jsonObj) {
        selectObj.options[i] = new Option(jsonObj[i][DisplayColumn], jsonObj[i][keyColumn]);
    }
}

/**
 *  finds an element from list data : <jsonobj>
 *  
 *  param <jsonObj>: list data
 *  param <findRowName>: variable name to be checked.
 *  param <findvalue>: data to be found. 
 */
function findTargetRowFromJSONArray(jsonObj, findRowName, findValue){
    for (var i in jsonObj) {
        for (var colName in jsonObj[i]) {
            if (colName == findRowName) {
                if (jsonObj[i][colName] == findValue) {
                    return jsonObj[i];
                }
            }
        }
    }
    //alert("findTargetRowFromJSONArray : not data founded findRowName =" 
    // + findRowName + " findValue=" + findValue);
}

/******************************************************************
 * event handlers for Mortgage section
 ******************************************************************/
 /**
  * initialization method for component - mortgage.
  */
function initMortgage(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //clear all
    clearHtmlItems(params);
    // init product
    if(handleInitProductCommon(params) == false ) return;
    if (isRateLocked(params) == true && isPageEditable() == true) {
        lockComboBox(getCbProductObj(params), getInitProductIdValue(params));
    }
    // init Rate
    handleInitRateCommon(params, getInterestRates, true);
    if (isRateLocked(params) == true && isPageEditable() == true) {
        lockComboBox(getCbRateInventoryObj(params), getInitRateInventoryIdValue(params));
    }
    // init paymentTerm
    populatePaymentTerm(params, getPaymentTerms(params));
    // init repaymenttype
    handleInitRepaymentTypeCommon(params, true);
}

/**
 * this method has to be tied  onChange event of component - morgage - product combobox
 * and it changes related html fields
 */    
function handleMTGProductChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    // init Rate 
    handleInitRateCommon(params, getInterestRates, false);
    // change paymentTerm
    populatePaymentTerm(params, getPaymentTerms(params));
    //populate repaymenttype: 
    handleInitRepaymentTypeCommon(params, false);
}

/**
 * this method has to be tied  onChange event of component - MTG - Interest Rate combobox
 * and it changes related html fields
 */     
function handleMTGRateChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
   // set posted rate for update
    handleRateChangeCommon(params);
}

/******************************************************************
 * event handlers for LOC section
 ******************************************************************/
 /**
  * initialization method for component - LOC.
  */    
function initLOC(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //clear all
    clearHtmlItems(params);
    // init product
    if(handleInitProductCommon(params) == false ) return;
    // init Rate 
    handleInitRateCommon(params, getInterestRates, true);
    // init repaymenttype
    handleInitRepaymentTypeCommon(params, true);
}

/**
 * this method has to be tied  onChange event of component - LOC - product combobox
 * and it changes related html fields
 */     
function handleLOCProductChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //populate Rate
    handleInitRateCommon(params, getInterestRates, false);
    //populate repaymenttype: 
    handleInitRepaymentTypeCommon(params, false);
}

/**
 * this method has to be tied  onChange event of component - LOC - Interest Rate combobox
 * and it changes related html fields
 */     
function handleLOCRateChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
   // set posted rate for update
    handleRateChangeCommon(params);
}

/******************************************************************
 * event handlers for Overdraft section
 ******************************************************************/
 /**
  * initialization method for component - Overdraft.
  */    
function initOverdraft(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //clear all
    clearHtmlItems(params);
    // init product
    if(handleInitProductCommon(params) == false ) return;
    // init Rate 
    handleInitRateCommon(params, getInterestPercentage, true);
}

/**
 * this method has to be tied  onChange event of component - overdraft - product combobox
 * and it changes related html fields
 */     
function handleOverdraftProductChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //populate Rate
    handleInitRateCommon(params, getInterestPercentage, false);
}
 
/**
 * this method has to be tied  onChange event of component - overdraft - Interest Rate combobox
 * and it changes related html fields
 */     
function handleOverdraftRateChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
   // set posted rate for update
    handleRateChangeCommon(params);
}

/******************************************************************
 * event handlers for Credit Card section
 ******************************************************************/
 /**
  * initialization method for component - Overdraft.
  */    
function initCreditCard(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //clear all
    clearHtmlItems(params);
    // init product
    if(handleInitProductCommon(params) == false ) return;
    //init Rate
    handleInitRateCommon(params, getInterestPercentage, true);
}

/**
 * this method has to be tied  onChange event of component - overdraft - product combobox
 * and it changes related html fields
 */     
function handleCreditCardProductChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //populate Rate
    handleInitRateCommon(params, getInterestPercentage, false);
}
 
/**
 * this method has to be tied  onChange event of component - overdraft - Interest Rate combobox
 * and it changes related html fields
 */     
function handleCreditCardRateChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
   // set posted rate for update
    handleRateChangeCommon(params);
}

/******************************************************************
 * event handlers for Loan section
 ******************************************************************/
 /**
  * initialization method for component - Overdraft.
  */    
function initLoan(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //clear all
    clearHtmlItems(params);
    // init product
    if(handleInitProductCommon(params) == false ) return;
    //populate Rate
    handleInitRateCommon(params, getInterestPercentage, true);
    // init paymentTerm
    populatePaymentTerm(params, getPaymentTerms(params));
}

/**
 * this method has to be tied  onChange event of component - overdraft - product combobox
 * and it changes related html fields
 */     
function handleLoanProductChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
    //populate Rate
    handleInitRateCommon(params, getInterestPercentage, false);
    // change paymentTerm
    populatePaymentTerm(params, getPaymentTerms(params));
}
 
/**
 * this method has to be tied  onChange event of component - overdraft - Interest Rate combobox
 * and it changes related html fields
 */     
function handleLoanRateChange(componentTypeId, indexOfTiledview){
    var params = makeParametersObj(componentTypeId, indexOfTiledview);
   // set posted rate for update
    handleRateChangeCommon(params);
}

/******************************************************************
 * common utility methods for handlers
 ******************************************************************/
 /**
  * clear all  combobox and textbox related to pricing logic dynamic function 
  */
function clearHtmlItems(params){
    if(getCbProductObj(params)) getCbProductObj(params).length = 0;
    if(getCbRateInventoryObj(params)) getCbRateInventoryObj(params).length = 0;
    if(getCbRepeymentTypeObj(params)) getCbRepeymentTypeObj(params).length = 0;
    if(getTxPaymentDescriptionObj(params)) getTxPaymentDescriptionObj(params).value = "";
    if(getTxPostedRateObj(params)) getTxPostedRateObj(params).value = "";
}

/**
 *  retuns parameter object which contains componentTypeId and indexOfTiledview
 */
function makeParametersObj(componentTypeId, indexOfTiledview) {
    var params = {};
    params.componentTypeId = componentTypeId;
    params.indexOfTiledview = indexOfTiledview;
    return params;
}

/**
 * common function for postedRate update
 * param: rateArray : this is optional parameter.
 * if rateArray is passed, this method reuse it, otherwise call getInterestPercentage
 * to download source data for postedRate
 */     
function handleRateChangeCommon(params, rateArray){
    // download ratearray if it is undefined
    rateArray = rateArray ||  getInterestPercentage(params);
    var aRate = findTargetRowFromJSONArray(rateArray,
         CONST_JSON_COL.RATE.RATEID , getCbRateInventoryObj(params).value);
    //adding value to txPostedRate 
    getTxPostedRateObj(params).value =  aRate[CONST_JSON_COL.RATE.POSTEDRATE];
}

/**
 *  common function for procut combobox
 *  if the page is not editable, lock rate combobox. 
 *  returns false if no option list is created
 * */
function handleInitProductCommon(params){
    // populate combobox and set default: component.mtgProdId
    populateProduct(params, getProducts(params));
    if (getCbProductObj(params).length == 0) { 
        return false; 
    }
    // select the same value as current value in component table 
    selectRow(getCbProductObj(params), getInitProductIdValue(params));
    // if the page is not editable, lock the product drop donw list.
    if (isPageEditable() == false) {
        lockComboBox(getCbProductObj(params), getInitProductIdValue(params));
    }    
    return true;
}

/**
 *  common function for rate combobox
 *  executes <fnAjax> function to download source data
 *  if the page is not editable, lock rate combobox. 
 *  call handleRateChangeCommon to set percentage value to hidden field
 *  param: fnAjax - function name which to be called.
 *             selectefault - true : select row that has the same value as component.rateInventoryid
 *                                - false : select 1st row 
 */
function handleInitRateCommon(params, fnAjax, selectDefault){
    // populate combobox and set default: component.pricingRateInventoryId
    var rateArr = fnAjax(params);
    populateRate(params, rateArr);
    if (getCbRateInventoryObj(params).length == 0) { 
        return;
    }
    if(!selectDefault) {
        // set posted rate for update
        handleRateChangeCommon(params, rateArr);
        return;
    }
    // select the same value as current value in component table
    selectRow(getCbRateInventoryObj(params), getInitRateInventoryIdValue(params));
    // if the page is not editable, lock the rate drop donw list.
    if (isPageEditable() == false) {
        lockComboBox(getCbRateInventoryObj(params), getInitRateInventoryIdValue(params));
    }
    // set posted rate for update
    handleRateChangeCommon(params, rateArr);
}

/**
 *  common function for repayment type combobox
 *  param: isInit - true : select row that has value of component.repaymentTypeid
 *                       - false: select row that has value of mtgProd.repayemntTypeid 
 */
function handleInitRepaymentTypeCommon(params, isInit){
    var repaymentTypeArray = getRePaymentTypes(params);
    populateRepaymentType(params, repaymentTypeArray);
    if(isInit){
        // for init : select row by repaymenttypeid in component table
        selectRow(getCbRepeymentTypeObj(params), getInitRepaymentTypeIdValue(params));
    }else{
        // product onchange : select row by repaymenttypeid in mtgProd table 
        var aRepaymentType = findTargetRowFromJSONArray(repaymentTypeArray, 
        CONST_JSON_COL.REPAYMENTTYPE.PRODUCTID, getCbProductObj(params).value);
        if (aRepaymentType) {
            selectRow(getCbRepeymentTypeObj(params), aRepaymentType.repaymentId);
        }
    }
}


// --- MCM Impl Team XS_2.26 Add Component starts -----------//
/**
 * returns the component type combobox.
 */
function getCbComponentTypeObjForAdd() {
    return document.getElementById("cbComponentTypeAdd");
}

/**
 * returns the component product combobox.
 */
function getCbProductObjForAdd() {
    return document.getElementById("cbComponentProductAdd");
}

/**
 * generate  product combobox.
 */
function initCbProductForAdd(){
    var params = {};
    params.componentTypeId = -1; // get all  
    populateSelectObjByJSON(getCbProductObjForAdd(), getProducts(params), 
        CONST_JSON_COL.PRODUCT.PRODUCTID, CONST_JSON_COL.PRODUCT.PRODUCTDESC);
   // populate componentType
   handleCbProductForAddChange();
}

function handleCbProductForAddChange(){
    var compType = getComponentTypeForAdd();
    getCbComponentTypeObjForAdd().length = 0;
    getCbComponentTypeObjForAdd().options[0] =  new Option(compType.compTypeDesc, compType.compTypeId);
}

// --- MCM Impl Team XS_2.26 Add Component ends -----------//