// --------------------- Functions to handle Online Calculator and Online Help (begin) ------------------
// Functions for handleing the Goto function on the ToolBar
//  - Jump to the selected page if changed by Mouse click
//  - Also handle keyboard if user pressed 'Enter' on the combo box
// ------------------------------------------------------------------------------------------------------
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function OpenCalculatorBrowser()
{
	var winCalc = window.open('/tools/menu.html','Calculator_Document','resizable,scrollbars,status=yes,titlebar=yes,width=500,height=550,screenY=250,screenX=0');
	winCalc.focus();
}

function OpenHelpBrowser()
{
	var winHelp = window.open('/MosHelp/Index.htm','Help_Document','resizable,scrollbars,status=yes,titlebar=yes,width=500,height=550,screenY=250,screenX=0');
	winHelp.focus();
}

// --------------------- Functions to handle Online Calculator and Online Help (End) ------------------
//--Release2.1--start//
//// BXP help should be called based on the session languageId.
function OpenHelpFrenchBrowser()
{
	var winFrenchHelp = window.open('/MosHelpFrench/Index.htm','Help_Document','resizable,scrollbars,status=yes,titlebar=yes,width=500,height=550,screenY=250,screenX=0');
	winFrenchHelp.focus();
}

//New Exchange Link functions
function OpenExchangeBrowser()
{
	var code = eval("document.forms[0].elements['" + PAGE_NAME + "_hdFolderCode" + "']");
	if (code.value != -1)
	{
		var winExch = window.open(code.value,'Exchange','resizable,scrollbars,status=yes,titlebar=yes,width=800,height=600,screenY=100,screenX=100');
		winExch.focus();
	}
	//no link to open otherwise
}

function changeExchangeImage(found){
    var imgExch = "";
    var imagesPath = "../images/";
  
    if(found)
    {
    	if(LANGUAGECODE == "0")
    	{
    		imgExch = imagesPath + "tool_exchange_active.gif";
    	}
    	else
    	{
    		imgExch = imagesPath + "tool_exchange_active_fr.gif";
    	}    	
    } 
    else 
    {
    	if(LANGUAGECODE == "0")
    	{
    		imgExch = imagesPath + "tool_exchange_inactive.gif";
    	}
    	else
    	{
    		imgExch = imagesPath + "tool_exchange_inactive_fr.gif";
    	}    	
    }
    getHtmlElement("exchangeLinkImage").src = imgExch;
}

function enableExchangeLink()
{
	var code = eval("document.forms[0].elements['" + PAGE_NAME + "_hdFolderCode" + "']");
	if (code.value == "off")
	{
            getHtmlElement("exchangeLink").style.visibility = HIDDEN;
	}
	else
	{
        	getHtmlElement("exchangeLink").style.visibility = VISIBLE;
		if (code.value == -1)
		{
			changeExchangeImage(false);
		}
		else
		{
			changeExchangeImage(true);
		}
	}
}
