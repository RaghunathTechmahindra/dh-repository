/**
 * Title: componentInfoCheckBoxis.js
 * <p>
 * Description: JavaScript functions for Dynamic check boxis change.
 * @author MCM Impl Team.
 * @version 1.0 Aug 21, 2008 : artf763316. replacing XS_2.60.
 */

/** constant values */
var CONST_CLS_MIPREMIUM = "allocateMIPremium";
var CONST_CLS_TAXESCROW = "allocateTaxEscrow";
var CONST_ID_TAXPAYOR = "cbTaxPayor";
var CONST_ID_MIPAYOR = "cbMIPayor";
var CONST_CLS_MIUPFRONT = "rbMIUpfront";

/**
 * initialize method. this method is called when a screen is loaded.
 */
function initConponentInfoCheckBoxis(){
    flipChIncludeMIPremiums(grabMIUpfrontSelected());
    flipChIncludeTaxPayments(document.getElementById(CONST_ID_TAXPAYOR));
}

/**
 * returns either MIUpfront-Yes or MIUpfront-No object that selected.
 */
function grabMIUpfrontSelected(){
	var miUps = grabMIUPfrontObjects();
	for(var i in miUps){
		if (miUps[i].checked == true) return miUps[i];
	}
	//nothing is selected.
	return null;
}

/**
 * returns array of MIUpfront radio button object
 * @return array: [0] Yes, [1] No
 */
function grabMIUPfrontObjects(){
	var miUps = getElementsByClass(CONST_CLS_MIUPFRONT, "input");
	if (miUps[0].value == 'Y') return miUps;
	//make sure that value:Y is first, but it wouldn't be executed. 
	//just to be on the safe side...
    var temp = miUps[0];
    miUps[0] = miUps[1];
    miUps[1] = temp;
    return miUps; 
}

/**
 * handle MI Payor drop down list change.
 * selects MI UPfront radio button. 
 */
function selectMIUpfront(){
    var miPayor = document.getElementById(CONST_ID_MIPAYOR).value;
    //MIPayor = 0:blank, 1:Borrower, 2:Lender 
    //if MIPayer is Lender, MIUpfront will be set to Yes, else it will be set to No
    var aMiUp = grabMIUPfrontObjects()[(miPayor == "2" || miPayor == "2.0") ? 0 : 1];
    aMiUp.checked = true;
    flipChIncludeMIPremiums(aMiUp);
}

/**
 * chage disable/enable of all "Include MI Premium", 
 * based on miUpfront's selection. 
 */
function flipChIncludeMIPremiums(miUpfrontObj){
	if(!miUpfrontObj) return; //when no miUpfront selected, do nothing.
	//if miUpfront:Y is selected, 'Include Tax Payment' check boxis will be disabled.
	changeDisableAllCheckBoxies(CONST_CLS_MIPREMIUM, (miUpfrontObj.value == "Y"));
}

/**
 * chage disable/enable of all "Include Tax Payment", 
 * based on miUpfront's selection. 
 */
function flipChIncludeTaxPayments(taxPayerObj){
	if(!taxPayerObj) return;
	//Tax Payer = 0: Borrower, 1:Lender 
	//if 'Borrower' is selected, 'Include Tax Payment' check boxis will be disabled
	changeDisableAllCheckBoxies(CONST_CLS_TAXESCROW, (taxPayerObj.value == 0));
}

/**
 * turn off other "allocateMIPremium" check boxis.
 * this method is called by onChange event of each "allocateMIPremium" check box.
 */
function turnOffOtherAllocateMIPremium(chkBoxObj) {
	turnOffOtherCheckBoxis(chkBoxObj,CONST_CLS_MIPREMIUM);
}

/**
 * turn off other "allocateTaxEscrow" check boxis.
 * this method is called by onChange event of each "allocateTaxEscrow" check box.
 */
function turnOffOtherAllocateTaxEscrow(chkBoxObj) {
    turnOffOtherCheckBoxis(chkBoxObj,CONST_CLS_TAXESCROW);
}

/**
 * turn off check on all check boxis which has class=<className>
 * excepto for the same check box as <chkBoxObj>
 */
function turnOffOtherCheckBoxis(chkBoxObj, className) {
    if (chkBoxObj.checked == false) {
        return;
    }
    var targets = getElementsByClass(className, "input");
    for(var i in targets ){
        if(chkBoxObj == targets[i] ) continue;
        targets[i].checked = false;
    }
    chkBoxObj.checked = true;
}

/**
 * set desabled=<disabledValue> to all check box objects 
 * which are class=<className>
 */
function changeDisableAllCheckBoxies(className, disabledValue){
	var targets = getElementsByClass(className, "input");
    for(var i in targets ){
    	//only for disabling, uncheck.
        if (disabledValue==true)targets[i].checked = false; 
        targets[i].disabled = disabledValue;
    }
}

/**
 * utility method.
 * returns array of objects which has class attribute as <serchClass>
 * @param searchClass : class name to find.
 * @param tag : tag name to nallow down serch criteria. default: '*'
 * @param node : node to find. default: document.
 */
function getElementsByClass(searchClass, tag /*optional*/,  node /*optional*/) {
    var classElements =  new Array();
    node = node ? node : document;
    tag = tag ?  tag :  '*';
    var els = node.getElementsByTagName(tag);
    var elsLen = els.length;
    var pattern =  new RegExp('.*' + searchClass + '.*');
    for (i = 0, j = 0; i < elsLen; i++) {
        if (pattern.test(els[i].className)) {
            classElements[j] = els[i];
            j++;
        }
    }
    return classElements;
}
