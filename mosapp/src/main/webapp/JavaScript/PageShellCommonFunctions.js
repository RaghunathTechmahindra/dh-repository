// ==================== Page Shell Common JavaScript Functions (Begin)========================================
//  Note : this file should be included in every MOS page
//        Except some special pages like iWorkQueue which have different page Shell section(s)
// ================================================================================================
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function tool_click(n) {
	var itemtomove = (NTCP) ? document.pagebody : document.all.pagebody.style;
	var alertchange = (NTCP) ? document.alertbody:document.all.alertbody.style;

	if(n==1){
		var imgchange = "tool_goto"; var hide1img="tool_prev"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.gotobar : document.all.gotobar.style;
		var hideother1 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==2){
		var imgchange = "tool_prev"; var hide1img="tool_goto"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.dialogbar : document.all.dialogbar.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==3){
		var imgchange = "tool_tools"; var hide1img="tool_goto"; var hide2img="tool_prev";
		var itemtochange = (NTCP) ? document.toolpop : document.all.toolpop.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
	}
	if(n==1 || n==2 || n==3){
		if (itemtochange.visibility==VISIBLE) {
			itemtochange.visibility=HIDDEN;
			itemtomove.top = (NTCP) ? 100:134;
			changeImg(imgchange,'off');
			if(alertchange.visibility==VISIBLE){alertchange.top = (NTCP) ? 100:134;}
		}
		else {
			itemtochange.visibility=VISIBLE;
			changeImg(imgchange,'on');
			if(n!=2){itemtomove.top=150;}
			else{itemtomove.top=290;}
			if(alertchange.visibility==VISIBLE){
				if(n!=2){alertchange.top =  150;}
				else{alertchange.top = 290;}
			}
			if(hideother1.visibility==VISIBLE){hideother1.visibility=HIDDEN; changeImg(hide1img,'off');}
			if(hideother2.visibility==VISIBLE){hideother2.visibility=HIDDEN; changeImg(hide2img,'off');}
		}
	}
	if(n==4){
		var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
		itemtoshow.visibility=VISIBLE;
	}
	if(n==5){
		var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
		var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
		itemtoshow.visibility=VISIBLE;
		if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
    location.href="#loadtarget"
	}
}

if(document.images){
  //--> Modified by Billy to get the Image based on Language
  if(LANGUAGE == "fr")
  {
	tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on_fr.gif";
	tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto_fr.gif";
	tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on_fr.gif";
	tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev_fr.gif";
	tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on_fr.gif";
	tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools_fr.gif";
  }
  else
  {
  tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on.gif";
	tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto.gif";
	tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on.gif";
	tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev.gif";
	tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on.gif";
	tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools.gif";
  }
}

function changeImg(imgNam,onoff){
	if(document.images){
		document.images[imgNam].src = eval (imgNam+onoff+'.src');
	}
}


function OnTopClick(){
	self.scroll(0,0);
}

var pmWin;
var pmCont = '';

function openPMWin()
{
var numberRows = pmMsgs.length;

pmCont+='<html><head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  var lastWasSeparator = false;
  for(z=0;z<=numberRows-1;z++)
  {
  	if (pmMsgTypes[z]==3 && lastWasSeparator == true) {
  		;
	}
  	else if (pmMsgTypes[z]==2) {
  		writeRows+='<tr><td colspan=2><font size=2>'+pmMsgs[z]+'</td></tr>';
  	}
  	else {
  		var pmMessIcon = " ";
		if(pmMsgTypes[z]==0){pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
		else if(pmMsgTypes[z]==1){pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
		writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
	}
	if (pmMsgTypes[z]!=3)
		writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	lastWasSeparator = pmMsgTypes[z]==3;
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close.gif" width=84 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body></html>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  var SessionID = eval("document.forms[0].elements['" + PAGE_NAME + "_sessionUserId" + "']");
    
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+ SessionID.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');

  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWinFr()
{
var numberRows = pmMsgs.length;

pmCont+='<html><head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  var lastWasSeparator = false;
  for(z=0;z<=numberRows-1;z++)
  {
  	if (pmMsgTypes[z]==3 && lastWasSeparator == true) {
  		;
	}
  	else if (pmMsgTypes[z]==2) {
  		writeRows+='<tr><td colspan=2><font size=2>'+pmMsgs[z]+'</td></tr>';
  	}
  	else {
  		var pmMessIcon = " ";
		if(pmMsgTypes[z]==0){pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
		else if(pmMsgTypes[z]==1){pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
		writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
	}
	if (pmMsgTypes[z]!=3)
		writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	lastWasSeparator = pmMsgTypes[z]==3;
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close_fr.gif" width=84 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body></html>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  var SessionID = eval("document.forms[0].elements['" + PAGE_NAME + "_sessionUserId" + "']");
    
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+ SessionID.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');

  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
	pmWin.document.open();
	pmWin.document.write(pmCont);
	pmWin.document.close();
	pmWin.focus();
}


function generateAM()
{
var numberRows = amMsgs.length;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
	amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	var writeRows='';
  	var lastWasSeparator = false;

	for(z=0;z<=numberRows-1;z++)
  	{
  		if (amMsgTypes[z]==3 && lastWasSeparator == true) {
  			;
		}
  		else if (amMsgTypes[z]==2) {
			writeRows+='<tr><td colspan=2><font size=2>'+pmMsgs[z]+'</td></tr>';
		}
		else  {
			var amMessIcon= " ";
			if(amMsgTypes[z]==0){amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
			else if(amMsgTypes[z]==1){amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
			writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
		}
		if (amMsgTypes[z]!=3)
			writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

		lastWasSeparator = amMsgTypes[z]==3;

  	}
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}

//FXP27053 -- Deal Notes Lazy Load
function popDealNotes() {
	
	var jsessionid = document.forms[0].action;
    if (jsessionid.indexOf(";") > 0) {
        jsessionid = jsessionid.substr(jsessionid.indexOf(";"));
    } else {
        jsessionid = "";
    }
    var dealNotesPopup = window.open("pgDealNotes2" + jsessionid, "dealnotes", "resizable,scrollbars,status=yes,titlebar=yes,width=800,height=600,hotkeys");
	dealNotesPopup.moveTo(0,0);
	dealNotesPopup.focus();

	//window.open("pgSignOn", "signon", "resizable,scrollbars,status=yes,titlebar=yes,width=800,height=600,hotkeys");
}
//End of FXP27053

// ==================== Page Shell Common JavaScript Functions (End)========================================
