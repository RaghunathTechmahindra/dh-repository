// ------------------------------------ Functions to handle Goto (begin) --------------------------------
// Functions for handleing the Goto function on the ToolBar
//  - Jump to the selected page if changed by Mouse click
//  - Also handle keyboard if user pressed 'Enter' on the combo box
// ------------------------------------------------------------------------------------------------------
var isGotoMeunDown = false;

var AIGUG_COLLAPSED_CANCELATION = false;
var MI_COLLAPSED_CANCELATION = false;

function handleGotoKetPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
//alert("The key = " + keycode);
  if (keycode == 13)
  {
    performGoto();
    return false;
  }

  return true;
}

function performGoto()
{
	setSubmitFlag(true);
	//document.forms[0].action="../MosSystem/" + document.forms[0].name + ".btProceed_onWebEvent(btProceed)";
  document.forms[0].action += "?" + PAGE_NAME + "_btProceed=";
	//alert("document.forms[0].action= " + document.forms[0].action);
  // check if any outstanding request
  if(IsSubmitButton())
	  document.forms[0].submit();
}

function toggleMenuDownFlag()
{
  // For testing -- BILLY 01Apr2002
  //var theField =(NTCP) ? event.target : event.srcElement;

	if(isGotoMeunDown)
  {
		//theField.size = 0;
    isGotoMeunDown = false;
  }
	else
  {
		//theField.size = 20;
    isGotoMeunDown = true;
  }
}

// Added the Validation functions for Deal # validation
function isDigitGoto(inp)
{
	if(inp == '0' || inp == '1' || inp == '2'  || inp == '3' || inp == '4' ||
   	   inp == '5' || inp == '6' || inp == '7'  || inp == '8' || inp == '9')
	{
	return true;
	}

	return false;
}

// Function to check if the input value is an Integer
function isIntegerGoto(inValue)
{
	for(var i=0;i<inValue.length;i++)
	{
		if(  isDigitGoto(inValue.charAt(i)) == false )
			return false;
	}
	return true;
}

// Function to check if the attached field value is an Integer with requestLength
//		Note : if requestLength = 0 means any length
function isFieldValideDealNum()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;
    var min = 0;
    var max = 2147483647;
    var requestLength = 0;

	//FXP23569 Trim all the spaces front and end of the value
	theFieldValue = theFieldValue.replace(/^\s+|\s+$/g, "");
	//Start of FXP26561: 
	//Need to set the value in the form after trimming white spaces.
	theField.value = theFieldValue;
	//End of FXP26561
	
	if( !isIntegerGoto(theFieldValue) )	{
		alert(DEAL_NUM_NUMERIC_ONLY);
		var tmp = theField.value;
		theField.focus();
		theField.select();
		return false;
	/**
     * BEGIN FIX FOR FXP21088 
     * Mohan V Premkumar
     * 04/07/2008
     */
	} else if(!valuevalidation(theFieldValue, min, max)) {
	   alert(printf(DEAL_NUM_OUT_OF_RANGE, min, max));
	   theField.focus();
       theField.select();
	   return false;
	/**
     * END FIX FOR FXP21088 
     */   
	} else {
	
		// Check Length
		if( requestLength > 0 )
		{
			if( theFieldValue.length != requestLength )
			{
				alert(printf(INPUT_NUM_DIGITS, requestLength));
				theField.focus();
				theField.select();
				return false;
			}
		}
	}
	return true;
}

/**
 * BEGIN FIX FOR FXP21088 
 * Mohan V Premkumar
 * 04/07/2008
 */
function valuevalidation(entered, min, max) {
    // Value Validation by Henrik Petersen / NetKontoret
    // Explained at www.echoecho.com/jsforms.htm
    // Please do not remove this line and the two lines above.
    with (entered)
    {        
        checkvalue=parseInt(entered)
        if ((checkvalue > max) || (checkvalue < min)){
           return false;
        } else {
           return true;
        }
    }
}
/**
 * END FIX FOR FXP21088 
 */ 
 
// Also included the default button stuffes here
var isSubmitFlag = true;
var isSubmited = false;

function setSubmitFlag(flag)
{
	isSubmitFlag = flag;
	enableQualifyProduct();
}

function enableQualifyProduct()
{
	if(document.getElementById("cbQualifyProductType") != null)
	{
		document.getElementById("cbQualifyProductType").disabled  = false;
		document.getElementById("txQualifyRate").disabled  = false;
		//document.forms[0].pgUWorksheet_cbQualifyProductType.disabled = false;
		//document.forms[0].pgUWorksheet_txQualifyRate.disabled = false;
	}
}

function setSubmitedFlag(flag)
{
	isSubmited = flag;
}

function IsSubmitButton()
{
  if(!isSubmited && isSubmitFlag)
  {
      // 5.0 MI, deleted the code for MI screen
      // Set the flag to prevent submit again
      isSubmited = true;
      return (true);
  }
  else
  {
    if(isSubmited)
      alert(NO_SECOND_SELECTION);
    return(false);
  }
}

function IsSubmited()
{
  if(isSubmited)
  {
    alert(NO_SECOND_SELECTION);
    return(false);
  }
  else
  {
    return(true);
  }
}

//Test by BILLY 15July2002
function performActMsgBtSubmit(theParameters)
{

    setSubmitFlag(true);
    document.forms[0].action += "?" + PAGE_NAME + "_btActMsg=" + theParameters;
    // check if any outstanding request
    if(IsSubmitButton()) {
        //5.0 MI, deleted the code for MI screen
        document.forms[0].submit();
    }

}


// ------------------------------------ Functions to handle Goto (end) --------------------------------