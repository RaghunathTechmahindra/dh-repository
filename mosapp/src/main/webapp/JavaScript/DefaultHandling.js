// ========== Some Utility functions for Validation and Default handling -- By BILLY 31Jan2001 (BEGIN) =================
// =====================================================================================================================
// Function to set defaults of one or more inputs (usually TEXT inputs) based upon the selected option
// of a choice box input (usually a SELECT input). The choice box and associated inputs may occur just once in the form,
// or there may be repeated multiple occurance of same-named inputs in multiple rows. The multiple row scenarion is
// demonstrated in the following table fragment showing named inputs "cbFeeDescription" (option values omitted),
// "tbFeeAmount" and  "tbFeeDisc" repeated in the two rows of a table.
//
// Modifications : Some function has been modified for JATO naming format -- By BILLY 08July2002
//                 Bug fix on some functions to handle non-repeated fileds -- By BILLY 16July2003
// =====================================================================================================================

// Input -- selectName : short Source control name e.g. "cbIncomeType"
//          inputNames : List of target field short name (same row) e.g. "['txEmployPrecentageIncludeInGDS', ...]"
//  Modified by BILLY 08July2002
function setDefaults(selectName, inputNames)
{
	// alert("selectName = " + selectName);
	// alert("inputNames = " + inputNames);
	// alert("nonSelectControl = " + nonSelectControl);

	var changedSelect =(NTCP) ? event.target : event.srcElement;
	var selectedOptionValue = changedSelect.value;
  var control_Full_Name = changedSelect.name;

	// alert("selected == " + selectedOptionValue);

	if (selectedOptionValue == -1)
		return;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(changedSelect);

	var dfltNdx = getDefaultNdx(selectedOptionValue, "VALS" + selectName);

	// alert("default index = " + dfltNdx);

	if (dfltNdx == -1)
		return;

	var numInputsToDefault = inputNames.length;

  var name1stPart = get1stPartName(control_Full_Name);

	for (i = 0; i < numInputsToDefault; ++i)
	{
		dfltVals  = eval("VALS" + inputNames[i]);

		dfltValue = dfltVals[dfltNdx];

		inputRowNdx = "";

		if (rowNdx != -1)
    {
			inputRowNdx = rowNdx+"]_";
      // Not set the value if the target field disabled
      var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + inputNames[i] + "']");
      if( theControl.disabled == false ) theControl.value = dfltValue;
    }
    else
    {
      // Not set the value if the target field disabled
      var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + inputNames[i] + "']");
      if( theControl.disabled == false ) theControl.value = dfltValue;
    }
	}
}

//--XC_BNAME_CR--start--//
function setBooleanDefaults(selectName, inputNames)
{
	// alert("selectName = " + selectName);
	// alert("inputNames = " + inputNames);
	// alert("nonSelectControl = " + nonSelectControl);

	var changedSelect =(NTCP) ? event.target : event.srcElement;
	var selectedOptionValue = changedSelect.value;
  	var control_Full_Name = changedSelect.name;
  	var ret = false; // default value
  	
//alert("selected == " + selectedOptionValue);

	if (selectedOptionValue == -1)
		return;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(changedSelect);

	var dfltNdx = getDefaultNdx(selectedOptionValue, "VALS" + selectName);

//alert("default index = " + dfltNdx);

	if (dfltNdx == -1)
		return;

	var numInputsToDefault = inputNames.length;

  	var name1stPart = get1stPartName(control_Full_Name);

	for (i = 0; i < numInputsToDefault; ++i)
	{
		dfltVals  = eval("VALS" + inputNames[i]);

		dfltValue = dfltVals[dfltNdx];

		inputRowNdx = "";

		if (rowNdx != -1)
    		{
			inputRowNdx = rowNdx+"]_";
      			// Not set the value if the target field disabled
      			var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + inputNames[i] + "']");
      			if( theControl.disabled == false )
      			{
      				theControl.value = dfltValue;
      				ret = true;
      			}	
    		}
    		else
    		{
      			// Not set the value if the target field disabled
      			var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + inputNames[i] + "']");
      			if( theControl.disabled == false )
      			{
      				theControl.value = dfltValue;
      				ret = true;
      			}
    		}
	} // end for
	
	return ret;
}
//--XC_BNAME_CR--end--//


// =====================================================================================================================
//	Function to set defaults to all appearance of the corresponding fields
// =====================================================================================================================
// Input -- selectName : short Source control name e.g. "cbIncomeType"
//          inputNames : List of target field short name (same row) e.g. "['txEmployPrecentageIncludeInGDS', ...]"
//          control_Full_Name : one fullFieldName e.g. "pgPageName_RepeatName[2]_controlName"
//
// Note : Assume use only on Repeated rows
//
function setDefaultsAll(selectName, inputNames, control_Full_Name)
{
  // get number of repeated rows within the same repeated group
	var numOfRepeatRows = getNumOfRow(control_Full_Name);
	var numInputsToDefault = inputNames.length;

  var name1stPart = get1stPartName(control_Full_Name);

	for( x=0; x < numOfRepeatRows ; ++x )
	{
    currRowNdx = x+"]_";
    //var changedSelect = eval("document.forms[0]." + selectName + currRowNdx);
    var changedSelect = eval("document.forms[0].elements['" + name1stPart + currRowNdx + selectName + "']");
		var selectedOptionValue = getSelectedOptionValue(changedSelect);
    var dfltNdx = getDefaultNdx(selectedOptionValue, "VALS" + selectName);

	  // alert("default index = " + dfltNdx);

	  if (dfltNdx == -1)
      continue;

	  for (i = 0; i < numInputsToDefault; ++i)
	  {
      // alert("selected == " + selectedOptionValue);

	    if (selectedOptionValue == -1)
		      continue;

		  dfltVals  = eval("VALS" + inputNames[i]);

		  dfltValue = dfltVals[dfltNdx];

		  // Not set the value if the target field disabled
      //var theControl = eval("document.forms[0]." + inputNames[i] + currRowNdx);
      var theControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + inputNames[i] + "']");
		  if( theControl.disabled == false ) theControl.value = dfltValue;
	  }
  }
}

// =====================================================================================================================
//	Function to get the Selected value of a Selectable Control
//		return -1 if nothing selected
// =====================================================================================================================
function getSelectedOptionValue(comboBox)
{
	for(i=0; i<comboBox.options.length;i++)
	{
		if( comboBox.options[i].selected == true)
	   		return comboBox.options[i].value;
	}

	return -1;
}

// =====================================================================================================================
//	Function to find the matched entry from the Array (listName) and returns the corresponding index
//		Note : return -1 if not matched
// =====================================================================================================================
function getDefaultNdx(value, listName)
{
	eval("var lst = " + listName);

	// alert(listName + " = " + lst);

	lim = lst.length;

	// alert(" comparison is string type = " + (isNaN(lst[0])) + ", value = " + value);

	for(i=0; i< lim; i++)
	{
		if(value == lst[i])
			return i;
	}

	// alert("no match == " + value);

	return -1;
}

// ===============================================================================================================
// Function to check all appearance of myControl (myControlName) and set the Targeted controls (theNamesToBeSet)
//	to disabled = true and value = defaultValue if the value of myControl == checkValue. Otherwise, the trageted
//	controls with be set to Enable (disabled = false).
//================================================================================================================
function setAllValueAndDisableIfMatched(myControlName, theNamesToBeSet, checkValue, defaultValue, control_Full_Name)
{
	// get number of repeated rows with a non-select control within the same repeated group
	var numOfRepeatRows = getNumOfRow(control_Full_Name);
	var numInputsToCheck = theNamesToBeSet.length;

  var name1stPart = get1stPartName(control_Full_Name);

	for( x=0; x < numOfRepeatRows ; ++x )
	{
		currRowNdx = x+"]_";
		//var myControl = eval("document.forms[0]." + myControlName + inputRowNdx);
    var myControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + myControlName + "']");
    var myValue = myControl.value;
//alert("The value of " + x + " : " + 	myValue + " and the checkValue : " + checkValue);
		if( trim(myValue) == trim(checkValue) )
		{
			for (i = 0; i < numInputsToCheck ; ++i)
			{
				//var theControl = eval("document.forms[0]." + theNamesToBeSet[i] + inputRowNdx);
        var theControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + theNamesToBeSet[i] + "']");
				theControl.value = defaultValue;
				theControl.disabled = true;
			}
		}
		else
		{
			for (i = 0; i < numInputsToCheck ; ++i)
			{
				//var theControl = eval("document.forms[0]." + theNamesToBeSet[i] + inputRowNdx);
        var theControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + theNamesToBeSet[i] + "']");
        theControl.disabled = false;
			}
		}
 	}
}

// ===============================================================================================================
// Function to check all appearance of myControl (myControlName) and set the Targeted controls (theNamesToBeSet)
//	to disabled = true and value = defaultValue if the value of myControl != checkValue. Otherwise, the trageted
//	controls with be set to Enable (disabled = false).
//================================================================================================================
function setAllValueAndDisableIfNotMatched(myControlName, theNamesToBeSet, checkValue, defaultValue, control_Full_Name)
{
	// get number of repeated rows with a non-select control within the same repeated group
	var numOfRepeatRows = getNumOfRow(control_Full_Name);
	var numInputsToCheck = theNamesToBeSet.length;

  var name1stPart = get1stPartName(control_Full_Name);

	for( x=0; x < numOfRepeatRows ; ++x )
	{
		currRowNdx = x+"]_";
		//var myControl = eval("document.forms[0]." + myControlName + inputRowNdx);
    var myControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + myControlName + "']");
    var myValue = myControl.value;
//alert("The value of " + x + " : " + 	myValue + " and the checkValue : " + checkValue);
		if( trim(myValue) != trim(checkValue) )
		{
			for (i = 0; i < numInputsToCheck ; ++i)
			{
				//var theControl = eval("document.forms[0]." + theNamesToBeSet[i] + inputRowNdx);
        var theControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
				theControl.disabled = true;
			}
		}
		else
		{
			for (i = 0; i < numInputsToCheck ; ++i)
			{
				//var theControl = eval("document.forms[0]." + theNamesToBeSet[i] + inputRowNdx);
        var theControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + theNamesToBeSet[i] + "']");
        theControl.disabled = false;
			}
		}
 	}
}

// =======================================================================================
// Function to populate the field value to other controls with no condition
// =======================================================================================
function populateValueTo(theNamesToBePopulate)
{
	//alert("myControlName = " + myControlName );
	////alert("theNamesToBePopulate= " + theNamesToBePopulate);
	//alert("nonSelectControl = " + nonSelectControl);

	var theTarget =(NTCP) ? event.target : event.srcElement;
	var theValue = theTarget.value;
  	var control_Full_Name = theTarget.name;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(theTarget);

	////alert("default row index = " + rowNdx + " Value = " + theValue);

	var numInputsToCheck = theNamesToBePopulate.length;
        ////alert("NumberToCheck: " + numInputsToCheck);

  	var name1stPart = get1stPartName(control_Full_Name);

        ////alert("Name1stPart: " + name1stPart);

	for (i = 0; i < numInputsToCheck ; ++i)
	{
		inputRowNdx = "";
		if (rowNdx != -1)
    {
      inputRowNdx = rowNdx+"]_";
      //var theControl = eval("document.forms[0]." + theNamesToBePopulate[i] + inputRowNdx);
      var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBePopulate[i] + "']");
      theControl.value = theValue;
      ////alert("TheNamesToBePopulate[" + i + "]: " + theNamesToBePopulate[i]);

    }
    else if (rowNdx == -1)
    {
      //// Proper Generic combination for the non-repeatable.
      var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_"  + theNamesToBePopulate[i] + "']");
      theControl.value = theValue;
      ////alert("TheControl in minus 1 mode: " + theControl);
    }
	}
}

// =======================================================================================
// Function to populate the all field value to other controls with no condition
// =======================================================================================
function populateAllValueTo(myControlName , theNamesToBePopulate, control_Full_Name)
{
	// get number of repeated rows with a non-select control within the same repeated group
	var numOfRepeatRows = getNumOfRow(control_Full_Name);
  var numInputsToCheck = theNamesToBePopulate.length;

  var name1stPart = get1stPartName(control_Full_Name);

	for( x=0; x < numOfRepeatRows ; ++x )
	{
    currRowNdx = x+"]_";

		//var myControl = eval("document.forms[0]." + myControlName + currRowNdx);
    var myControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + myControlName + "']");
	  var theValue = myControl.value;

	  for (i = 0; i < numInputsToCheck ; ++i)
	  {
		  //var theControl = eval("document.forms[0]." + theNamesToBePopulate[i] + currRowNdx);
      var theControl = eval("document.forms[0].elements['" + name1stPart + currRowNdx + theNamesToBePopulate[i] + "']");
      theControl.value = theValue;
	  }
  }
}

// ===============================================================================================
// Function to set the defaultValue to the targeted controls (theNamesToBeSet) if the value of
//	myControlName == myValue on the same row.
// ===============================================================================================
function setValueIfMatched(theNamesToBeSet, myValue, defaultValue)
{
	var theTarget =(NTCP) ? event.target : event.srcElement;
	var theValue = theTarget.value;
  var control_Full_Name = theTarget.name;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(theTarget);

	//alert("default row index = " + rowNdx + " Value = " + theValue);

	var numInputsToCheck = theNamesToBeSet.length;
  var name1stPart = get1stPartName(control_Full_Name);

	if( trim(theValue) == trim(myValue) )
	{
		for (i = 0; i < numInputsToCheck ; ++i)
		{
      if (rowNdx != -1)
      {
        inputRowNdx = rowNdx+"]_";
        //var theControl = eval("document.forms[0]." + theNamesToBeSet[i] + inputRowNdx);
        //alert("The Control name = " + "document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
      }
      else
      {
        //// Proper Generic combination for the non-repeatable.
        var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
      }
		}
	}
}

// ===============================================================================================
// Function to set the value from otherFieldName.value to the targeted controls (theNamesToBeSet) if the value of
//	myControlName == myValue on the same row.
// ===============================================================================================
function setValueIfMatchedFromOtherField(theNamesToBeSet, myValue, otherFieldName)
{
	var theTarget =(NTCP) ? event.target : event.srcElement;
	var theValue = theTarget.value;
  var control_Full_Name = theTarget.name;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(theTarget);

	//alert("default row index = " + rowNdx + " Value = " + theValue);

	var numInputsToCheck = theNamesToBeSet.length;
  var name1stPart = get1stPartName(control_Full_Name);

	if( trim(theValue) == trim(myValue) )
	{
		for (i = 0; i < numInputsToCheck ; ++i)
		{
      if (rowNdx != -1)
      {
        inputRowNdx = rowNdx+"]_";
        //var theControl = eval("document.forms[0]." + theNamesToBeSet[i] + inputRowNdx);
        var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        //var theOtherCtl = eval("document.forms[0]." + otherFieldName + inputRowNdx);
        var theOtherCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + otherFieldName + "']");
        theControl.value = theOtherCtl.value;
      }
      else
      {
        var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theNamesToBeSet[i] + "']");
        var theOtherCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + otherFieldName + "']");
        theControl.value = theOtherCtl.value;
      }
		}
	}
}

// ===============================================================================================
// Function to set the defaultValue to the targeted controls (theNamesToBeSet) if the value of
//	myControlName != myValue on the same row.
// ===============================================================================================
function setValueIfNotMatched(theNamesToBeSet, myValue, defaultValue)
{
	var theTarget =(NTCP) ? event.target : event.srcElement;
	var theValue = theTarget.value;
  var control_Full_Name = theTarget.name;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(theTarget);

	//alert("default row index = " + rowNdx + " Value = " + theValue);

	var numInputsToCheck = theNamesToBeSet.length;
  var name1stPart = get1stPartName(control_Full_Name);

	if( trim(theValue) != trim(myValue) )
	{
		for (i = 0; i < numInputsToCheck ; ++i)
		{
      if (rowNdx != -1)
      {
        inputRowNdx = rowNdx+"]_";
        var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
      }
      else
      {
        //// Proper Generic combination for the non-repeatable.
        var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
      }
		}
	}
}

// ===============================================================================================
// Function to set the value from otherFieldName.value to the targeted controls (theNamesToBeSet) if the value of
//	myControlName <> myValue on the same row.
// ===============================================================================================
function setValueIfNotMatchedFromOtherField(theNamesToBeSet, myValue, otherFieldName)
{
	var theTarget =(NTCP) ? event.target : event.srcElement;
	var theValue = theTarget.value;
  var control_Full_Name = theTarget.name;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(theTarget);

	//alert("default row index = " + rowNdx + " Value = " + theValue);

	var numInputsToCheck = theNamesToBeSet.length;
  var name1stPart = get1stPartName(control_Full_Name);

	if( trim(theValue) != trim(myValue) )
	{
		for (i = 0; i < numInputsToCheck ; ++i)
		{
      if (rowNdx != -1)
      {
        inputRowNdx = rowNdx+"]_";
        var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        var theOtherCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + otherFieldName + "']");
        theControl.value = theOtherCtl.value;
      }
      else
      {
        //// Proper Generic combination for the non-repeatable.
        var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theNamesToBeSet[i] + "']");
        var theOtherCtl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + otherFieldName + "']");
        theControl.value = theOtherCtl.value;
      }
		}
	}
}

// ===============================================================================================
// Function to set the defaultValue and disable the targeted controls (theNamesToBeSet) if the value of
//	myControlName == myValue on the same row.
// ===============================================================================================
function setValueAndDisableIfMatched(theNamesToBeSet, myValue, defaultValue)
{
	var theTarget =(NTCP) ? event.target : event.srcElement;
	var theValue = theTarget.value;
  var control_Full_Name = theTarget.name;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(theTarget);

	//alert("default row index = " + rowNdx + " Value = " + theValue);

	var numInputsToCheck = theNamesToBeSet.length;
  var name1stPart = get1stPartName(control_Full_Name);

	if( trim(theValue) == trim(myValue) )
	{
		for (i = 0; i < numInputsToCheck ; ++i)
		{
      if (rowNdx != -1)
      {
        inputRowNdx = rowNdx+"]_";
        var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
        theControl.disabled = true;
      }
      else
      {
        var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
        theControl.disabled = true;
      }
		}
	}
	else
	{
		for (i = 0; i < numInputsToCheck ; ++i)
		{
      if (rowNdx != -1)
      {
        inputRowNdx = rowNdx+"]_";
        var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        theControl.disabled = false;
      }
      else
      {
        var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theNamesToBeSet[i] + "']");
        theControl.disabled = false;
      }
		}
	}
}

// ===============================================================================================
// Function to set the defaultValue and disable the targeted controls (theNamesToBeSet) if
//	myControlName.value != myValue on the same row.
// ===============================================================================================
function setValueAndDisableIfNotMatched(theNamesToBeSet, myValue, defaultValue)
{
  var theTarget =(NTCP) ? event.target : event.srcElement;
	var theValue = theTarget.value;
  var control_Full_Name = theTarget.name;

	// determine row (if repeated)
	var rowNdx = getRepeatRowId(theTarget);

	alert("default row index = " + rowNdx + " Value = " + theValue);

	var numInputsToCheck = theNamesToBeSet.length;
  var name1stPart = get1stPartName(control_Full_Name);

	if( trim(theValue) != trim(myValue) )
	{
		for (i = 0; i < numInputsToCheck ; ++i)
		{
      if (rowNdx != -1)
      {
        inputRowNdx = rowNdx+"]_";
        var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
        theControl.disabled = true;
      }
      else
      {
        var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theNamesToBeSet[i] + "']");
        theControl.value = defaultValue;
        theControl.disabled = true;
      }
		}
	}
	else
	{
		for (i = 0; i < numInputsToCheck ; ++i)
		{
      if (rowNdx != -1)
      {
        inputRowNdx = rowNdx+"]_";
        var theControl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + theNamesToBeSet[i] + "']");
        theControl.disabled = false;
      }
      else
      {
        var theControl = eval("document.forms[0].elements['" + PAGE_NAME + "_" + theNamesToBeSet[i] + "']");
        theControl.disabled = false;
      }
		}
	}
}

// =======================================================================================
// Function to left trim a String
// =======================================================================================
function ltrim(argvalue) {

  while (1) {
    if (argvalue.substring(0, 1) != " ")
      break;
    argvalue = argvalue.substring(1, argvalue.length);
  }

  return argvalue;
}

// =======================================================================================
// Function to right trim a String
// =======================================================================================
function rtrim(argvalue) {

  while (1) {
    if (argvalue.substring(argvalue.length - 1, argvalue.length) != " ")
      break;
    argvalue = argvalue.substring(0, argvalue.length - 1);
  }

  return argvalue;
}

// =======================================================================================
// Function to left and right trim a String
// =======================================================================================
function trim(argvalue) {
  var tmpstr = ltrim(argvalue);

  return rtrim(tmpstr);

}

// ========== Some Utility functions for Validation and Default handling -- By BILLY 31Jan2001 (END) =================

