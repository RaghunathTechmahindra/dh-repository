
function generateDocument(flag) {
	if(provId == provON) {
		handleChangeBrokerAsLender();
	}
	var fff = eval("document.forms[0]." + pageName + "_hdGenerateDocumentPdf.value");

	if (fff == "true") 
	{
		// set the flag to false
		//newValueForFlag = eval("document.forms[0]." + pageName + "_hdGenerateDocument");
		//newValueForFlag.value = "false";
		
		var genDocFlag = document.getElementById(pageName +"_hdGenerateDocumentPdf"); 
		genDocFlag.value="false";

		var genDocFlag2 = document.getElementById(pageName +"_hdGenerateDocumentPdf"); 

		var dealId = eval("document.forms[0]." + pageName + "_stDealId.value");
		var copyId = eval("document.forms[0]." + pageName + "_hdCopyId.value");
		var provAdbbr;
		if( provId== provAB) {
			provAbbr = "AB"; 
		} else if(provId== provNL ) {
			provAbbr = "NL";
		} else if(provId == provNS) {
			provAbbr = "NS";
		} else if(provId ==provPEI ) {
			provAbbr = "PE";
		} else if(provId == provON) {
			provAbbr = "ON";
		}
		alert(provAbbr);
		// change the language id to 1 for french and province if not ON
		window.open("DocumentGenerator?dealId=" + dealId + "&copyId=" + copyId + "&languageId=0" + "&provinceAbbr=" + provAbbr , 

"DisclosureStatement");
	}
}



function setCancelFlag(tof)
{
	//Bug# 1698 fix - start
	//added condition check to execute block only for province Ontario
	if(provId == provON){
	 handleChangeBrokerAsLender();
	 var instr = document.getElementById(pageName +"_hdInstr"); 
	if(tof || instr != null)
		instr.value="cancel";
	else
		instr.value="";
	}
	//Bug# 1698 fix - end
}



function isPositiveNumeric(isDecimal)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	if(!isDecimal || (theFieldValue < 0))
	{
		alert(printf(NUMERIC_ONLY));	//use NUMERIC_ONLY according to FS.
		theField.focus();
		theField.select();
		return false;
	}

	return true;
}

function limitLength(len)
{
	var theField = event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	if(theFieldValue.length > len)
	{
		theField.blur();
		theField.focus();
		return false;
	}

	return true;
}

function alertMsg(len)
{
	var theField = event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	if(theFieldValue.length > len)
	{
		alert("The maximum number of characters is " + len + ". Please adjust your input accordingly");
		theField.focus();
		var rng = theField.createTextRange();
		rng.move("textedit");
		rng.select();
	}
}
