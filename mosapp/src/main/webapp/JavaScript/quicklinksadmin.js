/*******************************************************************************
 * Quick Link JS page
 */
/**
 * the constructor.
 */

/*
 *  pgQuick Links Java script code - start
 */
function enableTextFields(rowNum)
{
	document.getElementById('urlAddress['+rowNum +']').disabled=false;
	document.getElementById('descEnglish['+rowNum +']').disabled=false;
	document.getElementById('descFrench['+rowNum +']').disabled=false;
}

function disableTextFields(rowNum)
{
	document.getElementById('urlAddress['+rowNum +']').disabled=true;
	document.getElementById('descEnglish['+rowNum +']').disabled=true;
	document.getElementById('descFrench['+rowNum +']').disabled=true;
}

function clearTextFields(rowNum)
{
	document.getElementById('urlAddress['+rowNum +']').value="";
	document.getElementById('descEnglish['+rowNum +']').value="";
	document.getElementById('descFrench['+rowNum +']').value="";
}

function moveUp(obj) 
{	
	var NdxIndex = parseInt(obj.value) - 1;
	var currIndex = parseInt(obj.value);
	
	if(document.getElementById("stViewOnlyTag") != null && (document.pgQuickLinks.stViewOnlyTag.src.indexOf("ViewOnly", 0) != -1))
		return;
	
	if (document.getElementById('urlAddress['+obj.value+']').value == "" &
			document.getElementById('descEnglish['+obj.value +']').value == "" &
			document.getElementById('descFrench['+obj.value +']').value == "" )
	{
		return;
	}
	
	var tempAddress = document.getElementById('urlAddress['+NdxIndex+']').value;
	var tempEnglish = document.getElementById('descEnglish['+NdxIndex+']').value;
	var tempFrench = document.getElementById('descFrench['+NdxIndex+']').value;
	
	document.getElementById('urlAddress['+NdxIndex+']').value = document.getElementById('urlAddress['+obj.value +']').value;
	document.getElementById('descEnglish['+NdxIndex +']').value = document.getElementById('descEnglish['+obj.value +']').value;
	document.getElementById('descFrench['+NdxIndex +']').value = document.getElementById('descFrench['+obj.value +']').value;
	
	document.getElementById('urlAddress['+currIndex+']').value = tempAddress;
	document.getElementById('descEnglish['+currIndex +']').value = tempEnglish;
	document.getElementById('descFrench['+currIndex +']').value = tempFrench; 
	
	// Change the add / edit button to based on URL
	
	if(document.getElementById('urlAddress['+currIndex +']').value.length == 0 || 
			document.getElementById('descEnglish['+currIndex +']').value.length == 0 || 
			document.getElementById('descFrench['+currIndex +']').value.length == 0 ) 
		if(LANGUAGECODE == "0")
			document.pgQuickLinks.btAdd[currIndex].src="../images/add_sm.gif";
		else 
			document.pgQuickLinks.btAdd[currIndex].src="../images/add_sm_fr.gif";
	else 
		if(LANGUAGECODE == "0")
			document.pgQuickLinks.btAdd[currIndex].src="../images/edit_sm.gif";
		else
			document.pgQuickLinks.btAdd[currIndex].src="../images/edit_sm_fr.gif";
	
	disableTextFields(currIndex);
	
	if(LANGUAGECODE == "0") {
		document.pgQuickLinks.btTestLink[currIndex].src="../images/testlink_sm_disabled.gif";
		document.pgQuickLinks.btClear[currIndex].src="../images/clear_sm_disabled.gif";
	} else {
		document.pgQuickLinks.btTestLink[currIndex].src="../images/testlink_sm_fr_disabled.gif";
		document.pgQuickLinks.btClear[currIndex].src="../images/clear_sm_fr_disabled.gif";
	} 
	
	if(document.getElementById('urlAddress['+NdxIndex +']').value.length == 0 || 
			document.getElementById('descEnglish['+NdxIndex +']').value.length == 0 || 
			document.getElementById('descFrench['+NdxIndex +']').value.length == 0 ) 
		if(LANGUAGECODE == "0")
			document.pgQuickLinks.btAdd[NdxIndex].src="../images/add_sm.gif";
		else 
			document.pgQuickLinks.btAdd[NdxIndex].src="../images/add_sm_fr.gif";
	else 
		if(LANGUAGECODE == "0")
			document.pgQuickLinks.btAdd[NdxIndex].src="../images/edit_sm.gif";
		else 
			document.pgQuickLinks.btAdd[NdxIndex].src="../images/edit_sm_fr.gif";
}

function moveDown(obj) 
{
	var NdxIndex = parseInt(obj.value) + 1;
	var currIndex = parseInt(obj.value);
	
	if(document.getElementById("stViewOnlyTag") != null && (document.pgQuickLinks.stViewOnlyTag.src.indexOf("ViewOnly", 0) != -1))
		return;
	
	if (document.getElementById('urlAddress['+obj.value+']').value == "" &
			document.getElementById('descEnglish['+obj.value +']').value == "" &
			document.getElementById('descFrench['+obj.value +']').value == "" )
	{
		return;
	}
	var tempAddress = document.getElementById('urlAddress['+NdxIndex+']').value;
	var tempEnglish = document.getElementById('descEnglish['+NdxIndex+']').value;
	var tempFrench = document.getElementById('descFrench['+NdxIndex+']').value;
	
	document.getElementById('urlAddress['+NdxIndex +']').value = document.getElementById('urlAddress['+obj.value +']').value;
	document.getElementById('descEnglish['+NdxIndex +']').value = document.getElementById('descEnglish['+obj.value +']').value;
	document.getElementById('descFrench['+NdxIndex +']').value = document.getElementById('descFrench['+obj.value +']').value;
	
	document.getElementById('urlAddress['+currIndex+']').value = tempAddress;
	document.getElementById('descEnglish['+currIndex +']').value = tempEnglish;
	document.getElementById('descFrench['+currIndex +']').value = tempFrench; 
	
	// Change the add / edit button to based on URL
	if(document.getElementById('urlAddress['+currIndex +']').value.length == 0 || 
			document.getElementById('descEnglish['+currIndex +']').value.length == 0 || 
			document.getElementById('descFrench['+currIndex +']').value.length == 0 ) 
		if(LANGUAGECODE == "0") 
			document.pgQuickLinks.btAdd[currIndex].src="../images/add_sm.gif";
		else
			document.pgQuickLinks.btAdd[currIndex].src="../images/add_sm_fr.gif";
	
	else 
		if(LANGUAGECODE == "0") 
			document.pgQuickLinks.btAdd[currIndex].src="../images/edit_sm.gif";
		else 
			document.pgQuickLinks.btAdd[currIndex].src="../images/edit_sm_fr.gif";
		
	disableTextFields(currIndex);
	
	if(LANGUAGECODE == "0") {
		document.pgQuickLinks.btTestLink[currIndex].src="../images/testlink_sm_disabled.gif";
		document.pgQuickLinks.btClear[currIndex].src="../images/clear_sm_disabled.gif";
	} else {
		document.pgQuickLinks.btTestLink[currIndex].src="../images/testlink_sm_fr_disabled.gif";
		document.pgQuickLinks.btClear[currIndex].src="../images/clear_sm_fr_disabled.gif";
	} 
	
	if(document.getElementById('urlAddress['+NdxIndex +']').value.length == 0 || 
			document.getElementById('descEnglish['+NdxIndex +']').value.length == 0 || 
			document.getElementById('descFrench['+NdxIndex +']').value.length == 0 ) 
		if(LANGUAGECODE == "0")
			document.pgQuickLinks.btAdd[NdxIndex].src="../images/add_sm.gif";
		else 
			document.pgQuickLinks.btAdd[NdxIndex].src="../images/add_sm_fr.gif";
	else 
		if(LANGUAGECODE == "0")
			document.pgQuickLinks.btAdd[NdxIndex].src="../images/edit_sm.gif";
		else 
			document.pgQuickLinks.btAdd[NdxIndex].src="../images/edit_sm_fr.gif";
}

function clearFields(obj)
{
	
	var inputRowNdx = obj.value;
	
	if(document.pgQuickLinks.btClear[inputRowNdx].src.indexOf("disabled.gif", 0) == -1)
	{
		clearTextFields(inputRowNdx);
		
		disableTextFields(inputRowNdx);
		if(LANGUAGECODE == "0") {
			document.pgQuickLinks.btAdd[inputRowNdx].src="../images/add_sm.gif";
			document.pgQuickLinks.btTestLink[inputRowNdx].src="../images/testlink_sm_disabled.gif";
			document.pgQuickLinks.btClear[inputRowNdx].src="../images/clear_sm_disabled.gif";
		} else {
			document.pgQuickLinks.btAdd[inputRowNdx].src="../images/add_sm_fr.gif";
			document.pgQuickLinks.btTestLink[inputRowNdx].src="../images/testlink_sm_fr_disabled.gif";
			document.pgQuickLinks.btClear[inputRowNdx].src="../images/clear_sm_fr_disabled.gif";
		}
	}
}

function addEditQuickLinks(obj)
{
	var inputRowNdx = obj.value;
	if(document.pgQuickLinks.btAdd[inputRowNdx].src.indexOf("disabled.gif", 0) == -1)
	{
		enableTextFields(inputRowNdx);
		
		// enable clear and Test link buttons
		if(LANGUAGECODE == "0") {
			document.pgQuickLinks.btTestLink[inputRowNdx].src="../images/testlink_sm.gif";
			document.pgQuickLinks.btClear[inputRowNdx].src="../images/clear_sm.gif";
		} else {
			document.pgQuickLinks.btTestLink[inputRowNdx].src="../images/testlink_sm_fr.gif";
			document.pgQuickLinks.btClear[inputRowNdx].src="../images/clear_sm_fr.gif";
		}
	
		// Change the add / edit button to disable state - english
		if(LANGUAGECODE == "0")
			if(document.pgQuickLinks.btAdd[inputRowNdx].src.indexOf("add_sm.gif", 0) != -1)
				document.pgQuickLinks.btAdd[inputRowNdx].src="../images/add_sm_disabled.gif";
			else
				document.pgQuickLinks.btAdd[inputRowNdx].src="../images/edit_sm_disabled.gif";
		else
		// French language
		if(document.pgQuickLinks.btAdd[inputRowNdx].src.indexOf("add_sm_fr.gif", 0) != -1)
			document.pgQuickLinks.btAdd[inputRowNdx].src="../images/add_sm_fr_disabled.gif";
		else
			document.pgQuickLinks.btAdd[inputRowNdx].src="../images/edit_sm_fr_disabled.gif";
	}
}

function testURLAddress(obj)
{
	// Open the the browser with URL
	var inputRowNdx = obj.value;
	if(document.pgQuickLinks.btTestLink[inputRowNdx].src.indexOf("disabled.gif", 0) == -1)
	{
		var theControl =  document.getElementById('urlAddress['+inputRowNdx +']');
		var url = theControl.value;
		if(url.length != 0)
		{
			if((url.indexOf('http://') == -1) && (url.indexOf('https://') == -1))
				url = 'http://' + url;
			if(LANGUAGECODE == "0")  {
				document.pgQuickLinks.btAdd[inputRowNdx].src="../images/edit_sm.gif";
				document.pgQuickLinks.btClear[inputRowNdx].src="../images/clear_sm.gif";
			} else {
				document.pgQuickLinks.btAdd[inputRowNdx].src="../images/edit_sm_fr.gif";
				document.pgQuickLinks.btClear[inputRowNdx].src="../images/clear_sm_fr.gif";
			}
				
			
			var strVal = 'pgDisclaimer'+ '?urlLink=' + url;
			window.open(strVal,'','resizable,scrollbars,status=yes,titlebar=yes,width=800,height=600,hotkeys');  
		}
	} 
}

function initPage()
{
	var inputRowNdx = 0;
	
	document.all.btUp[0].style.visibility='hidden';
	document.all.btDown[9].style.visibility='hidden';
	
	for( inputRowNdx=0; inputRowNdx < 10; inputRowNdx++ )
	{
		var theControl = document.getElementById('urlAddress['+inputRowNdx +']');
		
		disableTextFields(inputRowNdx);
		
		if(LANGUAGECODE == "0")  {
			document.getElementsByName("btTestLink").item(inputRowNdx).src="../images/testlink_sm_disabled.gif";
			document.getElementsByName("btClear").item(inputRowNdx).src="../images/clear_sm_disabled.gif";
		} else {
			document.getElementsByName("btTestLink").item(inputRowNdx).src="../images/testlink_sm_fr_disabled.gif";
			document.getElementsByName("btClear").item(inputRowNdx).src="../images/clear_sm_fr_disabled.gif";
		}
		
		if(theControl.value.length == 0)
			if(LANGUAGECODE == "0") 
				if(document.getElementById("stViewOnlyTag") != null && (document.pgQuickLinks.stViewOnlyTag.src.indexOf("ViewOnly", 0) != -1) )
					document.getElementsByName("btAdd").item(inputRowNdx).src="../images/add_sm_disabled.gif";
				else
					document.getElementsByName("btAdd").item(inputRowNdx).src="../images/add_sm.gif";
			else 
				if(document.getElementById("stViewOnlyTag") != null && (document.pgQuickLinks.stViewOnlyTag.src.indexOf("ViewOnly", 0) != -1))
					document.getElementsByName("btAdd").item(inputRowNdx).src="../images/add_sm_fr_disabled.gif";
				else
					document.getElementsByName("btAdd").item(inputRowNdx).src="../images/add_sm_fr.gif";
		else
			if(LANGUAGECODE == "0") 
				if(document.getElementById("stViewOnlyTag") != null && (document.pgQuickLinks.stViewOnlyTag.src.indexOf("ViewOnly", 0) != -1))
					document.getElementsByName("btAdd").item(inputRowNdx).src="../images/edit_sm_disabled.gif";
				else 
					document.getElementsByName("btAdd").item(inputRowNdx).src="../images/edit_sm.gif";
			else
				if( document.getElementById("stViewOnlyTag") != null && (document.pgQuickLinks.stViewOnlyTag.src.indexOf("ViewOnly", 0) != -1))
					document.getElementsByName("btAdd").item(inputRowNdx).src="../images/edit_sm_fr_disabled.gif";
				else
					document.getElementsByName("btAdd").item(inputRowNdx).src="../images/edit_sm_fr.gif";
	}
}

	
/*
 *  pgQuick Links Java script code - end
 */