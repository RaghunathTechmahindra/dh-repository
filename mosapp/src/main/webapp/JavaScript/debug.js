// ****************************** debug.js
var debug = new debug();

function debug() {
    this.html = ""; // buffer
    this.hWin = null; // window handle
    this.bDebug = true;

    this.color = "";

    this.setDebug = function(flag) {
        this.bDebug = flag;
    }

    this.clear = function() {
        this.html = "";
        this.flush();
    }

    this.flush = function() {
        if (false == this.bDebug)
            return;
        if (null == this.hWin || this.hWin.closed) {
            this.hWin = window
                    .open("", "debug",
                            "height=200,width=400,menubar=yes,scrollbars=yes,resizable=yes");
        }
        this.hWin.document.open("text/html", "replace");
        this.hWin.document.write(this.html);
        this.hWin.document.close();
        this.hWin.focus();
    }

    this.print = function(html) {
        this.color = (this.color == "#FAF5D4") ? "#EBCC67" : "#FAF5D4";
        this.html += ("<div style='background-color:" + this.color + "'>"
                + this.htmlchar(html) + "</div>\n");
    }

    this.inspect = function(obj) {

        if (typeof obj == "number") {
            return "" + obj;

        } else if (typeof obj == "string") {
            return "\"" + obj + "\"";

        } else if (typeof obj == "function") {
            return "" + obj;

        } else if (typeof obj == "object") {
            var str = this.to_s(obj, "");
            return "{" + str + "}";
        } else {
            return "<" + (typeof obj) + ":" + obj + ">";
        }
    }

    this.to_s = function(obj, indent) {
        var delimiter = ", \n";

        var str = "";
        for (key in obj) {
            if (str != "")
                str += delimiter;
            str += indent;

            var value = obj[key];
            if (!value) {
                str += "" + key + "=>undefined";
                continue;
            }

            if (typeof value == "number") {
                str += "" + key + "=>" + value + "";

            } else if (typeof value == "string") {
                str += "" + key + '=>"' + value + '"';

            } else if (typeof value == "function") {
                str += "" + key + "()";

            } else if (typeof value == "object") {
                value = "\n" + this.to_s(value, indent + "    ");
                str += "" + key + "=>" + value + "";

            } else {
                str += "" + key + "=><" + (typeof value) + ":" + value + ">";
            }
        }
        if (str == "") {
            str += "" + obj;
        }
        return str;
    }

    this.htmlchar = function(str) {
        str = str.replace(/&/g, "&amp;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
        str = str.replace(/\"/g, "&quot;");
        str = str.replace(/\n/g, "<br>\n");
        return str;
    }

    this.p = function(elem) {
        this.print(this.inspect(elem));
        this.flush();
    }
}