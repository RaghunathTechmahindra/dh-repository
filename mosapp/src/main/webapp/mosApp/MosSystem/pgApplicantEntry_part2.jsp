<HTML>
<%@page info="pgApplicantEntry" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgApplicantEntryViewBean">

<!--START CREDIT REFERENCE//-->
<jato:text name="stTargetCredit" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Credit Reference</b></font></td>
</tr>

<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedCreditReference" type="mosApp.MosSystem.pgApplicantEntryRepeatedCreditReferenceTiledView">
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdCreditRefId" /><jato:hidden name="hdCreditRefCopyId" /></td>
<td valign=top><font size=2 color=3366cc><b>Credit Reference Type:</b></font><br>
  <jato:combobox name="cbCreditRefsType" /></td>
<td valign=top colspan=3><font size=2 color=3366cc><b>Reference Description:</b></font><br>
  <jato:textField name="txReferenceDesc" size="80" maxLength="80" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Institution Name:</b></font><br>
<jato:textField name="txInstitutionName" size="20" maxLength="35" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Time with Reference:</b></font><br>
<font size=2>Yrs: </font><jato:textField name="txTimeWithRefYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Mths: </font><jato:textField name="txTimeWithRefMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>Account Number:</b></font><br>
<jato:textField name="txAccountName" size="20" maxLength="35" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Current Balance:</b></font><br>
$  <jato:textField name="txCurrentBalance" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=bottom align=right colspan=5>&nbsp;<br>
  <jato:button name="btDeleteCreditRef" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_creditref.gif" />&nbsp;&nbsp;&nbsp;</td>
</tr>


<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
</jato:tiledView>


<!--END REPEATING ROW//-->

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr><td colspan=5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAditionalCreditRefs" extraHtml="width=176 height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_creditref.gif" /></td></tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END CREDIT REFERENCE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START ASSETS//-->
<jato:text name="stTargetAsset" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=7 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Assets</b></font></td>
</tr>

<tr><td colspan=7>&nbsp;</td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Asset Type</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Asset Description</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Asset Value</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Include in Net Worth?</b></font></td>
<td valign=top><font size=2 color=3366cc><b>% included in Net Worth</td>
<td>&nbsp;</td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>


<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedAssets" type="mosApp.MosSystem.pgApplicantEntryRepeatedAssetsTiledView">
<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdAssetId" /><jato:hidden name="hdAssetCopyId" /></td>
<td valign=top><jato:combobox name="cbAssetType" /></td>
<td valign=top><jato:textField name="txAssetDesc" size="35" maxLength="80" /></td>
<td valign=top>$ 
<jato:textField name="txAssetValue" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><jato:combobox name="cbAssetIncludeInNetworth" /></td>
<td valign=top>
<jato:textField name="txPercentageIncludedInNetworth" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /> %</td>
<td valign=top align=right><jato:button name="btDeleteAsset" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_asset.gif" />&nbsp;&nbsp;</td>
</tr>

<tr><td colspan=7><jato:hidden name="hdPercentageIncludedInNetworth" /><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=7>

<table border=0 cellspacing=3 width=490>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAddtionalAssets" extraHtml="width=117 height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_asset.gif" /></td>
<td valign=top><font size=2 color=3366cc><b>Total Applicant Assets:</b></font><br>
<jato:textField name="txTotalAsset" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>


</table>
<!--END ASSETS//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>


<!--START CREDIT BUREAU LIABILITIES//-->
<jato:text name="stTargetCreditBureauLiab" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=13 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<input type="hidden" id="hdCreditBureauTileSize" value='<jato:text name="stCreditBureauTileSize"/>'/>
<td colspan=1 valign=top height="21" nowrap><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
<jato:text name="stLiabiliesLabel" escape="false" /></b></font></td>
<td colspan=1 valign=top nowrap><b><font size="2" color="#3366CC">Bureau Summary:</font><font size=3 color=3366cc>&nbsp;&nbsp;<br>
  </font></b>
<jato:textArea name="txCreditBureauSummary" extraHtml="readonly" rows="4" cols="80" /></td>
<td colspan=11 valign=top align=right height="21"><a href='JavaScript: creditBureauClicked()'><img src="../images/less.gif" id='creditBureauImage' border="0"/></a></td>

</tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>

<tr><td colspan=7 bgcolor=d1ebff height="16"><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top ><font size=2 color=3366cc><b>CB</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Type</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Description</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Limit</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Amount</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Mthly Pay</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Export</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Delete</b></font></td>
</tr>
<tr><td colspan=10 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=10 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<jato:tiledView name="RepeatedCreditBureauLiabilities" type="mosApp.MosSystem.pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView">
<tr>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdCBLiabilityId" /><jato:hidden name="hdCBLiabilityCopyId" /><jato:hidden name="hdCBLiabilityPercentageIncludeInGDS" /><jato:hidden name="hdCBLiabilityPercentageIncludeInTDS" /></td>
<td valign=top >
<jato:checkbox name="chCreditBureauIndicator" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityType" size="20" maxLength="20" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityDesc" size="60" maxLength="60" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityLimit" formatType="decimal" formatMask="###0.00; (-#)" size="10" maxLength="14" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityAmount" formatType="decimal" formatMask="###0.00; (-#)" size="10" maxLength="14" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityMonthlyPayment" formatType="decimal" formatMask="###0.00; (-#)" size="8" maxLength="14" /></td>
<td valign=top align="center" ><jato:checkbox name="chCreditBureauLiabilityExport" extraHtml="onclick = \"exportChkBoxClicked();\"" /></td>
<td valign=top align="center" ><jato:checkbox name="chCreditBureauLiabilityDelete" extraHtml="onclick = 'exportChkBoxClicked();'" /></td>
</tr>


<tr id='creditBureanDateSection<jato:text name="stTileIndex"/>'>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=3 ><font size=2 color=3366cc><b>Maturity Date</b>&nbsp;&nbsp;&nbsp; Mth:&nbsp; <jato:combobox name="cbMonths" />: Day:<jato:textField name="tbDay" formatType="string" formatMask="??" size="2" maxLength="2" />Yr:<jato:textField name="tbYear" formatType="string" formatMask="????" size="4" maxLength="4" /></td>
</tr>

<tr><td colspan=10 height="1"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->



</jato:tiledView>

<tr><td colspan=7 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=13 height="48">

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top width="0%" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stBureauViewButton" escape="false" />&nbsp;</td>
<td valign=top width="40%" nowrap><font size=2 color=3366cc><b><jato:text name="stTotalLiabiliesLabel" escape="false" /></b></font><br>
<jato:textField name="txCreditBureauTotalLiab" fireDisplayEvents="true" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
<td valign=top width="40%" nowrap><font size=2 color=3366cc><b><jato:text name="stMonthPaymentLabel" escape="false" /></b></font><br>
<jato:textField name="txCreditBureauTotalMonthlyLiab" fireDisplayEvents="true" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" />&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b><jato:text name="stCreditScoreLabel" escape="false" /></b></font><br>
<jato:textField name="txCreditScore" formatType="decimal" formatMask="###0; (-#)" size="20" maxLength="20" />&nbsp;</td>
<td valign=top width="10%"align="right"><jato:button name="btExportDeleteLiability" extraHtml="onClick = 'exportBtnClicked(); setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ExportAbove.gif" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=11 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=11 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END CREDIT BUREAU LIABILITIES//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START LIABILITIES//-->
<jato:text name="stTargetLiab" fireDisplayEvents="true" escape="false" />

<!-- Start GDS/TDS Section //-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor="#d1ebff">
<td colspan=9><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=9><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff>&nbsp;<b>Combined GDS:</b></font><br>
&nbsp;<font size=2 color=ccffff><jato:text name="stCombinedGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>GDS (Qualifying Rate):</b></font><br>
<font size=2><jato:text name="stCombined3YrGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Combined GDS (Borrower Only):</b></font><br>
<font size=2><jato:text name="stCombinedBorrowerGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff>&nbsp;<b>Combined TDS:</b></font><br>
&nbsp;<font size=2 color=ccffff><jato:text name="stCombinedTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>TDS (Qualifying Rate):</b></font><br>
<font size=2><jato:text name="stCombined3YrTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Combined TDS (Borrower Only):</b></font><br>
<font size=2><jato:text name="stCombinedBorrowerTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td><tr>
</table>
<!-- End GDS/TDS Section //-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=13 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<input type="hidden" id="hdLiabilityTileSize" value='<jato:text name="stLiabilityTileSize"/>'/>
<td colspan=3 valign=top height="21"><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Liabilities</b></font></td>
<td colspan=11 align=right height="21"><a href='JavaScript: liabilityClicked()'><img src="../images/less.gif" id='liabilityImage' border="0"/></a></td>
</tr>

<tr><td colspan=11 bgcolor=d1ebff height="16"><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<tr>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top ><font size=2 color=3366cc><b>CB</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Type</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Description</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Limit</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Amount</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Mthly Pay</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Payoff</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>GDS?</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>%</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>TDS?</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>%</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Delete?</b></font></td>
</tr>
<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=13 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<jato:tiledView name="RepeatedLiabilities" type="mosApp.MosSystem.pgApplicantEntryRepeatedLiabilitiesTiledView">
<tr>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdLiabilityId" /><jato:hidden name="hdLiabilityCopyId" /><jato:hidden name="hdLiabilityPercentageIncludeInGDS" /><jato:hidden name="hdLiabilityPercentageIncludeInTDS" /></td>
<td valign=top >
<jato:checkbox name="chCreditBureauIndicator" extraHtml="disabled = true"/></td>
<td valign=top ><jato:combobox name="cbLiabilityType" /></td>
<td valign=top >
<jato:textField name="txLiabilityDesc" size="25" maxLength="80" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityLimit" formatType="decimal" formatMask="###0.00; (-#)" size="10" maxLength="14" /></td>
<td valign=top >
<!-- Express 3.2 Merge: DES340960 starts -->
<jato:textField name="txLiabilityAmount" formatType="currency" formatMask="###0.00" size="10" maxLength="14" /></td>
<td valign=top >
<jato:textField name="txLiabilityMonthlyPayment" formatType="currency" formatMask="###0.00" size="8" maxLength="14" /></td>
<!-- Express 3.2 Merge: DES340960 ends -->
<td valign=top ><jato:combobox name="cbLiabilityPayOffType" /></td>
<td valign=top ><jato:combobox name="cblLiabilityIncludeInGDS" /></td>
<td valign=top >
<jato:textField name="txLiabilityPercentageIncludeInGDS" formatType="decimal" formatMask="###0.00; (-#)" size="5" maxLength="6" /></td>
<td valign=top ><jato:combobox name="cblLiabilityIncludeInTDS" /></td>
<td valign=top >
<jato:textField name="txLiabilityPercentageIncludeInTDS" formatType="decimal" formatMask="###0.00; (-#)" size="5" maxLength="6" /></td>
<td valign=top align="center" ><jato:checkbox name="chbLiabilityDelete" extraHtml="onclick = 'deleteChkBoxClicked();'" /></td>
</tr>

<tr id='liabilityDateSection<jato:text name="stTileIndex"/>'>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=3 ><font size=2 color=3366cc><b>Maturity Date</b>&nbsp;&nbsp;&nbsp; Mth:&nbsp; <jato:combobox name="cbMonths" />: Day:<jato:textField name="tbDay" formatType="string" formatMask="??" size="2" maxLength="2" />Yr:<jato:textField name="tbYear" formatType="string" formatMask="????" size="4" maxLength="4" /></td>
</tr>
<tr><td colspan=13 height="1"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->

</jato:tiledView>

<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=13 height="48">

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top width="0%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAditionalLiabilities" extraHtml="width=132 height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_liability.gif" /></td>
<td valign=top width="0%" nowrap><font size=2 color=3366cc><b>Total Applicant Liabilities:</b></font><br>
<jato:textField name="txTotalLiab" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
<td valign=top width="0%" nowrap><font size=2 color=3366cc><b>Monthly Liabilities Payments:</b></font><br>
<jato:textField name="txTotalMonthlyLiab" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" />&nbsp;</td>
<td valign=top width="100%"align="right"><jato:button name="btRecalculate" extraHtml="width=75 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/recalculate.gif" />&nbsp;<jato:button name="btDeleteLiability" extraHtml="onClick = 'deleteBtnClicked(); setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_liability.gif" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=13 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END LIABILITIES//-->


</jato:useViewBean>
</HTML>
