
<HTML>
<%@page info="pgDealResolution" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealResolutionViewBean">

<HEAD>
<TITLE>Résolution de la demande</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff onload="checkMIResponseLoop();">
<jato:form name="pgDealResolution" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="false" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
	<!--HEADER//-->
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>

    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
          
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!--BODY OF PAGE//-->
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#D1EBFF>
<tr>  
  <td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
</tr>  

<tr>
  <td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>  

<!-- title line -->
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  	<td cellpadding=6 colspan=3 align="left" height="40" nowrap>
    	<font color=3366cc><b>Résoudre la demande:</b></font>
  	</td>
  	<td colspan=5 align="left" >
  		<jato:text name="stIncludeSrvcNumberStart" escape="false" />
  			<font size="2">
  			<jato:text name="stApprovalServicingNumLabel" escape="true" />
    		<jato:textField name="tbServMortgNum" size="20" maxLength="35" />
    		</font>
  		<jato:text name="stIncludeSrvcNumberEnd" escape="false" />
  	</td>
</tr>  

<!-- First row -->
<tr>
  <td width="0%">&nbsp;</td>
  <jato:text name="stIncludeApproveOptionStart" escape="false" />
  <td NOWRAP width="0%"><font size="2">&nbsp;&nbsp;&nbsp; 
  <jato:radioButtons name="rbApproveDeal" extraHtml="onClick='approveDealClicked()'" /></font></td>
  <td align="left" NOWRAP width="0%">&nbsp;</td>
  <td NOWRAP width="0%" align="left">&nbsp;</td>
  <td NOWRAP width="50%" colspan=3>
  	<font size="2">
  	<jato:text name="stApprovalTypeLabel" escape="true" />
  	<jato:combobox name="cbApprovalType" extraHtml="onChange='cbApprovalTypeChanged()'" />&nbsp;&nbsp;
  	</font>
  </td>  
  <jato:text name="stIncludeApproveOptionEnd" escape="false" />
</tr>

<tr>
  <td colspan=8>&nbsp;</td>
</tr>  


<!-- Second row -->
<tr>
  <td width="0%">&nbsp;</td>
  <td NOWRAP width="0%"><font size="2">&nbsp;&nbsp;&nbsp;<jato:radioButtons name="rbHoldPendingInfo" extraHtml="onClick='holdPendingInfoClicked()'" /></font></td>
  <td NOWRAP width="0%" align="left">&nbsp;&nbsp;<font size="2"><jato:radioButtons name="rbHoldFor" extraHtml="onClick='holdForClickedInit()'" /></font></td>
  <td NOWRAP width="0%" align="left">&nbsp;</td>
  <td NOWRAP width="0%" align="left">
  	<jato:textField name="tbHoldForDays" extraHtml="onChange='holdForDaysChanged()'" size="2" maxLength="2" />&nbsp;<font size="2">jours&nbsp;&nbsp;</font>
  </td>
  <td NOWRAP width="0%" align="left">
  	<jato:textField name="tbHoldForHrs" extraHtml="onChange='holdForDaysChanged()'" size="2" maxLength="2" 
  	onBlur="isFieldInIntRange(0, 23);"/>&nbsp;<font size="2">h.&nbsp;&nbsp;</font>
  </td>
  <td NOWRAP width="0%" align="left">
  	<jato:textField name="tbHoldForMins" extraHtml="onChange='holdForDaysChanged()'" size="2" maxLength="2" 
  	onBlur="isFieldInIntRange(0, 59);"/>&nbsp;<font size="2">min.&nbsp;&nbsp;</font>
  </td>
  <td NOWRAP width="50%" align="left">
  	<font size="2">&nbsp;&nbsp;&nbsp;&nbsp;Raison:</font><jato:combobox name="cbHoldReason" extraHtml="onChange='holdForDaysChanged()'" fireDisplayEvents="true" />
  </td> 
</tr>

<!-- Third row -->
<tr>
  <td width="0%">&nbsp;</td>
  <td NOWRAP width="0%">&nbsp;</td>
  <td NOWRAP width="0%" align="left">&nbsp;&nbsp;<font size="2"><jato:radioButtons name="rbHoldUntil" extraHtml="onClick='holdUntilClicked()'" /></font></td>
  <td NOWRAP width="0%" align="right">&nbsp;&nbsp;<font size="2">Mois:&nbsp;</font></td>
  <td NOWRAP width="0%" align="left">
  	<jato:combobox name="cbHoldUntilMonth" extraHtml="onChange='holdUntilMonthChanged()'" 
  	onBlur="isFieldValidDate('tbHoldUntilYear', 'cbHoldUntilMonth', 'tbHoldUntilDay');"/><font size="2">&nbsp;Jour:&nbsp;</font>
  </td>
  <td NOWRAP width="0%" align="left">
  	<jato:textField name="tbHoldUntilDay" extraHtml="onFocus='holdUntilDayFocused()'  onChange='holdUntilDayChanged()'" size="2" maxLength="2" 
  	onBlur="if(isFieldInIntRange(1, 31)) isFieldValidDate('tbHoldUntilYear', 'cbHoldUntilMonth', 'tbHoldUntilDay');"/><font size="2">&nbsp;An:&nbsp;</font>
  </td>
  <td NOWRAP width="0%" align="left" colspan=2>
  	<jato:textField name="tbHoldUntilYear" extraHtml="onFocus='holdUntilYearFocused()'  onChange='holdUntilYearChanged()'" size="4" maxLength="4" 
  	onBlur="if(isFieldInIntRange(1800, 2100)) isFieldValidDate('tbHoldUntilYear', 'cbHoldUntilMonth', 'tbHoldUntilDay');"/>
  </td>
</tr>

<tr>
  <td colspan=8>&nbsp;</td>
</tr>  

<!-- Fourth row -->
<tr>
  <td width="0%">&nbsp;</td>
  <td NOWRAP width="0%"><font size="2">&nbsp;&nbsp;&nbsp; 
  <jato:radioButtons name="rbDeny" extraHtml="onClick='denyClicked()'" /></font></td>
  <td align="RIGHT" NOWRAP width="0%">&nbsp;</td>
  <td NOWRAP width="0%" align="right">&nbsp;&nbsp; <font size="2">Raison:&nbsp;</font></td>
  <td NOWRAP width="50%" colspan=4><jato:combobox name="cbDenialOptions" extraHtml="onChange='cbDenialOptionsChanged()'" fireDisplayEvents="true" /></td>  
</tr>

<tr>
  <td colspan=8>&nbsp;</td>
</tr>  

<!-- Fifth row -->
<tr>
  <td width="0%">&nbsp;</td>
  <td NOWRAP width="0%"><font size="2">&nbsp;&nbsp;&nbsp; 
  <jato:radioButtons name="rbCollapseDeal" extraHtml="onClick='collapseDealClicked()'" /></font></td>
  <td NOWRAP width="0%">&nbsp;</td> 
  <td NOWRAP width="0%">&nbsp;</td> 
  <td width="50%" colspan=4>&nbsp;</td>
</tr>

<tr>
  <td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>  

<!-- title line for Broker Note -->
<tr>
	<td>&nbsp;</td>
  	<td cellpadding=6 colspan=7 align="left" height="40" valign="middle">
    <font color=3366cc><b>Note au courtier:</b></font>
  </td>
</tr> 
<tr>
  <td width="0%">&nbsp;</td>
  <td colspan=7 NOWRAP><font size="2">&nbsp;&nbsp;&nbsp; </font>
  <jato:textArea name="tbBrokerNoteText" elementId="tbBrokerNoteText" extraHtml="wrap=soft maxlength=249" rows="4" cols="100" /></td>
</tr> 
<tr>
  <td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr> 
<tr>  
  <td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
</tr>  

</table>
</center>

<!-- 
************************************************************************
  Bottom line with submit/cancel button
************************************************************************
--> 
<p>
<table border=0 width=100% bgcolor=ffffff>
  <tr><td align=right><jato:button name="btToUWSheet" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/worksheet_fr.gif" /> <jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;<jato:button name="btCancel" extraHtml="height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> <jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td></tr>
</table>
<p><p>
</div>

<!--
*************************************************************************
   End of the data entry table section
*************************************************************************
-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>

<script src="../JavaScript/screens/pgDealResolution.js" type="text/javascript"></script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
