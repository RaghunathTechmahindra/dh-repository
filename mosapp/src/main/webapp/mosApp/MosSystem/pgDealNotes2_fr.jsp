<%-- This page is added for FXP27053 Deal Notes Lazy Load --%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealNotes2ViewBean">
	<html>
	<head>
		<title>DealNote View</title>
		<style type="text/css">
			* {font-size:x-small;}
			body {background-color:#d1ebff;}
			caption {font-size:small; font-weight:bold; color:#3366cc;}
			tr.dealnoteheader td {white-space:nowrap; font-weight:bold; color:#3366cc; padding-right:6px; border-bottom:1px solid #006699;}
			tr.dealnotecontent td { white-space:nowrap; font-size:xx-small; font-family:"Lucida Console"; padding-right:6px; border-bottom:1px solid #006699;}
		</style>
	</head>
	<body>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<caption>Notes de la demande</caption>
			<col width="100%" />
			<col span="3" width="0%" />
			<tr>
				<td colspan="4">Les notes de la demande suivantes s'appliquent �(Demande no <jato:text name="stDealId" escape="true" formatType="decimal" formatMask="###0; (-#)" />)</td>
			</tr>
			<tr class="dealnoteheader">
				<td>Note:</td>
				<td>Date de la note:</td>
				<td>Nom de l'utilisateur:</td>
				<td>Cat�gorie de note:</td>
			</tr>
			<jato:tiledView name="dealNoteTile" type="mosApp.MosSystem.pgDealNotes2TiledView">
			<tr class="dealnotecontent">
				<td style="white-space:normal;"><jato:text name="stNoteText1" escape="false" /></td>
				<td><jato:text name="stNoteDate1" escape="true" formatType="date" formatMask="en|MMM dd yyyy" /></td>
				<td><jato:text name="stUserName1" escape="true" /></td>
				<td><jato:text name="stNoteCategory1" escape="true" /></td>
			</tr>
			</jato:tiledView>
			<tr>
				<td colspan="4" style="text-align:center; padding-top:20px;"><a href="javascript:self.close();"><img src="../images/close_fr.gif" border="0" /></a></td>
			</tr>
		</table>
	</body>
	</html>
</jato:useViewBean>