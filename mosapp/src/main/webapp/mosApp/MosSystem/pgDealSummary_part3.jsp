
<HTML>
<!-- Begin of DealSummary Part 3 include -->
<%@page info="pgDealSummary" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealSummaryViewBean">

<!-- Start - Liability Details -->
<div id="dealsumliab" class="dealsumliab" name="dealsumliab">
<center>
	<!-- Start - Borrower Summary Details-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>Applicant Reference:</b></font>
			</td>
		</tr>
	</table>

	<jato:tiledView name="RepeatedLiabDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedLiabDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top width=30%>
				<font size=2 color=3366cc><b>Applicant Name:</b></font><br>
				<font size=2>
					<jato:text name="stLDBorrSalutation" escape="true" /> <jato:text name="stLDBorrFirstName" escape="true" /> <jato:text name="stLDBorrMidInitial" escape="true" /> <jato:text name="stLDBorrLastName" escape="true" /> <jato:text name="stLDBorrSuffix" escape="true" />
   				</font>
			</td>
			<td valign=top width=30%>
				<font size=2 color=3366cc><b>Applicant Type:</b></font><br>
				<font size=2><jato:text name="stLDBorrType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
			<td valign=top width=40%>
				<font size=2 color=3366cc><b>Primary Borrower?</b></font><br>
				<font size=2><jato:text name="stLDBorrFlag" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>
		</tr>
	</table>
	<!-- End Borrower Summary Details-->
	
	<!-- Begin CreditBureau Liability Details-->
	
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<jato:text name="stBeginHideCBLiabSection" fireDisplayEvents="true" escape="true" />
		<tr><td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc colspan=8><b>Credit Bureau Liability Details:</b></font>
			</td>
		</tr>
			
		<tr>
        	<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>CB</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Liability Type</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Liability Description</b></font>
			</td>
            <td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Limit</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Amount</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Mthly Pay</b></font>
			</td>
            <td valign=top nowrap>
				<font size=2 color=3366cc><b>Maturity Date</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>GDS %</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>TDS %</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Payoff</b></font>
			</td>
		</tr>

	<jato:tiledView name="CreditBureauLiability" type="mosApp.MosSystem.pgDealSummaryCreditBureauLiabilityTiledView">
		<tr>
            <td valign=top nowrap><font size=2><jato:text name="stCreditBureauIndicator" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font></td>
			<td valign=top colspan=1 nowrap><font size=2><jato:text name="stCBType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font></td>
			<td valign=top colspan= nowrap><font size=2><jato:text name="stCBLiabilityDesc" escape="true" /></font></td>
            <td valign=top ><font size=2><jato:text name="stCreditBureauLiabilityLimit" escape="true" formatType="currency" formatMask="$#,##0.00; -#" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBTotalAmount" escape="true" formatType="currency" formatMask="$#,##0.00; -#" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBMonthlyPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
            <td valign=top nowrap><font size=2><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBLiabilityGDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBLiabilityTDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBLiabilityPayOffDesc" escape="true" /></font></td>
		</tr>
	</jato:tiledView>

		<tr><td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
	<jato:text name="stEndHideCBLiabSection" fireDisplayEvents="true" escape="true" />
	
<!-- End CreditBureau Liability Details-->

<!-- Begin Liability Details-->
	<jato:text name="stBeginHideLiabSection" fireDisplayEvents="true" escape="true" />
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc colspan=2><b>Liability Details:</b></font>
			</td>
		</tr>
	
		<tr>
            <td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>CB</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Liability Type</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Liability Description</b></font>
			</td>
            <td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Limit</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Amount</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Mthly Pay</b></font>
			</td>
            <td valign=top nowrap>
				<font size=2 color=3366cc><b>Maturity Date</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>GDS %</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>TDS %</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Payoff</b></font>
			</td>
		</tr>

	<jato:tiledView name="Liability" type="mosApp.MosSystem.pgDealSummaryLiabilityTiledView">
		<tr>
            <td valign=top nowrap><font size=2><jato:text name="stCreditBureauIndicator" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font></td>
			<td valign=top colspan=1 nowrap><font size=2><jato:text name="stLDType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font></td>
			<td valign=top colspan=1 nowrap><font size=2><jato:text name="stLDLiabilityDesc" escape="true" /></font></td>
            <td valign=top ><font size=2><jato:text name="stCreditBureauLiabilityLimit" escape="true" formatType="currency" formatMask="$#,##0.00; -#" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDTotalAmount" escape="true" formatType="currency" formatMask="$#,##0.00; -#" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDMonthlyPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
            <td valign=top nowrap><font size=2><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDLiabilityGDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDLiabilityTDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDLiabilityPayOffDesc" escape="true" /></font></td>
		</tr>
	</jato:tiledView>

	<jato:text name="stEndHideLiabSection" fireDisplayEvents="true" escape="true" />
	
	<tr><td colspan=10><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
	
	</table>
<!-- End Liability Details-->

</jato:tiledView>

	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<td align=right><a onclick="return IsSubmited();" href="javascript:viewSub('main')">
			<img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a></td>
	</table>
</center>
</div>


<!--ADDITONAL ASSET INFORMATION---------------------------//-->
<div id="dealsumasst" class="dealsumasst" name="dealsumasst">
<center>
	<!-- Start - Borrower Summary Details-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>Asset Details:</b></font>
			</td>
		</tr>
	</table>

	
	<jato:tiledView name="RepeatedAssetDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedAssetDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Name:</b></font><br>
				<font size=2><jato:text name="stADBborrSalutation" escape="true" /> <jato:text name="stADBFName" escape="false" /> <jato:text name="stADBMInitial" escape="true" />  <jato:text name="stADBLName" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Type:</b></font><br>
				<font size=2><jato:text name="stADBType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Primary Borrower?</b></font><br>
				<font size=2><jato:text name="stADBPBFlag" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Net Worth</b></font><br>
				<font size=2><jato:text name="stADBApplicationNetworthASDtls" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
		</tr>
<!--		<tr>
			<td align=center colspan=8>
				<img src="../images/blue_line.gif" width=100% height=2 alt="" border="0">
			</td>
		</tr>
-->		
	</table>
	<!-- End Borrower Summary Details-->
<jato:text name="stBeginHideAssetSection" fireDisplayEvents="true" escape="true" />

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top colspan=2><font size=3 color=3366cc colspan=2><b>Assets:</b></font></td>
		</tr>
		<tr>
			<td valign=top ><font size=2 color=3366cc><b>Asset Type</b></font></td>
			<td valign=top ><font size=2 color=3366cc><b>Asset Description</b></font></td>
			<td valign=top ><font size=2 color=3366cc><b>Asset Value</b></font></td>
			<td valign=top ><font size=2 color=3366cc><b>% Included in Net Worth</b></font></td>
		</tr>
	
	<jato:tiledView name="RepeatedAssets" type="mosApp.MosSystem.pgDealSummaryRepeatedAssetsTiledView">
		<tr>
			<td valign=top ><font size=2><jato:text name="stADBAssetType" escape="true" formatType="string" formatMask="????????????????????" /></font></td>
			<td valign=top ><font size=2><jato:text name="stADBDescription" escape="true" /></font></td>
			<td valign=top ><font size=2><jato:text name="stADBValue" escape="true" formatType="currency" formatMask="$#,##0.00; (#)" /></font></td>
			<td valign=top ><font size=2><jato:text name="stADBPercentageIncInNetworthASDtls" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
		</tr>
	</jato:tiledView>
	</table>

<jato:text name="stEndHideAssetSection" fireDisplayEvents="true" escape="true" />
	<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
	<jato:text name="stBeginHideCredRefSection" fireDisplayEvents="true" escape="true" />
		<tr><td align=center colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8>
				<font size=2 color=3366cc><b>Credit Reference</b></font>
			</td>
		</tr>
	</table>

	<jato:tiledView name="RepeatedCreditRef" type="mosApp.MosSystem.pgDealSummaryRepeatedCreditRefTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Credit Reference Type</b></font><br>
				<font size=2><jato:text name="stADCreditReferenceType" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Reference Description</b></font><br>
				<font size=2><jato:text name="stADReferenceDesc" escape="true" /></font>
			</td>
		</tr>
		<tr>	
			<td valign=top><font size=2 color=3366cc><b>Insitution Name</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>Time with Reference</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>Account Number</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>Current Balance</b></font></td>
		</tr>
		<tr>
			<td valign=top ><font size=2><jato:text name="stADInsitutionName" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font></td>
			<td valign=top><font size=2><jato:text name="stADTimeWithReference" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???????????????" /></font></td>
			<td valign=top><font size=2><jato:text name="stADAccountNumber" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font></td>
			<td valign=top><font size=2><jato:text name="stADCurrentBalance" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td><tr>
		</tr>
		<tr>
	</jato:tiledView>
	
	<jato:text name="stEndHideCredRefSection" fireDisplayEvents="true" escape="true" />
	<tr><td align=center colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
	</table>
	

	</jato:tiledView>
	
	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<tr>
			<td align=right><a onclick="return IsSubmited();" href="javascript:viewSub('main')">
				<img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a>
			</td>
		</tr>
	</table>
</center>	
</div>


<!--ADDITONAL PROPERTY INFORMATION---------------------------//-->
<div id="dealsumprop" class="dealsumprop" name="dealsumprop">
<center>
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=6>
				<font color=3366cc><b>Property Details:</b></font>
			</td>
		</tr>
	</table>
	
	<jato:tiledView name="RepeatedPropertyDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedPropertyDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>	
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Primary Property?</b></font><br>
				<font size=2><jato:text name="stPDprimaryProperty" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Property Address</b></font><br>
				<font size=2><jato:text name="stPDstreetNumber" escape="true" /> <jato:text name="stPDStreetName" escape="true" /> <jato:text name="stPDStreetType" escape="true" /> <jato:text name="stPDStreetDirection" escape="true" /> <jato:text name="stPDAddressLine2" escape="true" /><jato:text name="stPDUnitNumber" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>City</b></font><br>
				<font size=2><jato:text name="stPDcity" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Province</b></font><br>
				<font size=2><jato:text name="stPDprovince" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Postal Code</b></font><br>
				<font size=2><jato:text name="stPDpostCode1" escape="true" /> <jato:text name="stPDpostCode2" escape="true" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top colspan=4>
				<font size=2 color=3366cc><b>Legal Description:</b></font><br>
				<font size=2><jato:text name="stPDlegalLine1" escape="true" /><br><jato:text name="stPDlegalLine2" escape="true" /> <br><jato:text name="stPDlegalLine3" escape="true" />
				</font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Dwelling Type:</b></font><br>
				<font size=2><jato:text name="stPDwellingType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Dwelling Style:</b></font><br>
				<font size=2><jato:text name="stPDwellingStyle" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top colspan=4>
				<font size=2 color=3366cc><b>Builder Name:</b></font><br>
				<font size=2><jato:text name="stPDBuilderName" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Garage Size:</b></font><br>
				<font size=2><jato:text name="stPDGarageSize" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Garage Type:</b></font><br>
				<font size=2><jato:text name="stPDGarageType" escape="true" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>New Construction</b></font><br>
				<font size=2><jato:text name="stPDNewConstructor" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Structure Age:</b></font><br>
				<font size=2><jato:text name="stPDStructureAge" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b># of Units:</b></font><br>
				<font size=2><jato:text name="stPDNoOfUnits" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b># of Bedrooms:</b></font><br>
				<font size=2><jato:text name="stPDNumberOfBedrooms" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Living Space:</b></font><br>
				<font size=2><jato:text name="stPDLivingSpace" escape="true" formatType="decimal" formatMask="###0; (-#)" /> <jato:text name="stPDLivingSpaceUnitOfMeasure" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Lot Size:</b></font><br>
				<font size=2><jato:text name="stPDlotSize" escape="true" formatType="decimal" formatMask="###0; (-#)" /> <jato:text name="stPDLotSizeUnitOfMeasure" fireDisplayEvents="true" escape="true" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Heat Type:</b></font><br>
				<font size=2><jato:text name="stPDHeatType" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Environmental Hazard:</b></font><br>
				<font size=2><jato:text name="stPDInsulatedWithUFFI" fireDisplayEvents="true" escape="true" formatType="string" formatMask="??????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Water:</b></font><br>
				<font size=2><jato:text name="stPDWater" escape="true" formatType="string" formatMask="??????????" /></font>
			</td>
			<td valign=top >
				<font size=2 color=3366cc><b>Sewage:</b></font><br>
				<font size=2><jato:text name="stPDSewage" escape="true" formatType="string" formatMask="??????????" /></font>
			</td>
			<td valign=top >
				<font size=2 color=3366cc><b>MLS:</b></font><br>
				<font size=2><jato:text name="stPDMLSFlag" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>

		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Property Usage:</b></font><br>
				<font size=2><jato:text name="stPDPropertyUsage" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Occupancy:</b></font><br>
				<font size=2><jato:text name="stPDOccupancyType" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Property Type:</b></font><br>
				<font size=2><jato:text name="stPDPropertyType" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Property Location:</b></font><br>
				<font size=2><jato:text name="stPDropertyLocation" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Zoning:</b></font><br>
				<font size=2><jato:text name="stPDZoning" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
		</tr>		
		
		<%-- Release3.1 Apr 12, 2006 begins --%>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Tenure:</b></font><br>
				<font size=2><jato:text name="stTenure" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Energy Efficiency:</b></font><br>
				<font size=2><jato:text name="stMiEnergyEfficiency" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Subdivision Discount:</b></font><br>
				<font size=2><jato:text name="stSubdivisionDiscount" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>On Reserve Trust Agreement#:</b></font><br>
				<font size=2><jato:text name="stOnReserveTrustAgreement" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
		</tr>
		<%-- Release3.1 Apr 12, 2006 ends --%>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Purchase Price:</b></font><br>
				<font size=2><jato:text name="stPDPurchasePrice" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Land Value:</b></font><br>
				<font size=2><jato:text name="stPDLandValue" escape="true" formatType="currency" formatMask="$#,##0.00; (#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Equity Available:</b></font><br>
				<font size=2><jato:text name="stPDEquityAvailable" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Estimated Value:</b></font><br>
				<font size=2><jato:text name="stPDEstimatedAppraisalValue" escape="true" formatType="currency" formatMask="$#,##0.00; (#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Actual Appraised Value:</b></font><br>
				<font size=2><jato:text name="stPDActualAppraisalValue" escape="true" formatType="currency" formatMask="$#,##0.00; (#)" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Appraisal Date:</b></font><br>
				<font size=2><jato:text name="stPDAppraisalDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
			</td>
			<td valign=top colspan=5>
				<font size=2 color=3366cc><b>Appraisal Source:</b></font><br>
				<font size=2><jato:text name="stPDAppraisalSource" escape="true" formatType="string" formatMask="??????????????????????????????????" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Appraiser:</b></font><br>
				<font size=2><jato:text name="stPDAppraiserFistName" escape="true" formatType="string" formatMask="??????????????????????????????????" /> <jato:text name="stPDAppraiserInitial" escape="true" /> <jato:text name="stPDAppraiserLastName" escape="true" /></font>
			</td>
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Appraiser Address:</b></font><br>
				<font size=2><jato:text name="stPDAppraiserAddress" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Appraiser Phone #:</b></font><br>
				<font size=2><jato:text name="stPDAppraisalPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stPDAppPhoneExt" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Appraiser Fax #:</b></font><br>
				<font size=2><jato:text name="stPDAppraiserFax" escape="true" formatType="string" formatMask="(???)???-????" /></font>
			</td>
		</tr>
		<tr>
			<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td>
		</tr>
	</table>

</jato:tiledView>


	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<td align=right><a onclick="return IsSubmited();" href="javascript:viewSub('main')">
			<img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a>
		</td>
	</table>
</center>	
</div>

<!-- End Of Property Details -->

<!-- Begin Of Property Expenses Details-->
<div id="dealsumpropexp" class="dealsumpropexp" name="dealsumpropexp">
<center>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=3>
				<font color=3366cc><b>Expense Details:</b></font>
			</td>
		</tr>
	</table>
	
	<jato:tiledView name="RepeatedPropertyAddressAndExpenses" type="mosApp.MosSystem.pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>	
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Primary Property?</b></font><br>
				<font size=2><jato:text name="stEXPrimaryProperty" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>
		</tr>
	</table>
	
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Property Address</b></font><br>
				<font size=2><jato:text name="stEXStreetNumber" escape="true" /> <jato:text name="stEXStreetName" escape="true" /> <jato:text name="stExStreetType" escape="true" /> <jato:text name="stExStreetDirection" escape="true" /> <jato:text name="stExAddressLine2" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>City</b></font><br>
				<font size=2><jato:text name="stEXCity" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Province</b></font><br>
				<font size=2><jato:text name="stEXProvince" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Postal Code</b></font><br>
				<font size=2><jato:text name="stEXPosCode1" escape="true" /> <jato:text name="stEXPosCode2" escape="true" /></font>
			</td>
		</tr>
	</table>
<!-- End Property Summary Details-->
	
<!-- Begin Of Property Expenses Details from Part 2-->
<jato:text name="stBeginHidePropExpensesSection" fireDisplayEvents="true" escape="true" />
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Expense Type</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;Expense Description</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;Expense Period</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;Expense Amount</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp; % Included in GDS</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;% Included in TDS</b></font>
			</td>
		</tr>

	<jato:tiledView name="RepeatedPropertyExpenses" type="mosApp.MosSystem.pgDealSummaryRepeatedPropertyExpensesTiledView">
		<tr>
			<td valign=top ><font size=2><jato:text name="stExpenseType" escape="true" formatType="string" formatMask="????????????????????" /></font></td>
			<td valign=top ><font size=2><jato:text name="stExpenseDescription" escape="true" /></font></td>
			<td valign=top ><font size=2><jato:text name="stExpensePeriod" escape="true" formatType="string" formatMask="????????????????????" /></font></td>
			<td valign=top ><font size=2><jato:text name="stExpenseAmount" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
			<td valign=top ><font size=2><jato:text name="stIncludeGDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
			<td valign=top ><font size=2><jato:text name="stIncludeTDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
		</tr>
	</jato:tiledView>
	</table>
<jato:text name="stEndHidePropExpensesSection" fireDisplayEvents="true" escape="true" />
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=2 alt="" border="0">
			</td>
		</tr>
	</table>

</jato:tiledView>

	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<td align=right><a onclick="return IsSubmited();" href="javascript:viewSub('main')">
			<img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a></td>
	</table>
</center>
</div>
<!-- End Of Property Expenses -->
</jato:useViewBean>
<!-- End of DealSummary Part 3 include -->
</HTML>