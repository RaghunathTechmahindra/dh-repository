<%--
 /**Title: pgUWorksheet.jsp
 *Description:UnderwriterWorksheet Screen
 *@version 1.0 Initial Version
 *@version 1.1 06-JUN-2008 XS_2.45 Included pgQualifyingDetailsPagelet 
 *@version 1.2 31-July-2008 MCM Team: added changeMIUpfront in setMIUpFront
 *@version 1.3 Aug 21, 2008 : artf763316. replacing XS_2.60.
 * FXP33488 April 25, 2012
 */
--%>
<HTML>

<%@page info="pgUWorksheet" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgUWorksheetViewBean">

<!--
05/Sep/2006 DVG #DG494 #4533
16/Jun/2006 DVG #DG440 #3483
-->

<HEAD>
<TITLE>Underwriter Worksheet</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
.createnewscen1 {position: absolute; left: 0; top: expression((QLE)?312:287); width:400px; visibility:hidden;}
.createnewscen2 {position: relative; left: 0; top: 0; width:400px; visibility:hidden; display:none;}
.refisection {position: relative; left: 0; top: 0; display:none;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>
<script src="../JavaScript/componentInfoCheckBoxis.js" type="text/javascript"></script>
<script src="../JavaScript/rc/MITypeFilter.js" type="text/javascript"></script>
<script src="../JavaScript/UWorkSheetJS.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--
//====================== Local JavaScript functions !!!(needed to be adjusted for Jato FieldFormat later) =================
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';
var pgName = "<%=viewBean.PAGE_NAME%>";
var fetchedRate = 0;

function tool_click(n)
{
    var itemtomove = (NTCP) ? document.pagebody : document.all.pagebody.style;
    var alertchange = (NTCP) ? document.alertbody:document.all.alertbody.style;
    var createnew1 = (NTCP)?document.createnewscen1:document.all.createnewscen1.style;
    var createnew2 = (NTCP)?document.createnewscen2:document.all.createnewscen2.style;

    if(n==1)
    {
        var imgchange = "tool_goto"; var hide1img="tool_prev"; var hide2img="tool_tools";
        var itemtochange = (NTCP) ? document.gotobar : document.all.gotobar.style;
        var hideother1 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
        var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
    }

    if(n==2)
    {
        var imgchange = "tool_prev"; var hide1img="tool_goto"; var hide2img="tool_tools";
        var itemtochange = (NTCP) ? document.dialogbar : document.all.dialogbar.style;
        var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
        var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
    }

    if(n==3)
    {
        var imgchange = "tool_tools"; var hide1img="tool_goto"; var hide2img="tool_prev";
        var itemtochange = (NTCP) ? document.toolpop : document.all.toolpop.style;
        var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
        var hideother2 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
    }

    if(n==1 || n==2 || n==3)
    {
        if (itemtochange.visibility==VISIBLE)
        {
            itemtochange.visibility=HIDDEN;
            itemtomove.top = (NTCP) ? 100:((QLE)?135:110);
            changeImg(imgchange,'off');
            if(alertchange.visibility==VISIBLE){alertchange.top = (NTCP) ? 100:((QLE)?135:110);}
            if(createnewscen1.visibility==VISIBLE){createnewscen1.visibility=HIDDEN;}
            if(createnewscen2.visibility==VISIBLE){createnewscen2.visibility=HIDDEN;}
        } else
        {
            itemtochange.visibility=VISIBLE;
            changeImg(imgchange,'on');
            if(n!=2){itemtomove.top=150;}
            else{itemtomove.top=290;}
            if(alertchange.visibility==VISIBLE)
            {
                if(n!=2){alertchange.top =  150;}
                else{alertchange.top = 290;}
            }
            if(hideother1.visibility==VISIBLE){hideother1.visibility=HIDDEN; changeImg(hide1img,'off');}
            if(hideother2.visibility==VISIBLE){hideother2.visibility=HIDDEN; changeImg(hide2img,'off');}
            if(createnew1.visibility==VISIBLE){createnew1.visibility=HIDDEN;}
            if(createnew2.visibility==VISIBLE){createnew2.visibility=HIDDEN;}
        }
    }

    if(n==4)
    {
        var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
        itemtoshow.visibility=VISIBLE;
    }

    if(n==5)
    {
        var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
        var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
        itemtoshow.visibility=VISIBLE;
        if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN;}
       // FXP33488 afterr clicking OK we are not in AML mode, and do not need to hide the pencil -->
        amGenerate = 'N';
        showRatePencil('N');
    }
}

if(document.images){
    tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on.gif";
    tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto.gif";
    tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on.gif";
    tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev.gif";
    tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on.gif";
    tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools.gif";
}

function changeImg(imgNam,onoff)
{
    if(document.images)
    {
        document.images[imgNam].src = eval (imgNam+onoff+'.src');
    }
}

function newScen(whichBut)
{
  // check any lockable service is in progress or not.
  var varReqInProgress = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_hdReqInProgress");
    if (varReqInProgress.value == 'Y') {
      // there is a lockable service is in process, show up error message and return.
        alert(LOCKABLE_SERVICE_IN_PROGRESS);
      return;
    }

    if(whichBut==1){var createnew = (NTCP)?document.createnewscen1:document.all.createnewscen1.style;}
    if(whichBut==2){var createnew = (NTCP)?document.createnewscen2:document.all.createnewscen2.style;}

    if(createnew.visibility!=VISIBLE)
    {
        if(whichBut==2){createnew.pixelTop -= 75}
        createnew.visibility=VISIBLE;
        createnew.display='inline';
    } else if(createnew.visibility==VISIBLE)
    {
        if(whichBut==2){createnew.pixelTop += 75}
        createnew.visibility=HIDDEN;
        createnew.display='none';

    }
}

function OnTopClick(){
    self.scroll(0,0);
}

function onOtherDetailsClick(){
    self.scroll(1116, 1090);
}


var pmWin;
var pmCont = '';

function openPMWin()
{
var numberRows = pmMsgs.length;

pmCont+='<head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  var lastWasSeparator = false;
  for(z=0;z<=numberRows-1;z++)
  {
    if (pmMsgTypes[z]==3 && lastWasSeparator == true) {
        ;
    }
    else if (pmMsgTypes[z]==2) {
        writeRows+='<tr><td colspan=2><font size=2>'+pmMsgs[z]+'</td></tr>';
    }
    else {
        var pmMessIcon = " ";
        if(pmMsgTypes[z]==0){pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
        else if(pmMsgTypes[z]==1){pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
        writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
    }
    if (pmMsgTypes[z]!=3)
        writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

    lastWasSeparator = pmMsgTypes[z]==3;
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close.gif" width=84 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].<%= viewBean.PAGE_NAME%>_sessionUserId.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');

  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//      otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
    pmWin.document.open();
    pmWin.document.write(pmCont);
    pmWin.document.close();
    pmWin.focus();
}


function generateAM()
{
var numberRows = amMsgs.length;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
	

    amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

    var writeRows='';
    var lastWasSeparator = false;

    for(z=0;z<=numberRows-1;z++)
    {
        if (amMsgTypes[z]==3 && lastWasSeparator == true) {
            ;
        }
        else if (amMsgTypes[z]==2) {
            writeRows+='<tr><td colspan=2><font size=2>'+pmMsgs[z]+'</td></tr>';
        }
        else  {
            var amMessIcon= " ";
            if(amMsgTypes[z]==0){amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
            else if(amMsgTypes[z]==1){amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
            writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
        }
        if (amMsgTypes[z]!=3)
            writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

        lastWasSeparator = amMsgTypes[z]==3;

    }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();

}


function isThereMoreThanOneYes(nonSelectedControl)
{
    var theField =(NTCP) ? event.target : event.srcElement;
    var theFieldValue = theField.value;
    var theFieldName = theField.name;

    if(theFieldValue == "Y")
    {
        var rowId = getRepeatRowId(theField);
        var totRows =  getNumOfRow(theFieldName );

        var name1stPart = get1stPartName(theFieldName);
        var name2ndPart = get2ndPartName(theFieldName);

        if(totRows == 1)
            return;

        for(var i=0;i<totRows;i++)
        {
            var ctlname = eval("document.forms[0].elements['" + name1stPart + i + name2ndPart + "']");

            var ctlValue = ctlname.value;
            if(ctlValue == "Y" && i != rowId)
                ctlname.value = 'N';
        }
    }

    return;
}

// New method to populate the Loan Amount with revised LTV %
function populateLoanAmtWithLTV()
{
    var origLTVValue = document.forms[0].<%= viewBean.PAGE_NAME%>_hdUWOrigLTV.value * 1;
    var theLTVValue = document.forms[0].<%= viewBean.PAGE_NAME%>_txUWLTV.value * 1;

    //Do nothing if LTV not changed
    if(theLTVValue == origLTVValue)
        return;

    var theTotalPurchasePrice = document.forms[0].<%= viewBean.PAGE_NAME%>_hdUWPurchasePrice.value;
    var theLoanCtl = document.forms[0].<%= viewBean.PAGE_NAME%>_txUWLoanAmount;

    // Calculate the Loan Amount
    theLoanCtl.value = theTotalPurchasePrice * theLTVValue / 100;

}

// BMO_MI_CR. When MI processing is handled outside Xpress (BMO case) the following should be done:
//================================================================================================ 
// 1. MI Status combobox should be open for BMO client (MI process is outside BXP).
// 2. if cbMIIndicator == 5 (Mortgage Insurance Pre Qualification).
// 3. When BMO the txMIPremium should be open always to allow them to edit the value. The niew MI Rule
//    (the new value can not be less than 0 and more than principal * .051) should be run on Submit.
// 4. hdOutsideXpress = 0 -- MI process inside BXP (regular client).
// 5. hdOutsideXpress = 1 -- MI process outside BXP (BMO, manual access to the db).
// 
// As a result of this CR SetMIPolicyNoDisplay() functions is adjusted
// and the new one SetMIPreQualifCertNumDisplay(), SetMIExistingPolicyDisplay(), and isMiCertificateNoPopulated() 
// were created in order to implement this CR (with isFieldIntegerInDiapason() utility function).
//==================================================================================================
function SetMIPolicyNoDisplay()
{
    var theMIInsurer = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdMIInsurer.value");
    var theMIPolicyNoCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txUWMICertificate");
    ////var theMIStatusUpdateFlag = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdMIStatusUpdateFlag.value");
    var theMIInsurerCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer");
    var theOutsideXpress = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdOutsideXpress.value");
    var theMIInsurerId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer.value");
    var theMIStatusId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIStatus.value");

    var theMIIndicatorId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIIndicator.value");
    var theMIPreQCertNumCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txUWMIPreQCertNum");

        //--BMO_MI_CR//
        if(theOutsideXpress == 0) //MI process inside BXP
        {
        if(theMIInsurerId == 1) //--> CMHC
        {
            theMIPolicyNoCtl.value = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdMIPolicyNoCMHC.value");;
        }
        else if(theMIInsurerId == 2) //--> GE
        {
            theMIPolicyNoCtl.value = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdMIPolicyNoGE.value");
        }
        else if(theMIInsurerId == 3) //--> AIGUG
        {
            theMIPolicyNoCtl.value = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdMIPolicyNoAIGUG.value");
        }
        else if(theMIInsurerId == 4) //--> PMI
        {
            theMIPolicyNoCtl.value = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdMIPolicyNoPMI.value");
        }
        else  //--> Clear it if not selected
        {
            theMIPolicyNoCtl.value = "";
        }
                
                /***
        if(theMIStatusUpdateFlag == "N")
        {
          theMIInsurerCtl.disabled = true;
        }
        else if(theMIStatusUpdateFlag == "Y")
        {
          theMIInsurerCtl.disabled = false;
        }
        **/
        
                theMIPolicyNoCtl.disabled = true;   
        }        
        else if(theOutsideXpress == 1) //MI process outside BXP
        {
                theMIPolicyNoCtl.disabled = false;
        if(theMIInsurerId == 1)
        {
          // SEAN Ticket #2265 Jan 09, 2006: switch to business rule to validate this field.
            //isFieldIntegerInDiapason(theMIPolicyNoCtl, 0, 10);
            
             // Approved and Std Guidlined or UW Required
            if(theMIStatusId == 16 && 
              (theMIIndicatorId == 1 || theMIIndicatorId == 2))
            {
          // SEAN Ticket #2265 Jan 09, 2006: switch to business rule to validate this field.
                //isFieldEmpty(theMIPolicyNoCtl);       
            }
            // Approved and Pre-Qualificated
            else if(theMIStatusId == 16 && theMIIndicatorId == 5) 
            {
                isFieldEmpty(theMIPreQCertNumCtl);      
            }                       
            
                                    
            return false;           
        }
        else if(theMIInsurerId == 2)
        {
            //// The definition of this field in db is VARCHAR2(35).            
          // SEAN Ticket #2265 Jan 09, 2006: switch to business rule to validate this field.
            //isFieldIntegerInDiapason(theMIPolicyNoCtl, 10, 35);
            
             // Approved and Std Guidlined or UW Required
            if(theMIStatusId == 16 && 
              (theMIIndicatorId == 1 || theMIIndicatorId == 2))
            {
          // SEAN Ticket #2265 Jan 09, 2006: switch to business rule to validate this field.
                //isFieldEmpty(theMIPolicyNoCtl);       
            }
            // Approved and Pre-Qualificated
            else if(theMIStatusId == 16 && theMIIndicatorId == 5) 
            {
                isFieldEmpty(theMIPreQCertNumCtl);      
            }                       
                        
            return false;
        }       
                
        }        
}


// BMO_MI_CR new functions.
//// Provides field control under all business cases.
function SetMIPreQualifCertNumDisplay()
{
    var theMIInsurer = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdMIInsurer.value");
    var theMIIndicatorId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIIndicator.value");
    var theMIInsurerId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer.value");    
    var theOutsideXpress = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdOutsideXpress.value");    
    var theMIPreQCertNumCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txUWMIPreQCertNum");
    var theMIStatusId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIStatus.value");  
    var theMIInsurerCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer");
    var theMIPolicyNoCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txUWMICertificate");
    
    if(theOutsideXpress == 1) //// BMO execution path.
    {   
        theMIPreQCertNumCtl.disabled = false;

        if(theMIInsurerId == 1)
        {
            isFieldIntegerInDiapason(theMIPreQCertNumCtl, 0, 10);
            
             // Approved and Std Guidlined or UW Required
            if(theMIStatusId == 16 && 
              (theMIIndicatorId == 1 || theMIIndicatorId == 2))
            {
          // SEAN Ticket #2265 Jan 09, 2006: switch to business rule to validate this field.
                //isFieldEmpty(theMIPolicyNoCtl);
                theMIPreQCertNumCtl.disabled = true;                
            }
            // Approved and Pre-Qualificated
            else if(theMIStatusId == 16 && theMIIndicatorId == 5) 
            {
                isFieldEmpty(theMIPreQCertNumCtl);      
            }
            else if (theMIIndicatorId == 3 || theMIIndicatorId == 4)
            {
                theMIPreQCertNumCtl.disabled = true;
            }
                        
            return false;           
        }
        else if(theMIInsurerId == 2)
        {
            //// The definition of this field in db is VARCHAR2(35).            
            isFieldIntegerInDiapason(theMIPreQCertNumCtl, 10, 35);
            
             // Approved and Std Guidlined or UW Required
            if(theMIStatusId == 16 && 
              (theMIIndicatorId == 1 || theMIIndicatorId == 2))
            {
          // SEAN Ticket #2265 Jan 09, 2006: switch to business rule to validate this field.
                //isFieldEmpty(theMIPolicyNoCtl);
                theMIPreQCertNumCtl.disabled = true;                
            }
            // Approved and Pre-Qualificated
            else if(theMIStatusId == 16 && theMIIndicatorId == 5) 
            {
                isFieldEmpty(theMIPreQCertNumCtl);      
            }
            else if (theMIIndicatorId == 3 || theMIIndicatorId == 4)
            {
                theMIPreQCertNumCtl.disabled = true;
            }
            
            return false;
        }       
    }
    else if(theOutsideXpress == 0)   //// regular: all clients execution path.
    {
        theMIPreQCertNumCtl.disabled = true;    
    }
} 

function SetMIExistingPolicyDisplay()
{
    var theMIInsurerId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer.value");
    var theOutsideXpress = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdOutsideXpress.value");    
    var theMIExistingPolicyCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txUWMIExistingPolicy");
    
    if(theOutsideXpress == 1) //// BMO execution path.
    {   
        theMIExistingPolicyCtl.disabled = false;

        if(theMIInsurerId == 1)
        {
            isFieldIntegerInDiapason(theMIExistingPolicyCtl, 0, 10);            
                        return false;
        }
        else if(theMIInsurerId == 2)
        {
            //// The definition of this field in db is VARCHAR2(35).
            isFieldIntegerInDiapason(theMIExistingPolicyCtl, 10, 35);
            return false;
        }       
    }
} 

// Function to check if the attached field value is an Integer with requestLength
// in the diapason of (min, max) length.
var currFieldToValidate = "";

function isFieldIntegerInDiapason(theField, requestLengthMin, requestLengthMax)
{       
    
    var theFieldValue = theField.value;
    var theFieldName = theField.name;   

    // Check if this is the current field to validate
    //      -- In order to prevent dead lock
    if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
    {
        currFieldToValidate = theFieldName;
    }
    else
    {
        return true;
    }

    if(theFieldValue == "")
    {
        currFieldToValidate = "";
        return true;
    }
    if( !isInteger(theFieldValue) )
    {
        alert(NUMERIC_ONLY);
        var tmp = theField.value;
        theField.focus();
        theField.select();
        return false;
    }
    else
    {
        // Check Length
        if( requestLengthMax > requestLengthMin )
        {
            if( theFieldValue.length < requestLengthMin ||
                theFieldValue.length > requestLengthMax)
            {
                alert(printf(INPUT_DIGITS_RANGE, requestLengthMin, requestLengthMax));
                                
                theField.focus();               
                theField.select();
                
                return false;
            }
        }
    }
    currFieldToValidate = "";
    return true;
}

function isFieldEmpty(theField)
{       
    
    var theFieldValue = theField.value;
    var theFieldName = theField.name;   

    // Check if this is the current field to validate
    //      -- In order to prevent dead lock
    if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
    {
        currFieldToValidate = theFieldName;
    }
    else
    {
        return true;
    }

    if(theFieldValue == "")
    {
        currFieldToValidate = "";
        alert(INPUT_VALUE);
        theField.focus();               
        theField.select();      
        return false;
    }
    currFieldToValidate = "";
    return true;
}

//--DJ_CR_203.1--start--//
//// Provides field control under all business cases.
function SetLineOfCreditDisplay()
{
    var theProprietairePlusLOCCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txProprietairePlusLOC");
    var theProprietairePlusLOCVal = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txProprietairePlusLOC.value");
    var theRbProprietaireCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_rbProprietairePlus");
    
    theRbProprietaireCtl.onclick;


   for (j=0; j < theRbProprietaireCtl.length; ++j)
   {
        var theHomeOwnerProtRadioCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_rbProprietairePlus" + "[j]");
            //// LineOfCredit is disabled, default
        if(theHomeOwnerProtRadioCtl.value == "N" && 
            eval("document.forms[0].<%= viewBean.PAGE_NAME%>_rbProprietairePlus" + "[j]").checked == true) 
        {           
            document.forms[0].<%= viewBean.PAGE_NAME%>_txProprietairePlusLOC.value = 0.00;    
            theProprietairePlusLOCCtl.disabled = true;
        }
            //// LineOfCredit is allowed
        else if(theHomeOwnerProtRadioCtl.value == "Y" && 
                eval("document.forms[0].<%= viewBean.PAGE_NAME%>_rbProprietairePlus" + "[j]").checked == true) 
        {   
            theProprietairePlusLOCCtl.disabled = false;
        }
      }     
}

<%--Release3.1 May 03 begins  --%>
function hideRequestStandardService()
{
    var MIInsurerId = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbUWMIInsurer.value");
    var theMITypeId = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbUWMIType.value");

    //[Request Standard Service]
    var eDivReqStdSvc = document.getElementById("divReqStdSvc");
 
    //hide [Request Standard Service] if NOT GE, 
    //or if GE and (basic or full service)
    //LEN215466: reset rbRequestStandardService by unchecked all
    if(MIInsurerId != 2 || ((MIInsurerId == 2) && (theMITypeId == 1 || theMITypeId == 2))) {
        eDivReqStdSvc.style.display="none";
        var rbRequestStandardServices = document.getElementsByName("<%=viewBean.PAGE_NAME%>_rbRequestStandardService");
        for(i=0;i<rbRequestStandardServices.length;i++)
            if (rbRequestStandardServices[i].checked)
                rbRequestStandardServices[i].checked = false;
    } else
        eDivReqStdSvc.style.display="";
}

function getSelectedRadioValue(radio){
    var retval = "";
    for(var i=0;i<radio.length;i++){
        if(radio[i].checked==true) 
            retval=radio[i].value;
    }
    return retval;
}

<%--Release3.1 May 03 ends  --%>

//--DJ_CR_203.1--start--//

//========================================================================================================
// Function to initialize the page -- may be difference for every pages
//      It should be used in the onLoad event of <body> tag
// ========================================================================================================
function initPage(event)
{
    // Set MIPolicyNo
    SetMIPolicyNoDisplay();
    //--BMO_MI_CR--//
    // Set PreQualifCertNumDisplay
    SetMIPreQualifCertNumDisplay();
    // Set SetMIExistingPolicyDisplay 
    SetMIExistingPolicyDisplay();
    
    //--Release3.1 MI
    // hide or display Request Standard Service
    hideRequestStandardService();
    hideSubProgressAdvance();
    //CR03
    changeExpirationDate();
    //CR03
    
	//5.0 MI -- start
	var miTypeFilter = MITypeFilterFactory(
	        document.getElementById("cbMIInsurer"), 
	        document.getElementById("cbMIType"));
	miTypeFilter();
	
	// Ticket 267
	initMITypeOnDenied();
	//5.0 MI -- end
	//Qualifying rate
//	qualifyChkBoxClicked(true);
initQPR();
}
//--BMO_MI_CR-end--//

//lock miindicator, mitype and miinsurer when dealstatus is collapsed or denied - Ticket 267
function initMITypeOnDenied(){
	var DEAL_STATUS_COLLAPSED = "23";
	var DEAL_STATUS_DENIED = "24";

	var miType = document.getElementById("cbMIType");
	var dealStatusId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdDealStatusId.value");
	if ((DEAL_STATUS_COLLAPSED == dealStatusId || DEAL_STATUS_DENIED == dealStatusId)) 
	{
		miType.disabled = true;
	}
}

// Function to populate the field value to other controls if the value of the other control(s) are null
function populateValueToIfNull(myControlName , theNamesToBePopulate, nonSelectControl)
{
    var theTarget =(NTCP) ? event.target : event.srcElement;
    var theValue = theTarget.value;

    // determine row (if repeated)
    var rowNdx = getRowNdx(nonSelectControl, theTarget , myControlName );
    var numInputsToCheck = theNamesToBePopulate.length;

    for (i = 0; i < numInputsToCheck ; ++i)
    {
        inputRowNdx = "";
        if (rowNdx != -1)
            inputRowNdx = "[rowNdx]";
        var theControl = eval("document.forms[0]." + theNamesToBePopulate[i] + inputRowNdx);
        if( trim(theControl.value) == "" || theControl.value == null )
            theControl.value = theValue;
    }
}

function keepValue(select)
{
    var changedSelect =(NTCP) ? event.target : event.srcElement;
    var selectedOptionValue = getSelectedOptionValue(changedSelect);

    if (selectedOptionValue == -1)
        return;

    if (selectedOptionValue == 0) {
        return false;
    }

    else {
            theControl.value = selectedOptionValue;
    }

    return true;
}

function displayValue(){
    document.open ;
    document.write ( document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer.options[  document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer.selectedIndex  ].value  ) ;
    document.close;
}

    function isNegative ()
    {
        var inpTextbox =(NTCP) ? event.target : event.srcElement;
        var inp = inpTextbox.value;
        var inpName = inpTextbox.name;

        if (inp.length > 0)
        {
            var sign = inp.charAt(0);

            if (sign == '-')
            {
                inpTextbox.select();
                return false;
            }

            for(var i = 1; i < inp.length; i++)
            {
                if( !isDigit( inp.charAt(i) ) )
                {
                    inpTextbox.select();
                    return false;
                }
            }

        }
        return true;
    }


// New method to check wether to Pop-up or hide the Refinance Section
//  -- by BILLY 07Feb2002
<jato:text name="stVALSData" escape="false" />
function DisplayOrHideRefiSection()
{
    var theDealPurposeId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWDealPurpose.value;

    var theRefiSection = (NTCP)?document.refisection:document.all.refisection.style;

    var theIdx = -1;

    // Get the array index from the DealPurpose array
    for(i=0; i< VALScbDealPurpose.length; i++)
    {
        if(theDealPurposeId == VALScbDealPurpose[i])
        {
            theIdx = i;
            break;
        }

    }

    // Check if the corresponding RefinanceFlag is 'Y' ==> Display or 'N' ==> Hide
    //--> Modified to Show the section by default : By Billy 12June2003
    if((theIdx >= 0) && (VALSRefinanceFlags[theIdx] == 'N'))
    {
        theRefiSection.display='none';
    }
    else
    {
        theRefiSection.display='inline';
    }
}

//begin Qualify Rate 
function initQPR() //called on page load/reload
{
	var qualifyProduct = document.getElementById("cbQualifyProductType");
	if (document.getElementById("chQualifyRateOverride").checked)
	{
		qualifyProduct.disabled = false;
		showProdPencil();
	}
	else
	{
		if(document.getElementById("chQualifyRateOverrideRate").checked)
		{
			var productId = document.getElementById("hdQualifyProductId").value;
			qualifyProduct.disabled = true;
			var qualifyProduct = document.getElementById("cbQualifyProductType");
			//if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
				hideProdPencil();
			//else
			//	showProdPencil();
		}
		else
		{
			selectRow(qualifyProduct, document.getElementById("hdQualifyProductId").value);
			qualifyProduct.disabled = true;
			hideProdPencil();
		}
	}	
	
	if (document.getElementById("chQualifyRateOverrideRate").checked)
	{
		var qualifyRate = document.getElementById("txQualifyRate");
		var extantRate = qualifyRate.value;
		getQualifyProductRate();
		if (parseFloat(extantRate) == parseFloat(qualifyRate.value))
		{
			hideRatePencil();
			document.getElementById("chQualifyRateOverrideRate").checked = false;
			document.getElementById("txQualifyRate").disabled = true;
		}
		else
		{
			qualifyRate.value = extantRate;
			showRatePencil("N");
			document.getElementById("txQualifyRate").disabled = false;
		}
	}
	else
	{
		document.getElementById("txQualifyRate").disabled = true;
		getQualifyProductRate();
		hideRatePencil();
	}

	qualifyRateToggle();
	
}

function qualifyChkBoxClicked() 
{	
    var chk = document.getElementById("chQualifyRateOverride").checked;
    var rate = document.getElementById("hdQualifyRate").value;
    var productId = document.getElementById("hdQualifyProductId").value;

	var disableRate = document.getElementById("QualifyRateDisabled").value;
	
    var qualifyRate = document.getElementById("txQualifyRate");
    var qualifyProduct = document.getElementById("cbQualifyProductType");
    
	qualifyProduct.disabled = !chk;

	//document.getElementById("chQualifyRateOverrideRate").disabled = !chk;
//	document.getElementById("chQualifyRateOverrideRate").checked = false;
//	document.getElementById("txQualifyRate").disabled = !(document.getElementById("chQualifyRateOverrideRate").checked); 

	if(!chk) 
	{
		selectRow(qualifyProduct, productId);
		hideProdPencil();
	}

	if (!(document.getElementById("chQualifyRateOverrideRate").checked))
		getQualifyProductRate();
	
	if (parseFloat(rate) == parseFloat(qualifyRate.value))
		hideRatePencil();
	else
		showRatePencil(disableRate);

	qualifyRateToggle();
}

function qualifyProductChanged()
{
	getQualifyProductRate(); 
	qualifyRateToggle(); 
	var qualifyProduct = document.getElementById("cbQualifyProductType");
	var productId = document.getElementById("hdQualifyProductId").value;
	if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
		hideProdPencil();
	else
		showProdPencil();
}

function qualifyRateChkBoxClicked()
{
	var chk = document.getElementById("chQualifyRateOverrideRate").checked;
	getQualifyProductRate();
	document.getElementById("txQualifyRate").disabled = !chk; 
	hideRatePencil();
}
function qualifyRateToggle()
{
    var qualifyRate = document.getElementById("txQualifyRate");
	
	if (document.getElementById("QualifyRateDisabled").value == 'Y')
	{
		document.getElementById("staticQualifyRateSpan").innerHTML = "<font size=3>" + qualifyRate.value + " %</font>";
		document.getElementById("editableQualifyRateSpan").style.display = 'none';
	}
	else
	{
		document.getElementById("staticQualifyRateSpan").innerHTML = ""; //should make it take up no space
		document.getElementById("editableQualifyRateSpan").style.display = 'block';
	}
}

function hideRatePencil()
{
	getHtmlElement("ratePencilSpan").style.visibility = HIDDEN;
}

function showRatePencil(disableRate)
{
	var chk = document.getElementById("chQualifyRateOverrideRate").checked;
	if (disableRate == "N" && chk && amGenerate != "Y") //no asterisks required when you can't change the rate
	{
		var qualifyRate = document.getElementById("txQualifyRate");
		if (parseFloat(qualifyRate.value) != parseFloat(fetchedRate))
			getHtmlElement("ratePencilSpan").style.visibility = VISIBLE;
		else
			getHtmlElement("ratePencilSpan").style.visibility = HIDDEN;
	}
	else {
		getHtmlElement("ratePencilSpan").style.visibility = HIDDEN;		
	}
}

function hideProdPencil()
{
	getHtmlElement("prodPencilSpan").style.visibility = HIDDEN;
}

function showProdPencil()
{
	//don't show if we are on an Active Message
	if (amGenerate != "Y")
		getHtmlElement("prodPencilSpan").style.visibility = VISIBLE;
}

// Added Fix for QC 704
 function qualifyProdRate1()
{
var qualifyRate = document.getElementById("txQualifyRate").value;
if (qualifyRate.length == 0)
{
getQualifyProductRate();
}
}
// End QC 704

//FXP33510  v5.0_XpS - Dual Qualifying Rate - Qualifying Product was changed with different LTV 

function qualifyProdRate()
{
	var rateChk = document.getElementById("chQualifyRateOverrideRate").checked;
	var prodChk = document.getElementById("chQualifyRateOverride").checked;
	var productId = document.getElementById("hdQualifyProductId").value;
	var rate = document.getElementById("hdQualifyRate").value;
		
	//alert(rateChk + " = rateChk");
	//alert(prodChk + " = prodChk");
	
	if(rateChk && prodChk)
	{
		//alert("ALL GOOD");
		var qualifyProduct = document.getElementById("cbQualifyProductType");
		qualifyProduct.disabled = false;
		//alert(productId);
		//alert(qualifyProduct.options[qualifyProduct.options.selectedIndex].value);
		if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
			hideProdPencil();
		else
			showProdPencil();
			
	
		document.getElementById("txQualifyRate").disabled = false; 
		
		var qualifyRate = document.getElementById("txQualifyRate");
		var extantRate = qualifyRate.value;
		getQualifyProductRate();
		if (parseFloat(extantRate) == parseFloat(qualifyRate.value))
		{
			hideRatePencil();
		}
		else
		{
			qualifyRate.value = extantRate;
			showRatePencil("N");
			document.getElementById("txQualifyRate").disabled = false;
		}
		
	}
	else if(prodChk && !rateChk)
	{
		//alert("1111 prodChk && !rateChk " );
		var qualifyProduct = document.getElementById("cbQualifyProductType");
		
		qualifyProduct.disabled = false;
		
		if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
			hideProdPencil();
		else
			showProdPencil();
		
		qualifyRateChkBoxClicked();
	}
	else if(!prodChk && rateChk)
	{
		//alert("2222 !prodChk && rateChk = " );
		/////var chk = document.getElementById("chQualifyRateOverride").checked;
	   
		var disableRate = document.getElementById("QualifyRateDisabled").value;
		
	    var qualifyRate = document.getElementById("txQualifyRate");
	    var qualifyProduct = document.getElementById("cbQualifyProductType");
	    
		qualifyProduct.disabled = true;
		qualifyRate.disabled = false;
	
		if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value != productId)
		{
			selectRow(qualifyProduct, productId);
			getQualifyProductRate();
		}
		hideProdPencil();
		
		//document.getElementById("chQualifyRateOverrideRate").disabled = !chk;
	//	document.getElementById("chQualifyRateOverrideRate").checked = false;
	//	document.getElementById("txQualifyRate").disabled = !(document.getElementById("chQualifyRateOverrideRate").checked); 
	
		////if(!chk) 
		////{
			////selectRow(qualifyProduct, productId);
			if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
				hideProdPencil();
			else
				showProdPencil();
		////}
	
		////if (!(document.getElementById("chQualifyRateOverrideRate").checked))
		////	getQualifyProductRate();
		
		////if (parseFloat(rate) == parseFloat(qualifyRate.value))
		////	hideRatePencil();
		////else
		////	showRatePencil(disableRate);
	
		////qualifyRateToggle();
	}
	else if(!prodChk && !rateChk)
	{
		//alert("3333 !prodChk && !rateChk = ");
		qualifyChkBoxClicked();
		qualifyRateChkBoxClicked();
	}
	
}

//FXP33510  v5.0_XpS - Dual Qualifying Rate - Qualifying Product was changed with different LTV - end

//end Qualify Rate

function selectRow(selectObj, selectValue){
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value == selectValue) 
            selectObj.options[i].selected = true;
    }
}

//===========================================================================

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

//MI fixes - FXP32874 - Begin
function checkMIInsurer()
{
	var theMIInsurerId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer.value");	
	var theMIIndicatorId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIIndicator.value");

	
	//QC-Ticket-268- Start
	var theSpecialFeature = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWSpecialFeature.value");
	var theMIInsurerName = document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer[document.forms[0].<%= viewBean.PAGE_NAME%>_cbUWMIInsurer.selectedIndex].text
	//QC-Ticket-268- End
	
	if((theMIIndicatorId ==  1 || theMIIndicatorId == 2 || theMIIndicatorId == 4 || theMIIndicatorId == 5) && theMIInsurerId < 1)
		{
			alert(MI_PROVIDER_EMPTY);
			return false;
		}
		
	//QC-Ticket-268- Start
/*	//Pre Approval
	if(theSpecialFeature == 1 && theMIInsurerId != 0 ){
		alert(SPECIAL_FEATURE_PRE_APPROVAL+" "+theMIInsurerName+".");
		return false;
	}*/
		
	//Application for Loan Assessment
 	if(theMIIndicatorId == 4 && theMIInsurerId == 3 ) {
		alert(theMIInsurerName+" "+MI_APPLICATION_LOAN_ASSESMENT);
 		return false;		
 	}
 	
 	//Mortgage Insurance Pre Qualification
 	if(theMIIndicatorId == 5) {
 		if(theMIInsurerId == 2 || theMIInsurerId == 3) {
		alert(theMIInsurerName+" "+MI_PRE_QUALIFICATION);
 			return false;
 		}
 	}
	//QC-Ticket-268- End
	 	
		setSubmitFlag(true);
		return true;
}
//MI fixes - FXP32874 - End
</SCRIPT>

</HEAD>

<body bgcolor=ffffff onload="initPage(); checkMIResponseLoop();">
<jato:form name="pgUWorksheet" method="post" onSubmit="return IsSubmitButton();">

<jato:hidden name="sessionUserId" />
<jato:hidden name="hdMIPolicyNoCMHC" />
<jato:hidden name="hdMIPolicyNoGE" />
<jato:hidden name="hdOutsideXpress" />
<jato:hidden name="hdMIInsurer" /> 
<jato:hidden name="hdMIStatusId" />
<jato:hidden name="hdMIIndicatorId" />
<jato:hidden name="hdMIPremium" />
<jato:hidden name="hdMIPreQCertNum" />
<jato:hidden name="hdReqInProgress" />
<jato:hidden name="hdReqInProgressForAnotherScenario" />
<jato:hidden name="hdMIPolicyNoAIGUG" />
<jato:hidden name="hdMIPolicyNoPMI" />
<jato:hidden name="hdDealStatusId" />


<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<input type="hidden" name="hdShowOtherDetails" value="Y">

    <%@include file="/JavaScript/CommonHeader_en.txt" %>  

<%@include file="/JavaScript/CommonToolbar_en.txt" %>  

<!--Quick Link TOOLBAR//-->
<%@include file="/JavaScript/QuickLinkToolbar.txt" %>

<%@include file="/JavaScript/TaskNavigater.txt" %> 

<!--PAGE BODY//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>
<table border=0 width=100% cellpadding=0 cellspacing=0>
    <tr>
        <td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
    </tr>
</table>

<!--DEAL SUMMARY SNAPSHOT//-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=B8D0DC>
    <td align=center colspan=8
        ><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>
    <td rowspan=2>&nbsp;</td>
    <td><font size=1>DEAL #:</font><br><font size=2><b><jato:text name="stDealId" escape="true" formatType="decimal" formatMask="###0; (-#)" /></b></font></td>
    <td><font size=1>BORROWER NAME:</font><br><font size=2><b><jato:text name="stBorrFirstName" escape="true" />
</b></font></td>
    <td><font size=1>DEAL STATUS:</font><br><font size=2><b><jato:text name="stDealStatus" escape="true" /></b></font></td>
    <td><font size=1>DEAL STATUS DATE:</font><br><font size=2><b><jato:text name="stDealStatusDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
    <td><font size=1>SOURCE FIRM:</font><br><font size=2><b><jato:text name="stSourceFirm" escape="true" /></b></font></td>
    <td><font size=1>SOURCE:</font><br><font size=2><b><jato:text name="stSource" escape="true" /></b></font></td>
    <td><font size=1>UNDERWRITER:</font><br><font size=2><b><jato:text name="stUnderwriterLastName" escape="true" />, <jato:text name="stUnderwriterFirstName" escape="true" /> <jato:text name="stUnderwriterMiddleInitial" escape="true" />.</b></font></td>
<tr>
    <!-- ========== BMO II CCAPS Section 4.6 begins ========== -->
    <!-- by Neil on Feb 14, 2005 -->
    <!--<td align=center colspan=8>&nbsp;</td>-->
    <td align=left colspan=8><font size=2><jato:text name="stCCAPS" escape="true" /><b><jato:text name="stServicingMortgageNumber" escape="true" /></b></font>&nbsp;</td>
    <!-- ========== BMO II CCAPS Section 4.6 ends ========== -->
<tr>
    <td rowspan=2>&nbsp;</td>
    <td><font size=1>DEAL TYPE:</font><br><font size=2><b><jato:text name="stDealType" escape="true" /></b></font></td>
    <td><font size=1>DEAL PURPOSE:</font><br><font size=2><b><jato:text name="stDealPurpose" escape="true" /></b></font></td>
    <td><font size=1>TOT LN AMT:</font><br><font size=2><b>$ <jato:text name="stTotalLoanAmount" escape="true" formatType="decimal" formatMask="#,##0.00; (-#)" /></b></font></td>
    <td><font size=1>APPLICATION DATE:</font><br><font size=2><b><jato:text name="stApplicationDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
    <td><font size=1>PAYMENT TERM:</font><br><font size=2><b><jato:text name="stPmtTerm" escape="true" /></b></font></td>
    <td><font size=1>EST CLOSING DATE:</font><br><font size=2><b><jato:text name="stEstClosingDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
    <td><font size=1>SPECIAL FEATURE:</font><br><font size=2><b><jato:text name="stSpecialFeature" escape="true" /></b></font></td>
<tr>
    <td align=center colspan=8
        ><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>


<!--END DEAL SUMMARY SNAPSHOT//-->

<!--PAGE BODY //-->

<input type="hidden" name="stLenderProductId" value ="<jato:text name="stLenderProductId" fireDisplayEvents="true" escape="true" />">
<input type="hidden" name="stRateTimeStamp" value ="<jato:text name="stRateTimeStamp" fireDisplayEvents="true" escape="true" />">
<input type="hidden" name="stRateLock" value="<jato:text name="stRateLock" fireDisplayEvents="true" escape="true" />">
<input type="hidden" name="stIsPageEditableForServlet" value="<jato:text name="stIsPageEditableForServlet" fireDisplayEvents="true" escape="true" />">

<jato:hidden name="hdLongPostedDate" />


<p>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<!--START CURRENT SCENARIO//-->
<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Current Scenario</b></font></td><tr>
<td colspan=6>&nbsp;</td><tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Scenario [#]&nbsp;</b></font><jato:text name="stScenarioNo" escape="true" formatType="decimal" formatMask="###0; (-#)" /></td>
<td valign=top><font size=2 color=3366cc><b>Scenario Description:</b></font></td>
<td valign=top>
<jato:textField name="txUWScenerioDesc" size="50" maxLength="50" /></td>
<td valign=top><font size=2 color=3366cc><b>Recommended?</b></font> <font size=2><jato:text name="stRecoemendScenario" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Locked?</b></font> <font size=2><jato:text name="stLocked" escape="true" /></font></td><tr>

<td>&nbsp;</td>
<td valign=bottom><jato:text name="stNewScenario1" fireDisplayEvents="true" escape="false" /></td>
<td valign=top><font size=2 color=3366cc><b>Scenarios:</b></font></td>
<td valign=top colspan=3><jato:combobox name="cbScenarios" />&nbsp;&nbsp;&nbsp;<jato:button name="btGoScenario" extraHtml="width=35 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/go.gif" />&nbsp;&nbsp;</td><tr>


<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<!--END CURRENT SCENARIO//-->

</table>
<p>

<!-- START GCD Summary Section -->
<jato:text name="stIncludeGCDSumStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td></tr>
<tr>
    <td valign=top colspan=1>&nbsp;</td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Status:</b></font><br><font size=2><jato:text name="txCreditDecisionStatus"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Decision:</b></font><br><font size=2><jato:text name="txCreditDecision"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Credit Bureau Score:</b></font><br><font size=2><jato:text name="txGlobalCreditBureauScore"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Risk Rating:</b></font><br><font size=2><jato:text name="txGlobalRiskRating"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Internal Score:</b></font><br><font size=2><jato:text name="txGlobalInternalScore"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>MI Status :</b></font><br><font size=2><jato:text name="txMIStatus"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc></font><br><jato:button name="btCreditDecisionPG" extraHtml="onClick = 'setSubmitFlag(true);'"
              fireDisplayEvents="true" src="../images/credit_decision.gif"/></td>
</tr>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>
  </table>
<br><jato:text name="stIncludeGCDSumEnd" escape="false" />
<!-- End GCD Summary Section -->

<!--START GDS/TDS DETAILS//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor="#d1ebff">
<td colspan=9><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=9><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=9 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;<font size=2><jato:text name="stGdsTdsDetailsLabel" escape="true" /></font></b></font></td><tr>
<td colspan=9>&nbsp;</td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff>&nbsp;<b>Combined GDS:</b></font><br>
&nbsp;<font size=2 color=ccffff><jato:text name="stCombinedGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>GDS (Qualifying Rate):</b></font><br>
<font size=2><jato:text name="stCombined3YrGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Combined GDS (Borrower Only):</b></font><br>
<font size=2><jato:text name="stCombinedBorrowerGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff>&nbsp;<b>Combined TDS:</b></font><br>
&nbsp;<font size=2 color=ccffff><jato:text name="stCombinedTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>TDS (Qualifying Rate):</b></font><br>
<font size=2><jato:text name="stCombined3YrTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Combined TDS (Borrower Only):</b></font><br>
<font size=2><jato:text name="stCombinedBorrowerTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td><tr>

<!--Express 5.0 Qualifying Rate Detail-->
<tr style='display: <jato:text name="stQualifyRateEdit"/>'>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="hidden" id="hdQualifyRate" value='<jato:text name="sthdQualifyRate"/>'/>
    <input type="hidden" id="hdQualifyProductId" value='<jato:text name="sthdQualifyProductId"/>'/>
	<input type="hidden" id="QualifyRateDisabled" value='<jato:text name="hdQualifyRateDisabled"/>'/>
</td>
<td colspan=2><font size=2 color=3366cc><b>Net Rate:</b></font><br>
<font size=2>
<table width="100%"><td align="left"><jato:text name="stUWNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</td></font>
<td align="right"><span id='ratePencilSpan' visibility='hidden'><img src="../images/pencil.png" align="bottom"></span></td></table></td>
<td colspan=1><table width="100%"><td align="left"><font size=2 color=3366cc><b>Qualifying Rate:</b></font><br>
<span id='staticQualifyRateSpan'></span><span id="editableQualifyRateSpan"><jato:textField name="txQualifyRate"  formatType="decimal" formatMask="###0.000;-#" size="5" maxLength="5"/> %<font size=2 color=3366cc><jato:checkbox name="chQualifyRateOverrideRate" /><b>&nbsp;&nbsp;Override</b></font></span></td>
<td align="right"><span id='prodPencilSpan' visibility='hidden'>&nbsp;<br><img src="../images/pencil.png" align="bottom"></span></td></table></td>
<td colspan=1><font size=2 color=3366cc><b>Qualifying Product:</b></font><br><font size=2><jato:combobox name="cbQualifyProductType"/></font></td>
<td colspan=3 nowrap><font size=2 color=3366cc>&nbsp;<br><jato:checkbox name="chQualifyRateOverride" /><b>&nbsp;&nbsp;Override</b></font></td>
<td ><jato:button name="btRecalculateGDS" extraHtml="width=75 height=15 alt='' border='0' onClick = 'setSubmitFlag(true); populateLoanAmtWithLTV(); qualifyProdRate1();'" fireDisplayEvents="true" src="../images/recalculate.gif" />&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr style='display: <jato:text name="stQualifyRateHidden"/>'>
  <td valign=top>&nbsp;&nbsp;&nbsp;</td>
  <td colspan=2><font size=2 color=3366cc><b>Net Rate:</b></font><br>
  <font size=2><jato:text name="stUWNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</font></td>
  <td><font size=2 color=3366cc><b>Qualifying Rate:</b></font><br>
  <font size=2><jato:text name="stQualifyRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</font></td>
  <td colspan=2><font size=2 color=3366cc><b>Qualifying Product:</b></font><br>
  <font size=2><jato:text name="stQualifyProductType" escape="true" /></font></td>
</tr>
<!--Express 5.0 Qualifying Rate Detail-->


<td colspan=11><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=11><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=11><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>


<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Applicant Name:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Applicant Type:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>GDS:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>GDS (Qualifying Rate):</b></font></td>
<td valign=top><font size=2 color=3366cc><b>TDS:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>TDS (Qualifying Rate):</b></font></td>
<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<td valign=top><font size=2 color=3366cc><b>Insurance<br>Options :</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Life<br>Coverage :</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Disability<br>Coverage :</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Guarantor<br>Other Loans :</b></font></td>
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
<tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatGDSTDSDetails" type="mosApp.MosSystem.pgUWorksheetRepeatGDSTDSDetailsTiledView">

<td colspan=11><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
<jato:hidden name="hdGDSTDSApplicantId" />
<jato:hidden name="hdGDSTDSApplicantCopyId" /></td><tr>
<td colspan=11><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2><jato:text name="stGDSTDSFirstName" escape="true" /> <jato:text name="stGDSTDSMiddleInitial" escape="true" /> <jato:text name="stGDSTDSLastName" escape="true" /> <jato:text name="stGDSTDSSuffix" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stGDSApplicantType" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stApplicantGDS" escape="true" formatType="decimal" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stApplicant3YrGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stApplicantTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stApplicant3YrTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
<jato:text name="stIncludeLifeDisContentStart" escape="false" />
<td valign=top><font size=2><jato:text name="stInsProportions" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stLifeCoverage" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stDisabilityCoverage" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stGuarantorOtherLoans" escape="true" fireDisplayEvents="true" /></font></td>
<jato:text name="stIncludeLifeDisContentEnd" escape="false" />
<tr>

<td colspan=11><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

</jato:tiledView>
<!--END REPEATING ROW//-->

<td colspan=11><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<td colspan=11><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

<tr><td colspan=10><table border=0 cellspacing=3 width=100%>
<tr>
<td colspan=11>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btLDInsuranceDetails" extraHtml="width=111  height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ld_insurance_details_en.gif" /></td>
</tr>
</table>
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />

</td>
</tr>

<td colspan=11><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=11><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
<!--END GDS/TDS DETAILS//-->

    <p><!--MCM Impl team changes starts - XS_2.45--></p>
    
        <jato:containerView name="qualifyingDetailsPagelet" type="mosApp.MosSystem.pgQualifyingDetailsPageletView">
            <jsp:include page="pgQualifyingDetailsPagelet.jsp"/>
        </jato:containerView>
     
       <!--MCM Impl team changes ends - XS_2.45-->

<p>

<%@include file="pgUWorksheet_terms.jsp" %>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td><tr>
</table>

<!-- Include Lender Product Rate function --//-->
<%@include file="pgUWorksheet_lpr.jsp" %>

<!-- Include Part 2 of UWWorkSheet --//-->
<%@include file="pgUWorksheet_part2.jsp" %>

</center>
</div>
<!-- END BODY OF PAGE// -->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopWithNoteSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<!--CREATE NEW SCENARIO POPUP LAYER//-->
<div id="createnewscen1" class="createnewscen1" name="createnewscen1">
    <table border=0 cellpadding=0 cellspacing=0 border=0 bgcolor=99ccff>
    <td colspan=4><img src="../images/blue_line.gif" width=100% height=2></td><tr>
    <td><font size=2>&nbsp;&nbsp;</font></td>
    <td valign=middle><font size=2>&nbsp;Based On:</font><br><jato:combobox name="cbScenarioPickListTop" /></td>
    <td valign=bottom>&nbsp;&nbsp;<jato:button name="btProceedToCreatScenarioTop" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/proceed.gif" /></td>
    <td align=right rowspan=2><A onclick="return IsSubmited();" HREF="javascript:newScen(1);"><img src="../images/go_close.gif" width=16 height=42 border=0></a></td><tr>
    <td colspan=4><img src="../images/blue_line.gif" width=100% height=2></td><tr>
    </table>
</div>

<!--END CREATE NEW SCENARIO POPUP LAYER//-->


<BR><jato:hidden name="hdDealId" />
<BR><jato:hidden name="hdDealCopyId" />
<jato:hidden name="hdUWPurchasePrice" />
<jato:hidden name="hdUWCombinedLendingValue" />

</jato:form>

<script language="javascript">
<!--
if(NTCP){
    document.dialogbar.left=55;
    document.dialogbar.top=79;
    document.toolpop.left=317;
    document.toolpop.top=79;
    document.pagebody.top=100;
    document.alertbody.top=100;
    document.createnewscen1.left=40;
    document.createnewscen1.top=282;
  document.createnewscen2.left=10;
    document.createnewscen2.top=2060;

}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
    tool_click(4)
}
else{
    tool_click(5)
    location.href="#loadtarget"
}


if(pmGenerate=="Y")
{
    openPMWin();
}

delayInitLenderProductProfile();

DisplayOrHideRefiSection();

//executes initializing method for component info related check boxies.
initConponentInfoCheckBoxis();


//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
