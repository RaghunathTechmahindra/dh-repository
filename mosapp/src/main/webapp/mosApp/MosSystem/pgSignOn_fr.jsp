
<HTML>
<%@page info="pgSignOn" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgSignOnViewBean">

<HEAD>
<TITLE>Ouverture de session</TITLE>

<STYLE>
<!--
.pagebody {position: absolute; visibility: hidden; width: 100%; align: center;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 10; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function tool_click(n) {
	if(n==1){
		var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
		itemtoshow.visibility=VISIBLE;
	}
	if(n==5){
		var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
		var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
		itemtoshow.visibility=VISIBLE;
		if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
	}
}

function OnTopClick(){
	self.scroll(0,0);
}

var pmWin;
var pmCont = '';

function openPMWin()
{
var numberRows = pmMsgs.length;

pmCont+='<head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  for(z=0;z<=numberRows-1;z++)
  {
	  if(pmMsgTypes[z]==0){var pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
	  if(pmMsgTypes[z]==1){var pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
	  writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
	  writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close_fr.gif" width=84 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].sessionUserId.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');
    
  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
	pmWin.document.open();
	pmWin.document.write(pmCont);
	pmWin.document.close();
	pmWin.focus();
}

function generateAM()
{
var numberRows = amMsgs.length;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';

if(amGenerate!="Y")
{
amCont+='NONE</center>';
amCont+='</div>';
document.write(amCont);
document.close();
return;
}

amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
	amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	var writeRows='';

	for(z=0;z<=numberRows-1;z++)
  {
		if(amMsgTypes[z]==0){var amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
		if(amMsgTypes[z]==1){var amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
		writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
		writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}

// Local JS functions
function handleKeyPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;

  if (keycode == 13)
  {
  	if(IsSubmitButton())
	{
		setSubmitFlag(true);
   		performSubmit();
    	return false;
    }
  }
 
  return true;
}

function performSubmit()
{
	//document.forms[0].action="MosSystem/" + document.forms[0].name + ".btSubmit_onWebEvent(btSubmit)"; 
	document.forms[0].action += "?pgSignOn_btSubmit="; 
	//document.forms[0].action += ".btSubmit_onWebEvent(btSubmit)"; 

	//alert("The action = " + document.forms[0].action);
	document.forms[0].submit();
}

// Fack method for consistance
function IsSubmited()
{
  return(true);
}

function setFocusToUserID()
{
 	// set forcus in User ID
	try{
		document.pgSignOn.pgSignOn_tfUserID.focus();
	}
	catch(err){ }
}

//-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff onload="setFocusToUserID();">
<jato:form name="pgSignOn" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">
var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="false" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<div id="pagebody" class="pagebody" name="pagebody">
<br><p><br>
<center>
<table border=0 width=533 bgcolor="003366" cellpadding=0 cellspacing=0>
<td>&nbsp;&nbsp;&nbsp;&nbsp;<font size=4 color="ffcc33">Ouvrir une session</font></td>
<td align=right><img src="../images/SignOnDHExpress.gif" width=236 height=29 alt="<jato:text name="stVersion" fireDisplayEvents="true" escape="true" />" border="0"></td><tr>
<td colspan=2 bgcolor=ffffff><img src="../images/white.gif" width=1 height=1 alt="" border="0"></td>
</table>

<table border=0 width=533 bgcolor=99ccff cellpadding=0 cellspacing=0>
<td colspan=3><img src="../images/sign_line1.gif" width=533 height=1 alt="" border="0"></td><tr>
<td><font color=red><jato:text name="stMessage" escape="true" /></font>
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<font color="3366cc"><b>Veuillez entrer votre code d'usager et mot de passe:</b></font>
<br><img src="../images/sign_line2.gif" width=308 height=1 alt="" border="0"></td>
<td rowspan="3"><img src="../images/DH_logo.gif" alt="" border="0"></td>
<tr>
<td align=center><br>
<table border=0 width=200 cellpadding=2 cellspacing=0>
<td><font size=2 color="3366cc"><b>CODE D'USAGER:</b></font></td>
<td>
<jato:textField name="tfUserID" extraHtml="onkeypress='return handleKeyPressed();'" size="10" maxLength="10" /></td><tr>
<td><font size=2 color="3366cc"><b>PASSE:</b></font></td>
<td><jato:password name="tfPassword" extraHtml="onkeypress='return handleKeyPressed();'" size="10" maxLength="20" /></td><tr>
</table></td><tr>
<td><br></td><tr>
<td align=center colspan=3 height=50><jato:button name="btSubmit" extraHtml=" width=80 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/sign_submit_fr.gif" />&nbsp;&nbsp;&nbsp;
	<!--<jato:button name="btCancel" extraHtml=" width=86 height=25 alt='' border='0'" src="../images/cancel_fr.gif" /> -->
	<a href="javascript:window.close()" onclick="return IsSubmited();" onMouseOver="self.status='Cancel'; return true;" onMouseOut="self.status='';  return true;"><img src="../images/cancel_fr.gif" width=86 height=25 alt="" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>

</table>

<table border=0 width=533 bgcolor=99ccff cellpadding=0 cellspacing=0>

<td colspan="3" width="533" height="5" valign="middle" bgcolor="#99CCFF" align="center">
  <p style="margin-top: 3"> <img src="../images/sign_line2.gif" width=500 height=1 alt="" border="0"> </p>
</td> <tr>
<td width="20" bgcolor="99ccff" rowspan="2"></td>
</center>
<td align="justify" style="padding: 5" bgcolor="99ccff">
  <p style="word-spacing: 0; line-height: 100%; margin-right: 0; margin-top: 0" align="center"><span lang="EN-CA"><b><u><font color="3366cc" size="4">MISE EN GARDE</font></u></b></span></p>
</td>
<center>
<td width=20 bgcolor="99ccff" rowspan="2"></td>
 <tr>
<td align="justify" style="padding-left: 20; padding-right: 20; padding-top: 5; padding-bottom: 0" bgcolor="99ccff" >
  <font size="2" color="3366cc">Ce site Web est un site priv� r�serv� exclusivement � D+H Soci�t� en commandite et � ses filiales, ainsi qu'� leurs employ�s et invit�s respectifs.<br>
L'utilisation de ce site Web par toute personne autre qu'un employ� ou un invit� de D+H  Soci�t� en commandite ou de ses filiales est strictement interdite. D+H  Soci�t� en commandite se pr�vaudra de tous les recours pr�vus par la loi pour emp�cher l'utilisation de ce site Web par des personnes non autoris�es.     Ce site Web peut �tre surveill� en tout temps en raison des n�cessit�s du service. Par cons�quent, si vous n��tes pas autoris� � utiliser ce site, N�ESSAYEZ PAS D�OUVRIR UNE SESSION!<br>
Le contenu du pr�sent site Web est prot�g� par des droits d�auteur. <b>� Tous les autres droits sont r�serv�s, D+H  Soci�t� en commandite, 2001-2012.</b> Tous les autres droits sont r�serv�s. Toute utilisation commerciale, y compris la publication, la diffusion ou la redistribution dans un m�dia quelconque, est interdite.
</font>
</td>
<tr>  
<td colspan="3" width="533" valign="top" height="20" bgcolor="#99CCFF" align="center"> 
  <img src="../images/sign_line2.gif" width=500 height=1 alt="" border="0"> </td> 
</table>
</center>
</div>

<SCRIPT LANGUAGE="JavaScript">
<!--
if(NTCP){
	document.pagebody.top=50;
	document.alertbody.top=50;
}

var isAlert="<jato:text name="stErrorFlag" escape="true" />";
var isFatal="N";

if(isAlert=="Y" || isFatal=="Y"){
tool_click(1)
}
else{
tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</SCRIPT> </jato:form>
</BODY>

</jato:useViewBean>
</HTML>