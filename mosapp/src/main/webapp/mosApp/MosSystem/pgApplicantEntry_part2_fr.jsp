<HTML>
<%@page info="pgApplicantEntry" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgApplicantEntryViewBean">

<!--START CREDIT REFERENCE//-->
<jato:text name="stTargetCredit" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Type de r�f�rence de cr�dit</b></font></td>
</tr>

<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedCreditReference" type="mosApp.MosSystem.pgApplicantEntryRepeatedCreditReferenceTiledView">
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdCreditRefId" /><jato:hidden name="hdCreditRefCopyId" /></td>
<td valign=top><font size=2 color=3366cc><b>Type de r�f�rence de cr�dit:</b></font><br>
  <jato:combobox name="cbCreditRefsType" /></td>
<td valign=top colspan=3><font size=2 color=3366cc><b>Description de la r�f�rence:</b></font><br>
  <jato:textField name="txReferenceDesc" size="80" maxLength="80" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Nom de l'institution:</b></font><br>
<jato:textField name="txInstitutionName" size="20" maxLength="35" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Temps avec r�f�rence:</b></font><br>
<font size=2>Ans: </font><jato:textField name="txTimeWithRefYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Mois: </font><jato:textField name="txTimeWithRefMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o </sup>de compte:</b></font><br>
<jato:textField name="txAccountName" size="20" maxLength="35" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Solde:</b></font><br>
$  <jato:textField name="txCurrentBalance" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=bottom align=right colspan=5>&nbsp;<br>
  <jato:button name="btDeleteCreditRef" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_creditref_fr.gif" />&nbsp;&nbsp;&nbsp;</td>
</tr>


<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
</jato:tiledView>


<!--END REPEATING ROW//-->

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr><td colspan=5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAditionalCreditRefs" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_creditref_fr.gif" /></td></tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END CREDIT REFERENCE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START ASSETS//-->
<jato:text name="stTargetAsset" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=7 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Actif</b></font></td>
</tr>

<tr><td colspan=7>&nbsp;</td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Type d'actif</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Description de l'actif</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Valeur de l'actif</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Comprendre dans la valeur nette?</b></font></td>
<td valign=top><font size=2 color=3366cc><b>% compris dans la valeur nette</td>
<td>&nbsp;</td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>


<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedAssets" type="mosApp.MosSystem.pgApplicantEntryRepeatedAssetsTiledView">
<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdAssetId" /><jato:hidden name="hdAssetCopyId" /></td>
<td valign=top><jato:combobox name="cbAssetType" /></td>
<td valign=top><jato:textField name="txAssetDesc" size="35" maxLength="80" /></td>
<td valign=top nowrap>$ 
<jato:textField name="txAssetValue" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><jato:combobox name="cbAssetIncludeInNetworth" /></td>
<td valign=top>
<jato:textField name="txPercentageIncludedInNetworth" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /> %</td>
<td valign=top align=right><jato:button name="btDeleteAsset" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_asset_fr.gif" />&nbsp;&nbsp;</td>
</tr>

<tr><td colspan=7><jato:hidden name="hdPercentageIncludedInNetworth" /><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=7>

<table border=0 cellspacing=3 width=490>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAddtionalAssets" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_asset_fr.gif" /></td>
<td valign=top><font size=2 color=3366cc><b>Actif total du demandeur:</b></font><br>
<jato:textField name="txTotalAsset" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>


</table>
<!--END ASSETS//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>


<!--START CREDIT BUREAU LIABILITIES//-->
<jato:text name="stTargetCreditBureauLiab" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=3 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=3 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<input type="hidden" id="hdCreditBureauTileSize" value='<jato:text name="stCreditBureauTileSize"/>'/>
<td colspan=1 valign=top height="21" nowrap><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
<jato:text name="stLiabiliesLabel" escape="false" /></b></font></td>
<td colspan=1 valign=top nowrap><b><font size="2" color="#3366CC">R�capitulatif du Bureau de cr�dit:</font><font size=3 color=3366cc>&nbsp;&nbsp;<br>
  </font></b>
<jato:textArea name="txCreditBureauSummary" extraHtml="readonly" rows="4" cols="80" /></td>
<td colspan=1 valign=top align=right height="21"><a href='JavaScript: creditBureauClicked()'><img src="../images/less.gif" id='creditBureauImage' border="0"/></a></td>

</tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>

<tr><td colspan=7 bgcolor=d1ebff height="16"><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<tr>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top ><font size=2 color=3366cc><b>BC</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Type</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Description</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Limite</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Montant</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Paie mensuelle</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Exporter</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Supprimer</b></font></td>
</tr>
<tr><td colspan=12 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=12 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<jato:tiledView name="RepeatedCreditBureauLiabilities" type="mosApp.MosSystem.pgApplicantEntryRepeatedCreditBureauLiabilitiesTiledView">
<tr>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdCBLiabilityId" /><jato:hidden name="hdCBLiabilityCopyId" /><jato:hidden name="hdCBLiabilityPercentageIncludeInGDS" /><jato:hidden name="hdCBLiabilityPercentageIncludeInTDS" /></td>
<td valign=top >
<jato:checkbox name="chCreditBureauIndicator" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityType" size="20" maxLength="20" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityDesc" size="60" maxLength="60" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityLimit" formatType="decimal" formatMask="###0.00; (-#)" size="10" maxLength="14" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityAmount" formatType="decimal" formatMask="###0.00; (-#)" size="10" maxLength="14" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityMonthlyPayment" formatType="decimal" formatMask="###0.00; (-#)" size="8" maxLength="14" /></td>
<td valign=top align="center" ><jato:checkbox name="chCreditBureauLiabilityExport" extraHtml="onclick = \"exportChkBoxClicked();\"" /></td>
<td valign=top align="center" ><jato:checkbox name="chCreditBureauLiabilityDelete" extraHtml="onclick = 'exportChkBoxClicked();'" /></td>
</tr>

<tr id='creditBureanDateSection<jato:text name="stTileIndex"/>'>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=3 ><font size=2 color=3366cc><b>Date d'�ch�ance</b>&nbsp;&nbsp;&nbsp; Mth:&nbsp; <jato:combobox name="cbMonths" />: Day:<jato:textField name="tbDay" formatType="string" formatMask="??" size="2" maxLength="2" />Yr:<jato:textField name="tbYear" formatType="string" formatMask="????" size="4" maxLength="4" /></td>
</tr>

<tr><td colspan=10 height="1"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->

</jato:tiledView>

<tr><td colspan=7 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=13 height="48">

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top width="20%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stBureauViewButton" escape="false" />&nbsp;</td>
<td valign=top width="30%" nowrap><font size=2 color=3366cc><b><jato:text name="stTotalLiabiliesLabel" escape="false" /></b></font><br>
<jato:textField name="txCreditBureauTotalLiab" fireDisplayEvents="true" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
<td valign=top width="30%" nowrap><font size=2 color=3366cc><b><jato:text name="stMonthPaymentLabel" escape="false" /></b></font><br>
<jato:textField name="txCreditBureauTotalMonthlyLiab" fireDisplayEvents="true" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" />&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b><jato:text name="stCreditScoreLabel" escape="false" /></b></font><br>
<jato:textField name="txCreditScore" formatType="decimal" formatMask="###0; (-#)" size="20" maxLength="20" />&nbsp;</td>
<td valign=top width="10%"align="right"><jato:button name="btExportDeleteLiability" extraHtml="onClick = 'exportBtnClicked(); setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ExportAbove_fr.gif" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=11 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=11 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END CREDIT BUREAU LIABILITIES//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START LIABILITIES//-->
<jato:text name="stTargetLiab" fireDisplayEvents="true" escape="false" />

<!-- Start GDS/TDS Section //-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor="#d1ebff">
<td colspan=9><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=9><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff>&nbsp;<b>ABD combin�:</b></font><br>
&nbsp;<font size=2 color=ccffff><jato:text name="stCombinedGDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font></td>
<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>ABD (Taux Admissible):</b></font><br>
<font size=2><jato:text name="stCombined3YrGDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>ABD combin� (emprunteur uniquement):</b></font><br>
<font size=2><jato:text name="stCombinedBorrowerGDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font></td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff>&nbsp;<b>ATD combin�:</b></font><br>
&nbsp;<font size=2 color=ccffff><jato:text name="stCombinedTDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font></td>
<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>ATD (Taux Admissible):</b></font><br>
<font size=2><jato:text name="stCombined3YrTDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>ATD combin� (emprunteur uniquement):</b></font><br>
<font size=2><jato:text name="stCombinedBorrowerTDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font></td><tr>
</table>
<!-- End GDS/TDS Section //-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=13 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<input type="hidden" id="hdLiabilityTileSize" value='<jato:text name="stLiabilityTileSize"/>'/>
<td colspan=3 valign=top height="21"><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Passif</b></font></td>
<td colspan=11 align=right height="21"><a href='JavaScript: liabilityClicked()'><img src="../images/less.gif" id='liabilityImage' border="0"/></a></td>
</tr>

<tr><td colspan=13 bgcolor=d1ebff height="16"><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<tr>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top ><font size=2 color=3366cc><b>BC</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Type</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Description</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Limite</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Montant</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Paie mensuelle</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Pay�</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>ABD?</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>%</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>ATD?</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>%</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Supprimer?</b></font></td>
</tr>
<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=13 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<jato:tiledView name="RepeatedLiabilities" type="mosApp.MosSystem.pgApplicantEntryRepeatedLiabilitiesTiledView">
<tr>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdLiabilityId" /><jato:hidden name="hdLiabilityCopyId" /><jato:hidden name="hdLiabilityPercentageIncludeInGDS" /><jato:hidden name="hdLiabilityPercentageIncludeInTDS" /></td>
<td valign=top >
<jato:checkbox name="chCreditBureauIndicator" extraHtml="disabled = true"/></td>
<td valign=top ><jato:combobox name="cbLiabilityType" /></td>
<td valign=top >
<jato:textField name="txLiabilityDesc" size="15" maxLength="80" /></td>
<td valign=top >
<jato:textField name="txCreditBureauLiabilityLimit" formatType="decimal" formatMask="###0.00; (-#)" size="10" maxLength="14" /></td>
<td valign=top >
<!-- Express 3.2 Merge: DES340960 starts -->
<jato:textField name="txLiabilityAmount" formatType="currency" formatMask="###0.00" size="10" maxLength="14" /></td>
<td valign=top >
<jato:textField name="txLiabilityMonthlyPayment" formatType="currency" formatMask="###0.00" size="8" maxLength="14" /></td>
<!-- Express 3.2 Merge: DES340960 ends -->
<td valign=top ><jato:combobox name="cbLiabilityPayOffType" /></td>
<td valign=top ><jato:combobox name="cblLiabilityIncludeInGDS" /></td>
<td valign=top >
<jato:textField name="txLiabilityPercentageIncludeInGDS" formatType="decimal" formatMask="###0.00; (-#)" size="5" maxLength="6" /></td>
<td valign=top ><jato:combobox name="cblLiabilityIncludeInTDS" /></td>
<td valign=top >
<jato:textField name="txLiabilityPercentageIncludeInTDS" formatType="decimal" formatMask="###0.00; (-#)" size="5" maxLength="6" /></td>
<td valign=top align="center" ><jato:checkbox name="chbLiabilityDelete" extraHtml="onclick = 'deleteChkBoxClicked();'" /></td>
</tr>
<tr id='liabilityDateSection<jato:text name="stTileIndex"/>'>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=3 ><font size=2 color=3366cc><b>Date d'�ch�ance</b>&nbsp;&nbsp;&nbsp; Mth:&nbsp; <jato:combobox name="cbMonths" />: Day:<jato:textField name="tbDay" formatType="string" formatMask="??" size="2" maxLength="2" />Yr:<jato:textField name="tbYear" formatType="string" formatMask="????" size="4" maxLength="4" /></td>
</tr>
<tr><td colspan=13 height="1"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->

</jato:tiledView>

<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=13 height="48">

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top width="0%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAditionalLiabilities" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_liability_fr.gif" /></td>
<td valign=top width="0%" nowrap><font size=2 color=3366cc><b>Total du passif du demandeur:</b></font><br>
<jato:textField name="txTotalLiab" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
<td valign=top width="0%" nowrap><font size=2 color=3366cc><b>Paiements mensuels du passif:</b></font><br>
<jato:textField name="txTotalMonthlyLiab" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" />&nbsp;</td>
<td valign=top width="100%"align="right"><jato:button name="btRecalculate" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/recalculate_fr.gif" />&nbsp;<jato:button name="btDeleteLiability" extraHtml="onClick = 'deleteBtnClicked(); setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_liability_fr.gif" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=13 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=13 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END LIABILITIES//-->


</jato:useViewBean>
</HTML>
