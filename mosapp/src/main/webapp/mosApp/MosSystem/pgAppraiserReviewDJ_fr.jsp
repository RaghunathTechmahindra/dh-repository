
<HTML>
<%@page info="pgAppraiserReviewDJ" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgAppraiserReviewDJViewBean">

<HEAD>
<TITLE>Examen de l'�valuation</TITLE>
<STYLE> 
<!--
.aobody {position: relative; left: 0; top: 0; display:none;}
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--
// =============== Local JavaScript ================================================
function DisplayOrHideAOSection()
{
//alert("Just in !!");

	var theAppraisalSource = (NTCP) ? event.target : event.srcElement;
	var theAppraisalSourceValue = theAppraisalSource.value;
//alert("theAppraisalSourceValue : " + theAppraisalSourceValue);

	var rowNdx = getRepeatRowId(theAppraisalSource);
//alert("rowNdx : " + rowNdx);
	var pid = 0;
	var name1stPart = get1stPartName(theAppraisalSource.name);
	
	if (rowNdx != -1)
    {
		var inputRowNdx = rowNdx+"]_";
      	// Not set the value if the target field disabled
      	pid = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "hbPropertyId'].value");
    }
    else
    {
      	// Not set the value if the target field disabled
      	pid = eval("document.forms[0].elements['" + PAGE_NAME + "_" + "hbPropertyId'].value");
    }
    
    
	//alert(" pid = " + pid);

	var theAOSection = eval("document.all.AOSection_"+pid+".style");

	//--> Show the section if AppraisalSource = "Full Appraisal"
	if(theAppraisalSourceValue != 1)
	{
		//alert(" Set Hidden !! ");
		theAOSection.display='none';
	}
	else
	{
		//alert(" Set Display !! ");
		theAOSection.display='inline';
	}
}

function DisplayOrHideAllAOSection()
{
//alert("Just in !!");

	// get number of repeated rows within the same repeated group
	var numOfRepeatRows = getNumOfRow("pgAppraiserReviewDJ_Repeated1[0]_cbAppraisalSource");
//alert("numOfRepeatRows = " + numOfRepeatRows);
  	var name1stPart = get1stPartName("pgAppraiserReviewDJ_Repeated1[0]_cbAppraisalSource");

	for( x=0; x < numOfRepeatRows ; ++x )
	{
    	var currRowNdx = x+"]_";
    	var theAppraisalSourceValue = eval("document.forms[0].elements['" + name1stPart + currRowNdx + "cbAppraisalSource'].value");
				
		// get PID
      	pid = eval("document.forms[0].elements['" + name1stPart + currRowNdx + "hbPropertyId'].value");
      
		//alert(" pid = " + pid);

		var theAOSection = eval("document.all.AOSection_"+pid+".style");

		//--> Show the section if AppraisalSource = "Full Appraisal"
		if(theAppraisalSourceValue != 1)
		{
			//alert(" Set Hidden !! ");
			theAOSection.display='none';
		}
		else
		{
			//alert(" Set Display !! ");
			theAOSection.display='inline';
		}
	}
}

//===================================================================================
-->
</SCRIPT>

</HEAD>

<body bgcolor=ffffff onload = "if(isAOSupported == true)DisplayOrHideAllAOSection();">
<jato:form name="pgAppraiserReviewDJ" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

//Indicator to specify if Appraisal Order is supported
var isAOSupported = <jato:text name="stIsAOSupported" escape="false" fireDisplayEvents="true" />;

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<!--HEADER//-->
	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %>  

<!--END HEADER//-->

<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>

<tr>
    <TD colSpan=7><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>
<tr>
 	<TD colSpan=7><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
<tr>
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Propri�t�</b></font></td>
<tr>
    <TD colSpan=7><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
    
</table>

<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgAppraiserReviewDJRepeated1TiledView">
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>
<TR>
   	<TD colSpan=7><IMG alt="" border=0 height=1 src="../images/blue_line.gif" width="100%"></TD>
<tr>
   	<TD colSpan=7><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
<tr>
	<td vAlign=top> &nbsp;</td>
<tr>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Propri�t� principale?</b></font></td>
	<td colspan=5 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Adresse de la propri�t�:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Nombre de propri�t�</b></font></td>		
<tr>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stPrimaryProperty" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font></td>
	<td colspan=5 ALIGN=LEFT valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stPropertyAddress" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stPropertyOccurence" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font></td>		
<tr>
<TD vAlign=top>&nbsp;</TD> 
<tr>
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Nom de l'�valuateur</b></font></td> 
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Nom abr�g� de l'�valuateur:</b></font></td>	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Nom de la compagnie d'�valuation:</b></font></td>
	<td colspan=4 ALIGN=LEFT valign=top>&nbsp;&nbsp; <jato:button name="btChangeAppraiser" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/change_appraiser_fr.gif" /> <jato:button name="btAddAppraiser" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_Appraiser_fr.gif" /></td>
<tr>
	<td colspan=1 ALIGN=LEFT valign=top><font size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stAppraiserName" escape="true" /></font></td> 
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stAppraiserShortName" escape="true" /></font></td>	
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stAppraiserCompanyName" escape="true" /></font></td>
	<td colspan=4 ALIGN=LEFT valign=top>&nbsp;</td>
<tr>
	<td vAlign=top> &nbsp;</td>
<tr>
	<td colspan=4 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Adresse de l'�valuateur:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>N<sup>o</sup> de t�l�phone:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>N<sup>o</sup> de t�l�copieur:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Adresse de courriel:</b></font></td>
<tr>
	<td colspan=4 ALIGN=LEFT valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stAppraiserAddress" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stAppraiserExtension" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stFax" escape="true" formatType="string" formatMask="(???)???-????" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stEmail" escape="true" /></font></td>
<tr>
	<td vAlign=top> &nbsp;</td>
<tr>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Prix d'achat:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>�valuation estimative:</b></font></td>	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>�valuation r�elle:</b></font></td>
	<td colspan=4 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date de l'�valuation:</b></font></td>
	
<tr>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stPropertyPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;$<jato:textField name="tbEstimatedAppraisal" formatType="decimal" formatMask="###0.00; (-#)" size="13" maxLength="13" /></font></td>	
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;$<jato:textField name="tbActualAppraisal" formatType="decimal" formatMask="###0.00; (-#)" size="13" maxLength="13" /></font></td>
	<td colspan=4 ALIGN=LEFT valign=top><font size=2 >&nbsp;&nbsp;Mois:&nbsp; <jato:combobox name="cbMonths" /> &nbsp; Jour:&nbsp;<jato:textField name="tbDay" formatType="string" formatMask="??" size="2" maxLength="2" />
	&nbsp;An:&nbsp;<jato:textField name="tbYear" formatType="string" formatMask="????" size="4" maxLength="4" /></font></td>
	
<tr>
	<td vAlign=top> &nbsp;</td>	
<tr>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Source de l'�valuation:</b></font></td>	
	<td colspan=6 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Notes de l'�valuateur:</b></font></td>
<tr>
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:combobox name="cbAppraisalSource" extraHtml="onChange='if(isAOSupported == true)DisplayOrHideAOSection();'" /></font><br>
	<font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Statut de l��valuation:&nbsp;</b></font><font size=2><jato:text name="stAppraisalStatus" escape="true" /></font>
</td>
	<td colspan=6 valign=top><font size=2 >&nbsp;&nbsp;<jato:textArea name="tbAppraiserNotes" extraHtml="readonly" rows="3" cols="80" />
</font></td>
<tr>
<td vAlign=top>&nbsp;<jato:hidden name="hbPropertyId" /><jato:hidden name="hdAppraisalDate" /></td>
<tr>
</table>

<!-- New section to handle Appraisal Order (Begin) -->
<div id='AOSection_<jato:text name="stPropertyId" escape="true" />' 
	class="aobody" name='AOSection_<jato:text name="stPropertyId" escape="true" />' >

<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0 height="213">
<tr>
<td colSpan=7 height="5"><IMG alt="" border=0 height=1 src="../images/blue_line.gif" width="100%"></td>
</tr>
<tr>
    <td colSpan=7 vAlign=top height="15"><FONT color=#3366cc size=2><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Commande d��valuation</B></FONT></td>
</tr>
<tr>
    <td height="19">&nbsp;</td>
</tr>  
<tr>
	<td colspan=2 height="15"><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilisez donn�s du demandeur</b></font><jato:checkbox name="chUseBorrowerInfo" /></td>
	<td colspan=5 height="15"><font size=2 color=3366cc><b>&nbsp;&nbsp;</td>
</tr>
<tr>
	<td height="19">&nbsp;</td>
</tr>
<tr>
	<td colspan=7 height="34"><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nom du propri�taire:<br></b></font>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:textField name="tbPropertyOwnerName" size="35" maxLength="35" /></td>
</tr>
<tr>
	<td height="19">&nbsp;</td>
</tr>
<tr>
	<td colspan=2 height="34"><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nom du contact:<br></b></font>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:textField name="tbAOContactName" size="35" maxLength="35" /></td>
	<td colspan=2 height="34"><font size=2 color=3366cc><b>No. de t�l�phone bureau:<br></b></font>
		<font size=2>
		(<jato:textField name="tbAOContactWorkPhoneNum1" size="3" maxLength="3" />)
		<jato:textField name="tbAOContactWorkPhoneNum2" size="3" maxLength="3" />-
		<jato:textField name="tbAOContactWorkPhoneNum3" size="4" maxLength="4" />X
		<jato:textField name="tbAOContactWorkPhoneNumExt" size="6" maxLength="6" />
		</font>
	</td>
	<td colspan=2 height="34"><font size=2 color=3366cc><b>No. de t�l�phone cellulaire:<br></b></font>
		<font size=2>
		(<jato:textField name="tbAOContactCellPhoneNum1" size="3" maxLength="3" />)
		<jato:textField name="tbAOContactCellPhoneNum2" size="3" maxLength="3" />-
		<jato:textField name="tbAOContactCellPhoneNum3" size="4" maxLength="4" />
		</font>
	</td>
	<td colspan=1 height="34"><font size=2 color=3366cc><b>No. de t�l�phone maison:<br></b></font>
		<font size=2>
		(<jato:textField name="tbAOContactHomePhoneNum1" size="3" maxLength="3" />)
		<jato:textField name="tbAOContactHomePhoneNum2" size="3" maxLength="3" />-
		<jato:textField name="tbAOContactHomePhoneNum3" size="4" maxLength="4" />
		</font>
	</td>
</tr>
<tr>
	<td height="19">&nbsp;</td>
</tr>
<tr>
<td colspan=7 height="34"><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remarques:<br></b></font>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:textArea name="tbCommentsForAppraiser" rows="4" cols="80" />
</td>
</tr>
</table>
</div>
<!-- New section to handle Appraisal Order (End) -->

</jato:tiledView>
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>	
    <TD colSpan=7><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
<TR>
    <TD colSpan=7><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>

<TR>
    <TD colSpan=7 vAlign=top><FONT color=#3366cc size=3><B>&nbsp;&nbsp;&nbsp;R�capitulatif de l'�valuation</B></FONT></TD>
<TR>
    <TD>&nbsp;</TD>
<TR>
    <TD colSpan=7><IMG alt="" border=0 height=2 
      src="../images/light_blue.gif" width=1></TD>
<TR>

<td &nbsp;</td>	
<tr>	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Prix d'achat total:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>�valuation estimative totale:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>�valuation r�elle totale:</b></font></td>
<tr>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stTotalPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stTotalEstimatedAppraisal" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>	
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stTotalActualAppraisal" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<tr>
<td vAlign=top> &nbsp;</td>	
	

<TR>
    <TD colSpan=7><IMG alt="" border=0 height=2  src="../images/light_blue.gif" width=1></TD>
<TR>
    <TD colSpan=7><IMG alt="" border=0 height=1 src="../images/blue_line.gif" width="100%"></TD>

</table>

<P>
<!--
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TR>
    <TD align=right><IMG alt="" border=0 height=25 src="../images/submit_fr.gif" width=83>&nbsp;&nbsp;&nbsp;&nbsp;
   					<IMG alt="" border=0 height=25 src="../images/cancel_fr.gif" width=65>
   	</TD>
</TABLE>
-->
<P>
<P>
<P>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<td align=right>
	<jato:button name="btSubmitSendAOReq" extraHtml="alt='Hit to submit and send Appraisal Order' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/SubmitSendAppraisal_fr.gif" />&nbsp;&nbsp;
	<jato:button name="btSubmit" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;
	<jato:button name="btCancel" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> 
	<jato:button name="btOK" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
