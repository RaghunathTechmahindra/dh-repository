
<HTML>
<%@page info="pgSourceofBusSearch" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgSourceofBusSearchViewBean">

<HEAD>
<TITLE>Recherche de la source d'affaire</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include French System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>

 </HEAD>
<body bgcolor=ffffff onKeyPress="return submitenter(event);">
<jato:form name="pgSourceofBusSearch" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var enterKeyFlag = "";


function setFocus()
{
	enterKeyFlag = '1';
}

function resetFocus()
{
	enterKeyFlag = '0';
}


function submitenter(e)
{
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;

	if(keycode == 13 && enterKeyFlag == '1' )
	{
		//document.pgSourceofBusSearch.action = '../MosSystem/pgSourceofBusSearch.btSearch_onWebEvent(btSearch)';
		document.forms[0].action += "?<%= viewBean.PAGE_NAME%>_btSearch="; 
		document.pgSourceofBusSearch.submit();
		return false;
	}
	
	return true;
		
}		

//--> New Function to check if SystemType selected
//--> By Billy 03Nov2003
function isSystemTypeOK()
{
	if(document.forms[0].pgSourceofBusSearch_cbSystemType.value == -1)
	{
		alert("Veuillez choisir le type de syst�me en premier!");
		return false;
	}
	else
		return true;
}


var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>

<!--HEADER//-->
    <%@include file="/JavaScript/CommonHeader_fr.txt" %>  

<%@include file="/JavaScript/CommonToolbar_fr.txt" %>  
</center>
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

<jato:text name="stIncludeDSSstart" escape="false" />
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br> 
<jato:text name="stIncludeDSSend" escape="false" />

<!--END DEAL SUMMARY SNAPSHOT//-->


<!--BODY OF PAGE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
</table>
<table border=0 width=100% cellpadding=3 cellspacing=0 bgcolor=d1ebff>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Crit�res de recherche</b></font></td><tr>
<td colspan=5>&nbsp;</td><tr>

<td rowspan=8>&nbsp;&nbsp;</td>

<td><font size=2 color=3366cc><b>Nom de la source:</b></font></td>
<td colspan=3>
<jato:textField name="tbSourceName" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="50" maxLength="50" /></td><tr>

<td><font size=2 color=3366cc><b>Firme source:</b></font></td>
<td>
<jato:textField name="tbSourceFirm" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="35" maxLength="35" /></td><tr>

<td><font size=2 color=3366cc><b>Ville de la source:</b></font></td>
<td>
<jato:textField name="tbSourceCity" size="20" maxLength="20" /></td><tr>

<td><font size=2 color=3366cc><b>Province de la source:</b></font></td>
<td colspan=3><jato:combobox name="cbProvince" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" /></td><tr>

<td><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone de la source:</b></font></td>
<td>( 
<jato:textField name="tbSourcePhoneNoPart1" size="3" maxLength="3" /> ) 
<jato:textField name="tbSourcePhoneNoPart2" size="3" maxLength="3" /> - 
<jato:textField name="tbSourcePhoneNoPart3" size="4" maxLength="4" /></td><tr>

<td><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur de la source:</b></font></td>
<td>( 
<jato:textField name="tbSourceFaxPart1" size="3" maxLength="3" /> ) 
<jato:textField name="tbSourceFaxPart2" size="3" maxLength="3" /> - 
<jato:textField name="tbSourceFaxPart3" size="4" maxLength="4" /></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=3><img src="../images/blue_line.gif" width=100% height=1></td>
</table>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><jato:button name="btSearch" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/submit_fr.gif" />&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="height=25 alt='' border='0'  onClick = 'setSubmitFlag(true);'" src="../images/cancel_fr.gif" /></td>
</table>

<!-- RESULTS //-->

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#d1ebff>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td bgcolor=#3366CC colspan=6>&nbsp;&nbsp;<font size=2 color=#ccffff><b>R�sultats</b>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="rowsDisplayed" escape="true" /></font></td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

<jato:tiledView name="rpSourceBusiness" type="mosApp.MosSystem.pgSourceofBusSearchrpSourceBusinessTiledView">

<tr>
<td>&nbsp;&nbsp;</td>
<td><font size=2 color=#3366cc><b>Nom de la source</b></font></td>
<td><font size=2 color=#3366cc><b>Firme source</b></font></td>
<td><font size=2 color="#3366cc"><b>Type de syst�me</b></font></td>
<td><font size=2 color=#3366cc><b>Statut du profil</b></font></td>
<td><font size=2 color=#3366cc><b>Ville</b></font></td>

<tr>
<td>&nbsp;&nbsp;<jato:hidden name="hdRowNum" /> <jato:button name="btSelect" extraHtml="width=81 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/select_fr.gif" /> <jato:button name="btDetails" extraHtml="width=47 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/details_fr.gif" /></td>
<td><font size=2><jato:text name="stSourceName" escape="true" /></font></td>
<td><font size=2><jato:text name="stSourceFirmResult" escape="true" /></font></td>
<td><font size=2><jato:text name="stSourceFirmSysType" escape="true" /></font></td>
<td><font size=2><jato:text name="stStatus" escape="true" /></font></td>
<td><font size=2><jato:text name="stCity" escape="true" /></font></td>
</tr>

<tr>
<td>&nbsp;&nbsp;</td>
<td><font size=2 color=#3366cc><b>Province</b></font></td>
<td><font size=2 color=#3366cc><b>Cat�gorie de la source</b></font></td>
<td>&nbsp;</td>
<td><font size=2 color=#3366cc><b>N<sup>o</sup> de t�l�phone</b></font></td>
<td><font size=2 color=#3366cc><b>N<sup>o</sup> de t�l�copieur</b></font></td>
</tr>

<tr>
<td>&nbsp;&nbsp;</td>
<td><font size=2><jato:text name="stProvince" escape="true" /></font></td>
<td><font size=2><jato:text name="stSourceCategory" escape="true" /></font></td>
<td>&nbsp;</td>
<td><font size=2><jato:text name="stPhoneNumber" escape="true" /></font></td>
<td><font size=2><jato:text name="stFaxNumber" escape="true" /></font></td>
</tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</jato:tiledView>

</table>

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr>
<td align=left><font size=2 color=#3366cc><b>Type de syst�me:</b></font><jato:combobox name="cbSystemType" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" /><jato:button name="btAddSource" src="../images/add_SOB_fr.gif" extraHtml="alt='' border='0' onClick='return isSystemTypeOK();'" /> <jato:button name="btAddFirm" src="../images/add_Source_firm_fr.gif" extraHtml="onClick = 'return isSystemTypeOK();'" /></td>
<td align=right>&nbsp;<jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous_fr.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2_fr.gif" /></td>
</tr>

<tr>
<td align=right colspan="2"><br><img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();"></td>
</tr>

</table>

</center>
<br><br><br><br><br>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR>
<BR><jato:hidden name="hdRowNum2" />


</jato:form>


<script language="javascript">
<!--
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
