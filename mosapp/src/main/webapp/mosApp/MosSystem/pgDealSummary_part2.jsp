
<HTML>
<!-- Begin of DealSummary Part 2 include -->
<%@page info="pgDealSummary" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealSummaryViewBean">
<script language="javascript"> 
function toggleQualifyProduct() {
	var qualProd = document.getElementById("stQualifyProduct").value;
	var text = document.getElementById("displayText");
	if(qualProd == "") {
		text.innerHTML = "";
  	}
} 
function toggleQualifyPencil() {
	var overrideProd = document.getElementById("hdQualifyRateOverride").value;
	var overrideRate = document.getElementById("hdQualifyRateOverrideRate").value;
	if((overrideProd == "Y" || overrideRate == "Y") && amGenerate == "N") 
		document.getElementById("DQRPencil").style.visibility="visible"; 
	else
		document.getElementById("DQRPencil").style.visibility = "hidden";
} 
		

</script>

<!-- Start Of Progress Advance Sectiom-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
		<td colspan=5></td>
	</tr>

	<tr>
		<td colspan=5>
			<font color=3366cc><b>Progress Advance:</b></font>
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Progress Advance</b></font><br>
			<font size=2><jato:text name="stProgressAdvance" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Advance to Date Amount</b></font><br>
			<font size=2><jato:text name="stAdvanceToDateAmt" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Next Advance Amount</b></font><br>
			<font size=2><jato:text name="stNextAdvanceAmt" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Advance Number</b></font><br>
			<font size=2><jato:text name="stAdvanceNumber" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
		</td>
	</tr>
	<tr>
		<td colspan=5>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=5>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
</table>
<!-- End Of Progress Advance Sectiom-->

<!-- Start Of Down Payment-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
			<td colspan=8>
		</td>
	</tr>

	<tr>

		<td >
			<font color=3366cc><b>Down Payment:</b></font>
		</td>
	</tr>

	<tr>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Down Payment Source</b></font>
		</td>	
		<td valign=top>
			<font size=2 color=3366cc><b>Down Payment Description</b></font>
		</td>			
		<td valign=top>
			<font size=2 color=3366cc><b>Down Payment Amount</b></font>
		</td>			
		<td valign=top>
			<font size=2 color=3366cc><b>&nbsp;&nbsp;</b></font>
		</td>			
		
	</tr>
	
<jato:tiledView name="RepeatedDownPayments" type="mosApp.MosSystem.pgDealSummaryRepeatedDownPaymentsTiledView">
		<tr>
			<td valign=top colspan=2>
				<font size=2><jato:text name="stDownPaymentSource" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>	
			<td valign=top>
				<font size=2><jato:text name="stDownPaymentDesc" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>		
			<td valign=top>
				<font size=2><jato:text name="stDownPaymentAmount" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>		
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;</b></font>
			</td>			
				
		</tr>
</jato:tiledView>
</table>

<!-- End Of Down Payment-->



<!-- Start Of Escrow Payments -->

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>

		<tr>
			<td >
				<font color=3366cc><b>Escrow Payments:</b></font>
			</td>
		</tr>

		<tr>
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Escrow Type:</b></font>
			</td>			
			<td valign=top>
				<font size=2 color=3366cc><b>Escrow Payment Description:</b></font>
			</td>			
			<td valign=top>
				<font size=2 color=3366cc><b>Escrow Payment:</b></font>
			</td>			
		</tr>

<jato:text name="stBeginHideEscrowSection" fireDisplayEvents="true" escape="true" />

<jato:tiledView name="RepeatedEscrowPayments" type="mosApp.MosSystem.pgDealSummaryRepeatedEscrowPaymentsTiledView">
		<tr>
			<td valign=top colspan=3>
				<font size=2><jato:text name="stEscrowType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>			
			<td valign=top>
				<font size=2><jato:text name="stEscrowPaymentDesc" escape="true" formatType="string" formatMask="????????????????????????????????????" /></font>
			</td>			
			<td valign=top colspan=3>
				<font size=2><jato:text name="stEscrowPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>			
		</tr>
</jato:tiledView>

<jato:text name="stEndHideEscrowSection" fireDisplayEvents="true" escape="true" />
	</table>

	<!-- End Of Escrow Payments-->

	<!-- Start Of Decisioning Information-->
<input type="hidden" id="hdQualifyRateOverride" value='<jato:text name="hdQualifyRateOverride"/>'/>
<input type="hidden" id="hdQualifyRateOverrideRate" value='<jato:text name="hdQualifyRateOverrideRate"/>'/>
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>Decisioning Information:</b></font>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Credit Score</b></font><br>
				<font size=2><jato:text name="stCreditScore" escape="true" formatType="decimal" formatMask="#,##0; (#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Combined GDS</b></font><br>
				<font size=2><jato:text name="stCombinedGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>GDS (Qualifying Rate)</b></font><br>
				<font size=2><jato:text name="stCombined3YrGds" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Combined TDS</b></font><br>
				<font size=2><jato:text name="stCombinedTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>TDS (Qualifying Rate)</b></font><br>
				<font size=2><jato:text name="stCombined3YrTds" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
	        <td valign=top  style='display: <jato:text name="stQualifyRateEdit"/>'>
                <font size=2 color=3366cc><b>Qualifying Rate Details</b></font><br>
                <font size=2><input type="hidden" id="stQualifyProduct" value='<jato:text name="stQualifyProduct"/>'/><div id="displayText"><jato:text name="stQualifyProduct" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><jato:text name="stQualifyRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %&nbsp;<img src="../images/pencil.png" id="DQRPencil"></font>
            </td>
			<td>				
				<a onclick="return IsSubmited();" href="javascript:viewSub('gdstds')">
					<img src="../images/gds-tds-details.gif" width=88 height=15 alt="" border="0">
				</a>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Combined Income Amount</b></font><br>
				<font size=2><jato:text name="stcombinedTotalIncome" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font> 
				<a onclick="return IsSubmited();" href="javascript:viewSub('inc')">
					<img src="../images/income.gif" width=88 height=15 alt="" border="0">
				</a>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Combined Total Liabilities Amount</b></font><br>
				<font size=2><jato:text name="stcombinedTotalLiabilities" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font> 
				<a onclick="return IsSubmited();" href="javascript:viewSub('liab')">
					<img src="../images/liabilities.gif" width=99 height=15 alt="" border="0">
				</a>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Combined Total Assets Amount</b></font><br>
				<font size=2><jato:text name="stcombinedTotalAssets" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font> 
				<a onclick="return IsSubmited();" href="javascript:viewSub('asst')">
					<img src="../images/asset.gif" width=76 height=15 alt="" border="0">
				</a>
			</td>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
	</table>
	<!-- End Of Decisioning Information-->
	
	
	<!-- Start Of Primary Property Information-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>Primary Property Information:</b></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Property Address</b></font><br>
				<font size=2><jato:text name="stPropertyStreetno" escape="true" /> <jato:text name="stPropertyStreetname" escape="true" /> <jato:text name="stPropertyStreetType" escape="true" />  
				<jato:text name="stPropertyStreetDirection" escape="true" /> <jato:text name="stPropertyAddress2" escape="true" /> <jato:text name="stPropertyUnitNumber" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>City</b></font><br>
				<font size=2><jato:text name="stPropertyCity" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Province</b></font><br>
				<font size=2><jato:text name="stPropertyProvince" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Postal Code</b></font><br>
				<font size=2><jato:text name="stPropertyPostalCode" escape="true" /> <jato:text name="stPropertyPostalCode1" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Property Usage</b></font><br>
				<font size=2><jato:text name="stPropertyUsage" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Occupancy</b></font><br>
				<font size=2><jato:text name="stPropertyOccupancy" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Property Type</b></font><br>
				<font size=2><jato:text name="stPropertyType" escape="true" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b># of Properties</b></font><br>
				<font size=2><jato:text name="stNoOfProperties" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<a onclick="return IsSubmited();" href="javascript:viewSub('prop')">
					<img src="../images/property.gif" width=92 height=15 alt="" border="0">
				</a>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Purchase Price</b></font><br>
				<font size=2><jato:text name="stPropertyPurchasePrice" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Estimated Appraised Value</b></font><br>
				<font size=2><jato:text name="stPropertyEstimatedAppraisalValue" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Actual Appraised Value</b></font><br>
				<font size=2><jato:text name="stPropertyActualApraisedValue" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
			<td valign=top >
				<font size=2 color=3366cc><b>Land Value</b></font><br>
				<font size=2><jato:text name="stPropertyLandValue" escape="true" formatType="currency" formatMask="$#,##0.00; (#)" /></font>
			</td>
			<td valign=top >
				<font size=2 color=3366cc><b>Combined Property Expenses</b></font><br>
				<font size=2><jato:text name="stCombinedTotalPropertyExp" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
			<td valign=top >
				<a onclick="return IsSubmited();" href="javascript:viewSub('expen')">
					<img src="../images/expense-details.gif" width=92 height=15 alt="" border="0">
				</a>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
	</table>
	<!-- End Of Primary Property Information-->
	
	
	<!-- Start Of Mortgage Insurer Information-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=5>
				<font color=3366cc><b>Mortgage Insurance Information:</b></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Indicator</b></font><br>
				<font size=2><jato:text name="stMIIndictorDesc" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Status</b></font><br>
				<font size=2><jato:text name="stMIRequestStatus" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Existing Policy #</b></font><br>
				<font size=2><jato:text name="stMIExistingPolicyNum" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Certificate #</b></font><br>
				<font size=2><jato:text name="stMIPoliceNumber" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Prequalification MI Certificate #</b></font><br>
				<font size=2><jato:text name="stMIPrequalificationMICertNum" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Type</b></font><br>
				<font size=2><jato:text name="stMIType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Insurer</b></font><br>
				<font size=2><jato:text name="stMIInsurer" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Payor</b></font><br>
				<font size=2><jato:text name="stMIPayorDesc" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Upfront</b></font><br>
				<font size=2><jato:text name="stMIUpfront" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>MI Premium</b></font><br>
				<font size=2><jato:text name="stMIPremium" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>MI RU Intervention</b></font><br>
				<font size=2><jato:text name="stMIRUIntervention" fireDisplayEvents="true" escape="true" /></font>
			</td>
			
			<%-- Release3.1 Apr 12, 2006 begins --%>

            <td valign=top>
			<!-- SEAN DJ SPEC-Progress Advance Type July 25, 2005:  -->
        <font size=2 color=3366cc><b>Progress Advance Type:</b></font><br>
				<font size="2"><jato:text name="stProgressAdvanceType" escape="true"/></font>
      <!-- SEAN DJ SPEC-PAT END -->
			</td>
			
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>Progress Advance Inspection By:</B></FONT><BR>
				<font size=2><jato:text name="stProgressAdvanceInspectionBy" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>Self Directed RRSP:</B></FONT><BR>
				<font size=2><jato:text name="stSelfDirectedRRSP" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>CMHC Product Tracker Identifier:</B></FONT><BR>
				<font size=2><jato:text name="stCMHCProductTrackerIdentifier" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			
		</tr>
		
		<TR>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>LOC Repayment:</B></FONT><BR>
				<font size=2><jato:text name="stLOCRepayment" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>Request Standard Service:</B></FONT><BR>
				<font size=2><jato:text name="stRequestStandardService" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>LOC Amortization:</B></FONT><BR>
				<font size=2><jato:text name="stLOCAmortization" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top colspan=2>
				<FONT color=#3366cc size=2><B>LOC Interest Only Maturity Date:</B></FONT><BR>
				<font size=2><jato:text name="stLOCInterestOnlyMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
			</TD
		</TR>
		<%-- Release3.1 Apr 12, 2006 ends --%>
		
		<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
		<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>			
	</table>
	<!-- End Of Mortgage Insurer Information-->

	<!-- Start Of Life Disability Insurance-->
	<jato:text name="stIncludeLifeDisSectionStart" escape="false" />
	
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td colspan=8></td></tr>
		<tr><td ><font color=3366cc><b>Life & Disability Insurance Information:</b></font></td></tr>

		<tr>
		<td valign=top><font size=2 color=3366cc><b>Applicant Name:</b></font></td>	
		<td valign=top><font size=2 color=3366cc><b>Applicant Type:</b></font></td>			
		<td valign=top><font size=2 color=3366cc><b>Insurance Options:</b></font></td>
		<td valign=top><font size=2 color=3366cc><b>Life<br>Coverage :</b></font></td>
		<td valign=top><font size=2 color=3366cc><b>Disability<br>Coverage :</b></font></td>
		<td valign=top><font size=2 color=3366cc><b>Guarantor<br>Other Loans :</b></font></td>		
		<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;</b></font></td>
		</tr>

		<jato:tiledView name="RepeatedLifeDisabilityInsurance" type="mosApp.MosSystem.pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView">
		<tr>
		<td valign=top><font size=2><jato:text name="stLDFirstName" escape="true" /> <jato:text name="stLDMiddleInitial" escape="true" /> <jato:text name="stLDLastName" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stLDApplicantType" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stInsProportions" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stLifeCoverage" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stDisabilityCoverage" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stGuarantorOtherLoans" escape="true" fireDisplayEvents="true" /></font></td>		
		</tr>
		
	</jato:tiledView>
	</table>
		<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>	
	<jato:text name="stIncludeLifeDisSectionEnd" escape="false" />		
	<!-- End Of Life Disability Insurance-->

	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
		<tr>
			<td align=right><jato:button name="btOkMainPageButton" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" /></td>		
		</tr>		
	</table>
				
</center>
</div>

<!-- End Of Main Page-->

<!-- Start Of Application Details Panel-->

<div id="dealsumborr" class="dealsumborr" name="dealsumborr">
<center>
	

<jato:tiledView name="RepeatedBorrowerDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedBorrowerDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=6>
				<font color=3366cc><b>Applicant Details:</b></font>
			</td>
		</tr>
	</table>
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Name</b></font><br>
				<font size=2> <jato:text name="stAPDtlsApplicantSalutation" escape="true" /> <jato:text name="stAPDtlsFirstName" escape="true" /> <jato:text name="stAPDtlsMiddleInitial" escape="true" /> <jato:text name="stAPDtlsLastName" escape="true" /> <jato:text name="stAPDtlsSuffix" escape="true" /></font>
			</td>
			<td valign=top>
			<font size=2 color=3366cc><b>Gender:</b></font><br>
			<font size=2><jato:text name="stGender" /></font>
		</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Type</b></font><br>
				<font size=2><jato:text name="stAPDtlsApplicantType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Primary Borrower?</b></font><br>
				<font size=2><jato:text name="stAPDtlsPrimaryBorrower" fireDisplayEvents="true" escape="true" /></font>
			</td>
				<td valign=top>
			<font size=2 color=3366cc><b>Solicitation:</b></font><br>
			<font size=2><jato:text name="stSolicitation" /></font>
		</td>
            <td valign=top>
				<font size=2 color=3366cc><b>Language Preference:</b></font><br>
				<font size=2><jato:text name="stAPDtlsLanguagePreference" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td colspan=6>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Date Of Birth:</b></font><br>
				<font size=2> <jato:text name="stAPDtlsDateOfBirth" escape="true" formatType="date" formatMask="MMM dd yyyy" /> </font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Marital Status:</b></font><br>
				<font size=2><jato:text name="stAPDtlsMaritalStatus" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>S.I.N.:</b></font><br>
				<font size=2><jato:text name="stAPDtlsSINNo" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Citizenship Status:</b></font><br>
				<font size=2><jato:text name="stAPDtlsCitizenship" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b># of Dependants:</b></font><br>
				<font size=2><jato:text name="stAPDtlsNoOfDependants" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top >
			<font size=2 color=3366cc><b>Smoker:</b></font><br>
			<font size=2><jato:text name="stSmoker"  /></font> 
		</td>
	
		</tr>
		<tr>
			<td colspan=6>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Existing Client?</b></font><br>
				<font size=2><jato:text name="stAPDtlsExistingClient" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Reference Client #:</b></font><br>
				<font size=2><jato:text name="stAPDtlsReferenceCllientNo" escape="true" formatType="decimal" formatMask="###0; (#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b># of Times Bankrupt:</b></font><br>
				<font size=2><jato:text name="stAPDtlsNoOfTimeBankrupt" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Bankruptcy Status:</b></font><br>
				<font size=2><jato:text name="stAPDtlsBankruptcyStatus" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Staff of Lender?</b></font><br>
				<font size=2><jato:text name="stAPDtlsStaffOfLender" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top >
			<font size=2 color=3366cc><b>Employee# :</b></font><br>
			<font size=2><jato:text name="stEmployeeNumber"  /></font> 
		</td>
		</tr>
		<tr>
			<td colspan=6>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Home Phone#:</b></font><br>
				<font size=2><jato:text name="stAPDtlsHomePhone" escape="true" formatType="string" formatMask="(???)???-????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Work Phone #:</b></font><br>
				<font size=2><jato:text name="stAPDtlsWorkPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stAPDtlsWorkPhoneExt" fireDisplayEvents="true" escape="true" /></font>
			</td>
            <td valign=top>
				<font size=2 color=3366cc><b>Cell Phone #:</b></font><br>
				<font size=2><jato:text name="stAPDtlsCellPhone" escape="true" formatType="string" formatMask="(???)???-????" /> </font>
			</td>
			
			<td valign=top>
				<font size=2 color=3366cc><b>Fax #:</b></font><br>
				<font size=2><jato:text name="stAPDtlsFaxNumber" escape="true" formatType="string" formatMask="(???)???-????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Email Address:</b></font><br>
				<font size=2><jato:text name="stAPDtlsEmailAddress" escape="true" /></font>
			</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Preferred Method of Contact:</b></font><br>
			<font size=2><jato:text name="stPreferredMethodOfContact"  /></font></td>
		</tr>
		
		
<!-- Start Of Identifications section Information-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

		<tr>
			<td colspan=8>
			<font color=3366cc><b>Identification:</b></font>
			 
			</td>
		</tr>

		
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgIdentificationTypeRepeated1TiledView">

		<tr>

		<td valign=top>
			<font size=2 color=3366cc><b>Identification Type:</b></font><br>
			<font size=2><jato:text name="stIdentificationType" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Identification Number:</b></font><br>
			<font size=2><jato:text name="stIdentificationNumber" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Identification Source Country:</b></font><br>
			<font size=2><jato:text name="stIdentificationSourceCountry" /></font>
		</td>
		
		
</tr>


</jato:tiledView>
	


</table>
<!-- End Of Identification Information-->
 		<!-- Start Of Application Address Information -->
		<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>	
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>
		<!--<table border=0 width=100% cellpadding=1 cellspacing=0>-->
		<jato:tiledView name="RepeatedApplicantAddress" type="mosApp.MosSystem.pgDealSummaryRepeatedApplicantAddressTiledView">
			<tr>
				<td valign=top colspan=2>
					<font size=2 color=3366cc><b>Address:</b></font><br>
					<font size=2><jato:text name="stAPDAddressLine1" escape="true" /><br><jato:text name="stAPDAddressLine2" escape="true" /> </font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>City:</b></font><br>
					<font size=2><jato:text name="stAPDCity" escape="true" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Province:</b></font><br>
					<font size=2><jato:text name="stAPDProvince" escape="true" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Postal Code:</b></font><br>
					<font size=2><jato:text name="stAPDPostalCode1" escape="true" /> <jato:text name="stAPDPostalCode2" escape="true" /></font>
				</td>
			</tr>
			<tr>
				<td colspan=6>
					<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
				</td>
			</tr>
			<tr>
				<td valign=top>
					<font size=2 color=3366cc><b>Time at Residence:</b></font><br>
					<font size=2><jato:text name="stAPDTimeAtResidence" fireDisplayEvents="true" escape="true" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Address Status:</b></font><br>
					<font size=2><jato:text name="stAPDAddressStatus" escape="true" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Residential Status:</b></font><br>
					<font size=2><jato:text name="stAPDResidentialStatus" escape="true" /></font>
				</td>
			</tr>

</jato:tiledView>
			<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
		</table>	
		<!-- End Of Application Address Information -->
		</tr>
	</table>

	<br>

</jato:tiledView>

		<table border=0 width=100%>
			<tr>
				<td align=right>
					<img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();">
				</td>		
			</tr>		
		</table>

	<table border=0 width=100%>
		<td align=right>
			<a onclick="return IsSubmited();" href="javascript:viewSub('main')">
				<img src="../images/ok.gif" width=86 height=25 alt="" border="0">
			</a>
		</td>
	</table>
</center>
</div>
<!-- End Of Application Details Panel-->



<!-- Start Of General Panels - dropdown displays -->

<!-- Start Of Change Password Drop Down Panel -->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<!-- End Of Change Password Drop Down Panel -->


<!-- Start Of Previous Links Drop down Panel -->
<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>
<!-- End Of Previous Links Drop down Panel -->


<!-- End Of General Panels Like Error Message, Dropdown display-->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->


<!-- Start Of GDS/TDS Details Panel -->

<div id="gdstds" class="gdstds" name="gdstds">
<center>
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td colspan=9><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=9><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		
		<tr>
				<td colspan=6>
					<font color=3366cc><b>GDS Details:</b></font>
				</td>
			</tr>
			<tr>
				<td valign=top colspan=6>
					<font size=2 color=3366cc><b>Combined GDS:</b></font><br>
					<font size=2><jato:text name="stCombinedGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>GDS (Qualifying Rate):</b></font><br>
					<font size=2><jato:text name="stCombined3YrGds" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Combined GDS(Borrower Only):</b></font><br>
					<font size=2><jato:text name="stCombinedBorrowerGDS" escape="true" /></font>
				</td>
			</tr>

	
	<jato:tiledView name="RepeatedGDS" type="mosApp.MosSystem.pgDealSummaryRepeatedGDSTiledView">
		<tr>
			<td valign=top colspan=6>
				<font size=2 color=3366cc><b>Applicant:</b></font><br>
				<font size=2><jato:text name="stApplicantFirstNameGDS" escape="true" /> <jato:text name="stApplicantInitialGDS" escape="true" /> <jato:text name="stApplicantLastNameGDS" escape="true" formatType="string" formatMask="???????????????????????????????????" /> <jato:text name="stApplicantSuffixGDS" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Type:</b></font><br>
				<font size=2><jato:text name="stApplicantTypeGDS" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>GDS:</b></font><br>
				<font size=2><jato:text name="stGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>GDS (Qualifying Rate):</b></font><br>
				<font size=2><jato:text name="st3YrGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
</jato:tiledView>		
	</table>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=2 alt="" border="0">
			</td>
		</tr>
	</table>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>TDS Details:</b></font>
			</td>
		</tr>
		<tr>
			<td valign=top colspan=8>
				<font size=2 color=3366cc><b>Combined TDS:</b></font><br>
				<font size=2><jato:text name="stCombinedTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>TDS (Qualifying Rate):</b></font><br>
				<font size=2><jato:text name="stCombined3YrTds" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Combined TDS(Borrower Only):</b></font><br>
				<font size=2><jato:text name="stCombinedBorrowerTDS" escape="true" /></font>
			</td>
		</tr>
	<jato:tiledView name="RepeatedTDS" type="mosApp.MosSystem.pgDealSummaryRepeatedTDSTiledView">
		<tr>
			<td valign=top colspan=8>
				<font size=2 color=3366cc><b>Applicant:</b></font><br>
				<font size=2>	<jato:text name="stApplicantFirstNameTDS" escape="true" /> <jato:text name="stApplicantInitialTDS" escape="true" /> <jato:text name="stApplicantLastNameTDS" escape="true" formatType="string" formatMask="???????????????????????????????????" /> <jato:text name="stApplicantSuffixTDS" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Type:</b></font><br>
				<font size=2><jato:text name="stApplicantTypeTDS" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>TDS:</b></font><br>
				<font size=2><jato:text name="stTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>TDS (Qualifying Rate):</b></font><br>
				<font size=2><jato:text name="st3YrTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font>
			</td>
		</tr>
</jato:tiledView>
		<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
			<tr>
				<td colspan=8>
					<img src="../images/blue_line.gif" width=100% height=2 alt="" border="0">
				</td>
			</tr>
		</table>
	</table>
	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<td align=right>
			<a onclick="return IsSubmited();" href="javascript:viewSub('main')">
				<img src="../images/ok.gif" width=86 height=25 alt="" border="0">
			</a>
		</td>
	</table>
</center>
</div>
<!-- End Of GDS/TDS Details Panel-->

<!--Start Of Income Details Panel-->
<div id="dealsuminc" class="dealsuminc" name="dealsuminc">
<center>
	<!-- Start - Borrower Summary Details-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=6>
				<font color=3366cc><b>Income Details:</b></font>
			</td>
		</tr>
	</table>
	
	<jato:tiledView name="RepeatedIncomeDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedIncomeDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Name:</b></font><br>
				<font size=2><jato:text name="stIDBorrSalutation" escape="true" /> <jato:text name="stIDborrFirstName" escape="true" /> <jato:text name="stIDBorrMidInitial" escape="true" /> <jato:text name="stIDBorrLastName" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Applicant Type:</b></font><br>
				<font size=2><jato:text name="stIDBorrType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Primary Borrower?</b></font><br>
				<font size=2><jato:text name="stIDBorrFlag" fireDisplayEvents="true" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td colspan=5>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
	</table>
	<!-- End Borrower Summary Details-->

	<!-- Start - Employement Income Details-->

	<jato:text name="stBeginHideEmpSection" fireDisplayEvents="true" escape="true" />
	
	<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=6>
				<font color=3366cc><b>&nbsp;&nbsp;Employment Income</b></font>
			</td>
		</tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
	</table>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<jato:tiledView name="Employment" type="mosApp.MosSystem.pgDealSummaryEmploymentTiledView">
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc colspan=2><b>&nbsp;&nbsp;Employer Name:</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmployerName" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Employment Status</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmploymentStatus" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Employment Type</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmploymentType" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Employment Phone #</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDWorkPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stIDMWorkPhoneExt" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Employment Fax #</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDFaxNum" escape="true" formatType="string" formatMask="(???)???-????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Employment Email Address</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmployeeEmailAddress" escape="true" /></font>
			</td>
		</tr>
		<tr bgcolor=d1ebff>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Employment Mailing Address</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmplAddr1" escape="true" /><br>&nbsp;&nbsp;<jato:text name="stIDEmplAddr2" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;City</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmplCity" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Province</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmplProv" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Postal Code</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmplPostCode1" escape="true" />&nbsp;&nbsp;<jato:text name="stIDEmplPostCode2" escape="true" /></font>
			</td>
		</tr>
		<tr bgcolor=d1ebff>
			<td valign=top >
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Industry Sector</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDIndustrySector" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Occupation</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDOccupation" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Job Title</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDJobTitle" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Time at Job</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDTimeAtJob" fireDisplayEvents="true" escape="true" /></font>
			</td>
		</tr>
		<tr bgcolor=d1ebff>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Income Type</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomeType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Income Description</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomeDesc" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Income Period</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomePeriod" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Income Amount</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomeAmount" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;% Included in GDS</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomePercentIncludeGDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;% Included in TDS</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomePercentIncludeTDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
			</td>
		</tr>
	
</jato:tiledView>
<jato:text name="stEndHideEmpSection" fireDisplayEvents="true" escape="true" />

	<!-- End - Employement Income Details-->

	<!-- Start - Other Income Details-->

<!--	<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff> -->
<jato:text name="stBeginHideOtherIncSection" fireDisplayEvents="true" escape="true" />

		<tr>
			<td colspan=4>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Other Income</b></font>
			</td>
		</tr>
		<tr>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Income Type</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Income Description</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Income Period</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Income Amount</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;% Included in GDS</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;% Included in TDS</b></font></td>
		</tr>
	<jato:tiledView name="OtherIncome" type="mosApp.MosSystem.pgDealSummaryOtherIncomeTiledView">
		<tr>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDIncomeType" escape="true" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDOtherIncomeDesc" escape="true" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDIncomePeriod" escape="true" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDIncomeAmount" escape="true" formatType="currency" formatMask="$#,##0.00; -#" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDOtherIncomeGDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDOtherIncomeTDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
		</tr>
	</jato:tiledView>

<jato:text name="stEndHideOtherIncSection" fireDisplayEvents="true" escape="true" />
		<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
	</table>
<!-- End - Other Income Details-->
</jato:tiledView>
	<!-- End - Income Details-->

	<table border=0 width=100%>
		<tr>
			<td align=right>
			<img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>
	
	<table border=0 width=100%>
		<td align=right>
			<a onclick="return IsSubmited();" href="javascript:viewSub('main')">
				<img src="../images/ok.gif" width=86 height=25 alt="" border="0">
			</a>
		</td>
	</table>
	</center>
	</div>
<!-- End - All Income Details-->

<!-- Include Part 3 of DealSummary --//-->
<jsp:include page="pgDealSummary_part3.jsp" />

</jato:useViewBean>
<!-- End of DealSummary Part 2 include -->
</HTML>