<%@ page contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1" %>
<HTML>
<%@page info="pgAppraiserReview" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgAppraiserReviewViewBean">

<HEAD>
<TITLE>Examen de l'�valuation/AVM</TITLE>
<STYLE> 
<!--
.aobody {position: relative; left: 0; top: 0; display:none;}
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression(QLE?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression(QLE?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
	QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>


<SCRIPT LANGUAGE="JavaScript">
<!--
// =============== Local JavaScript ================================================
function DisplayOrHideAOSection()
{
//alert("Just in !!");

  var theAppraisalSource = (NTCP) ? event.target : event.srcElement;
  var theAppraisalSourceValue = theAppraisalSource.value;
//alert("theAppraisalSourceValue : " + theAppraisalSourceValue);

  var rowNdx = getRepeatRowId(theAppraisalSource);
//alert("rowNdx : " + rowNdx);
  var pid = 0;
  var name1stPart = get1stPartName(theAppraisalSource.name);
  
  if (rowNdx != -1)
    {
    var inputRowNdx = rowNdx+"]_";
        // Not set the value if the target field disabled
        pid = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "hbPropertyId'].value");
    }
    else
    {
        // Not set the value if the target field disabled
        pid = eval("document.forms[0].elements['" + PAGE_NAME + "_" + "hbPropertyId'].value");
    }
    
    
  //alert(" pid = " + pid);

  var theAOSection = eval("document.all.AOSection_"+pid+".style");

  //--> Show the section if AppraisalSource = "Full Appraisal"
  if(theAppraisalSourceValue != 1)
  {
    //alert(" Set Hidden !! ");
    theAOSection.display='none';
  }
  else
  {
    //alert(" Set Display !! ");
    theAOSection.display='inline';
  }
}

function DisplayOrHideAllAOSection()
{
//alert("Just in !!");

  // get number of repeated rows within the same repeated group
  var numOfRepeatRows = getNumOfRow("pgAppraiserReview_Repeated1[0]_cbAppraisalSource");
//alert("numOfRepeatRows = " + numOfRepeatRows);
    var name1stPart = get1stPartName("pgAppraiserReview_Repeated1[0]_cbAppraisalSource");

  for( x=0; x < numOfRepeatRows ; ++x )
  {
      var currRowNdx = x+"]_";
      var theAppraisalSourceValue = eval("document.forms[0].elements['" + name1stPart + currRowNdx + "cbAppraisalSource'].value");
        
    // get PID
        pid = eval("document.forms[0].elements['" + name1stPart + currRowNdx + "hbPropertyId'].value");
      
    //alert(" pid = " + pid);

    var theAOSection = eval("document.all.AOSection_"+pid+".style");

    //--> Show the section if AppraisalSource = "Full Appraisal"
    if(theAppraisalSourceValue != 1)
    {
      //alert(" Set Hidden !! ");
      theAOSection.display='none';
    }
    else
    {
      //alert(" Set Display !! ");
      theAOSection.display='inline';
    }
  }
}

// Function to check if the max length of the field
function isFieldMaxLengthSel(maxLength)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
	}

	if( theFieldValue.length > maxLength )
  {	
  	alert(CHAR_MAX_255);
		theField.focus();
		theField.select();
		return false;
  }
	currFieldToValidate = "";
	return true;
}

function showViewOnlyButton(index) {
    if(getAppraisalRequestButton(index)) {
        getAppraisalRequestButton(index).src = "../images/request_fr.gif";
    }
}
//===================================================================================
-->
</SCRIPT>

</HEAD>

<body bgcolor=ffffff onload = "if(isAOSupported == true)DisplayOrHideAllAOSection(); checkMIResponseLoop();" onresize="resetPosition()" onscroll="resetPosition()" onmousewheel="resetPosition()">
<jato:form name="pgAppraiserReview" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

//Indicator to specify if Appraisal Order is supported
var isAOSupported = <jato:text name="stIsAOSupported" escape="false" fireDisplayEvents="true" />;

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<!--HEADER//-->
	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
    
    <!--QUICK LINK MENU BAR//-->
	<%@include file="/JavaScript/QuickLinkToolbar.txt" %>  
	
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 
	
	<script src="../JavaScript/rc/services.js" type="text/javascript"></script>
	<script src="../JavaScript/rc/components.js" type="text/javascript"></script>
	<script src="../JavaScript/rc/AppraisalAVM.js" type="text/javascript"></script>
	
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->
<input type="hidden" id="hdDealId" value='<jato:text name="stHDDealId"/>'/>
<input type="hidden" id="hdDealCopyId" value='<jato:text name="stHDDealCopyId"/>'/>

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
  <tr>
    <td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
  </tr>
</table>

<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgAppraiserReviewRepeated1TiledView">
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0><tbody>

<tr>
    <TD colSpan=7><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>
</tr>
<tr>
  <TD colSpan=7><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
</tr>
<tr>
  <td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Propri�t�</b></font></td>
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Propri�t� principale?</b></font></td>
  <td colspan=5 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Adresse de la propri�t�:</b></font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Nombre de propri�t�:</b></font></td>    
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stPrimaryProperty" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font></td>
  <td colspan=5 ALIGN=LEFT valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stPropertyAddress" escape="true" /></font></td>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stPropertyOccurence" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font></td>
</tr>
<tr>
      <TD colSpan=7><IMG alt="" border=0 height=3 src="../images/light_blue.gif" width=1></TD>
</tr>
</tbody></table>

<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0><tbody>
<tr>
    <TD colSpan=7 align="center"><IMG alt="" border=0 height=2 src="../images/blue_line.gif" width="98%"></TD>
</tr>
<tr>
    <TD colSpan=7><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
</tr>
<tr>
  <td colspan=7 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Demande d'�valuation</b></font></td>
</tr>
<tr>
  <td colspan=7><img src="../images/light_blue.gif" height=8 width=100% alt="" border="0"></td>
</tr>
<tr>
  <td colspan=7 ALIGN=LEFT valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btChangeAppraiser" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/change_appraiser_fr.gif" /> <jato:button name="btAddAppraiser" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_Appraiser_fr.gif" />
  </td>
</tr>
<tr>
  <td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Nom de l'�valuateur:</b></font></td> 
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Nom abr�g� de l'�valuateur:</b></font></td> 
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Nom de la compagnie d'�valuation:</b></font></td>
  <td colspan=4 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Note d'�valuateur:</b></font></td>
</tr>
<tr>
  <td colspan=1 ALIGN=LEFT valign=top><font size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stAppraiserName" escape="true" /></font></td> 
  <td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stAppraiserShortName" escape="true" /></font></td> 
  <td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stAppraiserCompanyName" escape="true" /></font></td>
  <td colspan=4 rowspan=3 valign=top><font size=2 >&nbsp;&nbsp;<jato:textArea name="tbAppraiserNotes" rows="3" cols="50" /></font></td>
</tr>
<tr>
  <td colspan=3 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Adresse de l'�valuateur:</b></font></td>
</tr>
<tr>
  <td colspan=3 ALIGN=LEFT valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stAppraiserAddress" escape="true" /></font></td>
</tr>
<tr>
  <td vAlign=top> &nbsp;</td>
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Num�ro de t�l�phone:</b></font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Num�ro de t�l�copieur:</b></font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Addresse �lectronique:</b></font></td>
  <td colspan=4 ALIGN=LEFT valign=top>
	<div style='visibility: <jato:text name="stAppraisalTypeHidden"/>'>
	  <font size=2 color=3366cc>&nbsp;&nbsp;<b>�tat d'�valuation:</b></font>
	</div></td>
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stAppraiserExtension" escape="true" /></font></td>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stFax" escape="true" formatType="string" formatMask="(???)???-????" /></font></td>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stEmail" escape="true" /></font></td>
  <td colspan=4 ALIGN=LEFT valign=top>
	<div style='visibility: <jato:text name="stAppraisalTypeHidden"/>'>
	  <font size=2 >&nbsp;&nbsp;<jato:text name="stAppraisalStatus" escape="true" /></font>
	</div></td>
</tr>
<tr>
  <td vAlign=top> &nbsp;</td>
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Prix d'achat:</b></font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>�valuation estimative:</b></font></td>  
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Evaluation r�alis�:</b></font></td>
  <td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date de l'�valuation:</b></font></td>
  <td colspan=3 valign=top>
	  <div style='visibility: <jato:text name="stAppraisalTypeHidden"/>'>
			<font size=2 color=3366cc>&nbsp;&nbsp;<b>Type d'�valuation:</b></font>
		</div></td> 
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stPropertyPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;$<jato:textField name="tbEstimatedAppraisal" formatType="decimal" formatMask="###0.00; (-#)" size="13" maxLength="13" /></font></td> 
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;$<jato:textField name="tbActualAppraisal" formatType="decimal" formatMask="###0.00; (-#)" size="13" maxLength="13" /></font></td>
  <td colspan=1 ALIGN=LEFT valign=top><font size=2 >&nbsp;&nbsp;Mois:&nbsp; <jato:combobox name="cbMonths" extraHtml="id='cbOCSProvider'" /> &nbsp; Jour:&nbsp;<jato:textField name="tbDay" formatType="string" formatMask="??" size="2" maxLength="2" />
  &nbsp;Ann:&nbsp;<jato:textField name="tbYear" formatType="string" formatMask="????" size="4" maxLength="4" /></font></td>
  <td colspan=3 ALIGN=LEFT valign=top>
	  <jato:text name="stAppraisalSourceStart" escape="false"/>
		&nbsp;&nbsp;<jato:combobox name="cbAppraisalSource" extraHtml="onChange='if(isAOSupported == true)DisplayOrHideAOSection();'" />
		<jato:text name="stAppraisalSourceEnd" escape="false"/>
  </td>
</tr>
<tr>
  <td colspan=7><img src="../images/light_blue.gif" height=5 width=100% alt="" border="0"></td>
</tr>
<tr>
  <td colspan=7 align="center"><img src="../images/blue_line.gif" width="96%" height=1 alt="" border="0"></td>
</tr>
<tr>
<td vAlign=top colspan=7><IMG alt="" border=0 height=5 src="../images/light_blue.gif" width="100%"></td>
</tr>
</tbody></table>
<jato:hidden name="hbPropertyId" /><jato:hidden name="hdAppraisalDate" />
<input type="hidden" id='hdPropertyId<jato:text name="stTileIndex"/>' value='<jato:text name="stPropertyId"/>'/>
<input type="hidden" id='stAppraisalRequestId<jato:text name="stTileIndex"/>' value='<jato:text name="stAppraisalRequestId"/>'/>
<input type="hidden" id='stAppraisalBorrowerId<jato:text name="stTileIndex"/>' value='<jato:text name="stAppraisalBorrowerId"/>'/>
<input type="hidden" id='stAppraisalContactId<jato:text name="stTileIndex"/>' value='<jato:text name="stAppraisalContactId"/>'/>
<input type="hidden" id='stAppraisalStatusId<jato:text name="stTileIndex"/>' value='<jato:text name="stAppraisalStatusId"/>'/>

<!-- appraisal request section start. -->
<jato:text name="stAppraisalHiddenStart" escape="false"/>
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0><tbody>
<tr>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Fournisseur:</b></font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Type d'�valuation:</b></font></td>
  <td colspan=2 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Instructions sp�ciaux:</b></font></td>
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <jato:combobox name="cbAppraisalProvider" />
  </font></td>
  <td colspan=1 ALIGN=LEFT valign=top>
    <font size=2 >&nbsp;&nbsp;<jato:combobox name="cbAppraisalProduct" /></font>
  </td>
  <td colspan=2 ALIGN=LEFT valign=top>
    &nbsp;&nbsp;<jato:textArea name="tbAppraisalSpecialInst" rows="3" cols="80" />
  </td>
</tr>
<tr>
  <td vAlign=top colspan=4><IMG alt="" border=0 height=8 src="../images/light_blue.gif" width="100%"></td>
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>�tat d'�valuation:</b><br/>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id='appraisalStatus<jato:text name="stTileIndex"/>'><jato:text name="stAppraisalRequestStatus"/></span>
  </font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date-heure:</b><br/>
  &nbsp;&nbsp;<span id='appraisalDate<jato:text name="stTileIndex"/>'>
	<jato:text name="stAppraisalRequestStatusDate" formatType="date" formatMask="fr|MMM dd yyyy HH:mm" /></span>
  </font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Num�ro de r�f�rence  du fournisseur de service:</b><br/>
  &nbsp;&nbsp;<span id='appraisalRefNum<jato:text name="stTileIndex"/>'>
	<jato:text name="stAppraisalRequestRefNum" escape="true"/></span>
  </font></td>
  <td colspan="1" align="center" valign="top"><font size=2 color=3366cc>
  	<a href='JavaScript: sendAppraisalRequest(<jato:text name="stTileIndex"/>)'>
		<img id='appraisalRequestButton<jato:text name="stTileIndex"/>' border="0" height="25" src="../images/request_fr.gif" width="86"/></a>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  	<a href='JavaScript: sendAppraisalRequest(<jato:text name="stTileIndex"/>, true)'><img border="0" height="25" src="../images/cancelOrder_fr.gif" /></a>
  </font></td>
</tr>
<tr>
  <td colspan=4 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Message d'�tat d'�valuation:</b><br/>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:textArea name="tbAppraisalStatusMessages" extraHtml="readonly" rows="3" cols="80" />
  </font></td>
</tr>
<tr>
  <td colspan=4><img src="../images/light_blue.gif" height=8 width=100% alt="" border="0"></td>
</tr>
<tr>
  <td colspan=4 align="center"><img src="../images/blue_line.gif" width="96%" height=1 alt="" border="0"></td>
</tr>
<tr>
<td colspan=4><IMG alt="" border=0 height=8 src="../images/light_blue.gif" width="100%"></td>
</tr>
</tbody></table>
<jato:text name="stAppraisalHiddenEnd" escape="false"/>
<!-- appraisal request section END. -->

<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0 id='apprContactInfoTable<jato:text name="stTileIndex"/>'><tbody>
<tr>
  <td colspan=4 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Carte de visite de l'�valuateur:</b></font></td>
</tr>
<tr>
  <td colspan=4><img src="../images/light_blue.gif" height=5 width=100% alt="" border="0"></td>
</tr>
<tr>
  <td colspan=4 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Utilis� informations du pr�teur primaire</b>
  </font><jato:checkbox name="chPrimaryBorrowerInfo"/></td>
</tr>
<tr>
  <td valign="top" nowrap>&nbsp;&nbsp;&nbsp;</td>
  <td colspan=2 valign=top><font size=2 color=3366cc><b>Pr�nom du correspondent:</b><br/>
    <jato:textField name="stAOContactFirstName" size="20" maxLength="20" fireDisplayEvents="true"/>
  </font></td>
  <td valign=top><font size=2 color=3366cc><b>Nom du correspondent:</b><br/>
    <jato:textField name="stAOContactLastName" size="20" maxLength="20" fireDisplayEvents="true"/>
  </font></td>
  <td colspan=2 valign=top><font size=2 color=3366cc><b>Nom du propri�taire:</b><br/>
    <jato:textField name="stAOPropertyOwnerName" maxLength="40" fireDisplayEvents="true"/>
  </font></td>
</tr>
<tr>
  <td valign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td colspan=2 valign=top><font size=2 color=3366cc><b>Courriel du Contact:</b></font><br/>
    <jato:textField name="tbAOEmail" maxLength="50" fireDisplayEvents="true"/>
	</td>
  <td colspan=1 valign=top><font size=2 color=3366cc><b>No. de contact de t�l�phone bureau:</b></font><br/>
    (<jato:textField name="tbAOWorkPhoneAreaCode" size="3" maxLength="3" fireDisplayEvents="true"/>) 
    <jato:textField name="tbAOWorkPhoneFirstThreeDigits" size="3" maxLength="3" fireDisplayEvents="true"/> -
    <jato:textField name="tbAOWorkPhoneLastFourDigits" size="4" maxLength="4" fireDisplayEvents="true"/> X 
    <jato:textField name="tbAOWorkPhoneNumExtension" size="6" maxLength="6" fireDisplayEvents="true"/>
  </td>
  <td colspan=1 valign=top><font size=2 color=3366cc><b>No. de contact de t�l�phone cellulaire:</b></font><br/>
    (<jato:textField name="tbAOCellPhoneAreaCode" size="3" maxLength="3" fireDisplayEvents="true"/>) 
    <jato:textField name="tbAOCellPhoneFirstThreeDigits" size="3" maxLength="3" fireDisplayEvents="true"/> -
	<jato:textField name="tbAOCellPhoneLastFourDigits" size="4" maxLength="4" fireDisplayEvents="true"/>
  </td>
  <td colspan=1 valign=top><font size=2 color=3366cc><b>No. de contact de t�l�phone maison:</b></font><br/>
    (<jato:textField name="tbAOHomePhoneAreaCode" size="3" maxLength="3" fireDisplayEvents="true"/>) 
    <jato:textField name="tbAOHomePhoneFirstThreeDigits" size="3" maxLength="3" fireDisplayEvents="true"/> -
	<jato:textField name="tbAOHomePhoneLastFourDigits" size="4" maxLength="4" fireDisplayEvents="true"/>
  </td>
</tr>
<tr>
    <TD colSpan=4><IMG alt="" border=0 height=5 src="../images/light_blue.gif" width=1></TD>
</tr>
</tbody></table>

<script>
    initialize(<jato:text name="stTileIndex"/>,<jato:text name="stRead"/>);
</script>

<!-- New section to handle Appraisal Order (Begin) -->
<div id='AOSection_<jato:text name="stPropertyId" escape="true" />' 
  class="aobody" name='AOSection_<jato:text name="stPropertyId" escape="true" />' >

<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0 height="213">
<tr>
<td colSpan=7 height="5"><IMG alt="" border=0 height=1 src="../images/blue_line.gif" width="100%"></td>
</tr>
<tr>
    <td colSpan=7 vAlign=top height="15"><FONT color=#3366cc size=2><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demande d'�valuation</B></FONT></td>
</tr>
<tr>
    <td height="19">&nbsp;</td>
</tr>  
<tr>
  <td colspan=2 height="15"><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilis� informations du pr�teur primaire</b></font><jato:checkbox name="chUseBorrowerInfo" /></td>
  <td colspan=5 height="15"><font size=2 color=3366cc><b>&nbsp;&nbsp;</td>
</tr>
<tr>
  <td height="19">&nbsp;</td>
</tr>
<tr>
  <td colspan=7 height="34"><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name of Property Owner:<br></b></font>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:textField name="tbPropertyOwnerName" size="35" maxLength="35" /></td>
</tr>
<tr>
  <td height="19">&nbsp;</td>
</tr>
<tr>
  <td colspan=2 height="34"><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pr�nom du correspondent:<br></b></font>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:textField name="tbAOContactName" size="35" maxLength="35" /></td>
  <td colspan=2 height="34"><font size=2 color=3366cc><b>T�l�phone du Contact au Travail:<br></b></font>
    <font size=2>
    (<jato:textField name="tbAOContactWorkPhoneNum1" size="3" maxLength="3" />)
    <jato:textField name="tbAOContactWorkPhoneNum2" size="3" maxLength="3" />-
    <jato:textField name="tbAOContactWorkPhoneNum3" size="4" maxLength="4" />X
    <jato:textField name="tbAOContactWorkPhoneNumExt" size="6" maxLength="6" />
    </font>
  </td>
  <td colspan=2 height="34"><font size=2 color=3366cc><b>Cellulaire du Contact:<br></b></font>
    <font size=2>
    (<jato:textField name="tbAOContactCellPhoneNum1" size="3" maxLength="3" />)
    <jato:textField name="tbAOContactCellPhoneNum2" size="3" maxLength="3" />-
    <jato:textField name="tbAOContactCellPhoneNum3" size="4" maxLength="4" />
    </font>
  </td>
  <td colspan=1 height="34"><font size=2 color=3366cc><b>T�l�phone du Contact � Domicile:<br></b></font>
    <font size=2>
    (<jato:textField name="tbAOContactHomePhoneNum1" size="3" maxLength="3" />)
    <jato:textField name="tbAOContactHomePhoneNum2" size="3" maxLength="3" />-
    <jato:textField name="tbAOContactHomePhoneNum3" size="4" maxLength="4" />
    </font>
  </td>
</tr>
<tr>
  <td height="19">&nbsp;</td>
</tr>
<tr>
<td colspan=7 height="34"><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comments:<br></b></font>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:textArea name="tbCommentsForAppraiser" rows="4" cols="80" />
</td>
</tr>
</table>
</div>
<!-- New section to handle Appraisal Order (End) -->

<!-- Appraisal Summary -->
<jato:text name="stAppraisalHiddenStart" escape="false"/>
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0><tbody>
<tr>  
    <TD colSpan=5><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
</tr>
<TR>
    <TD colSpan=5 align="center"><IMG alt="" border=0 height=2 src="../images/blue_line.gif" width="98%"></TD>
</tr>
<TR>
    <TD colSpan=5 vAlign=top><FONT color=#3366cc size=3><B>&nbsp;&nbsp;&nbsp;Sommaire d'�valuation</B></FONT></TD>
</tr>
<tr>  
    <TD colSpan=5><IMG alt="" border=0 height=5 src="../images/light_blue.gif" width=1></TD>
</tr>
<tr>  
  <td valign=top><font size=2 color=3366cc>&nbsp;</font></td>
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date-heur re�u</b></font></td>  
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Type d'�valuation</b></font></td>
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date d'�valuation</b></font></td>  
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Montant d'�valuation realize</b></font></td>
</tr>
<tr>  
    <TD colSpan=5><IMG alt="" border=0 height=3 src="../images/light_blue.gif" width=1></TD>
</tr>
<jato:text name="stAppraisalSummary" escape="false"/>
<tr>  
    <TD colSpan=5><IMG alt="" border=0 height=5 src="../images/light_blue.gif" width=1></TD>
</tr>
</tbody></table>
<table><tbody>
<tr>  
    <TD><IMG alt="" border=0 height=5 src="../images/light_blue.gif" width=1></TD>
</tr>
</tbody></table>

<jato:text name="stAppraisalHiddenEnd" escape="false"/>
<!-- Appraisal Summary END -->

<!-- AVM Order -->
<jato:text name="stAVMOrderHidden" escape="false"/>
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0><tbody>
<tr>  
    <TD colSpan=4><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
</tr>
<TR>
    <TD colSpan=4 align="center"><IMG alt="" border=0 height=2 src="../images/blue_line.gif" width="98%"></TD>
</tr>
<TR>
    <TD colSpan=4 vAlign=top><FONT color=#3366cc size=3><B>&nbsp;&nbsp;&nbsp;Demande d'�valuation automatis�e</B></FONT></TD>
</tr>
<tr>  
    <TD colSpan=4><IMG alt="" border=0 height=5 src="../images/light_blue.gif" width=1></TD>
</tr>
<tr>
  <td colspan=2 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Fournisseur AVM:</b><br/>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:combobox name="cbAVMProvider" /></font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Produit:</b><br/>
  &nbsp;&nbsp;<jato:combobox name="cbAVMProduct"/></font></td>
  <td colspan="1" align="left">
    &nbsp;&nbsp;<a href='JavaScript: sendAVMRequest(<jato:text name="stTileIndex"/>)'><img alt="request" border="0" height="25" src="../images/request_fr.gif" width="86"/></a>
  </td>
</tr>
<tr>  
    <TD colSpan=4><IMG alt="" border=0 height=3 src="../images/light_blue.gif" width=1></TD>
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>�tat AVM:</b><br/>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id='avmStatus<jato:text name="stTileIndex"/>'><jato:text name="stAVMRequestStatus"/></span>
  </font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date-heure:</b><br/>
  &nbsp;&nbsp;<span id='avmDate<jato:text name="stTileIndex"/>'><jato:text name="stAVMRequestDate" formatType="date" formatMask="fr|MMM dd yyyy"/></span>
  </font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Message d'�tat AVM:</b><br/>
  &nbsp;&nbsp;<span id='avmStatusMsg<jato:text name="stTileIndex"/>'><jato:text name="stAVMRequestStatusMsg"/></span>
  </font></td>
  <td colspan="1" align="center" valign="top">&nbsp;</td>
</tr>
<TR>
    <TD colSpan=4><IMG alt="" border=0 height=3 src="../images/light_blue.gif" width=1></TD>
</tr>
</tbody></table>


<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0><tbody id='avmSummary<jato:text name="stTileIndex"/>'>
<tr>  
    <TD colSpan=6><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
</tr>
<TR>
    <TD colSpan=6 align="center"><IMG alt="" border=0 height=2 src="../images/blue_line.gif" width="98%"></TD>
</tr>
<TR>
    <TD colSpan=6 vAlign=top><FONT color=#3366cc size=3><B>&nbsp;&nbsp;&nbsp;Sommaire d'�valuation automatis�e</B></FONT></TD>
</tr>
<tr>  
    <TD colSpan=6><IMG alt="" border=0 height=5 src="../images/light_blue.gif" width=1></TD>
</tr>
<tr>  
  <td valign=top><font size=2 color=3366cc>&nbsp;</font></td>
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Fournisseur AVM</b></font></td>  
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Valeur AVM</b></font></td>
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Niveau de confidentialit�</b></font></td>  
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Contrefaire de terme valuation</b></font></td>
  <td valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date de valuation</b></font></td>
</tr>
<tr>  
    <TD colSpan=6><IMG alt="" border=0 height=3 src="../images/light_blue.gif" width=1></TD>
</tr>
<jato:text name="stAVMReports" escape="false"/>
</tbody></table>
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0><tbody>
<tr>  
    <TD colSpan=6><IMG alt="" border=0 height=8 src="../images/light_blue.gif" width=1></TD>
</tr>
<TR>
    <TD colSpan=6><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>
</tr>
</tbody></table>

<table><tbody><tr><td>&nbsp;</td></tr></tbody></table>
<jato:text name="stAVMOrderHiddenEnd" escape="false"/>

</jato:tiledView> 
<!-- property tiled view END -->

<!-- valuation summary -->
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0><tbody>
<tr>  
    <TD colSpan=3><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
</tr>
<TR>
    <TD colSpan=3><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>
</tr>
<TR>
    <TD colSpan=3 vAlign=top><FONT color=#3366cc size=3><B>&nbsp;&nbsp;&nbsp;R�capitulatif de l'�valuation</B></FONT></TD>
</tr>
<tr>  
    <TD colSpan=3><IMG alt="" border=0 height=5 src="../images/light_blue.gif" width=1></TD>
</tr>
<tr>  
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Prix d'achat total:</b></font></td>
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Valuation totale estim�e:</b></font></td>  
  <td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Valuation totale r�alis�e:</b></font></td>
</tr>
<tr>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stTotalPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stTotalEstimatedAppraisal" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>  
  <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stTotalActualAppraisal" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
</tr>
<tr>  
    <TD colSpan=3><IMG alt="" border=0 height=8 src="../images/light_blue.gif" width=1></TD>
</tr>
<TR>
    <TD colSpan=3><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>
</tr>
</tbody></table>

<P>
<!--
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TR>
    <TD align=right><IMG alt="" border=0 height=25 src="../images/submit.gif" width=86>&nbsp;&nbsp;&nbsp;&nbsp;
            <IMG alt="" border=0 height=25 src="../images/cancel.gif" width=86>
    </TD>
</TABLE>
-->
<P>
<P>
<P>
<table border=0 width=100% cellpadding=0 cellspacing=0>
  <td align=right>
  <jato:button name="btSubmitToRead" extraHtml="border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/SubmitToRead_fr.gif" />&nbsp;&nbsp;
  <jato:button name="btSubmitSendAOReq" extraHtml="alt='Hit to submit and send Appraisal Order' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/SubmitSendAppraisal_fr.gif" />&nbsp;&nbsp;
  <jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;
  <jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/sign_cancel_fr.gif" /> 
  <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" />
  </td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
  document.dialogbar.left=55;
  document.dialogbar.top=79;
  document.toolpop.left=317;
  document.toolpop.top=79;
  document.pagebody.top=100;
  document.alertbody.top=100;
}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
  tool_click(4)
}
else{
  tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
