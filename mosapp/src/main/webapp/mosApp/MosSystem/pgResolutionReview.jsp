
<HTML>
<%@page info="pgResolutionReview" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgResolutionReviewViewBean">

<HEAD>
<TITLE>Resolution Review</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>

</SCRIPT>
</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgResolutionReview" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<p>
<center>
<!--HEADER-->
	<%@include file="/JavaScript/CommonHeader_en.txt" %>  

<%@include file="/JavaScript/CommonToolbar_en.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>    


<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>
<p>
<table border=0 width=100% cellpadding=2 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
<tr>
	<td colspan=8><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
<tr>

<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Deal Information</b></font></td><tr>
<td colspan=6>&nbsp;</td><tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Servicing Ref. #:</b></font>
<td valign=top><font size=2 color=3366cc><b>Lender:</b></font>
<td valign=top><font size=2 color=3366cc><b>Investor:</b></font>
<td valign=top><font size=2 color=3366cc><b>Reference Source App #:</b></font>
<td valign=top><font size=2 color=3366cc><b>Cross-sell:</b></font>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2><jato:text name="stServicingRefNumber" escape="true" formatType="string" formatMask="???????????????" /></font></td>
<td valign=top><font size=2><jato:text name="stLender" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stInvestor" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stReferenceSourceAppNo" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stCrossCell" escape="true" /></font></td>
</tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Product:</b></font><br>
<font size=2><jato:text name="stProduct" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Rate Code:</b></font><br>
<font size=2><jato:text name="stRateCode" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Rate Date:</b></font><br>
<font size=2><jato:text name="stRateDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Posted Interest Rate:</b></font><br>
<font size=2><jato:text name="stPostedInterestRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Net Rate:</b></font><br>
<font size=2><jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Discount:</b></font>
<td valign=top><font size=2 color=3366cc><b>Premium:</b></font>
<td valign=top><font size=2 color=3366cc><b>Buydown Rate:</b></font>
<td valign=top><font size=2 color=3366cc><b>Rate Locked In:</b></font>
<td valign=top><font size=2 color=3366cc><b>Pre-App. Est. Purchase Price:</b></font>
</tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2><jato:text name="stDiscount" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stPremium" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stBuydownRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stRateLockedIn" fireDisplayEvents="true" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stPreAppEstPurchasePrice" escape="true" formatType="currency" formatMask="#,##0.00; -#" /></font></td>
</tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>P&I Payment:</b></font>
<td valign=top><font size=2 color=3366cc><b>Total Payment:</b></font>
<td valign=top><font size=2 color=3366cc><b>Total Purchase Price:</b></font>
<td valign=top><font size=2 color=3366cc><b>Total Estimated Value:</b></font>
<td valign=top><font size=2 color=3366cc><b>Total Actual Appraised Value:</b></font>
</tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2><jato:text name="stPIPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stTotalPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stTotalPurchasePrice" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stTotalEstimatedValue" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stTotalActualAppraisedVal" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
</tr>


<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Maturity Date:</b></font>
<td valign=top><font size=2 color=3366cc><b>First Payment Date:</b></font>
<td valign=top colspan=3><font size=2 color=3366cc><b>Effective Amortization:</b></font>
</tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
<td valign=top><font size=2><jato:text name="stFirstPaymentDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
<td valign=top><font size=2><jato:text name="stEffectiveAmortization" fireDisplayEvents="true" escape="true" /></font></td>
<tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<!--END DEAL INFORMATION//-->
<!--START BRIDGE DETAILS//-->
<jato:text name="stBeginHideBridgeDetails" fireDisplayEvents="true" escape="false" />
<td colspan=6 bgcolor=ffffff>&nbsp;</td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>

<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Bridge Details</b></font></td><tr>
<td colspan=6>&nbsp;</td>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Bridge Purpose:</b></font><br>
<td valign=top><font size=2 color=3366cc><b>Bridge Loan Amount:</b></font><br>
<td valign=top><font size=2 color=3366cc><b>Maturity Date:</b></font><br>
<td valign=top colspan=2><font size=2 color=3366cc><b>Rate Code:</b></font><br>
</tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2><jato:text name="stBridgePurpose" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBridgeLoanAmount" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stBridgeMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
<td valign=top><font size=2><jato:text name="stBridgeRateCode" escape="true" /></font></td>
</tr>
<tr>
</tr>
<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Posted Interest Rate:</b></font><br>
<td valign=top><font size=2 color=3366cc><b>Rate Date:</b></font><br>
<td valign=top><font size=2 color=3366cc><b>Discount:</b></font><br>
<td valign=top><font size=2 color=3366cc><b>Premium:</b></font><br>
<td valign=top><font size=2 color=3366cc><b>Net Rate:</b></font><br>
</tr>
<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2><jato:text name="stBridgePostedInterestRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBridgeRateDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
<td valign=top><font size=2><jato:text name="stBridgeDiscount" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stBridgePremium" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
<td valign=top><font size=2><jato:text name="stBridgeNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
</tr>
<tr>
    <td colspan=7><IMG alt="" border=0 height=2  src="../images/light_blue.gif" width=1></td>
<tr>
    <td colspan=7><IMG alt="" border=0 height=1 src="../images/blue_line.gif" width="100%"></td>
</tr>
<jato:text name="stEndHideBridgeDetails" fireDisplayEvents="true" escape="false" />
<!--END BRIDGE DETAILS//-->
</table>

<!-- Submit and Cancel buttons //-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" />&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
</table>

</div>


<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>


<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>
</jato:useViewBean>
</HTML>