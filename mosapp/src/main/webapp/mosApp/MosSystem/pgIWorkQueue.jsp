<HTML>
<%@page info="pgIWorkQueue" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgIWorkQueueViewBean">

<HEAD>
<TITLE>Individual WorkQueue1</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression(QLE?135:110); left: 10; width:100%; right-margin:5%;}
.prioritybody {position: absolute; visibility: hidden; top: expression(QLE?120:95); left: 10; width:100%; right-margin:5%;}
.sortpop {position: absolute; visibility:hidden; top:expression(QLE?260:235); left:10;}
.filterpop {position:absolute; visibility:hidden; top:expression(QLE?260:235); left:60;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression(QLE?135:110); width: 100%;overflow: hidden;}

-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
	QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<!--
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function tool_click(n) {
    var itemtomove = (NTCP) ? document.pagebody : document.all.pagebody.style;
    var alertchange = (NTCP) ? document.alertbody:document.all.alertbody.style;
    var sortbody = (NTCP) ? document.sortpop:document.all.sortpop.style;
    var prioritymove = (NTCP)?document.prioritybody:document.all.prioritybody.style;
    if(sortbody.visibility==VISIBLE){sortbody.visibility=HIDDEN;}
    if(n==1){
        var imgchange = "tool_goto"; var hide1img="tool_prev"; var hide2img="tool_tools";
        var itemtochange = (NTCP) ? document.gotobar : document.all.gotobar.style;
        var hideother1 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
        var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
    }
    if(n==2){
        var imgchange = "tool_prev"; var hide1img="tool_goto"; var hide2img="tool_tools";
        var itemtochange = (NTCP) ? document.dialogbar : document.all.dialogbar.style;
        var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
        var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
    }
    if(n==3){
        var imgchange = "tool_tools"; var hide1img="tool_goto"; var hide2img="tool_prev";
        var itemtochange = (NTCP) ? document.toolpop : document.all.toolpop.style;
        var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
        var hideother2 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
    }
    if(n==1 || n==2 || n==3){
        if (itemtochange.visibility==VISIBLE) {
            itemtochange.visibility=HIDDEN;
            itemtomove.top = (NTCP) ? 100:(QLE ? 135:110);            
            //prioritymove.top=(NTCP)?100:110;
            changeImg(imgchange,'off');
            if(alertchange.visibility==VISIBLE){alertchange.top = (NTCP) ? 100:(QLE ? 135:110);}
        }
        else {
            itemtochange.visibility=VISIBLE;
            changeImg(imgchange,'on');
            if(n!=2){
                itemtomove.top=150;
                //prioritymove.top=150;
            }
            else{
                itemtomove.top=290;
                //prioritymove.top=290;
            }
            if(alertchange.visibility==VISIBLE){
                if(n!=2){alertchange.top =  150;}
                else{alertchange.top = 290;}
            }
            if(hideother1.visibility==VISIBLE){hideother1.visibility=HIDDEN; changeImg(hide1img,'off');}
            if(hideother2.visibility==VISIBLE){hideother2.visibility=HIDDEN;
changeImg(hide2img,'off');}
        }
    }
    if(n==4){
        var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
        itemtoshow.visibility=VISIBLE;
    }
    if(n==5){
            if(document.forms[0].displayprioritytasks.value=="Y")
        {
            n = 6;
        }
        else
        {
            var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
            var priorityhide = (NTCP)?document.prioritybody:document.all.prioritybody.style;
            var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
            itemtoshow.visibility=VISIBLE;
            if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
            if(priorityhide.visibility==VISIBLE){priorityhide.visibility=HIDDEN}
            var dealbuttons = (NTCP) ? document.all.dealButtons: document.all.dealButtons.style;
            dealbuttons.visibility = VISIBLE;
	        enableExchangeLink();
        }
    }
    if(n==6){
        var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
        var itemtoshow = (NTCP) ? document.prioritybody: document.all.prioritybody.style;
        itemtoshow.visibility=VISIBLE;
        if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
        document.forms[0].displayprioritytasks.value="N";
        var dealbuttons = (NTCP) ? document.all.dealButtons: document.all.dealButtons.style;
        dealbuttons.visibility = HIDDEN;
        getHtmlElement("exchangeLink").style.visibility = HIDDEN;
    }
}

if(document.images){
    tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on.gif";
    tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto.gif";
    tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on.gif";
    tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev.gif";
    tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on.gif";
    tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools.gif";
}
function changeImg(imgNam,onoff){
    if(document.images){
        document.images[imgNam].src = eval (imgNam+onoff+'.src');
    }
}

function showsortfilter(sfnum){
var hidetool = (NTCP)?document.toolpop:document.all.toolpop.style;
var hidegoto = (NTCP)?document.gotobar:document.all.gotobar.style;
var hidedialog = (NTCP)?document.dialogbar:document.all.dialogbar.style;
var movepage = (NTCP)?document.pagebody: document.all.pagebody.style;

hidetool.visibility=HIDDEN;changeImg('tool_tools','off');
hidegoto.visibility=HIDDEN;changeImg('tool_goto','off');
hidedialog.visibility=HIDDEN;changeImg('tool_prev','off');
movepage.top=(NTCP)?100:(QLE ? 135:110);

var showsortfilter =  (sfnum==1)?(NTCP)?document.sortpop:document.all.sortpop.style:(NTCP)?document.filterpop:document.all.filterpop.style;
var hidesortfilter = (sfnum==2)?(NTCP)?document.sortpop:document.all.sortpop.style:(NTCP)?document.filterpop:document.all.filterpop.style;
hidesortfilter.visibility=HIDDEN;
showsortfilter.visibility=VISIBLE;

}

function cancelsortfilter(sfnum){
var hidesortfilter = (sfnum==1)?(NTCP)?document.sortpop:document.all.sortpop.style:(NTCP)?document.filterpop:document.all.filterpop.style;
hidesortfilter.visibility=HIDDEN;
}

function OnTopClick(){
    self.scroll(0,0);
}

var pmWin;
var pmCont = '';

function openPMWin()
{
var numberRows = pmMsgs.length;

pmCont+='<head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  for(z=0;z<=numberRows-1;z++)
  {
      if(pmMsgTypes[z]==0){var pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
      if(pmMsgTypes[z]==1){var pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
      writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
      writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close.gif" width=84 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  ////  ORIGINAL VERSION.
  ////pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].sessionUserId.value,
    ////'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');

  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].<%= viewBean.PAGE_NAME%>_sessionUserId.value,
     'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');

  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
    pmWin.document.open();
    pmWin.document.write(pmCont);
    pmWin.document.close();
    pmWin.focus();
}


function generateAM()
{
var numberRows = amMsgs.length;;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
    amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

    var writeRows='';

    for(z=0;z<=numberRows-1;z++)
  {
        if(amMsgTypes[z]==0){var amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
        if(amMsgTypes[z]==1){var amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
        writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
        writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}


//-->
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function performSubmit(theParameters)
{
    setSubmitFlag(true);
    document.forms[0].action += "?" + PAGE_NAME + "_btSubmit=" + theParameters;

alert("Vova, from Submit");
alert("document.forms[0].action= " + document.forms[0].action);
  // check if any outstanding request
  ////if(IsSubmitButton())
     document.forms[0].submit();
alert("After Submit");
}
</SCRIPT>


</HEAD>
<body bgcolor=ffffff link=000000 vlink=000000 alink=000000>
<jato:form name="pgIWorkQueue" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";

////alert("pmHasTitle: " + "<jato:text name="stPmHasTitle" escape="true" />");

var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<input type="hidden" name="displayprioritytasks" value="<jato:text name="displayprioritytasks" escape="true" />">
<!--form-->


	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
 	<%@include file="/JavaScript/QuickLinkToolbar.txt" %>
	<!--END HEADER//-->

	<!--DEAL SUMMARY SNAPSHOT//-->
	<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>

	<!--END DEAL SUMMARY SNAPSHOT//-->
<p>
<!--BODY OF PAGE : 7 columns -->
<table border=0 width=100% bgcolor=FFFFCC cellspacing=0 cellpadding=0>
<tr>
	<!-- ========== actions line ========== -->
	<td colspan=2 align="left" bgcolor="FFFFFF" valign="bottom">
	<a href="javascript:showsortfilter(1)" onclick="return IsSubmited();"><img border="0" src="../images/sort.gif" width="50" height="20"></a><img
					border="0" src="../images/white.gif" width="1" height="1"><a
					href="javascript:showsortfilter(2)" onclick="return IsSubmited();"><img border="0"
					src="../images/filter.gif" width="56" height="20"></a><img border="0"
					src="../images/white.gif" width="1" height="1"><jato:button name="btFilterSortReset" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/reset_sortfilter.gif" /><img
					border="0" src="../images/white.gif" width="1" height="1"></td>
	<td valign="bottom" align=left  bgcolor=FFFFFF colspan=3><font size=2><b>Sort:</b><jato:text name="stSortOptionDesc" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Filter:</b><jato:text name="stFilterOptionDesc" escape="true" /></font></td>
	<!-- ========== SCR#921 begins ========== -->
	<!-- by Neil on Feb/04/2005 -->
	<!-- Buf fixed: restored this line -->
	<td valign=bottom align=right width=30% nowrap bgcolor=FFFFFF colspan=2><font size=2><jato:text name="rowsDisplayed" escape="true" />&nbsp;&nbsp;</font></td></tr>
	<!-- ========== SCR#921 ends ========== -->
<!-- =========== data area ========== -->
<tr>
	<td bgcolor=#ffcc33 valign="middle" width="10%"><font size=2>&nbsp;</font></td>
	<td bgcolor=#FFCC33 valign="middle" width="15%"><font size=2><b>Source/Firm<br>Borrower Name</b></font></td>
	<td bgcolor=#FFCC33 valign="middle" width="10%"><font size=2><b>Deal Number</b></font></td>
	<!-- ========== SCR#750 begins ============================================= -->
	<!-- by Neil on Dec/16/2004 -->
	<!--
	<td bgcolor=#FFCC33 valign="bottom"><font size=2><b>Task<br>Description</b></font></td>
	<td bgcolor=#FFCC33 valign="bottom"><font size=2><b>Status</b></font></td>
	<td bgcolor=#FFCC33 valign="middle"><font size=2><b>Priority</b></font></td>
	-->
	<td bgcolor=#FFCC33 valign="middle" width="20%"><font size=2><b>Task Description /<br>Priority:</b></font></td>
	<td bgcolor=#FFCC33 valign="middle" width="20%"><font size=2><b>Task / Deal<br>Status:</b></font></td>
	<!-- ========== SCR#750 ends =============================================== -->
	<td bgcolor=#FFCC33 valign="middle" width="15%"><font size=2><b>Due Date/Time</b></font></td>
	<td bgcolor=#FFCC33 valign="middle" width="10%"><font size=2><b>Warning</b></font></td>
</tr>
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgIWorkQueueRepeated1TiledView">
<tr valign="top" height=5>
	<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"><jato:hidden name="hdDealId" /><jato:hidden name="hdWqID" /><jato:hidden name="hdTaskId" /></td>
</tr>
<tr valign="top">
	<!--99ccff-->
	<td bgcolor="<jato:text name="bgColor" escape="true" />" width="10%"><jato:button name="taskButton" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/open.gif" /><jato:button name="snapShotButton" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/snapshot_2.gif" /></td>
	<td bgcolor="<jato:text name="bgColor" escape="true" />" width="15%"><font size=2><jato:text name="stSource" escape="true" />/<jato:text name="stSourceFirm" escape="true" /><br><jato:text name="stBorrower" escape="true" /></font></td>
	<td bgcolor="<jato:text name="bgColor" escape="true" />" width="10%"><font size=2><jato:text name="stDealNumber" escape="true" formatType="decimal" formatMask="###0; (-#)" /><br>
	            <jato:text name="stInstitutionId" escape="true" /></font></td>
	<!-- ========== SCR#750 begins ============================================= -->
	<!-- by Neil on Dec/16/2004 -->
	<!--
	<td bgcolor=<jato:text name="bgColor" escape="true" />><font size=2><jato:text name="stTaskDescription" escape="true" />&nbsp;&nbsp;</font></td>
	<td bgcolor=<jato:text name="bgColor" escape="true" />><font size=2><jato:text name="stTaskStatus" escape="true" />&nbsp;&nbsp;</font></td>
	<td bgcolor=<jato:text name="bgColor" escape="true" />><font size=2><jato:text name="stTaskPriority" escape="true" />&nbsp;&nbsp;</font></td>
	-->
	<td bgcolor="<jato:text name="bgColor" escape="true" />" width="20%"><font size=2><jato:text name="stTaskDescription" escape="true" /><br><jato:text name="stTaskPriority" escape="true" /></font></td>
	<td bgcolor="<jato:text name="bgColor" escape="true" />" width="20%"><font size=2 style="<jato:text name="taskStatusOrHoldReasonStyle" escape="true" />"><jato:text name="stTaskStatus" escape="true" /></font><br><font size=2><jato:text name="stDealStatus" escape="true" /></font></td>
	<!-- ========== SCR#750 ends =============================================== -->
	<td bgcolor="<jato:text name="bgColor" escape="true" />" width="15%"><font size=2><jato:text name="stTaskDueDate" fireDisplayEvents="true" escape="true" formatType="date" formatMask="MMM dd yyyy / hh:mm" /></font></td>
	<td bgcolor="<jato:text name="bgColor" escape="true" />" width="10%"><font size=2><jato:text name="stTaskWarning" escape="true" /><jato:text name="stTimeStatus" escape="true" /></font></td>
</tr>
</jato:tiledView>
<tr>
	<td colspan=7 valign="bottom"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</tr>
</table>
<!-- ========== end table ========== -->
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous.gif" />&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2.gif" /></td>
</table>
<p>
<table border=0 width=100% bgcolor=FFFFCC cellspacing=0 cellpadding=0>

<td bgcolor=#FFCC33 colspan=3><font size=3><b>Outstanding Tasks By Priority:</b></font></td><tr>
<td bgcolor=#ffcc33 colspan=1><font size=2><br>&nbsp;</font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Priority:</b></font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Task Count:</b></font></td><tr>

<td colspan=3><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<jato:tiledView name="rpPirorityCount" type="mosApp.MosSystem.pgIWorkQueuerpPirorityCountTiledView">

<td colspan=1><font size=2>&nbsp;&nbsp;</font></td>
<td colspan=1><font size=2>&nbsp;&nbsp;<jato:text name="stPriorityLabel" escape="true" /></font></td>
<td colspan=1><font size=2>&nbsp;&nbsp;<jato:text name="stPriorityCount" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font></td><tr>

<td colspan=3><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
</jato:tiledView>
</table>

</center> </div>


<div id="prioritybody" class="prioritybody" name="prioritybody">
<center>
<p>
<table border=0 width=100%><td><font color=003366><b><font size="4">Priority Tasks Notice</font>&nbsp;
    <font size="2">--&nbsp; </font></b><font size="2">select '<b>OK</b>' to
    continue</font></font></td></table>
<!--BODY OF PAGE//-->
<table border=0 width=100% bgcolor=FFFFCC cellspacing=0 cellpadding=0>
<td bgcolor=#ffcc33 valign="middle"><font size=2><br>&nbsp;</font></td>
<td bgcolor=#FFCC33 valign="middle"><font size=2><b>Source/Firm<br>Borrower Name</b></font></td>
<td bgcolor=#FFCC33 valign="middle"><font size=2><b>Deal Number</b></font></td>
<td bgcolor=#FFCC33 valign="middle"><font size=2><b>Task Description</b></font></td>
<td bgcolor=#FFCC33 valign="middle"><font size=2><b>Status</b></font></td>
<td bgcolor=#FFCC33 valign="middle"><font size=2><b>Priority</b></font></td>
<td bgcolor=#FFCC33 valign="middle"><font size=2><b>Due Date/Time</b></font></td>
<td bgcolor=#FFCC33 valign="middle"><font size=2><b>Warning</b></font></td><tr>

<jato:tiledView name="Repeated2" type="mosApp.MosSystem.pgIWorkQueueRepeated2TiledView">

<!--99ccff-->
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"><jato:hidden name="hdBPDealId" /><jato:hidden name="hdBPWqID" /><jato:hidden name="hdBPTaskId" /></td><tr>
<td>&nbsp;</td>
<td ><font size=2><jato:text name="stBPSource" escape="true" />/<jato:text name="stBPSourceFirm" escape="true" />&nbsp;&nbsp;<br><jato:text name="stBPBorrower" escape="true" /> </font></td>
<td ><font size=2><jato:text name="stBPDealNumber" escape="true" formatType="decimal" formatMask="###0; (-#)" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stBPTaskDescription" escape="true" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stBPTaskStatus" escape="true" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stBPTaskPriority" escape="true" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stBPTaskDueDate" fireDisplayEvents="true" escape="true" formatType="date" formatMask="MMM dd yyyy hh:mm" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stBPTaskWarning" escape="true" />&nbsp;<jato:text name="stBPTimeStatus" escape="true" /></font></td><tr>

</jato:tiledView>

<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

</table>
<p>
<table border=0 width=100%><td align=right><a onclick="return IsSubmited();" href="javascript:tool_click(5)"><img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a></td></table>

</center><br><br><br><br><br>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>
<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<div id="sortpop" name="sortpop" class="sortpop">
<!-- <form name="sortform"> -->
<table border=0 cellpadding=3 cellspacing=0 bgcolor=99ccff>
<td colspan=2>
	<font size=2>Sort by:</font><br>
	<div name="hidden">
	<jato:combobox name="cbSortOptions" extraHtml="onChange='if(isSortByMeunDown) performSortBySubmit();' onclick='toggleSortByMenuDownFlag();' onkeypress='return handleSortByKeyPressed()' onBlur='isSortByMeunDown=false;'" />
	</div>
</td>
<!--
<td><jato:button name="btSortButton" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/sign_submit.gif" /></td>
-->
<td colspan=2 valign=middle bgcolor="5e84cf">
    <a onclick="return IsSubmited();" href="javascript:cancelsortfilter(1)">
    <img src="../images/close_arrow.gif" width=14 height=30 alt="" border="0"></a>
</td>
</table>
<!--form-->
</div>

<div id="filterpop" name="filterpop" class="filterpop">
<!-- <form name="filterform"> -->
<table border=0 cellpadding=3 cellspacing=0 bgcolor=99ccff>
<td colspan=2><font size=2>Filter by:</font><br>
<font size=2><div name="hidden"><jato:combobox name="cbFilterOptions" extraHtml="onChange='if(isFilterByMeunDown) performFilterBySubmit();' onclick='toggleFilterByMenuDownFlag();' onkeypress='return handleFilterByKeyPressed()' onBlur='isFilterByMeunDown=false;'" /></div></font><br>
</td>
<!--
<td><jato:button name="btFilterSubmitButton" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/sign_submit.gif" /></td>
-->
<td colspan=2 valign=middle bgcolor="5e84cf">
    <a onclick="return IsSubmited();" href="javascript:cancelsortfilter(2)">
    <img src="../images/close_arrow.gif" width=14 height=30 alt="" border="0"></a>
</td>
</table>
<!--form-->
</div>


<script language="javascript">
<!--
if(NTCP){
    document.dialogbar.left=55;
    document.dialogbar.top=79;
    document.toolpop.left=317;
    document.toolpop.top=79;
    document.pagebody.top=100;
    document.prioritybody.top=85;
    document.alertbody.top=100;
    document.sortpop.top=200;
    document.filterpop.top=200;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(4)
}
else{
if(document.forms[0].displayprioritytasks.value=="Y"){tool_click(6);}
else{tool_click(5);}
}

if(pmGenerate=="Y")
{
openPMWin();
}

//========== New method to handle auto refresh for SortBy and filter comboboxes ==============
var isSortByMeunDown = false;
function handleSortByKeyPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
//alert("The key = " + keycode);
  if (keycode == 13)
  {
    performSortBySubmit();
    return false;
  }

  return true;
}

function performSortBySubmit()
{
    setSubmitFlag(true);
    //document.forms[0].action="../mosApp/MosSystem/" + document.forms[0].name + ".btSortButton_onWebEvent(btGo)";
    document.forms[0].action += "?<%= viewBean.PAGE_NAME%>_btSortButton=";
    if(IsSubmitButton())
      document.forms[0].submit();
}

function toggleSortByMenuDownFlag()
{
    if(isSortByMeunDown )
        isSortByMeunDown = false;
    else
        isSortByMeunDown = true;
}

var isFilterByMeunDown = false;
function handleFilterByKeyPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
//alert("The key = " + keycode);
  if (keycode == 13)
  {
    performFilterBySubmit();
    return false;
  }

  return true;
}

function performFilterBySubmit()
{
    setSubmitFlag(true);
    //document.forms[0].action="../mosApp/MosSystem/" + document.forms[0].name + ".btFilterSubmitButton_onWebEvent(btGo)";
    document.forms[0].action += "?<%= viewBean.PAGE_NAME%>_btFilterSubmitButton=";
    if(IsSubmitButton())
      document.forms[0].submit();
}

function toggleFilterByMenuDownFlag()
{
    if(isFilterByMeunDown)
        isFilterByMeunDown = false;
    else
        isFilterByMeunDown = true;
}

// =========================================================================================


//-->
</script>

<!--END BODY//-->


</jato:form>
</BODY>
</jato:useViewBean>
</HTML>
