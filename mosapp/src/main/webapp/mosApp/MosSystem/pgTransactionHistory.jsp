
<HTML>
<%@page info="pgTransactionHistory" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgTransactionHistoryViewBean">

<HEAD>
<TITLE>Transaction History</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
.details{position:absolute; visibility:hidden; left:200; width:420;border-width:1pt;border-style: outset;border-color:"red"}

-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--
// =============  Local JavaScript =================//
function openDetailsBar(line)
{
	offset = "" + (125 + 37*line);
	closeDetailsBar();
	
	if (document.all)
	{
		eval("document.all.details" + line + ".style.top=" + offset);
		eval("document.all.details" + line + ".style.visibility=\'visible\'");
		 
	}
    if (document.layers)
	{
		 eval("document.details" + line + ".top=" + offset);
		 eval("document.details" + line + ".visibility=\'show\'");
	}
	
}

function closeDetailsBar()
{
	if (document.all)
	{
		if(document.all.details0)
		{
			if(	document.all.details0.style.visibility=VISIBLE )
				document.all.details0.style.visibility=HIDDEN;
		}
		else
			return;

		if(document.all.details1)
		{
			if(	document.all.details1.style.visibility=VISIBLE )	
				document.all.details1.style.visibility=HIDDEN;
		}
		else	return;

		if(document.all.details2)
		{
			if(	document.all.details2.style.visibility=VISIBLE )	
				document.all.details2.style.visibility=HIDDEN;
		}
		else
			return;

		if(document.all.details3)
		{
			if(	document.all.details3.style.visibility=VISIBLE )	
				document.all.details3.style.visibility=HIDDEN;
		}
		else
			return;

		if(document.all.details4)
		{
			if(	document.all.details4.style.visibility=VISIBLE )	
				document.all.details4.style.visibility=HIDDEN;
		}
		else
			return;
		
		if(document.all.details5)
		{
			if(	document.all.details5.style.visibility=VISIBLE )	
				document.all.details5.style.visibility=HIDDEN;
		}
		else
			return;

		if(document.all.details6)
		{
			if(	document.all.details6.style.visibility=VISIBLE )	
				document.all.details6.style.visibility=HIDDEN;
		}
		else
			return;

		if(document.all.details7)
		{
			if(	document.all.details7.style.visibility=VISIBLE )	
				document.all.details7.style.visibility=HIDDEN;
		}
		else
			return;

		if(document.all.details8)
		{
		if(	document.all.details8.style.visibility=VISIBLE )	
			document.all.details8.style.visibility=HIDDEN;
		}
		else
			return;		

		if(document.all.details9)
		{
		if(	document.all.details9.style.visibility=VISIBLE )	
			document.all.details9.style.visibility=HIDDEN;
		}
		else
			return;
			
	}
	
	if (document.layers)
	{
		if( document.details0 )
		{
			if( document.details0.visibility=VISIBLE )
				document.details0.visibility=HIDDEN;
		}
		else
			return;

		if( document.details1 )
		{
		if( document.details1.visibility=VISIBLE )
			document.details1.visibility=HIDDEN;
		}
		else
			return;

		if( document.details2 )
		{
			if( document.details2.visibility=VISIBLE )
				document.details2.visibility=HIDDEN;
		}
		else
			return;

		if( document.details3 )
		{
			if( document.details3.visibility=VISIBLE )
				document.details3.visibility=HIDDEN;
		}
		else
			return;

		if( document.details4 )
		{
			if( document.details4.visibility=VISIBLE )
				document.details4.visibility=HIDDEN;
		}
		else
			return;

		if( document.details5 )
		{
			if( document.details5.visibility=VISIBLE )
				document.details5.visibility=HIDDEN;
		}
		else
			return;

		if( document.details6 )
		{
			if( document.details6.visibility=VISIBLE )
				document.details6.visibility=HIDDEN;
		}
		else
			return;

		if( document.details7 )
		{
			if( document.details7.visibility=VISIBLE )
				document.details7.visibility=HIDDEN;
		}
		else
			return;

		if( document.details8 )
		{
			if( document.details8.visibility=VISIBLE )
				document.details8.visibility=HIDDEN;
		}
		else
			return;

		if( document.details9 )
		{
			if( document.details9.visibility=VISIBLE )
				document.details9.visibility=HIDDEN;
		}
		else
			return;
	}
}
//=================== End Local JavaScript Functions ===================//
-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgTransactionHistory" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
	<%@include file="/JavaScript/CommonHeader_en.txt" %>  

<%@include file="/JavaScript/CommonToolbar_en.txt" %>  

<!--Quick Link TOOLBAR//-->
<%@include file="/JavaScript/QuickLinkToolbar.txt" %>

<%@include file="/JavaScript/TaskNavigater.txt" %>    

<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>
<!--
<form>
-->
<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>

<tr>
    <TD colSpan=7><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>
<tr>
 	<TD colSpan=7><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
<tr>
	<td colspan=7 valign=top><font size=3 color=3366cc>&nbsp;&nbsp; &nbsp;&nbsp;<b>Transaction &nbsp;&nbsp; History: &nbsp;&nbsp; 
	Displaying &nbsp;&nbsp; Transaction &nbsp;&nbsp; History Records:&nbsp; <jato:text name="stBeginLine" escape="true" />&nbsp;-&nbsp;<jato:text name="stEndLine" escape="true" /> &nbsp;&nbsp;of &nbsp;&nbsp;[<jato:text name="stTotalLines" escape="true" />]</b></font></td>
<tr>
    	 <TD colSpan=8><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>
<TR>

<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgTransactionHistoryRepeated1TiledView">
   	<TD colSpan=8><IMG alt="" border=0 height=1 src="../images/blue_line.gif" width="100%"></TD>
<tr>
    <TD colSpan=7><IMG alt="" border=0 height=2 src="../images/light_blue.gif" width=1></TD>
<tr>  
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc><b>Date:</b></font></td>
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc><b>Field:</b></font></td>
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc><b>Context:</b></font></td>
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc><b>Previous Value:</b></font></td>	
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc><b>Current Value:</b></font></td>		
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 color=3366cc><b>Screen/Function:</b></font></td>		
	

<tr>
	<td ALIGN=LEFT valign=top><font size=2 ><jato:text name="stTransactionDate" fireDisplayEvents="true" escape="true" formatType="date" formatMask="MMM  dd  yyyy  hh:mm" /></font></td>
	<td ALIGN=LEFT valign=top><font size=2 ><jato:text name="stField" escape="true" /></font></td>
	
	
	
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 ><jato:text name="stContext" escape="true" /></font>
 <img src="../images/details.gif" alt="" border="0" align=right
		onClick = "openDetailsBar( <jato:text name="stLineNumber" escape="true" /> );" >  </td>



	<td colspan=1 ALIGN=LEFT valign=top><font size=2 ><jato:text name="stPreviousValue" escape="true" /></font></td>
	<td colspan=1 ALIGN=LEFT valign=top><font size=2 ><jato:text name="stCurrentValue" escape="true" /></font></td>
	
	
	
	<td><div id=<jato:text name="stDivId" escape="true" /> class="details" name=<jato:text name="stDivId" escape="true" /> > 
		<table border=0 width=100% bgcolor=99ccff cellpadding=0 cellspacing=0>
			<td><table border=0 width=90% bgcolor=99ccff cellpadding=0 cellspacing=0>
				<tr>
				<td colspan=1 align=left><font size=2 color=3366cc>&nbsp;&nbsp;<b>Context Source: </b> </font></td>
				<td colspan=1 align=left><font size=2 color=3366cc>&nbsp;&nbsp;<b>Entity:</b></font></td>
				<td colspan=1 align=left><font size=2 color=3366cc>&nbsp;&nbsp;<b>UserID:</b></font></td>
				<tr>
				<td colspan=1 align=left><font size=2 >&nbsp;&nbsp; <jato:text name="stContextSource" escape="true" /> </font></td>
				<td colspan=1 align=left><font size=2 >&nbsp;&nbsp; <jato:text name="stEntity" escape="true" /> </font></td>
				<td colspan=1 align=left><font size=2 >&nbsp;&nbsp; <jato:text name="stUser" escape="true" /> </font></td>
				</table>
			</td>
			<td align=right><img src= "../images/arrowb_left.gif" width=15  height=15 alt="" border="0" 
			onClick = "closeDetailsBar();" >  </td>
		</table>
		</div>
	</td>
<tr>

</jato:tiledView>
	
    <TD colSpan=7><IMG alt="" border=0 height=3 src="../images/dark_bl.gif" width="100%"></TD>
</TABLE>

<br>

<table border=0 width=100% cellpadding=0 cellspacing=0> <!--Forward and back buttons table-->
		<tr>
			<td align=right><jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous.gif" />&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2.gif" /></td>
</table>
<br>
 <table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" />&nbsp;&nbsp;</td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>