
<HTML>
<%@page info="pgDealSearch" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealSearchViewBean">

<HEAD>
<TITLE>Deal Search</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff onKeyPress="return submitenter(event);">
<jato:form name="pgDealSearch" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">


var enterKeyFlag = false;

function setFocus()
{
	enterKeyFlag = true;
}

function resetFocus()
{
	enterKeyFlag = false;
}

function submitenter(e)
{
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;
	
	if(keycode == 13 && enterKeyFlag)
	{
		if(IsSubmitButton())
		{
			setSubmitFlag(true);
			//alert("Before set action !!");
			document.pgDealSearch.action += "?<%= viewBean.PAGE_NAME%>_btSearch=";
			document.pgDealSearch.submit();
			//alert("After submit !!");
		}
		return false;
	}
	
	return true;	
}		

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();



</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<!--HEADER//-->
	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
    
    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
     
<!--END HEADER//-->

<div id="pagebody" class="pagebody" name="pagebody">
<center>

<!--BODY OF PAGE//-->
<p>
<center>


<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
</table>
<table border=0 width=100% cellpadding=3 cellspacing=0 bgcolor=d1ebff>
<td colspan=2 valign=top nowrap><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Search Criteria</b></font></td><tr>
<td colspan=2>&nbsp;</td><tr>

<td rowspan=9>&nbsp;&nbsp;</td>

<td><font size=2 color=3366cc><b>Deal Number:</b></font></td>
<td>
<jato:textField name="tbDealNumber" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="10" maxLength="10" />
</td>
<td><font size=2 color=3366cc><b>Servicing Number:</b></font></td>
<td>
<jato:textField name="tbServicingNum" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="20" maxLength="20" />
</td>
<tr>

<td><font size=2 color=3366cc><b>Borrower First Name:</b></font></td>
<td>
<jato:textField name="tbBorrowerFirstName" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="15" maxLength="15" /> <font size=2 color=3366cc><b>Last Name: </b></font>
<jato:textField name="tbBorrowerLastName" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="15" maxLength="15" /></td>
<td><font size=2 color=3366cc><b>Search for:</b></font></td>
<td><font size=2><jato:radioButtons name="rbPrimaryBorrowerOnly" /></font></td><tr>

<td><font size=2 color=3366cc><b>Borrower Mailing Address:</b></font></td>
<td colspan=3>
<jato:textField name="tbBorrowerMailingAddress" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="35" maxLength="35" /></td><tr>

<td><font size=2 color=3366cc><b>Borrower Home Phone #:</b></font></td>
<td>( 
<jato:textField name="tbBorrowerHomePhoneNumber1" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="3" maxLength="3" /> ) 
<jato:textField name="tbBorrowerHomePhoneNumber2" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="3" maxLength="3" /> - 
<jato:textField name="tbBorrowerHomePhoneNumber3" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="4" maxLength="4" /></td>
<td><font size=2 color=3366cc><b>Borrower Work Phone #:</b></font></td>
<td>( 
<jato:textField name="tbBorrowerWorkPhoneNumber1" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="3" maxLength="3" /> ) 
<jato:textField name="tbBorrowerWorkPhoneNumber2" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="3" maxLength="3" /> - 
<jato:textField name="tbBorrowerWorkPhoneNumber3" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="4" maxLength="4" /></td><tr>

<td><font size=2 color=3366cc><b>Property Street Number:</b></font></td>
<td>
<jato:textField name="tbPropertyStreetNumber" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="10" maxLength="10" /></td>
<td><font size=2 color=3366cc><b>Property Street Name:</b></font></td>
<td>
<jato:textField name="tbPropertyStreetName" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="35" maxLength="35" /></td><tr>

<td><font size=2 color=3366cc><b>Property City:</b></font></td>
<td colspan=3>
<jato:textField name="tbPropertyCity" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="20" maxLength="20" /></td><tr>

<td><font size=2 color=3366cc><b>Source Name:</b></font></td>
<td>
<jato:textField name="tbSourceName" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="20" maxLength="20" /></td>
<td><font size=2 color=3366cc><b>Source Application ID:</b></font></td>
<td>
<jato:textField name="tbSourceAppId" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="20" maxLength="20" /></td><tr>

<td><font size=2 color=3366cc><b>Underwriter:</b></font></td>
<td>
<jato:textField name="tbUnderwriter" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="35" maxLength="35" /></td>
<td><font size=2 color=3366cc><b>Administrator:</b></font></td>
<td>
<jato:textField name="tbAdministrator" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="35" maxLength="35" /></td><tr>

<td><font size=2 color=3366cc><b>&nbsp;</b></font></td>
<td>&nbsp;</td>
<td><font size=2 color=3366cc><b>Funder:</b></font></td>
<td>
<jato:textField name="tbFunder" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="35" maxLength="35" /></td>

</table>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=3><img src="../images/light_blue.gif" width=100% height=1></td>
</table>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><jato:button name="btSearch" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/submit.gif" />&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/sign_cancel.gif" /></td>
</table>

<!-- RESULTS //-->

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#d1ebff>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td bgcolor=#3366CC colspan=7>&nbsp;&nbsp;<font size=2 color=#ccffff><b>RESULTS</b>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="rowsDisplayed" escape="true" /></font></td><tr>


<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgDealSearchRepeated1TiledView">

<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=1>&nbsp;&nbsp;<jato:hidden name="hdRowNum" /></td>
<td><font size=2 color=#3366cc><b>Deal #</b></font></td>
<td><font size=2 color=#3366cc><b>Primary ?</b></font></td>
<td><font size="2" color="#3366cc"><b>Borrower Name</b></font></td>
<td nowrap><font size="2" color="#3366cc"><b>Borrower Home Phone #</b></font></td>
<td nowrap><font size="2" color="#3366cc"><b>Borrower Mailing Address</b></font></td>
<td><font size=2 color=#3366cc><b>Source / Source App ID / Servicing #</b></font></td><tr>

<td colspan=1><jato:button name="btSelect" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/dealsummary.gif" /> &nbsp; <jato:button name="btSnapShotDeal" src="../images/snapshot_2.gif" /></td>
<td><font size=2><jato:text name="stDealNumber" escape="true" /></font></td>
<td><font size=2><jato:text name="stPrimBorrower" escape="true" /></font></td>
<td><font size=2><jato:text name="stApplicantName" escape="true" /></font></td>
<td><font size=2><jato:text name="stApplicantHmTel" escape="true" /></font></td>
<td><font size=2><jato:text name="stApplicantAddr" escape="true" /></font></td>
<td><font size=2><jato:text name="stSourceName" escape="true" /> / <jato:text name="stSourceAppId" escape="true" /> / <jato:text name="stServicingNum" escape="true" /></td><tr>

<td colspan=1>&nbsp;&nbsp;</td>
<td colspan=1>&nbsp;</td>

<td colspan=1><font size=2 color=#3366cc><b>Status</b></font></td>
<td colspan=1><font size=2 color=#3366cc><b><jato:text name="stInstitutionLabel" escape="true" /></b></font></td>

<td colspan=1><font size="2" color="#3366cc"><b>Borrower Work Phone #</b></font></td>
<td colspan=1><font size=2 color=#3366cc><b>Property Address</b></font></td>
<td colspan=1><font size=2 color=3366cc><b>Underwriter/Administrator/Funder</b></font><tr>

<td colspan=1>&nbsp;&nbsp;</td>
<td colspan=1>&nbsp;</td>

<td colspan=1><font size=2><jato:text name="stDealStatus" escape="true" /></font></td>
<td colspan=1><font size=2><jato:text name="stInstitutionName" escape="true" /></font></td>

<td colspan=1><font size=2><jato:text name="stApplicantWorkTel" escape="true" /></font></td>
<td colspan=1><font size=2><jato:text name="stPropertyAddr" escape="true" /></font></td>
<td colspan=1><font size=2><jato:text name="stUWName" escape="true" />/<jato:text name="stAdminName" escape="true" />/<jato:text name="stFunderName" escape="true" /></font></td><tr>

<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

</jato:tiledView>

<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr>
<td align=right><jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2.gif" /></td>
</tr>

<tr>
<td align=right><br><img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();"></td>
</tr>

</table>

</center>
<br><br><br><br><br>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR><jato:hidden name="hdRowNum2" />


</jato:form>


<script language="javascript">
<!--
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>