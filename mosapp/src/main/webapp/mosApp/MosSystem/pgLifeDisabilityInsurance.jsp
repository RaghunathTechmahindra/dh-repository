
<HTML>
<%@page info="pgLifeDisabilityInsurance" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgLifeDisabilityInsuranceViewBean">

<!--
04/Jul/2006 DVG #DG456 #3510  CR313 - RTP routine - changes in Express
-->

<HEAD>
<TITLE>Mortgage Life and Disability Insurance Quotation</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>

<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcs.js" type="text/javascript"></script> 

<SCRIPT LANGUAGE="JavaScript">

//// IMPORTANT! The Tiled implementation of LifeCoverageRequested is not deleted (only commented)
//// because DJ doesn't signed the final specs yet and the current implementation doesn't cover
//// a couple of important cases. They can come back after the testing.
var lifeDisabilityNumOptions = 3;

//// Provides field control under all business cases.
function SetLifeDisabilityDisplay()
{
	var theInsProportionsId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbInsProportions.value");
	var hdInsProportionsId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdInsuranceProportionsId.value");	
	var theBtSubmitCtl = "document.forms[0].<%= viewBean.PAGE_NAME%>_btSubmit";
	////document.forms[0].action += "?" + PAGE_NAME + "_btRecalculate=" + theParameters;

	 	
////alert("HdIInsProportionsId: " + hdInsProportionsId);
////alert("theInsProportionsId: " + theInsProportionsId);
////alert("theBtSubmitCtl: " + theBtSubmitCtl);

	 ////var numOfBorrower = getNumOfRow("pgLifeDisabilityInsurance_Repeated1[0]_txLifePercCoverageReq");	 
	 ////var name1stPart = get1stPartName("pgLifeDisabilityInsurance_Repeated1[0]_txLifePercCoverageReq");
	 
	 var numOfBorrower = getNumOfRow("pgLifeDisabilityInsurance_Repeated1[0]_cbBorrowerGender");	 
	 var name1stPart = get1stPartName("pgLifeDisabilityInsurance_Repeated1[0]_cbBorrowerGender");	 	 
	 var theLifePercCoverageCtlDeal = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txLifePercCoverageReqReq");
	 
////alert("NumOfBorrower: " + numOfBorrower);
////alert("Name1stPart: " + name1stPart);

	//// Temporarily. Until HomeBASE increase their capacity to ten users 
	//// an per specs. Also add the precise logic on submit via BRules after.
	if(numOfBorrower <= 4) 
	{	
		for( x=0; x < numOfBorrower; ++x )
		{
		    inputRowNdx = x + "]_";
////alert("InputRowNdx: " + inputRowNdx);

		    ////var theLifePercCoverageCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "txLifePercCoverageReq" + "']");
		    var theGenderCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbBorrowerGender" + "']");
		    		    
////alert("theLifePercCoverageCtl: " + theLifePercCoverageCtl);
////alert("theLifePercCoverageName: " + theLifePercCoverageCtl.name);
		    if(hdInsProportionsId == 0) //// blank, default
		    {	
			    ////theLifePercCoverageCtl.disabled = true;
			    theLifePercCoverageCtlDeal.disabled = true;
                    	    theGenderCtl.disabled = true;
                    	    ////theBtSubmitCtl.disabled = true;
                    }  	    
		    else if(hdInsProportionsId == 1) //// 'Home Owner Plus'
		    {
			    ////theLifePercCoverageCtl.value = 100;
			    ////theLifePercCoverageCtl.disabled = true;

			    theLifePercCoverageCtlDeal.value = 100;
			    theLifePercCoverageCtlDeal.disabled = true;

                    	    theGenderCtl.disabled = false;
                    	    ////theBtSubmitCtl.disabled = true;
		    }
		    else if(hdInsProportionsId == 2) //// 'Total'
		    {	
			    ////theLifePercCoverageCtl.value = 100;
			    ////theLifePercCoverageCtl.disabled = true;
 
			    theLifePercCoverageCtlDeal.value = 100;
			    theLifePercCoverageCtlDeal.disabled = true;
			    
                    	    theGenderCtl.disabled = false;
                    	    ////theBtSubmitCtl.disabled = true;
		    }
		    else if(hdInsProportionsId == 3) //// 'Regular'
		    {	
			    ////theLifePercCoverageCtl.disabled = false;
                    	    theGenderCtl.disabled = false;
                    	    ////theBtSubmitCtl.disabled = true;

                    	                        	            
                    	    /**
                    	    (theLifePercCoverageCtlDeal.value == 100 ||
                    	     theLifePercCoverageCtlDeal.value == 0)
                    	    { 
                    	       theLifePercCoverageCtlDeal.value = 0;
                    	    }
                    	    **/
                    	    
			    theLifePercCoverageCtlDeal.disabled = false;			    
		    }		 
		    else if(hdInsProportionsId == 4) //// 'Declined'
		    {	
			    ////theLifePercCoverageCtl.disabled = true;
                    	    theGenderCtl.disabled = false;
                    	    
			    theLifePercCoverageCtlDeal.value = 0;                    	    
			    theLifePercCoverageCtlDeal.disabled = true;
			    ////theBtSubmitCtl.disabled = true;
		    }
		    		    
		    for (j=0; j < lifeDisabilityNumOptions; ++j)
		    {
			    var theLifeInsRadioCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbLifeInsurance" + "']" + "[j]");
			    var theDisabilityInsRadioCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbDisabilityInsurance" + "']" + "[j]");

////alert("theLifeInsRadioCtl[" + j + "]: " + theLifeInsRadioCtl);		    		    
////alert("theLifeInsRadioValue[" + j + "]: " + theLifeInsRadioCtl.value);
////alert("theLifeInsRadioCtl[" + j + "]Checked?: " + theLifeInsRadioCtl.checked);		    		    
////alert("theLifeInsRadioCtl[" + j + "]DefaultChecked?: " + theLifeInsRadioCtl.defaultChecked);		    		    
////alert("theLifeInsRadioCtl[" + j + "]TabIndex: " + theLifeInsRadioCtl.tabIndex);		    		    
////alert("theDisabilityInsRadioValue[" + j + "]Checked?: " + theDisabilityInsRadioCtl.checked + " Index: " + j);


			    if(hdInsProportionsId == 0) //// blank, default
			    {	
				    theLifeInsRadioCtl.checked = false;
				    theDisabilityInsRadioCtl.checked = false;
				    theLifeInsRadioCtl.disabled = true;
				    theDisabilityInsRadioCtl.disabled = true;
			    }  	    
			    else if(hdInsProportionsId == 1) //// 'Home Owner Plus'
			    {
				    theLifeInsRadioCtl.disabled = false;
				    theDisabilityInsRadioCtl.disabled = false;		   
			    }
			    else if(hdInsProportionsId == 2) //// 'Total'
			    {	
				    theLifeInsRadioCtl.disabled = false;
				    theDisabilityInsRadioCtl.disabled = false;
				    
				    if(theLifeInsRadioCtl.checked == true && j == 1) // 'Refused' Radio Button value should be set.
				    {					    
					    eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbDisabilityInsurance" + "']" + "[j]").checked = true;				    
			            }		    				   
				    
			    }
			    else if(hdInsProportionsId == 3) //// 'Regular'
			    {	
				    theLifeInsRadioCtl.disabled = false;
				    theDisabilityInsRadioCtl.disabled = false;
				    
				    if(theLifeInsRadioCtl.checked == true && j == 1) // 'Refused' Radio Button value should be set.
				    {
////alert("theLifeInsRadioCtl[" + j + "]Checked?: " + theLifeInsRadioCtl.checked);		    		    
////alert("theLifeInsRadioCtl[" + j + "]DefaultChecked?: " + theLifeInsRadioCtl.defaultChecked);
////alert("InputRowNdxRefused: " + x);
////alert("IndexOfCheckedRadioButton: " + j);
					    
					    eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbDisabilityInsurance" + "']" + "[j]").checked = true;				    
			             }		    
				    
			    }
			    else if(hdInsProportionsId == 4) //// 'Declined'
			    {	
				    theLifeInsRadioCtl.disabled = false;
				    theDisabilityInsRadioCtl.disabled = false;
				    				    
				    if(j == 1) // 'Refused' Radio Button value should be set.
				    {
					  eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbDisabilityInsurance" + "']" + "[j]").checked = true;				    
					  eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbLifeInsurance" + "']" + "[j]").checked = true;				    					   
			            }
			    }			    
		      }
		}
	}        
}

//// Provides field control under all business cases.
function SetLifeDisDisplayInsureOnlyApplicant()
{
	var theInsProportionsId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbInsProportions.value");
	var hdInsProportionsId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdInsuranceProportionsId.value");	
	////document.forms[0].action += "?" + PAGE_NAME + "_btRecalculate=" + theParameters;

	 	
//alert("HdIInsProportionsId: " + hdInsProportionsId);
//alert("theInsProportionsId: " + theInsProportionsId);

	 var numOfInsureOnlyAppl = getNumOfRow("pgLifeDisabilityInsurance_Repeated2[0]_cbInsureOnlyApplicantGender");	 
	 var name1stPart = get1stPartName("pgLifeDisabilityInsurance_Repeated2[0]_cbInsureOnlyApplicantGender");	 	 
	 var theLifePercCoverageCtlDeal = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txLifePercCoverageReqReq");
	 
//alert("NumOfInsureOnlyAppl: " + numOfInsureOnlyAppl);
//alert("Name1stPart: " + name1stPart);

	//// Very Temporarily. Until HomeBASE increase their capacity to ten users 
	//// an per specs. Also add the precise logic on submit via BRules after.
	if(numOfInsureOnlyAppl <= 4 && numOfInsureOnlyAppl > 0) 
	{	
		for( x=0; x < numOfInsureOnlyAppl; ++x )
		{
		    inputRowNdx = x + "]_";
//alert("InputRowNdx: " + inputRowNdx);

		    var theGenderCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "cbInsureOnlyApplicantGender" + "']");
		    		    
		    if(hdInsProportionsId == 0) //// blank, default
		    {	
			    theLifePercCoverageCtlDeal.disabled = true;
                    	    theGenderCtl.disabled = true;
                    }  	    
		    else if(hdInsProportionsId == 1) //// 'Home Owner Plus'
		    {
			    theLifePercCoverageCtlDeal.value = 100;
			    theLifePercCoverageCtlDeal.disabled = true;

                    	    theGenderCtl.disabled = false;
		    }
		    else if(hdInsProportionsId == 2) //// 'Total'
		    {	
			    theLifePercCoverageCtlDeal.value = 100;
			    theLifePercCoverageCtlDeal.disabled = true;
			    
                    	    theGenderCtl.disabled = false;
		    }
		    else if(hdInsProportionsId == 3) //// 'Regular'
		    {	
                    	    theGenderCtl.disabled = false;                    	    
			    theLifePercCoverageCtlDeal.disabled = false;			    
		    }		 
		    else if(hdInsProportionsId == 4) //// 'Declined'
		    {	
                    	    theGenderCtl.disabled = false;
                    	    
			    theLifePercCoverageCtlDeal.value = 0;                    	    
			    theLifePercCoverageCtlDeal.disabled = true;
		    }
		    		    
		    for (j=0; j < lifeDisabilityNumOptions; ++j)
		    {
			    var theLifeInsRadioCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbLifeInsurance" + "']" + "[j]");
			    var theDisabilityInsRadioCtl = eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbDisabilityInsurance" + "']" + "[j]");

////alert("theLifeInsRadioCtl[" + j + "]: " + theLifeInsRadioCtl);		    		    
////alert("theLifeInsRadioValue[" + j + "]: " + theLifeInsRadioCtl.value);
////alert("theLifeInsRadioCtl[" + j + "]Checked?: " + theLifeInsRadioCtl.checked);		    		    
////alert("theLifeInsRadioCtl[" + j + "]DefaultChecked?: " + theLifeInsRadioCtl.defaultChecked);		    		    
////alert("theLifeInsRadioCtl[" + j + "]TabIndex: " + theLifeInsRadioCtl.tabIndex);		    		    
////alert("theDisabilityInsRadioValue[" + j + "]Checked?: " + theDisabilityInsRadioCtl.checked + " Index: " + j);


			    if(hdInsProportionsId == 0) //// blank, default
			    {	
				    theLifeInsRadioCtl.checked = false;
				    theDisabilityInsRadioCtl.checked = false;
				    theLifeInsRadioCtl.disabled = true;
				    theDisabilityInsRadioCtl.disabled = true;
			    }  	    
			    else if(hdInsProportionsId == 1) //// 'Home Owner Plus'
			    {
				    theLifeInsRadioCtl.disabled = false;
				    theDisabilityInsRadioCtl.disabled = false;		   
			    }
			    else if(hdInsProportionsId == 2) //// 'Total'
			    {	
				    theLifeInsRadioCtl.disabled = false;
				    theDisabilityInsRadioCtl.disabled = false;
				    
				    if(theLifeInsRadioCtl.checked == true && j == 1) // 'Refused' Radio Button value should be set.
				    {					    
					    eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbDisabilityInsurance" + "']" + "[j]").checked = true;				    
			            }		    				   
				    
			    }
			    else if(hdInsProportionsId == 3) //// 'Regular'
			    {	
				    theLifeInsRadioCtl.disabled = false;
				    theDisabilityInsRadioCtl.disabled = false;
				    
				    if(theLifeInsRadioCtl.checked == true && j == 1) // 'Refused' Radio Button value should be set.
				    {
////alert("theLifeInsRadioCtl[" + j + "]Checked?: " + theLifeInsRadioCtl.checked);		    		    
////alert("theLifeInsRadioCtl[" + j + "]DefaultChecked?: " + theLifeInsRadioCtl.defaultChecked);
////alert("InputRowNdxRefused: " + x);
////alert("IndexOfCheckedRadioButton: " + j);
					    
					    eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbDisabilityInsurance" + "']" + "[j]").checked = true;				    
			             }		    
				    
			    }
			    else if(hdInsProportionsId == 4) //// 'Declined'
			    {	
				    theLifeInsRadioCtl.disabled = false;
				    theDisabilityInsRadioCtl.disabled = false;
				    				    
				    if(j == 1) // 'Refused' Radio Button value should be set.
				    {
					  eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbDisabilityInsurance" + "']" + "[j]").checked = true;				    
					  eval("document.forms[0].elements['" + name1stPart + inputRowNdx + "rbLifeInsurance" + "']" + "[j]").checked = true;				    					   
			            }
			    }			    
		      }
		}
	}        
}

 
function initPage(event)
{
	// Set MI Premium Display
	SetLifeDisabilityDisplay();
	SetLifeDisDisplayInsureOnlyApplicant();	
}


</SCRIPT>
</head>

<body bgcolor=ffffff link=000000 vlink=000000 alink=000000 onload="initPage(event)"; onClick="initPage(event);">

<jato:form name="pgLifeDisabilityInsurance" method="post" onSubmit="return IsSubmitButton();">
<p>

<jato:hidden name="sessionUserId" />

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<jato:hidden name="hdInsuranceProportionsId" />
<jato:hidden name="hdInputToRTPCalc" />
<jato:hidden name="hdInputToInfocalCalc" />
<jato:hidden name="hdOutputFromRTPInfocalCalc" />

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

	<!--END HEADER//-->

	<!--DEAL SUMMARY SNAPSHOT//-->
	<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>

	<!--END DEAL SUMMARY SNAPSHOT//-->

<!--BODY OF PAGE//-->

<P>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>
	<td colspan=5><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
<tr>
	<td colspan=5><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
<tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Insurance Options:</b></font></td>
<td valign=top><jato:combobox name="cbInsProportions" /></td>
<td valign=top><font size=2 color=3366cc><b>Life Coverage Requested:</b></font></td>
<td valign=top><jato:textField name="txLifePercCoverageReqReq" size="5" maxLength="5" />%</td><tr>

<tr>
	<td colspan=5><img src="../images/light_blue.gif" width=100% height=4 alt="" border="0"></td>
<tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Principal & Interest Payment:</b></font></td>
<td valign=top><font size=2>$ <jato:text name="stUWPIPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></td>
<td valign=top><font size=2 color=3366cc><b>Mortgage Amount:</b></font></td>
<td valign=top><font size=2>$ <jato:text name="stMortgageInsuranceAmount" escape="true" formatType="currency" formatMask="#,##0.00; (-#)"/></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Life Insurance Premium:</b></font></td>
<td valign=top><font size=2>$ <font size=2><jato:text name="stLifePremium" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></td>
<td valign=top><font size=2 color=3366cc><b>Payment Frequency:</b></font></td>
<td valign=top><font size=2><jato:text name="stPaymentFrequency" escape="true" /><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Disability Insurance Premium:</b></font></td>
<td valign=top><font size=2>$ <jato:text name="stDisabilityPremium" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Int. Rate Before Life/Disability:</b></font></td>
<td valign=top><font size=2><jato:text name="stUWNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</font><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Combined Premium:</b></font></td>
<td valign=top><font size=2>$ <jato:text name="stCombinedLDPremium" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Add. Rate for Life/Disability:</b></font></td>
<td valign=top><font size=2><jato:text name="stAddRateForLifeDisability" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</font><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Total Payment Including Life/Disability:</b></font></td>
<td valign=top><font size=2>$ <jato:text name="stPmntInclLifeDis" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Total Rate incl. Life/Disability:</b></font></td>
<td valign=top><font size=2><jato:text name="stTotalRateInclLifeDis" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</font><tr>
<tr>
	<td colspan=5><img src="../images/light_blue.gif" width=100% height=4 alt="" border="0"></td>
<tr>
	<td colspan=5><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
</table>

<p>

<!--Borrower Life Insurance table-->
 <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
	<td bgcolor=3366cc colspan=75>&nbsp;&nbsp;<font size=2 color=#ccffff>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stRowsDisplayed" escape="true" /></font></td>
  <tr>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;<font size=2 color=3366cc><b>Enrolled Person:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Primary<br>Borrower:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Smoker:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>		
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Gender:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Age:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Life:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Disability:</b></font></td>
  </tr>
	<td colspan=75><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
	<td colspan=75><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<td colspan=75><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
  
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgLifeDisabilityInsuranceRepeated1TiledView">
<tr><jato:hidden name="hdBorrowerId" /></tr>
  <tr>	
	<td colspan=5 NOWRAP>&nbsp;&nbsp;<font size=2><jato:text name="stBorrowerFirstName" escape="true" />&nbsp;<jato:text name="stBorrowerMiddleInitial" escape="true" />&nbsp;<jato:text name="stBorrowerLastName" escape="true" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2><jato:text name="stPrimaryBorrowerFlag" escape="true" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2><jato:combobox name="cbBorrowerSmoker" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
	<td colspan=5 NOWRAP><font size=2><jato:combobox name="cbBorrowerGender" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2><jato:text name="stBorrowerAge" escape="true" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size="2"><jato:radioButtons name="rbLifeInsurance" layout="vertical" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
	<td colspan=5 NOWRAP><font size="2"><jato:radioButtons name="rbDisabilityInsurance" layout="vertical" /></font></td>
  </tr>
	<tr><td colspan=75><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>	
	<tr><td colspan=75><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
	<tr><td colspan=75><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>  	
</jato:tiledView>
<!--END REPEATING ROW//-->

<td colspan=105><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td><tr>
</table>


<!--Insure Only Applicant table-->
 <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
	<td bgcolor=3366cc colspan=90>&nbsp;&nbsp;<font size=2 color=#ccffff>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stRowsDisplayedInsureOnlyAppl" escape="true" /></font></td>
  <tr>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;<font size=2 color=3366cc><b>First Name:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;<font size=2 color=3366cc><b>Last Name:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Primary<br>Borrower:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Smoker:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>		
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Gender:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Date of Birth:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Age:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Life:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Disability:</b></font></td>
  </tr>
	<td colspan=90><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
	<td colspan=90><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<td colspan=90><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
  
<jato:tiledView name="Repeated2" type="mosApp.MosSystem.pgLifeDisabilityInsuranceRepeated2TiledView">
<tr><jato:hidden name="hdInsureOnlyApplicantId" /></tr>
<tr><jato:hidden name="hdCopyId" /></tr>
  <tr>	
	<td colspan=5 NOWRAP>&nbsp;&nbsp;<font size=2><jato:textField name="txInsureOnlyApplicantFirstName" size="13" maxLength="13" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;<font size=2><jato:textField name="txInsureOnlyApplicantLastName" size="20" maxLength="20" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
	<td colspan=5 NOWRAP><font size=2><jato:text name="stPrimaryInsureOnlyApplicantFlag" escape="true" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2><jato:combobox name="cbInsureOnlyApplicantSmoker" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
	<td colspan=5 NOWRAP><font size=2><jato:combobox name="cbInsureOnlyApplicantGender" /></font></td>
        <td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	
<td colspan=5 NOWRAP><font size=2>Mth: </font><jato:combobox name="cbIOApplicantDOBMonth" /> <font size=2>Day: </font>
<jato:textField name="txIOApplicantDOBDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Yr: </font>
<jato:textField name="txIOApplicantDOBYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></td>
	
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2><jato:text name="stInsureOnlyApplicantAge" escape="true" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
	<td colspan=5 NOWRAP><font size="2"><jato:radioButtons name="rbLifeInsurance" layout="vertical" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
	<td colspan=5 NOWRAP><font size="2"><jato:radioButtons name="rbDisabilityInsurance" layout="vertical" /></font></td>
        <td colspan=5><jato:button name="btDeleteApplicant" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/del_spouse.gif" /></td>
   </tr>
	<tr><td colspan=90><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>	
	<tr><td colspan=90><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<tr><td colspan=90><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>  	
</jato:tiledView>
<!--END REPEATING ROW//-->

<td colspan=105><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
</table>

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddInsureOnlyApplicant" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_spouse.gif" /></td>
</tr>

<p>
<!-- #DG456
object classid="CLSID:15530FD8-38BD-4166-88AE-4A73A9A82DFF"
	id="djCalcObj"
	codebase="/Mos/DJCalc_v103.CAB#version=1,0,0,3">
</object-->
<OBJECT ID="djCalcObj"
CLASSID="CLSID:8DB27789-0EF0-4D4E-91E8-D3C3D2C823FB"
CODEBASE="/Mos/DJCalc_v104.CAB#version=1,0,0,4">
</OBJECT>
</p>

<SCRIPT LANGUAGE="JavaScript">
//// Set of functions providing the communication with the HomeBASE implementation of RTP/Infocal DLLs DJ 
//// calc engine.
function doDLLCalc()
{
////alert("The result : " + document.djCalcObj);
	
	var hdInputToRTPCalcInfo = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdInputToRTPCalc.value");
	var hdInputToInfocalCalcInfo = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdInputToInfocalCalc.value");


///alert("HdStringToRTP: " + hdInputToRTPCalcInfo);
///alert("HdStringToInfocal: " + hdInputToInfocalCalcInfo);

	var infocal_str = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdInputToInfocalCalc.value");
////alert("The infocal_str : " + infocal_str);
	var rtp_str = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdInputToRTPCalc.value");

////alert("The rtp_str : " + rtp_str);
        
        ////var hdOutputFromRTPInfocalCalcInfo = "";
        
	if(infocal_str != "" && rtp_str != "")
	{
		document.forms[0].<%= viewBean.PAGE_NAME%>_hdOutputFromRTPInfocalCalc.value = document.djCalcObj.Calc(rtp_str, infocal_str);
	
///alert("The result : " + document.djCalcObj.Calc(rtp_str, infocal_str));        	
        }
        
	else if(infocal_str == "" && rtp_str == "")
	{
		document.forms[0].<%= viewBean.PAGE_NAME%>_hdOutputFromRTPInfocalCalc.value = "";
	
////alert("The result : " + hdOutputFromRTPInfocalCalcInfo);        	
        }

	var hdOutputFromRTPInfocalCalcInfo = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdOutputFromRTPInfocalCalc.value");
	        
         
////alert("ResultToXpress: " + hdOutputFromRTPInfocalCalcInfo);

}

/**
function invokeActiveXControl(id)
{
  if(document.all) {
  	document.all["djCalcObj"].doDLLCalc();
}
**/

doDLLCalc();

var timer;

timer = setTimeout("performBtRecalcSubmit('OK',61)", 1000);
window.clearTimeout(timer);


function performBtRecalcSubmit(theParameters)
{
	setSubmitFlag(true);
	document.forms[0].action += "?" + PAGE_NAME + "_btRecalculate=" + theParameters;

///alert("document.forms[0].action= " + document.forms[0].action);
  // check if any outstanding request
  if(IsSubmitButton())
  {
	 document.forms[0].submit();
	 setSubmitFlag(false);
  } 
////alert("After Submit");	 
}

</SCRIPT>


<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<td align=right><jato:button name="btRecalculate" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/recalculate.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btSubmit" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel.gif" />&nbsp;&nbsp;<jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>

</center>
<br><br><br><br><br>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<!--
</jato:form>
-->

<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
	openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
