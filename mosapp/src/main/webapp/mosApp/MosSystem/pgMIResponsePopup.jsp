<%--
5.0 MI, Aug 10, 2011, Hiro
 --%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgMIResponsePopupViewBean">
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Strict //EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html>
		<head>
			<meta http-equiv="Content-Type"
				content="text/html; charset=ISO-8859-1">
			<title>Mortgage Insurance Response</title>
			<style type="text/css">
				* { margin: 0; padding: 0; }  
				html,body { height: 100%; }  
				img { border-style: none; }  
				body,textarea { background-color: #d1ebff; }  
				textarea { padding: 5px; border-color: black; }  
				p { white-space: nowrap; color: #3366cc; font-weight: bold; }  
				#title { text-align: center; font-size: x-large; padding-top: 15px; }  
				#subtitle { padding-top: 30px; padding-left: 10px; padding-bottom: 0px; font-size: large; }  
				#miresponse { padding-top: 3px; padding-left: 10px; }  
				#close { margin-left: auto; margin-left: auto; text-align: center; margin-top: 30px; }
			</style>
		</head>
		<body>
			<p id="title"> Mortgage Insurance Response </p>
			<p id="subtitle"> The following MI Response applies to Deal Number: <jato:text name="stDealId" escape="true" /> </p>
			<div id="miresponse"> <jato:textArea rows="30" cols="90" name="txMIResponse" readOnly="true" /> </div>
			<div id="close">
				<a href="javascript:self.close();"><img src="../images/close.gif" /></a>
			</div>
		</body>
	</html>
</jato:useViewBean>