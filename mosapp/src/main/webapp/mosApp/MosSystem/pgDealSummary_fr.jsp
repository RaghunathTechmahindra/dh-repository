<%--
 /**
 *Title: pgDealSummary_fr.jsp
 *Description:Pagelet to display Deal Summary in French language.
 *@author:MCM Impl Team
 *@version 1.0 Existing Version
 *@version 1.1 02-Aug-2008 XS_16.21
 *           - Added pagelet for Qualifying Detail section
 			 - Incorporated FSD changes for Additional Principal Payment & 
 			 	Prepayment Option 	 
 			 - Added Existing Account Reference, Product type & Additional Details in Refinance Info section.
 			 - Added Component Information section
 *@version 1.2 14-Aug-2008 XS_16.21 Incorporated review comments -  updated the formatmask for currency with French support
 *@version 1.3 18-Aug-2008 XS_16.21 Incorporated review comments -  updated the formatmask for decimal with French support for stCashBackInPercentage 
 *@version 1.4 Bugfix: FXP22927  - Correct display format for netrate,premium,discount and added new field for component loan section to display payment amount.
 *@version 1.5 MCM, FXP23175, Oct 31, 08, changed div tag(pagebody)'s position.
 */
--%>
<HTML>
<%@page info="pgDealSummary" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealSummaryViewBean">

<HEAD>
<TITLE>R�capitulatif de la demande</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?215:190); width: 100%;overflow: hidden;}
.dealsumborr {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsumasst {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsuminc {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsumliab {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsumprop {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.gdstds {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsumpropexp {position: absolute; visibility: hidden; top: expression((QLE)?315:190); left: 10; width:100%; right-margin:5%;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>


<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellForDealSummary.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script> 

</HEAD>

<body bgcolor=ffffff>
<jato:form name="pgDealSummary" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
	<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
	<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	
	<!--TOOLBAR//-->
 	<%@include file="/JavaScript/CommonToolbar_fr.txt" %>
 	
 	    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
    
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %>


<!--DEAL SUMMARY SNAPSHOT//-->
	<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %><br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<%-- FXP23175, MCM, Oct 31, 08, changed position --%>
<div id="pagebody" class="pagebody" name="pagebody">

<!-- START GCD Summary Section -->
<jato:text name="stIncludeGCDSumStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td></tr>
<tr>
	<td valign=top colspan=1>&nbsp;</td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Statut D�cision de cr�dit:</b></font><br><font size=2><jato:text name="txCreditDecisionStatus"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>D�cision de cr�dit:</b></font><br><font size=2><jato:text name="txCreditDecision"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Score bureau cr�dit:</b></font><br><font size=2><jato:text name="txGlobalCreditBureauScore"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Cote d'octroi:</b></font><br><font size=2><jato:text name="txGlobalRiskRating"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Score carte:</b></font><br><font size=2><jato:text name="txGlobalInternalScore"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>D�cision assureur-pr�t:</b></font><br><font size=2><jato:text name="stMIRequestStatus"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc></font>&nbsp<br></td>

</tr>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>
  </table>
<jato:text name="stIncludeGCDSumEnd" escape="false" /><br>
<!-- End GCD Summary Section -->

<!-- Start Of Summary Buttons-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
<tr><td  align=left valign=middle colspan=6>
	&nbsp;&nbsp;<jato:button name="btPrintDealSummaryLite" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/printdeal_fr.gif" />
	&nbsp;&nbsp;
	<!-- SEAN Ticket #1681 June 27, 2005: Hide the print deal summary bt for DJ. -->
	<jato:text name="stBeginHidePrintDealSummary" fireDisplayEvents="true" escape="true" />
	<jato:button name="btPrintDealSummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/DealSummaryDetails_fr.gif" />
	<jato:text name="stEndHidePrintDealSummary" fireDisplayEvents="true" escape="true" />
	<!-- SEAN Ticket #1681 END -->
	&nbsp;&nbsp;<jato:button name="btPartySummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/party_summary_fr.gif" />
      &nbsp;&nbsp;<jato:button name="btDeclineLetter" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/printdeclineletter_fr.gif" />
	</td>
	<td  align=right valign=middle colspan=2>
       <!--  #1676 - Hide Reverse funding if user doesn't have rights, Catherine, 2-Sep-05 begin -->
       <jato:text name="stHideReverseBtnStart" escape="false"/>
        <jato:button name="btReverseFunding" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/reverse_funding.gif" fireDisplayEvents="true" />
       <jato:text name="stHideReverseBtnEnd" escape="false"/>
       <!--  #1676  ======================================================================= end -->
	</td>

</tr>
</table>
<!-- End Of Summary Buttons-->

<!-- Start Of Application Information-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>	
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<font color=3366cc><b>Information sur la demande:</b></font>
			<a onclick="return IsSubmited();" href="javascript:viewSub('borr')">
				<img src="../images/applicant_details_fr.gif" width=153 height=16 alt="" border="0" hspace=0 vspace=0>
			</a> 
		</td>
	</tr>

	<tr>
		<td valign=top >
			<font size=2 color=3366cc><b>Emprunteur principal:</b></font><br>
			<font size=2> <jato:text name="stFirstName" escape="false" /> <jato:text name="stMiddleInitial" escape="true" />  <jato:text name="stLastName" escape="true" />  <jato:text name="stSuffix" escape="true" /></font> 
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Langue d�sir�e:</b></font><br>
			<font size=2><jato:text name="stLanguagePreference" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Client existant?</b></font><br>
			<font size=2><jato:text name="stExistingClient" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>N<sup>o</sup> de r�f�rence du client:</b></font><br>
			<font size=2><jato:text name="stReferenceClientNumber" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Souscripteur/Administrateur/Pr�pos� � l'avance des fonds:</b></font><br>
			<font size=2><jato:text name="stUnderwriter" fireDisplayEvents="true" escape="true" formatType="string" formatMask="????????????????????????????????????????" />/<jato:text name="stAdministrator" fireDisplayEvents="true" escape="true" formatType="string" formatMask="????????????????????????????????????????" />/<jato:text name="stFunder" fireDisplayEvents="true" escape="true" formatType="string" formatMask="????????????????????????????????????????" /></font>
		</td>
	</tr>
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>



	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone maison:</b></font><br>
			<font size=2><jato:text name="stHomePhone" escape="true" formatType="string" formatMask="(???)???-????" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone bureau:</b></font><br>
			<font size=2><jato:text name="stWorkPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stBorrowerWorkPhoneExt" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top >
			<font size=2 color=3366cc><b>Nbre d'emprunteurs:</b></font><br>
			<font size=2><jato:text name="stNoOfBorrowers" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font> 
		</td>
		<td valign=top >
			<font size=2 color=3366cc><b>Nbre d'endosseurs:</b></font><br>
			<font size=2><jato:text name="stNoOfGuarantors" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font> 
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Succursale:</b></font><br>
			<font size=2><jato:text name="stBranch" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
</table>
<!-- End Of Application Information-->

<!--MCM Impl team changes starts - XS_16.21 -- Qualifying Details -->
    
	<jato:containerView name="qualifyingDetailsPagelet" type="mosApp.MosSystem.pgQualifyingDetailsPageletView">
		<jsp:include page="pgQualifyingDetailsPageletSummary_fr.jsp"/>
	</jato:containerView>
	 
<!--MCM Impl team changes ends -  XS_16.21-->


<!-- Start Of Deal Information-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
	<tr>
		<td colspan=8>
			<font color=3366cc><b>Information sur la demande:</b></font>
		</td>
	</tr>
	
	<%-- XS_16.21 August 01,2008-- Incorporated Release3.1 - Change begins --%>
	<TR>
		<TD vAlign=top>
			<FONT color=#3366cc size=2><B>Type de produit:</B></FONT><BR>
			<FONT size=2><jato:text name="stProductType" fireDisplayEvents="true" escape="true" /></FONT> 
		</TD>
		<TD vAlign=top>
			<FONT color=#3366cc size=2><B>Charge:</B></FONT><BR>
			<FONT size=2><jato:text name="stCharge" fireDisplayEvents="true" escape="true" /></FONT> 
		</TD>
		<TD vAlign=top>
			<FONT color=#3366cc size=2><B>Date de retrait de la condition de financement:</B></FONT><BR>
			<FONT size=2><jato:text name="stFinancingDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></FONT> 
		</TD>
	</TR>
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	<%-- XS_16.21 August 01,2008-- Incorporated Release3.1 - Change ends --%>
	
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Montant total du pr�t:</b></font><br>
			<font size=2><jato:text name="stDITotalLoanAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Terme du remboursement:</b></font><br>
			<font size=2><jato:text name="stPaymentTerm" fireDisplayEvents="true" escape="true" /> - <jato:text name="stPaymentTermDesc" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Amortissement effectif:</b></font><br>
			<font size=2><jato:text name="stEffectiveAmortization" fireDisplayEvents="true" escape="true" formatType="decimal" formatMask="fr|#,##0; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Fr�quence des paiements:</b></font><br>
			<font size=2><jato:text name="stPaymentFrequency" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>RPV combin�:</b></font><br>
			<font size=2><jato:text name="stCombinedLTV" fireDisplayEvents="true" escape="true" /></font>
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Code taux:</b></font><br>
			<font size=2><jato:text name="stRateCode" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Date du taux:</b></font><br>
			<font size=2><jato:text name="stRateDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Taux affich�:</b></font><br>
			<font size=2><jato:text name="stPostedRate" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Taux net:</b></font><br>
			<font size=2><jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Taux immobilis�:</b></font><br>
			<font size=2><jato:text name="stRateLockedIn" fireDisplayEvents="true" escape="true" /></font>
		</td>
	</tr>	

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Escompte:</b></font><br>
			<font size=2><jato:text name="stDiscount" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Prime:</b></font><br>
			<font size=2><jato:text name="stPremium" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Taux d'int�r�t r�duit:</b></font><br>
			<font size=2><jato:text name="stBuyDownRate" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Traitement sp�cial:</b></font><br>
			<font size=2><jato:text name="stDISpecialFeature" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Prix d'achat estimatif pr�autoris�:</b></font><br>
			<font size=2><jato:text name="stPreAppPricePurchase" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Remise en argent($):</b></font><br>
			<font size=2><jato:text name="stCashBackInDollars" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)"/></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Remise en argent(%):</b></font><br>
			<font size=2><jato:text name="stCashBackInPercentage" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Paiement de C&I:</b></font><br>
			<font size=2><jato:text name="stPIPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
<%--
			/***************MCM Impl team changes starts - XS_16.21 *******************/
		--%>
			<font size=2 color=3366cc><b>Paiement suppl�mentaire sur le capital:</b></font><br>
<%--
			/***************MCM Impl team changes ends - XS_16.21 *******************/
		--%>
			<font size=2><jato:text name="stAdditionalPrincipalPayemt" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Paiement total des fonds plac�s en main tierce:</b></font><br>
			<font size=2><jato:text name="stTotalEscrowPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Paiement total:</b></font><br>
			<font size=2><jato:text name="stTotalPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Date du premier paiement:</b></font><br>
			<font size=2><jato:text name="stFirstPaymentDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
		</td>
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Date d'�ch�ance:</b></font><br>
			<font size=2><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Deuxi�me hypoth�que?</b></font><br>
			<font size=2><jato:text name="stSecondMortgage" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Avoir disponible:</b></font><br>
			<font size=2><jato:text name="stEquityAvailable" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
	
		<td valign=top>
			<font size=2 color=3366cc><b>Mise de fonds requise:</b></font><br>
			<font size=2><jato:text name="stRequiredDownPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Produit, taux et paiement r�quis::</b></font><br>
				<font size=2><jato:text name="stHomeBASERateProductPmnt" escape="false" /></font>
			</td>	
		
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Raison de la demande:</b></font><br>
			<font size=2><jato:text name="stDIDealPurpose" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Secteur d'activit�:</b></font><br>
			<font size=2><jato:text name="stDILOB" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Type de la demande:</b></font><br>
			<font size=2><jato:text name="stDIDealType" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Produit:</b></font><br>
			<font size=2><jato:text name="stProduct" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Option de remboursement anticip�:</b></font><br>
<%--
			/***************MCM Impl team changes starts - XS_16.21 Incorporated FSD Change*******************/
		--%>
			<font size=2><jato:text name="stPrePaymentOption" escape="true" /></font>
<%--
			/***************MCM Impl team changes ends - XS_16.21 *******************/
		--%>			
		</td>
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Type de r�f�rence:</b></font><br>
			<font size=2><jato:text name="stReferenceType" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>N<sup>o</sup> de r�f�rence:</b></font><br>
			<font size=2><jato:text name="stReferenceNo" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Date de cl�ture pr�vue:</b></font><br>
			<font size=2><jato:text name="stEstimatedClosingDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Date de cl�ture r�elle:</b></font><br>
			<font size=2><jato:text name="stActualClosingDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Option de remboursement anticip�:</b></font><br>
			<font size=2><jato:text name="stPrivilegePaymentOption" escape="true" /></font>
		</td>

	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>N<sup>o</sup> de demande de la source de r�f�rence:</b></font><br>
			<font size=2><jato:text name="stReferenceSourceApp" escape="true" /></font>
		</td>
	</tr>
<jato:tiledView name="RepeatedSOB" type="mosApp.MosSystem.pgDealSummaryRepeatedSOBTiledView">
	<tr>
		<td colspan=9 valign=top><font size=3 color=3366cc><b>
		Information sur la source&nbsp;<jato:text name="stSourceNumber" escape="true" /></b></font></td>
	</tr>
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Source:</b></font><br>
			<font size=2><jato:text name="stSource" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Firme source:</b></font><br>
			<font size=2><jato:text name="stUWSourceFirm" escape="true" /></font>
		</td>
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Adresse de la source:</b></font><br>
			<font size=2><jato:text name="stUWSourceAddressLine1" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone de la source:</b></font><br>
			<font size=2><jato:text name="stUWSourceContactPhone" escape="true" formatType="string" formatMask="(???)???-????" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur de la source:</b></font><br>
			<font size=2><jato:text name="stUWSourceFax" escape="true" formatType="string" formatMask="(???)???-????" /></font>
		</td>
	</tr>
	<tr>
		<!-- contactEmailAddress -->
		<td valign="top" colspan="1">
			<font size="2" color="3366cc"><b>Courriel:</b></font><br>
			<jato:text name="stContactEmailAddress" />
		</td>
		<td valign="top" colspan="1">
			<font size="2" color="3366cc"><b>Num�ro d'enregistrement du permis:</b></font><br>
			<jato:text name="stLicenseNumber" />
		</td>
		<!-- psDescription -->
		<td valign="top">
			<font size="2" color="3366cc"><b>Statut:</b></font><br>
			<jato:text name="stPsDescription" />
		</td>
		<!-- systemTypeDescription -->
		<td valign="top">
			<font size="2" color="3366cc"><b>Syst�me:</b></font><br>
			<jato:text name="stSystemTypeDescription" />
		</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr><td>&nbsp;</td>
	</tr>
	</jato:tiledView>
<tr>		
	<td valign=top>
		<font size=2 color=3366cc><b>Autres produits:</b></font><br>
		<font size=2><jato:text name="stCrossSell" escape="true" /></font>
	</td>
	<td valign=top colspan=2>
	<jato:text name="stIncludeFinancingProgramStart" escape="false" />
		<font size=2 color=3366cc><b>Programme de financement:</b></font><br>
		<font size=2><jato:text name="stFinancingProgram" escape="true" /></font>
	<jato:text name="stIncludeFinancingProgramEnd" escape="false" />
	</td>
	<td valign=top>
		<font size=2 color=3366cc><b>Programme d'affiliation:</b></font><br>
		<font size=2><jato:text name="stAffiliationProgram" /></font>
	</td>
</tr>	
	
	<!-- ========== DJ#725 begins ========== -->
	<!-- by Neil at Nov/30/2004 -->
	<tr>
		<td>&nbsp;</td>
	</tr>

	<!-- ========== DJ#725 ends ========== -->
				
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Prix d'achat:</b></font><br>
			<font size=2><jato:text name="stTotalPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Valeur estimative</b></font><br>
			<font size=2><jato:text name="stTotalEstimatedValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Valeur estimative r�elle:</b></font><br>
			<font size=2><jato:text name="stTotalActualAppraisedValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Pr�teur:</b></font><br>
			<font size=2><jato:text name="stLender" escape="true" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Investisseur:</b></font><br>
			<font size=2><jato:text name="stInvestor" escape="true" /></font>
		</td>
	</tr>	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	<!-- PPI start -->
	<jato:text name="stDealPurposeTypeHiddenStart" escape="false"/>
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Valeur des Am�liorations:</b></font><br>
			<font size=2><jato:text name="stRefiImprovementValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Achat + Am�liorations:</b></font><br>
			<font size=2><jato:text name="stImprovedValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>

	    </td>
	</tr>
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
    <jato:text name="stDealPurposeTypeHiddenEnd" escape="false"/>
	<!-- PPI end -->
</table>
<!-- End Of Deal Information-->


			<!-- Start Of Bridge Details-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

<jato:text name="stBeginHideBridgeSection" fireDisplayEvents="true" escape="true" />
	
	<tr>
		<td colspan=8>
			<font color=3366cc><b>D�tails du pr�t-relais:</b></font>
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>But du pr�t-relais</b></font><br>
			<font size=2><jato:text name="stBridgePurpose" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Pr�teur</b></font><br>
			<font size=2><jato:text name="stBridgeLender" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Produit</b></font><br>
			<font size=2><jato:text name="stBridgeProduct" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Description du terme du paiement</b></font><br>
			<font size=2><jato:text name="stBridgePaymentTermDesc" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Code taux</b></font><br>
			<font size=2><jato:text name="stBridgeRateCode" fireDisplayEvents="true" escape="true" /></font>
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Taux d'int�r�t affich�</b></font><br>
			<font size=2><jato:text name="stBridgePostedInterestRate" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Date du taux</b></font><br>
			<font size=2><jato:text name="stBridgeRateDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Escompte</b></font><br>
			<font size=2><jato:text name="stBridgeDiscount" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Prime</b></font><br>
			<font size=2><jato:text name="stBridgePremium" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Taux net</b></font><br>
			<font size=2><jato:text name="stBridgeNetRate" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Montant du pr�t-relais</b></font><br>
			<font size=2><jato:text name="stBridgeLoanAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Terme r�el du paiement</b></font><br>
			<font size=2><jato:text name="stBridgeActualPaymentTerm" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Date d'�ch�ance</b></font><br>
			<font size=2><jato:text name="stBridgeMaturityDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
		</td>
	</tr>
	<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

<jato:text name="stEndHideBridgeSection" fireDisplayEvents="true" escape="true" />
</table>


<!-- End Of Bridge Details-->
<!-- Start Of Refinance Info Sectiom-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	
<jato:text name="stBeginHideRefiSection" fireDisplayEvents="true" escape="true" />

	<tr>
		<td colspan=5></td>
	</tr>
	
	<tr>
		<td colspan=5>
			<font color=3366cc><b>D�tails de l'hypoth�que sur plus value/Transfert/Refinancement:</b></font>
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Date d'achat</b></font><br>
			<font size=2><jato:text name="stRefiPurchaseDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Pr�teur</b></font><br>
			<font size=2><jato:text name="stRefiMortgageHolder" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Prix d'achat original</b></font><br>
			<font size=2><jato:text name="stRefiOrigPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Valeur des am�liorations</b></font><br>
			<font size=2><jato:text name="stRefiImprovementValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Amortissement mixte</b></font><br>
			<font size=2><jato:text name="stRefiBlendedAmorization" fireDisplayEvents="true" escape="true" /></font>
		</td>
	</tr>

	<tr>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>But</b></font><br>
			<font size=2><jato:text name="stRefiPurpose" escape="true" /></font>
		</td>
		<%--
			/***************MCM Impl team changes starts - XS_16.21 (Added Existing Account Reference)*******************/
		--%>
		<td valign=top>
			<font size=2 color=3366cc><b>R�f�rence au compte existant</b></font><br>
			<font size=2><jato:text name="stRefExistingMTGNumber" escape="true" /></font>
		</td>
		<%--
			/***************MCM Impl team changes ends - XS_16.21 *******************/
		--%>
		<td valign=top>
			<font size=2 color=3366cc><b>Montant original de l'hypoth�que</b></font><br>
			<font size=2><jato:text name="stRefiOrigMtgAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Montant de l'hypoth�que impay�</b></font><br>
			<font size=2><jato:text name="stRefiOutstandingMtgAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<%-- Release3.1 Apr 12, 2006 begins --%>
		<td valign=top>
		    <font color=#3366cc size=2><b>Type de Produit:</b></font><br>
			<font size=2><jato:text name="stRefiProductType" fireDisplayEvents="false" escape="true" /></font>
		</td>
		<%-- Release3.1 Apr 12, 2006 ends --%>
	</tr>
	
	<tr>
		<td valign=top colspan=5>
			<font size=2 color=3366cc><b>Am�liorations</b></font><br>
			<font size=2><jato:text name="stRefiImprovementDesc" escape="true" /></font>
		</td>
	</tr>
	<%--
		/***************MCM Impl team changes starts - XS_16.21 - Aug 01,2008 (Added Additional Details section) *******************/
		--%>
	<tr>
		<td valign=top colspan=5>
			<font size=2 color=3366cc><b>D�tails suppl�mentaires</b></font><br>
			<font size=2><jato:text name="stRefiAdditionalInformation" escape="false" /></font>
		</td>
	</tr>
		<%--
		/***************MCM Impl team changes ends - XS_16.21 - Aug 01,2008 *******************/
		--%>
		
	<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
<jato:text name="stEndHideRefiSection" fireDisplayEvents="true" escape="true" />
</table>
<!-- End Of Refinance Info Section -->

<!--  XS_16.21 Aug,01,2008 changes -- start - Added Component Information section-->
<%--
/***************MCM Impl team changes starts - XS_16.7 ************************/
--%>
<!--  Start of component information section -->
<jato:text name="stIncludeComponentInfoSectionStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
		<td colspan=10>
			<font color=3366cc><b>Renseignements sur la composante:</b></font>
		</td>
</tr>

	<%--
		/***************Component Summary Section Starts ************************/
	--%>
	<tr>
	<td width="14%"><font color=3366cc><b>Sommaire:</b></font></td>
	<td width="5%"><font  size=2 color=3366cc><b>Montant total:</font></td>
	<td width="9%"><font  size=2 color=3366cc><b>Montant non attribu�:</font></td>

	<td width="18%"><font  size=2 color=3366cc><b>Montant total de la remise en argent:</font></td>
	</tr>
	<tr>
	<td width="14%"></td>
			  <%--
			  /**********MCM Impl team changes starts - XS_16.21 | version 1.2 -- Format Mask Change(Incorporated Review Comment) ********/
			  --%>
	<td width="10%"><font size=2><jato:text name="stTotalAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
	<td width="10%"><font size=2><jato:text name="stUnAllocatedAmount" formatType="currency" formatMask="fr|#,##0.00$;" escape="false" /></font></td>
	<td><font size=2><jato:text name="stTotalCashBackAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
			  <%--
			  /*******MCM Impl team changes ends - XS_16.21 | version 1.2 -- Format Mask Change(Incorporated Review Comment) ***********/
			  --%>
	</tr>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>	
<tr>
<td colspan=10>
<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
</td>
</tr>
<tr>
<td  width="14%"><font  size=2 color=3366cc><b>Type:</font></td>
<td  width="20%"><font  size=2 color=3366cc><b>Produit:</font></td>
<td  width="10%"><font  size=2 color=3366cc><b>Montant:</font></td>
<td width="10%"><font  size=2 color=3366cc><b>Inclure la prime d�assurance:</font></td>
<td width="10%"><font  size=2 color=3366cc><b>Montant du paiement:</font></td>
<td width="10%"><font  size=2 color=3366cc><b>Attribuer l'imp�t foncier:</font></td>
<td width="10%"><font  size=2 color=3366cc><b>Taux net:</font></td>
<td width="8%"><font  size=2 color=3366cc><b>Escompte:</font></td>
<td width="8%"><font  size=2 color=3366cc><b>Prime:</font></td>
<td><font  size=2 color=3366cc><b>Taux d�int�r�t r�duit:</font></td>
</tr>	

<tr>
		<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
<%/*Mortgage Component*/ %>
<jato:tiledView name="RepeatedMortgageComponents" type="mosApp.MosSystem.pgDealSummaryComponentMortgageTiledView">
<tr>
<td ><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
 <%--
 /*****MCM Impl team changes starts - XS_16.21 | version 1.2 -- Format Mask Change(Incorporated Review Comment)*******/ 
 --%>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stAllocateMIPremium" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPaymentAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stAllowTaxEscrow" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true"  /></font></td>
<td ><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stBuyDownRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
 <%--
 /********MCM Impl team changes ends - XS_16.21 | version 1.2 -- Format Mask Change(Incorporated Review Comment)*****/ 
 --%>
		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

</jato:tiledView>

<%/*LOC COMPONENT*/ %>
<jato:tiledView name="RepeatedLocComponents" type="mosApp.MosSystem.pgDealSummaryComponentLocTiledView">
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<%--
/*******MCM Impl team changes starts - XS_16.21 | version 1.2 -- Format Mask Change(Incorporated Review Comment)********/ 
--%>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stAllocateMIPremium" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPaymentAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stAllowTaxEscrow" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true"  /></font></td>
<td><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
 <%--
 /*********MCM Impl team changes ends - XS_16.21 | version 1.2 -- Format Mask Change(Incorporated Review Comment)******/ 
 --%>

		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

</jato:tiledView>
<%/*CREDIT CARD COMPONENT*/ %>
<jato:tiledView name="RepeatedCreditCardComponents" type="mosApp.MosSystem.pgDealSummaryComponentCreditCardTiledView">
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
<td></td>
<td></td>
<td></td>
<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
<td></td>
<td></td>
<td></td>

		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
</jato:tiledView>
<%/*LOAN COMPONENT*/ %>
<jato:tiledView name="RepeatedLoanComponents" type="mosApp.MosSystem.pgDealSummaryComponentLoanTiledView">
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
<td></font></td>
<td><font size=2><jato:text name="stTotalPayment" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
<td></font></td>
 <%--
 /********MCM Impl team changes starts - XS_16.21 | version 1.2 -- Format Mask Change(Incorporated Review Comment)**********/ 
 --%>
<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
			  <%--
			  /**********MCM Impl team changes ends - XS_16.21 | version 1.2 -- Format Mask Change(Incorporated Review Comment) ********/
			  --%>
<td></td>

		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

</jato:tiledView>
<%/*OVER DRAFT COMPONENT*/ %>
<jato:tiledView name="RepeatedOverDraftComponents" type="mosApp.MosSystem.pgDealSummaryComponentOverDraftTiledView">
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
<td></td>
<td></td>
<td></td>
<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true"  /></font></td>
<td></td>
<td></td>
<td></td>
		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
</jato:tiledView>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<jato:text name="stIncludeComponentDetailsButtonStart" escape="false" />
<td align="right"><jato:button name="btComponentDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/component_detail_fr.gif" /></td>
<jato:text name="stIncludeComponentDetailsButtonEnd" escape="false" />
</tr>
<tr>
	<td colspan=10>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
	</td>
</tr>
	

</table>
<jato:text name="stIncludeComponentInfoSectionEnd" escape="false" />
<%--
/****End of the componentinfo section***/
--%>


<%--
/***************MCM Impl team changes ends - XS_16.7***************************/
--%>

<!-- XS_16.21 Aug 01, 2008 - Changes ends -->

<!-- Include Part 2 of DealSummary --//-->
<jsp:include page="pgDealSummary_part2_fr.jsp" />


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.dealsumborr.top=100;
	document.dealsumasst.top=100;
	document.dealsuminc.top=100;
	document.dealsumliab.top=100;
	document.dealsumprop.top=100;
	document.gdstds.top = 100;
	document.alertbody.top=100;
}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(4)
}
else{
tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
    </script>

<!--END BODY//-->
<br><jato:hidden name="sessionUserId" />

<jato:hidden name="hdExecuteEscow" />

<SCRIPT LANGUAGE="JavaScript">
toggleQualifyProduct();
toggleQualifyPencil();
</script>
</jato:form>
</body>
</jato:useViewBean>
</HTML>
