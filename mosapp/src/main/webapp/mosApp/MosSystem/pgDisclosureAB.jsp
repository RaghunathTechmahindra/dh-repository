
<HTML>
<%@page info="pgDisclosureAB" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDisclosureABViewBean">

<HEAD>
<TITLE>Disclosure Statement</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/ProvinceGenericCommonFunctions.js" type="text/javascript"></script>
</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgDisclosureAB" method="post" onSubmit="is_submit_button();">

<SCRIPT LANGUAGE="JavaScript">

var isCreateDocButton = false;
var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();
var provId="eval(<%=viewBean.handler.provinceId%>)";
var pageName="<%= viewBean.PAGE_NAME%>";
var provON="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ONTARIO%>)";
var provAB="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ALBERTA%>)";
var provNL="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_NEWFOUNDLAND%>)";
var provNS="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_NOVA_SCOTIA%>)";
var provPEI="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_PRINCE_EDWARD_ISLAND%>)";
var provON="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ONTARIO%>)";
function checkDealStatus() 
{
	var dealStatusId = eval("document.forms[0]."+ pageName +"_hdDealStatusId.value");
	
	if(dealStatusId == "23")
		var msgStatus = "Collapsed.";
	if(dealStatusId == "24")
		var msgStatus = "Denied."
		
	var msg = "You cannot generate a disclosure document while the deal is " + msgStatus;
		
	if(dealStatusId == "23" || dealStatusId == "24")
	{
		alert(msg);
		setSubmitFlag(false);
		return;
	}
	else
	{ 
		if(provON == provId ){
			setCancelFlag(false);
		}
		//setSubmitFlag(true);
		isCreateDocButton = true;
		document.forms[0].submit();
	}
}

function is_submit_button()
{
	if(!isCreateDocButton)
	{
		return IsSubmitButton();
	}
	else
	{
		isCreateDocButton = false;
	}
}




</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

	<!--END HEADER//-->

	<!--DEAL SUMMARY SNAPSHOT//-->
	<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>

	<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<!-- PUT HERE ANY SUSTOM CHILDREN ELEMENTS (TEXTBOXES, TILED VIEWS, ETC.)//-->

<table border=0 BGCOLOR=99ccff width=100%>
<!--Disclosure Date Section -->
<!--AB Bonus/Discount Section -->	

<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 9.<br>d (i). Bonus or discount paid by borrower (if applicable)
</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;$&nbsp;</b><jato:textField name="101Q090V" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

<!--AB Section 8 -->	
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 8: Where the term of the loan is subject to variations, it shall vary in the following manner:</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:textArea name="101Q080U" rows="4" cols="80" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

<!--AB Section 10 -->	
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 10: Where the actual percentage rate is subject to variations, it shall vary in the following manner, based on the following conditions:</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:textArea name="101Q10AC" rows="4" cols="80"  /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	
<!--AB Section 11 -->	
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 11: The terms and conditions of repayment before maturity of the loan contract is as follows:</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:textArea name="101Q10AD" rows="4" cols="80"  /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	
</table>
	<jato:hidden name="hdCopyId" />
	<jato:hidden name="stDealId"/>
	<jato:hidden name="hdGenerateDocumentPdf" />
	<jato:hidden name="hdDealStatusId" />
	<jato:hidden name="hdInstr" />
<P>

<table border=0 width=100% cellpadding=0 cellspacing=0>
      <td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'checkDealStatus(); '" src="../images/submit.gif" />&nbsp;&nbsp;<jato:button name="btSaveAndExit" extraHtml="width=130 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/SaveAndExit.gif" />&nbsp;&nbsp;<jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/cancel.gif" /></td>
</table>


</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
