
<HTML>
<%@page info="pgFeeReview" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgFeeReviewViewBean">

<HEAD>
<TITLE>Fee Review</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
.addFeeDialog{visibility:hidden; width:400;border-width:1pt;border-style: outset;border-color:"red"}

-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--
//========== Local JavaScript -- Modified by Billy for JATO field format -- 26July2002 ===================
function findSelectedOptionAndSetDefaults( feeTypeArr, amountArr, methodArr, monthArr, dayArr, yearArr, statusId)
{
var changedComboBox =(NTCP) ? event.target : event.srcElement;
var selectedOptionValue = getSelectedOptionValue(changedComboBox);	//getValue of the selected option
var control_Full_Name = changedComboBox.name;
var selectedRow = getRepeatRowId(changedComboBox);
var name1stPart = get1stPartName(control_Full_Name);

var numElem; 
var numOptions;
var keyIndex= -1;	//feeTypeArr index
var matched = false;
var i=0;
var j=0;

var theCurrCtl;

	numElem = getNumOfRow(control_Full_Name);
	theCurrCtl = eval("document.pgFeeReview.elements['pgFeeReview_Repeated1[0]_cbFeeDescription']");
	//numOptions=document.pgFeeReview.pgFeeReview_Repeated1[0]_cbFeeDescription.options.length;
	numOptions=theCurrCtl.options.length;
	
	if( selectedOptionValue == -1)
		return;
	
//compare with others
	for(j=0; j<numElem; j++)
	{
		theCurrCtl = eval("document.pgFeeReview.elements['" + name1stPart + j + "]_cbFeeDescription" + "']");
		if(j != selectedRow && selectedOptionValue == getSelectedOptionValue(theCurrCtl) )
		{
			matched = true;
			break;
		}
	 }

   	 if(matched == true)
	 {
	    alert(FEE_TYPE_ALREADY_SET);
		//alert("You have selected Fee Type, already been chosen on the screen");
		//Reset current row values
		changedComboBox.selectedIndex = -1;

		
		theCurrCtl = eval("document.pgFeeReview.elements['" + name1stPart + selectedRow + "]_tbFeeAmount" + "']");
		theCurrCtl.value = 0.0;
		theCurrCtl = eval("document.pgFeeReview.elements['" + name1stPart + selectedRow + "]_cbPaymentMethod" + "']");
		theCurrCtl.selectedIndex= -1;
		theCurrCtl = eval("document.pgFeeReview.elements['" + name1stPart + selectedRow + "]_cbMonth" + "']");
		theCurrCtl.selectedIndex= -1;
		theCurrCtl = eval("document.pgFeeReview.elements['" + name1stPart + selectedRow + "]_tbDay" + "']");
		theCurrCtl.value = 0;
		theCurrCtl = eval("document.pgFeeReview.elements['" + name1stPart + selectedRow + "]_tbYear" + "']");
		theCurrCtl.value = 0;
		theCurrCtl = eval("document.pgFeeReview.elements['" + name1stPart + selectedRow + "]_cbFeeStatus" + "']");
		theCurrCtl.selectedIndex= -1;
		return;
	}
	
		
//get key index in the key feeType array

	for(i=0; i<feeTypeArr.length;i++)
	{
	   if(selectedOptionValue==feeTypeArr[i])
	   {
		keyIndex = i;
		break;
	   }
	}
	
		
if (keyIndex==-1)
	return;

//set defaults
  defaults(selectedRow , amountArr[keyIndex], methodArr[keyIndex], monthArr[keyIndex],
		dayArr[keyIndex], yearArr[keyIndex], statusId);
  
}


function getSelectedOptionValue(comboBox)
{
    for(i=0; i<comboBox.options.length;i++)
    {
	if( comboBox.options[i].selected == true)
	   return comboBox.options[i].value;
    }
    return -1;
}


function defaults(lineNumber, defaultFeeAmount, defaultFeePaymentMethodId, defaultMonth, defaultDay, defaultYear, defaultStatusId )
{
	var theCurrCtl = eval("document.pgFeeReview.elements['pgFeeReview_Repeated1[" + lineNumber + "]_tbFeeAmount" + "']");
	theCurrCtl.value = defaultFeeAmount;
	
	theCurrCtl = eval("document.pgFeeReview.elements['pgFeeReview_Repeated1[" + lineNumber + "]_cbPaymentMethod" + "']");
	for (var i = 0; i < theCurrCtl.options.length; i++) 
	{ 
		if (theCurrCtl.options[i].value==defaultFeePaymentMethodId)
	    {
			theCurrCtl.options[i].selected=true;
		 	break;
		}
	}
	
	theCurrCtl = eval("document.pgFeeReview.elements['pgFeeReview_Repeated1[" + lineNumber + "]_cbMonth" + "']");
	for (var i = 0; i < theCurrCtl.options.length; i++) 
	{ 
    	if(theCurrCtl.options[i].value==defaultMonth)
    	{
			 theCurrCtl.options[i].selected=true;
			 break;
		}
	}
	
	theCurrCtl = eval("document.pgFeeReview.elements['pgFeeReview_Repeated1[" + lineNumber + "]_tbDay" + "']");
	theCurrCtl.value = defaultDay;
	
	theCurrCtl = eval("document.pgFeeReview.elements['pgFeeReview_Repeated1[" + lineNumber + "]_tbYear" + "']");
	theCurrCtl.value = defaultYear;

	theCurrCtl = eval("document.pgFeeReview.elements['pgFeeReview_Repeated1[" + lineNumber + "]_cbFeeStatus" + "']");
	for (var i = 0; i < theCurrCtl.options.length; i++) 
	{
		 if( theCurrCtl.options[i].value==defaultStatusId)
		 {
		 	theCurrCtl.options[i].selected=true;
		 	break;
		 }
	}
}

// Function to check if the new FeeType is used 
function isNewFeeTypeSelected()
{

if(isAddCancelPressed == true)
	return;

var theComboBox =(NTCP) ? event.target : event.srcElement;
var selectedOptionValue = getSelectedOptionValue(theComboBox);	//getValue of the selected option


var j=0;
var isUsed = false;

	if( selectedOptionValue == -1)
	{
		//alert("Non selected ... (-1) !!");
		return;
	}
		
	var numElem = getNumOfRow("pgFeeReview_Repeated1[0]_tbFeeAmount");
	var theCurrCtl;
	
	//compare with others
	// Handle multiple rows
	for(j=0; j<numElem; j++)
	{
		theCurrCtl = eval("document.pgFeeReview.elements['pgFeeReview_Repeated1[" + j + "]_cbFeeDescription" + "']");

		if(selectedOptionValue == getSelectedOptionValue(theCurrCtl) )
		{
			isUsed = true;
		}
	 }
	 
   	 if(isUsed == true)
	 {
	 	alert(FEE_TYPE_ALREADY_SET);
	  	theComboBox.focus();
	 }
	return;
}

var isAddCancelPressed = false;

//=====================================================================================
// Functions to handle pop-up dialog 
function openAddFeeDialog()
{
	if (document.all)
	{
		document.all.addFeeDialog.style.visibility='visible';
	}
    if (document.layers)
	{
		document.addFeeDialog.visibility='show';
	}
	isAddCancelPressed = false;
	//document.pgFeeReview.cbAddFeeDesc.selectedIndex = 0;
	document.pgFeeReview.pgFeeReview_cbAddFeeDesc.value = -1;
	//document.pgFeeReview.cbAddFeeDesc.option[0].selected = true;
}

function closeAddFeeDialog()
{
	if (document.all)
	{
		document.all.addFeeDialog.style.visibility='hidden';
	}
	if (document.layers)
	{
		document.addFeeDialog.style.visibility='hide';
	}
	
	isAddCancelPressed = true;
}
//=======================================================================================


//-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff onload="checkMIResponseLoop();">
<jato:form name="pgFeeReview" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
    
    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 
	
	<!--END HEADER//-->

	<!--DEAL SUMMARY SNAPSHOT//-->
	<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>

	<!--END DEAL SUMMARY SNAPSHOT//-->


<!-- START BODY OF PAGE//-->

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<!--Fee Review table-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
	<tr>			
		<td colspan=8><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td>	
	</tr>	
	<tr>			
		<td colspan=8><font size=4 color=#3366cc><b>Fee Details:</b></font></td>
	</tr>	
	<tr>
		<td>&nbsp;&nbsp;<font size=2 color=3366cc><b>Description:</b></font></td>
		<td><font size=2 color=3366cc><b>Amount:</b></font></td>
		<td colspan=3><font size=2 color=3366cc><b>Payment Date:</b></font></td>
		<td><font size=2 color=3366cc><b>Payment Method:</b></font></td>
		<td><font size=2 color=3366cc><b>Status:</b></font></td>

<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgFeeReviewRepeated1TiledView">

		<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
	<tr>
		<td nowrap><font size=2><jato:hidden name="hdDealFeeId" /><jato:combobox name="cbFeeDescription" />&nbsp;</font></td>
		<td nowrap><font size=2><jato:text name="stDollarSign" fireDisplayEvents="true" escape="true" />
        <jato:textField name="tbFeeAmount" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" />&nbsp;</font></td>
		<td nowrap colspan=3><font size=2><jato:text name="stMonthLabel" fireDisplayEvents="true" escape="true" /><jato:combobox name="cbMonth" /><jato:text name="stDayLabel" fireDisplayEvents="true" escape="true" />
        <jato:textField name="tbDay" size="2" maxLength="2" /><jato:text name="stYearLabel" fireDisplayEvents="true" escape="true" />
        <jato:textField name="tbYear" size="4" maxLength="4" />&nbsp;</font></td>
		<td><font size=2><jato:combobox name="cbPaymentMethod" />&nbsp;</font></td>
		<td><font size=2><jato:combobox name="cbFeeStatus" />&nbsp;</font></td>
		<td><jato:button name="btDelete" extraHtml="width=45 height=14 alt='' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete.gif" /></td>

		<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
		<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</jato:tiledView>

		<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td></tr>

		<td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stAddFeeButtonHTML" fireDisplayEvents="true" escape="false" /></td>
		<td colspan=7>
			<div id=addFeeDialog class="addFeeDialog" name=addFeeDialog> 
				<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>
		
				<tr>
				<td align=left rowspan="2" valign="middle" width="0%" nowrap><font size="2" color="#3366CC"><b> Fee
                  Type :&nbsp; </b></font></td>
				<td align=left rowspan="2" valign="middle" width="0%"><jato:combobox name="cbAddFeeDesc" extraHtml="onBlur='isNewFeeTypeSelected();'" /></td>
				</tr>
				<tr>
					<td align=right valign="bottom" nowrap width="100%" ><jato:button name="btAddFee" extraHtml="width=60 height=20 alt='' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" /> 
						<a href="javascript:closeAddFeeDialog()"><img src= "../images/cancel.gif" width=60  height=20 alt="" border="0"></a>
					</td>
				</tr>

				</table>
			</div>
		</td>    

		
		<tr><td colspan=8>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>

		<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
		<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><jato:button name="btBackwardButton" extraHtml="width=84 height=19 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous.gif" />&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="width=84 height=19 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2.gif" /></td>
</table>

<!--
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
	<tr>

		<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	</tr>

	<tr>
		<td colspan=7> <font size=4  color=#3366cc><b>Fee Summary:</b></font></td>
	</tr>
	
	<tr>
		<td colspan=7><font size=2 color=3366cc><b>Total Fee Amount</b></font></td>
	</tr>

	<tr>
		<td colspan=7><font size=2 ><b></b></font></td>
	</tr>
	<tr>

		<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	</tr>

</table>
-->
<!-- Sample : Submit and Cancel buttons //-->
<p>
 <table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/sign_cancel.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR>
<BR>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>