<%--
 /**Title:pgComponentInfoPagelet.jsp
 * Description:Pagelet to sumDeal display fields of Component Summary.
 * @author: MCM Impl Team
 * @version 1.0 2008-6-11
 * @version 1.1 03-07-2008 XS_2.25 Added Deal Summary Section and Component Summary Section.
 * @version 1.2 03-07-2008 XS_2.36 Added ComponentCreditCard Section
 * @version 1.3 04-07-2008 Added MIAllocation/PropertyTax Allocation JS Handling
 * @version 1.4 July 08-2008 fixed MIAllocation/PropertyTax Allocation errorJS Handling
 * @version 1.5 July 08-2008 fixed Js issues in Mtg Component section - artf740067
 * @version 1.6 July 08-2008 fixed Js issues in Mtg Component section - artf739249
 * @version 1.7 07-07-2008 XS_2.37 Added Loan Section
 * @version 1.8 07-07-2008 XS_2.36 Added Credit Card Section
 * @version 1.9 July 10, 2008 artf733712: fixed maxlength for Cashback amount in MTG section
 *              also, added size and maxlength attributes to txMortgageAmount in MTG section
 *              and txCreditCardAmount in CreditCard section.
 * @version 1.10 July 10, artf738341: deleted disable attribute and added readOnly 
 *               attribute for payment term description and effective amortization 
 *               period in mortgage section and for payment term description in Loans section.
 * @version 1.11 July 11, 2008:  artf741023 + clean up : 
                 Added line separator for LOC section and Credit Card section. 
                 Plus, aligned cosmetic look and feel.
   @version 1.12 July 11, 2008: changed "Total Payment:" label in Loans section to "P&I"
 * @version 1.13 added Add Component section - XS_2.26 Modified
 * @version 1.14 modified the format for Credit Card Amount because of artf751496
 * @version 1.15 Aug 14, 2008: fixed artf762702
 * @version 1.16 Aug 23, 2008: artf763316. replacing XS_2.60
 * @version 1.17 Sep 10, 2008:Bug Fix :artf778636 : Allignment correction for the component type in Add component section.
 */
--%>
<HTML>

<%@page info="pgComponentDetails" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgComponentDetailsViewBean">

<HEAD>
<TITLE>Component Details</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
.refisection {position: relative; left: 0; top: 0; display:none;}
.greyedDisabled {background-color: #dddddd; color:#000000; font-size: 15;}
.blueDisabled {background-color: #d1ebff; color:#000000;}
.sectionTitle {font-size: medium; font: bold; color: 3366cc}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>
<script src="../JavaScript/componentInfoCheckBoxis.js" type="text/javascript"></script>
<script src="../JavaScript/windowScrollPositioning.js" type="text/javascript"></script><!--FXP23869-->

<SCRIPT LANGUAGE="JavaScript">
<!--

function onOtherDetailsClick(){
    self.scroll(1116, 1090);
}


function isThereMoreThanOneYes(nonSelectedControl)
{
    var theField =(NTCP) ? event.target : event.srcElement;
    var theFieldValue = theField.value;
    var theFieldName = theField.name;

    if(theFieldValue == "Y")
    {
        var rowId = getRepeatRowId(theField);
        var totRows =  getNumOfRow(theFieldName );

        var name1stPart = get1stPartName(theFieldName);
        var name2ndPart = get2ndPartName(theFieldName);

        if(totRows == 1)
            return;

        for(var i=0;i<totRows;i++)
        {
            var ctlname = eval("document.forms[0].elements['" + name1stPart + i + name2ndPart + "']");

            var ctlValue = ctlname.value;
            if(ctlValue == "Y" && i != rowId)
                ctlname.value = 'N';
        }
    }

    return;
}
<jato:text name="stVALSData" escape="false" /> 

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);

var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

//========================================================================================================
// Function to initialize the page -- may be difference for every pages
//      It should be used in the onLoad event of <body> tag
// ========================================================================================================
function initPage(event)
{
    // Set MIPolicyNo
    //SetMIPolicyNoDisplay();
    //--BMO_MI_CR--//
    // Set PreQualifCertNumDisplay
    //SetMIPreQualifCertNumDisplay();
    // Set SetMIExistingPolicyDisplay 
    //SetMIExistingPolicyDisplay();
    
    //--Release3.1 MI
    // hide or display Request Standard Service
    //hideRequestStandardService();
    //hideSubProgressAdvance();
  //CR03
  //changeExpirationDate();
  //CR03
  //<!-- XS 2.26 Add Component change starts -->
  initCbProductForAdd();
  //<!-- XS 2.26 Add Component change starts -->
  
  //MCM/4.1 Jan 16, 2009 - FXP23869: vMCM_Component_Details_scrolling_up 
  applyScrollPosition();
}
-->
</SCRIPT>
</HEAD>
<!--HEADER//-->

<body bgcolor=ffffff onload="initPage(); checkMIResponseLoop();">
<jato:form name="pgComponentDetails" method="post" onSubmit="return(IsSubmitButton());" >
<%-- FXP23869, MCM/4.1, Jan 20 2009, saving/resetting screen scroll position - start --%>
<jato:textField name="txWindowScrollPosition" elementId="txWindowScrollPosition" style="display: none"/>
<%-- FXP23869, MCM/4.1, Jan 20 2009, saving/resetting screen scroll position - end --%>
<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>

<%@include file="/JavaScript/CommonHeader_en.txt" %>  

<%@include file="/JavaScript/CommonToolbar_en.txt" %>  

<script src="../JavaScript/rc/PricingData.js" type="text/javascript"></script>

<div id="pagebody" class="pagebody" name="pagebody">
<center>

<!-- VIEW ONLY TAG  //-->
<table border=0 width=100% cellpadding=0 cellspacing=0>
    <tr>
        <td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></td>
    </tr>
</table>


<!--DEAL SUMMARY SNAPSHOT - UWWS MODE //-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=B8D0DC>
    <td align=center colspan=8
        ><img src="../images/white.gif" width=10 height=1
        ><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
    <tr>
        <td rowspan=2>&nbsp;</td>
        <td><font size="1">DEAL #:</font><br><font size=2><b><jato:text name="stDealId" escape="true" formatType="decimal" formatMask="###0; (-#)" /></b></font></td>
        <td><font size="1">BORROWER NAME:</font><br><font size=2><b><jato:text name="stBorrFirstName" escape="true" /></b></font></td>
        <td><font size="1">DEAL STATUS:</font><br><font size=2><b><jato:text name="stDealStatus" escape="true" /></b></font></td>
        <td><font size="1">DEAL STATUS DATE:</font><br><font size=2><b><jato:text name="stDealStatusDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
        <td><font size="1">SOURCE FIRM:</font><br><font size=2><b><jato:text name="stSourceFirm" escape="true" /></b></font></td>
        <td><font size="1">SOURCE:</font><br><font size=2><b><jato:text name="stSource" escape="true" /></b></font></td>
        <td><font size="1">UNDERWRITER:</font><br><font size=2><b>
            <jato:text name="stUnderwriterLastName" escape="true" />, 
            <jato:text name="stUnderwriterFirstName" escape="true" /> <jato:text name="stUnderwriterMiddleInitial" escape="true" />.</b></font></td>
    </tr>
    <tr>
        <!-- ========== BMO II CCAPS Section 4.6 begins ========== -->
        <!-- by Neil on Feb 14, 2005 -->
        <!-- <td align=center colspan=8>&nbsp;</td> -->
        <td align="left" colspan="8"><font size=2><jato:text name="stCCAPS" escape="true" /><b><jato:text name="stServicingMortgageNumber" escape="true" /></b></font>&nbsp;</td>
        <!-- ========== BMO II CCAPS Section 4.6 ends ========== -->
    <tr>
    <tr>
        <td>&nbsp;</td>
        <td><font size=1>DEAL TYPE:</font><br><font size=2><b><jato:text name="stDealType" escape="true" /></b></font></td>
        <td><font size=1>DEAL PURPOSE:</font><br><font size=2><b><jato:text name="stDealPurpose" escape="true" /></b></font></td>
        <td><font size=1>TOT LN AMT:</font><br><font size=2><b>$ <jato:text name="stTotalLoanAmount" escape="true" formatType="decimal" formatMask="#,##0.00; (-#)" /></b></font></td>
        <td><font size=1>APPLICATION DATE:</font><br><font size=2><b><jato:text name="stApplicationDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
        <td><font size=1>PAYMENT TERM:</font><br><font size=2><b><jato:text name="stPmtTerm" escape="true" /></b></font></td>
        <td><font size=1>EST CLOSING DATE:</font><br><font size=2><b><jato:text name="stEstClosingDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
        <td><font size=1>SPECIAL FEATURE:</font><br><font size=2><b><jato:text name="stSpecialFeature" escape="true" /></b></font></td>
    </tr>
    <tr>
        <td align=center colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
    </tr>
</table>

<p>
<%--
 /*
  *Deal Summary Section starts
  */
--%>
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d1ebff">
  <tr><td><img  src="../images/blue_line.gif" width="100%" height="1"></td></tr>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	
	    <tr><td><font size=1>&nbsp;</font></td></tr>
		<tr><td colspan="5"><font class="sectionTitle">&nbsp;&nbsp;&nbsp;Deal Summary</font></td></tr>
	    <tr><td><font size=1>&nbsp;</font></td></tr>
	    <tr>
	    <td width="18%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color="3366cc"><b>LOB:</b></font></td>
	    <td width="18%"><font size=2 color="3366cc"><b>Product:</b></font></td>
	    <td width="18%"><font size=2 color="3366cc"><b>MI Premium:</b></font></td>
	    <td width="18%"><font size=2 color="3366cc"><b>Tax Escrow:</b></font></td>
	    <td ><font size=2 color="3366cc"><b>Combined LTV:</b></font></td>
	    </tr>
	    <tr>
	    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2><jato:text name="stLOB" fireDisplayEvents="false" escape="true" /></font></td>
	    <td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
	    <td><font size=2><jato:text name="stMIPremium" formatType="currency" formatMask="$#,##0.00; -#" escape="false" /></font></td>
	    <td><font size=2><jato:text name="stTaxEscrow" formatType="currency" formatMask="$#,##0.00; -#" escape="false" /></font></td>
	    <td><font size=2><jato:text name="stCombinedLTV" formatType="decimal" formatMask="##0.00; -#" escape="false" />%</font></td>

</table>
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d1ebff">
  <tr><td><img  src="../images/blue_line.gif" width="100%" height="1"></td></tr>
</table>
  <%--
 /*
  *Deal Summary Section ends
  */
--%>
<p>

<!-- XS 2.26 Add Component starts -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d1ebff">
  <tr><td><img  src="../images/blue_line.gif" width="100%" height="1"></td></tr>
</table>
<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="d1ebff">
    <tr><td><font size=1>&nbsp;</font></td></tr>
    <tr><td><font class="sectionTitle">&nbsp;&nbsp;&nbsp;Add Component</font></td></tr>
    <tr><td><font size=1>&nbsp;</font></td></tr>
    <tr>
        <td width="35%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <font size=2 color=3366cc><b>Component Type:</b></font>
            <jato:combobox name="cbComponentType" elementId="cbComponentTypeAdd" styleClass="greyedDisabled"/>
        </td>
        <td width="35%"  align="left"><font size="2" color="3366cc">
            <b>Product:</b></font>
            <jato:combobox name="cbComponentProduct" elementId="cbComponentProductAdd" onChange="handleCbProductForAddChange()"/>
        </td>
        <td  align="left">
            <jato:button name="btAddComponent" onClick="setSubmitFlag(true)" fireDisplayEvents="true" src="../images/add_component.gif" />
        </td>
	</tr>
</table>


<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d1ebff">
  <tr><td><img  src="../images/blue_line.gif" width="100%" height="1"></td></tr>
</table>
<!--XS 2.26 Add Component ends  -->

<p>
<%--
 /*
  *Component Summary Section starts
  */
--%>
  <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d1ebff">
    <tr><td><img  src="../images/blue_line.gif" width="100%" height="1"></td></tr>
  </table>
  
  <table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
        <tr><td><font size=1>&nbsp;</font></td></tr>
        <tr><td><font class="sectionTitle">&nbsp;&nbsp;&nbsp;Component Summary</font></td></tr>
        <tr><td><font size=1>&nbsp;</font></td></tr>
	    <tr>
		    <td width="35%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color="3366cc"><b>Total Amount:</b></font>
		    <font size=2><jato:text name="stTotalAmount" formatType="currency" formatMask="$#,##0.00; -#" escape="false" /></font></td>
		    <td width="35%"><font size=2 color="3366cc"><b>Unallocated Amount:</b></font>
		    <font size=2><jato:text name="stUnAllocatedAmount" formatType="currency" formatMask="$#,##0.00; -#" escape="false" /></font></td>
		    <td><font size=2 color="3366cc"><b>Total Cashback Amount:</b></font>
		    <font size=2><jato:text name="stTotalCashBackAmount" formatType="currency" formatMask="$#,##0.00; -#" escape="false" /></font></td>
	    <tr>
  </table>
  
  <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d1ebff">
    <tr><td><img  src="../images/blue_line.gif" width="100%" height="1"></td></tr>
  </table>
<%--
 /*
  *Component Summary Section ends
  */
--%>  
<p>
<!-- COMPONENT DETAILS //-->
<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="d1ebff">

    <%-- ------------------ start line of component details --------------------- --%>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d1ebff">
        <tr>
           <td><img  src="../images/blue_line.gif" width="100%" height="1"></td>
        </tr>
        <tr><td><font size=1>&nbsp;</font></td></tr>
        <tr><td><font class="sectionTitle">&nbsp;&nbsp;&nbsp;Component Details</font></td></tr>
        <tr><td><font size=1>&nbsp;</font></td></tr>
    </table>

	

	<!-- global hidden field-->
    <jato:textField name="stIsPageEditable" elementId="isPageEditable" style="display: none"/>
    <jato:textField name="stLenderProfileId" elementId="lenderProfileId" style="display: none"/>  
	
	<!-- MORTGATE COMPONENT TILE START //-->
    <jato:tiledView name="MTGComp" type="mosApp.MosSystem.pgComponentDetailsMTGTiledView">
    <jato:hidden name="hdComponentIdMTG" />
    <jato:hidden name="hdCopyIdMTG" />
    <table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
    
        <colgroup>
            <col width="230" align="left"/>
            <col width="150" align="left"/>
            <col width="250" align="left"/>
            <col width="230" align="left"/>
            <col width="150" align="left"/>
            <col align="left"/>
        </colgroup>
	    
	    <tr>
	       <td colspan=6 valign=top><jato:image name="imMTGSectionDevider" /></b></font></td>
	    </tr>
	    <tr>
	       <td colspan=6 valign=top>
	           <font class="sectionTitle">&nbsp;&nbsp;&nbsp;<jato:text name="stMTGTitle" escape="false"/></font>
	       </td>
	    </tr>
    	<tr>
		   <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color="3366cc"><b>Component Type:</b></font></td>
		   <td valign="top" colspan=2><font size=2><jato:combobox name="cbComponentType" styleClass="greyedDisabled" /></font></td>
		   <td valign="top"><font size=2 color="3366cc"><b>Product:</b></font></td>
		   <td valign="top" colspan=2><jato:combobox name="cbMtgProduct" /></td>
		</tr>
    	    <tr>
	        <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Mortgage Amount:</b></font></td>
	        <td valign="top" colspan=2><font size=2>$<jato:textField name="txMortgageAmount"  formatType="decimal" formatMask="###0.00; (-#)" size="14" maxLength="14" /></font></td>
	        <td valign="top"><font size=2 color=3366cc><b>Posted Interest Rate:</b></font></td>
	        <td valign="top" colspan=2><jato:combobox name="cbMtgPostedInterestRate" /></td>
	    </tr>
	    <tr>
	        <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>MI Premium:</b></font></td>
	        <td valign="top"><font size=2><jato:text name="stMIPremium" escape="false" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
	        <td valign="top"><font size=2 color=3366cc ><b>Allocate MI Premium</b></font>&nbsp;<jato:checkbox name="chAllocateMIPremium" styleClass="allocateMIPremium" onClick="turnOffOtherAllocateMIPremium(this)"/></td>
	        <td valign="top"><font size=2 color=3366cc><b>Discount:</b></font></td>
	        <td valign="top" colspan=2><jato:textField name="txDiscount" formatType="decimal" formatMask="###0.000;-#" size="6" maxLength="6" /> % </td>
	    </tr>
		<tr>
	        <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Total Mortgage Amount:</b></font></td>
	        <td valign=top colspan=2><font size=2><jato:text name="stTotalAmount" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
	        <td valign=top><font size=2 color=3366cc><b>Premium:</b></font></td>
	        <td valign=top colspan=2><jato:textField name="txPremium" formatType="decimal" formatMask="###0.000;-#" size="6" maxLength="6" /> % </td>
	    </tr>
	    <tr>
	        <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Amortization Period:</b></font></td>
	        <td valign=top colspan=2>
	            <font size=2>Yrs: </font><jato:textField name="txAmortizationPeriodYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" />
	            <font size=2>Mths: </font><jato:textField name="txAmortizationPeriodMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" />
	        </td>
	        <td valign=top><font size=2 color=3366cc><b>Buydown:</b></font></td>
	        <td valign=top colspan=2><jato:textField name="txBuyDown" formatType="decimal" formatMask="###0.000;-#" size="6" maxLength="6" /> % </td>
	    </tr>
	    <tr>
	        <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Effective Amortization Period:</b></font></td>
	        <td valign=top colspan=2>
	            <font size=2>Yrs: </font><jato:textField name="txEffectAPYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" styleClass="blueDisabled" readOnly="true"/>
	            <font size=2>Mths: </font><jato:textField name="txEffectAPMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" styleClass="blueDisabled" readOnly="true"/>
	        </td>
	        <td valign=top><font size=2 color=3366cc><b>Net Rate:</b></font></td>
	        <td valign="top" colspan=2><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="###0.000;-#" /> % </font></td>
	    </tr>
	    <tr>
	        <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Payment Term Description:</b></font></td>
	        <td valign=top colspan=2><font size=2>
	           <jato:textField name="tbPaymentTermDescription" size="20" maxLength="35" styleClass="blueDisabled" readOnly="true"/></font></td>
			<td valign=top><font size=2 color=3366cc><b>P&I:</b></font></td>
			<td valign=top colspan=2><font size=2><jato:text name="stPIPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
	    </tr>

		<tr>
			<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Actual Payment Term:</b></font></td>
			<td valign=top colspan=2>
			  <font size=2>Yrs: </font><jato:textField name="txActualPayYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" />
			  <font size=2>Mths: </font><jato:textField name="txActualPayMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" />
			</td>
			<td valign=top><font size=2 color=3366cc><b>Additional P&I:</b></font></td>
			<td valign=top colspan=2><font size=2>$<jato:textField name="txAdditionalPIPay" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></font></td>
		</tr>
		<tr>
			<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Payment Frequency:</b></font></td>
			<td valign=top colspan=2><jato:combobox name="cbPaymentFrequency" /></td>
			<td valign=top><font size=2 color=3366cc><b>Tax Escrow:</b></font></td>
			<td valign=top><font size=2><jato:text name="stPropertyTax" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
	        <td valign=top><font size=2  color=3366cc><b>Allocate Tax Escrow</b></font>&nbsp;<jato:checkbox name="chAllocateTaxEscrow" styleClass="allocateTaxEscrow" onClick="turnOffOtherAllocateTaxEscrow(this)" /></font><td>
		</tr>
		<tr>
			<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Prepayment Penalty:</b></font></td>
			<td valign=top colspan=2><jato:combobox name="cbPrePaymentPenalty" /></td>
			<td valign=top><font size=2 color=3366cc><b>Total Payment:</b></font></td>
			<td valign=top><font size=2><jato:text name="stTotalPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
		</tr>
		<tr>
			<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Privilege Payment Option:</b></font></td>
			<td valign=top colspan=2><jato:combobox name="cbPrivilegePaymentOption" /></td>
			<td colspan=3 />
	    </tr>
		<tr>
			<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Commission Code:</b></font></td>
			<td valign=top colspan=2><jato:textField name="txCommissionCode" size="35" maxLength="35" /></td>
			<td valign=top><font size=2 color=3366cc><b>Holdback Amount:</b></font></td>
			<td valign=top><font size=2>$<jato:textField name="txHoldbackAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></font></td>
		</tr>
	    <tr>
			<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Cashback Percentage:</b></font></td>
			<td valign=top colspan=2><jato:textField name="txCashBackInPercentage" formatType="decimal" formatMask="###0.00; (-#)" size="6" maxLength="6" />%</td>
			<td valign=top><font size=2 color=3366cc><b>Rate Guarantee Period:</b></font></td>
			<td valign=top colspan=2><jato:textField name="txRateGuaranteePeriod" size="3" maxLength="3" /><font size=2 color=3366cc>&nbsp;<b>Days</b></font></td>
		</tr>
	    <tr>
		<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Cashback Amount:</b></font></td>
		<td valign=top><font size=2>$<jato:textField name="txCashbackAmount" formatType="decimal" formatMask="###0.00; (-#)" size="14" maxLength="14" /></font></td>
	        <td valign=top><font size=2 color=3366cc><b>Cashback $ Override</b></font>&nbsp;
	        <jato:checkbox name="chCashbackOverride" />
		</td>  
	        <td valign=top><font size=2 color=3366cc><b>Existing Account:</b></font></td>
	        <td valign=top colspan=2><jato:comboBox name="cbExistingAccountFlag" /></td>
	   </tr>
	   <tr>
	        <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Repayment Type:</b></font></td>
	        <td valign=top colspan=2><jato:combobox name="cbRepaymentType"  /></td> 
	        <td valign=top><font size=2 color=3366cc><b>Existing Account Reference:</b></font></td>
	        <td valign=top colspan=2><jato:textField name="txExistingAccountRef" size="35" maxLength="35"/></td>
	    </tr>
	    <tr>
	        <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Rate Locked In:</b></font></td>
	        <td valign=top colspan=2><jato:combobox name="cbRateLockedIn" /></td>
	        <td valign=top><font size=2 color=3366cc><b>First Payment Date:</b></font></td>
	        <td valign=top  colspan=2>
	      <font size="2">Mth: </font><jato:combobox name="cbFirstPaymentDateMonth" />
	            <font size="2">Day: </font><jato:textField name="txFirstPaymentDateDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" />
	            <font size="2">Yr: </font> <jato:textField name="txFirstPaymentDateYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" />
	            </td>
	        </tr>
	    <tr>
	        <td colspan=3 />
	        <td valign=top><font size=2 color=3366cc><b>Maturity Date:</b></font></td>
	        <td valign=top colspan=2><font size="2"><jato:text name="stMaturityDate"
	            escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
	    </tr>
	    <tr>
	        <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Additional Info:</b></font></td>
	        <td valign=top colspan=5><font size="2"><jato:textArea name="txMtgAdditionalInfo" rows="2" cols="100" 
	            extraHtml="wrap=soft maxlength=249 onchange='return isFieldHasSpecialChar();'"/></font></td>
	    </tr>
	    <tr>
	        <td colspan=6 align=right>
	        <jato:button name="btRecalcMTGComp" fireDisplayEvents="true" src="../images/recalculate.gif" onClick="setSubmitFlag(true); saveScrollPosition();"/>
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btDeleteMTGComp"extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete.gif" />
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	    </tr>
	</table>

	<%-- init parameters for dynamic combo box start--%>
    <jato:textField  name="stMtgProduct"  style="display: none"/>
    <jato:textField  name="stPostedInterestRate" style="display: none"/>
    <jato:textField  name="stRepaymentType" style="display: none"/>
    <jato:textField  name="stRateLock" style="display: none"/>
    <jato:textField  name="txPostedRate" style="display: none"/>
    <jato:staticText name="stInitJavaScript" escape="false" />
    <%-- init parameters for dynamic combo box ends--%>
	
	</jato:tiledView>
	<!-- MORTGATE COMPONENT TILE ENDS //-->

	<!-- LOC COMPONENT TILE STARTS //-->
    <jato:tiledView name="RepeatedLOC" type="mosApp.MosSystem.pgComponentDetailsLOCTiledView">
        <jato:hidden name="hdComponentIDLOC" />
		<jato:hidden name="hdCopyIDLOC" />
		<table border="0" width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
			
	        <colgroup>
	            <col width="230" align="left"/>
	            <col width="150" align="left"/>
	            <col width="250" align="left"/>
	            <col width="230" align="left"/>
	           <col width="150" align="left"/>
	            <col align="left"/>
	        </colgroup>
                  
	        <tr>
	           <td colspan=6 valign=top><jato:image name="imSectionDevider" /></b></font></td>
	        </tr>
	        <tr>
	           <td colspan=6 valign=top height="20"><font class="sectionTitle">&nbsp;&nbsp;&nbsp;<jato:text name="stTitle"  escape="false"/></font></td>
	        </tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Component Type: </b></font></td>
				<td valign=top colspan=2><font size=2><jato:combobox
					name="cbComponentTypeLOC" 
					styleClass="greyedDisabled" /></font></td>
				<td valign=top><font size=2 color=3366cc><b>Product: </b></font></td>
				<td valign=top colspan=2><font size=2><jato:combobox name="cbProductTypeLOC" /></font></td>
			</tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>LOC Amount: </b></font></td>
				<td valign=top colspan=2><font size=2>$<jato:textField
					name="tbLOCAmount" formatType="decimal" formatMask="###0.00;(-#)"
					size="14" maxLength="14" /></font></td>
				<td valign=top><font size=2 color=3366cc><b>Posted Interest Rate: </b></font></td>
				<td valign=top colspan=2><font size=2><jato:combobox  name="cbPostedInterestRateLOC" /></font></td>
			</tr>
               <tr>
                <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>MI Premium:</b></font></td>
                <td valign=top><font size=2><jato:text name="stMIPremiumLOC" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font></td>
                   <td valign=top><font size=2 color=3366cc><b>Allocate MI	Premium:</b></font>&nbsp;<jato:checkbox name="chAllocateMIPremiumLOC" styleClass="allocateMIPremium" onClick="turnOffOtherAllocateMIPremium(this)"/></td>  
                   <td valign=top><font size=2 color=3366cc><b>Discount:</b></font></td>
                   <td valign=top colspan=2><jato:textField name="tbDiscountLOC" formatType="decimal" formatMask="###0.000;(-#)"
					size="6" maxLength="6" />%</td>
               </tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Total	LOC Amount: </b></font></td>
				<td valign=top colspan=2><font size=2><jato:text
					name="stTotalLOCAmount" escape="true" formatType="currency"
					formatMask="$#,##0.00; (-#)" /></font></td>
				<td valign=top><font size=2 color=3366cc><b>Premium:
				</b></font></td>
				<td valign=top colspan=2><font size=2><jato:textField
					name="tbPremiumLOC" formatType="decimal" formatMask="###0.000;(-#)"
					size="6" maxLength="6" />%</font></td>
			</tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Payment Frequency:</b> </font></td>
				<td valign=top colspan=2><font size=2><jato:combobox
					name="cbPaymentFrequencyLOC" /></font></td>
				<td valign=top><font size=2 color=3366cc> <b>Net Rate: </b></font></td>
				<td valign=top colspan=2><font size=2><jato:text
					name="stNetRateLOC" escape="true" formatType="decimal"
					formatMask="###0.000;(-#)" />%</font></td>
			</tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Commission Code:</b> </font></td>
				<td valign=top colspan=2><font size=2><jato:textField
					name="tbCommissionCodeLOC" size="35" maxLength="35" /></font></td>
				<td valign=top><font size=2 color=3366cc> <b>Interest Only Payment:</b> </font></td>
				<td valign=top colspan=2><font size=2><jato:text
					name="stInterestOnlyPaymentLOC" escape="true" formatType="currency"
					formatMask="$#,##0.00; (-#)" /></font></td>
			</tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><B>Repayment Type: </B></font></td>
				<td valign=top colspan=2><font size=2><jato:combobox
					name="cbRepaymentTypeLOC" /></font></td>
				<td valign=top ><font size=2 color=3366cc> <B>Tax Escrow:</B> </font></td>
				<td valign=top><font size=2><jato:text
					name="stTaxEscrowLOC" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" />
				<td valign=top><font size=2 color=3366cc><B>Allocate Tax Escrow:</B></font><jato:checkbox name="chAllocateTaxEscrowLOC" styleClass="allocateTaxEscrow" onClick="turnOffOtherAllocateTaxEscrow(this)"/></font></td>
			</tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><B>Existing Account:</B> </font></td>
				<td valign=top colspan=2><font size=2><jato:combobox
					name="cbExistingAccountLOC" /></font></td>
				<td valign=top><font size=2 color=3366cc> <B>Total Payment: </B></font></td>
				<td valign=top colspan=2><font size=2><jato:text
					name="stTotalPaymentLOC" formatType="currency"
					formatMask="$#,##0.00; (-#)" /></font></td>
			</tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><B>Existing Account Reference: </B></font></td>
				<td valign=top colspan=2><font size=2><jato:textField
					name="tbExistingAccountRefLOC" size="35" maxLength="35" /></font></td>
				<td valign=top><font size=2 color=3366cc> <B>Holdback Amount: </B></font></td>
				<td valign=top colspan=2><font size=2>$<jato:textField
					name="tbHoldBackAmountLOC" formatType="decimal"
					formatMask="###0.00;(-#)" size="14" maxLength="14" /></font></td>
			</tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><B>First Payment Date:</B> </font></td>
				<td valign=top colspan=2><font size=2> Mth: </font><jato:combobox
					name="cbFirstPaymentMonthLOC" /> <font size=2>Day: </font> <jato:textField
					name="txFirstPaymentDayLOC" formatType="decimal"
					formatMask="###0;(-#)" size="2" maxLength="2" /> <font size=2>Yr:
				      </font> <jato:textField name="txFirstPaymentYearLOC" formatType="decimal"
					formatMask="###0;(-#)" size="4" maxLength="4" /></td>
				<td valign=top colspan="3"></td>
			</tr>
			<tr>
				<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><B>Additional Info:</B> </font></td>
				<td colspan=5 ><jato:textArea
					name="tbAdditionalInfoLOC" rows="2" cols="100" /></td>
			</tr>
			<tr>
				<td><font size="2"> &nbsp;</font></td>
			</tr>
			<tr>
				<td colspan=6 align=right><jato:button name="btRecalcLOCComp"
					fireDisplayEvents="true" src="../images/recalculate.gif"
					onClick="setSubmitFlag(true); saveScrollPosition();"/>
				&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btDeleteLOCComp"
					extraHtml="onClick = 'setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/delete.gif" />
				&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
		</table>
		
		<%-- init parameters for dynamic combo box start--%>
		<jato:textField  name="stProductLOC"  style="display: none"/>
		<jato:textField  name="stPostedInterestRate" style="display: none"/>
		<jato:textField  name="stRepaymentType" style="display: none"/>
		<jato:textField  name="txPostedRate" style="display: none"/>
		<jato:staticText name="stInitJavaScript" escape="false" />
		<%-- init parameters for dynamic combo box ends--%>
	</jato:tiledView>
	<!-- LOC COMPONENT TILE ENDS //-->

	<!-- CREDIT CARD COMPONENT TILE STARTS //-->
    <jato:tiledView name="CreditCardComp" type="mosApp.MosSystem.pgComponentCreditCardTiledView">
    
    <jato:hidden name="hdComponentIdCC" />
    <jato:hidden name="hdCopyIdCC" />
    <table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="d1ebff">

        <colgroup>
            <col width="230" align="left"/>
            <col width="150" align="left"/>
            <col width="250" align="left"/>
            <col width="230" align="left"/>
            <col width="150" align="left"/>
            <col align="left"/>
        </colgroup>
	    
	    <tr>
	       <td colspan=6 valign=top><jato:image name="imSectionDevider" /></b></font></td>
	    </tr>
	    <tr>
	       <td valign=top><font class="sectionTitle">&nbsp;&nbsp;&nbsp;<jato:text name="stTitle"  escape="false"/></font></td>
	       <%-- these TDs align columns --%><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	    </tr>
   		<tr>
		   <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color="3366cc"><b>Component Type:</b></font></td>
		   <td valign="top" colspan=2><font size=2><jato:combobox name="cbComponentType" extraHtml="class='greyedDisabled'" /></font></td>
		   <td valign="top"><font size=2 color="3366cc"><b>Product:</b></font></td>
		   <td valign="top" colspan=2><jato:combobox name="cbCreditCardProduct" /></td>
		</tr>
    	    <tr>
	        <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Credit Card Amount:</b></font></td>
	        <td valign="top" colspan=2><font size=2>$<jato:textField name="txCreditCardAmount"  formatType="decimal" formatMask="###0.00; (-#)" size="14" maxLength="14" /></font></td>
	        <td valign="top"><font size=2 color=3366cc><b>Interest Rate:</b></font></td>
	        <td valign="top" colspan=2><jato:combobox name="cbCreditCardInterestRate" /></td>
	    </tr>
	    <tr>
	        <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Additional Info:</b></font></td>
	        <td valign=top colspan=5><font size="2"><jato:textArea name="txAdditionalInfo" rows="2" cols="100" 
	            extraHtml="wrap=soft maxlength=249 onchange='return isFieldHasSpecialChar();'"/></font></td>
	    </tr>
	    <tr>
		<td><font size="2"> &nbsp;</font></td>
	</tr>
	<tr>
		<td colspan="6" align="right"><jato:button name="btRecalcCCComp"
			fireDisplayEvents="true" src="../images/recalculate.gif" onClick="setSubmitFlag(true); saveScrollPosition();"/>
		&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btDeleteCCComp"
			extraHtml="onClick = 'setSubmitFlag(true);'"
			fireDisplayEvents="true" src="../images/delete.gif" />
		&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
</table>

 <%-- init parameters for dynamic combo box start--%>
 <jato:textField  name="stMtgProduct"  style="display: none"/>
 <jato:textField  name="stInterestRate" style="display: none"/>
 <jato:textField  name="txPostedRate" style="display: none"/>
 <jato:staticText name="stInitJavaScript" escape="false" />
 <%-- init parameters for dynamic combo box ends--%>
        
</jato:tiledView>


	<!-- LOAN COMPONENT TILE STARTS //-->
        <jato:tiledView name="RepeatedLoan"
		type="mosApp.MosSystem.pgComponentDetailsLoanTiledView">
				<jato:hidden name="hdComponentIDLoan" />
				<jato:hidden name="hdCopyIDLoan" />
				<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

		            <colgroup>
		                <col width="230" align="left"/>
		                <col width="150" align="left"/>
		                <col width="250" align="left"/>
		                <col width="230" align="left"/>
		                <col width="150" align="left"/>
		                <col align="left"/>
		            </colgroup>
		            
			        <tr>
			           <td colspan=6 valign=top><jato:image name="imSectionDevider" /></b></font></td>
			        </tr>
			        <tr>
			           <td valign=top><font class="sectionTitle">&nbsp;&nbsp;&nbsp;<jato:text name="stTitle"  escape="false"/></font></td>
			           <%-- these TDs align columns --%><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
			        </tr>
					<tr>
						<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Component
						Type: </b></font></td>
						<td valign=top colspan=2><font size=2><jato:combobox
							name="cbComponentTypeLoan"
							styleClass="greyedDisabled" /></font></td>
						<td valign=top><font size=2 color=3366cc><b>Product:
						</b></font></td>
						<td valign=top colspan=2><font size=2><jato:combobox
							name="cbProductTypeLoan" /></font></td>
					</tr>
					<tr>
						<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Loan Amount: </b></font></td>
						<td valign=top colspan=2><font size=2>$<jato:textField
							name="tbLoanAmount" formatType="decimal" formatMask="###0.00;(-#)"
							size="14" maxLength="14" /></font></td>
						<td valign=top><font size=2 color=3366cc><b>Posted Interest Rate: </b></font></td>
						<td valign=top colspan=2><font size=2><jato:combobox
							name="cbPostedInterestRateLoan" /></font></td>
					</tr>
	                <tr>
	                    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Payment Term Description:</b></font></td>
	                    <td valign=top colspan=2><font size=2>
	                    <jato:textField name="tbPaymentTermDescriptionLoan" size="20" maxLength="35" styleClass="blueDisabled" readOnly="true"/></font></td>
	                    <td valign=top><font size=2 color=3366cc><b>Discount:</b></font></td>
	                    <td valign=top colspan=2><jato:textField name="tbDiscountLoan" formatType="decimal" formatMask="###0.000;(-#)"
							size="6" maxLength="6" />%</td>	                
					</tr>
					<tr>
			            <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Actual Payment Term:</b></font></td>
			               <td valign=top colspan=2>
			               <font size=2>Yrs: </font><jato:textField name="txActualPayYearsLoan" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" />
			               <font size=2>Mths: </font><jato:textField name="txActualPayMonthsLoan" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" />
			            </td>
						<td valign=top><font size=2 color=3366cc><b>Premium:
						</b></font></td>
						<td valign=top colspan=2><font size=2><jato:textField
							name="tbPremiumLoan" formatType="decimal" formatMask="###0.000;(-#)"
							size="6" maxLength="6" />%</font></td>		            
					</tr>
					<tr>
						<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Payment Frequency:</b> </font></td>
						<td valign=top colspan=2><font size=2><jato:combobox
							name="cbPaymentFrequencyLoan" /></font></td>
						<td valign=top><font size=2 color=3366cc> <b>Net
						Rate: </b></font></td>
						<td valign=top colspan=2><font size=2><jato:text
							name="stNetRateLoan" escape="true" formatType="decimal"
							formatMask="###0.000;(-#)" />%</font></td>
					</tr>
					<tr>
						<td valign=top colspan=3></td>
						<td valign=top><font size=2 color=3366cc> <B>P&I: </B></font></td>
						<td valign=top colspan=2><font size=2><jato:text
							name="stTotalPaymentLoan" formatType="currency"
							formatMask="$#,##0.00; (-#)" /></font></td>
					</tr>
					<tr>
						<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><B>Additional Info:</B> </font></td>
						<td colspan=5 ><jato:textArea
							name="tbAdditionalInfoLoan" rows="2" cols="100" /></td>
					</tr>
					<tr>
						<td><font size="2"> &nbsp;</font></td>
					</tr>
					<tr>
						<td colspan="6" align="right"><jato:button name="btRecalcLoanComp"
							fireDisplayEvents="true" src="../images/recalculate.gif" onClick="setSubmitFlag(true); saveScrollPosition();"/>
						&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btDeleteLoanComp"
							extraHtml="onClick = 'setSubmitFlag(true);'"
							fireDisplayEvents="true" src="../images/delete.gif" />
						&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				</table>
				
        <%-- init parameters for dynamic combo box start--%>
        <jato:textField  name="stMtgProduct"  style="display: none"/>
        <jato:textField  name="stInterestRate" style="display: none"/>
        <jato:textField  name="txPostedRate" style="display: none"/>
        <jato:staticText name="stInitJavaScript" escape="false" />
        <%-- init parameters for dynamic combo box ends--%>
        
			</jato:tiledView>
	<!-- LOAN COMPONENT TILE ENDS //-->


    <!-- XS_2.38 OVERDRAFT COMPONENT TILE STARTS //-->
    <jato:tiledView name="RepeatedOverdraft" type="mosApp.MosSystem.pgComponentDetailsOverdraftTiledView">
    
        <jato:hidden name="hdComponentId" />
        <jato:hidden name="hdCopyId" />
        
        <table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="d1ebff">
            <colgroup>
                <col width="230" align="left"/>
                <col width="150" align="left"/>
                <col width="250" align="left"/>
                <col width="230" align="left"/>
                <col width="150" align="left"/>
                <col align="left"/>
            </colgroup>
            
           <!-- ------------------------------------------------------------ -->
            <tr>
               <td colspan=6 valign=top><jato:image name="imSectionDevider" /></b></font></td>
            </tr>
            <tr>
               <td valign=top><font class="sectionTitle">&nbsp;&nbsp;&nbsp;<jato:text name="stTitle"  escape="false"/></font></td>
               <%-- these TDs align columns --%><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            </tr>
           <!-- ------------------------------------------------------------ -->
            <tr>
                <td valign="top">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <font size="2" color="3366cc"><b>Component Type:</b>
                    </font>
                </td>
                <td valign="top" colspan="2">
                    <jato:combobox name="cbComponentType" styleClass="greyedDisabled" />
                </td>
                <td valign="top">
                    <font size="2" color="3366cc"><b>Product:</b>
                    </font>
                </td>
                <td valign="top" colspan="2">
                    <jato:combobox name="cbProduct" />
                </td>
            </tr>
           <!-- ------------------------------------------------------------ -->
            <tr>
                <td valign="top">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <font size="2" color="3366cc"><b>Overdraft Amount: </b>
                    </font>
                </td>
                <td valign="top" colspan="2">
                    <font size="2">$<jato:textField name="txOverdraftAmount" formatType="decimal" formatMask="###0.00;(-#)" size="14" maxLength="14" /> 
                    </font>
                </td>
                <td valign="top">
                    <font size="2" color="3366cc"><b>Interest Rate: </b> 
                    </font>
                </td>
                <td valign="top" colspan="2">
                    <jato:combobox name="cbInterestRate" /> 
                </td>
            </tr>
           <!-- ------------------------------------------------------------ -->
            <tr>
                <td valign="top">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <font size="2" color="3366cc"><B>Additional Info:</B></font>
                </td>
                <td colspan="5">
                    <jato:textArea name="txAdditionalInfo" rows="2" cols="100" />
                </td>
            </tr>
           <!-- ------------------------------------------------------------ -->
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
           <!-- ------------------------------------------------------------ -->
            <tr>
                <td colspan="6" align="right">
                    <jato:button name="btRecalc" onClick="setSubmitFlag(true); saveScrollPosition();" fireDisplayEvents="true" src="../images/recalculate.gif" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <jato:button name="btDelete" onClick="setSubmitFlag(true);" fireDisplayEvents="true" src="../images/delete.gif" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
        </table>
        
        <%-- init parameters for dynamic combo box start--%>
        <jato:textField  name="stMtgProduct"  style="display: none"/>
        <jato:textField  name="stInterestRate" style="display: none"/>
        <jato:textField  name="txPostedRate" style="display: none"/>
        <jato:staticText name="stInitJavaScript" escape="false" />
        <%-- init parameters for dynamic combo box ends--%>
                
    </jato:tiledView>

    <!-- XS_2.38 OVERDRAFT COMPONENT TILE ENDS //-->

    <%-- ------------------ end line of component details --------------------- --%>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="d1ebff">
        <tr>
           <td><img  src="../images/blue_line.gif" width="100%" height="1"></td>
        </tr>
    </table>
    
</table>
<!--END COMPONENT DETAILS // -->

<!--BUTTONS //-->
<table border=0 width=100% cellpadding=0 cellspacing=0>
    <tr>
        <td align=center>
            <img src="../images/white.gif" width=10 height=20>
        </td>
    </tr>
    <tr><td align=right>
        <jato:button name="btCheckRule" fireDisplayEvents="true" src="../images/CheckRules.gif" onClick="setSubmitFlag(true); saveScrollPosition();"/>
        &nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btRecalc" fireDisplayEvents="true" src="../images/recalculateL.gif" onClick="setSubmitFlag(true); saveScrollPosition();"/>
        &nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btSubmit"extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" />
        <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" />       
        &nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="onClick =  \"if(!confirm(COMP_CANCEL_CHANGES) ) return false;  setSubmitFlag(true);\"" fireDisplayEvents="true" src="../images/cancel.gif" />
        <jato:button name="btCurrentCancel" 
                extraHtml="onClick =  \"if(!confirm(COMP_CANCEL_CHANGES) ) return false;  setSubmitFlag(true);\"" fireDisplayEvents="true" src="../images/cancelCurrentChanges.gif" />
    </td></tr>
</table>

</div>
</center>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopWithNoteSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>

<script language="javascript">
<!--
if(NTCP){
    document.dialogbar.left=55;
    document.dialogbar.top=79;
    document.toolpop.left=317;
    document.toolpop.top=79;
    document.pagebody.top=100;
    document.alertbody.top=100;
}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
    tool_click(4)
}
else{
    tool_click(5)
    location.href="#loadtarget"
}

if(pmGenerate=="Y")
{
    openPMWin();
}

//-->
</script>
</body>
</jato:useViewBean>
</HTML>
