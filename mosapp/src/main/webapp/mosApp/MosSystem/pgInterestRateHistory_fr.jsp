
<HTML>
<%@page info="pgInterestRateHistory" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgInterestRateHistoryViewBean">

<HEAD>
<TITLE>Historique des taux d�int�r�t</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgInterestRateHistory" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

	<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>
<!--
<form>
-->

<!-- START BODY OF PAGE//-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=16><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=16><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=16 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Historique des taux:</b></font></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Code:</b></font><br>
<font size=2><jato:text name="stCode" escape="true" formatType="string" formatMask="??????????" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Description:</b></font><br>
<font size=2><jato:text name="stDescription" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Statut du taux:</b></font><br>
<font size=2><jato:text name="stStatus" escape="true" formatType="string" formatMask="??????????" /></font></td>
<td valign=top colspan=3><font size=2 color=3366cc><b>Date de d�sactivation du taux:</b></font><br>
<font size=2><jato:text name="stDisabledDate" escape="true" /></font></td><tr>

<td colspan=16><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=16><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>En attente?</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Entr�e en vigueur:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Taux affich�:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Escompte maximale<br>autoris�e:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Meilleur<br>taux:</b></font></td>
<jato:text name="stIncludePrimeIndxSectionStart" escape="false" />
<td valign=top><font size=2 color=3366cc><b>Taux<br>d'introduction:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Indexe<br>de base:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Taux<br>de base:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Modif.<br>de base:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Taux<br>d�intr.<br>r�duit:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Taux<br>d'introduction<br>(jours):</b></font>
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" />
</td>
<tr>

<td colspan=16><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=16><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgInterestRateHistoryRepeated1TiledView">

<td colspan=16><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2><jato:text name="stPending" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stEffectiveDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy HH:mm" /></font></td>
<td valign=top><font size=2><jato:text name="stPostedRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stMaxDiscountRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBestRate" escape="true" /></font></td>
<jato:text name="stIncludePrimeIndxSectionStart" escape="false" />
<td valign=top><font size=2><jato:text name="stTeaserRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBaseIndex" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBaseRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBaseAdjustment" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stTeaserDiscount" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stTeaserTerms" escape="true" /></font>
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" />
</td>

<td colspan=16><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=16><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</jato:tiledView>
</table>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous_fr.gif" />&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2_fr.gif" /></td>
</table>


<!-- Sample : Submit and Cancel buttons //-->
<br><br>
 <table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" />&nbsp;&nbsp;</td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR>
<BR><jato:hidden name="hdBindDummy" />

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}


//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
