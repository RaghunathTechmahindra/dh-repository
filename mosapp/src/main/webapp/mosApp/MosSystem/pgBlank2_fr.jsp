
<HTML>
<%@page info="pgBlank2" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgBlank2ViewBean">

<HEAD>
<TITLE>Deal Notes</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function tool_click(n) {
	var itemtomove = (NTCP) ? document.pagebody : document.all.pagebody.style;
	var alertchange = (NTCP) ? document.alertbody:document.all.alertbody.style;

	if(n==1){
		var imgchange = "tool_goto"; var hide1img="tool_prev"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.gotobar : document.all.gotobar.style;
		var hideother1 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==2){
		var imgchange = "tool_prev"; var hide1img="tool_goto"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.dialogbar : document.all.dialogbar.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==3){
		var imgchange = "tool_tools"; var hide1img="tool_goto"; var hide2img="tool_prev";
		var itemtochange = (NTCP) ? document.toolpop : document.all.toolpop.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
	}
	if(n==1 || n==2 || n==3){
		if (itemtochange.visibility==VISIBLE) {
			itemtochange.visibility=HIDDEN;
			itemtomove.top = (NTCP) ? 100:110;
			changeImg(imgchange,'off');
			if(alertchange.visibility==VISIBLE){alertchange.top = (NTCP) ? 100:110;}
		} 
		else {
			itemtochange.visibility=VISIBLE;
			changeImg(imgchange,'on');
			if(n!=2){itemtomove.top=150;}
			else{itemtomove.top=290;}
			if(alertchange.visibility==VISIBLE){
				if(n!=2){alertchange.top =  150;}
				else{alertchange.top = 290;}
			}
			if(hideother1.visibility==VISIBLE){hideother1.visibility=HIDDEN; changeImg(hide1img,'off');}
			if(hideother2.visibility==VISIBLE){hideother2.visibility=HIDDEN; changeImg(hide2img,'off');}
		}
	}
	if(n==4){
		var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
		itemtoshow.visibility=VISIBLE;
	}
	if(n==5){
		var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
		var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
		itemtoshow.visibility=VISIBLE;
		if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
	}
}

if(document.images){
	tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on_fr.gif";
	tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto_fr.gif";
	tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on_fr.gif";
	tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev_fr.gif";
	tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on_fr.gif";
	tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools_fr.gif";
}

function changeImg(imgNam,onoff){
	if(document.images){
		document.images[imgNam].src = eval (imgNam+onoff+'.src');
	}
}

function OnTopClick(){
	self.scroll(0,0);
}

var pmWin;
var pmCont = '';

function openPMWin()
{
var numberRows = pmMsgs.length;

pmCont+='<head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  for(z=0;z<=numberRows-1;z++)
  {
	  if(pmMsgTypes[z]==0){var pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
	  if(pmMsgTypes[z]==1){var pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
	  writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
	  writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close_fr.gif" width=63 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].sessionUserId.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');
    
  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
	pmWin.document.open();
	pmWin.document.write(pmCont);
	pmWin.document.close();
	pmWin.focus();
}


function generateAM()
{
var numberRows = amMsgs.length;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
	amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	var writeRows='';

	for(z=0;z<=numberRows-1;z++)
  {
		if(amMsgTypes[z]==0){var amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
		if(amMsgTypes[z]==1){var amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
		writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
		writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}

//-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgBlank2" method="post">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    	<!--TOOLBAR//-->
    	<%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

	<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

<!-- START BODY OF PAGE//-->

<p>

<!-- Sample : Submit and Cancel buttons //-->
<br><br><br><br>
 <table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0'" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0'" src="../images/cancel_fr.gif" /></td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<table border=0 width=230 cellpadding=0 cellspacing=0 bgcolor=99ccff>
		<td>&nbsp&nbsp<font size=2><jato:href name="changePasswordHref">Changement de mot de passe</jato:href></font></td>
		<td rowspan=5 valign=top bgcolor="5e84cf"><a onclick="return IsSubmited();" href="javascript:tool_click(3);">
		<img src="../images/close_arrow.gif" width=14 height=42 border=0></a></td>
	<tr>
		<td><img src="../images/blue_line.gif" width=230 height=1 alt="" border="0"></td>
        <tr>
		<td>&nbsp&nbsp<font size=2><A onclick="return IsSubmited();" HREF="javascript:OpenCalculatorBrowser();tool_click(3);">Calculateurs</A></font></td>
        <tr>		
		<td><img src="../images/blue_line.gif" width=230 height=1 alt="" border="0"></td>		
	<tr>
		<td>&nbsp&nbsp<font size=2><jato:href name="toggleLanguageHref">Changer Langue</jato:href></font></td>
	<tr>
		<td valign=bottom><img src="../images/blue_line.gif" width=230 height=1 alt="" border="0"></td>
		<td bgcolor="5e84cf"><img src="../images/blue_line.gif" width=14 height=1 alt="" border="0"></td>		
</table>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<table border=0 width=414 cellpadding=0 cellspacing=0 bgcolor=99ccff>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href1"> </jato:href></font></td>
	<td rowspan=19 valign=top bgcolor="5e84cf"
		><A onclick="return IsSubmited();" HREF="javascript:tool_click(2);"
			><img src="../images/close_arrow.gif" width=14 height=42 alt="" border="0"></a></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href2"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href3"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href4"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href5"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href6"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href7"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href8"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href9"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href10"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
	<td bgcolor="5e84cf"><img src="../images/blue_line.gif" width=14 height=1 alt="" border="0"></td>
</table>
</div>


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
