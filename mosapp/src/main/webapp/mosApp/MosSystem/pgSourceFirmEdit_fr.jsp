
<HTML>
<%@page info="pgSourceFirmEdit" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgSourceFirmEditViewBean">

<HEAD>
<TITLE>Modification de la firme source</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}

-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgSourceFirmEdit" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="false" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
    <%@include file="/JavaScript/CommonHeader_fr.txt" %>  

<%@include file="/JavaScript/CommonToolbar_fr.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>    

</center>

<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

<jato:text name="stIncludeDSSstart" escape="false" />
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br> 
<jato:text name="stIncludeDSSend" escape="false" />

<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<td colspan=6><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
	<td colspan=6><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td><tr>
	<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;</b></font><font size=3 color=3366cc><b><jato:text name="stAction" escape="true" /></b></font></td><tr>
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	
	<td valign=top><font size=2 color=3366cc><b>Nom abr�g�:</b></font><br>
	<font size=2><jato:textField name="tbShortName" size="10" maxLength="10" /></font></td>

	<td valign=top><font size=2 color=3366cc><b>Statut:</b></font><br>
	<font size=2><jato:combobox name="cbStatus" /></font></td>

	<td valign=top nowarp><font size=2 color=3366cc><b>Type de syst�me:</b></font><br>
	<font size=2><jato:text name="stSystemType" escape="true" /></font></td>

	<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> client:</b></font><br>
	<font size=2><jato:textField name="tbClientNum" size="20" maxLength="20" /></font></td>
	
	<td valign=top><font size=2 color=3366cc><b>Autre code d�usager:</b></font><br>
	<font size=2>
    <jato:textField name="tbAlternativeId" size="20" maxLength="20" /></font></td><tr>
	
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=1 color=3366cc><b>Nom de la firme:</b></font><br>
	<font size=2><jato:textField name="tbFirmName" size="35" maxLength="35" /></font></td>
    <td colspan=1 valign=top><font size=2 color=3366cc><b>Num�ro d'enregistrement du permis:</b></font><br>
    <font size=2><jato:textField name="tbLicenseNumber" size="15" maxLength="15" /></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>Code de syst�me firme:</b></font><br>
	<font size=2><jato:textField name="tbSystemCode" size="20" maxLength="20" /></font></td>
	<td colspan=2 valign=top></td>

	<tr>
	
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

	<td colspan=6><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<!--START CONTACT INFORMATION//-->

	<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Coordonn�es</b></font></td><tr>
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
	
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign=top colspan=1><font size="2" color="3366cc"><b>Pr�nom:</b></font><br>
    <jato:textField name="tbContactFirstName" size="20" maxLength="20" /></td>
	<td valign=top colspan=1 nowrap><font size="2" color="3366cc"><b>Initiale:</b></font><br>
    <jato:textField name="tbContactMiddleInitial" size="1" maxLength="1" /></td>
	<td valign=top colspan=3><font size="2" color="3366cc"><b>Nom:</b></font><br>
    <jato:textField name="tbContactLastName" size="20" maxLength="20" /></td><tr>

	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone bureau:</b></font><br>
	( <jato:textField name="tbPhoneNumber1" size="3" maxLength="3" /> ) 
    <jato:textField name="tbPhoneNumber2" size="3" maxLength="3" /> - 
    <jato:textField name="tbPhoneNumber3" size="4" maxLength="4" /> <font size=2 color=3366cc><b>X</b></font> 
    <jato:textField name="tbPhoneExtension" size="6" maxLength="6" /></td>

	<td colspan=1 valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur:</b></font><br>
	( <jato:textField name="tbFaxNumber1" size="3" maxLength="3" /> ) 
    <jato:textField name="tbFaxNumber2" size="3" maxLength="3" /> - 
    <jato:textField name="tbFaxNumber3" size="4" maxLength="4" /></font></td>

	<td colspan=2 valign=top><font size=2 color=3366cc><b>Courriel:</b></font><br><font size="2">
    <jato:textField name="tbEmail" size="30" maxLength="50" fireDisplayEvents="true"/></font></td>
	
    <td valign=top colspan=2 height="74"><font size=2 color=3366cc><b>M�thode pr�f�r�e de distribution:</b></font><br>
    	<jato:combobox name="cbPrefDeliveryMethod" /></td><tr>    		
	
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign=top colspan=2><font size=2 color=3366cc><b>Adresse ligne 1:</b></font><br>
    <jato:textField name="tbAddressLine1" size="35" maxLength="35" /></td>
	<td valign=top colspan=3><font size=2 color=3366cc><b>Adresse ligne 2:</b></font><br>
    <jato:textField name="tbAddressLine2" size="35" maxLength="35" /></td><tr>

	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign=top><font size=2 color=3366cc><b>Ville:</b></font><br><font size="2">
    <jato:textField name="tbCity" size="20" maxLength="20" /></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>Province:</b></font><br><font size="2"><jato:combobox name="cbProvince" /></font></td>
	<td colspan=3 valign=top><font size=2 color=3366cc><b>Code postal:</b></font><br><font size="2">
    <jato:textField name="tbPostalCodeFSA" size="3" maxLength="3" /> 
    <jato:textField name="tbPostalCodeLDU" size="3" maxLength="3" /></font></td><tr>

	<td colspan=6><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>
	<td colspan=6><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

	<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Lien firme source � utilisateur</b></font></td><tr>
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
	
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=2 valign=top><font size=2 color=3366cc><b>Utilisateur :</b></font><br><font size="2"><jato:combobox name="cbAssociatedUser" fireDisplayEvents="true" /></font></td>
	<td colspan=2 valign=top><font size=2 color=3366cc><b>Entr�e en vigueur:</b></font><br>
	<font size="2">Mois: <jato:combobox name="cbAUserEffectiveMonth" /> Jour: 
    <jato:textField name="txAUserEffectiveDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> An: 
    <jato:textField name="txAUserEffectiveYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></font></td>
	<td valign=middle align="center"><jato:button name="btAUserUnassociate" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_association_fr.gif" /></td>

	<td colspan=6><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>
	<td colspan=6><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

	<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Lien de groupe de la firme de la source</b></font></td><tr>
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
	
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=2 valign=top><font size=2 color=3366cc><b>Nom du groupe:</b></font><br><font size="2"><jato:combobox name="cbAssociatedGroup" fireDisplayEvents="true" /></font></td>
	<td colspan=2 valign=top><font size=2 color=3366cc><b>Entr�e en vigueur:</b></font><br>
	<font size="2">Mois: <jato:combobox name="cbAUserEffectiveMonth" /> Jour: 
    <jato:textField name="txAGroupEffectiveDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> An: 
    <jato:textField name="txAGroupEffectiveYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></font></td>
	<td valign=middle align="center"><jato:button name="btAGroupUnassociate" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_association_fr.gif" /></td>

</table>

<BR>
	
<p>
<table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/cancel_fr.gif" />&nbsp;&nbsp;&nbsp;&nbsp;</td>
</table>
</center>
</div>	


<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
	document.addParty.top=100;
	
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
