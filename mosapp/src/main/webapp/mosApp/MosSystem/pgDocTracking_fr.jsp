
<HTML>
<%@page info="pgDocTracking" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDocTrackingViewBean">

<!--
07/Apr/2006 DVG #DG402 #2710  DJ - prod - too many documents created ....  
	- new check box to prevent creation of docs
-->

<HEAD>
<TITLE>Suivi des documents</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
.addConditionDialogStd{visibility:hidden; width:400;border-width:1pt;border-style: outset;border-color:"red"}
.addConditionDialogCust{visibility:hidden; width:400;border-width:1pt;border-style: outset;border-color:"red"}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/AddStandardCustomCond.js" type="text/javascript"></script>
<!--
//--Release2.1--_DTS_CR--start.
//// A user should be able to edit Standard/Custom conditions via DocumentTracking screen.
//// This js set is also provide the filtering of special characters for free form
//// condition content.
-->

<!--
//--Release2.1--_DTS_CR--end.
-->

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgDocTracking" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="true" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="true" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
	<!--QUICK LINK MENU BAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 
	
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
    <tr>
        <td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
    </tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>
<center>

<!--BODY OF PAGE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#d1ebff>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td>
<tr>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Nom du document:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Statut:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Date du statut du document:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Admin?</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Date due:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>R�le:</b></font></td>
<tr>
 <td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgDocTrackingRepeated1TiledView">
  <td valign=top colspan=1><jato:text name="stSignDocumentLabel" escape="true" /></td>
  <td valign=top colspan=1><jato:combobox name="cbSignDocStatus" /></td>
  <td valign=top colspan=1><jato:text name="stSignDocStatusDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></td>
  <td valign=top colspan=1><jato:text name="stSignByPass" escape="true" /></td>
  <td valign=top colspan=1><jato:text name="stSignDueDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></td>
  <td valign=top colspan=1><jato:text name="stSignResponsibility" escape="true" /></td>
  <td valign=top colspan=1><jato:button name="btSignDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details_fr.gif" /></td>
<tr>
  <td colspan=7><jato:hidden name="hdSignDocTrackId" /><jato:hidden name="hdSignRoleId" /><jato:hidden name="hdSignRequestDate" /><jato:hidden name="hdSignStatusDate" /><jato:hidden name="hdSignDocStatus" /><jato:hidden name="hdSignDocText" /><jato:hidden name="hdSignCopyId" /><jato:hidden name="hdSignResponsibility" /><jato:hidden name="hdSignDocumentLabel" />
  <img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>
</jato:tiledView>

<jato:tiledView name="Repeated2" type="mosApp.MosSystem.pgDocTrackingRepeated2TiledView">
  <td valign=top colspan=1><jato:text name="stOrdDocumentLabel" escape="true" /></td>
  <td valign=top colspan=1><jato:combobox name="cbOrdDocStatus" /></td>
  <td valign=top colspan=1><jato:text name="stOrdDocStatusDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></td>
  <td valign=top colspan=1><jato:text name="stOrdByPass" escape="true" /></td>
  <td valign=top colspan=1><jato:text name="stOrdDueDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></td>
  <td valign=top colspan=1><jato:text name="stOrdResponsibility" escape="true" /></td>
  <td valign=top colspan=1><jato:button name="btOrdDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details_fr.gif" /></td>
<tr>
  <td colspan=7><jato:hidden name="hdOrdDocTrackId" /><jato:hidden name="hdOrdRoleId" /><jato:hidden name="hdOrdRequestDate" /><jato:hidden name="hdOrdStatusDate" /><jato:hidden name="hdOrdDocStatus" /><jato:hidden name="hdOrdDocText" /><jato:hidden name="hdOrdCopyId" /><jato:hidden name="hdOrdResponsibility" /><jato:hidden name="hdOrdDocumentLabel" />
  <img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>

</jato:tiledView>

<td colspan=2 align=left><jato:button name="btPartySummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/party_summary_fr.gif" /></td>
<jato:text name="stDisplayDJCheckBoxStart" escape="false" /><td colspan=4 align=right><jato:checkbox name="ckbDJProduceCommitment" />&nbsp;<font size="2" color=3366cc><b>Imprimer lettres d'approbation sans conditions</b></font>&nbsp;&nbsp;</td><jato:text name="stDisplayDJCheckBoxEnd" escape="false" />
<td colspan=3 align=right><jato:checkbox name="ckProduceCondUpdateDoc" />&nbsp;<font size="2" color=3366cc><b>Produire une mise � jour des conditions pour le courtier</b></font>&nbsp;&nbsp;</td>
<tr>

<!-- Catherine, 7-Apr-05, pvcs#817  -->
<tr>
      <td colspan=7 align="left" valign=bottom><font color=3366cc><b>Note:</b></font></td>
</tr>
<tr>
  <td colspan=7 valign=top NOWRAP>
    <jato:textArea name="tbConditionNoteText" extraHtml="wrap=soft maxlength=249 onBlur='return isFieldHasSpecialChar();'" rows="4" cols="100" /></td>
</tr>
<!-- Catherine, 7-Apr-05, pvcs#817 end -->


<!-- #DG402 -->
<td colspan=6 align=right valign=middle> 
  <jato:text name="stDispDJProduceDocsStart" escape="false" />
  <jato:checkbox name="ckbDJProduceDocs" />&nbsp;<font size="2" color=3366cc>
  <b>Ne pas imprimer les documents</b></font>&nbsp;&nbsp;
  <jato:text name="stDispDJProduceDocsEnd" escape="false" />
</td>
<td colspan=1 align=right>
  <jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;
  <jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> 
  <jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" />
</td>
<!-- #DG402 end -->
<tr>

<td colspan=7><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td>
<tr>
<td colspan=7><font color=3366cc><b>Conditions du pr�pos� � l'avance de fonds:</b></font></td>
<tr>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Nom du document:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Statut:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Date du statut du document:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Admin?</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Date due:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>R�le:</b></font></td>
<tr>
 <td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>
<jato:tiledView name="Repeated3" type="mosApp.MosSystem.pgDocTrackingRepeated3TiledView">
  <td valign=top colspan=1><jato:text name="stFunderDocumentLabel" escape="true" /></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Approuv� </b></font><jato:checkbox name="ckFunderDocStatus" /></td>
  <td valign=top colspan=1><jato:text name="stFunderDocStatusDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></td>
  <td valign=top colspan=1><jato:text name="stFunderByPass" escape="true" /></td>
  <td valign=top colspan=1><jato:text name="stFunderDueDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></td>
  <td valign=top colspan=1><jato:text name="stFunderResponsibility" escape="true" /></td>
  <td valign=top colspan=1><jato:button name="btFunderDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details_fr.gif" /></td>
<tr>
  <td colspan=7><jato:hidden name="hdFunderDocTrackId" /><jato:hidden name="hdFunderCopyId" /><jato:hidden name="hdFunderRoleId" /><jato:hidden name="hdFunderRequestDate" /><jato:hidden name="hdFunderStatusDate" /><jato:hidden name="hdFunderDocStatus" /><jato:hidden name="hdFunderDocText" /><jato:hidden name="hdFunderResponsibility" /><jato:hidden name="hdFunderDocumentLabel" />
  <img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>

</jato:tiledView>

<td colspan=2 align=left><jato:button name="btPartySummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/party_summary_fr.gif" /></td>
<tr>
<td colspan=7 align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/sign_cancel_fr.gif" /> <jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
<tr>
 <td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>


<!-- Release2.1--TD_DTC_CR--start. A user should be able to edit Standard/Custom conditions via DocumentTracking screen //-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=5>
<table border=0 width=100% cellpadding=0 cellspacing=0  bgcolor=ffffff>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
</td>
</table>

<jato:text name="stIncludeStdCondSectionStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=7><jato:text name="stTargetStdCon" fireDisplayEvents="true" escape="false" /><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=7 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Conditions standard</b></font></td><tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<tr>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td colspan=2 valign=top><font size=2 color=3366cc><b>Condition</b></font></td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Action</b></font></td>
<tr>

<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<jato:tiledView name="Repeated4" type="mosApp.MosSystem.pgDocTrackingRepeated4TiledView">

<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdStdDocTrackId" /><jato:hidden name="hdStdCopyId" /></td>
<td valign=top ><jato:text name="stStdCondLabel" escape="true" /></td>
<td valign=top colspan=1></td>
<td valign=top ><jato:button name="btStdDetail" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details_fr.gif" /></td>
<td valign=top ><jato:combobox name="cbStdAction" extraHtml="disabled = true"/></td>
<td valign=top align=left>&nbsp;&nbsp;&nbsp;<jato:button name="btStdDeleteCondition" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_condition_fr.gif" /></td>
<tr>

<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</jato:tiledView>

</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td colspan=1 width="0%">
    <a href="javascript:openAddConditionDialog(1)"><img src="../images/add_stdcondition_fr.gif" ALIGN=TOP width=133 height=15 alt="" border="0"></a>&nbsp;&nbsp;
</td>
<td colspan=3>
    <div id=addConditionDialogStd class="addConditionDialogStd" name=addConditionDialogStd>
        <table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>

            <tr>
                <td align=left rowspan="2" valign="middle" width="0%" nowrap><font size="2" color="#3366CC"><b> Copier depuis :&nbsp; </b></font></td>
                <td align=left rowspan="2" valign="middle" width="0%"><jato:listbox name="lbStdCondition" size="1" multiple="true" /></td>
                <td align=left valign="top" width="100%"><img src= "../images/more_fr.gif" width=65  height=20 alt="" border="0"
                    onClick = "moreConditionDialog(1);" >  </td>
            </tr>
            <tr>
                <td align=right valign="bottom" nowrap width="100%" ><jato:button name="btStdAddCondition" extraHtml="width=65  height=20 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" /> <img src= "../images/cancel_fr.gif" width=60  height=20 alt="" border="0"
                    onClick = "closeAddConditionDialog(1);" >  </td>
            </tr>

        </table>
    </div>
</td>
<tr>

<td colspan=5>
<table border=0 width=100% cellpadding=0 cellspacing=0  bgcolor=ffffff>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td></tr>
</table>
</td>

</table>
<jato:text name="stIncludeStdCondSectionEnd" escape="false" />

<jato:text name="stTargetCustCon" fireDisplayEvents="true" escape="false" />

<jato:text name="stIncludeCustCondSectionStart" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Conditions personnalis�es</b></font></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<jato:tiledView name="Repeated5" type="mosApp.MosSystem.pgDocTrackingRepeated5TiledView">

<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdCusDocTrackId" /><jato:hidden name="hdCusCopyId" /></td>
  <td valign=top colspan=4><font size=2 color=3366cc><b>Condition</b></font></td>

<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign=top colspan=3>
  <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#D1EBFF>
  <td width="1%" rowspan="2">
  <jato:textArea name="tbCusCondition" rows="3" cols="100" />  </td>
  <td width="99%" align="left" valign="top">
  <img name="CustCondBtnLess" src= "../images/arrowy_left.gif" alt="Click to display less lines" border="0" onClick = "moreCustCondition(false);" >
  </td><tr>
  <td width="99%" align="left" valign="bottom">
  <img name="CustCondBtnMore" src= "../images/arrowy_right.gif" alt="Click to display more lines" border="0" onClick = "moreCustCondition(true);" >
  </td>
  </table>
  </td>
  <td valign=top align=left colspan=1>&nbsp;&nbsp;&nbsp;<jato:button name="btCusDeleteCondition" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_condition_fr.gif" /></td>

<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Nom du document:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Action:</b></font></td>
  <td valign=top><font size=2 color=3366cc><b>Responsabilit�:</b></font></td>
  <td valign=top colspan=1>&nbsp;</td>
<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign=top colspan=1>
  <jato:textField name="tbCusDocumentLabel" size="35" maxLength="35" /></td>
  <td valign=top colspan=1><jato:combobox name="cbCusAction" extraHtml="disabled = true"/></td>
  <td valign=top><jato:combobox name="cbCusResponsibility" /></td>
  <td valign=top colspan=1>&nbsp;</td>
<tr>
  <td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
<tr>
  <td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>

</jato:tiledView>

</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td colspan=1 width="0%">
    <a href="javascript:openAddConditionDialog(2)"><img src="../images/add_custcondition_fr.gif" ALIGN=TOP width=133 height=15 alt="" border="0"></a>&nbsp;&nbsp;
</td>
<td colspan=3>
    <div id=addConditionDialogCust class="addConditionDialogCust" name=addConditionDialogCust>
        <table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>
            <tr>
                <td align=left nowrap valign="middle" rowspan="3"><font size="2" color="#3366CC"><b> Copier depuis :</b></font><font size=2 color=3366cc>&nbsp;&nbsp;</font></td>

                <td align=left nowrap valign="middle" rowspan="3"><font size=2 ><jato:listbox name="lbCustCondition" size="1" multiple="true" /></font></td>
                <td align=left nowrap valign="top" colspan="2"> <img src= "../images/more_fr.gif" width=65  height=20 alt="" border="0"
                    onClick = "moreConditionDialog(2);" >  </td>
            </tr>
            <tr>
                <td align=left valign="middle" nowrap><font size=2 color=3366cc>&nbsp;
              </font><font size="2" color="#3366CC"><b>Language : </b></font>  </td>
                  <td align=left valign="middle" nowrap><font size=2 ><jato:combobox name="cbLanguagePreference" /></font>  </td>
            </tr>
            <tr>
                <td align=right valign="bottom" nowrap colspan="2"><jato:button name="btCusAddCondition" extraHtml="width=65  height=20 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" /> <img src= "../images/cancel_fr.gif" width=60  height=20 alt="" border="0"
                    onClick = "closeAddConditionDialog(2);" >  </td> </tr>
        </table>
    </div>
</td>
</table>

<table border=0 width=100%>
<td colspan=7 align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/sign_cancel_fr.gif" /> <jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
<tr>
 <td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
</table>

<jato:text name="stIncludeCustCondSectionEnd" escape="false" />

</center>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
    document.dialogbar.left=55;
    document.dialogbar.top=79;
    document.toolpop.left=317;
    document.toolpop.top=79;
    document.pagebody.top=100;
    document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
    tool_click(4)
}
else{
    tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
