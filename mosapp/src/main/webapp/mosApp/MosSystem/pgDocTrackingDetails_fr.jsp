
<HTML>
<%@page info="pgDocTrackingDetails" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDocTrackingDetailsViewBean">

<HEAD>
<TITLE>D�tails du Suivi du document</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/AddStandardCustomCond.js" type="text/javascript"></script>
<!--
//--Release2.1--_DTS_CR--start.
//// A user should be able to edit Standard/Custom conditions via DocumentTracking screen.
//// This js set also providess the filtering of special characters for free form
//// condition content.
-->
<!--
//--Release2.1--_DTS_CR--end.
-->

<SCRIPT LANGUAGE="JavaScript">
<!--
//==== Local JavaScript functions ================

////--Release2.1--TD_DTS_CR--start//
function moreCustConditionEngl(isMore)
{
	
	var kickedCtl =(NTCP) ? event.target : event.srcElement;
		
	// determine row (if repeated)
	var rowNdx = getRowNdx(kickedCtl);
	////alert("RowNdx: " + rowNdx);
	
	inputRowNdx = "";
	if (rowNdx != -1)
		inputRowNdx = "[" + rowNdx + "]_";	

	var theTextAreaCtl = eval("document.forms[0].elements['<%= viewBean.PAGE_NAME%>_" + "txEngDocText" + "']");
		
	////alert("TextAreaCtl: " + theTextAreaCtl);
	
	if(isMore == true)
	{
		theTextAreaCtl.rows += 3;
	}
	else
	{
		if(theTextAreaCtl.rows > 3)
			theTextAreaCtl.rows -= 3;
	}
}

function moreCustConditionFrench(isMore)
{
	var kickedCtl =(NTCP) ? event.target : event.srcElement;
	
	// determine row (if repeated)
	var rowNdx = getRowNdx(kickedCtl);
	////alert("RowNdx: " + rowNdx);

	inputRowNdx = "";
	if (rowNdx != -1)
		inputRowNdx = "[" + rowNdx + "]_";

	////var theTextAreaCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txFrenchDocText.length");
	//// More generic form.
	var theTextAreaCtl = eval("document.forms[0].elements['<%= viewBean.PAGE_NAME%>_" + "txFrenchDocText" + "']");
	
	////alert("TextAreaCtl: " + theTextAreaCtl);
	
	if(isMore == true)
	{
		theTextAreaCtl.rows += 3;
	}
	else
	{
		if(theTextAreaCtl.rows > 3)
			theTextAreaCtl.rows -= 3;
	}
}

function getRowNdx(changedSelect)
{
	var numElements = eval("document.forms[0]." + changedSelect.name + ".length");
	
	//Note : "undefined" not support fro JavaScript 1.2 or later versions 
	// if (numElements === undefined) 
	if (numElements == null)
		return 0;

	if (numElements == 1)
		return 0;

	for (i = 0; i < numElements; ++i)
	{
		var rowSelectInput = eval("document.forms[0]." + changedSelect.name + "[" + i + "]");

		if (rowSelectInput == changedSelect)
			return i;

	}
	
	return -1;
}
////--Release2.1--TD_DTS_CR--end//

//-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff onload="checkMIResponseLoop();">
<jato:form name="pgDocTrackingDetails" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="true" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="true" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<jato:hidden name="hdDocTrackId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
     <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
    
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

	<!--END HEADER//-->

<!-- START BODY OF PAGE//-->

<div id="pagebody" class="pagebody" name="pagebody">
<center>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<table border=0 width=100% bgcolor=d1ebff  color=ffffff cellpadding=0 cellspacing=0>


	<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<td colspan=5>&nbsp;</td>
	<tr>
	<td colspan=1 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;Demande n<sup>�</sup>:</b></font></td>
	<td colspan=1 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;Emprunteur principal:</b></font></td>
	<td colspan=1 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;Nom du document:</b></font></td>
	<td colspan=1 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;R�le:</b></font></td>
	<tr>
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stDealId" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stPrimaryBorrower" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stDocumentLabel" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stRole" escape="true" /></font></td>
	<tr>
	<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<td valign=top colspan=5><font size=4 color=3366cc>Coordonn�es</font></td><tr>
	<td colspan=5>&nbsp;</td><tr>
	<jato:text name="stCommentOpen" escape="false" />
	<td colspan=1 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;Nom:</b></font>
							 <font size=3 >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stContactName" escape="true" /></font></td>
	<tr>
	<td colspan=5>&nbsp;</td><tr>
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>N<sup>o</sup> de t�l�phone bureau:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>N<sup>o</sup> de t�l�copieur:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Adresse courriel:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<tr>
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stPhone" escape="true" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stFax" escape="true" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stEmail" escape="true" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<tr>
	<td colspan=5>&nbsp;</td><tr>
	<td colspan=1 valign=top>
		<font size=3 color=3366cc>&nbsp;&nbsp;<b>Adresse:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br>
		<font size=3 color=3366cc>&nbsp;&nbsp;line1:</font><font size=3>&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stAddrLine1" escape="true" /></font><br>
	</td>
	<td colspan=1 valign=top>&nbsp;
		<jato:text name="stBusinessIdPrefix" escape="false" /> <jato:text name="stBusinessId" escape="true" />	<jato:text name="stBusinessIdSuffix" escape="false" />
	</td>
	
	<td colspan=1 valign=top>&nbsp;
		<jato:text name="stPartyTypePrefix" escape="false" /> <jato:text name="stPartyType" escape="true" /> <jato:text name="stPartyTypeSuffix" escape="false" />
	</td>
	
	<td colspan=1 valign=top>&nbsp;
		<jato:text name="stContactCompanyNamePrefix" escape="false" /> <jato:text name="stContactCompanyName" escape="true" />	<jato:text name="stContactCompanyNameSuffix" escape="false" />
	</td>	
	<tr>
	<td colspan=2 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;line2:</font><font size=3>&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stAddrLine2" escape="true" /></b></font></td>
	<tr>
	<jato:text name="stCommentClose" escape="false" />
	
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgDocTrackingDetailsRepeated1TiledView">
	<td colspan=2 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;Nom:</b></font>
							 <font size=3 >&nbsp;&nbsp;<jato:text name="stApprContactName" escape="true" /></font>
	</td>
	<td colspan=3 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;Adresse de la propri�t�:</b></font></td>
	<tr>
	<td colspan=2 valign=top>&nbsp;</td>
	<td colspan=3 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stPropertyAddress" escape="true" /></font></td>
	<tr>
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>N<sup>o</sup> de t�l�phone bureau:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
	<td colspan=2 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>N<sup>o</sup> de t�l�copieur:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=2 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Adresse courriel:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<tr>
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stApprPhone" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
	<td colspan=2 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stApprFax" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=2 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stApprEmail" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<tr>
	<td colspan=2 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Adresse:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Code d�usager d�affaire:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Type d�intervenant:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Nom de la firme:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<tr>
	<td colspan=2 valign=top><font size=3 ><font size=3 color=3366cc>&nbsp;&nbsp;line1:</font>&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stApprAddrLine1" escape="true" /></font></td>
	
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stApprBusinessId" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stApprPartyType" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stApprContactCompanyName" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<tr>
	<td colspan=2 valign=top><font size=3 ><font size=3 color=3366cc>&nbsp;&nbsp;line2:</font>&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stApprAddrLine2" escape="true" /></font></td>
	<tr>
	<td colspan=5>&nbsp;</td><tr>
	<td colSpan=5><IMG alt="" border=0 height=2 src="../images/blue_line.gif" width=1></TD>
	<tr>
<jato:hidden name="hdEmptyLink" />
</jato:tiledView>

	<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<tr>
	<td valign=top colspan=1><font size=3 color=3366cc><b>Information sur le document</b></font></td>
	<tr>
	<td colspan=5>&nbsp;</td><tr>
	<td colspan=2 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Date de requ�te:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=2 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Date du statut du document:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=1 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Statut du document:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<tr>
	<td colspan=2 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stRequestDate" escape="true" formatType="date" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=2 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stDocStatusDate" escape="true" formatType="date" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<td colspan=1 valign=top><font size=3 >&nbsp;&nbsp;<jato:text name="stDocStatus" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>	
	<tr>
	<td colspan=5 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Texte du document en anglais:</b></font></td>	
	<tr>	
  	<tr>
	  <td valign=top colspan=5>
	  <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#D1EBFF>
	  <td width="1%" rowspan="2">
	  <jato:textArea name="txEngDocText" rows="3" cols="100" />  </td>  
	  <td width="99%" align="left" valign="top">
	  <img name="CustCondBtnLess" src= "../images/arrowy_left.gif" alt="Click to display less lines" border="0" onClick = "moreCustConditionEngl(false);" >
	  </td>
	  <tr>
	  <td width="99%" align="left" valign="bottom">
	  <img name="CustCondBtnMore" src= "../images/arrowy_right.gif" alt="Click to display more lines" border="0" onClick = "moreCustConditionEngl(true);" >
	  </td>  
	  </table>
	<tr>
	<td colspan=5 valign=top><font size=3 color=3366cc>&nbsp;&nbsp;<b>Texte du document en fran�ais:</b></font></td>
	<tr>
	<tr>
	  <td valign=top colspan=5>
	  <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#D1EBFF>
	  <td width="1%" rowspan="2">
	  <jato:textArea name="txFrenchDocText" rows="3" cols="100" />  </td>  
	  <td width="99%" align="left" valign="top">
	  <img name="CustCondBtnLess" src= "../images/arrowy_left.gif" alt="Click to display less lines" border="0" onClick = "moreCustConditionFrench(false);" >
	  </td>
	  <tr>
	  <td width="99%" align="left" valign="bottom">
	  <img name="CustCondBtnMore" src= "../images/arrowy_right.gif" alt="Click to display more lines" border="0" onClick = "moreCustConditionFrench(true);" >
	  </td>  
	  </table>
	<td colspan=5>&nbsp;</td><tr>
	<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
</table>

<table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/submit_fr.gif" />
		<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/cancel_fr.gif" />
</td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>


<BR><jato:hidden name="hdEmptyLinkMain" />

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
