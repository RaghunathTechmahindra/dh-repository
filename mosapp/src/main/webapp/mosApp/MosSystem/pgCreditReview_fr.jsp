
<HTML>
<%@page info="pgCreditReview" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgCreditReviewViewBean">

<HEAD>
<TITLE>Examen du cr�dit</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>


<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff onload="checkMIResponseLoop();">
<jato:form name="pgCreditReview" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</script>

<script>
var bWin;
var bWinContent = "";

function createBureauWindow()
{	

var bureauReport = '';

<jato:tiledView name="Repeated2" type="mosApp.MosSystem.pgCreditReviewRepeated2TiledView">
bureauReport += '<B>Report#&nbsp;';
bureauReport +=  Number(<jato:text name="hdRowNdx" escape="true" />).toString() + '</B>';
bureauReport += '&nbsp;&nbsp';
bureauReport += '<jato:text name="stCreditBureauReport" escape="false" /><BR>';
</jato:tiledView>
	
	bWinContent = "";
	bWinContent += "<HEAD><TITLE>Bureau View</TITLE><HEAD><BODY bgcolor=d1ebff>";
	bWinContent += "<form name=\"bureau_form\">" +
					"<table border=0 width=100% cellpadding=0 cellspacing=0>" +
					"<td valign=top><font size=2 >&nbsp;&nbsp;  </font></td>" +
					"<td align=left> " +
					bureauReport +	
					"</td><tr><td colspan=2 align=left><img src=\"../images/blue_line.gif\" width=100% height=1 border=\"0\"></td>"+
					"<tr><td colspan =2 align=right><BR>" +
					"<a href=\"javascript:window.close();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"../images/close_fr.gif\" width=63 height=25 alt=\"\" border=\"0\"></a></td></table>"+
					"</form></body>"
	
	
	if(typeof(bWin) == "undefined" || bWin.closed == true)
	{
  		bWin=window.open('/Mos/nocontent.htm','BureauWindow'+document.forms[0].<%= viewBean.PAGE_NAME%>_sessionUserId.value,
    		'resizable,scrollbars,status=yes,titlebar=yes,width=700,height=600,hotkeys,resizable');
   	}

	// For IE 5.00 or lower, need to delay a while before writing the message to the screen
	//		otherwise, will get an runtime exception - Note by Billy 17May2001
	var timer;
	timer = setTimeout("createBureauWindow2()", 500);

}

function createBureauWindow2()
{
	bWin.document.open();
	bWin.document.write(bWinContent);
	bWin.document.close();
	bWin.moveTo(100,100);
	bWin.focus();
}


var pWin;
var pWinContent = "";
var theFileName = 'FullAppraisal.pdf';
function createPDFWindow()
{	
	
	
	pWinContent = "";
	pWinContent += "<HEAD><TITLE>Credit Report</TITLE><HEAD><BODY bgcolor=d1ebff>";
	pWinContent += "<form name=\"bureau_form\">" +
					"<table border=0 width=100% cellpadding=0 cellspacing=0>" +
					"<td valign=top align=center><b><font size=4 color=3366cc>Credit Report<br></font></b></td><tr>" +
					"<tr><td colspan=2 align=left><img src=\"../images/blue_line.gif\" width=100% height=1 border=\"0\"><br></td><tr>"+
					"<td align=center> <embed width=800 height=600 src=\"/Mos/Doc/" + theFileName + "\"></td><tr>" +
					"<td colspan=2 align=left><img src=\"../images/blue_line.gif\" width=100% height=1 border=\"0\"><br></td><tr>"+
					"<td colspan =2 align=right><br>" +
					"<a href=\"javascript:window.close();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"../images/close_fr.gif\" width=86 height=25 alt=\"\" border=\"0\"></a></td></table>"+
					"</form></body>"

	if(typeof(pWin) == "undefined" || pWin.closed == true)
	{
  		pWin=window.open('/Mos/nocontent.htm','PDFWindow'+document.forms[0].<%= viewBean.PAGE_NAME%>_sessionUserId.value,
    		'resizable,scrollbars,status=yes,titlebar=yes,width=850,height=700,hotkeys,resizable');
   	}
   	
	// For IE 5.00 or lower, need to delay a while before writing the message to the screen
	//		otherwise, will get an runtime exception - Note by Billy 17May2001
	var timer;
	timer = setTimeout("createPDFWindow2()", 500);
}

function createPDFWindow2()
{
	pWin.document.open();
	pWin.document.write(pWinContent);
	pWin.document.close();
	pWin.focus();
}



</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
    
        <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %>
	
<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<table border=0 width=100% cellpadding=2 cellspacing=0 bgcolor=d1ebff>
<td align=center colspan=6
		><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
<tr>
<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Examen du cr�dit:</b></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgCreditReviewRepeated1TiledView">

	<td valign=top>&nbsp;&nbsp;<jato:hidden name="hdBorrowerId" /></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Nom du demandeur:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Emprunteur principal?</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Type de demandeur:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Client existant?</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Notes du client existant:</b></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stBorrowerName" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stPrimaryBorrowerFlag" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stBorrowerType" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stExistingClient" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:textField name="tbExistingClientComments" size="35" maxLength="35" /></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>ABD:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>ABD (Taux Admissible):</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>ATD:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>ATD (Taux Admissible):</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Valeur nette:</b></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stGDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stGDS3Year" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stTDS" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stTDS3Year" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stNetWorth" escape="true" formatType="currency" formatMask="#,##0.00; -#" /></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Nom du bureau de cr�dit:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Date du bureau de cr�dit:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Date d'extraction du bureau de cr�dit:</b></font></td>
    <td colspan=1 valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Date d'autorisation:</b></font></td>
    <td colspan=1 valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;M�thode d'autorisation:</b></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:combobox name="cbBureauName" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;Mth:&nbsp;<jato:combobox name="cbBureauMonth" />&nbsp;&nbsp;Day:<jato:textField name="tbBureauDay" size="2" maxLength="2" />&nbsp;&nbsp;Yr:<jato:textField name="tbBureauYear" size="4" maxLength="4" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;Mth:&nbsp;<jato:combobox name="cbBureauPulledMonth" />&nbsp;&nbsp;Day:<jato:textField name="tbBureauPulledDay" size="2" maxLength="2" />&nbsp;&nbsp;Yr:<jato:textField name="tbBureauPulledYear" size="4" maxLength="4" /></font></td>
    <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;Mth:&nbsp;<jato:combobox name="cbAuthorizationMonth" />&nbsp;&nbsp;Day:<jato:textField name="tbAuthorizationDay" size="2" maxLength="2" />&nbsp;&nbsp;Yr:<jato:textField name="tbAuthorizationYear" size="4" maxLength="4" /></font></td>
    <td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:textField name="tbAuthorizationMethod" size="30" maxLength="30" /></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=4 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>R�capitulatif du bureau de cr�dit:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Cote de cr�dit:</b></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=4 valign=top><font size=2 >&nbsp;&nbsp;<jato:textArea name="tbBureauSummary" rows="4" cols="80" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:textField name="tbCreditScore" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Nbre de faillites:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Statut de faillite:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Historique de ch�ques sans provision:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Historique de paiement:</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Statut g�n�ral:</b></font></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:textField name="tbTimesBankrupt" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:combobox name="cbBankruptStatus" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:combobox name="cbNSFHistory" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:combobox name="cbPaymentHistory" /></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:combobox name="cbGeneralStatus" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>

</jato:tiledView>

</table>

<table border=0 width=100%>
<jato:text name="stBureauViewButton" escape="false" /> 
<td align=right><jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();"></td><tr>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR>


</jato:form>

<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
