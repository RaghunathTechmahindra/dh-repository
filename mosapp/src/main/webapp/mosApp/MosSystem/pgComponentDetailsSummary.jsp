<%--
 /**Title: pgComponentDetailsSummary.jsp
 * MCM Impl Team.
 * Description:Component Deatils Summary Blank Screen.
 * @version 1.0 Initial Version
 * Change Log:
 * @version 1.1 17-June-2008 XS_16.15 Added Loan Information Section
 * Change Log:
 * @version 1.2 20-June-2008 XS_16.13 Added Mortgage Information Section
 * Change Log:
 * @version 1.3 23-June-2008 XS_16.12 Updated form name to pgComponentDetailsSummary and removed submitter java script function.
 * Change Log:
 * @version:1.4 24-June-2008 XS_16.14 Added LOC Component Section. 
 * @version:1.5 25-June-2008 XS_16.16 Added Credit Card and Overdraft Component Section.
 * @version:1.6 30-June-2008 Bug Fix:artf733316 - Effective amortization and Additional Details values display. 
 * @version:1.7 03-July-2008 Bug Fix:artf733255 - Removes extra line after the credit card section.
 * @version 1.8 23-Aug-2008 Bug Fix : - Added doller symbol for Total Mortgage amount and Total Payment.
*/ 
--%>
<HTML>
<%@page info="pgComponentDetailsSummary" language="java"%>

<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgComponentDetailsSummaryViewBean">

<HEAD>
<TITLE>Component Details Summary</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">

</SCRIPT>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgCompoenentDetailsSummary" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
<%@include file="/JavaScript/CommonHeader_en.txt" %>  
<%@include file="/JavaScript/CommonToolbar_en.txt" %>  
<%@include file="/JavaScript/TaskNavigater.txt" %>   
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<jato:text name="stIncludeDSSstart" escape="false" />
<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br> 
<jato:text name="stIncludeDSSend" escape="false" />
<!--END DEAL SUMMARY SNAPSHOT//-->


<!-- START BODY OF PAGE//-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=6><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<tr>
<td><font  size=3 color=3366cc><b>Component Details</font></td></tr>
<tr>
<td colspan=10>
<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
</td>
</tr>
<%--
 /*
  * @version:1.0 16-June-2008 XS_16.13  Mortgage Component Section Starts. 
  */
--%>

<jato:tiledView name="RepeatedMortgageComponents" type="mosApp.MosSystem.pgDealSummaryComponentMortgageTiledView">
<tr>
<td><font  size=2 color=3366cc><b>Component Type:</font></td>
<td><font  size=2 color=3366cc><b>Payment Term Description:</font></td>
<td><font  size=2 color=3366cc><b>Cashback Amount:</font></td>
<td><font  size=2 color=3366cc><b>Buydown Rate:</font></td>
<td><font  size=2 color=3366cc><b>Holdback Amount:</font></td>
<tr>
<td width="14%"><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPaymentTermDescription" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stCashbackAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stBuyDownRate"  formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
<td><font size=2><jato:text name="stHolbackAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>

<tr>
<td><font  size=2 color=3366cc><b>Mortgage Amount:</font></td>
<td><font  size=2 color=3366cc><b>Actual Payment Term:</font></td>
<td><font  size=2 color=3366cc><b>Repayment Type:</font></td>
<td><font  size=2 color=3366cc><b>Net Rate:</font></td>
<td><font  size=2 color=3366cc><b>Rate Guarantee Period:</font></td>
<tr>
<td width="14%"><font size=2><jato:text name="stMortgageAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stActualPaymentTermYrs" fireDisplayEvents="false" escape="true" />&nbsp;Yrs&nbsp;&nbsp;&nbsp;</font>
<font size=2><jato:text name="stActualPaymentTermMths" fireDisplayEvents="false" escape="true" />&nbsp;Mths</font></td>
<td><font size=2><jato:text name="stRepaymentType" fireDisplayEvents="false" escape="true"  /></font></td>
<td><font size=2><jato:text name="stNetRate"  formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
<td><font size=2><jato:text name="stRateGuaranteePeriod" formatType="decimal" formatMask="###0; (-#)" escape="true" /></font></td>

<tr>
<td><font  size=2 color=3366cc><b>MI Premium:</font></td>
<td><font  size=2 color=3366cc><b>Payment Frequency:</font></td>
<td><font  size=2 color=3366cc><b>Rate Locked In?:</font></td>
<td><font  size=2 color=3366cc><b>P&I Payment:</font></td>
<td><font  size=2 color=3366cc><b>Existing Account:</font></td>
<tr>
<td width="14%"><font size=2><jato:text name="stMiPremium" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true"/></font></td>
<td><font size=2><jato:text name="stPaymentFrequency" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stRateLockedIn" fireDisplayEvents="false" escape="true"  /></font></td>
<td><font size=2><jato:text name="stPAndIPayment" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stExistingAccount" fireDisplayEvents="false" escape="true"/></font></td>


<tr>
<td><font  size=2 color=3366cc><b>Allocate MI Premium:</font></td>
<td><font  size=2 color=3366cc><b>Prepayment Option:</font></td>
<td><font  size=2 color=3366cc><b>Product:</font></td>
<td><font  size=2 color=3366cc><b>Additional Principal Payment:</font></td>
<td><font  size=2 color=3366cc><b>Existing Account Reference:</font></td>
<tr>
<td width="14%"><font size=2><jato:text name="stAllocateMIPremium" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPrePaymentOption" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true"  /></font></td>
<td><font size=2><jato:text name="stAdditionalPrincipalPayment" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stExistingAccountReference" fireDisplayEvents="false" escape="true"/></font></td>

<tr>
<td><font  size=2 color=3366cc><b>Total Mortgage Amount:</font></td>
<td><font  size=2 color=3366cc><b>Privilege Payment Option:</font></td>
<td><font  size=2 color=3366cc><b>Posted Rate:</font></td>
<td><font  size=2 color=3366cc><b>Allocate Property Tax:</font></td>
<td><font  size=2 color=3366cc><b>First Payment Date:</font></td>
<tr>
<td width="14%"><font size=2><jato:text name="stAmount" fireDisplayEvents="false" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true"/></font></td>
<td><font size=2><jato:text name="stPrivilegePaymentOption" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPostedRate" formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
<td><font size=2><jato:text name="stAllowTaxEscrow" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stFirstPaymentDate" fireDisplayEvents="false" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>


<tr>
<td><font  size=2 color=3366cc><b>Amortization Period:</font></td>
<td><font  size=2 color=3366cc><b>Commission Code:</font></td>
<td><font  size=2 color=3366cc><b>Discount:</font></td>
<td><font  size=2 color=3366cc><b>Tax Escrow:</font></td>
<td><font  size=2 color=3366cc><b>Maturity Date:</font></td>
<tr>
<td><font size=2><jato:text name="stAmortizationYrs" fireDisplayEvents="false" escape="true" />&nbsp;Yrs&nbsp;&nbsp;&nbsp;</font>
<font size=2><jato:text name="stAmortizationMths" fireDisplayEvents="false" escape="true" />&nbsp;Mths</font></td>
<td><font size=2><jato:text name="stCommissionCode" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stDiscount"  formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
<td><font size=2><jato:text name="stTaxEscrow" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true"/></font></td>
<td><font size=2><jato:text name="stMaturityDate" fireDisplayEvents="false" escape="true" formatType="date" formatMask="MMM dd yyyy"/></font></td>
<tr>
<td><font  size=2 color=3366cc><b>Effective Amortization:</font></td>
<td><font  size=2 color=3366cc><b>Cashback Percentage:</font></td>
<td><font  size=2 color=3366cc><b>Premium:</font></td>
<td><font  size=2 color=3366cc><b>Total Payment:</font></td>
</tr>

<tr>
<td><font size=2><jato:text name="stEffectiveAmortizationYrs" fireDisplayEvents="false" escape="true" />&nbsp;Yrs&nbsp;&nbsp;&nbsp;</font>
<font size=2><jato:text name="stEffectiveAmortizationMths" fireDisplayEvents="false" escape="true" />&nbsp;Mths</font></td>
<td><font size=2><jato:text name="stCashbackPercetage" formatType="decimal" formatMask="#0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stPremium"  formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
<td><font size=2><jato:text name="stTotalPayment" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
</tr>
<tr>
<td><font  size=2 color=3366cc><b>Additional Details:</font></td>
</tr>
<tr>
<td><font size=2><jato:text name="stAdditionalDetails" fireDisplayEvents="false" escape="true"/></font></td>
</tr>
<tr>
		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
</tr>

</jato:tiledView>
<%--
 /*
  * @version:1.0 16-June-2008 XS_16.13  Mortgage Component Section Ends. 
  */
--%>
<%--
 /*
  * @version:1.4 24-June-2008 XS_16.14  LOCComponent Section Starts. 
  */
--%>

<jato:tiledView name="RepeatedLOCComponents" type="mosApp.MosSystem.pgDealSummaryComponentLocTiledView">
<tr>
	<td><font  size=2 color=3366cc><b>Component Type:</font></td>
	<td><font  size=2 color=3366cc><b>Commission Code:</font></td>
	<td><font  size=2 color=3366cc><b>Existing Account Reference:</font></td>
	<td><font  size=2 color=3366cc><b>Discount:</font></td>
	<td><font  size=2 color=3366cc><b>Allocate Tax Escrow:</font></td>
</tr>
<tr>
	<td width="14%"><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
	<td><font size=2><jato:text name="stCommissionCode" fireDisplayEvents="false" escape="true" /></font></td>
	<td><font size=2><jato:text name="stExistingAccountReference" fireDisplayEvents="false" escape="true" /></font></td>
	<td><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
	<td><font size=2><jato:text name="stAllocateTaxEscrow" fireDisplayEvents="false" escape="true" /></font></td>
</tr>

<tr>
	<td><font  size=2 color=3366cc><b>LOC Amount:</font></td>
	<td><font  size=2 color=3366cc><b>Repayment Type:</font></td>
	<td><font  size=2 color=3366cc><b>First Payment Date:</font></td>
	<td><font  size=2 color=3366cc><b>Premium:</font></td>
	<td><font  size=2 color=3366cc><b>Tax Escrow:</font></td>
</tr>

<tr>
	<td width="14%"><font size=2><jato:text name="stLocAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
	<td><font size=2><jato:text name="stRepaymentType" fireDisplayEvents="false" escape="true" /></font></td>
	<td><font size=2><jato:text name="stFirstPaymentDate" fireDisplayEvents="false" escape="true" formatType="date" formatMask="MMM dd yyyy"/></font></td>
	<td><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
	<td><font size=2><jato:text name="stTaxEscrow" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
</tr>

<tr>
	<td><font  size=2 color=3366cc><b>MI Premium:</font></td>
	<td><font  size=2 color=3366cc><b>Allocate MI Premium:</font></td>
	<td><font  size=2 color=3366cc><b>Product:</font></td>
	<td><font  size=2 color=3366cc><b>Net Rate:</font></td>
	<td><font  size=2 color=3366cc><b>Total Payment:</font></td>
</tr>

<tr>
	<td width="14%"><font size=2><jato:text name="stMIPremium" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true"/></font></td>
	<td><font size=2><jato:text name="stAllocateMIPremium" fireDisplayEvents="false" escape="true" /></font></td>
	<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
	<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
	<td><font size=2><jato:text name="stTotalPayment" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true"/></font></td>
</tr>

<tr>
	<td><font  size=2 color=3366cc><b>Total LOC Amount:</font></td>
	<td><font  size=2 color=3366cc><b>Existing Account:</font></td>
	<td><font  size=2 color=3366cc><b>Posted Rate:</font></td>
	<td><font  size=2 color=3366cc><b>Interest Only Payment:</font></td>
	<td><font  size=2 color=3366cc><b>Holdback Amount:</font></td>
</tr>

<tr>
	<td width="14%"><font size=2><jato:text name="stTotalLOCAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true"/></font></td>
	<td><font size=2><jato:text name="stExistingAccount" fireDisplayEvents="false" escape="true" /></font></td>
	<td><font size=2><jato:text name="stPostedRate" formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
	<td><font size=2><jato:text name="stInterestOnlyPayment" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true"/></font></td>
	<td><font size=2><jato:text name="stHoldbackAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
</tr>

<tr>
	<td><font  size=2 color=3366cc><b>Payment Frequency:</font></td>
</tr>

<tr>
	<td width="14%"><font size=2><jato:text name="stPaymentFrequency" fireDisplayEvents="false" escape="true" /></font></td>
</tr>

<tr>
	<td><font  size=2 color=3366cc><b>Additional Details:</font></td>
<tr>
	<td width="14%"><font size=2><jato:text name="stAdditionalDetails" fireDisplayEvents="false" escape="true" /></font></td>
</tr>	
<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
</tr>
</jato:tiledView>
<%--
 /*
  * @version:1.4 24-June-2008 XS_16.14  LOCComponent Section Ends
  */
--%>


<%--
 /*
  * @version:1.0 16-June-2008 XS_16.15  Loan Component Section Ends. 
  */
--%>

<%--
 /*
  * @version:1.4 24-June-2008 XS_16.16  Credit Card Component Section Starts. 
  */
--%>
<jato:tiledView name="RepeatedCreditCardComponents" type="mosApp.MosSystem.pgDealSummaryComponentCreditCardTiledView">
<tr>
    <td><font  size=2 color=3366cc><b>Component Type:</font></td>
	<td><font  size=2 color=3366cc><b>Credit Card Amount:</font></td>
	<td><font  size=2 color=3366cc><b>Product:</font></td>
	<td><font  size=2 color=3366cc><b>Interest Rate:</font></td>
</tr>
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
</tr>	

<tr>	
	<td><font  size=2 color=3366cc><b>Additional Details:</font></td>
</tr>
<tr>
<td><font size=2><jato:text name="stAdditionalInfo" fireDisplayEvents="false" escape="true" /></font></td>
</tr>
<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
</tr>
</jato:tiledView>

<%--
 /*
  * @version:1.0 16-June-2008 XS_16.15  Loan Component Section Starts. 
  */
--%>
<jato:tiledView name="RepeatedLoanComponents" type="mosApp.MosSystem.pgDealSummaryComponentLoanTiledView">
<tr>
<td><font  size=2 color=3366cc><b>Component Type:</font></td>
<td><font  size=2 color=3366cc><b>Payment Term Description:</font></td>
<td><font  size=2 color=3366cc><b>Payment Frequency:</font></td>
<td><font  size=2 color=3366cc><b>Posted Rate:</font></td>
<td><font  size=2 color=3366cc><b>Premium:</font></td>
<tr>
<td width="14%"><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPaymentTermDescription" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPaymentFrequency" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPostedRate" formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
<td><font size=2><jato:text name="stPremium"  formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
<tr>
<td><font  size=2 color=3366cc><b>Loan Amount:</font></td>
<td><font  size=2 color=3366cc><b>Actual Payment Term:</font></td>
<td><font  size=2 color=3366cc><b>Product:</font></td>
<td><font  size=2 color=3366cc><b>Discount:</font></td>
<td><font  size=2 color=3366cc><b>Net Rate:</font></td>
<tr>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stActualPaymentTermYear" fireDisplayEvents="false" escape="true" />&nbsp;Yrs&nbsp;&nbsp;&nbsp;</font>
<font size=2><jato:text name="stActualPaymentTermMonth" fireDisplayEvents="false" escape="true" />&nbsp;Mths</font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stDiscount"  formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
<tr>
<td><font  size=2 color=3366cc><b>Total Payment:</font></td>
<tr>
<td><font size=2><jato:text name="stTotalPayment" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true"/></font></td>
<tr>
<td><font  size=2 color=3366cc><b>Additional Details:</font></td>
<tr>
<td><font size=2><jato:text name="stAdditionalDetails" fireDisplayEvents="false" escape="true" /></font></td>
</tr>
<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
</tr>
</jato:tiledView>

<%--
 /*
  * @version:1.4 24-June-2008 XS_16.16  Credit Card Component Section Ends. 
  */
--%>

<%--
 /*
  * @version:1.4 24-June-2008 XS_16.16  Overdraft Component Section Starts. 
  */
--%>
<jato:tiledView name="RepeatedOverDraftComponents" type="mosApp.MosSystem.pgDealSummaryComponentOverDraftTiledView">
<tr>
    <td><font  size=2 color=3366cc><b>Component Type:</font></td>
	<td><font  size=2 color=3366cc><b>Overdraft Amount:</font></td>
	<td><font  size=2 color=3366cc><b>Product:</font></td>
	<td><font  size=2 color=3366cc><b>Interest Rate:</font></td>
</tr>
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="$#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="###0.000;-#" escape="true"/></font></td>
</tr>
<tr>	
	<td><font  size=2 color=3366cc><b>Additional Details:</font></td>
</tr>
<tr>
<td><font size=2><jato:text name="stAdditionalInfo" fireDisplayEvents="false" escape="true" /></font></td>
</tr>
<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
</tr>
</jato:tiledView>
<%--
 /*
  * @version:1.4 24-June-2008 XS_16.16  Overdraft Component Section Ends. 
  */
--%>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 >
<tr>
<td width="25%">&nbsp;</td>
<td align="right" colspan="10"><jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" /></td>
</tr>

</table>
</center>

</div>
<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>