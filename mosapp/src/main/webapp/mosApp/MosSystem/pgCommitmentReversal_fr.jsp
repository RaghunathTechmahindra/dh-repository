
<HTML>
<%@page info="pgCommitmentReversal" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgCommitmentReversalViewBean">

<HEAD>
<TITLE>Notes de la demande</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgCommitmentReversal" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="detectAlertTasks" />
<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor="003366">
	<td><table border=0 width=100% cellpadding=0 cellspacing=0><td valign=top width=5%>
				<img src="../images/bx_logo_fr.gif" width=131 height=29 alt="" border="0"></td>
				<td valign=middle><font size=4 color=ffcc33><jato:text name="stPageLabel" escape="true" /></font></td>
				<td align=right valign=middle><font color=ffffff>
					<font size=3><b><jato:text name="stCompanyName" escape="true" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
					</font></td>
			<tr>
				<td colspan=3 valign=top>
					<img src="../images/top_line.gif" width=100% height=2 alt="" border="0"></td>
			</table></td>
	<td align=right background="../images/top_spacer.gif" rowspan=2 width=5%>
	
<script language="javascript">
//--Release2.1--//		
// Include Common Applet Functions (short version, no Lender/Product/Rate module)
<%@include file="/JavaScript/AppletCall.txt" %>
</script></td>

<tr>
	<td bgcolor=ffffff align=center valign=top>
		<table border=0 width=100% cellpadding=0 cellspacing=0>
			<td><font size=2 color=003366>&nbsp;&nbsp;<jato:text name="stTodayDate" escape="true" /></font></td>
			<td align=left><font size=3 color=003366><b><jato:text name="stUserNameTitle" escape="true" /></b></font></td>
		</table></td>
</table>

<!--TOOLBAR//-->
<table border=0 width=100% cellpadding=0 cellspacing=0 background="../images/tool_spacer.gif" ALIGN=bottom>
 <td valign=top>
 	<input type=image name="toni" value="Don't Sumbit" src="../images/tool_spacer.gif" width=1 height=24 alt="" border="0" onClick = "setSubmitFlag(false);" onMouseOver="style.cursor='default'">	
 	<a onclick="return IsSubmited();" href="javascript:tool_click(1);"><img src="../images/tool_goto.gif" width=47 height=24 alt="" border="0" name="tool_goto"></a>
 	<a onclick="return IsSubmited();" href="javascript:tool_click(2);"><img src="../images/tool_prev_fr.gif" width=108 height=24 alt="" border="0" name="tool_prev"></a>
 	<jato:button name="btToolSearch" extraHtml="width=55 height=24 alt='' border='0'" src="../images/tool_search_fr.gif" />
 	<input type=image name="help" value="" src="../images/tool_help_fr.gif" width=43 height=24 alt="" border="0" onClick = "OpenHelpFrenchBrowser();setSubmitFlag(false);">				
 	<jato:button name="btToolLog" extraHtml="width=57 height=24 alt='' border='0'" src="../images/tool_log_fr.gif" />
 	<a onclick="return IsSubmited();" href="javascript:tool_click(3)"><img src="../images/tool_tools_fr.gif" width=45 height=24 alt="" border="0" name="tool_tools"></a>
 </td>
 <td valign=top align=right><jato:button name="btToolHistory" extraHtml="width=72 height=24 alt='' border='0'" src="../images/tool_history_fr.gif" /><jato:button name="btTooNotes" extraHtml="width=71 height=24 alt='' border='0'" src="../images/tool_notes.gif" /><jato:button name="btWorkQueueLink" extraHtml="width=94 height=24 alt='' border='0'" src="../images/tool_queue_fr.gif" /></td>
 </table>
</center>

<!--TASK NAVIGATER //-->

<table cellpadding=0 cellspacing=0>
<td valign=center><jato:button name="btPrevTaskPage" fireDisplayEvents="true" src="../images/arrowy_left.gif" /> <jato:button name="btNextTaskPage" fireDisplayEvents="true" src="../images/arrowy_right.gif" /></td>
<td valign=center><font size=1 color=003366>&nbsp;<jato:text name="stTaskName" fireDisplayEvents="true" escape="true" /><jato:text name="stPrevTaskPageLabel" fireDisplayEvents="true" escape="true" /><jato:text name="stNextTaskPageLabel" fireDisplayEvents="true" escape="true" /></font></td>
</table>

<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>
<!--
<form>
-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=B8D0DC>
	<td align=center colspan=8
		><img src="../images/white.gif" width=10 height=1
		><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>
	<td rowspan=2>&nbsp;</td>
	<td><font size=1>N<sup>O</sup> DE LA DEMANDE:</font><br><font size=2><b><jato:text name="stDealId" escape="true" /></b></font></td>
	<td><font size=1>NOM DE L'EMPRUNTEUR:</font><br><font size=2><b><jato:text name="stBorrFirstName" escape="true" />
</b></font></td>
	<td><font size=1>STATUT DE LA DEMANDE:</font><br><font size=2><b><jato:text name="stDealStatus" escape="true" /></b></font></td>
	<td><font size=1>DATE DE STATUT DE LA DEMANDE:</font><br><font size=2><b><jato:text name="stDealStatusDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
	<td><font size=1>FIRME SOURCE:</font><br><font size=2><b><jato:text name="stSourceFirm" escape="true" /></b></font></td>
	<td><font size=1>SOURCE:</font><br><font size=2><b><jato:text name="stSource" escape="true" /></b></font></td>
	<td><font size=1>SECTEUR D'ACTIVIT�:</font><br><font size=2><b><jato:text name="stLOB" escape="true" /></b></font></td>
<tr>
	<td align=center colspan=8>&nbsp;</td>
<tr>
	<td rowspan=2>&nbsp;</td>
	<td><font size=1>TYPE DE DEMANDE:</font><br><font size=2><b><jato:text name="stDealType" escape="true" /></b></font></td>
	<td><font size=1>BUT DE LA DEMANDE:</font><br><font size=2><b><jato:text name="stDealPurpose" escape="true" /></b></font></td>
	<td><font size=1>MONTANT TOTAL DU PR�T:</font><br><font size=2><b><jato:text name="stTotalLoanAmount" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></b></font></td>
	<td><font size=1>PRIX D'ACHAT:</font><br><font size=2><b><jato:text name="stPurchasePrice" escape="true" formatType="currency" formatMask="#,##0.00; -#" /></b></font></td>
	<td><font size=1>TERME DU PAIEMENT:</font><br><font size=2><b><jato:text name="stPmtTerm" escape="true" /></b></font></td>
	<td><font size=1>DATE DE CL�TURE PR�VUE:</font><br><font size=2><b><jato:text name="stEstClosingDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
	<td><font size=1>TRAITEMENT SP�CIAL:</font><br><font size=2><b><jato:text name="stSpecialFeature" escape="true" /></b></font></td>
<tr>
	<td align=center colspan=8
		><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!--BODY OF PAGE//-->

<p>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=4><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=4>&nbsp;&nbsp;<font color=3366cc><b>Offre d'engagement actuelle</b></font></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b>Date d��mission</b></font><br><font size=2><jato:text name="stIssueDate" escape="true" /></font></td>
<td><font size=2 color=3366cc><b>Date d�expiration</b></font><br><font size=2><jato:text name="stExpDate" escape="true" /></font></td>
<td><font size=2 color=3366cc><b>Date d�acceptation</b></font><br><font size=2><jato:text name="stAcceptDate" escape="true" /></font></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=4>&nbsp;&nbsp;<font color=3366cc><b>Renverser l'Offre d'engagement</b></font>&nbsp;&nbsp;<jato:checkbox name="cbReverse" /></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
</table>
<P>
<table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0'" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0'" src="../images/cancel_fr.gif" /></td>
</table>

</center>
<br><br><br><br><br>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>