
<HTML>
<%@page info="pgSourceResubmission" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgSourceResubmissionViewBean">

<head>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include French System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>
</head>
<body bgcolor=ffffff link=000000 vlink=000000 alink=000000" onload="popDealNotes();">

<jato:form name="pgSourceResubmission" method="post" onSubmit="return IsSubmitButton();">
<p>

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="false" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<!--
</form>
-->
<p>
<center>

<!--HEADER//-->
    <%@include file="/JavaScript/CommonHeader_fr.txt" %>  

<%@include file="/JavaScript/CommonToolbar_fr.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>    


<!--END HEADER//-->

<!--DEAL INFO BANNER//-->
<div id="pagebody" class="pagebody" name="pagebody">

<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<!--<form name="bodyform"> -->
<input type="hidden" name="netLoanAmount" value="100000"><!--POPULATED FROM SERVER//-->
<input type="hidden" name="ltv" value="83.20"><!--POPULATED FROM SERVER//-->

<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br> 
<!--END DEAL INFO BANNER//-->

<!--BEGIN of Page Contents -->
<p>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=5><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td><tr>

<td colspan=5><font size=3 color=3366cc><b>Source Information:</b></font></td>

<tr>
<td valign=top>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Nom de la source:</b></font><br><font size="2"><jato:text name="stSourceFirstName" escape="true" />&nbsp;<jato:text name="stSourceLastName" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone de la source:</b></font><br><font size="2"><jato:text name="stSourcePhoneNum" escape="true" formatType="string" formatMask="(???)???-????" />&nbsp;x&nbsp;<jato:text name="stSourcePhoneExt" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur de la source:</b></font><br><font size="2"><jato:text name="stSourceFaxNum" escape="true" formatType="string" formatMask="(???)???-????" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Code de demande de la source:</b></font><br><font size="2"><jato:text name="stSourceApplicationId" escape="true" /></font></td>

<tr>
<td valign=top colspan=5>&nbsp;&nbsp;</td>

<tr>
<td colspan=5><font size=3 color=3366cc><b>Information de la demande d�origine:</b></font></td>

<tr>
<td valign=top>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de la demande:</b></font><br><font size="2"><jato:text name="stOrigDealId" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Statut:</b></font><br><font size="2"><jato:text name="stOrigDealStatus" escape="true" /></font></td>
<!-- SEAN Ticket #1786 July 29, 2005 -->
<td valign=top><font size=2 color=3366cc><b>Date de la demande:</b></font><br><font size="2"><jato:text name="stOrigApplicationDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy hh:mm" /></font></td>
<td valign=top></td>

<tr>
<td valign=top>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Souscripteur de la demande:</b></font><br><font size="2"><jato:text name="stOrigUnderwriter" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Administrateur de la demande:</b></font><br><font size="2"><jato:text name="stOrigAdministrator" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Pr�pos� � l�avance les fonds:</b></font><br><font size="2"><jato:text name="stOrigFunder" escape="true" /></font></td>
<td valign=top></td>

<tr>
<td valign=top colspan=5>&nbsp;&nbsp;</td>

<tr>
<td colspan=5><font size=3 color=3366cc><b>Information resoumise:</b></font></td>

<tr>
<td valign=top>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de la demande:</b></font><br><font size="2"><jato:text name="stNewDealId" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Statut de la resoumission:</b></font><br><font size="2"><jato:text name="stNewDealStatus" escape="true" /></font></td>
<!-- SEAN Ticket #1786 July 29, 2005 -->
<td valign=top><font size=2 color=3366cc><b>Date de la resoumission:</b></font><br><font size="2"><jato:text name="stNewApplicationDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy hh:mm" /></font></td>
<td valign=top></td>

<tr>
<td valign=top>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Souscripteur de la demande:</b></font><br><font size="2"><jato:text name="stNewUnderwriter" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Administrateur de la demande:</b></font><br><font size="2"><jato:text name="stNewAdministrator" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Pr�pos� � l�avance les fonds:</b></font><br><font size="2"><jato:text name="stNewFunder" escape="true" /></font></td>
<td valign=top></td>

<tr>
<td colspan=5><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td>
</table>

<p>
<table border=0 width=100% bgcolor=ffffff>
  <tr><td align=right><jato:button name="btReDecision" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/Redecision_fr.gif" /> <jato:button name="btUpdateViaDealMod" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/UpdateVia_fr.gif" /> <jato:button name="btAdoptNewDeal" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/Adopt_fr.gif" /> <jato:button name="btCancel" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/Exit_fr.gif" /></td></tr>
</table>

</div>

<!-- END of Page Contents -->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopWithNoteSection_fr.txt" %>
</div>


<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(4)
}
else{
tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->
<p>


</jato:form>
</body>



</jato:useViewBean>
</HTML>
