
<HTML>
<%@page info="pgDisclosureNS" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDisclosureNSViewBean">

<HEAD>
<TITLE>Disclosure Statement</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/ProvinceGenericCommonFunctions.js" type="text/javascript"></script>
</HEAD>
<body color=ffffff>
<jato:form name="pgDisclosureNS" method="post" onSubmit="is_submit_button();">

<SCRIPT LANGUAGE="JavaScript">

var isCreateDocButton = false;
var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

var provId="eval(<%=viewBean.handler.provinceId%>)";
var pageName="<%= viewBean.PAGE_NAME%>";
var provON="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ONTARIO%>)";
var provAB="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ALBERTA%>)";
var provNL="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_NEWFOUNDLAND%>)";
var provNS="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_NOVA_SCOTIA%>)";
var provPEI="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_PRINCE_EDWARD_ISLAND%>)";
var provON="eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ONTARIO%>)";

// TAR : 1696 - Added the function for Unchecking Will/Will Not Checkbox & Yes/No Checkbox
// 'Will' be Unchecked when 'Will Not' is selected and vice-versa.
// 'Yes' will be unchecked when 'No' is selected and vice-versa.

function disableCheckbox(valueObj)
{
	if((document.forms[0].<%= viewBean.PAGE_NAME%>_104Q14AE_Y.checked == true) && (valueObj == 'Will')	)
	{
		document.forms[0].<%= viewBean.PAGE_NAME%>_104Q14AE_N.checked = false;
	}
	if((document.forms[0].<%= viewBean.PAGE_NAME%>_104Q14AE_N.checked == true) && (valueObj == 'WillNot'))
	{
		document.forms[0].<%= viewBean.PAGE_NAME%>_104Q14AE_Y.checked = false;
	}

	if((document.forms[0].<%= viewBean.PAGE_NAME%>_104Q17AI_Y.checked == true) && (valueObj == 'Yes'))
	{
		document.forms[0].<%= viewBean.PAGE_NAME%>_104Q17AI_N.checked = false;
	}
	if((document.forms[0].<%= viewBean.PAGE_NAME%>_104Q17AI_N.checked == true) && (valueObj == 'No'))
	{
		document.forms[0].<%= viewBean.PAGE_NAME%>_104Q17AI_Y.checked = false;
	}	
}
function checkDealStatus() 
{
	var dealStatusId = eval("document.forms[0]."+ pageName +"_hdDealStatusId.value");
	
	if(dealStatusId == "23")
		var msgStatus = "Collapsed.";
	if(dealStatusId == "24")
		var msgStatus = "Denied."
		
	var msg = "You cannot generate a disclosure document while the deal is " + msgStatus;
		
	if(dealStatusId == "23" || dealStatusId == "24")
	{
		alert(msg);
		setSubmitFlag(false);
		return;
	}
	else
	{ 
		if(provON ==provId ){
			setCancelFlag(false);
		}
		//setSubmitFlag(true);
		isCreateDocButton = true;
		document.forms[0].submit();
	}
}

function is_submit_button()
{
	if(!isCreateDocButton)
	{
		return IsSubmitButton();
	}
	else
	{
		isCreateDocButton = false;
	}
}

</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

	<!--END HEADER//-->

	<!--DEAL SUMMARY SNAPSHOT//-->
	<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>

	<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<!-- PUT HERE ANY SUSTOM CHILDREN ELEMENTS (TEXTBOXES, TILED VIEWS, ETC.)//-->

<table border=0 BGCOLOR=99ccff width=100%>
<!--Disclosure Date Section -->

<!--NS Date (month/day/year) -->
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date of Disclosure:</b></font></td>
	<td colspan=2 valign=top><font size=2 >&nbsp;&nbsp;Mth:&nbsp;<jato:combobox name="104Q010A_M" />&nbsp;&nbsp;Day:<jato:textField name="104Q010A_D" size="2" maxLength="2" />&nbsp;&nbsp;Yr:<jato:textField name="104Q010A_Y" size="4" maxLength="4" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<!--NS Section 9 -->	
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 9. The method of calculation of interest is as follows:</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:textField name="104Q090W" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	
<!--NS Section 14 -->
<!-- TAR : 1696 - Unchecking Will Not when Will is checked and vice-versa -->
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=3valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 14. In the case of a construction mortgage, interest will <jato:checkbox name="104Q14AE_Y" onClick="disableCheckbox('Will');" /> or will not <jato:checkbox name="104Q14AE_N" onClick="disableCheckbox('WillNot');"/> be deducted on advances by the mortgage lender prior to disbursement to the mortgage borrower.</b></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<!--NS Section 15 -->	
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 15. Where the annual percentage rate for the term of the mortgage is subject to variations, it shall vary in the following manner:</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:textField name="104Q15AF" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	
<!--NS Section 16 -->	
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 16. The terms and conditions of repayment before maturity of the mortgage loan when due, the following charges may be imposed (i.e. late payment penalty):</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:textField name="104Q16AG" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>	

<!--NS Section 17 -->	
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 17. Where the mortgage loan is not repaid at maturity or payments are not made when due, the following charges may be imposed (i.e. late payment penalty):</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:textField name="104Q17AH" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

<!--NS Section 18 -->	
<!-- TAR : 1696 - Unchecking Yes when No is checked and vice-versa -->
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 18. Subject to the agreement, municipal taxes will be collected from the mortgage borrower (mortgagor) by the mortgage lender (mortgagee) in advance.</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:checkbox name="104Q17AI_Y" onClick="disableCheckbox('Yes');"/> Yes &nbsp;&nbsp;<jato:checkbox name="104Q17AI_N" onClick="disableCheckbox('No');"/> No</font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>			
<!--NS If Yes -->	
<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>If 'Yes' interest will be paid in the following manner:</b></font></td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:textField name="104Q18AJ" /></font></td>
<tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
	<jato:hidden name="hdCopyId" />
	<jato:hidden name="stDealId"/>
	<jato:hidden name="hdGenerateDocumentPdf" />
	<jato:hidden name="hdDealStatusId" />
	<jato:hidden name="hdInstr" />
<P>

<table border=0 width=100% cellpadding=0 cellspacing=0>
      <td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'checkDealStatus(); '" src="../images/submit.gif" />&nbsp;&nbsp;<jato:button name="btSaveAndExit" extraHtml="width=130 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/SaveAndExit.gif" />&nbsp;&nbsp;<jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/cancel.gif" /></td>
</table>


</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
