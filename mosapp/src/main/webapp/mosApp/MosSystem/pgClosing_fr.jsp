
<HTML>
<%@page info="pgClosing" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgClosingViewBean">

<!--
12/Sep/2006 DVG #DG504 #4603  
-->

<HEAD>
<TITLE>Activit� de cl�ture</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression(QLE?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression(QLE?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
	QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff onLoad="taggleOCSButtons()" onresize="resetPosition()" onscroll="resetPosition()" onmousewheel="resetPosition()">
<jato:form name="pgClosing" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

function showViewOnlyButton() {
    getOCSRequestButton().src = "../images/request_fr.gif";
}


function checkLength()
{
  var inForm = document.pgClosing;

  if ( inForm.pgClosing_tbInstructions.value.toString().length > 255 )
  {
    alert(CHAR_MAX_255);
    inForm.pgClosing_tbInstructions.value = inForm.pgClosing_tbInstructions.value.toString().substr(0,254);
    inForm.pgClosing_tbInstructions.focus();
  } 

} 

// Function to check if the max length of the field
function checkcommentLength(maxLength)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	// Check if this is the current field to validate
	//		-- In order to prevent dead lock
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
  {	
		currFieldToValidate = theFieldName;
  }
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}

	if(theFieldValue == "")
	{
		currFieldToValidate = "";
		return true;
    }

	if( theFieldValue.length > maxLength )
	{
		alert(CHAR_MAX_255);
		theField.focus();
		theField.select();
		return false;
	}
	currFieldToValidate = "";
	return true;
}


function TruncateInstruction()
{
  var inForm = document.pgClosing;

  if ( inForm.pgClosing_tbInstructions.value.toString().length > 255 )
  {
    inForm.pgClosing_tbInstructions.value = inForm.pgClosing_tbInstructions.value.toString().substr(0,254);
  } 

} 

</SCRIPT>

<jato:hidden name="sessionUserId" />
<jato:hidden name="hdAddrId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
     
    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
     
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %>
	
	<!-- SEAN Closing Service: -->
	<script src="../JavaScript/rc/services.js" type="text/javascript"></script>
	<script src="../JavaScript/rc/Closing.js" type="text/javascript"></script>
	<script src="../JavaScript/rc/BorrowerExtractor.js" type="text/javascript"></script>

<!--END HEADER//-->
<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->
<input type="hidden" id="hdDealId" value='<jato:text name="stHDDealId"/>'/>
<input type="hidden" id="hdDealCopyId" value='<jato:text name="stHDDealCopyId"/>'/>

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
  <tr>
    <td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
  </tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<tr><td><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Coordonn�es du notaire</b></font></td><tr>
<td colspan=6>&nbsp;</td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Nom:</b></font><br>
<font size=2><jato:text name="stPartyName" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Firme:</b></font><br>
<font size=2><jato:text name="stPartyCompanyName" escape="true" /></font></td>
<td colspan=3 valign=top><jato:button name="btAssignSolicitor" extraHtml="width=86 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/assign_solicitor_fr.gif" /> <jato:button name="btChangeSolicitor" extraHtml="width=86 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/change_solicitor_fr.gif" /></td><tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>
<td valign=top colspan=2> <font size=2 color=3366cc><b>Adresse:</b></font><br>
<font size=2><jato:text name="stAddressLine1" escape="true" /></font><br>
<font size=2><jato:text name="stAddressLine2" escape="true" /></font> </td>    
<td valign=top><font size=2 color=3366cc><b>Ville:</b></font><br>
<font size=2><jato:text name="stCity" escape="true" formatType="string" formatMask="????????????????????" /><br></td>

<td valign=top><font size=2 color=3366cc><b>Code postal:</b></font><br>
<font size=2><jato:text name="stPostalFSALDU" escape="true" formatType="string" formatMask="???????" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Province:</b></font><br>
<font size=2><jato:text name="stProvinceAbbreviation" escape="true" formatType="string" formatMask="??" /></font></td><tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>de t�l�phone:</b></font><br>
<font size=2><jato:text name="stContactPhoneNumber" escape="true" /> <jato:text name="stContactPhoneNumExtension" fireDisplayEvents="true" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>de t�l�copieur:</b></font><br>
<font size=2><jato:text name="stContactFaxNumber" escape="true" /></font></td>
<td valign=top colspan=3><font size=2 color=3366cc><b>Adresse de courriel:</b></font><br>
<font size=2><jato:text name="stContactEmailAddress" escape="true" formatType="string" formatMask="??????????????????????????????????????????????????" /></font></td><tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>
<td valign=top colspan=3><font size=2 color=3366cc><b>Instructions sp�ciales du notaire:</b></font><br>
<font size=2>
<jato:textArea name="tbInstructions" extraHtml="id='tbInstructions' onKeyPress='checkLength();' onBlur='TruncateInstruction();'" rows="5" cols="51" /></font> </td>
<td valign="top" colspan="2">&nbsp;<br/>
<font size=2 color=3366cc><b>Produire le Packet de Juriste Conseil</b></font> <jato:checkbox name="chProduceSolicitorPkg" /></td><tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<!--<textarea rows=5 cols=80 wrap=soft></textarea></td><tr>-->

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<!-- Begin Closing Detail Section -->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=4 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Information sur la cl�ture</b></font></td><tr>
<td colspan=4>&nbsp;</td><tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Date de cl�ture pr�vue:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stEstimatedCloseDate" escape="true" formatType="date" formatMask="MMM dd yyyy" />&nbsp;&nbsp;&nbsp;</font></td>
<td width="80%" nowrap> </td>
<tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Date d'ajustement des int�r�ts:&nbsp;
  &nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stInterestAdjustmentDate" escape="true" formatType="date" formatMask="MMM dd yyyy" />&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top width="80%" nowrap><font size=2><jato:text name="stIADNumDaysDesc" escape="true" /></font></td>
<tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Date du premier paiement:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stFirstPaymentDate" escape="true" formatType="date" formatMask="MMM dd yyyy" />&nbsp;&nbsp;&nbsp;</font></td>
<!-- #DG504 add label 
 <td valign=top width="80%" nowrap><font size=2><jato:text name="stFirstPaymentDateMthlyDesc" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font></td-->
<td valign=top width="80%" nowrap>
  <font size=2 color=3366cc>
    <b><jato:text name="stOneMonthAfterIAD" escape="true"/>&nbsp;&nbsp;&nbsp;</b>
  <font size=2 color=0>
    <jato:text name="stFirstPaymentDateMthlyDesc" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" />
<tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Date d'�ch�ance:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" />&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top width="80%" nowrap><font size=2><jato:text name="stForceFirstMonthDesc" escape="true" /></font></td>
<tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Int�r�t par jour:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stPerDiemInterest" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" />&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top width="80%" nowrap><font size=2></font></td>
<tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Taux d'int�r�t:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stInterestRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" />&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top width="80%" nowrap><font size=2><jato:text name="stInterestCompoundMsg" escape="true" /></font></td>
<tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Montant du pr�t:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stLoanAmount" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" />&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top width="80%" nowrap><font size=2></font></td>
<tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Ajustement des int�r�ts:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stIADInterest" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" />&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top width="80%" nowrap><font size=2></font></td>
<tr>

<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b>Montant retenu:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stHoldBackAmount" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" />&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top width="80%" nowrap><font size=2></font></td>
<tr>

<jato:tiledView name="rptDealFees" type="mosApp.MosSystem.pgClosingrptDealFeesTiledView">
<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width="10%" nowrap><font size=2 color=3366cc><b><jato:text name="stFeeType" escape="true" />:&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top nowrap width="10%"><font size=2><jato:text name="stFeeAmount" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" />&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top width="80%" nowrap><font size=2></font></td>
<tr>
</jato:tiledView>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

</table>
<!-- End Closing Detail Section -->
<br/>
<!-- Outsourced Closing Service section -->
<jato:text name="stOCSHiddenStart" escape="false"/>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff><tbody>
<tr><td><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
</tbody></table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff><tbody>
<tr><td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Service de fermeture externalis�</b></font></td></tr>
<tr><td colspan=5>&nbsp;</td></tr>

<tr>
  <td valign=top nowrap>
    <input type="hidden" id="hdOcsProductId" value='<jato:text name="sthdProductId"/>'/>
    <input type="hidden" id="hdOcsRequestId" value='<jato:text name="hdOcsRequestId"/>'/>
    <input type="hidden" id="hdOCSRequestStatusId" value='<jato:text name="stOCSRequestStatusId"/>'/>
    <input type="hidden" id="hdRefNum" value='<jato:text name="stOCSRequestRefNum"/>'/>
  </td>
  <td valign=top nowrap><font size=2 color=3366cc><b>Fournisseur:</b></font><br/>
    <jato:combobox name="cbOCSProvider" extraHtml="id='cbOCSProvider' onChange='ocsProviderChange()'"/>
  </td>
  <td valign=top nowrap><font size=2 color=3366cc><b>Produit:</b></font><br/>
    <font size=2><jato:combobox name="cbOCSProduct" extraHtml="id='cbOCSProduct'"/></font> 
  </td>
  <td valign=top nowrap><font size=2 color=3366cc><b>Num�ro de R�f�rence:</b></font><br/>
    <font size=2><span id='ocsRequestRef'><jato:text name="stOCSRequestRefNum" escape="true"/></span></font>
  </td>
  <td valign=top nowrap>&nbsp;</td>
</tr>
<tr>
    <td valign=top>&nbsp;</td>
</tr>
<tr>
  <td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign=top nowrap><font size=2 color=3366cc><b>Statut:</b></font><br/>
    <font size=2><span id='ocsStatus'><jato:text name="stOCSRequestStatus"/></span></font>
    <input type="hidden" id="hdOCSRequestStatus" value='<jato:text name="stOCSRequestStatus"/>'/>
  </td>
  <td valign=top wrap><font size=2 color=3366cc><b>Message de statut:</b></font><br/>
    <font size=2><span id='ocsStatusMsg'><jato:text name="stOCSRequestStatusMsg"/></span></font>
  </td>
  <td valign=top nowrap><font size=2 color=3366cc><b>Date/Temps de Statut:</b></font><br/>
    <font size=2><span id='ocsDate'><jato:text name="stOCSRequestStatusDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy HH:mm"  /></span></font>
  </td>
    <td valign=bottom nowrap>
      <a href='JavaScript: sendOCSRequest()'><img alt="" id="ocsRequestButton" border="0" height="25" src="../images/request_fr.gif" width="86" /></a>
    <a href='JavaScript:  sendOCSRequest(true)'><img alt="" border="0" height="25" src="../images/cancelOrder_fr.gif"/></a>
    </td>
</tr>
<tr>
    <td valign=top>&nbsp;</td>
</tr>
<tr>
  <td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>
<tr>
  <td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</tr>
</tbody></table>
<jato:text name="stOCSHiddenEnd" escape="false"/>
<!-- END Outsourced Closing Service Section -->
<br/>
<!-- Fixed Price Closing Package section -->
<jato:text name="stFPCHiddenStart" escape="false"/>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff><tbody>
<tr><td><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
</tbody></table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff><tbody>
<tr><td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Fermeture � prix fixe</b></font></td></tr>
<tr><td colspan=5>&nbsp;</td></tr>

<tr>
  <td valign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="hidden" id="hdFpcRequestId" value='<jato:text name="hdFpcRequestId"/>'/>
  <input type="hidden" id="hdFpcContactId" value='<jato:text name="hdFpcContactId"/>'/>
  <input type="hidden" id="hdFpcBorrowerId" value='<jato:text name="hdFpcBorrowerId"/>'/>
  <input type="hidden" id="hdFpcProviderRefNo" value='<jato:text name="sthdFpcProviderRefNo"/>'/>
  <input type="hidden" id="hdFPCRequestStatusId" value='<jato:text name="stFPCRequestStatusId"/>'/>
  <input type="hidden" id="hdFpcProviderId" value='<jato:text name="sthdFpcProviderId"/>'/>
  <input type="hidden" id="hdFpcProductId" value='<jato:text name="sthdFpcProductId"/>'/>

  <jato:hidden name="stHDDealCopyId" />
	<jato:hidden name="hdOcsRequestId" />
	<jato:hidden name="hdFpcRequestId" />
	<jato:hidden name="hdFpcContactId" />
  </td>
  <td valign="top" nowrap><font size=2 color=3366cc><b>Fournisseur:</b></font><br/>
    <jato:combobox name="cbFPCProvider" extraHtml="id='cbFPCProvider' onChange='fpcProviderChange()'"/>
  </td>
  <td valign="top" nowrap><font size=2 color=3366cc><b>Produit:</b></font><br/>
    <jato:combobox name="cbFPCProduct" extraHtml="id='cbFPCProduct'"/>
  </td>
  <td valign="top" nowrap><font size=2 color=3366cc><b>Statut:</b></font><br/>
    <font size=2><span id='fpcStatus'><jato:text name="stFPCRequestStatus" /></span></font>
  </td>
  <td valign="top" nowrap><font size=2 color=3366cc><b>Date/Temps de Statut:</b></font><br/>
    <font size=2><span id='fpcDate'><jato:text name="stFPCRequestDate" escape="true" formatType="date" formatMask="fr|MMM  dd  yyyy  HH:mm" /></span></font>
  </td>
</tr>
<tr>
    <td valign=top>&nbsp;</td>
</tr>

<tr>
  <td valign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td nowrap><font size=2 color=3366cc><b>Utiliser l'information primaire d'emprunteur</b></font>
    <jato:checkbox name="chFPCBorrowerInfo" extraHtml="id='fpcBorrowerCheckbox' onClick='primaryBorrowerClicked();'" />
  </td>
  <td valign="top" nowrap><font size=2 color=3366cc><b>Pr�nom du contact:</b><br/>
    <jato:textField name="stFPCContactFirstName" size="20" maxLength="20" extraHtml="id='contactFirstName'" fireDisplayEvents="true"/></font>
  </td>
  <td valign="top" nowrap><font size=2 color=3366cc><b>Nom du contact:</b></font><br/>
    <jato:textField name="stFPCContactLastName" size="20" maxLength="20" extraHtml="id='contactLastName'" fireDisplayEvents="true"/>
  </td>
</tr>
<tr>
    <td valign=top>&nbsp;</td>
</tr>
<tr>
  <td valign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign="top" nowrap><font size=2 color=3366cc><b>Courriel:</b></font><br/>
    <jato:textField name="tbFPCEmail" maxLength="50" extraHtml="id='contactEmail'" fireDisplayEvents="true"/>
  </td>
  <td valign="top"><font size=2 color=3366cc><b>No. de t�l�phone bureau:</b></font><br>
    (<jato:textField name="tbFPCWorkPhoneAreaCode" extraHtml="id='contactWorkArea'" size="3" maxLength="3" fireDisplayEvents="true"/>) 
    <jato:textField name="tbFPCWorkPhoneFirstThreeDigits" size="3" extraHtml="id='contactWorkFirst'" maxLength="3" fireDisplayEvents="true"/> -
    <jato:textField name="tbFPCWorkPhoneLastFourDigits" size="4" maxLength="4" extraHtml="id='contactWorkLast'" fireDisplayEvents="true"/> X 
    <jato:textField name="tbFPCWorkPhoneNumExtension" size="6" maxLength="6" extraHtml="id='contactWorkExt'" fireDisplayEvents="true"/>
  </td>
  <td valign="top"><font size=2 color=3366cc><b>No. de t�l�phone cellulaire:</b></font><br>
    (<jato:textField name="tbFPCCellPhoneAreaCode" size="3"  extraHtml="id='contactCellArea'" maxLength="3" fireDisplayEvents="true"/>) 
    <jato:textField name="tbFPCCellPhoneFirstThreeDigits" size="3" extraHtml="id='contactCellFirst'" maxLength="3" fireDisplayEvents="true"/> -
    <jato:textField name="tbFPCCellPhoneLastFourDigits" size="4" extraHtml="id='contactCellLast'" maxLength="4" fireDisplayEvents="true"/>
  </td>
  <td valign="top"><font size=2 color=3366cc><b>No. de t�l�phone maison:</b></font><br>
    (<jato:textField name="tbFPCHomePhoneAreaCode" size="3" extraHtml="id='contactHomeArea'" maxLength="3" fireDisplayEvents="true"/>) 
    <jato:textField name="tbFPCHomePhoneFirstThreeDigits" size="3" extraHtml="id='contactHomeFirst'" maxLength="3" fireDisplayEvents="true"/> -
    <jato:textField name="tbFPCHomePhoneLastFourDigits" size="4" extraHtml="id='contactHomeLast'" maxLength="4" fireDisplayEvents="true"/>
  </td>
</tr>
<tr>
    <td valign=top>&nbsp;</td>
</tr>
<tr>
  <td valign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td colspan="3" valign="top" nowrap><font size=2 color=3366cc>
    <div style='visibility: <jato:text name="stFpcCommentHidden"/>'>
       <b>Commentaires:</b></font><br/>
          <jato:textArea name="stFPCComments" extraHtml="id='fpcComments' onChange='return checkcommentLength(255)'" rows="5" cols="80" />
    </div>
  </td>
  <td>  
  <a href='JavaScript: sendFPCRequest()'>
      <img alt="" id="submitRequest" border="0" height="25" src="../images/submit_referral_fr.gif" width="116" />
      </a>
  </td>
</tr>

<tr>
  <td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>
<tr>
  <td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</tr>
</tbody></table>
<jato:text name="stFPCHiddenEnd" escape="false"/>
<!-- END Fixed Price Closing Package section -->

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<!--<td align=right><img src="../images/submit.gif" width=86 height=25 alt="" border="0">&nbsp;&nbsp;&nbsp;<img src="../images/cancel.gif" width=86 height=25 alt="" border="0"></td>-->
  <td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>

<p>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<!-- <jato:text name="stDateMask" escape="true" /> -->


</jato:form>


<script language="javascript">
<!--
if(NTCP){
  document.dialogbar.left=55;
  document.dialogbar.top=79;
  document.toolpop.left=317;
  document.toolpop.top=79;
  document.pagebody.top=100;
  document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
  tool_click(4)
}
else{
  tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
