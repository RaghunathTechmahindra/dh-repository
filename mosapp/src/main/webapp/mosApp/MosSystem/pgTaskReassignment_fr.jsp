
<HTML>
<%@page info="pgTaskReassignment" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgTaskReassignmentViewBean">

<HEAD>
<TITLE>R�affectation de la t�che</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include French System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
function handleInstitutionKeyPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
	//alert("handleInstitutionKeyPressed The key = " + keycode);
  if (keycode == 0)
  {
    performInstitutionGo();
    return false;
  }
 
  return true;
}

function performInstitutionGo()
{
	setSubmitFlag(true);
	document.forms[0].action += "?<%=viewBean.PAGE_NAME%>_btGo=";
  
  if(IsSubmitButton())
	  document.forms[0].submit();
}
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgTaskReassignment" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>  

<%@include file="/JavaScript/CommonToolbar_fr.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>   

<!--END HEADER//-->


<div id="pagebody" class="pagebody" name="pagebody">
<center>
<!--
<form>
-->
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>

	<td colspan=9><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<td colspan=9 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;D�tails de la r�affectation de la demande/t�che</b></font></td><tr>
	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td><tr>
	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Souscripteur:</b>&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>Administrateur:&nbsp;&nbsp;</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>Pr�pos� � l�avance de fonds:&nbsp;&nbsp;</b></font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>T�che actuelle:</b>&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>Affect�e �:&nbsp;&nbsp;</b></font></td>

	<tr>
	<td colspan=2 valign=top><font size=2 >&nbsp;&nbsp;<jato:text name="stUnderwriter" escape="true" /></font></td>
	<td colspan=2 valign=top><font size=2 ><jato:text name="stAdministrator" escape="true" /></font></td>
	<td colspan=2 valign=top><font size=2 ><jato:text name="stFunder" escape="true" /></font></td>
	<td colspan=2 valign=top><font size=2 ><jato:text name="stTaskDescription" escape="true" /></font></td>
	<td colspan=1 valign=top><font size=2 ><jato:text name="stAssignedTo" escape="true" /></font></td>
	<tr>
</table>

<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>		
	<jato:hidden name="toggleIndividualTaskDealReassignment" />
	<jato:content name="dealPageOnly">
	<td colspan=3><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<td colspan=3 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;R�affectation d�une t�che individuelle</b></font></td><tr>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td><tr>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>R�affecter</b>&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 >			   &nbsp;&nbsp;<jato:combobox name="cbReassign" /></font></td><tr>
	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td><tr>
	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Affecter �:</b></font></td>
	<td colspan=1 valign=top><font size=2 >&nbsp;&nbsp;<jato:combobox name="cbReassignTo" /></font></td>
	<tr>
	</jato:content>
	<td colspan=3><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<td colspan=3 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;R�affectation de t�ches/demandes multiples</b></font></td><tr>
	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td><tr>
	<jato:content name="multiInstOnly">
		<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;</font></td>
		<!-- <jato:button name="btGo" extraHtml="onClick = setSubmitFlag(true); performGo();" src="../images/go.gif" /> -->
		<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;<b> Institution:</b>&nbsp;&nbsp;</font></td> 
		<td colspan=1 valign=top><font size=2 >            &nbsp;&nbsp;<jato:combobox name="cbInstitution" onChange="handleInstitutionKeyPressed()"	fireDisplayEvents="true" /></font></td><tr>
		<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td><tr>
	</jato:content>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>De:</b>&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 >            &nbsp;&nbsp;<jato:combobox name="cbMultyDealFrom" /></font></td><tr>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td><tr>
	
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;</font></td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;�:</b></font></td>
	<td colspan=1 valign=top><font size=2 >			   &nbsp;&nbsp;<jato:combobox name="cbMultyDealTo" /></font></td>
	<tr>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;</font></td><tr>
	<td colSpan=3><IMG alt="" border=0 height=2 src="../images/blue_line.gif" width=1></TD><tr>
</table>

<br>
<table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/submit_fr.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/cancel_fr.gif" /></td><tr>
<td ><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td align=right><img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();"></td><tr>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
