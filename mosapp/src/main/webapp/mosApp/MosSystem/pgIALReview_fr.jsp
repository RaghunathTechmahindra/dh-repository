
<HTML>
<%@page info="pgIALReview" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgIALReviewViewBean">

<head>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}

.dealsumborr {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function tool_click(n) {
	var itemtomove = (NTCP) ? document.pagebody : document.all.pagebody.style;
	var alertchange = (NTCP) ? document.alertbody:document.all.alertbody.style;
	var borrSub = (NTCP)?document.dealsumborr:document.all.dealsumborr.style;

	if(n==1){
		var imgchange = "tool_goto"; var hide1img="tool_prev"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.gotobar : document.all.gotobar.style;
		var hideother1 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==2){
		var imgchange = "tool_prev"; var hide1img="tool_goto"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.dialogbar : document.all.dialogbar.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==3){
		var imgchange = "tool_tools"; var hide1img="tool_goto"; var hide2img="tool_prev";
		var itemtochange = (NTCP) ? document.toolpop : document.all.toolpop.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
	}
	if(n==1 || n==2 || n==3){
		if (itemtochange.visibility==VISIBLE) {
			itemtochange.visibility=HIDDEN;
			itemtomove.top = (NTCP) ? 100:110;
			borrSub.top = (NTCP) ? 100:110;
			changeImg(imgchange,'off');
			if(alertchange.visibility==VISIBLE){alertchange.top = (NTCP) ? 100:110;}
		} 
		else {
			itemtochange.visibility=VISIBLE;
			changeImg(imgchange,'on');
			if(n!=2){itemtomove.top=150;borrSub.top = 150;}
			else{itemtomove.top=290;borrSub.top=290;}
			if(alertchange.visibility==VISIBLE){
				if(n!=2){alertchange.top =  150;}
				else{alertchange.top = 290;}
			}
			if(hideother1.visibility==VISIBLE){hideother1.visibility=HIDDEN; changeImg(hide1img,'off');}
			if(hideother2.visibility==VISIBLE){hideother2.visibility=HIDDEN;
changeImg(hide2img,'off');}
		}
	}
	if(n==4){
		var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
		itemtoshow.visibility=VISIBLE;
	}
	if(n==5){
		var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
		var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
		itemtoshow.visibility=VISIBLE;
		if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
	}
}

if(document.images){
	tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on.gif";
	tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto.gif";
	tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on_fr.gif";
	tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev_fr.gif";
	tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on_fr.gif";
	tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools_fr.gif";
}
function changeImg(imgNam,onoff){
	if(document.images){
		document.images[imgNam].src = eval (imgNam+onoff+'.src');
	}
}

function changeArrow(iName){
if(NTCP){
	document.pagebody.document.images[iName+''+1].src="../images/arrow_right.gif";
	document.pagebody.document.images[iName+''+2].src="../images/arrow_left.gif";
}
else{
	document.images[iName+''+1].src="../images/arrow_right.gif";
	document.images[iName+''+2].src="../images/arrow_left.gif";
}
return;
}

probArr = new Array();
probArr[1] = "creditscore";
probArr[2] = "creditbursum";
probArr[3] = "creditburnam";
probArr[4] = "reportdate";
probArr[5] = "filedate";
probArr[6] = "existclient";
probArr[7] = "payhistory";
probArr[8] = "nsf";
probArr[9] = "clientcomment";

function markProblem(){
var form1 = document.forms[0];
for(i=1;i<=probArr.length-1;i++){
var checkVal = eval('form1.'+probArr[i]);
if(checkVal.value=="Y"){changeArrow(probArr[i]);}
}
}

function viewSub(subsect){
var borrSub = (NTCP)?document.dealsumborr:document.all.dealsumborr.style;
var pageHide = (NTCP)?document.pagebody:document.all.pagebody.style;

if(subsect=="borr"){borrSub.visibility=VISIBLE;}

if(subsect=="main"){
	borrSub.visibility=HIDDEN;
	pageHide.visibility=VISIBLE;
}
else{
	pageHide.visibility=HIDDEN;
}
}

function OnTopClick(){
	self.scroll(0,0);
}

var pmWin;
var pmCont = '';

function openPMWin()
{
var numberRows = pmMsgs.length;

pmCont+='<head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  for(z=0;z<=numberRows-1;z++)
  {
	  if(pmMsgTypes[z]==0){var pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
	  if(pmMsgTypes[z]==1){var pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
	  writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
	  writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close_fr.gif" width=84 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].<%= viewBean.PAGE_NAME%>_sessionUserId.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');
    
  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
	pmWin.document.open();
	pmWin.document.write(pmCont);
	pmWin.document.close();
	pmWin.focus();
}


function generateAM()
{
var numberRows = amMsgs.length;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
	amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	var writeRows='';

	for(z=0;z<=numberRows-1;z++)
  {
		if(amMsgTypes[z]==0){var amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
		if(amMsgTypes[z]==1){var amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
		writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
		writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}
//-->

</SCRIPT>

</head>
<body bgcolor=ffffff link=000000 vlink=000000 alink=000000>
<jato:form name="pgIALReview" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<!--PROBLEMS TO HIGHLIGHT - FLAGGED FROM SERVER//-->
<input type="hidden" name="creditscore" value="Y">
<input type="hidden" name="creditbursum" value="">
<input type="hidden" name="creditburnam" value="">
<input type="hidden" name="reportdate" value="">
<input type="hidden" name="filedate" value="">
<input type="hidden" name="existclient" value="">
<input type="hidden" name="payhistory" value="">
<input type="hidden" name="nsf" value="Y">
<input type="hidden" name="clientcomment" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

	<!--END HEADER//-->


<!--DEAL INFO BANNER//-->
	<!--DEAL SUMMARY SNAPSHOT//-->
	<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
	<!--END DEAL SUMMARY SNAPSHOT//-->
<!--END DEAL INFO BANNER//-->
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=4><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td>&nbsp;&nbsp;</td>

<td valign=top><font size=2 color=3366cc><b>RPV combin�</b></font><br>
<font size=2>
<jato:textField name="tbCombinedLTV" formatType="currency" formatMask="###0.00; (-#)" size="20" maxLength="20" /></font></td>

<td valign=top><font size=2 color=3366cc><b>ABD combin�</b></font><br>
<font size=2>
<jato:textField name="tbCombinedGDS" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></font></td>

<td valign=top><font size=2 color=3366cc><b>ATD combin�</b></font><br>
<font size=2>
<jato:textField name="tbCombinedTDS" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></font></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;</td>

<td valign=top><font size=2 color=3366cc><b>Revenu combin�</b></font><br>
<font size=2>
<jato:textField name="tbCombinedIncome" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Actif combin�</b></font><br>
<font size=2>
<jato:textField name="tbCombinedAssets" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Passif combin�</b></font><br>
<font size=2>
<jato:textField name="tbCombinedLiabilities" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></font></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=4 bgcolor=ffffff>&nbsp;</td><tr>
<td colspan=4><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;</td>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>

<jato:tiledView name="RepeatedBorrowers" type="mosApp.MosSystem.pgIALReviewRepeatedBorrowersTiledView">
<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Nom de l�emprunteur</b></font><br>
<font size=2><jato:text name="stBorrowerName" escape="true" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Principal?</b></font><br>
<font size=2><jato:text name="stPrimaryFlag" escape="true" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Borrower Type</b></font><br>
<font size=2><jato:text name="stBorrowerType" escape="true" /></font></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>ATD</b></font><br>
<jato:textField name="stBorrowerTDS" size="20" maxLength="20" /></td>

<td valign=top><font size=2 color=3366cc><b>ABD</b></font><br>
<jato:textField name="stBorrowerGDS" size="20" maxLength="20" /></td>

<td valign=top><font size=2 color=3366cc><b>Valeur nette</b></font><br>
<jato:textField name="stNetworth" size="20" maxLength="20" /></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>

<jato:tiledView name="RepeatedEmployer" type="mosApp.MosSystem.pgIALReviewRepeatedEmployerTiledView">

<td>&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Profession/M�tier</b></font><br>
<font size=2></font><jato:combobox name="cbOccupation" /></td>

<td valign=top colspan=2><font size=2 color=3366cc><b>Industry Segmt</b></font><br>
<font size=2></font><jato:combobox name="cbIndustry" /></td><tr>

<jato:hidden name="hdEmpBorrowerId" />
<jato:hidden name="hdEmployerId" />
</jato:tiledView>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;</td>
<td colspan=4><font size=3 color=3366cc><b>Revenu</b></font>&nbsp;&nbsp;<jato:button name="btAddIncome" extraHtml="width=135 height=15 alt='' border='0'" src="../images/add_income_fr.gif" /></td><tr>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b>Income Type Description</b></font></td>
<td><font size=2 color=3366cc><b>P�riode du revenu</b></font></td>
<td><Font size=2 color=3366cc><b>Montant du revenu $</b></font></td>
<td><font size=2 color=3366cc><b>Comprendre dans l'ABD? </b></font></td>
<td><font size=2 color=3366cc><b>Comprendre dans l'ATD? </b></font></td><tr>

<jato:tiledView name="RepeatedIncome" type="mosApp.MosSystem.pgIALReviewRepeatedIncomeTiledView">

<td colspan=8 bgcolor=3366cc><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b><jato:text name="stIndex2" escape="true" /></b></font><jato:combobox name="cbIncomType" /></td>
<td><jato:combobox name="cbIncomePeriod" /></td>
<td>
<jato:textField name="tbIncomeAmount" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></td>
<td><jato:combobox name="cbIncomeIncludeInGDS" /></td>
<td><jato:combobox name="cbIncomeIncludeInTDS" /></td>

<td valign=top align=right><jato:button name="btDeleteIncome" extraHtml="width=52 height=15 alt='' border='0'" fireDisplayEvents="true" src="../images/delete_fr.gif" />&nbsp;&nbsp;</td><tr>

<jato:hidden name="hdIncomeBorrowerId" />
<jato:hidden name="hdIncomeId" />
</jato:tiledView>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>


<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td>&nbsp;&nbsp;</td>
<td colspan=3><font size=3 color=3366cc><b>Assets</b></font>&nbsp;&nbsp;<jato:button name="btAddAsset" extraHtml="width=122 height=15 alt='' border='0'" src="../images/add_asset_fr.gif" /></td><tr>
<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b>Description de l�actif</b></font></td>
<td><font size=2 color=3366cc><b>Valeur de l�actif $</b></font></td>
<td><font size=2 color=3366cc><b>Comprendre dans l'ABD?</b></font></td>
<td><font size=2 color=3366cc><b>Comprendre dans l'ATD?</b></font></td><tr>

<jato:tiledView name="RepeatedAddAsset" type="mosApp.MosSystem.pgIALReviewRepeatedAddAssetTiledView">
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b><jato:text name="stIndex" escape="true" /></b></font> 
<jato:textField name="tbAssetDescription" size="20" maxLength="20" /></td>
<td>
<jato:textField name="tbAssetValue" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></td>
<td><jato:combobox name="cbAssetIncludeInGDS" /></td>
<td><jato:combobox name="cbAssetIncludeInTDS" /></td>
<td align=right><jato:button name="btDeleteAsset" extraHtml=" width=52 height=15 alt='' border='0'" fireDisplayEvents="true" src="../images/delete_fr.gif" /></td><td>&nbsp;&nbsp;</td><tr>

<jato:hidden name="hdAssetId" />
<jato:hidden name="hdAssetBorrowerId" />

</jato:tiledView>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td>&nbsp;&nbsp;</td>
<td colspan=4><font size=3 color=3366cc><b>Passif</b></font>&nbsp;&nbsp;<jato:button name="btAddLiability" extraHtml="width=134 height=15 alt='' border='0'" src="../images/add_liability_fr.gif" /></td><tr>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b>Liability Type Description</b></font></td>
<td><font size=2 color=3366cc><b>Liability Amount $</b></font></td>
<td><font size=2 color=3366cc><b>Liability Monthly Payment $</b></font></td>
<td><font size=2 color=3366cc><b>Comprendre dans l'ABD? </b></font></td>
<td><font size=2 color=3366cc><b>Comprendre dans l'ATD? </b></font></td><tr>

<jato:tiledView name="RepeatedLiability" type="mosApp.MosSystem.pgIALReviewRepeatedLiabilityTiledView">
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b><jato:text name="stIndex1" escape="true" /></b></font><jato:combobox name="cbLiabilityType" /></td>
<td>
<jato:textField name="tbliabTotalAmount" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></td>
<td>
<jato:textField name="tbliabMonyhlyPayment" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></td>
<td><jato:combobox name="cbliabIncudedInGDS" /></td>
<td><jato:combobox name="cbliabIncudedInTDS" /></td>
<td align=right><jato:button name="btDeleteLiability" extraHtml="width=52 height=15 alt='' border='0'" fireDisplayEvents="true" src="../images/delete_fr.gif" /></td> 
<td>&nbsp;&nbsp;</td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<jato:hidden name="hdLiabliltyBorrowerId" />
<jato:hidden name="hdLiabilityId" />
</jato:tiledView>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>


<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>
<td>&nbsp;&nbsp;</td>
<td colspan=4><font size=3 color=3366cc><b>Type de r�f�rence de cr�dit</b></font>&nbsp;&nbsp;<jato:button name="btAddCredit" extraHtml="width=127 height=15 alt='' border='0'" src="../images/add_credit_fr.gif" /></td><tr>
<jato:tiledView name="RepeatedCredit" type="mosApp.MosSystem.pgIALReviewRepeatedCreditTiledView">
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b>Description de la r�f�rence</b></font><br>
<jato:textField name="tbCreditReferencrDescription" size="20" maxLength="20" /></td>
<td colspan=2><font size=2 color=3366cc><b>Nom de l'institution</b></font><br>
<jato:textField name="tbInstituationName" size="20" maxLength="20" /></td>
<td rowspan=2><jato:button name="btDeleteCredit" extraHtml="width=52 height=15 alt='' border='0'" fireDisplayEvents="true" src="../images/delete_fr.gif" /></td><tr>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b>Account Type</b></font><br>
<jato:combobox name="cbCreditRefType" /></td>
<td><font size=2 color=3366cc><b>Num�ro de compte</b></font><br>
<jato:textField name="tbAccountNumber" size="20" maxLength="20" /></td>
<td><font size=2 color=3366cc><b>Solde actuel $</b></font><br>
<jato:textField name="tbCurrentBalance" formatType="currency" formatMask="#,##0.00; (-#)" size="20" maxLength="20" /></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td><tr>
<jato:hidden name="hdCreditBorrowerId" />
<jato:hidden name="hdCreditId" />
</jato:tiledView>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td><tr>
<td colspan=8 bgcolor=ffffff>&nbsp;</td><tr>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td><tr>
<p>

<jato:hidden name="hdBorrowerid" />
</jato:tiledView>
</table>
<P>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0'" src="../images/submit_fr.gif" />&nbsp;<jato:button name="btNext" extraHtml="width=86 height=25 alt='' border='0'" src="../images/next.gif" />&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0'" src="../images/cancel_fr.gif" /></td>
</table>
<p>
</center>

</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<!--ADDITONAL BORROWER INFORMATION---------------------------//-->
<div id="dealsumborr" class="dealsumborr" name="dealsumborr">
</div>
<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(4)
}
else{
tool_click(5)
}

//markProblem();

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->
</table>


</jato:form>
</body>
</jato:useViewBean>
</HTML>
