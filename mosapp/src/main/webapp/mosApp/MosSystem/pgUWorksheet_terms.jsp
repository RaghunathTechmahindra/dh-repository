
<!--START TERMS//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=7 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Terms</b></font></td><tr>

<tr>
<%-- Release3.1 Apr 13, 2006 begins --%>
<td colspan=4>&nbsp;</td>
<TD vAlign=top><FONT color=#3366cc size=2><B>Product Type:</B></FONT></TD>
<td valign=top><jato:combobox name="cbProductType" /></td>
<%-- Release3.1 Apr 13, 2006 ends --%>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Total Purchase Price:</b></font></td>
<td valign=top colspan=2><font size=2>$ <jato:text name="stUWPurchasePrice" escape="true" formatType="decimal" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Charge:</b></font></td>
<td valign=top><jato:combobox name="cbUWCharge" /></td>
</tr>

<jato:text name="stDealPurposeTypeHiddenStart" escape="false"/>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Value of Improvements:</b></font></td>
<td valign="top"><font size=2>$ <jato:text name="txUWRefiImprovementValue"
    escape="true" formatType="decimal" formatMask="#,##0.00; (-#)" /></font></td></tr>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>As Improved Value:</b></font></td>
<td valign="top"><font size=2>$ <jato:text name="txUWImprovedValue"
    escape="true" formatType="decimal" formatMask="#,##0.00; (-#)" /></font></td></tr>	
<jato:text name="stDealPurposeTypeHiddenEnd" escape="false"/>


<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Down Payment:</b></font></td>
<td valign=top><font size=2>$ <jato:text name="stUWDownPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>LOB:</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:combobox name="cbUWLOB" /></td>
<td valign=top><font size=2 color=3366cc><b>Lender:</b></font></td>
<td valign=top><jato:combobox name="cbUWLender" onClick="recordCurrentSelection()"/></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=3></td>
<td valign=top><font size=2 color=3366cc><b>Requested Product,<br>Rate and Payment:</b></font></td>
<td valign=top><font size=2><jato:text name="stHomeBASERateProductPmnt" escape="false" /></font></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><div style='visibility:hidden'><font size=2 color=d1ebff><b>Bridge Loan:</b></font></div></td>
<td valign=top colspan=2><font size=2><jato:text name="stUWBridgeLoan" fireDisplayEvents="true" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font>&nbsp;&nbsp;&nbsp;<jato:button name="btBridgeReview" extraHtml="width=89  height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/review_bridge.gif" /></td>
<td valign=top><font size=2 color=3366cc><b>Product:</b></font></td>
<td valign=top><jato:combobox name="cbUWProduct" onClick="recordCurrentSelection()"/><jato:hidden name="hdInterestTypeId" /></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Loan:</b></font></td>
<td valign=top>$<jato:textField name="txUWLoanAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>LTV:</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdUWOrigLTV" />
<jato:textField name="txUWLTV" formatType="decimal" formatMask="###0.00; (-#)" size="6" maxLength="6" />%</td>
<td valign=top><font size=2 color=3366cc><b>Posted Interest Rate:</b></font></td>
<td valign=top><jato:combobox name="cbUWPostedInterestRate" /></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><jato:text name="stIncludeMIPremiumStart" escape="false" /><b><jato:text name="stMIPremiumTitle" escape="true" /></b><jato:text name="stIncludeMIPremiumEnd" escape="false" /></font></td>
<td valign=top><font size=2><jato:text name="stIncludeMIPremiumValueStart" escape="false" /><jato:text name="stMIPremium" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /><jato:text name="stIncludeMIPremiumValueEnd" escape="false" /></font></td>
<td valign=top><jato:button name="btRecalculateLTV" extraHtml="width=75 height=15 alt='' border='0' onClick = 'setSubmitFlag(true); populateLoanAmtWithLTV();'" fireDisplayEvents="true" src="../images/recalculate.gif" /></td>
<td valign=top><font size=2 color=3366cc><b>Discount:</b></font></td>
<td valign=top><jato:textField name="txUWDiscount" formatType="decimal" formatMask="###0.000;-#" size="6" maxLength="6" /> %</td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Total Loan:</b></font></td>
<td valign=top colspan=2><font size=2><jato:text name="stUWTotalLoan" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Premium:</b></font></td>
<td valign=top><jato:textField name="txUWPremium" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Amortization Period:</b></font></td>
<td valign=top colspan=2><font size=2>Yrs: </font>
<jato:textField name="txUWAmortizatioPeriodYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /><font size=2>Mths: </font>
<jato:textField name="txUWAmortizatioPeriodMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>Buydown:</b></font></td>
<td valign=top><jato:textField name="txUWBuyDown" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Effective Amortization Period:</b></font></td>
<td valign=top colspan=2><font size=2>Yrs: </font>
<jato:textField name="txUWEffectiveAmortizationYears" size="3" maxLength="3" /> <font size=2>Mths: </font>
<jato:textField name="txUWEffectiveAmortizationMonths" size="3" maxLength="3" /></td>
<td valign=top><font size=2 color=3366cc><b>Net Rate:</b></font></td>
<td valign=top><font size=2><jato:text name="stUWNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</font></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Payment Term Description: </b></font></td>
<td valign=top><font size=2><jato:textField name="tbUWPaymentTermDescription" size="20" maxLength="35" /></font><jato:hidden name="hdPaymentTermId" /></td>
<td valign=top><font size=2 color=3366cc><b>Rate Code:</b></font>&nbsp;<jato:textField name="tbUWRateCode" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>P&I:</b></font></td>
<td valign=top><font size=2><jato:text name="stUWPIPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Actual Payment Term:</b></font></td>
<td valign=top colspan=2><font size=2>Yrs: </font>
<jato:textField name="txUWActualPayYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Mths: </font>
<jato:textField name="txUWActualPayMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>Additional P&I:</b></font></td>
<td valign=top>$<jato:textField name="txUWAdditionalPIPay" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Payment Frequency:</b></font></td>
<td valign=top colspan=2><jato:combobox name="cbUWPaymentFrequency" /></td>
<td valign=top><font size=2 color=3366cc><b>Total Escrow:</b></font></td>
<td valign=top><font size=2><jato:text name="stUWTotalEscrow" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Prepayment Penalty:</b></font></td>
<td valign=top colspan=2><jato:combobox name="cbUWPrePaymentPenalty" /></td>
<td valign=top><font size=2 color=3366cc><b>Total Payment:</b></font></td>
<td valign=top><font size=2><jato:text name="stUWTotalPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Privilege Payment Option:</b></font></td>
<td valign=top colspan=2><jato:combobox name="cbUWPrivilegePaymentOption" /></td>
<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<td valign=top><font size=2 color=3366cc><b>Payment Including Life / Disability:</b></font></td>
<td valign=top><font size=2><jato:text name="stPmntInclLifeDis" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Commission Code:</b></font></td>
<td valign=top colspan=2><jato:textField name="txCommisionCode" size="35" maxLength="35" /></td>
<td valign=top><font size=2 color=3366cc><b>Holdback Amount:</b></font></td>
<td valign=top>$<jato:textField name="txUWAdvanceHold" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Cashback Percentage:</b></font></td>
<td valign=top colspan=2><jato:textField name="txCashBackInPercentage" formatType="decimal" formatMask="###0.00; (-#)" size="6" maxLength="6" />%</td>
<td valign=top><font size=2 color=3366cc><b>Rate Guarantee Period:</b></font></td>
<!-- Ticket 3851: NBC Implementation Team - Change formatMask to disply Rate Gurantee Period as integer -->
<td valign=top><jato:textField name="txRateGuaranteePeriod" formatType="decimal" formatMask="###0;-#" size="6" maxLength="6" /><font size=2 color=3366cc>&nbsp;<b>Days</b></font></td>
</tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Cashback Amount:</b></font></td>
<td valign=top>$<jato:textField name="txCashBackInDollars" formatType="decimal" formatMask="###0.00; (-#)" size="14" maxLength="16" /></td>
<td valign=top><font size=2 color=3366cc><b><nbsp>Cashback $ Override</b></font><font size=2><jato:checkbox name="chCashBackOverrideInDollars" /></font></td>
</tr>

<tr><td valign=top colspan=5>&nbsp;</td></tr>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top colspan=2>&nbsp;</td>
<td valign=top colspan=2><jato:button name="btFeeReview" extraHtml="width=70 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/fee_review.gif" /></td>
</tr>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<td valign=top bgcolor=3366cc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top bgcolor=3366cc colspan=5><font size=3 color=ccffff>&nbsp;<b>Required to Qualify:</b></font></td>
</tr>

<tr>
<td valign=top bgcolor=3366cc>&nbsp;</td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff><b>Maximum Principal Balance Allowed:</b><br>
<jato:text name="stUWMaximumPrincipalBalanceAllowed" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff><b>OR</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff><b>Minimum Income Required:</b><br>
<jato:text name="stUWMinimumIncomeRequired" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff><b>OR</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
<td valign=top bgcolor=3366cc><font size=2 color=ccffff><b>Maximum TDS Expenses Allowed:</b><br>
<jato:text name="stUWMaximumTDSExpenseAllowed" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
</tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td>&nbsp;</td>
<td colspan=5><jato:button name="btRecalculate" extraHtml="width=75 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/recalculate.gif" />&nbsp;&nbsp;&nbsp;</td><tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</table>
<!--END TERMS//-->
