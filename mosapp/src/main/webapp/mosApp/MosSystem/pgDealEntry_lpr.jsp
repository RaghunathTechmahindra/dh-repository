<HTML>
<%@page info="pgDealEntry" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealEntryViewBean">

<script src="../JavaScript/rc/LenderProductRate.js" type="text/javascript"></script>  

<script language="javascript">

	var lenderArray = [];
	var productArray = [];
	var rateArray = [];
	var rateCodeArray = [];
	var paymentTermArray = [];
	var productIdArray = [];
	var paymentTermIdArray = [];
    var applicationDate = "<jato:text name="stAppDateForServlet" escape="true" />";
    var rateDisDate = "<jato:text name="stRateDisDateForServlet" escape="true" />";
                
    
    function delayInitLenderProductProfile()
    {
        /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4*******************/
        clearHtmlItems("all");
        /***************MCM Impl team changes ends - XS_2.1, XS_2.2, XS_2.4*******************/
        populateLenderList();

        var rateLock = "<jato:text name="stRateLock" fireDisplayEvents="true" escape="true" />";
        var dealEntry = "<jato:text name="stDealEntry" fireDisplayEvents="true" escape="true" />";
        var defaultProductId = "<jato:text name="stDefaultProductId" fireDisplayEvents="true" escape="true" />";
        var lenderProductId = "<jato:text name="stLenderProductId" fireDisplayEvents="true" escape="true" />";
        var rateTimeStamp = "<jato:text name="stRateTimeStamp" fireDisplayEvents="true" escape="true" />";
	    var rateInventoryId = "<jato:text name="stRateInventoryId" fireDisplayEvents="true" escape="true" />";
	    var pmntTermId = "<jato:text name="hdPaymentTermId" fireDisplayEvents="true" escape="true" />";
	    var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
	    var isPageEditable = "<jato:text name="stIsPageEditableForServlet" fireDisplayEvents="true" escape="true" />";
	    if(<jato:text name="stLenderProfileId" fireDisplayEvents="true" escape="true" /> >= 0)
	        var existedLenderId=<jato:text name="stLenderProfileId" fireDisplayEvents="true" escape="true" />;
	    else
            var existedLenderId=0;

        for ( i = 0 ;  i <=  document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options.length -1 ; i ++  )
        {
	        if ( document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options[i].value == existedLenderId  )
	        break; // found, quit.
        }
        if (i < document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options.length)
        {
            document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.selectedIndex = i ;
            //--DisableProductRateTicket#570--10Aug2004--start--// 
            if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
            //--DisableProductRateTicket#570--10Aug2004--end--//
            {
                populateLenderListRestricted();
                document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.length = 1;
            }
        }
        else
        return; // no match on lender - don't attempt further population

        ////alert("DelayILPP:: stamp4");

        getListInitial("document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct", existedLenderId);

        if (productArray.length == 0)
            return;

        tempProductId = -1 ;
        tempDefaultProdId = -1;

        // I. Default stuff is related to the brand new deal (page DealEntry) only!!
        ////if ( (document.forms[0].<%= viewBean.PAGE_NAME%>_stDealEntry.value == "Y") &&
        ////     (document.forms[0].<%= viewBean.PAGE_NAME%>_stDefaultProductId != null) )    // set default ProductId for this Lender
        if ( (dealEntry == "Y") &&
             (defaultProductId != null) )     // set default ProductId for this Lender
        {

            ////for ( i = 0 ; i <= document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length - 1; i ++ )
            for ( i = 0 ; i <= document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length; i ++ )
            {
                ////alert("DelayILPP:: stamp6");
                tempDefaultProdId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[i].value ; // get ProductId
                ////if  ( tempDefaultProdId ==  document.forms[0].<%= viewBean.PAGE_NAME%>_stLenderProductId.value  )
        if  ( tempDefaultProdId ==  lenderProductId  )
        {
          break ;
                    }
                          ////alert("DelayILPP:: stamp7");

        ////if  ( (tempDefaultProdId == document.forms[0].<%= viewBean.PAGE_NAME%>_stDefaultProductId.value) &&
        ////    (document.forms[0].<%= viewBean.PAGE_NAME%>_stLenderProductId.value == 0) )
        if  ( (tempDefaultProdId == defaultProductId) &&
            (lenderProductId == 0) )
        {
          break ;
                    }
                  }

      document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.selectedIndex = i;
            defaultProductId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;

      ////alert("DelayILPP:: stamp8");

      //--DisableProductRateTicket#570--10Aug2004--start--// 
      if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
      //--DisableProductRateTicket#570--10Aug2004--end--//
            {
    ////document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0].value = document.forms[0].<%= viewBean.PAGE_NAME%>_stLenderProductId.value;
    document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0].value = lenderProductId;
                productId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;
                populateProductRestricted( productId );
            }

    ////alert("DelayILPP:: stamp9");

    getListInitial("document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate", defaultProductId);

    ////alert("DelayILPP:: stamp8");

      tempRate = "";
      ////if ( document.forms[0].<%= viewBean.PAGE_NAME%>_stRateTimeStamp != null )     // check existed postedInterestRate
      if ( rateTimeStamp != null )    // check existed postedInterestRate
      {
          selectedRateId = 0 ;
        for ( i = 0 ; i <= document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length - 1 ; i ++ )
        {
          tempRate = document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i].value ;
          tempRate = tempRate.substring ( tempRate.length - 32 , tempRate.length )  ;
          ////if  ( tempRate ==  document.forms[0].<%= viewBean.PAGE_NAME%>_stRateTimeStamp.value  )
          if  ( tempRate ==  rateTimeStamp  )
          {
            selectedRateId = i ;
            break ;
          }
        }
        if (document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length > 0)
          document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex = selectedRateId ;
      }

      //--DisableProductRateTicket#570--10Aug2004--start--// 
      if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
      //--DisableProductRateTicket#570--10Aug2004--end--//
                {
                    rateValue = document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex ].value ;
                    populateRateRestricted( rateValue );
      }

        } // end DealEntry if

        // II. Page 'Deal Modification' case. First, pick up the saved ProductId for this LenderProfileId
        ////else if( document.forms[0].<%= viewBean.PAGE_NAME%>_stDealEntry.value == "N")
        else if( dealEntry == "N")
        {
        ////if ( document.forms[0].<%= viewBean.PAGE_NAME%>_stLenderProductId != null )     // set pre-selected ProductId
        if ( lenderProductId != null )    // set pre-selected ProductId
        {
        for ( i = 0 ; i <= document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length - 1 ; i ++ )
        {
          tempProductId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[i].value ;  // get ProductId
          ////if  ( tempProductId ==  document.forms[0].<%= viewBean.PAGE_NAME%>_stLenderProductId.value  )
          if  ( tempProductId ==  lenderProductId  )
            break ;
        }
        }

        if (tempProductId == -1)
        {
          i = 0;
          tempProductId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0].value;
        }

        document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.selectedIndex = i;

        //--DisableProductRateTicket#570--10Aug2004--start--// 
        if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
        //--DisableProductRateTicket#570--10Aug2004--end--//
              {
    ////document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0].value = document.forms[0].<%= viewBean.PAGE_NAME%>_stLenderProductId.value;
    document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0].value = lenderProductId;
                productId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;
                populateProductRestricted( productId );
              }

        getListInitial("document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate", tempProductId);

        tempRate = "";
        ////if ( document.forms[0].<%= viewBean.PAGE_NAME%>_stRateTimeStamp != null )     // check existed postedInterestRate
        if ( rateTimeStamp != null )    // check existed postedInterestRate
        {
          selectedRateId = 0;
          index = 0;

        //// Release2.1 optimization.
        for ( i = 0 ; i <= document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length - 1 ; i ++ )
        {
          tempRate = document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i].value ;  // get PricingRateInventoryId
          //alert("tempRate: " + tempRate);
          tempIndex = document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i].index ;  // get PricingRateInventoryId
          //alert("tempIndex: " + tempIndex);
          //alert("InventoryId_again: " + rateInventoryId);

          ////--Release2.1--//
          //// The whole java logic is changed now and based on pricingRateInventoryid
          //// passed here. This fragment along with the UWorksheet should be polished
          //// and separated.
          ////tempRate_1 = document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i].value ;
          ////tempRate_1 = tempRate.substring ( tempRate.length - 32 , tempRate.length );
          /////alert("tempRate_again:" +  tempRate_1);

          ////if  ( tempRate ==  document.forms[0].<%= viewBean.PAGE_NAME%>_stRateTimeStamp.value  )
          ////if  ( tempRate ==  rateTimeStamp  )
          if  ( tempRate ==  rateInventoryId )

          {
            //--Release2.1--//
            //// The logic is changed now (see above).
            ////selectedRateId = i ;
            index = tempIndex;

            //alert("SelectedRateIdInIf: " + selectedRateId);
            //alert("IndexInIf: " + index);
            break ;
          }
        }
        if (document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length > 0)
        {
          ////alert("IndexToSet: " + index);

          //--Release2.1--//
          //// The logic is changed now (see above).
          ////document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex = selectedRateId;
          document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex = index;

                                }
        }

              // case of locked rate
        //--DisableProductRateTicket#570--10Aug2004--start--// 
        if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
        //--DisableProductRateTicket#570--10Aug2004--end--//
              {
                  rateValue = document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex ].value ;
                  populateRateRestricted( rateValue );
        }
           } // end if else DealModification case

            
            /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4*******************/
            populateProductType();
            populateRePaymentType(true); //set default value from deal table
            swichDisplayComponentInfo();
            /***************MCM Impl team changes ends - XS_2.1, XS_2.2, XS_2.4*******************/
		
		///Qualifying Rate 5.0
		//Only set it on load if it was not already set from DB
		if(document.forms[0].<%= viewBean.PAGE_NAME%>_txQualifyRate.value=="")
			getQualifyProductRate()
	}


    function getLenderProfiles()
    {
        var extractor = new filogix.express.service.LPRProcessorExtractor();
        temp = extractor.getLendersString();
    
        lenderArray  = new Array;
        string2Array ( temp , lenderArray );
    }

    //--DisableProductRateTicket#570--10Aug2004--start--//    
    function getLenderProducts( lenderId ) {

        var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
        var rateDisDate = "<jato:text name="stRateDisDateForServlet" escape="true" />";
                                                                                            
        var extractor = new filogix.express.service.LPRProcessorExtractor();
        extractor.lenderProfileId = lenderId;
        extractor.rateDisabledDate = rateDisDate;
        extractor.dealRateStatusId = dealRateStatusId;
        extractor.applicationDate = applicationDate;
        
        temp = extractor.getProductsString();
        productArray = new Array ;
        string2Array ( temp , productArray );

    }
    //--DisableProductRateTicket#570--10Aug2004--end--// 

    function getPaymentTerms ( )
    {
        var extractor = new filogix.express.service.LPRProcessorExtractor();
        temp = extractor.getPaymentTermString();
        
        tempPaymentTermArray = new Array ;
        string2Array ( temp , tempPaymentTermArray  );
    
        for ( i = 0 ; i <= tempPaymentTermArray.length -1 ; i ++ )
        {
            tempItem = tempPaymentTermArray [i];
            sepPos =  tempItem.indexOf ( "," ) ;
      
            //--Release2.1--//
	        //// Populate new PaymentTermId array to populate new hidden 
	        //// PaymentTermId field for database population from the screen.
	        sepIds = tempItem.indexOf (":");
      
            if ( sepPos > 0 )
            {
                paymentTermArray[i] = tempItem.substring(1, sepPos-1 );
        
                //--Release2.1--//
                //// Populate new PaymentTermId array to populate new hidden 
                //// PaymentTermId field for database population from the screen.
                ////productIdArray[i] = tempItem.substring( sepPos+2 , tempItem.length -1 ) ;

                productIdArray[i] = tempItem.substring( sepPos+2 , sepIds - 1 ) ;           
                paymentTermIdArray[i] = tempItem.substring( sepIds+2 , tempItem.length -1 ) ;
        
            }
        }
    }

    function getRateCodes( productId )
    {
        ////alert("getRateCodes::start");

        lenderId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;
        //// Do it locally. Application Date is for future delivery of the RateAdmin bug.
        applicationDate = "<jato:text name="stAppDateForServlet" escape="true" />";
        //--DisableProductRateTicket#570--10Aug2004--start--// 
        var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
        var rateDisDate = "<jato:text name="stRateDisDateForServlet" escape="true" />";
        //--DisableProductRateTicket#570--10Aug2004--end--// 
                
        var extractor = new filogix.express.service.LPRProcessorExtractor();
        extractor.lenderProfileId = lenderId;
        extractor.productId = productId;
        extractor.dealRateStatusId = dealRateStatusId;
        extractor.applicationDate = applicationDate;
        extractor.rateDisabledDate = rateDisDate;
        temp = extractor.getRateCodeString();
        

	    rateCodeArray = new Array;
	    string2Array( temp, rateCodeArray);
	
	    for ( i=0 ; i <= rateCodeArray.length -1 ; i ++ )
	    {
	        tempRateCode = rateCodeArray[i] ;
	        pos = tempRateCode.indexOf ( "," ) ;
	        if ( pos > 0 )
	            rateCodeArray[i] = tempRateCode.substring ( 1, pos-1  ) ;
	    }
    }

    function getLenderProductRates( productId )
    {
        lenderId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;
    
        //// Do it locally. Application Date is for future delivery of the RateAdmin bug.
	    applicationDate = "<jato:text name="stAppDateForServlet" escape="true" />";                                
	    //--DisableProductRateTicket#570--10Aug2004--start--//    
	    var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
	    var rateDisDate = "<jato:text name="stRateDisDateForServlet" escape="true" />";
        //--DisableProductRateTicket#570--10Aug2004--end--// 
        
        var hdDealId = "<jato:text name="hdDealId" escape="true" />";
        var hdDealCopyId = "<jato:text name="hdDealCopyId" escape="true" />";
        
        var extractor = new filogix.express.service.LPRProcessorExtractor();
        extractor.lenderProfileId = lenderId;
        extractor.productId = productId;
        extractor.dealId = hdDealId;
        extractor.dealCopyId = hdDealCopyId;
        extractor.dealRateStatusId = dealRateStatusId;
        extractor.applicationDate = applicationDate;
        extractor.rateDisabledDate = rateDisDate;
        temp = extractor.getLenderProductRateString();

        rateArray = new Array;
        string2Array( temp, rateArray);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SourceString Format:
  //    'productName', 'id' ? 'productName', 'id' ?

	function string2Array( sourceStr, destArray )
	{
		var lineDelim = "<%= com.filogix.express.web.rc.lpr.converter.AbstractLegacyCSVConverter.LINE_DELIMITER %>";
	    i = sourceStr.indexOf (lineDelim);
	    itemCounter = 0;
	    aitem = "";
	    aitem = sourceStr.substring( 1, 2);
	    while ( i > 0 )
	    {
	        aitem = sourceStr.substring ( 0,  i  );
		    destArray[ itemCounter ] = aitem;
		    sourceStr = sourceStr.substring( i + 1 );
		    i = sourceStr.indexOf(lineDelim) ;
		    itemCounter ++  ;
	    }
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function populateLenderList() {

      ////alert("populateLenderList::start");

      getLenderProfiles() ; // populate the LenderArray

      ////alert("populateLenderList::after getLenderProfiles");

      ////DIFFERENT TESTING OF THE LENDERID INVOCATION.

      ////alert("populateLenderList::LenderArrayLength: " + lenderArray.length);
      ////alert("populateLenderList::cbLenderObject: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.value);
      ////alert("populateLenderList::cbLenderOptionsLength: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options.length);
      ////alert("populateLenderList::cbChargeTermObject: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbChargeTerm.value);


      ////alert("PLL::RateLockValue: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbRateLocked.value);
      ////alert("PLL::RateLockName: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbRateLocked.name);

      ////alert("PLL::RateLockBoundName: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbRateLocked.boundName);
      ////alert("PLL::RateLockParent: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbRateLocked.parent);
      ////alert("PLL::RateLockOptionList: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbRateLocked.options);
      ////alert("PLL::RateLockOptionListSize: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbRateLocked.options.size);

      ////alert("populateLenderList::0_element: " + lenderArray[0]);

      //// VERY TEMP TO TEST THE COMBOBOX POPULATION.
      ////eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbRateLocked.options[0]=" + "new Option(" + lenderArray[0] + ")" );
      ////alert("populateLenderList::cbLenderElement: " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbRateLocked.value);

  while ( lenderArray.length < document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options.length ) {
    document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options[ (document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options.length - 1 ) ] = null ;
  }
  for ( var i=0; i< lenderArray.length; i++) {
    eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options[i]=" + "new Option(" + lenderArray[i] + ")" );
  }
  document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0]=null;

  ////alert("populateLenderList::finished");
}

function populateLenderListRestricted() {
  getLenderProfiles() ; // populate the LenderArray

  for ( var i=0; i< lenderArray.length; i++) {
    eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options[i]=" + "new Option(" + lenderArray[document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.selectedIndex] + ")" );
  }

  document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options[0].length = 1;

  document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0]=null;

}

function populateProduct(){


  ////alert("populateProduct::start");

  while (productArray.length < document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length) {
    document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[(document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length - 1)] = null;
  }
  for (var i=0; i < productArray.length; i++) {
    eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[i]=" + "new Option(" + productArray[i]+")");
  }

  if ( document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length > 0 )
  {
    getPaymentTerms();
    populatePaymentTerm() ;
  }else {
    document.forms[0].<%= viewBean.PAGE_NAME%>_tbPaymentTermDescription.value = "" ;
  }

  ////alert("populateProduct::finish");
}

function populateProductRestricted(id){

  document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0] = null;

  for (var i=0; i < productArray.length; i++)
  {
      a = new Object();
        a = productArray[i].split (",");

      index = new String(a[1]).match(/\d+/);

    if  ( index == id)
        {
        eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[0]=" + "new Option(" + productArray[i]+")");
        document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.length = 1;
          break ;
        }
  }
}

function showRate(){
  document.open ;
  document.write (  " Rate = " + document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[  document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex  ].value  ) ;
  document.close;
}

function populateRate() {

  while (0 < document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length) {
    document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[(document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length - 1)] = null;
  }
  for (var i=0; i < rateArray.length; i++) {
    eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i]=" + "new Option(" + rateArray[i]+")");
  }

}


function populateRateRestricted(inValue)
{
    document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[0] = null;

    strIn = "\'" + new String(inValue) + "\'";

  for (var i=0; i < rateArray.length; i++)
  {
      a = new Object();
        a = rateArray[i].split (",");
        index = new String(a[1]);
    if  ( index == strIn)
        {
        eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[0]=" + "new Option(" + rateArray[i]+")");
        document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.length = 1;
          break ;
        }
  }
}

function populateRateCode( ) {


  id = document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex ;

  if ( ( id == -1 ) ||  ( rateCodeArray.length <=0)  ) {
    document.forms[0].<%= viewBean.PAGE_NAME%>_tbRateCode.value = "";

    document.forms[0].<%= viewBean.PAGE_NAME%>_hdLongPostedDate.value = "";

    }
  else {
    document.forms[0].<%= viewBean.PAGE_NAME%>_tbRateCode.value = rateCodeArray[ id ];

        document.forms[0].<%= viewBean.PAGE_NAME%>_hdLongPostedDate.value = document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate.options[ id ].value ;
    }
}


function populatePaymentTerm( ) {
  tempId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.selectedIndex ;

  if ( tempId == null )
    return ;
  if ( tempId ==  -1 )
    tempId = 0 ;

  pId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options[ tempId ].value ;

  for ( id= -1,   i = 0 ; i <= productIdArray.length -1 ; i ++ )
  {
    if ( productIdArray[i] == pId  )
    {   id = i ;
      break ;
    }
  }
  if ( id == -1  )
  {
    document.forms[0].<%= viewBean.PAGE_NAME%>_tbPaymentTermDescription.value = "";
  }
  else
  {
    document.forms[0].<%= viewBean.PAGE_NAME%>_tbPaymentTermDescription.value = paymentTermArray[id];
        
    //--Release2.1--//
    //// Populate new PaymentTermId array to populate new hidden 
    //// PaymentTermId field for database population from the screen. 
    pmntTermId = paymentTermIdArray[id];    
    document.forms[0].<%= viewBean.PAGE_NAME%>_hdPaymentTermId.value = paymentTermIdArray[id];
    
    ////alert("PaymentTermIdPopulate: " + paymentTermIdArray[id]);  
    ////alert("PaymentTermPopulate: " + paymentTermArray[id]);
    ////alert("Final: " + document.forms[0].<%= viewBean.PAGE_NAME%>_hdPaymentTermId.value);    
    
        }   
}

function getListInitial(whichtype, id)
{
  ////alert("getListInitial::start");

  if(whichtype=="document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct")
  {
    getLenderProducts( id );

    ////alert("getListInitial::ID: " + id);

    populateProduct();

    return;
  }

  if(whichtype=="document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate")
  {
    if (id == -1)
    {
      rateArray = new Array ;
      populateRate();
      populateRateCode();
    }

    else
    {
      getLenderProductRates( id );
      populateRate();

      getRateCodes( id);
      populateRateCode( );

      getPaymentTerms();
      populatePaymentTerm( );
    }

    return;
  }
}

function getList(whichtype){

  var afterProduct="false";
  if(whichtype=="document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct")
  {
  	/***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4*******************/
  	if(IsChangingLenderOK() == false) return;
  	clearHtmlItems("lender");
  	/***************MCM Impl team changes ends - XS_2.1, XS_2.2, XS_2.4*******************/
  	
    lenderId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;

    getLenderProducts( lenderId ) ;   // Change the products
    populateProduct();

        afterProduct="true";
  }
  if ( productArray.length == 0 )
  {
    rateArray = new Array ;
    populateRate();
    populateRateCode();
  }

  else if (  (whichtype=="document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate")  || ( afterProduct=="true")   )  {

    if ( afterProduct != "true" )
      productId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;
    else if ( productArray.length > 0 )
        productId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options [ 0 ].value ;

    /***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4*******************/
    if(afterProduct != "true"){ //runs only when this method called by product list
	    if (IsChangingProductOK() == false) return;
	    clearHtmlItems("product");
    }
    /***************MCM Impl team changes ends - XS_2.1, XS_2.2, XS_2.4*******************/
    
    getLenderProductRates( productId ) ;
    populateRate();

    getRateCodes( productId );
    populateRateCode( ) ;

    populatePaymentTerm( ) ;
  }

	/***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4*******************/
	//populate productType : only corresponding data
	populateProductType();
	//populate repaymentType : only corresponding 
	populateRePaymentType(false);
	// finally, record current selection of lender and product
	recordCurrentSelection();
	/***************MCM Impl team changes ends - XS_2.1, XS_2.2, XS_2.4*******************/
}

function getListRestricted(whichtype){

  var afterProduct="false";
  if(whichtype=="forms[0].<%= viewBean.PAGE_NAME%>_cbProduct")
  {
    lenderId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;

    getLenderProducts( lenderId ) ;   // Change the products
        populateProductRestricted();

        document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length = 1;

        afterProduct="true";
  }
  if ( productArray.length == 0 )
  {
    rateArray = new Array ;
    populateRate();
    populateRateCode();
  }

  else if (  (whichtype=="forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate")  || ( afterProduct=="true")   )  {
    if ( afterProduct != "true" ) {
      productId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;
      populateProductRestricted();

            document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length = 1;
    }

    else if ( productArray.length > 0 ) {
        productId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options [ 0 ].value ;
                document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct.options.length = 1;

    }

    getLenderProductRates( productId ) ;
    populateRateRestricted();

    getRateCodes( productId );
    populateRateCode( ) ;

    populatePaymentTerm( ) ;
  }
}

/*******************************************************************************/
/***************MCM Impl team changes starts - XS_2.1, XS_2.2, XS_2.4*******************/
//  message keys.
filogix.util.RichClientMessages.sysMsgKeys = new Array();
filogix.util.RichClientMessages.sysMsgKeys =
    ["INVALID_PRODUCT_CHANGE_COMPONENTS_EXIST"];
    
// CONSTANT value 
var CONST_JSON_COL = {
    REPAYMENTTYPE: {
        PRODUCTID: "productId",
        REPAYMENTID: "repaymentId",
        REPAYMENTDESC: "repaymentDesc"
    },
     COMPONENT_ELIGIBLE_CHECK: {
        ELIGIBLE_FLG: "compEligibleFlg"
    },
    COUNT_COMPONENTS: {
        COMPONENT_SIZE: "componentSize"
    },
     PRODUCTTYPE: {
        PRODUCTTYPEID: "productTypeId",
        PRODUCTTYPEDESC: "productTypeDesc"
    },
     RATECODE: {
        RATECODE: "rateCode"
    }
};

/******************************************************************
 * Html access methods
 ******************************************************************/
function getCbLenderObj(){
    return  document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender;
}

function getCbProductObj(){
    return  document.forms[0].<%= viewBean.PAGE_NAME%>_cbProduct;
}

function getCbPostedInterestRateObj(){
    return  document.forms[0].<%= viewBean.PAGE_NAME%>_cbPostedInterestRate;
}

function getCbProductTypeObj(){
    return  document.forms[0].<%= viewBean.PAGE_NAME%>_cbProductType;
}

function getCbRepaymentTypeObj(){
    return  document.forms[0].<%= viewBean.PAGE_NAME%>_cbRepaymentType;
}

function getTbPaymentTermDescriptionObj(){
    return  document.forms[0].<%= viewBean.PAGE_NAME%>_tbPaymentTermDescription;
}

function getTbRateCodeObj(){
    return  document.forms[0].<%= viewBean.PAGE_NAME%>_tbRateCode;
}

/**
 * returns value of jato:text name="stDefaultRepaymentTypeId" 
 */
function getStDefaultRepaymentTypeIdValue(){
    return <jato:text name="stDefaultRepaymentTypeId" />;
}

/******************************************************************
 * ajax download methods
 ******************************************************************/
function getAjaxProductType() {

    var extractor = new filogix.express.service.LPRProcessorExtractor();
    extractor.productId = getCbProductObj().value;
    return extractor.getProductTypeString();    
}

function getAjaxRePaymentType(){
    var extractor = new filogix.express.service.LPRProcessorExtractor();
    extractor.productId = getCbProductObj().value;
    return extractor.getRepaymentTypeString();
}

function getAjaxComponentEligibleCheck(){
    var extractor = new filogix.express.service.LPRProcessorExtractor();
    extractor.productId = getCbProductObj().value;
    return extractor.getComponentEligibleCheckString();
}

function getAjaxCountComponents(){
    var extractor = new filogix.express.service.LPRProcessorExtractor();
    return extractor.getCountComponentsString();
}
/******************************************************************
 * populating methods and handler
 ******************************************************************/
/**
 * populates product type combo box
 * this combo box only display corresponding value with the selected product.
 */
function populateProductType(){
    //clear
    getCbProductTypeObj().length = 0;
    
    // populate combobox and set default: component.mtgProdId
    populateSelectObjByJSON(getCbProductTypeObj(), getAjaxProductType(), 
        CONST_JSON_COL.PRODUCTTYPE.PRODUCTTYPEID, 
        CONST_JSON_COL.PRODUCTTYPE.PRODUCTTYPEDESC);
    
    // always lock  
    lockComboBox(getCbProductTypeObj(), getCbProductTypeObj().value);
}

/**
 * populates repayment type combo box
 * this combo box only display corresponding value with the selected product.
 */
function populateRePaymentType(forInitializing){
    //clear
    getCbRepaymentTypeObj().length = 0;
    
    //download data 
    var repaymentTypeArray = getAjaxRePaymentType();
    
    // populate combobox
    populateSelectObjByJSON(getCbRepaymentTypeObj(), repaymentTypeArray, 
        CONST_JSON_COL.REPAYMENTTYPE.REPAYMENTID,   
        CONST_JSON_COL.REPAYMENTTYPE.REPAYMENTDESC);

    //select row by repaymenttypeid in mtgProd table
    var aRepaymentType;
    if(forInitializing){
        //defaultRepaymetTypeid paramemter was specified
        aRepaymentType = findTargetRowFromJSONArray(repaymentTypeArray, 
            CONST_JSON_COL.REPAYMENTTYPE.REPAYMENTID, getStDefaultRepaymentTypeIdValue());
    }else{ 
        aRepaymentType = findTargetRowFromJSONArray(repaymentTypeArray, 
            CONST_JSON_COL.REPAYMENTTYPE.PRODUCTID, getCbProductObj().value);
    }
    if (aRepaymentType) { // corresponding repayment found
        selectRow(getCbRepaymentTypeObj(), aRepaymentType.repaymentId);
    }
}

/**
 *  this method checks if changing product combobox is ok or not.
 *  criteria :
 *  on cbProduct change:
 *      > componentEligibleFlg sould be "Y" or no compoent should exist
 *   
 * @return boolean - true: it's ok to change, false: check error.
 */
function IsChangingProductOK(){
    
    var compChek = getAjaxComponentEligibleCheck();
    var countComp = getAjaxCountComponents();
    if(compChek.compEligibleFlg != "Y" && countComp.componentSize > 0){
        complainAboutChangingProduct();
        revertProductChange();
        return false;
    }

    swichDisplayComponentInfo(compChek.compEligibleFlg == "Y");
    return true;   
}

/**
 *  this method checks if changing lender combobox is ok or not.
 *  criteria :
 *  on cbLender change:
 *      > no compoent should exist
 *   
 * @return boolean - true: it's ok to change, false: check error.
 */
function IsChangingLenderOK(){
    var countComp = getAjaxCountComponents();
    //if components exist, changing is not allowed
    if(countComp.componentSize > 0) {
        complainAboutChangingProduct();
        revertLenderChange();
        return false;
    }
    return true;   
}

// previous value of cblender
var previousLenderId=-1;
// previous value of cbProduct
var previousProductId=-1;

/**
 *  record selected value of cbLender and cbProduct
 *  this method will be called when user click cbLender or cbProduct
 *  or bottom of  getList method
 */
function recordCurrentSelection(){
    previousLenderId = getCbLenderObj().value;
    previousProductId = getCbProductObj().value;
}

/**
 *  revert back user's selection of cbLender 
 */
function revertLenderChange(){
    selectRow(getCbLenderObj(), previousLenderId);
}

/**
 *  revert back user's selection of cbProduct
 */
function revertProductChange(){
    selectRow(getCbProductObj(), previousProductId);
}

/**
 *  display error message
 */
function complainAboutChangingProduct(){
    var message = new filogix.util.RichClientMessages();
    /* Bug Fix : FXP2558 : Removed Exsting Code and added following line of code */
    alert(INVALID_PRODUCT_CHANGE_COMPONENTS_EXIST);
}

/**
 * swich component info section's display
 * @param display(optional) : boolean 
 *   - true: show comp info section, false: hide comp info section
 *  if display parameter is not specified, this method trys to download using
 *  getAjaxComponentEligibleCheck data. 
 */
function swichDisplayComponentInfo(display){
    display = display || getAjaxComponentEligibleCheck().compEligibleFlg == "Y";
    //FXP24347 starts
    if (display) {
        var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
        var extractor = new filogix.express.service.LPRProcessorExtractor();
        extractor.lenderProfileId = 0;
        extractor.applicationDate = applicationDate;
        extractor.rateDisabledDate = rateDisDate;
        extractor.dealRateStatusId = dealRateStatusId;
        extractor.componentTypeId = -1;
        extractor.isUmbrella = "N";
        var ps = extractor.getProductsString();
        var temp = new Array();
        string2Array(ps, temp);
        if (temp.length<=0)
            display = false;
    }
    //FXP24347 ends
    var divComponentInfo = document.getElementById("divComponentInfo");
    if(display == false){
       divComponentInfo.style.display="none";
    }else{
        divComponentInfo.style.display="block";
    }
}

/**
 * clear html objects
 * @param level , 
 *        "all" : delete all related html objects
 *        "lender" : product, rate, paymenttype 
 *        "product" : delete all related html objects
 */
function clearHtmlItems(level){
    
    level = level || "all"; //default - all
    switch (level) {
        case "all":
            if(getCbLenderObj()) getCbLenderObj().length = 0;
        case "lender":
            if(getCbProductObj()) getCbProductObj().length = 0;
        case "product":
            if(getCbPostedInterestRateObj()) getCbPostedInterestRateObj().length = 0;
            if(getTbPaymentTermDescriptionObj()) getTbPaymentTermDescriptionObj().value = "";
            if(getCbProductTypeObj()) getCbProductTypeObj().length = 0;
            if(getCbRepaymentTypeObj()) getCbRepaymentTypeObj().length = 0;
            if(getTbRateCodeObj()) getTbRateCodeObj().value = "";
    }  
}

/******************************************************************
 * utilities
 ******************************************************************/
/**
 *  selects specific row in <selectObj> which has the same value as <selectValue>
 *  if no row is fund, nothing is selected.
 * 
 * param <selectObj> - html select object
 * param <selectValue> - the value you want to select
 */
function selectRow(selectObj, selectValue){
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value == selectValue) 
            selectObj.options[i].selected = true;
    }
}

/**
 *  selects specific row in <selectObj> which has the same value as <selectValue> and delete other rows.
 *  if no row is fond, all rows are deleted. 
 * 
 * param <selectObj> - html select object
 * param <selectValue> - the value you don't want to delete
 */
function lockComboBox(selectObj, selectValue){
    var op = null;
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value == selectValue) {
            op = selectObj.options[i];
            break;
        }
    }
    selectObj.length = 0;
    if (!op) return;
    selectObj.options[0] = op;
}

/**
 *  populate html select object : <selectObj> using <jsonObj>.
 *  <keyColumn>and<DesplayColumn> must exist in <jsonObj>
 *  <keyColumn> data in <jsonObj> is set to <selectObj> as "value"
 *  <DesplayColumn> data in <jsonObj> is set to <selectObj> as desplay value
 * 
 *  param <selectObj> : html select object
 *  param <jsonObj> : list data of JSON data
 *  param <keyColumn> : column name to be used for value of select object
 *  param <DisplayColumn>: column name to be used for display data of select object
 */
function populateSelectObjByJSON(selectObj, jsonObj, keyColumn, DisplayColumn){
    selectObj.options.length = 0;
    for (var i in jsonObj) {
        selectObj.options[i] = new Option(jsonObj[i][DisplayColumn], jsonObj[i][keyColumn]);
    }
}

/**
 *  finds an element from list data : <jsonobj>
 *  
 *  param <jsonObj>: list data
 *  param <findRowName>: variable name to be checked.
 *  param <findvalue>: data to be found. 
 */
function findTargetRowFromJSONArray(jsonObj, findRowName, findValue){
    for (var i in jsonObj) {
        for (var colName in jsonObj[i]) {
            if (colName == findRowName) {
                if (jsonObj[i][colName] == findValue) {
                    return jsonObj[i];
                }
            }
        }
    }
}

/***************MCM Impl team changes ends -  XS_2.1, XS_2.2, XS_2.4*******************/
/*******************************************************************************/

	function getQualifyProductRate()
    {
        lenderId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;

                //// Do it locally. Application Date is for future delivery of the RateAdmin bug.
        applicationDate = "<jato:text name="stAppDateForServlet" escape="true" />";                                
        //--DisableProductRateTicket#570--10Aug2004--start--//      
        var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
        var rateDisDate = "<jato:text name="stRateDisDateForServlet" escape="true" />";
        
        //#DG494 just add deal id  - took the opportunity to change the format
        var hdDealId = "<jato:text name="hdDealId" escape="true" />";
        var hdDealCopyId = "<jato:text name="hdDealCopyId" escape="true" />";
        var productId = productId = document.forms[0].<%= viewBean.PAGE_NAME%>_cbQualifyProductType.options [ document.forms[0].<%= viewBean.PAGE_NAME%>_cbQualifyProductType.selectedIndex ].value ;
		
        var extractor = new filogix.express.service.LPRProcessorExtractor();
        extractor.lenderProfileId = lenderId;
        extractor.productId = productId;
        extractor.dealId = hdDealId;
        extractor.dealCopyId = hdDealCopyId;
        extractor.dealRateStatusId = dealRateStatusId;
        extractor.applicationDate = applicationDate;
        extractor.rateDisabledDate = rateDisDate;
        
		temp = extractor.getQualifyProductRateString();
        
        var qualifyRateArray = new Array;
        string2Array( temp, qualifyRateArray);
		
		document.forms[0].<%= viewBean.PAGE_NAME%>_txQualifyRate.value = round_decimals(qualifyRateArray[0], 3);
		
		var hdQualifyRate = document.getElementById("hdQualifyRate");
        //hdQualifyRate.value = parseFloat(qualifyRateArray[0]);
		fetchedRate = parseFloat(qualifyRateArray[0]);
	}
</script>

</jato:useViewBean>
</HTML>