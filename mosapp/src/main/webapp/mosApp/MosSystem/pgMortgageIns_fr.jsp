
<HTML>
<%@page info="pgMortgageIns" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgMortgageInsViewBean">

<head>
<TITLE>Assurance pr�t-hypoth�caire</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcs.js" type="text/javascript"></script>

</head>

<body bgcolor=ffffff link=000000 vlink=000000 alink=000000>

<jato:form name="pgMortgageIns" method="post" onSubmit="return IsSubmitButton();">

<p>

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="false" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<jato:hidden name="hdLienPosition" elementId="hdLienPosition" /> 
<jato:hidden name="hdMIStatusUpdateFlag" elementId="hdMIStatusUpdateFlag" /> 
<jato:hidden name="hdMIPremium" />
<jato:hidden name="hdMIStatusId" elementId="hdMIStatusId" />
<jato:hidden name="hdMIPolicyNoCMHC" elementId="hdMIPolicyNoCMHC"/>
<jato:hidden name="hdMIPolicyNoGE" elementId="hdMIPolicyNoGE" />
<jato:hidden name="hdProgressAdvance" elementId="hdProgressAdvance" />
<jato:hidden name="hdProgressAdvanceHidden" elementId="hdProgressAdvanceHidden"/> 
<input type="hidden" id="hdDealId" value='<jato:text name="stHDDealId"/>'/>
<input type="hidden" id="hdDealCopyId" value='<jato:text name="stHDDealCopyId"/>'/>
<jato:hidden name="hdMIPolicyNoAIGUG" elementId="hdMIPolicyNoAIGUG" />
<jato:hidden name="hdMIPolicyNoPMI" elementId="hdMIPolicyNoPMI" />
<jato:hidden name="hdDealStatusId" elementId="hdDealStatusId" />
<jato:hidden name="hdPreviousMIIndicatorId" elementId="hdPreviousMIIndicatorId"/>
<!--QC-Ticket-268- Start-->
<jato:hidden name="hdSpecialFeatureId" elementId="hdSpecialFeatureId"/>
<!--QC-Ticket-268- End-->

<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
    
    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
    
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 
	
    <!--END HEADER//-->

    <!--DEAL INFO BANNER//-->

	<!--DEAL SUMMARY SNAPSHOT//-->
		<div id="pagebody" class="pagebody" name="pagebody">
		<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>

	<!--END DEAL SUMMARY SNAPSHOT//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<!--<form name="bodyform"> -->
<input type="hidden" name="netLoanAmount" value="100000"><!--POPULATED FROM SERVER//-->
<input type="hidden" name="ltv" value="83.20"><!--POPULATED FROM SERVER//-->

<!--END DEAL INFO BANNER//-->
 
<!-- ////SYNCADD Source Info. Start -->
<TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#d1ebff border=0>
<TBODY>
<TR>
  <TD colSpan=7><IMG height=2 alt="" src="../images/light_blue.gif" width="100%" border=0></TD>

 <tr>
 <td valign=top>&nbsp;</td>
 <td colspan=6><font size=3 color=3366cc><b>Information sur la source:</b></font></td>
 
 <tr>
 <td valign=top></td>
 <td valign=top><font size=2 color=3366cc><b>Firme source:</b></font><br><FONT size=2><jato:text name="stSourceFirmName" escape="true" /></FONT></td>
 <td valign=top><font size=2 color=3366cc><b>Source:</b></font><br><FONT size=2><jato:text name="stSOBFirstName" escape="true" />&nbsp;<jato:text name="stSOBLastName" escape="true" /></FONT></td>
 <td valign=top><font size=2 color=3366cc><b>Adresse de la source:</b></font><BR><FONT size=2><jato:text name="stSOBAddressLine1" escape="true" />&nbsp;<jato:text name="stSOBAddressLine2" escape="true" />&nbsp;<jato:text name="stSOBCity" escape="true" />&nbsp;<jato:text name="stSOBProvince" escape="true" /></FONT></td>
 <td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone de la source:</b></font><br><FONT size=2><jato:text name="stSOBPhoneNumber" escape="true" formatType="string" formatMask="(???)???-????" />x<jato:text name="stSOBPhoneExt" escape="true" /></FONT></td>
 <td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur de la source:</b></font><br><FONT size=2><jato:text name="stSOBFaxNumber" escape="true" formatType="string" formatMask="(???)???-????" /></FONT></td>
 <td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de demande de la source de r�f�rence:</b></font><br><FONT size=2><jato:text name="stSOBRefNum" escape="true" /></FONT></td>

<TR>
  <TD colSpan=7><IMG height=1 alt="" src="../images/dark_bl.gif" width="100%" border=0></TD>
</TBODY>
</TABLE>
 <!-- //SYNCADD END Source Info //-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>
<td><font size=3 color=3366cc><b>Assurance pr�t-hypoth�caire:</b></font></td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Indicateur de l�assurance:</b></font><br><jato:combobox name="cbMIIndicator" elementId="cbMIIndicator"/></td>
<td valign=top><font size=2 color=3366cc><b>Statut de l�assurance:</b></font><br><jato:combobox name="cbMIStatus" elementId="cbMIStatus"/></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>N<sup>o</sup> de police de l�assurance existant:</b></font><BR>
<jato:textField name="txMIExistingPolicy" elementId="miPolicyNumber" size="25" maxLength="35" /></td>
<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Assureur pr�t-hypoth�caire:</b></font><br><jato:combobox name="cbMIInsurer" elementId="cbMIInsurer"/></td>
<td valign=top><font size=2 color=3366cc><b>Type d�assurance:</b></font><br><jato:combobox name="cbMIType" elementId="cbMIType" /></td>
<td valign=top><font size=2 color=3366cc><b>Payeur de l�assurance:</b></font><br><jato:combobox name="cbMIPayor" /></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> du certificat de pr�qualification:</b></font><br><jato:textField name="txMIPreQCertNum" elementId="txMIPreQCertNum" size="25" maxLength="50" /></td>
<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Prime d�assurance:</b></font><br><font size = 4><b>$</b></font><jato:textField name="txMIPremium" elementId="txMIPremium" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de certificat:</b></font><br>
<jato:textField name="txMIPolicyNo" elementId="txMIPolicyNo" size="25" maxLength="50" extraHtml="disabled" /></td>
<td valign=top><font size=2 color=3366cc><b>Frais pr�lev�s � l�acquisition d�assurance</b></font><br><font size="2">
<jato:radioButtons name="rbMIUpfront" layout="horizontal" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Intervention du souscripteur hypoth�caire requise</b></font><br><font size="2">
<jato:radioButtons name="rbMIRUIntervention" layout="horizontal" /></font></td>

<tr>
	<td valign="top"></td>
	<td valign="top"> <!--  colspan="4" -->
		<div id="divProgressAdvanceType">
          <font size=2 color=3366cc><b>Type d'avance �chelonn�e:</b></font><br>
			<jato:combobox name="cbProgressAdvanceType"/>&nbsp;&nbsp;&nbsp;&nbsp;
		</div>
	</td>
	 
<td valign=top>
<div id="divProgressAdvanceInspectionBy">
<font size=2 color=3366cc><b>Inspection de l'avance de progress par:</b></font><br><font size="2">
<jato:radioButtons name="rbProgressAdvanceInspectionBy" layout="horizontal" /></font>
</div>
</td>

<td valign=top><font size=2 color=3366cc><b>Hypotheques REER autog�r�:</b></font><br><font size="2">
<jato:radioButtons name="rbSelfDirectedRRSP" layout="horizontal" /></font></td>

<td valign=top><font size=2 color=3366cc><b>L'identificateur poursuivant de produit pour SCHL:</b></font><br>
<jato:textField name="txCMHCProductTrackerIdentifier" size="5" maxLength="5" extraHtml="onBlur='isFieldInteger(0);'" /></b></font></td>

</tr>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Remboursement MCVD:</b></font><br>
<jato:combobox name="cbLOCRepaymentType" /></td>

<td valign=top>
<div id="divReqStdSvc"><font size=2 color=3366cc><b>Demande pour sevice de base:</b></font><br>
<jato:radioButtons name="rbRequestStandardService" layout="horizontal" styleClass="rbRequestStandardService"/>
</div>
</td>

<td valign=top><font size=2 color=3366cc><b>Amortissement MCVD:</b></font><br>
<jato:text name="stLOCAmortizationMonths" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></td>

<td valign=top><font size=2 color=3366cc><b>Date d'�ch�ance pour un pr�t non Amorti MCVD:</b></font><br>
<jato:text name="stLOCInterestOnlyMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></td>
<!--FXP18626: </br>-->

</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td valign=top></td>
<td valign=top colspan=3 width="100%">
  <p align="left"><font size=2 color=3366cc><b>Commentaires de l�assureur:</b></font><br>&nbsp;&nbsp;
  <jato:textArea name="txMIComments" rows="4" cols="80" elementId="txMIComments" extraHtml="onBlur='return isFieldHasSpecialChar();'" /></p>
</td>
<td valign=bottom align="right" width="0%" nowrap>

<jato:button name="btToDealMod" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/correcterrors_fr.gif"  styleClass="offWithMIResponse" /><br>
<br />
<jato:button name="btProcessMI" src="../images/processMI_fr.gif" elementId="btProcessMI" style="visibility:hidden" onClick="return false;" />
<jato:button name="btSubmit" defaultValue="SUBMIT" extraHtml="width=86 height=25 alt='' border='0'" fireDisplayEvents="true"  src="../images/submit_fr.gif" styleClass="offWithMIResponse" elementId="btSubmit" />
<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/sign_cancel_fr.gif" /> 
<jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true); '" fireDisplayEvents="true" src="../images/ok.gif"  styleClass="offWithMIResponse"  />
</td>

<tr>
<td valign=top></td>
<td valign=top colspan=4><font size=2 color=3366cc><b>R�ponse de l�assureur:</b></font><br>&nbsp;&nbsp;
  <jato:textArea name="txMIResponse" elementId="miResponseArea" extraHtml="readonly" rows="25" cols="80" /></td>
<tr>
<td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td>
</table>


<p>
<br><br><br><br><br>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>


<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<script language="javascript">
<!--
if(pmGenerate=="Y") {
	openPMWinFr();
}

//-->
</script>


<%-- putting script at the bottom (refer http://developer.yahoo.com/performance/rules.html) --%>

<script src="../JavaScript/rc/services-simple.js" type="text/javascript"></script>
<script src="../JavaScript/rc/services-simple-mi.js" type="text/javascript"></script>
<script src="../JavaScript/rc/services-simple-mi-cg.js" type="text/javascript"></script>
<script src="../JavaScript/rc/pendingMIprocess.js" type="text/javascript"></script>

<script src="../JavaScript/rc/MITypeFilter.js" type="text/javascript"></script>
<script src="../JavaScript/AMLHandlingForMI.js" type="text/javascript"></script>
<script src="../JavaScript/rc/miresponseForMI.js" type="text/javascript"></script>

<script src="../JavaScript/screens/pgMortgageIns.js" type="text/javascript"></script>
    
</jato:form>
</body>
</jato:useViewBean>
</HTML>
