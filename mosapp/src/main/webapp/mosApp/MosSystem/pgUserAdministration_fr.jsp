
<HTML>
<%@page info="pgUserAdministration" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgUserAdministrationViewBean">

<HEAD>
<TITLE>Administration utilisateur</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include French System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>

</HEAD>

<SCRIPT LANGUAGE="JavaScript">

//===================== Local JavaScript (begin) -- adjusted by Billy for Jato FieldName format ==================
///////////////////////////// populating comboboxes ////////////////////
<jato:text name="stInstitutionArray" escape="false" />
<jato:text name="stRegionArray" escape="false" />
<jato:text name="stBranchArray" escape="false" />
<jato:text name="stGroupArray" escape="false" />


<jato:text name="stPageAccessArray" escape="false" />

//////////////////////////////////////////////////////////////
function populateInstitutions()
{
	var changedComboBox =(NTCP) ? event.target : event.srcElement;
	var inForm = document.pgUserAdministration;
	
	while (i0.length < inForm.pgUserAdministration_cbRegionNames.options.length) 
	{
		inForm.pgUserAdministration_cbRegionNames.options[(inForm.pgUserAdministration_cbRegionNames.options.length - 1)] = null;
	}

	//Clear All options
	inForm.pgUserAdministration_cbRegionNames.options.length = 0;
	for (var i=0; i < i0.length; i++)     
	{
		eval("inForm.pgUserAdministration_cbRegionNames.options[i]=" + "new Option(" + i0[i]+")");
	}
}

/////////////////////////////////////////////////////////////
function populateRegions()
{
	var changedComboBox =(NTCP) ? event.target : event.srcElement;
	var selectedOptionValue = getSelectedOptionValue(changedComboBox);	//getValue of the selected option
	var selectedOptionIndex = getSelectedOptionIndex(changedComboBox);	//getValue of the selected option

	arr = new Array();
	
	if(selectedOptionValue.length == 0 || selectedOptionValue == -1)
    {
    	clearComboBoxes("r");
		setDefaults("r");
		return;
	}

	var inForm = document.pgUserAdministration;

	//Modified by BILLY 19June2001 -- Populate Array name with InstitutionId
	//arr = findArrType(selectedOptionIndex-1,"r");
	arr = eval("r" + round_decimals(selectedOptionValue, 0));

//	while (arr.length < inForm.cbRegionNames.options.length) 
//		{
//			inForm.cbRegionNames.options[(inForm.cbRegionNames.options.length - 1)] = null;
//		}
	var x = selectedOptionValue + "";
	
	//Clear All options
	inForm.pgUserAdministration_cbRegionNames.options.length = 0;

  	for (var i=0; i < arr.length; i++) 
	{
		eval("inForm.pgUserAdministration_cbRegionNames.options[i]=" + "new Option(" + arr[i]+")");
	}
}
//////////////////////////////////////////////////////////////

function populateBranches()
{

	var changedComboBox =(NTCP) ? event.target : event.srcElement;
	var selectedOptionValue = getSelectedOptionValue(changedComboBox);	//getValue of the selected option
	var selectedOptionIndex = getSelectedOptionIndex(changedComboBox);	//getValue of the selected option

	arr = new Array();
	
	if(selectedOptionValue.length == 0 || selectedOptionValue == -1)
	{
    	clearComboBoxes("b");
		setDefaults("b");
		return;
	}
	    
	var inForm = document.pgUserAdministration;
	
	//Modified by BILLY 19June2001 -- Populate Array name with RegionId
	//arr = findArrType(selectedOptionIndex-1,"b");
	arr = eval("b" + round_decimals(selectedOptionValue, 0));

	//Clear All options
	inForm.pgUserAdministration_cbBranchNames.options.length = 0;

	for (var i=0; i < arr.length; i++) 
	{
		eval("inForm.pgUserAdministration_cbBranchNames.options[i]=" + "new Option(" + arr[i]+")");
	}
}

////////////////////////////////////////////////////////////   
function populateGroups()
{
	var changedComboBox =(NTCP) ? event.target : event.srcElement;
	var selectedOptionValue = getSelectedOptionValue(changedComboBox);	//getValue of the selected option
	var selectedOptionIndex = getSelectedOptionIndex(changedComboBox);	//getValue of the selected option

	arr = new Array();
	
	if(selectedOptionValue.length == 0 || selectedOptionValue == -1)
	{
	   	clearComboBoxes("g");
		setDefaults("g");
		return;
	}
	
	var inForm = document.pgUserAdministration;
	
	//Modified by BILLY 19June2001 -- Populate Array name with BranchId
	//arr = findArrType(selectedOptionIndex-1,"g");
	arr = eval("g" + round_decimals(selectedOptionValue, 0));

	//Clear All options
	//inForm.pgUserAdministration_cbGroupProfileName.options.length = 0;
	inForm.pgUserAdministration_lbGroups.options.length = 0;

	for (var i=0; i < arr.length; i++) 
	{
		//eval("inForm.pgUserAdministration_cbGroupProfileName.options[i]=" + "new Option(" + arr[i]+")");
		eval("inForm.pgUserAdministration_lbGroups.options[i]=" + "new Option(" + arr[i]+")");
	}

}
/////////////////////////////////////////////////////////// 

function setDefaults(arrtype)
{

	var inForm = document.pgUserAdministration;

 	arrReg = new Array("'S/O','-1'");
	arrBra = new Array("'S/O','-1'");
	arrGrp = new Array("'S/O','-1'");

	switch(arrtype)
	{
		case 'r':
			eval("inForm.pgUserAdministration_cbRegionNames.options[0]	=" + "new Option(" + arrReg[0]+")");
			eval("inForm.pgUserAdministration_cbBranchNames.options[0]	=" + "new Option(" + arrBra[0]+")");
			//eval("inForm.pgUserAdministration_cbGroupProfileName.options[0]	=" + "new Option(" + arrGrp[0]+")");
			eval("inForm.pgUserAdministration_lbGroups.options[0]	=" + "new Option(" + arrGrp[0]+")");
			break;
		case 'b':
			eval("inForm.pgUserAdministration_cbBranchNames.options[0]	=" + "new Option(" + arrBra[0]+")");
			//eval("inForm.pgUserAdministration_cbGroupProfileName.options[0]	=" + "new Option(" + arrGrp[0]+")");
			eval("inForm.pgUserAdministration_lbGroups.options[0]	=" + "new Option(" + arrGrp[0]+")");
			break;
		case 'g':
			//eval("inForm.pgUserAdministration_cbGroupProfileName.options[0]	=" + "new Option(" + arrGrp[0]+")");
			eval("inForm.pgUserAdministration_lbGroups.options[0]	=" + "new Option(" + arrGrp[0]+")");
			break;
		default:
			break;
	}
}
///////////////////////////////////////////////////////////

function clearComboBoxes(arrtype)
{
	var inForm = document.pgUserAdministration;
	switch(arrtype)
	{
	case 'r':
		while (arr.length < inForm.pgUserAdministration_cbRegionNames.options.length) 
		{
			inForm.pgUserAdministration_cbRegionNames.options[(inForm.pgUserAdministration_cbRegionNames.options.length - 1)] = null;
		}
		while (arr.length < inForm.pgUserAdministration_cbBranchNames.options.length) 
		{
			inForm.pgUserAdministration_cbBranchNames.options[(inForm.pgUserAdministration_cbBranchNames.options.length - 1)] = null;
		}
		//while (arr.length < inForm.pgUserAdministration_cbGroupProfileName.options.length) 
		//{
		//	inForm.pgUserAdministration_cbGroupProfileName.options[(inForm.pgUserAdministration_cbGroupProfileName.options.length - 1)] = null;
		//}
		while (arr.length < inForm.pgUserAdministration_lbGroups.options.length) 
		{
			inForm.pgUserAdministration_lbGroups.options[(inForm.pgUserAdministration_lbGroups.options.length - 1)] = null;
		}
		break;
		
	case 'b':
		while (arr.length < inForm.pgUserAdministration_cbBranchNames.options.length) 
		{
			inForm.pgUserAdministration_cbBranchNames.options[(inForm.pgUserAdministration_cbBranchNames.options.length - 1)] = null;
		}
		//while (arr.length < inForm.pgUserAdministration_cbGroupProfileName.options.length) 
		//{
		//	inForm.pgUserAdministration_cbGroupProfileName.options[(inForm.pgUserAdministration_cbGroupProfileName.options.length - 1)] = null;
		//}
		while (arr.length < inForm.pgUserAdministration_lbGroups.options.length) 
		{
			inForm.pgUserAdministration_lbGroups.options[(inForm.pgUserAdministration_lbGroups.options.length - 1)] = null;
		}
		break;

	case 'g':
		//while (arr.length < inForm.pgUserAdministration_cbGroupProfileName.options.length) 
		//{
		//	inForm.pgUserAdministration_cbGroupProfileName.options[(inForm.pgUserAdministration_cbGroupProfileName.options.length - 1)] = null;
		//}
		while (arr.length < inForm.pgUserAdministration_lbGroups.options.length) 
		{
			inForm.pgUserAdministration_lbGroups.options[(inForm.pgUserAdministration_lbGroups.options.length - 1)] = null;
		}

		break;

	default:
		break;

	}
}
///////////////////////////////////////////////////////////
function getSelectedOptionValue(comboBox)
{
    for(i=0; i<comboBox.options.length;i++)
    {
	if( comboBox.options[i].selected == true)
	   return comboBox.options[i].value;
    }
    return -1;
}

////////////////////////////////////////////////////////////

function getSelectedOptionIndex(comboBox)
{
    for(i=0; i<comboBox.options.length;i++)
    {
	if( comboBox.options[i].selected == true)
	   return i;
    }
    return -1;
}


////////////////////////////////////////////////////////////
function findArrType(ind,type)
{
	arr = new Array();

	if(type == "i")
	{
		if(ind == 0)
			return i0;
		if(ind==1)
			return i1;
		if(ind==2)
			return i2;
		if(ind==3)
			return i3;
		if(ind==4)
			return i4;
		if(ind==5)
			return i5;
		if(ind==6)
			return i6;
		if(ind==7)
			return i7;
		if(ind==8)
			return i8;
		if(ind==9)
			return i9;
		if(ind==10)
			return i10;
		if(ind==11)
			return i11;
	}
	if(type == "r")
	{
		if(ind == 0)
			return r0;
		if(ind==1)
			return r1;
		if(ind==2)
			return r2;
		if(ind==3)
			return r3;
		if(ind==4)
			return r4;
		if(ind==5)
			return r5;
		if(ind==6)
			return r6;
		if(ind==7)
			return r7;
		if(ind==8)
			return r8;
		if(ind==9)
			return r9;
		if(ind==10)
			return r10;
		if(ind==11)
			return r11;
	}
	if(type == "b")
	{
		if(ind == 0)
			return b0;
		if(ind==1)
			return b1;
		if(ind==2)
			return b2;
		if(ind==3)
			return b3;
		if(ind==4)
			return b4;
		if(ind==5)
			return b5;
		if(ind==6)
			return b6;
		if(ind==7)
			return b7;
		if(ind==8)
			return b8;
		if(ind==9)
			return b9;
		if(ind==10)
			return b10;
		if(ind==11)
			return b11;
	}
	if(type == "g")
	{
		if(ind == 0)
			return g0;
		if(ind==1)
			return g1;
		if(ind==2)
			return g2;
		if(ind==3)
			return g3;
		if(ind==4)
			return g4;
		if(ind==5)
			return g5;
		if(ind==6)
			return g6;
		if(ind==7)
			return g7;
		if(ind==8)
			return g8;
		if(ind==9)
			return g9;
		if(ind==10)
			return g10;
		if(ind==11)
			return g11;
	}

	if(type == "sa")
	{
		if(ind == 0)
			return sa0;
		if(ind==1)
			return sa1;
		if(ind==2)
			return sa2;
		if(ind==3)
			return sa3;
		if(ind==4)
			return sa4;
		if(ind==5)
			return sa5;
		if(ind==6)
			return sa6;
		if(ind==7)
			return sa7;
		if(ind==8)
			return sa8;
		if(ind==9)
			return sa9;
		if(ind==10)
			return sa10;
		if(ind==11)
			return sa11;
		if(ind==12)
			return sa12;
		if(ind==13)
			return sa13;
	}

	return arr;
}

function isEmptyField(val)
{
	if(val == "" )
		return true;
	return false;
}		
/////////////////// Business Rules for Higher Joint Approver /////////////////

function getSelectedHigherIndex(highapp)
{
	var inForm = document.pgUserAdministration;

	highapp = highapp + ".0";
	
    for(i=0; i<inForm.pgUserAdministration_cbHigherApprovers.options.length;i++)
    {
	if( inForm.pgUserAdministration_cbHigherApprovers.options[i].value == highapp)
	   return i;
    }
    return -1;
}

function getSelectedJointIndex(jointapp)
{

	var inForm = document.pgUserAdministration;

	jointapp = jointapp + ".0";

    for(i=0; i<inForm.pgUserAdministration_cbJointApprovers.options.length;i++)
    {
	if( inForm.pgUserAdministration_cbJointApprovers.options[i].value == jointapp)
	   return i;
    }
    return -1;
}


function checkHighJointUserType(cbb)
{
	var inForm = document.pgUserAdministration;
	var highapp  = "<jato:text name="stOldSelectedHigherApp" fireDisplayEvents="true" escape="true" />";
	var jointapp = "<jato:text name="stOldSelectedJointApp" fireDisplayEvents="true" escape="true" />";
	
	var cb = inForm.pgUserAdministration_cbUserTypes;

	var selectedOptionValue = getSelectedOptionValue(cb);

//alert("Selected UserType : " + selectedOptionValue);

	if(selectedOptionValue != -1 && selectedOptionValue != 0)
	{
		// user is admin type.
		if( selectedOptionValue != 1 && selectedOptionValue != 2  && 
		selectedOptionValue != 3 && selectedOptionValue != 4  && 
		selectedOptionValue != 5 && selectedOptionValue != 13
		)
		{
			var selectedHigherValue = getSelectedOptionValue(inForm.pgUserAdministration_cbHigherApprovers);
			var selectedJointValue  = getSelectedOptionValue(inForm.pgUserAdministration_cbJointApprovers);
			var str =   HIGHER_APPROVER_REQ;  
						  
//alert("selectedHigherValue  : " + selectedHigherValue + " selectedJointValue : " + selectedJointValue );

			if(selectedHigherValue == "" && selectedJointValue == "")
			{
				var high = getSelectedHigherIndex(highapp);
				var joint = getSelectedJointIndex(jointapp);
				
				if( (cbb == "cbJointApprovers") && joint != -1)
					inForm.pgUserAdministration_cbJointApprovers.options[joint].selected=true;
				else
					if( (cbb == "cbHigherApprovers") && high != -1)
						inForm.pgUserAdministration_cbHigherApprovers.options[high].selected=true;
				alert(str);
			}	
			else
			{
				if(selectedHigherValue != "" && selectedJointValue != "")
				{
					var high = getSelectedHigherIndex(highapp);
					var joint = getSelectedJointIndex(jointapp);
					
					if( (cbb == "cbJointApprovers") && joint != -1)
						inForm.pgUserAdministration_cbJointApprovers.options[joint].selected=true;
					else
						if( (cbb == "cbHigherApprovers") && high != -1)
							inForm.pgUserAdministration_cbHigherApprovers.options[high].selected=true;
					alert(str);
				}
			}
		}			 	
		else
		{
			if(selectedHigherValue != "" && selectedJointValue != "")
			{
				alert(NON_DECISIONING_USER);					
				inForm.pgUserAdministration_cbHigherApprovers.options[0].selected=true;
				inForm.pgUserAdministration_cbJointApprovers.options[0].selected=true;
			}
		}
	}
	else
	{	
		alert(USER_TYPE_REQ);					
		inForm.pgUserAdministration_cbUserTypes.options[0].selected = true;
		inForm.pgUserAdministration_cbHigherApprovers.options[0].selected=true;
		inForm.pgUserAdministration_cbJointApprovers.options[0].selected=true;
	}	
}

/////////////////////// Populate Repeated Screen Access /////////////////

function populateRepeatedScreenAccess(numofrows)
{
	var changedComboBox =(NTCP) ? event.target : event.srcElement;
	var selectedOptionValue = getSelectedOptionValue(changedComboBox);	//getValue of the selected option
	var selectedOptionIndex = getSelectedOptionIndex(changedComboBox);	//getValue of the selected option

	var inForm = document.pgUserAdministration;

	arr = new Array();
	
	var inputRowNdx;
	var theControl;
	if( selectedOptionValue == 0)
	{
		for (var i=0; i < numofrows; i++) 
		{
			//inForm.cbPageAccessTypes[i].selectedIndex = 0;
			inputRowNdx = i+"]_";
			theControl = eval("document.forms[0].elements['pgUserAdministration_Repeated1[" + inputRowNdx + "cbPageAccessTypes" + "']");
			theControl.selectedIndex = 0;
		}
		return;
	}	
		
	arr = findArrType(selectedOptionIndex-1,"sa");

	for (var i=0; i < arr.length; i++) 
	{
		//inForm.cbPageAccessTypes[i].selectedIndex = arr[i];
		inputRowNdx = i+"]_";
		theControl = eval("document.forms[0].elements['pgUserAdministration_Repeated1[" + inputRowNdx + "cbPageAccessTypes" + "']");
		theControl.selectedIndex = arr[i];
	}
		
}

/////////////////////////// End of populating comboboxes //////////////

//============= Local Field Validation functions ====================
	function isAlphabet()
	{   
	    var inpTextbox =(NTCP) ? event.target : event.srcElement;
	    var s = inpTextbox.value;
	    var inpName = inpTextbox.name;
	
		var i;
		var msg;

	    if (isEmpty(s))
	      return true;

	    for (i = 0; i < s.length; i++)
	    {
	        // Check that current character is letter.
	        var c = s.charAt(i);

	        if (isDigit(c))
	        {
				
				if(inpName == 'pgUserAdministration_tbUserTitle')
					msg = printf(FIELD_ALPHA_ONLY, "Titre");
				if(inpName == 'pgUserAdministration_tbUserFirstName')
					msg = printf(FIELD_ALPHA_ONLY, "Pr�nom");
				if(inpName == 'pgUserAdministration_tbUserLastName')
					msg = printf(FIELD_ALPHA_ONLY, "Nom");
				if(inpName == 'pgUserAdministration_tbUserInitialName')
					msg = printf(FIELD_ALPHA_ONLY, "Initiale");

		        alert(msg);
		        inpTextbox.select();
		        return false;
		    }    
	    }

	    // All characters are letters.
	    return true;
	}


	function isAlphabetic ()
	{   
	    var inpTextbox =(NTCP) ? event.target : event.srcElement;
	    var s = inpTextbox.value;
	    var inpName = inpTextbox.name;
	
		var i;
		var msg;

	    if (isEmpty(s))
	      return true;


	    for (i = 0; i < s.length; i++)
	    {
	        // Check that current character is letter.
	        var c = s.charAt(i);

	        if (!isLetter(c))
	        {
				if(inpName == 'pgUserAdministration_tbUserTitle')
					msg = printf(FIELD_ALPHA_ONLY, "Titre");
				if(inpName == 'pgUserAdministration_tbUserFirstName')
					msg = printf(FIELD_ALPHA_ONLY, "Pr�nom");
				if(inpName == 'pgUserAdministration_tbUserLastName')
					msg = printf(FIELD_ALPHA_ONLY, "Nom");
				if(inpName == 'pgUserAdministration_tbUserInitialName')
					msg = printf(FIELD_ALPHA_ONLY, "Initiale");

		        alert(msg);
		        inpTextbox.select();
		        return false;
		    }    
	    }

	    // All characters are letters.
	    return true;
	}

	function isEmpty(s)
	{   return ((s == null) || (s.length == 0))
	}

	function isLetter (c)
	{   return ( ((c >= "a") && (c <= "z")) || ((c >= "A") && (c <= "Z")) )
	}

   function isValidAreaCode()
   {
	    var inpTextbox =(NTCP) ? event.target : event.srcElement;
	    var inp = inpTextbox.value;
	    var inpName = inpTextbox.name;
        			           
	    if (isEmpty(inp))
	      return true;

		if(inp.length != 3)
    	{
			alert(AREA_CODE_3_DIGITS);
    		inpTextbox.select(); 			
			return false;    	
    	}   
		
        if(inp.length == 3)
        {	
        	var fdigit = inp.charAt(0);
        	var sdigit = inp.charAt(1);
        	var tdigit = inp.charAt(2);
        	        	
        	if(!isDigit(fdigit) || !isDigit(sdigit) || !isDigit(tdigit))
        	{
				alert(AREA_CODE_NUMERIC);
        		inpTextbox.select(); 			
				return false;    	
        	}   
             
        }
        return true;     
   }

   function isValidPhoneNumber()
   {
	    var inpTextbox =(NTCP) ? event.target : event.srcElement;
	    var inp = inpTextbox.value;
	    var inpName = inpTextbox.name;
        			           
	    if (isEmpty(inp))
	      return true;

		if(inp.length != 3)
    	{
			if(inpName == 'pgUserAdministration_tbWorkPhoneNumFirstThreeDigits')
				alert(WORK_PHONE_FORMAT);
			else
				alert(FAX_NUMBER_FORMAT);
				
    		inpTextbox.select(); 			
			return false;    	
    	}   
		
        if(inp.length == 3)
        {	
        	var fdigit = inp.charAt(0);
        	var sdigit = inp.charAt(1);
        	var tdigit = inp.charAt(2);
        	
        	
        	if(!isDigit(fdigit) || !isDigit(sdigit) || !isDigit(tdigit))
        	{
				alert(PHONE_NUMERIC);
        		inpTextbox.select(); 			
				return false;    	
        	}   
             
        }
        return true;     
   }


   function isValidPhoneNumber1()
   {
	    var inpTextbox =(NTCP) ? event.target : event.srcElement;
	    var inp = inpTextbox.value;
	    var inpName = inpTextbox.name;
        			           
	    if (isEmpty(inp))
	      return true;

		if(inp.length != 4)
    	{
			if(inpName == 'pgUserAdministration_tbWorkPhoneNumFirstThreeDigits')
				alert(WORK_PHONE_FORMAT);
			else
				alert(FAX_NUMBER_FORMAT);
				
    		inpTextbox.select(); 			
			return false;    	
    	}   
		
        if(inp.length == 4)
        {	
        	var fdigit = inp.charAt(0);
        	var sdigit = inp.charAt(1);
        	var tdigit = inp.charAt(2);
        	var fodigit = inp.charAt(3);
        	
        	
        	if(!isDigit(fdigit) || !isDigit(sdigit) || !isDigit(tdigit)|| !isDigit(fodigit))
        	{
				alert(PHONE_NUMERIC);
        		inpTextbox.select(); 			
				return false;    	
        	}   
             
        }
        return true;     
   }



   function isValidExtensionPhoneNum()
   {
	    var inpTextbox =(NTCP) ? event.target : event.srcElement;
	    var inp = inpTextbox.value;
	    var inpName = inpTextbox.name;
		
        if(inp.length != 0)
        {	
        	
        	for(i=0;i<inp.length;i++)
        	{
        		if(!isDigit(inp.charAt(0)) )
        		{
					alert(PHONE_EXT_NUMERIC);
	        		inpTextbox.select(); 			
					return false;    	
				}	
        	}   
             
        }
        return true;     
   }


//////////////////////////////////////////////////////////////////////////
//// BMO wants to use "'" in e-mail.

	function isvalidEmailChar (s)
	{   var i;

	    for (i = 0; i < s.length; i++)
	    {
	        var c = s.charAt(i);

	        if (! (isLetter(c) || isDigit(c) || (c=='@') || (c=='.') || (c=='_') || (c=='-') || (c=='+')
	              || (c=="\'")  )) 
   	        {
	       		return false;
		}
	    }

	    return true;
	}

	function isEmail ()
	{ 
	    var inpTextbox =(NTCP) ? event.target : event.srcElement;
	    var s = inpTextbox.value;
	    var inpName = inpTextbox.name;

		if(s.length != 0)
		{
		    if (isWhitespace(s))
		    {
				alert(INVALID_EMAIL);
				inpTextbox.select();    		 
				return false;
		    }

		    if (!isvalidEmailChar(s)) 
		    {
				alert(INVALID_EMAIL);
				inpTextbox.select();    		 
				return false;
		    }

		    atOffset = s.lastIndexOf('@');

		    if ( atOffset < 1 )
		    {
				alert(INVALID_EMAIL);
				inpTextbox.select();    		 
				return false;
		    }   
		    else 
		    {
				dotOffset = s.indexOf('.', atOffset);

				if ( dotOffset < atOffset + 2 || dotOffset > s.length - 2 ) 
				{
					alert(INVALID_EMAIL);
					inpTextbox.select();    		 
					return false;
				}
			}
		}
	   return true;
	}

	function isWhitespace (s)
	{   

		var i;
		var whitespace = " \t\n\r";
	    if (isEmpty(s)) return true;

	    for (i = 0; i < s.length; i++)
	    {
	        var c = s.charAt(i);

	        if (whitespace.indexOf(c) == -1) return false;
	    }

	    return true;
	}

/////////////////////////// End of VALIDATION //////////////

//========== New method to handle auto-page refresh if UserName combobox changed ==============
var isUserMeunDown = false;

function handleUserNameKeyPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
//alert("The key = " + keycode);
  if (keycode == 13)
  {
    performGo();
    return false;
  }
 
  return true;
}

function performGo()
{
	setSubmitFlag(true); 
	//document.forms[0].action="../MosSystem/" + document.forms[0].name + ".btGo_onWebEvent(btGo)";  
	document.forms[0].action += "?<%= viewBean.PAGE_NAME%>_btGo="; 
	//alert("document.forms[0].action= " + document.forms[0].action);
  // check if any outstanding request
  if(IsSubmitButton())
	  document.forms[0].submit();
}

function toggleUserMenuDownFlag()
{
	if(isUserMeunDown)
		isUserMeunDown = false;
	else
		isUserMeunDown = true;
}

//--> Method to display more rows in the listbox -- By Billy 11Aug2003
function moreRows()
{
	if (document.all)
	{
		document.all.<%= viewBean.PAGE_NAME%>_lbGroups.size *=2;
		if(document.all.<%= viewBean.PAGE_NAME%>_lbGroups.size > document.all.<%= viewBean.PAGE_NAME%>_lbGroups.options.length)
			document.all.<%= viewBean.PAGE_NAME%>_lbGroups.size = document.all.<%= viewBean.PAGE_NAME%>_lbGroups.options.length;
	}
    if (document.layers)
	{
		document.<%= viewBean.PAGE_NAME%>_lbGroups.size *=2;
		if(document.<%= viewBean.PAGE_NAME%>_lbGroups.size > document.<%= viewBean.PAGE_NAME%>_lbGroups.options.length)
			document.<%= viewBean.PAGE_NAME%>_lbGroups.size = document.<%= viewBean.PAGE_NAME%>_lbGroups.options.length;
	}
}

//--> Function to scroll the Multiple Select (ListBox) to the first Selected Option
//--> By Billy 13Aug2003
function scrollListBox()
{
	// Assume it only support IE
	var theListBox = document.all.<%= viewBean.PAGE_NAME%>_lbGroups;

	var numOptions = theListBox.options.length;
	var selectedInds = new Array();
	
	
//alert("theListBox.selectedIndex (before) = " + theListBox.selectedIndex);
//alert("numOptions = " + numOptions);
	var ind = 0;
	for(i=0; i<numOptions; i++)
	{
		if(theListBox.options[i].selected == true)
		{
			//alert("Option # " + i + " Selected !!");
			selectedInds[ind++] = i;
		}
	}

	for(i=0; i<numOptions; i++)
	{
		if(theListBox.options[i].selected == true)
		{
			theListBox.selectedIndex = i;
			//break;
		}
	}
//alert("theListBox.selectedIndex (After) = " + theListBox.selectedIndex);
	selectedInds.reverse();
	for(i=0; i<selectedInds.length; i++)
	{
		var theSelectedInd = selectedInds[i];
		//alert("(after) Option # " + theSelectedInd + " Selected !!");
		theListBox.options[theSelectedInd].selected = true;
	}

	//theListBox.options = oldOptions;
}

// ============================ Local JavaScript (end) ============================


var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<body bgcolor=ffffff onload="scrollListBox();">
<jato:form name="pgUserAdministration" method="post" onSubmit="return IsSubmitButton();">

<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
    <%@include file="/JavaScript/CommonHeader_fr.txt" %>  

<%@include file="/JavaScript/CommonToolbar_fr.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>   
<script src="../JavaScript/rc/useradmin.js" type="text/javascript"></script>
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

<!-- START BODY OF PAGE//-->

<!-- START USER ADMIN PAGE//-->

<center>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<tr>
<td colspan=7 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Information sur l�utilisateur:</b></font>&nbsp;&nbsp;&nbsp;<jato:button name="btAddNewUser" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/add_newuser_fr.gif" fireDisplayEvents="true" /></td>
</tr>

<tr>
<td colspan=7 valign=top><font size=3 color=3366cc></td>
</tr>

<jato:text name="stBeginHidingSelAndGoButton" fireDisplayEvents="true" escape="true" />
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=6><font size=2 color=3366cc><b><jato:text name="stUserLabel" fireDisplayEvents="true" escape="true" /></b></font><br> <jato:combobox name="cbUserNames" /> &nbsp;&nbsp; </td>
</tr>
<jato:text name="stEndHidingSelAndGoButton" fireDisplayEvents="true" escape="true" />
<!-- <jato:button name="btGo" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/go_fr.gif" /> -->

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Statut:</b></font><br><jato:combobox name="cbProfileStatus" fireDisplayEvents="true" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Type d�utilisateur:</b></font><br><jato:combobox name="cbUserTypes" fireDisplayEvents="true" /></td>
<td valign=top colspan=4><font size=2 color=3366cc><b>Titre:</b></font><br>
<jato:textField name="tbUserTitle" size="35" maxLength="35" fireDisplayEvents="true" /></td>
</tr>

<tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>

<tr>
<td colspan=7 valign=top><font size=3 color=3366cc></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Appellation:</b></font><br>
		<jato:combobox name="cbSalutations" fireDisplayEvents="true" /></td>
<td valign=top colspan=2>
	<table border=0 width=0% cellpadding=1 cellspacing=2 bgcolor=d1ebff>
	<td valign=top><font size=2 color=3366cc><b>Pr�nom:&nbsp;&nbsp;</b></font><br>
		<jato:textField name="tbUserFirstName" size="15" maxLength="15" fireDisplayEvents="true" /></td>
	<td valign=top><font size=2 color=3366cc><b>Initiale:&nbsp;&nbsp;</b></font><br>
		<jato:textField name="tbUserInitialName" size="1" maxLength="1" fireDisplayEvents="true" /></td>
	<td valign=top><font size=2 color=3366cc><b>Nom:&nbsp;&nbsp;</b></font><br>
    <jato:textField name="tbUserLastName" size="15" maxLength="15" fireDisplayEvents="true" /></td>
	</table>
</td>
<td valign=top><font size=2 color=3366cc><b>Code d�usager:</b></font><br>
<jato:hidden name="hdLoginId"/><jato:textField name="tbLoginId" size="10" maxLength="10" fireDisplayEvents="true" extraHtml="id='tbLoginId' onBlur='return checkLoginId();'"/></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Code d�usager d�affaire:</b></font><br>
<jato:textField name="tbBusinessId" size="20" maxLength="20" fireDisplayEvents="true" /></td>
</tr>

<tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top nowrap><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone bureau:</b></font><br>
(<jato:textField name="tbWorkPhoneNumberAreaCode" size="3" maxLength="3" fireDisplayEvents="true" /> ) 
<jato:textField name="tbWorkPhoneNumFirstThreeDigits" size="3" maxLength="3" fireDisplayEvents="true" />-<jato:textField name="tbWorkPhoneNumLastFourDigits" size="4" maxLength="4" fireDisplayEvents="true" /> X 
<jato:textField name="tbWorkPhoneNumExtension" size="6" maxLength="6" fireDisplayEvents="true" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur:</b></font><br>(<jato:textField name="tbFaxNumberAreaCode" size="3" maxLength="3" fireDisplayEvents="true" />) 
<jato:textField name="tbFaxNumFirstThreeDigits" size="3" maxLength="3" fireDisplayEvents="true" />-<jato:textField name="tbFaxNumLastFourDigits" size="4" maxLength="4" fireDisplayEvents="true" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Adresse �lectronique:</b></font><br>
<jato:textField name="tbEmailAddress" size="30" maxLength="100" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Bilingue?</b></font><br><jato:combobox name="cbLanguages" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Langue:</b></font><br><jato:combobox name="cbDefaultLanguage" fireDisplayEvents="true" /></td>
</tr>

<tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>

<tr>
<td valign=top>&nbsp;&nbsp;</td>
<td valign=top colspan=6>
	<table border=0 width=0% cellpadding=1 cellspacing=2 bgcolor=d1ebff>
		<td valign=top><font size=2 color=3366cc><b>Institution:</b></font><br><jato:combobox name="cbInstitutionNames" fireDisplayEvents="true" /></td>
		<td valign=top><font size=2 color=3366cc><b>R�gion:</b></font><br><jato:combobox name="cbRegionNames" fireDisplayEvents="true" /></td>
		<td valign=top><font size=2 color=3366cc><b>Succursale:</b></font><br><jato:combobox name="cbBranchNames" fireDisplayEvents="true" /></td>
		<!-- Commented out by Billy 11Aug2003 : changed to support multiple Group assignment
		<td valign=top><font size=2 color=3366cc><b>Group:</b></font><br><jato:combobox name="cbGroupProfileName" fireDisplayEvents="true" /></td></tr>
		-->
		<td valign=top><font size=2 color=3366cc><b>Groupe:</b></font><br>
			<jato:listbox name="lbGroups" size="1" multiple="true" />
		</td>
		<td valign=top><font size=2 color=3366cc><b> </b></font><br>
			<img src= "../images/more_fr.gif" width=40  height=15 alt="" border="0" onClick = "moreRows();" >&nbsp;&nbsp;
		</td>
		<td valign=top colspan=1 nowrap>
			<jato:text name="stBeginHidingNewGroupButton" fireDisplayEvents="true" escape="true" />
			<font size=2 color=3366cc><b>Nouveau Groupe:</b></font><br>
			<jato:textField name="tbNewGroupName" size="15" maxLength="25" fireDisplayEvents="true" />
			<jato:button name="btCreateNewGroup" extraHtml="onClick = 'setSubmitFlag(true);' alt='Cr�er un nouveau groupe' width=18  height=18" src="../images/NewGroup.gif" fireDisplayEvents="true" />
			<jato:text name="stEndHidingNewGroupButton" fireDisplayEvents="true" escape="true" />
		</td>
		<!-- ========== SCR537 begins ========== -->
		<!-- by Neil on Dec/02/2004              -->
		<td valign=top>
		  <font size=2 color=3366cc><b>Avertissement tache?</b></font><br>
		  <jato:combobox name="cbTaskAlert" fireDisplayEvents="false" />
		</td>
		<!-- ========== SCR537 ends   ========== -->
	</table>
</td>
</tr>

<tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Approbateur sup�rieur:</b></font><br><jato:combobox name="cbHigherApprovers" fireDisplayEvents="true" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Co- approbateur:</b></font><br><jato:combobox name="cbJointApprovers" fireDisplayEvents="true" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Directeur:</b></font><br><jato:combobox name="cbManagers" fireDisplayEvents="true" /></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Associ�:</b></font><br><jato:combobox name="cbPartnerProfileNames" fireDisplayEvents="true" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Rempla�ant:</b></font><br><jato:combobox name="cbStandIn" fireDisplayEvents="true" /></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>
<td valign=middle><font size=2 color=3366cc><b>Nouveau mot de passe:</b></font><br><jato:password name="tbPassword" size="20" maxLength="20" fireDisplayEvents="true" /></td>
<td valign=middle colspan=1><font size=2 color=3366cc><b>V�rifier Nouveau mot de passe:</b></font><br><jato:password name="tbVerifyPassword" size="20" maxLength="20" fireDisplayEvents="true" /></td>
<td valign=top colspan=1><jato:checkbox name="chForceToChangePW" fireDisplayEvents="true" /><font size=2 color=3366cc>L�utilisateur devra changer de mot de passe � la prochaine ouverture de session</font><br>
	<jato:checkbox name="chLockPW" fireDisplayEvents="true" /><font size=2 color=3366cc>Mot de passe verrouill�</font><br>
  <jato:checkbox name="chNewPW" fireDisplayEvents="true" /><font size=2 color=3366cc>Nouveau mot de passe (Le mot de passe sera verrouill� si l�utilisateur n�ouvre pas une session au bout d�un certain nombre de jours donn�s)</font>
</td><tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Session ouverte par utilisateur ?</b></font><br><jato:text name="stLoginFlag" escape="true" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Date de la derni�re ouverture de session:</b></font><br><jato:text name="stLastLoginTime" escape="true" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Date de la derni�re fermeture de session:</b></font><br><jato:text name="stLastLogoutTime" escape="true" fireDisplayEvents="true" /></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Limite du secteur d�activit� A:</b></font><br>$<jato:textField name="tbLineOfBusiness1" formatType="decimal" formatMask="###0.00;-#" size="13" maxLength="11" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Limite du secteur d�activit� B:</b></font><br>$<jato:textField name="tbLineOfBusiness2" formatType="decimal" formatMask="###0.00;-#" size="13" maxLength="11" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Limite du secteur d�activit� C:</b></font><br>$<jato:textField name="tbLineOfBusiness3" formatType="decimal" formatMask="###0.00;-#" size="13" maxLength="11" fireDisplayEvents="true" /></td><tr>
<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td valign=top colspan=4 align=right><jato:button name="btScreenAccess" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/screen_access_fr.gif" fireDisplayEvents="true" />&nbsp;&nbsp;&nbsp;</td><tr>

<td colspan=4><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

</table>

</center>




<!-- END OF USER ADMIN PAGE//-->

<jato:text name="stBeginHidingScreenAcc" fireDisplayEvents="true" escape="true" />

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#d1ebff>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td bgcolor=3366cc colspan=7>&nbsp;&nbsp;<font size=2 color=#ccffff><b>Acc�s �cran:</b>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>

<tr>
<td><font size=2 color=#3366cc><b>Nom de l��cran:</b></font></td>
<td><font size=2 color=#3366cc><b>Acc�s:</b></font></td>
</tr>
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgUserAdministrationRepeated1TiledView">
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
<tr>
<td><font size=2><jato:text name="stScreenName" escape="true" /></font></td>
<td><font size=2><jato:text name="stBlankSpace" escape="false" /> <jato:text name="stOverideIndicator" escape="true" /> <jato:combobox name="cbPageAccessTypes" /></font></td>
</tr>

<jato:hidden name="hdPageId" />
</jato:tiledView>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();"></td><tr>
</table>

<jato:text name="stEndHidingScreenAcc" fireDisplayEvents="true" escape="true" />

<p>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right>
	<!-- ========== SCR#239 begins ========== -->
	<!-- by Neil on Jan/04/2005 -->
	<!--
	<jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/submit.gif" />&nbsp;&nbsp;&nbsp;
	-->
	<jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/submit.gif" fireDisplayEvents="true" />&nbsp;&nbsp;&nbsp;
	<!-- ========== SCR#239 ends ========== -->
	<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0'  onClick = 'setSubmitFlag(true);'" src="../images/sign_cancel.gif" />
</td>

</table>

<p>

</center>

</div>
<!--
high:<input type="text" name="high">
joint:<input type="text" name="joint">
comp:<input type="text" name="comp">
ttt:<input type="text" name="ttt">
-->


<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>
<!--
<form>
-->

<!--
</form>
-->

<BR><jato:hidden name="hd2RowNum" />


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
