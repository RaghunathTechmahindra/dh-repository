
<HTML>
<%@page info="pgInterestRateAdmin" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgInterestRateAdminViewBean">

<HEAD>
<TITLE>Administration des taux d'int�r�t</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->

</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgInterestRateAdmin" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
    
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>
<!--
<form>
-->

<!-- START BODY OF PAGE//-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=16><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=16><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=16 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Taux:</b></font></td><tr>
<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Code:&nbsp;&nbsp;&nbsp;&nbsp;</b></font></td>
<td valign=top><font size=2 color=3366cc><b>En attente/<br>D�sactiv�:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Description:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Produit actuel/Prime indexe actuelle</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Taux affich�:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Escompte maximale<br>permise:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Meilleur<br>taux:</b></font></td>
<jato:text name="stIncludePrimeIndxSectionStart" escape="false" />
<td valign=top><font size=2 color=3366cc><b>Taux<br>d'introduction:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Indexe<br>de base:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Taux<br>de base:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Modif.<br>de base:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Taux<br>d�intr.<br>r�duit:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Taux<br>d'introduction<br>(jours):</b></font></td>
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" />
<tr>
<td colspan=16><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=16><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgInterestRateAdminRepeated1TiledView">
<td colspan=2><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"><jato:hidden name="hdRowNdx" /></td><tr>
<td><jato:button name="btUpdate" extraHtml="width=23 height=23 alt='' border='0'" fireDisplayEvents="true" src="../images/open.gif" />&nbsp;&nbsp;<jato:button name="btHistory" extraHtml="width=23 height=23 alt='' border='0'" src="../images/snapshot_2.gif" /></td>
<td valign=top><font size=2><jato:text name="stCode" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stStatus" escape="true" />
<jato:text name="stIncludePrimeIndxSectionStart" escape="false" /><br>
<jato:text name="stPrimeRateStatus" escape="true" />
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" /></font></td>
<td valign=top><font size=2><jato:text name="stDescription" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stEffectiveDate" escape="true" />
<jato:text name="stIncludePrimeIndxSectionStart" escape="false" /><br>
<jato:text name="stPrimeRateEffDate" escape="true" />
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" /></font></td>
<td valign=top><font size=2><jato:text name="stPostedRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stMaxDiscountRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBestRate" escape="true" /></font></td>
<jato:text name="stIncludePrimeIndxSectionStart" escape="false" />
<td valign=top><font size=2><jato:text name="stTeaserRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBaseIndex" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBaseRate" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stBaseAdjustment" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stTeaserDiscount" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stTeaserTerms" escape="true" /></font></td>
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" />
<tr>

<td colspan=16><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=16><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</jato:tiledView>

</table>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous_fr.gif" />&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2_fr.gif" /></td>
</table>

<jato:text name="stIncludePrimeIndxSectionStart" escape="false" />

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=24><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=24><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=10 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Indexes de prime:</b></font></td><tr>
<valign=top valign=top align=right><font size=2 color=3366cc><b></b></font></td>
<td colspan=2><font size=2 color=3366cc><b>Indexe de base:</b></font></td>
<td colspan=6><font size=2 color=3366cc><b>Entr�e en vigueur:</b></font></td>
<td colspan=2><font size=2 color=3366cc><b>Taux de base courrant:</b></font></td>
<td colspan=6><font size=2 color=3366cc><b>Indexe prime actualis�e:</b></font></td>
<td colspan=4><font size=2 color=3366cc><b>Heure (24h):</b></td>
<td colspan=2><font size=2 color=3366cc><b>Taux de base:</b></font></td>
<tr>

<td colspan=24><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=24><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<jato:tiledView name="RepeatedPrimeIndex" type="mosApp.MosSystem.pgInterestRateAdminRepeatedPrimeIndexTiledView">
<td valign=top><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"><jato:hidden name="hdPrimeIndexProfileId" /></td><tr>

<td colspan=2><font size=2><jato:text name="stPrimeIndexName" escape="true" /></font></td>
<td colspan=6><font size=2><jato:text name="stLastUpdateDate" fireDisplayEvents="true" escape="true" formatType="date" formatMask="fr|MMM dd yyyy hh:mm" /></font></td>
<td colspan=2><font size=2><jato:text name="stPrimeIndexRate" escape="true" formatType="decimal" formatMask="fr|#,##0.00;(-#,##0.00)" /></font></td>

<td colspan=2 NOWRAP><font size=2>Mth: </font><jato:combobox name="cbIndexRateEffMonth" /> <font size=2>Jour: </font>
<td colspan=2 td align=left><font size=2><jato:textField name="txIndexRateEffDay" formatType="decimal" formatMask="###0;(-###0)" size="2" maxLength="2" /> <font size=2>Yr: </font>
<td colspan=2 td align=left><font size=2><jato:textField name="txIndexRateEffYear" formatType="decimal" formatMask="###0;(-###0)" size="4" maxLength="4" /></td>

<td colspan=2><font size=2><jato:textField name="txIndexRateEffHour" size="2" maxLength="2" /> : 
<td colspan=2 td align=left><jato:textField name="txIndexRateEffMinute" size="2" maxLength="2" /></font></td>
<td colspan=2><font size=2><jato:textField name="txPrimeIndexEffRate" size="7" maxLength="7" />%</font></td>

<td colspan=2><jato:button name="btUpdatePrimeRates" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/UpdateIndex_fr.gif" /></td>
<tr>

<td colspan=24><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=24><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</jato:tiledView>
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" />

</table>

<!-- Sample : Submit and Cancel buttons //-->
<table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" />
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}


//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
