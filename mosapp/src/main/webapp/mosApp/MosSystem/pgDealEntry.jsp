<%--
 /**Title: pgDealEntry.jsp
 *Description:Deal Entry Screen
 *@version 1.0 Initial Version
 *@version 1.1 06-JUN-2008 XS_2.13 Included pgQualifyingDetailsPagelet 
 *@version 1.2 31-July-2008 MCM Team: added changeMIUpfront in setMIUpFront
 *@version 1.3 Aug 21, 2008 : artf763316. replacing XS_2.60.
 *@version 1.4 Sep 3, 2008 : artf751603. .
 */
--%>
<html>
<%@page info="pgDealEntry" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealEntryViewBean">
  

  <head>
  <title>Deal Entry/Modification</title>

  <style>
<!--
.gotobar {
  position: absolute;
  left: 10;
  top:  100;
  width: 300px;
  background-color: 99ccff;
  overflow: hidden;
  visibility: hidden;
  height: 42px;
}

.dialogbar {
  position: absolute;
  left: 57;
  top: 100;
  overflow: hidden;
  visibility: hidden;
}

.toolpop {
  position: absolute;
  left: 320;
  top: 100;
  width: 300px;
  overflow: hidden;
  visibility: hidden;
}

.pagebody {
  position: absolute;
  visibility: hidden;
  top: expression((QLE)?135:110);
  left: 10;
  width: expression(document.body.clientWidth<984 ? '984px' : '100%');
  right-margin: 5%;
}

.alertbody {
  position: absolute;
  visibility: hidden;
  left: 10;
  top: expression((QLE)?135:110);
  width: 100%;
  overflow: hidden;
}

.validlayer {
  border: 4pt outset 3300cc;
  position: absolute;
  visibility: hidden;
  left: 30;
  top: 110;
  width: 700;
  background-color: 3300cc;
  overflow: hidden;
  align: center
}

.softvalidlayer {
  border: 4pt outset 3300cc;
  position: absolute;
  visibility: hidden;
  left: 30;
  top: 110;
  width: 700;
  background-color: 3300cc;
  overflow: hidden;
  align: center
}

.refisection {
  position: relative;
  left: 0;
  top: 0;
  display: none;
}
-->
</style>
  <script language="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</script>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>


<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/UWorkSheetJS.js" type="text/javascript"></script>
<script src="../JavaScript/componentInfoCheckBoxis.js" type="text/javascript"></script>
<script src="../JavaScript/rc/MITypeFilter.js" type="text/javascript"></script>

  <script language="JavaScript">
  
<!--
var fetchedRate = 0;
// Local JavaScript functions
// MI fixes - FXP32874 - Begin
function checkMIInsurer()
{
	var theMIInsurerId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbMIInsurer.value");	
	var theMIIndicatorId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbMIIndicator.value");
	
	if((theMIIndicatorId ==  1 || theMIIndicatorId == 2 ||
		theMIIndicatorId == 4 || theMIIndicatorId ==5) && theMIInsurerId < 1)
		{
			alert(MI_PROVIDER_EMPTY);
			return false;
		}
		setSubmitFlag(true);
		return true;
}
// MI fixes - FXP32874 - End
function isThereMoreThanOneYes()
{
  var theField =(NTCP) ? event.target : event.srcElement;
  var theFieldValue = theField.value;
  var theFieldName = theField.name;

  //alert("isThereMoreThan::FieldFullName: " + theFieldName );

  if(theFieldValue == "Y")
  {
    var rowId = getRepeatRowId(theField);
    var totRows =  getNumOfRow(theFieldName );

    var name1stPart = get1stPartName(theFieldName);
      var name2ndPart = get2ndPartName(theFieldName);

    //alert("Total Num of row = " + totRows);

    if(totRows == 1)
      return;

    for(var i=0; i < totRows; i++)
    {
      var ctlname = eval("document.forms[0].elements['" + name1stPart + i + name2ndPart + "']");
      var ctlValue = ctlname.value;
      if(ctlValue == "Y" && i != rowId)
        ctlname.value = 'N';
    }
  }

  return;
}

// Method to set MIPolicyNo field based on the selected MI-Insurer
// -- By Billy 16July2003
function SetMIPolicyNoDisplay()
{
  var theMIInsurer = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbMIInsurer.value");
  var theMIPolicyNoCtl = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_txMICertificate");
  
  if(theMIInsurer == 1) //--> CMHC
  {
    theMIPolicyNoCtl.value = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_hdMIPolicyNoCMHC.value");
  }
  else if(theMIInsurer == 2) //--> GE
  {
    theMIPolicyNoCtl.value = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_hdMIPolicyNoGE.value");
  }
  else  //--> Clear it if not selected
  {
    theMIPolicyNoCtl.value = "";
  }
} 

//--DJ_CR_203.1--start--//
//// Provides field control under all business cases.
function SetLineOfCreditDisplay()
{
    var theProprietairePlusLOCCtl = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_txProprietairePlusLOC");
    var theProprietairePlusLOCVal = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_txProprietairePlusLOC.value");
    var theRbProprietaireCtl = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_rbProprietairePlus");
    
    theRbProprietaireCtl.onclick;

////alert("theProprietairePlusLOCCtl: " + theProprietairePlusLOCCtl);
////alert("theProprietairePlusLOCVal: " + theProprietairePlusLOCVal);
////alert("theRbProprietaireCtlLength: " + theRbProprietaireCtl.length);

   for (j=0; j < theRbProprietaireCtl.length; ++j)
   {
      var theHomeOwnerProtRadioCtl = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_rbProprietairePlus" + "[j]");
      
///alert("theHomeOwnerProtRadioCtl[" + j + "]: " + theHomeOwnerProtRadioCtl);               
///alert("theHomeOwnerProtRadioCtl[" + j + "]: " + theHomeOwnerProtRadioCtl.value);
///alert("Checked[" + j + "]?: " + eval("document.forms[0].<%=viewBean.PAGE_NAME%>_rbProprietairePlus" + "[j]").checked);

            //// LineOfCredit is disabled, default
      if(theHomeOwnerProtRadioCtl.value == "N" && 
        eval("document.forms[0].<%=viewBean.PAGE_NAME%>_rbProprietairePlus" + "[j]").checked == true) 
      {       
        document.forms[0].<%=viewBean.PAGE_NAME%>_txProprietairePlusLOC.value = 0.00;    
        theProprietairePlusLOCCtl.disabled = true;
      }
            //// LineOfCredit is allowed
      else if(theHomeOwnerProtRadioCtl.value == "Y" && 
            eval("document.forms[0].<%=viewBean.PAGE_NAME%>_rbProprietairePlus" + "[j]").checked == true) 
      { 
        theProprietairePlusLOCCtl.disabled = false;
      }
      }     
}

<%--Release3.1 May 03 begins  --%>

function hideRequestStandardService()
{
 var MIInsurerId = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbMIInsurer.value");
 var theMITypeId = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbMIType.value");
 
 //[Request Standard Service]
 var eDivReqStdSvc = document.getElementById("divReqStdSvc");
 
 //hide [Request Standard Service] if NOT GE, 
 //or if GE and (basic or full service)
 //LEN215466: reset rbRequestStandardService by unchecked all
 if(MIInsurerId != 2 || ((MIInsurerId == 2) && (theMITypeId == 1 || theMITypeId == 2))) {
  eDivReqStdSvc.style.display="none";
  var rbRequestStandardServices = document.getElementsByName("<%=viewBean.PAGE_NAME%>_rbRequestStandardService");
  for(i=0;i<rbRequestStandardServices.length;i++)
   if (rbRequestStandardServices[i].checked)
    rbRequestStandardServices[i].checked = false;  
 } else
  eDivReqStdSvc.style.display="";
}

function getSelectedRadioValue(radio){
    var retval = "";
    for(var i=0;i<radio.length;i++){
        if(radio[i].checked==true) 
            retval=radio[i].value;
    }
    return retval;
}

function hideSubProgressAdvance()
{
 //[Progress Advance Type] & [Progress Advance Inspection By]
 var varProgressAdvanceCtl = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_rbProgressAdvance");
 var varProgressAdvance = getSelectedRadioValue(varProgressAdvanceCtl);

 var eDivProgressAdvanceType = document.getElementById("divProgressAdvanceType");
 var eDivProgressAdvanceInspectionBy = document.getElementById("divProgressAdvanceInspectionBy");
 
 //hide if deal.progressAdvance <> Y
 if(varProgressAdvance != "Y" && varProgressAdvance != "y")
 {
  eDivProgressAdvanceType.style.display="none";
  eDivProgressAdvanceInspectionBy.style.display="none";
 }
 else
 {
  eDivProgressAdvanceType.style.display="";
  eDivProgressAdvanceInspectionBy.style.display="";
 }
 
 //SRC #1679 - show Progress Advance Type if varForProgressAdvance = "Y" or "y"
 var varForceProgressAdvanceCtl = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_hdForceProgressAdvance");
 var varForceProgressAdvance = varForceProgressAdvanceCtl.value;
 if (varForceProgressAdvance == 'Y' || varForceProgressAdvance == 'y') {
    eDivProgressAdvanceType.style.display="";
 }
 
}
<%--Release3.1 May 03 ends  --%>

//--DJ_CR_203.1--start--//


function setChangedEstimatedClosingDate ()
{
    document.forms[0].<%=viewBean.PAGE_NAME%>_hdChangedEstimatedClosingDate.value = "Y";
}

//  ========================================================================================================
// Function to initialize the page -- may be difference for every pages
//    It should be used in the onLoad event of <body> tag
// ========================================================================================================
function initPage()
{
  // Set MIPolicyNo
  SetMIPolicyNoDisplay();
  //--Release3.1 MI
  // hide or display Request Standard Service
  hideRequestStandardService();
  hideSubProgressAdvance();
  //CR03
  changeExpireDate();
  //CR03
  
  //5.0 MI -- start
  var miTypeFilter = MITypeFilterFactory(
          document.getElementById("cbMIInsurer"), 
          document.getElementById("cbMIType"));
  miTypeFilter();
  
  // Ticket 267
  initMITypeOnDenied();
  //5.0 MI -- end
  
  initQPR();
}

//lock miindicator, mitype and miinsurer when dealstatus is collapsed or denied - Ticket 267
function initMITypeOnDenied(){
	var DEAL_STATUS_COLLAPSED = "23";
	var DEAL_STATUS_DENIED = "24";

	var miType = document.getElementById("cbMIType");
	var dealStatusId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdDealStatusId.value");
	if ((DEAL_STATUS_COLLAPSED == dealStatusId || DEAL_STATUS_DENIED == dealStatusId)) 
	{
		miType.disabled = true;
	}
}

//CR03 start
function changeExpireDate()
{   
    var varAutoCalculateCtl = eval("document.forms[0].<%=viewBean.PAGE_NAME%>_rbAutoCalCommitDate");

    var varAutoCalculate = getSelectedRadioValue(varAutoCalculateCtl);
    var eExpirationDateMonth = document.getElementById("cbCommExpirationDateMonth");
    var eExpirationDateDay = document.getElementById("txCommExpirationDateDay");
    var eExpirationDateYear = document.getElementById("txCommExpirationDateYear");
    
    if(varAutoCalculate == "Y") {
        eExpirationDateMonth.disabled = true;
        eExpirationDateDay.disabled = true;
        eExpirationDateYear.disabled = true;
    }
    else {
        eExpirationDateMonth.disabled = false;
        eExpirationDateDay.disabled = false;
        eExpirationDateYear.disabled = false;
    }
}

//CR03 end

//begin Qualify Rate 
function initQPR() //called on page load/reload
{
	var qualifyProduct = document.getElementById("cbQualifyProductType");
	if (document.getElementById("chQualifyRateOverride").checked)
	{
		qualifyProduct.disabled = false;
		showProdPencil();
	}
	else
	{
		if(document.getElementById("chQualifyRateOverrideRate").checked)
		{
			var productId = document.getElementById("hdQualifyProductId").value;
			qualifyProduct.disabled = true;
			var qualifyProduct = document.getElementById("cbQualifyProductType");
			//if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
				hideProdPencil();
			//else
			//	showProdPencil();
		}
		else
		{
			selectRow(qualifyProduct, document.getElementById("hdQualifyProductId").value);
			qualifyProduct.disabled = true;
			hideProdPencil();
		}
	}	
	
	if (document.getElementById("chQualifyRateOverrideRate").checked)
	{
		var qualifyRate = document.getElementById("txQualifyRate");
		var extantRate = qualifyRate.value;
		getQualifyProductRate();
		if (parseFloat(extantRate) == parseFloat(qualifyRate.value))
		{
			hideRatePencil();
			document.getElementById("chQualifyRateOverrideRate").checked = false;
			document.getElementById("txQualifyRate").disabled = true;
		}
		else
		{
			qualifyRate.value = extantRate;
			showRatePencil("N");
			document.getElementById("txQualifyRate").disabled = false;
		}
	}
	else
	{
		document.getElementById("txQualifyRate").disabled = true;
		getQualifyProductRate();
		hideRatePencil();
	}

	qualifyRateToggle();
	
}

function qualifyChkBoxClicked() 
{	
    var chk = document.getElementById("chQualifyRateOverride").checked;
    var rate = document.getElementById("hdQualifyRate").value;
    var productId = document.getElementById("hdQualifyProductId").value;

	var disableRate = document.getElementById("QualifyRateDisabled").value;
	
    var qualifyRate = document.getElementById("txQualifyRate");
    var qualifyProduct = document.getElementById("cbQualifyProductType");
    
	qualifyProduct.disabled = !chk;

	//document.getElementById("chQualifyRateOverrideRate").disabled = !chk;
//	document.getElementById("chQualifyRateOverrideRate").checked = false;
//	document.getElementById("txQualifyRate").disabled = !(document.getElementById("chQualifyRateOverrideRate").checked); 

	if(!chk) 
	{
		selectRow(qualifyProduct, productId);
		getQualifyProductRate();
		hideProdPencil();
	}
	else
		getQualifyProductRate();
	
	if (parseFloat(rate) == parseFloat(qualifyRate.value))
		hideRatePencil();
	else
		showRatePencil(disableRate);

	qualifyRateToggle();
}


function qualifyProductChanged()
{
	getQualifyProductRate(); 
	qualifyRateToggle(); 
	var qualifyProduct = document.getElementById("cbQualifyProductType");
	var productId = document.getElementById("hdQualifyProductId").value;
	if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
		hideProdPencil();
	else
		showProdPencil();
}

function qualifyRateChkBoxClicked()
{
	var chk = document.getElementById("chQualifyRateOverrideRate").checked;
	getQualifyProductRate();
	document.getElementById("txQualifyRate").disabled = !chk; 
	hideRatePencil();
}
function qualifyRateToggle()
{
    var qualifyRate = document.getElementById("txQualifyRate");
	
	if (document.getElementById("QualifyRateDisabled").value == 'Y')
	{
		document.getElementById("staticQualifyRateSpan").innerHTML = "<font size=3>" + qualifyRate.value + " %</font>";
		document.getElementById("editableQualifyRateSpan").style.display = 'none';
	}
	else
	{
		document.getElementById("staticQualifyRateSpan").innerHTML = ""; //should make it take up no space
		document.getElementById("editableQualifyRateSpan").style.display = 'block';
	}
}

function hideRatePencil()
{
	getHtmlElement("ratePencilSpan").style.visibility = HIDDEN;
}

function showRatePencil(disableRate)
{
	var chk = document.getElementById("chQualifyRateOverrideRate").checked;
	if (disableRate == "N" && chk && amGenerate != "Y") //no asterisks required when you can't change the rate
	{
		var qualifyRate = document.getElementById("txQualifyRate");
		if (parseFloat(qualifyRate.value) != parseFloat(fetchedRate))
			getHtmlElement("ratePencilSpan").style.visibility = VISIBLE;
		else
			getHtmlElement("ratePencilSpan").style.visibility = HIDDEN;
	}
	else
		getHtmlElement("ratePencilSpan").style.visibility = HIDDEN;
}

function hideProdPencil()
{
	getHtmlElement("prodPencilSpan").style.visibility = HIDDEN;
}

function showProdPencil()
{
	//don't show if we are on an Active Message
	if (amGenerate != "Y")
		getHtmlElement("prodPencilSpan").style.visibility = VISIBLE;
}

//end Qualify Rate
function selectRow(selectObj, selectValue){
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value == selectValue) 
            selectObj.options[i].selected = true;
    }
}
//======================= Local JS function (End) ===================================
//FXP33510  v5.0_XpS - Dual Qualifying Rate - Qualifying Product was changed with different LTV 

function qualifyProdRate()
{
	var rateChk = document.getElementById("chQualifyRateOverrideRate").checked;
	var prodChk = document.getElementById("chQualifyRateOverride").checked;
	var productId = document.getElementById("hdQualifyProductId").value;
	var rate = document.getElementById("hdQualifyRate").value;
		
	//alert(rateChk + " = rateChk");
	//alert(prodChk + " = prodChk");
	
	if(rateChk && prodChk)
	{
		//alert("ALL GOOD");
		var qualifyProduct = document.getElementById("cbQualifyProductType");
		qualifyProduct.disabled = false;
		//alert(productId);
		//alert(qualifyProduct.options[qualifyProduct.options.selectedIndex].value);
		if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
			hideProdPencil();
		else
			showProdPencil();
			
	
		document.getElementById("txQualifyRate").disabled = false; 
		
		var qualifyRate = document.getElementById("txQualifyRate");
		var extantRate = qualifyRate.value;
		getQualifyProductRate();
		if (parseFloat(extantRate) == parseFloat(qualifyRate.value))
		{
			hideRatePencil();
		}
		else
		{
			qualifyRate.value = extantRate;
			showRatePencil("N");
			document.getElementById("txQualifyRate").disabled = false;
		}
		
	}
	else if(prodChk && !rateChk)
	{
		//alert("1111 prodChk && !rateChk " );
		var qualifyProduct = document.getElementById("cbQualifyProductType");
		//alert(qualifyProduct.options[qualifyProduct.options.selectedIndex].value );
		qualifyProduct.disabled = false;
		
		if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
			hideProdPencil();
		else
			showProdPencil();
		
		qualifyRateChkBoxClicked();
	}
	else if(!prodChk && rateChk)
	{
		//alert("2222 !prodChk && rateChk = " );
		/////var chk = document.getElementById("chQualifyRateOverride").checked;
	   
		var disableRate = document.getElementById("QualifyRateDisabled").value;
		
	    var qualifyRate = document.getElementById("txQualifyRate");
	    var qualifyProduct = document.getElementById("cbQualifyProductType");
	    
		qualifyProduct.disabled = true;
		qualifyRate.disabled = false;
	
		if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value != productId)
		{
			selectRow(qualifyProduct, productId);
			getQualifyProductRate();
		}
		hideProdPencil();
	
		////if(!chk) 
		////{
			////selectRow(qualifyProduct, productId);
			if(qualifyProduct.options[qualifyProduct.options.selectedIndex].value == productId)
				hideProdPencil();
			else
				showProdPencil();
		////}
	
		////if (!(document.getElementById("chQualifyRateOverrideRate").checked))
		////	getQualifyProductRate();
		
		////if (parseFloat(rate) == parseFloat(qualifyRate.value))
		////	hideRatePencil();
		////else
		////	showRatePencil(disableRate);
	
		////qualifyRateToggle();
	}
	else if(!prodChk && !rateChk)
	{
		//alert("3333 !prodChk && !rateChk = ");
		qualifyChkBoxClicked();
		qualifyRateChkBoxClicked();
	}
	
}

//FXP33510  v5.0_XpS - Dual Qualifying Rate - Qualifying Product was changed with different LTV - end

//-->
</script>

  </head>

  <body bgcolor="ffffff" link="000000" vlink="000000" alink="000000"
    onload="initPage(); checkMIResponseLoop();" >

  <jato:form name="pgDealEntry" method="post"
    onSubmit="return IsSubmitButton();">


    <script language="JavaScript">

// New method to check wether to Pop-up or hide the Refinance Section
//  -- by BILLY 07Feb2002
<jato:text name="stVALSData" escape="false" />

var temp = <jato:text name="stVALSData" escape="false" />;

////alert("stVALSData: " + temp);

function DisplayOrHideRefiSection()
{
  var theDealPurposeId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbDealPurpose.value;

  ////alert("DealPurposesId: " + theDealPurposeId);

  var theRefiSection = (NTCP)?document.refisection:document.all.refisection.style;

  var theIdx = -1;

  // Get the array index from the DealPurpose array

  for(i=0; i< VALScbDealPurpose.length; i++)
  {
    ////alert("VALScbDealPurpose[" + i + "]: " + VALScbDealPurpose[i]);

    if(theDealPurposeId == VALScbDealPurpose[i])
    {
      theIdx = i;
      break;
    }

  }

  // Check if the corresponding RefinanceFlag is 'Y' ==> Display or 'N' ==> Hide

  ////alert("The index after the break" + theIdx);
  ////alert("VALSRefinanceFlags: " + VALSRefinanceFlags[theIdx]);

  //--> Modified to Show the section by default : By Billy 12June2003
  if((theIdx >= 0) && (VALSRefinanceFlags[theIdx] == 'N'))
  {
    theRefiSection.display='none';
  }
  else
  {
    theRefiSection.display='inline';
  }
}
//===========================================================================


var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
////alert("pmGenerate: " + pmGenerate);
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
////alert("pmHasTitle: " + pmHasTitle);
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
////alert("pmHasInfo: " + pmHasInfo);
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
////alert("pmHasTable: " + pmHasTable);
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
////alert("pmHasOk: " + pmHasOk);
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
////alert("pmTitle: " + pmTitle);
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
////alert("pmInfoMsg: " + pmInfoMsg);
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
////alert("pmOnOk: " + pmOnOk);
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
////alert("pmMsgTypesASize: " + pmMsgTypes.length);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);
////alert("pmMsgsASize: " + pmMsgs.length);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
////alert("amGenerate: " + amGenerate);
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
////alert("amHasTitle: " + amHasTitle);
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
////alert("amHasInfo: " + amHasInfo);
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
////alert("amHasTable: " + amHasTable);
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
////alert("amTitle: " + amTitle);
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
////alert("amInfoMsg: " + amInfoMsg);
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
////alert("amDialogMsg: " + amDialogMsg);
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
////alert("amButtonsHtml: " + amButtonsHtml);
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
////alert("amMsgTypes: " + amMsgTypes);
////alert("amMsgTypesLength: " + amMsgTypes.length);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);
////alert("amMsgs: " + amMsgs);
////alert("amMsgsLength: " + amMsgs.length);

generateAM();

</script>

    <p><jato:hidden name="sessionUserId" /> 
       <jato:hidden name="hdMIPolicyNoCMHC" /> 
       <jato:hidden name="hdMIPolicyNoGE" /> 
       <jato:hidden name="hdForceProgressAdvance" />
       <jato:hidden name="hdDealStatusId" />
    <input type="hidden" name="isAlert" value="<jato:text name='stErrorFlag' escape='true' />"> 
    <input type="hidden" name="isFatal" value=""> 

    <!--HEADER//-->
    <%@include file="/JavaScript/CommonHeader_en.txt" %>
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
     <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
    <!--TASK NAVIGATER //-->
    <%@include file="/JavaScript/TaskNavigater.txt" %>
    <!--END HEADER//-->

    <!--DEAL SUMMARY SNAPSHOT//-->
    <div id="pagebody" class="pagebody" name="pagebody" >
    <center><jato:text name="stIncludeDSSstart" escape="false" />

    <table border="0" width="100%" cellpadding="0" cellspacing="0"
      bgcolor="B8D0DC">
      <td align="center" colspan="8"><img src="../images/blue_line.gif" width="100%"
        height="1" alt="" border="0"></td>
      <tbody>
        <tr>
          <td rowspan="2">&nbsp;</td>
          <td><font size="1">DEAL #:</font><br>
          <font size="2"><b><jato:text name="stDealId" escape="true"
            formatType="decimal" formatMask="###0; (-#)" /></b></font></td>
          <td><font size="1">BORROWER NAME:</font><br>
          <font size="2"><b><jato:text name="stBorrFirstName" escape="true" />
          </b></font></td>
          <td><font size="1">DEAL STATUS:</font><br>
          <font size="2"><b><jato:text name="stDealStatus" escape="true" /></b></font></td>
          <td><font size="1">DEAL STATUS DATE:</font><br>
          <font size="2"><b><jato:text name="stDealStatusDate" escape="true"
            formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
          <td><font size="1">SOURCE FIRM:</font><br>
          <font size="2"><b><jato:text name="stSourceFirm" escape="true" /></b></font></td>
          <td><font size="1">SOURCE:</font><br>
          <font size="2"><b><jato:text name="stSource" escape="true" /></b></font></td>
          <td><font size="1">LOB:</font><br>
          <font size="2"><b><jato:text name="stLOB" escape="true" /></b></font></td>
        </tr>
        <tr>
          <!-- ========== SCR#859 begins ========== -->
          <!-- by Neil on Feb 15, 2005 -->
          <!--<td align="center" colspan="8">&nbsp;</td>-->
          <td align="left" colspan="8"><font size="2"><jato:text name="stCCAPS" escape="true" /><b><jato:text name="stServicingMortgageNumber" escape="true" />&nbsp;</b></font></td>
          <!-- ========== SCR#859 ends ========== -->
        </tr>
        <tr>
          <td rowspan="2">&nbsp;</td>
          <td><font size="1">DEAL TYPE:</font><br>
          <font size="2"><b><jato:text name="stDealType" escape="true" /></b></font></td>
          <td><font size="1">DEAL PURPOSE:</font><br>
          <font size="2"><b><jato:text name="stDealPurpose" escape="true" /></b></font></td>
          <td><font size="1">TOT LN AMT:</font><br>
          <font size="2"><b><jato:text name="stTotalLoanAmount" escape="true"
            formatType="currency" formatMask="#,##0.00; (-#)" /></b></font></td>
          <td><font size="1">PURCHASE PRICE:</font><br>
          <font size="2"><b><jato:text name="stPurchasePrice" escape="true"
            formatType="currency" formatMask="#,##0.00; -#" /></b></font></td>
          <td><font size="1">PAYMENT TERM:</font><br>
          <font size="2"><b><jato:text name="stPmtTerm" escape="true" /></b></font></td>
          <td><font size="1">EST CLOSING DATE:</font><br>
          <font size="2"><b><jato:text name="stEstClosingDate" escape="true"
            formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
          <td><font size="1">SPECIAL FEATURE:</font><br>
          <font size="2"><b><jato:text name="stSpecialFeature" escape="true" /></b></font></td>

        </tr>
        <tr>
          <td align="center" colspan="8"><img src="../images/blue_line.gif"
            width="100%" height="1" alt="" border="0"></td>
        </tr>
      </tbody>
    </table>
    <p></p>
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td align="center" valign="middle" colspan="4"><font size="3"><jato:text
            name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
        </tr>
      </tbody>
    </table>

    <jato:text name="stIncludeDSSend" escape="false" /> <!--END DEAL SUMMARY SNAPSHOT//-->

    <br>

    <!-- START BODY OF PAGE//-->

  <!-- START GCD Summary Section -->
<jato:text name="stIncludeGCDSumStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td></tr>
<tr>
    <td valign=top colspan=1>&nbsp;</td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Status:</b></font><br><font size=2><jato:text name="txCreditDecisionStatus"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Decision:</b></font><br><font size=2><jato:text name="txCreditDecision"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Credit Bureau Score:</b></font><br><font size=2><jato:text name="txGlobalCreditBureauScore"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Risk Rating:</b></font><br><font size=2><jato:text name="txGlobalRiskRating"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>Internal Score:</b></font><br><font size=2><jato:text name="txGlobalInternalScore"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc><b>MI Status :</b></font><br><font size=2><jato:text name="stMIStatus"/></font></td>
    <td valign=top colspan=1><font size=2 color=3366cc></font><br><jato:button name="btCreditDecisionPG" extraHtml="onClick = 'setSubmitFlag(true);'"
              fireDisplayEvents="true" src="../images/credit_decision.gif"/></td>
</tr>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>
  </table>
<jato:text name="stIncludeGCDSumEnd" escape="false" /><br>
<!-- End GCD Summary Section -->  
    

    <table border="0" width="100%" cellpadding="0" cellspacing="0"
      bgcolor="d1ebff">
      <tbody>
        <tr>
          <td colspan="8"><img src="../images/dark_bl.gif" width="100%"
            height="3" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="8"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>
      </tbody>
    </table>
    <table border="0" width="100%" cellpadding="1" cellspacing="0"
      bgcolor="d1ebff">

      <tbody>
        <tr>
          <td valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;Deal
          Number:</b></font><jato:text name="stDealNo" escape="true"
            formatType="decimal" formatMask="###0; (-#)" /></td>
        </tr>
        <tr>
          <td valign="top"><font size="3" color="3366cc"><jato:button
            name="btRessurect"
            extraHtml="width=60 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'"
            fireDisplayEvents="true" src="../images/Resurrect.gif" /> <jato:button
            name="btPreapprovalFirm"
            extraHtml="width=104 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'"
            fireDisplayEvents="true" src="../images/preapproval_firm.gif" /></font></td>
        </tr>
        <tr>
          <td><img src="../images/light_blue.gif" width="1" height="2" alt=""
            border="0"></td>
        </tr>
        <tr>
          <td><img src="../images/blue_line.gif" width="100%" height="1" alt=""
            border="0"></td>
        </tr>
      </tbody>
    </table>

    <!--START GDS/TDS DETAILS//-->
    <table border="0" width="100%" cellpadding="1" cellspacing="0"
      bgcolor="#d1ebff">
      <td colspan="9"><img src="../images/light_blue.gif" width="1"
        height="1" alt="" border="0"></td>
      <tbody>
        <tr>
          <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td valign="top" bgcolor="3366cc"><font size="2" color="ccffff">&nbsp;<b>Combined
          GDS:</b></font><br>
          &nbsp;<font size="2" color="ccffff"><jato:text name="stCombinedGDS"
            escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
          <td>&nbsp;&nbsp;</td>
          <td valign="top"><font size="2" color="3366cc"><b>GDS (Qualifying Rate):</b></font><br>
          <font size="2"><jato:text name="stCombined3YrGDS" escape="true"
            formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>Combined GDS
          (Borrower Only):</b></font><br>
          <font size="2"><jato:text name="stCombinedBorrowerGDS"
            escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
          <td valign="top" bgcolor="3366cc"><font size="2" color="ccffff">&nbsp;<b>Combined
          TDS:</b></font><br>
          &nbsp;<font size="2" color="ccffff"><jato:text name="stCombinedTDS"
            escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
          <td>&nbsp;&nbsp;</td>
          <td valign="top"><font size="2" color="3366cc"><b>TDS (Qualifying Rate):</b></font><br>
          <font size="2"><jato:text name="stCombined3YrTDS" escape="true"
            formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>Combined TDS
          (Borrower Only):</b></font><br>
          <font size="2"><jato:text name="stCombinedBorrowerTDS"
            escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
        </tr>
        <!--Express 5.0 Qualifying Rate Detail-->
<tr style='display: <jato:text name="stQualifyRateEdit"/>'>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="hidden" id="hdQualifyRate" value='<jato:text name="sthdQualifyRate"/>'/>
    <input type="hidden" id="hdQualifyProductId" value='<jato:text name="sthdQualifyProductId"/>'/>
	<input type="hidden" id="QualifyRateDisabled" value='<jato:text name="hdQualifyRateDisabled"/>'/>
</td>
<td colspan=2>
	<font size=2 color=3366cc><b>Net Rate:</b></font><br>
	<font size=2>
	<table width="100%">
		<td align="left">
			<jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" />
			%
		</td></font>
		<td align="right">
			<span id='ratePencilSpan' visibility='hidden'>
				<img src="../images/pencil.png" align="bottom">
			</span>
		</td>
	</table>
</td>
<td colspan=1>
	<table width="100%">
		<td align="left">
			<font size=2 color=3366cc><b>Qualifying Rate:</b></font><br>
			<span id='staticQualifyRateSpan'></span>
			<span id="editableQualifyRateSpan">
				<jato:textField name="txQualifyRate" formatType="decimal" formatMask="###0.000;-#" size="5" maxLength="5"/>
				%
				<jato:checkbox name="chQualifyRateOverrideRate" />
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Override</b></font>
			</span>
		</td>
		<td align="right">
			<span id='prodPencilSpan' visibility='hidden'>
				&nbsp;<br><img src="../images/pencil.png" align="bottom">
			</span>
		</td>
	</table>
</td>
<td colspan=1><font size=2 color=3366cc><b>Qualifying Product:</b></font><br><font size=2><jato:combobox name="cbQualifyProductType"/></font></td>
<td colspan=3 nowrap><font size=2 color=3366cc>&nbsp;<br><jato:checkbox name="chQualifyRateOverride" /><b>&nbsp;&nbsp;Override</b></font></td>
<td ><jato:button name="btRecalculateGDS" extraHtml="width=75 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/recalculate.gif" />&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <!-- Qualify Rate Change -->
<tr style='display: <jato:text name="stQualifyRateHidden"/>'>
  <td valign=top>&nbsp;&nbsp;&nbsp;</td>
  <td colspan=2><font size=2 color=3366cc><b>Net Rate:</b></font><br>
  <font size=2><jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</font></td>
  <td><font size=2 color=3366cc><b>Qualifying Rate:</b></font><br>
  <font size=2><jato:text name="stQualifyRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</font></td>
  <td colspan=2><font size=2 color=3366cc><b>Qualifying Product:</b></font><br>
  <font size=2><jato:text name="stQualifyProductType" escape="true" /></font></td>
</tr>
<!--Express 5.0 Qualifying Rate Detail-->
        <tr>

          <td colspan="11"><img src="../images/light_blue.gif" width="1"
            height="1" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="11"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        </tr>
      </tbody>
    </table>
    <table border="0" width="100%" cellpadding="1" cellspacing="0"
      bgcolor="d1ebff">
      <td colspan="11"><img src="../images/light_blue.gif" width="1"
        height="1" alt="" border="0"></td>
      <tbody>
        <tr>

          <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td valign="top"><font size="2" color="3366cc"><b>Applicant Name:</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>Applicant Type:</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>GDS:</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>GDS (Qualifying Rate):</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>TDS:</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>TDS (Qualifying Rate):</b></font></td>
          <jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
          <td valign="top"><font size="2" color="3366cc"><b>Insurance<br>
          Options :</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>Life<br>
          Coverage :</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>Disability<br>
          Coverage :</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>Guarantor<br>
          Other Loans :</b></font></td>
          <jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
        </tr>
        <tr>

          <!--START REPEATING ROW//-->
          <jato:tiledView name="RepeatGDSTDSDetails"
            type="mosApp.MosSystem.pgDealEntryRepeatGDSTDSDetailsTiledView">

            <td colspan="11"><img src="../images/blue_line.gif" width="100%"
              height="1" alt="" border="0"></td>
            <tr>
              <td valign="top">&nbsp;<jato:hidden name="hdGDSTDSApplicantId" /><jato:hidden
                name="hdGDSTDSApplicantCopyId" /></td>
              <td valign="top"><font size="2"><jato:text
                name="stGDSTDSFirstName" escape="true" /> <jato:text
                name="stGDSTDSMiddleInitial" escape="true" /> <jato:text
                name="stGDSTDSLastName" escape="true" />
                 <jato:text name="stGDSTDSSuffix" escape="true" /></font></td>
              <td valign="top"><font size="2"><jato:text
                name="stGDSApplicantType" escape="true" /></font></td>
              <td valign="top"><font size="2"><jato:text name="stApplicantGDS"
                escape="true" formatType="decimal" formatMask="#,##0.00; (-#)" /></font></td>
              <td valign="top"><font size="2"><jato:text
                name="stApplicant3YrGDS" escape="true" formatType="decimal"
                formatMask="###0.00; (-#)" /></font></td>
              <td valign="top"><font size="2"><jato:text name="stApplicantTDS"
                escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></font></td>
              <td valign="top"><font size="2"><jato:text
                name="stApplicant3YrTDS" escape="true" formatType="decimal"
                formatMask="###0.00; (-#)" /></font></td>
              <jato:text name="stIncludeLifeDisContentStart" escape="false" />
              <td valign="top"><font size="2"><jato:text
                name="stInsProportions" escape="true" /></font></td>
              <td valign="top"><font size="2"><jato:text name="stLifeCoverage"
                escape="true" /></font></td>
              <td valign="top"><font size="2"><jato:text
                name="stDisabilityCoverage" escape="true" /></font></td>
              <td valign="top"><font size="2"><jato:text
                name="stGuarantorOtherLoans" escape="true"
                fireDisplayEvents="true" /></font></td>
              <jato:text name="stIncludeLifeDisContentEnd" escape="false" />
            </tr>
            <tr>

            </tr>
          </jato:tiledView>
          <!--END REPEATING ROW//-->

          <td colspan="11"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>
        <tr>
          <jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
          <td colspan="11"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>

        </tr>
        <tr>
          <td colspan="11">
          <table border="0" cellspacing="3" width="100%">
            <tbody>
              <tr>
                <td colspan="10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button
                  name="btLDInsuranceDetails"
                  extraHtml="width=99  height=15 onClick = 'setSubmitFlag(true);'"
                  fireDisplayEvents="true"
                  src="../images/ld_insurance_details_en.gif" /></td>
              </tr>
            </tbody>
          </table>
          <jato:text name="stIncludeLifeDisLabelsEnd" escape="false" /></td>
        </tr>

        <td colspan="11"><img src="../images/light_blue.gif" width="1"
          height="2" alt="" border="0"></td>
        <tr>
          <td colspan="11"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        </tr>
      </tbody>
    </table>
    <!--END GDS/TDS DETAILS//-->

    <p><!--MCM Impl team changes starts - XS_2.13--></p>
    
        <jato:containerView name="qualifyingDetailsPagelet" type="mosApp.MosSystem.pgQualifyingDetailsPageletView">
            <jsp:include page="pgQualifyingDetailsPagelet.jsp"/>
        </jato:containerView>
     
     <!--MCM Impl team changes ends - XS_2.13-->
     
     <!--END QUALIFYING DETAILS//-->
    
    <p><!--START APPLICANT INFORMATION//--></p>
    <table border="0" width="100%" cellpadding="1" cellspacing="0"
      bgcolor="d1ebff">
      <tbody>
        <tr>
          <td colspan="9" bgcolor="d1ebff"><img src="../images/blue_line.gif"
            width="100%" height="1" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="9" bgcolor="d1ebff"><img src="../images/light_blue.gif"
            width="1" height="2" alt="" border="0"></td>
        </tr>
        <jato:hidden name="hdDealId" />
        <jato:hidden name="hdDealCopyId" />
        <input type="hidden" name="stLenderProductId"
          value='<jato:text name="stLenderProductId" fireDisplayEvents="true" escape="true" />'>
        <input type="hidden" name="stDefaultProductId"
          value='<jato:text name="stDefaultProductId" fireDisplayEvents="true" escape="true" />'>
        <input type="hidden" name="stDealEntry"
          value='<jato:text name="stDealEntry" fireDisplayEvents="true" escape="true" />'>
        <input type="hidden" name="stRateTimeStamp"
          value='<jato:text name="stRateTimeStamp" fireDisplayEvents="true" escape="true" />'>
        <input type="hidden" name="stRateLock"
          value='<jato:text name="stRateLock" fireDisplayEvents="true" escape="true" />'>

        <jato:hidden name="hdLongPostedDate" />

        <tr>
          <td colspan="9" valign="top" bgcolor="d1ebff"><font size="3"
            color="3366cc" bgcolor="d1ebff"><b>&nbsp;&nbsp;&nbsp; Applicant
          Information</b></font></td>
        </tr>
        <tr>
          <td colspan="9" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
        </tr>

        <tr>
          <td valign="top" bgcolor="d1ebff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td valign="top" bgcolor="d1ebff"><font size="2" color="3366cc"><b>Primary
          Borrower</b></font></td>
          <td valign="top" bgcolor="d1ebff"><font size="2" color="3366cc"><b>Name</b></font></td>
          <td valign="top" bgcolor="d1ebff"><font size="2" color="3366cc"><b>Language</b></font></td>
          <td valign="top" bgcolor="d1ebff"><font size="2" color="3366cc"><b>Home
          Phone #</b></font></td>
          <td valign="top" bgcolor="d1ebff"><font size="2" color="3366cc"><b>Work
          Phone #</b></font></td>
          <td valign="top" bgcolor="d1ebff"><font size="2" color="3366cc"><b>Applicant
          Type</b></font></td>
          <td colspan="2" bgcolor="d1ebff">&nbsp;</td>
        </tr>

        <tr>
          <td colspan="9" bgcolor="d1ebff"><img src="../images/light_blue.gif"
            width="1" height="2" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="9" bgcolor="d1ebff"><img src="../images/blue_line.gif"
            width="100%" height="1" alt="" border="0"></td>
        </tr>

        <!--START REPEATING ROW//-->
        <jato:tiledView name="RepeatApplicant"
          type="mosApp.MosSystem.pgDealEntryRepeatApplicantTiledView">

          <tr>
            <td colspan="9"><img src="../images/light_blue.gif" width="1"
              height="2" alt="" border="0"> <jato:hidden name="hdApplicantId" />
            <jato:hidden name="hdApplicantCopyId" /></td>
          </tr>

          <tr>
            <td valign="top" bgcolor="d1ebff">&nbsp;</td>
            <td valign="top" bgcolor="d1ebff"><jato:combobox
              name="cbPrimaryApplicant" /></td>
            <td valign="top" bgcolor="d1ebff"><jato:text name="stFirstName"
              escape="true" /> <jato:text name="stMiddleInitial" escape="true" />
            <jato:text name="stLastName" escape="true" /> <jato:text name="stSuffix" escape="true" /></td>
            <td valign="top" bgcolor="d1ebff"><jato:text name="stLanguage"
              escape="true" /></td>
            <td valign="top" bgcolor="d1ebff"><jato:text name="stHomePhoneNo"
              escape="true" formatType="string" formatMask="(???)???-????" /></td>
            <td valign="top" bgcolor="d1ebff"><jato:text name="stWorkPhoneNo"
              escape="true" formatType="string" formatMask="(???)???-????" /><jato:text
              name="stWorkPhoneExt" escape="true" /></td>
            <td valign="top" bgcolor="d1ebff"><jato:text
              name="stApplicantType" escape="true" /></td>
            <td valign="top" bgcolor="d1ebff"><jato:button
              name="btDeleteApplicant"
              extraHtml="onClick = 'setSubmitFlag(true);'"
              fireDisplayEvents="true" src="../images/delete_app.gif" /></td>
            <td valign="top" bgcolor="d1ebff"><jato:button
              name="btApplicantDetails"
              extraHtml="onClick = 'setSubmitFlag(true);'"
              fireDisplayEvents="true" src="../images/details.gif" /></td>
          </tr>

          <tr>
            <td colspan="9"><img src="../images/light_blue.gif" width="1"
              height="2" alt="" border="0"></td>
          </tr>
          <tr>
            <td colspan="9"><img src="../images/blue_line.gif" width="100%"
              height="1" alt="" border="0"></td>
          </tr>

          <!--END REPEATING ROW//-->



        </jato:tiledView>

        <tr>
          <td colspan="9"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>

        <tr>
          <td colspan="9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button
            name="btAddApplicant"
            extraHtml="width=81  height=15 onClick = 'setSubmitFlag(true);'"
            fireDisplayEvents="true" src="../images/add_app.gif" /> <jato:button
            name="btDupApplicant"
            extraHtml="width=81  height=15 onClick = 'setSubmitFlag(true);'"
            fireDisplayEvents="true" src="../images/Duplicate.gif" />
<!-- SEAN GECF IV Document Type Drop Down. -->
<jato:text name="stHideIVDocumentTypeStart" escape="false"/>
&nbsp;<font size=2 color=3366cc><b>Income Verification Type:</b></font> <jato:combobox name="cbIVDocumentType" />
<jato:text name="stHideIVDocumentTypeEnd" escape="false"/>
<!-- END GECF IV Document Type Drop Down. -->
            </td>
        </tr>

        <tr>
          <td colspan="9"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="9"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        </tr>

      </tbody>
    </table>
    <p><!--END APPLICANT INFORMATION//--> <!--START SOURCE INFORMATION//-->
<table border="0" width="100%" cellpadding="1" cellspacing="0"
  bgcolor="d1ebff">
  <tbody>
<tr>
  <td colspan="3" valign="top">
    <font size="2" color="3366cc">
      <b>Reference Source App #:</b>
    </font><br>
    <jato:textField name="txReferenceSourceApp" 
      size="35" maxLength="35" />
  </td>
</tr>
  <jato:tiledView name="RepeatedSOB" type="mosApp.MosSystem.pgDealEntryRepeatedSOBTiledView">
    </p>
        <tr>
          <td colspan="7"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="7"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>


        <tr>
          <td colspan="7" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;
          Source Information&nbsp;<jato:text name="stSourceNumber" escape="true" /></b></font></td>
        </tr>

        <tr>
          <td colspan="7" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
        </tr>

        <tr>
          <td valign="top"><font size="2" color="3366cc"><b>Source Firm</b></font><br>
          <jato:text name="stUWSourceFirm" escape="true" /></td>
          <td valign="top"><font size="2" color="3366cc"><b>Source</b></font><br>
          <jato:text name="stSourceFirstName" escape="true" /> <jato:text
            name="stSourceLastName" escape="true" /></td>
          <td valign="top"><font size="2" color="3366cc"><b>Source Address</b></font><br>
          <jato:text name="stUWSourceAddressLine1" escape="true" /></td>
          <td valign="top"><font size="2" color="3366cc"><b>Source Phone #</b></font><br>
          <jato:text name="stUWSourceContactPhone" escape="true" formatType="string"
            formatMask="(???)???-????" /> </td>
          <td valign="top"><font size="2" color="3366cc"><b>Source Fax #</b></font><br>
          <jato:text name="stUWSourceFax" escape="true" formatType="string"
            formatMask="(???)???-????" /></td>
          <td valign="top">&nbsp;</td>
        </tr>
        <tr>
          <!-- ========== DJ#725 begins ========== -->
          <!-- by Neil at Nov/29/2004 -->
          <!-- contactEmailAddress -->
          <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td valign="top"><font size="2" color="3366cc"><b>License Registration Number:</b></font><br>
            <jato:text name="stLicenseNumber" />
          </td>
          <td valign="top">
            <font size="2" color="3366cc"><b>Email:</b></font><br>
            <jato:text name="stContactEmailAddress" />
          </td>
          <!-- psDescription -->
          <td valign="top">
            <font size="2" color="3366cc"><b>Status:</b></font><br>
            <jato:text name="stPsDescription" />
          </td>
          <!-- systemTypeDescription -->
          <td valign="top">
            <font size="2" color="3366cc"><b>System Type:</b></font><br>
            <jato:text name="stSystemTypeDescription" />
          </td>
          <!-- ========== DJ#725 ends ========== -->
        </tr>
        <tr>
          <td colspan="7"><img src="../images/blue_line.gif" width="100%"
            height="2" alt="" border="0"></td>
        </tr>

        <tr>
          <td colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
          <jato:hidden name="hdSourceOfBusinessProfileId" />
          <jato:button name="btSOBDetails"
            extraHtml="onClick = 'setSubmitFlag(true);'"
            src="../images/details.gif" /> <jato:button name="btChangeSource"
            extraHtml="width=89  height=15 onClick = 'setSubmitFlag(true);'"
            fireDisplayEvents="true" src="../images/change_source.gif" /></td>
        </tr>

</jato:tiledView>
        <tr>
          <td colspan="7"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="7"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        </tr>

      </tbody>
    </table>
    <p><!--END SOURCE INFORMATION//--> <!--START PROPERTY INFORMATION//-->
    </p>
    <table border="0" width="100%" cellpadding="1" cellspacing="0"
      bgcolor="d1ebff">
      <tbody>
        <tr>
          <td colspan="7"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="7"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>

        <tr>
          <td colspan="7" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;
          Property Information</b></font></td>
        </tr>

        <tr>
          <td colspan="7" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
        </tr>

        <tr>
          <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td valign="top"><font size="2" color="3366cc"><b>Primary Property</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>Property Address</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>Purchase Price</b></font></td>
          <td valign="top"><font size="2" color="3366cc"><b>LTV</b></font></td>
          <td colspan="2">&nbsp;</td>
        </tr>

        <tr>
          <td colspan="7"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="7"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        </tr>



        <!--START REPEATING ROW//-->
        <jato:tiledView name="RepeatPropertyInformation"
          type="mosApp.MosSystem.pgDealEntryRepeatPropertyInformationTiledView">

          <tr>
            <td colspan="7"><img src="../images/light_blue.gif" width="1"
              height="2" alt="" border="0"> <jato:hidden name="hdPropertyId" />
            <jato:hidden name="hdPropertyCopyId" /></td>
          </tr>

          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top"><jato:combobox name="cbPrimaryProperty" /></td>
            <td valign="top"><jato:text name="stPropertyStreetNumber"
              escape="true" /> <jato:text name="stPropertyStreetName"
              escape="true" /> <jato:text name="stPropertyStreetType"
              escape="true" /> <jato:text name="stPropertyStreetDirection"
              escape="true" /> <jato:text name="stProvince" escape="true" /></td>
            <td valign="top"><jato:text name="stPurchasePricePropertyPart"
              escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></td>
            <td valign="top"><jato:text name="stLTV" escape="true" /></td>
            <td valign="top"><jato:button name="btDeleteProperty"
              extraHtml="onClick = 'setSubmitFlag(true);'"
              fireDisplayEvents="true" src="../images/delete_property.gif" /></td>
            <td valign="top"><jato:button name="btPropertyDetails"
              extraHtml="onClick = 'setSubmitFlag(true);'"
              src="../images/details.gif" /></td>
          </tr>

          <tr>
            <td colspan="7"><img src="../images/light_blue.gif" width="1"
              height="2" alt="" border="0"></td>
          </tr>
          <tr>
            <td colspan="7"><img src="../images/blue_line.gif" width="100%"
              height="1" alt="" border="0"></td>
          </tr>
          <!--END REPEATING ROW//-->

        </jato:tiledView>

        <tr>
          <td colspan="7"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>

        <tr>
          <td colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button
            name="btAddProperty"
            extraHtml="width=76  height=15 onClick = 'setSubmitFlag(true);'"
            fireDisplayEvents="true" src="../images/add_property.gif" /></td>
        </tr>

        <tr>
          <td colspan="7"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="7"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        </tr>

      </tbody>
    </table>
    <!--END PROPERTY INFORMATION//--> 
<%--
<script language="javascript">

  function delayInitLenderProductProfile()
  {
          ////alert("DelayILPP:: stamp1");

    ////alert("DelayILPP::stLenderProfileId: " + "<jato:text name="stLenderProfileId" fireDisplayEvents="true" escape="true" />");

    populateLenderList();

    ////alert("DelayILPP:: stamp2");

    ////alert("DelayILPP::stLenderProfileId: " + "<jato:text name="stLenderProfileId" fireDisplayEvents="true" escape="true" />");
    ////alert("DelayILPP::stRateLock: " + "<jato:text name="stRateLock" fireDisplayEvents="true" escape="true" />");

    var rateLock = "<jato:text name="stRateLock" fireDisplayEvents="true" escape="true" />";
    ////var rateLock = "Y";
    ////alert("DelayILPP::stRateLock_again: " + rateLock);

    var dealEntry = "<jato:text name="stDealEntry" fireDisplayEvents="true" escape="true" />";
    ////alert("DelayILPP::stDealEntry: " + dealEntry);

    var defaultProductId = "<jato:text name="stDefaultProductId" fireDisplayEvents="true" escape="true" />";
    ////alert("DelayILPP::stDefaultProductId: " + defaultProductId);

    var lenderProductId = "<jato:text name="stLenderProductId" fireDisplayEvents="true" escape="true" />";
    ////alert("DelayILPP::stLenderProductId: " + lenderProductId);

    var rateTimeStamp = "<jato:text name="stRateTimeStamp" fireDisplayEvents="true" escape="true" />";
    ////alert("DelayILPP::stRateTimeStamp: " + rateTimeStamp);

    var rateInventoryId = "<jato:text name="stRateInventoryId" fireDisplayEvents="true" escape="true" />";
    ////alert("RateInventoryId: " + rateInventoryId);
    
    //--Release2.1--//
    //// PaymentTermId var to handle the PaymentTermId except the PaymentTermDescription
    var pmntTermId = "<jato:text name="hdPaymentTermId" fireDisplayEvents="true" escape="true" />";
    ////alert("PaymentTermIdOriginal: " + pmntTermId);
    
    //--DisableProductRateTicket#570--10Aug2004--start--//  
    var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
    //alert("DealRateStatusId: " + dealRateStatusId);
    
    var isPageEditable = "<jato:text name="stIsPageEditableForServlet" fireDisplayEvents="true" escape="true" />";
    //alert("stIsPageEditableForServlet: " + isPageEditable);   
    //--DisableProductRateTicket#570--10Aug2004--end--//
    
         if(<jato:text name="stLenderProfileId" fireDisplayEvents="true" escape="true" /> >= 0)
                  var existedLenderId=<jato:text name="stLenderProfileId" fireDisplayEvents="true" escape="true" />;
               else
                  var existedLenderId=0;

      for ( i = 0 ;  i <=  document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options.length -1 ; i ++  )
      {
      if ( document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options[i].value == existedLenderId  )
      break; // found, quit.
      }

      ////alert("DelayILPP:: stamp3");

      if (i < document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options.length)
            {
      document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.selectedIndex = i ;
      //--DisableProductRateTicket#570--10Aug2004--start--// 
      if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
      //--DisableProductRateTicket#570--10Aug2004--end--//
                {
                    populateLenderListRestricted();
                    document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.length = 1;
                }
      }
      else
      return; // no match on lender - don't attempt further population

      ////alert("DelayILPP:: stamp4");

      getListInitial("document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct", existedLenderId);

    if (productArray.length == 0)
       return;

    tempProductId = -1 ;
    tempDefaultProdId = -1;

        // I. Default stuff is related to the brand new deal (page DealEntry) only!!
  ////if ( (document.forms[0].<%=viewBean.PAGE_NAME%>_stDealEntry.value == "Y") &&
        ////     (document.forms[0].<%=viewBean.PAGE_NAME%>_stDefaultProductId != null) )     // set default ProductId for this Lender
  if ( (dealEntry == "Y") &&
             (defaultProductId != null) )     // set default ProductId for this Lender
  {
      ////alert("DelayILPP:: stamp5");
      ////alert("DelayILPP::cbProductLength: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length);

      ////for ( i = 0 ; i <= document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length - 1; i ++ )
      for ( i = 0 ; i <= document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length; i ++ )
      {
        ////alert("DelayILPP:: stamp6");
        tempDefaultProdId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[i].value ; // get ProductId
        ////if  ( tempDefaultProdId ==  document.forms[0].<%=viewBean.PAGE_NAME%>_stLenderProductId.value  )
        if  ( tempDefaultProdId ==  lenderProductId  )
        {
          break ;
                    }
                          ////alert("DelayILPP:: stamp7");

        ////if  ( (tempDefaultProdId == document.forms[0].<%=viewBean.PAGE_NAME%>_stDefaultProductId.value) &&
        ////    (document.forms[0].<%=viewBean.PAGE_NAME%>_stLenderProductId.value == 0) )
        if  ( (tempDefaultProdId == defaultProductId) &&
            (lenderProductId == 0) )
        {
          break ;
                    }
                  }

      document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.selectedIndex = i;
            defaultProductId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;

      ////alert("DelayILPP:: stamp8");

      //--DisableProductRateTicket#570--10Aug2004--start--// 
      if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
      //--DisableProductRateTicket#570--10Aug2004--end--//
            {
    ////document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0].value = document.forms[0].<%=viewBean.PAGE_NAME%>_stLenderProductId.value;
    document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0].value = lenderProductId;
                productId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;
                populateProductRestricted( productId );
            }

    ////alert("DelayILPP:: stamp9");

    getListInitial("document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate", defaultProductId);

    ////alert("DelayILPP:: stamp8");

      tempRate = "";
      ////if ( document.forms[0].<%=viewBean.PAGE_NAME%>_stRateTimeStamp != null )    // check existed postedInterestRate
      if ( rateTimeStamp != null )    // check existed postedInterestRate
      {
          selectedRateId = 0 ;
        for ( i = 0 ; i <= document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length - 1 ; i ++ )
        {
          tempRate = document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i].value ;
          tempRate = tempRate.substring ( tempRate.length - 32 , tempRate.length )  ;
          ////if  ( tempRate ==  document.forms[0].<%=viewBean.PAGE_NAME%>_stRateTimeStamp.value  )
          if  ( tempRate ==  rateTimeStamp  )
          {
            selectedRateId = i ;
            break ;
          }
        }
        if (document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length > 0)
          document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex = selectedRateId ;
      }

      //--DisableProductRateTicket#570--10Aug2004--start--// 
      if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
      //--DisableProductRateTicket#570--10Aug2004--end--//
                {
                    rateValue = document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex ].value ;
                    populateRateRestricted( rateValue );
      }

        } // end DealEntry if

        // II. Page 'Deal Modification' case. First, pick up the saved ProductId for this LenderProfileId
        ////else if( document.forms[0].<%=viewBean.PAGE_NAME%>_stDealEntry.value == "N")
        else if( dealEntry == "N")
        {
        ////if ( document.forms[0].<%=viewBean.PAGE_NAME%>_stLenderProductId != null )    // set pre-selected ProductId
        if ( lenderProductId != null )    // set pre-selected ProductId
        {
        for ( i = 0 ; i <= document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length - 1 ; i ++ )
        {
          tempProductId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[i].value ;  // get ProductId
          ////if  ( tempProductId ==  document.forms[0].<%=viewBean.PAGE_NAME%>_stLenderProductId.value  )
          if  ( tempProductId ==  lenderProductId  )
            break ;
        }
        }

        if (tempProductId == -1)
        {
          i = 0;
          tempProductId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0].value;
        }

        document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.selectedIndex = i;

        //--DisableProductRateTicket#570--10Aug2004--start--// 
        if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
        //--DisableProductRateTicket#570--10Aug2004--end--//
              {
    ////document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0].value = document.forms[0].<%=viewBean.PAGE_NAME%>_stLenderProductId.value;
    document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0].value = lenderProductId;
                productId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;
                populateProductRestricted( productId );
              }

        getListInitial("document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate", tempProductId);

        tempRate = "";
        ////if ( document.forms[0].<%=viewBean.PAGE_NAME%>_stRateTimeStamp != null )    // check existed postedInterestRate
        if ( rateTimeStamp != null )    // check existed postedInterestRate
        {
          selectedRateId = 0;
          index = 0;

        //// Release2.1 optimization.
        for ( i = 0 ; i <= document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length - 1 ; i ++ )
        {
          tempRate = document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i].value ;  // get PricingRateInventoryId
          //alert("tempRate: " + tempRate);
          tempIndex = document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i].index ;  // get PricingRateInventoryId
          //alert("tempIndex: " + tempIndex);
          //alert("InventoryId_again: " + rateInventoryId);

          ////--Release2.1--//
          //// The whole java logic is changed now and based on pricingRateInventoryid
          //// passed here. This fragment along with the UWorksheet should be polished
          //// and separated.
          ////tempRate_1 = document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i].value ;
          ////tempRate_1 = tempRate.substring ( tempRate.length - 32 , tempRate.length );
          /////alert("tempRate_again:" +  tempRate_1);

          ////if  ( tempRate ==  document.forms[0].<%=viewBean.PAGE_NAME%>_stRateTimeStamp.value  )
          ////if  ( tempRate ==  rateTimeStamp  )
          if  ( tempRate ==  rateInventoryId )

          {
            //--Release2.1--//
            //// The logic is changed now (see above).
            ////selectedRateId = i ;
            index = tempIndex;

            //alert("SelectedRateIdInIf: " + selectedRateId);
            //alert("IndexInIf: " + index);
            break ;
          }
        }
        if (document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length > 0)
        {
          ////alert("IndexToSet: " + index);

          //--Release2.1--//
          //// The logic is changed now (see above).
          ////document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex = selectedRateId;
          document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex = index;

                                }
        }

              // case of locked rate
        //--DisableProductRateTicket#570--10Aug2004--start--// 
        if (rateLock == "Y" || isPageEditable == "N") // case of locked rate or non-editable page
        //--DisableProductRateTicket#570--10Aug2004--end--//
              {
                  rateValue = document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex ].value ;
                  populateRateRestricted( rateValue );
        }
           } // end if else DealModification case


////////////////////


         }

  ////alert("After invoke delayInitLenderProductProfile");

////////////////////////////////
////////////////////////////////

  function getLenderProfiles()
  {
    ////alert("getLenderProfilesOrig::start");

    //// Test methods with the more reliable new style applet call.
    ////alert("getLenderProfilesOrig::ATN_galia1: " + document.applets["ATN"].name);
    ////alert("getLenderProfilesOrig::temp_new: " + document.ATN['getLenderProfiles']());
    ////alert("getLenderProfilesOrig::temp_old: " + document.ATN.getLenderProfiles());

    temp =  document.ATN.getLenderProfiles();
    lenderArray  = new Array;
    string2Array ( temp , lenderArray );

    ////alert("string2Array: " + temp);
  }

  //--DisableProductRateTicket#570--10Aug2004--start--//    
  function getLenderProducts( lenderId ) {
//alert("getLenderProducts stamp3");

    var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
    var rateDisDate = "<jato:text name="stRateDisDateForServlet" escape="true" />";
    
                var lprd = "LenderId," + lenderId + "|" + "DealRateStatusId," + dealRateStatusId + "|" 
                  + "AppDate," + applicationDate + "|" + "RateDisDate," + rateDisDate + "|";
                                                                                            
//alert("LenderId: " + lenderId);
//alert("DealRateStatusId: " + dealRateStatusId);
//alert("RateDisDate: " + rateDisDate);
                    
                  
    temp =  document.ATN.getLenderProducts( lprd.toString() )  ;  // call the Applet
    // For new applet tag
    ////temp =  document.all.ATN.getLenderProducts( lprd.toString() )  ;  // call the Applet
//alert("temp: " + temp);

    productArray = new Array ;
    string2Array ( temp , productArray );

    }
    //--DisableProductRateTicket#570--10Aug2004--end--// 

  function getPaymentTerms ( )
  {
     temp =  document.ATN.getPaymentTerms( )  ;  // call the Applet
     tempPaymentTermArray = new Array ;
     string2Array ( temp , tempPaymentTermArray  );

     ////alert("getPaymentTermsOrig::PaymentTerms " + temp);
    
    for ( i = 0 ; i <= tempPaymentTermArray.length -1 ; i ++ )
    {
      tempItem = tempPaymentTermArray [i];
            
      sepPos =  tempItem.indexOf ( "," ) ;
      
      //--Release2.1--//
      //// Populate new PaymentTermId array to populate new hidden 
      //// PaymentTermId field for database population from the screen.
      sepIds = tempItem.indexOf (":");
      
      if ( sepPos > 0 )
      {
        paymentTermArray[i] = tempItem.substring(1, sepPos-1 );
        
        //--Release2.1--//
        //// Populate new PaymentTermId array to populate new hidden 
        //// PaymentTermId field for database population from the screen.
        ////productIdArray[i] = tempItem.substring( sepPos+2 , tempItem.length -1 ) ;

        productIdArray[i] = tempItem.substring( sepPos+2 , sepIds - 1 ) ;           
        paymentTermIdArray[i] = tempItem.substring( sepIds+2 , tempItem.length -1 ) ;
        
        ////alert("PaymentTermArray[" + i + "]: " + paymentTermArray[i]);
        ////alert("productIdArray[" + i + "]: " + productIdArray[i]);       
        ////alert("paymentTermIdArray[" + i + "]: " + paymentTermIdArray[i]);       
      }
    }
    
  }

  function getRateCodes( productId )
  {
    ////alert("getRateCodes::start");

    lenderId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;

                //// Do it locally. Application Date is for future delivery of the RateAdmin bug.
    applicationDate = "<jato:text name="stAppDateForServlet" escape="true" />";
    //--DisableProductRateTicket#570--10Aug2004--start--// 
    var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
    var rateDisDate = "<jato:text name="stRateDisDateForServlet" escape="true" />";
    
                var lprd = "LenderId," + lenderId + "|" + "ProductId," + productId + "|" + 
                     "DealRateStatusId," + dealRateStatusId + "|" + "AppDate," + applicationDate + "|" +
                     "RateDisDate," + rateDisDate + "|";
      //--DisableProductRateTicket#570--10Aug2004--end--// 
                
//alert("RC_LenderId: " + lenderId);
//alert("RC_DealRateStatusId: " + dealRateStatusId);
//alert("RC_rateDisDate: " + rateDisDate);
//alert("RC_ProductId: " + productId);
//alert("RC_ApplicationDate: " + applicationDate);               
//alert("RC_lprd: " + lprd);

    ////temp = document.ATN.getRateCodes (lenderId, productId);
    // For new applet tag
    ////temp = document.all.ATN.getRateCodes (lprd.toString());
    //// Porting to Sun JVM with one parameter passing to Applet.       
    temp = document.ATN.getRateCodes( lprd.toString()  );   

////alert("getRateCodes::RateCodesArray: " + temp);

    rateCodeArray = new Array;
    string2Array( temp, rateCodeArray);

    for ( i=0 ; i <= rateCodeArray.length -1 ; i ++ )
    {
      tempRateCode = rateCodeArray[i] ;
      pos = tempRateCode.indexOf ( "," ) ;
      if ( pos > 0 )
        rateCodeArray[i] = tempRateCode.substring ( 1, pos-1  ) ;
    }
  }

  function getLenderProductRates( productId )
  {
    lenderId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;
    
                //// Do it locally. Application Date is for future delivery of the RateAdmin bug.
    applicationDate = "<jato:text name="stAppDateForServlet" escape="true" />";                                
    //--DisableProductRateTicket#570--10Aug2004--start--//    
    var dealRateStatusId = "<jato:text name="stDealRateStatusForServlet" escape="true" />";
    var rateDisDate = "<jato:text name="stRateDisDateForServlet" escape="true" />";
    
                var lprd = "LenderId," + lenderId + "|" + "ProductId," + productId + "|" + 
                     "DealRateStatusId," + dealRateStatusId + "|" + "AppDate," + applicationDate + "|" +
                     "RateDisDate," + rateDisDate + "|";
      //--DisableProductRateTicket#570--10Aug2004--end--// 
                
//alert("LenderId: " + lenderId);
///alert("ProductId5_1: " + productId);
//alert("dealRateStatusId5_1: " + dealRateStatusId);
//alert("dealRateDisDate5_1: " + rateDisDate);
//alert("ApplicationDate: " + applicationDate);                
//alert("lprd5_1: " + lprd);

    ////temp = document.ATN.getLenderProductRates( lenderId, productId  )  ;
    // For new applet tag
    ////temp = document.all.ATN.getLenderProductRates( lprd.toString()  )  ;
    //// Porting to Sun JVM with one parameter passing to Applet.
    temp = document.ATN.getLenderProductRates( lprd.toString()  )  ;
    
////alert("getLenderProductRates: " + temp);

    rateArray = new Array;
    string2Array( temp, rateArray);
  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SourceString Format:
  //    'productName', 'id' ? 'productName', 'id' ?

  function string2Array( sourceStr, destArray )
  {
    i = sourceStr.indexOf ( "?" );
    itemCounter = 0;
    aitem = "";
    aitem = sourceStr.substring( 1, 2);
    while ( i > 0 )
    {
      aitem = sourceStr.substring ( 0,  i  );
      destArray[ itemCounter ] = aitem;
      sourceStr = sourceStr.substring( i + 1 );
      i = sourceStr.indexOf( "?" ) ;
      itemCounter ++  ;
    }
  }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function populateLenderList() {

      ////alert("populateLenderList::start");

      getLenderProfiles() ; // populate the LenderArray

      ////alert("populateLenderList::after getLenderProfiles");

      ////DIFFERENT TESTING OF THE LENDERID INVOCATION.

      ////alert("populateLenderList::LenderArrayLength: " + lenderArray.length);
      ////alert("populateLenderList::cbLenderObject: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.value);
      ////alert("populateLenderList::cbLenderOptionsLength: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options.length);
      ////alert("populateLenderList::cbChargeTermObject: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbChargeTerm.value);


      ////alert("PLL::RateLockValue: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbRateLocked.value);
      ////alert("PLL::RateLockName: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbRateLocked.name);

      ////alert("PLL::RateLockBoundName: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbRateLocked.boundName);
      ////alert("PLL::RateLockParent: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbRateLocked.parent);
      ////alert("PLL::RateLockOptionList: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbRateLocked.options);
      ////alert("PLL::RateLockOptionListSize: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbRateLocked.options.size);

      ////alert("populateLenderList::0_element: " + lenderArray[0]);

      //// VERY TEMP TO TEST THE COMBOBOX POPULATION.
      ////eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbRateLocked.options[0]=" + "new Option(" + lenderArray[0] + ")" );
      ////alert("populateLenderList::cbLenderElement: " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbRateLocked.value);

  while ( lenderArray.length < document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options.length ) {
    document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options[ (document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options.length - 1 ) ] = null ;
  }
  for ( var i=0; i< lenderArray.length; i++) {
    eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options[i]=" + "new Option(" + lenderArray[i] + ")" );
  }
  document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0]=null;

  ////alert("populateLenderList::finished");
}

function populateLenderListRestricted() {
  getLenderProfiles() ; // populate the LenderArray

  for ( var i=0; i< lenderArray.length; i++) {
    eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options[i]=" + "new Option(" + lenderArray[document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.selectedIndex] + ")" );
  }

  document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options[0].length = 1;

  document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0]=null;

}

function populateProduct(){


  ////alert("populateProduct::start");

  while (productArray.length < document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length) {
    document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[(document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length - 1)] = null;
  }
  for (var i=0; i < productArray.length; i++) {
    eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[i]=" + "new Option(" + productArray[i]+")");
  }

  if ( document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length > 0 )
  {
    getPaymentTerms();
    populatePaymentTerm() ;
  }else {
    document.forms[0].<%=viewBean.PAGE_NAME%>_tbPaymentTermDescription.value = "" ;
  }

  ////alert("populateProduct::finish");
}

function populateProductRestricted(id){

  document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0] = null;

  for (var i=0; i < productArray.length; i++)
  {
      a = new Object();
        a = productArray[i].split (",");

      index = new String(a[1]).match(/\d+/);

    if  ( index == id)
        {
        eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[0]=" + "new Option(" + productArray[i]+")");
        document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.length = 1;
          break ;
        }
  }
}

function showRate(){
  document.open ;
  document.write (  " Rate = " + document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[  document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex  ].value  ) ;
  document.close;
}

function populateRate() {

  while (0 < document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length) {
    document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[(document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options.length - 1)] = null;
  }
  for (var i=0; i < rateArray.length; i++) {
    eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[i]=" + "new Option(" + rateArray[i]+")");
  }

}


function populateRateRestricted(inValue)
{
    document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[0] = null;

    strIn = "\'" + new String(inValue) + "\'";

  for (var i=0; i < rateArray.length; i++)
  {
      a = new Object();
        a = rateArray[i].split (",");
        index = new String(a[1]);
    if  ( index == strIn)
        {
        eval("document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[0]=" + "new Option(" + rateArray[i]+")");
        document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.length = 1;
          break ;
        }
  }
}

function populateRateCode( ) {


  id = document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.selectedIndex ;

  if ( ( id == -1 ) ||  ( rateCodeArray.length <=0)  ) {
    document.forms[0].<%=viewBean.PAGE_NAME%>_tbRateCode.value = "";

    document.forms[0].<%=viewBean.PAGE_NAME%>_hdLongPostedDate.value = "";

    }
  else {
    document.forms[0].<%=viewBean.PAGE_NAME%>_tbRateCode.value = rateCodeArray[ id ];

        document.forms[0].<%=viewBean.PAGE_NAME%>_hdLongPostedDate.value = document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate.options[ id ].value ;
    }
}


function populatePaymentTerm( ) {
  tempId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.selectedIndex ;

  if ( tempId == null )
    return ;
  if ( tempId ==  -1 )
    tempId = 0 ;

  pId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options[ tempId ].value ;

  for ( id= -1,   i = 0 ; i <= productIdArray.length -1 ; i ++ )
  {
    if ( productIdArray[i] == pId  )
    {   id = i ;
      break ;
    }
  }
  if ( id == -1  )
  {
    document.forms[0].<%=viewBean.PAGE_NAME%>_tbPaymentTermDescription.value = "";
  }
  else
  {
    document.forms[0].<%=viewBean.PAGE_NAME%>_tbPaymentTermDescription.value = paymentTermArray[id];
        
    //--Release2.1--//
    //// Populate new PaymentTermId array to populate new hidden 
    //// PaymentTermId field for database population from the screen. 
    pmntTermId = paymentTermIdArray[id];    
    document.forms[0].<%=viewBean.PAGE_NAME%>_hdPaymentTermId.value = paymentTermIdArray[id];
    
    ////alert("PaymentTermIdPopulate: " + paymentTermIdArray[id]);  
    ////alert("PaymentTermPopulate: " + paymentTermArray[id]);
    ////alert("Final: " + document.forms[0].<%=viewBean.PAGE_NAME%>_hdPaymentTermId.value);   
    
        }   
}

function getListInitial(whichtype, id)
{
  ////alert("getListInitial::start");

  if(whichtype=="document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct")
  {
    getLenderProducts( id );

    ////alert("getListInitial::ID: " + id);

    populateProduct();

    return;
  }

  if(whichtype=="document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate")
  {
    if (id == -1)
    {
      rateArray = new Array ;
      populateRate();
      populateRateCode();
    }

    else
    {
      getLenderProductRates( id );
      populateRate();

      getRateCodes( id);
      populateRateCode( );

      getPaymentTerms();
      populatePaymentTerm( );
    }

    return;
  }
}

function getList(whichtype){

  var afterProduct="false";
  if(whichtype=="document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct")
  {
    lenderId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;

    getLenderProducts( lenderId ) ;   // Change the products
    populateProduct();

        afterProduct="true";
  }
  if ( productArray.length == 0 )
  {
    rateArray = new Array ;
    populateRate();
    populateRateCode();
  }

  else if (  (whichtype=="document.forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate")  || ( afterProduct=="true")   )  {

    if ( afterProduct != "true" )
      productId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;
    else if ( productArray.length > 0 )
        productId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options [ 0 ].value ;

    getLenderProductRates( productId ) ;
    populateRate();

    getRateCodes( productId );
    populateRateCode( ) ;

    populatePaymentTerm( ) ;
  }

}

function getListRestricted(whichtype){

  var afterProduct="false";
  if(whichtype=="forms[0].<%=viewBean.PAGE_NAME%>_cbProduct")
  {
    lenderId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbLender.selectedIndex ].value ;

    getLenderProducts( lenderId ) ;   // Change the products
        populateProductRestricted();

        document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length = 1;

        afterProduct="true";
  }
  if ( productArray.length == 0 )
  {
    rateArray = new Array ;
    populateRate();
    populateRateCode();
  }

  else if (  (whichtype=="forms[0].<%=viewBean.PAGE_NAME%>_cbPostedInterestRate")  || ( afterProduct=="true")   )  {
    if ( afterProduct != "true" ) {
      productId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options [ document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.selectedIndex ].value ;
      populateProductRestricted();

            document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length = 1;
    }

    else if ( productArray.length > 0 ) {
        productId = document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options [ 0 ].value ;
                document.forms[0].<%=viewBean.PAGE_NAME%>_cbProduct.options.length = 1;

    }

    getLenderProductRates( productId ) ;
    populateRateRestricted();

    getRateCodes( productId );
    populateRateCode( ) ;

    populatePaymentTerm( ) ;
  }
}

</script> 
--%>

<!-- Include Lender Product Rate function --//-->
<jsp:include page="pgDealEntry_lpr.jsp"/>

<!-- Include Part 2 of DealEntry --//--> 
<jsp:include page="pgDealEntry_part2.jsp" /> 
<!--
<jato:text name="stRateLock" fireDisplayEvents="true" escape="true" />
<jato:text name="stPageTitle" escape="true" />
-->
</center>
    </div>
  </jato:form>

  <script language="javascript">
<!--

if(NTCP){
  document.dialogbar.left=55;
  document.dialogbar.top=79;
  document.toolpop.left=317;
  document.toolpop.top=79;
  document.pagebody.top=100;
  document.alertbody.top=100;
}
if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(4)
}
else{
  tool_click(5)
  location.href="#loadtarget"
}


if(pmGenerate=="Y")
{
openPMWin();
}


//// IMPORTANT!! THE ORDER OF THESE TWO MAJOR CALLS HAS BEEN CHANGED
//// COMPARING WITH ND VERSION. DisplayOrHideRefiSection() CALL MUST BE DONE
//// HERE AND delayInitLenderProductProfile() FROM THE AFTER THE INITIALIZATION
//// OF THE FINANCIAL DETAILS SECTION ABOVE.

delayInitLenderProductProfile();

DisplayOrHideRefiSection();

//executes initializing method for component info related check boxies.
initConponentInfoCheckBoxis();

//-->
</script>


  <div></div>


  <!--END BODY//-->

  </body>

</jato:useViewBean>
</html>
