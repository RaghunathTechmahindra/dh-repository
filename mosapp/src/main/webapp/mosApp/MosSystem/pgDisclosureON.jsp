
<HTML>
<%@page info="pgDisclosureON" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDisclosureONViewBean">

<HEAD>
<TITLE>Disclosure Statement Page</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression(QLE?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>

</HEAD>
<body onload="setCancelFlag(true);" bgcolor=ffffff>
<jato:form name="pgDisclosureON" method="post" onSubmit="is_submit_button();">

<SCRIPT LANGUAGE="JavaScript">

var isCreateDocButton = false;
var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

function handleChangeBrokerAsLender() {
	var theBrokerAsLender = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_020E.value");
	var theNatureOfRelationship = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_020F.value");

	if (theBrokerAsLender == "Y")
    {
		document.forms[0].<%= viewBean.PAGE_NAME%>_020F.disabled = false;
	}
	else if (theBrokerAsLender == "N")
	{
		document.forms[0].<%= viewBean.PAGE_NAME%>_020F.disabled = true;
		document.forms[0].<%= viewBean.PAGE_NAME%>_020F.value = '';
	}
}

function checkDealStatus() 
{
	var dealStatusId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdDealStatusId.value");
	
	if(dealStatusId == "23")
		var msgStatus = "Collapsed.";
	if(dealStatusId == "24")
		var msgStatus = "Denied."
		
	var msg = "You cannot generate a disclosure document while the deal is " + msgStatus;
		
	if(dealStatusId == "23" || dealStatusId == "24")
	{
		alert(msg);
		setSubmitFlag(false);
		return;
	}
	else
	{
		setCancelFlag(false);
		//setSubmitFlag(true);
		isCreateDocButton = true;
		document.forms[0].submit();
	}
}

function is_submit_button()
{
	if(!isCreateDocButton)
	{
		return IsSubmitButton();
	}
	else
	{
		isCreateDocButton = false;
	}
}

function generateDocument(flag) {

	handleChangeBrokerAsLender();

	var fff = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdGenerateDocumentPdf.value");

	if (fff == "true") 
	{
		// set the flag to false
		//newValueForFlag = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdGenerateDocument");
		//newValueForFlag.value = "false";
		
		var genDocFlag = document.getElementById("pgDisclosureON_hdGenerateDocumentPdf"); 
		genDocFlag.value="false";

		var genDocFlag2 = document.getElementById("pgDisclosureON_hdGenerateDocumentPdf"); 

		var dealId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_stDealId.value");
		var copyId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdCopyId.value");
		var provAdbbr;
		if(eval(<%=viewBean.handler.provinceId%>) == eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ALBERTA%>)) {
			provAbbr = "AB"; 
		} else if(eval(<%=viewBean.handler.provinceId%>) == eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_NEWFOUNDLAND%>)) {
			provAbbr = "NL";
		} else if(eval(<%=viewBean.handler.provinceId%>) == eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_NOVA_SCOTIA%>)) {
			provAbbr = "NS";
		} else if(eval(<%=viewBean.handler.provinceId%>) == eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_PRINCE_EDWARD_ISLAND%>)) {
			provAbbr = "PE";
		} else if(eval(<%=viewBean.handler.provinceId%>) == eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ONTARIO%>)) {
			provAbbr = "ON";
		}
		alert(provAbbr);
		// change the language id to 1 for french and province if not ON
		window.open("DocumentGenerator?dealId=" + dealId + "&copyId=" + copyId + "&languageId=0" + "&provinceAbbr=" + provAbbr , "DisclosureStatement");
	}
}

function isPositiveNumeric(isDecimal)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	if(!isDecimal || (theFieldValue < 0))
	{
		alert(printf(NUMERIC_ONLY));	//use NUMERIC_ONLY according to FS.
		theField.focus();
		theField.select();
		return false;
	}

	return true;
}

function limitLength(len)
{
	var theField = event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	if(theFieldValue.length > len)
	{
		theField.blur();
		theField.focus();
		return false;
	}

	return true;
}

function alertMsg(len)
{
	var theField = event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	if(theFieldValue.length > len)
	{
		alert("The maximum number of characters is " + len + ". Please adjust your input accordingly");
		theField.focus();
		var rng = theField.createTextRange();
		rng.move("textedit");
		rng.select();
	}
}

function setCancelFlag(tof)
{
	//Bug# 1698 fix - start
	//added condition check to execute block only for province Ontario
	if(eval(<%=viewBean.handler.provinceId%>) == eval(<%=com.basis100.deal.docprep.Dc.PROVINCE_ONTARIO%>)){
	 handleChangeBrokerAsLender();
	 var instr = document.getElementById("pgDisclosureON_hdInstr"); 
	if(tof || instr != null)
		instr.value="cancel";
	else
		instr.value="";
	}
	//Bug# 1698 fix - end
}

</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
    
     <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %>
	
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>

<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<!-- PUT HERE ANY SUSTOM CHILDREN ELEMENTS (TEXTBOXES, TILED VIEWS, ETC.)//-->

<table border=0 BGCOLOR=99ccff width=100%>
<!--Disclosure Date Section -->
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 2: Is the Mortgage broker, or any party with other than an arm�s length business relationship to the mortgage broker, acting as a lender for this mortgage?</b></font></td>
      <tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2>&nbsp;&nbsp;<jato:combobox name="020E" extraHtml="onChange='return handleChangeBrokerAsLender();'"  /></font></td>

      <tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 2: If Yes (to above question), describe nature of relationship, name and address of lender</b></font></td>
      <tr>
	<td colspan=4 valign=top><font size=2 >&nbsp;&nbsp;<jato:textArea name="020F" rows="4" cols="80"/></font></td>

      <tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Date to appear on printed Disclosure Statement form:</b></font></td>
      <tr>
	<td colspan=2 valign=top><font size=2 >&nbsp;&nbsp;Mth:&nbsp;<jato:combobox name="13UU_MNT" />&nbsp;&nbsp;Day:<jato:textField name="13UU_DAY" size="2" maxLength="2" />&nbsp;&nbsp;Yr:<jato:textField name="13UU_YEAR" size="4" maxLength="4" /></font></td>

      <tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 8: The mortgage is not renewable on the same terms as described in items 4 and 5 above and does not contain  any privileges or penalties (including charges for NSF cheques) except as follows:</b></font></td>
      <tr>
	<td colspan=4 valign=top><font size=2 >&nbsp;&nbsp;<jato:textArea name="08NN" rows="4" cols="80" /></font></td>

      <tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 9: Other terms and conditions of the mortgage</b></font></td>
      <tr>
	<td colspan=4 valign=top><font size=2 >&nbsp;&nbsp;<jato:textArea name="09OO" rows="4" cols="80" /></font></td>

      <tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

	<tr>
	<td valign=top>&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc>&nbsp;&nbsp;<b>Section 11: Broker Registration No.</b></font></td>
      <tr>
	<td colspan=4 valign=top><font size=2 >&nbsp;&nbsp;<jato:textArea name="11TT" rows="4" cols="80" /></font></td>

      <tr>
	<td align=center colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
	<jato:hidden name="hdCopyId" />
	<jato:hidden name="stDealId"/>
	<jato:hidden name="hdGenerateDocumentPdf" />
	<jato:hidden name="hdDealStatusId" />
	<jato:hidden name="hdInstr" />
<P>

<table border=0 width=100% cellpadding=0 cellspacing=0>
      <td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'checkDealStatus(); '" src="../images/submit.gif" />&nbsp;&nbsp;<jato:button name="btSaveAndExit" extraHtml="width=130 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/SaveAndExit.gif" />&nbsp;&nbsp;<jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/cancel.gif" /></td>
</table>


</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
