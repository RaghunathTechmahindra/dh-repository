
<HTML>
<%@page info="pgPropertyEntry" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgPropertyEntryViewBean">

<HEAD>
<TITLE>Property Entry</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression(QLE?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression(QLE?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcs.js" type="text/javascript"></script>


<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
	QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<!--
//	========================================================================================================
// Function to initialize the page -- may be difference for every pages
//		It should be used in the onLoad event of <body> tag
// ========================================================================================================
function initPage()
{
	
	// Init all Title handling
	////alert("initPropertyEntryPage: start");
	
	// Set default value for Hidden GDS TDG field 
	setDefaultsAll("cbPropertyExpenseType", ["hdPropertyExpenseGDSPercentage", "hdPropertyExpenseTDSPercentage"], "pgPropertyEntry_RepeatedPropertyExpenses[0]_cbPropertyExpenseType");	
	
	hideSubdDiscount();
	hideOnReserveTrustAgreementNumber();
}

function hideSubdDiscount()
{
	var insurerId = document.pgPropertyEntry.pgPropertyEntry_hdMortgageInsurerId.value;
	var newConstr = document.pgPropertyEntry.pgPropertyEntry_cbNewConstruction.value;

	var subdDiv = document.getElementById("subd");

	if(insurerId == 1 && (newConstr == 1 || newConstr == 2))
		subdDiv.style.display="";
	else
		subdDiv.style.display="none";
}

function hideOnReserveTrustAgreementNumber()
{
    var insurerId = document.pgPropertyEntry.pgPropertyEntry_hdMortgageInsurerId.value;
    var propType = document.pgPropertyEntry.pgPropertyEntry_cbPropertType.value;

    var onReserveDiv = document.getElementById("divOnReservTrustAgreementNumber");

    if(insurerId == 1 && propType == 5)
        onReserveDiv.style.display="";
    else
        onReserveDiv.style.display="none";
}
//-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff onload="initPage(); checkMIResponseLoop();">
<jato:form name="pgPropertyEntry" method="post" onSubmit="return(IsSubmitButton());">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

<jato:text name="stVALSData" escape="false" /> 

</SCRIPT>

<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
	<%@include file="/JavaScript/CommonHeader_en.txt" %>  

<%@include file="/JavaScript/CommonToolbar_en.txt" %>
<%@include file="/JavaScript/QuickLinkToolbar.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>    



<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>

<!--START PROPERTY INFORMATION//-->


<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=7><jato:hidden name="hdPropertyId" /><jato:hidden name="hdPropertyCopyId" /><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=7 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Property Information</b></font></td>
</tr>
<tr><td colspan=7>&nbsp;</td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Property Street Number:</b></font><br>
<jato:textField name="txPropertyStreetNumber" size="10" maxLength="10" /></td>
<td valign=top><font size=2 color=3366cc><b>Property Street Name:</b></font><br>
<jato:textField name="txPropertyStreetName" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>Property Street Type:</b></font><br>
<jato:combobox name="cbPropertyStreetType" /></td>
<td valign=top><font size=2 color=3366cc><b>Property Street Direction:</b></font><br>
<jato:combobox name="cbPropertyStreetDirection" /></td>
<td valign=top colspan="2"><font size=2 color=3366cc><b>Property Unit Number:</b></font><br>
<jato:textField name="txPropertyUnitNumber" size="10" maxLength="10" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td colspan=2 valign=top><font size=2 color=3366cc><b>Property Address Line 2:</b></font><br>
<jato:textField name="txPropertyAddressLine2" size="20" maxLength="35" /></td>
<td valign=top><font size=2 color=3366cc><b>Property City:</b></font><br>
<jato:textField name="txPropertyCity" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>Property Province:</b></font><br>
<jato:combobox name="cbPropertyProvince" /></td>
<td valign=top colspan="2"><font size=2 color=3366cc><b>Property Postal Code:</b></font><br>
<jato:textField name="txPropertyPostalFSA" size="3" maxLength="3" /> 
<jato:textField name="txPropertyPostalLDU" size="3" maxLength="3" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top colspan=6><font size=2 color=3366cc><b>Legal Description Line 1:</b></font><br>
<jato:textField name="txLegalDesc1" size="35" maxLength="35" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Legal Description Line 2:</b></font><br>
<jato:textField name="txLegalDesc2" size="35" maxLength="35" /></td>

<td valign=top><font size=2 color=3366cc><b>New Construction?</b></font><br>
<jato:combobox name="cbNewConstruction" extraHtml="onChange=hideSubdDiscount();" /></td>

<td valign=top >
<div id="subd">
<font size=2 color=3366cc><b>Subdivision Discount:</b></font><br>
<font size="2"><jato:radioButtons name="rbSubdivisionDiscount" layout="horizontal" /></font>
</div>
</td>

<td valign=top><font size=2 color=3366cc><b>Structure Age:</b></font><br>
<jato:textField name="txStructureAge" formatType="decimal" formatMask="###0; (-#)" size="5" maxLength="3" /></td>
<td valign=top colspan="2"><font size=2 color=3366cc><b>Builder Name:</b></font><br>
<jato:textField name="txBuilderName" size="35" maxLength="80" />

<jato:hidden name="hdMortgageInsurerId" />
</td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Legal Description Line 3:</b></font><br>
<jato:textField name="txLegalDesc3" size="35" maxLength="35" /></td>
<td valign=top><font size=2 color=3366cc><b>Dwelling Type:</b></font><br>
<jato:combobox name="cbDwellingType" /></td>
<td valign=top><font size=2 color=3366cc><b>Dwelling Style:</b></font><br>
<jato:combobox name="cbDwellingStyle" /></td>
<td valign=top><font size=2 color=3366cc><b>Garage Size:</b></font><br>
<jato:combobox name="cbGarageSize" /></td>
<td valign=top><font size=2 color=3366cc><b>Garage Type:</b></font><br>
<jato:combobox name="cbGarageType" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b># of Units:</b></font><br>
<jato:textField name="txNoOfUnits" formatType="decimal" formatMask="###0; (-#)" size="5" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b># of Bedrooms:</b></font><br>
<jato:textField name="txNoOfBedRooms" formatType="decimal" formatMask="###0; (-#)" size="5" maxLength="2" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Living Space:</b></font><br>
<jato:textField name="txLivingSpace" formatType="decimal" formatMask="###0; (-#)" size="8" maxLength="8" /> <jato:combobox name="cbUnits" /></td>
<td valign=top colspan="3"><font size=2 color=3366cc><b>Lot Size:</b></font><br>
<jato:textField name="txLotSize" formatType="decimal" formatMask="###0; (-#)" size="8" maxLength="8" /> X 
<jato:textField name="txLotSize2" formatType="decimal" formatMask="###0; (-#)" size="8" maxLength="8" /> <jato:combobox name="cbLotSizeUnits" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Heat Type:</b></font><br>
<jato:combobox name="cbHeatType" /></td>

<td valign=top><font size=2 color=3366cc><b>Environmental Hazard:</b></font><br>
<!-- jato:combobox name="cbUFFIInsulation" / -->
<jato:radioButtons name="rbUFFIInsulation" layout="horizontal" />
</td>

<td valign=top><font size=2 color=3366cc><b>Water:</b></font><br>
<jato:combobox name="cbWaterType" /></td>
<td valign=top ><font size=2 color=3366cc><b>Sewage:</b></font><br>
<jato:combobox name="cbSewageType" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>MLS:</b></font><br>
  <font size="2">
<jato:radioButtons name="rbMLSListing" layout="horizontal" /></font></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Property Usage:</b></font><br>
<jato:combobox name="cbPropertyUsageType" /></td>
<td valign=top><font size=2 color=3366cc><b>Occupancy:</b></font><br>
<jato:combobox name="cbOccupancy" /></td>
<td valign=top><font size=2 color=3366cc><b>Property Type:</b></font><br>
<jato:combobox name="cbPropertType" extraHtml="onChange=hideOnReserveTrustAgreementNumber();" /></td>
<td valign=top><font size=2 color=3366cc><b>Property Location:</b></font><br>
<jato:combobox name="cbPropertyLocation" /></td>
<td valign=top colspan="2"><font size=2 color=3366cc><b>Zoning:</b></font><br>
<jato:combobox name="cbZoning" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Tenure:</b></font><br>
<jato:combobox name="cbTenureType" /></td>

<td valign=top ><font size=2 color=3366cc><b>MI Energy Efficiency:</b></font><br>
  <font size="2">
<jato:radioButtons name="rbMIEnergyEfficiency" layout="horizontal" /></font></td>

<td valign=top colspan="2">
<div id="divOnReservTrustAgreementNumber">
<font size=2 color=3366cc><b>On Reserve Trust Agreement #:</b></font><br>
<jato:textField name="txOnReserveTrustAgreementNumber" formatType="decimal" formatMask="###0; (-#)" size="5" maxLength="4" />
</div>
</td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Purchase Price:</b></font><br>
$ 
<jato:textField name="txPurchasePrice" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top><font size=2 color=3366cc><b>Land Value:</b></font><br>
$ 
<jato:textField name="txLandValue" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top><font size=2 color=3366cc><b>Estimated Value:</b></font><br>
$ 
<jato:textField name="txEstimatedValue" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top ><font size=2 color=3366cc><b>LTV:</b></font><br>
<jato:text name="stLTV" escape="true" formatType="decimal" formatMask="###0.00; (-#)" /></td>

<td valign=top><font size=2 color=3366cc><b>Request Appraisal:</b></font><br>
<jato:combobox name="cbRequestAppraisal" /></td>

</tr>


<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
</table>

<!--END PROPERTY INFORMATION//-->

<p>

<!--START PROPERTY EXPENSES//-->
<jato:text name="stTargetPE" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=10><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=10 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Property Expenses</b></font></td>
</tr>

<tr><td colspan=10 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<tr>
<td valign=top >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top ><font size=2 color=3366cc><b>Type:</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Description</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Period</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>Amount</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>GDS?</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>%</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>TDS?</b></font></td>
<td valign=top ><font size=2 color=3366cc><b>%</b></font></td>
<td valign=top >&nbsp;&nbsp;</td>


</tr>
<tr><td colspan=11 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=11 height="5" valign="middle"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<jato:tiledView name="RepeatedPropertyExpenses" type="mosApp.MosSystem.pgPropertyEntryRepeatedPropertyExpensesTiledView">
<tr>
<td valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=middle><jato:combobox name="cbPropertyExpenseType" /></td>
<td valign=middle>
<jato:textField name="txPropertyExpenseDesc" size="20" maxLength="80" /></td>
<td valign=middle><jato:combobox name="cbExpensePeriod" /></td>
<td valign=middle>
<jato:textField name="txExpenseAmount" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=middle><jato:combobox name="cbPropertyExpenseIncludeGDS" /></td>
<td valign=middle>
<jato:textField name="txPropertyExpenseGDSPercentage" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /></td>
<td valign=middle><jato:combobox name="cbPropertyExpenseIncludeTDS" /></td>
<td valign=middle>
<jato:textField name="txPropertyExpenseTDSPercentage" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /></td>
<td valign=middle align=right><jato:button name="btDeleteExpense" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_expense.gif" /></td>
</tr>

<tr><td colspan=11><jato:hidden name="hdPropertyExpensePropId" /><jato:hidden name="hdPropertyExpenseId" /><jato:hidden name="hdPropertyExpenseCopyId" /><jato:hidden name="hdPropertyExpenseGDSPercentage" /><jato:hidden name="hdPropertyExpenseTDSPercentage" />
		<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=11><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>


<!--END REPEATING ROW//-->

</jato:tiledView>

<tr><td colspan=11><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=11>

<table border=0 cellspacing=3 width=490>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddExpense" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_expense.gif" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Total Annual Property Expenses:</b></font>&nbsp;<jato:textField name="txTotalPropertyExp" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=11><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=11><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<!--END PROPERTY EXPENSES//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<p>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><jato:button name="btPropertySubmit" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btPropertyCancel" extraHtml="onClick =  \"if(!confirm('You are leaving the Property Entry screen. Your changes have not yet been saved. To continue without saving click \\\'OK\\\'. To return to Property Entry screen click \\\'Cancel\\\'') ) return false;  setSubmitFlag(true);\"" fireDisplayEvents="true" src="../images/cancel.gif" /><jato:button name="btPropertyCancelCurrentChanges" extraHtml="onClick =  \"if(!confirm('You are leaving the Property Entry screen. Your changes have not yet been saved. To continue without saving click \\\'OK\\\'. To return to Property Entry screen click \\\'Cancel\\\'') ) return false;  setSubmitFlag(true);\"" fireDisplayEvents="true" src="../images/cancelCurrentChanges.gif" /><jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td></tr>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<table border=0 width=414 cellpadding=0 cellspacing=0 bgcolor=99ccff>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href1"> </jato:href></font></td>
	<td rowspan=19 valign=top bgcolor="5e84cf"><A onclick="return IsSubmited();" HREF="javascript:tool_click(2);">
	<img src="../images/close_arrow.gif" width=14 height=42 alt="" border="0"></a></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href2"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href3"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href4"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href5"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href6"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href7"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href8"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href9"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<font size=2><jato:href name="Href10"> </jato:href></font></td>
<tr>
	<td><img src="../images/blue_line.gif" width=400 height=1 alt="" border="0"></td>
	<td bgcolor="5e84cf"><img src="../images/blue_line.gif" width=14 height=1 alt="" border="0"></td>
</table>
</div>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
	location.href="#loadtarget"
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</jato:form>

</BODY>

</jato:useViewBean>
</HTML>
