
<HTML>
<%@page info="pgPartyAdd" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgPartyAddViewBean">

    <HEAD>
    <TITLE>Party Add</TITLE>
    <STYLE>
    <!--
    .dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
    .toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
    .pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
    .alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
    
    -->
    
    </STYLE>
<SCRIPT LANGUAGE="JavaScript">


<%@include file="/JavaScript/SystemMessages.txt" %>

    </SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>   
 
    </HEAD>
    <body bgcolor=ffffff link=000000>
    <jato:form name="pgPartyAdd" method="post" onSubmit="return IsSubmitButton();">
    
    <SCRIPT LANGUAGE="JavaScript">
    
    var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
    var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
    var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
    var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
    var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
    var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
    var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
    var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
    pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
    pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);
    
    
    var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
    var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
    var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
    var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
    var amTitle = "<jato:text name="stAmTitle" escape="true" />";
    var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
    var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
    var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
    amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
    amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);
    
    generateAM();
    
    </SCRIPT>
    

    <jato:hidden name="sessionUserId" />
    <jato:hidden name="hdPartyId" />
    <jato:hidden name="hdPartyTypeId" />        
    <input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
    <input type="hidden" name="isFatal" value="">


    

  
	<%@include file="/JavaScript/CommonHeader_en.txt" %>  

<%@include file="/JavaScript/CommonToolbar_en.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>    

 
    <div id="pagebody" class="pagebody" name="pagebody">
    <jato:text name="stIncludeDSSstart" escape="false" />
		<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br> 
    <jato:text name="stIncludeDSSend" escape="false" />
        
    <table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff height="475">
    <td colspan=8 height="7"><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
    <td colspan=8 valign=top height="21"><font size=3 color=3366cc><b><jato:text name="stAction" escape="true" /></b></font></td><tr>
    <td colspan=8 height="21">&nbsp;</td><tr>
    
    <td valign=top colspan=2 height="32"><font size=2 color=3366cc><b>Party Type:</b></font><br>
    <font size=2><jato:combobox name="cbAddPartyType" /></font></td>
    
    <td valign=top  colspan=2 height="32"><font size=2 color=3366cc><b>Party Status:</b></font><br>
    <font size=2><jato:combobox name="cbAddPartyStatus" /></font></td>
    
    <td valign=top  colspan=2 height="32"><font size=2 color=3366cc><b><jato:text name="stPartyShortNameLabel" escape="true" /></b></font><br>
    <font size=2>
    <jato:textField name="tbAddPartyShortName" size="20" maxLength="20" /></font></td>
    
    <td valign=top  colspan=2 height="32"><font size=2 color=3366cc><b><jato:text name="stClientNumberLabel" escape="true" /></b></font><br>
    <font size=2><jato:textField name="tbAddClientNumber" size="9" maxLength="9" /></font></td>
    
    <tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	</tr>

    <tr>
      <td valign=top  colspan=4 rowspan="3">
      	<font size=2 color=3366cc><b>Special Notes:</b></font><br>
      	<font size=2><jato:textArea name="tbNotes" rows="4" cols="60" /></font>
      </td>
     
      <td valign=top  colspan=2>
      	<jato:text name="stIncludeBranchTransitInfoStart" escape="false" />  
    	<font size=2 color=3366cc><b>GE Branch#:</b></font><br>
    	<font size=2><jato:textField name="txBranchTransitNumGE" size="20" maxLength="9" /></font>
    	<jato:text name="stIncludeBranchTransitInfoEnd" escape="false" />
      </td>
      
      <td valign=top  colspan=2>
      	<jato:text name="stIncludeBranchTransitInfoStart" escape="false" />
      	<font size=2 color=3366cc><b>CMHC Branch#:</b></font><br>
    	<font size=2><jato:textField name="txBranchTransitNumCMHC" size="20" maxLength="9" /></font>
    	<jato:text name="stIncludeBranchTransitInfoEnd" escape="false" />  
      </td>
    </tr>
    
	<tr>
		<td colspan=1 valign=top>
			<jato:text name="stIncludeBankInfoStart" escape="false" />
			<font size=2 color=3366cc><b>Bank and Transit Number:</b></font><br>
        	<font size=2><jato:textField name="tbBankABABankNum" size="3" maxLength="3" />&nbsp;-&nbsp;<jato:textField name="tbBankABATransitNum" size="16" maxLength="16" /></font>
        	<jato:text name="stIncludeBankInfoEnd" escape="false" />
		</td>
		<td colspan=2 valign=top>
			<jato:text name="stIncludeBankInfoStart" escape="false" />
			<font size=2 color=3366cc><b>Bank Account Number:</b></font><br>
        	<font size=2><jato:textField name="tbBankAccountNum" size="20" maxLength="20" /></font>
        	<jato:text name="stIncludeBankInfoEnd" escape="false" />
		</td>
	</tr>
	<tr>
		<td colspan=3 valign=top>
		<jato:text name="stIncludeBankInfoStart" escape="false" />
		<font size=2 color=3366cc><b>Bank Name:</b></font><br>
        <font size=2><jato:textField name="tbBankName" size="35" maxLength="35" /></font>
        <jato:text name="stIncludeBankInfoEnd" escape="false" />
		</td>
	</tr>
	
	<tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	</tr>

    <tr>
    <td valign=top  colspan=8 height="32" nowrap><font size=2 color=3366cc><b>Party Name:</b></font><br>
    <jato:textField name="txPartyNameFirstPart" size="53" maxLength="50" />
    <jato:textField name="txPartyNameSecondPart" size="53" maxLength="50" />
    <jato:textField name="txPartyNameThirdPart" size="53" maxLength="50" />
    </td><tr>
    
    <td valign=top colspan=8 height="36"><font size=2 color=3366cc><b>Company Name:</b></font><br>
    <jato:textField name="tbAddCompanyNameDisplay" size="35" maxLength="35" /></td><tr>    
        
    <td colspan=8 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
    <td colspan=8 height="7"><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
    <td colspan=8 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
    
    

    
    <td colspan=8 valign=top height="21"><font size=3 color=3366cc><b>Contact Information</b></font></td><tr>
    <td colspan=8 height="21">&nbsp;</td><tr>
    
    <td colspan=8 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
    
    <td valign=top colspan=2 height="36"><font size="2" color="3366cc"><b>First Name:</b></font><br>
      <jato:textField name="tbAddPartyFirstName" size="20" maxLength="20" /></td>
    <td valign=top colspan=2 nowrap height="36"><font size="2" color="3366cc"><b>Middle Initial:</b></font><br>
      <jato:textField name="tbAddPartyMiddleInitial" size="1" maxLength="1" /></td>
    <td valign=top colspan=2 height="36"><font size="2" color="3366cc"><b>Last Name:</b></font><br>
      <jato:textField name="tbAddPartyLastName" size="20" maxLength="20" /></td>
    <tr>
    
    <td colspan=8 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
    
    <td valign=top colspan=2 nowrap height="74"><font size=2 color=3366cc><b>Work Phone #:</b></font><br>
    ( <jato:textField name="tbPhoneNumber1" size="3" maxLength="3" /> ) 
    <jato:textField name="tbPhoneNumber2" size="3" maxLength="3" /> - 
    <jato:textField name="tbPhoneNumber3" size="4" maxLength="4" /> <font size=2 color=3366cc><b>X</b></font>  
    <jato:textField name="tbAddWorkPhoneExtension" size="6" maxLength="6" /></font></td>
    
    <td valign=top colspan=2 height="74"><font size=2 color=3366cc><b>Fax #:</b></font><br>
    ( <jato:textField name="tbFaxNumber1" size="3" maxLength="3" /> ) 
    <jato:textField name="tbFaxNumber2" size="3" maxLength="3" /> - 
    <jato:textField name="tbFaxNumber3" size="4" maxLength="4" /></font></td>
    
    <td valign=top colspan=2 height="74"><font size=2 color=3366cc><b>E-mail Address:</b></font><br>
    	<jato:textField name="tbAddEmail" size="30" maxLength="50" fireDisplayEvents="true"/></td>
    	
    <td valign=top colspan=2 height="74"><font size=2 color=3366cc><b>Preferred Delivery Method:</b></font><br>
    	<jato:combobox name="cbPrefDeliveryMethod" /></td><tr>    	
    
    <td colspan=8 height="14"><img src="../images/light_blue.gif" width=1 height=10 alt="" border="0"></td><tr>
    <td colspan=8 height="5"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
    <td colspan=8 height="14"><img src="../images/light_blue.gif" width=1 height=10 alt="" border="0"></td><tr>
    
    <td valign=top colspan=4 height="36"><font size=2 color=3366cc><b>Address Line 1:</b></font><br>
    	<jato:textField name="tbAddAddressLine1" size="35" maxLength="35" /></td>
    
    <td valign=top colspan=4 height="36"><font size=2 color=3366cc><b>Address Line 2:</b></font><br>
    	<jato:textField name="tbAddAddressLine2" size="35" maxLength="35" /></td><tr>
    
    <td colspan=6 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
    
    <td valign=top colspan=4 height="55"><font size=2 color=3366cc><b>City:</b></font><br>
    	<jato:textField name="tbAddCity" size="20" maxLength="20" /></td>
    
    <td valign=top colspan=2 height="55"><font size=2 color=3366cc><b>Province:</b></font><br>
    	<jato:combobox name="cbAddProvince" /></td>
    
    <td valign=top colspan=2 height="55"><font size=2 color=3366cc><b>Postal Code:</b></font><br>
    	<jato:textField name="tbAddPostalCodeFSA" size="3" maxLength="3" /> 
    <jato:textField name="tbAddPostalCodeLDU" size="3" maxLength="3" /></td><tr>
    
    <td colspan=6 height="6"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
    
    <td valign=top colspan=2 height="55"><font size=2 color=3366cc><b>Location:</b></font><br>
    	<jato:textField name="tbLocation" size="50" maxLength="50" /><td><tr>
    
    
    <td colspan=8 height="8"><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td>
    
    </table>

    
    <BR><jato:hidden name="hdDummyForRowGen" />
    	
    <p>
    <table border=0 width=100%>
    <td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/submit.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/sign_cancel.gif" />&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </table>
    </center>
    </div>	
    
    

    
    <div id="toolpop" class="toolpop" name="toolpop">
    <%@include file="/JavaScript/ToolPopSection.txt" %>
    </div>
    
    <div id="dialogbar" class="dialogbar" name="dialogbar">
    <%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
    </div>
                
    </jato:form>
    
    
    <script language="javascript">
    
    if(NTCP){
    	document.dialogbar.left=55;
    	document.dialogbar.top=79;
    	document.toolpop.left=317;
    	document.toolpop.top=79;
    	document.pagebody.top=100;
    	document.alertbody.top=100;
    	document.addParty.top=100;
    	
    }
    
    
    
    if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
    	tool_click(4)
    }
    else{
    	tool_click(5)
    }
    
    if(pmGenerate=="Y")
    {
    openPMWin();
    }
    
    
    </script>
    
    
    </BODY>
    
    </jato:useViewBean>
</HTML>
