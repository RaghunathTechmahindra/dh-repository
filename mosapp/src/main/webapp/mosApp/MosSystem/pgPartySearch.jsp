
<HTML>
<%@page info="pgPartySearch" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgPartySearchViewBean">

<HEAD>
<TITLE>Party Search</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
  
</HEAD>

<body bgcolor=ffffff onKeyPress="return submitenter(event);">
<jato:form name="pgPartySearch" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">
<!--
//======================= Local JavaScript (begin) ============================
var enterKeyFlag = "";

function setFocus()
{	
	enterKeyFlag = '1';	
}

function resetFocus()
{
	enterKeyFlag = '0';
}

function submitenter(e)
{
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;

	if(keycode == 13 && enterKeyFlag == '1' )
	{
		//document.pgPartySearch.action = '../MosSystem/pgPartySearch.btSubmit_onWebEvent(btSubmit)';
		document.pgPartySearch.action += "?<%= viewBean.PAGE_NAME%>_btSubmit="; 
		document.pgPartySearch.submit();
		return false;
	}
	
	return true;
}		
//======================= Local JavaScript (end) ============================

//-->
</SCRIPT>



<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<%@include file="/JavaScript/CommonHeader_en.txt" %>  

<%@include file="/JavaScript/CommonToolbar_en.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>    
<!--HEADER//-->

<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

    <jato:text name="stIncludeDSSstart" escape="false" />
		<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br> 
    <jato:text name="stIncludeDSSend" escape="false" />

<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
</table>

<table border=0 width=100% cellpadding=3 cellspacing=0 bgcolor=d1ebff>
<td rowspan=9>&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b>Party Name:</b></font></td>
<td>
<jato:textField name="tbPartyShortName" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="25" maxLength="10" /></td><tr>
<td><font size=2 color=3366cc><b>Party Type:</b></font></td>
<td><jato:combobox name="cbPartyType" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" /></td><tr>
<td><font size=2 color=3366cc><b>Client or Branch Transit#:</b></font></td>
<td>
<jato:textField name="tbClientNum" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="25" maxLength="10" /></td><tr>
<td><font size=2 color=3366cc><b>Company:</b></font></td>
<td>
<jato:textField name="tbCompany" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="35" maxLength="35" /></td><tr>
<td><font size=2 color=3366cc><b>City:</b></font></td>
<td>
<jato:textField name="tbCity" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="20" maxLength="20" /></td><tr>
<td><font size=2 color=3366cc><b>Province:</b></font></td>
<td><jato:combobox name="cbProvince" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" /></td><tr>
<td><font size=2 color=3366cc><b>Location:</b></font></td>
<td>
<jato:textField name="tbLocation" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="25" maxLength="50" /></td><tr>
<td><font size=2 color=3366cc><b>Party Phone #:</b></font></td>
<td>( 
<jato:textField name="tbPhoneNumber1" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="3" maxLength="3" /> ) 
<jato:textField name="tbPhoneNumber2" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="3" maxLength="3" /> - 
<jato:textField name="tbPhoneNumber3" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="4" maxLength="4" /></td><tr>
<td><font size=2 color=3366cc><b>Party Fax #:</b></font></td>
<td>( 
<jato:textField name="tbFaxNumber1" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="3" maxLength="3" /> ) 
<jato:textField name="tbFaxNumber2" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="3" maxLength="3" /> - 
<jato:textField name="tbFaxNumber3" extraHtml="onFocus='setFocus();' onBlur='resetFocus();'" size="4" maxLength="4" /></td>
</table>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=3><img src="../images/light_blue.gif" width=100% height=1></td>
</table>

<p>
<table border=0 width=100%>
<tr>
<td align=left>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' default='true' onClick = 'setSubmitFlag(true);'" src="../images/submit.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0'  onClick = 'setSubmitFlag(true);'" src="../images/sign_cancel.gif" /></td>
</tr>
</table>


<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#d1ebff>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td bgcolor=3366cc colspan=8>&nbsp;&nbsp;<font size=2 color=#ccffff><b>Search
  Results :</b>&nbsp;<jato:text name="rowsDisplayed" escape="true" /></font></td><tr>

<BR>

<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgPartySearchRepeated1TiledView">
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<tr>
<td colspan=2>&nbsp;&nbsp;<jato:hidden name="hdPartyId" /><jato:hidden name="hdPartyTypeId" /><jato:hidden name="hdPartyStatusId" /></td>
<td><font size=2 color=#3366cc><b>Party Name</b></font></td>
<td><font size=2 color=#3366cc><b>Party Type</b></font></td>
<td><font size=2 color=#3366cc><b>Party Status</b></font></td>
<td><font size=2 color=#3366cc><b>Phone Number</b></font></td>
<td><font size=2 color=#3366cc><b>Fax Number</b></font></td>
<td>&nbsp;&nbsp;</td>
</tr>

<tr>
<td>&nbsp;&nbsp;</td>
<td><jato:button name="btSelectParty" extraHtml=" onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/assigndeal.gif" /></td>
<td><font size=2><jato:text name="stPartyShortName" escape="true" /></font></td>
<td><font size=2><jato:text name="stPartyType" escape="true" /></font></td>
<td><font size=2><jato:text name="stPartyStatus" escape="true" /></font></td>
<td><font size=2><jato:text name="stPhoneNumber" escape="true" /></font></td>
<td><font size=2><jato:text name="stFaxNumber" escape="true" /></font></td>
<td>&nbsp;&nbsp;</td>
</tr>

<tr>
<td colspan=2>&nbsp;&nbsp;</td>
<td><font size=2 color=#3366cc><b>Company Name</b></font></td>
<td><font size=2 color=#3366cc><b>Client or Branch Transit#</b></font></td>
<td><font size=2 color=#3366cc><b>City</b></font></td>
<td><font size=2 color=#3366cc><b>Province</b></font></td>
<td colspan=2><font size=2 color=#3366cc><b>Location</b></font></td>
</tr>

<tr>
<td colspan=2>&nbsp;&nbsp;</td>
<td><font size=2><jato:text name="stPartyCompanyName" escape="true" /></font></td>
<td><font size=2><jato:text name="stClientOrBranchTransitNum" escape="true" /></font></td>
<td><font size=2><jato:text name="stCity" escape="true" /></font></td>
<td><font size=2><jato:text name="stProvince" escape="true" /></font></td>
<td><font size=2><jato:text name="stLocation" escape="true" /></font></td>

<td align=left><jato:button name="btReview" extraHtml="width=45 height=15 alt='' border='0'  onClick = 'setSubmitFlag(true);'" src="../images/details.gif" /></td>	
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>


</jato:tiledView>

<tr>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=3 alt="" border="0"></td>
</tr>
</table>

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr>
<td align=left><jato:button name="btAddParty" extraHtml=" onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/addnewparty.gif" /></td>
<td align=right><jato:button name="btBackwardButton" extraHtml="width=84 height=19 alt='' border='0'  onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous.gif" />&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="width=64 height=19 alt='' border='0'  onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more.gif" /></td>
</tr>
<tr>
<td align=right></td>
<td align=right><br><img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();"></td>
</tr>
</table>
</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
