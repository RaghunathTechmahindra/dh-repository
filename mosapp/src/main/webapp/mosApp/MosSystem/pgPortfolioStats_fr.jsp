
<HTML>
<%@page info="pgPortfolioStats" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgPortfolioStatsViewBean">

<HEAD>
<TITLE>Statistiques du portefeuille de pr�ts</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgPortfolioStats" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
    <%@include file="/JavaScript/CommonHeader_fr.txt" %>  

<%@include file="/JavaScript/CommonToolbar_fr.txt" %>  
<!--END HEADER//-->

<div id="pagebody" class="pagebody" name="pagebody">
<center>
<!--
<form>
-->

<!-- START BODY OF PAGE//-->

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>

		<td colspan=4><img src="../images/light_blue.gif" width=100% height=3></td>
	<tr>
		<td><font size=2 color=3366cc><b>Statistiques du portefeuille de pr�ts:</b></font></td>
	</tr>
	<tr>
		<td> </td>
		<td></td>
	</tr>

	<tr>
		<td><b>Institution:</b> </td>
		<td><jato:combobox name="cbInstitutions" /> </td>
	</tr>
	<tr>
		<td> </td>
		<td></td>
	</tr>
	<tr>
		<td><b>R�gion:</b> </td>
		<td><jato:combobox name="cbRegions" /> </td>
	</tr>
	<tr>
		<td> </td>
		<td></td>
	</tr>
	<tr>
		<td><b>Succursale:</b> </td>
		<td><jato:combobox name="cbBranches" /> </td>
	</tr>
	<tr>
		<td> </td>
		<td></td>
	</tr>
	<tr>
		<td><b>Groupe:</b> </td>
		<td><jato:combobox name="cbGroups" /> </td>
	</tr>
	<tr>
		<td> </td>
		<td></td>
	</tr>
	<tr>
		<td><b>Utilisateur:</b> </td>
		<td><jato:combobox name="cbUsers" /> </td>
	</tr>
	<tr>
		<td> </td>
		<td></td>
	</tr>
	<tr>
		<td><b>Firme source:</b></td>
		<td><jato:combobox name="cbSourceFirm" /> </td>
	</tr>
	<tr>
		<td> </td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3><img src="../images/light_blue.gif" width=100% height=3></td>
	</tr>
	<tr>
		<td><font size=2 color=3366cc><b>S�lectionner p�riode d�sir�e:</b></font></td>
		<td><b>De:</b> </td><td>&nbsp;&nbsp;&nbsp;&nbsp; </td> <td><b> �:</b> </td>
	</tr>
	<tr>
		<td></td>
		<td colspan=2>Mois:<jato:combobox name="cbMonthFrom" /> Jour:<jato:textField name="tbDayFrom" size="2" maxLength="2" /> An:<jato:textField name="tbYearFrom" size="4" maxLength="4" /></td>
		<td colspan=2>Mois: <jato:combobox name="cbMonthTo" />  Jour:<jato:textField name="tbDayTo" size="2" maxLength="2" />   An:<jato:textField name="tbYearTo" size="4" maxLength="4" /></td>
	</tr>
	<tr>
		<td colspan=4><img src="../images/light_blue.gif" width=100% height=3></td><tr>
		<td colspan=4><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
<p>
<table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/cancel_fr.gif" /></td>
</table>
<P>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
</table>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#d1ebff>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td bgcolor=3366cc colspan=7>&nbsp;&nbsp;<font size=2 color=#ccffff><b>Statistiques pour l'�quipe des utilisateurs</b></font></td><tr>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td bgcolor=4FA7FF>&nbsp;&nbsp;</td>
<td bgcolor=4FA7FF>&nbsp;</td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Demandes re�ues</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Approuv�es</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>D�bours�es</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Refus�es</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Annul�es</b></font></td><tr>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;</td>
<td><font size=2>&nbsp;</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td><tr>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;</td>
<td><font size=2>&nbsp;</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td><tr>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td colspan=7 bgcolor=ffffff>&nbsp;</td><tr>

<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td>&nbsp;&nbsp;</td>
<td><font size=2 color=3366cc><b>Totaux:</b></font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td>
<td><font size=2>100000<br>$99999999.99</font></td><tr>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
</table>
<p>
<table border=0 width=100%>
<td align=right>&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" /></td>
</table>

<!-- Sample : Submit and Cancel buttons //-->
</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR><jato:text name="stDateMask" escape="true" />


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}


if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
