
<HTML>
<%@page info="pgDealProgress" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealProgressViewBean">


<HEAD>
<TITLE>Progression de la demande</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.sortpop {position: absolute; visibility:hidden; top:expression((QLE)?261:206); left:10;}
.filterpop {position:absolute; visibility:hidden; top:expression((QLE)?261:206); left:60;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}

.sorting {position:absolute; visibility:hidden; top:expression((QLE)?155:130); left:60; width:600;}
.filtering {position:absolute; visibility:hidden; top:155; left:60; width:600;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff link=000000 vlink=000000 alink=000000>
<jato:form name="pgDealProgress" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="true" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="true" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="detectAlertTasks" />
<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<input type="hidden" name="sortPreference" value="2">
<input type="hidden" name="filter0" value="N">
<input type="hidden" name="filter1" value="Y">
<input type="hidden" name="filter2" value="N">
<input type="hidden" name="filter3" value="N">
<input type="hidden" name="filter4" value="N">
<!--form-->

	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
    
    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
    
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!--BODY OF PAGE//-->

<P>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td valign=bottom align=left><font size=2><jato:text name="rowsDisplayed" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><img src="../images/white.gif" width=173 height=1></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td bgcolor=4FA7FF valign=top>&nbsp;&nbsp;</td>
<td bgcolor=4FA7FF colspan=7><font size=2 color=#003399><b></b></font></td><tr>

<td bgcolor=4FA7FF valign=top>&nbsp;&nbsp;</td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Description de la t�che:</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Statut de la t�che:</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Priorit�:</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Date/Heure due:</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Affect�e �:</b></font></td>
<td bgcolor=4FA7FF><font size=2 color=#003399><b>Mise en garde:</b></font></td>
<tr>


<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgDealProgressRepeated1TiledView">


<!--99ccff-->
<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;</td>
<td><font size=2><jato:text name="stTaskDescription" escape="true" /></font></td>
<td><font size=2><jato:text name="stTaskStatus" escape="true" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stTaskPriority" escape="true" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stTaskDueDate" fireDisplayEvents="true" escape="true" formatType="date" formatMask="fr|MMM dd yyyy hh:mm" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stAssignedTo" escape="true" />&nbsp;&nbsp;</font></td>
<td><font size=2><jato:text name="stTimeStatus" escape="true" /></font></td><tr>



</jato:tiledView>

<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</table>

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><jato:button name="btOk" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous_fr.gif" />&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2_fr.gif" /></td>

</table>

</center><br><br><br><br><br>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>
<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<div id="sortpop" name="sortpop" class="sortpop">
<!--form name="sortform"-->
<table border=0 cellpadding=3 cellspacing=0 bgcolor=99ccff>
<td colspan=2><font size=2>Trier par:</font><br></td><tr>
<td><a onclick="return IsSubmited();" href="javascript:cancelsortfilter(1)"><img src="../images/cancel_fr.gif" width=86 height=25 alt="" border="0"></a></td>
<td></td>
</table>
<!--form-->
</div>

<div id="filterpop" name="filterpop" class="filterpop">
<!--form name="filterform"-->
<table border=0 cellpadding=3 cellspacing=0 bgcolor=99ccff>
<td colspan=2><font size=2>Filtrer par:</font><br>
<font size=2></font><br>
</td><tr>
<td><a onclick="return IsSubmited();" href="javascript:cancelsortfilter(2)"><img src="../images/cancel_fr.gif" width=86 height=25 alt="" border="0"></a></td>
<td></td>
</table>
<!--form-->
</div>

<div id="sorting" name="sorting" class="sorting">
<table border=0 width=100% cellpadding=5 cellspacing=0>
<td align=center><table border=0 width=300 bgcolor=99ccff><td align=center><font size=4><br><br><br>Tri en cours...<br><br><br><br></font></td></table></td>
</table>
</div>
<div id="filtering" name="filtering" class="filtering">
<table border=0 width=100% cellpadding=5 cellspacing=0>
<td align=center><table border=0 width=300 bgcolor=99ccff><td align=center><font size=4><br><br><br>Filtrage en cours...<br><br><br><br></font></td></table></td>
</table>
</div>



<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
	document.sortpop.top=194;
	document.filterpop.top=194;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(4)
}
else{
tool_click(5);
}

if(pmGenerate=="Y")
{
openPMWin();
}


//-->
</script>

<!--END BODY//-->

<BR>

<BR>

</jato:form>
</BODY>

</jato:useViewBean>
</HTML>
