<HTML>
<%@page info="pgMWorkQueueSearch" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgMWorkQueueSearchViewBean">


<HEAD>
<TITLE>Master Work Queue Search</TITLE>

<link rel="stylesheet" type="text/css" href="../JavaScript/calendar.css" />
<script type="text/javascript" src="../JavaScript/calendar_en.js"></script>

<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.sortpop {position: absolute; visibility:hidden; top:expression((QLE)?231:206); left:10;}
.filterpop {position:absolute; visibility:hidden; top:expression((QLE)?231:206); left:60;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/windowScrollPositioning.js" type="text/javascript"></script><%--FXP30170--%> 

<SCRIPT LANGUAGE="JavaScript">
<!--
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function tool_click(n) {
  var itemtomove = (NTCP) ? document.pagebody : document.all.pagebody.style;
  var alertchange = (NTCP) ? document.alertbody:document.all.alertbody.style;
  var sortbody = (NTCP) ? document.sortpop:document.all.sortpop.style;
  if(sortbody.visibility==VISIBLE){sortbody.visibility=HIDDEN;}
  if(n==1){
    var imgchange = "tool_goto"; var hide1img="tool_prev"; var hide2img="tool_tools";
    var itemtochange = (NTCP) ? document.gotobar : document.all.gotobar.style;
    var hideother1 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
    var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
  }
  if(n==2){
    var imgchange = "tool_prev"; var hide1img="tool_goto"; var hide2img="tool_tools";
    var itemtochange = (NTCP) ? document.dialogbar : document.all.dialogbar.style;
    var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
    var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
  }
  if(n==3){
    var imgchange = "tool_tools"; var hide1img="tool_goto"; var hide2img="tool_prev";
    var itemtochange = (NTCP) ? document.toolpop : document.all.toolpop.style;
    var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
    var hideother2 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
  }
  if(n==1 || n==2 || n==3){
    if (itemtochange.visibility==VISIBLE) {
      itemtochange.visibility=HIDDEN;
      itemtomove.top = (NTCP) ? 100:((QLE)?135:110);
      changeImg(imgchange,'off');
      if(alertchange.visibility==VISIBLE){alertchange.top = (NTCP) ? 100:((QLE)?135:110);}
    } 
    else {
      itemtochange.visibility=VISIBLE;
      changeImg(imgchange,'on');
      if(n!=2){itemtomove.top=150;}
      else{itemtomove.top=290;}
      if(alertchange.visibility==VISIBLE){
        if(n!=2){alertchange.top =  150;}
        else{alertchange.top = 290;}
      }
      if(hideother1.visibility==VISIBLE){hideother1.visibility=HIDDEN; changeImg(hide1img,'off');}
      if(hideother2.visibility==VISIBLE){hideother2.visibility=HIDDEN;
changeImg(hide2img,'off');}
    }
  }
  if(n==4){
    var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
    itemtoshow.visibility=VISIBLE;
  }
  if(n==5){
    var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
    var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
    itemtoshow.visibility=VISIBLE;
    if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN;}
  }
  if(n==6){
    var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
    itemtoshow.visibility=VISIBLE;
    if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN;}
  }
}

if(document.images){
  tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on.gif";
  tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto.gif";
  tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on.gif";
  tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev.gif";
  tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on.gif";
  tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools.gif";
}
function changeImg(imgNam,onoff){
  if(document.images){
    document.images[imgNam].src = eval (imgNam+onoff+'.src');
  }
}

function showsortfilter(sfnum){
var hidetool = (NTCP)?document.toolpop:document.all.toolpop.style;
var hidegoto = (NTCP)?document.gotobar:document.all.gotobar.style;
var hidedialog = (NTCP)?document.dialogbar:document.all.dialogbar.style;
var movepage = (NTCP)?document.pagebody: document.all.pagebody.style;

var sortBox = (NTCP)?document.sortpop:document.all.sortpop.style;
var filterBox = (NTCP)?document.filterpop:document.all.filterpop.style;

hidetool.visibility=HIDDEN;changeImg('tool_tools','off');
hidegoto.visibility=HIDDEN;changeImg('tool_goto','off');
hidedialog.visibility=HIDDEN;changeImg('tool_prev','off');
movepage.top=(NTCP)?100:((QLE)?135:110);

  switch(sfnum)
  {
  case  1 : 
  {
    sortBox.visibility=VISIBLE;
    filterBox.visibility=HIDDEN;
    f_tcalHideAll();
    break;
  }
  case  2 : 
  {
    sortBox.visibility=HIDDEN;
    filterBox.visibility=VISIBLE;
    break;
  }

  }
}

function cancelsortfilter(sfnum){
var hidesortfilter;
 switch(sfnum)
 {
 case  1 : 
  {
    hidesortfilter=(NTCP)?document.sortpop:document.all.sortpop.style;
    break;
  }
 case  2:
  {
        hidesortfilter=(NTCP)?document.filterpop:document.all.filterpop.style;
        f_tcalHideAll();
        break;
    }
 }
  hidesortfilter.visibility=HIDDEN;
}

function OnTopClick(){
  self.scroll(0,0);
}

var pmWin;

function openPMWin()
{
var numberRows = pmMsgs.length;

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  var SessionID = eval("document.forms[0].elements['" + PAGE_NAME + "_sessionUserId" + "']");
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+ SessionID.value,
  'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');

  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

pmWin.focus();

var pmCont = '';

pmCont+='<ht';    //a trick to prevent to prevent ND generate its line:  NetDynamics Server. Version 5.0.0.22
pmCont+='ml><he';
pmCont+='ad><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  for(z=0;z<=numberRows-1;z++)
  {
    if(pmMsgTypes[z]==0){var pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
    if(pmMsgTypes[z]==1){var pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
    writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
    writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close.gif" width=84 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</bo'; //a trick to prevent to prevent ND generate its line:  NetDynamics Server. Version 5.0.0.22
pmCont+='dy></h';
pmCont+='tml>';

pmWin.document.write(pmCont);
pmWin.document.close();
}


function generateAM()
{
var numberRows = amMsgs.length;;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
  amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

  var writeRows='';

  for(z=0;z<=numberRows-1;z++)
  {
    if(amMsgTypes[z]==0){var amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
    if(amMsgTypes[z]==1){var amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
    writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
    writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}

//-->
</SCRIPT>

<style type="text/css">
	table.tableResult td {border-bottom: 1px solid #000000; white-space: nowrap;}
</style>


</HEAD>
<body bgcolor=ffffff link=000000 vlink=000000 alink=000000 onload="if(document.forms[0].isFatal.value!='Y'&&document.forms[0].isAlert.value!='Y'&&!document.all.paginationNavi){showsortfilter(2);}applyScrollPosition();">
<jato:form name="pgMWorkQueueSearch" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<input type="text" name="txWindowScrollPosition" id="txWindowScrollPosition" value="<%= request.getAttribute("txWindowScrollPosition") %>" style="display: none"/><%--FXP30170--%> 
<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<jato:hidden name="hdSelectedRowID" />
<jato:hidden name="hdCurrentFirstRowID" />

<!--form-->

  <!--HEADER//--> 
  <%@include file="/JavaScript/CommonHeader_en.txt" %>
    
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
    
    <!--QUICK LINK MENU BAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
      
  <!--END HEADER//-->

  <!--DEAL SUMMARY SNAPSHOT//-->
  <div id="pagebody" class="pagebody" name="pagebody">
  <%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>
  <!--END DEAL SUMMARY SNAPSHOT//-->
<!--BODY OF PAGE//-->

<table border=0 width=100% bgcolor=FFFFCC cellpadding=0 cellspacing=0>
<tr>
  <td valign=bottom align=left width=30% nowrap bgcolor=FFFFFF><a onclick="return IsSubmited();" href="javascript:showsortfilter(1);"><img src="../images/mwqs.sort.gif" alt="" border="0" style="margin-right:1px;"></a><a onclick="return IsSubmited();" href="javascript:showsortfilter(2);"><img src="../images/mwqs.search.gif" alt="" border="0" style="margin-right:1px;"></a><jato:button name="btFilterSortReset" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/mwqs.clear.search.results.gif" style="margin-right:1px;" /><jato:button name="btShowAllTasks" extraHtml="onClick='setSubmitFlag(true);'" src="../images/mwqs.show.all.tasks.gif" style="margin-right:1px;" /></td>

  <td valign=bottom align=right width=30% nowrap bgcolor=FFFFFF><span id="userToUser"><jato:button name="btUserToUserReassign" extraHtml="onClick='setSubmitFlag(true);'" src="../images/mwqs.user.to.user.reassign.gif" style="vertical-align:bottom; margin-right:1px;" /></span><img src="../images/mwqs.bottom.of.page.gif" alt="" border="0" style="vertical-align:bottom; margin-right:1px; cursor:pointer;" onclick="self.scrollTo(0, document.body.scrollHeight);" /><font size=2><b>Include hidden ?</b>&nbsp;<jato:checkbox name="chIsDisplayHidden" extraHtml="onClick='performFilterBySubmit();'" /></font></td>
</tr>
<tr>
  <td bgcolor=3366CC valign=middle colspan=2 height=25 nowrap><font size=3>&nbsp;&nbsp;<b>Displaying &nbsp; Tasks:&nbsp; <span id='displayedRows'><jato:text name="rowsDisplayed" escape="true" /></span></b></font>
  <font size=2 style="color:#000000;margin-left:50px;"><b>Sort:</b>&nbsp;<jato:text name="stSortOption" escape="true" />&nbsp;&nbsp;<b>Filter:</b>&nbsp;<jato:text name="stUserFilter" escape="true" /><img src="../images/ellipsis_no_trans.gif" width="20px" height="20px" style="position: absolute;" alt="<jato:text name="stLocationFull" escape="true" />" border="0" onClick="return IsSubmited();">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Task Status:</b>&nbsp;<jato:text name="stStatusFilter" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Task Description:</b><jato:text name="stTaskName" escape="true" />&nbsp;<img src="../images/ellipsis_no_trans.gif" style="position: absolute;" width=20 height=20 alt="<jato:text name="stTaskNameFull" escape="true" />" border="0" onClick="return IsSubmited();"></font></td>
</tr>
</table>
<table class="tableResult" border=0 width=100% bgcolor=#FFFFCC cellpadding=0 cellspacing=0>
<tr>
<td bgcolor=#FFCC33 colspan=1><font size=2><br>&nbsp;</font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Assigned To:</b></font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>UserType:</b></font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Login ID:</b></font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Source/Firm<br>Borrower:</b></font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Deal #:</b></font></td>
<!-- ========== SCR#750 begins ========== -->
<!-- by Neil on Dec/20/2004 -->
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Task Description: /<br>Priority</b></font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Task /<br>Deal Status:</b></font></td>
<!--<td bgcolor=#FFCC33 colspan=1><font size=2><b>Priority:</b></font></td>-->
<!-- ========== SCR#750 ends ========== -->
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Due Date/Time:</b></font></td>
<td bgcolor=#FFCC33 colspan=1><font size=2><b>Warning:</b></font></td><tr>
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgMWorkQueueSearchRepeated1TiledView">
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1>
  <jato:hidden name="hdWqID" /><jato:hidden name="hdUserTypeId" /><jato:hidden name="hdDealId" /><jato:hidden name="hdCopyId" /><jato:hidden name="hdApplicationId" />
  &nbsp;<jato:button name="btReassign" extraHtml="onClick = 'saveScrollPosition();setSubmitFlag(true);'" src="../images/open.gif" />&nbsp;<jato:button name="btSnapShot" extraHtml="onClick = 'saveScrollPosition();setSubmitFlag(true);'" src="../images/snapshot_2.gif" /></td>
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1><font size=2><jato:text name="stAssignedTo" escape="true" /> </font></td>
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1><font size=2><jato:text name="stUtDescription" escape="true" /> </font></td>
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1><font size=2><jato:text name="stLogin" escape="true" /> </font></td>
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1><font size=2><jato:text name="stBPSource" escape="true" />/<jato:text name="stBPSourceFirm" escape="true" />&nbsp;&nbsp;<br><jato:text name="stBorrower" escape="true" /> </font></td>
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1><font size=2><jato:text name="stDealNumber" escape="true" />&nbsp;&nbsp;
<br><jato:hidden name="hdInstitutionId" /><jato:text name="stInstitutionId" escape="true" /></font></td>
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1>
  <font size=2><jato:text name="stTaskDescription" escape="true" /></font><br>
  <font size=2><jato:text name="stTaskPriority" escape="true" />&nbsp;&nbsp;</font>
</td>
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1>
    <div style="<jato:text name="taskStatusOrHoldReasonStyle" escape="true" />">
      <font size=2><jato:text name="stTaskStatus" escape="true" /></font>
    </div>
    <font size=2><jato:text name="stDealStatus" escape="true" /></font>
</td>

<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1><font size=2><jato:text name="stTaskDueDate" fireDisplayEvents="true" escape="true" formatType="date" formatMask="MMM dd yyyy /  hh:mm" />&nbsp;&nbsp;</font></td>
<td bgcolor=<jato:text name="bgColor" escape="true" /> colspan=1><font size=2><jato:text name="stTaskWarning" escape="true" />&nbsp;&nbsp;</font></td><tr>
</jato:tiledView>

</table>


<!-- This Div acts as the repository for the scrolling table -->
<div id="scrolltable" style="position:relative; visibility: normal; borderBottom:solid #000000 2px; borderLeft:none; borderRight:none; borderTop:none;">
</div>

<input type="hidden" name="theCurRowNum" value="0"/>
<input type="hidden" name="theRowPerPage" value="0"/>

<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-top:6px;">
<col align="left" width="10%"/>
<col width="80%"/>
<col align="right" width="10%"/>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous.gif" />  &nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2.gif" /></td>
</tr>
</table>
<jato:content name="paginationNavi">
<input type="hidden" name="paginationNavi" value="true" />
</jato:content>





<table border="0" width="100%" bgcolor="#FFFFCC" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#FFCC33" colspan="3"><font size="3"><b>Outstanding Tasks By:</b></font></td>
	</tr>
	<tr>
		<td bgcolor="#FFCC33"><font size="2"><br/>&nbsp;</font></td>
		<td bgcolor="#FFCC33"><font size="2"><b>Task Name:</b></font></td>
		<td bgcolor="#FFCC33" width="50%"><font size="2"><b>Task Count:</b></font></td>
	</tr>
	<tr>
		<td colspan="3"><img src="../images/blue_line.gif" width="100%" height="1" alt="" border="0"></td>
	</tr>
	<jato:tiledView name="Repeated3" type="mosApp.MosSystem.pgMWorkQueueSearchRepeated3TiledView">
	<tr>
		<td><font size="2">&nbsp;&nbsp;</font></td>
		<td><font size="2">&nbsp;&nbsp;<jato:text name="stTaskNameLabel" escape="true" /></font></td>
		<td width="50%"><font size="2">&nbsp;&nbsp;<jato:text name="stTaskNameCount" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font></td>
	</tr>
	<tr>
		<td colspan="3"><img src="../images/blue_line.gif" width="100%" height="1" alt="" border="0"></td>
	</tr>
	</jato:tiledView>
	<tr>
		<td bgcolor="#FFCC33" colspan="3"><font size="3"><b>Outstanding Tasks By:</b></font></td>
	</tr>
	<tr>
		<td bgcolor="#FFCC33"><font size="2"><br>&nbsp;</font></td>
		<td bgcolor="#FFCC33"><font size="2"><b>Priority:</b></font></td>
		<td bgcolor="#FFCC33" width="50%"><font size="2"><b>Task Count:</b></font></td>
	</tr>
	<tr>
		<td colspan="3"><img src="../images/blue_line.gif" width="100%" height="1" alt="" border="0"></td>
	</tr>
	<jato:tiledView name="Repeated2" type="mosApp.MosSystem.pgMWorkQueueSearchRepeated2TiledView">
	<tr>
		<td><font size="2">&nbsp;&nbsp;</font></td>
		<td><font size="2">&nbsp;&nbsp;<jato:text name="stPriorityLabel" escape="true" /></font></td>
		<td width="50%"><font size="2">&nbsp;&nbsp;<jato:text name="stPriorityCount" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font></td>
	</tr>
	<tr>
		<td colspan="3"><img src="../images/blue_line.gif" width="100%" height="1" alt="" border="0"></td>
	</tr>
	</jato:tiledView>
</table>
<table border="0" width="100%">
	<tr>
		<td align="right"><jato:button name="btSubmit" extraHtml="width='86' height='25' alt='' border='0' onclick='setSubmitFlag(true);'" src="../images/ok.gif" />&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td align="right"><img src="../images/return_totop.gif" width="122" height="25" alt="" border="0" onclick="OnTopClick();"/></td>
	</tr>
</table>






<br><br><br>
</div>


<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>
<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<div id="sortpop" name="sortpop" class="sortpop">
<table border=0 cellpadding=3 cellspacing=0 bgcolor=99ccff>
<tr><td colspan=2><font size=2>Sort by:</font><br><div name="hidden"><jato:combobox name="cbSortOptions" extraHtml="onChange='if(isSortByMeunDown) performSortBySubmit();' onclick='toggleSortByMenuDownFlag();' onkeypress='return handleSortByKeyPressed()' onBlur='isSortByMeunDown=false;'" /></div></td>
<td colspan=2 valign=middle bgcolor="5e84cf">
  <a onclick="return IsSubmited();" href="javascript:cancelsortfilter(1)">
  <img src="../images/close_arrow.gif" width=14 height=30 alt="" border="0"></a>
</td>
</table>
</div>





<div id="filterpop" class="filterpop">
<table border="0" cellpadding="9" cellspacing="0" bgcolor="#99ccff">
	<col/>
	<col style="font-size:smaller;" />
	<tr>
		<td rowspan="8" valign="top" bgcolor="#5e84cf"><a onclick="return IsSubmited();" href="javascript:cancelsortfilter(2)">  <img src="../images/close_arrow.gif" width="14" height="30" alt="" border="0" /></a></td>
		<td>Search by Branch:<br/><jato:listbox name="lbMWQBranches" size="3" multiple="true" /></td>
	</tr>
	<tr>
		<td>Search by User:<br/><jato:combobox name="cbUserFilter" /></td>
	</tr>
	<tr>
		<td>Search by Task Status:<br/><jato:combobox name="cbStatusFilter" /></td>
	</tr>
	<tr>
		<td>Search by Task Name:<br/><jato:combobox name="cbTaskNameFilter" /></td>
	</tr>
	<tr>
		<td>Search by Deal Status:<br/><jato:combobox name="cbDealStatusFilter" /></td>
	</tr>
	<tr>
		<td>Search by Expiry Date:<br/>From:<jato:textField name="cbTaskExpiryDateStartFilter" size="10" maxLength="10" elementId="cbTaskExpiryDateStartFilter" style="vertical-align:middle;"/><script type="text/javascript">new tcal({'controlname':'cbTaskExpiryDateStartFilter'});</script>&nbsp;&nbsp;to:<jato:textField name="cbTaskExpiryDateEndFilter" size="10" maxLength="10" elementId="cbTaskExpiryDateEndFilter" style="vertical-align:middle;"/><script type="text/javascript">new tcal({'controlname':'cbTaskExpiryDateEndFilter'});</script></td>
	</tr>
	<tr>
		<td>Deal ID: <jato:textField name="tbDealIdFilter" extraHtml="onBlur='return isFieldValideDealNum();'" size="10" maxLength="10" /></td>
	</tr>
	<tr>
		<td align="center"><jato:button name="btFilterSubmitButton" extraHtml="width=60 height=18 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/sign_submit.gif" /></td>
	</tr>
</table>
</div>




<script language="javascript">
<!--
if(NTCP){
  document.dialogbar.left=55;
  document.dialogbar.top=79;
  document.toolpop.left=317;
  document.toolpop.top=79;
  document.pagebody.top=100;
  document.alertbody.top=100;
  document.sortpop.top=194;
  document.filterpop.top=194;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(4);
}
else{
tool_click(5);
}

if(pmGenerate=="Y")
{
openPMWin();
}

hideUserToUserButton();

//========== New method to handle auto refresh for SortBy and filter comboboxes ==============
var isSortByMeunDown = false;
function handleSortByKeyPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
//alert("The key = " + keycode);
  if (keycode == 13)
  {
    performSortBySubmit();
    return false;
  }
  return true;
}

function performSortBySubmit()
{
  setSubmitFlag(true); 
  //document.forms[0].action="../MosSystem/" + document.forms[0].name + ".btSortButton_onWebEvent(btGo)";  
  document.forms[0].action += "?<%= viewBean.PAGE_NAME%>_btSortButton=";
  if(IsSubmitButton())
   document.forms[0].submit();
}

function toggleSortByMenuDownFlag()
{
  if(isSortByMeunDown )
    isSortByMeunDown = false;
  else
    isSortByMeunDown = true;
}

//--Meger--//
// Added checkbox to display Hidden tasks or not
// By Billy 08Oct2002
function performFilterBySubmit()
{
  setSubmitFlag(true); 
  //document.forms[0].action="../MosSystem/" + document.forms[0].name + ".btFilterSubmitButton_onWebEvent(btFilterSubmitButton)";  
  document.forms[0].action += "?<%= viewBean.PAGE_NAME%>_btFilterSubmitButton=";
  if(IsSubmitButton())
    document.forms[0].submit();
}

// =================================================================================================

//-->

function hideUserToUserButton()
{
	var d = document.getElementById('displayedRows'); 
	if (d.innerHTML == "")
	{
		getHtmlElement("userToUser").style.visibility = VISIBLE;
	}
	else
	{
		getHtmlElement("userToUser").style.visibility = HIDDEN;
	}
}

</script>

<!--END BODY//-->


</jato:form>
</BODY>

</jato:useViewBean>
</HTML>
