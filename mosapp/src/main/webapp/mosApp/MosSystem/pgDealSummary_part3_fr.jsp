
<HTML>
<!-- Begin of DealSummary Part 3 include -->
<%@page info="pgDealSummary" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealSummaryViewBean">

<!-- Start - Liability Details -->
<div id="dealsumliab" class="dealsumliab" name="dealsumliab">
<center>
	<!-- Start - Borrower Summary Details-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>R�f�rence du demandeur:</b></font>
			</td>
		</tr>
	</table>

	<jato:tiledView name="RepeatedLiabDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedLiabDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top width=30%>
				<font size=2 color=3366cc><b>Nom du demandeur:</b></font><br>
				<font size=2>
					<jato:text name="stLDBorrSalutation" escape="true" /> <jato:text name="stLDBorrFirstName" escape="true" /> <jato:text name="stLDBorrMidInitial" escape="true" /> <jato:text name="stLDBorrLastName" escape="true" /> <jato:text name="stLDBorrSuffix" escape="true" />
   				</font>
			</td>
			<td valign=top width=30%>
				<font size=2 color=3366cc><b>Type de demandeur:</b></font><br>
				<font size=2><jato:text name="stLDBorrType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
			<td valign=top width=40%>
				<font size=2 color=3366cc><b>Emprunteur principal?</b></font><br>
				<font size=2><jato:text name="stLDBorrFlag" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>
		</tr>
	</table>
	<!-- End Borrower Summary Details-->
	
	<!-- Begin CreditBureau Liability Details-->
	
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<jato:text name="stBeginHideCBLiabSection" fireDisplayEvents="true" escape="true" />
		<tr><td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc colspan=8><b>�l�ments du passif pour le Bureau de cr�dit:</b></font>
			</td>
		</tr>
			
		<tr>
		    <td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>BC</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Type de passif</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Description des �l�ments du passif</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Limite</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Montant</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Remb. mensuel</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Date d'�ch�ance</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>% ABD</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>% ATD</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Pay�</b></font>
			</td>
		</tr>

	<jato:tiledView name="CreditBureauLiability" type="mosApp.MosSystem.pgDealSummaryCreditBureauLiabilityTiledView">
		<tr>
            <td valign=top nowrap><font size=2><jato:text name="stCreditBureauIndicator" fireDisplayEvents="true" escape="true" formatType="string" formatMask="fr|???" /></font></td>
			<td valign=top colspan=1 nowrap><font size=2><jato:text name="stCBType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font></td>
			<td valign=top colspan=1 nowrap><font size=2><jato:text name="stCBLiabilityDesc" escape="true" /></font></td>
			<td valign=top ><font size=2><jato:text name="stCreditBureauLiabilityLimit" escape="true" formatType="currency" formatMask="fr|#,##0.00$; -#" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBTotalAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; -#" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBMonthlyPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBLiabilityGDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBLiabilityTDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stCBLiabilityPayOffDesc" escape="true" /></font></td>
		</tr>
	</jato:tiledView>

		<tr><td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
	<jato:text name="stEndHideCBLiabSection" fireDisplayEvents="true" escape="true" />
	
<!-- End CreditBureau Liability Details-->

<!-- Begin Liability Details-->
	<jato:text name="stBeginHideLiabSection" fireDisplayEvents="true" escape="true" />
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc colspan=2><b>�l�ments du passif:</b></font>
			</td>
		</tr>
	
		<tr>
		    <td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>BC</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Type de passif</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Description des �l�ments du passif</b></font>
			</td>
			<td valign=top  colspan=1 nowrap>
				<font size=2 color=3366cc><b>Limite</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Montant</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Remb. mensuel</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Date d'�ch�ance</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>% ABD</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>% ATD</b></font>
			</td>
			<td valign=top nowrap>
				<font size=2 color=3366cc><b>Pay�</b></font>
			</td>
		</tr>

	<jato:tiledView name="Liability" type="mosApp.MosSystem.pgDealSummaryLiabilityTiledView">
		<tr>
		    <td valign=top nowrap><font size=2><jato:text name="stCreditBureauIndicator" fireDisplayEvents="true" escape="true" formatType="string" formatMask="fr|???" /></font></td>
			<td valign=top colspan=1 nowrap><font size=2><jato:text name="stLDType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font></td>
			<td valign=top colspan=1 nowrap><font size=2><jato:text name="stLDLiabilityDesc" escape="true" /></font></td>
			<td valign=top ><font size=2><jato:text name="stCreditBureauLiabilityLimit" escape="true" formatType="currency" formatMask="fr|#,##0.00$; -#" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDTotalAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; -#" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDMonthlyPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
            <td valign=top nowrap><font size=2><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDLiabilityGDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDLiabilityTDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
			<td valign=top nowrap><font size=2><jato:text name="stLDLiabilityPayOffDesc" escape="true" /></font></td>
		</tr>
	</jato:tiledView>

	<jato:text name="stEndHideLiabSection" fireDisplayEvents="true" escape="true" />
	
	<tr><td colspan=10><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
	
	</table>
<!-- End Liability Details-->

</jato:tiledView>

	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<td align=right><a onclick="return IsSubmited();" href="javascript:viewSub('main')">
			<img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a></td>
	</table>
</center>
</div>


<!--ADDITONAL ASSET INFORMATION---------------------------//-->
<div id="dealsumasst" class="dealsumasst" name="dealsumasst">
<center>
	<!-- Start - Borrower Summary Details-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>�l�ments de l�actif:</b></font>
			</td>
		</tr>
	</table>

	
	<jato:tiledView name="RepeatedAssetDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedAssetDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Nom du demandeur:</b></font><br>
				<font size=2><jato:text name="stADBborrSalutation" escape="true" /> <jato:text name="stADBFName" escape="false" /> <jato:text name="stADBMInitial" escape="true" />  <jato:text name="stADBLName" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de demandeur:</b></font><br>
				<font size=2><jato:text name="stADBType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Emprunteur principal?</b></font><br>
				<font size=2><jato:text name="stADBPBFlag" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Valeur nette du demandeur</b></font><br>
				<font size=2><jato:text name="stADBApplicationNetworthASDtls" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
		</tr>
<!--		<tr>
			<td align=center colspan=8>
				<img src="../images/blue_line.gif" width=100% height=2 alt="" border="0">
			</td>
		</tr>
-->		
	</table>
	<!-- End Borrower Summary Details-->
<jato:text name="stBeginHideAssetSection" fireDisplayEvents="true" escape="true" />

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top colspan=2><font size=3 color=3366cc colspan=2><b>Actif:</b></font></td>
		</tr>
		<tr>
			<td valign=top ><font size=2 color=3366cc><b>Type d�actif</b></font></td>
			<td valign=top ><font size=2 color=3366cc><b>Description de l�actif</b></font></td>
			<td valign=top ><font size=2 color=3366cc><b>Valeur de l�actif</b></font></td>
			<td valign=top ><font size=2 color=3366cc><b>% compris dans la valeur nette</b></font></td>
		</tr>
	
	<jato:tiledView name="RepeatedAssets" type="mosApp.MosSystem.pgDealSummaryRepeatedAssetsTiledView">
		<tr>
			<td valign=top ><font size=2><jato:text name="stADBAssetType" escape="true" formatType="string" formatMask="????????????????????" /></font></td>
			<td valign=top ><font size=2><jato:text name="stADBDescription" escape="true" /></font></td>
			<td valign=top ><font size=2><jato:text name="stADBValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (#)" /></font></td>
			<td valign=top ><font size=2><jato:text name="stADBPercentageIncInNetworthASDtls" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
		</tr>
	</jato:tiledView>
	</table>

<jato:text name="stEndHideAssetSection" fireDisplayEvents="true" escape="true" />
	<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
	<jato:text name="stBeginHideCredRefSection" fireDisplayEvents="true" escape="true" />
		<tr><td align=center colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8>
				<font size=2 color=3366cc><b>R�f�rence de cr�dit</b></font>
			</td>
		</tr>
	</table>

	<jato:tiledView name="RepeatedCreditRef" type="mosApp.MosSystem.pgDealSummaryRepeatedCreditRefTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Type de r�f�rence de cr�dit</b></font><br>
				<font size=2><jato:text name="stADCreditReferenceType" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Description de la r�f�rence</b></font><br>
				<font size=2><jato:text name="stADReferenceDesc" escape="true" /></font>
			</td>
		</tr>
		<tr>	
			<td valign=top><font size=2 color=3366cc><b>Nom de l'institution</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>Temps pass� avec la r�f�rence</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>Num�ro de compte</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>Solde actuel</b></font></td>
		</tr>
		<tr>
			<td valign=top ><font size=2><jato:text name="stADInsitutionName" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font></td>
			<td valign=top><font size=2><jato:text name="stADTimeWithReference" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???????????????" /></font></td>
			<td valign=top><font size=2><jato:text name="stADAccountNumber" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font></td>
			<td valign=top><font size=2><jato:text name="stADCurrentBalance" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td><tr>
		</tr>
		<tr>
	</jato:tiledView>
	
	<jato:text name="stEndHideCredRefSection" fireDisplayEvents="true" escape="true" />
	<tr><td align=center colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
	</table>
	

	</jato:tiledView>
	
	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<tr>
			<td align=right><a onclick="return IsSubmited();" href="javascript:viewSub('main')">
				<img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a>
			</td>
		</tr>
	</table>
</center>	
</div>


<!--ADDITONAL PROPERTY INFORMATION---------------------------//-->
<div id="dealsumprop" class="dealsumprop" name="dealsumprop">
<center>
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=6>
				<font color=3366cc><b>D�tails de la propri�t�:</b></font>
			</td>
		</tr>
	</table>
	
	<jato:tiledView name="RepeatedPropertyDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedPropertyDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>	
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Propri�t� principale?</b></font><br>
				<font size=2><jato:text name="stPDprimaryProperty" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Adresse de la propri�t�</b></font><br>
				<font size=2><jato:text name="stPDstreetNumber" /> <jato:text name="stPDStreetName" escape="true" /> <jato:text name="stPDStreetType" escape="true" /> <jato:text name="stPDStreetDirection" escape="true" /> <jato:text name="stPDAddressLine2" escape="true" /><jato:text name="stPDUnitNumber" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Ville</b></font><br>
				<font size=2><jato:text name="stPDcity" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Province</b></font><br>
				<font size=2><jato:text name="stPDprovince" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Code postal</b></font><br>
				<font size=2><jato:text name="stPDpostCode1" escape="true" /> <jato:text name="stPDpostCode2" escape="true" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top colspan=4>
				<font size=2 color=3366cc><b>Description l�gale:</b></font><br>
				<font size=2><jato:text name="stPDlegalLine1" escape="true" /><br><jato:text name="stPDlegalLine2" escape="true" /> <br><jato:text name="stPDlegalLine3" escape="true" />
				</font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Type d�habitation:</b></font><br>
				<font size=2><jato:text name="stPDwellingType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Style d�habitation:</b></font><br>
				<font size=2><jato:text name="stPDwellingStyle" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top colspan=4>
				<font size=2 color=3366cc><b>Nom du constructeur:</b></font><br>
				<font size=2><jato:text name="stPDBuilderName" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Taille du garage:</b></font><br>
				<font size=2><jato:text name="stPDGarageSize" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de garage:</b></font><br>
				<font size=2><jato:text name="stPDGarageType" escape="true" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Nouvelle construction</b></font><br>
				<font size=2><jato:text name="stPDNewConstructor" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>�ge de la structure:</b></font><br>
				<font size=2><jato:text name="stPDStructureAge" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Nbre d�unit�s:</b></font><br>
				<font size=2><jato:text name="stPDNoOfUnits" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Nbre de pi�ces:</b></font><br>
				<font size=2><jato:text name="stPDNumberOfBedrooms" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Espace habitable:</b></font><br>
				<font size=2><jato:text name="stPDLivingSpace" escape="true" formatType="decimal" formatMask="###0; (-#)" /> <jato:text name="stPDLivingSpaceUnitOfMeasure" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Taille du terrain:</b></font><br>
				<font size=2><jato:text name="stPDlotSize" escape="true" formatType="decimal" formatMask="###0; (-#)" /> <jato:text name="stPDLotSizeUnitOfMeasure" fireDisplayEvents="true" escape="true" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de chauffage:</b></font><br>
				<font size=2><jato:text name="stPDHeatType" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Risque �cologique:</b></font><br>
				<font size=2><jato:text name="stPDInsulatedWithUFFI" fireDisplayEvents="true" escape="true" formatType="string" formatMask="??????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Eau:</b></font><br>
				<font size=2><jato:text name="stPDWater" escape="true" formatType="string" formatMask="??????????" /></font>
			</td>
			<td valign=top >
				<font size=2 color=3366cc><b>Eaux us�es:</b></font><br>
				<font size=2><jato:text name="stPDSewage" escape="true" formatType="string" formatMask="??????????" /></font>
			</td>
			<td valign=top >
				<font size=2 color=3366cc><b>S.I.A:</b></font><br>
				<font size=2><jato:text name="stPDMLSFlag" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>

		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Usage de la propri�t�:</b></font><br>
				<font size=2><jato:text name="stPDPropertyUsage" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Occupation:</b></font><br>
				<font size=2><jato:text name="stPDOccupancyType" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de propri�t�:</b></font><br>
				<font size=2><jato:text name="stPDPropertyType" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Emplacement de la propri�t�:</b></font><br>
				<font size=2><jato:text name="stPDropertyLocation" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Zonage:</b></font><br>
				<font size=2><jato:text name="stPDZoning" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
		</tr>		
		
<%-- Release3.1 Apr 12, 2006 begins --%>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Tenure:</b></font><br>
				<font size=2><jato:text name="stTenure" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Efficacit� �nerg�tique pour Assurance Hypotheque:</b></font><br>
				<font size=2><jato:text name="stMiEnergyEfficiency" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Rabais de subdivision:</b></font><br>
				<font size=2><jato:text name="stSubdivisionDiscount" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Le numero de reserve pour la convetion de fiducie:</b></font><br>
				<font size=2><jato:text name="stOnReserveTrustAgreement" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
		</tr>
<%-- Release3.1 Apr 12, 2006 ends --%>
		
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Prix d�achat:</b></font><br>
				<font size=2><jato:text name="stPDPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Valeur du terrain:</b></font><br>
				<font size=2><jato:text name="stPDLandValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Avoir disponible:</b></font><br>
				<font size=2><jato:text name="stPDEquityAvailable" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Valeur estimative:</b></font><br>
				<font size=2><jato:text name="stPDEstimatedAppraisalValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Valeur r�elle de l��valuation:</b></font><br>
				<font size=2><jato:text name="stPDActualAppraisalValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (#)" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Date de l��valuation:</b></font><br>
				<font size=2><jato:text name="stPDAppraisalDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>
			</td>
			<td valign=top colspan=5>
				<font size=2 color=3366cc><b>Source de l��valuation:</b></font><br>
				<font size=2><jato:text name="stPDAppraisalSource" escape="true" formatType="string" formatMask="??????????????????????????????????" /></font>
			</td>
		</tr>

		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>�valuateur:</b></font><br>
				<font size=2><jato:text name="stPDAppraiserFistName" escape="true" formatType="string" formatMask="??????????????????????????????????" /> <jato:text name="stPDAppraiserInitial" escape="true" /> <jato:text name="stPDAppraiserLastName" escape="true" /></font>
			</td>
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Adresse de l��valuateur:</b></font><br>
				<font size=2><jato:text name="stPDAppraiserAddress" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone:</b></font><br>
				<font size=2><jato:text name="stPDAppraisalPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stPDAppPhoneExt" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur:</b></font><br>
				<font size=2><jato:text name="stPDAppraiserFax" escape="true" formatType="string" formatMask="(???)???-????" /></font>
			</td>
		</tr>
		<tr>
			<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td>
		</tr>
	</table>

</jato:tiledView>


	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<td align=right><a onclick="return IsSubmited();" href="javascript:viewSub('main')">
			<img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a>
		</td>
	</table>
</center>	
</div>

<!-- End Of Property Details -->

<!-- Begin Of Property Expenses Details-->
<div id="dealsumpropexp" class="dealsumpropexp" name="dealsumpropexp">
<center>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=3>
				<font color=3366cc><b>D�tails des d�penses:</b></font>
			</td>
		</tr>
	</table>
	
	<jato:tiledView name="RepeatedPropertyAddressAndExpenses" type="mosApp.MosSystem.pgDealSummaryRepeatedPropertyAddressAndExpensesTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>	
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Propri�t� principale?</b></font><br>
				<font size=2><jato:text name="stEXPrimaryProperty" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
			</td>
		</tr>
	</table>
	
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Adresse de la propri�t�</b></font><br>
				<font size=2><jato:text name="stEXStreetNumber" escape="true" /> <jato:text name="stEXStreetName" escape="true" /> <jato:text name="stExStreetType" escape="true" /> <jato:text name="stExStreetDirection" escape="true" /> <jato:text name="stExAddressLine2" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Ville</b></font><br>
				<font size=2><jato:text name="stEXCity" escape="true" formatType="string" formatMask="????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Province</b></font><br>
				<font size=2><jato:text name="stEXProvince" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Code postal</b></font><br>
				<font size=2><jato:text name="stEXPosCode1" escape="true" /> <jato:text name="stEXPosCode2" escape="true" /></font>
			</td>
		</tr>
	</table>
<!-- End Property Summary Details-->
	
<!-- Begin Of Property Expenses Details from Part 2-->
<jato:text name="stBeginHidePropExpensesSection" fireDisplayEvents="true" escape="true" />
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de d�penses</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;Description des d�penses</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;P�riode des d�penses</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;Montant des d�penses</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp; % compris dans l�ABD</b></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp; % compris dans l�ATD</b></font>
			</td>
		</tr>

	<jato:tiledView name="RepeatedPropertyExpenses" type="mosApp.MosSystem.pgDealSummaryRepeatedPropertyExpensesTiledView">
		<tr>
			<td valign=top ><font size=2><jato:text name="stExpenseType" escape="true" formatType="string" formatMask="????????????????????" /></font></td>
			<td valign=top ><font size=2><jato:text name="stExpenseDescription" escape="true" /></font></td>
			<td valign=top ><font size=2><jato:text name="stExpensePeriod" escape="true" formatType="string" formatMask="????????????????????" /></font></td>
			<td valign=top ><font size=2><jato:text name="stExpenseAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
			<td valign=top ><font size=2><jato:text name="stIncludeGDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
			<td valign=top ><font size=2><jato:text name="stIncludeTDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
		</tr>
	</jato:tiledView>
	</table>
<jato:text name="stEndHidePropExpensesSection" fireDisplayEvents="true" escape="true" />
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=2 alt="" border="0">
			</td>
		</tr>
	</table>

</jato:tiledView>

	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<td align=right><a onclick="return IsSubmited();" href="javascript:viewSub('main')">
			<img src="../images/ok.gif" width=86 height=25 alt="" border="0"></a></td>
	</table>
</center>
</div>
<!-- End Of Property Expenses -->
</jato:useViewBean>
<!-- End of DealSummary Part 3 include -->
</HTML>