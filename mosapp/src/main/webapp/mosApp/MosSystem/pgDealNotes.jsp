
<HTML>
<%@page info="pgDealNotes" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealNotesViewBean">

<!--
11/Sep/2006 DVG #DG502 #4070  
  <%/*22 Oct,2008 MCM Impl Team - BugFix :FXP22794 changing font size and font face to 1 and Lucida Console respectively */%>
-->

<HEAD>
<TITLE>Deal Notes</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->

</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>


<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgDealNotes" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array("<jato:text name="stPmMsgTypes" escape="true" />");
pmMsgs = new Array("<jato:text name="stPmMsgs" escape="false" />");


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="false" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>
<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<!--HEADER//-->
	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
    
     <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %>  

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!--BODY OF PAGE//-->

<P>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>
<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>
	<td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
<tr>
	<td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
<tr>
	<td>&nbsp;&nbsp;</td>
	
	<td valign=top><font size=2 color=3366cc><b><nobr>Note Category:&nbsp;&nbsp;</nobr></b></font></td>
	<td><jato:combobox name="cbNoteCategory" /></td>
<tr>
	<td>&nbsp;&nbsp;</td>
	<td valign=top><font size=2 color=3366cc><b>Note:</b></font></td>
	<td>
	  <!-- #DG502 prevent 'bad' chars --> 
    <jato:textArea name="tbNoteText" rows="4" cols="100" 
      extraHtml="wrap=soft maxlength=249 onchange='return isFieldHasSpecialChar();'"/>
  </td>
<tr>
	<td colspan=3><img src="../images/light_blue.gif" width=100% height=4 alt="" border="0"></td>
<tr>
</table>

<P>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" />&nbsp;&nbsp;<jato:button name="btSubmitAndExit" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/SaveAndExit.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/Exit.gif" />&nbsp;&nbsp;<jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>

<p>

<!--Deal Notes table-->
 <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
	<td bgcolor=3366cc colspan=35>&nbsp;&nbsp;<font size=2 color=#ccffff><b>EXISTING DEAL NOTES</b>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="stRowsDisplayed" escape="true" /></font></td>
  <tr>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;<font size=2 color=3366cc><b>Note Date:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>User Name:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Note Category:</b></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=2 color=3366cc><b>Note:</b></font></td>
  </tr>	
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgDealNotesRepeated1TiledView">
  <tr>
	<td colspan=35>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
  <%/*BugFix :FXP22794 changing font size and font face to 1 and Lucida Console respectively */%>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;<font size=1 face='Lucida Console'><jato:text name="stNoteDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=1 face='Lucida Console'><jato:text name="stUserName" escape="true" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=5 NOWRAP><font size=1 face='Lucida Console'><jato:text name="stNoteCategory" escape="true" /></font></td>
	<td colspan=5 NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td colspan=5><font size=1 face='Lucida Console'><jato:text name="stNoteText" escape="false" /></font></td><!-- Catherine: #2475 -->
  </tr>

</jato:tiledView>
  <tr>
	<td colspan=35><img src="../images/dark_bl.gif" width=100% height=4 alt="" border="0"></td>
  </tr>
</table>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0> <!--Forward and back buttons table-->
		<tr>
			<td align=right><jato:button name="btBackwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/previous.gif" />&nbsp;&nbsp;<jato:button name="btForwardButton" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/more2.gif" /></td>

</table>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><img src="../images/return_totop.gif" width=122 height=25 alt="" border="0" onClick="OnTopClick();"></td><tr>
</table>


</center>
<br><br><br><br><br>
</div>


<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopWithNoteSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>