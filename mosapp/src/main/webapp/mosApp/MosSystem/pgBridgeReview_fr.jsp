
<HTML>
<%@page info="pgBridgeReview" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgBridgeReviewViewBean">

<HEAD>
<TITLE>BridgeReview</TITLE>
<STYLE TYPE="text/css">
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
H1 {color:blue}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">

<!--
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function tool_click(n) {
	var itemtomove = (NTCP) ? document.pagebody : document.all.pagebody.style;
	var alertchange = (NTCP) ? document.alertbody:document.all.alertbody.style;
	if(n==1){
		var imgchange = "tool_goto"; var hide1img="tool_prev"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.gotobar : document.all.gotobar.style;
		var hideother1 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==2){
		var imgchange = "tool_prev"; var hide1img="tool_goto"; var hide2img="tool_tools";
		var itemtochange = (NTCP) ? document.dialogbar : document.all.dialogbar.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.toolpop:document.all.toolpop.style;
	}
	if(n==3){
		var imgchange = "tool_tools"; var hide1img="tool_goto"; var hide2img="tool_prev";
		var itemtochange = (NTCP) ? document.toolpop : document.all.toolpop.style;
		var hideother1 = (NTCP) ? document.gotobar:document.all.gotobar.style;
		var hideother2 = (NTCP) ? document.dialogbar:document.all.dialogbar.style;
	}
	if(n==1 || n==2 || n==3){
		if (itemtochange.visibility==VISIBLE) {
			itemtochange.visibility=HIDDEN;
			itemtomove.top = (NTCP) ? 100:110;
			changeImg(imgchange,'off');
			if(alertchange.visibility==VISIBLE){alertchange.top = (NTCP) ? 100:110;}
		} 
		else {
			itemtochange.visibility=VISIBLE;
			changeImg(imgchange,'on');
			if(n!=2){itemtomove.top=150;}
			else{itemtomove.top=290;}
			if(alertchange.visibility==VISIBLE){
				if(n!=2){alertchange.top =  150;}
				else{alertchange.top = 290;}
			}
			if(hideother1.visibility==VISIBLE){hideother1.visibility=HIDDEN; changeImg(hide1img,'off');}
			if(hideother2.visibility==VISIBLE){hideother2.visibility=HIDDEN; changeImg(hide2img,'off');}
		}
	}
	if(n==4){
		var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
		itemtoshow.visibility=VISIBLE;
	}
	if(n==5){
		var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
		var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
		itemtoshow.visibility=VISIBLE;
		if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
	}
	if(n==6){
		var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
		itemtoshow.visibility=VISIBLE;
		if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
	}
}

if(document.images){
	tool_gotoon =new Image(); tool_gotoon.src="../images/tool_goto_on.gif";
	tool_gotooff =new Image(); tool_gotooff.src="../images/tool_goto.gif";
	tool_prevon =new Image(); tool_prevon.src="../images/tool_prev_on_fr.gif";
	tool_prevoff =new Image(); tool_prevoff.src="../images/tool_prev_fr.gif";
	tool_toolson =new Image(); tool_toolson.src="../images/tool_tools_on_fr.gif";
	tool_toolsoff =new Image(); tool_toolsoff.src="../images/tool_tools_fr.gif";
}
function changeImg(imgNam,onoff){
	if(document.images){
		document.images[imgNam].src = eval (imgNam+onoff+'.src');
	}
}

function showsortfilter(sfnum){
var hidetool = (NTCP)?document.toolpop:document.all.toolpop.style;
var hidegoto = (NTCP)?document.gotobar:document.all.gotobar.style;
var hidedialog = (NTCP)?document.dialogbar:document.all.dialogbar.style;
var movepage = (NTCP)?document.pagebody: document.all.pagebody.style;

hidetool.visibility=HIDDEN;changeImg('tool_tools','off');
hidegoto.visibility=HIDDEN;changeImg('tool_goto','off');
hidedialog.visibility=HIDDEN;changeImg('tool_prev','off');
movepage.top=(NTCP)?100:110;

}

function OnTopClick(){
	self.scroll(0,0);
}

var pmWin;
var pmCont = '';

function openPMWin()
{
var numberRows = pmMsgs.length;

pmCont+='<head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  for(z=0;z<=numberRows-1;z++)
  {
	  if(pmMsgTypes[z]==0){var pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
	  if(pmMsgTypes[z]==1){var pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
	  writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
	  writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close_fr.gif" width=63 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].sessionUserId.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');
    
  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
	pmWin.document.open();
	pmWin.document.write(pmCont);
	pmWin.document.close();
	pmWin.focus();
}


function generateAM()
{
var numberRows = amMsgs.length;;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
	amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	var writeRows='';

	for(z=0;z<=numberRows-1;z++)
  {
		if(amMsgTypes[z]==0){var amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
		if(amMsgTypes[z]==1){var amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
		writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
		writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}

// Include Common Functions
function OpenCalculatorBrowser()
{
	var winCalc = window.open('/tools/menu.html','Calculator_Document','resizable,scrollbars,status=yes,titlebar=yes,width=500,height=550,screenY=250,screenX=0');
	winCalc.focus();
	
}


function OpenHelpBrowser()
{


	var winHelp = window.open('/MosHelp/index.htm','Help_Document','resizable,scrollbars,status=yes,titlebar=yes,width=500,height=550,screenY=250,screenX=0');
	winHelp.focus();
	
}



// Include Common Goto Functions
// ------------------------------------ Functions to handle Goto (begin) --------------------------------
// Functions for handleing the Goto function on the ToolBar
//  - Jump to the selected page if changed by Mouse click
//  - Also handle keyboard if user pressed 'Enter' on the combo box
// ------------------------------------------------------------------------------------------------------
var isGotoMeunDown = false;

function handleGotoKetPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
//alert("The key = " + keycode);
  if (keycode == 13)
  {
    performGoto();
    return false;
  }

  return true;
}

function performGoto()
{
	setSubmitFlag(true);
	document.forms[0].action="../MosSystem/" + document.forms[0].name + ".btProceed_onWebEvent(btProceed)";
	//alert("document.forms[0].action= " + document.forms[0].action);
  // check if any outstanding request
  if(IsSubmitButton())
	  document.forms[0].submit();
}

function toggleMenuDownFlag()
{
  // For testing -- BILLY 01Apr2002
  //var theField =(NTCP) ? event.target : event.srcElement;

	if(isGotoMeunDown)
  {
		//theField.size = 0;
    isGotoMeunDown = false;
  }
	else
  {
		//theField.size = 20;
    isGotoMeunDown = true;
  }
}

// Added the Validation functions for Deal # validation
function isDigitGoto(inp)
{
	if(inp == '0' || inp == '1' || inp == '2'  || inp == '3' || inp == '4' ||
   	   inp == '5' || inp == '6' || inp == '7'  || inp == '8' || inp == '9')
	{
	return true;
	}

	return false;
}

// Function to check if the input value is an Integer
function isIntegerGoto(inValue)
{
	for(var i=0;i<inValue.length;i++)
	{
		if(  isDigitGoto(inValue.charAt(i)) == false )
			return false;
	}
	return true;
}

// Function to check if the attached field value is an Integer with requestLength
//		Note : if requestLength = 0 means any length
function isFieldValideDealNum()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

  var requestLength = 0;

	if( !isIntegerGoto(theFieldValue) )
	{
		alert(DEAL_NUM_NUMERIC_ONLY);
		var tmp = theField.value;
		theField.focus();
		theField.select();
		return false;
	}
	else
	{
		// Check Length
		if( requestLength > 0 )
		{
			if( theFieldValue.length != requestLength )
			{
				alert(printf(INPUT_NUM_DIGITS, requestLength));
				theField.focus();
				theField.select();
				return false;
			}
		}
	}
	return true;
}

// Also included the default button stuffes here
var isSubmitFlag = true;
var isSubmited = false;

function setSubmitFlag(flag)
{
	isSubmitFlag = flag;
}

function setSubmitedFlag(flag)
{
	isSubmited = flag;
}

function IsSubmitButton()
{
  if(!isSubmited && isSubmitFlag)
  {
    // Set the flag to prevent submit again
    isSubmited = true;
	  return(true);
  }
  else
  {
    if(isSubmited)
      alert(NO_SECOND_SELECTION);
    return(false);
  }
}

function IsSubmited()
{
  if(isSubmited)
  {
    alert(NO_SECOND_SELECTION);
    return(false);
  }
  else
  {
    return(true);
  }
}

// ------------------------------------ Functions to handle Goto (end) --------------------------------

 

//-->
</SCRIPT>
</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgBridgeReview" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<!--
</form>
-->
<p>
<center>
<!--HEADER//-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor="003366">
	<td><table border=0 width=100% cellpadding=0 cellspacing=0><td valign=top width=5%>
				<img src="../images/bx_logo_fr.gif" width=131 height=29 alt="" border="0"></td>
				<td valign=middle><font size=4 color=ffcc33><jato:text name="stPageLabel" escape="true" /></font></td>
				<td align=right valign=middle><font color=ffffff>
					<font size=3><b><jato:text name="stCompanyName" escape="true" /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
					</font></td>
			<tr>
				<td colspan=3 valign=top>
					<img src="../images/top_line.gif" width=100% height=2 alt="" border="0"></td>
			</table></td>
	<td align=right background="../images/top_spacer.gif" rowspan=2 width=5%>

<script language="javascript">

	
			var detectAlertTasks = document.forms[0].detectAlertTasks.value;
			var sessionUserId = document.forms[0].sessionUserId.value;
						
// original value
//            lenderArray = new Array ("'Select Lender'","'cbLender 1','L1'","'cbLender 2','L2'");
            
            lenderArray = new Array ("'Select Rate'","'Rate 1','R1'","'Rate 2','R2'");              
            productArray =  new Array("'Select Product'","'cbProduct 1','P1'","'cbProduct 2','P2'","'cbProduct 3','P3'","'cbProduct 4','P4'");
            rateArray =  new Array("'Select Rate'","'cbPostedInterestRate 1','R1'","'cbPostedInterestRate 2','R2'");
            rateCodeArray = new Array(); 
            paymentTermArray = new Array();
            productIdArray = new Array();  
					
			loadApplet();

			function setalertflag(x)
			{
				detectAlertTasks = x;
				document.forms[0].detectAlertTasks.value = detectAlertTasks;
			}

			function clickWorkQueue(){
				location.href="workqueue.html"
			}
						
			function loadApplet(){
				detectAlertTasks = document.forms[0].detectAlertTasks.value;
				document.open();
				document.write('<APPLET CODEBASE="/eNet_applets/" CODE="mos.applets.ATNApplet" NAME="ATN" WIDTH=66 HEIGHT=49 MAYSCRIPT>' +
					'<PARAM NAME="detectAlertTasks" VALUE="' + detectAlertTasks + '">'+
					'<PARAM NAME="servletServer" VALUE="http://localhost">' + 
					'<PARAM NAME="servletServerPort" VALUE="8000">' + 
					'<PARAM NAME="turnOnLenderProductRate" VALUE="Y">' + 					
					'<PARAM NAME="applicationDate" VALUE="<jato:text name="stAppDateForServlet" escape="true" />">' + 
					'<PARAM NAME="rateFormat" VALUE="L">' + 				
					'<PARAM NAME="sessionUserId" VALUE="'+ sessionUserId +'"></APPLET>'   ) ;
				document.close();
			}
		

			function initLenderProductProfile()
			{
			  populateLenderList(); 
			  
			  if(<jato:text name="stLenderProfileId" fireDisplayEvents="true" escape="false" /> >= 0)
              	var existedLenderId=<jato:text name="stLenderProfileId" fireDisplayEvents="true" escape="false" />;
              else
              	var existedLenderId=0;
              
			  for ( i = 0 ;  i <=  document.forms[0].cbLender.options.length -1 ; i ++  )
			  {
			  	if ( document.forms[0].cbLender.options[i].value == existedLenderId  ) 
					break; // found, quit. 												
			  }
			  
			  if (i < document.forms[0].cbLender.options.length) 
			  	document.forms[0].cbLender.selectedIndex = i ; 							  
			  else
			  	return; // no match on lender - don't attempt further population	
			 	
			  	
			  getListInitial("cbProduct", existedLenderId);
			  
			  if (productArray.length == 0)
			  	return;
			  
			  tempProductId = -1 ;			 
			  if ( document.forms[0].stLenderProductId != null ) 		// set default ProductId
			  {
				for ( i = 0 ; i <= document.forms[0].cbProduct.options.length - 1 ; i ++ )
				{
					tempProductId = document.forms[0].cbProduct.options[i].value ;  // get ProductId 
					if  ( tempProductId ==  document.forms[0].stLenderProductId.value  )   
						break ; 
				}
				
			  }
			  
			  if (tempProductId == -1)
			  {
			  	i = 0;
			  	tempProductId = document.forms[0].cbProduct.options[0].value;
			  }
			  
			  document.forms[0].cbProduct.selectedIndex = i; 
			  
			  getListInitial("cbPostedInterestRate", tempProductId);
			
			  tempRate = "";
			  if ( document.forms[0].stRateTimeStamp != null ) 		// check existed postedInterestRate 
			  {	
			    selectedRateId = 0 ; 
				for ( i = 0 ; i <= document.forms[0].cbPostedInterestRate.options.length - 1 ; i ++ )
				{
					tempRate = document.forms[0].cbPostedInterestRate.options[i].value ;
					tempRate = tempRate.substring ( tempRate.length - 32 , tempRate.length )  ; 	
					if  ( tempRate ==  document.forms[0].stRateTimeStamp.value  )   						
					{
						selectedRateId = i ; 
						break ; 
					}
				}
				if (document.forms[0].cbPostedInterestRate.options.length > 0)
					document.forms[0].cbPostedInterestRate.selectedIndex = selectedRateId ; 
			  }	
			}
			  
			function getLenderProfiles() {
				temp =  document.ATN.getLenderProfiles(); 							
				lenderArray  = new Array ; 
				string2Array ( temp , lenderArray ); 					
			  }		
			  					  			  
			function getLenderProducts( lenderId ) {
				temp =  document.ATN.getLenderProducts( lenderId )  ;  // call the Applet 												
				productArray = new Array ; 
				string2Array ( temp , productArray ); 					
			  }						  

			function getPaymentTerms ( ) {
				temp =  document.ATN.getPaymentTerms( )  ;  // call the Applet 												
				tempPaymentTermArray = new Array ; 
				string2Array ( temp , tempPaymentTermArray  ); 					
				
				for ( i = 0 ; i <= tempPaymentTermArray.length -1 ; i ++ )
				{
					tempItem = tempPaymentTermArray [i]; 
					sepPos =  tempItem.indexOf ( "," ) ; 
					if ( sepPos > 0 ) 
					{
						paymentTermArray[i] = tempItem.substring(1, sepPos-1 );
						productIdArray[i] = tempItem.substring( sepPos+2 , tempItem.length -1 ) ;
					}					
				}		
				
			  }			
			  				  			  			 
			function getRateCodes( productId) 
			{	
				lenderId = document.forms[0].cbLender.options [ document.forms[0].cbLender.selectedIndex ].value ;
				temp = document.ATN.getRateCodes (lenderId, productId)  ;				
				rateCodeArray = new Array;
				string2Array( temp, rateCodeArray); 

				for ( i=0 ; i <= rateCodeArray.length -1 ; i ++ )
				{
					tempRateCode = rateCodeArray[i] ;		
					pos = tempRateCode.indexOf ( "," ) ; 
					if ( pos > 0 )
						rateCodeArray[i] = tempRateCode.substring ( 1, pos-1  ) ;		
				}
			 }			 			 
			 
			function getLenderProductRates( productId ) 
			{
				lenderId = document.forms[0].cbLender.options [ document.forms[0].cbLender.selectedIndex ].value ;
				temp = document.ATN.getLenderProductRates(lenderId, productId)  ;				
				rateArray = new Array;
				string2Array( temp, rateArray); 

			 }
			 			 
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
			// SourceString Format: 
			//	  'productName', 'id' ? 'productName', 'id' ?  
			
			function string2Array( sourceStr, destArray ) 
			{	
				i = sourceStr.indexOf ( "?" );
				itemCounter = 0; 
				aitem = ""; 
				aitem = sourceStr.substring( 1, 2); 
				while ( i > 0 )
				{
					aitem = sourceStr.substring ( 0,  i  ); 
					destArray[ itemCounter ] = aitem; 
					sourceStr = sourceStr.substring( i + 1 ); 
					i = sourceStr.indexOf( "?" ) ; 	
					itemCounter ++  ; 				
				}
			}			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function populateLenderList() {
	getLenderProfiles() ; // populate the LenderArray 	

	while ( lenderArray.length < document.forms[0].cbLender.options.length ) {
		document.forms[0].cbLender.options[ (document.forms[0].cbLender.options.lenght - 1 ) ] = null ;
	}
	for ( var i=0; i< lenderArray.length; i++) {		
		eval("document.forms[0].cbLender.options[i]=" + "new Option(" + lenderArray[i] + ")" );
	}
	document.forms[0].cbProduct.options[0]=null; 
}

function populateProduct(){

	while (productArray.length < document.forms[0].cbProduct.options.length) {
		document.forms[0].cbProduct.options[(document.forms[0].cbProduct.options.length - 1)] = null;
	}
	for (var i=0; i < productArray.length; i++) {
		eval("document.forms[0].cbProduct.options[i]=" + "new Option(" + productArray[i]+")");
	}

	if ( document.forms[0].cbProduct.options.length > 0 ) 
	{
		getPaymentTerms(); 
		populatePaymentTerm() ; 	
	}else {
		document.forms[0].tbPaymentTermDescription.value = "" ; 
	}	

}

function showRate(){
	document.open ;
	document.write (  " Rate = " + document.forms[0].cbPostedInterestRate.options[  document.forms[0].cbPostedInterestRate.selectedIndex  ].value  ) ;
	document.close;
}

function populateRate() {  	

	while (0 < document.forms[0].cbPostedInterestRate.options.length) {
		document.forms[0].cbPostedInterestRate.options[(document.forms[0].cbPostedInterestRate.options.length - 1)] = null;
	}
	for (var i=0; i < rateArray.length; i++) {
		eval("document.forms[0].cbPostedInterestRate.options[i]=" + "new Option(" + rateArray[i]+")");
	}

}

function populateRateCode( ) {
	id = document.forms[0].cbPostedInterestRate.selectedIndex ;
	
	if ( ( id == -1 ) ||  ( rateCodeArray.length <=0)  ) {
		document.forms[0].tbRateCode.value = "";
		document.forms[0].hdLongPostedDate.value = ""; 
    }		
	else {
		document.forms[0].tbRateCode.value = rateCodeArray[ id ];
	    document.forms[0].hdLongPostedDate.value = document.forms[0].cbPostedInterestRate.options[ id ].value ; 
 
    }		
}


function populatePaymentTerm( ) {
	tempId = document.forms[0].cbProduct.selectedIndex ; 

	if ( tempId == null )
		return ; 
	if ( tempId ==  -1 )
		tempId = 0 ; 

	pId = document.forms[0].cbProduct.options[ tempId ].value ;  

	for ( id= -1,   i = 0 ; i <= productIdArray.length -1 ; i ++ )
	{
		if ( productIdArray[i] == pId  )
		{  	id = i ;
			break ;
		}
	}			
	if ( id == -1  )	
		document.forms[0].tbPaymentTermDescription.value = ""; 
	else
		document.forms[0].tbPaymentTermDescription.value = paymentTermArray[id]; 
}

function getListInitial(whichtype, id)
{
	if(whichtype=="cbProduct")
	{		
		getLenderProducts( id ) ;
		populateProduct(); 

		return;
	}
	
	if(whichtype=="cbPostedInterestRate")
	{
		if (id == -1)
		{
			rateArray = new Array ;
			populateRate();   
			populateRateCode(); 
		}
			
		else
		{
			getLenderProductRates( id ) ;		
			populateRate();   
			
			getRateCodes( id);
			populateRateCode( ) ; 				
			

			getPaymentTerms();
			populatePaymentTerm( ) ; 
		}	
		
		return;
	}
}

function getList(whichtype){

	var afterProduct="false"; 
	if(whichtype=="cbProduct")
	{		
		lenderId = document.forms[0].cbLender.options [ document.forms[0].cbLender.selectedIndex ].value ;
		
		getLenderProducts( lenderId ) ; 	// Change the products 
		populateProduct(); 

		afterProduct="true"; 
	}
	if ( productArray.length == 0 )
	{
		rateArray = new Array ;
		populateRate();   
		populateRateCode(); 
	}
		
	else if (  (whichtype=="cbPostedInterestRate")  || ( afterProduct=="true")   )  {
		
		if ( afterProduct != "true" ) 
			productId = document.forms[0].cbProduct.options [ document.forms[0].cbProduct.selectedIndex ].value ;		
		else if ( productArray.length > 0 ) 
				productId = document.forms[0].cbProduct.options [ 0 ].value ;		
				
		getLenderProductRates( productId ) ;		
		populateRate();   
		
		getRateCodes(productId);
		populateRateCode( ) ; 				
		
		populatePaymentTerm( ) ; 
	}	
		
}

</script>
</td>			
								
<tr>
	<td bgcolor=ffffff align=center valign=top>
		<table border=0 width=100% cellpadding=0 cellspacing=0>
			<td><font size=2 color=003366>&nbsp;&nbsp;<jato:text name="stTodayDate" escape="true" /></font></td>
			<td align=left><font size=3 color=003366><b><jato:text name="stUserNameTitle" escape="true" /></b></font></td>
		</table></td>
</table>

    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
<center>
<jato:text name="stIncludeDSSstart" escape="true" />
<jato:text name="stIncludeDSSend" escape="true" />

<!--END DEAL SUMMARY SNAPSHOT//-->
<!--START OF BODY//--->

<BR><input type="hidden" name="stLenderProductId" value ="<jato:text name="stLenderProductId" fireDisplayEvents="true" escape="true" />">
<BR><input type="hidden" name="stRateTimeStamp" value ="<jato:text name="stRateTimeStamp" fireDisplayEvents="true" escape="true" />">

<jato:hidden name="hdLongPostedDate" />

<center>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=4 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;D�tails du pr�t-relais:</b></font></td><tr>
<td colspan=4>&nbsp;</td><tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<tr>
<td valign=top colspan=3><font size=2 color=3366cc><b>But du pr�t-relais:</b></font><br><jato:combobox name="cbBridgePurpose" /></td>
<td valign=top colspan=3><font size=2 color=3366cc><b>Pr�teur:</b></font><br>
<jato:combobox name="cbLender" /><td valign=top colspan=4><font size=2 color=3366cc><b>Produit:</b></font><br>
<jato:combobox name="cbProduct" /><td valign=top colspan=3><font size=2 color=3366cc><b>Description du terme du paiement:</b></font><br>
<jato:textField name="tbPaymentTermDescription" size="20" maxLength="20" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Code taux:</b></font><br>
<jato:textField name="tbRateCode" size="5" maxLength="5" /></td>
</tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<tr>
<td valign=top colspan=6><font size=2 color=3366cc><b>Taux d'int�r�t affich�:</b></font><br>
<jato:combobox name="cbPostedInterestRate" /><td valign=top colspan=3><font size=2 color=3366cc><b>Discount:</b></font><br>
<jato:textField name="tbDiscount" size="20" maxLength="5" /></font></td>
<td valign=top colspan=3><font size=2 color=3366cc><b>Prime:</b></font><br>
<jato:textField name="tbPremium" size="20" maxLength="5" /></td>
<td valign=top colspan=2 ><font size=2 color=3366cc><b>Taux net:</b></font><br><jato:text name="stNetRate" fireDisplayEvents="true" escape="false" /></td>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<tr>
<td valign=top colspan=3><font size=2 color=3366cc><b>Montant du pr�t-relais:</b></font><br><font size=2 color=3366cc><b>$</font></b><jato:textField name="tbBridgeLoanAmount" size="20" maxLength="13" /></td>
<td valign=top colspan=6><font size=2 color=3366cc><b>Date d'�ch�ance:</b></font><br><font size=2>Mois: </font><jato:combobox name="cbMaturityDateMonth" /> <font size=2>Jour: </font>
<jato:textField name="tbMaturityDateDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>An: </font>
<jato:textField name="tbMaturityDateYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></td>
<td valign=top align=center colspan=2><jato:button name="btCalculation" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/review_bridge.gif" /></td>
<td valign=top aling=center colspan=2><jato:button name="btDelete" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/delete_fr.gif" /></td>
</tr>

</table>

<!--END CONTACT INFORMATION//-->

<!-- SALE PROPERTY ADDRESS SEARCH RESULTS //-->

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=4 valign=top><font size=3 color=3366cc><b>D�tails de la vente de biens</b>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:text name="rowsDisplayed" escape="true" /></font></td><tr>
<td colspan=4>&nbsp;</td><tr>

<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<jato:tiledView name="rpSaleAddress" type="mosApp.MosSystem.pgBridgeReviewrpSaleAddressTiledView">

<tr>
<td><font size=2 color=#3366cc><b>Adresse ligne 1:</b></font></td>
</tr>

<tr>
<td><font size=2><jato:text name="stPropertyAddressLine1" escape="true" /></font></td>
</tr>

<tr>
<td><font size=2 color=#3366cc><b>Adresse ligne 2</b></font></td>
</tr>

<tr>
<td>&nbsp;&nbsp;</td>
<td><font size=2><jato:text name="stPropertyAddressLine2" escape="true" /></font></td>
</tr>

<tr>
<td><font size=2 color=#3366cc><b>Ville</b></font></td>
<td><font size=2 color=#3366cc><b>Province</b></font></td>
<td><font size=2 color=#3366cc><b>Code Postal</b></font></td>

<tr>
<td><font size=2><jato:text name="stPropertyCity" escape="true" /></font></td>
<td><font size=2><jato:text name="stPropertyProvince" escape="true" /></font></td>
<td><font size=2><jato:text name="stPropertyPostalCodeLDU" escape="true" /> <jato:text name="stPropertyPostalCodeFSA" escape="true" /></font></td>
</tr>

<td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<jato:hidden name="hdRowNum" />
<jato:hidden name="hdPropertyId" />
<jato:hidden name="hdPropertyCopyId" />
</jato:tiledView>

</table>

<!-- Submit and Cancel buttons //-->
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td></tr>
</table>



<p>

</center>
</div>

<!--<td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>-->

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>


<BR>

</jato:form>

<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>
</jato:useViewBean>
</HTML>
