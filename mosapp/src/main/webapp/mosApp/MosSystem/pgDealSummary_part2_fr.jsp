
<HTML>
<!-- Begin of DealSummary Part 2 include -->
<%@page info="pgDealSummary" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealSummaryViewBean">
<script language="javascript"> 
function toggleQualifyProduct() {
	var qualProd = document.getElementById("stQualifyProduct").value;
	var text = document.getElementById("displayText");
	if(qualProd == "") {
		text.innerHTML = "";
  	}
} 
function toggleQualifyPencil() {
	var overrideProd = document.getElementById("hdQualifyRateOverride").value;
	var overrideRate = document.getElementById("hdQualifyRateOverrideRate").value;
	if((overrideProd == "Y" || overrideRate == "Y") && amGenerate == "N") 
		document.getElementById("DQRPencil").style.visibility="visible"; 
	else
		document.getElementById("DQRPencil").style.visibility = "hidden";
} 

</script>
<!-- Start Of Progress Advance Sectiom-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
		<td colspan=5></td>
	</tr>

	<tr>
		<td colspan=5>
			<font color=3366cc><b>Avance progressive:</b></font>
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Avance progressive</b></font><br>
			<font size=2><jato:text name="stProgressAdvance" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Montant de l'avance � ce jour</b></font><br>
			<font size=2><jato:text name="stAdvanceToDateAmt" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Montant de la prochaine avance</b></font><br>
			<font size=2><jato:text name="stNextAdvanceAmt" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Num�ro de l'avance</b></font><br>
			<font size=2><jato:text name="stAdvanceNumber" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
		</td>
	</tr>
	<tr>
		<td colspan=5>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=5>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
</table>
<!-- End Of Progress Advance Sectiom-->

<!-- Start Of Down Payment-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
			<td colspan=8>
		</td>
	</tr>

	<tr>

		<td >
			<font color=3366cc><b>Mise de fonds:</b></font>
		</td>
	</tr>

	<tr>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Source de la mise de fonds</b></font>
		</td>	
		<td valign=top>
			<font size=2 color=3366cc><b>Description de la mise de fonds</b></font>
		</td>			
		<td valign=top>
			<font size=2 color=3366cc><b>Montant de la mise de fonds</b></font>
		</td>			
		<td valign=top>
			<font size=2 color=3366cc><b>&nbsp;&nbsp;</b></font>
		</td>			
		
	</tr>
	
<jato:tiledView name="RepeatedDownPayments" type="mosApp.MosSystem.pgDealSummaryRepeatedDownPaymentsTiledView">
		<tr>
			<td valign=top colspan=2>
				<font size=2><jato:text name="stDownPaymentSource" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>	
			<td valign=top>
				<font size=2><jato:text name="stDownPaymentDesc" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>		
			<td valign=top>
				<font size=2><jato:text name="stDownPaymentAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>		
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;</b></font>
			</td>			
				
		</tr>
</jato:tiledView>
</table>

<!-- End Of Down Payment-->



<!-- Start Of Escrow Payments -->

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>

		<tr>
			<td >
				<font color=3366cc><b>Paiement des fonds plac�s en main tierce:</b></font>
			</td>
		</tr>

		<tr>
			<td valign=top colspan=3>
				<font size=2 color=3366cc><b>Type de fonds:</b></font>
			</td>			
			<td valign=top>
				<font size=2 color=3366cc><b>Description des fonds:</b></font>
			</td>			
			<td valign=top>
				<font size=2 color=3366cc><b>Paiement des fonds:</b></font>
			</td>			
		</tr>

<jato:text name="stBeginHideEscrowSection" fireDisplayEvents="true" escape="true" />

<jato:tiledView name="RepeatedEscrowPayments" type="mosApp.MosSystem.pgDealSummaryRepeatedEscrowPaymentsTiledView">
		<tr>
			<td valign=top colspan=3>
				<font size=2><jato:text name="stEscrowType" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>			
			<td valign=top>
				<font size=2><jato:text name="stEscrowPaymentDesc" escape="true" formatType="string" formatMask="????????????????????????????????????" /></font>
			</td>			
			<td valign=top colspan=3>
				<font size=2><jato:text name="stEscrowPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>			
		</tr>
</jato:tiledView>

<jato:text name="stEndHideEscrowSection" fireDisplayEvents="true" escape="true" />
	</table>

	<!-- End Of Escrow Payments-->

	<!-- Start Of Decisioning Information-->
<input type="hidden" id="hdQualifyRateOverride" value='<jato:text name="hdQualifyRateOverride"/>'/>
<input type="hidden" id="hdQualifyRateOverrideRate" value='<jato:text name="hdQualifyRateOverrideRate"/>'/>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>Information sur la prise de d�cision:</b></font>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Cote de cr�dit</b></font><br>
				<font size=2><jato:text name="stCreditScore" escape="true" formatType="decimal" formatMask="fr|#,##0; (#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ABD combin�</b></font><br>
				<font size=2><jato:text name="stCombinedGDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ABD (Taux Admissible)</b></font><br>
				<font size=2><jato:text name="stCombined3YrGds" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ATD combin�</b></font><br>
				<font size=2><jato:text name="stCombinedTDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ATD (Taux Admissible)</b></font><br>
				<font size=2><jato:text name="stCombined3YrTds" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
            <td valign=top  style='display: <jato:text name="stQualifyRateEdit"/>'>
                <font size=2 color=3366cc><b>D�tails du Taux Admissible</b></font><br>
                <font size=2><input type="hidden" id="stQualifyProduct" value='<jato:text name="stQualifyProduct"/>'/><div id="displayText"><jato:text name="stQualifyProduct" escape="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><jato:text name="stQualifyRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %&nbsp;<img src="../images/pencil.png" id="DQRPencil"></font>
            </td>
			<td>				
				<a onclick="return IsSubmited();" href="javascript:viewSub('gdstds')">
					<img src="../images/gds-tds-details_fr.gif" width=117 height=15 alt="" border="0">
				</a>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Montant du revenu combin�</b></font><br>
				<font size=2><jato:text name="stcombinedTotalIncome" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font> 
				<a onclick="return IsSubmited();" href="javascript:viewSub('inc')">
					<img src="../images/income_fr.gif" width=98 height=15 alt="" border="0">
				</a>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Total du passif combin�</b></font><br>
				<font size=2><jato:text name="stcombinedTotalLiabilities" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font> 
				<a onclick="return IsSubmited();" href="javascript:viewSub('liab')">
					<img src="../images/liabilities_fr.gif" width=107 height=15 alt="" border="0">
				</a>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Total de l'actif combin�</b></font><br>
				<font size=2><jato:text name="stcombinedTotalAssets" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font> 
				<a onclick="return IsSubmited();" href="javascript:viewSub('asst')">
					<img src="../images/asset_fr.gif" width=107 height=15 alt="" border="0">
				</a>
			</td>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
	</table>
	<!-- End Of Decisioning Information-->
	
	
	<!-- Start Of Primary Property Information-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>Information sur la propri�t� principale:</b></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Adresse de la propri�t�</b></font><br>
				<font size=2><jato:text name="stPropertyStreetno" escape="true" /> <jato:text name="stPropertyStreetname" escape="true" /> <jato:text name="stPropertyStreetType" escape="true" />  <jato:text name="stPropertyStreetDirection" escape="true" /> <jato:text name="stPropertyAddress2" escape="true" /> <jato:text name="stPropertyUnitNumber" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Ville</b></font><br>
				<font size=2><jato:text name="stPropertyCity" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Province</b></font><br>
				<font size=2><jato:text name="stPropertyProvince" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Code postal</b></font><br>
				<font size=2><jato:text name="stPropertyPostalCode" escape="true" /> <jato:text name="stPropertyPostalCode1" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Usage de la propri�t�</b></font><br>
				<font size=2><jato:text name="stPropertyUsage" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Occupation</b></font><br>
				<font size=2><jato:text name="stPropertyOccupancy" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de propri�t�</b></font><br>
				<font size=2><jato:text name="stPropertyType" escape="true" /></font>
			</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Nbre de propri�t�s</b></font><br>
				<font size=2><jato:text name="stNoOfProperties" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<a onclick="return IsSubmited();" href="javascript:viewSub('prop')">
					<img src="../images/property_fr.gif" width=124 height=15 alt="" border="0">
				</a>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Prix d'achat</b></font><br>
				<font size=2><jato:text name="stPropertyPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Valeur estimative de l'�valuation</b></font><br>
				<font size=2><jato:text name="stPropertyEstimatedAppraisalValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Valeur r�elle de l'�valuation</b></font><br>
				<font size=2><jato:text name="stPropertyActualApraisedValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
			<td valign=top >
				<font size=2 color=3366cc><b>Valeur du terrain</b></font><br>
				<font size=2><jato:text name="stPropertyLandValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (#)" /></font>
			</td>
			<td valign=top >
				<font size=2 color=3366cc><b>D�penses combin�es de la propri�t�</b></font><br>
				<font size=2><jato:text name="stCombinedTotalPropertyExp" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
			<td valign=top >
				<a onclick="return IsSubmited();" href="javascript:viewSub('expen')">
					<img src="../images/expense-details_fr.gif" width=117 height=15 alt="" border="0">
				</a>
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
	</table>
	<!-- End Of Primary Property Information-->
	
	
	<!-- Start Of Mortgage Insurer Information-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=5>
				<font color=3366cc><b>Information sur l'assurance pr�t-hypoth�caire:</b></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Indicateur de l'assurance</b></font><br>
				<font size=2><jato:text name="stMIIndictorDesc" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Statut de l'assurance</b></font><br>
				<font size=2><jato:text name="stMIRequestStatus" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> de police de l'assurance existant</b></font><br>
				<font size=2><jato:text name="stMIExistingPolicyNum" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> de certificat</b></font><br>
				<font size=2><jato:text name="stMIPoliceNumber" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> du certificat de pr�qualification</b></font><br>
				<font size=2><jato:text name="stMIPrequalificationMICertNum" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Type d'assurance</b></font><br>
				<font size=2><jato:text name="stMIType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Assureur pr�t-hypoth�caire</b></font><br>
				<font size=2><jato:text name="stMIInsurer" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Payeur de l'assurance</b></font><br>
				<font size=2><jato:text name="stMIPayorDesc" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Frais pr�lev�s � l'acquisition d'assurance</b></font><br>
				<font size=2><jato:text name="stMIUpfront" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Prime d'assurance</b></font><br>
				<font size=2><jato:text name="stMIPremium" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Intervention du souscripteur hypoth�caire requise</b></font><br>
				<font size=2><jato:text name="stMIRUIntervention" fireDisplayEvents="true" escape="true" /></font>
			</td>
			
<%-- Release3.1 Apr 12, 2006 begins --%>
								
			<td valign=top>
			<!-- SEAN DJ SPEC-Progress Advance Type July 26, 2005:  -->
        <font size=2 color=3366cc><b>Type d'avance �chelonn�e:</b></font><br>
				<font size="2"><jato:text name="stProgressAdvanceType" escape="true"/></font>
      <!-- SEAN DJ SPEC-PAT END -->
			</td>
		
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>Inspection de l'avance de progress par:</B></FONT><BR>
				<font size=2><jato:text name="stProgressAdvanceInspectionBy" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>Hypotheques REER autog�r� :</B></FONT><BR>
				<font size=2><jato:text name="stSelfDirectedRRSP" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>L'identificateur poursuivant de produit pour SCHL:</B></FONT><BR>
				<font size=2><jato:text name="stCMHCProductTrackerIdentifier" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			
		</tr>
		
		<TR>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>Remboursement MCVD:</B></FONT><BR>
				<font size=2><jato:text name="stLOCRepayment" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>Demande pour sevice de base:</B></FONT><BR>
				<font size=2><jato:text name="stRequestStandardService" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top>
				<FONT color=#3366cc size=2><B>Amortissement MCVD:</B></FONT><BR>
				<font size=2><jato:text name="stLOCAmortization" fireDisplayEvents="true" escape="true" /></font>
			</TD>
			<TD vAlign=top colspan=2>
				<FONT color=#3366cc size=2><B>Date d'�ch�ance pour un pr�t non Amorti MCVD:</B></FONT><BR>
				<font size=2><jato:text name="stLOCInterestOnlyMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
			</TD
		</TR>
<%-- Release3.1 Apr 12, 2006 ends --%>
		
			<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
			<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>			

	</table>
	<!-- End Of Mortgage Insurer Information-->

	<!-- Start Of Life Disability Insurance-->
	<jato:text name="stIncludeLifeDisSectionStart" escape="false" />
	
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td colspan=8></td></tr>
		<tr><td ><font color=3366cc><b>Renseignements - Assurance Vie / Invalidit�:</b></font></td></tr>

		<tr>
		<td valign=top><font size=2 color=3366cc><b>Nom du demandeur:</b></font></td>	
		<td valign=top><font size=2 color=3366cc><b>Type de demandeur:</b></font></td>			
		<td valign=top><font size=2 color=3366cc><b>Options d'assurance:</b></font></td>
		<td valign=top><font size=2 color=3366cc><b>Vie<br>Couverture<br>d'assurance:</b></font></td>
		<td valign=top><font size=2 color=3366cc><b>Invalidit�<br>Couverture:</b></font></td>
		<td valign=top><font size=2 color=3366cc><b>Caution sur<br>autres pr�ts?:</b></font></td>		
		<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;</b></font></td>
		</tr>

		<jato:tiledView name="RepeatedLifeDisabilityInsurance" type="mosApp.MosSystem.pgDealSummaryRepeatedLifeDisabilityInsuranceTiledView">
		<tr>
		<td valign=top><font size=2><jato:text name="stLDFirstName" escape="true" /> <jato:text name="stLDMiddleInitial" escape="true" /> <jato:text name="stLDLastName" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stLDApplicantType" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stInsProportions" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stLifeCoverage" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stDisabilityCoverage" escape="true" /></font></td>
		<td valign=top><font size=2><jato:text name="stGuarantorOtherLoans" escape="true" fireDisplayEvents="true" /></font></td>		
		</tr>
		
	</jato:tiledView>
	</table>
		<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>	
	<jato:text name="stIncludeLifeDisSectionEnd" escape="false" />			
		
	<!-- End Of Life Disability Insurance-->

		<table border=0 width=100%>
			<tr>
				<td align=right>
					<img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();">
				</td>		
			</tr>		
			<tr>
				<td align=right><jato:button name="btOkMainPageButton" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" /></td>		
			</tr>		
		</table>

</center>
</div>

<!-- End Of Main Page-->


<!-- Start Of Application Details Panel-->

<div id="dealsumborr" class="dealsumborr" name="dealsumborr">
<center>
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td colspan=6>
				<font color=3366cc><b>Coordonn�es du demandeur:</b></font>
			</td>
		</tr>
	</table>

<jato:tiledView name="RepeatedBorrowerDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedBorrowerDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Nom du demandeur</b></font><br>
				<font size=2> <jato:text name="stAPDtlsApplicantSalutation" escape="true" /> <jato:text name="stAPDtlsFirstName" escape="true" /> <jato:text name="stAPDtlsMiddleInitial" escape="true" /> <jato:text name="stAPDtlsLastName" escape="true" /> <jato:text name="stAPDtlsSuffix" escape="true" /></font>
			</td>
			<td valign=top>
			<font size=2 color=3366cc><b>Sexe:</b></font><br>
			<font size=2><jato:text name="stGender" /></font>
		</td>
			<td valign=top>			
				<font size=2 color=3366cc><b>Type de demandeur</b></font><br>
				<font size=2><jato:text name="stAPDtlsApplicantType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Emprunteur principal?</b></font><br>
				<font size=2><jato:text name="stAPDtlsPrimaryBorrower" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
			<font size=2 color=3366cc><b>D�marchage:</b></font><br>
			<font size=2><jato:text name="stSolicitation" /></font>	
			</td>
            <td valign=top>
				<font size=2 color=3366cc><b>Langue d�sir�e:</b></font><br>
				<font size=2><jato:text name="stAPDtlsLanguagePreference" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td colspan=6>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Date de naissance:</b></font><br>
				<font size=2> <jato:text name="stAPDtlsDateOfBirth" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /> </font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>�tat civil:</b></font><br>
				<font size=2><jato:text name="stAPDtlsMaritalStatus" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>NAS:</b></font><br>
				<font size=2><jato:text name="stAPDtlsSINNo" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Statut de citoyennet�:</b></font><br>
				<font size=2><jato:text name="stAPDtlsCitizenship" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Nbre de personnes � charge:</b></font><br>
				<font size=2><jato:text name="stAPDtlsNoOfDependants" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top >
			<font size=2 color=3366cc><b>Fumeur:</b></font><br>
			<font size=2><jato:text name="stSmoker"  /></font> 
		</td>			
		</tr>
		<tr>
			<td colspan=6>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Client existant?</b></font><br>
				<font size=2><jato:text name="stAPDtlsExistingClient" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> de r�f�rence client:</b></font><br>
				<font size=2><jato:text name="stAPDtlsReferenceCllientNo" escape="true" formatType="decimal" formatMask="###0; (#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Nbre de faillites:</b></font><br>
				<font size=2><jato:text name="stAPDtlsNoOfTimeBankrupt" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Statut de faillite:</b></font><br>
				<font size=2><jato:text name="stAPDtlsBankruptcyStatus" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Personnel du pr�teur?</b></font><br>
				<font size=2><jato:text name="stAPDtlsStaffOfLender" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top >
			<font size=2 color=3366cc><b>N� d'employ�:</b></font><br>
			<font size=2><jato:text name="stEmployeeNumber"  /></font> 
		</td>			
		</tr>
		<tr>
			<td colspan=6>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone maison:</b></font><br>
				<font size=2><jato:text name="stAPDtlsHomePhone" escape="true" formatType="string" formatMask="(???)???-????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone bureau:</b></font><br>
				<font size=2><jato:text name="stAPDtlsWorkPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stAPDtlsWorkPhoneExt" fireDisplayEvents="true" escape="true" /></font>
			</td>
            <td valign=top>
				<font size=2 color=3366cc><b>Num�ro de t�l�phone cellulaire #:</b></font><br>
				<font size=2><jato:text name="stAPDtlsCellPhone" escape="true" formatType="string" formatMask="(???)???-????" /> </font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur:</b></font><br>
				<font size=2><jato:text name="stAPDtlsFaxNumber" escape="true" formatType="string" formatMask="(???)???-????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Adresse courriel:</b></font><br>
				<font size=2><jato:text name="stAPDtlsEmailAddress" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>M�thode de communication pr�f�r�e:</b></font><br>
				<font size=2><jato:text name="stPreferredMethodOfContact"  /></font>
			</td>
		</tr>
		<tr>
			<td colspan=6>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>

		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
		
		<tr>
		<!-- Start Of Identifications section Information-->

   <table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

		<tr>
			<td colspan=8>
			<font color=3366cc><b>Identification:</b></font>
			 
			</td>
		</tr>

		
      <jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgIdentificationTypeRepeated1TiledView">

		<tr>

		  <td valign=top>
			<font size=2 color=3366cc><b>Type d'identification:</b></font><br>
			<font size=2><jato:text name="stIdentificationType" /></font>
		  </td>
		  <td valign=top>
			<font size=2 color=3366cc><b>Num�ro d'identification:</b></font><br>
			<font size=2><jato:text name="stIdentificationNumber" /></font>
		  </td>
		  <td valign=top>
			<font size=2 color=3366cc><b>Pays de la source d'identification:</b></font><br>
			<font size=2><jato:text name="stIdentificationSourceCountry" /></font>
		  </td>
		
	   </tr>


      </jato:tiledView>
	
     </table>
   </tr>
<!-- End Of Identification Information-->
   <tr>
		
 		<!-- Start Of Application Address Information -->
		<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<!--<table border=0 width=100% cellpadding=1 cellspacing=0>-->
		<jato:tiledView name="RepeatedApplicantAddress" type="mosApp.MosSystem.pgDealSummaryRepeatedApplicantAddressTiledView">
			<tr>
				<td valign=top colspan=2>
					<font size=2 color=3366cc><b>Adresse:</b></font><br>
					<font size=2><jato:text name="stAPDAddressLine1" escape="true" /><br><jato:text name="stAPDAddressLine2" escape="true" /> </font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Ville:</b></font><br>
					<font size=2><jato:text name="stAPDCity" escape="true" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Province:</b></font><br>
					<font size=2><jato:text name="stAPDProvince" escape="true" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Code postal:</b></font><br>
					<font size=2><jato:text name="stAPDPostalCode1" escape="true" /> <jato:text name="stAPDPostalCode2" escape="true" /></font>
				</td>
			</tr>
			<tr>
				<td colspan=6>
					<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
				</td>
			</tr>
			<tr>
				<td valign=top>
					<font size=2 color=3366cc><b>Temps pass� � cette r�sidence:</b></font><br>
					<font size=2><jato:text name="stAPDTimeAtResidence" fireDisplayEvents="true" escape="true" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Statut de l'adresse:</b></font><br>
					<font size=2><jato:text name="stAPDAddressStatus" escape="true" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>Statut r�sidentiel:</b></font><br>
					<font size=2><jato:text name="stAPDResidentialStatus" escape="true" /></font>
				</td>
			</tr>

</jato:tiledView>
			<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
		</table>	
		<!-- End Of Application Address Information -->
		</tr>
	</table>

	<table>
			<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
	</table>

</jato:tiledView>

		<table border=0 width=100%>
			<tr>
				<td align=right>
					<img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();">
				</td>		
			</tr>		
		</table>

	<table border=0 width=100%>
		<td align=right>
			<a onclick="return IsSubmited();" href="javascript:viewSub('main')">
				<img src="../images/ok.gif" width=86 height=25 alt="" border="0">
			</a>
		</td>
	</table>
</center>
</div>
<!-- End Of Application Details Panel-->



<!-- Start Of General Panels - dropdown displays -->

<!-- Start Of Change Password Drop Down Panel -->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<!-- End Of Change Password Drop Down Panel -->


<!-- Start Of Previous Links Drop down Panel -->
<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>
<!-- End Of Previous Links Drop down Panel -->


<!-- Start of Goto Page Drop Dowm Panel -->
<!-- End of Goto Page Drop Dowm Panel -->

<!-- End Of General Panels Like Error Message, Dropdown display-->
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->


<!-- Start Of GDS/TDS Details Panel -->

<div id="gdstds" class="gdstds" name="gdstds">
<center>
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td colspan=9><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=9><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		
		<tr>
				<td colspan=6>
					<font color=3366cc><b>D�tails de l'ABD:</b></font>
				</td>
			</tr>
			<tr>
				<td valign=top colspan=6>
					<font size=2 color=3366cc><b>ABD combin�:</b></font><br>
					<font size=2><jato:text name="stCombinedGDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>ABD (Taux Admissible):</b></font><br>
					<font size=2><jato:text name="stCombined3YrGds" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
				</td>
				<td valign=top>
					<font size=2 color=3366cc><b>ABD combin� (emprunteur uniquement):</b></font><br>
					<font size=2><jato:text name="stCombinedBorrowerGDS" escape="true" /></font>
				</td>
			</tr>

	
	<jato:tiledView name="RepeatedGDS" type="mosApp.MosSystem.pgDealSummaryRepeatedGDSTiledView">
		<tr>
			<td valign=top colspan=6>
				<font size=2 color=3366cc><b>Demandeur:</b></font><br>
				<font size=2><jato:text name="stApplicantFirstNameGDS" escape="true" /> <jato:text name="stApplicantInitialGDS" escape="true" /> <jato:text name="stApplicantLastNameGDS" escape="true" formatType="string" formatMask="???????????????????????????????????" /> <jato:text name="stApplicantSuffixGDS" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de demandeur:</b></font><br>
				<font size=2><jato:text name="stApplicantTypeGDS" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ABD:</b></font><br>
				<font size=2><jato:text name="stGDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ABD (Taux Admissible):</b></font><br>
				<font size=2><jato:text name="st3YrGDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
</jato:tiledView>		
	</table>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<img src="../images/blue_line.gif" width=100% height=2 alt="" border="0">
			</td>
		</tr>
	</table>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8>
				<font color=3366cc><b>D�tails de l'ATD:</b></font>
			</td>
		</tr>
		<tr>
			<td valign=top colspan=8>
				<font size=2 color=3366cc><b>ATD combin�:</b></font><br>
				<font size=2><jato:text name="stCombinedTDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ATD (Taux Admissible):</b></font><br>
				<font size=2><jato:text name="stCombined3YrTds" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ATD combin� (Emprunteur uniquement):</b></font><br>
				<font size=2><jato:text name="stCombinedBorrowerTDS" escape="true" /></font>
			</td>
		</tr>
	<jato:tiledView name="RepeatedTDS" type="mosApp.MosSystem.pgDealSummaryRepeatedTDSTiledView">
		<tr>
			<td valign=top colspan=8>
				<font size=2 color=3366cc><b>Demandeur:</b></font><br>
				<font size=2>	<jato:text name="stApplicantFirstNameTDS" escape="true" /> <jato:text name="stApplicantInitialTDS" escape="true" /> <jato:text name="stApplicantLastNameTDS" escape="true" formatType="string" formatMask="???????????????????????????????????" /> <jato:text name="stApplicantSuffixTDS" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de demandeur:</b></font><br>
				<font size=2><jato:text name="stApplicantTypeTDS" escape="true" formatType="string" formatMask="???????????????????????????????????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ATD:</b></font><br>
				<font size=2><jato:text name="stTDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>ATD (Taux Admissible):</b></font><br>
				<font size=2><jato:text name="st3YrTDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" /></font>
			</td>
		</tr>
</jato:tiledView>
		<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
			<tr>
				<td colspan=8>
					<img src="../images/blue_line.gif" width=100% height=2 alt="" border="0">
				</td>
			</tr>
		</table>
	</table>
	<table border=0 width=100%>
		<tr>
			<td align=right>
				<img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>

	<table border=0 width=100%>
		<td align=right>
			<a onclick="return IsSubmited();" href="javascript:viewSub('main')">
				<img src="../images/ok.gif" width=86 height=25 alt="" border="0">
			</a>
		</td>
	</table>
</center>
</div>
<!-- End Of GDS/TDS Details Panel-->

<!--Start Of Income Details Panel-->
<div id="dealsuminc" class="dealsuminc" name="dealsuminc">
<center>
	<!-- Start - Borrower Summary Details-->
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=6>
				<font color=3366cc><b>D�tails du revenu:</b></font>
			</td>
		</tr>
	</table>
	
	<jato:tiledView name="RepeatedIncomeDetails" type="mosApp.MosSystem.pgDealSummaryRepeatedIncomeDetailsTiledView">
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>Nom du demandeur:</b></font><br>
				<font size=2><jato:text name="stIDBorrSalutation" escape="true" /> <jato:text name="stIDborrFirstName" escape="true" /> <jato:text name="stIDBorrMidInitial" escape="true" /> <jato:text name="stIDBorrLastName" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Type de demandeur:</b></font><br>
				<font size=2><jato:text name="stIDBorrType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>Emprunteur principal?</b></font><br>
				<font size=2><jato:text name="stIDBorrFlag" fireDisplayEvents="true" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td colspan=5>
				<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
			</td>
		</tr>
	</table>
	<!-- End Borrower Summary Details-->

	<!-- Start - Employement Income Details-->

	<jato:text name="stBeginHideEmpSection" fireDisplayEvents="true" escape="true" />
	
	<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=6>
				<font color=3366cc><b>&nbsp;&nbsp;Revenu d'emploi</b></font>
			</td>
		</tr>
		<tr>
			<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
		</tr>
	</table>

	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<jato:tiledView name="Employment" type="mosApp.MosSystem.pgDealSummaryEmploymentTiledView">
		<tr>
			<td valign=top colspan=2>
				<font size=2 color=3366cc colspan=2><b>&nbsp;&nbsp;Nom de l'employeur:</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmployerName" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Statut d'emploi</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmploymentStatus" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Type d'emploi</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmploymentType" escape="true" /></font>
			</td>
		</tr>
		<tr>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Num�ro de t�l�phone bureau</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDWorkPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stIDMWorkPhoneExt" fireDisplayEvents="true" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Num�ro de t�l�copie bureau</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDFaxNum" escape="true" formatType="string" formatMask="(???)???-????" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Adresse �lectronique bureau</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmployeeEmailAddress" escape="true" /></font>
			</td>
		</tr>
		<tr bgcolor=d1ebff>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Adresse du bureau</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmplAddr1" escape="true" /><br>&nbsp;&nbsp;<jato:text name="stIDEmplAddr2" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Ville</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmplCity" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Province</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmplProv" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Code postal</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDEmplPostCode1" escape="true" />&nbsp;&nbsp;<jato:text name="stIDEmplPostCode2" escape="true" /></font>
			</td>
		</tr>
		<tr bgcolor=d1ebff>
			<td valign=top >
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Secteur d�activit�</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDIndustrySector" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Profession/M�tier</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDOccupation" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Titre/Poste</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDJobTitle" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Depuis combien de temps</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDTimeAtJob" fireDisplayEvents="true" escape="true" /></font>
			</td>
		</tr>
		<tr bgcolor=d1ebff>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Type de revenu</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomeType" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Description du revenu</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomeDesc" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;P�riode du revenu</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomePeriod" escape="true" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Montant du revenu</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomeAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;% compris dans l�ABD</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomePercentIncludeGDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
			</td>
			<td valign=top>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;% compris dans l�ATD</b></font><br>
				<font size=2>&nbsp;&nbsp;<jato:text name="stIDMIncomePercentIncludeTDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font>
			</td>
		</tr>
	
</jato:tiledView>
<jato:text name="stEndHideEmpSection" fireDisplayEvents="true" escape="true" />

	<!-- End - Employement Income Details-->

	<!-- Start - Other Income Details-->

<!--	<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff> -->
<jato:text name="stBeginHideOtherIncSection" fireDisplayEvents="true" escape="true" />

		<tr>
			<td colspan=4>
				<font size=2 color=3366cc><b>&nbsp;&nbsp;Autre revenu</b></font>
			</td>
		</tr>
		<tr>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Type de revenu</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Description du revenu</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;P�riode du revenu</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;Montant du revenu</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;% compris dans l�ABD</b></font></td>
			<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;% compris dans l�ATD</b></font></td>
		</tr>
	<jato:tiledView name="OtherIncome" type="mosApp.MosSystem.pgDealSummaryOtherIncomeTiledView">
		<tr>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDIncomeType" escape="true" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDOtherIncomeDesc" escape="true" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDIncomePeriod" escape="true" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDIncomeAmount" escape="true" formatType="currency" formatMask="fr|#,##0.00$; -#" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDOtherIncomeGDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
			<td valign=top><font size=2>&nbsp;&nbsp;<jato:text name="stIDOtherIncomeTDS" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /></font></td>
		</tr>
	</jato:tiledView>

<jato:text name="stEndHideOtherIncSection" fireDisplayEvents="true" escape="true" />
		<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td></tr>
	</table>
<!-- End - Other Income Details-->
</jato:tiledView>
	<!-- End - Income Details-->

	<table border=0 width=100%>
		<tr>
			<td align=right>
			<img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0" onClick="OnTopClick();">
			</td>		
		</tr>		
	</table>
	
	<table border=0 width=100%>
		<td align=right>
			<a onclick="return IsSubmited();" href="javascript:viewSub('main')">
				<img src="../images/ok.gif" width=86 height=25 alt="" border="0">
			</a>
		</td>
	</table>
	</center>
	</div>
<!-- End - All Income Details-->

<!-- Include Part 3 of DealSummary -->
<jsp:include page="pgDealSummary_part3_fr.jsp" />

</jato:useViewBean>
<!-- End of DealSummary Part 2 include -->
</HTML>
