<%--
 /**Title: pgDealSummary.jsp
 *Description:Deal Summary Screen
 *@version 1.0 Initial Version
 *@version 1.1 06-JUN-2008 XS_16.9 Included pgQualifyingDetailsPagelet 
 *@version 1.2 XS_16.7 -- 10/06/2008  -- Added Component information section.
 *@version 1.3 Bugfix: FXP22927  - Correct display format for netrate,premium,discount and added new field for component loan section to display payment amount.
 *@version 1.4 MCM, FXP23175, Oct 31, 08, changed div tag(pagebody)'s position.
 */
--%>
<%--
 /*
  * MCM Impl Team. Change Log
  * XS_16.8 -- 13/06/2008  -- Added Additional Details and Existing Account Reference fields in Refinance section.
  * 
 */
  --%>
<HTML>
<%@page info="pgDealSummary" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealSummaryViewBean">

<HEAD>
<TITLE>Deal Summary</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?215:190); width: 100%;overflow: hidden;}
.dealsumborr {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsumasst {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsuminc {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsumliab {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsumprop {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.gdstds {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
.dealsumpropexp {position: absolute; visibility: hidden; top: expression((QLE)?215:190); left: 10; width:100%; right-margin:5%;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>


<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellForDealSummary.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>  

</HEAD>

<body bgcolor=ffffff>
<jato:form name="pgDealSummary" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
	<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
	<input type="hidden" name="isFatal" value="">

	
	<!--HEADER//-->
	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
      
      <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
    
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %>  

<!--END HEADER//-->



<!--DEAL SUMMARY SNAPSHOT//-->
<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>
<!--END DEAL SUMMARY SNAPSHOT//-->

<%-- FXP23175, MCM, Oct 31, 08, changed position --%>
<div id="pagebody" class="pagebody" name="pagebody">

<!-- START GCD Summary Section -->
<jato:text name="stIncludeGCDSumStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td></tr>
<tr>
	<td valign=top colspan=1>&nbsp;</td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Status:</b></font><br><font size=2><jato:text name="txCreditDecisionStatus"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Decision:</b></font><br><font size=2><jato:text name="txCreditDecision"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Credit Bureau Score:</b></font><br><font size=2><jato:text name="txGlobalCreditBureauScore"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Risk Rating:</b></font><br><font size=2><jato:text name="txGlobalRiskRating"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Internal Score:</b></font><br><font size=2><jato:text name="txGlobalInternalScore"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>MI Status :</b></font><br><font size=2><jato:text name="stMIRequestStatus"/></font></td>
	<td valign=top colspan=1><font size=2 color=3366cc></font>&nbsp<br></td>
</tr>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>
  </table>
<jato:text name="stIncludeGCDSumEnd" escape="false" /><br>
<!-- End GCD Summary Section -->

<!-- Start Of Summary Buttons-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<tr><td align=top colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
<tr><td  align=left valign=middle colspan=6>
	&nbsp;&nbsp;<jato:button name="btPrintDealSummaryLite" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/printdeal.gif" />
	&nbsp;&nbsp;
	<!-- SEAN Ticket #1681 June 27, 2005: Hide the print deal summary bt for DJ. -->
	<jato:text name="stBeginHidePrintDealSummary" fireDisplayEvents="true" escape="true" />
	<jato:button name="btPrintDealSummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/DealSummaryDetails.gif" />
	<jato:text name="stEndHidePrintDealSummary" fireDisplayEvents="true" escape="true" />
	<!-- SEAN Ticket #1681 END -->
	&nbsp;&nbsp;<jato:button name="btPartySummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/party_summary.gif" />
      &nbsp;&nbsp;<jato:button name="btDeclineLetter" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/printdeclineletter.gif" />
	</td>
	<td  align=right valign=middle colspan=2>
	   <!--  #1676 - Hide Reverse funding if user doesn't have rights, Catherine, 2-Sep-05 begin -->
	   <jato:text name="stHideReverseBtnStart" escape="false"/>
		<jato:button name="btReverseFunding" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/reverse_funding.gif" fireDisplayEvents="true" />
	   <jato:text name="stHideReverseBtnEnd" escape="false"/>
	   <!--  #1676  ======================================================================= end -->
	</td>
</tr>
</table>
<!-- End Of Summary Buttons-->

<!-- Start Of Application Information-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>	
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<font color=3366cc><b>Application Information:</b></font>
			<a onclick="return IsSubmited();" href="javascript:viewSub('borr')">
				<img src="../images/applicant_details.gif" width=97 height=16 alt="" border="0" hspace=0 vspace=0>
			</a> 
		</td>
	</tr>

	<tr>
		<td valign=top >
			<font size=2 color=3366cc><b>Primary Borrower:</b></font><br>
			<font size=2> <jato:text name="stFirstName" escape="false" /> <jato:text name="stMiddleInitial" escape="true" />  <jato:text name="stLastName" escape="true" /> <jato:text name="stSuffix" escape="true" /></font> 
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Language Preference:</b></font><br>
			<font size=2><jato:text name="stLanguagePreference" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Existing Client?</b></font><br>
			<font size=2><jato:text name="stExistingClient" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Ref.Client #:</b></font><br>
			<font size=2><jato:text name="stReferenceClientNumber" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Underwriter/Administrator/Funder:</b></font><br>
			<font size=2><jato:text name="stUnderwriter" fireDisplayEvents="true" escape="true" formatType="string" formatMask="????????????????????????????????????????" />/<jato:text name="stAdministrator" fireDisplayEvents="true" escape="true" formatType="string" formatMask="????????????????????????????????????????" />/<jato:text name="stFunder" fireDisplayEvents="true" escape="true" formatType="string" formatMask="????????????????????????????????????????" /></font>
		</td>
	</tr>
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>



	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Home Phone #:</b></font><br>
			<font size=2><jato:text name="stHomePhone" escape="true" formatType="string" formatMask="(???)???-????" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Work Phone #:</b></font><br>
			<font size=2><jato:text name="stWorkPhone" escape="true" formatType="string" formatMask="(???)???-????" /> <jato:text name="stBorrowerWorkPhoneExt" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top >
			<font size=2 color=3366cc><b># of Borrowers:</b></font><br>
			<font size=2><jato:text name="stNoOfBorrowers" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font> 
		</td>
		<td valign=top >
			<font size=2 color=3366cc><b># of Guarantors:</b></font><br>
			<font size=2><jato:text name="stNoOfGuarantors" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font> 
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Branch:</b></font><br>
			<font size=2><jato:text name="stBranch" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
</table>
<!-- End Of Application Information-->

<!--MCM Impl team changes starts - XS_16.9-->
    
	<jato:containerView name="qualifyingDetailsPagelet" type="mosApp.MosSystem.pgQualifyingDetailsPageletView">
		<jsp:include page="pgQualifyingDetailsPageletSummary.jsp"/>
	</jato:containerView>
	 
<!--MCM Impl team changes ends - XS_16.9-->
<!-- Start Of Deal Information-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
	<tr>
		<td colspan=8>
			<font color=3366cc><b>Deal Information:</b></font>
		</td>
	</tr>
	<%-- Release3.1 Apr 12, 2006 begins --%>
	<TR>
		<TD vAlign=top>
			<FONT color=#3366cc size=2><B>Product Type:</B></FONT><BR>
			<FONT size=2><jato:text name="stProductType" fireDisplayEvents="true" escape="true" /></FONT> 
		</TD>
		<TD vAlign=top>
			<FONT color=#3366cc size=2><B>Charge:</B></FONT><BR>
			<FONT size=2><jato:text name="stCharge" fireDisplayEvents="true" escape="true" /></FONT> 
		</TD>
		<TD vAlign=top>
			<FONT color=#3366cc size=2><B>Financing Waiver Date:</B></FONT><BR>
			<FONT size=2><jato:text name="stFinancingDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></FONT> 
		</TD>
	</TR>
	<%-- Release3.1 Apr 12, 2006 ends --%>
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Total Loan Amount:</b></font><br>
			<font size=2><jato:text name="stDITotalLoanAmount" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Payment Term:</b></font><br>
			<font size=2><jato:text name="stPaymentTerm" fireDisplayEvents="true" escape="true" /> - <jato:text name="stPaymentTermDesc" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Effective Amortization:</b></font><br>
			<font size=2><jato:text name="stEffectiveAmortization" fireDisplayEvents="true" escape="true" formatType="decimal" formatMask="#,##0; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Payment Frequency:</b></font><br>
			<font size=2><jato:text name="stPaymentFrequency" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Combined LTV:</b></font><br>
			<font size=2><jato:text name="stCombinedLTV" fireDisplayEvents="true" escape="true" /></font>
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Rate Code:</b></font><br>
			<font size=2><jato:text name="stRateCode" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Rate Date:</b></font><br>
			<font size=2><jato:text name="stRateDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Posted Rate:</b></font><br>
			<font size=2><jato:text name="stPostedRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Net Rate:</b></font><br>
			<font size=2><jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Rate Locked In:</b></font><br>
			<font size=2><jato:text name="stRateLockedIn" fireDisplayEvents="true" escape="true" /></font>
		</td>
	</tr>	

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Discount:</b></font><br>
			<font size=2><jato:text name="stDiscount" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Premium:</b></font><br>
			<font size=2><jato:text name="stPremium" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Buydown Rate:</b></font><br>
			<font size=2><jato:text name="stBuyDownRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Special Feature:</b></font><br>
			<font size=2><jato:text name="stDISpecialFeature" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Pre-App. Est. Purchase Price:</b></font><br>
			<font size=2><jato:text name="stPreAppPricePurchase" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Cashback($):</b></font><br>
			<font size=2><jato:text name="stCashBackInDollars" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)"/></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Cashback(%):</b></font><br>
			<font size=2><jato:text name="stCashBackInPercentage" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
	
		
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>P&I Payment:</b></font><br>
			<font size=2><jato:text name="stPIPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Additional Principal Payment:</b></font><br>
			<font size=2><jato:text name="stAdditionalPrincipalPayemt" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
			
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Total Escrow Payment:</b></font><br>
			<font size=2><jato:text name="stTotalEscrowPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Total Payment:</b></font><br>
			<font size=2><jato:text name="stTotalPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>First Payment Date:</b></font><br>
			<font size=2><jato:text name="stFirstPaymentDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
		</td>
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Maturity Date:</b></font><br>
			<font size=2><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Second Mortgage?</b></font><br>
			<font size=2><jato:text name="stSecondMortgage" fireDisplayEvents="true" escape="true" formatType="string" formatMask="???" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Equity Available:</b></font><br>
			<font size=2><jato:text name="stEquityAvailable" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
	
		<td valign=top>
			<font size=2 color=3366cc><b>Required Downpayment:</b></font><br>
			<font size=2><jato:text name="stRequiredDownPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
			<td valign=top colspan=2>
				<font size=2 color=3366cc><b>Requested Product, Rate and Payment:</b></font><br>
				<font size=2><jato:text name="stHomeBASERateProductPmnt" escape="false" /></font>
		</td>	

	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Deal Purpose:</b></font><br>
			<font size=2><jato:text name="stDIDealPurpose" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>LOB:</b></font><br>
			<font size=2><jato:text name="stDILOB" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Deal Type:</b></font><br>
			<font size=2><jato:text name="stDIDealType" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Product:</b></font><br>
			<font size=2><jato:text name="stProduct" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top  colspan=2>
			<font size=2 color=3366cc><b>Prepayment Option:</b></font><br>
			<font size=2><jato:text name="stPrePaymentOption" escape="true" /></font>
		</td>
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Reference Type:</b></font><br>
			<font size=2><jato:text name="stReferenceType" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Reference #:</b></font><br>
			<font size=2><jato:text name="stReferenceNo" escape="true" formatType="decimal" formatMask="###0; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Est. Closing Date:</b></font><br>
			<font size=2><jato:text name="stEstimatedClosingDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Actual Closing Date:</b></font><br>
			<font size=2><jato:text name="stActualClosingDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Privilege Payment Option:</b></font><br>
			<font size=2><jato:text name="stPrivilegePaymentOption" escape="true" /></font>
		</td>

	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Reference Source App #:</b></font><br>
			<font size=2><jato:text name="stReferenceSourceApp" escape="true" /></font>
		</td>
	</tr>
<jato:tiledView name="RepeatedSOB" type="mosApp.MosSystem.pgDealSummaryRepeatedSOBTiledView">
	<tr>
		<td colspan="6" valign="top"><font size="3" color="3366cc"><b>Source
		Information&nbsp;<jato:text name="stSourceNumber" escape="true" /></b></font></td>
	</tr>
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Source:</b></font><br>
			<font size=2><jato:text name="stSource" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Source Firm:</b></font><br>
			<font size=2><jato:text name="stUWSourceFirm" escape="true" /></font>
		</td>
	</tr>	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Source Address:</b></font><br>
			<font size=2><jato:text name="stUWSourceAddressLine1" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Source Phone:</b></font><br>
			<font size=2><jato:text name="stUWSourceContactPhone" escape="true" formatType="string" formatMask="(???)???-????" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Source Fax:</b></font><br>
			<font size=2><jato:text name="stUWSourceFax" escape="true" formatType="string" formatMask="(???)???-????" /></font>
		</td>
	</tr>
	<tr>
		<!-- contactEmailAddress -->
		<td valign="top" colspan="1">
			<font size="2" color="3366cc"><b>Email:</b></font><br>
			<jato:text name="stContactEmailAddress" />
		</td>
		<td valign="top" colspan="1">
			<font size="2" color="3366cc"><b>License Registration Number:</b></font><br>
			<jato:text name="stLicenseNumber" />
		</td>
		<!-- psDescription -->
		<td valign="top">
			<font size="2" color="3366cc"><b>Status:</b></font><br>
			<jato:text name="stPsDescription" />
		</td>
		<!-- systemTypeDescription -->
		<td valign="top">
			<font size="2" color="3366cc"><b>System Type:</b></font><br>
			<jato:text name="stSystemTypeDescription" />
		</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr><td>&nbsp;</td>
	</tr>
</jato:tiledView>
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Cross-sell:</b></font><br>
			<font size=2><jato:text name="stCrossSell" escape="true" /></font>
		</td>
		<td valign=top colspan=2>
		<jato:text name="stIncludeFinancingProgramStart" escape="false" />
			<font size=2 color=3366cc><b>Financing Program:</b></font><br>
			<font size=2><jato:text name="stFinancingProgram" escape="true" /></font>
		<jato:text name="stIncludeFinancingProgramEnd" escape="false" />
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Affiliation Program:</b></font><br>
			<font size=2><jato:text name="stAffiliationProgram" /></font>
		</td>
	</tr>

	<!-- ========== DJ#725 begins ========== -->
	<!-- by Neil at Nov/30/2004 -->
	<tr>
		<td>&nbsp;</td>
	</tr>

	<!-- ========== DJ#725 ends ========== -->
				
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Total Purchase Price:</b></font><br>
			<font size=2><jato:text name="stTotalPurchasePrice" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Total Estimated Value:</b></font><br>
			<font size=2><jato:text name="stTotalEstimatedValue" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Total Actual Appraised Value:</b></font><br>
			<font size=2><jato:text name="stTotalActualAppraisedValue" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Lender:</b></font><br>
			<font size=2><jato:text name="stLender" escape="true" /></font>
		</td>
		<td valign=top colspan=2>
			<font size=2 color=3366cc><b>Investor:</b></font><br>
			<font size=2><jato:text name="stInvestor" escape="true" /></font>
		</td>
	</tr>	
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
	
		<!-- PPI start -->
	<jato:text name="stDealPurposeTypeHiddenStart" escape="false"/>
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Value of Improvements:</b></font><br>
			<font size=2><jato:text name="stRefiImprovementValue" escape="true" formatType="currency" formatMask="$ #,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>As Improved Value:</b></font><br>
			<font size=2><jato:text name="stImprovedValue" escape="true" formatType="currency" formatMask="$ #,##0.00; (-#)" /></font>

	    </td>
	</tr>
	
	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
    <jato:text name="stDealPurposeTypeHiddenEnd" escape="false"/>
	<!-- PPI end -->
	
</table>
<!-- End Of Deal Information-->

			<!-- Start Of Bridge Details-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

<jato:text name="stBeginHideBridgeSection" fireDisplayEvents="true" escape="true" />
	
	<tr>
		<td colspan=8>
			<font color=3366cc><b>Bridge Details:</b></font>
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Bridge Purpose</b></font><br>
			<font size=2><jato:text name="stBridgePurpose" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Lender</b></font><br>
			<font size=2><jato:text name="stBridgeLender" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Product</b></font><br>
			<font size=2><jato:text name="stBridgeProduct" fireDisplayEvents="true" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Payement Term Description</b></font><br>
			<font size=2><jato:text name="stBridgePaymentTermDesc" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Rate Code</b></font><br>
			<font size=2><jato:text name="stBridgeRateCode" fireDisplayEvents="true" escape="true" /></font>
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Posted Interest Rate</b></font><br>
			<font size=2><jato:text name="stBridgePostedInterestRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Rate Date</b></font><br>
			<font size=2><jato:text name="stBridgeRateDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Discount</b></font><br>
			<font size=2><jato:text name="stBridgeDiscount" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Premium</b></font><br>
			<font size=2><jato:text name="stBridgePremium" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Net Rate</b></font><br>
			<font size=2><jato:text name="stBridgeNetRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /></font>
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Bridge Loan Amount</b></font><br>
			<font size=2><jato:text name="stBridgeLoanAmount" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Actual Payment Term</b></font><br>
			<font size=2><jato:text name="stBridgeActualPaymentTerm" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Marturity Date</b></font><br>
			<font size=2><jato:text name="stBridgeMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
		</td>
	</tr>
	<tr>
		<td colspan=8>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

<jato:text name="stEndHideBridgeSection" fireDisplayEvents="true" escape="true" />
</table>


<!-- End Of Bridge Details-->

<!-- Start Of Refinance Info Sectiom-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	
<jato:text name="stBeginHideRefiSection" fireDisplayEvents="true" escape="true" />

	<tr>
		<td colspan=5></td>
	</tr>
	
	<tr>
		<td colspan=5>
			<font color=3366cc><b>Equity Take-Out/Switch/Refinance Information:</b></font>
		</td>
	</tr>
	
	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Date Purchased</b></font><br>
			<font size=2><jato:text name="stRefiPurchaseDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Mortgage Holder</b></font><br>
			<font size=2><jato:text name="stRefiMortgageHolder" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Original Purchase Price</b></font><br>
			<font size=2><jato:text name="stRefiOrigPurchasePrice" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Value of Improvements</b></font><br>
			<font size=2><jato:text name="stRefiImprovementValue" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Blended Amorization</b></font><br>
			<font size=2><jato:text name="stRefiBlendedAmorization" fireDisplayEvents="true" escape="true" /></font>
		</td>
	</tr>

	<tr>
		<td valign=top>
			<font size=2 color=3366cc><b>Purpose</b></font><br>
			<font size=2><jato:text name="stRefiPurpose" escape="true" /></font>
		</td>
		<%--
			/***************MCM Impl team changes starts - XS_16.8 *******************/
		--%>
		<td valign=top>
			<font size=2 color=3366cc><b>Existing Account Reference</b></font><br>
			<font size=2><jato:text name="stRefExistingMTGNumber" escape="true" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Original Mtg. Amount</b></font><br>
			<font size=2><jato:text name="stRefiOrigMtgAmount" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<td valign=top>
			<font size=2 color=3366cc><b>Outstanding Mtg. Amount</b></font><br>
			<font size=2><jato:text name="stRefiOutstandingMtgAmount" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)" /></font>
		</td>
		<%-- Release3.1 Apr 12, 2006 begins --%>
		<td valign=top>
		    <font color=#3366cc size=2><b>Product Type:</b></font><br>
			<font size=2><jato:text name="stRefiProductType" fireDisplayEvents="false" escape="true" /></font>
		</td>
		<%-- Release3.1 Apr 12, 2006 ends --%>
	</tr>
	
	<tr>
		<td valign=top colspan=5>
			<font size=2 color=3366cc><b>Improvements</b></font><br>
			<font size=2><jato:text name="stRefiImprovementDesc" escape="true" /></font>
		</td>
	</tr>
	<tr>
		<td valign=top colspan=5>
			<font size=2 color=3366cc><b>Additional Details</b></font><br>
			<font size=2><jato:text name="stRefiAdditionalInformation" escape="false" /></font>
		</td>
	</tr>
		<%--
		/***************MCM Impl team changes ends - XS_16.8 *******************/
		--%>
	<tr>
		<td colspan=5>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
		</td>
	</tr>

	<tr>
		<td colspan=5>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>

<jato:text name="stEndHideRefiSection" fireDisplayEvents="true" escape="true" />
</table>
<!-- End Of Refinance Info Section -->


<%--
/***************MCM Impl team changes starts - XS_16.7 ************************/
--%>
<!--  Start of component information section -->
<jato:text name="stIncludeComponentInfoSectionStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
		<td colspan=10>
			<font color=3366cc><b>Component Information:</b></font>
		</td>
</tr>

	<%--
		/***************Component Summary Section Starts ************************/
	--%>
	<tr>
	<td width="14%"><font color=3366cc><b>Summary:</b></font></td>
	<td width="5%"><font  size=2 color=3366cc><b>Total Amount:</font></td>
	<td width="9%"><font  size=2 color=3366cc><b>Unallocated Amount:</font></td>

	<td width="18%"><font  size=2 color=3366cc><b>Total Cashback Amount:</font></td>
	</tr>
	<tr>
	<td width="14%"></td>
	<td width="10%"><font size=2><jato:text name="stTotalAmount" formatType="currency" formatMask="#,##0.00; (-#)" escape="true" /></font></td>
	<td width="10%"><font size=2><jato:text name="stUnAllocatedAmount" escape="false" formatType="currency" formatMask="#,##0.00;"/></font></td>
	<td><font size=2><jato:text name="stTotalCashBackAmount" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
	</tr>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>	
<tr>
<td colspan=10>
<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
</td>
</tr>
<tr>
<td  width="14%"><font  size=2 color=3366cc><b>Type:</font></td>
<td  width="20%"><font  size=2 color=3366cc><b>Product:</font></td>
<td  width="10%"><font  size=2 color=3366cc><b>Amount:</font></td>
<td width="10%"><font  size=2 color=3366cc><b>Include MI Premium:</font></td>
<td width="10%"><font  size=2 color=3366cc><b>Payment Amount:</font></td>
<td width="10%"><font  size=2 color=3366cc><b>Allocate Tax Escrow:</font></td>
<td width="10%"><font  size=2 color=3366cc><b>Net Rate:</font></td>
<td width="8%"><font  size=2 color=3366cc><b>Discount:</font></td>
<td width="8%"><font  size=2 color=3366cc><b>Premium:</font></td>
<td><font  size=2 color=3366cc><b>Buy Down Rate:</font></td>
</tr>	

<tr>
		<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
<%/*Mortgage Component*/ %>
<jato:tiledView name="RepeatedMortgageComponents" type="mosApp.MosSystem.pgDealSummaryComponentMortgageTiledView">
<tr>
<td ><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stAllocateMIPremium" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPaymentAmount" formatType="currency" formatMask="#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stAllowTaxEscrow" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="###0.000; -#" /></font></td>
<td ><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
<td><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
<td><font size=2><jato:text name="stBuyDownRate" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

</jato:tiledView>

<%/*LOC COMPONENT*/ %>
<jato:tiledView name="RepeatedLocComponents" type="mosApp.MosSystem.pgDealSummaryComponentLocTiledView">
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stAllocateMIPremium" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stPaymentAmount" formatType="currency" formatMask="#,##0.00; (-#)" escape="true" /></font></td>
<td><font size=2><jato:text name="stAllowTaxEscrow" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stNetRate"  escape="true" formatType="decimal" formatMask="###0.000; -#" /></font></td>
<td><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
<td><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>


		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

</jato:tiledView>
<%/*CREDIT CARD COMPONENT*/ %>
<jato:tiledView name="RepeatedCreditCardComponents" type="mosApp.MosSystem.pgDealSummaryComponentCreditCardTiledView">
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="#,##0.00; (-#)" escape="true" /></font></td>
<td></td>
<td></td>
<td></td>
<td><font size=2><jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="###0.000; -#" /></font></td>
<td></td>
<td></td>
<td></td>

		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
</jato:tiledView>
<%/*LOAN COMPONENT*/ %>
<jato:tiledView name="RepeatedLoanComponents" type="mosApp.MosSystem.pgDealSummaryComponentLoanTiledView">
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="#,##0.00; (-#)" escape="true" /></font></td>
<td></font></td>
<td><font size=2><jato:text name="stTotalPayment" formatType="currency" formatMask="#,##0.00; (-#)" escape="true" /></font></td>
<td></font></td>
<td><font size=2><jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="###0.000; -#" /></font></td>
<td><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
<td><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="###0.000;-#" escape="true" /></font></td>
<td></td>

		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>

</jato:tiledView>
<%/*OVER DRAFT COMPONENT*/ %>
<jato:tiledView name="RepeatedOverDraftComponents" type="mosApp.MosSystem.pgDealSummaryComponentOverDraftTiledView">
<tr>
<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
<td><font size=2><jato:text name="stAmount" formatType="currency" formatMask="#,##0.00; (-#)" escape="true"/></font></td>
<td></td>
<td></td>
<td></td>
<td><font size=2><jato:text name="stNetRate" escape="true" formatType="decimal" formatMask="###0.000; -#" /></font></td>
<td></td>
<td></td>
<td></td>
		<tr>
			<td colspan=10>
				<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
			</td>
		</tr>
</jato:tiledView>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<jato:text name="stIncludeComponentDetailsButtonStart" escape="false" />
<td align="right"><jato:button name="btComponentDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/component_detail.gif" /></td>
<jato:text name="stIncludeComponentDetailsButtonEnd" escape="false" />
</tr>
<tr>
	<td colspan=10>
			<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
	</td>
</tr>
	

</table>
<jato:text name="stIncludeComponentInfoSectionEnd" escape="false" />
<!--  End of the componentinfo section-->


<%--
/***************MCM Impl team changes ends - XS_16.7***************************/
--%>


<!-- Include Part 2 of DealSummary --//-->
<jsp:include page="pgDealSummary_part2.jsp" />


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.dealsumborr.top=100;
	document.dealsumasst.top=100;
	document.dealsuminc.top=100;
	document.dealsumliab.top=100;
	document.dealsumprop.top=100;
	document.gdstds.top = 100;
	document.alertbody.top=100;
}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(4)
}
else{
tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
    </script>

<!--END BODY//-->
<br><jato:hidden name="sessionUserId" />

<jato:hidden name="hdExecuteEscow" />

<SCRIPT LANGUAGE="JavaScript">
toggleQualifyProduct();
toggleQualifyPencil();
</script>
</jato:form>
</body>
</jato:useViewBean>
</HTML>
