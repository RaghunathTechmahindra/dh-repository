
<HTML>
<%@page info="pgPartyReview" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgPartyReviewViewBean">

<HEAD>
<TITLE>Party Review</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--
//======================== Local JavaScript (begin) ===========================
function submitenter(e)
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;

  if (keycode == 13)
     {
     	//document.pgPartyReview.action = '../MosSystem/pgPartyReview.btModifyParty_onWebEvent(btModifyParty)';
     	document.pgPartyReview.action += "?<%= viewBean.PAGE_NAME%>_btModifyParty="; 
	  	document.pgPartyReview.submit();
	  }
     return true;
}
//======================== Local JavaScript (end) ===========================
//-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff onKeyPress="return submitenter(event);">
<jato:form name="pgPartyReview" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 
	<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>

<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff></td>
	<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
	<td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td><tr>
	<td colspan=8>&nbsp;</td><tr>
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=1 valign=top><font size=2 color=3366cc><b>Party Type:</b></font><br>
	<font size=2><jato:text name="stPartyType" escape="true" /></font></td>

	<td colspan=1 valign=top><font size=2 color=3366cc><b>Party Status:</b></font><br>
	<font size=2><jato:text name="stPartyStatus" escape="true" /></font></td>

	<td colspan=1 valign=top><font size=2 color=3366cc><b><jato:text name="stPartyShortNameLabel" escape="true" /></b></font><br>
	<font size=2><jato:text name="stPartyShortName" escape="true" /></font></td>

	<td colspan=2 valign=top><font size=2 color=3366cc><b><jato:text name="stClientNumberLabel" escape="true" /></b></font><br>
	<font size=2><jato:text name="stClientNumber" escape="true" /></font></td>
	
	<tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	<tr>

	<tr>
		<td valign=top rowspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td colspan=2 valign=top rowspan="3"><font size=2 color=3366cc><b>Special Notes:</b></font><br>
	    	<jato:textArea name="tbNotes" extraHtml="readonly" rows="5" cols="51" />
	    </td>
        
        <td colspan=1 valign=top>
        	<jato:text name="stIncludeBranchTransitInfoStart" escape="false" />
        	<font size=2 color=3366cc><b>GE Branch#:</b></font><br>
        	<font size=2><jato:text name="stBranchTransitNumGE" escape="true" />&nbsp;</font>
        	<jato:text name="stIncludeBranchTransitInfoEnd" escape="false" />
        </td>
        <td colspan=2 valign=top>
        	<jato:text name="stIncludeBranchTransitInfoStart" escape="false" />
        	<font size=2 color=3366cc><b>CMHC Branch#:</b></font><br>
        	<font size=2><jato:text name="stBranchTransitNumCMHC" escape="true" />&nbsp;</font>
        	<jato:text name="stIncludeBranchTransitInfoEnd" escape="false" />
        </td>

	</tr>
	<tr>
		<td colspan=1 valign=top>
			<jato:text name="stIncludeBankInfoStart" escape="false" />
			<font size=2 color=3366cc><b>Bank and Transit Number:</b></font><br>
        	<font size=2><jato:text name="stBankTransitNum" escape="true" />&nbsp;</font>
        	<jato:text name="stIncludeBankInfoEnd" escape="false" />
		</td>
		<td colspan=2 valign=top>
			<jato:text name="stIncludeBankInfoStart" escape="false" />
			<font size=2 color=3366cc><b>Bank Account Number:</b></font><br>
        	<font size=2><jato:text name="stBankAccNum" escape="true" />&nbsp;</font>
        	<jato:text name="stIncludeBankInfoEnd" escape="false" />
		</td>
	</tr>
	<tr>
		<td colspan=3 valign=top>
		<jato:text name="stIncludeBankInfoStart" escape="false" />
		<font size=2 color=3366cc><b>Bank Name:</b></font><br>
        <font size=2><jato:text name="stBankName" escape="true" />&nbsp;</font>
        <jato:text name="stIncludeBankInfoEnd" escape="false" />
		</td>
	</tr>
	
	<tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	<tr>

    <tr>
       	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>        
        <td colspan=2 valign=top align=left><font size=2 color=3366cc><b>Party Name:</b></font><br>
        <font size=2>
        <jato:text name="stPartyName" escape="true" /></font></td>
	       
        <td colspan=4 valign=top align=left><font size=2 color=3366cc><b>Company Name:</b></font><br>
        <font size=2>
        <jato:text name="stPartyCompanyName" escape="true" /></font></td>
    </tr>
    <tr>
		<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	</tr>
	
</table>
	
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
	<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>

<!--START CONTACT INFORMATION//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

	<td colspan=4 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Contact Information</b></font></td><tr>
	<td colspan=4>&nbsp;</td>
	<tr>
	<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign=top><font size=2 color=3366cc><b>Contact Name:</b></font><br>
		<font size=2><jato:text name="stPartyFirstName" escape="true" /> <jato:text name="stPartyMiddleInitial" escape="true" /> <jato:text name="stPartyLastName" escape="true" /></font>
	</td>
	<tr>
	<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	<tr>
	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Work Phone #:</b></font><br>
		<font size=2><jato:text name="stWorkPhoneNumber" escape="true" /></font>
	</td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Fax #:</b></font><br>
		<font size=2><jato:text name="stFaxNumber" escape="true" /></font>
	</td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>E-mail Address:</b></font><br>
		<font size=2><jato:text name="stEmail" escape="true" /></font>
	</td>
	<td valign=top colspan=1><font size=2 color=3366cc><b>Preferred Delivery Method:</b></font><br>
		<font size=2><jato:text name="stPrefDeliveryMethod" escape="true" /></font>
	</td>	
	<tr>
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=10 alt="" border="0"></td><tr>
	<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
	<td colspan=6><img src="../images/light_blue.gif" width=1 height=10 alt="" border="0"></td><tr>

	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign=top><font size=2 color=3366cc><b>Address Line 1:</b></font><br>
		<font size=2><jato:text name="stAddressLine1" escape="true" /></font>
	</td>
			
	<td valign=top colspan=2><font size=2 color=3366cc><b>Address Line 2:</b></font><br>
		<font size=2><jato:text name="stAddressLine2" escape="true" /></font>
	</td>
	<tr>

	<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			
	<td valign=top><font size=2 color=3366cc><b>City:</b></font><br>
		<font size=2><jato:text name="stCity" escape="true" /></font>
	</td>
			
	<td valign=top><font size=2 color=3366cc><b>Province:</b></font><br>
		<font size=2><jato:text name="stProvince" escape="true" /></font>
	</td>
			
	<td valign=top><font size=2 color=3366cc><b>Postal Code:</b></font><br>
		<font size=2><jato:text name="stPostalCodeFSA" escape="true" /> <jato:text name="stPostalCodeLDU" escape="true" /></font>
	</td>
	<tr>

	<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

	<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	
	<td valign=top><font size=2 color=3366cc><b>Location:</b></font><br>
			<font size=2><jato:text name="stLocation" escape="true" /></font>
	</td>
	
	<tr>

	<td colspan=4><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td>
</table>
<p>
<table border=0 width=100%>
<td align=right><jato:button name="btModifyParty" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/edit.gif" />&nbsp;&nbsp;<jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" /></td>
</table>
</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR><jato:hidden name="hdPartyId" />
<BR><jato:hidden name="hdPartyTypeId" />
<BR><jato:hidden name="hdPartyStatusId" />


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
