<%--
 /*
  * MCM Impl Team. Change Log
  * XS_2.55 -- July 24, 2008  -- Added Component Information Section
  * XS_2.57 -- Aug 12, 2008, added French translation.
  * artf763316 Aug 21, 2008 : replacing XS_2.60.  
 */
  --%>

<!--PAGE BODY//-->

<!--START PROGRESS ADVANCE//-->
 <jato:hidden name="hdForceProgressAdvance" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Avance progressive</b></font></td><tr>
<td colspan=5>&nbsp;</td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Avance progressive:</b></font></td>
<td valign=top><font size="2">
<jato:radioButtons name="rbProgressAdvance" layout="horizontal" extraHtml="onClick='hideSubProgressAdvance();'" /></font></td><%--Release3.1 extraHtml--%>
<td valign=top><font size=2 color=3366cc><b>Montant de la prochaine avance:</b></font></td>
<td valign=top>$ 
<jato:textField name="txNextAdvanceAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Montant de l'avance � ce jour:</b></font></td>
<td valign=top>$ 
<jato:textField name="txAdvanceToDateAmt" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Num�ro de l'avance:</b></font></td>
<td valign=top>
<jato:textField name="txAdvanceNumber" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td><tr>
</table>
<!--END PROGRESS ADVANCE//-->

<!--START OTHER DETAILS//-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Autres d�tails</b></font></td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td>&nbsp;</td>
<td valign=top></td>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b><jato:text name="stBranchTransitLabel" escape="false" fireDisplayEvents="true" /></b></font></td>
<td valign=top><font size=2><jato:text name="stBranchTransitNumber" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stPartyName" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stAddressLine1" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stCity" escape="false" fireDisplayEvents="true" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>But de la demande:</b></font></td>
<td valign=top><jato:combobox name="cbUWDealPurpose" extraHtml="onChange='DisplayOrHideRefiSection();'" /></td>
<td valign=top><font size=2 color=3366cc><b>Succursale:</b></font></td>
<td valign=top><jato:combobox name="cbUWBranch" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Type de la demande:</b></font></td>
<td valign=top><jato:combobox name="cbUWDealType" /></td>
<td valign=top><font size=2 color=3366cc><b>Autres produits:</b></font></td>
<td valign=top><jato:combobox name="cbUWCrossSell" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Traitement sp�cial:</b></font></td>
<td valign=top><jato:combobox name="cbUWSpecialFeature" /></td>
<%-- LEN497489 --%>
<tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Prix d'achat estimatif pr�autoris�:</b></font></td>
<td valign=top>$ 
<jato:textField name="txUWPreAppEstPurchasePrice" formatType="currency" formatMask="###0.00; (-#)" size="16" maxLength="16" /></td>
<td valign=top><font size=2 color=3366cc><b>Type de r�f�rence:</b></font></td>
<td valign=top><jato:combobox name="cbUWReferenceType" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Type de remboursement:</b></font></td>
<td valign=top><jato:combobox name="cbUWRepaymentType" /></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de r�f�rence de la demande:</b></font></td>
<td valign=top>
<jato:textField name="txUWReferenceDealNo" formatType="decimal" formatMask="###0; (-#)" size="10" maxLength="10" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Deuxi�me hypoth�que existante?</b></font></td>
<td valign=top><jato:combobox name="cbUWSecondMortgageExist" /></td>
<td valign=top><font size=2 color=3366cc><b>Date pr�vue de cl�ture:</b></font></td>
<td valign=top><font size=2>Mois: </font><jato:combobox name="cbUWEstimatedClosingDateMonth" /> <font size=2>Jour: </font>
<jato:textField name="txUWEstimatedClosingDateDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>An: </font>
<jato:textField name="txUWEstimatedClosingDateYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></td><tr>

<td>&nbsp;</td>
<td valign=top><jato:text name="stIncludeFinancingProgramStart" escape="false" /><font size=2 color=3366cc><b>Programme de financement:</b></font><jato:text name="stIncludeFinancingProgramEnd" escape="false" /></td>
<td valign=top><jato:text name="stIncludeFinancingProgramStart" escape="false" /><jato:combobox name="cbUWFinancingProgram" /><jato:text name="stIncludeFinancingProgramEnd" escape="false" /></td>

<td valign=top><font size=2 color=3366cc><b>Date du premier paiement:</b></font></td>
<td valign=top><font size=2>Mois: </font><jato:combobox name="cbUWFirstPaymetDateMonth" /> <font size=2>Jour: </font>
<jato:textField name="txUWFirstPaymetDateDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>An: </font>
<jato:textField name="txUWFirstPaymetDateYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Payeur de taxes:</b></font></td>
<td valign=top><jato:combobox name="cbUWTaxPayor"  elementId="cbTaxPayor" onChange="flipChIncludeTaxPayments(this)"/></td>
<td valign=top><font size=2 color=3366cc><b>Date d'�ch�ance:</b></font></td>
<td valign=top><font size=2><jato:text name="stUWMaturityDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Taux immobilis�?</b></font></td>
<td valign=top><jato:combobox name="cbUWRateLockedIn" /></td>
<td valign=top><font size=2 color=3366cc><b>Date de retour pr�vue de l'offre d'engagement:</b></font></td>
<td valign=top><font size=2>Mois: </font><jato:combobox name="cbUWCommitmentExpectedDateMonth" /> <font size=2>Jour: </font>
<jato:textField name="txUWCommitmentExpectedDateDay" size="2" maxLength="2" /> <font size=2>An: </font>
<jato:textField name="txUWCommitmentExpectedDateYear" size="4" maxLength="4" />
</td></tr>

    <%--CR03 start --%>
    <tr>
		<td>&nbsp;</td>
		<td valign="top">&nbsp;</td>
		<td valign="top">&nbsp;</td>
		<td valign="top"><font size="2" color="3366cc"><b>Calculer automatiquement la date d'expiration de l'offre:</b></font></td>
		<td valign="top"><font size="2"> 
			<jato:radioButtons name="rbAutoCalculateCommitmentDate" layout="horizontal" extraHtml="onClick='changeExpirationDate();'" />
			</font></td>
	</tr>
	<tr>
		<td>&nbsp;<jato:hidden name="hdCommitmentExpiryDate" />
				<input type="hidden" id="hdCommitmentExpiryDate" value='<jato:text name="hdCommitmentExpiryDate"/>'/>
				<jato:hidden name="hdMosProperty" />
				<input type="hidden" id="hdMosProperty" value='<jato:text name="hdMosProperty"/>'/></td>
				<td valign="top"><font size="2" color="3366cc"><b>Date de retrait de la condition de financement:</b></font></td>
				<td valign="top"><font size="2">Mois: </font><jato:combobox
					name="cbUWFinancingWaiverDateMonth" extraHtml="id='cbUWFinancingWaiverDateMonth'" /> <font size="2">Jour: </font>
				<jato:textField name="txUWFinancingWaiverDateDay" size="2"
					maxLength="2" extraHtml="id='txUWFinancingWaiverDateDay'"/> <font size="2">An: </font> <jato:textField
					name="txUWFinancingWaiverDateYear" size="4" maxLength="4" extraHtml="id='txUWCFinancingWaiverDateYear'"/></td>
		<td valign="top"><font size="2" color="3366cc"><b>Date d'expiration de l'offre d'engagement:</b></font></td>
		<td valign="top"><font size="2">Mois: </font><jato:combobox
			name="cbUWCommitmentExpirationDateMonth" extraHtml="id='cbUWCommitmentExpirationDateMonth'" /> <font size="2">Jour: </font>
		<jato:textField name="txUWCommitmentExpirationDateDay" size="2"
			maxLength="2" extraHtml="id='txUWCommitmentExpirationDateDay'"/> <font size="2">An: </font> <jato:textField
			name="txUWCommitmentExpirationDateYear" size="4" maxLength="4" extraHtml="id='txUWCommitmentExpirationDateYear'"/></td>
	</tr>
          <%--CR03 end --%>
	<tr>



<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b><jato:text name="stMarketTypeLabel" escape="false" fireDisplayEvents="true" /></b></font></td>
<td valign=top><font size=2><jato:text name="stMarketType" escape="false" fireDisplayEvents="true" /></font></td>
<%--***** Change by NBC Impl. Team - Version 1.1 - Start*****--%>			
<td valign="top"><font size="2" color="3366cc"><b>Programme d'affiliation</b></font></td>
<td valign="top"><jato:combobox name="cbAffiliationProgram" extraHtml="onChange='DisplayOrHideRefiSection();'" /></td>
<%--***** Change by NBC Impl. Team - Version 1.1 - End*****--%>
<tr>
                                                                                                   
<td>&nbsp;</td>
<td colspan=4><jato:button name="btUWPartySummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/party_summary_fr.gif" /></td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td><tr>
</table>

<!--END OTHER DETAILS//-->


<!--START Refinance Info.//-->
<div id="refisection" class="refisection" name="refisection">
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;D�tails de l'hypoth�que sur plus value/Transfert/Refinancement</b></font></td><tr>
<td colspan=5>&nbsp;</td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Date d'achat:</b></font></td>
<td valign=top><font size=2>Mois: </font><jato:combobox name="cbUWRefiOrigPurchaseDateMonth" /> <font size=2>Jour: </font>
<jato:textField name="txUWRefiOrigPurchaseDateDay" size="2" maxLength="2" /> <font size=2>An: </font>
<jato:textField name="txUWRefiOrigPurchaseDateYear" size="4" maxLength="4" /></td>
<td valign=top><font size=2 color=3366cc><b>But:</b></font></td>
<td valign=top>
<jato:textField name="txUWRefiPurpose" size="35" maxLength="80" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Pr�teur:</b></font></td>
<td valign=top>
<jato:textField name="txUWRefiMortgageHolder" size="35" maxLength="80" /></td>
<td valign=top><font size=2 color=3366cc><b>Amortissement mixte:</b></font></td>
<td valign=top><font size=2>
<jato:radioButtons name="rbBlendedAmortization" layout="horizontal" /></font></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Prix d'achat original:</b></font></td>
<td valign=top>$ 
<jato:textField name="txUWRefiOrigPurchasePrice" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Montant de l'hypoth�que originale:</b></font></td>
<td valign=top>$ 
<jato:textField name="txUWRefiOrigMtgAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Valeur des am�liorations:</b></font></td>
<td valign=top>$ 
<jato:textField name="txUWRefiImprovementValue" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Montant de l'hypoth�que impay�:</b></font></td>
<td valign=top>$ 
<jato:textField name="txUWRefiOutstandingMtgAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>

<%--
/***************MCM Impl team changes starts - XS_2.57 (French translation of XS_2.46) *******************/
--%>            
            <tr>

                <td>&nbsp;</td>
                <td valign="top"><font size="2" color="3366cc"><b>Am�liorations:</b></font></td>
<%-- Release3.1 Apr 18, 2006 begins --%>
                <td valign="top"><jato:textField name="txUWRefiImprovementsDesc" size="35" maxLength="80" /></td>
                <TD vAlign=top><FONT color=#3366cc size=2><B>R�f�rence au compte existant:</B></FONT></TD>
                <td valign="top"><jato:textField name="txUWReferenceExistingMTGNo" size="35"  maxLength="35"/></td>
<%-- Release3.1 Apr 18, 2006 ends / LEN497489 --%>

                <td colspan="5"><img src="../images/light_blue.gif" width="1"
                    height="2" alt="" border="0"></td>
            </tr>
            <tr>

                <td>&nbsp;</td>
                <td valign="top"><font size="2" color="3366cc"><b>Type de Produit:</b></font></td>
                <td valign="top"><jato:combobox name="cbRefiProductType" /></td>
                <TD vAlign=top><FONT color=#3366cc size=2><B>&nbsp;</B></FONT></TD>
                <td valign="top">&nbsp;</td>

                <td colspan="5"><img src="../images/light_blue.gif" width="1"
                    height="2" alt="" border="0"></td>
            </tr>
            <tr>

                <td>&nbsp;</td>
                <td valign="top"><font size="2" color="3366cc"><b>D�tails suppl�mentaires:</b></font></td>
                <td vAlign=top colspan=3>
                    <jato:textArea name="txRefiAdditionalInformation" rows="4" cols="122" 
      extraHtml="wrap=soft maxlength=512 onchange='return isFieldHasSpecialChar();'"/>
                </td>

                <td colspan="5"><img src="../images/light_blue.gif" width="1"
                    height="2" alt="" border="0"></td>
            </tr>
<%--
/***************MCM Impl team changes ends - XS_2.57 (French translation of XS_2.46) *******************/
--%>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td><tr>
</table>
</div>
<!--END Refinance Info.//-->

<%--
***************MCM Impl team changes Starts - XS_2.55 *******************/
--%>
	<!-- START Component Summary Info -->
	<div id="divComponentInfo">
	<table border=0 width=100% cellpadding=0 cellspacing=0>
		<tr>
			<jato:containerView name="componentInfoPagelet" type="mosApp.MosSystem.pgComponentInfoPageletViewBean">
				<jsp:include page="pgComponentInfoPagelet_fr.jsp"/>
			</jato:containerView>
		</tr>
	</table>
	</div>
	<!-- End Component Summary Info -->
<%--
***************MCM Impl team changes Starts - XS_2.55 *******************/
--%>



<!--START MORTGAGE INSURANCE//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Assurance pr�t-hypoth�caire</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Indicateur d'assurance:</b></font><br><jato:combobox name="cbUWMIIndicator" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Statut de l'assurance</b></font><br><jato:combobox name="cbUWMIStatus" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>N<sup>o</sup> de police de l'assurance existant:</b></font><br>
<jato:textField name="txUWMIExistingPolicy" size="35" maxLength="35" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Type d'assureur</b></font><br>
	<jato:combobox name="cbUWMIInsurer" elementId="cbMIInsurer" extraHtml="onChange=SetMIPolicyNoDisplay(); hideRequestStandardService();" /><%--Release3.1 extraHtml--%>
</td>
<td valign=top><font size=2 color=3366cc><b>Type d'assurance:</b></font><br>
    <jato:combobox name="cbUWMIType" elementId="cbMIType" extraHtml="onChange=hideRequestStandardService();" /><%--Release3.1 extraHtml--%>
</td>
<td valign=top><font size=2 color=3366cc><b>Payeur de l'assurance:</b></font><br>
	<jato:combobox name="cbUWMIPayor"  onChange="selectMIUpfront()" elementId="cbMIPayor" /></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> du certificat de pr�qualification:</b></font><br><font size="2">
<jato:textField name="txUWMIPreQCertNum" size="25" maxLength="50" /></font></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Prime d'assurance:</b></font><br>$ 
<jato:textField name="txUWMIPremium" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de certificat:</b></font><br><font size="2">
<jato:textField name="txUWMICertificate" size="25" maxLength="50" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Frais pr�lev�s � l'acquisition d'assurance</b></font><br><font size=2>
<jato:radioButtons name="rbUWMIUpfront" layout="horizontal" styleClass="rbMIUpfront" onClick="flipChIncludeMIPremiums(this)"/></font></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Intervention du souscripteur hypoth�caire requise</b></font><br><font size=2>
<jato:radioButtons name="rbUWMIRUIntervention" layout="horizontal" /></font></td>
</tr>

			<!-- SEAN DJ SPEC-Progress Advance Type July 26, 2005:  -->
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
			  	<td valign="top">&nbsp;</td>
				<td valign="top">
<div id="divProgressAdvanceType" name="divProgressAdvanceType">
					<font size=2 color=3366cc><b>Type d'avance �chelonn�e:</b></font><br>
					<jato:combobox name="cbProgressAdvanceType"/>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
				</td>
      <!-- SEAN DJ SPEC-PAT END -->

<%-- Release3.1 Apr 18, 2006 begins --%>

				<TD vAlign=top>
<div id="divProgressAdvanceInspectionBy" name="divProgressAdvanceInspectionBy">
					<FONT color=#3366cc size=2><B>Inspection de l'avance de progress par:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbProgressAdvanceInspectionBy" layout="horizontal" />
					</FONT>
</div>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Hypotheques REER autog�r�:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbSelfDirectedRRSP" layout="horizontal" />
					</FONT>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>L'identificateur poursuivant de produit pour SCHL:</B></FONT><BR>
					<jato:textField name="txCMHCProductTrackerIdentifier" size="5" maxLength="5" />
				</TD>
			</TR>
			<TR>
				<TD vAlign=top></TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Remboursement MCVD:</B></FONT><BR>
					<jato:combobox name="cbLOCRepaymentTypeId" />
				</TD>
				<TD vAlign=top>
<div id="divReqStdSvc" name="divReqStdSvc">	
					<FONT color=#3366cc size=2><B>Demande pour sevice de base:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbRequestStandardService" layout="horizontal" />
					</FONT>
</div>			
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Amortissement MCVD:</B></FONT><BR>
					<FONT size=2>
						 <jato:text name="stLOCAmortizationMonths" fireDisplayEvents="true" escape="true" /> 
					</FONT>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Date d'�ch�ance pour un pr�t non Amorti MCVD:</B></FONT><BR>
					<FONT size=2>
						<jato:text name="stLOCInterestOnlyMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" />
					</FONT> 
				</TD>
			</tr>				
<%-- Release3.1 Apr 18, 2006 ends --%>

<%-- 5.0 MI start --%>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <font color="#3366cc" size="2"><b>R�ponse de l'assureur:</b></font>&nbsp;
                    <jato:button name="btExpandMIResponse" src="../images/expand_fr.gif" fireDisplayEvents="true" onClick="javaScript:handleMIResponseExpand(); return false;" />
                    <br />
                    <jato:textField name="txMIResponseFirstLine" size="35" maxLength="35" readOnly="true"/></td>
                </td>
                <td colspan="3"></td>
            </tr>
<%-- 5.0 MI end --%>

<tr>
<td valign=top>&nbsp;</td>
<td colspan=5 td valign=top><jato:button name="btMIReview" extraHtml="width=215 height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/mortgage_insreview_fr.gif" style="margin-top: 30px;" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<!--END MORTGAGE INSURANCE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START MORTGAGE OPTIONS//-->
<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Options pr�t hypoth�caire</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;Multi-projets:</b></font>
<font size=2><jato:radioButtons name="rbMultiProject" layout="horizontal" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Protection propri�taire:</b></font>
<font size=2><jato:radioButtons name="rbProprietairePlus" layout="horizontal" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Ligne de cr�dit:</b></font>
<jato:textField name="txProprietairePlusLOC" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
<!--END MORTGAGE OPTIONS//-->

<!--START SOURCE INFORMATION//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=6 valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de demande de la source de r�f�rence:</b></font><br>
<jato:textField name="tbUWReferenceSourceApplNo" size="35" maxLength="35" /></td>
<jato:tiledView name="RepeatedSOB" type="mosApp.MosSystem.pgUWRepeatedSOBTiledView">	
	<tr>
		<td colspan=10><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

		<td colspan=6 valign=top><font size=3 color=3366cc><b>Information sur la source&nbsp;<jato:text name="stSourceNumber" escape="true" />
		</b></font><jato:hidden name="hdSourceOfBusinessProfileId" /></td><tr>
		<td colspan=6>&nbsp;</td><tr>

		<td valign=top><font size=2 color=3366cc><b>Firme de la source:</b></font><br>
		<font size=2><jato:text name="stUWSourceFirm" escape="true" /></font></td>
		<td valign=top><font size=2 color=3366cc><b>Source:</b></font><br>
		<font size=2><jato:text name="stSourceFirstName" escape="true" /> <jato:text name="stSourceLastName" escape="true" /></font></td>
		<td valign=top><font size=2 color=3366cc><b>Adresse de la source:</b></font><br>
		<font size=2><jato:text name="stUWSourceAddressLine1" escape="true" /> <jato:text name="stUWSourceCity" escape="true" /> <jato:text name="stUWSourceProvince" escape="true" /></font></td>
		<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�phone de la source:</b></font><br>
		<font size=2><jato:text name="stUWSourceContactPhone" escape="true" formatType="string" formatMask="(???)???-????" /> x <jato:text
					name="stUWSourceContactPhoneExt" fireDisplayEvents="true" escape="true" /></font></td>
		<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de t�l�copieur de la source:</b></font><br>
		<font size=2><jato:text name="stUWSourceFax" escape="true" formatType="string" formatMask="(???)???-????" /></font></td><tr>

		<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	</tr>
	<tr>
        <td valign="top"><font size="2" color="3366cc"><b>Num�ro d'enregistrement du permis:</b></font><br>
            <jato:text name="stLicenseNumber" />
        </td>
		<!-- contactEmailAddress -->
		<td valign="top">
			<font size="2" color="3366cc"><b>Courriel:</b></font><br>
			<font size="2"><jato:text name="stContactEmailAddress" escape="true" />
		</td>
		<!-- psDescription -->
		<td valign="top">
			<font size="2" color="3366cc"><b>Statut:</b></font><br>
			<font size="2"><jato:text name="stPsDescription" escape="true" />
		</td>
		<!-- systemTypeDescription -->
		<td valign="top">
			<font size="2" color="3366cc"><b>Syst�me:</b></font><br>
			<font size="2"><jato:text name="stSystemTypeDescription" escape="true" />
		</td>
	</tr>
	<tr>
		<td valign=top>&nbsp;<br><jato:button name="btSOBDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details_fr.gif" />
		<jato:button name="btChangeSource" extraHtml="width=104  height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/change_source_fr.gif" /></td>
	</tr>
</jato:tiledView>
<tr>

<td colspan=10><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>

</table>
<!--END SOURCE INFORMATION//-->
<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td><tr>
</table>

<!--START APPLICANT FINANCIAL DETAILS//-->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>

<td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=10><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=10 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Information financi�re du demandeur</b></font></td><tr>
<td colspan=10>&nbsp;
<!-- SEAN GECF IV Document Type Drop Down. -->
<jato:text name="stHideIVDocumentTypeStart" escape="false"/>
&nbsp;<font size=2 color=3366cc><b>Type de v�rification du revenu:</b></font> <jato:combobox name="cbIVDocumentType"/>&nbsp;&nbsp;&nbsp;&nbsp;
<jato:text name="stHideIVDocumentTypeEnd" escape="false"/>
<!-- END GECF IV Document Type Drop Down. --></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Principal?</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Nom du demandeur:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Type de demandeur:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Revenu total:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Total du passif:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Total de l'actif:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Valeur nette:</b></font></td>
<td colspan=2>&nbsp;</td><tr>
<td colspan=10><img src="../images/light_blue.gif" width=1 height=1 alt="" border="0"></td><tr>
<td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatApplicantFinancialDetails" type="mosApp.MosSystem.pgUWorksheetRepeatApplicantFinancialDetailsTiledView">
<td valign=middle>&nbsp;<jato:hidden name="hdFinancialApplicantId" /><jato:hidden name="hdFinancialApplicanCopyId" /></td>
<td valign=middle><jato:combobox name="cbUWPrimaryApplicant" extraHtml="onChange=isThereMoreThanOneYes('hdFinancialApplicantId');" /></td>
<td valign=middle><font size=2><jato:text name="stFinancialApplicanFirstName" escape="true" /> <jato:text name="stFinancialApplicanMiddleInitial" escape="true" /> <jato:text name="stFinancialApplicanLastName" escape="true" /> <jato:text name="stFinancialApplicanSuffix" escape="true" /></font></td>
<td valign=middle><font size=2><jato:text name="stFinancialApplicanType" escape="true" /></font></td>
<td valign=middle><font size=2><jato:text name="stFinancialApplicanTotalIncome" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td valign=middle><font size=2><jato:text name="stFinancialApplicanTotalLiabilities" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td valign=middle><font size=2><jato:text name="stFinancialApplicanTotalAssets" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td valign=middle><font size=2><jato:text name="stFinancialApplicantNetWorth" escape="true" formatType="currency" formatMask="fr|#,##0.00$; -#,##0.00" /></font></td>
<td valign=middle>&nbsp;<jato:button name="btDeleteApplicant" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_app_fr.gif" /></td>
<td valign=middle><jato:button name="btApplicantDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details_fr.gif" />&nbsp;<jato:button name="btLiabilitiesDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/liabilities_fr.gif" /></td><tr>
<td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
</jato:tiledView>
<!--END REPEATING ROW//-->

<td colspan=10><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td valign=top colspan="4">&nbsp;<jato:button name="btAddApplicant" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_app_fr.gif" />&nbsp;<jato:button name="btDupApplicant" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/Duplicate_fr.gif" />&nbsp;<font size=2 color=3366cc><b>Totaux combin�s:</b></font></td>
<td colspan=1><font size=2><jato:text name="stUWCombinedTotalIncome" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td colspan=1><font size=2><jato:text name="stUWCombinedTotalLiabilities" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td colspan=1><font size=2><jato:text name="stUWCombinedTotalAssets" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td colspan=1><font size=2><jato:text name="stUWTotalNetWorth" fireDisplayEvents="true" escape="true" formatType="currency" formatMask="fr|#,##0.00$; -#,##0.00$" /></font></td>
<td colspan=4>&nbsp;<jato:button name="btCreditReport" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/credit_report_fr.gif" /></td>
<tr>
<td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<!--END APPLICANT FINANCIAL DETAILS//-->


<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td><tr>
</table>

<!--START PROPERTY INFORMATION//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=8 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Information sur la propri�t�</b></font></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatPropertyInformation" type="mosApp.MosSystem.pgUWorksheetRepeatPropertyInformationTiledView">

<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
<jato:hidden name="hdPropertyId" />
<jato:hidden name="hdPropertyCopyId" /></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Principal?</b></font><br>
<jato:combobox name="cbUWPrimaryProperty" /></td>
<td valign=top colspan=4><font size=2 color=3366cc><b>Adresse de la propri�t�:</b></font><br>
<font size=2><jato:text name="stPropertyAddressStreetNumber" escape="true" /> <jato:text name="stPropertyAddressStreetName" escape="true" /> <jato:text name="stPropertyAddressStreetType" escape="true" /> <jato:text name="stPropertyAddressStreetDirection" escape="true" /> <jato:text name="stPropertyAddressUnitNumber" escape="true" /><br>
<jato:text name="stPropertyAddressCity" escape="true" /> <jato:text name="stPropertyAddressProvince" escape="true" /> <jato:text name="stPropertyAddressPostalFSA" escape="true" /> <jato:text name="stPropertyAddressPostalLDU" escape="true" /></font></td>
<td><jato:button name="btDeleteProperty" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_property_fr.gif" /></td>
<td><jato:button name="btPropertyDetails" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details_fr.gif" /></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Prix d'achat:</b></font><br>
<font size=2><jato:text name="stPropertyPurchasePrice" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Valeur estimative totale:</b></font><br>
<font size=2><jato:text name="stPropertyTotalEstimatedValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Valeur estimative r�elle:</b></font><br>
<font size=2><jato:text name="stPropertyActualAppraisedValue" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Type de propri�t�:</b></font><br>
<font size=2><jato:text name="stPropertyType" escape="true" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Type d'occupation:</b></font><br>
<font size=2><jato:text name="stPropertyOccupancyType" escape="true" /></font></td>
<td colspan=2>&nbsp;</td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<!--END REPEATING ROW//-->

</jato:tiledView>

<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td colspan=8>
<table border=0 cellspacing=3 width=600>
<td valign=top><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddProperty" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_property_fr.gif" /></td>
<td valign=top><br><jato:button name="btAppraisalReview" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/appraisal_review_fr.gif" /></td>
<td valign=top><br><font size=2 color=3366cc><b>D�penses totales de la propri�t�:</b></font>&nbsp;&nbsp;<font size=2><jato:text name="stTotalPropertyExpenses" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
</table>
</td><tr>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
<!--END PROPERTY INFORMATION//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td><tr>
</table>

<!--START UW DOWN PAYMENT INFO//-->
<jato:text name="stUWTargetDownPayment" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=8 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Mise de fonds</b></font></td>
</tr>

<tr><td colspan=4 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Source de la mise de fonds</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Description de la mise de fonds</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Mise de fonds</b></font></td>
<td>&nbsp;</td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedUWDownPayment" type="mosApp.MosSystem.pgUWorksheetRepeatedUWDownPaymentTiledView">
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
<jato:hidden name="hdUWDownPaymentSourceId" />
<jato:hidden name="hdUWDownPaymentCopyId" /></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><jato:combobox name="cbUWDownPaymentSource" /></td>
<td valign=top>
<jato:textField name="txUWDPSDescription" size="35" maxLength="35" /></td>
<td valign=top>$ 
<jato:textField name="txUWDownPayment" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><jato:button name="btDeleteDownPayment" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_downpay_fr.gif" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=4>
<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddDownPayment" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_downpay_fr.gif" /></td>
<td valign=top><font size=2 color=3366cc><b>Total de la mise de fonds:</b></font>&nbsp;$ 
<jato:textField name="txUWTotalDown" formatType="decimal" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>Mise de fonds requise:</b></font>&nbsp; <jato:text name="stUWDownPaymentRequired" escape="true" formatType="decimal" formatMask="fr|#,##0.00$; -#" /></td>
</tr>
</table>
</td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<!--END DOWN PAYMENT INFO//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=79 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START ESCROW DETAILS//-->
<jato:text name="stTargetEscrow" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Information sur les fonds plac�s en main tierce</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Type de fonds</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Description des fonds</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Paiement des fonds</b></font></td>
<td>&nbsp;</td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<jato:tiledView name="RepeatedEscrowDetails" type="mosApp.MosSystem.pgUWorksheetRepeatedEscrowDetailsTiledView">

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
<jato:hidden name="hdEscrowPaymentId" />
<jato:hidden name="hdEscrowPaymentCopyId" /></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><jato:combobox name="cbUWEscrowType" /></td>
<td valign=top>
<jato:textField name="txUWEscrowPaymentDesc" size="35" maxLength="35" /></td>
<td valign=top>$ 
<jato:textField name="txUWEscrowPayment" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><jato:button name="btDeleteEscrow" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_escrow_fr.gif" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</jato:tiledView>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5>
<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddEscrow" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_escrow_fr.gif" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Total des fonds:</b></font>&nbsp;$ 
<jato:textField name="tbUWEscrowPaymentTotal" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
</tr>
</table>
</td>
</tr>
</table>

<!--END ESCROW DETAILS//-->

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=ffffff>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td colspan=8 align="left">&nbsp;&nbsp;</td><tr>

<td>&nbsp;&nbsp;<jato:text name="stNewScenario2" fireDisplayEvents="true" escape="false" /></td>
<td>&nbsp;&nbsp;<jato:button name="btSaveScenario" extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/save_scenario_lrg_fr.gif" /></td>
<td>&nbsp;&nbsp;<jato:button name="btVerifyScenario" extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/save_and_check_rules_fr.gif" /></td>
<td>&nbsp;&nbsp;<jato:button name="btConditions" extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'" src="../images/conditions_lrg_fr.gif" /></td>
<td>&nbsp;&nbsp;<jato:button name="btResolveDeal" extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/resolve_deal_lrg_fr.gif" /></td>
<td>&nbsp;&nbsp;<jato:button name="btCollapseDenyDeal" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/CollapseDeny_fr.gif" /></td>
<td>&nbsp;&nbsp;<jato:button name="btUWCancel" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/return_fr.gif" /> <jato:button name="btOK" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/return_fr.gif" /></td>
<td>&nbsp;&nbsp;<a onclick="return IsSubmited();" href="#top"><img src="../images/return_totop_fr.gif" width=117 height=25 alt="" border="0"></a></td><tr>

<td colspan=8 align="left"><div id="createnewscen2" class="createnewscen2" name="createnewscen2">
	<table border=0 cellpadding=0 cellspacing=0 border=0 bgcolor=99ccff>
	<td colspan="4"><img src="../images/blue_line.gif" width=100% height=2></td><tr>
	<td><font size=2>&nbsp;&nbsp;</font></td>
	<td valign=middle><font size=2>&nbsp;Based On:</font><br><jato:combobox name="cbScenarioPickListBottom" /></td>
	<td valign=bottom>&nbsp;&nbsp;<jato:button name="btProceedToCreateScenarioBottom" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/proceed_fr.gif" /></td>
	<td align=right rowspan=2><A onclick="return IsSubmited();" HREF="javascript:newScen(2);"><img src="../images/go_close.gif" width=16 height=42 border=0></a></td><tr>
	<td colspan=4><img src="../images/blue_line.gif" width=100% height=2></td>
	</table>
</div></td>

</table>

<!-- END BODY OF PAGE -- Part 2// -->
