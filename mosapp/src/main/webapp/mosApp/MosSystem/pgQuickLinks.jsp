
<HTML>
<%@page info="pgQuickLinks" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgQuickLinksViewBean">

<HEAD>
<TITLE>URL Administration</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
  

</HEAD>

<body bgcolor=ffffff onload="initPage(event)";>
<jato:form name="pgQuickLinks" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">
<!--
//======================= Local JavaScript (begin) ============================
var enterKeyFlag = "";

function setFocus()
{	
	enterKeyFlag = '1';	
}

function resetFocus()
{
	enterKeyFlag = '0';
}

function submitpage()
{
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;
	
	for( inputRowNdx=0; inputRowNdx < 10; inputRowNdx++ )
	{
		enableTextFields(inputRowNdx);
	}
	
	if(keycode == 13 && enterKeyFlag == '1' )
	{
		document.pgQuickLinks.action += "?<%= viewBean.PAGE_NAME%>_btSubmit="; 
		document.pgQuickLinks.submit();
		return false;
	}
	return true;
}	

function handleCopyFromBranchKeyPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
		//alert("The key handleCopyFromBranchKeyPressed= " + keycode);
  if (keycode == 0)
  {
    performCopyFrom();
    return false;
  }
 
  return true;
}

function performCopyFrom()
{
	setSubmitFlag(true);
	document.forms[0].action += "?<%=viewBean.PAGE_NAME%>_btCopyFrom=";
  
  if(IsSubmitButton())
	  document.forms[0].submit();
}


function handleBranchKeyPressed()
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;
	//alert("handleBranchKeyPressed The key = " + keycode);
  if (keycode == 0)
  {
    performBranchGo();
    return false;
  }
 
  return true;
}

function performBranchGo()
{
	setSubmitFlag(true);
	document.forms[0].action += "?<%=viewBean.PAGE_NAME%>_btGo=";
  
  if(IsSubmitButton())
	  document.forms[0].submit();
}

//======================= Local JavaScript (end) ============================

//-->
</SCRIPT>



<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />
<jato:hidden name="hdQuickLinkEnableDisplay" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<%@include file="/JavaScript/CommonHeader_en.txt" %>  

<%@include file="/JavaScript/CommonToolbar_en.txt" %>
<%@include file="/JavaScript/TaskNavigater.txt" %>
<script src="../JavaScript/quicklinksadmin.js" type="text/javascript"></script>    
<!--HEADER//-->

<!--END HEADER//-->
<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
    <tr>
        <td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
    </tr>
</table>

<table border=0 width=100% cellpadding=3 cellspacing=0 bgcolor=d1ebff>
<tr>
	<td colspan=8><font size=3 color=3366cc><b>URL Details:</b></font></td>
</tr>

<tr>
<td colspan=8><img src="../images/light_blue.gif" width=100% height=2></td>
</tr>
</table>

<!-- quick link -->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>

<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign=top colspan=7 align="left"> <font size=2 color=3366cc><b>Branch:	</b></font>
		<jato:combobox name="cbBranchNames" onChange="handleBranchKeyPressed()"	fireDisplayEvents="true" />
		 <font size=2 color=3366cc> <jato:text name="stAllBranches"  fireDisplayEvents="true" escape="true" /> </font>
	</td>
</tr>

<tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>
<!-- <jato:button name="btGo" extraHtml="onClick = setSubmitFlag(true); performGo();" src="../images/go.gif" /> -->
<!-- <jato:button name="btCopyFrom" extraHtml="onClick = setSubmitFlag(true); performGo();" src="../images/go.gif" /> -->
<tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</tr>

<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>
		<font size=2 color=3366cc>URL Description:
		<jato:text name="stDescriptionEnglish" fireDisplayEvents="true" escape="true" /></font>
	</td>
	<td>
		<font size=2 color=3366cc>URL Description(fr):<jato:text
					name="stDescriptionFrench" fireDisplayEvents="true"
					escape="true" />
		</b>
		</font>
	</td>
	<td>
		<font size=2 color=3366cc>URL Address:<jato:text
					name="stUrlAddress" fireDisplayEvents="true"
					escape="true" />
		</b>
		</font>
	</td>
	<td colspan=4></td>
</tr>

<tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td>
</tr>

<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td align="left" tdcolspan=7> <font size=2 color="3366cc">QUICK LINK BUTTONS </font> </td>
<tr>

<tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td>
</tr>

<% int i = 0; %>
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgQuickLinkTiledView">
   
     
              
<tr>
  
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
	<jato:hidden name="hdQuickLinkId" />
    <jato:hidden name="hdSortId" /></td>
	<td valign="left">
    	<jato:textField name="tbDescriptionEnglish" size="40" maxLength="100"/>
    </td>
    <td valign="left">
    	<jato:textField name="tbDescriptionFrench" size="40" maxLength="100"/>
    </td>
    <td valign="left">
    	<jato:textField name="tbUrlAddress"  size="40" maxLength="500"/>
    </td>
    <td align="left" colspan="4">
    	
    	<img name="btAdd" value='<%=i%>' fireDisplayEvents="true" src="../images/add_sm.gif" onClick = "addEditQuickLinks(this);"/>&nbsp;&nbsp;&nbsp;       
        <img name="btTestLink" value='<%=i%>' fireDisplayEvents="true"  src="../images/testlink_sm.gif" onClick = "testURLAddress(this);" />&nbsp;&nbsp;&nbsp;
        <img  name="btClear"  value='<%=i%>' src="../images/clear_sm.gif" onClick = "clearFields(this);"/>                    
		<img name="btDown" value='<%=i%>' src="../images/arrowy_down.gif" onClick = "moveDown(this);"/>
        <img name="btUp" value='<%=i++%>' src="../images/arrowy_up.gif" onClick = "moveUp(this);"/>
        
    </td>
</tr>   
 
<tr>     	
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td colspan=7 align="left">	<font size=2 color="3366cc"> <jato:text name="stQuicklinkList" fireDisplayEvents="true" escape="true" /> </font>
	</td>
</tr> 

 	
</jato:tiledView>  
       
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan=7 align="left"><font size=2 color=3366cc><b>Copy From Branch:</b></font>
		<jato:combobox name="cbCopyFromBranchNames" onChange="handleCopyFromBranchKeyPressed()" fireDisplayEvents="true" />
		<font size=2 color=3366cc> <jato:text name="stAllBranches" fireDisplayEvents="true" escape="true" /> </font>
	</td>
</tr>
<tr>
	<td colspan=8><img src="../images/light_blue.gif" width=1 height=4 alt="" border="0"></td>
</tr>
</table>

<table border=0 width=100%>
<tr>
<td align=left>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align=right><jato:button name="btSave" extraHtml=" alt='' border='0'  onClick = 'submitpage()'" src="../images/save_lrg.gif" />&nbsp;&nbsp;
<jato:button name="btCancel" extraHtml=" alt='' border='0'  onClick = 'setSubmitFlag(true);'" src="../images/cancel_lrg.gif" fireDisplayEvents="true" /></td>
</tr>
</table>

<p>
</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
