
<HTML>
<%@page info="pgCreditDecision" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgCreditDecisionViewBean">

<HEAD>
<TITLE>D�cision de cr�dit</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include French System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgCreditDecision" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">


	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

	<!--END HEADER//-->

	<!--DEAL SUMMARY SNAPSHOT//-->
	<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
	<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<!-- PUT HERE ANY SUSTOM CHILDREN ELEMENTS (TEXTBOXES, TILED VIEWS, ETC.)//-->
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>

<table border=0 width=100%>
<tr>
<td>

<!-- CREDIT DECISION SUMMARY -->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
<tr>
<td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td>

<tr>
<td valign=top>&nbsp;</td>
<td colspan=6><font size=3 color=3366cc><b>Sommaire d�cision de cr�dit</b></font></td>

<tr>
<td valign=top></td>
<td valign=top>
</td><td valign=top></td>
<td valign=top>
</td><td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Derni�re soumission:</b></font>
</td><td valign=top><jato:textField name="txLastUpdatedDate" size="20" maxLength="35"/>
<jato:hidden name="hdResponseId"/>
</td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Bureau de cr�dit:</b></font>
</td><td valign=top><jato:textField name="txGlobalCreditBureau" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Score carte:</b></font>
</td><td valign=top><jato:textField name="txGlobalInternalScore" size="18"/></td>
<td valign=top rowspan=3 colspan=2><font size=2 color=3366cc><b>Derni�re soumission:</b></font>
<br><jato:textArea name="txDecisionMessages" cols="35" rows="4" style="color: gray;font-weight: bold" extraHtml="readonly"/></td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Score  bureau de cr�dit:</b></font>
</td><td valign=top><jato:textField name="txGlobalCreditBureauScore" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Statut d�cision cr�dit:</b></font>
</td><td valign=top><jato:textField name="txCreditDecisionStatus" size="18"/>
<jato:hidden name="hdRequestId"/><jato:hidden name="hdResponseId"/><jato:hidden name="hdCreditDecisionRequestStatusId"/><jato:hidden name="hdDealStatusCategoryId"/>
</td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Cote d'octroi:</b></font>
</td><td valign=top><jato:textField name="txGlobalRiskRating" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>D�cision de cr�dit:</b></font>
</td><td valign=top><jato:textField name="txCreditDecision" size="18"/></td>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
</table>
<!-- END CREDIT DECISION SUMMARY -->
</td>

<tr>
<td>
<!-- LOAN SUMMARY -->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<td colspan=10><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
<tr>
<td colspan=10><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td>

<tr>
<td valign=top>&nbsp;</td>
<td colspan=9><font size=3 color=3366cc><b>Sommaire pr�t</b></font></td>

<tr>
<td valign=top colspan=9>&nbsp;</td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Taux:</b></font>
</td><td valign=top><jato:textField name="txPostedRate" size="10" formatType="decimal"/><font size=2 color=3366cc><b>&nbsp;%</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Paiement mensuel:</b></font>
</td><td valign=top><jato:textField name="txMonthlyPayment" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Mise de fonds:</b></font>
</td><td valign=top><jato:textField name="txDownPayment" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Ville/province propri�t�:</b></font>
</td><td valign=top><jato:textField name="txLocation" size="25"/></td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Terme:</b></font>
</td><td valign=top><jato:textField name="txActualPaymentTerm" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Produit:</b></font>
</td><td valign=top><jato:textField name="txProduct" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Ratio pr�t/valeur:</b></font>
</td><td valign=top><jato:textField name="txLTV" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Occup� par:</b></font>
</td><td valign=top><jato:textField name="txOccupiedBy" size="15"/></td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Amortissement:</b></font>
</td><td valign=top><jato:textField name="txAmortizationTerm" size="15"/></td>
<td valign=top colspan=4></td>
<td valign=top><font size=2 color=3366cc><b>Type de propri�t�:</b></font>
</td><td valign=top><jato:textField name="txPropertyType" size="15"/></td>

<tr><td colspan=10><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=10><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
</table>
<!-- END LOAN SUMMARY -->
</td>

<tr>
<td>
<!-- MI RESPONSE -->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
<tr>
<td colspan=8><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td>

<tr>
<td valign=top>&nbsp;</td>
<td colspan=7><font size=3 color=3366cc><b>D�cision assureur-pr�t</b></font></td>

<tr>
<td valign=top colspan=8>&nbsp;</td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Assureur-pr�t:</b></font>
</td><td valign=top><jato:textField name="txMortgageInsurer" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Risque emprunteur principal:</b></font>
</td><td valign=top><jato:textField name="txPrimaryApplicantRisk" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Risque  march�:</b></font>
</td><td valign=top><jato:textField name="txMarketRisk" size="15"/></td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>D�cision assureur-pr�t:</b></font>
</td><td valign=top><jato:textField name="txMIStatus" size="30"/></td>
<td valign=top><font size=2 color=3366cc><b>Risque co-emprunteur:</b></font>
</td><td valign=top><jato:textField name="txSecondaryApplicantRisk" size="15"/></td>
<td valign=top><font size=2 color=3366cc><b>Risque  propri�t�:</b></font>
</td><td valign=top><jato:textField name="txPropertyRisk" size="15"/></td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>decision assureur-pr�t:</b></font>
</td><td valign=top><jato:textField name="txMIDecision" size="30"/></td>
<td valign=top colspan=2></td>
<td valign=top><font size=2 color=3366cc><b>Risque voisinage:</b></font>
</td><td valign=top><jato:textField name="txNeighbourhoodRisk" size="15"/></td>

<tr>
<td valign=top></td>
<td valign=top><font size=2 color=3366cc><b>Montant assur�:</b></font>
</td><td valign=top><jato:textField name="txAmountInsured" size="15"   formatType="decimal" formatMask="#,##0.00;(-#,##0.00)"/></td>
<td valign=top></td>
<td valign=top></td>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!-- END MI RESPONSE -->
</td>

<tr>
<td>
<!-- APPLICANT SUMMARY -->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<td colspan=10><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td>
<tr>
<td colspan=10><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td>

<tr>
<td colspan=7><font size=3 color=3366cc><b>Sommaire emprunteur (s)</b></font></td>

<tr>
<td valign=top colspan=8>&nbsp;</td>

<tr>
<td valign=top rowspan=10 width="25%">
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<td valign=top height=25>&nbsp;</td>
<tr>
<td valign=top height=25>&nbsp;</td>
<tr>
<td valign=top height=27><font size=2 color=3366cc><b>Type d'emprunteur:</b></font></td>
<tr>
<td valign=top height=27><font size=2 color=3366cc><b>Bureau de cr�dit:</b></font></td>
<tr>
<td valign=top height=27><font size=2 color=3366cc><b>Score bureau de cr�dit:</b></font></td>
<tr>
<td valign=top height=26><font size=2 color=3366cc><b>Cote d'octroi:</b></font></td>
<tr>
<td valign=top height=28><font size=2 color=3366cc><b>Score carte:</b></font></td>
<tr>
<td valign=top height=29><font size=2 color=3366cc><b>ABD:</b></font></td>
<tr>
<td valign=top height=29><font size=2 color=3366cc><b>ATD:</b></font></td>
<tr>
<td valign=top height=27><font size=2 color=3366cc><b>Valeur nette:</b></font></td>
<tr>
<td valign=top height=27><font size=2 color=3366cc><b>Revenu: </b></font></td>
<tr>
<td valign=top height=28><font size=2 color=3366cc><b>Autre revenu:</b></font></td>
<tr>
<td valign=top height=27><font size=2 color=3366cc><b>Revenu total:</b></font></td>
<tr>
<td valign=top height=28><font size=2 color=3366cc><b>Type de revenu:</b></font></td>
<tr>
<td valign=top height=28><font size=2 color=3366cc><b>Temps � l'emploi:</b></font></td>
<tr>
<td valign=top height=28><font size=2 color=3366cc><b>Bureau de cr�dit utilis� pour D�cision de cr�dit:</b></font></td>
</table>
</td>

<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgCreditDecisionRepeated1TiledView">
<td valign=top rowspan=10 width=15%>
<table border=0 width=100% cellpadding=1 cellspacing=1 bgcolor=d1ebff>
<tr>
<td valign=top><font size=2 color=3366cc><b><u><jato:text name="stBorrowerNumber"/></u></b></font>
<br></td>
<tr>
<td valign=top><jato:hidden name="hdApplicantId"/><jato:hidden name="hdApplicantCopyId"/><jato:hidden name="hdApplicantNumber"/><jato:hidden name="hdApplicantResponseId"/>
<font size=2 color=3366cc><b>&nbsp;<jato:button name="btApplicantDetails"  src="../images/details_fr.gif" /></b></font>
</td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantType" size="13"  style="color: gray;font-weight: bold" extraHtml=" readonly"/></b></font>
</td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantCreditBureau" size="13"  style="color: gray;font-weight: bold" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantCreditBureauScore" size="13" style="color: gray;font-weight: bold" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantRiskRating" size="13" style="color: gray;font-weight: bold" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantInternalScore" size="13" style="color: gray;font-weight: bold" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantGDS" size="13" style="color: gray;font-weight: bold" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantTDS" size="13" style="color: gray;font-weight: bold" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantNetWorth" size="13" style="color: gray;font-weight: bold"  formatType="decimal" formatMask="#,##0.00;(-#,##0.00)" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantIncome" size="13" style="color: gray;font-weight: bold"  formatType="decimal" formatMask="#,##0.00;(-#,##0.00)" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantOtherIncome" size="13" style="color: gray;font-weight: bold"  formatType="decimal" formatMask="#,##0.00;(-#,##0.00)" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantTotalIncome" size="13" style="color: gray;font-weight: bold"  formatType="decimal" formatMask="#,##0.00;(-#,##0.00)" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantIncomeType" size="13" style="color: gray;font-weight: bold" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b><jato:textField name="txApplicantMonthsOfService" size="13" style="color: gray;font-weight: bold" extraHtml="readonly"/></b></font>
<br></td>
<tr>
<td valign=top><font size=2 color=3366cc><b>
<jato:combobox name="cbApplicantRequestedCreditBureau"/></b></font>
<br></td>

</table>
</td>
</jato:tiledView>

<td>
<table border=0 width=100% cellpadding=1 cellspacing=1 bgcolor=d1ebff>
<td valign=top><font size=2 color=3366cc><b><u>Total</u></b><br>&nbsp;</font>
<br></td>
<tr>
<td valign=top height=30><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=top height=20><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=top height=20><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=top height=20><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=top height=20><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=top height=20><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=center height=25><font size=2 color=3366cc><jato:textField name="txCombinedGDS" size="12"  style="color: gray;font-weight: bold"  extraHtml="readonly"/></font></td>
<tr>
<td valign=top height=25><font size=2 color=3366cc><jato:textField name="txCombinedTDS" size="12" style="color: gray;font-weight: bold"  extraHtml="readonly"/></font></td>
<tr>
<td valign=top height=25><font size=2 color=3366cc><jato:textField name="txTotalNetWorth" size="12"  style="color: gray;font-weight: bold"  formatType="decimal" formatMask="#,##0.00;(-#,##0.00)" extraHtml="readonly"/></font></td>
<tr>
<td valign=top height=25><font size=2 color=3366cc><jato:textField name="txTotalIncome" size="12"  style="color: gray;font-weight: bold"  formatType="decimal" formatMask="#,##0.00;(-#,##0.00)" extraHtml="readonly"/></font></td>
<tr>
<td valign=top height=25><font size=2 color=3366cc><jato:textField name="txTotalOtherIncome" size="12"  style="color: gray;font-weight: bold"  formatType="decimal" formatMask="#,##0.00;(-#,##0.00)" extraHtml="readonly"/></font></td>
<tr>
<td valign=top height=25><font size=2 color=3366cc><jato:textField name="txTotalCombinedIncome" size="12"  style="color: gray;font-weight: bold"  formatType="decimal" formatMask="#,##0.00;(-#,##0.00)" extraHtml="readonly"/></font></td>
<tr>
<td valign=top height=10><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=top height=10><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=top height=30><font size=2 color=3366cc>&nbsp;</font></td>
<tr>
<td valign=top height=30><font size=2 color=3366cc>&nbsp;</font></td>

</table>
</td>
</table>

<!-- END APPLICANT SUMMARY -->
</td>


<tr>
<td align=center><jato:button name="btGetCreditDecision" src="../images/get_creditdecision_fr.gif"/>&nbsp;&nbsp;&nbsp;<jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" /></td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
