<%--
 /**
 *Title: pgQualifyingDetailsPageletSummary_fr.jsp
 *Description:Pagelet to display Qualifying Details from POS in French language.
 *@author:MCM Impl Team
 *@version 1.0 30-JUL-2008 XS_16.21 Initial Version 
 *@version 1.1 13-AUG-2008 XS_16.21 Incorporated review comment - Changed currency format mask from "$#,##0.00; (-#)" to "fr|#,##0.00$; (-#)"
 */
--%>
<%@ page info ="pgQualifyingDetailsPageletSummary" language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:pagelet>
 <jato:text name="stQualifyingDetailsSectionStart" escape="false" />
	 <table border="0" width="100%" cellpadding="0" cellspacing="0"
      bgcolor="d1ebff">
      <td colspan="11"><img src="../images/light_blue.gif" width="1"
            height="1" alt="" border="0"></td>
        <tr>
          <td colspan="11"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
	
   		<tbody>  
   		  <tr>
		  <tr>
		<td colspan=8>
			<img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
		</td>
	</tr>
		  <td valign="top"><font size="3" color="3366cc"><b> Détails d'admissibilité obtenus de la source d'affaire </b></font>
	 
         <jato:text name="stQualifyingDetailsFieldsStart" escape="false" />
          </td></tr>
	 	 <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#d1ebff">
      <td align="center" colspan="8"><img src="/images/white.gif" width="10" height="1"><img src="/images/blue_line.gif" width="100%"
        height="1" alt="" border="0"></td>
		 <tr>
		 	  <td valign="top" width="12%"><font size="2" color="3366cc"><b> Taux:</b></font><br></td>
			  <td valign="top" width="13%"><font size="2" color="3366cc"><b>Période de calcul de l'intérêt:</b></font><br></td>
			  <td valign="top" width="13%"><font size="2" color="3366cc"><b>Amortissement:</b></font><br></td>
			  <td valign="top" width="13%"><font size="2" color="3366cc"><b>Type de remboursement:</b></font><br></td>
			  <td valign="top" width="13%"><font size="2" color="3366cc"><b>Paiement:</b></font><br></td>
			  <td valign="top" width="13%"><font size="2" color="3366cc"><b>ABD:</b></font><br></td>
			  <td valign="top" width="13%"><font size="2" color="3366cc"><b>ATD:</b></font><br></td>
			  
         </tr> 
         <tr>
         	  <td width="12%"><font size="2">
				<jato:text name="stRate" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" />%
			  </font></td>
			  <td width="13%"><font size="2">
				<jato:text name="stCompoundingPeriod" escape="true" />
			  </font></td>
			  <td width="13%"><font size="2">
				<jato:text name="stAmortizationYrs" escape="true" formatType="decimal" formatMask="###0; (-#)" />&nbsp;Ans&nbsp;&nbsp;&nbsp;<jato:text name="stAmortizationMths" escape="true" formatType="decimal" formatMask="###0; (-#)" />&nbsp;Mois
				</font></td>
			  <td width="13%"><font size="2">
				<jato:text name="stRepaymentType" escape="true" />
				</font></td>
			  <td width="13%"><font size="2">
			  
			  <jato:text name="stPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)"  />

				</font></td>
			  <td width="13%"><font size="2">
				<jato:text name="stGDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)"  />
				</font></td>
			  <td width="13%"><font size="2">
				<jato:text name="stTDS" escape="true" formatType="decimal" formatMask="fr|###0.00; (-#)" />
				</font></td>
          <tr>
		<td colspan="11"><img src="../images/light_blue.gif" width="1"
            height="1" alt="" border="0"></td>
      	</tr>
        </table>
		<jato:text name="stQualifyingDetailsFieldsEnd" escape="false" />
		
		<jato:text name="stQualifyingDetailsMessageStart" escape="false" />
		<font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Non disponible</font>
		<tr>
		<td colspan="11"><img src="../images/light_blue.gif" width="1"
            height="1" alt="" border="0"></td>
      	</tr>
		<jato:text name="stQualifyingDetailsMessageEnd" escape="false" />
		</tbody>
    </table>
 <jato:text name="stQualifyingDetailsSectionEnd" escape="false" />
</jato:pagelet>
	
