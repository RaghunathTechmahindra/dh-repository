<%--**
 * Title: pgApplicantEntry.jsp
 *
 * Description: Applicant Entry page. 
 *
 *
 * version 1.6  <br>
 * Date: 08/02/06 <br>
 * Author: NBC/PP Implementation Team 
 * Change:Modifications in  Applicant tile 
 * 		Added Gender, Smoker/Non-Smoker, Preferred Method of Contact, Employee Number
 *      
 *      Added new Identification Tile
 *	
 */
--%>
<HTML>
<%@page info="pgApplicantEntry" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgApplicantEntryViewBean">

<HEAD>
<TITLE>Applicant Entry</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression(QLE?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression(QLE?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcs.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
	QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<!--
<%-- FXP26551.1: This behavior is causing top toolbar to display incorrectly upon resizing
the window, because both objects referenced by this function do not exist in the page--%>
/*
window.onresize = setSize; 
function setSize()
{
	//alert("Resize " +document.getElementById("pagebody").offsetWidth);
	document.getElementById("test").style.width = document.getElementById("pagebody").offsetWidth +"px";
	document.getElementById("alertbody").style.width = document.getElementById("pagebody").offsetWidth +"px";
	
}
*/
<%-- End of FXP26551.1 --%>
//	========================================================================================================
// Function to initialize the page -- may be difference for every pages
//		It should be used in the onLoad event of <body> tag
// 
// The following was adjusted for JATO field format -- By BILLY 25July2002
// ========================================================================================================
function initPage()
{
	//alert(document.getElementById("pagebody").offsetWidth);
<%-- FXP26551.2: These references to non-existing elements are causing
java script errors--%>

	//document.getElementById("test").style.width = document.getElementById("pagebody").offsetWidth +"px";
	//document.getElementById("alertbody").style.width = document.getElementById("pagebody").offsetWidth +"px";
<%-- End of FXP26551.2 --%>
	// Init all Title handling
	setAllValueAndDisableIfNotMatched("cbJobTitle", ["txJobTitle"], "0", "", "pgApplicantEntry_RepeatedEmployment[0]_cbJobTitle");
	// Init all EmploymentStatus handling
	setAllValueAndDisableIfMatched("cbEmploymentStatus", ["txEmployPercentageIncludeInGDS","txEmployPercentageIncludeInTDS"],
										"1", "0", "pgApplicantEntry_RepeatedEmployment[0]_cbEmploymentStatus");
	setAllValueAndDisableIfMatched("cbEmploymentStatus", ["cbEmployIncludeInGDS","cbEmployIncludeInTDS"],
										"1", "N", "pgApplicantEntry_RepeatedEmployment[0]_cbEmploymentStatus");
	// Set default value for Hidden GDS TDG field 
	setDefaultsAll("cbIncomeType", ["hdEmployPercentageIncludeInGDS", "hdEmployPercentageIncludeInTDS"], "pgApplicantEntry_RepeatedEmployment[0]_cbIncomeType");
	setDefaultsAll("cbOtherIncomeType", ["hdOtherIncomePercentIncludeInGDS", "hdOtherIncomePercentIncludeInTDS"], "pgApplicantEntry_RepeatedOtherIncome[0]_cbOtherIncomeType");
	setDefaultsAll("cbAssetType", ["hdPercentageIncludedInNetworth"], "pgApplicantEntry_RepeatedAssets[0]_cbAssetType");
	setDefaultsAll("cbLiabilityType", ["hdLiabilityPercentageIncludeInGDS", "hdLiabilityPercentageIncludeInTDS"], "pgApplicantEntry_RepeatedLiabilities[0]_cbLiabilityType");
}

//	========================================================================================================
// 	Functions for Multiple Records Deleting 
// ========================================================================================================
var numSelectedItemForDelete = 0;
var numSelectedItemForExport = 0;

var isDeleteButtonPressed = false;
var isExportButtonPressed = false;

function isAnyItemSelectedForDelete()
{
	if( numSelectedItemForDelete > 0 )
	{
		alert(LIABILITY_DELETION_ERROR);
//		alert("numSelectedItemForDelete = " + numSelectedItemForDelete);
		return true;
	}
		
	return false;
}

function deleteChkBoxClicked()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldName = theField.name;
	
	if(	theField.checked == true )
		numSelectedItemForDelete += 1;
	else
		numSelectedItemForDelete -= 1;
	
//	alert("Current checked flag (" + theFieldName + ") = " + theField.checked + " numSelectedItemForDelete = " + numSelectedItemForDelete);
	
}

function deleteBtnClicked()
{
	isDeleteButtonPressed = true;
}

function isAnyItemSelectedForExport()
{
	if( numSelectedItemForExport > 0 )
	{
		alert(LIABILITY_EXPORT_ERROR);
//		alert("numSelectedItemForExport = " + numSelectedItemForExport);
		return true;
	}
	
			
	return false;
}

function exportChkBoxClicked()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldName = theField.name;
	
	if(	theField.checked == true )
		numSelectedItemForExport += 1;
	else
		numSelectedItemForExport -= 1;
	
//	alert("Current checked flag (" + theFieldName + ") = " + theField.checked + " numSelectedItemForExport = " + numSelectedItemForExport);
	
}

function exportBtnClicked()
{
	isExportButtonPressed = true;
}

function IsSubmitButtonWithDeleteExportHandle()
{
	// Check if any export or Delete Item(s) selected
	if((isExportButtonPressed == false && isAnyItemSelectedForExport() == true) || 
		(isDeleteButtonPressed == false && isAnyItemSelectedForDelete() == true))
	{
		//reset the previous submit action
		setSubmitedFlag(false);
		return false;
	}
	else
	{
		isExportButtonPressed = false;
		isDeleteButtonPressed = false;
		return IsSubmitButton();
	}
}
//-->

function creditBureauClicked()
{
	var creditBureauSize = document.getElementById("hdCreditBureauTileSize").value;
	if( creditBureauSize > 0) {
	    var creditBureauIcon = document.getElementById("creditBureauImage");
	    var section = document.getElementById("creditBureanDateSection" + '0' );
	    if (section.style.display == 'inline'|| section.style.display =='') {
	        for(var i = 0; i < creditBureauSize; i ++) {
	            var divCreditBureauDateSection = document.getElementById("creditBureanDateSection" + i );
	            divCreditBureauDateSection.style.display='none';
	            document.getElementById("creditBureauImage").src="../images/plus.gif";
	        }
	    } else {
	        for(var i = 0; i < creditBureauSize; i ++) {
	            var divCreditBureauDateSection = document.getElementById("creditBureanDateSection" + i );
	            divCreditBureauDateSection.style.display='inline';
	            document.getElementById("creditBureauImage").src="../images/less.gif";
	        }
	    }
    }
}


function liabilityClicked()
{
	var liabilitySize = document.getElementById("hdLiabilityTileSize").value;
	if( liabilitySize > 0) {
	    var liabilityIcon = document.getElementById("liabilityImage");
	    var section = document.getElementById("liabilityDateSection" + '0' );
	    if (section.style.display == 'inline'|| section.style.display =='') {
	        for(var i = 0; i < liabilitySize; i ++) {
	            var divLiabilityDateSection = document.getElementById("liabilityDateSection" + i );
	            divLiabilityDateSection.style.display='none';
	            document.getElementById("liabilityImage").src="../images/plus.gif";
	        }
	    } else {
	        for(var i = 0; i < liabilitySize; i ++) {
	            var divLiabilityDateSection = document.getElementById("liabilityDateSection" + i );
	            divLiabilityDateSection.style.display='inline';
	            document.getElementById("liabilityImage").src="../images/less.gif";
	        }
	    }
    }
}
</SCRIPT>


</HEAD>
<body bgcolor=ffffff onload="initPage(); checkMIResponseLoop();">
<jato:form name="pgApplicantEntry" method="post" onSubmit="return(IsSubmitButtonWithDeleteExportHandle())" >

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

<jato:text name="stVALSData" escape="false" />

// ============================ Functions for CreditBureau Report -- By BILLY 11Jan2002 =================================
var bWin;
var bWinContent = "";

function createBureauWindow()
{	

var bureauReport = '';

<jato:tiledView name="rptCreditBureauReports" type="mosApp.MosSystem.pgApplicantEntryrptCreditBureauReportsTiledView">
bureauReport += '<B>Report#&nbsp;';
bureauReport +=  Number(<jato:text name="hdRowNdx" escape="true" />).toString() + '</B>';
bureauReport += '&nbsp;&nbsp';
bureauReport += '<jato:text name="stCreditBureauReport" escape="false" /><BR>';
</jato:tiledView>
		
	bWinContent = "";
	bWinContent += "<HEAD><TITLE>Bureau View</TITLE><HEAD><BODY bgcolor=d1ebff>";
	bWinContent += "<form name=\"bureau_form\">" +
					"<table border=0 width=100% cellpadding=0 cellspacing=0>" +
					"<td valign=top><font size=2 >&nbsp;&nbsp;  </font></td>" +
					"<td align=left> " +
					bureauReport +	
					"</td><tr><td colspan=2 align=left><img src=\"../images/blue_line.gif\" width=100% height=1 border=\"0\"></td>"+
					"<tr><td colspan =2 align=right><BR>" +
					"<a href=\"javascript:window.close();\" onMouseOver=\"self.status=\'\';return true;\"><img src=\"../images/close.gif\" width=86 height=25 alt=\"\" border=\"0\"></a></td></table>"+
					"</form></body>"
	
	
	if(typeof(bWin) == "undefined" || bWin.closed == true)
	{
  		bWin=window.open('/Mos/nocontent.htm','BureauWindow'+document.forms[0].<%= viewBean.PAGE_NAME%>_sessionUserId.value,
    		'resizable,scrollbars,status=yes,titlebar=yes,width=700,height=600,hotkeys,resizable');
   	}

	// For IE 5.00 or lower, need to delay a while before writing the message to the screen
	//		otherwise, will get an runtime exception - Note by Billy 17May2001
	var timer;
	timer = setTimeout("createBureauWindow2()", 500);

}

function createBureauWindow2()
{
	bWin.document.open();
	bWin.document.write(bWinContent);
	bWin.document.close();
	bWin.moveTo(100,100);
	bWin.focus();
}

// ================= CreditBureau Functions Ended -- By BILLY 11Jan2002 ====================================

</SCRIPT>


<%--FXP26551.3: Various HTML fixes for page structure.--%>
<!--HEADER//-->
	<jato:hidden name="sessionUserId" />
	<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
	<input type="hidden" name="isFatal" value="">
	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %>
<!--End of HEADER//-->

<div id="pagebody" class="pagebody" name="pagebody">

<center>

<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
</table>

<!--START APPLICANT DETAILS//-->
<table border=0 width=100% cellpadding=0 cellspacing=0>
<%-- End of FXP26551.3 --%>

<tr><td>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff height="247">
<tr><td colspan=6 height="2"><jato:hidden name="hdApplicantId" /><jato:hidden name="hdApplicantCopyId" /><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=6 valign=top height="17"><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Applicant Details</b></font></td>
</tr>
<tr><td colspan=6 height="19">&nbsp;</td></tr>

<tr>
<td valign=top height="34">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>
<table>
<tr>
<td valign=top height="34"><font size=2 color=3366cc><b>Salutation:</b></font><br>
<jato:combobox name="cbSalutation" /></td>
	<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
<td valign=top height="34"><font size=2 color=3366cc><b>Gender:</b></font><br>
<jato:combobox name="cbBorrowerGender" /></td>
</tr>
</table>
</td>
<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
<td valign=top height="34"><font size=2 color=3366cc><b>First Name:</b></font><br>
<jato:textField name="txFirstName" size="20" maxLength="20" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Middle Initial:</b></font><br>
<jato:textField name="txMiddleInitials" size="2" maxLength="1" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Last Name:</b></font><br>
<jato:textField name="txLastName" size="20" maxLength="20" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Suffix:</b></font><br>
<jato:combobox name="cbSuffix" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Type:</b></font><br>
<jato:combobox name="cbApplicantType" /></td>
</tr>

<tr><td colspan=6 height="2"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top height="34">&nbsp;</td>
<td valign=top height="34"><font size=2 color=3366cc><b>Date of Birth:</b></font><br>
<font size=2>Mth: </font><jato:combobox name="cbApplicantDOBMonth" /> <font size=2>Day: </font>
<jato:textField name="txApplicantDOBDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Yr: </font>
<jato:textField name="txApplicantDOBYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Marital Status:</b></font><br>
<jato:combobox name="cbMartialStatus" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>S.I.N.:</b></font><br>
<jato:textField name="txSINNo" formatType="decimal" formatMask="###0; (-#)" size="9" maxLength="9" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Citizenship Status:</b></font><br>
<jato:combobox name="cbCitizenship" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b># of Dependants:</b></font><br>
<jato:textField name="txNoOfDependants" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="2" /></td>

<%--***** Change by NBC Impl. Team - Version 1.6 - Start *****--%>
<td valign=top height="34"><font size=2 color=3366cc><b>Smoker:</b></font><br>
<jato:combobox name="cbSmokeStatus" /></td>
<%--***** Change by NBC Impl. Team - Version 1.6 - End *****--%>

</tr>

<tr><td colspan=6 height="2"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top height="34">&nbsp;</td>
<td valign=top height="34"><font size=2 color=3366cc><b>Existing Client?</b></font><br>
<jato:combobox name="cbExistingClient" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Reference Client #:</b></font><br>
<%-- FXP26805, 4.2GR, Oct 21,09 stop formatting as number start--%>
<jato:textField name="txReferenceClientNo" size="20" maxLength="15" />
<%-- FXP26805, 4.2GR, Oct 21,09 stop formatting as number end--%>
</td>
<td valign=top height="34"><font size=2 color=3366cc><b># of Times Bankrupt:</b></font><br>
<jato:textField name="txNoOfTimesBankrupt" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Bankruptcy Status:</b></font><br>
<jato:combobox name="cbBankruptcyStatus" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Staff of Lender?</b></font><br>
<jato:combobox name="cbStaffOfLender" /></td>

<%--***** Change by NBC Impl. Team - Version 1.6 - Start *****--%>
<td valign=top height="34"><font size=2 color=3366cc><b>Employee #:</b></font><br>
<jato:textField name="txEmployeeNo" formatType="string" size="20" maxLength="35" /></td>
<%--***** Change by NBC Impl. Team - Version 1.6 - End *****--%>

</tr>

<tr><td colspan=6 height="2"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top height="34">&nbsp;</td>
<td valign=top height="34"><font size=2 color=3366cc><b>Home Phone #:</b></font><br>
( <jato:textField name="txApplicantHomePhoneAreaCode" size="3" maxLength="3" /> ) 
<jato:textField name="txApplicantHomePhoneExchange" size="3" maxLength="3" /> - 
<jato:textField name="txApplicantHomePhoneRoute" size="4" maxLength="4" /></td>
<td valign=top colspan=2 height="34"><font size=2 color=3366cc><b>Work Phone #:</b></font><br>
( <jato:textField name="txApplicantWorkPhoneAreaCode" size="3" maxLength="3" /> ) 
<jato:textField name="txApplicantWorkPhoneExchange" size="3" maxLength="3" /> - 
<jato:textField name="txApplicantWorkPhoneRoute" size="4" maxLength="4" /> X 
<jato:textField name="txApplicantWorkPhoneExt" size="6" maxLength="6" /></td>
<td valign=top colspan=1 height="34"><font size=2 color=3366cc><b>Cell Phone #:</b></font><br>
( <jato:textField name="txApplicantCellAreaCode" size="3" maxLength="3" /> ) 
<jato:textField name="txApplicantCellExchange" size="3" maxLength="3" /> - 
<jato:textField name="txApplicantCellRoute" size="4" maxLength="4" /></td>
<td valign=top colspan=1 height="34"><font size=2 color=3366cc><b>Fax #:</b></font><br>
( <jato:textField name="txApplicantFaxAreaCode" size="3" maxLength="3" /> ) 
<jato:textField name="txApplicantFaxExchange" size="3" maxLength="3" /> - 
<jato:textField name="txApplicantFaxRoute" size="4" maxLength="4" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>First Time Buyer:</b></font><br>
<jato:combobox name="cbFirstTimeBuyer" /></td>
</tr>

<tr><td colspan=6 height="2"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top height="34">&nbsp;</td>
<td valign=top height="34"><font size=2 color=3366cc><b>E-mail Address:</b></font><br>
<jato:textField name="txEmailAddress" size="35" maxLength="50" fireDisplayEvents="true" /></td>
<td valign=top colspan=2 height="34"><font size=2 color=3366cc><b>Language Preference:</b></font><br>
<jato:combobox name="cbLanguagePreference" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Guarantor Other Loans:</b></font><br>
<jato:combobox name="cbGuarantorOtherLoans" /></td>

<%--***** Change by NBC Impl. Team - Version 1.6 - Start *****--%>
<td valign=top height="34"><font size=2 color=3366cc><b> Preferred Contact Method:</b></font><br>
<jato:combobox name="cbPreferredContactMethod" /></td>
<td valign=top height="34"><font size=2 color=3366cc><b>Solicitation:</b></font><br>
<jato:combobox name="cbSolicitation"/></td>
<%--***** Change by NBC Impl. Team - Version 1.6 - End *****--%>

</tr>

<tr><td colspan=8 height="2"><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8 height="1"><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<!--END APPLICANT DETAILS//-->
<p>
</td></tr>


<tr><td>
<%--***** Change by NBC Impl. Team - Version 1.6 - Start*****--%>
<!--START IDENTFICATION//-->
<jato:text name="stTargetIdentification" fireDisplayEvents="true" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
	<tr>
		<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	</tr>
	
	<tr>
		<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Identification</b></font></td>
	</tr>
	
	<tr><td colspan=6 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>
	
	<!--START REPEATING ROW//-->
	
	<jato:tiledView name="RepeatedIdentification"
	 	type="mosApp.MosSystem.pgApplicantEntryRepeatedIdentificationTiledView">
	
	<tr>
		<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdIdentificationId" /><jato:hidden name="hdIdentificationCopyId" /></td>
		<td valign=top><font size=2 color=3366cc><b>Identification Number:</b></font><br>
			<jato:textField name="txIdentificationNumber" size="35" maxLength="35" />
		</td>
	
		<td valign=top height="34"><font size=2 color=3366cc><b>Identification Type:</b></font><br>
			<jato:combobox name="cbIdentificationType" />
	    </td>
	
	    <td valign=top><font size=2 color=3366cc><b>Identification Source Country :</b></font><br>
			<jato:textField name="txIdentificationSourceCountry" size="30" maxLength="30" />
	    </td>

	    <td valign=bottom align=right>&nbsp;<br>
	    	<jato:button name="btDeleteApplicantIdentification" extraHtml="onClick ='setSubmitFlag(true);'" 
	    		fireDisplayEvents="true" src="../images/delete_identification.gif" />&nbsp;&nbsp;&nbsp;
	    </td>
	<tr>
	
	<tr>
		<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	</tr>

	<tr>
		<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	</tr>
	
	<!--END REPEATING ROW//-->
	
	</jato:tiledView>
	
	<tr>
		<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	</tr>
	
	<tr>
		<td colspan=6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<jato:button name="btAddAdditionalApplicantIdentification" extraHtml="
				onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_identification.gif" />
		</td>
	</tr>
	
	<tr>
	    <td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
	</tr>
	
	<tr>
	    <td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
	</tr>
	
	</table>
	<!--END IDENTFICATION//-->
</td></tr>

	
<tr><td>	
	<table border=0 width=100% cellpadding=0 cellspacing=0>
		<tr>
			<td align=right>
				<a onclick="return IsSubmited();" href="#top">
					<img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0">
				</a>
			</td>
		</tr>
</table>
</td></tr>


<tr><td>
<%--***** Change by NBC Impl. Team - Version 1.6 - End *****--%>
<!--START ADDRESS//-->
<jato:text name="stTargetAddress" fireDisplayEvents="true" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Address</b></font></td>
</tr>

<tr><td colspan=6 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedApplicantAddress" type="mosApp.MosSystem.pgApplicantEntryRepeatedApplicantAddressTiledView">


<!--Change by NBC Impl. Team - Version 1.2 - Start// -->
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Street #:</b></font><br>
<jato:textField name="txStreetNumber" size="10" maxLength="10" /></td>
<td valign=top><font size=2 color=3366cc><b>Street Name:</b></font><br>
<jato:textField name="txStreetName" size="35" maxLength="35" /></td>
<td valign=top><font size=2 color=3366cc><b>Street Type:</b></font><br>
<jato:combobox name="cbStreetType" /></td>
<td valign=top><font size=2 color=3366cc><b>Direction:</b></font><br>
<jato:combobox name="cbStreetDirection" /></td>
<td valign=top><font size=2 color=3366cc><b>Unit #:</b></font><br>
<jato:textField name="txUnitNumber" size="10" maxLength="10" /></td>
<tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="appAddressId" /><jato:hidden name="appAddressCopyId" /><jato:hidden name="appAddressAddrId" /><jato:hidden name="appAddressAddrCopyId" /></td>
<td valign=top><font size=2 color=3366cc><b>Address Line 1:</b></font><br>
<jato:textField name="txApplicantAddressLine1" size="35" maxLength="35" /></td>
<td valign=top><font size=2 color=3366cc><b>City:</b></font><br>
<jato:textField name="txApplicantCity" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>Province:</b></font><br>
<jato:combobox name="cbApplicantAddressProvince" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Postal Code:</b></font><br>
<jato:textField name="txApplicantPostalCodeFSA" formatType="string" formatMask="???" size="3" maxLength="3" /> 
<jato:textField name="txApplicantLDU" formatType="string" formatMask="???" size="3" maxLength="3" /></td>
<td valign=top>&nbsp;</td>

</tr>

<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Address Line 2:</b></font><br>
<jato:textField name="txApplicantAddressLine2" size="35" maxLength="35" /></td>
<!--Change by NBC Impl. Team - Version 1.2 - End// -->
<td valign=top><font size=2 color=3366cc><b>Address Status:</b></font><br>
<jato:combobox name="cbApplicantAddressStatus" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Time at Residence:</b></font><br>
<font size=2>Yrs: </font>
<jato:textField name="txApplicantTimeYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Mths: </font>
<jato:textField name="txApplicantTimeMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>Residential Status:</b></font><br>
<jato:combobox name="cbApplicantAddResidentialStatus" /></td>
<td valign=bottom align=right>&nbsp;<br>
  <jato:button name="btDeleteApplicantAddress" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_address.gif" />&nbsp;&nbsp;&nbsp;</td>
</tr>


<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<!--END REPEATING ROW//-->

</jato:tiledView>

<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr><td colspan=6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAddtionalApplicantAddress" extraHtml="width=130 height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_address.gif" /></td></tr>

<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END ADDRESS//-->
</td></tr>


<tr><td>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
</td></tr>


<tr><td>
<!--START EMPLOYMENT//-->
<jato:text name="stTargetEmployment" fireDisplayEvents="true" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=7 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Employment</b></font></td>
</tr>

<tr><td colspan=7 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedEmployment" type="mosApp.MosSystem.pgApplicantEntryRepeatedEmploymentTiledView">
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdEmploymentId" /><jato:hidden name="hdEmploymentCopyId" /><jato:hidden name="hdContactId" /><jato:hidden name="hdContactCopyId" /></td>
<td valign=top><font size=2 color=3366cc><jato:hidden name="hdIncomeId" /><jato:hidden name="hdIncomeCopyId" /><jato:hidden name="hdAddrId" /><jato:hidden name="hdAddrCopyId" /><jato:hidden name="hdJobTitle" /><b>Employer Name:</b></font><br>
<jato:textField name="txEmployerName" size="30" maxLength="40" /></td>
<td valign=top><font size=2 color=3366cc><b>Employment Status:</b></font><br>
<jato:combobox name="cbEmploymentStatus" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Employment Type:</b></font><br>
<jato:combobox name="cbEmploymentType" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top nowrap><font size=2 color=3366cc><b>Employer Phone #:</b></font><br>
( <jato:textField name="txEmployerPhoneAreaCode" size="3" maxLength="3" /> ) 
<jato:textField name="txEmployerPhoneExchange" size="3" maxLength="3" /> - 
<jato:textField name="txEmployerPhoneRoute" size="4" maxLength="4" /> X 
<jato:textField name="txEmployerPhoneExt" size="6" maxLength="6" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Employer Fax #:</b></font><br>
( <jato:textField name="txEmployerFaxAreaCode" size="3" maxLength="3" /> ) 
<jato:textField name="txEmployerFaxExchange" size="3" maxLength="3" /> - 
<jato:textField name="txEmployerFaxRoute" size="4" maxLength="4" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Employer E-mail Address:</b></font><br>
<jato:textField name="txEmployerEmailAddress" size="35" maxLength="50" fireDisplayEvents="true"/></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Employer Mailing Address Line 1:</b></font><br>
<jato:textField name="txEmployerMailingAddressLine1" size="30" maxLength="35" /></td>
<td valign=top><font size=2 color=3366cc><b>City:</b></font><br>
<jato:textField name="txEmployerCity" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>Province:</b></font><br>
<jato:combobox name="cbEmployerProvince" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Postal Code:</b></font><br>
<jato:textField name="txEmployerPostalCodeFSA" size="3" maxLength="3" /> 
<jato:textField name="txEmployerPostalCodeLDU" size="3" maxLength="3" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top colspan=6><font size=2 color=3366cc><b>Employer Mailing Address Line 2:</b></font><br>
<jato:textField name="txEmployerMailingAddressLine2" size="30" maxLength="35" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Job Title:</b></font><br>
<jato:combobox name="cbJobTitle" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Custom Job Title:</b></font><br>
<jato:textField name="txJobTitle" size="20" maxLength="50" /></td>
<td valign=top colspan=1><font size=2 color=336cc><b>Occupation:</b></font><br>
<jato:combobox name="cbOccupation" /></td>
<td valign=top><font size=2 color=3366cc><b>Industry Sector:</b></font><br>
<jato:combobox name="cbIndustrySector" /></td>
<td valign=top colspan=1 nowrap><font size=2 color=3366cc><b>Time at Job:</b></font><br>
<font size=2>Yrs: </font>
<jato:textField name="txTimeAtJobYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Mths: </font>
<jato:textField name="txTimeAtJobMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Income Type:</b></font><br>
<jato:combobox name="cbIncomeType" /></td>
<td valign=top><font size=2 color=3366cc><b>Income Description:</b></font><br>
<jato:textField name="txIncomeDesc" size="30" maxLength="80" /></td>
<td valign=top><font size=2 color=3366cc><b>Income Period:</b></font><br>
<jato:combobox name="cbIncomePeriod" /></td>
<td valign=top nowrap><font size=2 color=3366cc><b>Income Amount:</b></font><br>
$ 
<jato:textField name="txIncomeAmount" extraHtml="onChange = 'calcBorrowerTotalIncome();'" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Include in GDS?</b></font><br>
<jato:combobox name="cbEmployIncludeInGDS" /></td>
<td valign=top><font size=2 color=3366cc><b>% included in GDS:</b></font><br>
<jato:hidden name="hdEmployPercentageIncludeInGDS" />
<jato:textField name="txEmployPercentageIncludeInGDS" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /> %</td>
<td valign=top><font size=2 color=3366cc><b>Include in TDS?</b></font><br>
<jato:combobox name="cbEmployIncludeInTDS" /></td>
<td valign=top><font size=2 color=3366cc><b>% included in TDS:</b></font><br>
 <jato:hidden name="hdEmployPercentageIncludeInTDS" />
<jato:textField name="txEmployPercentageIncludeInTDS" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /> %</td>
<td colspan=2 align=right valign=top>&nbsp;<br><jato:button name="btDeleteEmployment" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_employ.gif" />&nbsp;&nbsp;</td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr><td colspan=7>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAdtionalEmployment" extraHtml="width=152 height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_employ.gif" /></td></tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END EMPLOYMENT//-->
</td></tr>


<tr><td>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
</td></tr>


<tr><td>
<!--START OTHER INCOME //-->
<jato:text name="stTargetIncome" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=7 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Other Income</b></font></td>
</tr>

<tr><td colspan=7 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedOtherIncome" type="mosApp.MosSystem.pgApplicantEntryRepeatedOtherIncomeTiledView">
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdOtherIncomeId" /><jato:hidden name="hdOtherIncomeCopyId" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Income Type:</b></font><br>
<jato:combobox name="cbOtherIncomeType" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Income Description:</b></font><br>
<jato:textField name="txOtherIncomeDesc" size="30" maxLength="80" /></td>
<td valign=top><font size=2 color=3366cc><b>Income Period:</b></font><br>
<jato:combobox name="cbOtherIncomePeriod" extraHtml="onChange = 'calcBorrowerTotalIncome();'" /></td>
<td valign=top><font size=2 color=3366cc><b>Income Amount:</b></font><br>
$ 
<jato:textField name="txOtherIncomeAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Include in GDS?</b></font><br>
<jato:combobox name="cbOtherIncomeIncludeInGDS" /></td>
<td valign=top><font size=2 color=3366cc><b>% included in GDS:</b></font><br>
<jato:hidden name="hdOtherIncomePercentIncludeInGDS" />
<jato:textField name="txOtherIncomePercentIncludeInGDS" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /> %</td>
<td valign=top><font size=2 color=3366cc><b>Include in TDS?</b></font><br>
<jato:combobox name="cbOtherIncomeIncludeInTDS" /></td>
<td valign=top><font size=2 color=3366cc><b>% included in TDS:</b></font><br>
<jato:hidden name="hdOtherIncomePercentIncludeInTDS" />
<jato:textField name="txOtherIncomePercentIncludeInTDS" formatType="decimal" formatMask="###0; (-#)" size="3" maxLength="3" /> %</td>
<td valign=top align=right colspan=2>&nbsp;<br>
<jato:button name="btDeleteOtherIncome" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_income.gif" />&nbsp;&nbsp;</td>
</tr>


<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=7>

<table border=0 cellspacing=3 width=490>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddAdditionalOtherIncome" extraHtml="width=128 height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_income.gif" /></td>
<td valign=top><font size=2 color=3366cc><b>Total Applicant Income:</b></font>&nbsp;<jato:textField name="txTotalIncome" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=7><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=7><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--END OTHER INCOME TESTING //-->
</td></tr>


<tr><td>
<!-- Include Part 2 of ApplicantEntry --//-->
<jsp:include page="pgApplicantEntry_part2.jsp" />
</td></tr>


<tr><td>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
</td></tr>


<tr><td>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<!-- NBC-team6- ticket 3846,3847,3850 added call to js function to validate Street Name Fields on onClick event-->
<tr><td align=right><jato:button name="btApplicantSubmit" extraHtml="onClick = \"if(!isStreetNameEntered()) return false; setSubmitFlag(true);\"" fireDisplayEvents="true" src="../images/ok.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btApplicantCancel" extraHtml="onClick = \"if(!confirm(APP_CANCEL_CHANGES) ) return false;  setSubmitFlag(true);\"" fireDisplayEvents="true" src="../images/cancel.gif" /><jato:button name="btApplicantCancelCurrentChanges" extraHtml="onClick = \"if(!confirm(APP_CANCEL_CHANGES) ) return false;  setSubmitFlag(true);\"" fireDisplayEvents="true" src="../images/cancelCurrentChanges.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td></tr>
</table>
</td></tr>


</table>
</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>

<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
	
}
else{
	tool_click(5)
	//location.href="#loadtarget"
}

if(pmGenerate=="Y")
{
openPMWin();
}

-->
</script>

<!--END BODY//-->



</BODY>

</jato:useViewBean>
</HTML>
