
<HTML>
<%@page info="pgInterestRateUpdate" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgInterestRateUpdateViewBean">

<HEAD>
<TITLE>Mise � jour des taux d�int�r�t</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcs.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--
//========== Local JavaScript functions -- adjusted for Jato FieldName format -- Billy 07Aug2002 ================
function getPostRate()
{
	return(document.pgInterestRateUpdate.pgInterestRateUpdate_tbPostedRate.value);
}

function changeBestRate(fieldName)
{
	var theField =(NTCP) ? event.target : event.srcElement;
	var theFieldValue = theField.value;
	var theFieldName = theField.name;

	var postRateVal = document.pgInterestRateUpdate.pgInterestRateUpdate_tbPostedRate.value;
	var maxDiscountVal = document.pgInterestRateUpdate.pgInterestRateUpdate_tbMaxDiscountRate.value;
	
// Check if this is the current field to validate 
	//		-- In order to prevent dead lock 
	if( currFieldToValidate == "" || currFieldToValidate == theFieldName )
	{
		currFieldToValidate = theFieldName;
	}
	else
	{
		//alert("Locked by = " + currFieldToValidate);
		return true;
	}
	
	if(theFieldValue == "") 
	{
		currFieldToValidate = "";
		return true;	
	}
	
	
	if(theFieldName == 'tbMaxDiscountRate')
	{
		if(eval(postRateVal.toString()) < eval(maxDiscountVal.toString()) )
		{
			alert(MAX_DISCOUNT);
			theField.select();
			return;
		}	
	}	


	//var val = postRateVal - maxDiscountVal;
	var v1 = postRateVal * 1000;
	var v2 = maxDiscountVal *1000;
	var val = (v1 - v2)/1000;

	if(! isNaN(val) )
	{
		val = val*1.0;
		var totString = val.toString();
		var indDec = totString.indexOf(".");

		if(indDec != -1)
		{
			var preDec = totString.substring(0,indDec);
			var postDec = totString.substring(indDec+1,totString.length);

			if(postDec.length > 3)
				postDec = postDec.substring(0,3);
			else 
			{
				if(postDec.length < 3)
				{
				    if(postDec.length == 0)			
						postDec ="000";
				     if(postDec.length == 1)			
						postDec = postDec + "00";
				     if(postDec.length == 2)			
						postDec =postDec + "0";
				}
			}

			totString = preDec+"."+postDec;
		}
		else
			totString += ".000";

		document.pgInterestRateUpdate.pgInterestRateUpdate_tbBestRate.value = totString;
		currFieldToValidate = "";
	}
}
//==================== End Local JavaScript =======================================
//-->

function SetPrimeIndexHide()
{
//	var theBaseIndexId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_cbBaseIndex.value"); // #3533
	var hdBaseIndexId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdBaseIndexId.value");
	var hdBaseIndexDummyId = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_hdDummy.value");

//alert("theBaseIndexId: " + theBaseIndexId);
//alert("hdBaseIndexId: " + hdBaseIndexId);
//alert("hdBaseIndexDummyId : " + hdBaseIndexDummyId);
	
//	var theTeaserRateCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txTeaserRate"); // #3533
	var thePostedRateCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_tbPostedRate");
//	var theBaseRateCtl = eval("document.forms[0].<%= viewBean.PAGE_NAME%>_txBaseRate"); // #3533

	if(hdBaseIndexId == 0) //// 'No Prime Index Rate Selected'
	{	
	    //theTeaserRateCtl.disabled = false; 
	    thePostedRateCtl.disabled = false; 
	    //theBaseRateCtl.disabled = false;
	}
	else if(hdBaseIndexId != 0  && hdBaseIndexDummyId != hdBaseIndexId) //// any other case
	{
	    alert(ASSIGN_NEW_PRIME_RATE_FIRST);
	    //theTeaserRateCtl.disabled = true; 
	    //thePostedRateCtl.disabled = true; 
	    //theBaseRateCtl.disabled = true; 	
	}
	
}

function initPage(event)
{
	// init SetPrimeIndexHide
	SetPrimeIndexHide();
}

</SCRIPT>

</HEAD>
<body bgcolor=ffffff link=000000 vlink=000000 alink=000000 onload="initPage(event)";>
<jato:form name="pgInterestRateUpdate" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">


//////////////////////////////////////////////

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<jato:hidden name="hdBaseIndexId" />

<p>
<center>
	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>
<!--
<form>
-->
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" escape="true" /></font></td>
	</tr>
</table>

<!-- START BODY OF PAGE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=8 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Mise � jour des taux:</b></font></td><tr>

<td colspan=8>&nbsp;</td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Code:</b></font> </td>
<td valign=top><font size=2 color=3366cc><b>Description:</b></font></td>
<td valign=top colspan=5><font size=2 color=3366cc><b>Statut du taux:</b></td><tr>


<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2><jato:text name="stCode" escape="true" formatType="string" formatMask="??????????" /></font></td>
<td valign=top><font size=2><jato:text name="stDescription" escape="true" /></font></td>
<td valign=top><font size=2><jato:combobox name="cbStatus" /></font></td><tr>

<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td valign=top>&nbsp;&nbsp;&nbsp;</td>
<td valign=top ><font size=2 color=3366cc><b>Entr�e en vigueur:</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Heure (24h):</b></td>
<td valign=top><font size=2 color=3366cc><b>Taux affich�:</td>
<td valign=top><font size=2 color=3366cc><b>Escompte maximale <br>autoris�e:</b></td>
<td valign=top><font size=2 color=3366cc><b>Meilleur taux:</b></font></td>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2>Mois: <jato:combobox name="cbMonth" /> Jour: 
<jato:textField name="tbDay" size="2" maxLength="2" /> An: 
<jato:textField name="tbYear" size="4" maxLength="4" /> </font></td>
<td valign=top><font size=2>
<jato:textField name="tbHour" size="2" maxLength="2" /> : 
<jato:textField name="tbMinute" size="2" maxLength="2" /></font></td>
<td valign=top><font size=2>
<jato:textField name="tbPostedRate" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</font></td>
<td valign=top><font size=2>
<jato:textField name="tbMaxDiscountRate" formatType="decimal" formatMask="###0.000; -#" size="6" maxLength="6" /> %</font></td>
<td valign=top><font size=2>
<jato:textField name="tbBestRate" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</font></td>
<tr>

<td colspan=8>&nbsp;</td><tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc></font></td>
<jato:text name="stIncludePrimeIndxSectionStart" escape="false" />
<td valign=top><font size=2 color=3366cc><b>Terme d'introduction<br>(jours):</b></font> </td>
<td valign=top><font size=2 color=3366cc><b>Taux d'introduction:</b></font></td>
<td valign=top colspan=5><font size=2 color=3366cc><b>Taux d'introduction r�duit:</b></td><tr>
<tr>


<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2></td>
<td valign=top><font size=2><font size=2><jato:textField name="txTeaserTerms" size="4" maxLength="4" /></font></td>
<td valign=top><font size=2><jato:textField name="txTeaserRate" formatType="decimal" formatMask="###0.000; -#" size="6" maxLength="6" /> %</font></td>
<td valign=top><font size=2><jato:textField name="txTeaserDiscount" formatType="decimal" formatMask="###0.000; -#" size="6" maxLength="6" /> %</font></td><tr>

<td colspan=8>&nbsp;</td><tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Indexe de base:</b></font> </td>
<td valign=top><font size=2 color=3366cc><b>Derni�rement actualis�:</b></font> </td>
<td valign=top><font size=2 color=3366cc><b>Taux de base:</b></font></td>
<td valign=top colspan=5><font size=2 color=3366cc><b>Modification de base:</b></td><tr>
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" />

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<jato:text name="stIncludePrimeIndxSectionStart" escape="false" />
<td valign=top><font size=2><jato:combobox name="cbBaseIndex" /></font></td>
<td valign=top><font size=2><jato:text name="stBaseIndxLastUpdateTime" fireDisplayEvents="true" escape="true" formatType="date" formatMask="fr|MMM dd yyyy hh:mm" /></font></td>
<td valign=top><font size=2><jato:textField name="txBaseRate" formatType="decimal" formatMask="###0.000; -#" size="6" maxLength="6" /> %</font></td>
<td valign=top><font size=2><jato:textField name="txBaseAdjustment" formatType="decimal" formatMask="###0.000; -#" size="6" maxLength="6" /> %</font></td>
<jato:text name="stIncludePrimeIndxSectionEnd" escape="false" />
<tr>

<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</table>

<!-- Sample : Submit and Cancel buttons //-->
<br><br>
 <table border=0 width=100%>
<td align=right><jato:button name="btRecalcRate" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/recalculate_fr.gif" />
<jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> <jato:button name="btOk" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR><jato:hidden name="hdDummy" />
<BR>
<BR>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}


//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
