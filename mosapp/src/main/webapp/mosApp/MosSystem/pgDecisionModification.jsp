
<HTML>
<%@page info="pgDecisionModification" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDecisionModificationViewBean">

<HEAD>
<TITLE>Decision Modification</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
</HEAD>
<body bgcolor=ffffff onKeyPress="return submitenter(event);">
<jato:form name="pgDecisionModification" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">
var fieldname = "";

function getfocus()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	fieldname = theField.name;
}

function setFocus()
{
	fieldname = 'tools';
}

function submitenter(e)
{
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;

	if(keycode == 13)
	{
	
		if (fieldname == 'pgDecisionModification_btCancel' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += "?pgDecisionModification_btCancel=";
			document.pgDecisionModification.submit();
			return false;
		}
			
		if (fieldname == 'pgDecisionModification_btProceed' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btProceed=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'pgDecisionModification_btWorkQueueLink' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btWorkQueueLink=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'pgDecisionModification_btToolHistory' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btToolHistory=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'pgDecisionModification_btTooNotes' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btTooNotes=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'pgDecisionModification_btToolSearch' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btToolSearch=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'pgDecisionModification_btToolLog' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btToolLog=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'pgDecisionModification_btPrevTaskPage' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btPrevTaskPage=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'pgDecisionModification_btNextTaskPage' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btNextTaskPage=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'pgDecisionModification_btNextTaskPage' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btNextTaskPage=';
			document.pgDecisionModification.submit();
			return false;
		}

		if (fieldname == 'pgDecisionModification_btSubmit' )
		{
			setSubmitFlag(true);
			document.pgDecisionModification.action += '?pgDecisionModification_btSubmit=';
			document.pgDecisionModification.submit();
			return false;
		}
		if (fieldname == 'help' )
		{
			setSubmitFlag(false);
			OpenHelpBrowser();			
			return false;
		}
		if (fieldname == 'tools' )
		{
			setSubmitFlag(false);
			tool_click(3);			
			return false;
		}
		
	}	
	return true;
}


var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_en.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
    
    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 
	
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_en.txt" %> <br>

<!--END DEAL SUMMARY SNAPSHOT//-->


<!-- START BODY OF PAGE//-->

<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=4 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Deal Information</b></font></td><tr>
<td colspan=4>&nbsp;</td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

<td valign=top><font size=2 color=3366cc><b>Issue Date:</b></font><br>
<font size=2><jato:text name="stDealIssueDate" escape="true" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Expiration Date:</b></font><br>
<font size=2><jato:text name="stDealExpirationDate" escape="true" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Acceptance Date:</b></font><br>
<font size=2><jato:text name="stDealAcceptDate" escape="true" /></font></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;</td>

<td valign=top colspan=3><font size=2 color=3366cc><b>Primary Property Address:</b></font><br>
<font size=2><jato:text name="stStreetNumber" escape="true" />  <jato:text name="stStreetName" escape="true" />  <jato:text name="stStreetType" escape="true" /> <jato:text name="stStreetDirection" escape="true" /> <jato:text name="stAddressLine2" escape="true" /></font></td><tr>

<td colspan=4><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=4><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</table>
<p>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td colspan=2><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=2><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=2 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Decision Modification</b></font></td><tr>
<td colspan=2>&nbsp;</td><tr>

<td colspan=2><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

<td valign=top><font size=2 color=3366cc><b>Modify Decision:</b></font><br>
<jato:combobox name="cbDecisionModificationType" extraHtml="onFocus='getfocus();'" /></td><tr>

<td colspan=2><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=2><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</table>

<P>
<table border=0 width=100% cellpadding=0 cellspacing=0>
<!--
<td align=right><img src="../images/submit.gif" width=86 height=25 alt="" border="0">&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/cancel.gif" width=86 height=25 alt="" border="0"></td>
-->
<td align=right><jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0'  onFocus='getfocus();' onClick = 'setSubmitFlag(true);'" src="../images/submit.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0'  onFocus='getfocus();' onClick = 'setSubmitFlag(true);'" src="../images/sign_cancel.gif" /></td>
</table>
</center>
<!-- END BODY OF PAGE//-->

</div>
<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>


</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>