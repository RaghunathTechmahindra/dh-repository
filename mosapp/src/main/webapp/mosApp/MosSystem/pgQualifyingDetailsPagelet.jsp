<%--
 /**
 *Title: pgQualifyingDetailsPagelet.jsp
 *Description:Pagelet to display Qualifying Details from POS.
 *@author:MCM Impl Team
 *@version 1.0 06-JUN-2008 XS_2.13 Initial Version 
 */
--%>
<%@ page info ="pgQualifyingDetailsPagelet" language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:pagelet>
 <jato:text name="stQualifyingDetailsSectionStart" escape="false" />
     <table border="0" width="100%" cellpadding="0" cellspacing="0"
      bgcolor="d1ebff">
      <td colspan="11"><img src="../images/light_blue.gif" width="1"
            height="1" alt="" border="0"></td>
        <tr>
          <td colspan="11"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td>
        <tbody> 
        
        <td colspan=9><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
        <tr>
          <td valign="top"><font size="3" color="3366cc"><b> &nbsp;&nbsp;&nbsp;Qualifying Details from Source of Business </b></font>
     
         <jato:text name="stQualifyingDetailsFieldsStart" escape="false" />
          </td></tr>
         <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#d1ebff">
      <td align="center" colspan="8"><img src="/images/white.gif" width="10" height="1"><img src="/images/blue_line.gif" width="100%"
        height="1" alt="" border="0"></td>
        <tr>
          <td colspan="7" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
        </tr>
         <tr>
              <td valign="top" width="3%"><font size="2" color="3366cc"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;</b></font><br></td>
              <td valign="top" width="12%"><font size="2" color="3366cc"><b>Rate:</b></font><br></td>
              <td valign="top" width="13%"><font size="2" color="3366cc"><b>Compounding Period:</b></font><br></td>
              <td valign="top" width="13%"><font size="2" color="3366cc"><b>Amortization:</b></font><br></td>
              <td valign="top" width="13%"><font size="2" color="3366cc"><b>Repayment Type:</b></font><br></td>
              <td valign="top" width="13%"><font size="2" color="3366cc"><b>Payment:</b></font><br></td>
              <td valign="top" width="13%"><font size="2" color="3366cc"><b>GDS:</b></font><br></td>
              <td valign="top" width="13%"><font size="2" color="3366cc"><b>TDS:</b></font><br></td>
              
         </tr> 
         <tr>
             <td valign="top" width="4%"><font size="2" color="3366cc"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</b></font><br></td>   
              <td width="12%"><font size="2">
                <jato:text name="stRate" escape="true" formatType="decimal" formatMask="###0.000; (-#)" />%
              </font></td>
              <td width="13%"><font size="2">
                <jato:text name="stCompoundingPeriod" escape="true" />
              </font></td>
              <td width="13%"><font size="2">
                <jato:text name="stAmortizationYrs" escape="true" formatType="decimal" formatMask="###0; (-#)" />&nbsp;Yrs&nbsp;&nbsp;&nbsp;<jato:text name="stAmortizationMths" escape="true" formatType="decimal" formatMask="###0; (-#)" />&nbsp;Mths
                </font></td>
              <td width="13%"><font size="2">
                <jato:text name="stRepaymentType" escape="true" />
                </font></td>
              <td width="13%"><font size="2"><jato:text name="stPayment" escape="true" formatType="currency" formatMask="$#,##0.00; (-#)"   />
                </font></td>
              <td width="13%"><font size="2">
                <jato:text name="stGDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)"  />
                </font></td>
              <td width="13%"><font size="2">
                <jato:text name="stTDS" escape="true" formatType="decimal" formatMask="###0.00; (-#)" />
                </font></td>
                </tr>
        <tr>
          <td colspan="7" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
        </tr>
        <tr>
        <td colspan="11"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="11"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td></tr>
        </tr>
        </table>
        <jato:text name="stQualifyingDetailsFieldsEnd" escape="false" />
        
        <jato:text name="stQualifyingDetailsMessageStart" escape="false" />
        <font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Not Available</font>
        <tr>
        <tr>
          <td colspan="7" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
        </tr>
        <td colspan="11"><img src="../images/light_blue.gif" width="1"
            height="2" alt="" border="0"></td>
        </tr>
        <tr>
          <td colspan="11"><img src="../images/blue_line.gif" width="100%"
            height="1" alt="" border="0"></td></tr>
        <jato:text name="stQualifyingDetailsMessageEnd" escape="false" />
        </tbody>
    </table>
 <jato:text name="stQualifyingDetailsSectionEnd" escape="false" />
</jato:pagelet>
    
