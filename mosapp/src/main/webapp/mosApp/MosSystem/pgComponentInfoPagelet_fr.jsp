<%--
 /**Title:pgComponentInfoPagelet.jsp
 * Description:Pagelet to display fields of Component Summary.
 * @author: MCM Impl Team.
 * @version 1.0 2008-6-11
 * @version 1.1 2008-7-17 added each component ties in component info section 
 * @version 1.2 2008-7-21 fixed format for amount, netRate, Discount, premium, BuyDownRate
 * @version 1.3 2008-7-23 fixed Tax payment Escrow title
 * @version 1.3e 2008-7-24 copied to French screen and mofieied
 * @version 1.4 2008-7-25 fixed Total Payment for Loan (artf750799 )
 * @version 1.5 2008-7-28-31 XS 2.60 added ComponentId and CopyId as hidden field for each component tile
 *                                                  added js to control check box enable/disable, only one can be selected
 * @version 1.6 Aug 21, 2008, artf763316 Aug 21, 2008 : replacing XS_2.60.
 * @version 1.7 Sep 25, 2008, FXP22611 changed formatMask "fr|###0,000;-#" to "fr|###0.000; (-#)"
 */
--%>
<%@page info="pgComponentInfoPagelet" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>

	<jato:pagelet>
	
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr>
			<td colspan=11><img src="../images/blue_line.gif" width=100%
				height=1 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=11><img src="../images/light_blue.gif" width=1
				height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan=11 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Renseignements sur la composante
		</b></font></td>
		</tr>
		<tr>
			<td colspan=5>&nbsp;</td>
		</tr>
		<tr>
			<td colspan=11><img src="../images/light_blue.gif" width=1
				height=2 alt="" border="0"></td>
		</tr>
		<tr>
			<td valign=top>&nbsp;&nbsp;&nbsp;</td>
			<td valign=top><font size=2 color=3366cc><b>Sommaire:
			</b></font></td>
			<td valign=top><font size=2 color=3366cc> <b>Montant total:</b> </font>
			<font size =2 ><jato:text name="stTotalAmount" escape="true" 
			formatType="currency" formatMask="fr|#,##0.00$; (-#)" />
			</font></td>
			<td valign=top><font size=2 color=3366cc><b>Montant non attribu�:</b></font> 
			<font size=2 ><jato:text name="stUnallocatedAmount" escape="false" formatType="currency" formatMask="fr|#,##0.00$;"/>
			</font></td>
			<td valign=top><font size=2 color=3366cc><b>Montant total de la remise en argent:</b></font>
			 <font size =2> <jato:text name="stCashbackAmount" escape="true" formatType="currency" 
			formatMask="fr|#,##0.00$; (-#)" /></font></td>
		</tr>
		<tr>
			<td colspan=12><img src="../images/blue_line.gif" width=100%
				height=1 alt="" border="0"></td>
		</tr>
	</table>
	<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
		<tr><td colspan=12><img src="../images/light_blue.gif" width=100%
				height=5 alt="" border="0"></td></tr>

		<tr>
			<td width="40" />
			<td width="100" align="left"><font size=2 color=3366cc><b>Type:</b></font></td>
			<td width="230" align="left"><font size=2 color=3366cc><b>Produit:</b></font></td>
			<td width="70" align="right"><font size=2 color=3366cc><b>Montant:</b></font></td>
			<td  width="150" align="center"><font size=2 color=3366cc><b>Inclure la prime d�assurance:</b></font></td>
			<td width="100" align="right"><font size=2 color=3366cc><b>Montant du paiement:</b></font></td>
			<td  width="100" align="center"><font size=2 color=3366cc><b>Inclure paiement des taxes:</b></font></td>
			<td  width="70" align="right"><font size=2 color=3366cc><b>Taux net:</b></font></td>
			<td  width="70" align="right"><font size=2 color=3366cc><b>Escompte:</b></font></td>
			<td  width="70" align="right"><font size=2 color=3366cc><b>Prime:</b></font></td>
			<td  width="100" align="right"><font size=2 color=3366cc><b>Taux d�int�r�t r�duit:</b></font></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan=12><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
		</tr>
		<!-- MCM TEAM XS 2.8 TILED PART STARTS -->
		<!-- Mortgage Component -->
		
		<jato:tiledView name="TiledMTGComp" type="mosApp.MosSystem.pgCompInfoMTGTiledView">
			<tr>
				<td><jato:hidden name="hdComponentId" /> <jato:hidden name='hdCopyId' /></td>
				<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
				<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
				<td align=center><font size=2><jato:checkbox name="chAllocateMIPremium" styleClass="allocateMIPremium" onClick="turnOffOtherAllocateMIPremium(this)"/></font></td>
				<td align=right><font size=2><jato:text name="stPaymentAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
				<td align=center><font size=2><jato:checkbox name="chAllocateTaxEscrow" styleClass="allocateTaxEscrow" onClick="turnOffOtherAllocateTaxEscrow(this)"/></font></td>
				<td align=right><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td width="100" align=right><font size=2><jato:text name="stBuyDownRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan=12>
					<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
				</td>
			</tr>
		</jato:tiledView>
		<!-- LOC COMPONENT -->
		<jato:tiledView name="TiledLOCComp" type="mosApp.MosSystem.pgCompInfoLOCTiledView">
			<tr>
				<td><jato:hidden name="hdComponentId" /> <jato:hidden name='hdCopyId' /></td>
				<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
				<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
				<td align=center><font size=2><jato:checkbox name="chAllocateMIPremium" styleClass="allocateMIPremium" onClick="turnOffOtherAllocateMIPremium(this)" /></font></td>
				<td align=right><font size=2><jato:text name="stPaymentAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
				<td align=center><font size=2><jato:checkbox name="chAllocateTaxEscrow" styleClass="allocateTaxEscrow" onClick="turnOffOtherAllocateTaxEscrow(this)"/></font></td>
				<td align=right><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td />
				<td />
			</tr>
			<tr>
				<td colspan=12>
					<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
				</td>
			</tr>
		</jato:tiledView>

		<!-- CREDIT CARD COMPONENT -->
		<jato:tiledView name="TiledCCComp" type="mosApp.MosSystem.pgCompInfoCreditCardTiledView">
			<tr>
				<td />
				<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
				<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
				<td></td>
				<td></td>
				<td></td>
				<td align=right><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td></td>
				<td></td>
				<td></td>
				<td />
			</tr>
			<tr>
				<td colspan=12>
					<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
				</td>
			</tr>
		</jato:tiledView>

		<!-- LOAN COMPONENT -->
		<jato:tiledView name="TiledLoanComp" type="mosApp.MosSystem.pgCompInfoLoanTiledView">
			<tr>
				<td />
				<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
				<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
				<td></td>
				<td align=right><font size=2><jato:text name="stTotalPayment" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
				<td></td>
				<td align=right><font size=2><jato:text name="stNetRate" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stDiscount" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stPremium" formatType="decimal" formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td></td>
				<td />
			</tr>
			<tr>
				<td colspan=12>
					<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
				</td>
			</tr>
		</jato:tiledView>


		<!-- OVER DRAFT COMPONENT -->		
		<jato:tiledView name="TiledODComp" type="mosApp.MosSystem.pgCompInfoOverdraftTiledView">
			<tr>
				<td>&nbsp;&nbsp;&nbsp;</td>
				<td><font size=2><jato:text name="stComponentType" fireDisplayEvents="false" escape="true" /></font></td>
				<td><font size=2><jato:text name="stProductName" fireDisplayEvents="false" escape="true" /></font></td>
				<td align=right><font size=2><jato:text name="stAmount" formatType="currency" formatMask="fr|#,##0.00$; (-#)" escape="true" /></font></td>
				<td></td>
				<td></td>
				<td></td>
				<td align=right><font size=2><jato:text name="stNetRate" formatType="decimal"  formatMask="fr|###0.000; (-#)" escape="true" /></font></td>
				<td></td>
				<td></td>
				<td></td>
				<td>&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan=12>
					<img src="../images/blue_line.gif" width=100% height=1 alt="" border="0">
				</td>
			</tr>
		</jato:tiledView>

		<tr>
		<td colspan = 10> </td><td><jato:button name="btComponentDetail" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/component_detail_fr.gif" /></td>
		</tr>
		<tr>
			<td colspan=12><img src="../images/blue_line.gif" width=100%
				height=1 alt="" border="0"></td>
		</tr>
	</table>

	<table border=0 width=100% cellpadding=0 cellspacing=0>
		<td align=right><a onclick="return IsSubmited();" href="#top"><img
			src="../images/return_top_sm_fr.gif" width=83 height=15 alt=""
			border="0"></a></td>
		<tr>
	</table>

	</jato:pagelet>


