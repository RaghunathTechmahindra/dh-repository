<%--
 /*
  * MCM Impl Team. Change Log
  * XS_2.46 -- 06/09/2008  -- Added Additional Details and Existing Account Reference fields in Refinance section.
  * XS_2.44 -- July 21 2008  -- Added Component Info section as pagelet
  * artf763316 Aug 21, 2008 : replacing XS_2.60.
 */
  --%>
	
	<!--PAGE BODY//-->

	<!--START PROGRESS ADVANCE//-->
<jato:hidden name="hdForceProgressAdvance" />
	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">

		<td colspan="5"><img src="../images/blue_line.gif" width="100%"
			height="1" alt="" border="0"></td>
		<tbody>
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<td colspan="5" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;Progress
				Advance</b></font></td>
			</tr>
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
			<tr>

				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<td valign="top">&nbsp;&nbsp;&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Progress Advance:</b></font></td>
				<td valign="top"><font size="2"> 
					<jato:radioButtons name="rbProgressAdvance" layout="horizontal" extraHtml="onClick='hideSubProgressAdvance();'" /><%--Release3.1 extraHtml--%>
					</font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Next Advance
				Amount:</b></font></td>
				<td valign="top">$ <jato:textField name="txNextAdvanceAmount"
					formatType="currency" formatMask="###0.00; (-#)" size="14"
					maxLength="14" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Advance to Date
				Amount:</b></font></td>
				<td valign="top">$ <jato:textField name="txAdvanceToDateAmt"
					formatType="currency" formatMask="###0.00; (-#)" size="14"
					maxLength="14" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Advance Number:</b></font></td>
				<td valign="top"><jato:textField name="txAdvanceNumber"
					formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
			</tr>
			<tr>

				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
		</tbody>
	</table>

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<td align="right"><a onclick="return IsSubmited();" href="#top"><img
			src="../images/return_top_sm.gif" width="83" height="15" alt=""
			border="0"></a></td>
		<tbody>
			<tr>
			</tr>
		</tbody>
	</table>
	<!--END PROGRESS ADVANCE//-->

	<!--START OTHER DETAILS//-->

	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">

		<td colspan="5"><img src="../images/blue_line.gif" width="100%"
			height="1" alt="" border="0"></td>
		<tbody>
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<td colspan="5" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;Other
				Details</b></font></td>
			</tr>
			<tr>

				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"></td>
				<td valign="top"></td>
				<td valign="top"><font size="2" color="3366cc"><b><jato:text
					name="stBranchTransitLabel" escape="false" fireDisplayEvents="true" /></b></font></td>
				<td valign="top"><font size="2"><jato:text
					name="stBranchTransitNumber" escape="false"
					fireDisplayEvents="true" />&nbsp;<font size="2"><jato:text
					name="stPartyName" escape="false" fireDisplayEvents="true" />&nbsp;<font
					size="2"><jato:text name="stAddressLine1" escape="false"
					fireDisplayEvents="true" />&nbsp;<font size="2"><jato:text
					name="stCity" escape="false" fireDisplayEvents="true" /></font></font></font></font></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Deal Purpose:</b></font></td>
				<td valign="top"><jato:combobox name="cbUWDealPurpose"
					extraHtml="onChange='DisplayOrHideRefiSection();'" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Branch:</b></font></td>
				<td valign="top"><jato:combobox name="cbUWBranch" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Deal Type:</b></font></td>
				<td valign="top"><jato:combobox name="cbUWDealType" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Cross Sell:</b></font></td>
				<td valign="top"><jato:combobox name="cbUWCrossSell" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Special Feature:</b></font></td>
				<td valign="top"><jato:combobox name="cbUWSpecialFeature" /></td>
                <%-- LEN497489 --%>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Pre-App.Est.Purchase
				Price:</b></font></td>
				<td valign="top">$ <jato:textField name="txUWPreAppEstPurchasePrice"
					formatType="currency" formatMask="###0.00; (-#)" size="16"
					maxLength="16" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Reference Type:</b></font></td>
				<td valign="top"><jato:combobox name="cbUWReferenceType" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Repayment Type:</b></font></td>
				<td valign="top"><jato:combobox name="cbUWRepaymentType" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Reference Deal #:</b></font></td>
				<td valign="top"><jato:textField name="txUWReferenceDealNo"
					formatType="decimal" formatMask="###0; (-#)" size="10"
					maxLength="10" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Second Mortgage
				Exists?</b></font></td>
				<td valign="top"><jato:combobox name="cbUWSecondMortgageExist" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Estimated Closing
				Date:</b></font></td>
				<td valign="top"><font size="2">Mth: </font><jato:combobox
					name="cbUWEstimatedClosingDateMonth" /> <font size="2">Day: </font>
				<jato:textField name="txUWEstimatedClosingDateDay"
					formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" />
				<font size="2">Yr: </font> <jato:textField
					name="txUWEstimatedClosingDateYear" formatType="decimal"
					formatMask="###0; (-#)" size="4" maxLength="4" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><jato:text name="stIncludeFinancingProgramStart"
					escape="false" /><font size="2" color="3366cc"><b>Financing
				Program:</b></font><jato:text name="stIncludeFinancingProgramEnd"
					escape="false" /></td>
				<td valign="top"><jato:text name="stIncludeFinancingProgramStart"
					escape="false" /><jato:combobox name="cbUWFinancingProgram" /><jato:text
					name="stIncludeFinancingProgramEnd" escape="false" /></td>

				<td valign="top"><font size="2" color="3366cc"><b>First Payment
				Date:</b></font></td>
				<td valign="top"><font size="2">Mth: </font><jato:combobox
					name="cbUWFirstPaymetDateMonth" /> <font size="2">Day: </font> <jato:textField
					name="txUWFirstPaymetDateDay" formatType="decimal"
					formatMask="###0; (-#)" size="2" maxLength="2" /> <font size="2">Yr:
				</font> <jato:textField name="txUWFirstPaymetDateYear"
					formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Tax Payor:</b></font></td>
				<td valign="top"><jato:combobox name="cbUWTaxPayor" elementId="cbTaxPayor" onChange="flipChIncludeTaxPayments(this)"/></td>
				<td valign="top"><font size="2" color="3366cc"><b>Maturity Date:</b></font></td>
				<td valign="top"><font size="2"><jato:text name="stUWMaturityDate"
					escape="true" formatType="date" formatMask="MMM dd yyyy" /></font></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Rate Locked In?</b></font></td>
				<td valign="top"><jato:combobox name="cbUWRateLockedIn" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Commitment
				Expected Return Date:</b></font></td>
				<td valign="top"><font size="2">Mth: </font><jato:combobox
					name="cbUWCommitmentExpectedDateMonth" /> <font size="2">Day: </font>
				<jato:textField name="txUWCommitmentExpectedDateDay" size="2"
					maxLength="2" /> <font size="2">Yr: </font> <jato:textField
					name="txUWCommitmentExpectedDateYear" size="4" maxLength="4" /></td>
			</tr>
            <%--CR03 start --%>
            <tr>
				<td>&nbsp;<jato:hidden name="hdCommitmentExpiryDate" />
				<input type="hidden" id="hdCommitmentExpiryDate" value='<jato:text name="hdCommitmentExpiryDate"/>'/>
				<jato:hidden name="hdMosProperty" />
				<input type="hidden" id="hdMosProperty" value='<jato:text name="hdMosProperty"/>'/>
				</td>
				<td valign="top">&nbsp;</td>
				<td valign="top">&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Automatically Calculate 
				Commitment Expiration Date:</b></font></td>
				<td valign="top"><font size="2"> 
					<jato:radioButtons name="rbAutoCalculateCommitmentDate" layout="horizontal" extraHtml="id='rbAutoCalculateCommitmentDate' 
					      onClick='changeExpirationDate();'" />
					</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Financing Waiver Date:</b></font></td>
				<td valign="top"><font size="2">Mth: </font><jato:combobox
					name="cbUWFinancingWaiverDateMonth" extraHtml="id='cbUWFinancingWaiverDateMonth'" /> <font size="2">Day: </font>
				<jato:textField name="txUWFinancingWaiverDateDay" size="2"
					maxLength="2" extraHtml="id='txUWFinancingWaiverDateDay'"/> <font size="2">Yr: </font> <jato:textField
					name="txUWFinancingWaiverDateYear" size="4" maxLength="4" extraHtml="id='txUWCFinancingWaiverDateYear'"/></td>
				<td valign="top"><font size="2" color="3366cc"><b>Commitment Expiration Date:</b></font></td>
				<td valign="top"><font size="2">Mth: </font><jato:combobox
					name="cbUWCommitmentExpirationDateMonth" extraHtml="id='cbUWCommitmentExpirationDateMonth'" /> <font size="2">Day: </font>
				<jato:textField name="txUWCommitmentExpirationDateDay" size="2"
					maxLength="2" extraHtml="id='txUWCommitmentExpirationDateDay'"/> <font size="2">Yr: </font> <jato:textField
					name="txUWCommitmentExpirationDateYear" size="4" maxLength="4" extraHtml="id='txUWCommitmentExpirationDateYear'"/></td>
			</tr>
            <%--CR03 end --%>
			<tr>
				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b><jato:text
					name="stMarketTypeLabel" escape="false" fireDisplayEvents="true" /></b></font></td>
				<td valign="top"><font size="2"><jato:text name="stMarketType"
					escape="false" fireDisplayEvents="true" /></font></td>
				<%--***** Change by NBC Impl. Team - Version 1.2 - Start*****--%>			
				<td valign="top"><font size="2" color="3366cc"><b>Affiliation Program</b></font></td>
				<td valign="top"><jato:combobox name="cbAffiliationProgram" extraHtml="onChange='DisplayOrHideRefiSection();'" /></td>
				<%--***** Change by NBC Impl. Team - Version 1.2 - End*****--%>
			</tr>
			<tr>


				<td>&nbsp;</td>
				<td colspan="4"><jato:button name="btUWPartySummary"
					extraHtml="onClick = 'setSubmitFlag(true);'"
					src="../images/party_summary.gif" /></td>
			</tr>
			<tr>

				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
		</tbody>
	</table>

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<td align="right"><a onclick="return IsSubmited();" href="#top"><img
			src="../images/return_top_sm.gif" width="83" height="15" alt=""
			border="0"></a></td>
		<tbody>
			<tr>
			</tr>
		</tbody>
	</table>

	<!--END OTHER DETAILS//-->


	<!--START Refinance Info.//-->
	<div id="refisection" class="refisection" name="refisection">
	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">

		<td colspan="5"><img src="../images/blue_line.gif" width="100%"
			height="1" alt="" border="0"></td>
		<tbody>
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<td colspan="5" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;Equity
				Take-Out/Switch/Refinance Information</b></font></td>
			</tr>
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
			<tr>

				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Date Purchased:</b></font></td>
				<td valign="top"><font size="2">Mth: </font><jato:combobox
					name="cbUWRefiOrigPurchaseDateMonth" /> <font size="2">Day: </font>
				<jato:textField name="txUWRefiOrigPurchaseDateDay" size="2"
					maxLength="2" /> <font size="2">Yr: </font> <jato:textField
					name="txUWRefiOrigPurchaseDateYear" size="4" maxLength="4" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Purpose:</b></font></td>
				<td valign="top"><jato:textField name="txUWRefiPurpose" size="35"
					maxLength="80" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Mortgage Holder:</b></font></td>
				<td valign="top"><jato:textField name="txUWRefiMortgageHolder"
					size="35" maxLength="80" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Blended
				Amortization:</b></font></td>
				<td valign="top"><font size="2"> <jato:radioButtons
					name="rbBlendedAmortization" layout="horizontal" /></font></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Original Purchase
				Price:</b></font></td>
				<td valign="top">$ <jato:textField name="txUWRefiOrigPurchasePrice"
					formatType="currency" formatMask="###0.00; (-#)" size="14"
					maxLength="14" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Original Mtg.
				Amount:</b></font></td>
				<td valign="top">$ <jato:textField name="txUWRefiOrigMtgAmount"
					formatType="currency" formatMask="###0.00; (-#)" size="14"
					maxLength="14" /></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Value of
				Improvements:</b></font></td>
				<td valign="top">$ <jato:textField name="txUWRefiImprovementValue"
					formatType="currency" formatMask="###0.00; (-#)" size="14"
					maxLength="14" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Outstanding Mtg.
				Amount:</b></font></td>
				<td valign="top">$ <jato:textField
					name="txUWRefiOutstandingMtgAmount" formatType="currency"
					formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
			</tr>
<%--
/***************MCM Impl team changes starts - XS_2.46 *******************/
--%>			
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Improvements:</b></font></td>
<%-- Release3.1 Apr 18, 2006 begins --%>
				<td valign="top"><jato:textField name="txUWRefiImprovementsDesc" size="35" maxLength="80" /></td>
				<TD vAlign=top><FONT color=#3366cc size=2><B>Existing Account Reference:</B></FONT></TD>
				<td valign="top"><jato:textField name="txUWReferenceExistingMTGNo" size="35"  maxLength="35"/></td>
<%-- Release3.1 Apr 18, 2006 ends / LEN497489 --%>

				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Product Type:</b></font></td>
				<td valign="top"><jato:combobox name="cbRefiProductType" /></td>
				<TD vAlign=top><FONT color=#3366cc size=2><B>&nbsp;</B></FONT></TD>
				<td valign="top">&nbsp;</td>

				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<td>&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Additional Details:</b></font></td>
				<td vAlign=top colspan=3>
					<jato:textArea name="txRefiAdditionalInformation" rows="4" cols="122" 
      extraHtml="wrap=soft maxlength=512 onchange='return isFieldHasSpecialChar();'"/>
				</td>

				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
<%--
/***************MCM Impl team changes ends - XS_2.46 *******************/
--%>			
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
		</tbody>
	</table>

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<td align="right"><a onclick="return IsSubmited();" href="#top"><img
			src="../images/return_top_sm.gif" width="83" height="15" alt=""
			border="0"></a></td>
		<tbody>
			<tr>
			</tr>
		</tbody>
	</table>
	</div>
	<!--END Refinance Info.//-->
<%--
***************MCM Impl team changes Starts - XS_2.44 *******************/
--%>
	<!-- START Component Summary Info -->
	<div id="divComponentInfo">
	<table border=0 width=100% cellpadding=0 cellspacing=0>
		<tr>
			<jato:containerView  name="componentInfoPagelet" type="mosApp.MosSystem.pgComponentInfoPageletViewBean">
				<jsp:include page="pgComponentInfoPagelet.jsp"/>
			</jato:containerView >
		</tr>
	</table>
	</div>
	<!-- End Component Summary Info -->
<%--
***************MCM Impl team changes Starts - XS_2.44 *******************/
--%>
	<!--START MORTGAGE INSURANCE//-->
	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">
		<tbody>
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td colspan="5" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;Mortgage
				Insurance</b></font></td>
			</tr>
			<tr>
				<td colspan="5" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
			</tr>

			<tr>
				<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td valign="top" colspan="1"><font size="2" color="3366cc"><b>Mortgage
				Insurance Indicator:</b></font><br>
				<jato:combobox name="cbUWMIIndicator" /></td>
				<td valign="top" colspan="1"><font size="2" color="3366cc"><b>Mortgage
				Insurance Status</b></font><br>
				<jato:combobox name="cbUWMIStatus" /></td>
				<td valign="top" colspan="2"><font size="2" color="3366cc"><b>MI
				Existing Policy #:</b></font><br>
				<jato:textField name="txUWMIExistingPolicy" size="35" maxLength="35" /></td>
			</tr>

			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td valign="top">&nbsp;</td>

                <td valign="top"><font size="2" color="3366cc"><b>MI Insurer</b></font><br>
                <jato:combobox name="cbUWMIInsurer" elementId="cbMIInsurer" extraHtml="onChange=SetMIPolicyNoDisplay(); hideRequestStandardService();" /></td><%--Release3.1 extraHtml--%>
				
				<td valign="top"><font size="2" color="3366cc"><b>MI Type:</b></font><br>
				<jato:combobox name="cbUWMIType" elementId="cbMIType" extraHtml="onChange=hideRequestStandardService();" /></td>
								
				<td valign="top"><font size="2" color="3366cc"><b>MI Payor:</b></font><br>
				<jato:combobox name="cbUWMIPayor" onChange="selectMIUpfront()" elementId="cbMIPayor" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>Pre Qualification
				MI Certificate #:</b></font><br>
				<font size="2"> <jato:textField name="txUWMIPreQCertNum" size="25"
					maxLength="50" /></font></td>
			</tr>

			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td valign="top">&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>MI Premium:</b></font><br>
				$ <jato:textField name="txUWMIPremium" formatType="decimal"
					formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
				<td valign="top"><font size="2" color="3366cc"><b>MI Certificate #:</b></font><br>
				<font size="2"> <jato:textField name="txUWMICertificate" size="25"
					maxLength="50" /></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>MI Upfront</b></font><br>
				<font size="2"> 
				    <jato:radioButtons name="rbUWMIUpfront"	layout="horizontal" styleClass="rbMIUpfront" onClick="flipChIncludeMIPremiums(this)"/>
				</font></td>
				<td valign="top" colspan="2"><font size="2" color="3366cc"><b>MI RU
				Intervention</b></font><br>
				<font size="2"> <jato:radioButtons name="rbUWMIRUIntervention"
					layout="horizontal" /></font></td>
			</tr>

			<!-- SEAN DJ SPEC-Progress Advance Type July 20, 2005:  -->
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
			  	<td valign="top">&nbsp;</td>
				<td valign="top">
<div id="divProgressAdvanceType" name="divProgressAdvanceType">
          			<font size=2 color=3366cc><b>Progress Advance Type:</b></font><br>
					<jato:combobox name="cbProgressAdvanceType"/>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
				</td>


      <!-- SEAN DJ SPEC-PAT END -->

<%-- Release3.1 Apr 18, 2006 begins --%>

				<TD vAlign=top>
<div id="divProgressAdvanceInspectionBy" name="divProgressAdvanceInspectionBy">

					<FONT color=#3366cc size=2><B>Progress Advance Inspection By:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbProgressAdvanceInspectionBy" layout="horizontal" />
					</FONT>
</div>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Self Directed RRSP:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbSelfDirectedRRSP" layout="horizontal" />
					</FONT>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>CMHC Product Tracker Identifier:</B></FONT><BR>
					<jato:textField name="txCMHCProductTrackerIdentifier" size="5" maxLength="5" />
					
					
				</TD>
			</TR>
			<TR>
				<TD vAlign=top></TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>LOC Repayment:</B></FONT><BR>
					<jato:combobox name="cbLOCRepaymentTypeId" />
				</TD>
				<TD vAlign=top>
<div id="divReqStdSvc" name="divReqStdSvc">
					<FONT color=#3366cc size=2><B>Request Standard Service:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbRequestStandardService" layout="horizontal" />
					</FONT>
</div>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>LOC Amortization:</B></FONT><BR>
					<FONT size=2>
						 <jato:text name="stLOCAmortizationMonths" fireDisplayEvents="true" escape="true" /> 
					</FONT>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>LOC Interest Only Maturity Date:</B></FONT><BR>
					<FONT size=2>
						<jato:text name="stLOCInterestOnlyMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" />
					</FONT> 
				</TD>
			</tr>				
<%-- Release3.1 Apr 18, 2006 ends --%>

<%-- 5.0 MI start --%>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <font color="#3366cc" size="2"><b>MI Response:</b></font>&nbsp;
                    <jato:button name="btExpandMIResponse" src="../images/expand.gif" fireDisplayEvents="true" onClick="javaScript:handleMIResponseExpand(); return false;" />
                    <br />
                    <jato:textField name="txMIResponseFirstLine" size="35" maxLength="35" readOnly="true"/></td>
                </td>
                <td colspan="3"></td>
            </tr>
<%-- 5.0 MI end --%>

			<tr>
				<td valign="top">&nbsp;</td>
				<td colspan="5" td valign="top"><jato:button name="btMIReview"
					extraHtml="width=156 height=15 onClick = 'setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/mortgage_insreview.gif" style="margin-top: 30px;" /></td>
			</tr>

			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>

		</tbody>
	</table>

	<!--END MORTGAGE INSURANCE//-->

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td align="right"><a onclick="return IsSubmited();" href="#top"><img
					src="../images/return_top_sm.gif" width="83" height="15" alt=""
					border="0"></a></td>
			</tr>
		</tbody>
	</table>

	<!--START MORTGAGE OPTIONS//-->
	<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">
		<tbody>
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td colspan="5" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;Mortgage
				Options</b></font></td>
			</tr>
			<tr>
				<td colspan="5" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
			</tr>

			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td valign="top"><font size="2" color="3366cc"><b>&nbsp;&nbsp;&nbsp;&nbsp;Multiproject:</b></font>
				<font size="2"><jato:radioButtons name="rbMultiProject"
					layout="horizontal" /></font></td>

				<td valign="top"><font size="2" color="3366cc"><b>Homeowner's
				Protection:</b></font> <font size="2"><jato:radioButtons
					name="rbProprietairePlus" layout="horizontal" /></font></td>

				<td valign="top"><font size="2" color="3366cc"><b>Line of Credit:</b></font>
				<jato:textField name="txProprietairePlusLOC" formatType="decimal"
					formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
			</tr>

			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
		</tbody>
	</table>

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td align="right"><a onclick="return IsSubmited();" href="#top"><img
					src="../images/return_top_sm.gif" width="83" height="15" alt=""
					border="0"></a></td>
			</tr>
		</tbody>
	</table>
	<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
	<!--END MORTGAGE OPTIONS//-->


	<!--START SOURCE INFORMATION//-->
	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">
		<td colspan="10"><img src="../images/blue_line.gif" width="100%"
			height="1" alt="" border="0"></td>
		<tbody>
		<tr>
			<td valign="top">
				<font size="2" color="3366cc"><b>Reference Source App #:</b></font><br>
				<jato:textField name="tbUWReferenceSourceApplNo" size="35" maxLength="35" />
			</td>
		</tr>
<jato:tiledView name="RepeatedSOB" type="mosApp.MosSystem.pgUWRepeatedSOBTiledView">	
			<tr>
				<td colspan="10"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="6" valign="top"><font size="3" color="3366cc"><b>Source
				Information&nbsp;<jato:text name="stSourceNumber" escape="true" /></b></font><jato:hidden name="hdSourceOfBusinessProfileId" /></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td valign="top"><font size="2" color="3366cc"><b>Source Firm:</b></font><br>
				<font size="2"><jato:text name="stUWSourceFirm" escape="true" /></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Source:</b></font><br>
				<font size="2"><jato:text name="stSourceFirstName" escape="true" />
				<jato:text name="stSourceLastName" escape="true" /></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Source Address:</b></font><br>
				<font size="2"><jato:text name="stUWSourceAddressLine1"
					escape="true" /> <jato:text name="stUWSourceCity" escape="true" />
				<jato:text name="stUWSourceProvince" escape="true" /></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Source Phone #:</b></font><br>
				<font size="2">
				<!-- SEAN Ticket #1674 June 30, 2005: get rid of the format pattern. -->
				<jato:text name="stUWSourceContactPhone"
					escape="true" formatType="string" formatMask="(???)???-????" /> x <jato:text
					name="stUWSourceContactPhoneExt" fireDisplayEvents="true" escape="true" /></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Source Fax #:</b></font><br>
				<font size="2"><jato:text name="stUWSourceFax" escape="true"
					formatType="string" formatMask="(???)???-????" /></font></td>
			</tr>
			<tr>

				<td colspan="6"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<!-- ========== DJ#725 begins ========== -->
				<!-- by Neil at Nov/29/2004 -->
				<!--============Vlad static must be escaped true === -->				
				<!-- contactEmailAddress -->
                <td valign="top"><font size="2" color="3366cc"><b>License Registration Number:</b></font><br>
                    <jato:text name="stLicenseNumber" />
                </td>
				<td valign="top">
					<font size="2" color="3366cc"><b>Email:</b></font><br>
					<font size="2"><jato:text name="stContactEmailAddress" escape="true" />
				</td>
				<!-- psDescription -->
				<td valign="top">
					<font size="2" color="3366cc"><b>Status:</b></font><br>
					<font size="2"><jato:text name="stPsDescription" escape="true" />
				</td>
				<!-- systemTypeDescription -->
				<td valign="top">
					<font size="2" color="3366cc"><b>System Type:</b></font><br>
					<font size="2"><jato:text name="stSystemTypeDescription" escape="true" />
				</td>
				<!-- ========== DJ#725 ends ========== -->
			</tr>
			<tr>
				<td valign="top">&nbsp;<br>
				<jato:button name="btSOBDetails"
					extraHtml="onClick = 'setSubmitFlag(true);'"
					src="../images/details.gif" /> <jato:button name="btChangeSource"
					extraHtml="width=89  height=15 onClick = 'setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/change_source.gif" /></td>
				<td colspan="10"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

	</jato:tiledView>
			<tr>
				<td colspan="10"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>

			</tr>
		</tbody>
	</table>
	<!--END SOURCE INFORMATION//-->
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<td align="right"><a onclick="return IsSubmited();" href="#top"><img
			src="../images/return_top_sm.gif" width="83" height="15" alt=""
			border="0"></a></td>
		<tbody>
			<tr>
			</tr>
		</tbody>
	</table>

	<!--START APPLICANT FINANCIAL DETAILS//-->
	<table border="0" width="100%" cellpadding="0" cellspacing="0"
		bgcolor="d1ebff">

		<td colspan="10"><img src="../images/blue_line.gif" width="100%"
			height="1" alt="" border="0"></td>
		<tbody>
			<tr>
				<td colspan="10"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="10" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;Applicant
				Financial Details</b></font></td>
			</tr>
			<tr>
				<td colspan="10">&nbsp;
<!-- SEAN GECF IV Document Type Drop Down. -->
<jato:text name="stHideIVDocumentTypeStart" escape="false"/>
&nbsp;<font size=2 color=3366cc><b>Income Verification Type:</b></font> <jato:combobox name="cbIVDocumentType"/>&nbsp;&nbsp;&nbsp;&nbsp;
<jato:text name="stHideIVDocumentTypeEnd" escape="false"/>
<!-- END GECF IV Document Type Drop Down. -->
				</td>
			</tr>
			<tr>

				<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Primary?</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Applicant Name:</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Applicant Type:</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Total Income:</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Total Liabilities:</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Total Assets:</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Net Worth:</b></font></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="10"><img src="../images/light_blue.gif" width="1"
					height="1" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="10"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
			<tr>

				<!--START REPEATING ROW//-->
				<jato:tiledView name="RepeatApplicantFinancialDetails"
					type="mosApp.MosSystem.pgUWorksheetRepeatApplicantFinancialDetailsTiledView">
					<td valign="middle">&nbsp;<jato:hidden
						name="hdFinancialApplicantId" /><jato:hidden
						name="hdFinancialApplicanCopyId" /></td>
					<td valign="middle"><jato:combobox name="cbUWPrimaryApplicant"
						extraHtml="onChange=isThereMoreThanOneYes('hdFinancialApplicantId');" /></td>
					<td valign="middle"><font size="2"><jato:text
						name="stFinancialApplicanFirstName" escape="true" /> <jato:text
						name="stFinancialApplicanMiddleInitial" escape="true" /> <jato:text
						name="stFinancialApplicanLastName" escape="true" /> <jato:text
						name="stFinancialApplicanSuffix" escape="true" /></font></td>
					<td valign="middle"><font size="2"><jato:text
						name="stFinancialApplicanType" escape="true" /></font></td>
					<td valign="middle"><font size="2"><jato:text
						name="stFinancialApplicanTotalIncome" escape="true"
						formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
					<td valign="middle"><font size="2"><jato:text
						name="stFinancialApplicanTotalLiabilities" escape="true"
						formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
					<td valign="middle"><font size="2"><jato:text
						name="stFinancialApplicanTotalAssets" escape="true"
						formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
					<td valign="middle"><font size="2"><jato:text
						name="stFinancialApplicantNetWorth" escape="true"
						formatType="currency" formatMask="#,##0.00; -#,##0.00" /></font></td>
					<td valign="middle">&nbsp;<jato:button name="btDeleteApplicant"
						extraHtml="onClick = 'setSubmitFlag(true);'"
						fireDisplayEvents="true" src="../images/delete_app.gif" /></td>
					<td valign="middle"><jato:button name="btApplicantDetails"
						extraHtml="onClick = 'setSubmitFlag(true);'"
						src="../images/details.gif" />&nbsp;<jato:button
						name="btLiabilitiesDetails"
						extraHtml="onClick = 'setSubmitFlag(true);'"
						src="../images/liabilities.gif" /></td>
					<tr>
						<td colspan="10"><img src="../images/blue_line.gif" width="100%"
							height="1" alt="" border="0"></td>
					</tr>
					<tr>
					</tr>
				</jato:tiledView>
				<!--END REPEATING ROW//-->

				<td colspan="10"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td valign="top" colspan="4">&nbsp;<jato:button
					name="btAddApplicant" extraHtml="onClick = 'setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/add_app.gif" />&nbsp;<jato:button
					name="btDupApplicant" extraHtml="onClick = 'setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/Duplicate.gif" />&nbsp;<font
					size="2" color="3366cc"><b>Combined Totals:</b></font></td>
				<td colspan="1"><font size="2"><jato:text
					name="stUWCombinedTotalIncome" escape="true" formatType="currency"
					formatMask="#,##0.00; (-#)" /></font></td>
				<td colspan="1"><font size="2"><jato:text
					name="stUWCombinedTotalLiabilities" escape="true"
					formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
				<td colspan="1"><font size="2"><jato:text
					name="stUWCombinedTotalAssets" escape="true" formatType="currency"
					formatMask="#,##0.00; (-#)" /></font></td>
				<td colspan="1"><font size="2"><jato:text name="stUWTotalNetWorth"
					fireDisplayEvents="true" escape="true" formatType="currency"
					formatMask="#,##0.00; -#,##0.00" /></font></td>
				<td colspan="4">&nbsp;<jato:button name="btCreditReport"
					extraHtml="onClick = 'setSubmitFlag(true);'"
					src="../images/credit_report.gif" /></td>
			</tr>
			<tr>
				<td colspan="10"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
		</tbody>
	</table>

	<!--END APPLICANT FINANCIAL DETAILS//-->


	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<td align="right"><a onclick="return IsSubmited();" href="#top"><img
			src="../images/return_top_sm.gif" width="83" height="15" alt=""
			border="0"></a></td>
		<tbody>
			<tr>
			</tr>
		</tbody>
	</table>

	<!--START PROPERTY INFORMATION//-->
	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">

		<td colspan="8"><img src="../images/blue_line.gif" width="100%"
			height="1" alt="" border="0"></td>
		<tbody>
			<tr>
				<td colspan="8"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="8" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;Property
				Information</b></font></td>
			</tr>
			<tr>
				<td colspan="8"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>

				<!--START REPEATING ROW//-->
				<jato:tiledView name="RepeatPropertyInformation"
					type="mosApp.MosSystem.pgUWorksheetRepeatPropertyInformationTiledView">

					<td colspan="8"><img src="../images/blue_line.gif" width="100%"
						height="1" alt="" border="0"> <jato:hidden name="hdPropertyId" />
					<jato:hidden name="hdPropertyCopyId" /></td>
					<tr>
						<td colspan="8"><img src="../images/light_blue.gif" width="1"
							height="2" alt="" border="0"></td>
					</tr>
					<tr>
						<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td valign="top"><font size="2" color="3366cc"><b>Primary?</b></font><br>
						<jato:combobox name="cbUWPrimaryProperty" /></td>
						<td valign="top" colspan="4"><font size="2" color="3366cc"><b>Property
						Address:</b></font><br>
						<font size="2"><jato:text name="stPropertyAddressStreetNumber"
							escape="true" /> <jato:text name="stPropertyAddressStreetName"
							escape="true" /> <jato:text name="stPropertyAddressStreetType"
							escape="true" /> <jato:text
							name="stPropertyAddressStreetDirection" escape="true" /> <jato:text
							name="stPropertyAddressUnitNumber" escape="true" /><br>
						<jato:text name="stPropertyAddressCity" escape="true" /> <jato:text
							name="stPropertyAddressProvince" escape="true" /> <jato:text
							name="stPropertyAddressPostalFSA" escape="true" /> <jato:text
							name="stPropertyAddressPostalLDU" escape="true" /></font></td>
						<td><jato:button name="btDeleteProperty"
							extraHtml="onClick = 'setSubmitFlag(true);'"
							fireDisplayEvents="true" src="../images/delete_property.gif" /></td>
						<td><jato:button name="btPropertyDetails"
							extraHtml="onClick = 'setSubmitFlag(true);'"
							src="../images/details.gif" /></td>
					</tr>
					<tr>
						<td colspan="8"><img src="../images/light_blue.gif" width="1"
							height="2" alt="" border="0"></td>
					</tr>
					<tr>
						<td valign="top">&nbsp;</td>
						<td valign="top"><font size="2" color="3366cc"><b>Purchase Price:</b></font><br>
						<font size="2"><jato:text name="stPropertyPurchasePrice"
							escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
						<td valign="top"><font size="2" color="3366cc"><b>Total Estimated
						Value:</b></font><br>
						<font size="2"><jato:text name="stPropertyTotalEstimatedValue"
							escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
						<td valign="top"><font size="2" color="3366cc"><b>Actual Appraised
						Value:</b></font><br>
						<font size="2"><jato:text name="stPropertyActualAppraisedValue"
							escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
						<td valign="top"><font size="2" color="3366cc"><b>Property Type:</b></font><br>
						<font size="2"><jato:text name="stPropertyType" escape="true" /></font></td>
						<td valign="top"><font size="2" color="3366cc"><b>Occupancy Type:</b></font><br>
						<font size="2"><jato:text name="stPropertyOccupancyType"
							escape="true" /></font></td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="8"><img src="../images/light_blue.gif" width="1"
							height="2" alt="" border="0"></td>
					</tr>
					<tr>
						<!--END REPEATING ROW//-->

					</tr>
				</jato:tiledView>

				<td colspan="8"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
			<tr>

				<td colspan="8">
				<table border="0" cellspacing="3" width="600">
					<td valign="top"><br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button
						name="btAddProperty" extraHtml="onClick = 'setSubmitFlag(true);'"
						fireDisplayEvents="true" src="../images/add_property.gif" /></td>
					<td valign="top"><br>
					<jato:button name="btAppraisalReview"
						extraHtml="onClick = 'setSubmitFlag(true);'"
						src="../images/appraisal_review.gif" /></td>
					<td valign="top"><br>
					<font size="2" color="3366cc"><b>Total Property Expenses:</b></font>&nbsp;&nbsp;<font
						size="2"><jato:text name="stTotalPropertyExpenses" escape="true"
						formatType="currency" formatMask="#,##0.00; (-#)" /></font></td>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="8"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
		</tbody>
	</table>
	<!--END PROPERTY INFORMATION//-->

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<td align="right"><a onclick="return IsSubmited();" href="#top"><img
			src="../images/return_top_sm.gif" width="83" height="15" alt=""
			border="0"></a></td>
		<tbody>
			<tr>
			</tr>
		</tbody>
	</table>

	<!--START UW DOWN PAYMENT INFO//-->
	<jato:text name="stUWTargetDownPayment" fireDisplayEvents="true"
		escape="false" />

	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">
		<tbody>
			<tr>
				<td colspan="8"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="8"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td colspan="8" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;
				Down Payment</b></font></td>
			</tr>

			<tr>
				<td colspan="4" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
			</tr>

			<tr>
				<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Down Payment
				Source</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Down Payment
				Description</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Down Payment</b></font></td>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td colspan="8"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="8"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>

			<!--START REPEATING ROW//-->
			<jato:tiledView name="RepeatedUWDownPayment"
				type="mosApp.MosSystem.pgUWorksheetRepeatedUWDownPaymentTiledView">
				<tr>
					<td colspan="8"><img src="../images/light_blue.gif" width="1"
						height="2" alt="" border="0"> <jato:hidden
						name="hdUWDownPaymentSourceId" /> <jato:hidden
						name="hdUWDownPaymentCopyId" /></td>
				</tr>

				<tr>
					<td valign="top">&nbsp;</td>
					<td valign="top"><jato:combobox name="cbUWDownPaymentSource" /></td>
					<td valign="top"><jato:textField name="txUWDPSDescription"
						size="35" maxLength="35" /></td>
					<td valign="top">$ <jato:textField name="txUWDownPayment"
						formatType="currency" formatMask="###0.00; (-#)" size="14"
						maxLength="14" /></td>
					<td valign="top"><jato:button name="btDeleteDownPayment"
						extraHtml="onClick = 'setSubmitFlag(true);'"
						fireDisplayEvents="true" src="../images/delete_downpay.gif" /></td>
				</tr>

				<tr>
					<td colspan="8"><img src="../images/light_blue.gif" width="1"
						height="2" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="8"><img src="../images/blue_line.gif" width="100%"
						height="1" alt="" border="0"></td>
				</tr>

				<!--END REPEATING ROW//-->
			</jato:tiledView>

			<tr>
				<td colspan="8"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td colspan="4">
				<table border="0" cellspacing="3" width="100%">
					<tbody>
						<tr>
							<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button
								name="btAddDownPayment"
								extraHtml="onClick = 'setSubmitFlag(true);'"
								fireDisplayEvents="true" src="../images/add_downpay.gif" /></td>
							<td valign="top"><font size="2" color="3366cc"><b>Total Down
							Payment:</b></font>&nbsp; <jato:textField name="txUWTotalDown"
								formatType="decimal" formatMask="###0.00; (-#)" size="20"
								maxLength="20" /></td>
							<td valign="top"><font size="2" color="3366cc"><b>Down Payment
							Required:</b></font>&nbsp; <jato:text
								name="stUWDownPaymentRequired" escape="true"
								formatType="decimal" formatMask="#,##0.00; -#" /></td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>

			<tr>
				<td colspan="8"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="8"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>

		</tbody>
	</table>

	<!--END DOWN PAYMENT INFO//-->

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td align="right"><a onclick="return IsSubmited();" href="#top"><img
					src="../images/return_top_sm.gif" width="83" height="15" alt=""
					border="0"></a></td>
			</tr>
		</tbody>
	</table>

	<!--START ESCROW DETAILS//-->
	<jato:text name="stTargetEscrow" fireDisplayEvents="true"
		escape="false" />

	<table border="0" width="100%" cellpadding="1" cellspacing="0"
		bgcolor="d1ebff">
		<tbody>
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td colspan="5" valign="top"><font size="3" color="3366cc"><b>&nbsp;&nbsp;&nbsp;
				Escrow Details</b></font></td>
			</tr>
			<tr>
				<td colspan="5" bgcolor="d1ebff"><font size="1">&nbsp;</font></td>
			</tr>

			<tr>
				<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td valign="top"><font size="2" color="3366cc"><b>Escrow Type</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Escrow Payment
				Description</b></font></td>
				<td valign="top"><font size="2" color="3366cc"><b>Escrow Payment</b></font></td>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>
			<tr>
				<td colspan="5"><img src="../images/blue_line.gif" width="100%"
					height="1" alt="" border="0"></td>
			</tr>

			<jato:tiledView name="RepeatedEscrowDetails"
				type="mosApp.MosSystem.pgUWorksheetRepeatedEscrowDetailsTiledView">

				<tr>
					<td colspan="5"><img src="../images/light_blue.gif" width="1"
						height="2" alt="" border="0"> <jato:hidden
						name="hdEscrowPaymentId" /> <jato:hidden
						name="hdEscrowPaymentCopyId" /></td>
				</tr>

				<tr>
					<td valign="top">&nbsp;</td>
					<td valign="top"><jato:combobox name="cbUWEscrowType" /></td>
					<td valign="top"><jato:textField name="txUWEscrowPaymentDesc"
						size="35" maxLength="35" /></td>
					<td valign="top">$ <jato:textField name="txUWEscrowPayment"
						formatType="currency" formatMask="###0.00; -#" size="14"
						maxLength="14" /></td>
					<td valign="top"><jato:button name="btDeleteEscrow"
						extraHtml="onClick = 'setSubmitFlag(true);'"
						fireDisplayEvents="true" src="../images/delete_escrow.gif" /></td>
				</tr>

				<tr>
					<td colspan="5"><img src="../images/light_blue.gif" width="1"
						height="2" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="../images/blue_line.gif" width="100%"
						height="1" alt="" border="0"></td>
				</tr>

			</jato:tiledView>

			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td colspan="5">
				<table border="0" cellspacing="3" width="100%">
					<tbody>
						<tr>
							<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button
								name="btAddEscrow" extraHtml="onClick = 'setSubmitFlag(true);'"
								fireDisplayEvents="true" src="../images/add_escrow.gif" /></td>
							<td valign="top" colspan="2"><font size="2" color="3366cc"><b>Total
							Escrow Payment:</b></font>&nbsp;$ <jato:textField
								name="tbUWEscrowPaymentTotal" formatType="decimal"
								formatMask="###0.00; -#" size="20" maxLength="20" /></td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>

	<!--END ESCROW DETAILS//-->

	<table border="0" width="100%" cellpadding="0" cellspacing="0"
		bgcolor="ffffff">
		<td colspan="8"><img src="../images/blue_line.gif" width="100%"
			height="1" alt="" border="0"></td>
		<tbody>
			<tr>

				<td colspan="8" align="left">&nbsp;&nbsp;</td>
			</tr>
			<tr>

				<td>&nbsp;&nbsp;<jato:text name="stNewScenario2"
					fireDisplayEvents="true" escape="false" /></td>
				<td>&nbsp;&nbsp;<jato:button name="btSaveScenario"
					extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/save_scenario_lrg.gif" /></td>
				<td>&nbsp;&nbsp;<jato:button name="btVerifyScenario"
					extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/save_and_check_rules.gif" /></td>
				<td>&nbsp;&nbsp;<jato:button name="btConditions"
					extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'"
					src="../images/conditions_lrg.gif" /></td>
				<td>&nbsp;&nbsp;<jato:button name="btResolveDeal"
					extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/resolve_deal_lrg.gif" /></td>
				<td>&nbsp;&nbsp;<jato:button name="btCollapseDenyDeal"
					extraHtml="onClick = 'setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/CollapseDeny.gif" /></td>
				<td>&nbsp;&nbsp;<jato:button name="btUWCancel"
					extraHtml="onClick = 'setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/return.gif" /> <jato:button 
					name="btOK" extraHtml="onClick = 'setSubmitFlag(true);'"
					fireDisplayEvents="true" src="../images/return.gif" /></td>
				<td>&nbsp;&nbsp;<a onclick="return IsSubmited();" href="#top"><img
					src="../images/return_totop.gif" width="122" height="25" alt=""
					border="0"></a></td>
			</tr>
			<tr>

				<td colspan="8" align="left">
				<div id="createnewscen2" class="createnewscen2"
					name="createnewscen2">
				<table border="0" cellpadding="0" cellspacing="0" border="0"
					bgcolor="99ccff">
					<td colspan="4"><img src="../images/blue_line.gif" width="100%"
						height="2"></td>
					<tbody>
						<tr>
							<td><font size="2">&nbsp;&nbsp;</font></td>
							<td valign="middle"><font size="2">&nbsp;Based On:</font><br>
							<jato:combobox name="cbScenarioPickListBottom" /></td>
							<td valign="bottom">&nbsp;&nbsp;<jato:button 
								name="btProceedToCreateScenarioBottom"
								extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'"
								src="../images/proceed.gif" /></td>
							<td align="right" rowspan="2"><a onclick="return IsSubmited();"
								href="javascript:newScen(2);"><img src="../images/go_close.gif"
								width="16" height="42" border="0"></a></td>
						</tr>
						<tr>
							<td colspan="4"><img src="../images/blue_line.gif" width="100%"
								height="2"></td>
						</tr>
					</tbody>
				</table>
				</div>
				</td>

			</tr>
		</tbody>
	</table>

	<!-- END BODY OF PAGE -- Part 2// -->
