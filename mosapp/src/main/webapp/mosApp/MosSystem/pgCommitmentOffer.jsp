
<HTML>
<%@page info="pgCommitmentOffer" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgCommitmentOfferViewBean">

<HEAD>
<TITLE>Condition Review</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
.addConditionDialogStd{visibility:hidden; width:400;border-width:1pt;border-style: outset;border-color:"red"}
.addConditionDialogCust{visibility:hidden; width:400;border-width:1pt;border-style: outset;border-color:"red"}

-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<!--
//--Release2.1--_DTS_CR--start.
//// This js set also provides the filtering of special characters for free form
//// condition content.
-->

<!--
//--Release2.1--_DTS_CR--end.
-->


<SCRIPT LANGUAGE="JavaScript">
<!--
//==== Local JavaScript functions ================
function openAddConditionDialog(which)
{
    if (document.all)
    {
        if(which == 1)
        {
         document.all.addConditionDialogStd.style.visibility='visible';
         document.all.pgCommitmentOffer_lbStdCondition.size=10;

        }
        else
        {
         document.all.addConditionDialogCust.style.visibility='visible';
         document.all.pgCommitmentOffer_lbCustCondition.size=10;
         self.scroll(0,10000);
        }
         
    }
    if (document.layers)
    {
        if(which == 1)
        {
         document.addConditionDialogStd.visibility='show';
         document.pgCommitmentOffer_lbStdCondition.size=10;
        }
        else
        {
         document.addConditionDialogCust.visibility='show';
         document.pgCommitmentOffer_lbCustCondition.size=10;
         self.scroll(0,10000);
        }
    }

}

function closeAddConditionDialog(which)
{
    if (document.all)
    {
        if(which == 1)
        {
         document.all.addConditionDialogStd.style.visibility='hidden';
         document.all.pgCommitmentOffer_lbStdCondition.size=1;
        }
        else
        {
         document.all.addConditionDialogCust.style.visibility='hidden';
         document.all.pgCommitmentOffer_lbCustCondition.size=1;
        }
    }
    if (document.layers)
    {
        if(which == 1)
        {
         document.addConditionDialogStd.style.visibility='hide';
         document.pgCommitmentOffer_lbStdCondition.size=1;
        }
        else
        {
         document.addConditionDialogCust.style.visibility='hide';
         document.pgCommitmentOffer_lbCustCondition.size=1;
        }

    }
}

function moreConditionDialog(which)
{
    if (document.all)
    {
        if(which == 1)
        {
            document.all.pgCommitmentOffer_lbStdCondition.size *=2;
            if(document.all.pgCommitmentOffer_lbStdCondition.size > document.all.pgCommitmentOffer_lbStdCondition.options.length)
                document.all.pgCommitmentOffer_lbStdCondition.size = document.all.pgCommitmentOffer_lbStdCondition.options.length;
        }
        else
        {
            document.all.pgCommitmentOffer_lbCustCondition.size *=2;
            if(document.all.pgCommitmentOffer_lbCustCondition.size > document.all.pgCommitmentOffer_lbCustCondition.options.length)
                document.all.pgCommitmentOffer_lbCustCondition.size = document.all.pgCommitmentOffer_lbCustCondition.options.length;
            self.scroll(0,10000);
        }
        
    }
    if (document.layers)
    {
        if(which == 1)
        {
         document.pgCommitmentOffer_lbStdCondition.size *=2;
         if(document.pgCommitmentOffer_lbStdCondition.size > document.pgCommitmentOffer_lbStdCondition.options.length)
                document.pgCommitmentOffer_lbStdCondition.size = document.pgCommitmentOffer_lbStdCondition.options.length;
        }
        else
        {
         document.pgCommitmentOffer_lbCustCondition.size *=2;
         if(document.pgCommitmentOffer_lbCustCondition.size > document.pgCommitmentOffer_lbCustCondition.options.length)
                document.pgCommitmentOffer_lbCustCondition.size = document.pgCommitmentOffer_lbCustCondition.options.length;
         self.scroll(0,10000);
        }
        
    }
}

//--> This function was modified for JATO field format -- By BILLY 25July2002
function moreCondition(isMore, conditionType)
{
    var kickedCtl =(NTCP) ? event.target : event.srcElement;

    // determine row (if repeated)
    var rowNdx = getRowNdx(kickedCtl);

    var inputRowNdx = "";
    if (rowNdx != -1)
        inputRowNdx = "[" + rowNdx + "]_";

    var theTextAreaCtl = eval("document.forms[0].elements['pgCommitmentOffer_Repeated3" + inputRowNdx + conditionType + "']");

    if(isMore == true)
    {
        theTextAreaCtl.rows += 3;
    }
    else
    {
        if(theTextAreaCtl.rows > 3)
            theTextAreaCtl.rows -= 3;
    }
}

function getRowNdx(changedSelect)
{
    //Test if the name is already jato-generated pattern thus already contains row index.
    var pattern = /pgCommitmentOffer_Repeated3\[(\d+)\]/;
    if(changedSelect.name.match(pattern)) {
        return RegExp.$1;
    }

    //Otherwise, find out the row number by counting.
    var numElements = eval("document.forms[0]." + changedSelect.name + ".length");
    
    //Note : "undefined" not support fro JavaScript 1.2 or later versions 
    // if (numElements === undefined) 
    if (numElements == null)
        return 0;

    if (numElements == 1)
        return 0;

    for (i = 0; i < numElements; ++i)
    {
        var rowSelectInput = eval("document.forms[0]." + changedSelect.name + "[" + i + "]");

        if (rowSelectInput == changedSelect)
            return i;

    }
    
    return -1;
}

//Function that displays the defualt condition after user clicks on 'View Defualt' button.
function displayDefCond() {

    var srcElement =(NTCP) ? event.target : event.srcElement;

    // determine row (if repeated)
    var rowNdx = getRowNdx(srcElement);
    displayDefCond1(rowNdx);
}

function displayDefCond1(rowNdx) {

    var defTextArea = eval("document.forms[0].elements['pgCommitmentOffer_Repeated3[" + rowNdx + "]_tbDefCondition" +"']");
    defTextArea.style.display = "inline";
    
    var lessBtn = eval("document.forms[0].elements['pgCommitmentOffer_Repeated3[" + rowNdx + "]_imDefCondBtnLess" +"']");
    lessBtn.style.display = "inline";

    var moreBtn = eval("document.forms[0].elements['pgCommitmentOffer_Repeated3[" + rowNdx + "]_imDefCondBtnMore" +"']");
    moreBtn.style.display = "inline";

    var useDefCond = eval("document.forms[0].elements['pgCommitmentOffer_Repeated3[" + rowNdx + "]_chUseDefaultCondition" +"']");
    useDefCond.parentNode.parentNode.parentNode.vAlign = "top";
    useDefCond.parentNode.parentNode.parentNode.rowSpan = "1";

    var viewDefBtn = eval("document.forms[0].elements['pgCommitmentOffer_Repeated3[" + rowNdx + "]_imViewDefCondBtn" + "']");
    viewDefBtn.style.display = "none";

    var hiddenFlag = eval("document.forms[0].elements['pgCommitmentOffer_Repeated3[" + rowNdx + "]_hdDisplayDefaultCondition" + "']");
    hiddenFlag.value = "Y";
}


function dftCondExpandOnload() {

     for (index=0; index>=0; index++) {
          var cus =eval( "document.forms[0].elements['pgCommitmentOffer_Repeated3[" + index + "]_tbCusCondition" + "']");
          if ( cus == null ) 
              return;

          var hiddenFlag = eval("document.forms[0].elements['pgCommitmentOffer_Repeated3[" + index + "]_hdDisplayDefaultCondition" + "']");
          if (hiddenFlag.value=='Y') {
              displayDefCond1(index);
          }
     }
}



//-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff onload="checkMIResponseLoop();dftCondExpandOnload();" >
<jato:form name="pgCommitmentOffer" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="false" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>

<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">
<input type="hidden" name="hdShowOtherDetails" value="">
    <!--HEADER//-->
    <%@include file="/JavaScript/CommonHeader_en.txt" %>
      
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_en.txt" %>
    <!--Quick Link TOOLBAR//-->
    <%@include file="/JavaScript/QuickLinkToolbar.txt" %>
      
    <!--TASK NAVIGATER //-->
    <%@include file="/JavaScript/TaskNavigater.txt" %>
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=B8D0DC>
    <td align=center colspan=8
        ><img src="../images/white.gif" width=10 height=1
        ><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>
    <td rowspan=2>&nbsp;</td>
    <td><font size=1>DEAL #:</font><br><font size=2><b><jato:text name="stDealId" escape="true" /></b></font></td>
    <td><font size=1>BORROWER NAME:</font><br><font size=2><b><jato:text name="stBorrFirstName" escape="true" />
</b></font></td>
    <td><font size=1>DEAL STATUS:</font><br><font size=2><b><jato:text name="stDealStatus" escape="true" /></b></font></td>
    <td><font size=1>DEAL STATUS DATE:</font><br><font size=2><b><jato:text name="stDealStatusDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
    <td><font size=1>SOURCE FIRM:</font><br><font size=2><b><jato:text name="stSourceFirm" escape="true" /></b></font></td>
    <td><font size=1>SOURCE:</font><br><font size=2><b><jato:text name="stSource" escape="true" /></b></font></td>
    <td><font size=1>UNDERWRITER:</font><br><font size=2><b><jato:text name="stUnderwriterLastName" escape="true" />, <jato:text name="stUnderwriterFirstName" escape="true" /> <jato:text name="stUnderwriterMiddleInitial" escape="true" />.</b></font></td>
<tr>
    <td align=center colspan=8>&nbsp;</td>
<tr>
    <td rowspan=2>&nbsp;</td>
    <td><font size=1>DEAL TYPE:</font><br><font size=2><b><jato:text name="stDealType"  escape="true" /></b></font></td>
    <td><font size=1>DEAL PURPOSE:</font><br><font size=2><b><jato:text name="stDealPurpose"  escape="true" /></b></font></td>
    <td><font size=1>TOT LN AMT:</font><br><font size=2><b>$ <jato:text name="stTotalLoanAmount" escape="true" formatType="currency" formatMask="#,##0.00; -#" /></b></font></td>
    <td><font size=1>APPLICATION DATE:</font><br><font size=2><b><jato:text name="stApplicationDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
    <td><font size=1>PAYMENT TERM:</font><br><font size=2><b><jato:text name="stPmtTerm" escape="true" /></b></font></td>
    <td><font size=1>EST CLOSING DATE:</font><br><font size=2><b><jato:text name="stEstClosingDate" escape="true" formatType="date" formatMask="MMM dd yyyy" /></b></font></td>
    <td><font size=1>SPECIAL FEATURE:</font><br><font size=2><b><jato:text name="stSpecialFeature" escape="true" /></b></font></td>
<tr>
    <td align=center colspan=8
        ><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>
<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
    <tr>
        <td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
    </tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=d1ebff>
<td colspan=8><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
</table>
<center>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<!--DEFAULT CONDITIONS//-->

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Default Conditions</b></font></td>
<td><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<tr>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td colspan=1 valign=top><font size=2 color=3366cc><b>Condition</b></font></td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Action</b></font></td>
<tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<jato:tiledView name="Repeated1" type="mosApp.MosSystem.pgCommitmentOfferRepeated1TiledView">
<!--START DEFAULT CONDITION ROW//-->
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdDefDocTrackId" /></td>
<td valign=top><jato:text name="stDefConditionLabel" escape="true" /></td>
<td valign=top><jato:button name="btDefDetail" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details.gif" /></td>
<td colspan=2 valign=top><jato:combobox name="cbDefAction" /></td>

<tr>
<td colspan=7><jato:hidden name="hdDefDocTrackId" /><jato:hidden name="hdDefRoleId" /><jato:hidden name="hdDefRequestDate" /><jato:hidden name="hdDefStatusDate" /><jato:hidden name="hdDefDocStatus" /><jato:hidden name="hdDefDocText" /><jato:hidden name="hdDefCopyId" /><jato:hidden name="hdDefResponsibility" /><jato:hidden name="hdDefDocumentLabel" />
<tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
</jato:tiledView>
<!--END DEFAULT  CONDITION ROW//-->

<!-- Start DocTracking Section -->
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Document Tracking Conditions</b></font></td>
<td><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<!--START SIGNED DOCTRACKING CONDITION ROW//-->
<jato:tiledView name="Repeated5" type="mosApp.MosSystem.pgCommitmentOfferRepeated5TiledView">
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdSignDocDocTrackId" /></td>
<td valign=top><jato:text name="stSignDocConditionLabel" escape="true" /></td>
<td valign=top><jato:button name="btSignDocDetail" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details.gif" /></td>
<td colspan=2 valign=top><jato:combobox name="cbSignDocAction" extraHtml="disabled = true" /></td>

<tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

</jato:tiledView>
<!--END SIGNED DOCTRACKING CONDITION ROW//-->

<!--START DOCTRACKING CONDITION ROW//-->
<jato:tiledView name="Repeated4" type="mosApp.MosSystem.pgCommitmentOfferRepeated4TiledView">
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdDocDocTrackId" /></td>
<td valign=top><jato:text name="stDocConditionLabel" escape="true" /></td>
<td valign=top><jato:button name="btDocDetail" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details.gif" /></td>
<td colspan=2 valign=top><jato:combobox name="cbDocAction" /></td>

<tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<!--END DOCTRACKING CONDITION ROW//-->
</jato:tiledView>

<td colspan=6>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=ffffff>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
</td>
<tr>

<!--STANDARD CONDITIONS//-->

<td colspan=5><jato:text name="stTargetStdCon" fireDisplayEvents="true" escape="false" /><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Standard Conditions</b></font></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>

<tr>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td colspan=1 valign=top><font size=2 color=3366cc><b>Condition</b></font></td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Action</b></font></td>
<tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<jato:tiledView name="Repeated2" type="mosApp.MosSystem.pgCommitmentOfferRepeated2TiledView">
<!--START STANDARD CONDITION ROW//-->
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdStdDocTrackId" /></td>
<td valign=top ><jato:text name="stStdCondLabel" escape="true" /></td>
<td valign=top ><jato:button name="btStdDetail" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/details.gif" /></td>
<td valign=top ><jato:combobox name="cbStdAction" /></td>
<td valign=top align=left>&nbsp;&nbsp;&nbsp;<jato:button name="btStdDeleteCondition" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_condition.gif" /></td>
<tr>

<tr>
  <td colspan=7><jato:hidden name="hdStdDocTrackId" /><jato:hidden name="hdStdRoleId" /><jato:hidden name="hdStdRequestDate" /><jato:hidden name="hdStdStatusDate" /><jato:hidden name="hdStdDocStatus" /><jato:hidden name="hdStdDocText" /><jato:hidden name="hdStdCopyId" /><jato:hidden name="hdStdResponsibility" /><jato:hidden name="hdStdDocumentLabel" />

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<!--END STANDARD CONDITION ROW//-->

</jato:tiledView>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td colspan=1 width="0%">
    <a href="javascript:openAddConditionDialog(1)"><img src="../images/add_stdcondition.gif" ALIGN=TOP width=133 height=15 alt="" border="0"></a>&nbsp;&nbsp;
</td>
<td colspan=3>
    <div id=addConditionDialogStd class="addConditionDialogStd" name=addConditionDialogStd> 
        <table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>
        
            <tr>
                <td align=left rowspan="2" valign="middle" width="0%" nowrap><font size="2" color="#3366CC"><b> Copy from :&nbsp; </b></font></td>
                <td align=left rowspan="2" valign="middle" width="0%"><jato:listbox name="lbStdCondition" size="1" multiple="true" /></td>
                <td align=left valign="top" width="100%"><img src= "../images/more.gif" width=60  height=20 alt="" border="0" 
                    onClick = "moreConditionDialog(1);" >  </td>
            </tr>
            <tr>
                <td align=right valign="bottom" nowrap width="100%" ><jato:button name="btStdAddCondition" extraHtml="width=60  height=20 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" /> <img src= "../images/cancel.gif" width=60  height=20 alt="" border="0" 
                    onClick = "closeAddConditionDialog(1);" >  </td>
            </tr>

        </table>
    </div>
</td>    
<tr>

<td colspan=6>
<table border=0 width=100% cellpadding=0 cellspacing=0  bgcolor=ffffff>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
</td>

</table>

<jato:text name="stTargetCustCon" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<!--CUSTOM CONDITIONS//-->
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Custom Conditions</b></font></td><tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<jato:tiledView name="Repeated3" type="mosApp.MosSystem.pgCommitmentOfferRepeated3TiledView">
<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdCusDocTrackId" /></td>
  <td valign=top colspan=4><font size=2 color=3366cc><b>Condition</b></font></td>
  
<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign=top colspan=3>
  <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#D1EBFF>
  <td width="1%" rowspan="2">
  <jato:textArea name="tbCusCondition" rows="3" cols="100" />  </td>  
  <td width="99%" align="left" valign="top">
  <img name="CustCondBtnLess" src= "../images/arrowy_left.gif" alt="Click to display less lines" border="0" onClick = "moreCondition(false, 'tbCusCondition');" >
  </td><tr>
  <td width="99%" align="left" valign="bottom">
  <img name="CustCondBtnMore" src= "../images/arrowy_right.gif" alt="Click to display more lines" border="0" onClick = "moreCondition(true, 'tbCusCondition');" >
  </td>  
  </table>
  </td>
  <td valign=top align=left colspan=2>&nbsp;&nbsp;&nbsp;<jato:button name="btCusDeleteCondition" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_condition.gif" /><br/><br/>
  &nbsp;&nbsp;&nbsp;<jato:image name="imViewDefCondBtn" alt="Click to View Default Condition" border="0" onClick = "displayDefCond();"/>
  </td>
</tr>

<!-- Corresponding default condition -->
<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign="top" colspan="3">
    <table border="0" width="100%" hight=1 cellpadding="0" cellspacing="0" bgcolor="#D1EBFF">
        <tr>
        <td width="1%" rowspan="2">
            <jato:textArea name="tbDefCondition" rows="3" cols="100" readOnly="true" style="background-color:#d1ebff;display:none;"/>
        </td>
        <td width="99%" align="left" valign="top">
            <jato:image name="imDefCondBtnLess" alt="Click to display less lines" border="0" onClick = "moreCondition(false, 'tbDefCondition');" style = "display:none;" />
        </td>
        </tr>
        <tr>
        <td width="99%" align="left" valign="bottom">
            <jato:image name="imDefCondBtnMore" alt="Click to display more lines" border="0" onClick = "moreCondition(true, 'tbDefCondition');" style = "display:none;" />
        </td>
        </tr>
    </table>
  </td>
  <td valign="bottom" align="left" colspan="1" rowspan="3">&nbsp;&nbsp;&nbsp;<font size="2" color="3366cc"><b><jato:checkbox name="chUseDefaultCondition" label="Use Default Condition"/></b></font></td>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;<jato:hidden name="hdDisplayDefaultCondition" /></td>

</tr>
<!-- End of Corresponding default condition -->

<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Document Label:</b></font></td>
  <td valign=top colspan=1><font size=2 color=3366cc><b>Action:</b></font></td>
  <td valign=top><font size=2 color=3366cc><b>Responsibility:</b></font></td>
  <td valign=top colspan=1>&nbsp;</td>
<tr>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign=top colspan=1>
  <jato:textField name="tbCusDocumentLabel" size="35" maxLength="35" /></td>
  <td valign=top colspan=1><jato:combobox name="cbCusAction" /></td>
  <td valign=top><jato:combobox name="cbCusResponsibility" /></td>
  <td valign=top colspan=1>&nbsp;</td>
<tr>
  <td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
<tr>
  <td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
<tr>
  
<!--END CUSTOM CONDITION ROW//-->

</jato:tiledView>
</table>

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td colspan=1 width="0%">
    <a href="javascript:openAddConditionDialog(2)"><img src="../images/add_custcondition.gif" ALIGN=TOP width=133 height=15 alt="" border="0"></a>&nbsp;&nbsp;
</td>
<td colspan=3>
    <div id=addConditionDialogCust class="addConditionDialogCust" name=addConditionDialogCust> 
        <table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>
            <tr>
                <td align=left nowrap valign="middle" rowspan="3"><font size="2" color="#3366CC"><b> Copy from :</b></font><font size=2 color=3366cc>&nbsp;&nbsp;</font></td>
            
                <td align=left nowrap valign="middle" rowspan="3"><font size=2 ><jato:listbox name="lbCustCondition" size="1" multiple="true" /></font></td>
                <td align=left nowrap valign="top" colspan="2"> <img src= "../images/more.gif" width=60  height=20 alt="" border="0" 
                    onClick = "moreConditionDialog(2);" >  </td>
            </tr>
            <tr>
                <td align=left valign="middle" nowrap><font size=2 color=3366cc>&nbsp;
              </font><font size="2" color="#3366CC"><b>Language : </b></font>  </td>
                <td align=left valign="middle" nowrap><font size=2 ><jato:combobox name="cbLanguagePreference" /></font>  </td>
            </tr>
            <tr>
                <td align=right valign="bottom" nowrap colspan="2"><jato:button name="btCusAddCondition" extraHtml="width=60  height=20 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" /> <img src= "../images/cancel.gif" width=60  height=20 alt="" border="0" 
                    onClick = "closeAddConditionDialog(2);" >  </td> </tr>
        </table>
    </div>
</td>    
</table>

<table border=0 width=100%>
<td align=right><jato:text name="stDisplayDJCheckBoxStart" escape="false" />Print Commitment Documents without conditions <jato:checkbox name="ckbDJProduceCommitment" /><jato:text name="stDisplayDJCheckBoxEnd" escape="false" /><jato:button name="btResolveDeal" extraHtml="alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/resolve_deal_lrg.gif" />&nbsp;&nbsp;<jato:button name="btSubmit" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/sign_cancel.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>
</center>

</center>
</div>

<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

<BR><jato:hidden name="hdBindDummy" />

</jato:form>


<script language="javascript">
<!--
if(NTCP){
    document.dialogbar.left=55;
    document.dialogbar.top=79;
    document.toolpop.left=317;
    document.toolpop.top=79;
    document.pagebody.top=100;
    document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
    tool_click(4)
}
else{
    tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>