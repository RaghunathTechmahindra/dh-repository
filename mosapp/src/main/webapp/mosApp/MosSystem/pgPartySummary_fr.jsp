
<HTML>
<%@page info="pgPartySummary" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgPartySummaryViewBean">

<HEAD>
<TITLE>Sommaire des intervenants</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: expression((QLE)?135:110); left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: expression((QLE)?135:110); width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<% String quickLinkEnabled = (String) viewBean.getDisplayFieldValue("hdQuickLinkEnableDisplay");%>
   QLE = "<%=quickLinkEnabled%>" == "Y";
</SCRIPT>

<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--
//======================= Local JavaScript (begin) ============================
function submitenter(e)
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;

  if (keycode == 13)
     {
     	document.pgPartySummary.action = '../MosSystem/pgPartySummary.btOK_onWebEvent(btOK)';
	  	document.pgPartySummary.submit();
	  }
     return true;
}
//======================= Local JavaScript (end) ============================
//-->
</SCRIPT>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgPartySummary" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<p>
<center>
<!--HEADER//-->
    <%@include file="/JavaScript/CommonHeader_fr.txt" %>  

<%@include file="/JavaScript/CommonToolbar_fr.txt" %>

<!--Quick Link TOOLBAR//-->
 <%@include file="/JavaScript/QuickLinkToolbar.txt" %>  

<%@include file="/JavaScript/TaskNavigater.txt" %>    
<!--END HEADER//-->

<!--DEAL SUMMARY SNAPSHOT//-->
<div id="pagebody" class="pagebody" name="pagebody">
<center>

<jato:text name="stIncludeDSSstart" escape="false" />
<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br> 
<jato:text name="stIncludeDSSend" escape="false" />

<!--END DEAL SUMMARY SNAPSHOT//-->

<!-- START BODY OF PAGE//-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff><tr>
<td colspan=6><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr><tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr><tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Type d'intervenant</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Nom de l'intervenant</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Nom de la compagnie</b></font></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de téléphone bureau</b></font></td>
<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de télécopieur</b></font></td></tr><tr>

<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr><tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<jato:tiledView name="RepeatedResults" type="mosApp.MosSystem.pgPartySummaryRepeatedResultsTiledView"><tr>
<td>&nbsp;<jato:hidden name="hdPartyProfileId" /><jato:hidden name="hdPartyCopyId" /></td>
<td valign=top><font size=2><jato:text name="stPartyType" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stPartyName" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stPartyCompanyName" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stPartyPhoneNumber" escape="true" /></font></td>
<td valign=top><font size=2><jato:text name="stPartyFaxNumber" escape="true" /></font></td></tr>
<tr>
<td align=right valign=center colspan=6><jato:button name="btDelete" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/removefromdeal_fr.gif" />&nbsp;&nbsp;&nbsp;<jato:button name="btDetails" extraHtml="width=47 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/details_fr.gif" />&nbsp;</td></tr>
<tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr><tr>
<td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

</jato:tiledView>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td valign=top colspan=2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAssign" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/assignparty_deal_fr.gif" /></td><tr>
<td align=right  colspan=2><jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" src="../images/ok.gif" /></td>
</table>
</center>
</div>
<!-- END BODY OF PAGE//-->

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
