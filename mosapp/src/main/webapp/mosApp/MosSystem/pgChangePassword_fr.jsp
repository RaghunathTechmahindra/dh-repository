
<HTML>
<%@page info="pgChangePassword" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgChangePasswordViewBean">

<head>
<TITLE>Changement de mot de passe</TITLE>
<style>
<!--
.pagebody {position: absolute; visibility: hidden; width: 100%; align: center;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 10; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>

</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>

<SCRIPT LANGUAGE="JavaScript">
var NTCP = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);
var HIDDEN = (NTCP) ? 'hide' : 'hidden';
var VISIBLE = (NTCP) ? 'show' : 'visible';

function tool_click(n) {

	if(n==1){
		var itemtoshow = (NTCP) ? document.alertbody: document.all.alertbody.style;
		itemtoshow.visibility=VISIBLE;
	}
	if(n==5){
		var itemtohide = (NTCP) ? document.alertbody: document.all.alertbody.style;
		var itemtoshow = (NTCP) ? document.pagebody: document.all.pagebody.style;
		itemtoshow.visibility=VISIBLE;
		if(itemtohide.visibility==VISIBLE){itemtohide.visibility=HIDDEN}
	}
}

function OnTopClick(){
	self.scroll(0,0);
}

var pmWin;
var pmCont = '';

function openPMWin()
{
var numberRows = pmMsgs.length;

pmCont+='<head><title>Messages</title>';

pmCont+='<script language="javascript">function onClickOk(){';
if(pmOnOk==0){pmCont+='self.close();';}
pmCont+='}<\/script></head><body bgcolor=d1ebff>';

if(pmHasTitle=="Y")
{
  pmCont+='<center><font size=3 color=3366cc><b>'+pmTitle+'</b></font></center><p>';
}

if(pmHasInfo=="Y")
{
  pmCont+='<font size=2>'+pmInfoMsg+'</font><p>';
}

if(pmHasTable=="Y")
{
  pmCont+='<center><table border=0 width=100%><tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0">';

  var writeRows='';
  for(z=0;z<=numberRows-1;z++)
  {
	  if(pmMsgTypes[z]==0){var pmMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
	  if(pmMsgTypes[z]==1){var pmMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
	  writeRows += '<tr><td valign=top width=20>'+pmMessIcon+'</td><td valign=top><font size=2>'+pmMsgs[z]+'</td></tr>';
	  writeRows+='<tr><td colspan=2><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }

  pmCont+=writeRows+'</table></center>';
 }


if(pmHasOk=="Y")
{
  pmCont+='<p><table width=100% border=0><td align=center><a href="javascript:onClickOk()" onMouseOver="self.status=\'\';return true;"><img src="../images/close_fr.gif" width=63 height=25 alt="" border="0"></a></td></table>';
}

pmCont+='</body>';

if(typeof(pmWin) == "undefined" || pmWin.closed == true)
{
  pmWin=window.open('/Mos/nocontent.htm','passMessWin'+document.forms[0].sessionUserId.value,
    'resizable,scrollbars,status=yes,titlebar=yes,width=500,height=400,screenY=250,screenX=0');
    
  if(NTCP)
  {}
  else
  {pmWin.moveTo(10,250);}
}

// For IE 5.00 or lower, need to delay a while before writing the message to the screen
//		otherwise, will get an runtime exception - Note by Billy 17May2001
var timer;
timer = setTimeout("openPMWin2()", 500);

}

function openPMWin2()
{
	pmWin.document.open();
	pmWin.document.write(pmCont);
	pmWin.document.close();
	pmWin.focus();
}


function generateAM()
{
var numberRows = amMsgs.length;

var amCont = '';

amCont+='<div id="alertbody" class="alertbody" name="alertbody">';

amCont+='<center>';
amCont+='<table border=0 width=100% bgcolor=d1ebff cellpadding=0 cellspacing=0>';
amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';
amCont+='<tr><td colspan=3><br><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>';

if(amHasTitle=="Y")
{
amCont+='<td align=center colspan=3><font size=3 color=3366cc><b>'+amTitle+'</b></font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasInfo=="Y")
{
amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amInfoMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';
}

if(amHasTable=="Y")
{
	amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';

	var writeRows='';

	for(z=0;z<=numberRows-1;z++)
  {
		if(amMsgTypes[z]==0){var amMessIcon="<img src='../images/critical.gif' width=15 height=15 border=0>";}
		if(amMsgTypes[z]==1){var amMessIcon="<img src='../images/warning.gif' width=15 height=15 border=0>";}
		writeRows += '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top width=20>'+amMessIcon+'</td><td valign=top><font size=2>'+amMsgs[z]+'</td></tr>';
		writeRows+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=1 alt="" border="0"></td></tr>';
  }
  amCont+=writeRows+'<tr><td colspan=3>&nbsp;<br></td></tr>';
}

amCont+='<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
amCont+='<td colspan=2><font size=2>'+amDialogMsg+'</font></td></tr>';
amCont+='<tr><td colspan=3>&nbsp;<br></td></tr>';

amCont+='<tr><td colspan=3><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td></tr>';

amCont+='<td valign=top align=center bgcolor=ffffff colspan=3><br>'+amButtonsHtml+'&nbsp;&nbsp;&nbsp;</td></tr>';

amCont+='</table></center>';

amCont+='</div>';

document.write(amCont);

document.close();
}

// Fack method for consistance
function IsSubmited()
{
  return(true);
}

</SCRIPT>


</head>

<body bgcolor=ffffff onKeyPress="return submitenter(event);">
<jato:form name="pgChangePassword" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

var fieldname = "";

function getfocus()
{
	var theField =(NTCP) ? event.target : event.srcElement;
	fieldname = theField.name;
}



function submitenter(e)
{
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;

	if (keycode == 13 && fieldname == 'btCancel_onWebEvent(btCancel)' )
	{
			////document.pgChangePassword.action = '../mosApp/MosSystem/pgChangePassword.btCancel_onWebEvent(btCancel)';
			document.pgChangePassword.action += "?<%= viewBean.PAGE_NAME%>_btCancel=";						
			document.pgChangePassword.submit();
			return false;
	}

	if(keycode == 13)
	{
		////document.pgChangePassword.action = '../mosApp/MosSystem/pgChangePassword.btOK_onWebEvent(btOK)';
		document.pgChangePassword.action += "?<%= viewBean.PAGE_NAME%>_btOK=";							
		document.pgChangePassword.submit();
		return false;
	}	

	return true;
}





var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
//var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
var pmOnOk = "<jato:text name="stPmOnOk" escape="true" />";
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="false" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();


</SCRIPT>

<jato:hidden name="detectAlertTasks" />

<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

<div id="pagebody" class="pagebody" name="pagebody">
<br><p><br>
<center>


<table border=0 width=533 bgcolor="003366" cellpadding=0 cellspacing=0>
<td>&nbsp;&nbsp;&nbsp;&nbsp;<font size=4 color="ffcc33">Changement de mot de passe</font></td>
<td align=right><img src="../images/SignOnDHExpress.gif" width=236 height=29 alt="" border="0"></td>
<tr><td colspan=2 bgcolor=ffffff><img src="../images/white.gif" width=1 height=1 alt="" border="0"></td>
<tr><td colspan=2><img src="../images/sign_line1.gif" width=533 height=1 alt="" border="0"></td>
</table>

<table border=0 width=533 bgcolor=ffffcc cellpadding=0 cellspacing=0>
<tr><td>&nbsp;&nbsp;</td><td><br><jato:text name="stChangeMessage" escape="true" /></td>
<tr><td><br>&nbsp;&nbsp;</td><td>Veuillez entrer votre mot de passe actuel et un nouveau mot de passe:</td>
<tr><td colspan=2 align=center><br>

<table border=0 width=300>
<td><font size=2><b>MOT DE PASSE ACTUEL:</b></font></td><td><jato:password name="tbCurrent" size="20" maxLength="20" /></td><tr>
<td colspan=2>&nbsp;</td><tr>
<td><font size=2><b>NOUVEAU MOT DE PASSE:</b></font></td><td><jato:password name="tbNew" size="20" maxLength="20" /></td><tr>
<td><font size=2><b>V�RIFIER MOT DE PASSE:</b></font></td><td><jato:password name="tbVerify" size="20" maxLength="20" /></td><tr>
</table></td><tr>
<td colspan=2 align=right><br><jato:button name="btOK" extraHtml=" onFocus='getfocus();'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="width=86 height=25 alt='' border='0' onFocus='getfocus();'" fireDisplayEvents="true" src="../images/sign_cancel2.gif" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><tr>
<td colspan=2><br></td>
</table>

</center>
<script language="javascript">

if(NTCP){
	document.pagebody.top=50;
	document.alertbody.top=50;
}

if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
tool_click(1)
}
else{
tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}


</script>


</div>


</jato:form>


</body>


</jato:useViewBean>
</HTML>