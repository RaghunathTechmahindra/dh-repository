<%--
 /*
  * MCM Impl Team. Change Log
  * XS_2.55 -- July 24, 2008  -- Added Component Information Section
  * XS_2.60 -- July 31 2008  -- fixed to call JS setMIUpfront when MIPayor is changed (existing bug)
  * XS_2.55 Aug 11, 2008 added french version of XS_2.7
  * artf763316 Aug 21, 2008 : replacing XS_2.60.
  * FXP23384 Commitment Expiration Date is displayed twice 
 */
  --%>
<HTML>
<%@page info="pgDealEntry" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealEntryViewBean">

<!--START FINANCIAL DETAILS//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr><td colspan=8 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
D�tails financiers</b></font></td>
</tr><tr><td colspan=8 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>


<tr>
<td valign=top colspan=1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<%-- Release3.1 begins  --%>
<td valign=top colspan=1>
	<font size=2 color=3366cc><b>Type de Produit:</b></font><br>
	<jato:combobox name="cbProductType" />
</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>But de la demande:</b></font><br><jato:combobox name="cbDealPurpose" extraHtml="onChange='DisplayOrHideRefiSection();'" /></td>
<%-- Release3.1 ends--%>
<td valign=top colspan=3><font size=2 color=3366cc><b>Type de demande:</b></font><br><jato:combobox name="cbDealType" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Secteur d'activit�:</b></font><br><jato:combobox name="cbLOB" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Pr�teur:</b></font><br><jato:combobox name="cbLender"  onClick="recordCurrentSelection()" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Produit:</b></font><br><jato:combobox name="cbProduct"  onClick="recordCurrentSelection()" /></td>
<td valign=top colspan=3><font size=2 color=3366cc><b><b>Produit, taux et paiement r�quis:</b></font><br><font size=2><jato:text name="stHomeBASERateProductPmnt" escape="false" /></font></td>

</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Description du terme du paiement:</b></font><br>
<jato:textField name="tbPaymentTermDescription" size="20" maxLength="20" /><jato:hidden name="hdPaymentTermId" /></td>
<td valign=top><font size=2 color=3366cc><b>Code taux</b></font><br>
<jato:textField name="tbRateCode" size="20" maxLength="20" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Taux immobilis�:</b></font><br><jato:combobox name="cbRateLocked" /></td>
<td valign=top><font size="2" color="#3366CC"><b>Rang</b></font><br><jato:combobox name="cbChargeTerm" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Montant du pr�t existant:</b></font><br></td>
<jato:hidden name = "hdChangedEstimatedClosingDate" />
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Taux d'int�r�t affich�:</b></font><br><jato:combobox name="cbPostedInterestRate" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Escompte:</b></font><br>
<jato:textField name="txDiscount" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Prime:</b></font><br>
<jato:textField name="txPremium" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</td>
<td valign=top>
<table><tr>
<td><font size=2 color=3366cc><b>Taux d'int�r�t r�duit:</b></font><br>
<jato:textField name="txBuydownRate" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> % &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td nowrap><font size=2 color=3366cc><b>Taux net</b></font><br><jato:text name="stNetRate" fireDisplayEvents="true" escape="true" formatType="decimal" formatMask="fr|###0.000; (-#)" /> % &nbsp;&nbsp;&nbsp;</td>
</tr></table>

</td>
<td><font size=2 color=3366cc>&nbsp;<b>P�riode de garantie du taux:</b></font><br><jato:textField name="txRateGuaranteePeriod" size="3" maxLength="3"/><font size=2 color=3366cc>&nbsp;<b>Jours</b></font></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Montant du pr�t requis:</b></font><br>$<jato:textField name="txRequestedLoanAmount" formatType="currency" formatMask="###0.00; -#" size="13" maxLength="13" /> </td>
<td valign=top><font size=2 color=3366cc><b>Fr�quence des paiements:</b></font><br><jato:combobox name="cbPayemntFrequencyTerm" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Amortissement:</b></font><br><font size=2>Ans </font>
<jato:textField name="txAmortizationYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Mois </font>
<jato:textField name="txAmortizationMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>Amortissement effectif:</b></font><br><jato:text name="stEffectiveAmortizationYears" escape="true" formatType="decimal" formatMask="###0; (-#)" /> <font size=2>Ans, </font><jato:text name="stEffectiveAmortizationMonths" escape="true" formatType="decimal" formatMask="###0; (-#)" /> <font size=2>Mois</font></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Paiement suppl�mentaire du capital:</b></font><br>$
<jato:textField name="txAdditionalPrincipalPayment" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<!-- PPI -->
<jato:text name="stDealPurposeTypeHiddenStart" escape="false"/>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Valeur des am�liorations:</b></font><br>
    <font size=2> <jato:text name="txRefiImprovementValue"
    escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Achat + am�liorations:</b></font><br>
    <font size=2> <jato:text name="txImprovedValue"
    escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></font></td><tr>	
<jato:text name="stDealPurposeTypeHiddenEnd" escape="false"/>
<!-- PPI -->

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Terme r�el du paiement:  </b></font><br><font size=2>An </font>
<jato:textField name="txPayTermYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /><font size=2>Mois </font>
<jato:textField name="txPayTermMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>Paiement du C&I:</b></font><br><jato:text name="stPIPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Paiement total des fonds plac�s en main tierce:</b></font><br><jato:text name="stTotalEscrowPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Paiement total:</b></font><br><jato:text name="stTotalPayment" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></td>
<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<td valign=top colspan=2><font size=2 color=3366cc><b>Paiement incluant ass. vie / invalidit�:</b></font><br><jato:text name="stPmntInclLifeDis" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></td>
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=5><font size=2 color=3366cc><b>Type de remboursement:</b></font><br><jato:combobox name="cbRepaymentType" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Date pr�vue de cl�ture:</b></font><br><font size=2>Mois </font><jato:combobox name="cbEstClosingDateMonths" extraHtml="onChange='setChangedEstimatedClosingDate()'" /> <font size=2>Jour </font>
<jato:textField name="txEstClosingDateDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2"  extraHtml="onChange='setChangedEstimatedClosingDate()'"/> <font size=2>An </font>
<jato:textField name="txEstClosingDateYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4"  extraHtml="onChange='setChangedEstimatedClosingDate()'"/></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Date du premier paiement:</b></font><br><font size=2>Mois </font><jato:combobox name="cbFirstPayDateMonth" /> <font size=2>Jour </font>
<jato:textField name="txFirstPayDateDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>An </font>
<jato:textField name="txFirstPayDateYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Date d'�ch�ance:</b></font><br><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="fr|MMM  dd  yyyy" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Prix d'achat estimatif pr�autoris�:</b></font><br>$
<jato:textField name="txPreAppEstPurchasePrice" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>2�me hypoth�que existante?</b></font><br><jato:combobox name="cbSecondMortgageExist" /></td>

<td valign=top colspan=4>
<table><tr>
<td>
<font size=2 color=3366cc><b>P�nalit� de remboursement anticip�:</b></font><br><jato:combobox name="cbPrePaymentPenanlty" />
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;<font size=2 color=3366cc><b>Programme de financement:</b></font><br>&nbsp;&nbsp;&nbsp;&nbsp;<jato:combobox name="cbFinancingProgram" /></td>
</tr>
<tr>
<td valign=top colspan=2><font size=2 color=3366cc><b>Option de remboursement anticip�:</b></font><br><jato:combobox name="cbPrivilagePaymentOption" /></td>
<td valign=top colspan=1><font size=2 color=3366cc>
<jato:text name="stIncludeFinancingProgramStart" escape="false" />
<jato:text name="stIncludeFinancingProgramEnd" escape="false" />
</td>
</tr>
</table>
</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Traitement sp�cial</b></font><br><jato:combobox name="cbSpecialFeature" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<jato:text name="stIncludeCommitmentStart" escape="false" />
<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Date de retour pr�vue de l'offre d'engagement:</b></font><br><font size=2>Mois </font><jato:combobox name="cbCommExpectedDateMonth" /> <font size=2>Jour </font>
<jato:textField name="txCommExpectedDateDay" size="2" maxLength="2" /> <font size=2>An </font>
<jato:textField name="txCommExpectedDateYear" size="4" maxLength="4" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>P�riode d'engagement:</b></font><br><jato:text name="stCommitmentPeriod" escape="true" formatType="decimal" formatMask="fr|###0$; (-#)" /></td>
<!-- 
<td valign=top><font size=2 color=3366cc><b>Date d'expiration de l'offre d'engagement:</b></font><br><jato:text name="stCommExpDate" escape="true" formatType="date" formatMask="fr|dd-MMM-yyyy" /></td>
 -->
<td valign=top colspan=2><font size=2 color=3366cc><b>Date d'acceptation de l'offre d'engagement:</b></font><br><jato:text name="stCommAcceptanceDate" escape="true" formatType="date" formatMask="fr|dd-MMM-yyyy" /></td>
</tr>
<jato:text name="stIncludeCommitmentEnd" escape="false" />

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<%--CR03 start --%>
<tr>
    <td valign=top colspan=1>&nbsp;<jato:hidden name="hdCommitmentExpiryDate" />
		<input type="hidden" id="hdCommitmentExpiryDate" value='<jato:text name="hdCommitmentExpiryDate"/>'/>
    	<jato:hidden name="hdMosProperty" />
		<input type="hidden" id="hdMosProperty" value='<jato:text name="hdMosProperty"/>'/>
    </td>
    <td valign="top" colspan=2><font size="2" color="3366cc"><b>Calculer automatiquement la date d'expiration de l'offre:</b>
        </font><br><font size="2"> 
	    <jato:radioButtons name="rbAutoCalCommitDate" layout="horizontal" extraHtml="onClick='changeExpireDate();'" />
		</font>
    </td>
    <td valign=top colspan=2><font size=2 color=3366cc><b>Date d'expiration de l'offre d'engagement:</b></font><br><font size=2>Mois: </font>
    <jato:combobox name="cbCommExpirationDateMonth" extraHtml="id='cbCommExpirationDateMonth'"/> <font size=2>Jour: </font>
    <jato:textField name="txCommExpirationDateDay" size="2" maxLength="2" extraHtml="id='txCommExpirationDateDay'"/> <font size=2>An: </font>
    <jato:textField name="txCommExpirationDateYear" size="4" maxLength="4" extraHtml="id='txCommExpirationDateYear'"/></td>
<td valign=top colspan=1><font size=2 color=3366cc><b><jato:text name="stMarketTypeLabel" escape="false" fireDisplayEvents="true" /></b></font><br>
<font size=2><jato:text name="stMarketType" escape="false" fireDisplayEvents="true" /></font></td>
    <td valign=top colspan=2><font size=2 color=3366cc><b>Date de retrait de la condition de financement:</b></font><br><font size=2>Mois: </font>
    <jato:combobox name="cbFinancingWaiverDateMonth" extraHtml="id='cbFinancingWaiverDateMonth'"/> <font size=2>Jour: </font>
    <jato:textField name="txFinancingWaiverDateDay" size="2" maxLength="2" extraHtml="id='txFinancingWaiverDateDay'"/> <font size=2>An: </font>
    <jato:textField name="txFinancingWaiverDateYear" size="4" maxLength="4" extraHtml="id='txFinancingWaiverDateYear'"/></td>
</tr>
<%--CR03 end --%>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Montant du pr�t-relais:</b></font><br>$ <jato:text name="stBridgeLoanAmount" escape="true" formatType="currency" formatMask="fr|###0.00$; (-#)" /> &nbsp;&nbsp;&nbsp;<jato:button name="btReviewBridge" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/review_bridge.gif" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Montant de la retenue:</b></font><br>$
<jato:textField name="txAdvanceHold" formatType="decimal" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Code de commissionnement:</b></font><br>
<jato:textField name="txCommisionCode" size="35" maxLength="35" /></td>
</tr>


<tr>
<td valign=top colspan=1>&nbsp;</td>

<td valign=top><font size=2 color=3366cc><b>Pourcentage de remise en argent:</b></font><br><jato:textField name="txCashBackInPercentage" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="16" />%</td>

<td valign=top><font size=2 color=3366cc><b>Montant de remise en argent:</b></font><br>$<jato:textField name="txCashBackInDollars" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="16" /></td>

<td valign=top colspan=2><font size=2><br><jato:checkbox name="chCashBackOverrideInDollars" /></font><font size=2 color=3366cc><b>Donner priorit� � la remise en argent</b></font></td>
<%--***** Change by NBC Impl. Team - Version 1.3 - Start*****--%>
<td valign=top align=left colspan=2><font size=2 color=3366cc><b>Programme d'affiliation:</b></font><br><jato:combobox name="cbAffiliationProgram" extraHtml="onChange='DisplayOrHideRefiSection();'" /></td>
<%--***** Change by NBC Impl. Team - Version 1.3 - End*****--%>
</tr>

<tr>

<td>&nbsp;</td>
<td colspan=7><jato:button name="btRecalculate" extraHtml="width=75 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/recalculate_fr.gif" />&nbsp;&nbsp;&nbsp;</td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--END FINANCIAL DETAILS//-->


<!--START Refinance Info.//-->
<div id="refisection" class="refisection" name="refisection">
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;D�tails de l'hypoth�que sur plus value/Transfert/Refinancement</b></font></td><tr>
<td colspan=5>&nbsp;</td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Date d'achat:</b></font></td>
<td valign=top><font size=2>Mois </font><jato:combobox name="cbRefiOrigPurchaseDateMonth" /> <font size=2>Jour </font>
<jato:textField name="txRefiOrigPurchaseDateDay" size="2" maxLength="2" /> <font size=2>An </font>
<jato:textField name="txRefiOrigPurchaseDateYear" size="4" maxLength="4" /></td>
<td valign=top><font size=2 color=3366cc><b>But:</b></font></td>
<td valign=top><jato:textField name="txRefiPurpose" size="35" maxLength="80" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Pr�teur:</b></font></td>
<td valign=top>
<jato:textField name="txRefiMortgageHolder" size="35" maxLength="80" /></td>
<td valign=top><font size=2 color=3366cc><b>Amortissement mixte:</b></font></td>
<td valign=top><font size=2>
<jato:radioButtons name="rbBlendedAmortization" layout="horizontal" /></font></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Prix d'achat original:</b></font></td>
<td valign=top>$ 
<jato:textField name="txRefiOrigPurchasePrice" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Montant original de l'hypoth�que:</b></font></td>
<td valign=top>$ 
<jato:textField name="txRefiOrigMtgAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Valeur des am�liorations:</b></font></td>
<td valign=top>$ 
<jato:textField name="txRefiImprovementValue" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Montant de l'hypoth�que impay�:</b></font></td>
<td valign=top>$ 
<jato:textField name="txRefiOutstandingMtgAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Am�liorations:</b></font></td>
<td valign=top width="480">
	<%-- Release3.1 Apr 26, 2006 ; chenged size --%>
	<%-- Ticket #3463 chenged size--%>
	<jato:textField name="txRefiImprovementsDesc" size="73"  maxLength="80"/>
</td>


<%--
/***************MCM Impl team changes starts - XS_2.55(XS_2.7) *******************/
--%>
<td vAlign=top><font color=#3366cc size=2><b>R�f�rence au compte existant:</b></font></td>
<td vAlign=top>
    <jato:textField name="txRefExistingMTGNumber" extraHtml="onBlur='return isAnySpecialChar(this);'" size="35"  maxLength="35"/>
</td>

<tr>
<td>&nbsp;</td>
<td vAlign=top><font color=#3366cc size=2><b>Type de Produit:</b></font></td>
<td vAlign=top>
    <jato:combobox name="cbRefiProductType" />
</td>

<%-- Release3.1 Apr 26, 2006 begins--%>
<td vAlign=top>&nbsp;</td>
<td vAlign=top>&nbsp;
</td>
<tr>
<td>&nbsp;</td>
<td vAlign=top><font color=#3366cc size=2><b>D�tails suppl�mentaires:</b></font></td>
<td vAlign=top colspan=3>
    <jato:textArea name="txRefiAdditionalInformation" rows="4" cols="100" 
      extraHtml="wrap=soft maxlength=512"/>
</td>
<%--
/***************MCM Impl team changes ends - XS_2.55(XS_2.7) *********************/
--%>


<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td><tr>
</table>
</div>
<!--END Refinance Info.//-->

<!-- MCM Impl team changes starts - XS 2.55 -->
<div id="divComponentInfo">
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<jato:containerView name="componentInfoPagelet" type="mosApp.MosSystem.pgComponentInfoPageletViewBean">
			<jsp:include page="pgComponentInfoPagelet_fr.jsp"/>
		</jato:containerView>
	</tr>
</table>
</div>
<!-- MCM Impl team changes ends - XS 2.55 -->

<!--START PROGRESS ADVANCE//-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Avance progressive</b></font></td><tr>
<td colspan=5>&nbsp;</td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Avance progressive:</b></font></td>
<td valign=top><font size="2">
<jato:radioButtons name="rbProgressAdvance" layout="horizontal" extraHtml="onClick='hideSubProgressAdvance();'" /></font></td><%--Release3.1 extraHtml--%>
<td valign=top><font size=2 color=3366cc><b>Montant de la prochaine avance: </b></font></td>
<td valign=top>$ 
<jato:textField name="txNextAdvanceAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Montant de l'avance � ce jour:</b></font></td>
<td valign=top>$ 
<jato:textField name="txAdvanceToDateAmt" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Num�ro de l'avance:</b></font></td>
<td valign=top>
<jato:textField name="txAdvanceNumber" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td><tr>
</table>
<!--END PROGRESS ADVANCE//-->

<!--START DOWN PAYMENT INFO//-->
<jato:text name="stTargetDownPayment" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=8 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Mise de fonds</b></font></td>
</tr>

<tr><td colspan=8 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Source de la mise de fonds</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Description de la mise de fonds</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Mise de fonds</b></font></td>
<td>&nbsp;</td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedDownPayment" type="mosApp.MosSystem.pgDealEntryRepeatedDownPaymentTiledView">
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
<jato:hidden name="hdDownPaymentSourceId" />
<jato:hidden name="hdDownPaymentCopyId" /></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><jato:combobox name="cbDownPaymentSource" /></td>
<td valign=top>
<jato:textField name="txDPSDescription" size="35" maxLength="35" /></td>
<td valign=top>$ 
<jato:textField name="txDownPayment" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top><jato:button name="btDeleteDownPayment" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_downpay_fr.gif" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=4>

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddDownPayment" extraHtml="width=110  height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_downpay_fr.gif" /></td>
<td valign=top><font size=2 color=3366cc><b>Mise de fonds totale</b></font>&nbsp;$ 
<jato:textField name="txTotalDown" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>Mise de fonds requise</b></font>&nbsp;<jato:text name="stDownPaymentRequired" escape="true" formatType="currency" formatMask="fr|#,##0.00$; (-#)" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<!--END DOWN PAYMENT INFO//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START ESCROW DETAILS//-->
<jato:text name="stTargetEscrow" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Fonds plac�s en main tierce</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Type de fonds</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Description des fonds</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Paiement des fonds</b></font></td>
<td>&nbsp;</td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedEscrowDetails" type="mosApp.MosSystem.pgDealEntryRepeatedEscrowDetailsTiledView">

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
<jato:hidden name="hdEscrowPaymentId" />
<jato:hidden name="hdEscrowCopyId" /></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><jato:combobox name="cbEscrowType" /></td>
<td valign=top>
<jato:textField name="stEscrowPaymentDesc" size="20" maxLength="35" /></td>
<td valign=top>$ 
<jato:textField name="stEscrowPayment" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top><jato:button name="btDeleteEscrow" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_escrow_fr.gif" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5>

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddEscrow" extraHtml="width=71  height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_escrow_fr.gif" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Total des fonds</b></font>&nbsp;$ 
<jato:textField name="txTotalEscrow" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END ESCROW DETAILS//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START NON-FINANCIAL DETAILS//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Information non financi�re</b></font></td>
</tr>
<tr><td colspan=6 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Succursale</b></font><br><jato:combobox name="cbBranch" fireDisplayEvents="true" /></td>
<%-- LEN497489 --%>
<td valign=top><font size=2 color=3366cc><b>Autres produits</b></font><br><jato:combobox name="cbCrossSell" /></td>
<td valign=top><font size=2 color=3366cc><b>Payeur de taxe</b></font><br><jato:combobox name="cbTaxPayor" elementId="cbTaxPayor" onChange="flipChIncludeTaxPayments(this)"/></td>
<td valign=top><br><jato:button name="btPartySummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/party_summary_fr.gif" /></td>
</tr>                                                 


<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b><jato:text name="stBranchTransitLabel" escape="false" fireDisplayEvents="true" /></b></font><br>
<td valign=top><font size=2><jato:text name="stBranchTransitNumber" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stPartyName" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stAddressLine1" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stCity" escape="false" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Type de r�f�rence</b></font><br>
<jato:text name="stReferenceType" escape="true" /></td>
<td valign=top><font size=2 color=3366cc><b>No de r�f�rence de la demande</b></font><br>
<jato:text name="stReferenceDealNo" escape="true" /></td>
</tr>

<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
</table>

<!--END NON-FINANCIAL DETAILS//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START MORTGAGE INSURANCE//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Assurance pr�t-hypoth�caire</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Indicateur de l'assurance</b></font><br><jato:combobox name="cbMIIndicator" /></td>
<td valign=top><font size=2 color=3366cc><b>Statut de l'assurance</b></font><br><jato:text name="stMIStatus" escape="true" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>No de police de l'assurance existant</b></font><br>
<jato:textField name="txMIExistingPolicy" size="35" maxLength="35" /></td>
</tr>

<tr><td colspan=2><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Type d'assureur</b></font><br>
<jato:combobox name="cbMIInsurer" elementId="cbMIInsurer" extraHtml="onChange='SetMIPolicyNoDisplay(); hideRequestStandardService();'" /></td><%--Release3.1 extraHtml--%>
<td valign=top><font size=2 color=3366cc><b>Type d'assurance</b></font><br>
<jato:combobox name="cbMIType" elementId="cbMIType" extraHtml="onChange='hideRequestStandardService();'" /></td><%--Release3.1 extraHtml--%>
<td valign=top><font size=2 color=3366cc><b>Payeur de l'assurance</b></font><br>
<%--MCM Team added extraHtml --%>
<jato:combobox name="cbMIPayor"  onChange="selectMIUpfront()" elementId="cbMIPayor"/></td>
<td valign=top><font size=2 color=3366cc><b>No du certificat de pr�qualification</b></font><br><font size="2"><jato:text name="stMIPreQCertNum" escape="true" /></font></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Prime d'assurance</b></font><br>$ 
<jato:textField name="txMIPremium" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top><font size=2 color=3366cc><b>No de certificat</b></font><br><font size="2">
<jato:textField name="txMICertificate" size="25" maxLength="50" extraHtml="disabled" /></font></td>
<td valign=top><font size=2 color=3366cc><b>Frais pr�lev�s � l'acquisition d'assurance</b></font><br><font size=2>
<jato:radioButtons name="rbMIUpfront" layout="horizontal" styleClass="rbMIUpfront" onClick="flipChIncludeMIPremiums(this)"/></font></td>
<td valign=top><font size=2 color=3366cc><b>Intervention du souscripteur hypoth�caire requise</b></font><br><font size=2>
<jato:radioButtons name="rbMIRUIntervention" layout="horizontal" /></font></td>
</tr>
			<!-- SEAN DJ SPEC-Progress Advance Type July 26, 2005:  -->
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
			  <td valign="top">&nbsp;</td>
				<td valign="top">
<div id="divProgressAdvanceType" name="divProgressAdvanceType">
					<font size=2 color=3366cc><b>Type d'avance �chelonn�e:</b></font><br>
					<jato:combobox name="cbProgressAdvanceType"/>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
				</td>
      <!-- SEAN DJ SPEC-PAT END -->

<%-- Release3.1 Apr 18, 2006 begins --%>
				<TD vAlign=top>
<div id="divProgressAdvanceInspectionBy" name="divProgressAdvanceInspectionBy">			
					<FONT color=#3366cc size=2><B>Inspection de l'avance de progress par:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbProgressAdvanceInspectionBy" layout="horizontal" />
					</FONT>
</div>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Hypotheques REER autog�r�:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbSelfDirectedRRSP" layout="horizontal" />
					</FONT>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>L'identificateur poursuivant de produit pour SCHL:</B></FONT><BR>
					<jato:textField name="txCMHCProductTrackerIdentifier" size="5" maxLength="5" />
					
					
				</TD>
			</TR>
			<TR>
				<td valign="top">&nbsp;</td>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Remboursement MCVD:</B></FONT><BR>
					<jato:combobox name="cbLOCRepaymentTypeId" />
				</TD>
				<TD vAlign=top>
<div id="divReqStdSvc" name="divReqStdSvc">
					<FONT color=#3366cc size=2><B>Demande pour sevice de base:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbRequestStandardService" layout="horizontal" />
					</FONT>
</div>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Amortissement MCVD:</B></FONT><BR>
					<FONT size=2>
						 <jato:text name="stLOCAmortizationMonths" fireDisplayEvents="true" escape="true" /> 
					</FONT>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Date d'�ch�ance pour un pr�t non Amorti MCVD:</B></FONT><BR>
					<FONT size=2>
						<jato:text name="stLOCInterestOnlyMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" />
					</FONT> 
				</TD>
			</tr>				
<%-- Release3.1 Apr 18, 2006 ends --%>


<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<!--END MORTGAGE INSURANCE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm_fr.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START MORTGAGE OPTIONS//-->
<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Options pr�t hypoth�caire</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;Multi-projets:</b></font>
<font size=2><jato:radioButtons name="rbMultiProject" layout="horizontal" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Protection propri�taire:</b></font>
<font size=2><jato:radioButtons name="rbProprietairePlus" layout="horizontal" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Ligne de cr�dit:</b></font>
<jato:textField name="txProprietairePlusLOC" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<p>
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
<!--END MORTGAGE OPTIONS//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><jato:button name="btDealSubmit" extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btDealCancel" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td></tr>
</table>



</center>
</div>

<div id="validlayer" class="validlayer" name="validlayer">
<center>
<table border=0 width=650 cellpadding=4>
	<td align=center><br><font size=4 face="arial, helvetica" color=ffcc00>

		Required Data Missing

	</font></td>
<tr>
	<td>
	<center>
	<a onclick="return IsSubmited();" href="validShow()"><img src="../images/ok_yel_fr.gif" width=86 height=25 alt="" border="0"></a>
	</center></td>
	
</table>
</center>
</div>

<div id="softvalidlayer" class="softvalidlayer" name="softvalidlayer">
<center>
<table border=0 width=650 cellpadding=4>
	<td align=center><br><font size=4 face="arial, helvetica" color=ffcc00>

		Informational Message: Data Missing

		</font></td>
<tr>
	<td>
	<center>
 	<a onclick="return IsSubmited();" href="validShow()"><img src="../images/ok_yel_fr.gif" width=86 height=25 alt="" border="0"></a>
	</center></td>
</table>
</center>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopWithNoteSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:useViewBean>
</HTML>
