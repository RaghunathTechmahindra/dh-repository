<%--
 /*
  * MCM Impl Team. Change Log
  * XS_2.7 -- 06/09/2008  -- Added Additional Details and Existing Account Reference fields in Refinance section.
  * XS_2.60 -- July 31 2008  -- fixed to call JS setMIUpfront when MIPayor is changed (existing bug)
  * artf763316 Aug 21, 2008 : replacing XS_2.60.
  * artf765246 Aug 23,2008 :Added new special character validation.
  * FXP23384 Commitment Expiration Date is displayed twice 
 */
  --%>
<HTML>
<%@page info="pgDealEntry" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgDealEntryViewBean">

<p>
<!--START FINANCIAL DETAILS//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr><td colspan=8 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Financial Details</b></font></td>
</tr><tr><td colspan=8 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>


<tr>
<td valign=top colspan=1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<%-- Release3.1 begins  --%>
<td valign=top colspan=1>
	<font size=2 color=3366cc><b>Product Type:</b></font><br>
	<jato:combobox name="cbProductType" />
</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Deal Purpose:</b></font><br><jato:combobox name="cbDealPurpose" extraHtml="onChange='DisplayOrHideRefiSection();'" /></td>
<%-- Release3.1 ends--%>
<td valign=top colspan=3><font size=2 color=3366cc><b>Deal Type:</b></font><br><jato:combobox name="cbDealType" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>LOB:</b></font><br><jato:combobox name="cbLOB" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Lender:</b></font><br><jato:combobox name="cbLender" onClick="recordCurrentSelection()"/></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Product:</b></font><br><jato:combobox name="cbProduct" onClick="recordCurrentSelection()"/></td>
<td valign=top colspan=3><font size=2 color=3366cc><b><b>Requested Product, Rate and Payment:</b></font><br><font size=2><jato:text name="stHomeBASERateProductPmnt" escape="false" /></font></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Payment Term Description:</b></font><br>
<jato:textField name="tbPaymentTermDescription" size="20" maxLength="20" /><jato:hidden name="hdPaymentTermId" /></td>
<td valign=top><font size=2 color=3366cc><b>Rate Code:</b></font><br>
<jato:textField name="tbRateCode" size="20" maxLength="20" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Rate Locked In:</b></font><br><jato:combobox name="cbRateLocked" /></td>
<td valign=top><font size=2 color=3366cc><b>Charge:</b></font><br><jato:combobox name="cbChargeTerm" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Existing Loan Amount:</b></font><br></td>
<jato:hidden name = "hdChangedEstimatedClosingDate" />
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Posted Interest Rate:</b></font><br><jato:combobox name="cbPostedInterestRate" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Discount:</b></font><br>
<jato:textField name="txDiscount" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Premium:</b></font><br>
<jato:textField name="txPremium" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</td>
<td valign=top><font size=2 color=3366cc><b>Buydown Rate:</b></font><br>
<jato:textField name="txBuydownRate" formatType="decimal" formatMask="###0.000; (-#)" size="6" maxLength="6" /> %</td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Net Rate:</b></font><br><jato:text name="stNetRate" fireDisplayEvents="true" escape="true" formatType="decimal" formatMask="###0.000; (-#)" /> %</td>
<td valign=top><font size=2 color=3366cc>&nbsp;<b>Rate Guarantee Period:</b></font><br><jato:textField name="txRateGuaranteePeriod" size="3" maxLength="3"/>
<font size=2 color=3366cc>&nbsp;<b>Days</b></font>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Req. Loan Amount:</b></font><br>$<jato:textField name="txRequestedLoanAmount" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /> </td>
<td valign=top><font size=2 color=3366cc><b>Payment Frequency:</b></font><br><jato:combobox name="cbPayemntFrequencyTerm" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Amortization:</b></font><br><font size=2>Yrs: </font>
<jato:textField name="txAmortizationYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Mths: </font>
<jato:textField name="txAmortizationMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>Effective Amortization:</b></font><br><jato:text name="stEffectiveAmortizationYears" escape="true" formatType="decimal" formatMask="###0; (-#)" /> <font size=2>Yrs, </font><jato:text name="stEffectiveAmortizationMonths" escape="true" formatType="decimal" formatMask="###0; (-#)" /> <font size=2>Mths</font></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Additional Principal Payment:</b></font><br>$
<jato:textField name="txAdditionalPrincipalPayment" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<!-- PPI -->
<jato:text name="stDealPurposeTypeHiddenStart" escape="false"/>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Value of Improvements:</b></font><br>
    <font size=2> <jato:text name="txRefiImprovementValue"
    escape="true" formatType="currency" formatMask="$ #,##0.00; (-#)" /></font></td>
<td valign=top><font size=2 color=3366cc><b>As Improved Value:</b></font><br>
    <font size=2> <jato:text name="txImprovedValue"
    escape="true" formatType="currency" formatMask="$ #,##0.00; (-#)" /></font></td><tr>	
<jato:text name="stDealPurposeTypeHiddenEnd" escape="false"/>
<!-- PPI -->

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Actual Payment Term:</b></font><br><font size=2>Yrs: </font>
<jato:textField name="txPayTermYears" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /><font size=2>Mths: </font>
<jato:textField name="txPayTermMonths" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td>
<td valign=top><font size=2 color=3366cc><b>P & I Payment:</b></font><br><jato:text name="stPIPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Total Escrow Payment:</b></font><br><jato:text name="stTotalEscrowPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Total Payment:</b></font><br><jato:text name="stTotalPayment" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></td>
<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<td valign=top colspan=2><font size=2 color=3366cc><b>Payment Including Life / Disability:</b></font><br><jato:text name="stPmntInclLifeDis" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></td>
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=5><font size=2 color=3366cc><b>Repayment Type:</b></font><br><jato:combobox name="cbRepaymentType" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Estimated Closing Date:</b></font><br><font size=2>Mth: </font><jato:combobox name="cbEstClosingDateMonths" extraHtml="onChange='setChangedEstimatedClosingDate()'" /> <font size=2>Day: </font>
<jato:textField name="txEstClosingDateDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2"  extraHtml="onChange='setChangedEstimatedClosingDate()'"/> <font size=2>Yr: </font>
<jato:textField name="txEstClosingDateYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4"  extraHtml="onChange='setChangedEstimatedClosingDate()'"/></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>First Payment Date:</b></font><br><font size=2>Mth: </font><jato:combobox name="cbFirstPayDateMonth" /> <font size=2>Day: </font>
<jato:textField name="txFirstPayDateDay" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /> <font size=2>Yr: </font>
<jato:textField name="txFirstPayDateYear" formatType="decimal" formatMask="###0; (-#)" size="4" maxLength="4" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Maturity Date:</b></font><br><jato:text name="stMaturityDate" escape="true" formatType="date" formatMask="MMM  dd  yyyy" /></td>
<td valign=top colspan=1><font size=2 color=3366cc><b>Pre-App. Est. Purchase Price:</b></font><br>$
<jato:textField name="txPreAppEstPurchasePrice" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>2nd Mortgage Exists?</b></font><br><jato:combobox name="cbSecondMortgageExist" /></td>
<td valign=top><font size=2 color=3366cc><b>Pre-payment Penalty:</b></font><br><jato:combobox name="cbPrePaymentPenanlty" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Privilege Payment Option:</b></font><br><jato:combobox name="cbPrivilagePaymentOption" /></td>
<td valign=top colspan=1><font size=2 color=3366cc>
<jato:text name="stIncludeFinancingProgramStart" escape="false" />
<b>Financing Program:</b></font><br><jato:combobox name="cbFinancingProgram" />
<jato:text name="stIncludeFinancingProgramEnd" escape="false" />
</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Special Feature:</b></font><br><jato:combobox name="cbSpecialFeature" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<jato:text name="stIncludeCommitmentStart" escape="false" />
<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Commitment Expected Return Date:</b></font><br><font size=2>Mth: </font><jato:combobox name="cbCommExpectedDateMonth" /> <font size=2>Day: </font>
<jato:textField name="txCommExpectedDateDay" size="2" maxLength="2" /> <font size=2>Yr: </font>
<jato:textField name="txCommExpectedDateYear" size="4" maxLength="4" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Commitment Period:</b></font><br><jato:text name="stCommitmentPeriod" escape="true" formatType="decimal" formatMask="###0; (-#)" /></td>
<!--  
<td valign=top><font size=2 color=3366cc><b>Commitment Expiration Date:</b></font><br><jato:text name="stCommExpDate" escape="true" formatType="date" formatMask="dd-MMM-yyyy" /></td>
-->
<td valign=top colspan=2><font size=2 color=3366cc><b>Commitment Acceptance Date:</b></font><br><jato:text name="stCommAcceptanceDate" escape="true" formatType="date" formatMask="dd-MMM-yyyy" /></td>
</tr>
<jato:text name="stIncludeCommitmentEnd" escape="false" />

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<%--CR03 start --%>
<tr>
    <td valign=top colspan=1>&nbsp;<jato:hidden name="hdCommitmentExpiryDate" />
		<input type="hidden" id="hdCommitmentExpiryDate" value='<jato:text name="hdCommitmentExpiryDate"/>'/>
		<jato:hidden name="hdMosProperty" />
		<input type="hidden" id="hdMosProperty" value='<jato:text name="hdMosProperty"/>'/>
	</td>
    <td valign="top" colspan=2><font size="2" color="3366cc"><b>Automatically Calculate Commitment Expiration Date:</b>
        </font><br><font size="2"> 
	    <jato:radioButtons name="rbAutoCalCommitDate" layout="horizontal" extraHtml="onClick='changeExpireDate();'" />
		</font>
    </td>
    <td valign=top colspan=2><font size=2 color=3366cc><b>Commitment Expiration Date:</b></font><br><font size=2>Mth: </font>
    <jato:combobox name="cbCommExpirationDateMonth" extraHtml="id='cbCommExpirationDateMonth'"/> <font size=2>Day: </font>
    <jato:textField name="txCommExpirationDateDay" size="2" maxLength="2" extraHtml="id='txCommExpirationDateDay'"/> <font size=2>Yr: </font>
    <jato:textField name="txCommExpirationDateYear" size="4" maxLength="4" extraHtml="id='txCommExpirationDateYear'"/></td>
<td valign=top colspan=1><font size=2 color=3366cc><b><jato:text name="stMarketTypeLabel" escape="false" fireDisplayEvents="true" /></b></font><br>
<font size=2><jato:text name="stMarketType" escape="false" fireDisplayEvents="true" /></font></td>
    <td valign=top colspan=2><font size=2 color=3366cc><b>Financing Waiver Date:</b></font><br><font size=2>Mth: </font>
    <jato:combobox name="cbFinancingWaiverDateMonth" extraHtml="id='cbFinancingWaiverDateMonth'"/> <font size=2>Day: </font>
    <jato:textField name="txFinancingWaiverDateDay" size="2" maxLength="2" extraHtml="id='txFinancingWaiverDateDay'"/> <font size=2>Yr: </font>
    <jato:textField name="txFinancingWaiverDateYear" size="4" maxLength="4" extraHtml="id='txFinancingWaiverDateYear'"/></td>

</tr>
<%--CR03 end --%>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr>
<td valign=top colspan=1>&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Bridge Loan Amount:</b></font><br>$ <jato:text name="stBridgeLoanAmount" escape="true" formatType="currency" formatMask="###0.00; (-#)" /> &nbsp;&nbsp;&nbsp;<jato:button name="btReviewBridge" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/review_bridge.gif" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Holdback Amount:</b></font><br>$
<jato:textField name="txAdvanceHold" formatType="decimal" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
    <td valign=top colspan=2><font size=2 color=3366cc><b>Commission Code:</b></font><br>
	<jato:textField name="txCommisionCode" size="35" maxLength="35" /></td>

</tr>


<tr>
<td valign=top colspan=1>&nbsp;</td>

<td valign=top><font size=2 color=3366cc><b>Cashback Percentage:</b></font><br><jato:textField name="txCashBackInPercentage" formatType="decimal" formatMask="###0.00; (-#)" size="6" maxLength="6" />%</td>

<td valign=top><font size=2 color=3366cc><b>Cashback Amount:</b></font><br>$<jato:textField name="txCashBackInDollars" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="12" /></td>

<td valign=top colspan=2><font size=2><br><jato:checkbox name="chCashBackOverrideInDollars" /></font><font size=2 color=3366cc><b>Cashback $ Override</b></font></td>
<%--***** Change by NBC Impl. Team - Version 1.8 - Start*****--%>
<td valign=top><font size=2 color=3366cc><b>Affiliation Program:</b></font><br><jato:combobox name="cbAffiliationProgram" extraHtml="onChange='DisplayOrHideRefiSection();'" /></td>
<%--***** Change by NBC Impl. Team - Version 1.8 - End*****--%>
</tr>

<tr>

<td>&nbsp;</td>
<td colspan=7><jato:button name="btRecalculate" extraHtml="width=75 height=15 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/recalculate.gif" />&nbsp;&nbsp;&nbsp;</td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
<!--END FINANCIAL DETAILS//-->

<!--START Refinance Info.//-->
<div id="refisection" class="refisection" name="refisection">
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Equity Take-Out/Switch/Refinance Information</b></font></td><tr>
<td colspan=5>&nbsp;</td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top width=400px><font size=2 color=3366cc><b>Date Purchased:</b></font></td>
<td valign=top><font size=2>Mth: </font><jato:combobox name="cbRefiOrigPurchaseDateMonth" /> <font size=2>Day: </font>
<jato:textField name="txRefiOrigPurchaseDateDay" size="2" maxLength="2" /> <font size=2>Yr: </font>
<jato:textField name="txRefiOrigPurchaseDateYear" size="4" maxLength="4" /></td>
<td valign=top width=300px><font size=2 color=3366cc><b>Purpose:</b></font></td>
<td valign=top><jato:textField name="txRefiPurpose" size="28" maxLength="80" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Mortgage Holder:</b></font></td>
<td valign=top>
<jato:textField name="txRefiMortgageHolder" size="35" maxLength="80" /></td>
<td valign=top><font size=2 color=3366cc><b>Blended Amortization:</b></font></td>
<td valign=top><font size=2>
<jato:radioButtons name="rbBlendedAmortization" layout="horizontal" /></font></td><tr>

<td>&nbsp;</td>
<td valign=top nowrap><font size=2 color=3366cc><b>Original Purchase Price:</b></font></td>
<td valign=top>$ 
<jato:textField name="txRefiOrigPurchasePrice" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Original Mtg. Amount:</b></font></td>
<td valign=top>$ 
<jato:textField name="txRefiOrigMtgAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td><tr>

<td>&nbsp;</td>
<td valign=top nowrap><font size=2 color=3366cc><b>Value of Improvements:</b></font></td>
<td valign=top>$ 
<jato:textField name="txRefiImprovementValue" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Outstanding Mtg. Amount:</b></font></td>
<td valign=top>$ 
<jato:textField name="txRefiOutstandingMtgAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<tr>
<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Improvements:</b></font></td>
      <td valign=top width="480">
	    <%-- Release3.1 Apr 26, 2006 ; chenged size --%>
	    <%-- Ticket #3463 chenged size--%>
	    <jato:textField name="txRefiImprovementsDesc" size="63"  maxLength="80"/>
</td>

<%--
/***************MCM Impl team changes starts - XS_2.7 *******************/
--%>
<td vAlign=top><font color=#3366cc size=2><b>Existing Account Reference:</b></font></td>
<td vAlign=top>
	<jato:textField name="txRefExistingMTGNumber" extraHtml="" size="35"  maxLength="35"/>
</td>

<tr>
<td>&nbsp;</td>
<td vAlign=top><font color=#3366cc size=2><b>Product Type:</b></font></td>
<td vAlign=top>
	<jato:combobox name="cbRefiProductType" />
</td>

<%-- Release3.1 Apr 26, 2006 begins--%>
<td vAlign=top>&nbsp;</td>
<td vAlign=top>&nbsp;
</td>
<tr>
<td>&nbsp;</td>
<td vAlign=top><font color=#3366cc size=2><b>Additional Details:</b></font></td>
<td vAlign=top colspan=3>
	<jato:textArea name="txRefiAdditionalInformation" rows="4" cols="98" 
      extraHtml="wrap=soft maxlength=512"/>
</td>
<%--
/***************MCM Impl team changes ends - XS_2.7 *********************/
--%>
<%-- Release3.1 Apr 26, 2006 begins--%>

<%-- Release3.1 Apr 26, 2006 ends--%>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td>
<tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td><tr>
</table>
</div>
<!--END Refinance Info.//-->

<!-- MCM Impl team changes starts - XS 2.9 -->
<div id="divComponentInfo">
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<jato:containerView name="componentInfoPagelet" type="mosApp.MosSystem.pgComponentInfoPageletViewBean">
			<jsp:include page="pgComponentInfoPagelet.jsp"/>
		</jato:containerView>
	</tr>
</table>
</div>
<!-- MCM Impl team changes ends - XS 2.9 -->

<!--START PROGRESS ADVANCE//-->

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>

<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Progress Advance</b></font></td><tr>
<td colspan=5>&nbsp;</td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>

<td valign=top>&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Progress Advance:</b></font></td>
<td valign=top><font size="2">
<jato:radioButtons name="rbProgressAdvance" layout="horizontal" extraHtml="onClick='hideSubProgressAdvance();'" /></font></td><%--Release3.1 extraHtml--%>
<td valign=top><font size=2 color=3366cc><b>Next Advance Amount:</b></font></td>
<td valign=top>$ 
<jato:textField name="txNextAdvanceAmount" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td><tr>

<td>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Advance to Date Amount:</b></font></td>
<td valign=top>$ 
<jato:textField name="txAdvanceToDateAmt" formatType="currency" formatMask="###0.00; (-#)" size="14" maxLength="14" /></td>
<td valign=top><font size=2 color=3366cc><b>Advance Number:</b></font></td>
<td valign=top>
<jato:textField name="txAdvanceNumber" formatType="decimal" formatMask="###0; (-#)" size="2" maxLength="2" /></td><tr>

<td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td><tr>
<td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td><tr>
</table>
<!--END PROGRESS ADVANCE//-->

<!--START DOWN PAYMENT INFO//-->
<jato:text name="stTargetDownPayment" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=8 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Down Payment</b></font></td>
</tr>

<tr><td colspan=8 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Down Payment Source</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Down Payment Description</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Down Payment</b></font></td>
<td>&nbsp;</td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedDownPayment" type="mosApp.MosSystem.pgDealEntryRepeatedDownPaymentTiledView">
<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
<jato:hidden name="hdDownPaymentSourceId" />
<jato:hidden name="hdDownPaymentCopyId" /></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><jato:combobox name="cbDownPaymentSource" /></td>
<td valign=top>
<jato:textField name="txDPSDescription" size="35" maxLength="35" /></td>
<td valign=top>$ 
<jato:textField name="txDownPayment" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top><jato:button name="btDeleteDownPayment" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_downpay.gif" /></td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=4>

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddDownPayment" extraHtml="width=110  height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_downpay.gif" /></td>
<td valign=top><font size=2 color=3366cc><b>Total Down Payment:</b></font>&nbsp;$ 
<jato:textField name="txTotalDown" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
<td valign=top><font size=2 color=3366cc><b>Down Payment Required:</b></font>&nbsp;<jato:text name="stDownPaymentRequired" escape="true" formatType="currency" formatMask="#,##0.00; (-#)" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=8><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=8><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<!--END DOWN PAYMENT INFO//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START ESCROW DETAILS//-->
<jato:text name="stTargetEscrow" fireDisplayEvents="true" escape="false" />

<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Escrow Details</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Escrow Type</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Escrow Payment Description</b></font></td>
<td valign=top><font size=2 color=3366cc><b>Escrow Payment</b></font></td>
<td>&nbsp;</td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--START REPEATING ROW//-->
<jato:tiledView name="RepeatedEscrowDetails" type="mosApp.MosSystem.pgDealEntryRepeatedEscrowDetailsTiledView">

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0">
<jato:hidden name="hdEscrowPaymentId" />
<jato:hidden name="hdEscrowCopyId" /></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><jato:combobox name="cbEscrowType" /></td>
<td valign=top>
<jato:textField name="stEscrowPaymentDesc" size="20" maxLength="35" /></td>
<td valign=top>$ 
<jato:textField name="stEscrowPayment" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top><jato:button name="btDeleteEscrow" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/delete_escrow.gif" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

<!--END REPEATING ROW//-->
</jato:tiledView>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5>

<table border=0 cellspacing=3 width=100%>
<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btAddEscrow" extraHtml="width=71  height=15 onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/add_escrow.gif" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Total Escrow Payment:</b></font>&nbsp;$ 
<jato:textField name="txTotalEscrow" formatType="currency" formatMask="###0.00; -#" size="20" maxLength="20" /></td>
</tr>
</table>

</td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>
<!--END ESCROW DETAILS//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START NON-FINANCIAL DETAILS//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=6 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;
Non-Financial Details</b></font></td>
</tr>
<tr><td colspan=6 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Branch:</b></font><br><jato:combobox name="cbBranch" fireDisplayEvents="true" /></td>
<%-- LEN497489 --%>
<td valign=top><font size=2 color=3366cc><b>Cross Sell:</b></font><br><jato:combobox name="cbCrossSell" /></td>
<td valign=top><font size=2 color=3366cc><b>Tax Payor:</b></font><br><jato:combobox name="cbTaxPayor" elementId="cbTaxPayor" onChange="flipChIncludeTaxPayments(this)" /></td>
<td valign=top><br><jato:button name="btPartySummary" extraHtml="onClick = 'setSubmitFlag(true);'" src="../images/party_summary.gif" /></td>
</tr>                                                 


<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b><jato:text name="stBranchTransitLabel" escape="false" fireDisplayEvents="true" /></b></font><br>
<td valign=top><font size=2><jato:text name="stBranchTransitNumber" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stPartyName" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stAddressLine1" escape="false" fireDisplayEvents="true" />&nbsp;<font size=2><jato:text name="stCity" escape="false" fireDisplayEvents="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Reference Type:</b></font><br>
<jato:text name="stReferenceType" escape="true" /></td>
<td valign=top><font size=2 color=3366cc><b>Reference Deal #:</b></font><br>
<jato:text name="stReferenceDealNo" escape="true" /></td>
</tr>

<tr><td colspan=6><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
</table>

<!--END NON-FINANCIAL DETAILS//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START MORTGAGE INSURANCE//-->
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Mortgage Insurance</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Mortgage Insurance Indicator:</b></font><br><jato:combobox name="cbMIIndicator" /></td>
<td valign=top><font size=2 color=3366cc><b>Mortgage Insurance Status:</b></font><br><jato:text name="stMIStatus" escape="true" /></td>
<td valign=top colspan=2><font size=2 color=3366cc><b>MI Existing Policy #:</b></font><br>
<jato:textField name="txMIExistingPolicy" size="35" maxLength="35" /></td>
</tr>

<tr><td colspan=2><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr> 
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>MI Insurer</b></font><br>
	<jato:combobox name="cbMIInsurer" elementId="cbMIInsurer" extraHtml="onChange='SetMIPolicyNoDisplay(); hideRequestStandardService();'" /><%--Release3.1 extraHtml--%>
</td>
<td valign=top><font size=2 color=3366cc><b>MI Type:</b></font><br>
    <jato:combobox name="cbMIType" elementId="cbMIType" extraHtml="onChange='hideRequestStandardService();'" /><%--Release3.1 extraHtml--%>
</td>
<td valign=top><font size=2 color=3366cc><b>MI Payor:</b></font><br>
<%--MCM Team added extraHtml --%>
	<jato:combobox name="cbMIPayor"  onChange="selectMIUpfront()"  elementId="cbMIPayor" />
</td>
<td valign=top><font size=2 color=3366cc><b>Pre Qualification MI Certificate #:</b></font><br><font size="2">
	<jato:text name="stMIPreQCertNum" escape="true" /></font>
</td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top>&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>MI Premium:</b></font><br>$ 
<jato:textField name="txMIPremium" formatType="currency" formatMask="###0.00; (-#)" size="13" maxLength="13" /></td>
<td valign=top><font size=2 color=3366cc><b>MI Certificate #:</b></font><br><font size="2">
<jato:textField name="txMICertificate" size="25" maxLength="50" extraHtml="disabled" /></font></td>
<td valign=top><font size=2 color=3366cc><b>MI Upfront</b></font><br><font size=2>
<jato:radioButtons name="rbMIUpfront" layout="horizontal" styleClass="rbMIUpfront" onClick="flipChIncludeMIPremiums(this)"/></font></td>
<td valign=top><font size=2 color=3366cc><b>MI RU Intervention</b></font><br><font size=2>
<jato:radioButtons name="rbMIRUIntervention" layout="horizontal" /></font></td>
</tr>
			<!-- SEAN DJ SPEC-Progress Advance Type July 22, 2005:  -->
			<tr>
				<td colspan="5"><img src="../images/light_blue.gif" width="1"
					height="2" alt="" border="0"></td>
			</tr>

			<tr>
				<td valign="top">&nbsp;</td>
				<td valign="top">
<div id="divProgressAdvanceType" name="divProgressAdvanceType">
	      			<font size=2 color=3366cc><b>Progress Advance Type:</b></font><br>
					<jato:combobox name="cbProgressAdvanceType"/>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
				</td>
      <!-- SEAN DJ SPEC-PAT END -->

<%-- Release3.1 Apr 18, 2006 begins --%>
				<TD vAlign=top>
<div id="divProgressAdvanceInspectionBy" name="divProgressAdvanceInspectionBy">
					<FONT color=#3366cc size=2><B>Progress Advance Inspection By:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbProgressAdvanceInspectionBy" layout="horizontal" />
					</FONT>
</div>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>Self Directed RRSP:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbSelfDirectedRRSP" layout="horizontal" />
					</FONT>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>CMHC Product Tracker Identifier:</B></FONT><BR>
					<jato:textField name="txCMHCProductTrackerIdentifier" size="5" maxLength="5" />
					
					
				</TD>
			</TR>
			<TR>
				<td valign="top">&nbsp;</td>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>LOC Repayment:</B></FONT><BR>
					<jato:combobox name="cbLOCRepaymentTypeId" />
				</TD>
				<TD vAlign=top>
<div id="divReqStdSvc" name="divReqStdSvc">
					<FONT color=#3366cc size=2><B>Request Standard Service:</B></FONT><BR>
					<FONT size=2>
						<jato:radioButtons name="rbRequestStandardService" layout="horizontal" />
					</FONT>
</div>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>LOC Amortization:</B></FONT><BR>
					<FONT size=2>
						 <jato:text name="stLOCAmortizationMonths" fireDisplayEvents="true" escape="true" /> 
					</FONT>
				</TD>
				<TD vAlign=top>
					<FONT color=#3366cc size=2><B>LOC Interest Only Maturity Date:</B></FONT><BR>
					<FONT size=2>
						<jato:text name="stLOCInterestOnlyMaturityDate" escape="true" formatType="date" formatMask="MMM dd yyyy" />
					</FONT> 
				</TD>
			</tr>				
<%-- Release3.1 Apr 18, 2006 ends --%>


<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>

</table>

<!--END MORTGAGE INSURANCE//-->

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>

<!--START MORTGAGE OPTIONS//-->
<jato:text name="stIncludeLifeDisLabelsStart" escape="false" />
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td colspan=5 valign=top><font size=3 color=3366cc><b>&nbsp;&nbsp;&nbsp;Mortgage Options</b></font></td>
</tr>
<tr><td colspan=5 bgcolor=d1ebff><font size=1>&nbsp;</font></td></tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>

<tr>
<td valign=top><font size=2 color=3366cc><b>&nbsp;&nbsp;&nbsp;&nbsp;Multiproject:</b></font>
<font size=2><jato:radioButtons name="rbMultiProject" layout="horizontal" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Homeowner's Protection:</b></font>
<font size=2><jato:radioButtons name="rbProprietairePlus" layout="horizontal" /></font></td>

<td valign=top><font size=2 color=3366cc><b>Line of credit:</b></font>
<jato:textField name="txProprietairePlusLOC" formatType="decimal" formatMask="###0.00; (-#)" size="20" maxLength="20" /></td>
</tr>

<tr><td colspan=5><img src="../images/light_blue.gif" width=1 height=2 alt="" border="0"></td></tr>
<tr><td colspan=5><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td></tr>
</table>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><a onclick="return IsSubmited();" href="#top"><img src="../images/return_top_sm.gif" width=83 height=15 alt="" border="0"></a></td></tr>
</table>
<jato:text name="stIncludeLifeDisLabelsEnd" escape="false" />
<!--END MORTGAGE OPTIONS//-->

<p>

<table border=0 width=100% cellpadding=0 cellspacing=0>
<tr><td align=right><jato:button name="btDealSubmit" extraHtml="onClick = 'return checkMIInsurer();setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit.gif" />&nbsp;&nbsp;&nbsp;&nbsp;<jato:button name="btDealCancel" extraHtml="onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td></tr>
</table>

</center>
</div>

<div id="validlayer" class="validlayer" name="validlayer">
<center>
<table border=0 width=650 cellpadding=4>
	<td align=center><br><font size=4 face="arial, helvetica" color=ffcc00>

		Required Data Missing

	</font></td>
<tr>
	<td>
	<center>
	<a onclick="return IsSubmited();" href="validShow()"><img src="../images/ok_yel.gif" width=86 height=25 alt="" border="0"></a>
	</center></td>
	
</table>
</center>
</div>

<div id="softvalidlayer" class="softvalidlayer" name="softvalidlayer">
<center>
<table border=0 width=650 cellpadding=4>
	<td align=center><br><font size=4 face="arial, helvetica" color=ffcc00>

		Informational Message: Data Missing

		</font></td>
<tr>
	<td>
	<center>
 	<a onclick="return IsSubmited();" href="validShow()"><img src="../images/ok_yel.gif" width=86 height=25 alt="" border="0"></a>
	</center></td>
</table>
</center>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopWithNoteSection.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:useViewBean>
</HTML>
