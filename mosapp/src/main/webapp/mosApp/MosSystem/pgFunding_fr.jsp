
<HTML>
<%@page info="pgFunding" language="java"%>
<%@taglib uri="/WEB-INF/jato.tld" prefix="jato"%>
<jato:useViewBean className="mosApp.MosSystem.pgFundingViewBean">

<HEAD>
<TITLE>Avance de fonds</TITLE>
<STYLE>
<!--
.dialogbar {position: absolute; left: 57; top: 87; overflow: hidden; visibility:hidden;}
.toolpop {position: absolute; left: 320; top: 87; width:300px; overflow: hidden; visibility:hidden;}
.pagebody {position: absolute; visibility: hidden; top: 110; left: 10; width:100%; right-margin:5%;}
.alertbody {position: absolute; visibility: hidden; left:10; top: 110; width: 100%;overflow: hidden;}
-->
</STYLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
// Include English System Messages
<%@include file="/JavaScript/SystemMessages_fr.txt" %>
-->
</SCRIPT>
<script src="../JavaScript/GenericCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/OnlineCalcAndHelp.js" type="text/javascript"></script>
<script src="../JavaScript/GotoHandling.js" type="text/javascript"></script>
<script src="../JavaScript/PageShellCommonFunctions.js" type="text/javascript"></script>
<script src="../JavaScript/FieldValidation.js" type="text/javascript"></script>
<script src="../JavaScript/DefaultHandling.js" type="text/javascript"></script>

</HEAD>
<body bgcolor=ffffff>
<jato:form name="pgFunding" method="post" onSubmit="return IsSubmitButton();">

<SCRIPT LANGUAGE="JavaScript">

/////////////////////////// End of VALIDATION //////////////
var pmGenerate = "<jato:text name="stPmGenerate" escape="true" />";
var pmHasTitle = "<jato:text name="stPmHasTitle" escape="true" />";
var pmHasInfo = "<jato:text name="stPmHasInfo" escape="true" />";
var pmHasTable = "<jato:text name="stPmHasTable" escape="true" />";
var pmHasOk = "<jato:text name="stPmHasOk" escape="true" />";
var pmTitle = "<jato:text name="stPmTitle" escape="true" />";
var pmInfoMsg = "<jato:text name="stPmInfoMsg" escape="false" />";
var pmOnOk = <jato:text name="stPmOnOk" escape="true" />;
pmMsgTypes = new Array(<jato:text name="stPmMsgTypes" escape="true" />);
pmMsgs = new Array(<jato:text name="stPmMsgs" escape="false" />);


var amGenerate = "<jato:text name="stAmGenerate" escape="true" />";
var amHasTitle = "<jato:text name="stAmHasTitle" escape="true" />";
var amHasInfo = "<jato:text name="stAmHasInfo" escape="true" />";
var amHasTable = "<jato:text name="stAmHasTable" escape="true" />";
var amTitle = "<jato:text name="stAmTitle" escape="true" />";
var amInfoMsg = "<jato:text name="stAmInfoMsg" escape="false" />";
var amDialogMsg = "<jato:text name="stAmDialogMsg" escape="true" />";
var amButtonsHtml = '<jato:text name="stAmButtonsHtml" escape="false" />';
amMsgTypes = new Array(<jato:text name="stAmMsgTypes" escape="true" />);
amMsgs = new Array(<jato:text name="stAmMsgs" escape="false" />);

generateAM();

</SCRIPT>


<jato:hidden name="sessionUserId" />
<input type="hidden" name="isAlert" value="<jato:text name="stErrorFlag" escape="true" />">
<input type="hidden" name="isFatal" value="">

	<!--HEADER//-->	
	<%@include file="/JavaScript/CommonHeader_fr.txt" %>
	  
    <!--TOOLBAR//-->
    <%@include file="/JavaScript/CommonToolbar_fr.txt" %>
      
	<!--TASK NAVIGATER //-->
	<%@include file="/JavaScript/TaskNavigater.txt" %> 

<!--END HEADER//-->

	<!--DEAL SUMMARY SNAPSHOT//-->
	<div id="pagebody" class="pagebody" name="pagebody">
	<%@include file="/JavaScript/CommonDealSummarySnapshot_fr.txt" %> <br>
	<!--END DEAL SUMMARY SNAPSHOT//-->

<!--BODY OF PAGE//-->
<center>
<p>
<table border=0 width=100% cellpadding=0 cellspacing=0>
	<tr>
		<td align=center valign=middle colspan=4><font size=3><jato:text name="stViewOnlyTag" fireDisplayEvents="true" escape="true" /></font></td>
	</tr>
</table>
<table border=0 width=100% cellpadding=1 cellspacing=0 bgcolor=d1ebff>
<tr>
<td colspan=6><img src="../images/dark_bl.gif" width=100% height=3 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td><tr>

<td colspan=6><font size=4 color=3366cc><b>D�tails de l'avance de fonds:</b></font>&nbsp;</td>
<tr>

<td valign=top><font size=2 color=3366cc><b>Date pr�vue d'avance de fonds:</b></font>&nbsp;<br>
  <font size="2">	<jato:text name="stFundDate" escape="true" formatType="date" formatMask="fr|MMM dd yyyy" /></font>&nbsp;</td>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Date r�elle d'avance de fonds:</b></font>&nbsp;<br>
	<font size="2">Mois:&nbsp;<jato:combobox name="cbMonths" />&nbsp;Jour:<jato:textField name="tbDay" size="2" maxLength="2" />&nbsp;An:<jato:textField name="tbYear" size="4" maxLength="4" /></font></td>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>
	<jato:button name="btFundDeal" extraHtml="width=130 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/FundDealYel_fr.gif" />&nbsp;&nbsp;
	<jato:button name="btUpload" extraHtml="alt='Click to Upload' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/upload_fr.gif" />
</td>
<td align="left">
	<jato:checkbox name="chBrokerCommPaid" fireDisplayEvents="true" /><font size=2 color=3366cc><jato:text name="stBrokerCommPaidLabel" escape="true" fireDisplayEvents="true" /></font>
</td>

<tr>

<td colspan=6><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=1 alt="" border="0"></td><tr>

<td colspan=6><font size=4 color=3366cc><b>Information suppl�mentaire:</b></font>&nbsp;</td>
<tr>

<td colspan=6 valign=top><font size=2 color=3366cc><b>Num�ro de gestion d'hypoth�que:</b></font>&nbsp;<br>
	<jato:textField name="tbServMortgNum" size="20" maxLength="35" />&nbsp;<jato:hidden name="hdBankName" /><jato:hidden name="hdBankABABankNum" /></td>
<tr>

<td valign=top><font size=2 color=3366cc><b>N<sup>o</sup> de compte et de succursale:</b></font>&nbsp;<br>
	<jato:textField name="tbBankABABankNum" size="3" maxLength="3" />&nbsp;-&nbsp;<jato:textField name="tbBankABATransitNum" size="16" maxLength="16" /></td>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top><font size=2 color=3366cc><b>Num�ro de compte bancaire:</b></font>&nbsp;<br>
	<jato:textField name="tbBankAccountNum" size="20" maxLength="20" /></td>
<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top colspan=2><font size=2 color=3366cc><b>Nom de la banque:</b></font>&nbsp;<br>
	<jato:textField name="tbBankName" size="60" maxLength="60" /></td>
<tr>

<td colspan=6><img src="../images/light_blue.gif" width=100% height=2 alt="" border="0"></td><tr>
<td colspan=6><img src="../images/blue_line.gif" width=100% height=2 alt="" border="0"></td>
</table>

<table border=0 width=100%>
<td align=right><jato:button name="btSubmit" extraHtml="width=83 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/submit_fr.gif" />&nbsp;&nbsp;<jato:button name="btCancel" extraHtml="height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/cancel_fr.gif" /> <jato:button name="btOK" extraHtml="width=86 height=25 alt='' border='0' onClick = 'setSubmitFlag(true);'" fireDisplayEvents="true" src="../images/ok.gif" /></td>
</table>
</center>

<br><br><br><br><br>
</div>

<div id="toolpop" class="toolpop" name="toolpop">
<%@include file="/JavaScript/ToolPopSection_fr.txt" %>
</div>

<div id="dialogbar" class="dialogbar" name="dialogbar">
<%@include file="/JavaScript/PreviousPagesLinksSection.txt" %>
</div>

</jato:form>


<script language="javascript">
<!--
if(NTCP){
	document.dialogbar.left=55;
	document.dialogbar.top=79;
	document.toolpop.left=317;
	document.toolpop.top=79;
	document.pagebody.top=100;
	document.alertbody.top=100;
}



if(document.forms[0].isAlert.value=="Y" || document.forms[0].isFatal.value=="Y"){
	tool_click(4)
}
else{
	tool_click(5)
}

if(pmGenerate=="Y")
{
openPMWin();
}

//-->
</script>

<!--END BODY//-->

</BODY>

</jato:useViewBean>
</HTML>
