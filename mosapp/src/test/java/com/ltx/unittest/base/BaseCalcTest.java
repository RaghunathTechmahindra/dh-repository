package com.ltx.unittest.base;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.ddsteps.data.DataLoader;
import org.ddsteps.data.support.DataLoaderFactory;
import org.ddsteps.testcase.support.DDStepsExcelTestCase;

import com.basis100.deal.calc.CalcEntityCache;
import com.basis100.deal.calc.DealCalc;
import com.basis100.deal.entity.DealEntity;
import com.ltx.unittest.util.CalcTestUtil;
import com.ltx.unittest.util.EntityTestUtil;

//import junit.framework.TestCase;

public abstract class BaseCalcTest extends BaseTest
{

	private DealCalc dealCalc = null;
	
	public BaseCalcTest(String calcName)
	{
		dealCalc = CalcTestUtil.getInstance().loadCalc(calcName);
		System.out.println(calcName);
	}
	
	public DataLoader createDataLoader() {
		return DataLoaderFactory.getCachingExcelDataLoader();
	}
	
	public final DealEntity loadTestDataObject(String entityName)
	{
		return EntityTestUtil.getInstance().loadEntity(entityName);
	}
	
   public abstract void testDoCalc() throws Exception;

	public DealCalc getDealCalc() {
		return dealCalc;
	}
	
	public void setEntityCacheAndSession()
  {
	Class classCalc = dealCalc.getClass();
	Class superClassofCalc = classCalc.getSuperclass();
	Field ff[] = superClassofCalc.getDeclaredFields();
	for ( Field field : ff )
	{
	  field.setAccessible(true);
	  if (field.getName().equalsIgnoreCase("entityCache"))
	  {
		Class fieldClasstest = field.getType();
		System.out.println(fieldClasstest.getName());
		Constructor con[] = fieldClasstest.getDeclaredConstructors();
		for ( Constructor conss : con )
		{
		  conss.setAccessible(true);
		  System.out.println(conss.getModifiers() + " " + conss.getName());

		  conss.setAccessible(true);
		  CalcEntityCache cache;
		  try
		  {
			cache = (CalcEntityCache) conss.newInstance(EntityTestUtil
				.getCalcMonitor());
			field.set(dealCalc, cache);
		  }
		  catch (Exception e)
		  {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }

		}
		break;
	  }
	}

	dealCalc.setResourceKit(EntityTestUtil.getSessionResourceKit());
  }

  public void clearEntityCache()
  {

	Class classCalc = dealCalc.getClass();
	Class superClassofCalc = classCalc.getSuperclass();
	Field ff[] = superClassofCalc.getDeclaredFields();
	for ( Field field : ff )
	{
	  field.setAccessible(true);
	  if (field.getName().equalsIgnoreCase("entityCache"))
	  {
		Class fieldClasstest = field.getType();
		System.out.println(fieldClasstest.getName());
		Constructor con[] = fieldClasstest.getDeclaredConstructors();
		for ( Constructor conss : con )
		{
		  conss.setAccessible(true);
		  System.out.println(conss.getModifiers() + " " + conss.getName());

		  conss.setAccessible(true);
		  CalcEntityCache cache;
		  try
		  {
		
			field.set(dealCalc, null);
		  }
		  catch (Exception e)
		  {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }

		}
		break;
	  }
	}


  }
	
}
