package com.ltx.unittest.base;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.pk.DealPK;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;
import com.ltx.unittest.util.PropertyManager;

import org.ddsteps.data.DataLoader;
import org.ddsteps.testcase.support.DDStepsExcelTestCase;

public abstract class BaseTest extends DDStepsExcelTestCase
{

    protected Deal deal;
    private MasterDeal masterDeal;
    protected DealPK dealPK;
    private int copyId;
    private int dealInstitutionId ;

    public BaseTest()
    {
        masterDeal = null;
        dealPK = null;
        try
        {
            masterDeal = (MasterDeal)EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MasterDeal");
            setDealInstitutionId(Integer.parseInt(PropertyManager.instance().getUtfDealInstitutionId()));
            EntityTestUtil.getInstance().getSessionResourceKit().getExpressState().setDealInstitutionId(getDealInstitutionId());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public abstract DataLoader createDataLoader();

    public void tearDownTest()
    {
    }
    public void tearDownMethod()
    {
      System.out.println("Data Removed. Resources Released");
      EntityTestUtil.getSessionResourceKit().freeResources();
    }
    protected int getCopyId()
    {
        return copyId;
    }

    protected void setCopyId(int copyId)
    {
        this.copyId = copyId;
    }

    protected Deal getDeal()
    {
        return deal;
    }

    protected void setDeal(Deal deal)
    {
        this.deal = deal;
    }

    protected DealPK getDealPK()
    {
        return dealPK;
    }

    public void setDealPK(DealPK dealPK)
    {
        this.dealPK = dealPK;
    }

    public MasterDeal getMasterDeal()
    {
        return masterDeal;
    }

    public SessionResourceKit getSessionResourceKit()
    {
        return EntityTestUtil.getSessionResourceKit();
    }

    public CalcMonitor getCalcMonitor()
    {
        return EntityTestUtil.getCalcMonitor();
    }

    public void setCalcMonitor(CalcMonitor calcMonitor)
    {
        EntityTestUtil.setCalcMonitor(calcMonitor);
    }

	public int getDealInstitutionId()
	{
	  return dealInstitutionId;
	}

	private void setDealInstitutionId(int dealInstitutionId)
	{
	  this.dealInstitutionId = dealInstitutionId;
	}
}