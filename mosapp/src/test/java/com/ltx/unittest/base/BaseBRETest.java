package com.ltx.unittest.base;

import java.util.Iterator;
import java.util.Vector;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.SessionStateModelImpl;

import org.ddsteps.data.DataLoader;
import org.ddsteps.data.support.DataLoaderFactory;
import org.ddsteps.testcase.support.DDStepsExcelTestCase;

import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.util.EntityTestUtil;
import com.ltx.unittest.util.PropertyManager;


/**
 * <p>Title: BaseBRETest</p>
 *
 * <p>Description: BaseTest Class for BRERule Engine.</p>
 *
 * <p>Copyright:  . (c)  * </p>
 *
 * <p>Company:  .</p>
 *
 * @author 
 * 
 * @version 1.0 (Initial Version �  July 2007)
 * @version 1.0 (Unit Test passed �  July 2007)
 *
 */
public class BaseBRETest extends BaseTest
{
  	//Static Variables
	public static PageEntry pe = null;
	public static SessionStateModelImpl theSessionState = null;
	public static SessionResourceKit srk = null;
	public static PassiveMessage pm =null; 
	public static BusinessRuleExecutor brExec = null; 
	public static Vector vRuleName= null;
	public static Vector vPmMsgs = null;
	
	//Instance Variables
	public String ruleName="";

	/*
	 * Loads the Mos System Properties
	 */
	static
	{
		PropertyManager.instance().initiate();
	}
	
	
	/**
	 *BaseBRETest()
	 *Default Constructor
	 * 
	 */
	public BaseBRETest()
	{
		pe = new PageEntry();
		 pm = new PassiveMessage();
		theSessionState = new SessionStateModelImpl();
		 srk = new SessionResourceKit();
		vRuleName = new Vector();
		vPmMsgs = new Vector();
		brExec = new BusinessRuleExecutor();
	}
	
	
	/**
  	 * createDataLoader()
  	 * Creates DataLoader to load Data from the Corresponding ExcelSheet.
  	 *
  	 * @param null <br>
  	 *
  	 * @return Loads the data from the Excel file - DataLoader <br>
  	 * @throws Exception 
  	 * 
  	 */
	public DataLoader createDataLoader() 
	{
		return DataLoaderFactory.getCachingExcelDataLoader();
	}
	
	
	/**
  	 * loadTestDataObject(String entityName)
  	 * loads data from the Excel sheet to the entity Properties.
  	 *
  	 * @param entityName - Name of the Entity <br>
  	 *
  	 * @return DealEntity - DealEntity Object<br>
  	 * 
  	 * @throws Exception 
  	 */
	public final DealEntity loadTestDataObject(String entityName)
	{
		return EntityTestUtil.getInstance().loadEntity(entityName);
	}
	
	
	/**
	 * execute BreRule()
	 * 
	 * Executes the business rule and returns the no of passive messages
	 * @return int - no of passive messages.
	 */
	public final int executeBreRule()
	{
	    int pmMsgsSize= 0;
		Iterator it;
		int l,size;
		
	   theSessionState.setSessionUserType(1);
	   //Commented by vikash for UTF migration
       //theSessionState.setSessionUserId(2);
       theSessionState.setLanguageId(0);
       
		try 
		{
		  pm=brExec.BREValidator(theSessionState,pe, srk, pm, vRuleName, true);
		  vPmMsgs = pm.getMsgs();
		  size = 0;
		  pmMsgsSize = vPmMsgs.size();
		  for (l=0;l<pmMsgsSize;l++)
		  {
			size = size +1;
		  }
		  return size;
		}
		catch (Exception e) 
		{
		  e.printStackTrace();
		}
		return 0;
	}

	/**
	 * getRuleName()
	 * gets the rule name
	 * @return String - name of the rule
	 */
	public String getRuleName()
	{
	  return ruleName;
	}

	/**
	 * setRuleName(String ruleName)
	 * sets the rule name
	 * 
	 */
	public void setRuleName(String ruleName)
	{
	  this.ruleName = ruleName;
	}	
}
