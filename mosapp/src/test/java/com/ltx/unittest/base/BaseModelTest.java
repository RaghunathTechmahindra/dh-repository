package com.ltx.unittest.base;

import java.sql.Connection;

import mosApp.ModelTypeMapImpl;
import mosApp.SQLConnectionManagerImpl;
import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.SessionStateModel;
import mosApp.MosSystem.SessionStateModelImpl;

import org.ddsteps.data.DataLoader;
import org.ddsteps.data.support.DataLoaderFactory;

import com.iplanet.jato.ModelManager;
import com.iplanet.jato.ModelTypeMap;
import com.iplanet.jato.RequestContextImpl;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.DatasetModelExecutionContext;
import com.iplanet.jato.model.DatasetModelExecutionContextImpl;
import com.iplanet.jato.model.ModelExecutionContext;
import com.iplanet.jato.model.sql.QueryModelBase;
import com.ltx.unittest.mocks.LM3MockHttpSession;
import com.ltx.unittest.mocks.LM3MockServletConfig;
import com.ltx.unittest.mocks.LM3MockServletContext;
import com.ltx.unittest.mocks.LM3MockServletRequest;
import com.ltx.unittest.mocks.LM3MockServletResponse;
import com.ltx.unittest.util.PropertyManager;

/**
 * <p>Title: BaseModelTest</p>
 *
 * <p>Description: BaseModel class for all the Model TestCase classes.</p>
 *
 * <p>Copyright: . (c) * </p>
 *
 * <p>Company: .</p>
 *
 * @author 
 * 
 * @version 1.0 (Initial Version �  July 2007)
 * @version 1.0 (Unit Test passed �  July 2007)
 *
 */
public abstract class BaseModelTest extends BaseTest {

	//Instance Variables
	public QueryModelBase model;

	static LM3MockServletRequest request = null;

	static LM3MockServletResponse response = null;

	static LM3MockServletContext ctxt = null;

	static RequestContextImpl context = null;

	static LM3MockServletConfig config = null;
	
	static LM3MockHttpSession session = null;
	
	private static ModelTypeMap MODEL_TYPE_MAP;

	public static DatasetModelExecutionContextImpl dbContext = null;

	static Connection connection = null;

	/**
	 * setUpBeforeClass()
	 * 
	 * @throws java.lang.Exception
	 */
	//@BeforeClass
	public void setUpBeforeClass(int dealInstitutionId) throws Exception {
		request = new LM3MockServletRequest();
		response = new LM3MockServletResponse();
		ctxt = new LM3MockServletContext();
		config = new LM3MockServletConfig();
		config.setServletContext(ctxt);
		session = new LM3MockHttpSession();
		session.setSessionContext(ctxt);
		request.setSession(session);
		SQLConnectionManagerImpl manager = new SQLConnectionManagerImpl();
		context = new RequestContextImpl("", ctxt, request, response);
		context.setSQLConnectionManager(manager);
		MODEL_TYPE_MAP = new ModelTypeMapImpl();
		ModelManager modelManager = new ModelManager(context, MODEL_TYPE_MAP);
		context.setModelManager(modelManager);
	    String defaultInstanceStateName =
	    modelManager.getDefaultModelInstanceName(SessionStateModel.class);
	    SessionStateModelImpl theSessionState = (SessionStateModelImpl)modelManager.getModel(
                            SessionStateModel.class,
                            defaultInstanceStateName,
                            true);
	   PageEntry pe =  new PageEntry();
	   pe.setDealInstitutionId(dealInstitutionId);
	   theSessionState.setCurrentPage(pe);
		dbContext = new DatasetModelExecutionContextImpl(
				ModelExecutionContext.OPERATION_RETRIEVE,
				DatasetModelExecutionContext.DATASET_OFFSET_BEGINNING,
				DatasetModelExecutionContext.DATASET_SIZE_ALL_PREFETCH);
		RequestManager.setRequestContext(context);

	}

	/**
	 * BaseModelTest(String modelName)
	 * Constructor that takes modelName.
	 * 
	 * @param modelName
	 */
	public BaseModelTest(String modelName, int dealInstitutionId){
		try {
			setUpBeforeClass(dealInstitutionId);
			Class clazz = Class.forName(modelName);
			Object obj = clazz.newInstance();
			model = (QueryModelBase) obj;
			model.setRequestContext(context);
			} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public BaseModelTest(String modelName){
		try {
			setUpBeforeClass(Integer.parseInt(PropertyManager.instance().getUtfDealInstitutionId()));
			Class clazz = Class.forName(modelName);
			Object obj = clazz.newInstance();
			model = (QueryModelBase) obj;
			model.setRequestContext(context);
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * createDataLoader()
	 * Creates DataLoader to load Data from the Corresponding ExcelSheet.
	 *
	 * @param <br>
	 *
	 * @return Loads the data from the Excel file - DataLoader <br>
	 * @throws Exception 
	 * 
	 */
	public DataLoader createDataLoader() {
		return DataLoaderFactory.getCachingExcelDataLoader();
	}

	/**
	 * loadTestDataObject(String modelName)
	 * loads the data from DDSteps XLSheet
	 * 
	 * @param modelName - Name of the Model
	 * 
	 * @return QueryModelBase - ModelImpl calss Object
	 */
	public final QueryModelBase loadTestDataObject(String modelName) {
		Class clazz;
		try {
			clazz = Class.forName(modelName);
			Object obj = clazz.newInstance();
			return (QueryModelBase) obj;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;

	}
	public SessionStateModel getSessionStateModel()
	{

	  String defaultInstanceStateName = RequestManager.getRequestContext()
	  .getModelManager().getDefaultModelInstanceName(SessionStateModel.class);
	  SessionStateModelImpl theSessionState = (SessionStateModelImpl) RequestManager
	  .getRequestContext().getModelManager().getModel(
		  SessionStateModel.class, defaultInstanceStateName, true, true);
	  return theSessionState;
	}
	
	public abstract void setupTestData() throws Exception;
	
	public abstract void tearDownTestData() throws Exception;

}
