package com.ltx.unittest.base;

import org.ddsteps.data.DataLoader;
import org.ddsteps.data.support.DataLoaderFactory;
import org.ddsteps.testcase.support.DDStepsExcelTestCase;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.MasterDeal;
import com.ltx.unittest.util.EntityTestUtil;

public abstract class BaseEntityTest extends BaseTest {

	protected DealEntity entity = null;
	
	public BaseEntityTest(String entityName) {
		super();
		entity = EntityTestUtil.getInstance().loadEntity(entityName);
		System.out.println(entity);
	}

	public DataLoader createDataLoader() {
		return DataLoaderFactory.getCachingExcelDataLoader();
	}

	public final DealEntity loadTestDataObject(String entityName) {
		return EntityTestUtil.getInstance().loadEntity(entityName);
	}
	
	public abstract void setupTestData() throws Exception;
	
	public abstract void tearDownTestData() throws Exception;
	
}
