package com.ltx.unittest.util;

import com.basis100.deal.calc.DealCalc;

public class CalcTestUtil {

	private static CalcTestUtil calcTestUtil;

	static {
		PropertyManager.instance().initiate();
	}

	public static CalcTestUtil getInstance() {
		if (calcTestUtil == null) {
			calcTestUtil = new CalcTestUtil();
		}
		return calcTestUtil;
	}

	public DealCalc loadCalc(String calcName) {
		Class clazz;
		try {
			clazz = Class.forName(calcName);
			Object obj = clazz.newInstance();
			return (DealCalc) obj;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
