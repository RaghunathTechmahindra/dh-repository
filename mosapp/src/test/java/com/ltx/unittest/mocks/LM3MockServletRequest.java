package com.ltx.unittest.mocks;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.mockobjects.servlet.MockHttpServletRequest;

public class LM3MockServletRequest extends MockHttpServletRequest implements HttpServletRequest{
    
    private HttpSession httpSession;
        
    public HttpSession getSession() {
        return httpSession;
    }
    
    public void setSession(HttpSession ahttpSession)
    {
        httpSession = ahttpSession;
    }
    
    public HttpSession getSession(boolean shouldCreate)
    {
        if (httpSession == null && shouldCreate)
        {
            httpSession = new LM3MockHttpSession();
        }
        
        return httpSession;
    }

    public StringBuffer getRequestURL() {

        return new StringBuffer();
    }

    public Map getParameterMap() {

		return null;
	}

	public void setCharacterEncoding(String encoding) {
	}
}
