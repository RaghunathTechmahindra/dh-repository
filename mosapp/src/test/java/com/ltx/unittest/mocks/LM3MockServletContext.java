package com.ltx.unittest.mocks;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletContext;

import com.mockobjects.servlet.MockServletContext;


public class LM3MockServletContext extends MockServletContext implements ServletContext {
	
	private HashMap attribute;
	
	public LM3MockServletContext()
	{
		super();
		attribute = new HashMap();
	}
	
	public String getRealPath(String str)
	{
		return "D:/LM3-Unit-test-Proj/WEB-INF/data";
	}

	public Object getAttribute(String name) {
		return attribute.get(name);
	}
	
	
	public void setAttribute(String key, Object value) {
		attribute.put(key, value);
	}

}
