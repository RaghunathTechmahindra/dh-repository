package com.ltx.unittest.mocks;

import com.mockobjects.servlet.MockHttpSession;
import java.util.HashMap;
import javax.servlet.ServletContext;

public class LM3MockHttpSession extends MockHttpSession
{

    private ServletContext servletContext;
    private HashMap attributeList;

    public LM3MockHttpSession()
    {
        attributeList = new HashMap();
    }

    public ServletContext getServletContext()
    {
        return servletContext;
    }

    public void setSessionContext(ServletContext aServletContext)
    {
        servletContext = aServletContext;
    }

    public Object getAttribute(Object key)
    {
        return attributeList.get(key);
    }

    public Object getAttribute(String key)
    {
        return attributeList.get(key);
    }

    public void setAttribute(Object key, Object value)
    {
        attributeList.put(key, value);
    }
}