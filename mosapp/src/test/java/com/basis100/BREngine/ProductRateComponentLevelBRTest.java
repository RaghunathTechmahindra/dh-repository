package com.basis100.BREngine;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.ltx.unittest.base.BaseBREEntityTest;
import com.ltx.unittest.base.BaseBRETest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: ProductRateComponentLevelBRTest</p>
 *
 * <p>Description: Unit test.</p>
 *
 * @author MCM Implementation Team
 * @version 1.0 11-Aug-2008 XS_1.7 Initial version
 * 
 */
public class ProductRateComponentLevelBRTest extends BaseBRETest {
  
   private Deal deal;	
   private MtgProd mtgProd;

   private MtgProd mtgProdDataEntity;  
   private Deal dealDataEntity;  	


   public ProductRateComponentLevelBRTest() {
      	  super();
	  dealDataEntity = (Deal) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Deal");
	  mtgProdDataEntity = (MtgProd) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MtgProd");
     }
   
   /**
    * <p>Description: Set up test data.</p>
    *
    * @version 1.0 11-Aug-2008 XS_1.7 Initial version
    * 
    */
   public void setupTestData() throws Exception {
  		MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
          deal = getMasterDeal().create(masterDealPK);
          dealPK = (DealPK) deal.getPk();
          deal.setScenarioNumber(1);
          deal.setCopyType("G");
		  deal.setDenialReasonId(dealDataEntity.getDenialReasonId());
  		deal.ejbStore();
  		setCopyId(deal.getCopyId());
  		System.out.println("DEAL ENTITY CREATED");
      // *****************Test related data setup starts here******************/

	  // Create MtgProd Object
        mtgProd = (MtgProd) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MtgProd");
        
        MtgProdPK mtgProdPK = new MtgProdPK(deal.getMtgProdId());
        mtgProd.findByPrimaryKey(mtgProdPK);
        mtgProd.setMtgProdId(mtgProdDataEntity.getMtgProdId());
        mtgProd.setComponentEligibleFlag(mtgProdDataEntity.getComponentEligibleFlag());
         mtgProd.ejbStore();
        System.out.println("MTGPORD ENTITY CREATED");
	  
		  
	  // *****************Test related data setup ends here******************/
   }

   /**
    * <p>Description: Test business rules.</p>
    *
    * @version 1.0 11-Aug-2008 XS_1.7 Initial version
    * 
    */
   public void testBusinessRule() throws Exception {
      setupTestData();

     BusinessRuleExecutor brExec = new BusinessRuleExecutor();
     srk.getExpressState().setDealInstitutionId(getDealInstitutionId()); 
      PassiveMessage pmProp = new PassiveMessage();
        
      brExec.BREValidator((DealPK) deal.getPk(), srk, pmProp, "IC-819");
      
      assertEquals(1, pmProp.getNumMessages());

   }

   /**
    * <p>Description: Clean test data.</p>
    *
    * @version 1.0 11-Aug-2008 XS_1.7 Initial version
    * 
    */
   public void tearDownTestData() throws Exception {

      if (deal != null) {
         deal.dcm = null;
      }
      try {
          System.out.println("TEAR DOWN STARTED");
	     getMasterDeal().dcm = null;
         getMasterDeal().removeDeal();
      } catch (RemoteException e) {
         e.printStackTrace();
      }
      System.out.println("ejb remove done");
   }

   public static Test suite() {
      return (Test) DDStepsSuiteFactory.createSuite(ProductRateComponentLevelBRTest.class);
   }

  public Deal getDealDataEntity() {
      return dealDataEntity;
   }
  
	public MtgProd getMtgProdDataEntity() {
      return mtgProdDataEntity;
   }



}
