package com.basis100.BREngine;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.base.BaseBRETest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: ComponentMortgagePropertyTaxBRTest</p>
 *
 * <p>Description: Unit test.</p>
 *
 * @author MCM Implementation Team
 * @version 1.0 23-Jul-2008 XS_2.40 Initial version
 * 
 */
public class ComponentMortgagePropertyTaxBRTest extends BaseBRETest {
  
   private EscrowPayment escrowpayment;
   private MtgProd mtgProd;
   private Component component;
   private Component componentOne;
   private ComponentMortgage componentMortgage;
   private ComponentLOC componentLOC;  

   private EscrowPayment escrowpaymentDataEntity;
   private MtgProd mtgProdDataEntity;  
   private Component componentDataEntity;
   private Component componentDataEntityOne;
   private ComponentMortgage componentMortgageDataEntity;
   private ComponentLOC componentLOCDataEntity; 
  


   public ComponentMortgagePropertyTaxBRTest() {
      	  super();
    escrowpaymentDataEntity = (EscrowPayment) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.EscrowPayment");
    mtgProdDataEntity = (MtgProd) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MtgProd");
	  componentDataEntity = (Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
    componentDataEntityOne = (Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
	  componentMortgageDataEntity = (ComponentMortgage) EntityTestUtil.getInstance().loadEntity( "com.basis100.deal.entity.ComponentMortgage");
    componentLOCDataEntity = (ComponentLOC) EntityTestUtil.getInstance().loadEntity( "com.basis100.deal.entity.ComponentLOC");
     }
   
   /**
    * <p>Description: Set up test data.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void setupTestData() throws Exception {
  		MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
          deal = getMasterDeal().create(masterDealPK);
          dealPK = (DealPK) deal.getPk();
          deal.setScenarioNumber(1);
          deal.setCopyType("G");
  		deal.ejbStore();
  		setCopyId(deal.getCopyId());
  		System.out.println("DEAL ENTITY CREATED");
      // *****************Test related data setup starts here******************/

      // Create EscrowPayment Object
      escrowpayment = (EscrowPayment) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.EscrowPayment");
      escrowpayment.create(dealPK);
      escrowpayment.setEscrowPaymentId(escrowpaymentDataEntity.getEscrowPaymentId());
      escrowpayment.setEscrowPaymentAmount(escrowpaymentDataEntity.getEscrowPaymentAmount());
      escrowpayment.ejbStore();
      System.out.println("EscrowPayment ENTITY CREATED");
      
      
      // Create MtgProd Object
        mtgProd = (MtgProd) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MtgProd");
        
        MtgProdPK mtgProdPK = new MtgProdPK(deal.getMtgProdId());
        mtgProd.findByPrimaryKey(mtgProdPK);
        mtgProd.setMtgProdId(mtgProdDataEntity.getMtgProdId());
        mtgProd.setComponentEligibleFlag(mtgProdDataEntity.getComponentEligibleFlag());
        mtgProd.ejbStore();
        System.out.println("MTGPORD ENTITY CREATED");
        // Create Component Object
        component = (Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        component.create(dealPK.getId(), dealPK.getCopyId(),componentDataEntity.getComponentTypeId(), 
                componentDataEntity.getMtgProdId());
        component.setPricingRateInventoryId(componentDataEntity.getPricingRateInventoryId());
        component.setRepaymentTypeId( componentDataEntity.getRepaymentTypeId());
        component.ejbStore();
        System.out.println("COMPONENT ENTITY CREATED");
        System.out.println("COMPONENTID MTG == "+component.getComponentId());
               
        // Create Component Mortgage Object
        componentMortgage = (ComponentMortgage) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentMortgage");

        componentMortgage.create(component.getComponentId(), component.getCopyId());
        
        componentMortgage.setPaymentFrequencyId(componentMortgageDataEntity.getPaymentFrequencyId());
        componentMortgage.setPrePaymentOptionsId(componentMortgageDataEntity.getPrePaymentOptionsId());
        componentMortgage.setPrivilegePaymentId(componentMortgageDataEntity.getPrivilegePaymentId());
        componentMortgage.setCashBackAmountOverride(componentMortgageDataEntity.getCashBackAmountOverride());
        componentMortgage.setRateLock(componentMortgageDataEntity.getRateLock());
        componentMortgage.setPropertyTaxAllocateFlag(componentMortgageDataEntity.getPropertyTaxAllocateFlag());
        componentMortgage.ejbStore();
    
        System.out.println("CM ENTITY CREATED");
		  
     
        // Create ComponentOne Object
        componentOne = (Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        componentOne.create(dealPK.getId(), dealPK.getCopyId(),componentDataEntity.getComponentTypeId(), 
                componentDataEntity.getMtgProdId());
        componentOne.setPricingRateInventoryId(componentDataEntity.getPricingRateInventoryId());
        componentOne.setRepaymentTypeId( componentDataEntity.getRepaymentTypeId());
        componentOne.ejbStore();
        System.out.println("COMPONENT ENTITY CREATED");
        System.out.println("COMPONENTID LOC == "+componentOne.getComponentId());
 
        // Create Component LOC Object
        componentLOC = (ComponentLOC) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLOC");

        // Create Component LOC Object
        componentLOC = (ComponentLOC) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLOC");

        componentLOC.create(component.getComponentId(), component.getCopyId());
        componentLOC.setPaymentFrequencyId(componentLOCDataEntity.getPaymentFrequencyId());
        componentLOC.setPrePaymentOptionsId(componentLOCDataEntity.getPrePaymentOptionsId());
        componentLOC.setPrivilegePaymentId(componentLOCDataEntity.getPrivilegePaymentId());
        componentLOC.setPropertyTaxAllocateFlag(componentLOCDataEntity.getPropertyTaxAllocateFlag());
        componentLOC.ejbStore();
        System.out.println("CL ENTITY CREATED");
	  // *****************Test related data setup ends here******************/
   }

   /**
    * <p>Description: Test business rules.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void testBusinessRule() throws Exception {
      setupTestData();

     BusinessRuleExecutor brExec = new BusinessRuleExecutor();
     srk.getExpressState().setDealInstitutionId(getDealInstitutionId()); 
      PassiveMessage pmProp = new PassiveMessage();
      
      brExec.BREValidator((DealPK) deal.getPk(), srk, pmProp, "DMC-INIT");
      brExec.BREValidator((DealPK) deal.getPk(), srk, pmProp, "DMC-002");
      
      assertEquals(1, pmProp.getNumMessages());


   }

   /**
    * <p>Description: Clean test data.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void tearDownTestData() throws Exception {

      if (deal != null) {
         deal.dcm = null;
      }
      try {
          System.out.println("TEAR DOWN STARTED");
          componentMortgage.ejbRemove();
          componentOne.ejbRemove();
          component.ejbRemove();
	     getMasterDeal().dcm = null;
         getMasterDeal().removeDeal();
      } catch (RemoteException e) {
         e.printStackTrace();
      }
      System.out.println("ejb remove done");
   }

   public static Test suite() {
      return (Test) DDStepsSuiteFactory.createSuite(ComponentMortgagePropertyTaxBRTest.class);
   }


  public EscrowPayment getescrowpaymentDataEntity() {
      return escrowpaymentDataEntity;
   }
   public MtgProd getMtgProdDataEntity() {
      return mtgProdDataEntity;
   }

  	public Component getComponentDataEntity() {
	      return componentDataEntity;
	   }
  	
    public Component getComponentDataEntityOne() {
        return componentDataEntityOne;
     }
    
	public ComponentMortgage getComponentMortgageDataEntity() {
	  return componentMortgageDataEntity;
   }

  public ComponentLOC getComponentLOCDataEntity() {
      return componentLOCDataEntity;
     }

}
