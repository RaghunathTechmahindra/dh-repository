package com.basis100.BREngine;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.base.BaseBRETest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: ComponentLOCFirstPaymentDateBRTest</p>
 *
 * <p>Description: Unit test.</p>
 *
 * @author MCM Implementation Team
 * @version 1.0 23-Jul-2008 XS_2.40 Initial version
 * 
 */
public class ComponentLOCFirstPaymentDateBRTest extends BaseBRETest {
  	
 
   private MtgProd mtgProd;
   private Component component;
   private ComponentLOC componentLOC;   

   private Deal dealDataEntity;  
   private MtgProd mtgProdDataEntity;  
   private Component componentDataEntity;
   private ComponentLOC componentLOCDataEntity;   
  


   public ComponentLOCFirstPaymentDateBRTest() {
      	  super();
      dealDataEntity = (Deal) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Deal");
      mtgProdDataEntity = (MtgProd) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MtgProd");
	  componentDataEntity = (Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
	  componentLOCDataEntity = (ComponentLOC) EntityTestUtil.getInstance().loadEntity( "com.basis100.deal.entity.ComponentLOC");

     }
   
   /**
    * <p>Description: Set up test data.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void setupTestData() throws Exception {
  		MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
          deal = getMasterDeal().create(masterDealPK);
          dealPK = (DealPK) deal.getPk();
          deal.setScenarioNumber(1);
          deal.setCopyType("G");
          deal.setEstimatedClosingDate(dealDataEntity.getEstimatedClosingDate());
          deal.ejbStore();
  		setCopyId(deal.getCopyId());
  		System.out.println("DEAL ENTITY CREATED");
      // *****************Test related data setup starts here******************/

	  // Create MtgProd Object
        mtgProd = (MtgProd) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MtgProd");
        
        MtgProdPK mtgProdPK = new MtgProdPK(deal.getMtgProdId());
        mtgProd.findByPrimaryKey(mtgProdPK);
        mtgProd.setMtgProdId(mtgProdDataEntity.getMtgProdId());
        mtgProd.setComponentEligibleFlag(mtgProdDataEntity.getComponentEligibleFlag());
        mtgProd.ejbStore();
        System.out.println("MTGPORD ENTITY CREATED");
        // Create Component Object
        component = (Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        component.create(dealPK.getId(), dealPK.getCopyId(),componentDataEntity.getComponentTypeId(), 
                componentDataEntity.getMtgProdId());
        component.setPricingRateInventoryId(componentDataEntity.getPricingRateInventoryId());
        component.setRepaymentTypeId( componentDataEntity.getRepaymentTypeId());
        component.ejbStore();
        System.out.println("COMPONENT ENTITY CREATED");
        System.out.println("COMPONENTID == "+component.getComponentId());
        // Create Component LOC Object
        componentLOC = (ComponentLOC) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLOC");

        componentLOC.create(component.getComponentId(), component.getCopyId());
        componentLOC.setPaymentFrequencyId(componentLOCDataEntity.getPaymentFrequencyId());
        componentLOC.setPrePaymentOptionsId(componentLOCDataEntity.getPrePaymentOptionsId());
        componentLOC.setPrivilegePaymentId(componentLOCDataEntity.getPrivilegePaymentId());
        componentLOC.setFirstPaymentDate(componentLOCDataEntity.getFirstPaymentDate());
        componentLOC.ejbStore();
        System.out.println("CM ENTITY CREATED");
		  
	  // *****************Test related data setup ends here******************/
   }

   /**
    * <p>Description: Test business rules.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void testBusinessRule() throws Exception {
      setupTestData();

     BusinessRuleExecutor brExec = new BusinessRuleExecutor();
     srk.getExpressState().setDealInstitutionId(getDealInstitutionId()); 
      PassiveMessage pmProp = new PassiveMessage();
      brExec.setCurrentComponent(component.getComponentId());
      
      brExec.BREValidator((DealPK) deal.getPk(), srk, pmProp, "DEC-004");
      
      assertEquals(1, pmProp.getNumMessages());

   }

   /**
    * <p>Description: Clean test data.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void tearDownTestData() throws Exception {

      if (deal != null) {
         deal.dcm = null;
      }
      try {
          System.out.println("TEAR DOWN STARTED");
          componentLOC.ejbRemove();
          component.ejbRemove();
	     getMasterDeal().dcm = null;
         getMasterDeal().removeDeal();
      } catch (RemoteException e) {
         e.printStackTrace();
      }
      System.out.println("ejb remove done");
   }

   public static Test suite() {
      return (Test) DDStepsSuiteFactory.createSuite(ComponentLOCFirstPaymentDateBRTest.class);
   }

   public Deal getDealDataEntity() {
      return dealDataEntity;
   }
	
	public MtgProd getMtgProdDataEntity() {
      return mtgProdDataEntity;
   }

  	public Component getComponentDataEntity() {
	      return componentDataEntity;
	   }

	public ComponentLOC getComponentLOCDataEntity() {
	  return componentLOCDataEntity;
   }



}
