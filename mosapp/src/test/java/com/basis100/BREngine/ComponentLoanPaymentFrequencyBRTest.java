package com.basis100.BREngine;

import junit.framework.Test;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.MtgProdPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.base.BaseBRETest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: ComponentMortgagePropertyEligibleTaxBRTest</p>
 *
 * <p>Description: Unit test.</p>
 *
 * @author MCM Implementation Team
 * @version 1.0 23-Jul-2008 XS_2.40 Initial version
 * 
 */
public class ComponentLoanPaymentFrequencyBRTest extends BaseBRETest {
  
   private MtgProd mtgProd;
   private Component component;
   private ComponentLoan componentLoan;   

   private MtgProd mtgProdDataEntity;  
   private Component componentDataEntity;
   private ComponentLoan componentLoanDataEntity;   
  


   public ComponentLoanPaymentFrequencyBRTest() {
      	  super();
	  mtgProdDataEntity = (MtgProd) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MtgProd");
	  componentDataEntity = (Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
	  componentLoanDataEntity = (ComponentLoan) EntityTestUtil.getInstance().loadEntity( "com.basis100.deal.entity.ComponentLoan");

     }
   
   /**
    * <p>Description: Set up test data.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void setupTestData() throws Exception {
  		MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
          deal = getMasterDeal().create(masterDealPK);
          dealPK = (DealPK) deal.getPk();
          deal.setScenarioNumber(1);
          deal.setCopyType("G");
  		deal.ejbStore();
  		setCopyId(deal.getCopyId());
  		System.out.println("DEAL ENTITY CREATED");
      // *****************Test related data setup starts here******************/

	  // Create MtgProd Object
        mtgProd = (MtgProd) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.MtgProd");
        
        MtgProdPK mtgProdPK = new MtgProdPK(deal.getMtgProdId());
        mtgProd.findByPrimaryKey(mtgProdPK);
        mtgProd.setMtgProdId(mtgProdDataEntity.getMtgProdId());
        mtgProd.setComponentEligibleFlag(mtgProdDataEntity.getComponentEligibleFlag());
        mtgProd.ejbStore();
        System.out.println("MTGPORD ENTITY CREATED");
        // Create Component Object
        component = (Component) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Component");
        component.create(dealPK.getId(), dealPK.getCopyId(),componentDataEntity.getComponentTypeId(), 
                componentDataEntity.getMtgProdId());
        component.setPricingRateInventoryId(componentDataEntity.getPricingRateInventoryId());
        component.setRepaymentTypeId( componentDataEntity.getRepaymentTypeId());
        component.ejbStore();
        System.out.println("COMPONENT ENTITY CREATED");
        System.out.println("COMPONENTID == "+component.getComponentId());
        // Create Component Mortgage Object
        componentLoan = (ComponentLoan) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.ComponentLoan");

        componentLoan.create(component.getComponentId(), component.getCopyId());
        componentLoan.setPaymentFrequencyId(componentLoanDataEntity.getPaymentFrequencyId());
         componentLoan.ejbStore();
        System.out.println("CL ENTITY CREATED");
		  
	  // *****************Test related data setup ends here******************/
   }

   /**
    * <p>Description: Test business rules.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void testBusinessRule() throws Exception {
      setupTestData();

     BusinessRuleExecutor brExec = new BusinessRuleExecutor();
     srk.getExpressState().setDealInstitutionId(getDealInstitutionId());
      PassiveMessage pmProp = new PassiveMessage();
      System.out.println("NO OF MESSAGE ="+pmProp.getNumMessages());
      brExec.setCurrentComponent(component.getComponentId());
      System.out.println("BEFORE CALLING COMPID = "+component.getComponentId());
      brExec.BREValidator((DealPK) deal.getPk(), srk, pmProp, "DEC-008");
      
      assertEquals(1, pmProp.getNumMessages());
      
   }

   /**
    * <p>Description: Clean test data.</p>
    *
    * @version 1.0 23-Jul-2008 XS_2.40 Initial version
    * 
    */
   public void tearDownTestData() throws Exception {

      if (deal != null) {
         deal.dcm = null;
      }
      try {
          System.out.println("TEAR DOWN STARTED");
          componentLoan.ejbRemove();
          component.ejbRemove();
	     getMasterDeal().dcm = null;
         getMasterDeal().removeDeal();
      } catch (RemoteException e) {
         e.printStackTrace();
      }
      System.out.println("ejb remove done");
   }

   public static Test suite() {
      return (Test) DDStepsSuiteFactory.createSuite(ComponentLoanPaymentFrequencyBRTest.class);
   }


	public MtgProd getMtgProdDataEntity() {
      return mtgProdDataEntity;
   }

  	public Component getComponentDataEntity() {
	      return componentDataEntity;
	   }

	public ComponentLoan getComponentLoanDataEntity() {
	  return componentLoanDataEntity;
   }



}
