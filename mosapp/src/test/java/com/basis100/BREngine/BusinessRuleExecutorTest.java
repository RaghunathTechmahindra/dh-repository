package com.basis100.BREngine;

import java.io.IOException;
import java.util.Vector;

import mosApp.MosSystem.PageEntry;
import mosApp.MosSystem.SessionStateModelImpl;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.FXDBTestCase;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.validation.BusinessRuleExecutor;

/**
 * Test Class for BusinessRuleExecutor from core
 */
public class BusinessRuleExecutorTest extends FXDBTestCase{
	
	// The logger
    private final static Logger _logger = LoggerFactory.getLogger(BusinessRuleExecutorTest.class);
    
    //Initial data to be loaded before each test case.
	private final static String INIT_XML_DATA = "BusinessRuleExecutorDataSet.xml";
	private IDataSet dataSetTest;
	
	/**
     * Constructor function
     */
    public BusinessRuleExecutorTest(String name) throws IOException, DataSetException{
    	super(name);
    	dataSetTest = new FlatXmlDataSet(this.getClass().getResource(BusinessRuleExecutor.class.getSimpleName() + "DataSetTest.xml"));
    }
    
    @Override
	protected IDataSet getDataSet() throws Exception {
    	return new FlatXmlDataSet(this.getClass().getResource(INIT_XML_DATA));
	}
    
    protected DatabaseOperation getSetUpOperation() throws Exception
    {
        return DatabaseOperation.INSERT;
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {
        return DatabaseOperation.DELETE;
    }
    
       
    /*
     * Test method for BREValidator (all overloaded methods) 
     */
    @Test
    public void testBREValidator() throws DataSetException
    {
    	_logger.info("start testBREValidator");
    	
    	ITable testBREValidator = dataSetTest.getTable("testBREValidator");
 	    int dealId=Integer.parseInt((String)testBREValidator.getValue(0,"dealId"));
 	    int copyId=Integer.parseInt((String)testBREValidator.getValue(0,"copyId"));
 	    int institutionProfileId=Integer.parseInt((String)testBREValidator.getValue(0,"institutionProfileId"));
 	    int langId=Integer.parseInt((String)testBREValidator.getValue(0,"langId"));
 	    int userTypeId=Integer.parseInt((String)testBREValidator.getValue(0,"userTypeId"));
 	    String rulePrefix1 = (String)testBREValidator.getValue(0,"rulePrefix1");
 	    String rulePrefix2 = (String)testBREValidator.getValue(0,"rulePrefix2");	
    	
    	BusinessRuleExecutor brexecutor = new BusinessRuleExecutor();
    	
    	PageEntry pe = new PageEntry();
    	pe.setDealInstitutionId(institutionProfileId);
    	pe.setPageDealId(dealId);
    	pe.setPageDealCID(copyId);
    	
    	SessionStateModelImpl theSession = new SessionStateModelImpl();
    	theSession.setLanguageId(langId);
    	theSession.setSessionUserType(userTypeId);
    	
    	srk.getExpressState().setDealInstitutionId(institutionProfileId);
    	
    	
    	Vector<String> rulePrefixList1 = new Vector<String>();
    	rulePrefixList1.add(rulePrefix1);
    	
    	Vector<String> rulePrefixList2 = new Vector<String>();
    	rulePrefixList2.add(rulePrefix2);
        
        boolean optimal = false;
        
        DealPK dealPK = new DealPK(dealId, copyId);
    	
    	try
    	{
    		// for Rule Prefix 1 when no passive message generates
    		PassiveMessage pm = brexecutor.BREValidator(theSession, pe, srk, null, rulePrefix1);
    		assert pm == null;
    		
    		PassiveMessage pm1 = brexecutor.BREValidator(theSession, pe, srk, null, rulePrefix1, optimal);
    		assert pm1 == null;
    		
    		PassiveMessage pm2 = brexecutor.BREValidator(theSession, pe, srk, null, rulePrefixList1);
    		assert pm2 == null;
    		
    		PassiveMessage pm3 = brexecutor.BREValidator(theSession, pe, srk, null, rulePrefixList1, optimal);
    		assert pm3 == null;
    		
    		PassiveMessage pm4 = brexecutor.BREValidator(dealPK, srk, null, rulePrefix1, langId);
    		assert pm4 == null;
    		
    		PassiveMessage pm5 = brexecutor.BREValidator(dealPK, srk, null, rulePrefix1);
    		assert pm5 == null;
    		
    		PassiveMessage pm6 = brexecutor.BREValidator(dealPK, srk, null, rulePrefix1, optimal, langId);
    		assert pm6 == null;
    		
    		PassiveMessage pm7 = brexecutor.BREValidator(dealPK, srk, null, rulePrefix1, optimal);
    		assert pm7 == null;
    		
    		PassiveMessage pm8 = brexecutor.BREValidator(dealPK, srk, null, rulePrefixList1, langId);
    		assert pm8 == null;
    		
    		PassiveMessage pm9 = brexecutor.BREValidator(dealPK, srk, null, rulePrefixList1);
    		assert pm9 == null;
    		
    		
    		// for Rule Prefix 1 when passive message generates
    		PassiveMessage pmsg = brexecutor.BREValidator(theSession, pe, srk, null, rulePrefix2);
    		assert pmsg.getMsgs() != null;
    		
    		PassiveMessage pmsg1 = brexecutor.BREValidator(theSession, pe, srk, null, rulePrefix2, optimal);
    		assert pmsg1.getMsgs() != null;
    		
    		PassiveMessage pmsg2 = brexecutor.BREValidator(theSession, pe, srk, null, rulePrefixList2);
    		assert pmsg2.getMsgs() != null;
    		
    		PassiveMessage pmsg3 = brexecutor.BREValidator(theSession, pe, srk, null, rulePrefixList2, optimal);
    		assert pmsg3.getMsgs() != null;
    		
    		PassiveMessage pmsg4 = brexecutor.BREValidator(dealPK, srk, null, rulePrefix2, langId);
    		assert pmsg4.getMsgs() != null;
    		
    		PassiveMessage pmsg5 = brexecutor.BREValidator(dealPK, srk, null, rulePrefix2);
    		assert pmsg5.getMsgs() == null;
    		
    		PassiveMessage pmsg6 = brexecutor.BREValidator(dealPK, srk, null, rulePrefix2, optimal, langId);
    		assert pmsg6.getMsgs() != null;
    		
    		PassiveMessage pmsg7 = brexecutor.BREValidator(dealPK, srk, null, rulePrefix2, optimal);
    		assert pmsg7.getMsgs() != null;
    		
    		PassiveMessage pmsg8 = brexecutor.BREValidator(dealPK, srk, null, rulePrefixList2, langId);
    		assert pmsg8.getMsgs() != null;
    		
    		PassiveMessage pmsg9 = brexecutor.BREValidator(dealPK, srk, null, rulePrefixList2);
    		assert pmsg9.getMsgs() != null;
    	}
    	catch (Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	
    	_logger.info("end testBREValidator");
    }
}
