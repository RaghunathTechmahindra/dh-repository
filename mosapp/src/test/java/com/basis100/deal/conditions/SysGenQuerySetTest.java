/*
 * @(#)SysGenQuerySetTest.java     Sep 11, 2007
 * 
 * Copyright (C) 2007 Filogix Limited Partnership. All rights reserved.
 */


package com.basis100.deal.conditions;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * @author JDai
 *
 */
public class SysGenQuerySetTest {
    
    //The log
    private static final Log _log = LogFactory.getLog(SysGenQuerySetTest.class);

    //The SysGenQuerySet entity
    private SysGenQuerySet loc;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        loc = new SysGenQuerySet();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /*
     * test thre result set
     */
    @Test
    public void testSysGenSet() throws Exception {
        
        Iterator loit = loc.iterator();
        while (loit.hasNext()) {
            //list the SysGenQuery Set Result
            _log.info(loit.next());
        }       
    }
}
