package com.filogix.express.web.rc.lpr.adapter;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.JSONProductTypeConverter;
import com.filogix.express.web.rc.lpr.dao.ComponentEligibeCheckDao;
import com.filogix.express.web.rc.lpr.dao.ProductTypeDao;

public class ComponentEligibleCheckAdapterTest {
    
    ComponentEligibleCheckAdapter adapter;
    SessionResourceKit srk;
    LPRRequestObject req;
    LPRResponseObject res;
    @Before
    public void setUp() throws Exception {
        adapter = new ComponentEligibleCheckAdapter();
        srk = new SessionResourceKit("test");
        req = new LPRRequestObject();
        res = new LPRResponseObject();
        adapter.setPricingLogic(new MockPricingLogic());

        req.put("productId", 1);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testProcessJSON() {
        
        adapter.process(srk, req, res);
        
        String actual = res.get("componentEligibleFlag").toString();
        String expected = "{compEligibleFlg=Y}";
        
        Assert.assertEquals(expected, actual);
        
    }

    private static class MockPricingLogic implements ComponentEligibeCheckDao {

        public String getComponentEligibleFlag() {
            return "Y";
        }

        public void load(SessionResourceKit srk, LPRRequestObject request)
                throws Exception {
            //Dummy
        }
    }
}
