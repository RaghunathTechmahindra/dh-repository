package com.filogix.express.web.rc.lpr.adapter;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.mtgrates.IPricingLogicProduct;
import com.basis100.mtgrates.ProductInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.JSONProductConverter;
import com.filogix.express.web.rc.lpr.converter.LegacyProductConverter;

public class PricingLogicProductAdapterTest {
    
    PricingLogicProductAdapter adapter;
    SessionResourceKit srk;
    LPRRequestObject req;
    LPRResponseObject res;
    @Before
    public void setUp() throws Exception {
        adapter = new PricingLogicProductAdapter();
        srk = new SessionResourceKit("test");
        req = new LPRRequestObject();
        res = new LPRResponseObject();
        adapter.setPricingLogic(new MockPricingLogic());
        
        req.put("lenderProfileId", 1);
        req.put("componentTypeId", 1);
        req.put("dealId", 1);
        req.put("dealCopyId", 1);
        
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testProcessJSON() {
        
        adapter.setConverter(new JSONProductConverter());
        adapter.process(srk, req, res);
        
        String actual = res.get("products").toString();
        String expected = "[{prodDesc=?100?, prodId=100}, {prodDesc=?200?, prodId=200}]";
        
        Assert.assertEquals(expected, actual);
        
    }

    @Test
    public void testProcessLegacy() {
        
        adapter.setConverter(new LegacyProductConverter());
        adapter.process(srk, req, res);
        
        String actual = res.get("productString").toString();
        String expected = "'?100?','100'^'?200?','200'^";
        
        Assert.assertEquals(expected, actual);
        
    }
    
    private static class MockPricingLogic implements IPricingLogicProduct {

        public List<ProductInfo> getLenderProducts(SessionResourceKit srk,
                int lenderProfileId, int componentTypeId, int dealId,
                int dealCopyId, boolean isUmbrella) {
            
            List<ProductInfo> list = new ArrayList<ProductInfo>();
            ProductInfo info = new ProductInfo();
            info.setMtgProdId(100);
            info.setRatecode("XXX");
            info.setRepaymenttypeid(1000);
            list.add(info);
            
            info = new ProductInfo();
            info.setMtgProdId(200);
            info.setRatecode("YYY");
            info.setRepaymenttypeid(2000);
            list.add(info);
            
            return list;
        }
    }
}
