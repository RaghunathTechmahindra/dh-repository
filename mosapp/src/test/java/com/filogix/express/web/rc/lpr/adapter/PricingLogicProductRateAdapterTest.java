package com.filogix.express.web.rc.lpr.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.mtgrates.IPricingLogicProductRate;
import com.basis100.mtgrates.RateInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.JSONProductRateConverter;
import com.filogix.express.web.rc.lpr.converter.LegacyProductRateConverter;

public class PricingLogicProductRateAdapterTest {
    
    PricingLogicProductRateAdapter adapter;
    SessionResourceKit srk;
    LPRRequestObject req;
    LPRResponseObject res;
    @Before
    public void setUp() throws Exception {
        adapter = new PricingLogicProductRateAdapter();
        srk = new SessionResourceKit("test");
        req = new LPRRequestObject();
        res = new LPRResponseObject();
        adapter.setPricingLogic(new MockPricingLogic());
        
        req.put("lenderProfileId", 1);
        req.put("componentTypeId", 1);
        req.put("dealId", 1);
        req.put("dealCopyId", 1);
        req.put("productId", 1);
        
        
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testProcessJSON() {
        
        adapter.setConverter(new JSONProductRateConverter());
        adapter.process(srk, req, res);
        
        String actual = res.get("rates").toString();
        String expected = "[{postedRate=10.1, rateDesc=10.1% | Max Discount | 6.0% Jun 21 3909 | 00:00 EST, rateId=15}, {postedRate=30.1, rateDesc=30.1% | Cap Over Posted | 0.0% Jul 21 3912 | 00:00 EST, rateId=10}]";
        
        Assert.assertEquals(expected, actual);
        
    }

    @Test
    public void testProcessLegacy() {
        
        adapter.setConverter(new LegacyProductRateConverter());
        adapter.process(srk, req, res);
        
        String actual = res.get("lenderProductRateString").toString();
        String expected = "'10.1% | Max Discount | 6.0% Jun 21 3909 | 00:00 EST','15'^'30.1% | Cap Over Posted | 0.0% Jul 21 3912 | 00:00 EST','10'^";
        
        Assert.assertEquals(expected, actual);
        
    }
    
    private static class MockPricingLogic implements IPricingLogicProductRate {

        public List<RateInfo> getLenderProductRates(SessionResourceKit srk,
                int productId, int dealId, int dealCopyId) {

            
            List<RateInfo> list = new ArrayList<RateInfo>();

            RateInfo info = new RateInfo();
            info.setEffectiveDate(new Date(2009,5,21)); //2009-06-21
            info.setMaxDiscount(6.0);
            info.setRateId(15);
            info.setPostedRate(10.1);
            
            list.add(info);
            
            info = new RateInfo();
            info.setEffectiveDate(new Date(2012,6,21)); //2012-07-21
            //info.setMaxDiscount(1.0);
            info.setRateId(10);
            info.setPostedRate(30.1);
            list.add(info);
            
            return list;
        }
    }
}
