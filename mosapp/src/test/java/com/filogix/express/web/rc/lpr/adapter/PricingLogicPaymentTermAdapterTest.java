package com.filogix.express.web.rc.lpr.adapter;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.mtgrates.IPricingLogicPaymentTerm;
import com.basis100.mtgrates.PaymentTermInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.JSONPaymentTermConverter;
import com.filogix.express.web.rc.lpr.converter.LegacyPaymentTermConverter;

public class PricingLogicPaymentTermAdapterTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testProcessJSON() {
        PricingLogicPaymentTermAdapter adapter = new PricingLogicPaymentTermAdapter();
        adapter.setConverter(new JSONPaymentTermConverter());
        adapter.setPricingLogic(new MockPricingLogic());
        
        SessionResourceKit srk = new SessionResourceKit("test");
        LPRRequestObject req = new LPRRequestObject();
        LPRResponseObject res = new LPRResponseObject();
        req.put("componentTypeId", 1);
        
        adapter.process(srk, req, res);
        
        String actual = res.get("paymentTerms").toString();
        String expected = "[{paymentTerm=?1000?, productId=100, paymentTermId=1000}, {paymentTerm=?2000?, productId=200, paymentTermId=2000}]";
        
        Assert.assertEquals(actual, expected);
        
    }

    @Test
    public void testProcessLegacy() {
        PricingLogicPaymentTermAdapter adapter = new PricingLogicPaymentTermAdapter();
        adapter.setConverter(new LegacyPaymentTermConverter());
        adapter.setPricingLogic(new MockPricingLogic());
        
        SessionResourceKit srk = new SessionResourceKit("test");
        LPRRequestObject req = new LPRRequestObject();
        LPRResponseObject res = new LPRResponseObject();
        req.put("componentTypeId", 1);
        
        adapter.process(srk, req, res);
        
        String actual = res.get("paymentTermString").toString();
        String expected = "'?1000?','100':'1000'^'?2000?','200':'2000'^";
        
        Assert.assertEquals(actual, expected);
        
    }
    
    private static class MockPricingLogic implements IPricingLogicPaymentTerm {

        public List<PaymentTermInfo> getProductPaymentTerms(
                SessionResourceKit srk, int componentTypeId, boolean isUmbrella) {

            List<PaymentTermInfo> list = new ArrayList<PaymentTermInfo>();
            PaymentTermInfo info = new PaymentTermInfo();
            info.setMtgProdId(100);
            info.setPaymentTermId(1000);
            list.add(info);
            info = new PaymentTermInfo();
            info.setMtgProdId(200);
            info.setPaymentTermId(2000);
            list.add(info);
            return list;
        }
    }
}
