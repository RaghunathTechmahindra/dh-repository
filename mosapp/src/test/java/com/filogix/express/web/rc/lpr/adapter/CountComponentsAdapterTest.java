package com.filogix.express.web.rc.lpr.adapter;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.dao.ComponentEligibeCheckDao;
import com.filogix.express.web.rc.lpr.dao.CountComponentsDao;

public class CountComponentsAdapterTest {
    
    CountComponentsAdapter adapter;
    SessionResourceKit srk;
    LPRRequestObject req;
    LPRResponseObject res;
    @Before
    public void setUp() throws Exception {
        adapter = new CountComponentsAdapter();
        srk = new SessionResourceKit("test");
        req = new LPRRequestObject();
        res = new LPRResponseObject();
        adapter.setPricingLogic(new MockPricingLogic());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testProcessJSON() {
        
        adapter.process(srk, req, res);
        
        String actual = res.get("countComponents").toString();
        String expected = "{componentSize=0}";
        
        Assert.assertEquals(expected, actual);
        
    }

    private static class MockPricingLogic implements CountComponentsDao {

        public int getComponentSize() {
            return 0;
        }

        public void load(SessionResourceKit srk, LPRRequestObject request)
                throws Exception {
        }


    }
}
