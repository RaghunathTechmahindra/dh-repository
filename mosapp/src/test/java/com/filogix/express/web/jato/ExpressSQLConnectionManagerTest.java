/*
 * @(#)ExpressSQLConnectionManagerTest.java    2007-8-1
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.jato;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.web.ExpressWebContext;

/**
 * ExpressSQLConnectionManagerTest is the unit test case for
 * <code>ExpressSQLConnectionManager</code> 
 *
 * @version   1.0 2007-8-1
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressSQLConnectionManagerTest extends TestCase {

    // The logger
    private final static Logger _log = 
        LoggerFactory.getLogger(ExpressSQLConnectionManagerTest.class);

    // jato datasource mapping key for test.
    private final static String DATA_SOURCE_KEY = "forTesting";

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(ExpressSQLConnectionManagerTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new ExpressSQLConnectionManagerTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public ExpressSQLConnectionManagerTest() {

    }

    /**
     * test get connection.
     */
    public void testGetConnection() throws Exception {

        Connection connection =
            new ExpressSQLConnectionManager().getConnection(DATA_SOURCE_KEY);
        assertTrue(null != connection);
        Statement statement = connection.createStatement();
        assertTrue(null != statement);
        String query = "SELECT count(*) from USERPROFILE";
        _log.info("Executing Query: <{}>", query);
        ResultSet result = statement.executeQuery(query);
        assertTrue(null != result);
        assertTrue(result.next());
        int count = result.getInt(1);
        assertTrue(count > 0);
        _log.info("Amount of user: [{}]", count);

        result.close();
        statement.close();
        connection.close();
    }

    /**
     * constructs a test case with the given test case name.
     */
    public ExpressSQLConnectionManagerTest(String name) {
        super(name);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {

        // setting up a datasource in jatao datasource mapping.
        ExpressSQLConnectionManager.
            addDataSourceMapping(DATA_SOURCE_KEY,
                                 ExpressWebContext.
                                 DATASOURCE_CONNECTION_FACTORY_KEY);
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }
}