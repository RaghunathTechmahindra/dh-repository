package com.filogix.express.web.rc.lpr.adapter;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.mtgrates.IPricingLogicLenderProfile;
import com.basis100.mtgrates.LenderInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.LegacyLenderConverter;

public class PricingLogicLenderAdapterTest {
    
    PricingLogicLenderAdapter adapter;
    SessionResourceKit srk;
    LPRRequestObject req;
    LPRResponseObject res;
    @Before
    public void setUp() throws Exception {
        adapter = new PricingLogicLenderAdapter();
        srk = new SessionResourceKit("test");
        req = new LPRRequestObject();
        res = new LPRResponseObject();
        adapter.setPricingLogic(new MockPricingLogic());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testProcessLegacy() {
        
        adapter.setConverter(new LegacyLenderConverter());
        adapter.process(srk, req, res);
        
        String actual = res.get("lendersString").toString();
        String expected = "'?10?','10'^'?20?','20'^";
        
        Assert.assertEquals(expected, actual);
        
    }
    
    private static class MockPricingLogic implements IPricingLogicLenderProfile {

        public List<LenderInfo> getLenderProfiles(SessionResourceKit srk) {

            List<LenderInfo> list = new ArrayList<LenderInfo>();

            LenderInfo info = new LenderInfo();
            info.setLenderProfileId(10);
            list.add(info);
            
            info = new LenderInfo();
            info.setLenderProfileId(20);
            list.add(info);
            
            return list;
        }
    }
}
