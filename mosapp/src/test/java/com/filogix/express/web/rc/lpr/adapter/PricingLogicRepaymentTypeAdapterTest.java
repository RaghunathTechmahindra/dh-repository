package com.filogix.express.web.rc.lpr.adapter;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.mtgrates.IPricingLogicRepaymentType;
import com.basis100.mtgrates.RepaymentTypeInfo;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.JSONRepaymentTypeConverter;

public class PricingLogicRepaymentTypeAdapterTest {
    
    PricingLogicRepaymentTypeAdapter adapter;
    SessionResourceKit srk;
    LPRRequestObject req;
    LPRResponseObject res;
    @Before
    public void setUp() throws Exception {
        adapter = new PricingLogicRepaymentTypeAdapter();
        srk = new SessionResourceKit("test");
        req = new LPRRequestObject();
        res = new LPRResponseObject();
        adapter.setPricingLogic(new MockPricingLogic());

        req.put("productId", 1);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testProcessJSON() {
        
        adapter.setConverter(new JSONRepaymentTypeConverter());
        adapter.process(srk, req, res);
        
        String actual = res.get("repaymentTypes").toString();
        String expected = "[{repaymentDesc=?100?, productId=10, repaymentId=100}, {repaymentDesc=?200?, productId=20, repaymentId=200}]";
        
        Assert.assertEquals(expected, actual);
        
    }

    private static class MockPricingLogic implements IPricingLogicRepaymentType {

        public List<RepaymentTypeInfo> getRepaymentTypes(
                SessionResourceKit srk, int productId) {
            
            List<RepaymentTypeInfo> list = new ArrayList<RepaymentTypeInfo>();

            RepaymentTypeInfo info = new RepaymentTypeInfo();
            info.setMtgProdId(10);
            info.setRepaymentTypeId(100);
            list.add(info);
            
            info = new RepaymentTypeInfo();
            info.setMtgProdId(20);
            info.setRepaymentTypeId(200);
            list.add(info);
            
            return list;
        }
    }
}
