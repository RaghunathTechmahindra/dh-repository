package com.filogix.express.web.rc.lpr.adapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import junit.framework.Assert;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.MtgProd;
import com.basis100.mtgrates.LenderInfo;
import com.basis100.mtgrates.PricingLogic2;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.LegacyLenderConverter;
import com.filogix.express.web.rc.lpr.dao.ComponentTypeDao;
import com.filogix.express.web.rc.lpr.dao.CountComponentsDao;
import com.filogix.express.web.rc.lpr.dao.CountComponentsDaoImpl;
/**
 * <p>
 * CountComponentsAdapterTestDB
 * </p>
 * Express Entity class unit test: CountComponentsAdapterTestDB
 */
public class PricingLogicLenderAdapterTestDB extends FXDBTestCase {
	    IDataSet dataSetTest;
	    private PricingLogicLenderAdapter adapter = null;	 
	    SessionResourceKit srk;
	    LPRRequestObject req;
	    LPRResponseObject res;

	public PricingLogicLenderAdapterTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				PricingLogicLenderAdapter.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		    adapter = new PricingLogicLenderAdapter();
	        srk = new SessionResourceKit("test");
	        req = new LPRRequestObject();
	        res = new LPRResponseObject();
	        adapter.setPricingLogic(new PricingLogic2());
	        adapter.setConverter(new LegacyLenderConverter());
	        }

	public void testProcess() throws Exception {
		ITable testProcess = dataSetTest.getTable("testProcess");		
		int lenderProfileId=Integer.parseInt((String)testProcess.getValue(0, "LENDERPROFILEID"));		
		srk.getExpressState().setDealInstitutionId(1);
		 LenderProfile lProfile=new LenderProfile(srk,lenderProfileId);
		 adapter.process(srk, req, res); 
         assert adapter.getPricingLogic()!=null;       
         Assert.assertEquals(lenderProfileId, lProfile.getLenderProfileId());
	}
}