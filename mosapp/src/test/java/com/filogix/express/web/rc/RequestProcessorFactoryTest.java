/*
 * @(#)RequestProcessorFactoryTest.java    2007-8-22
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.web.rc;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.web.ExpressWebContext;

/**
 * RequestProcessorFactoryTest - 
 *
 * @version   1.0 2007-8-22
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class RequestProcessorFactoryTest extends TestCase {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(RequestProcessorFactoryTest.class);

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(RequestProcessorFactoryTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new RequestProcessorFactoryTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public RequestProcessorFactoryTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public RequestProcessorFactoryTest(String name) {
        super(name);
    }

    /**
     * test get processors.
     */
    public void testGetRequestProcessor() throws Exception {

        RequestProcessorFactory factory =
            (RequestProcessorFactory)
            ExpressWebContext.getService("serviceProcessorFactory");
        assertTrue(null != factory);

        invoke(factory, "util.RichClientMessages");
        invoke(factory, "express.service.AppraisalOrderRequest");
    }

    /**
     * trying to invoke twice for the same request type.
     */
    protected void invoke(RequestProcessorFactory factory, String type)
        throws Exception {

        _logger.info("Trying to get a Processor for type [{}] ...", type);
        AbstractRequestProcessor processor =
            (AbstractRequestProcessor) factory.getRequestProcessor(type);
        assertTrue(null != processor);
        _logger.info("Succefully got processor: [{}]!", processor);
        RequestObject request = processor.getRequestObject();
        ResponseObject response = processor.getResponseObject();

        _logger.info("Trying to get a Processor with the same type ...");
        processor = (AbstractRequestProcessor)
            factory.getRequestProcessor(type);
        assertTrue(null != processor);
        _logger.info("Succefully got another processor: [{}]!", processor);
        RequestObject anotherRequest = processor.getRequestObject();
        ResponseObject anotherResponse = processor.getResponseObject();

        _logger.info("??????? The Request Objects are the Same? " +
                     (request == anotherRequest));
        assertFalse(request == anotherRequest);
        _logger.info("??????? The Response Objects are the Same? " +
                     (response == anotherResponse));
        assertFalse(response == anotherResponse);
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }
}