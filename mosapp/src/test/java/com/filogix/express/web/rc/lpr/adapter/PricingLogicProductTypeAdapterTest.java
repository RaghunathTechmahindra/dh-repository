package com.filogix.express.web.rc.lpr.adapter;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.converter.JSONProductTypeConverter;
import com.filogix.express.web.rc.lpr.dao.ProductTypeDao;

public class PricingLogicProductTypeAdapterTest {
    
    PricingLogicProductTypeAdapter adapter;
    SessionResourceKit srk;
    LPRRequestObject req;
    LPRResponseObject res;
    @Before
    public void setUp() throws Exception {
        adapter = new PricingLogicProductTypeAdapter();
        srk = new SessionResourceKit("test");
        req = new LPRRequestObject();
        res = new LPRResponseObject();
        adapter.setPricingLogic(new MockPricingLogic());

        req.put("productId", 1);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testProcessJSON() {
        
        adapter.setConverter(new JSONProductTypeConverter());
        adapter.process(srk, req, res);
        
        String actual = res.get("productType").toString();
        String expected = "[{productTypeDesc=Multiple Component, productTypeId=3}]";
        
        Assert.assertEquals(expected, actual);
        
    }

    private static class MockPricingLogic implements ProductTypeDao {

        public List<ProductTypeDao> findProductTypeListByProductId(
                SessionResourceKit srk, int productId) throws Exception {
            MockPricingLogic mock = new MockPricingLogic();
            //mock.setProductTypeId(3);
            List<ProductTypeDao> list = new ArrayList<ProductTypeDao>();
            list.add(mock);
            return list;
        }

        public int getProductTypeId() {
            return 3;
        }

        public void setProductTypeId(int productTypeId) {
            //dummy
        }
    }
}
