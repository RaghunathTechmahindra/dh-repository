package com.filogix.express.web.rc.lpr.adapter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import com.basis100.FXDBTestCase;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.MtgProd;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.web.rc.lpr.LPRRequestObject;
import com.filogix.express.web.rc.lpr.LPRResponseObject;
import com.filogix.express.web.rc.lpr.dao.ComponentTypeDao;
import com.filogix.express.web.rc.lpr.dao.CountComponentsDao;
import com.filogix.express.web.rc.lpr.dao.CountComponentsDaoImpl;
/**
 * <p>
 * CountComponentsAdapterTestDB
 * </p>
 * Express Entity class unit test: CountComponentsAdapterTestDB
 */
public class CountComponentsAdapterTestDB extends FXDBTestCase {
	    IDataSet dataSetTest;
	    private CountComponentsAdapter adapter = null;	 
	    SessionResourceKit srk;
	    LPRRequestObject req;
	    LPRResponseObject res;

	public CountComponentsAdapterTestDB(String name) throws IOException,
			DataSetException {
		super(name);
		dataSetTest = new FlatXmlDataSet(this.getClass().getResource(
				CountComponentsAdapter.class.getSimpleName() + "DataSetTest.xml"));
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();		
		    adapter = new CountComponentsAdapter();
	        srk = new SessionResourceKit("test");
	        req = new LPRRequestObject();
	        res = new LPRResponseObject();
	       adapter.setPricingLogic(new CountComponentsDaoImpl());
	        }

	public void testProcess() throws Exception {
		ITable testProcess = dataSetTest.getTable("testProcess");		
		int dealId=Integer.parseInt((String)testProcess.getValue(0, "DEALID"));	
		int copyId=Integer.parseInt((String)testProcess.getValue(0, "COPYID"));
		srk.getExpressState().setDealInstitutionId(1);		
		Deal deal=new Deal(srk, null, dealId,copyId);
		req.setDealId(dealId);
		req.setDealCopyId(copyId);
		 adapter.process(srk, req, res); 
         assert adapter.getPricingLogic()!=null;       
         Assert.assertEquals(dealId, deal.getDealId());
	}
}