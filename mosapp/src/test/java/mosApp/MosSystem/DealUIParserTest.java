package mosApp.MosSystem;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.entity.PrimeIndexRateInventory;
import com.basis100.deal.pk.PricingProfilePK;
import com.basis100.deal.pk.PricingRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateInventoryPK;
import com.basis100.deal.pk.PrimeIndexRateProfilePK;
import com.basis100.resources.SessionResourceKit;

public class DealUIParserTest {

	private static InstitutionProfile ip;
	private static PricingProfile pp;
	private static PricingRateInventory pi;
	private static PrimeIndexRateInventory pri;
	private static SessionResourceKit srk;
	
	private DealUIParser dealUIParser;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("------- setUpBeforeClass --------");
		srk = new SessionResourceKit();
		srk.beginTransaction();
		
		ip = new InstitutionProfile(srk);
		ip = ip.findByFirst();
		srk.getExpressState().setDealInstitutionId(ip.getInstitutionProfileId());
		
		pp = new PricingProfile(srk);
		PricingProfilePK ppPK = pp.createPrimaryKey();
		pp.create(ppPK);
		
		pi = new PricingRateInventory(srk);
		PricingRateInventoryPK piPK = pi.createPrimaryKey();
		pi.create(piPK, ppPK);
		
//		prp = new PrimeIndexRateProfile(srk);
//		PrimeIndexRateProfilePK prpPK = prp.createPrimaryKey();
//		prp.create(prpPK);
		
		//0:not in use
		PrimeIndexRateProfilePK prpPK = new PrimeIndexRateProfilePK(0);
		
		
		pri = new PrimeIndexRateInventory(srk);
		PrimeIndexRateInventoryPK priPK = pri.createPrimaryKey();
		pri.create(priPK, prpPK);
		
		pi.setPrimeIndexRateInventoryId(pri.getPrimeIndexRateInventoryId());
		pi.setIndexEffectiveDate(new Date());
		pi.setPrimeBaseAdj(3.3);
		pi.setTeaserDiscount(2.2);
		pi.setTeaserTerm(10);
		pi.ejbStore();
		
		pp.setPrimeIndexRateProfileId(pri.getPrimeIndexRateProfileId());
		pp.setRateCode("FOO!");
		pp.ejbStore();
		
		pri.setPrimeIndexRate(5.56D);
		pri.ejbStore();
		
		System.out.println("InstitutionProfileId = " + ip.getInstitutionProfileId());
		System.out.println("PricingProfileId = " + pp.getPricingProfileId());
		System.out.println("PricingRateInventoryID = " + pi.getPricingRateInventoryID());
		System.out.println("PrimeIndexRateInventoryId = " + pri.getPrimeIndexRateInventoryId());
		System.out.println("PrimeIndexRateProfileId = " + pri.getPrimeIndexRateProfileId());
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("------- tearDownAfterClass --------");
		srk.freeResources();
		System.out.println("db resource cleaned");
	}

	@Before
	public void setUp() throws Exception {
		dealUIParser = new DealUIParser(srk);
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testGetParsedPhone() {
		String[] phoneparts = {"416", "836", "1903"};
		assertEquals("4168361903", dealUIParser.getParsedPhone(phoneparts));
	}

	@Test
	public void testGetParsedDate() {
		
		String[] dateparts = {"1", "2", "2010"}; // m/d/y
		//should return d/m/y
		assertEquals("02012010", dealUIParser.getParsedDate(dateparts));
	}

	@Test
	public void testGetParsedMonth() {
		assertEquals("12", dealUIParser.getParsedMonth("12"));
		assertEquals("", dealUIParser.getParsedMonth("13"));
	}

	@Test
	public void testGetTotalTerms() {
		String[] duration = {"5", "6"}; //= 5years and 6months 
		// -> 66 months
		assertEquals("66", dealUIParser.getTotalTerms(duration));
	}

	@Test
	public void testGetPricingRateInventoryId() {
		String[] acombolinein = {"139"};
		assertEquals("139", dealUIParser.getPricingRateInventoryId(acombolinein));
	}

	@Test
	public void testGetPricingProfileIdFromServlet() {
		//nobody is using this method, so I don't test
	}

	@Test
	public void testGetRateDateFromPostedCombo() {
		String[] acombolinein = {String.valueOf(pi.getPricingRateInventoryID())};
		DateFormat theFormat = new SimpleDateFormat("ddMMyyyy");
		String expected = theFormat.format(pi.getIndexEffectiveDate());
		assertEquals(expected, dealUIParser.getRateDateFromPostedCombo(acombolinein));
	}

	@Test
	public void testGetDealPostedRateFromPostedCombo() {
		String[] acombolinein = {String.valueOf(pi.getPricingRateInventoryID())};
		String expected = String.valueOf(pi.getInternalRatePercentage());
		assertEquals(expected, dealUIParser.getDealPostedRateFromPostedCombo(acombolinein));
	}

	@Test
	public void testParseChoiceBoxId() {
		String[] aval  = {"139"};
		String expected = "139";
		assertEquals(expected, dealUIParser.parseChoiceBoxId(aval));
	}

	@Test
	public void testParseMtgProdBoxId() {
		String[] aval  = {"139"};
		String expected = "139";
		assertEquals(expected, dealUIParser.parseMtgProdBoxId(aval));
	}

	@Test
	public void testParseCompositeRateId() {
		String[] aval  = {"139"};
		String expected = "139";
		assertEquals(expected, dealUIParser.parseCompositeRateId(aval));
	}

	@Test
	public void testParseYesNo() {
		String[] aval = {"yes"};
		assertEquals("Y", dealUIParser.parseYesNo(aval));
	}

	@Test
	public void testGetValidString() {
		assertEquals("", DealUIParser.getValidString(null));
	}

	@Test
	public void testGetInterestTypeIdFromServlet() {
	}

	@Test
	public void testGetInterestTypeId() {
	}

	@Test
	public void testGetDealPrimeIndexRateFromPostedCombo() {
		String[] acombolinein = {String.valueOf(pi.getPricingRateInventoryID())};
		String expected = String.valueOf(pri.getPrimeIndexRate());
		
		assertEquals(expected, dealUIParser.getDealPrimeIndexRateFromPostedCombo(acombolinein));
	}

	@Test
	public void testGetDealPrimeBaseAdjFromPostedCombo() {
		String[] acombolinein = {String.valueOf(pi.getPricingRateInventoryID())};
		String expected = String.valueOf(pi.getPrimeBaseAdj());
		
		assertEquals(expected, dealUIParser.getDealPrimeBaseAdjFromPostedCombo(acombolinein));
	}

	@Test
	public void testGetDealPrimeIndexIdFromPostedCombo() {
		String[] acombolinein = {String.valueOf(pi.getPricingRateInventoryID())};
		String expected = String.valueOf(0); //not in use
		
		assertEquals(expected, dealUIParser.getDealPrimeIndexIdFromPostedCombo(acombolinein));
	}

	@Test
	public void testGetDealTeaserDiscountFromPostedCombo() {
		String[] acombolinein = {String.valueOf(pi.getPricingRateInventoryID())};
		String expected = String.valueOf(pi.getTeaserDiscount());
		
		assertEquals(expected, dealUIParser.getDealTeaserDiscountFromPostedCombo(acombolinein));
	}

	@Test
	public void testGetDealTeaserTermFromPostedCombo() {
		String[] acombolinein = {String.valueOf(pi.getPricingRateInventoryID())};
		String expected = String.valueOf(pi.getTeaserTerm());
		
		assertEquals(expected, dealUIParser.getDealTeaserTermFromPostedCombo(acombolinein));
	}

}
