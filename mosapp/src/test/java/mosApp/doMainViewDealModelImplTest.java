package mosApp;

/**
 * <p>Title: doDealEntryMainSelectModelImplTest</p>
 *
 * <p>Description: UnitTest class for mosApp.MosSystem.doMainViewDealModel class.</p>
 * @author  MCM Implementation Team
 * @version 1.0 15-June-2008 XS_16.8 Initial Version 
 */

import javax.servlet.http.HttpSession;

import junit.framework.Test;
import mosApp.MosSystem.doDealEntryMainSelectModelImpl;
import mosApp.MosSystem.doMainViewDealModelImpl;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.RemoteException;
import com.iplanet.jato.RequestManager;
import com.ltx.unittest.base.BaseModelTest;
import com.ltx.unittest.util.EntityTestUtil;

public class doMainViewDealModelImplTest extends BaseModelTest
{
    //Instance Variables
    private doMainViewDealModelImpl doMainViewDealModelImpl;

    Deal dealData = null;
    public doMainViewDealModelImplTest()
    {
        super("mosApp.MosSystem.doMainViewDealModelImpl");
        EntityTestUtil.getSessionResourceKit().getExpressState()
        .setDealInstitutionId(1);
        doMainViewDealModelImpl = (doMainViewDealModelImpl) model;
        dealData =(Deal)EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.Deal");
    }

    /**
     * This method is used to setup test data that will be used as a part of this test
     */
    public void setupTestData() throws Exception
    {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setRefiAdditionalInformation(dealData.getRefiAdditionalInformation());

        deal.ejbStore();

    }

    /**
     * 
     * <p>Description: This method tests the model for the select statement.</p>
     * @throws Exception
     */

    public void testdoDealEntryModel() throws Exception
    {
        try
        {
            setupTestData();
            doMainViewDealModelImpl.addUserWhereCriterion("dfDealId", "=",
                    new Integer(deal.getDealId()));
            doMainViewDealModelImpl.addUserWhereCriterion("dfCopyId", "=",
                    new Integer(deal.getCopyId()));
            doMainViewDealModelImpl.executeSelect(null);
            assertEquals(doMainViewDealModelImpl.getDfRefiAdditionalInformation(),dealData.getRefiAdditionalInformation());
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            tearDownTestData();
        }

    }

    public void tearDownTestData() throws Exception
    {

        if (deal != null)
        {
            deal.dcm = null;
        }
        try
        {
            
            getMasterDeal().removeDeal();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
        System.out.println("ejb remove done");

    }

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
        .createSuite(doMainViewDealModelImplTest.class);
    }

    public Deal getDealData()
    {
        return dealData;
    }

    public void setDealData(Deal dealData)
    {
        this.dealData = dealData;
    }

}
