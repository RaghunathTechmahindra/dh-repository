package mosApp;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import junit.framework.Test;

import com.basis100.deal.entity.ComponentSummary;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.RemoteException;
import com.ltx.unittest.base.BaseModelTest;
import com.ltx.unittest.util.EntityTestUtil;
import mosApp.MosSystem.doComponentInfoModelImpl;

/**
 * <p>Title: doComponentInfoModelImplTest</p>
 *
 * <p>Description: UnitTest class for mosApp.MosSystem.doComponentInfoModelImplTest class.</p>
 *
 * @author  MCM Implementation Team
 * @version 1.0 14-JUN-2008 XS_2.13 Initial Version
 *
 */

public class doComponentInfoModelImplTest extends BaseModelTest {

    private doComponentInfoModelImpl doComponentInfoModel;

    private ComponentSummary componentSummaryData;

    private ComponentSummary componentSummary;

    
    /**
     * <p>
     * Description: Loads the model and test data for the model.
     *</P>
     *@version 1.0 XS_2.9 Initial Version.
     */
    public doComponentInfoModelImplTest() {
        super("mosApp.MosSystem.doComponentInfoModelImpl");
        doComponentInfoModel = (doComponentInfoModelImpl) model;
        componentSummaryData = (ComponentSummary) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentSummary");
    }

    /**
     *<p>
     * Description: This method is used to setup test data that will be used as a part of this test
     *</P>
     *@version 1.0 XS_2.9 Initial Version.
     */
    public void setupTestData() throws Exception {
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();
        


        setCopyId(deal.getCopyId());

        componentSummary = (ComponentSummary) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentSummary");
        componentSummary.create(deal.getDealId(), deal.getCopyId());

        componentSummary.setTotalAmount(componentSummaryData.getTotalAmount());
        componentSummary.setTotalCashbackAmount(componentSummaryData
                .getTotalCashbackAmount());

        componentSummary.ejbStore();

    }

    /**
     * 
     * <p>Description: This method tests the model for the select statement.
     * </p>
     * @version 1.0 XS_2.9 Initial Version.
     * @throws Exception exception object.
     * 
     */
    public void testSelect() throws Exception {
        try {
            setupTestData();
            doComponentInfoModel.addUserWhereCriterion("dfDealId", "=",
                    new Integer(componentSummary.getDealId()));
            doComponentInfoModel.addUserWhereCriterion("dfCopyId", "=",
                    new Integer(componentSummary.getCopyId()));
            System.out.println(doComponentInfoModel.getSelectSQL());
            doComponentInfoModel.executeSelect(null);
            
            assertEquals(componentSummaryData.getTotalAmount(),doComponentInfoModel.getDfTotalAmount().doubleValue(),0.0);
            assertEquals(componentSummaryData.getTotalCashbackAmount(),doComponentInfoModel.getDfTotalCashBackAmount().doubleValue(),0.0);

        } catch (Exception e) {
            throw e;
        } finally {
            tearDownTestData();
        }

    }

    /**
     * <p>
     * Description: deletes the master deal and all the associated entites.
     * </P>
     * @version 1.0 XS_2.9 Initial Version.
     */
    public void tearDownTestData() throws Exception {

        if (deal != null) {
            deal.dcm = null;
        }
        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        System.out.println("ejb remove done");

    }
    
    /**
     * <p>
     * Description: returns the test object.
     * </p>
     * @version 1.0 XS_2.9 Initial Version.
     * @return Test test class object
     */
    public static Test suite() {
        return (Test) DDStepsSuiteFactory
                .createSuite(doComponentInfoModelImplTest.class);
    }

    /**
     * <p>
     * Description: Returns the ComponentSummary entity values from the excelsheet
     * </p>
     * @version 1.0 XS_2.9 Initial Version.
     * @retun QualifyDetail - qualifyDetail entity.
     */
    public ComponentSummary getComponentSummaryData()
    {
      return componentSummaryData;
    }

    /**
     * <p>
     * Description: Sets the ComponentSummary entity values from the excelsheet
     * </p>
     * @version 1.0 XS_2.9 Initial Version.
     * @param qualifyDetailFromExcel - qualifyDetail entity.
     */
    public void setComponentSummaryData(ComponentSummary componentSummaryData)
    {
      this.componentSummaryData = componentSummaryData;
    }
}
