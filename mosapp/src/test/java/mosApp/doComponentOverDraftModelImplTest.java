package mosApp;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.ComponentOverdraft;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;

import com.basis100.entity.RemoteException;

import com.basis100.picklist.BXResources;

import com.ltx.unittest.base.BaseModelTest;
import com.ltx.unittest.util.EntityTestUtil;

import junit.framework.Test;

import mosApp.MosSystem.SessionStateModelImpl;
import mosApp.MosSystem.doComponentCreditCardModelImpl;
import mosApp.MosSystem.doComponentOverDraftModelImpl;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

/**
 * <p>
 * Title: doComponentOverDraftModelImplTest
 * </p>
 * Description: UnitTest class for
 * mosApp.MosSystem.doComponentOverDraftModelImpl class.
 * </p>
 * @author MCM Implementation Team
 * @version 1.0 12-June-2008 XS_16.7 Initial Version
 * @version 1.1 7-July-2008 XS_2.38 Added additional overdraft fields
 */
public class doComponentOverDraftModelImplTest extends BaseModelTest
{
    // Instance Variables
    private doComponentOverDraftModelImpl doComponentOverDraftModelImpl;

    Deal dealData = null;

    Component componentData = null;

    Component component;

    ComponentOverdraft componentOverdraftData = null;

    ComponentOverdraft componentOverdraft;

    int langID;

    /**
     * Creates a new doComponentOverDraftModelImplTest object.
     */
    public doComponentOverDraftModelImplTest()
    {
        super("mosApp.MosSystem.doComponentOverDraftModelImpl");

        doComponentOverDraftModelImpl = (doComponentOverDraftModelImpl) model;
        dealData = (Deal) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Deal");
        componentData = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentOverdraftData = (ComponentOverdraft) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentOverdraft");
    }

    /**
     * This method is used to setup test data that will be used as a part of
     * this test
     */
    public void setupTestData() throws Exception
    {
        SessionStateModelImpl sessionaState = (SessionStateModelImpl) getSessionStateModel();
        sessionaState.setLanguageId(getLangID());

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.ejbStore();
        deal.setRefiAdditionalInformation(dealData
                .getRefiAdditionalInformation());

        /** ****************Component Data******************************** */
        component = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        component.create(deal.getDealId(), deal.getCopyId(), componentData
                .getComponentTypeId(), componentData.getMtgProdId());
        component.setPricingRateInventoryId(componentData.getPricingRateInventoryId());
        component.setRepaymentTypeId(componentData.getRepaymentTypeId());
        component.setPostedRate(componentData.getPostedRate());
        component.setAdditionalInformation(componentData.getAdditionalInformation());
        component.ejbStore();

        /**
         * ****************componentOverdraft
         * Data********************************
         */
        componentOverdraft = (ComponentOverdraft) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentOverdraft");
        componentOverdraft.create(component.getComponentId(), component
                .getCopyId());

        componentOverdraft.setOverDraftAmount(componentOverdraftData
                .getOverDraftAmount());

        componentOverdraft.ejbStore();

    }

    /**
     * <p>
     * Description: This method tests the model for the select statement.
     * </p>
     * @throws Exception
     */
    public void testdoComponentOverDraftModel() throws Exception
    {
        try
        {
            setupTestData();
            doComponentOverDraftModelImpl.addUserWhereCriterion("dfDealId",
                    "=", new Integer(deal.getDealId()));
            doComponentOverDraftModelImpl.addUserWhereCriterion("dfCopyId",
                    "=", new Integer(deal.getCopyId()));
            doComponentOverDraftModelImpl.executeSelect(null);

            assertEquals(doComponentOverDraftModelImpl.getDfComponentType(),
                    BXResources.getPickListDescription(deal
                            .getInstitutionProfileId(), "COMPONENTTYPE",
                            componentData.getComponentTypeId(),
                            getSessionStateModel().getLanguageId()));
            assertEquals(doComponentOverDraftModelImpl.getDfProductName(),
                    BXResources.getPickListDescription(deal
                            .getInstitutionProfileId(), "MTGPROD",
                            componentData.getMtgProdId(),
                            getSessionStateModel().getLanguageId()));

            assertEquals(doComponentOverDraftModelImpl.getDfOverDraftAmount()
                    .doubleValue(), componentOverdraftData.getOverDraftAmount());
            
            assertEquals(doComponentOverDraftModelImpl.getDfAdditionalInformation(),
            		component.getAdditionalInformation());
            
            assertEquals(doComponentOverDraftModelImpl.getDfMtgprodid()
                    .intValue(), component.getMtgProdId());
            
            assertEquals(doComponentOverDraftModelImpl.getDfPricingrateinventoryid()
                    .intValue(), component.getPricingRateInventoryId());
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            tearDownTestData();
        }
    }

    /**
     * This method tear downs the test data.
     */
    public void tearDownTestData() throws Exception
    {
        if (deal != null)
        {
            deal.dcm = null;

        }

        try
        {
            if (component != null)
            {
                component.ejbRemove(false);
            }
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }

        System.out.println("ejb remove done");
    }

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(doComponentOverDraftModelImplTest.class);
    }

    /**
     * Getters and Setters for the test data entity Starts
     */
    public Deal getDealData()
    {
        return dealData;
    }

    public void setDealData(Deal dealData)
    {
        this.dealData = dealData;
    }

    public Component getComponentData()
    {
        return componentData;
    }

    public void setComponentData(Component componentData)
    {
        this.componentData = componentData;
    }

    public ComponentOverdraft getComponentOverdraftData()
    {
        return componentOverdraftData;
    }

    public void setComponentLoanData(ComponentOverdraft componentOverdraftData)
    {
        this.componentOverdraftData = componentOverdraftData;
    }

    public int getLangID()
    {
        return langID;
    }

    public void setLangID(int langID)
    {
        this.langID = langID;
    }

    /**
     * Getters and Setters for the test data entity Ends
     */
}
