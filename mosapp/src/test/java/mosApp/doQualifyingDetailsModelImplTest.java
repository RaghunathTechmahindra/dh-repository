package mosApp;

import junit.framework.Test;
import mosApp.MosSystem.SessionStateModelImpl;
import mosApp.MosSystem.doQualifyingDetailsModelImpl;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.QualifyDetail;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.ltx.unittest.base.BaseModelTest;
import com.ltx.unittest.util.EntityTestUtil;

/**
 * <p>Title: doQualifyingDetailsModelImplTest</p>
 *
 * <p>Description: UnitTest class for mosApp.MosSystem.doQualifyingDetailsModelImpl class.</p>
 *
 * @author  MCM Implementation Team
 * @version 1.0 14-JUN-2008 XS_2.13 Initial Version
 *
 */
public class doQualifyingDetailsModelImplTest extends BaseModelTest
{

    private doQualifyingDetailsModelImpl doQualifyingDetailsModelImpl=null;

    private QualifyDetail qualifyingDetailDataEntity =null;

    private QualifyDetail qualifyDetail;

    private int langId;

    /**
     * <p>
     * Description: Loads the model and test data for the model.
     *</P>
     *@version 1.0 XS_2.13 Initial Version.
     */
    public doQualifyingDetailsModelImplTest()
    {
        super("mosApp.MosSystem.doQualifyingDetailsModelImpl");
        doQualifyingDetailsModelImpl =(doQualifyingDetailsModelImpl) model;
        qualifyingDetailDataEntity = (QualifyDetail) EntityTestUtil.getInstance().loadEntity("com.basis100.deal.entity.QualifyDetail");

    }

    /**
     *<p>
     * Description: This method is used to setup test data that will be used as a part of this test
     *</P>
     *@version 1.0 XS_2.13 Initial Version.
     */
    public void setupTestData() throws Exception {
        SessionStateModelImpl sessionstate =(SessionStateModelImpl) getSessionStateModel();
        sessionstate.setLanguageId(getLangId());
        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");

        deal.ejbStore();

        //*****************Test related data setup starts here******************/
        qualifyDetail = (QualifyDetail) EntityTestUtil.getInstance().loadEntity(
        "com.basis100.deal.entity.QualifyDetail");
        qualifyDetail.create(deal.getDealId(),qualifyingDetailDataEntity.getInterestCompoundingId(),qualifyingDetailDataEntity.getRepaymentTypeId());

        qualifyDetail.setQualifyRate(qualifyingDetailDataEntity.getQualifyRate());
        qualifyDetail.setAmortizationTerm(qualifyingDetailDataEntity.getAmortizationTerm());
        qualifyDetail.setPAndIPaymentAmountQualify(qualifyingDetailDataEntity.getPAndIPaymentAmountQualify());
        qualifyDetail.setQualifyGds(qualifyingDetailDataEntity.getQualifyGds());
        qualifyDetail.setQualifyTds(qualifyingDetailDataEntity.getQualifyTds());
        qualifyDetail.ejbStore();

    }

    /**
     * 
     * <p>Description: This method tests the model for the select statement.
     * </p>
     * @version 1.0 XS_2.13 Initial Version.
     * @throws Exception exception object.
     * 
     */

    public void testSelect() throws Exception {
        try
        {	

            setupTestData();

            doQualifyingDetailsModelImpl.addUserWhereCriterion("dfDealId", "=", new Integer(qualifyDetail.getDealId()));
            doQualifyingDetailsModelImpl.addUserWhereCriterion("dfInstitutionProfileId", "=", new Integer(qualifyDetail.getInstitutionProfileId()));
            doQualifyingDetailsModelImpl.executeSelect(null);


            assertEquals(qualifyingDetailDataEntity.getQualifyRate(),(doQualifyingDetailsModelImpl.getDfQualifyRate()).doubleValue(),0.0);
            assertEquals(BXResources.getPickListDescription(getDealInstitutionId(), "COMPOUNDPERIOD", qualifyingDetailDataEntity.getInterestCompoundingId(),getLangId()),doQualifyingDetailsModelImpl.getDfInterestCompoundingId());
            assertEquals(qualifyingDetailDataEntity.getAmortizationTerm(),(doQualifyingDetailsModelImpl.getDfAmortizationTerm()).doubleValue(),0.0);
            assertEquals(qualifyingDetailDataEntity.getPAndIPaymentAmountQualify(),(doQualifyingDetailsModelImpl.getDfPAndIPaymentAmountQualify()).doubleValue(),0.0);
            assertEquals(qualifyingDetailDataEntity.getQualifyGds(),(doQualifyingDetailsModelImpl.getDfQualifyGds()).doubleValue(),0.0);
            assertEquals(qualifyingDetailDataEntity.getQualifyTds(),(doQualifyingDetailsModelImpl.getDfQualifyTds()).doubleValue(),0.0);
            assertEquals(BXResources.getPickListDescription(getDealInstitutionId(), "REPAYMENTTYPE",qualifyingDetailDataEntity.getRepaymentTypeId(),getLangId()),doQualifyingDetailsModelImpl.getDfRepaymentTypeId());

        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            tearDownTestData();
        }

    }


    /**
     * <p>
     * Description: deletes the master deal and all the associated entites.
     * </P>
     * @version 1.0 XS_2.13 Initial Version.
     */
    public void tearDownTestData() throws Exception {

        if (deal != null) {
            deal.dcm = null;
        }
        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    /**
     * <p>
     * Description: returns the test object.
     * </p>
     * @version 1.0 XS_2.13 Initial Version.
     * @return Test test class object
     */
    public static Test suite() {

        return (Test) DDStepsSuiteFactory.createSuite(doQualifyingDetailsModelImplTest.class);

    }


    /**
     * <p>
     * Description: Returns the qualifyDetail entity values from the excelsheet
     * </p>
     * @version 1.0 XS_2.13 Initial Version.
     * @retun QualifyDetail - qualifyDetail entity.
     */
    public QualifyDetail getQualifyingDetailDataEntity() {
        return qualifyingDetailDataEntity;
    }


    /**
     * <p>
     * Description: Sets the qualifyDetail entity values from the excelsheet
     * </p>
     * @version 1.0 XS_2.13 Initial Version.
     * @param qualifyDetailFromExcel - qualifyDetail entity.
     */
    public void setQualifyDetailDataEntity(QualifyDetail qualifyingDetailDataEntity) {
        this.qualifyingDetailDataEntity = qualifyingDetailDataEntity;
    }

    public int getLangId() {
        return langId;
    }

    public void setLangId(int langId) {
        this.langId = langId;
    }



}

