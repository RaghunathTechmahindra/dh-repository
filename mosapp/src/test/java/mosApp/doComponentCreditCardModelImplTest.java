package mosApp;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentCreditCard;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;

import com.basis100.entity.RemoteException;

import com.basis100.picklist.BXResources;

import com.ltx.unittest.base.BaseModelTest;
import com.ltx.unittest.util.EntityTestUtil;

import junit.framework.Test;

import mosApp.MosSystem.SessionStateModelImpl;
import mosApp.MosSystem.doComponentCreditCardModelImpl;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

/**
 * <p>
 * Title: docomponentLoanModelImplTest
 * </p>
 * <p>
 * Description: UnitTest class for
 * mosApp.MosSystem.doComponentCreditCardModelImpl class.
 * </p>
 * @author MCM Implementation Team
 * @version 1.0 12-June-2008 XS_16.7 Initial Version
 * @version 1.1 7 July 2008 Added new feilds for XS_2.36 
 */
public class doComponentCreditCardModelImplTest extends BaseModelTest
{
    // Instance Variables
    private doComponentCreditCardModelImpl doComponentCreditCardModelImpl;

    Deal dealData = null;

    Component componentData = null;

    Component component;

    ComponentCreditCard componentCreditCardData = null;

    ComponentCreditCard componentCreditCard;

    int langID;

    /**
     * Description: Loads the model and test data for the model.
     * @version 1.0 XS_16.7 Initial Version.
     */
    public doComponentCreditCardModelImplTest()
    {
        super("mosApp.MosSystem.doComponentCreditCardModelImpl");

        doComponentCreditCardModelImpl = (doComponentCreditCardModelImpl) model;
        dealData = (Deal) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Deal");
        componentData = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentCreditCardData = (ComponentCreditCard) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentCreditCard");
    }

    /**
     * <p>
     * Description: This method is used to setup test data that will be used as
     * a part of this test
     * </P>
     * @version 1.0 XS_16.7 Initial Version.
     */
    public void setupTestData() throws Exception
    {
        SessionStateModelImpl sessionaState = (SessionStateModelImpl) getSessionStateModel();
        sessionaState.setLanguageId(getLangID());

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setRefiAdditionalInformation(dealData
                .getRefiAdditionalInformation());

        /** ****************Component Data******************************** */
        component = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        component.create(deal.getDealId(), deal.getCopyId(), componentData
                .getComponentTypeId(), componentData.getMtgProdId());
        component.setPostedRate(componentData.getPostedRate());
        
        component.setAdditionalInformation(componentData.getAdditionalInformation());
        
        component.ejbStore();

        /**
         * ****************componentCreditCardData********************************
         */
        componentCreditCard = (ComponentCreditCard) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentCreditCard");
        componentCreditCard.create(component.getComponentId(), component
                .getCopyId());

        componentCreditCard.setCreditCardAmount(componentCreditCardData
                .getCreditCardAmount());

        componentCreditCard.ejbStore();
        deal.ejbStore();
    }

    /**
     * <p>
     * Description: This method tests the model for the select statement.
     * </p>
     * @throws Exception
     */
    public void testdoComponentCreditCardModel() throws Exception
    {
        try
        {
            setupTestData();
            doComponentCreditCardModelImpl.addUserWhereCriterion("dfDealId",
                    "=", new Integer(deal.getDealId()));
            doComponentCreditCardModelImpl.addUserWhereCriterion("dfCopyId",
                    "=", new Integer(deal.getCopyId()));
            doComponentCreditCardModelImpl.executeSelect(null);

            assertEquals(doComponentCreditCardModelImpl.getDfComponentType(),
                    BXResources.getPickListDescription(deal
                            .getInstitutionProfileId(), "COMPONENTTYPE",
                            componentData.getComponentTypeId(),
                            getSessionStateModel().getLanguageId()));

            assertEquals(doComponentCreditCardModelImpl.getDfProductName(),
                    BXResources.getPickListDescription(deal
                            .getInstitutionProfileId(), "MTGPROD",
                            componentData.getMtgProdId(),
                            getSessionStateModel().getLanguageId()));
            assertEquals(doComponentCreditCardModelImpl.getDfCreditCardAmount()
                    .doubleValue(), componentCreditCardData
                    .getCreditCardAmount());
            assertEquals(doComponentCreditCardModelImpl.getDfAdditionalInformation()
            		, componentData.getAdditionalInformation());
            assertEquals(doComponentCreditCardModelImpl.getDfMtgProdId().intValue()
            		, componentData.getMtgProdId());
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            tearDownTestData();
        }
    }

    /**
     * <p>
     * Description: deletes the master deal and all the associated entites.
     * </P>
     * @version 1.0 XS_16.7 Initial Version.
     */
    public void tearDownTestData() throws Exception
    {
        if (deal != null)
        {
            deal.dcm = null;
            component.ejbRemove(false);
        }

        try
        {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }

        System.out.println("ejb remove done");
    }

    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(doComponentCreditCardModelImplTest.class);
    }

    /**
     * Getters and setters for the test data entity.Starts
     */
    public Deal getDealData()
    {
        return dealData;
    }

    public void setDealData(Deal dealData)
    {
        this.dealData = dealData;
    }

    public Component getComponentData()
    {
        return componentData;
    }

    public void setComponentData(Component componentData)
    {
        this.componentData = componentData;
    }

    public ComponentCreditCard getComponentCreditCardData()
    {
        return componentCreditCardData;
    }

    public void setComponentCreditCardData(
            ComponentCreditCard componentCreditCardData)
    {
        this.componentCreditCardData = componentCreditCardData;
    }

    public int getLangID()
    {
        return langID;
    }

    public void setLangID(int langID)
    {
        this.langID = langID;
    }

    /**
     * Getters and setters for the test data entity.Ends
     */
}
