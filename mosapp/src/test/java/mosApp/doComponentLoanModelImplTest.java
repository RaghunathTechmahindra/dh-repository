package mosApp;

import junit.framework.Test;
import mosApp.MosSystem.SessionStateModelImpl;
import mosApp.MosSystem.doComponentLoanModelImpl;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLoan;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.ltx.unittest.base.BaseModelTest;
import com.ltx.unittest.util.EntityTestUtil;


/**
 * <p>
 * Title: docomponentLoanModelImplTest
 * </p>
 * Description: UnitTest class for mosApp.MosSystem.doComponentLoanModelImpl
 * class.
 * </p>
 * @author MCM Implementation Team
 * @version 1.0 12-06-2008 XS_16.7
 * @version 1.1 16-06-2008 XS_16.15 Updated for Loan Component.
 * @version 1.2 23-06-2008 XS_16.15 Rview Comment : Added assert for posted rate and additional details.
 * @version 1.3 8-07-2008 XS_2.37 Added assert for ComponentTypeId,MtgProdId,PaymentFrequencyId
 */
public class doComponentLoanModelImplTest extends BaseModelTest {

    // Instance Variables
    private doComponentLoanModelImpl doComponentLoanModelImpl;
    Deal dealData = null;
    Component componentData = null;
    Component component;
    ComponentLoan componentLoanData = null;
    ComponentLoan componentLoan;
    int langID;

    /**
     * Description: Loads the model and test data for the model.
     * @version 1.0 XS_16.7 Initial Version.
     */
    public doComponentLoanModelImplTest () {
        super("mosApp.MosSystem.doComponentLoanModelImpl");
        doComponentLoanModelImpl = (doComponentLoanModelImpl) model;
        dealData = (Deal) EntityTestUtil.getInstance()
                                        .loadEntity("com.basis100.deal.entity.Deal");
        componentData = (Component) EntityTestUtil.getInstance()
                                                  .loadEntity("com.basis100.deal.entity.Component");
        componentLoanData = (ComponentLoan) EntityTestUtil.getInstance()
                                                          .loadEntity("com.basis100.deal.entity.ComponentLoan");

    }

    /**
     * <p>
     * Description: This method is used to setup test data that will be used as
     * a part of this test
     * </P>
     * @version 1.0 XS_16.7 Initial Version.
     * @version 1.2 23-06-2008 XS_16.15 Rview Comment : Added assert for posted rate and additional details.
     */
    public void setupTestData () throws Exception {

        SessionStateModelImpl sessionaState = (SessionStateModelImpl) getSessionStateModel();
        sessionaState.setLanguageId(getLangID());

        MasterDealPK masterDealPK = getMasterDeal()
                                        .createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setRefiAdditionalInformation(dealData.getRefiAdditionalInformation());

        /** ****************component Data******************************** */
        component = (Component) EntityTestUtil.getInstance()
                                              .loadEntity("com.basis100.deal.entity.Component");
        component.create(deal.getDealId(), deal.getCopyId(),
                         componentData.getComponentTypeId(),
                         componentData.getMtgProdId());
        component.setPricingRateInventoryId(componentData.getPricingRateInventoryId());
        component.setRepaymentTypeId(componentData.getRepaymentTypeId());
        component.setPostedRate(componentData.getPostedRate());
        component.setAdditionalInformation(componentData.getAdditionalInformation());
        component.ejbStore();

        /** ****************componentLoan Data******************************** */
        componentLoan = (ComponentLoan) EntityTestUtil.getInstance()
                                                      .loadEntity("com.basis100.deal.entity.ComponentLoan");
        componentLoan.create(component.getComponentId(), component.getCopyId());
        componentLoan.setPaymentFrequencyId(componentLoanData.getPaymentFrequencyId());
        componentLoan.setLoanAmount(componentLoanData.getLoanAmount());
        componentLoan.setNetInterestRate(componentLoanData.getNetInterestRate());
        componentLoan.setDiscount(componentLoanData.getDiscount());
        componentLoan.setPremium(componentLoanData.getPremium());
        componentLoan.setActualPaymentTerm(componentLoanData.getActualPaymentTerm());
        componentLoan.setPAndIPaymentAmount(componentLoanData.getPAndIPaymentAmount());

        componentLoan.ejbStore();
        deal.ejbStore();

    }

    /**
     * <p>
     * Description: This method tests the model for the select statement.
     * </p>
     * @throws Exception
     */
    public void testdoComponentLoanModel () throws Exception {

        try {

            setupTestData();
            doComponentLoanModelImpl.addUserWhereCriterion("dfDealId", "=",
                                                           new Integer(deal.getDealId()));
            doComponentLoanModelImpl.addUserWhereCriterion("dfCopyId", "=",
                                                           new Integer(deal.getCopyId()));
            doComponentLoanModelImpl.executeSelect(null);

            assertEquals(doComponentLoanModelImpl.getDfComponentType(),
                         BXResources.getPickListDescription(deal.getInstitutionProfileId(),"COMPONENTTYPE",
                         componentData.getComponentTypeId(),getSessionStateModel().getLanguageId()));

            assertEquals(doComponentLoanModelImpl.getDfProductName(),
                         BXResources.getPickListDescription(deal.getInstitutionProfileId(),"MTGPROD",
                         componentData.getMtgProdId(),getSessionStateModel().getLanguageId()));

            assertEquals(doComponentLoanModelImpl.getDfLoanAmount().doubleValue(),componentLoan.getLoanAmount());
            assertEquals(doComponentLoanModelImpl.getDfNetInstrestRate().doubleValue(),componentLoan.getNetInterestRate());
            assertEquals(doComponentLoanModelImpl.getDfDiscount().doubleValue(),componentLoan.getDiscount());
            assertEquals(doComponentLoanModelImpl.getDfPremium().doubleValue(),componentLoan.getPremium());
            assertEquals(doComponentLoanModelImpl.getDfPostedRate().doubleValue(),component.getPostedRate());
            assertEquals(doComponentLoanModelImpl.getDfAdditionalDetails(),component.getAdditionalInformation());
            
            assertEquals(doComponentLoanModelImpl.getDfComponentTypeId().intValue(),component.getComponentTypeId());
            assertEquals(doComponentLoanModelImpl.getDfProductId().intValue(),component.getMtgProdId());
            assertEquals(doComponentLoanModelImpl.getDfPaymentFrequencyId().intValue(),componentLoan.getPaymentFrequencyId());

        } catch (Exception e) {

            throw e;

        } finally {

            tearDownTestData();

        }

    }

    /**
     * <p>
     * Description: deletes the master deal and all the associated entites.
     * </P>
     * @version 1.0 XS_16.7 Initial Version.
     */
    public void tearDownTestData () throws Exception {

        if (deal != null) {

            deal.dcm = null;
            // componentLoan.ejbRemove(false);
            component.ejbRemove(false);

        }

        try {

            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();

        } catch (RemoteException e) {

            e.printStackTrace();

        }

        System.out.println("ejb remove done");

    }

    public static Test suite () {

        return (Test) DDStepsSuiteFactory.createSuite(doComponentLoanModelImplTest.class);

    }

    /**
     * Getters and setters for the test data entity Starts
     */
    public Deal getDealData () {

        return dealData;

    }

    public void setDealData (Deal dealData) {

        this.dealData = dealData;

    }

    public Component getComponentData () {

        return componentData;

    }

    public void setComponentData (Component componentData) {

        this.componentData = componentData;

    }

    public ComponentLoan getComponentLoanData () {

        return componentLoanData;

    }

    public void setComponentLoanData (ComponentLoan componentLoanData) {

        this.componentLoanData = componentLoanData;

    }

    public int getLangID () {

        return langID;

    }

    public void setLangID (int langID) {

        this.langID = langID;

    }

    /**
     * Getters and setters for the test data entity Starts
     */
}
