package mosApp;

import junit.framework.Test;
import mosApp.MosSystem.SessionStateModelImpl;
import mosApp.MosSystem.doComponentLocModelImpl;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentLOC;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PricingRateInventory;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;
import com.basis100.deal.pk.PricingRateInventoryPK;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.ltx.unittest.base.BaseModelTest;
import com.ltx.unittest.util.EntityTestUtil;


/**
 * <p>Title: doComponentLocModelImplTest</p>
 *  <p>Description: UnitTest class for mosApp.MosSystem.doComponentLocModelImpl class.</p>
 *
 * @author MCM Implementation Team
 * @version 1.0 12-June-2008 XS_16.7 Initial Version.
 * @version 1.1 27-June-2008 XS_16.14 Updated for NetRate,PostedRate
 */
public class doComponentLocModelImplTest extends BaseModelTest {
    // Instance Variables

    private doComponentLocModelImpl doComponentLocModelImpl;


    Deal dealData = null;
    Component componentData = null;
    Component component;
    ComponentLOC componentLocData = null;
    ComponentLOC componentLoc;
    PricingRateInventory pricingRateInvData = null;
    PricingRateInventory pricingRateInv;
    int langID;

    /**
     * Description: Loads the model and test data for the model.
     * @version 1.0 XS_16.7 Initial Version.
     * @version 1.1 XS_16.14 Added PricingRateInventory
     */
    public doComponentLocModelImplTest() {
        super("mosApp.MosSystem.doComponentLocModelImpl");

        doComponentLocModelImpl = (doComponentLocModelImpl) model;
        dealData = (Deal) EntityTestUtil.getInstance()
        .loadEntity("com.basis100.deal.entity.Deal");
        componentData = (Component) EntityTestUtil.getInstance()
        .loadEntity("com.basis100.deal.entity.Component");
        componentLocData = (ComponentLOC) EntityTestUtil.getInstance()
        .loadEntity("com.basis100.deal.entity.ComponentLOC");
        pricingRateInvData = (PricingRateInventory) EntityTestUtil.getInstance()
        .loadEntity("com.basis100.deal.entity.PricingRateInventory");
    } // end doComponentLocModelImplTest()

    /**
     * <p>
     * Description: This method is used to setup test data that will be used as
     * a part of this test
     * </P>
     * @version 1.0 XS_16.7 Initial Version.
     * @version 1.1 XS_16.14 Added setPostedRate,pricingRateInv
     */
    public void setupTestData() throws Exception {
        SessionStateModelImpl sessionaState = (SessionStateModelImpl) getSessionStateModel();
        sessionaState.setLanguageId(getLangID());

        MasterDealPK masterDealPK = getMasterDeal()
        .createPrimaryKey(getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setRefiAdditionalInformation(dealData.getRefiAdditionalInformation());
        deal.ejbStore();
        // Component Data
        component = (Component) EntityTestUtil.getInstance()
        .loadEntity("com.basis100.deal.entity.Component");
        component.create(deal.getDealId(), deal.getCopyId(),
                componentData.getComponentTypeId(),
                componentData.getMtgProdId());
        component.setPricingRateInventoryId(componentData.getPricingRateInventoryId());
        component.setRepaymentTypeId(componentData.getRepaymentTypeId());
        component.setPostedRate(componentData.getPostedRate());
        component.setAdditionalInformation(componentData.getAdditionalInformation());
        component.ejbStore();

        // ComponentMortgage
        componentLoc = (ComponentLOC) EntityTestUtil.getInstance()
        .loadEntity("com.basis100.deal.entity.ComponentLOC");
        componentLoc.create(component.getComponentId(), component.getCopyId());
        componentLoc.setPaymentFrequencyId(componentLocData.getPaymentFrequencyId());
        componentLoc.setPrePaymentOptionsId(componentLocData.getPrePaymentOptionsId());
        componentLoc.setPrivilegePaymentId(componentLocData.getPrivilegePaymentId());
        componentLoc.setTotalLocAmount(componentLocData.getTotalLocAmount());
        componentLoc.setMiAllocateFlag(componentLocData.getMiAllocateFlag());
        componentLoc.setTotalPaymentAmount(componentLocData.getTotalPaymentAmount());
        componentLoc.setPropertyTaxAllocateFlag(componentLocData.getPropertyTaxAllocateFlag());
        componentLoc.setNetInterestRate(componentLocData.getNetInterestRate());
        componentLoc.setDiscount(componentLocData.getDiscount());
        componentLoc.setPremium(componentLocData.getPremium());

        componentLoc.setCommissionCode(componentLocData.getCommissionCode());
        componentLoc.setExistingAccountNumber(componentLocData.getExistingAccountNumber());
        componentLoc.setLocAmount(componentLocData.getLocAmount());
        componentLoc.setPaymentFrequencyId(componentLocData.getPaymentFrequencyId());
        //componentLoc.setPropertyTaxEscrowAmount(componentLocData.getPropertyTaxEscrowAmount());
        componentLoc.setLocAmount(componentLocData.getLocAmount());

        componentLoc.ejbStore();

       
        //PricingRateInventory
        pricingRateInv = (PricingRateInventory) EntityTestUtil.getInstance()
        .loadEntity("com.basis100.deal.entity.PricingRateInventory");
        PricingRateInventoryPK pricingRateInvPK = new PricingRateInventoryPK(component.getPricingRateInventoryId());
        pricingRateInv.findByPrimaryKey(pricingRateInvPK);
        pricingRateInv.setInternalRatePercentage(pricingRateInvData.getInternalRatePercentage());
        pricingRateInv.ejbStore();
    } // end setupTestData()

    /**
     * <p>Description: This method tests the model for the select
     * statement.</p>
     *
     * @throws Exception
     */
    public void testdoComponentLocModel() throws Exception {
        try {
            setupTestData();
            doComponentLocModelImpl.addUserWhereCriterion("dfDealId", "=",
                    new Integer(deal.getDealId()));
            doComponentLocModelImpl.addUserWhereCriterion("dfCopyId", "=",
                    new Integer(deal.getCopyId()));
            doComponentLocModelImpl.executeSelect(null);

            assertEquals(doComponentLocModelImpl.getDfComponentType(),BXResources.getPickListDescription(
                    deal.getInstitutionProfileId(),"COMPONENTTYPE",componentData.getComponentTypeId(),getSessionStateModel().getLanguageId()));
            assertEquals(doComponentLocModelImpl.getDfProductName(),BXResources.getPickListDescription(
                    deal.getInstitutionProfileId(),"MTGPROD",componentData.getMtgProdId(),getSessionStateModel().getLanguageId()));

            assertEquals(doComponentLocModelImpl.getDfTotalLocAmount().doubleValue(),componentLocData.getTotalLocAmount());
            assertEquals(doComponentLocModelImpl.getDfMiAllocateFlag(), "Y");
            assertEquals(doComponentLocModelImpl.getDfTotalPaymentAmount().doubleValue(),componentLocData.getTotalPaymentAmount());
           // assertEquals(doComponentLocModelImpl.getDfPropertyTaxFlag(), "N");
            assertEquals(doComponentLocModelImpl.getDfNetInstrestRate().doubleValue(),componentLocData.getNetInterestRate());
            assertEquals(doComponentLocModelImpl.getDfDiscount().doubleValue(),componentLocData.getDiscount());
            assertEquals(doComponentLocModelImpl.getDfPremium().doubleValue(), componentLocData.getPremium());

            //for XS 2.33/34a
            //assertEquals(doComponentLocModelImpl.getDfPremium().doubleValue(), componentData.getAdditionalInformation());
            assertEquals(doComponentLocModelImpl.getDfCommissionCode(),componentLocData.getCommissionCode());
            assertEquals(doComponentLocModelImpl.getDfExistingAccountNumber().toString(),componentLocData.getExistingAccountNumber());
            //assertEquals(doComponentLocModelImpl.getDfDiscount().doubleValue(),componentLocData.getLocAmount());

            assertEquals(doComponentLocModelImpl.getDfPaymentFrequencyId().intValue(), componentLocData.getPaymentFrequencyId());
            //assertEquals(doComponentLocModelImpl.getDfProFlag(), componentLocData.getPropertyTaxEcrowAmount());

            assertEquals(doComponentLocModelImpl.getDfLocAmount().doubleValue(),componentLocData.getLocAmount());
            
            /** ******************MCM IMPL XS_16.14 STARTS****************************************** */
            assertEquals(doComponentLocModelImpl.getDfNetRate().doubleValue(),componentData.getPostedRate());
            assertEquals(doComponentLocModelImpl.getDfPostedRate().doubleValue(),pricingRateInvData.getInternalRatePercentage());
            /** ******************MCM IMPL XS_16.14 ENDS****************************************** */
        } 
        catch (Exception e) {
            throw e;
        } 
        finally {
            tearDownTestData();
        } 
    } // end testdoComponentLocModel()

    /**
     * <p>
     * Description: deletes the master deal and all the associated entites.
     * </P>
     * @version 1.0 XS_16.7 Initial Version.
     */
    public void tearDownTestData() throws Exception {
        if (deal != null) {
            deal.dcm = null;
            // componentLoc.ejbRemove(false);
            component.ejbRemove(false);
        } 

        try {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        } 
        catch (RemoteException e) {
            e.printStackTrace();
        } 

        System.out.println("ejb remove done");
    } // end tearDownTestData()

    /**
     * 
     * Getters and setters for the test Data entity Starts
     */
    public static Test suite() {
        return (Test) DDStepsSuiteFactory.createSuite(doComponentLocModelImplTest.class);
    } 


    public Deal getDealData() {
        return dealData;
    } 


    public void setDealData(Deal dealData) {
        this.dealData = dealData;
    } 


    public Component getComponentData() {
        return componentData;
    } 


    public void setComponentData(Component componentData) {
        this.componentData = componentData;
    } 


    public ComponentLOC getComponentLocData() {
        return componentLocData;
    } 


    public void setComponentLocData(ComponentLOC componentLocData) {
        this.componentLocData = componentLocData;
    } 


    public int getLangID() {
        return langID;
    } 


    public void setLangID(int langID) {
        this.langID = langID;
    } 
    /**
     * 
     * Getters and setters for the test Data entity Starts
     */

    public PricingRateInventory getPricingRateInvData()
    {
        return pricingRateInvData;
    }

    public void setPricingRateInvData(PricingRateInventory pricingRateInvData)
    {
        this.pricingRateInvData = pricingRateInvData;
    }
} // end doComponentLocModelImplTest
