/*
 * @(#)SQLConnectionManagerImplTest.java    2007-7-31
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package mosApp;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * SQLConnectionManagerImplTest - unit test case for
 * <code>SQLConnectionManagerImpl</code>.
 *
 * @version   1.0 2007-7-31
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class SQLConnectionManagerImplTest extends TestCase {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(SQLConnectionManagerImplTest.class);

    /**
     * choosing the testcase you want to test.
     */
    public static Test suite() {

        // Run all test cases.
        TestSuite suite = new TestSuite(SQLConnectionManagerImplTest.class);
        // Run selected test cases.
        //TestSuite suite = new TestSuite();
        //suite.addTest(new SQLConnectionManagerImplTest("testMethodName"));

        return suite;
    }

    /**
     * Constructor function
     */
    public SQLConnectionManagerImplTest() {
    }

    /**
     * constructs a test case with the given test case name.
     */
    public SQLConnectionManagerImplTest(String name) {
        super(name);
    }

    /**
     * test get connection.
     */
    public void testGetConnection() throws Exception {

        Connection connection =
            new SQLConnectionManagerImpl().getConnection("jdbc/orcl");
        assertTrue(null != connection);
        Statement statement = connection.createStatement();
        assertTrue(null != statement);
        String query = "SELECT count(*) from USERPROFILE";
        _log.info("Executing Query: " + query);
        ResultSet result = statement.executeQuery(query);
        assertTrue(null != result);
        assertTrue(result.next());
        int count = result.getInt(1);
        assertTrue(count > 0);
        _log.info("Amount of user: " + count);

        result.close();
        statement.close();
        connection.close();
    }

    /**
     * setting up the testing env.
     */
    protected void setUp() {
    }

    /**
     * clean the testing env.
     */
    protected void tearDown() {
    }
}