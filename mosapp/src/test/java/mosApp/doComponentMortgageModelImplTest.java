package mosApp;

import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.ComponentMortgage;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.MasterDealPK;

import com.basis100.entity.RemoteException;

import com.basis100.picklist.BXResources;

import com.ltx.unittest.base.BaseModelTest;
import com.ltx.unittest.util.EntityTestUtil;

import junit.framework.Test;

import mosApp.MosSystem.SessionStateModelImpl;
import mosApp.MosSystem.doComponentMortgageModelImpl;

import org.ddsteps.junit.suite.DDStepsSuiteFactory;

/**
 * <p>
 * Title: doDealEntryMainSelectModelImplTest
 * </p>
 * <p>
 * Description: UnitTest class for mosApp.MosSystem.doComponentMortgageModelImpl
 * class.
 * </p>
 * @author MCM Implementation Team
 * @version 1.0 12-June-2008 XS_16.7 Initial Version
 * @version 1.1 18-June-2008 XS_2.27 and 28a - added fields for componentMortgage section
 *  - modified setupTestData() and testdoComponentMortgageModel()
 */
public class doComponentMortgageModelImplTest extends BaseModelTest
{
    // Instance Variables

    private doComponentMortgageModelImpl doComponentMortgageModelImpl;

    Deal dealData = null;

    Component componentData = null;

    Component component;

    ComponentMortgage componentMortgageData = null;

    ComponentMortgage componentMortgage;

    int langID;

    /**
     * Description: Loads the model and test data for the model.
     * @version 1.0 XS_16.7 Initial Version.
     */
    public doComponentMortgageModelImplTest()
    {
        super("mosApp.MosSystem.doComponentMortgageModelImpl");
        EntityTestUtil.getSessionResourceKit().getExpressState()
                .setDealInstitutionId(1);
        doComponentMortgageModelImpl = (doComponentMortgageModelImpl) model;
        dealData = (Deal) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Deal");
        componentData = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        componentMortgageData = (ComponentMortgage) EntityTestUtil
                .getInstance().loadEntity(
                        "com.basis100.deal.entity.ComponentMortgage");
    }

    /**
     * <p>
     * Description: This method is used to setup test data that will be used as
     * a part of this test
     * </P>
     * @version 1.0 XS_16.7 Initial Version.
     * @version 1.1 18-June-2008 XS_2.27 and 28a - added fields for componentMortgage section
     */
    public void setupTestData() throws Exception
    {
        SessionStateModelImpl sessionaState = (SessionStateModelImpl) getSessionStateModel();
        sessionaState.setLanguageId(getLangID());

        MasterDealPK masterDealPK = getMasterDeal().createPrimaryKey(
                getDealInstitutionId());
        deal = getMasterDeal().create(masterDealPK);
        dealPK = (DealPK) deal.getPk();
        deal.setScenarioNumber(1);
        deal.setCopyType("G");
        deal.setRefiAdditionalInformation(dealData
                .getRefiAdditionalInformation());

        /** ****************Component Data******************************** */
        component = (Component) EntityTestUtil.getInstance().loadEntity(
                "com.basis100.deal.entity.Component");
        component.create(deal.getDealId(), deal.getCopyId(), componentData
                .getComponentTypeId(), componentData.getMtgProdId());
        component.setPricingRateInventoryId(componentData.getPricingRateInventoryId());
        component.setRepaymentTypeId(componentData.getRepaymentTypeId());
        component.setPostedRate(componentData.getPostedRate());
        component.setAdditionalInformation(componentData.getAdditionalInformation());
        component.ejbStore();

        /**
         * ****************componentMortgage
         * Data********************************
         */
        componentMortgage = (ComponentMortgage) EntityTestUtil.getInstance()
                .loadEntity("com.basis100.deal.entity.ComponentMortgage");
        componentMortgage.create(component.getComponentId(), component.getCopyId());
        componentMortgage.setPaymentFrequencyId(componentMortgageData.getPaymentFrequencyId());
        componentMortgage.setPrePaymentOptionsId(componentMortgageData.getPrePaymentOptionsId());
        componentMortgage.setPrivilegePaymentId(componentMortgageData.getPrivilegePaymentId());
        componentMortgage.setCashBackAmountOverride(componentMortgageData.getCashBackAmountOverride());
        componentMortgage.setRateLock(componentMortgageData.getRateLock());
        componentMortgage.setTotalMortgageAmount(componentMortgageData.getTotalMortgageAmount());
        componentMortgage.setMiAllocateFlag(componentMortgageData.getMiAllocateFlag());
        componentMortgage.setTotalPaymentAmount(componentMortgageData.getTotalPaymentAmount());
        componentMortgage.setPropertyTaxAllocateFlag(componentMortgageData.getPropertyTaxAllocateFlag());
        componentMortgage.setNetInterestRate(componentMortgageData.getNetInterestRate());
        componentMortgage.setDiscount(componentMortgageData.getDiscount());
        componentMortgage.setPremium(componentMortgageData.getPremium());
        componentMortgage.setBuyDownRate(componentMortgageData.getBuyDownRate());
        
        
        componentMortgage.setMortgageAmount(componentMortgageData.getMortgageAmount());
        componentMortgage.setPropertyTaxEscrowAmount(componentMortgageData.getPropertyTaxEscrowAmount());
        componentMortgage.setExistingAccountNumber(componentMortgageData.getExistingAccountNumber());
        componentMortgage.setEffectiveAmortizationMonths(componentMortgageData.getEffectiveAmortizationMonths());
        componentMortgage.setCommissionCode(componentMortgageData.getCommissionCode());
        componentMortgage.setCashBackPercent(componentMortgageData.getCashBackPercent());
        componentMortgage.setCashBackAmount(componentMortgageData.getCashBackAmount());
        componentMortgage.setAmortizationTerm(componentMortgageData.getAmortizationTerm());
        componentMortgage.setAdvanceHold(componentMortgageData.getAdvanceHold());
        componentMortgage.setAdditionalPrincipal(componentMortgageData.getAdditionalPrincipal());        
        //componentMortgage.setActualPaymentTerm(componentMortgageData.getActualPaymentTerm());
        componentMortgage.setRateGuaranteePeriod(componentMortgageData.getRateGuaranteePeriod());
        
        componentMortgage.ejbStore();
        deal.ejbStore();
    }

    /**
     * <p>
     * Description: This method tests the model for the select statement.
     * </p>
     * @throws Exception
     * @version 1.1 18-June-2008 XS_2.27 and 28a - added fields for componentMortgage section
     */
    public void testdoComponentMortgageModel() throws Exception
    {
        try
        {
            setupTestData();
            doComponentMortgageModelImpl.addUserWhereCriterion("dfDealId", "=",
                    new Integer(deal.getDealId()));
            doComponentMortgageModelImpl.addUserWhereCriterion("dfCopyId", "=",
                    new Integer(deal.getCopyId()));
            doComponentMortgageModelImpl.executeSelect(null);

            String taxFlag;
            String miFlag;

            if (componentMortgageData.getPropertyTaxAllocateFlag().equals("Y"))
            {
                taxFlag = "Yes";
            }
            else
            {
                taxFlag = "No";
            }

            if (componentMortgageData.getMiAllocateFlag().equals("Y"))
            {
                miFlag = "Yes";
            }
            else
            {
                miFlag = "No";
            }

            assertEquals(doComponentMortgageModelImpl.getDfComponentType(),BXResources.getPickListDescription
            		(deal.getInstitutionProfileId(), "COMPONENTTYPE",componentData.getComponentTypeId(),getSessionStateModel().getLanguageId()));
            assertEquals(doComponentMortgageModelImpl.getDfProductName(),BXResources.getPickListDescription
            		(deal.getInstitutionProfileId(), "MTGPROD",componentData.getMtgProdId(),getSessionStateModel().getLanguageId()));

            assertEquals(doComponentMortgageModelImpl.getDfTotalMortgageAmount().doubleValue(),componentMortgageData.getTotalMortgageAmount());
            assertEquals(doComponentMortgageModelImpl.getDfMiAllocateFlag(),miFlag);
            assertEquals(doComponentMortgageModelImpl.getDfTotalPaymentAmount().doubleValue(), componentMortgageData.getTotalPaymentAmount());
            assertEquals(doComponentMortgageModelImpl.getDfPropertyTaxFlag(),taxFlag);
            assertEquals(doComponentMortgageModelImpl.getDfNetInstrestRate().doubleValue(), componentMortgageData.getNetInterestRate());
            assertEquals(doComponentMortgageModelImpl.getDfDiscount().doubleValue(), componentMortgageData.getDiscount());
            assertEquals(doComponentMortgageModelImpl.getDfPremium().doubleValue(), componentMortgageData.getPremium());
            assertEquals(doComponentMortgageModelImpl.getDfBuyDownRate().doubleValue(), componentMortgageData.getBuyDownRate());
            
            //For 2.27 and 28.a
            assertEquals(doComponentMortgageModelImpl.getDfAdditionalInformation(),component.getAdditionalInformation());
            assertEquals(doComponentMortgageModelImpl.getDfMortgageAmount().doubleValue(),componentMortgageData.getMortgageAmount());
            assertEquals(doComponentMortgageModelImpl.getDfRateGuaranteePeriod().intValue(),componentMortgageData.getRateGuaranteePeriod());
            assertEquals(doComponentMortgageModelImpl.getDfEscrowPaymentAmount().doubleValue(),componentMortgageData.getPropertyTaxEscrowAmount());
            assertEquals(doComponentMortgageModelImpl.getDfExsitingAccountNumber(),componentMortgageData.getExistingAccountNumber());
            assertEquals(doComponentMortgageModelImpl.getDfEffectiveAmortizationInMonths().intValue(),componentMortgageData.getEffectiveAmortizationMonths());
            assertEquals(doComponentMortgageModelImpl.getDfCommissionCode(),componentMortgageData.getCommissionCode());
            assertEquals(doComponentMortgageModelImpl.getDfCashbackPercentage().doubleValue(),componentMortgageData.getCashBackPercent());
            assertEquals(doComponentMortgageModelImpl.getDfCashbackAmount().doubleValue(),componentMortgageData.getCashBackAmount());
            assertEquals(doComponentMortgageModelImpl.getDfAmortizationTerm().intValue(),componentMortgageData.getAmortizationTerm());
            assertEquals(doComponentMortgageModelImpl.getDfAdvancedHold().doubleValue(),componentMortgageData.getAdvanceHold());
            assertEquals(doComponentMortgageModelImpl.getDfAdditionalPrincipal().doubleValue(),componentMortgageData.getAdditionalPrincipal());
            //assertEquals(doComponentMortgageModelImpl.getDfActualPaymentTerm().intValue(),componentMortgageData.getActualPaymentTerm());
            
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            tearDownTestData();
        }
    }

    /**
     * <p>
     * Description: deletes the master deal and all the associated entites.
     * </P>
     * @version 1.0 XS_16.7 Initial Version.
     */
    public void tearDownTestData() throws Exception
    {
        if (deal != null)
        {
            deal.dcm = null;
            component.ejbRemove(false);
        }

        try
        {
            getMasterDeal().dcm = null;
            getMasterDeal().removeDeal();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }

        System.out.println("ejb remove done");
    }

    /**
     * Getters and setters for the test data entity starts
     */
    public static Test suite()
    {
        return (Test) DDStepsSuiteFactory
                .createSuite(doComponentMortgageModelImplTest.class);
    }

    public Deal getDealData()
    {
        return dealData;
    }

    public void setDealData(Deal dealData)
    {
        this.dealData = dealData;
    }

    public Component getComponentData()
    {
        return componentData;
    }

    public void setComponentData(Component componentData)
    {
        this.componentData = componentData;
    }

    public ComponentMortgage getComponentMortgageData()
    {
        return componentMortgageData;
    }

    public void setComponentMortgageData(ComponentMortgage componentMortgageData)
    {
        this.componentMortgageData = componentMortgageData;
    }

    public int getLangID()
    {
        return langID;
    }

    public void setLangID(int langID)
    {
        this.langID = langID;
    }
    /**
     * Getters and setters for the test data entity ends
     */
}
