<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="date" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:date="http://exslt.org/dates-and-times" xmlns:fcx="http://www.filogix.com/Schema/FCX/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:inb="http://genericlenderanyfinal.filogix.com/InboundFCXServerAnyFinal/">
	<xsl:output method="xml" indent="yes"/>
	<!--  #################################################################################
		  ############################## MAIN TEMPLATE -START #############################
	      #################################################################################
-->
	<xsl:template match="/">
		<soap:Envelope>
			<soap:Body>
				<inb:XMLResponseDealFCXType>
					<Document>
						<Deal>
				<!-- The following two fields are only used for debugging purposes:
				<useLegacyFlag><xsl:value-of select="$useLegacyFlag"/></useLegacyFlag>
				<useSubAgentFlag><xsl:value-of select="$useSubAgentFlag"/></useSubAgentFlag>	-->
							<InstitutionProfile>
								<ecniLenderId>
									<xsl:value-of select="$ecniLenderId"/>
								</ecniLenderId>
							</InstitutionProfile>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup">
								<xsl:call-template name="applicantGroup"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:lifeAndDisability/fcx:nonMortgageApplicant">
								<xsl:call-template name="InsureOnlyApplicant"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property">
								<xsl:call-template name="property"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:deal/fcx:downPaymentSource">
								<xsl:call-template name="downPaymentSource"/>
							</xsl:for-each>
							<xsl:if test="$isEscrowPayment='Y'">
								<xsl:call-template name="escrowPayment"/>
							</xsl:if>
							<xsl:if test="$isEscrowPayment2 = 'true'">
								<xsl:call-template name="escrowPayment2"/>
							</xsl:if>
							<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:originatorNotes">
								<xsl:call-template name="dealNotesLenderSubmission"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant">
								<xsl:call-template name="dealNotesApplicantInfo"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property">
								<xsl:call-template name="dealNotesSubjectProperty"/>
							</xsl:for-each>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Do not create deal notes extensions:
							<xsl:for-each select="//fcx:loanApplication/fcx:deal/fcx:extension">
								<xsl:call-template name="dealNotesDealExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:extension">
								<xsl:call-template name="dealNotesApplicantExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:asset/fcx:extension">
								<xsl:call-template name="dealNotesAssetExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:liability/fcx:broker/fcx:extension">
								<xsl:call-template name="dealNotesBrokerExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:liability/fcx:creditBureau/fcx:extension">
								<xsl:call-template name="dealNotesCBExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:employmentHistory/fcx:extension">
								<xsl:call-template name="dealNotesEmploymentExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:employmentHistory/fcx:income/fcx:extension">
								<xsl:call-template name="dealNotesIncomeExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:creditBureauReport/fcx:extension">
								<xsl:call-template name="dealNotesCBReportExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:lifeAndDisabilityApplication/fcx:extension">
								<xsl:call-template name="dealNotesLifeExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:extension/fcx:*">
								<xsl:call-template name="dealNotesMortgageExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:participant/fcx:extension/fcx:*">
								<xsl:call-template name="dealNotesParticipantExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:extension/fcx:*">
								<xsl:call-template name="dealNotesSubjectPropertyPropertyExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:extension/fcx:*">
								<xsl:call-template name="dealNotesSubjectPropertyExistingMortgageExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:extension/fcx:*">
								<xsl:call-template name="dealNotesOtherPropertyPropertyExtension"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:mortgage/fcx:extension/fcx:*">
								<xsl:call-template name="dealNotesOtherPropertyMortgageExtension"/>
							</xsl:for-each>	-->
							<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:taxationYear">
								<xsl:call-template name="dealNotesTaxYearExtension"/>
							</xsl:for-each>
							<xsl:choose>
								<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mtgProd/fcx:mpBusinessId 
									or //fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:rate/fcx:netRate
								 	or //fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:PAndIPaymentAmount">
									<xsl:call-template name="dealNotesPIExtension"/>
								</xsl:when>
							</xsl:choose>
							<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:paymentTermDd">
								<xsl:call-template name="dealNotesMortgagePayment"/>
							</xsl:for-each>
							<!-- Express 4.4 Remove - Start -->
							<!--
							<xsl:for-each select="//fcx:loanApplication/fcx:deal/fcx:financingWaiverDate">
								<xsl:call-template name="dealNotesWaiverDate"/>
							</xsl:for-each>
							-->
							<!-- Express 4.4 Remove - End -->
							<xsl:choose>
								<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:marketSubmissionNumber
									and //fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:marketSubmission ='Y'">
									<xsl:call-template name="dealNotesMarketSubmission"/>
								</xsl:when>
							</xsl:choose>
							<!-- 4.4.1 GOLD COPY FEB. 2012: The following is a new deal note: -->
							<xsl:choose>	<!-- Condo fee includes heat -->
								<xsl:when test="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:feesIncludeHeat">
									<xsl:call-template name="dealNotesFeeIncludesHeat"/>
								</xsl:when>
							</xsl:choose>
							<!-- 4.4.1 GOLD COPY FEB. 2012: (FXP29501) Removed, information already in the liabilities:
							<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property">
								<xsl:call-template name="dealNotesOtherProperty"/>
							</xsl:for-each>	-->
							<xsl:for-each select="//fcx:loanApplication/fcx:notes">
								<xsl:call-template name="dealNotesLoanApplication"/>
							</xsl:for-each>
							<xsl:call-template name="creditBureauReport"/>
							<xsl:if test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:firmProfile)))">
								<xsl:for-each select="//fcx:loanApplication/fcx:deal/fcx:firmProfile">
									<xsl:call-template name="firmProfile"/>
								</xsl:for-each>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile)))">
									<xsl:for-each select="//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile">
										<xsl:call-template name="sourceOfBusinessProfile"/>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="//fcx:loanApplication/fcx:deal/fcx:agentProfile">
										<xsl:call-template name="sourceOfBusinessProfile"/>
									</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
							<!-- Express 4.4 - Start -->
							<xsl:for-each select="//fcx:loanApplication/fcx:deal/fcx:agentProfile">
								<xsl:call-template name="sourceOfBusinessProfileAgent"/>
							</xsl:for-each>
							<!-- Express 4.4 - End -->
							<xsl:for-each select="//fcx:loanApplication/fcx:participant">
								<xsl:if test="fcx:partyTypeDd != -1">
									<xsl:call-template name="partyProfile"/>
								</xsl:if>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mtgProd">
								<MtgProd>
									<mpBusinessId>
										<xsl:value-of select="fcx:mpBusinessId"/>
									</mpBusinessId>
								</MtgProd>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:pricingRateInventory">
								<PricingRateInventory>
									<indexEffectiveDate>
										<xsl:value-of select="fcx:indexEffectiveDate"/>
									</indexEffectiveDate>
								</PricingRateInventory>
							</xsl:for-each>
							<LenderProfile>
								<lpBusinessId>
									<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:lenderProfile/fcx:lenderCode"/>
								</lpBusinessId>
							</LenderProfile>
							<applicationDate>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:submitRequestTime"/>
							</applicationDate>
							<amortizationTerm>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:amortizationTerm"/>
							</amortizationTerm>
							<xsl:call-template name="estimatedClosingDateValue"/>
							<dealTypeId>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:dealTypeDd"/>
							</dealTypeId>
							<sourceApplicationId>
								<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (605) has been changed, template not longer necessary:
								<xsl:call-template name="sourceApplicationIdValue"/>	-->
								<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (605) is now a direct mapping: -->
								 <xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:applicationId"/>
							</sourceApplicationId>
							<netLoanAmount>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:netLoanAmount"/>
							</netLoanAmount>
							<discount>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:rate/fcx:discount"/>
							</discount>
							<secondaryFinancing>
								<xsl:call-template name="secondaryFinancingValue"/>
							</secondaryFinancing>
							<originalMortgageNumber>
							<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (617) has been changed, not longer direct mapping: 
								<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:mortgage/fcx:currentMortgageNumber"/>	-->
							<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (617) now uses a template: -->	
								<xsl:call-template name="originalMortgageNumberValue"/>
							</originalMortgageNumber>
							<netInterestRate>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:rate/fcx:netRate"/>
							</netInterestRate>
							<dealPurposeId>
								<xsl:call-template name="dealPurposeIdValue"/>
							</dealPurposeId>
							<buydownRate>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:rate/fcx:buyDownRate"/>
							</buydownRate>
							<specialFeatureId>
								<xsl:call-template name="specialFeatureIdValue"/>
							</specialFeatureId>
							<lineOfBusinessId>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lineOfBusinessDd"/>
							</lineOfBusinessId>
							<mortgageInsurerId>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mtgProviderId"/>
							</mortgageInsurerId>
							<MARSNameSearch>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:lenderSpecificData/fcx:forceDuplicateSubmissionFlag"/>
							</MARSNameSearch>
							<refExistingMtgNumber>
							<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (641) has been changed, not longer direct mapping: 
								<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:currentMortgageNumber"/>  -->
							<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (641) now uses a template: -->
								<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage">
									<xsl:call-template name="refExistingMtgNumberValue"/>
								</xsl:for-each>
							</refExistingMtgNumber>
							<interestTypeId>
								<xsl:call-template name="interestTypeIdValue"/>
							</interestTypeId>
							<xsl:call-template name="paymentFrequencyIdValue"/>
							<prePaymentOptionsId>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:prepaymentOptionsDd"/>
							</prePaymentOptionsId>
							<electronicSystemSource>
								<xsl:call-template name="electronicSystemSourceValue"/>
							</electronicSystemSource>
							<requestedSolicitorName>
								<xsl:call-template name="requestedSolicitorNameValue"/>
							</requestedSolicitorName>
							<interimInterestAdjustmentDate>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:interestAdjustmentDate"/>
							</interimInterestAdjustmentDate>
							<lienPositionId>
								<!-- <xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd"/>
-->
								<!-- Ticket FXP26603: -->
								<xsl:call-template name="lienPositionIdValue"/>
							</lienPositionId>
							<MITypeId>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mtgProductId"/>
							</MITypeId>
							<preApproval>
								<xsl:call-template name="preApprovalValue"/>
							</preApproval>
							<premium>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:rate/fcx:premium"/>
							</premium>
							<refiCurrentBal>
								<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:balanceRemaining"/>
							</refiCurrentBal>
							<sourceSystemMailBoxNBR>
								<xsl:call-template name="sourceSystemMailBoxNBRValue"/>
							</sourceSystemMailBoxNBR>
							<xsl:call-template name="taxPayorIdValue"/>
							<additionalPrincipal>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:additionalPrincipal"/>
							</additionalPrincipal>
							<actualPaymentTerm>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:actualPaymentTerm"/>
							</actualPaymentTerm>
							<MIUpfront>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mtgInsIncludeFlag"/>
							</MIUpfront>
							<repaymentTypeId>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:repaymentTypeDd"/>
							</repaymentTypeId>
							<systemTypeId>
								<xsl:call-template name="systemTypeIdValue-deal"/>
							</systemTypeId>
							<existingLoanAmount>
								<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:balanceRemaining"/>
							</existingLoanAmount>
							<MIExistingPolicyNumber>
							<!-- 4.4.1 GOLD COPY FEB. 2012: (FXP32289)Rule template (769) has been modified and for-each also added: -->
								<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage">
									<xsl:call-template name="MIExistingPolicyNumberValue"/>
								</xsl:for-each>
							</MIExistingPolicyNumber>
							<PAPurchasePrice>
								<xsl:call-template name="PAPurchasePriceValue"/>
							</PAPurchasePrice>
							<checkNotesFlag>
								<xsl:value-of select="$checkNotesFlagValue"/>
							</checkNotesFlag>
							<progressAdvance>
								<xsl:call-template name="progressAdvanceValue"/>
							</progressAdvance>
							<refiPurpose>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:refiPurpose"/>
							</refiPurpose>
							<refiOrigPurchasePrice>
								<xsl:call-template name="refiOrigPurchasePriceValue"/>
							</refiOrigPurchasePrice>
							<refiBlendedAmortization>
								<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:refiBlendedAmortization"/>
							</refiBlendedAmortization>
							<refiOrigMtgAmount>
								<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:originalMortgageAmount"/>
							</refiOrigMtgAmount>
							<refiImprovementsDesc>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:refiImprovementsDesc"/>
							</refiImprovementsDesc>
							<refiImprovementAmount>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:refiImprovementAmount"/>
							</refiImprovementAmount>
							<refiOrigPurchaseDate>
								<xsl:call-template name="refiOrigPurchaseDateValue"/>
							</refiOrigPurchaseDate>
							<refiCurMortgageHolder>
								<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:existingMortgageHolder"/>
							</refiCurMortgageHolder>
							<mccMarketType>
								<xsl:call-template name="mccMarketTypeValue"/>
							</mccMarketType>
							<multiProject>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:lenderSpecificData/fcx:multiProject"/>
							</multiProject>
							<proprietairePlus>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:lenderSpecificData/fcx:proprietairePlus"/>
							</proprietairePlus>
							<proprietairePlusLOC>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:lenderSpecificData/fcx:proprietairePlusLOC"/>
							</proprietairePlusLOC>
							<sourceApplicationVersion>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:dealId"/>
							</sourceApplicationVersion>
							<productTypeId>
								<xsl:call-template name="productTypeIdValue"/>
							</productTypeId>
							<rateGuaranteePeriod>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:rateGuaranteeLength"/>
							</rateGuaranteePeriod>
							<cashBackAmount>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:cashBackAmt"/>
							</cashBackAmount>
							<cashBackPercent>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:cashBackPercentage"/>
							</cashBackPercent>
							<xsl:call-template name="affiliationProgramIdValue"/>
							<cashBackAmountOverride>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:cashBackOverride"/>
							</cashBackAmountOverride>
							<refiAdditionalInformation>
								<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:refiAdditionalInformation"/>
							</refiAdditionalInformation>
							<!-- 4.4.1 GOLD COPY FEB. 2012: Rule template (816) is now inside a for-each: -->
							<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage">
								<xsl:call-template name="refiProductTypeIdValue"/>
							</xsl:for-each>
							<locRepaymentTypeId>
								<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:locRepaymentTypeDd"/>
							</locRepaymentTypeId>
							<!-- Express 4.4 - Start -->
							<financingWaiverDate>
								<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:financingWaiverDate"/>
							</financingWaiverDate>
							<!-- Express 4.4 - End -->
							<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:qualifyDetail">
								<xsl:call-template name="qualifyDetail"/>
							</xsl:for-each>
							<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:component">
								<xsl:call-template name="component"/>
							</xsl:for-each>
							<!-- additional mapping: -->
							<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:concurrent">
								<xsl:if test="not(not(string(fcx:totalLoanAmount)))">
									<xsl:call-template name="downPaymentSource-additionalMapping"/>
								</xsl:if>
							</xsl:for-each>
						</Deal>
					</Document>
				</inb:XMLResponseDealFCXType>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>
	<!--  #################################################################################
		  ############################## MAIN TEMPLATE -END ###############################
	      #################################################################################
-->
	<!--  #################################################################################
		  ################################ TEMPLATES -START ###############################
	      #################################################################################
-->
	<!-- APPLICANTGROUP template - START -->
	<xsl:template name="applicantGroup">
		<xsl:for-each select="fcx:applicant">
			<Borrower>
				<borrowerNumber>
					<xsl:call-template name="borrowerNumberValue"/>
				</borrowerNumber>
				<borrowerFirstName>
					<xsl:value-of select="fcx:name/fcx:firstName"/>
				</borrowerFirstName>
				<borrowerMiddleInitial>
					<xsl:value-of select="fcx:name/fcx:middleInitial"/>
				</borrowerMiddleInitial>
				<borrowerLastName>
					<!-- Ticket FXP27550: substring first 20 characters of the last name -->
					<xsl:call-template name="borrowerLastNameValue"/>
				</borrowerLastName>
				<firstTimeBuyer>
					<xsl:value-of select="fcx:firstTimeBuyer"/>
				</firstTimeBuyer>
				<borrowerBirthDate>
					<!--<xsl:value-of select="fcx:birthDate"/> This should be the correct mapping-->
					<!-- this is a quick fix that should be deleted when new dateTime type is implemented in FCX for birthDate:-->
					<xsl:choose>
						<xsl:when test="contains(fcx:birthDate, 'T')">
							<xsl:value-of select="fcx:birthDate"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat(fcx:birthDate,'T00:00:00')"/>
						</xsl:otherwise>
					</xsl:choose>
				</borrowerBirthDate>
				<salutationId>
					<xsl:value-of select="fcx:name/fcx:salutationDd"/>
				</salutationId>
				<maritalStatusId>
					<xsl:value-of select="fcx:maritalStatusDd"/>
				</maritalStatusId>
				<socialInsuranceNumber>
					<xsl:value-of select="fcx:socialInsuranceNumber"/>
				</socialInsuranceNumber>
				<xsl:for-each select="fcx:phone">
					<xsl:call-template name="phoneValue"/>
				</xsl:for-each>
				<borrowerEmailAddress>
					<xsl:value-of select="fcx:emailAddress"/>
				</borrowerEmailAddress>
				<!-- Ticket FXP26792, the following mapping was included: -->
				<clientReferenceNumber>
					<xsl:value-of select="fcx:clientReferenceNumber"/>
				</clientReferenceNumber>
				<numberOfDependents>
					<xsl:call-template name="numberOfDependentsValue"/>
				</numberOfDependents>
				<creditScore>
					<xsl:value-of select="fcx:creditScore"/>
				</creditScore>
				<!-- Ticket FXP26739: adding this new element -->
				<existingClient>
					<xsl:value-of select="fcx:existingClient"/>
				</existingClient>
				<languagePreferenceId>
					<xsl:value-of select="fcx:languagePreferenceDd"/>
				</languagePreferenceId>
				<borrowerTypeId>
					<xsl:call-template name="borrowerTypeIdValue"/>
				</borrowerTypeId>
				<citizenshipTypeId>
					<xsl:value-of select="fcx:citizenshipTypeDd"/>
				</citizenshipTypeId>
				<primaryBorrowerFlag>
					<xsl:value-of select="fcx:primaryApplicantFlag"/>
				</primaryBorrowerFlag>
				<dateOfCreditBureauProfile>
					<xsl:value-of select="fcx:creditBureauReport/fcx:datePulled"/>
				</dateOfCreditBureauProfile>
				<creditBureauOnFileDate>
					<xsl:value-of select="fcx:creditBureauReport/fcx:datePulled"/>
				</creditBureauOnFileDate>
				<bureauAttachment>
					<xsl:call-template name="bureauAttachmentValue"/>
				</bureauAttachment>
				<bankruptcyStatusId>
					<xsl:call-template name="bankruptcyStatusIdValue"/>
				</bankruptcyStatusId>
				<staffOfLender>
					<xsl:call-template name="staffOfLenderValue"/>
				</staffOfLender>
				<creditBureauNameId>
					<xsl:call-template name="creditBureauNameIdValue"/>
				</creditBureauNameId>
				<xsl:call-template name="borrowerWorkPhoneExtensionValue"/>
				<creditBureauSummary>
					<xsl:value-of select="fcx:creditBureauReport/fcx:creditBureauSummary"/>
				</creditBureauSummary>
				<xsl:call-template name="borrowerGenderIdValue"/>
				<insuranceProportionsId>
					<xsl:value-of select="fcx:lifeAndDisabilityApplication/fcx:insuranceProportionsDd"/>
				</insuranceProportionsId>
				<xsl:call-template name="lifePercentCoverageReqValue-Borrower"/>
				<xsl:call-template name="disabilityStatusIdValue-Borrower"/>
				<smokeStatusId>
					<xsl:value-of select="fcx:smokerDd"/>
				</smokeStatusId>
				<prefContactMethodId>
					<xsl:value-of select="fcx:preferredContactMethodDd"/>
				</prefContactMethodId>
				<employeeNumber>
					<xsl:value-of select="fcx:employeeNumber"/>
				</employeeNumber>
				<xsl:call-template name="solicitationIdValue"/>
				<!-- Express 4.4 - Start -->
				<suffixId>
					<xsl:value-of select="fcx:name/fcx:suffixDd"/>
				</suffixId>
				<CBAuthorizationDate>
					<xsl:value-of select="fcx:creditBureauReport/fcx:cbAuthorizationDate"/>
				</CBAuthorizationDate>
				<CBAuthorizationMethod>
					<xsl:value-of select="fcx:creditBureauReport/fcx:cbAuthorizationMethod"/>
				</CBAuthorizationMethod>
				<!--<xsl:call-template name="creditBureauLanguageIDValue"/> Deleted, this value is not coming from Expert-->
				<!-- Express 4.4 - End -->
				<xsl:for-each select="fcx:identification">
					<BorrowerIdentification>
						<identificationTypeId>
							<xsl:value-of select="fcx:identificationTypeDd"/>
						</identificationTypeId>
						<identificationNumber>
							<!-- <xsl:value-of select="fcx:identificationNumber"/>-->
							<xsl:call-template name="identificationNumberValue"/>
						</identificationNumber>
						<identificationCountry>
							<xsl:value-of select="fcx:identificationCountry"/>
						</identificationCountry>
					</BorrowerIdentification>
				</xsl:for-each>
				<xsl:for-each select="fcx:addressDetail">
					<!-- Ticket FXP26597, Borrower Address must be created only when it's not Mailing Address:
-->
					<xsl:if test="fcx:addressTypeDd != 2">
						<BorrowerAddress>
							<!-- Ticket FXP26597: changing to rule -->
							<xsl:call-template name="borrowerAddressTypeIdValue"/>
							<monthsAtAddress>
								<xsl:value-of select="fcx:monthsAtAddress"/>
							</monthsAtAddress>
							<residentialStatusId>
								<xsl:call-template name="residentialStatusIdValue"/>
							</residentialStatusId>
							<xsl:call-template name="address"/>
						</BorrowerAddress>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="fcx:employmentHistory">
					<EmploymentHistory>
						<occupationId>
							<xsl:call-template name="occupationIdValue"/>
						</occupationId>
						<industrySectorId>
							<xsl:call-template name="industrySectorIdValue"/>
						</industrySectorId>
						<employerName>
							<xsl:value-of select="fcx:employerName"/>
						</employerName>
						<monthsOfService>
							<xsl:value-of select="fcx:monthsOfService"/>
						</monthsOfService>
						<xsl:for-each select="fcx:contact">
							<xsl:call-template name="contact"/>
						</xsl:for-each>
						<employmentHistoryTypeId>
							<xsl:value-of select="fcx:employmentHistoryTypeDd"/>
						</employmentHistoryTypeId>
						<xsl:for-each select="fcx:income">
							<xsl:call-template name="income"/>
						</xsl:for-each>
						<employmentHistoryStatusId>
							<xsl:value-of select="fcx:employmentHistoryStatusDd"/>
						</employmentHistoryStatusId>
						<jobTitle>
							<xsl:value-of select="substring(fcx:jobTitle, 1, 35)"/>
						</jobTitle>
						<jobTitleId>
							<xsl:value-of select="$jobTitleIdValue"/>
						</jobTitleId>
					</EmploymentHistory>
				</xsl:for-each>
				<!-- for each employmentHistory -->
				<xsl:for-each select="fcx:otherIncome">
					<xsl:call-template name="income2"/>
				</xsl:for-each>
				<xsl:if test="fcx:primaryApplicantFlag = 'Y'">
					<xsl:if test="../fcx:applicantGroupTypeDd = 0">
						<xsl:for-each select="../../fcx:subjectProperty/fcx:property/fcx:rentalIncome">
							<xsl:call-template name="income3"/>
						</xsl:for-each>
					</xsl:if>
					<xsl:if test="../fcx:applicantGroupTypeDd = 1">
						<xsl:for-each select="../../fcx:subjectProperty/fcx:property/fcx:rentalIncome">
							<xsl:call-template name="income3"/>
						</xsl:for-each>
					</xsl:if>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="../fcx:applicantGroupTypeDd = 0">
						<xsl:for-each select="fcx:otherProperty/fcx:property/fcx:rentalIncome">
							<xsl:call-template name="income4"/>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="../fcx:applicantGroupTypeDd = 1">
							<xsl:for-each select="fcx:otherProperty/fcx:property/fcx:rentalIncome">
								<xsl:call-template name="income4"/>
							</xsl:for-each>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:for-each select="fcx:asset">
					<xsl:call-template name="asset"/>
				</xsl:for-each>
				<xsl:for-each select="fcx:otherProperty/fcx:property">
					<xsl:call-template name="asset2"/>
				</xsl:for-each>
				<xsl:for-each select="fcx:liability/fcx:broker">
					<xsl:call-template name="liability"/>
				</xsl:for-each>
				<xsl:for-each select="fcx:otherProperty/fcx:mortgage">
					<xsl:call-template name="liability2"/>
				</xsl:for-each>
				<xsl:for-each select="fcx:liability/fcx:creditBureau">
					<xsl:call-template name="liability3"/>
				</xsl:for-each>
				<!-- FXP26758: start -->
				<xsl:for-each select="fcx:otherProperty/fcx:property/fcx:propertyExpense">
					<xsl:call-template name="liabilityPropertyExpense"/>
				</xsl:for-each>
				<xsl:for-each select="fcx:addressDetail/fcx:rentPaymentAmount">
					<xsl:call-template name="liabilityRent"/>
				</xsl:for-each>
				<!-- FXP26758: end -->
				<xsl:if test="fcx:primaryApplicantFlag = 'Y'">
					<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage">
						<xsl:call-template name="borrowerLiabilityMortgage"/>
					</xsl:for-each>
				</xsl:if>
				<xsl:variable name="dealPurposeId">
					<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd"/>
				</xsl:variable>
				<xsl:if test="fcx:primaryApplicantFlag = 'Y'">
					<xsl:if test="$dealPurposeId = 2 or $dealPurposeId = 3 
						or $dealPurposeId = 4">
						<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property">
							<xsl:call-template name="borrowerAsset"/>
						</xsl:for-each>
					</xsl:if>
				</xsl:if>
			</Borrower>
		</xsl:for-each>
		<!-- for each applicant -->
	</xsl:template>
	<!-- APPLICANTGROUP template - END -->
	<!-- ADDRESS template - START -->
	<xsl:template name="address">
		<Addr>
			<provinceId>
				<xsl:call-template name="provinceIdValue-long"/>
			</provinceId>
			<city>
				<xsl:value-of select="fcx:address/fcx:city"/>
			</city>
			<postalFSA>
				<xsl:value-of select="fcx:address/fcx:postalFsa"/>
			</postalFSA>
			<postalLDU>
				<xsl:value-of select="fcx:address/fcx:postalLdu"/>
			</postalLDU>
			<streetNumber>
				<xsl:value-of select="fcx:address/fcx:streetNumber"/>
			</streetNumber>
			<streetName>
				<xsl:value-of select="fcx:address/fcx:streetName"/>
			</streetName>
			<xsl:variable name="addrStreetId">
				<xsl:choose>
					<xsl:when test="fcx:address/fcx:streetTypeDd &lt; 100">
						<xsl:value-of select="fcx:address/fcx:streetTypeDd"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Ticket FXP26736 start -->
			<streetTypeId>
				<xsl:value-of select="$addrStreetId"/>
			</streetTypeId>
			<xsl:variable name="streetTypeTableLookupKey">
				<xsl:value-of select="fcx:address/fcx:streetTypeDd"/>
			</xsl:variable>
			<xsl:variable name="streetDescription">
				<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeTableLookupKey]/text()"/>
			</xsl:variable>
			<xsl:if test="$addrStreetId = 0">
				<addressLine2>
					<xsl:value-of select="normalize-space(concat($addressLine2, ' ', $streetDescription))"/>
				</addressLine2>
			</xsl:if>
			<!-- Ticket FXP26736 end -->
			<streetDirectionId>
				<xsl:call-template name="streetDirectionIdValue-long"/>
			</streetDirectionId>
			<unitNumber>
				<xsl:value-of select="fcx:address/fcx:unitNumber"/>
			</unitNumber>
		</Addr>
	</xsl:template>
	<!-- ADDRESS template - END -->
	<!-- ADDRESS-SHORT template - START -->
	<xsl:template name="address-short">
		<Addr>
			<provinceId>
				<xsl:call-template name="provinceIdValueShort"/>
			</provinceId>
			<addressLine1>
				<xsl:value-of select="fcx:addressLine1"/>
			</addressLine1>
			<addressLine2>
				<xsl:value-of select="fcx:addressLine2"/>
			</addressLine2>
			<city>
				<xsl:value-of select="fcx:city"/>
			</city>
			<postalFSA>
				<xsl:value-of select="fcx:postalFsa"/>
			</postalFSA>
			<postalLDU>
				<xsl:value-of select="fcx:postalLdu"/>
			</postalLDU>
		</Addr>
	</xsl:template>
	<!-- ADDRESS-SHORT template - END -->
	<!-- ADDRESSSFPSOB template - START -->
	<xsl:template name="addressSfpSob">
		<!-- Template used by SFP, SOB Profile and SOB Profile Agent -->
		<Addr>
			<provinceId>
				<xsl:call-template name="provinceIdAddressSfpSobValue"/>
			</provinceId>
			<xsl:call-template name="addressLine1addressLine2Value"/>
			<city>
				<xsl:value-of select="fcx:city"/>
			</city>
			<postalFSA>
				<xsl:value-of select="fcx:postalFsa"/>
			</postalFSA>
			<postalLDU>
				<xsl:value-of select="fcx:postalLdu"/>
			</postalLDU>
		</Addr>
	</xsl:template>
	<!-- ADDRESSSFPSOB template - END -->
	<!-- CONTACT template - START -->
	<xsl:template name="contact">
		<Contact>
			<contactFirstName>
				<xsl:value-of select="fcx:contactName/fcx:contactFirstName"/>
			</contactFirstName>
			<contactMiddleInitial>
				<xsl:value-of select="fcx:contactName/fcx:contactMiddleInitial"/>
			</contactMiddleInitial>
			<contactLastName>
				<xsl:value-of select="fcx:contactName/fcx:contactLastName"/>
			</contactLastName>
			<xsl:for-each select="fcx:contactPhone">
				<xsl:call-template name="contactPhoneNumberValue"/>
			</xsl:for-each>
			<!-- ticket FXP26690: -->
			<xsl:call-template name="contactEmailAddressValue"/>
			<salutationId>
				<xsl:value-of select="fcx:contactName/fcx:salutationDd"/>
			</salutationId>
			<!-- Note: only returns a value for Borrower / EmploymentHistory / Contact:
-->
			<contactJobTitle>
				<xsl:value-of select="substring(../fcx:jobTitle, 1, 35)"/>
			</contactJobTitle>
			<languagePreferenceId>
				<xsl:value-of select="fcx:languagePreferenceDd"/>
			</languagePreferenceId>
			<!-- Note: some contacts don't have a value for this: -->
			<preferredDeliveryMethodId>
				<xsl:value-of select="fcx:preferredDeliveryMethodDd"/>
			</preferredDeliveryMethodId>
			<xsl:for-each select="fcx:address/fcx:address2Type">
				<xsl:call-template name="address-short"/>
			</xsl:for-each>
		</Contact>
	</xsl:template>
	<!-- CONTACT template - END -->
	<!-- CONTACTSFPSOB template - START -->
	<xsl:template name="contactSfpSob">
		<!-- This template is used for SFProfile, SOB Profile Agent and SOB Profile Agent contact -->
		<Contact>
			<contactFirstName>
				<xsl:value-of select="fcx:contactName/fcx:contactFirstName"/>
			</contactFirstName>
			<contactMiddleInitial>
				<xsl:value-of select="fcx:contactName/fcx:contactMiddleInitial"/>
			</contactMiddleInitial>
			<contactLastName>
				<xsl:value-of select="fcx:contactName/fcx:contactLastName"/>
			</contactLastName>
			<xsl:for-each select="fcx:contactPhone">
				<xsl:call-template name="contactPhoneNumberValue"/>
			</xsl:for-each>
			<!-- ticket FXP26690: -->
			<xsl:call-template name="contactEmailAddressValue"/>
			<salutationId>
				<xsl:value-of select="fcx:contactName/fcx:salutationDd"/>
			</salutationId>
			<!-- Note: only returns a value for Borrower / EmploymentHistory / Contact: -->
			<contactJobTitle>
				<xsl:value-of select="../fcx:jobTitle"/>
			</contactJobTitle>
			<languagePreferenceId>
				<xsl:value-of select="fcx:languagePreferenceDd"/>
			</languagePreferenceId>
			<!-- Ticket FXP26732:(Note: some contacts don't have a value for this): -->
			<preferredDeliveryMethodId>
				<xsl:value-of select="fcx:preferredDeliveryMethodDd"/>
			</preferredDeliveryMethodId>
			<xsl:for-each select="fcx:address/fcx:address1Type">
				<xsl:call-template name="addressSfpSob"/>
			</xsl:for-each>
		</Contact>
	</xsl:template>
	<!-- CONTACTSFPSOB template - END -->
	
	<!-- EMPLOYMENT HISTORY INCOME template - START -->
	<xsl:template name="income">
		<Income>
			<incomePeriodId>
				<xsl:value-of select="fcx:incomePeriodDd"/>
			</incomePeriodId>
			<incomeTypeId>
				<xsl:call-template name="incomeTypeIdValue"/>
			</incomeTypeId>
			<incomeAmount>
				<xsl:value-of select="fcx:incomeAmount"/>
			</incomeAmount>
			<incIncludeInGDS>
				<xsl:call-template name="incIncludeInGDSValue"/>
			</incIncludeInGDS>
			<incIncludeInTDS>
				<xsl:call-template name="incIncludeInTDSValue"/>
			</incIncludeInTDS>
		<!-- 4.4.1 GOLD COPY FEB. 2012: The following field is not going to be mapped (it is going to be assigned by Express):
			<incPercentInGDS>
				<xsl:value-of select="fcx:incPercentInGds"/>
			</incPercentInGDS>		-->
		<!-- 4.4.1 GOLD COPY FEB. 2012: The following field is not going to be mapped (it is going to be assigned by Express):
			<incPercentInTDS>
				<xsl:value-of select="fcx:incPercentInTds"/>
			</incPercentInTDS>		-->
		<!-- 4.4.1 GOLD COPY FEB. 2012: The following rule is changing:
			<incomeDescription>
				<xsl:value-of select="fcx:incomeDescription"/>
			</incomeDescription>	-->
		<!-- 4.4.1 GOLD COPY FEB. 2012: (FXP32118) The following field has a new rule:	-->
			<incomeDescription>
				<xsl:call-template name="incomeDescriptionValue"/>
			</incomeDescription>
		</Income>
	</xsl:template>
	<!-- INCOME template - END -->
	<!-- INCOME2 template - START -->
	<xsl:template name="income2">
		<Income>
			<incomePeriodId>
				<xsl:value-of select="fcx:incomePeriodDd"/>
			</incomePeriodId>
			<incomeTypeId>
				<xsl:call-template name="incomeTypeIdValue2"/>
			</incomeTypeId>
			<incomeAmount>
				<xsl:value-of select="fcx:incomeAmount"/>
			</incomeAmount>
			<incomeDescription>
				<xsl:value-of select="fcx:incomeDescription"/>
			</incomeDescription>
		</Income>
	</xsl:template>
	<!-- INCOME2 template - END -->
	<!-- INCOME3 template - START -->
	<xsl:template name="income3">
		<Income>
			<incomePeriodId>
				<xsl:value-of select="$incomePeriodIValue3"/>
			</incomePeriodId>
			<xsl:call-template name="incomeTypeIdValue3"/>
			<incomeAmount>
				<xsl:value-of select="fcx:incomeAmount"/>
			</incomeAmount>
			<!-- Ticket FXP26758, changing from GDS to Gds -->
			<incPercentInGDS>
				<xsl:value-of select="fcx:incPercentInGds"/>
			</incPercentInGDS>
			<!-- Ticket FXP28473, changing element name from "incIncludeInTDS" to "incPercentInTDS"
-->
			<incPercentInTDS>
				<xsl:value-of select="fcx:incPercentInTds"/>
			</incPercentInTDS>
			<incomeDescription>
				<xsl:call-template name="incomeDescriptionValue3"/>
			</incomeDescription>
		</Income>
	</xsl:template>
	<!-- INCOME3 template - END -->
	<!-- INCOME4 template - START -->
	<xsl:template name="income4">
		<Income>
			<incomePeriodId>
				<xsl:value-of select="$incomePeriodIValue4"/>
			</incomePeriodId>
			<xsl:call-template name="incomeTypeIdValue4"/>
			<incomeAmount>
				<xsl:value-of select="fcx:incomeAmount"/>
			</incomeAmount>
			<incIncludeInGDS>
				<xsl:value-of select="$incIncludeInGDSValue4"/>
			</incIncludeInGDS>
			<incIncludeInTDS>
				<xsl:value-of select="$incIncludeInTDSValue4"/>
			</incIncludeInTDS>
			<!--Ticket FXP26758: changed from GDS and TDS to Gds and Tds to match what is coming from Expert-->
			<incPercentInGDS>
				<xsl:value-of select="fcx:incPercentInGds"/>
			</incPercentInGDS>
			<incPercentInTDS>
				<xsl:value-of select="fcx:incPercentInTds"/>
			</incPercentInTDS>
			<incomeDescription>
				<!-- Ticket FXP26796, new description rule: -->
				<xsl:call-template name="incomeDescriptionValue4"/>
			</incomeDescription>
		</Income>
	</xsl:template>
	<!-- INCOME4 template - END -->
	<!-- ASSET template - START -->
	<xsl:template name="asset">
		<Asset>
			<assetDescription>
				<xsl:value-of select="fcx:assetDescription"/>
			</assetDescription>
			<assetValue>
				<xsl:value-of select="fcx:assetValue"/>
			</assetValue>
			<assetTypeId>
				<xsl:call-template name="assetTypeIdValue"/>
			</assetTypeId>
		</Asset>
	</xsl:template>
	<!-- ASSET template - END -->
	<!-- ASSET2 template - START -->
	<xsl:template name="asset2">
		<Asset>
			<assetDescription>
				<xsl:call-template name="assetDescriptionValue2"/>
			</assetDescription>
			<assetValue>
				<xsl:call-template name="assetValueValue2"/>
			</assetValue>
			<assetTypeId>
				<xsl:call-template name="assetTypeIdValue2"/>
			</assetTypeId>
		</Asset>
	</xsl:template>
	<!-- ASSET2 template - END -->
	<!-- LIABILITY template (BROKER LIABILITY) - START -->
	<xsl:template name="liability">
		<Liability>
			<liabilityTypeId>
				<xsl:call-template name="liabilityTypeIdValue"/>
			</liabilityTypeId>
			<liabilityAmount>
				<xsl:value-of select="fcx:liabilityAmount"/>
			</liabilityAmount>
			<liabilityMonthlyPayment>
				<xsl:value-of select="fcx:liabilityMonthlyPayment"/>
			</liabilityMonthlyPayment>
			<includeInGDS>
				<xsl:value-of select="$includeInGDSValue"/>
			</includeInGDS>
			<includeInTDS>
				<xsl:call-template name="includeInTDSValue"/>
			</includeInTDS>
			<liabilityDescription>
				<xsl:value-of select="fcx:liabilityDescription"/>
			</liabilityDescription>
			<liabilityPayOffTypeId>
				<xsl:call-template name="liabilityPayOffTypeIdValue"/>
			</liabilityPayOffTypeId>
			<percentOutGDS>
				<xsl:call-template name="percentOutGDSValue"/>
			</percentOutGDS>
			<percentOutTDS>
				<xsl:value-of select="$percentOutTDSValue"/>
			</percentOutTDS>
			<!-- Express 4.4 - Start -->
			<creditLimit>
				<xsl:value-of select="fcx:creditLimit"/>
			</creditLimit>
			<maturityDate>
				<xsl:value-of select="fcx:maturityDate"/>
			</maturityDate>
			<creditBureauRecordIndicator>
				<xsl:value-of select="fcx:cbScraped"/>
			</creditBureauRecordIndicator>
			<!-- Express 4.4 0- End -->
		</Liability>
	</xsl:template>
	<!-- LIABILITY template (BROKER LIABILITY) - END -->

	<!-- LIABILITY2 template (FOR OTHER PROPERTY)- START -->
	<xsl:template name="liability2">
		<Liability>
			<liabilityTypeId>
				<xsl:choose>
					<xsl:when test="../fcx:property/fcx:occupancyTypeDd = 2">12
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</liabilityTypeId>
			<liabilityAmount>
				<xsl:value-of select="fcx:balanceRemaining"/>
			</liabilityAmount>
			<liabilityMonthlyPayment>
				<xsl:value-of select="fcx:PAndIPaymentAmountMonthly"/>
			</liabilityMonthlyPayment>
			<includeInGDS>
				<xsl:value-of select="$includeInGDSValue2"/>
			</includeInGDS>
			<includeInTDS>
				<xsl:value-of select="$includeInTDSValue2"/>
			</includeInTDS>
			<liabilityDescription>
				<xsl:call-template name="liabilityDescriptionValue2"/>
			</liabilityDescription>
			<liabilityPayOffTypeId>
				<xsl:value-of select="fcx:payoffTypeDd"/>
			</liabilityPayOffTypeId>
			<percentOutGDS>
				<xsl:value-of select="$percentOutGDSValue2"/>
			</percentOutGDS>
			<percentOutTDS>
				<xsl:value-of select="$percentOutTDSValue2"/>
			</percentOutTDS>
			<!-- 4.4.1 GOLD COPY FEB. 2012: This is a new mapped field:-->
			<maturityDate>
				<xsl:value-of select="fcx:maturityDate"/>
			</maturityDate>
		</Liability>
	</xsl:template>
	<!-- LIABILITY2 template (FOR OTHER PROPERTY) - END -->
	
	<!-- LIABILITY3 template (CREDIT BUREAU) - START -->
	<xsl:template name="liability3">
		<Liability>
			<liabilityTypeId>
				<xsl:call-template name="liabilityTypeIdValue3"/>
			</liabilityTypeId>
			<liabilityAmount>
				<xsl:value-of select="fcx:liabilityAmount"/>
			</liabilityAmount>
			<liabilityMonthlyPayment>
				<xsl:value-of select="fcx:liabilityMonthlyPayment"/>
			</liabilityMonthlyPayment>
			<!-- 4.4.1 GOLD COPY FEB. 2012: This is a new mapped field:-->
			<includeInGDS>
				<xsl:value-of select="$includeInGDSValue3"/>
			</includeInGDS>
			<includeInTDS>
				<xsl:call-template name="includeInTDSValue3"/>
			</includeInTDS>
			<liabilityDescription>
				<xsl:value-of select="fcx:liabilityDescription"/>
			</liabilityDescription>
			<liabilityPayOffTypeId>
				<xsl:value-of select="fcx:liabilityPayOffTypeDd"/>
			</liabilityPayOffTypeId>
			<percentOutGDS>
				<xsl:call-template name="percentOutGDSValue3"/>
			</percentOutGDS>
			<percentOutTDS>
				<xsl:value-of select="$percentOutTDSValue3"/>
			</percentOutTDS>
			<!-- Express 4.4 - Start -->
			<creditLimit>
				<xsl:value-of select="fcx:creditLimit"/>
			</creditLimit>
			<maturityDate>
				<xsl:value-of select="fcx:maturityDate"/>
			</maturityDate>
			<creditBureauRecordIndicator>
				<xsl:value-of select="fcx:cbScraped"/>
			</creditBureauRecordIndicator>
			<!-- Express 4.4 0- End -->
		</Liability>
	</xsl:template>
	<!-- LIABILITY3 template (CREDIT BUREAU) - END -->
	<!-- LIABILITY template (EXPENSES FOR OTHER PROPERTIES) - START -->
	<xsl:template name="liabilityPropertyExpense">
		<Liability>
			<xsl:variable name="propertyExpenseAmountValue">
				<xsl:value-of select="fcx:propertyExpenseAmount"/>
			</xsl:variable>
			<liabilityTypeId>9</liabilityTypeId>
			<liabilityAmount>
				<xsl:if test="fcx:propertyExpensePeriodDd = 0">
					<xsl:value-of select="$propertyExpenseAmountValue"/>
				</xsl:if>
				<xsl:if test="fcx:propertyExpensePeriodDd = 3">
					<xsl:value-of select="fcx:propertyExpenseAmount * 12"/>
				</xsl:if>
			</liabilityAmount>
			<liabilityMonthlyPayment>
				<xsl:if test="fcx:propertyExpensePeriodDd = 0">
				<!-- 4.4.1 GOLD COPY FEB. 2012: Round to 2 decimals was added (954): -->
					<xsl:value-of select="round($propertyExpenseAmountValue div 12 * 100) div 100"/>
				</xsl:if>
				<xsl:if test="fcx:propertyExpensePeriodDd = 3">
					<xsl:value-of select="fcx:propertyExpenseAmount"/>
				</xsl:if>
			</liabilityMonthlyPayment>
			<includeInGDS>
				<xsl:text>N</xsl:text>
			</includeInGDS>
			<includeInTDS>
				<xsl:text>Y</xsl:text>
			</includeInTDS>
			<liabilityDescription>
				<xsl:call-template name="LiabilityDescriptionOtherProperties"/>
			</liabilityDescription>
			<liabilityPayOffTypeId>0</liabilityPayOffTypeId>
			<percentOutGDS>0</percentOutGDS>
			<percentOutTDS>0</percentOutTDS>
		</Liability>
	</xsl:template>
	<xsl:template name="LiabilityDescriptionOtherProperties">
		<xsl:variable name="description">
			<xsl:variable name="propertyExpenseTypeId" select="fcx:propertyExpenseTypeDd"/>
			<!-- 4.4.1 GOLD COPY FEB. 2012: Removing harcoding to English (957): 
			<xsl:variable name="languageId">0</xsl:variable>	-->
			<!-- 4.4.1 GOLD COPY FEB. 2012: Checking for which language to use (957): -->
			<xsl:variable name="languageId">
				<xsl:choose>
					<xsl:when test="../../../../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">0</xsl:when>
					<xsl:otherwise>1</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="$propertyExpenseTypeTable/propertyExpenseTypes/propertyExpenseType[@code=$propertyExpenseTypeId and @lang=$languageId]/text()"/>
			<!-- Ticket FXP27414: some format changes -->
			<xsl:text>: with </xsl:text>
			<xsl:choose>
				<xsl:when test="../fcx:address/fcx:provinceDd = 11">
				<!-- 4.4.1 GOLD COPY FEB. 2012: Only printing if exists, rule (957): -->
					<xsl:if test="not(not(string(../fcx:address/fcx:unitNumber)))">
						<xsl:value-of select="../fcx:address/fcx:unitNumber"/>
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:value-of select="../fcx:address/fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="streetTypeId" select="../fcx:address/fcx:streetTypeDd"/>
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:streetName"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="streetDirectionId" select="../fcx:address/fcx:streetDirectionDd"/>
					<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$frenchId]/text()"/>
					<xsl:text> </xsl:text>
					<!-- Ticket FXP27648: fixing wrong path when retrieving city: -->
					<xsl:value-of select="../fcx:address/fcx:city"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="provinceId" select="../fcx:address/fcx:provinceDd"/>
					<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$frenchId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:address/fcx:postalFsa"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:address/fcx:postalLdu"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="countryTypeDd" select="../fcx:address/fcx:countryTypeDd"/>
				<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
					<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeDd and @lang=$frenchId]/text()"/>
				</xsl:when>
				<xsl:otherwise>
				<!-- 4.4.1 GOLD COPY FEB. 2012: Only printing if exists, rule (957): -->
					<xsl:if test="not(not(string(../fcx:address/fcx:unitNumber)))">
						<xsl:value-of select="../fcx:address/fcx:unitNumber"/>
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:value-of select="../fcx:address/fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:address/fcx:streetName"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="streetTypeId" select="../fcx:address/fcx:streetTypeDd"/>
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="streetDirectionId" select="../fcx:address/fcx:streetDirectionDd"/>
					<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$englishId]/text()"/>
					<xsl:text> </xsl:text>
					<!-- Ticket FXP27648: fixing wrong path when retrieving city: -->
					<xsl:value-of select="../fcx:address/fcx:city"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="provinceId" select="../fcx:address/fcx:provinceDd"/>
					<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$englishId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:address/fcx:postalFsa"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:address/fcx:postalLdu"/>
					<xsl:text> </xsl:text>
					<xsl:variable name="countryTypeDd" select="../fcx:address/fcx:countryTypeDd"/>
				<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
					<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeDd and @lang=$englishId]/text()"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="substring(normalize-space($description), 1, 80)"/>
	</xsl:template>
	<!-- LIABILITY template (EXPENSES FOR OTHER PROPERTIES) - END -->
	<xsl:template name="liabilityRent">
		<Liability>
			<liabilityTypeId>16</liabilityTypeId>
			<liabilityAmount>0</liabilityAmount>
			<liabilityMonthlyPayment>
				<xsl:value-of select="../fcx:rentPaymentAmount"/>
			</liabilityMonthlyPayment>
			<includeInGDS>N</includeInGDS>
			<includeInTDS>Y</includeInTDS>
			<liabilityDescription>
				<!-- 4.4.1 GOLD COPY FEB. 2012: Rule (969) is changing from hardcoded "Rent" to a value that depends on Language: -->
				<xsl:choose>
					<xsl:when test="../../../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Rent</xsl:when>
					<xsl:otherwise>Loyer</xsl:otherwise>
				</xsl:choose>
			</liabilityDescription>
			<liabilityPayOffTypeId>0</liabilityPayOffTypeId>
			<percentOutGDS>0</percentOutGDS>
			<percentOutTDS>0</percentOutTDS>
		</Liability>
	</xsl:template>
	<!-- INSUREONLYAPPLICANT template - START -->
	<xsl:template name="InsureOnlyApplicant">
		<InsureOnlyApplicant>
			<insureOnlyApplicantFirstName>
				<xsl:value-of select="fcx:name/fcx:firstName"/>
			</insureOnlyApplicantFirstName>
			<insureOnlyApplicantLastName>
				<xsl:value-of select="fcx:name/fcx:lastName"/>
			</insureOnlyApplicantLastName>
			<insureOnlyApplicantBirthDate>
				<xsl:value-of select="fcx:dateOfBirth"/>
			</insureOnlyApplicantBirthDate>
			<insureOnlyApplicantGenderId>
				<xsl:value-of select="fcx:genderDd"/>
			</insureOnlyApplicantGenderId>
			<insuranceProportionsId>
				<xsl:value-of select="fcx:insuranceDetails/fcx:insuranceProportionsDd"/>
			</insuranceProportionsId>
			<xsl:call-template name="lifePercentCoverageReqValue-InsureOnlyApplicant"/>
			<smokeStatusId>
				<xsl:value-of select="fcx:smokerDd"/>
			</smokeStatusId>
			<xsl:call-template name="disabilityStatusIdValue-InsureOnlyApplicant"/>
		</InsureOnlyApplicant>
	</xsl:template>
	<!-- INSUREONLYAPPLICANT template - END -->
	<!-- PROPERTY template - START -->
	<xsl:template name="property">
		<Property>
			<propertyTypeId>
				<xsl:call-template name="propertyTypeIdValue"/>
			</propertyTypeId>
		<!-- 4.4.1 GOLD COPY FEB. 2012: propertyUsageId is a new field that is mapped: -->
			<propertyUsageId>
				<xsl:call-template name="propertyUsageIdValue"/>
			</propertyUsageId>
			<dwellingStyleId>
				<xsl:call-template name="dwellingStyleIdValue"/>
			</dwellingStyleId>
			<occupancyTypeId>
				<xsl:call-template name="occupancyTypeIdValue"/>
			</occupancyTypeId>
			<propertyStreetNumber>
				<xsl:value-of select="fcx:address/fcx:streetNumber"/>
			</propertyStreetNumber>
		<!-- following template gets the values for propertyStreetName, propertyAddressLine2 and streetTypeId elements -->
			<xsl:call-template name="propertyStreetNameAddressLine2StreetTypeValue"/>
			<heatTypeId>
				<xsl:call-template name="heatTypeIdValue"/>
			</heatTypeId>
			<propertyCity>
				<xsl:value-of select="fcx:address/fcx:city"/>
			</propertyCity>
			<propertyPostalFSA>
				<xsl:value-of select="fcx:address/fcx:postalFsa"/>
			</propertyPostalFSA>
			<propertyPostalLDU>
				<xsl:value-of select="fcx:address/fcx:postalLdu"/>
			</propertyPostalLDU>
			<legalLine1>
				<xsl:value-of select="fcx:legalLine1"/>
			</legalLine1>
			<legalLine2>
				<xsl:value-of select="fcx:legalLine2"/>
			</legalLine2>
			<legalLine3>
				<xsl:value-of select="fcx:legalLine3"/>
			</legalLine3>
			<yearBuilt>
				<xsl:value-of select="fcx:yearBuilt"/>
			</yearBuilt>
			<insulatedWithUFFI>
				<xsl:call-template name="insulatedWithUFFIValue"/>
			</insulatedWithUFFI>
			<provinceId>
				<xsl:call-template name="provinceIdValue"/>
			</provinceId>
			<purchasePrice>
			<!-- 4.4.1 GOLD COPY FEB. 2012: purchasePrice is not longer a direct mapping:
				<xsl:value-of select="fcx:purchasePrice"/>	-->
			<!-- 4.4.1 GOLD COPY FEB. 2012: purchasePrice has a new rule:	-->
				<xsl:call-template name="purchasePriceValue"/>
			</purchasePrice>
			<newConstructionId>
				<xsl:value-of select="fcx:newConstructionDd"/>
			</newConstructionId>
			<estimatedAppraisalValue>
				<xsl:value-of select="fcx:estimatedAppraisalValue"/>
			</estimatedAppraisalValue>
			<actualAppraisalValue>
				<xsl:value-of select="fcx:actualAppraisalValue"/>
			</actualAppraisalValue>
			<unitNumber>
				<xsl:value-of select="fcx:address/fcx:unitNumber"/>
			</unitNumber>
			<!-- Tickets FXP26711, FXP26907 - round() and empty validation added: -->
			<xsl:call-template name="lotSizeValue"/>
			<primaryPropertyFlag>
				<xsl:value-of select="fcx:primaryPropertyFlag"/>
			</primaryPropertyFlag>
			<appraisalDateAct>
				<xsl:value-of select="fcx:appraisalDateAct"/>
			</appraisalDateAct>
			<!-- Tickets FXP26711, FXP26907 - round() and empty validation added: -->
			<xsl:call-template name="lotSizeDepthValue"/>
			<xsl:call-template name="lotSizeFrontageValue"/>
			<lotSizeUnitOfMeasureId>
				<xsl:value-of select="fcx:lotSizeUnitOfMeasureDd"/>
			</lotSizeUnitOfMeasureId>
			<numberOfUnits>
				<xsl:call-template name="numberOfUnitsValue"/>
			</numberOfUnits>
			<propertyOccurenceNumber>
				<xsl:value-of select="$propertyOccurenceNumberValue"/>
			</propertyOccurenceNumber>
			<sewageTypeId>
				<xsl:value-of select="fcx:sewageTypeDd"/>
			</sewageTypeId>
			<streetDirectionId>
				<xsl:call-template name="streetDirectionIdValue"/>
			</streetDirectionId>
			<waterTypeId>
				<xsl:value-of select="fcx:waterTypeDd"/>
			</waterTypeId>
			<livingSpace>
				<xsl:value-of select="fcx:livingSpace"/>
			</livingSpace>
			<livingSpaceUnitOfMeasureId>
				<!-- Ticket FXP26712: changed from "livingSpaceUnitOfMeasureDd" to "livingSpaceUnitOfMeasurDd": -->
				<xsl:value-of select="fcx:livingSpaceUnitOfMeasurDd"/>
			</livingSpaceUnitOfMeasureId>
			<structureAge>
				<xsl:value-of select="fcx:structureAge"/>
			</structureAge>
			<dwellingTypeId>
				<xsl:call-template name="dwellingTypeIdValue"/>
			</dwellingTypeId>
			<MLSListingFlag>
				<xsl:value-of select="fcx:mlsListingFlag"/>
			</MLSListingFlag>
			<xsl:call-template name="garageSizeIdValue"/>
			<garageTypeId>
				<xsl:call-template name="garageTypeIdValue"/>
			</garageTypeId>
			<builderName>
				<xsl:value-of select="fcx:builderName"/>
			</builderName>
			<tenureTypeId>
				<xsl:call-template name="tenureTypeIdValue"/>
			</tenureTypeId>
			<xsl:call-template name="requestAppraisalIdValue"/>
			<xsl:for-each select="fcx:propertyExpense">
				<!-- 4.4.1 GOLD COPY FEB. 2012: It is not going to be created in the case of heat expense included in the condo fee: -->
					<xsl:choose>
						<!-- 2 = heat cost, Y = condo fee includes heat cost -->
						<xsl:when test="fcx:propertyExpenseTypeDd = 2 and ../fcx:feesIncludeHeat = 'Y'"></xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="propertyExpense"/>
						</xsl:otherwise>
					</xsl:choose>
			</xsl:for-each>
		<!-- 4.4.1 GOLD COPY FEB. 2012: New rule (938 - 948) has been added: -->
			<xsl:for-each select="../../fcx:mortgage/fcx:concurrent">
				<xsl:if test="not(not(string(fcx:mortgageTypeDd)))
							and not(not(string(fcx:PAndIPaymentAmountMonthly)))">
					<xsl:call-template name="propertyExpenseConcurrent"/>
				</xsl:if>
			</xsl:for-each>
		</Property>
	</xsl:template>
	<!-- PROPERTY template - END -->
	<!-- PROPERTYEXPENSE template - START -->
	<xsl:template name="propertyExpense">
		<PropertyExpense>
			<propertyExpensePeriodId>
				<xsl:value-of select="fcx:propertyExpensePeriodDd"/>
			</propertyExpensePeriodId>
			<propertyExpenseTypeId>
				<xsl:call-template name="propertyExpenseTypeIdValue"/>
			</propertyExpenseTypeId>
			<propertyExpenseAmount>
				<xsl:call-template name="propertyExpenseAmountValue"/>
			</propertyExpenseAmount>
			<!-- FXP26758 start -->
			<propertyExpenseDescription>
				<xsl:value-of select="$propertyExpenseTypeId"/>
				<xsl:variable name="propertyExpenseTypeId" select="fcx:propertyExpenseTypeDd"/>
				<xsl:variable name="languageId">0</xsl:variable>
				<xsl:value-of select="$propertyExpenseTypeTable/propertyExpenseTypes/propertyExpenseType[@code=$propertyExpenseTypeId and @lang=$languageId]/text()"/>
			</propertyExpenseDescription>
			<!-- FXP26758 end -->
			<monthlyExpenseAmount>
				<xsl:value-of select="fcx:monthlyExpenseAmount"/>
			</monthlyExpenseAmount>
			<peIncludeInGDS>
				<xsl:choose>
					<xsl:when test="fcx:propertyExpenseTypeDd = 3 or
												fcx:propertyExpenseTypeDd = 4 or
												fcx:propertyExpenseTypeDd = 5 or
												fcx:propertyExpenseTypeDd = 6 or
												fcx:propertyExpenseTypeDd = 7 or
												fcx:propertyExpenseTypeDd = 8">N</xsl:when>
					<xsl:otherwise>Y</xsl:otherwise>
				</xsl:choose>
			</peIncludeInGDS>
			<peIncludeInTDS>
				<xsl:choose>
					<xsl:when test="fcx:propertyExpenseTypeDd = 3 or
												fcx:propertyExpenseTypeDd = 4 or
												fcx:propertyExpenseTypeDd = 5 or
												fcx:propertyExpenseTypeDd = 6 or
												fcx:propertyExpenseTypeDd = 7 or
												fcx:propertyExpenseTypeDd = 8">N</xsl:when>
					<xsl:otherwise>Y</xsl:otherwise>
				</xsl:choose>
			</peIncludeInTDS>
			<xsl:choose>
				<xsl:when test="fcx:propertyExpenseTypeDd = 3 or
												fcx:propertyExpenseTypeDd = 4 or
												fcx:propertyExpenseTypeDd = 5 or
												fcx:propertyExpenseTypeDd = 6 or
												fcx:propertyExpenseTypeDd = 7 or
												fcx:propertyExpenseTypeDd = 8">
					<pePercentInGDS>0</pePercentInGDS>
					<pePercentInTDS>0</pePercentInTDS>
				</xsl:when>
			</xsl:choose>
		</PropertyExpense>
	<!-- 4.4.1 GOLD COPY FEB. 2012: don't display PropertyExpense if propertyExpenseAmount is empty! - END: -->
	</xsl:template>
	<!-- PROPERTYEXPENSE template - START -->
	<!-- PROPERTYEXPENSE CONCURRENT template - START -->
	<!-- 4.4.1 GOLD COPY FEB. 2012: New rule (938 - 948) has been added: -->
	<xsl:template name="propertyExpenseConcurrent">
		<PropertyExpense>
			<propertyExpensePeriodId>3</propertyExpensePeriodId>
			<propertyExpenseTypeId>
				<xsl:choose>
					<xsl:when test="fcx:mortgageTypeDd = 1">7</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:mortgageTypeDd = 2">3</xsl:when>
							<xsl:otherwise>
								<xsl:if test="fcx:mortgageTypeDd = 3">4</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</propertyExpenseTypeId>
			<propertyExpenseAmount>
				<xsl:value-of select="fcx:PAndIPaymentAmountMonthly"/>
			</propertyExpenseAmount>
			<propertyExpenseDescription>
				<xsl:call-template name="propertyExpenseDescriptionConcurrentValue"/>
			</propertyExpenseDescription>
			<peIncludeInGDS>Y</peIncludeInGDS>
			<peIncludeInTDS>Y</peIncludeInTDS>
		</PropertyExpense>
	</xsl:template>
	<xsl:template name="propertyExpenseDescriptionConcurrentValue">
		<xsl:variable name="propExpDescrConcurr">
			<xsl:if test="not(not(string(fcx:lenderSubmission/fcx:lenderProfile/fcx:lenderName)))">
				<xsl:choose>
					<xsl:when test="../fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Lender: </xsl:when>
					<xsl:otherwise>Prêteur : </xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="fcx:lenderSubmission/fcx:lenderProfile/fcx:lenderName"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:if test="not(not(string(fcx:PAndIPaymentAmountMonthly)))">
				<xsl:choose>
					<xsl:when test="../fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Monthly Payment: </xsl:when>
					<xsl:otherwise>Paiement mensuel : </xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="fcx:PAndIPaymentAmountMonthly"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:if test="not(not(string(fcx:maturityDate)))">
				<xsl:choose>
					<xsl:when test="../fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Maturity Date: </xsl:when>
					<xsl:otherwise>Date d’échéance : </xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="fcx:maturityDate"/>
			</xsl:if>
		</xsl:variable>
		<xsl:value-of select="substring(normalize-space($propExpDescrConcurr), 1, 80)"/>
	</xsl:template>
	<!-- PROPERTYEXPENSE CONCURRENT template - END -->
	<!-- DOWNPAYMENTSOURCE template - START -->
	<xsl:template name="downPaymentSource">
		<DownPaymentSource>
			<downPaymentSourceTypeId>
				<xsl:call-template name="downPaymentSourceTypeIdValue"/>
			</downPaymentSourceTypeId>
			<amount>
				<xsl:value-of select="fcx:amount"/>
			</amount>
			<DPSDescription>
				<xsl:value-of select="fcx:description"/>
			</DPSDescription>
		</DownPaymentSource>
	</xsl:template>
	<!-- DOWNPAYMENTSOURCE template - END -->
	<!-- ESCROWPAYMENT template - START -->
	<xsl:template name="escrowPayment">
		<EscrowPayment>
			<escrowTypeId>
				<xsl:value-of select="$escrowTypeIdValue"/>
			</escrowTypeId>
			<escrowPaymentDescription>
		<!-- 4.4.1 GOLD COPY FEB. 2012: This rule for the field has been changed
				<xsl:value-of select="$escrowPaymentDescriptionValue"/>		-->
		<!-- 4.4.1 GOLD COPY FEB. 2012: This is the new rule for the description:	-->
				<xsl:call-template name="escrowPaymentDescriptionValue"/>
			</escrowPaymentDescription>
			<escrowPaymentAmount>
				<xsl:value-of select="$escrowPaymentAmountValue"/>
			</escrowPaymentAmount>
		</EscrowPayment>
	</xsl:template>
	<!-- ESCROWPAYMENT template - END -->
	<!-- ESCROWPAYMENT2 template - START -->
	<xsl:template name="escrowPayment2">
		<EscrowPayment>
			<escrowTypeId>
				<xsl:value-of select="$escrowTypeIdValue2"/>
			</escrowTypeId>
			<escrowPaymentDescription>
		<!-- 4.4.1 GOLD COPY FEB. 2012: This rule for the field has been changed
				<xsl:value-of select="$escrowPaymentDescriptionValue2"/>	-->
		<!-- 4.4.1 GOLD COPY FEB. 2012: This is the new rule for the description:	-->
				<xsl:call-template name="escrowPaymentDescriptionValue2"/>
			</escrowPaymentDescription>
			<escrowPaymentAmount>
				<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:adminFee"/>
			</escrowPaymentAmount>
		</EscrowPayment>
	</xsl:template>
	<!-- ESCROWPAYMENT2 template - END -->
	<!-- DEALNOTES template - START -->
	<xsl:template name="dealNotesApplicantInfo">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdApplicantInfo"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Start Applicant Information for: </xsl:text>
				<xsl:value-of select="$CRLF"/>
				<xsl:value-of select="fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:name/fcx:middleInitial"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>
				<!--  Ticket FXP27901: Adding Applicant Pair section: -->
				<xsl:if test="fcx:relationshipToPrimaryApplicant = 0">
					<xsl:text>Applicant Pair: </xsl:text>
					<xsl:value-of select="fcx:name/fcx:firstName"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:name/fcx:middleInitial"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:name/fcx:lastName"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<!-- Express 4.4 Remove - Start -->
				<!-- 
				<xsl:for-each select="./fcx:phone">
					<xsl:variable name="phoneNumber" select="fcx:phoneNumber" />
					<xsl:if test="($phoneNumber)">
						<xsl:if test="fcx:phoneTypeDd = 2">
							<xsl:text>Cell phone: </xsl:text><xsl:value-of select="fcx:phoneNumber" />
							<xsl:value-of select="$CRLF" />
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
				-->
				<!-- Express 4.4 Remove - End -->
				<xsl:for-each select="fcx:addressDetail[fcx:addressTypeDd = 0]">
					<xsl:variable name="countryTypeId" select="fcx:address/fcx:countryTypeDd"/>
					<xsl:if test="$countryTypeId != 1">
						<xsl:text>Current Address Country: </xsl:text>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
						<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeId and @lang=$englishId]/text()"/>
						<xsl:value-of select="$CRLF"/>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="./fcx:addressDetail">
					<xsl:variable name="postCode" select="fcx:address/fcx:internationalPostalCode"/>
					<xsl:variable name="addressTypeId" select="fcx:addressTypeDd"/>
					<xsl:if test="($postCode)">
						<xsl:if test="$addressTypeId = 1">
							<xsl:text>Previous International Postal Code: </xsl:text>
							<xsl:value-of select="fcx:address/fcx:internationalPostalCode"/>
							<xsl:value-of select="$CRLF"/>
						</xsl:if>
						<xsl:if test="$addressTypeId = 0">
							<xsl:text>Current International Postal Code: </xsl:text>
							<xsl:value-of select="fcx:address/fcx:internationalPostalCode"/>
							<xsl:value-of select="$CRLF"/>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="fcx:addressDetail[fcx:addressTypeDd = 1]">
					<xsl:variable name="countryTypeId" select="fcx:address/fcx:countryTypeDd"/>
					<xsl:if test="$countryTypeId != 1">
						<xsl:text>Previous Address Country: </xsl:text>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
						<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeId and @lang=$englishId]/text()"/>
						<xsl:value-of select="$CRLF"/>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="fcx:employmentHistory[fcx:employmentHistoryStatusDd = 0]">
					<xsl:variable name="countryTypeId" select="fcx:contact/fcx:address/fcx:address2Type/fcx:countryTypeDd"/>
					<xsl:if test="$countryTypeId != 1">
						<xsl:text>Current Employer Country: </xsl:text>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
						<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeId and @lang=$englishId]/text()"/>
						<xsl:value-of select="$CRLF"/>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="fcx:employmentHistory[fcx:employmentHistoryStatusDd = 1]">
					<xsl:variable name="countryTypeId" select="fcx:contact/fcx:address/fcx:address2Type/fcx:countryTypeDd"/>
					<xsl:if test="$countryTypeId != 1">
						<xsl:text>Previous Employer Country: </xsl:text>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
						<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeId and @lang=$englishId]/text()"/>
						<xsl:value-of select="$CRLF"/>
					</xsl:if>
				</xsl:for-each>
				<!-- Ticket FXP26798, "for-each" and format changed: -->
				<xsl:for-each select="./fcx:employmentHistory/fcx:selfEmploymentDetails">
					<xsl:text>Self-Employment Details:</xsl:text>
					<xsl:value-of select="$CRLF"/>
					<xsl:text>Business Type: </xsl:text>
					<xsl:value-of select="fcx:companyType"/>
					<xsl:value-of select="$CRLF"/>
					<xsl:text>Company Purpose: </xsl:text>
					<xsl:value-of select="fcx:operatingAs"/>
					<xsl:value-of select="$CRLF"/>
					<xsl:text>Gross Revenue: </xsl:text>
					<xsl:value-of select="fcx:grossRevenue"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:for-each>
				<!-- This was removed from the spreadsheet mapping: -->
				<!--
				<xsl:for-each select="./fcx:otherIncome">
					<xsl:text> Other Income: </xsl:text>
					<xsl:value-of select="fcx:annualIncomeAmount" />
				</xsl:for-each>
				<xsl:value-of select="$CRLF" />
				-->
				<xsl:text>End Applicant Information for: </xsl:text>
				<xsl:value-of select="fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:name/fcx:middleInitial"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select=" $englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdApplicantInfo"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Commencer les renseignements du demandeur pour : </xsl:text>
				<xsl:value-of select="$CRLF"/>
				<xsl:value-of select="fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:name/fcx:middleInitial"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>
				<!--  Ticket FXP27901: Adding Applicant Pair section: -->
				<xsl:if test="fcx:relationshipToPrimaryApplicant = 0">
					<xsl:text>Couple de demandeurs: </xsl:text>
					<xsl:value-of select="fcx:name/fcx:firstName"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:name/fcx:middleInitial"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:name/fcx:lastName"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<!-- Express 4.4 Remove - Start -->
				<!-- 
                <xsl:for-each select="./fcx:phone">
					<xsl:variable name="phoneNumber" select="fcx:phoneNumber" />
					<xsl:if test="($phoneNumber)">
						<xsl:if test="fcx:phoneTypeDd = 2">
							<xsl:text>Cellulaire: </xsl:text>
							<xsl:value-of select="fcx:phoneNumber" />
							<xsl:value-of select="$CRLF" />
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
				-->
				<!-- Express 4.4 Remove - End -->
				<xsl:for-each select="fcx:addressDetail[fcx:addressTypeDd = 0]">
					<xsl:variable name="countryTypeId" select="fcx:address/fcx:countryTypeDd"/>
					<xsl:if test="$countryTypeId != 1">
						<xsl:text>Adresse actuelle, pays: </xsl:text>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
						<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeId and @lang=$frenchId]/text()"/>
						<xsl:value-of select="$CRLF"/>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="./fcx:addressDetail">
					<xsl:variable name="postCode" select="fcx:address/fcx:internationalPostalCode"/>
					<xsl:variable name="addressTypeId" select="fcx:addressTypeDd"/>
					<xsl:if test="($postCode)">
						<xsl:if test="$addressTypeId = 1">
							<xsl:text>Code postal international précédent : </xsl:text>
							<xsl:value-of select="fcx:address/fcx:internationalPostalCode"/>
							<xsl:value-of select="$CRLF"/>
						</xsl:if>
						<xsl:if test="$addressTypeId = 0">
							<xsl:text>Code postal international actuel: </xsl:text>
							<xsl:value-of select="fcx:address/fcx:internationalPostalCode"/>
							<xsl:value-of select="$CRLF"/>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="fcx:addressDetail[fcx:addressTypeDd = 1]">
					<xsl:variable name="countryTypeId" select="fcx:address/fcx:countryTypeDd"/>
					<xsl:if test="$countryTypeId != 1">
						<xsl:text>Adresse précédente, pays: </xsl:text>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
						<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeId and @lang=$frenchId]/text()"/>
						<xsl:value-of select="$CRLF"/>
					</xsl:if>
				</xsl:for-each>
				<xsl:if test="./fcx:employmentHistory/fcx:contact/fcx:address/fcx:address2Type/fcx:countryTypeDd != 1">
					<xsl:if test="./fcx:employmentHistory/fcx:employmentHistoryStatusDd = 0">
						<xsl:text>Employeur actuel, pays: </xsl:text>
						<xsl:variable name="countryTypeDd" select="./fcx:employmentHistory/fcx:contact/fcx:address/fcx:address2Type/fcx:countryTypeDd"/>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
						<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeDd and @lang=$frenchId]/text()"/>
						<xsl:value-of select="$CRLF"/>
					</xsl:if>
				</xsl:if>
				<xsl:if test="./fcx:employmentHistory/fcx:contact/fcx:address/fcx:address2Type/fcx:countryTypeDd != 1">
					<xsl:if test="./fcx:employmentHistory/fcx:employmentHistoryStatusDd = 1">
						<xsl:text>Employeur précédent, pays: </xsl:text>
						<xsl:variable name="countryTypeDd" select="./fcx:employmentHistory/fcx:contact/fcx:address/fcx:address2Type/fcx:countryTypeDd"/>
						<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
						<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeDd and @lang=$frenchId]/text()"/>
						<xsl:value-of select="$CRLF"/>
					</xsl:if>
				</xsl:if>
				<!-- Tickets FXP26798 & EXP26770, "for-each" and format changed: -->
				<xsl:for-each select="./fcx:employmentHistory/fcx:selfEmploymentDetails">
					<xsl:text>Détails sur le travail autonome: </xsl:text>
					<xsl:value-of select="$CRLF"/>
					<xsl:text>Type d'entreprise: </xsl:text>
					<xsl:value-of select="fcx:companyType"/>
					<xsl:value-of select="$CRLF"/>
					<xsl:text>Objet de l'entreprise: </xsl:text>
					<xsl:value-of select="fcx:operatingAs"/>
					<xsl:value-of select="$CRLF"/>
					<xsl:text>Revenu brut: </xsl:text>
					<xsl:value-of select="fcx:grossRevenue"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:for-each>
				<!-- This was removed from the spreadsheet mapping: -->
				<!--
				<xsl:for-each select="./fcx:otherIncome">
					<xsl:text> Autres revenus: </xsl:text>
					<xsl:value-of select="fcx:annualIncomeAmount" />
				</xsl:for-each>
				<xsl:value-of select="$CRLF" />
				-->
				<xsl:text>Terminer les renseignements du demandeur pour: </xsl:text>
				<xsl:value-of select="fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:name/fcx:middleInitial"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select=" $frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesSubjectProperty">
		<xsl:if test="fcx:address/fcx:countryTypeDd != 1">
			<DealNotes>
				<dealNotesCategoryId>
					<xsl:value-of select="$dealNotesCategoryIdSubjectProperty"/>
				</dealNotesCategoryId>
				<dealNotesDate>
					<xsl:value-of select="date:date-time()"/>
				</dealNotesDate>
				<dealNotesText>
					<xsl:text>Subject Property Country: </xsl:text>
					<xsl:variable name="countryTypeDd" select="fcx:address/fcx:countryTypeDd"/>
					<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
					<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeDd and @lang=$englishId]/text()"/>
				</dealNotesText>
				<languagePreferenceId>
					<xsl:value-of select=" $englishId"/>
				</languagePreferenceId>
			</DealNotes>
			<DealNotes>
				<dealNotesCategoryId>
					<xsl:value-of select="$dealNotesCategoryIdSubjectProperty"/>
				</dealNotesCategoryId>
				<dealNotesDate>
					<xsl:value-of select="date:date-time()"/>
				</dealNotesDate>
				<dealNotesText>
					<xsl:text>Pays de la propriété visée : </xsl:text>
					<xsl:variable name="countryTypeDd" select="fcx:address/fcx:countryTypeDd"/>
					<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
					<xsl:value-of select="$countryTable/countries/country[@code=$countryTypeDd and @lang=$frenchId]/text()"/>
				</dealNotesText>
				<languagePreferenceId>
					<xsl:value-of select="$frenchId"/>
				</languagePreferenceId>
			</DealNotes>
		</xsl:if>
	</xsl:template>
	<xsl:template name="dealNotesDealExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdDealExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional Information: </xsl:text>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdDealExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire: </xsl:text>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesApplicantExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdApplicantExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for applicant: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:variable name="CRLF">
					<xsl:text>&#10;</xsl:text>
				</xsl:variable>
				<xsl:value-of select="$CRLF"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdApplicantExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour le demandeur: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:variable name="CRLF">
					<xsl:text>&#10;</xsl:text>
				</xsl:variable>
				<xsl:value-of select="$CRLF"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesAssetExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdAssetExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for applicant: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> asset
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:asset/fcx:assetDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdAssetExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour le demandeur: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>Actif 
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:asset/fcx:assetDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesBrokerExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdBrokerExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for applicant: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> Broker liability
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:liability/fcx:broker/fcx:liabilityDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdBrokerExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour le demandeur: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>Passif du courtier
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:liability/fcx:broker/fcx:liabilityDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesCBExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdCBExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for applicant: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> Credit bureau liability
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:liability/fcx:creditBureau/fcx:liabilityDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdCBExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour le demandeur: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> Passif du bureau de crédit: 
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:liability/fcx:creditBureau/fcx:liabilityDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesEmploymentExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdEmploymentExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for applicant: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> employment details for employer 
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:employmentHistory/fcx:employerName"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdEmploymentExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour le demandeur:</xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> Détails sur le travail
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:employmentHistory/fcx:employerName"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesIncomeExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdIncomeExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for applicant: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>  Income details for: 
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:employmentHistory/fcx:income/fcx:incomeDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdIncomeExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour le demandeur :</xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> Détails du revenu: 
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:employmentHistory/fcx:income/fcx:incomeDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesCBReportExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdCBReportExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for applicant: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>  credit bureau details  
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:employmentHistory/fcx:income/fcx:incomeDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdCBReportExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour le demandeur: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> Information supplémentaire du bureau de crédit: 
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:employmentHistory/fcx:income/fcx:incomeDescription"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesLifeExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdLifeExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for applicant: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/>  life and disability insurance details 
                    <xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdLifeExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text> Information supplémentaire du bureau de crédit: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:firstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:name/fcx:lastName"/>
				<xsl:value-of select="$CRLF"/> Détails d'assurance vie et invalidité:
					<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesMortgageExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdMortgageExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Mortgage additional details: </xsl:text>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdMortgageExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire de l'hypothèque: </xsl:text>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesParticipantExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdParticipantExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional Information for: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:participant/fcx:partyCompanyName"/>
				<xsl:for-each select="./*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdParticipantExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:participant/fcx:partyCompanyName"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesSubjectPropertyPropertyExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdSubjectPropertyPropertyExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for : </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:unitNumber"/>-<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:streetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:streetName"/>,
<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:city"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdSubjectPropertyPropertyExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour :</xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:unitNumber"/>-<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:streetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:streetName"/>,
<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:city"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesSubjectPropertyExistingMortgageExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdSubjectPropertyExistingMortgageExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for : </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:unitNumber"/>-<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:streetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:streetName"/>,
<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:city"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdSubjectPropertyExistingMortgageExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour :</xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:unitNumber"/>-<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:streetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:streetName"/>,
<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:address/fcx:city"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesOtherPropertyPropertyExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdOtherPropertyPropertyExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for : </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:unitNumber"/>-<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:streetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:streetName"/>,
<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:city"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdOtherPropertyPropertyExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour :</xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:unitNumber"/>-<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:streetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:streetName"/>,
<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:city"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesOtherPropertyMortgageExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdOtherPropertyMortgageExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Additional information for : </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:unitNumber"/>-<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:streetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:streetName"/>,
<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:city"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdOtherPropertyMortgageExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Information supplémentaire pour :</xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:unitNumber"/>-<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:streetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:streetName"/>,
<xsl:value-of select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:otherProperty/fcx:property/fcx:address/fcx:city"/>
				<xsl:for-each select="./fcx:*">
					<xsl:call-template name="generateElement"/>
				</xsl:for-each>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesTaxYearExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdTaxYearExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Tax Year : </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:taxationYear"/>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdTaxYearExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Exercice :</xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:taxationYear"/>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesPIExtension">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdPIExtension"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:choose>
					<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mtgProd/fcx:mpBusinessId">
						<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mtgProd/fcx:mpBusinessId"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>N/A</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>; </xsl:text>
				<xsl:value-of select="$CRLF"/>
				<xsl:choose>
					<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:rate/fcx:netRate">
						<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:rate/fcx:netRate"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>NONE</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>; </xsl:text>
				<xsl:choose>
					<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:PAndIPaymentAmount">
						<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:PAndIPaymentAmount"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>NONE</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$noLanguageId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesMortgagePayment">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdMortgagePayment"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<!-- Ticket FXP26972, adding lookup table: -->
				<xsl:text>Term Type : </xsl:text>
				<xsl:variable name="paymentTermDd" select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:paymentTermDd"/>
				<xsl:value-of select="$paymentTermTypeTable/paymentTermTypes/paymentTermType[@code=$paymentTermDd and @lang=$englishId]/text()"/>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdMortgagePayment"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<!-- Ticket FXP26972, adding lookup table: -->
				<xsl:text>Type de durée : </xsl:text>
				<xsl:variable name="paymentTermDd" select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:paymentTermDd"/>
				<xsl:value-of select="$paymentTermTypeTable/paymentTermTypes/paymentTermType[@code=$paymentTermDd and @lang=$frenchId]/text()"/>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<!-- Express 4.4 Remove - Start -->
	<!--
	<xsl:template name="dealNotesWaiverDate">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdWaiverDate" />
			</dealNotesCategoryId>
            <dealNotesDate><xsl:value-of select="date:date-time()" /></dealNotesDate>
			<dealNotesText>
				<xsl:text>Waiver Date   : </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:financingWaiverDate "/>
				<xsl:value-of select="$CRLF" />
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId" />
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdWaiverDate" />
			</dealNotesCategoryId>
            <dealNotesDate><xsl:value-of select="date:date-time()" /></dealNotesDate>
			<dealNotesText>
				<xsl:text>Date de l'exonération: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:financingWaiverDate "/>
				<xsl:value-of select="$CRLF" />
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId" />
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	-->
	<!-- Express 4.4 Remove - End -->
	<xsl:template name="dealNotesMarketSubmission">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdMarketSubmission"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Market Submission Number - : </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:marketSubmissionNumber "/>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdMarketSubmission"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Numéro de la demande envoyé au marché: </xsl:text>
				<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:marketSubmissionNumber "/>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: New Deal Note for condo fee that includes heat: -->	
	<xsl:template name="dealNotesFeeIncludesHeat">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdFeeIncludesHeat"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Condo Heat Included: </xsl:text>
					<xsl:choose>
						<xsl:when test="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:feesIncludeHeat = 'Y'">Yes</xsl:when>
						<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdFeeIncludesHeat"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:text>Chauffage inclus dans les frais de copropriété : </xsl:text>
					<xsl:choose>
						<xsl:when test="//fcx:loanApplication/fcx:subjectProperty/fcx:property/fcx:feesIncludeHeat = 'Y'">Oui</xsl:when>
						<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				<xsl:value-of select="$CRLF"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesOtherProperty">
		<!-- Ticket FXP26847, changing from absolute to relative paths!: -->
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdOtherProperty"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:value-of select="$CRLF"/>
				<xsl:if test="fcx:numberOfUnits">
					<xsl:text>Number of Units: </xsl:text>
					<xsl:value-of select="fcx:numberOfUnits"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="fcx:purchasePrice">
					<xsl:text>Purchase Price:  </xsl:text>
					<xsl:value-of select="fcx:purchasePrice"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="fcx:estimatedAppraisalValue">
					<xsl:text>Current Value:  </xsl:text>
					<xsl:value-of select="fcx:estimatedAppraisalValue"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="../fcx:mortgage/fcx:maturityDate">
					<xsl:text>Maturity Date:  </xsl:text>
					<xsl:value-of select="../fcx:mortgage/fcx:maturityDate"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="../fcx:mortgage/fcx:rate/fcx:interestRate">
					<xsl:text>Interest Rate: </xsl:text>
					<xsl:value-of select="../fcx:mortgage/fcx:rate/fcx:interestRate"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="../fcx:mortgage/fcx:existingMortgageHolder">
					<xsl:text>Lender: </xsl:text>
					<xsl:value-of select="../fcx:mortgage/fcx:existingMortgageHolder"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$englishId"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="$dealNotesCategoryIdOtherProperty"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="date:date-time()"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:value-of select="$CRLF"/>
				<xsl:if test="fcx:numberOfUnits">
					<xsl:text>Nombre d'entités: </xsl:text>
					<xsl:value-of select="fcx:numberOfUnits"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="fcx:purchasePrice">
					<xsl:text>Prix de vente:  </xsl:text>
					<xsl:value-of select="fcx:purchasePrice"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="fcx:estimatedAppraisalValue">
					<xsl:text>Valeur actuelle:  </xsl:text>
					<xsl:value-of select="fcx:estimatedAppraisalValue"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="../fcx:mortgage/fcx:maturityDate">
					<xsl:text>Date d'échéance:  </xsl:text>
					<xsl:value-of select="../fcx:mortgage/fcx:maturityDate"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="../fcx:mortgage/fcx:rate/fcx:netRate">
					<xsl:text>Taux d'intérêt: </xsl:text>
					<xsl:value-of select="../fcx:mortgage/fcx:rate/fcx:netRate"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
				<xsl:if test="../fcx:mortgage/fcx:lenderSubmission/fcx:lenderProfile/fcx:lenderName">
					<xsl:text>Prêteur: </xsl:text>
					<xsl:value-of select="../fcx:mortgage/fcx:lenderSubmission/fcx:lenderProfile/fcx:lenderName"/>
					<xsl:value-of select="$CRLF"/>
				</xsl:if>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="$frenchId"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesLenderSubmission">
		<DealNotes>
			<dealNotesCategoryId>7</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="//fcx:loanApplication/fcx:dealNotes/fcx:entryDate"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:originatorNotes"/>
			</dealNotesText>
			<xsl:choose>
				<xsl:when test="//fcx:loanApplication/fcx:originator-notes/fcx:language/fcx:code = 1 ">
					<languagePreferenceId>0</languagePreferenceId>
				</xsl:when>
				<xsl:when test="//fcx:loanApplication/fcx:originator-notes/fcx:language/fcx:code = 2 ">
					<languagePreferenceId>1</languagePreferenceId>
				</xsl:when>
			</xsl:choose>
		</DealNotes>
	</xsl:template>
	<xsl:template name="dealNotesLoanApplication">
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="//fcx:loanApplication/fcx:notes/fcx:categoryDd"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="//fcx:loanApplication/fcx:notes/fcx:entryDate"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:value-of select="//fcx:loanApplication/fcx:notes/fcx:text"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="//fcx:loanApplication/fcx:notes/fcx:languageDd"/>
			</languagePreferenceId>
		</DealNotes>
		<DealNotes>
			<dealNotesCategoryId>
				<xsl:value-of select="//fcx:loanApplication/fcx:notes/fcx:categoryDd"/>
			</dealNotesCategoryId>
			<dealNotesDate>
				<xsl:value-of select="//fcx:loanApplication/fcx:notes/fcx:entryDate"/>
			</dealNotesDate>
			<dealNotesText>
				<xsl:value-of select="//fcx:loanApplication/fcx:notes/fcx:text"/>
			</dealNotesText>
			<languagePreferenceId>
				<xsl:value-of select="//fcx:loanApplication/fcx:notes/fcx:languageDd"/>
			</languagePreferenceId>
		</DealNotes>
	</xsl:template>
	<!-- DEALNOTES template - END -->
	<!-- CREDITBUREAUREPORT template - START -->
	<!-- LEN595297 ticket - following template has been modified: 
	<xsl:template name="creditBureauReport">
		<CreditBureauReport>
			<creditReport>
				<xsl:call-template name="creditReportValue"/>
			</creditReport>
		</CreditBureauReport>
	</xsl:template>	-->
	<!-- LEN595297 ticket - this is the new template, it doesn't create empty nodes:-->
	<xsl:template name="creditBureauReport">
		<xsl:if test="$cbText != ''">
			<CreditBureauReport>
				<creditReport>
		 			<xsl:call-template name="creditReportValue"/>
		 		</creditReport>
			</CreditBureauReport>
		</xsl:if>
	</xsl:template>
	<!-- CREDITBUREAUREPORT template - END -->
	<!-- SOURCEFIRMPROFILE template - START -->
	<xsl:template name="firmProfile">
		<SourceFirmProfile>
			<xsl:call-template name="sourceFirmNameValue"/>
			<systemTypeId>
				<xsl:value-of select="$systemTypeIdValue-SourceFirmProfile"/>
			</systemTypeId>
			<xsl:for-each select="fcx:contact">
				<xsl:call-template name="contactSfpSob"/>
			</xsl:for-each>
			<sfShortName>
				<xsl:call-template name="sfShortNameValue"/>
			</sfShortName>
			<sourceFirmCode>
				<xsl:call-template name="sourceFirmCodeValue"/>
			</sourceFirmCode>
			<!-- Express 4.4 - Start -->
			<SFLicenseRegistrationNumber>
				<!-- 4.4.1 GOLD COPY FEB. 2012: This is rule (496) has been changed:
				<xsl:value-of select="fcx:licenseRegistrationNumber"/>	-->
				<!-- 4.4.1 GOLD COPY FEB. 2012: This is new template for rule (496): -->
				<xsl:call-template name="SFLicenseRegistrationNumberValue"/>
			</SFLicenseRegistrationNumber>
			<!-- Express 4.4 - End -->
		</SourceFirmProfile>
	</xsl:template>
	<!-- SOURCEFIRMPROFILE template - END -->
	<!-- SOURCEOFBUSINESSPROFILE template - START -->
	<xsl:template name="sourceOfBusinessProfile">
		<SourceOfBusinessProfile>
			<xsl:for-each select="fcx:contact">
				<xsl:call-template name="contactSfpSob"/>
			</xsl:for-each>
			<SOBPShortName>
				<xsl:call-template name="SOBPShortNameValue"/>
			</SOBPShortName>
			<sourceOfBusinessCategoryId>
				<xsl:call-template name="sourceOfBusinessCategoryIdValue"/>
			</sourceOfBusinessCategoryId>
			<notes>
				<xsl:call-template name="notesValue-SourceOfBusinessProfile"/>
			</notes>
			<sourceOfBusinessCode>
				<xsl:call-template name="sourceOfBusinessCodeValue"/>
			</sourceOfBusinessCode>
			<systemTypeId>
				<xsl:value-of select="$systemTypeIdValue-SourceOfBusinessProfile"/>
			</systemTypeId>
			<!-- Express 4.4 - Start -->
			<SOBLicenseRegistrationNumber>
				<xsl:value-of select="fcx:licenseRegistrationNumber"/>
			</SOBLicenseRegistrationNumber>
			<!-- Express 4.4 - End -->
		</SourceOfBusinessProfile>
	</xsl:template>
	<!-- SOURCEOFBUSINESSPROFILE template - END -->
	<!-- Express 4.4 - Start -->
	<!-- SOURCEOFBUSINESSPROFILEAGENT template - START -->
	<xsl:template name="sourceOfBusinessProfileAgent">
		<SourceOfBusinessProfileAgent>
			<xsl:for-each select="fcx:contact">
				<xsl:call-template name="contactSfpSob"/>
			</xsl:for-each>
			<SOBPShortName>
				<xsl:call-template name="SOBPShortNameValue"/>
			</SOBPShortName>
			<sourceOfBusinessCategoryId>
				<xsl:call-template name="sourceOfBusinessCategoryIdValue"/>
			</sourceOfBusinessCategoryId>
			<notes>
				<xsl:call-template name="notesValue-SourceOfBusinessProfile"/>
			</notes>
			<sourceOfBusinessCode>
				<xsl:call-template name="sourceOfBusinessAgentCodeValue"/>
			</sourceOfBusinessCode>
			<systemTypeId>
				<xsl:value-of select="$systemTypeIdValue-SourceOfBusinessProfile"/>
			</systemTypeId>
			<!-- Express 4.4 - Start -->
			<SOBLicenseRegistrationNumber>
				<xsl:value-of select="fcx:licenseRegistrationNumber"/>
			</SOBLicenseRegistrationNumber>
			<!-- Express 4.4 - End -->
		</SourceOfBusinessProfileAgent>
	</xsl:template>
	<!-- SOURCEOFBUSINESSPROFILEAGENT template - END -->
	<!-- Express 4.4 - End -->
	<!-- PARTYPROFILE template - START -->
	<xsl:template name="partyProfile">
		<PartyProfile>
			<partyName>
				<xsl:call-template name="partyNameValue"/>
			</partyName>
			<partyTypeId>
				<xsl:call-template name="partyTypeIdValue"/>
			</partyTypeId>
			<ptPShortName>
				<xsl:call-template name="ptPShortNameValue"/>
			</ptPShortName>
			<ptPBusinessId>
				<xsl:value-of select="fcx:branchTransitNo"/>
			</ptPBusinessId>
			<partyCompanyName>
				<xsl:value-of select="fcx:partyCompanyName"/>
			</partyCompanyName>
			<notes>
				<xsl:value-of select="fcx:notes"/>
			</notes>
			<location>
				<xsl:value-of select="fcx:branchLocation"/>
			</location>
			<xsl:for-each select="fcx:contact">
				<xsl:call-template name="contact"/>
			</xsl:for-each>
		</PartyProfile>
	</xsl:template>
	<!-- PARTYPROFILE template - END -->
	<!-- QUALIFYDETAIL template - START -->
	<xsl:template name="qualifyDetail">
		<QualifyDetail>
			<amortizationTerm>
				<xsl:value-of select="fcx:amortizationTerm"/>
			</amortizationTerm>
			<interestCompoundingId>
				<xsl:value-of select="fcx:interestCompoundDd"/>
			</interestCompoundingId>
			<pAndIPaymentAmountQualify>
				<xsl:value-of select="../fcx:PAndIPaymentAmountQualify"/>
			</pAndIPaymentAmountQualify>
			<qualifyGds>
				<xsl:call-template name="qualifyGdsValue"/>
			</qualifyGds>
			<qualifyRate>
				<!-- <xsl:value-of select="../fcx:qualifyRate"/>-->
				<xsl:call-template name="qualifyRateValue"/>
			</qualifyRate>
			<qualifyTds>
				<xsl:call-template name="qualifyTdsValue"/>
			</qualifyTds>
			<repaymentTypeId>
				<xsl:value-of select="fcx:repaymentTypeDd"/>
			</repaymentTypeId>
		</QualifyDetail>
	</xsl:template>
	<!-- QUALIFYDETAIL template - END -->
	<!-- COMPONENT template - START -->
	<xsl:template name="component">
		<Component>
			<additionalInformation>
				<xsl:value-of select="fcx:additionalInformation"/>
			</additionalInformation>
			<componentTypeId>
				<xsl:value-of select="fcx:componentTypeDd"/>
			</componentTypeId>
			<firstPaymentDateLimit>
				<xsl:value-of select="fcx:firstPaymentDateLimit"/>
			</firstPaymentDateLimit>
			<postedRate>
				<xsl:value-of select="fcx:interestRate"/>
			</postedRate>
			<MtgProd>
				<mpBusinessId>
					<xsl:value-of select="fcx:mtgProd/fcx:mpBusinessId"/>
				</mpBusinessId>
			</MtgProd>
			<PricingRateInventory>
				<indexEffectiveDate>
					<xsl:value-of select="fcx:pricingRateInventory/fcx:indexEffectiveDate"/>
				</indexEffectiveDate>
			</PricingRateInventory>
			<repaymentTypeId>
				<xsl:value-of select="fcx:repaymentTypeDd"/>
			</repaymentTypeId>
			<teaserMaturityDate>
				<xsl:value-of select="fcx:teaserMaturityDate"/>
			</teaserMaturityDate>
			<teaserNetInterestRate>
				<xsl:value-of select="fcx:teaserNetInterestRate"/>
			</teaserNetInterestRate>
			<teaserPIAmount>
				<xsl:value-of select="fcx:teaserPIAmount"/>
			</teaserPIAmount>
			<teaserRateInterestSaving>
				<xsl:value-of select="fcx:teaserRateInterestSaving"/>
			</teaserRateInterestSaving>
			<xsl:for-each select="fcx:componentMortgage">
				<ComponentMortgage>
					<actualPaymentTerm>
						<xsl:value-of select="fcx:actualPaymentTerm"/>
					</actualPaymentTerm>
					<additionalPrincipal>
						<xsl:value-of select="fcx:additionalPrincipal"/>
					</additionalPrincipal>
					<advanceHold>
						<xsl:value-of select="fcx:advanceHold"/>
					</advanceHold>
					<amortizationTerm>
						<xsl:value-of select="fcx:amortizationTerm"/>
					</amortizationTerm>
					<balanceRemainingAtEndOfTerm>
						<xsl:value-of select="fcx:balanceRemainingAtEndOfTerm"/>
					</balanceRemainingAtEndOfTerm>
					<buyDownRate>
						<xsl:value-of select="fcx:buyDownRate"/>
					</buyDownRate>
					<cashBackAmount>
						<xsl:value-of select="fcx:cashBackAmount"/>
					</cashBackAmount>
					<cashBackPercent>
						<xsl:value-of select="fcx:cashBackPercent"/>
					</cashBackPercent>
					<commissionCode>
						<xsl:value-of select="fcx:commissionCode"/>
					</commissionCode>
					<discount>
						<xsl:value-of select="fcx:discount"/>
					</discount>
					<effectiveAmortizationMonths>
						<xsl:value-of select="fcx:effectiveAmortizationMonths"/>
					</effectiveAmortizationMonths>
					<existingAccountIndicator>
						<xsl:value-of select="fcx:existingAccountIndicator"/>
					</existingAccountIndicator>
					<existingAccountNumber>
						<xsl:value-of select="fcx:existingAccountNumber"/>
					</existingAccountNumber>
					<firstPaymentDate>
						<xsl:value-of select="fcx:firstPaymentDate"/>
					</firstPaymentDate>
					<firstPaymentDateMonthly>
						<xsl:value-of select="fcx:firstPaymentDateMonthly"/>
					</firstPaymentDateMonthly>
					<iadNumberOfDays>
						<xsl:value-of select="fcx:iadNumberOfDays"/>
					</iadNumberOfDays>
					<interestAdjustmentAmount>
						<xsl:value-of select="fcx:interestAdjustmentAmount"/>
					</interestAdjustmentAmount>
					<interestAdjustmentDate>
						<xsl:value-of select="fcx:interestAdjustmentDate"/>
					</interestAdjustmentDate>
					<maturityDate>
						<xsl:value-of select="fcx:maturityDate"/>
					</maturityDate>
					<miAllocateFlag>
						<xsl:value-of select="fcx:miAllocateFlag"/>
					</miAllocateFlag>
					<mortgageAmount>
						<xsl:value-of select="fcx:mortgageAmount"/>
					</mortgageAmount>
					<netInterestRate>
						<xsl:value-of select="fcx:netInterestRate"/>
					</netInterestRate>
					<pAndIPaymentAmount>
						<xsl:value-of select="fcx:PAndIPaymentAmount"/>
					</pAndIPaymentAmount>
					<pAndIPaymentAmountMonthly>
						<xsl:value-of select="fcx:PAndIPaymentAmountMonthly"/>
					</pAndIPaymentAmountMonthly>
					<paymentFrequencyId>
						<xsl:value-of select="fcx:paymentFrequencyDd"/>
					</paymentFrequencyId>
					<perDiemInterestAmount>
						<xsl:value-of select="fcx:perDiemInterestAmount"/>
					</perDiemInterestAmount>
					<premium>
						<xsl:value-of select="fcx:premium"/>
					</premium>
					<prePaymentOptionsId>
						<xsl:value-of select="fcx:prepaymentOptionsDd"/>
					</prePaymentOptionsId>
					<privilegePaymentId>
						<xsl:value-of select="fcx:privilegePaymentDd"/>
					</privilegePaymentId>
					<propertyTaxAllocateFlag>
						<xsl:value-of select="fcx:propertyTaxAllocateFlag"/>
					</propertyTaxAllocateFlag>
					<propertyTaxEscrowAmount>
						<xsl:value-of select="fcx:propertyTaxEscrowAmount"/>
					</propertyTaxEscrowAmount>
					<rateGuaranteePeriod>
						<xsl:value-of select="fcx:rateGuaranteePeriod"/>
					</rateGuaranteePeriod>
					<totalMortgageAmount>
						<xsl:value-of select="fcx:totalMortgageAmount"/>
					</totalMortgageAmount>
					<!-- Following element was added as result of the Data Alignment project:
-->
					<cashBackAmountOverride>
						<xsl:value-of select="fcx:cashBackAmountOverride"/>
					</cashBackAmountOverride>
				</ComponentMortgage>
			</xsl:for-each>
			<xsl:for-each select="fcx:componentLOC">
				<ComponentLOC>
					<advanceHold>
						<xsl:value-of select="fcx:advanceHold"/>
					</advanceHold>
					<commissionCode>
						<xsl:value-of select="fcx:commissionCode"/>
					</commissionCode>
					<discount>
						<xsl:value-of select="fcx:discount"/>
					</discount>
					<existingAccountIndicator>
						<xsl:value-of select="fcx:existingAccountIndicator"/>
					</existingAccountIndicator>
					<existingAccountNumber>
						<xsl:value-of select="fcx:existingAccountNumber"/>
					</existingAccountNumber>
					<firstPaymentDate>
						<xsl:value-of select="fcx:firstPaymentDate"/>
					</firstPaymentDate>
					<interestAdjustmentDate>
						<xsl:value-of select="fcx:interestAdjustmentDate"/>
					</interestAdjustmentDate>
					<locAmount>
						<xsl:value-of select="fcx:locAmount"/>
					</locAmount>
					<miAllocateFlag>
						<xsl:value-of select="fcx:miAllocateFlag"/>
					</miAllocateFlag>
					<netInterestRate>
						<xsl:value-of select="fcx:netInterestRate"/>
					</netInterestRate>
					<pAndIPaymentAmount>
						<xsl:value-of select="fcx:PAndIPaymentAmount"/>
					</pAndIPaymentAmount>
					<pAndIPaymentAmountMonthly>
						<xsl:value-of select="fcx:PAndIPaymentAmountMonthly"/>
					</pAndIPaymentAmountMonthly>
					<paymentFrequencyId>
						<xsl:value-of select="fcx:paymentFrequencyDd"/>
					</paymentFrequencyId>
					<premium>
						<xsl:value-of select="fcx:premium"/>
					</premium>
					<prePaymentOptionsId>
						<xsl:value-of select="fcx:prepaymentOptionsDd"/>
					</prePaymentOptionsId>
					<privilegePaymentId>
						<xsl:value-of select="fcx:privilegePaymentDd"/>
					</privilegePaymentId>
					<propertyTaxAllocateFlag>
						<xsl:value-of select="fcx:propertyTaxAllocateFlag"/>
					</propertyTaxAllocateFlag>
					<propertyTaxEscrowAmount>
						<xsl:value-of select="fcx:propertyTaxEscrowAmount"/>
					</propertyTaxEscrowAmount>
					<totalLocAmount>
						<xsl:value-of select="fcx:totalLOCAmount"/>
					</totalLocAmount>
				</ComponentLOC>
			</xsl:for-each>
			<xsl:for-each select="fcx:componentLoan">
				<ComponentLoan>
					<actualPaymentTerm>
						<xsl:value-of select="fcx:actualPaymentTerm"/>
					</actualPaymentTerm>
					<discount>
						<xsl:value-of select="fcx:discount"/>
					</discount>
					<firstPaymentDate>
						<xsl:value-of select="fcx:firstPaymentDate"/>
					</firstPaymentDate>
					<iadNumberOfDays>
						<xsl:value-of select="fcx:iadNumberOfDays"/>
					</iadNumberOfDays>
					<interestAdjustmentAmount>
						<xsl:value-of select="fcx:interestAdjustmentAmount"/>
					</interestAdjustmentAmount>
					<interestAdjustmentDate>
						<xsl:value-of select="fcx:interestAdjustmentDate"/>
					</interestAdjustmentDate>
					<loanAmount>
						<xsl:value-of select="fcx:loanAmount"/>
					</loanAmount>
					<maturityDate>
						<xsl:value-of select="fcx:maturityDate"/>
					</maturityDate>
					<netInterestRate>
						<xsl:value-of select="fcx:netInterestRate"/>
					</netInterestRate>
					<pAndIPaymentAmount>
						<xsl:value-of select="fcx:PAndIPaymentAmount"/>
					</pAndIPaymentAmount>
					<pAndIPaymentAmountMonthly>
						<xsl:value-of select="fcx:PAndIPaymentAmountMonthly"/>
					</pAndIPaymentAmountMonthly>
					<paymentFrequencyId>
						<xsl:value-of select="fcx:paymentFrequencyDd"/>
					</paymentFrequencyId>
					<perDiemInterestAmount>
						<xsl:value-of select="fcx:perDiemInterestAmount"/>
					</perDiemInterestAmount>
					<premium>
						<xsl:value-of select="fcx:premium"/>
					</premium>
				</ComponentLoan>
			</xsl:for-each>
			<xsl:for-each select="fcx:componentCreditCard">
				<ComponentCreditCard>
					<creditCardAmount>
						<xsl:value-of select="fcx:creditCardAmount"/>
					</creditCardAmount>
				</ComponentCreditCard>
			</xsl:for-each>
			<xsl:for-each select="fcx:componentOverdraft">
				<ComponentOverdraft>
					<overDraftAmount>
						<xsl:value-of select="fcx:overDraftAmount"/>
					</overDraftAmount>
				</ComponentOverdraft>
			</xsl:for-each>
		</Component>
	</xsl:template>
	<!-- COMPONENT template - END -->
	<!-- DOWNPAYMENTSOURCE-ADDITIONALMAPPING template - START -->
	<xsl:template name="downPaymentSource-additionalMapping">
		<DownPaymentSource>
			<xsl:call-template name="downPaymentSourceTypeIdValue-additionalMapping"/>
			<amount>
				<xsl:value-of select="fcx:netLoanAmount"/>
			</amount>
		</DownPaymentSource>
	</xsl:template>
	<!-- DOWNPAYMENTSOURCE-ADDITIONALMAPPING template - END -->
	<!-- borrowerLiabilityMortgage template - START -->
	<xsl:template name="borrowerLiabilityMortgage">
		<Liability>
			<liabilityTypeId>0</liabilityTypeId>
			<liabilityAmount>
				<xsl:value-of select="fcx:balanceRemaining"/>
			</liabilityAmount>
			<liabilityMonthlyPayment>
				<xsl:value-of select="fcx:PAndIPaymentAmountMonthly"/>
			</liabilityMonthlyPayment>
			<includeInGDS>N</includeInGDS>
			<includeInTDS>Y</includeInTDS>
			<liabilityDescription>
			<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (925) has been changed:
				<xsl:variable name="description">
					<xsl:text>Subject Property, </xsl:text>
					<xsl:value-of select="fcx:existingMortgageHolder"/>
					<xsl:variable name="maturityDateValue" select="fcx:maturityDate"/>
					<xsl:if test="($maturityDateValue)">
						<xsl:text> Maturity Date: </xsl:text>
						<xsl:value-of select="fcx:maturityDate"/>
					</xsl:if>
				</xsl:variable>
				<xsl:value-of select="substring(normalize-space($description), 1, 80)"/>	-->
			<!-- 4.4.1 GOLD COPY FEB. 2012: This the new template for rule (925): -->
				<xsl:call-template name="liabilityDescriptionBorrowerLiabMrtgValue"/>
			</liabilityDescription>
			<!-- FXP31613 - 4.4.1, new fields added - START -->
			<maturityDate>
				<xsl:value-of select="fcx:maturityDate"/>
			</maturityDate>
			<creditLimit>
				<xsl:value-of select="fcx:creditLimit"/>
			</creditLimit>
			<!-- FXP31613 - 4.4.1, new fields added - END -->
			<liabilityPayOffTypeId>
				<xsl:value-of select="fcx:payoffTypeDd"/>
			</liabilityPayOffTypeId>
			<!-- 4.4.1 GOLD COPY FEB. 2012: This is new field, rule (931): -->
			<percentOutGDS>
				<xsl:value-of select="$percentOutGDSBorrowerLiabMrtgValue"/>
			</percentOutGDS>
		</Liability>
	</xsl:template>
	<!-- borrowerLiabilityMortgage template - END -->
	<xsl:template name="borrowerAsset">
		<Asset>
			<assetDescription>
				<xsl:variable name="description">
				<!-- 4.4.1 GOLD COPY FEB. 2012: Validation of Language has been addded to the rule (977): -->
					<xsl:choose>
						<xsl:when test="../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Subject Property: </xsl:when>
						<xsl:otherwise>Propriété visée : </xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="fcx:address/fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:streetName"/>
				</xsl:variable>
				<xsl:value-of select="substring(normalize-space($description), 1, 80)"/>
			</assetDescription>
			<assetValue>
				<xsl:if test="fcx:propertyValueIndexId = 'E'">
					<xsl:value-of select="fcx:estimatedAppraisalValue"/>
				</xsl:if>
				<xsl:if test="fcx:propertyValueIndexId = 'P'">
					<xsl:value-of select="fcx:purchasePrice"/>
				</xsl:if>
				<xsl:if test="fcx:propertyValueIndexId = 'A'">
					<xsl:value-of select="fcx:actualAppraisalValue"/>
				</xsl:if>
			</assetValue>
			<!-- 4.4.1 GOLD COPY/EXPRESS 5.0 MAR. 2012: (FXP33477) No longer a default value (979): 
			<assetTypeId>3</assetTypeId>	-->
			<!-- 4.4.1 GOLD COPY/EXPRESS 5.0 MAR. 2012: (FXP33477) new specification (979): -->
			<assetTypeId>
				<xsl:choose>
					<xsl:when test="fcx:occupancyTypeDd = 0 or fcx:occupancyTypeDd = 1 or fcx:occupancyTypeDd = 3">3</xsl:when>	<!-- Residence -->
					<xsl:otherwise>7</xsl:otherwise>	<!-- Rental Property -->
				</xsl:choose>
			</assetTypeId>
		</Asset>
	</xsl:template>
	<!--  #################################################################################
		  ################################ TEMPLATES - END ################################
	      #################################################################################
-->
	<!--  #################################################################################
		  ################################   RULES -START   ###############################
	      #################################################################################
-->
	<xsl:template name="parser">
		<xsl:param name="delimiter"/>
		<xsl:param name="position"/>
		<xsl:param name="counter"/>
		<xsl:param name="str"/>
		<xsl:choose>
			<xsl:when test="$counter &lt; $position">
				<xsl:call-template name="parser">
					<xsl:with-param name="delimiter" select="$delimiter"/>
					<xsl:with-param name="position" select="$position"/>
					<xsl:with-param name="counter" select="$counter+1"/>
					<xsl:with-param name="str" select="substring-after($str, $delimiter)"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="contains($str, $delimiter)">
						<xsl:value-of select="substring-before($str,$delimiter)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$str"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- GENERAL VARIABLES - START -->
	<xsl:variable name="lenderCode">
		<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:lenderProfile/fcx:lenderCode"/>
	</xsl:variable>
	<xsl:variable name="sourceSystem">
		<xsl:call-template name="parser">
			<xsl:with-param name="delimiter" select="'.'"/>
			<xsl:with-param name="position" select="2"/>
			<xsl:with-param name="counter" select="1"/>
			<xsl:with-param name="str" select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:routingSender"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="office">
		<xsl:call-template name="parser">
			<xsl:with-param name="delimiter" select="'.'"/>
			<xsl:with-param name="position" select="3"/>
			<xsl:with-param name="counter" select="1"/>
			<xsl:with-param name="str" select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:routingReceiver"/>
		</xsl:call-template>
	</xsl:variable>
	<!-- street ticket (November 2012) - new variable specific for electronicSystemSource and sourceSystemMailBoxNBR -->
	<xsl:variable name="officeSystemSource">
		<xsl:call-template name="parser">
			<xsl:with-param name="delimiter" select="'.'"/>
			<xsl:with-param name="position" select="3"/>
			<xsl:with-param name="counter" select="1"/>
			<xsl:with-param name="str" select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:routingSender"/>
		</xsl:call-template>
	</xsl:variable>
	<!-- street ticket (November 2012) - changed from routing Receiver to Routing Sender: -->
	<xsl:variable name="user">
		<xsl:call-template name="parser">
			<xsl:with-param name="delimiter" select="'.'"/>
			<xsl:with-param name="position" select="4"/>
			<xsl:with-param name="counter" select="1"/>
			<xsl:with-param name="str" select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:routingSender"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="ecniLenderId">
		<xsl:value-of select="$office"/>
		<!-- <xsl:value-of select="$institutionProfileIdTable/institutionProfileIds/institutionProfileId[@code=$office]/text()"/>
-->
	</xsl:variable>

	<!-- 4.4.1 GOLD COPY FEB. 2012: New variable useSubAgentFlag: -->
	<xsl:variable name="useSubAgentFlag">
		<xsl:choose>
			<xsl:when test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile)))">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- 4.4.1 GOLD COPY FEB. 2012: New variable useLegacyFlag: -->
	<xsl:variable name="useLegacyFlag">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:lenderProfile/fcx:useLegacyUserId = 'Y'">
				<xsl:choose>
					<xsl:when test="$useSubAgentFlag = 'Y'">
						<xsl:choose>
							<xsl:when test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:legacyOfficeId)))
										and not(not(string(//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:legacyUserId)))
										and not(not(string(//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:legacySystem)))
										and not(not(string(//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacyOfficeId)))
										and not(not(string(//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacyUserId)))
										and not(not(string(//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacySystem)))
										and (//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:legacyOfficeId = 
											//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacyOfficeId)
										and (//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:legacySystem = 
											//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacySystem)">Y</xsl:when>
							<xsl:otherwise>N</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacyOfficeId)))
										and not(not(string(//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacyUserId)))
										and not(not(string(//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacySystem)))">Y</xsl:when>
							<xsl:otherwise>N</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>		
	</xsl:variable>	

	<!-- GENERAL VARIABLES - END -->
	<!-- LOOKUP TABLE CALLING - START -->
	<xsl:variable name="escrowPaymentTable" select="document('escrowPayment.xml')"/>
	<xsl:variable name="systemTypeTable" select="document('systemType.xml')"/>
	<xsl:variable name="sourceFirmNameSupressTable" select="document('sourceFirmNameSupress.xml')"/>
	<xsl:variable name="setclosingdateto_todayTable" select="document('setclosingdateto_today.xml')"/>
	<!-- <xsl:variable name="institutionProfileIdTable" select="document('institutionProfileId.xml')"/>
-->
	<xsl:variable name="liabilityUseCbTable" select="document('liabilityUseCb.xml')"/>
	<xsl:variable name="heatDefaultTable" select="document('heatDefault.xml')"/>
	<xsl:variable name="sourceOfBusinessCategoryTable" select="document('sourceOfBusinessCategory.xml')"/>
	<xsl:variable name="countryTable" select="document('country.xml')"/>
	<xsl:variable name="streetTypeTable" select="document('streetType.xml')"/>
	<xsl:variable name="streetDirTable" select="document('streetDir.xml')"/>
	<xsl:variable name="provinceTable" select="document('province.xml')"/>
	<xsl:variable name="paymentTermTypeTable" select="document('paymentTermType.xml')"/>
	<xsl:variable name="propertyExpenseTypeTable" select="document('propertyExpenseType.xml')"/>
	<!-- LOOKUP TABLE CALLING - END -->
	<!-- DEALNOTES template rules - START-->
	<xsl:variable name="dealNotesCategoryIdApplicantInfo">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdSubjectProperty">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdDealExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdApplicantExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdAssetExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdBrokerExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdCBExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdEmploymentExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdIncomeExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdCBReportExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdLifeExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdMortgageExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdParticipantExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdSubjectPropertyPropertyExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdSubjectPropertyExistingMortgageExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdOtherPropertyPropertyExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdOtherPropertyMortgageExtension">13</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdTaxYearExtension">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdPIExtension">10</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdMortgagePayment">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdMortgage">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdWaiverDate">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdMarketSubmission">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdFeeIncludesHeat">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdOtherProperty">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdPropertyExpense">7</xsl:variable>
	<xsl:variable name="dealNotesCategoryIdApplicant">7</xsl:variable>
	<xsl:variable name="englishId">0</xsl:variable>
	<xsl:variable name="frenchId">1</xsl:variable>
	<xsl:variable name="noLanguageId">-1</xsl:variable>
	<xsl:variable name="CRLF">
		<xsl:text>&#10;</xsl:text>
	</xsl:variable>
	<!-- get element name template TODO fix -->
	<xsl:template name="generateElement">
		<xsl:text> </xsl:text>
		<xsl:value-of select="name(.)"/>:
<xsl:value-of select="."/>
	</xsl:template>
	<!-- DEALNOTES template rules - END-->
	<!-- APPLICANTGROUP template rules - START-->
	<xsl:template name="borrowerNumberValue">
		<xsl:call-template name="counter">
			<!-- indicates what is the ApplicantGroup position for the current Applicant:
-->
			<xsl:with-param name="applGroupPosition" select="count(../preceding-sibling::*)"/>
			<xsl:with-param name="nodeCounter" select="0"/>
			<xsl:with-param name="total" select="0"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="counter">
		<xsl:param name="applGroupPosition"/>
		<xsl:param name="nodeCounter"/>
		<xsl:param name="total"/>
		<xsl:choose>
			<xsl:when test="$nodeCounter &lt; $applGroupPosition">
				<xsl:call-template name="counter">
					<xsl:with-param name="applGroupPosition" select="$applGroupPosition"/>
					<xsl:with-param name="nodeCounter" select="$nodeCounter+1"/>
					<xsl:with-param name="total" select="$total+count(../../fcx:applicantGroup[$nodeCounter]/fcx:applicant)"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$total+position()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="borrowerLastNameValue">
		<xsl:value-of select="substring(fcx:name/fcx:lastName, 1, 20)"/>
	</xsl:template>
	<xsl:template name="phoneValue">
		<xsl:if test="fcx:phoneTypeDd = 1">
			<borrowerHomePhoneNumber>
				<xsl:value-of select="fcx:phoneNumber"/>
			</borrowerHomePhoneNumber>
		</xsl:if>
		<xsl:if test="fcx:phoneTypeDd = 3">
			<borrowerWorkPhoneNumber>
				<xsl:value-of select="fcx:phoneNumber"/>
			</borrowerWorkPhoneNumber>
		</xsl:if>
		<!-- Express 4.4 - Start -->
		<xsl:if test="fcx:phoneTypeDd = 2">
			<borrowerCellPhoneNumber>
				<xsl:value-of select="fcx:phoneNumber"/>
			</borrowerCellPhoneNumber>
		</xsl:if>
		<!-- Express 4.4 - End -->
		<xsl:if test="fcx:phoneTypeDd = 4">
			<borrowerFaxNumber>
				<xsl:value-of select="fcx:phoneNumber"/>
			</borrowerFaxNumber>
		</xsl:if>
	</xsl:template>
	<xsl:template name="numberOfDependentsValue">
		<xsl:choose>
			<xsl:when test="fcx:numberOfDependents &gt; 100">99</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:numberOfDependents"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="borrowerTypeIdValue">
		<xsl:if test="../fcx:applicantGroupTypeDd = 0">0</xsl:if>
		<xsl:if test="../fcx:applicantGroupTypeDd = 1">0</xsl:if>
		<xsl:if test="../fcx:applicantGroupTypeDd = 2">1</xsl:if>
	</xsl:template>
	<xsl:template name="bureauAttachmentValue">
		<xsl:choose>
			<xsl:when test="not(not(string(fcx:creditBureauReport/fcx:creditReport/fcx:creditReportText)))">Y</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="bankruptcyStatusIdValue">
		<xsl:choose>
			<xsl:when test="fcx:bankruptcyStatusDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:bankruptcyStatusDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="staffOfLenderValue">
		<xsl:choose>
			<xsl:when test="fcx:isEmployeeDd = 1">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="creditBureauNameIdValue">
		<!-- Removed due to logic change (EXP31555)
		<xsl:if test="fcx:creditBureauReport/fcx:creditBureauNameDd = -1">0</xsl:if>
		<xsl:if test="fcx:creditBureauReport/fcx:creditBureauNameDd = 1">1</xsl:if>
	 -->
		<!-- EXP31555 new logic: -->
		<xsl:variable name="firstCreditBureauReportNameDd">
			<xsl:value-of select="fcx:creditBureauReport/fcx:creditBureauNameDd"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$firstCreditBureauReportNameDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$firstCreditBureauReportNameDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="borrowerWorkPhoneExtensionValue">
		<xsl:for-each select="fcx:phone">
			<xsl:if test="fcx:phoneTypeDd = 3">
				<borrowerWorkPhoneExtension>
					<xsl:value-of select="fcx:phoneExtension"/>
				</borrowerWorkPhoneExtension>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="borrowerGenderIdValue">
		<!-- Ticket FXP26910, rule change: -->
		<xsl:choose>
			<xsl:when test="not(not(string(fcx:gender)))">
				<xsl:if test="normalize-space(fcx:gender) = 'M'">
					<borrowerGenderId>1</borrowerGenderId>
				</xsl:if>
				<xsl:if test="normalize-space(fcx:gender) = 'F'">
					<borrowerGenderId>2</borrowerGenderId>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="lifePercentCoverageReqValue-Borrower">
		<xsl:for-each select="fcx:lifeAndDisabilityApplication/fcx:insuranceCoverage">
			<xsl:if test="fcx:insuranceTypeDd = 1">
				<lifePercentCoverageReq>
					<xsl:value-of select="fcx:percentCoverageRequest"/>
				</lifePercentCoverageReq>
				<lifeStatusId>
					<xsl:value-of select="fcx:insuranceStatusDd"/>
				</lifeStatusId>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="disabilityStatusIdValue-Borrower">
		<xsl:for-each select="fcx:lifeAndDisabilityApplication/fcx:insuranceCoverage">
			<xsl:if test="fcx:insuranceTypeDd = 2">
				<disabilityStatusId>
					<xsl:value-of select="fcx:insuranceStatusDd"/>
				</disabilityStatusId>
				<disPercentCoverageReq>
					<xsl:value-of select="fcx:percentCoverageRequest"/>
				</disPercentCoverageReq>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="solicitationIdValue">
		<xsl:if test="not(not(string(fcx:solicitationDd)))">
			<solicitationId>
				<xsl:choose>
					<xsl:when test="fcx:solicitationDd = 1">1</xsl:when>
					<!-- yes -->
					<xsl:otherwise>2</xsl:otherwise>
					<!-- no -->
				</xsl:choose>
			</solicitationId>
		</xsl:if>
	</xsl:template>
	<!-- Express 4.4 - Start -->
	<!--
	<xsl:template name="creditBureauLanguageIDValue">
		<creditBureauLanguageID>
			<xsl:choose>
				<xsl:when test="not(not(string(fcx:creditBureauReport/fcx:language)))">
					<xsl:choose>
						<xsl:when test="fcx:creditBureauReport/fcx:language = 'en_ca'">0</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>-1</xsl:otherwise>
			</xsl:choose>
		</creditBureauLanguageID>
	</xsl:template>
	Deleted, this value is not coming from Expert 
	-->
	<!-- Express 4.4 - End -->
	<xsl:template name="identificationNumberValue">
		<xsl:value-of select="substring(fcx:identificationNumber, 1, 30)"/>
	</xsl:template>
	<xsl:template name="borrowerAddressTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:addressTypeDd != 2">
				<borrowerAddressTypeId>
					<xsl:value-of select="fcx:addressTypeDd"/>
				</borrowerAddressTypeId>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="residentialStatusIdValue">
		<xsl:choose>
			<xsl:when test="fcx:residentialStatusDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:residentialStatusDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="occupationIdValue">
		<xsl:choose>
			<xsl:when test="fcx:occupationDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:occupationDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="industrySectorIdValue">
		<xsl:choose>
			<xsl:when test="fcx:industrySectorDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:industrySectorDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="jobTitleIdValue">0</xsl:variable>
	<!-- APPLICANTGROUP template rules - END-->

	<!-- EMPLOYMENT HISTORY INCOME template rules - START -->
	<xsl:template name="incomeTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:incomeTypeDd = -1">11</xsl:when>
			<xsl:otherwise>
				<!-- Ticket EXP26765 (TypeDd = 11 added): -->
				<xsl:choose>
					<xsl:when test="fcx:incomeTypeDd = 11">12</xsl:when>
					<xsl:otherwise>
						<!-- Ticket FXP26759 (TypeDd = 10 added): -->
						<xsl:choose>
							<xsl:when test="fcx:incomeTypeDd = 10">11</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="fcx:incomeTypeDd"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="incIncludeInGDSValue">
		<xsl:choose>
			<xsl:when test="../fcx:employmentHistoryStatusDd = 0">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="incIncludeInTDSValue">
		<xsl:choose>
			<xsl:when test="../fcx:employmentHistoryStatusDd = 0">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="incomeDescriptionValue">
		<xsl:choose>
			<xsl:when test="not(not(string(fcx:incomeDescription)))">
				<xsl:value-of select ="fcx:incomeDescription"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not(not(string(../fcx:employerName)))">
						<xsl:value-of select="../fcx:employerName"/>
					</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- EMPLOYMENT HISTORY INCOME template rules - END -->
	
	<!-- INCOME2 template rules - START -->
	<xsl:template name="incomeTypeIdValue2">
		<xsl:choose>
			<xsl:when test="fcx:incomeTypeDd = -1">11</xsl:when>
			<xsl:otherwise>
				<!-- Ticket FXP26759, adding types 10 and 11: -->
				<xsl:choose>
					<xsl:when test="fcx:incomeTypeDd = 11">12</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:incomeTypeDd = 10">11</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="fcx:incomeTypeDd"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- INCOME2 template rules - END -->

	<!-- INCOME3 template rules - START -->
	<xsl:variable name="incomePeriodIValue3">3</xsl:variable>
	<xsl:template name="incomeTypeIdValue3">
		<xsl:choose>
			<xsl:when test="not(not(string(../fcx:rentalOffsetOption)))">
				<xsl:choose>
					<xsl:when test="../fcx:rentalOffsetOption = 1">
						<incomeTypeId>10</incomeTypeId>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="../fcx:rentalOffsetOption = 2">
								<incomeTypeId>9</incomeTypeId>
							</xsl:when>
							<xsl:otherwise>
								<incomeTypeId>10</incomeTypeId>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule for the field has been changed	
	<xsl:template name="incomeDescriptionValue3">
		<xsl:choose>
			<xsl:when test="not(string(fcx:incomeDescription))">Rental Income from Subject Property / Revenu de Location de la Propriété Visée</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:incomeDescription"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>		-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is the new rule for the field: -->	
	<xsl:template name="incomeDescriptionValue3">
		<xsl:choose>
			<xsl:when test="not(not(string(fcx:incomeDescription)))">
				<xsl:value-of select="fcx:incomeDescription"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="../../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Rental Income from Subject Property</xsl:when>
					<xsl:otherwise>Revenu de Location de la Propriété Visée</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- INCOME3 template rules - END -->
	<!-- INCOME4 template rules - START -->
	<xsl:variable name="incomePeriodIValue4">3</xsl:variable>
	<xsl:template name="incomeTypeIdValue4">
		<xsl:choose>
			<xsl:when test="not(not(string(../fcx:rentalOffsetOption)))">
				<xsl:choose>
					<xsl:when test="../fcx:rentalOffsetOption = 2">
						<!--  Ticket FXP26758, changing type to "9": -->
						<incomeTypeId>9</incomeTypeId>
					</xsl:when>
					<xsl:otherwise>
						<incomeTypeId>10</incomeTypeId>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- Ticket FXP26758, changing to "Y": -->
	<xsl:variable name="incIncludeInGDSValue4">Y</xsl:variable>
	<xsl:variable name="incIncludeInTDSValue4">Y</xsl:variable>
	<xsl:template name="incomeDescriptionValue4">
		<!-- Ticket FXP26796, new description rule: -->
		<xsl:choose>
			<xsl:when test="not(not(string(fcx:incomeDescription)))">
				<xsl:value-of select="fcx:incomeDescription"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="streetTypeId">
					<xsl:value-of select="../fcx:address/fcx:streetTypeDd"/>
				</xsl:variable>
				<xsl:variable name="streetDirectionId">
					<xsl:value-of select="../fcx:address/fcx:streetDirectionDd"/>
				</xsl:variable>
				<xsl:variable name="provinceId">
					<xsl:value-of select="../fcx:address/fcx:provinceDd"/>
				</xsl:variable>
				<xsl:variable name="languageId">
					<xsl:choose>
						<xsl:when test="$provinceId = 11">1</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="countryId">
					<xsl:value-of select="../fcx:address/fcx:countryTypeDd"/>
				</xsl:variable>
				<xsl:variable name="description">
					<xsl:choose>
						<xsl:when test="$provinceId != 11">
						<!-- 4.4.1 GOLD COPY MAR. 2012: FXP33426, Validate the existence of unit number: -->
							<xsl:if test="not(not(string(../fcx:address/fcx:unitNumber)))">
								<xsl:value-of select="../fcx:address/fcx:unitNumber"/>
								<xsl:text>-</xsl:text>
							</xsl:if>
							<xsl:value-of select="../fcx:address/fcx:streetNumber"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="../fcx:address/fcx:streetName"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$languageId]/text()"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="../fcx:address/fcx:city"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$languageId]/text()"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="../fcx:address/fcx:postalFsa"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="../fcx:address/fcx:postalLdu"/>
							<xsl:text> </xsl:text>
							<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
							<xsl:value-of select="$countryTable/countries/country[@code=$countryId and @lang=$languageId]/text()"/>
						</xsl:when>
						<xsl:otherwise>
						<!-- 4.4.1 GOLD COPY MAR. 2012: FXP33426, Validate the existence of unit number: -->
							<xsl:if test="not(not(string(../fcx:address/fcx:unitNumber)))">
								<xsl:value-of select="../fcx:address/fcx:unitNumber"/>
								<xsl:text>-</xsl:text>
							</xsl:if>
							<xsl:value-of select="../fcx:address/fcx:streetNumber"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="../fcx:address/fcx:streetName"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$languageId]/text()"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="../fcx:address/fcx:city"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$languageId]/text()"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="../fcx:address/fcx:postalFsa"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="../fcx:address/fcx:postalLdu"/>
							<xsl:text> </xsl:text>
							<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
							<xsl:value-of select="$countryTable/countries/country[@code=$countryId and @lang=$languageId]/text()"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Ticket FXP27404: only first 80 characters -->
				<xsl:value-of select="substring(normalize-space($description), 1, 80)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- INCOME4 template rules - END -->
	<!-- ASSET template rules - START -->
	<xsl:template name="assetTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:assetTypeDd = -1">8</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:assetTypeDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ASSET template rules - END -->
	<!-- ASSET2 template rules - START -->
	<xsl:template name="assetDescriptionValue2">
		<!-- FXP26596 (PCR): -->
		<xsl:variable name="streetTypeId">
			<xsl:value-of select="fcx:address/fcx:streetTypeDd"/>
		</xsl:variable>
		<xsl:variable name="streetDirectionId">
			<xsl:value-of select="fcx:address/fcx:streetDirectionDd"/>
		</xsl:variable>
		<xsl:variable name="provinceId">
			<xsl:value-of select="fcx:address/fcx:provinceDd"/>
		</xsl:variable>
		<xsl:variable name="languageId">
			<xsl:choose>
				<xsl:when test="$provinceId = 11">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="countryId">
			<xsl:value-of select="fcx:address/fcx:countryTypeDd"/>
		</xsl:variable>
		<xsl:variable name="description">
			<xsl:choose>
				<xsl:when test="$provinceId != 11">
				<!-- 4.4.1 GOLD COPY MAR. 2012: FXP33426, verify the existence of unit number: -->
					<xsl:if test="not(not(string(fcx:address/fcx:unitNumber)))">
						<xsl:value-of select="fcx:address/fcx:unitNumber"/>
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:value-of select="fcx:address/fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:streetName"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:city"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:postalFsa"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:postalLdu"/>
					<xsl:text> </xsl:text>
					<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
					<xsl:value-of select="$countryTable/countries/country[@code=$countryId and @lang=$languageId]/text()"/>
				</xsl:when>
				<xsl:otherwise>
				<!-- 4.4.1 GOLD COPY MAR. 2012: FXP33426, verify the existence of unit number: -->
					<xsl:if test="not(not(string(fcx:address/fcx:unitNumber)))">
						<xsl:value-of select="fcx:address/fcx:unitNumber"/>
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:value-of select="fcx:address/fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:streetName"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:city"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:postalFsa"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:address/fcx:postalLdu"/>
					<xsl:text> </xsl:text>
					<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
					<xsl:value-of select="$countryTable/countries/country[@code=$countryId and @lang=$languageId]/text()"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Ticket FXP27404: only first 80 characters -->
		<xsl:value-of select="substring(normalize-space($description), 1, 80)"/>
	</xsl:template>
	<xsl:template name="assetValueValue2">
		<xsl:if test="fcx:propertyValueIndexId = 'E'">
			<xsl:value-of select="fcx:estimatedAppraisalValue"/>
		</xsl:if>
		<xsl:if test="fcx:propertyValueIndexId = 'P'">
			<xsl:value-of select="fcx:purchasePrice"/>
		</xsl:if>
		<xsl:if test="fcx:propertyValueIndexId = 'A'">
			<xsl:value-of select="fcx:actualAppraisalValue"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="assetTypeIdValue2">
		<!-- Ticket FXP26979, rule change: -->
		<xsl:choose>
			<xsl:when test="fcx:occupancyTypeDd = 0 or fcx:occupancyTypeDd = 1 or fcx:occupancyTypeDd = 3">3</xsl:when>
			<xsl:otherwise>7</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ASSET2 template rules - END -->
	<!-- LIABILITY template rules (BROKER LIABILITY) - START -->
	<xsl:template name="liabilityTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:liabilityTypeDd = 14">15</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="fcx:liabilityTypeDd = 15">16</xsl:when>
					<xsl:otherwise>
						<!-- Ticket EXP26753: liabilityTypeDd = 16 added -->
						<xsl:choose>
							<xsl:when test="fcx:liabilityTypeDd = 16">22</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="fcx:liabilityTypeDd"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="includeInGDSValue">N</xsl:variable>
	<!-- 4.4.1 GOLD COPY FEB. 2012: The following rule has been changed: 
	<xsl:template name="includeInTDSValue">
		<xsl:choose>
			<xsl:when test="fcx:liabilityTypeDd = 0">0</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$liabilityUseCbLookupValue = 'Y'">N</xsl:when>
					<xsl:otherwise>Y</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>		-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is the new rule for the field: -->
	<xsl:template name="includeInTDSValue">
		<xsl:choose>
			<xsl:when test="fcx:liabilityTypeDd = 0 or fcx:liabilityTypeDd = 15">Y</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$liabilityUseCbLookupValue = 'Y'and not(not(string(../fcx:creditBureau)))">N</xsl:when>
					<xsl:otherwise>Y</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="liabilityUseCbLookupValue">
		<xsl:value-of select="$liabilityUseCbTable/liabilityUseCbs/liabilityUseCb[@code=$ecniLenderId]/text()"/>
	</xsl:variable>
	<xsl:template name="liabilityPayOffTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:liabilityPayOffTypeDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:liabilityPayOffTypeDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: The following rule has been changed:
	<xsl:template name="percentOutGDSValue">
		<xsl:choose>
			<xsl:when test="fcx:liabilityTypeDd = 0">0</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$liabilityUseCbLookupValue = 'Y'">2</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>		-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This the new rule for the field: -->
	<xsl:template name="percentOutGDSValue">
		<xsl:choose>
			<xsl:when test="fcx:liabilityTypeDd = 0 or fcx:liabilityTypeDd = 15">0</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$liabilityUseCbLookupValue = 'Y' and not(not(string(../fcx:creditBureau)))">2</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="percentOutTDSValue">0</xsl:variable>
	<!-- LIABILITY template rules (BROKER LIABILITY) - END -->

	<!-- LIABILITY2 template rules (FOR OTHER PROPERTY) - START -->
	<xsl:variable name="includeInGDSValue2">N</xsl:variable>
	<xsl:variable name="includeInTDSValue2">Y</xsl:variable>
	<xsl:template name="liabilityDescriptionValue2">
		<!-- 4.4.1 GOLD COPY FEB. 2012: New variable for English or French introduction: -->
		<xsl:variable name="introduction">
			<xsl:choose>
				<xsl:when test="../../../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Other property: </xsl:when>
				<xsl:otherwise>Propriété détenues: </xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- FXP26596 (PCR): -->
		<xsl:variable name="streetTypeId">
			<xsl:value-of select="../fcx:property/fcx:address/fcx:streetTypeDd"/>
		</xsl:variable>
		<xsl:variable name="streetDirectionId">
			<xsl:value-of select="../fcx:property/fcx:address/fcx:streetDirectionDd"/>
		</xsl:variable>
		<xsl:variable name="provinceId">
			<xsl:value-of select="../fcx:property/fcx:address/fcx:provinceDd"/>
		</xsl:variable>
		<xsl:variable name="languageId">
			<xsl:choose>
				<xsl:when test="$provinceId = 11">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="countryId">
			<xsl:value-of select="../fcx:property/fcx:address/fcx:countryTypeDd"/>
		</xsl:variable>
		<xsl:variable name="description">
			<xsl:choose>
				<xsl:when test="$provinceId != 11">
					<xsl:value-of select="$introduction"/>	<!-- 4.4.1 GOLD COPY FEB. 2012: New variable "introduction" is placed here-->
					<!-- 4.4.1 GOLD COPY MAR. 2012: FXP33426, verify the existence of unit number: -->
					<xsl:if test="not(not(string(../fcx:property/fcx:address/fcx:unitNumber)))">
						<xsl:value-of select="../fcx:property/fcx:address/fcx:unitNumber"/>
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:streetName"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:city"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:postalFsa"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:postalLdu"/>
					<xsl:text> </xsl:text>
					<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
					<xsl:value-of select="$countryTable/countries/country[@code=$countryId and @lang=$languageId]/text()"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$introduction"/>	<!-- 4.4.1 GOLD COPY FEB. 2012: New variable "introduction"is placed here-->
					<!-- 4.4.1 GOLD COPY MAR. 2012: FXP33426, verify the existence of unit number: -->
					<xsl:if test="not(not(string(../fcx:property/fcx:address/fcx:unitNumber)))">
						<xsl:value-of select="../fcx:property/fcx:address/fcx:unitNumber"/>
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:streetName"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:city"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:postalFsa"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="../fcx:property/fcx:address/fcx:postalLdu"/>
					<xsl:text> </xsl:text>
					<!-- 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: -->
					<xsl:value-of select="$countryTable/countries/country[@code=$countryId and @lang=$languageId]/text()"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="substring(normalize-space($description), 1, 80)"/>
	</xsl:template>
	<xsl:variable name="percentOutGDSValue2">0</xsl:variable>
	<xsl:variable name="percentOutTDSValue2">0</xsl:variable>
	<!-- LIABILITY2 (FOR OTHER PROPERTY) template rules - END -->
	
	<!-- LIABILITY3 template rules (CREDIT BUREAU) - START -->
	<xsl:template name="liabilityTypeIdValue3">
		<xsl:choose>
			<xsl:when test="fcx:liabilityTypeDd = 14">15</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="fcx:liabilityTypeDd = 15">16</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="fcx:liabilityTypeDd"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="includeInGDSValue3">N</xsl:variable>
	<xsl:template name="includeInTDSValue3">
		<xsl:choose>
			<xsl:when test="$liabilityUseCbLookupValue = 'Y'">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="percentOutGDSValue3">
		<xsl:choose>
			<xsl:when test="$liabilityUseCbLookupValue = 'Y'">0</xsl:when>
			<xsl:otherwise>2</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="percentOutTDSValue3">0</xsl:variable>
	<!-- LIABILITY3 template rules (CREDIT BUREAU) - END -->
	
	<!--INSURANCEONLYAPPLICANT template rules - START -->
	<xsl:template name="lifePercentCoverageReqValue-InsureOnlyApplicant">
		<xsl:for-each select="fcx:insuranceDetails/fcx:insuranceCoverage">
			<xsl:if test="fcx:insuranceTypeDd = 1">
				<lifePercentCoverageReq>
					<xsl:value-of select="fcx:percentCoverageRequest"/>
				</lifePercentCoverageReq>
				<lifeStatusId>
					<xsl:value-of select="fcx:insuranceStatusDd"/>
				</lifeStatusId>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="disabilityStatusIdValue-InsureOnlyApplicant">
		<xsl:for-each select="fcx:insuranceDetails/fcx:insuranceCoverage">
			<xsl:if test="fcx:insuranceTypeDd = 2">
				<disabilityStatusId>
					<xsl:value-of select="fcx:insuranceStatusDd"/>
				</disabilityStatusId>
				<disPercentCoverageReq>
					<xsl:value-of select="fcx:percentCoverageRequest"/>
				</disPercentCoverageReq>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--INSURANCEONLYAPPLICANT template rules - END -->
	<!-- PROPERTY template rules - START -->
	<xsl:template name="propertyTypeIdValue">
		<!-- Ticket FXP26655 -->
		<xsl:choose>
			<xsl:when test="fcx:dwellingTypeDd = 4">
				<xsl:choose>
					<xsl:when test="fcx:propertyTypeDd = 2">16</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:propertyTypeDd = 0">0</xsl:when>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="fcx:propertyTypeDd = 1">20</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:dwellingTypeDd = 0 or
											fcx:dwellingTypeDd = 1 or
											fcx:dwellingTypeDd = 11 or
											fcx:dwellingTypeDd = 12">0</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="fcx:dwellingTypeDd = 2 or
													fcx:dwellingTypeDd = 3 or
													fcx:dwellingTypeDd = 8 or
													fcx:dwellingTypeDd = 9 or
													fcx:dwellingTypeDd = 13 or
													fcx:dwellingTypeDd = 14">1</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="fcx:dwellingTypeDd = 5 or
														fcx:dwellingTypeDd = 6">2</xsl:when>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="propertyUsageIdValue">
		<xsl:choose>
			<xsl:when test="fcx:occupancyTypeDd = 0 or fcx:occupancyTypeDd = 1">0</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="fcx:occupancyTypeDd = 2">1</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:occupancyTypeDd = 3">2</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="dwellingStyleIdValue">
		<!-- Ticket FXP26747 - StyleDd mapping was changed: -->
		<xsl:if test="fcx:dwellingStyleDd = -1">1</xsl:if>
		<xsl:if test="fcx:dwellingStyleDd = 0">0</xsl:if>
		<xsl:if test="fcx:dwellingStyleDd = 1">2</xsl:if>
		<xsl:if test="fcx:dwellingStyleDd = 2">3</xsl:if>
		<xsl:if test="fcx:dwellingStyleDd = 3">4</xsl:if>
		<xsl:if test="fcx:dwellingStyleDd = 4">5</xsl:if>
		<xsl:if test="fcx:dwellingStyleDd = 5">6</xsl:if>
		<xsl:if test="fcx:dwellingStyleDd = 6">1</xsl:if>
	</xsl:template>
	<xsl:template name="occupancyTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:occupancyTypeDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<!-- Ticket EXP26844, type = 1 added: -->
					<xsl:when test="fcx:occupancyTypeDd = 1">0</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="fcx:occupancyTypeDd "/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="propertyStreetNameAddressLine2StreetTypeValue">
		<xsl:variable name="propertyStreetName">
			<xsl:choose>
				<xsl:when test="string-length(normalize-space(fcx:address/fcx:streetName)) &gt; 20">
					<xsl:choose>
						<xsl:when test="contains(normalize-space(substring(fcx:address/fcx:streetName, 1, 20)), ' ')">
							<xsl:value-of select="substring-before(normalize-space(substring(fcx:address/fcx:streetName, 1, 20)), ' ')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring(normalize-space(fcx:address/fcx:streetName), 1, 20 )"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="normalize-space(fcx:address/fcx:streetName)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="propertyAddressLine2">
			<xsl:value-of select="substring-after(normalize-space(fcx:address/fcx:streetName), $propertyStreetName)"/>
		</xsl:variable>
		<xsl:variable name="streetId">
			<xsl:choose>
				<xsl:when test="fcx:address/fcx:streetTypeDd &lt; 100">
					<xsl:value-of select="fcx:address/fcx:streetTypeDd"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="streetTypeTableLookupKey">
			<xsl:value-of select="fcx:address/fcx:streetTypeDd"/>
		</xsl:variable>
		<xsl:variable name="streetDescription">
			<xsl:choose>
				<xsl:when test="$streetId = 0">
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeTableLookupKey]/text()"/>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:variable>
		<streetTypeId>
			<xsl:value-of select="$streetId"/>
		</streetTypeId>
		<propertyStreetName>
			<xsl:value-of select="$propertyStreetName"/>
		</propertyStreetName>
		<propertyAddressLine2>
			<xsl:value-of select="normalize-space(concat($propertyAddressLine2, ' ', $streetDescription))"/>
		</propertyAddressLine2>
	</xsl:template>
	<xsl:template name="heatTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:heatTypeDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:heatTypeDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="insulatedWithUFFIValue">
		<xsl:choose>
			<xsl:when test="normalize-space(fcx:insulatedWithUffi)">
				<xsl:value-of select="normalize-space(fcx:insulatedWithUffi)"/>
			</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="lotSizeValue">
		<xsl:choose>
			<xsl:when test="not(not(string(fcx:lotSize)))">
				<lotSize>
					<xsl:value-of select="round(fcx:lotSize)"/>
				</lotSize>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="lotSizeDepthValue">
		<xsl:choose>
			<xsl:when test="not(not(string(fcx:lotSize)))">
				<lotSizeDepth>
					<xsl:value-of select="round(fcx:lotSize)"/>
				</lotSizeDepth>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="provinceIdValue">
		<xsl:choose>
			<!-- Ticket EXP26784 -->
			<xsl:when test="fcx:address/fcx:provinceDd &gt; 13 or
							fcx:address/fcx:provinceDd &lt; 1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:address/fcx:provinceDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- 4.4.1 GOLD COPY FEB. 2012: purchasePrice has a new rule:	-->
	<xsl:template name="purchasePriceValue">
		<xsl:choose>
			<xsl:when test = "not(not(string(fcx:purchasePrice))) and fcx:propertyValueIndexId = 'P'">
				<xsl:value-of select = "fcx:purchasePrice"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test = "not(not(string(fcx:estimatedAppraisalValue))) and fcx:propertyValueIndexId = 'E'">
						<xsl:value-of select = "fcx:estimatedAppraisalValue"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test = "not(not(string(fcx:actualAppraisalValue))) and fcx:propertyValueIndexId = 'A'">
								<xsl:value-of select = "fcx:actualAppraisalValue"/>
							</xsl:when>
							<xsl:otherwise/> <!-- if none of the aboves is true then no value is assigned -->
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="lotSizeFrontageValue">
		<xsl:choose>
			<xsl:when test="not(string(fcx:lotSize))"/>
			<xsl:otherwise>
				<lotSizeFrontage>1</lotSizeFrontage>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="numberOfUnitsValue">
		<xsl:choose>
			<xsl:when test="not(string(fcx:numberOfUnits))">1</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="fcx:numberOfUnits > 99">99</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="fcx:numberOfUnits"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="propertyOccurenceNumberValue">1</xsl:variable>
	<xsl:template name="streetDirectionIdValue">
		<xsl:choose>
			<xsl:when test="fcx:address/fcx:streetDirectionDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:address/fcx:streetDirectionDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="dwellingTypeIdValue">
		<!-- Ticket FXP26746 - TypeDd 0 and 1 added: -->
		<xsl:if test="fcx:dwellingTypeDd=0">0</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=1">1</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=2">2</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=3">2</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=4">3</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=5">4</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=6">4</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=7">5</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=8">6</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=9">6</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=10">7</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=11">8</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=12">8</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=13">9</xsl:if>
		<xsl:if test="fcx:dwellingTypeDd=14">9</xsl:if>
	</xsl:template>
	<xsl:template name="garageSizeIdValue">
		<xsl:if test="fcx:garageSizeDd != -1">
			<garageSizeId>
				<xsl:value-of select="fcx:garageSizeDd"/>
			</garageSizeId>
		</xsl:if>
	</xsl:template>
	<xsl:template name="garageTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:garageTypeDd = -1">3</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:garageTypeDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="tenureTypeIdValue">
		<xsl:if test="fcx:propertyTypeDd = 0">1</xsl:if>
		<xsl:if test="fcx:propertyTypeDd = 1">2</xsl:if>
		<xsl:if test="fcx:propertyTypeDd = 2">3</xsl:if>
	</xsl:template>
	<xsl:template name="requestAppraisalIdValue">
		<xsl:choose>
			<xsl:when test="fcx:requestAppraisalDd = 1">
				<requestAppraisalId>1</requestAppraisalId>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="fcx:requestAppraisalDd = 0">
					<requestAppraisalId>2</requestAppraisalId>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- PROPERTY template rules - END -->
	<!-- PROPERTYEXPENSE template rules - START -->
	<xsl:template name="propertyExpenseTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:propertyExpenseTypeDd = 3">5</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="fcx:propertyExpenseTypeDd = 4">5</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:propertyExpenseTypeDd = 5">5</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="fcx:propertyExpenseTypeDd = 6">5</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="fcx:propertyExpenseTypeDd = 7">5</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="fcx:propertyExpenseTypeDd = 8">5</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="fcx:propertyExpenseTypeDd"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="propertyExpenseAmountValue">
		<xsl:choose>
			<xsl:when test="fcx:propertyExpenseTypeDd != 2">
				<xsl:value-of select="fcx:propertyExpenseAmount"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="heatMin">
					<xsl:value-of select="$heatDefaultTable/heatDefaults/heatDefault[@code=$ecniLenderId]/valueHeatMin/text()"/>
				</xsl:variable>
				<xsl:variable name="heatMax">
					<xsl:value-of select="$heatDefaultTable/heatDefaults/heatDefault[@code=$ecniLenderId]/valueHeatMax/text()"/>
				</xsl:variable>
				<xsl:variable name="heatZero">
					<xsl:value-of select="$heatDefaultTable/heatDefaults/heatDefault[@code=$ecniLenderId]/valueHeatZero/text()"/>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="not(string($heatMin))">	<!-- 4.4.1 GOLD COPY FEB. 2012: the next line was changed from -1000000 -->
						<xsl:value-of select="fcx:propertyExpenseAmount"/>	
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<!-- Ticket FXP26605: -->
							<xsl:when test="fcx:propertyExpenseAmount and 
											fcx:propertyExpenseAmount != 0 and
											fcx:propertyExpenseAmount &gt; $heatMin and
											fcx:propertyExpenseAmount &lt; $heatMax">
								<xsl:value-of select="fcx:propertyExpenseAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="fcx:propertyExpensePeriodDd = 0">		<!-- Annually -->
									<xsl:if test="not(string(fcx:propertyExpenseAmount))">
										<xsl:value-of select="$heatZero"/>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="fcx:propertyExpenseAmount = 0">
											<xsl:value-of select="$heatZero"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="fcx:propertyExpenseAmount &lt; $heatMin">
												<xsl:value-of select="$heatMin"/>
											</xsl:if>
											<xsl:if test="fcx:propertyExpenseAmount &gt; $heatMax">
												<xsl:value-of select="$heatMax"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="fcx:propertyExpensePeriodDd = 1">		<!-- Semi-Annually -->
									<xsl:if test="not(string(fcx:propertyExpenseAmount))">
										<xsl:value-of select="$heatZero div 2"/>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="fcx:propertyExpenseAmount = 0">
											<xsl:value-of select="$heatZero div 2"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="fcx:propertyExpenseAmount &lt; $heatMin">
												<xsl:value-of select="$heatMin div 2"/>
											</xsl:if>
											<xsl:if test="fcx:propertyExpenseAmount &gt; $heatMax">
												<xsl:value-of select="$heatMax div 2"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="fcx:propertyExpensePeriodDd = 2">		<!-- Quarterly -->
									<xsl:if test="not(string(fcx:propertyExpenseAmount))">
										<xsl:value-of select="$heatZero div 4"/>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="fcx:propertyExpenseAmount = 0">
											<xsl:value-of select="$heatZero div 4"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="fcx:propertyExpenseAmount &lt; $heatMin">
												<xsl:value-of select="$heatMin div 4"/>
											</xsl:if>
											<xsl:if test="fcx:propertyExpenseAmount &gt; $heatMax">
												<xsl:value-of select="$heatMax div 4"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="fcx:propertyExpensePeriodDd = 3">		<!-- Monthly -->
									<xsl:if test="not(string(fcx:propertyExpenseAmount))">
										<xsl:value-of select="$heatZero div 12"/>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="fcx:propertyExpenseAmount = 0">
											<xsl:value-of select="$heatZero div 12"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="fcx:propertyExpenseAmount &lt; $heatMin">
												<xsl:value-of select="$heatMin div 12"/>
											</xsl:if>
											<xsl:if test="fcx:propertyExpenseAmount &gt; $heatMax">
												<xsl:value-of select="$heatMax div 12"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="fcx:propertyExpensePeriodDd = 4">		<!-- Semi-Monthly -->
									<xsl:if test="not(string(fcx:propertyExpenseAmount))">
										<xsl:value-of select="$heatZero div 24"/>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="fcx:propertyExpenseAmount = 0">
											<xsl:value-of select="$heatZero div 24"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="fcx:propertyExpenseAmount &lt; $heatMin">
												<xsl:value-of select="$heatMin div 24"/>
											</xsl:if>
											<xsl:if test="fcx:propertyExpenseAmount &gt; $heatMax">
												<xsl:value-of select="$heatMax div 24"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="fcx:propertyExpensePeriodDd = 5">		<!-- Bi-Weekly -->
									<xsl:if test="not(string(fcx:propertyExpenseAmount))">
										<xsl:value-of select="$heatZero div 26"/>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="fcx:propertyExpenseAmount = 0">
											<xsl:value-of select="$heatZero div 26"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="fcx:propertyExpenseAmount &lt; $heatMin">
												<xsl:value-of select="$heatMin div 26"/>
											</xsl:if>
											<xsl:if test="fcx:propertyExpenseAmount &gt; $heatMax">
												<xsl:value-of select="$heatMax div 26"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="fcx:propertyExpensePeriodDd = 6">		<!-- Weekly -->
									<xsl:if test="not(string(fcx:propertyExpenseAmount))">
										<xsl:value-of select="$heatZero div 52"/>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="fcx:propertyExpenseAmount = 0">
											<xsl:value-of select="$heatZero div 52"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="fcx:propertyExpenseAmount &lt; $heatMin">
												<xsl:value-of select="$heatMin div 52"/>
											</xsl:if>
											<xsl:if test="fcx:propertyExpenseAmount &gt; $heatMax">
												<xsl:value-of select="$heatMax div 52"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="fcx:propertyExpensePeriodDd &gt; 6">-1000000</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- PROPERTYEXPENSE tempalte rules - END -->
	<!-- ESCROWPAYMENT template rules - START -->
	<xsl:variable name="isEscrowPayment">
		<xsl:value-of select="$escrowPaymentTable/escrowPayments/escrowPayment[@code=$ecniLenderId]/text()"/>
	</xsl:variable>
	<xsl:variable name="escrowTypeIdValue">0</xsl:variable>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule for the field has been changed
		<xsl:variable name="escrowPaymentDescriptionValue">Municipal Tax Pmt/Taxes Municipales</xsl:variable>	-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is the new rule for the description	-->
	<xsl:template name="escrowPaymentDescriptionValue">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Municipal Tax Pmt</xsl:when>
			<xsl:otherwise>Taxes Municipales</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="escrowPaymentAmountValue">1</xsl:variable>
	<!-- ESCROWPAYMENT template rules - END -->
	<!-- ESCROWPAYMENT2 template rules - START -->
	<xsl:variable name="isEscrowPayment2">
		<xsl:choose>
			<xsl:when test="not(not(string(//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:adminFee)))">
				<xsl:value-of select="true()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="false()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="escrowTypeIdValue2">7</xsl:variable>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule for the field has been changed
		<xsl:variable name="escrowPaymentDescriptionValue2">Servicing Fee/Frais du service</xsl:variable>	-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is the new rule for the description:	-->
	<xsl:template name="escrowPaymentDescriptionValue2">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Servicing Fee</xsl:when>
			<xsl:otherwise>Frais du service</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ESCROWPAYMENT2 template rules - END -->

	<xsl:template name="provinceIdValue-long">
		<xsl:choose>
			<!-- Ticket EXP26784 -->
			<xsl:when test="fcx:address/fcx:provinceDd &gt; 13 or
							fcx:address/fcx:provinceDd &lt; 1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:address/fcx:provinceDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="streetDirectionIdValue-long">
		<xsl:choose>
			<xsl:when test="fcx:address/fcx:streetDirectionDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:address/fcx:streetDirectionDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- CREDITBUREAUREPORT template rules - START -->
	<xsl:variable name="newLine">
		<xsl:text>&#10;</xsl:text>
		<!-- &#13; (cr) was removed -DataPower issues -->
	</xsl:variable>
	<xsl:template name="creditReportValue">
		<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:creditBureauReport/fcx:creditReport">
			<xsl:value-of select="fcx:creditReportText"/>
			<xsl:value-of select="$newLine"/>
		</xsl:for-each>
	</xsl:template>
	<!-- LEN595297 ticket - this is a new variable used for the ticket-->
	<xsl:variable name="cbText">
		<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant/fcx:creditBureauReport/fcx:creditReport">
			<creditReport><xsl:value-of select="fcx:creditReportText"/></creditReport>
		</xsl:for-each>
	</xsl:variable>
	<!-- CREDITBUREAUREPORT template rules - END -->

	<!-- DOWNPAYMENTSOURCE template rules - START-->
	<xsl:template name="downPaymentSourceTypeIdValue">
		<xsl:choose>
			<!-- 4.4.1 GOLD COPY FEB. 2012: The value returned was changed from 7 to 14:	-->
			<xsl:when test="fcx:downPaymentSourceTypeDd = 8">14</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:downPaymentSourceTypeDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- DOWNPAYMENTSOURCE template rules - END -->
	<!-- CONTACT template rules - START -->
	<xsl:template name="contactPhoneNumberValue">
		<xsl:if test="fcx:phoneTypeDd = 3">
			<contactPhoneNumber>
				<xsl:value-of select="fcx:phoneNumber"/>
			</contactPhoneNumber>
			<contactPhoneNumberExtension>
				<xsl:value-of select="fcx:phoneExtension"/>
			</contactPhoneNumberExtension>
		</xsl:if>
		<xsl:if test="fcx:phoneTypeDd = 4">
			<contactFaxNumber>
				<xsl:value-of select="fcx:phoneNumber"/>
			</contactFaxNumber>
		</xsl:if>
	</xsl:template>
	<!-- ticket FXP26690: -->
	<xsl:template name="contactEmailAddressValue">
		<xsl:if test="not(not(string(fcx:contactEmailAddress)))">
			<contactEmailAddress>
				<xsl:value-of select="fcx:contactEmailAddress"/>
			</contactEmailAddress>
		</xsl:if>
	</xsl:template>
	<!-- CONTACT template rules - END -->
	<!-- ADDRESS SHORT template rules - START -->
	<xsl:template name="provinceIdValueShort">
		<xsl:choose>
			<!-- Ticket EXP26784 -->
			<xsl:when test="fcx:provinceDd &gt; 13 or
							fcx:provinceDd &lt; 1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:provinceDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ADDRESS SHORT template rules - END -->
	<!-- ADDRESSSFPSOB template rules - START -->
	<xsl:template name="provinceIdAddressSfpSobValue">
		<!-- Template used by SFP, SOB Profile and SOB Profile Agent -->
		<xsl:choose>
			<!-- Ticket EXP26784 -->
			<xsl:when test="fcx:provinceDd &gt; 13 or
							fcx:provinceDd &lt; 1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:provinceDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="addressLine1addressLine2Value">
		<!-- The following lines get the address description and put it in the variable "description": -->
		<xsl:variable name="streetTypeId">
			<xsl:value-of select="fcx:streetTypeDd"/>
		</xsl:variable>
		<xsl:variable name="streetDirectionId">
			<xsl:value-of select="fcx:streetDirectionDd"/>
		</xsl:variable>
		<xsl:variable name="provinceId">
			<xsl:value-of select="fcx:provinceDd"/>
		</xsl:variable>
		<xsl:variable name="languageId">
			<xsl:choose>
				<xsl:when test="$provinceId = 11">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="countryId">
			<xsl:value-of select="fcx:countryTypeDd"/>
		</xsl:variable>
		<xsl:variable name="description">
			<xsl:choose>
				<xsl:when test="$provinceId != 11">
					<!-- 4.4.1 GOLD COPY FEB. 2012: adding check if unitNumber exists: -->
					<xsl:if test="not(not(string(fcx:unitNumber)))">
						<xsl:value-of select="fcx:unitNumber"/>
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:value-of select="fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:streetName"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$languageId]/text()"/>
					<!-- 4.4.1 GOLD COPY FEB. 2012: Remove city, province, postal code and country!:
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:city"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:postalFsa"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:postalLdu"/>
					<xsl:text> </xsl:text>
					 4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup: 
					<xsl:value-of select="$countryTable/countries/country[@code=$countryId and @lang=$languageId]/text()"/>	-->
				</xsl:when>
				<xsl:otherwise>
					<!-- 4.4.1 GOLD COPY FEB. 2012: adding check if unitNumber exists: -->
					<xsl:if test="not(not(string(fcx:unitNumber)))">
						<xsl:value-of select="fcx:unitNumber"/>
						<xsl:text>-</xsl:text>
					</xsl:if>
					<xsl:value-of select="fcx:streetNumber"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetTypeTable/streettypes/streettype[@code=$streetTypeId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:streetName"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$streetDirTable/streetdirs/streetdir[@code=$streetDirectionId and @lang=$languageId]/text()"/>
					<!-- 4.4.1 GOLD COPY FEB. 2012: Remove city, province, postal code and country!:
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:city"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$provinceTable/provinces/province[@code=$provinceId and @lang=$languageId]/text()"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:postalFsa"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="fcx:postalLdu"/>
					<xsl:text> </xsl:text>
					4.4.1 GOLD COPY FEB. 2012: Adding language to Country lookup:
					<xsl:value-of select="$countryTable/countries/country[@code=$countryId and @lang=$languageId]/text()"/>	-->
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!--  following lines divide description into AddressLine1 and addressLine2: -->
		<!-- 4.4.1 GOLD COPY FEB. 2012: addressLine1 changed from string before first marker (' ') to string before last marker (' '): -->
		<xsl:variable name="addressLine1">
			<xsl:choose>
				<xsl:when test="string-length(normalize-space($description)) &gt; 35">
					<xsl:choose>
						<xsl:when test="contains(substring(normalize-space($description), 1, 35) , ' ')">
							<xsl:call-template name="substring-before-last">
								<xsl:with-param name="input" select="substring(normalize-space($description), 1, 35)" />
								<xsl:with-param name="marker" select="' '" />
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring(normalize-space($description), 1, 35 )"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="normalize-space($description)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="addressLine2">
			<xsl:value-of select="substring-after(normalize-space($description), $addressLine1)"/>
		</xsl:variable>
		<addressLine1>
			<xsl:value-of select="$addressLine1"/>
		</addressLine1>
		<!-- 4.4.1 GOLD COPY FEB. 2012: adding substring for addressLine2: -->
		<addressLine2>
			<xsl:value-of select="substring(normalize-space($addressLine2), 1, 35)"/>
		</addressLine2>
	</xsl:template>
	<xsl:template name="substring-before-last">
    	<xsl:param name="input"/>
    	<xsl:param name="marker" />
        <xsl:if test="contains($input, $marker)">
            <xsl:value-of select="substring-before($input, $marker)"/>
            <xsl:if test="contains(substring-after($input, $marker), $marker)">
                <xsl:value-of select="$marker"/>
            </xsl:if>
            <xsl:call-template name="substring-before-last">
                <xsl:with-param name="input" select="substring-after($input, $marker)"/>
                <xsl:with-param name="marker" select="$marker" />
            </xsl:call-template>
        </xsl:if>
	</xsl:template>
	<!-- ADDRESSSFPSOB template rules - END -->
	<!-- SOURCEFIRMPROFILE template rules - START -->
	<!-- 4.4.1 GOLD COPY FEB. 2012: The rule for sourceFirmName has been modified: 
	<xsl:template name="sourceFirmNameValue">
		<xsl:variable name="yesNo">
			<xsl:call-template name="sourceFirmNameValueLookup"/>
		</xsl:variable>
		<xsl:if test="$yesNo != 'Y'">
			<sourceFirmName>
				<xsl:value-of select="substring(fcx:firmName,1,35)"/>
			</sourceFirmName>
		</xsl:if>
	</xsl:template>
	<xsl:template name="sourceFirmNameValueLookup">
		<xsl:variable name="nodeValue">
			<xsl:choose>
				<xsl:when test="not(string(../fcx:submissionAgentProfile))">
					<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyOfficeId"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$sourceFirmNameSupressTable/sourceFirmNameSupresses/sourceFirmNameSupress[@code=$nodeValue]/text()"/>
	</xsl:template>	-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This the new rule for sourceFirmName:	--> 
	<xsl:template name="sourceFirmNameValue">
		<xsl:variable name="lookUpKey">
			<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
		</xsl:variable>
		<xsl:variable name="lookUpResult">
			<xsl:value-of select="$sourceFirmNameSupressTable/sourceFirmNameSupresses/sourceFirmNameSupress[@code=$lookUpKey]/text()"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$useLegacyFlag = 'Y'">
				<xsl:choose>
					<xsl:when test="$lookUpResult != 'Y'">
						<sourceFirmName>
							<xsl:value-of select="substring(fcx:firmName,1,35)"/>
						</sourceFirmName>
					</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<sourceFirmName>
					<xsl:value-of select="substring(fcx:firmName,1,35)"/>
				</sourceFirmName>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:variable name="systemTypeIdValue-SourceFirmProfile">
		<xsl:call-template name="systemTypeIdValue-SourceFirmProfile"/>
	</xsl:variable>
	<!-- 4.4.1 GOLD COPY FEB. 2012: The following template has been changed: 
	<xsl:template name="systemTypeIdValue-SourceFirmProfile">
		<xsl:variable name="systemTypeKeyValue">
			<xsl:choose>
				 Rule updated (K1019): 
				<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:lenderProfile/fcx:useLegacyUserId = 'Y'
								and (not(not(string(//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:legacySystem)))
								     or not(not(string(//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacySystem))))">
					<xsl:choose>
						<xsl:when test="not(string(//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile))">
							<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacySystem"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:legacySystem"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$sourceSystem"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$systemTypeTable/systemTypes/systemType[@code=$systemTypeKeyValue]/text()"/>
	</xsl:template>		-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is the new template for systemTypeIdValue-SourceFirmProfile: -->
	<xsl:template name="systemTypeIdValue-SourceFirmProfile">
		<xsl:variable name="lookUpKeyTable">
			<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:legacySystem"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$useLegacyFlag = 'Y'">
				<xsl:value-of select="$systemTypeTable/systemTypes/systemType[@code=$lookUpKeyTable]/text()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$systemTypeTable/systemTypes/systemType[@code=$sourceSystem]/text()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="sfShortNameValue">
		<xsl:value-of select="substring(fcx:firmName,1,10)"/>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (495) has been changed: 
	<xsl:template name="sourceFirmCodeValue">
		<xsl:choose>
			<xsl:when test="../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:lenderProfile/fcx:useLegacyUserId = 'Y'">
				<xsl:choose>
					<xsl:when test="not(not(string(../fcx:submissionAgentProfile/fcx:legacyOfficeId)))">
						<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyOfficeId"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="not(not(string(../fcx:agentProfile/fcx:legacyOfficeId)))">
								<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="fcx:firmCode"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not(not(string(../fcx:legacyDetails/fcx:legacyApplicationReference)))">
						<xsl:choose>
							<xsl:when test="not(not(string(../fcx:legacyDetails/fcx:legacyOfficeId)))">
								<xsl:value-of select="../fcx:legacyDetails/fcx:legacyOfficeId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="fcx:firmCode"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="fcx:firmCode"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>		-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is new template for rule (495): -->
	<xsl:template name="sourceFirmCodeValue">
		<xsl:choose>
			<xsl:when test="$useLegacyFlag = 'Y'">
				<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="fcx:firmCode"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is new template for rule (496): -->
	<xsl:template name="SFLicenseRegistrationNumberValue">
		<xsl:choose>
			<xsl:when test="not(not(string(fcx:licenseRegistrationNumber)))">
				<xsl:value-of select="fcx:licenseRegistrationNumber"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="../fcx:additionalData/@name = 'firmLicenseRegistrationNumber'">
						<xsl:choose>
							<xsl:when test="not(not(string(../fcx:additionalData)))">
								<xsl:value-of select="../fcx:additionalData"></xsl:value-of>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	
	<!-- SOURCEFIRMPROFILE template rules - END -->
	<!-- SOURCEOFBUSINESSPROFILE template rules - START -->
	<xsl:template name="SOBPShortNameValue">
		<xsl:value-of select="substring(fcx:contact/fcx:contactName/fcx:contactFirstName, 1, 1)"/>
		<xsl:value-of select="substring(fcx:contact/fcx:contactName/fcx:contactLastName, 1, 9)"/>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (522) has been changed:
	<xsl:template name="sourceOfBusinessCategoryIdValue">
		<xsl:variable name="lookupValue">
			<xsl:value-of select="$sourceOfBusinessCategoryTable/sourceOfBusinessCategories/sourceOfBusinessCategory[@code=$sourceSystem]/text()"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="not(string($lookupValue))">
				<xsl:if test="fcx:userTypeDd = 70">1</xsl:if>
				<xsl:if test="fcx:userTypeDd = 75">0</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$lookupValue"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>		-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is new template for rule (522): -->
	<xsl:template name="sourceOfBusinessCategoryIdValue">
		<xsl:variable name="sysTypeKey">
			<xsl:value-of select="$systemTypeIdValue-SourceFirmProfile"/>
		</xsl:variable>
		<xsl:variable name="lookupValue">
			<xsl:value-of select="$sourceOfBusinessCategoryTable/sourceOfBusinessCategories/sourceOfBusinessCategory[@code=$sysTypeKey]/text()"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="not(string($lookupValue))">
				<xsl:if test="fcx:userTypeDd = 70">1</xsl:if>
				<xsl:if test="fcx:userTypeDd = 75">0</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$lookupValue"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="notesValue-SourceOfBusinessProfile">
		<xsl:for-each select="fcx:contact/fcx:contactPhone">
			<xsl:if test="fcx:phoneTypeDd = 2">
				<xsl:value-of select="fcx:phoneNumber"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (526) has been changed: 
	<xsl:template name="sourceOfBusinessCodeValue">
		 modifications were introduced due to ticket FXP29812
		<xsl:choose>
			<xsl:when test="../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:lenderProfile/fcx:useLegacyUserId = 'Y'">
				<xsl:choose>
					<xsl:when test="not(not(string(../fcx:submissionAgentProfile/fcx:legacyOfficeId))) and not(not(string(../fcx:submissionAgentProfile/fcx:legacyUserId)))">
						<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyOfficeId"/>
						<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyUserId"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="not(not(string(../fcx:agentProfile/fcx:legacyOfficeId))) and not(not(string(../fcx:agentProfile/fcx:legacyUserId)))">
								<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
								<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="not(not(string(../fcx:submissionAgentProfile/fcx:userProfileId)))">
										<xsl:value-of select="../fcx:submissionAgentProfile/fcx:userProfileId"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="../fcx:agentProfile/fcx:userProfileId"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not(not(string(../fcx:legacyDetails/fcx:legacyApplicationReference))) and
									not(not(string(../fcx:legacyDetails/fcx:legacyOfficeId)))">
						<xsl:choose>
							<xsl:when test="$systemTypeIdValue-SourceOfBusinessProfile = 1">
								<xsl:choose>
									<xsl:when test="not(not(string(../fcx:submissionAgentProfile/fcx:legacyOfficeId))) and
													not(not(string(../fcx:submissionAgentProfile/fcx:legacyUserId)))">
										<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyOfficeId"/>
										<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyUserId"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyUserId"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="not(not(string(../fcx:agentProfile/fcx:legacyOfficeId))) and
													not(not(string(../fcx:agentProfile/fcx:legacyUserId)))">
										<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
										<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="not(not(string(../fcx:submissionAgentProfile/fcx:userProfileId)))">
								<xsl:value-of select="../fcx:submissionAgentProfile/fcx:userProfileId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="../fcx:agentProfile/fcx:userProfileId"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>		-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is new template for rule (526) sourceOfBusinessCode: -->
	<xsl:template name="sourceOfBusinessCodeValue">
		<xsl:choose>
			<xsl:when test="$useLegacyFlag = 'Y'">
				<xsl:choose>
					<xsl:when test="$useSubAgentFlag = 'Y'">
						<xsl:choose>
							<xsl:when test="../fcx:submissionAgentProfile/fcx:legacySystem = 'MB'">
								<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyOfficeId"/>
								<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyUserId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyUserId"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="../fcx:agentProfile/fcx:legacySystem = 'MB'">
								<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
								<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$useSubAgentFlag = 'Y'">
						<xsl:value-of select="../fcx:submissionAgentProfile/fcx:userProfileId"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="../fcx:agentProfile/fcx:userProfileId"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule template (558) has been changed - sourceOfBusinessAgentCode:
	 Express 4.4 - Start 
	<xsl:template name="sourceOfBusinessAgentCodeValue">
		<xsl:choose>
			<xsl:when test="../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:lenderProfile/fcx:useLegacyUserId = 'Y'">
				<xsl:choose>
					<xsl:when test="not(not(string(../fcx:submissionAgentProfile/fcx:legacyOfficeId))) and not(not(string(../fcx:submissionAgentProfile/fcx:legacyUserId)))">
						<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyOfficeId"/>
						<xsl:value-of select="../fcx:submissionAgentProfile/fcx:legacyUserId"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="not(not(string(../fcx:agentProfile/fcx:legacyOfficeId))) and not(not(string(../fcx:agentProfile/fcx:legacyUserId)))">
								<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
								<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="../fcx:agentProfile/fcx:userProfileId"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not(not(string(../fcx:legacyDetails/fcx:legacyApplicationReference)))">
						<xsl:choose>
							<xsl:when test="not(not(string(../fcx:legacyDetails/fcx:legacyOfficeId)))">
								<xsl:choose>
									<xsl:when test="$systemTypeIdValue-SourceOfBusinessProfile = 1">
										<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
										<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="../fcx:agentProfile/fcx:userProfileId"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="../fcx:agentProfile/fcx:userProfileId"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	
	Express 4.4 - End -->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is new template for rule (558) sourceOfBusinessAgentCode: -->
	<xsl:template name="sourceOfBusinessAgentCodeValue">
		<xsl:choose>
			<xsl:when test="$useLegacyFlag = 'Y'">
				<xsl:choose>
					<xsl:when test="../fcx:agentProfile/fcx:legacySystem = 'MB'">
						<xsl:value-of select="../fcx:agentProfile/fcx:legacyOfficeId"/>
						<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="../fcx:agentProfile/fcx:legacyUserId"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="../fcx:agentProfile/fcx:userProfileId"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="systemTypeIdValue-SourceOfBusinessProfile">
		<xsl:value-of select="$systemTypeIdValue-SourceFirmProfile"/>
	</xsl:variable>
	<!-- SOURCEOFBUSINESSPROFILE template rules - END -->
	<!-- PARTYPROFILE template rules - START -->
	<xsl:template name="partyNameValue">
		<xsl:value-of select="concat(fcx:contact/fcx:contactName/fcx:contactFirstName, ' ', fcx:contact/fcx:contactName/fcx:contactMiddleInitial, ' ', fcx:contact/fcx:contactName/fcx:contactLastName)"/>
	</xsl:template>
	<xsl:template name="partyTypeIdValue">
		<xsl:choose>
			<xsl:when test="fcx:partyTypeDd = 56">61</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="fcx:partyTypeDd = 57">61</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:partyTypeDd = 59">61</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="fcx:partyTypeDd"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ptPShortNameValue">
		<xsl:value-of select="substring(fcx:partyCompanyName, 1, 20)"/>
	</xsl:template>
	<!-- PARTYPROFILE template rules - END -->
	<!-- QUALIFYDETAIL template rules - START -->
	<xsl:template name="qualifyGdsValue">
		<xsl:choose>
			<!-- Ticket EXP27520: -->
			<!-- because the round function rounds it to the closest integer, I use the trick of multiplying by 100, 
			rounding it and	dividing by 100 to keep 2 decimals in the final result
-->
			<xsl:when test="(round(../fcx:qualifyingGds * 10000) div 100) &gt; 999.99">999.99</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="round(../fcx:qualifyingGds * 10000) div 100"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="qualifyRateValue">
		<xsl:choose>
			<xsl:when test="../fcx:qualifyRate &gt; 999.99">999.99</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="round(../fcx:qualifyRate * 100) div 100"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="qualifyTdsValue">
		<xsl:choose>
			<!-- Ticket EXP27520: -->
			<!-- because the round function rounds it to the closest integer, I use the trick of multiplying by 100, 
			rounding it and	dividing by 100 to keep 2 decimals in the final result
-->
			<xsl:when test="(round(../fcx:qualifyingTds * 10000) div 100) &gt; 999.99">999.99</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="round(../fcx:qualifyingTds * 10000) div 100"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- QUALIFYDETAIL template rules - END -->
	<!-- DEAL element rules - START -->
	<!-- ESTIMATEDCLOSINGDATE element rules - START-->
	<xsl:template name="estimatedClosingDateValue">
		<xsl:choose>
			<xsl:when test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:estimatedClosingDate)))">
				<estimatedClosingDate>
					<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:estimatedClosingDate"/>
				</estimatedClosingDate>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//fcx:loanApplication/fcx:deal/fcx:dealTypeDd = 0">
						<xsl:if test="$setclosingdateto_todayValueLookup = 'Y'">
							<estimatedClosingDate>
								<!-- TIMESTAMP -->
								<xsl:value-of select="date:date-time()"/>
							</estimatedClosingDate>
						</xsl:if>
					</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="setclosingdateto_todayValueLookup">
		<xsl:value-of select="$setclosingdateto_todayTable/setclosingdateto_todays/setclosingdateto_today[@code=$ecniLenderId]/text()"/>
	</xsl:variable>
	<!-- ESTIMATEDCLOSINGDATE element rules - END-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (605) has been changed, template is not longer necessary: 
	<xsl:template name="sourceApplicationIdValue">
		<xsl:choose>
			<xsl:when test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:legacyDetails/fcx:legacyApplicationReference)))">
				<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:legacyDetails/fcx:legacyApplicationReference"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:applicationId"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	-->
	<xsl:template name="secondaryFinancingValue">
		<xsl:variable name="concurrentMortgageTypeDdYesNo">
			<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:concurrent">
				<xsl:choose>
					<xsl:when test="fcx:mortgageTypeDd = 2">Y</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:mortgageTypeDd = 3">Y</xsl:when>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="requestedMortgageTypeDdYesNo">
			<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:requested">
				<xsl:choose>
					<xsl:when test="fcx:mortgageTypeDd = 2">Y</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="fcx:mortgageTypeDd = 3">Y</xsl:when>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="mortgagePayoffTypeDd">
			<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:mortgage">
				<xsl:if test="fcx:payoffTypeDd != 1 or fcx:payoffTypeDd != 2 ">Y</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="downPaymentSourceTypeDd">
			<xsl:for-each select="//fcx:loanApplication/fcx:deal/fcx:downPaymentSource">
				<xsl:if test="fcx:downPaymentSourceTypeDd = 10">Y</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd = 1">
				<xsl:choose>
					<xsl:when test="substring($concurrentMortgageTypeDdYesNo, 1, 1) = 'Y'">Y</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="substring($mortgagePayoffTypeDd, 1, 1) = 'Y'">Y</xsl:when>
							<xsl:when test="substring($downPaymentSourceTypeDd, 1, 1) = 'Y'">Y</xsl:when>
							<xsl:otherwise>N</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="substring($requestedMortgageTypeDdYesNo, 1, 1) = 'Y'">Y</xsl:when>
			<xsl:when test="substring($downPaymentSourceTypeDd, 1, 1) = 'Y'">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (617) now uses a template: -->
	<xsl:template name="originalMortgageNumberValue">
		<xsl:choose>
			<xsl:when test="not(not(string(//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:currentMortgageNumber)))">
				<xsl:value-of select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage/fcx:currentMortgageNumber"/>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="dealPurposeIdValue">
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 0">0</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 1">17</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 2">
			<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant">
				<xsl:if test="fcx:primaryApplicantFlag = 'Y'">
					<xsl:choose>
						<xsl:when test="fcx:existingClient = 'Y'">5</xsl:when>
						<xsl:otherwise>4</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 3">
			<xsl:for-each select="//fcx:loanApplication/fcx:applicantGroup/fcx:applicant">
				<xsl:if test="fcx:primaryApplicantFlag = 'Y'">
					<xsl:choose>
						<xsl:when test="fcx:existingClient = 'Y'">7</xsl:when>
						<xsl:otherwise>6</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 4">1</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 6">18</xsl:if>
		<!-- Ticket FXP26837 (purposeDd = 5 added): -->
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 5">11</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 7">19</xsl:if>
	</xsl:template>
	<xsl:template name="specialFeatureIdValue">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:deal/fcx:dealTypeDd = 1">1</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:singleProgressiveTypeDd = 1">3</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (641) now uses a template: -->
	<xsl:template name="refExistingMtgNumberValue">
		<xsl:if test="fcx:mortgageTypeDd = ../../fcx:mortgage/fcx:requested/fcx:mortgageTypeDd">
			<xsl:if test="not(not(string(fcx:currentMortgageNumber)))">
				<xsl:value-of select="fcx:currentMortgageNumber"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<xsl:template name="interestTypeIdValue">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:interestTypeDd = -1">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:interestTypeDd"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="paymentFrequencyIdValue">
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:paymentFrequencyDd != -1">
			<paymentFrequencyId>
				<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:paymentFrequencyDd"/>
			</paymentFrequencyId>
		</xsl:if>
	</xsl:template>
	<!-- street ticket - variable was changed from office to officeSystemSource:
	<xsl:template name="electronicSystemSourceValue">
		<xsl:choose>
			<xsl:when test="$user != ''">
				<xsl:value-of select="substring(concat($officeSystemSource, '.', $user), 1, 15)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring($officeSystemSource, 1, 15)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	-->
	
		<!-- street1c ticket (November 2012): New logic -->
	<xsl:template name="electronicSystemSourceValue">
		<xsl:choose>
			<xsl:when test="$useLegacyFlag = 'Y'">
				<xsl:choose>
					<xsl:when test="$user != ''">
						<xsl:value-of select="substring(concat($officeSystemSource, '.', $user), 1, 15)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring($officeSystemSource, 1, 15)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$useSubAgentFlag = 'Y'">
						<xsl:value-of select="substring(concat(//fcx:loanApplication/fcx:deal/fcx:firmProfile/fcx:firmCode,
												'.',
												 //fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:userProfileId), 1, 15)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring(concat(//fcx:loanApplication/fcx:deal/fcx:firmProfile/fcx:firmCode,
												 '.',
												 //fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:userProfileId), 1, 15)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="requestedSolicitorNameValue">
		<xsl:for-each select="//fcx:loanApplication/fcx:participant">
			<xsl:if test="fcx:partyTypeDd = 50">
				<xsl:value-of select="fcx:contact/fcx:contactName/fcx:contactFirstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="fcx:contact/fcx:contactName/fcx:contactLastName"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="lienPositionIdValue">
		<!-- Temporary change requested by John Rodrigues with FXP30999 (Loan Decision Mapping Update for mortgageTypeDd). 
			 These are the original values that are being removed(values since 4.2 as per ticket FXP26603):-->
		<!-- With ticket FXP31052, these values were restored: -->
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd = 1">0</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd = 2">1</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd = 3">3</xsl:if>
		<!-- Temporary change requested by John Rodrigues with FXP30999: These are the new values -->
		<!-- With ticket FXP31052, these values were rollbacked for testing -->
		<!--
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd = 1">1</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd = 2">2</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd = 3">3</xsl:if>
		-->
	</xsl:template>
	<xsl:template name="preApprovalValue">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:deal/fcx:dealTypeDd = 1">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- street ticket - variable was changed from office to officeSystemSource: (street1b ticket modified this logic)
	<xsl:template name="sourceSystemMailBoxNBRValue">
		<xsl:value-of select="substring($officeSystemSource, 1, 20)"/>
	</xsl:template>	-->
	
	<!-- street1b ticket (November 2012): new logic: (street1c modified this logic!!!)
	<xsl:template name="sourceSystemMailBoxNBRValue">
		<xsl:choose>
			<xsl:when test="$user != ''">
				<xsl:value-of select="substring(concat($officeSystemSource, '.', $user), 1, 20)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring($officeSystemSource, 1, 20)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	-->
	
	<!-- street1c ticket (November 2012): New logic -->
	<xsl:template name="sourceSystemMailBoxNBRValue">
		<xsl:choose>
			<xsl:when test="$useLegacyFlag = 'Y'">
				<xsl:choose>
					<xsl:when test="$user != ''">
						<xsl:value-of select="substring(concat($officeSystemSource, '.', $user), 1, 20)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring($officeSystemSource, 1, 20)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$useSubAgentFlag = 'Y'">
						<xsl:value-of select="substring(concat(//fcx:loanApplication/fcx:deal/fcx:firmProfile/fcx:firmCode,
												 '.', 
												 //fcx:loanApplication/fcx:deal/fcx:submissionAgentProfile/fcx:userProfileId), 1, 20)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring(concat(//fcx:loanApplication/fcx:deal/fcx:firmProfile/fcx:firmCode,
												 '.',
												 //fcx:loanApplication/fcx:deal/fcx:agentProfile/fcx:userProfileId), 1, 20)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="taxPayorIdValue">
		<xsl:if test="//fcx:loanApplication/fcx:deal/fcx:taxPayorDd != -1">
			<taxPayorId>
				<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:taxPayorDd"/>
			</taxPayorId>
		</xsl:if>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This rule (756) template has been changed:
	<xsl:template name="systemTypeIdValue-deal">
		<xsl:variable name="systemTypeKeyValue-deal">
			<xsl:choose>
				<xsl:when test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:legacyDetails/fcx:legacyApplicationReference)))">
					<xsl:choose>
						<xsl:when test="not(not(string(//fcx:loanApplication/fcx:deal/fcx:legacyDetails/fcx:legacyOfficeId)))">
							<xsl:value-of select="//fcx:loanApplication/fcx:deal/fcx:legacyDetails/fcx:legacySystem"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$sourceSystem"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$sourceSystem"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$systemTypeTable/systemTypes/systemType[@code=$systemTypeKeyValue-deal]/text()"/>
	</xsl:template>	-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This is new rule (756) template: -->
	<xsl:template name="systemTypeIdValue-deal">
		<xsl:value-of select="$systemTypeTable/systemTypes/systemType[@code=$sourceSystem]/text()"/>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: (FXP32289)This rule (769) has been changed, template not longer necessary:
	<xsl:template name="MIExistingPolicyNumberValue">
		<xsl:variable name="miNumbersString">
			<xsl:for-each select="//fcx:loanApplication/fcx:mortgage/fcx:concurrent">
				<xsl:if test="not(not(string(fcx:miReferenceNumber)))">
					<xsl:value-of select="fcx:miReferenceNumber"/>
					<xsl:text>?</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="substring-before($miNumbersString, '?')"/>
	</xsl:template>	-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: (FXP32289)This is new template for rule (769): -->
	<xsl:template name="MIExistingPolicyNumberValue">
		<xsl:if test="fcx:mortgageTypeDd = ../../fcx:mortgage/fcx:requested/fcx:mortgageTypeDd">
			<xsl:if test="not(not(string(fcx:miReferenceNumber)))">
				<xsl:value-of select="fcx:miReferenceNumber"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: Rule template (771) has been modified and for-each also added:
	<xsl:template name="PAPurchasePriceValue">
		<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property">
			<xsl:if test="fcx:primaryPropertyFlag = 'Y'">
				<xsl:if test="../../fcx:deal/fcx:dealTypeDd = 1">
					<xsl:value-of select="fcx:purchasePrice"/>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>		-->
	<!-- 4.4.1 GOLD COPY FEB. 2012: Rule template (771) this is the new template
			I added the for-each to make it easier to use relative paths (similar to CIBC rule): -->
	<xsl:template name="PAPurchasePriceValue">
		<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property">
				<xsl:if test="../../fcx:deal/fcx:dealTypeDd = 1">
					<xsl:choose>
						<xsl:when test = "not(not(string(fcx:purchasePrice))) and fcx:propertyValueIndexId = 'P'">
							<xsl:value-of select = "fcx:purchasePrice"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test = "not(not(string(fcx:estimatedAppraisalValue))) and fcx:propertyValueIndexId = 'E'">
									<xsl:value-of select = "fcx:estimatedAppraisalValue"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test = "not(not(string(fcx:actualAppraisalValue))) and fcx:propertyValueIndexId = 'A'">
											<xsl:value-of select = "fcx:actualAppraisalValue"/>
										</xsl:when>
										<xsl:otherwise/> <!-- if none of the aboves is true then no value is assigned -->
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:variable name="checkNotesFlagValue">Y</xsl:variable>
	<xsl:template name="progressAdvanceValue">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:singleProgressiveTypeDd = 1">Y</xsl:when>
			<xsl:otherwise>N</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="refiOrigPurchasePriceValue">
		<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property">
			<xsl:if test="fcx:primaryPropertyFlag = 'Y'">
				<xsl:value-of select="fcx:originalPurchasePrice"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="refiOrigPurchaseDateValue">
		<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:property">
			<xsl:if test="fcx:primaryPropertyFlag = 'Y'">
				<xsl:value-of select="fcx:refiOrigPurchaseDate"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="mccMarketTypeValue">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:marketSubmission = 'Y'">2</xsl:when>
			<xsl:otherwise>
				<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:marketSubmission = 'N'">1</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="productTypeIdValue">
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:loanTypeDd = 0">1</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:loanTypeDd = 1">2</xsl:if>
		<xsl:if test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:loanTypeDd = 2">3</xsl:if>
	</xsl:template>
	<xsl:template name="affiliationProgramIdValue">
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:affiliationProgramDd &gt; 9999">9999</xsl:when>
			<xsl:otherwise>
				<!-- Ticket FXP26907, addtional validation for empty values: -->
				<xsl:if test="not(not(string(//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:affiliationProgramDd)))">
					<affiliationProgramId>
						<xsl:value-of select="round(//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:affiliationProgramDd)"/>
					</affiliationProgramId>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: Rule template (816) is now inside a for-each: 
	<xsl:template name="refiProductTypeIdValue">
		 Ticket FXP26692: 
		<xsl:choose>
			<xsl:when test="//fcx:loanApplication/fcx:deal/fcx:dealPurposeDd = 1">
				<refiProductTypeId>
					<xsl:value-of select="//fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:loanTypeDd"/>
				</refiProductTypeId>
			</xsl:when>
			<xsl:otherwise>
				 Ticket EXP26884, rule changed: 
				<xsl:for-each select="//fcx:loanApplication/fcx:subjectProperty/fcx:existingMortgage">
					<xsl:if test="fcx:mortgageTypeDd = //fcx:loanApplication/fcx:mortgage/fcx:requested/fcx:mortgageTypeDd">
						<xsl:if test="fcx:loanTypeDd = 0">
							<refiProductTypeId>1</refiProductTypeId>
						</xsl:if>
						<xsl:if test="fcx:loanTypeDd = 1">
							<refiProductTypeId>2</refiProductTypeId>
						</xsl:if>
						<xsl:if test="fcx:loanTypeDd = 2">
							<refiProductTypeId>3</refiProductTypeId>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>		-->
	<xsl:template name="refiProductTypeIdValue">
		<xsl:if test="fcx:mortgageTypeDd = ../../fcx:mortgage/fcx:requested/fcx:mortgageTypeDd">
			<xsl:if test="fcx:loanTypeDd = 0">
				<refiProductTypeId>1</refiProductTypeId>
			</xsl:if>
			<xsl:if test="fcx:loanTypeDd = 1">
				<refiProductTypeId>2</refiProductTypeId>
			</xsl:if>
			<xsl:if test="fcx:loanTypeDd = 2">
				<refiProductTypeId>3</refiProductTypeId>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!-- DEAL element rules - END -->
	<!-- DOWNPAYMENTSOURCE-ADDTIONALMAPPING template rules - START -->
	<xsl:template name="downPaymentSourceTypeIdValue-additionalMapping">
		<xsl:variable name="value">
			<xsl:if test="fcx:mortgageTypeDd = 1">12</xsl:if>
			<xsl:if test="fcx:mortgageTypeDd = 2">10</xsl:if>
			<xsl:if test="fcx:mortgageTypeDd = 3">10</xsl:if>
		</xsl:variable>
		<downPaymentSourceTypeId>
			<xsl:value-of select="$value"/>
		</downPaymentSourceTypeId>
		<DPSDescription>
			<xsl:choose>
				<xsl:when test="$value = 12">First Mortgage</xsl:when>
				<xsl:otherwise>Secondary financing</xsl:otherwise>
			</xsl:choose>
		</DPSDescription>
	</xsl:template>
	<!-- DOWNPAYMENTSOURCE-ADDTIONALMAPPING template rules - END -->
	<!-- borrowerLiabilityMortgage template rules - START -->
	<!-- 4.4.1 GOLD COPY FEB. 2012: This the new template for rule (925): -->
	<xsl:template name="liabilityDescriptionBorrowerLiabMrtgValue">
		<xsl:variable name="description">
			<xsl:choose>
				<xsl:when test="../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'">Subject Property: </xsl:when>
				<xsl:otherwise>Propriété visée: </xsl:otherwise>
			</xsl:choose>
			<xsl:text> </xsl:text>
			<xsl:value-of select="fcx:existingMortgageHolder"/>	<!-- no need to check for existency -->
			<xsl:text> </xsl:text>
			<xsl:if test="not(not(string(fcx:maturityDate)))">
				<xsl:choose>
					<xsl:when test="../../fcx:mortgage/fcx:requested/fcx:lenderSubmission/fcx:language = 'en_ca'"> Maturity Date: </xsl:when>
					<xsl:otherwise> Date d’échéance: </xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:value-of select="fcx:maturityDate"/>
		</xsl:variable>
		<xsl:value-of select="substring(normalize-space($description), 1, 80)"/>
	</xsl:template>
	<!-- 4.4.1 GOLD COPY FEB. 2012: This the new value for rule (931): -->
	<xsl:variable name="percentOutGDSBorrowerLiabMrtgValue">0</xsl:variable>
	<!-- borrowerLiabilityMortgage template rules - END -->
	<!--  #################################################################################
		  ################################   RULES - END    ###############################
	      #################################################################################
-->
</xsl:stylesheet>
